#!/bin/python3
import os

injection_rate = [
    0.000277,
    0.002487,
    0.004697,
    0.006907,
    0.009117,
    0.011327,
    0.013537,
    0.015747,
    0.017957,
    0.020167,
    0.022377,
    0.024587,
    0.026797,
    0.029007,
    0.031217,
    0.033427,
    0.035637,
    0.037847,
    0.040057,
    0.042267,
    0.044477,
    0.046687,
    0.048897,
    0.051107,
    0.053317,
    0.055527,
    0.055556
]

routing_paths = ["min", "multipath"]
num_vcs = [12, 32, 64]
vc_buf_size = [18, 144, 1024]
internal_speedup = [1.0, 2.0]
injection_process = ["bernoulli", "on_off"]
sim_type = ["throughput", "latency"]
for temp_sim_type in sim_type:
    for temp_routing_path in routing_paths:
        for temp_num_vcs in num_vcs:
            for temp_vc_buff_size in vc_buf_size:
                for temp_internal_speedup in internal_speedup:
                    for temp_injection_process in injection_process:
                        for temp_injection_rate in injection_rate:
                            output_file = (
                                "results/"
                                + temp_sim_type
                                + str(temp_routing_path)
                                + str(temp_num_vcs)
                                + str(temp_vc_buff_size)
                                + str(int(temp_internal_speedup))
                                + str(temp_injection_rate)
                                + str(temp_injection_process)
                                + ".txt"
                            )
                            command = (
                                "./booksim "
                                + "networks/mezzanine_config "
                                + "injection_rate="
                                + str(temp_injection_rate)
                                + " routing_function="
                                + temp_routing_path
                                + " num_vcs="
                                + str(temp_num_vcs)
                                + " vc_buf_size="
                                + str(temp_vc_buff_size)
                                + " internal_speedup="
                                + str(temp_internal_speedup)
                                + " injection_process="
                                + temp_injection_process
                                + " sim_type="
                                + temp_sim_type
                                + " burst_alpha=0.0038461538 burst_beta=0.1 "
                                + " > "
                                + output_file
                            )
                            os.system(command)
