#!/bin/python3
import os
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams.update({"figure.max_open_warning": 0})

x = 0
y = [[]]

injection_rate = [
    0.000277,
    0.002487,
    0.004697,
    0.006907,
    0.009117,
    0.011327,
    0.013537,
    0.015747,
    0.017957,
    0.020167,
    0.022377,
    0.024587,
    0.026797,
    0.029007,
    0.031217,
    0.033427,
    0.035637,
    0.037847,
    0.040057,
    0.042267,
    0.044477,
    0.046687,
    0.048897,
    0.051107,
    0.053317,
    0.055527,
    0.055556,
]

normalized_injection_rate = []
for i in injection_rate:
    normalized_injection_rate.append(i * 18)


def extract_network_latency(lines):
    return lines[5].split()[4]


def extract_min_network_latency(lines):
    return lines[6].split()[2]


def extract_max_network_latency(lines):
    return lines[7].split()[2]


def extract_packet_rate(lines):
    return lines[17].split()[5]


def extract_min_packet_rate(lines):
    return lines[18].split()[2]


def extract_max_packet_rate(lines):
    return lines[19].split()[2]


def plot(
    injection_rate,
    y,
    sim_type,
    routing_paths,
    vc_buf_size,
    num_vcs,
    internal_speedup,
    injection_process,
):

    fig = plt.figure()
    ax = plt.axes()
    # ax.set_xlim([0,0.06])

    if "latency" == sim_type:
        plt.ylabel("Average Latency in Cycles")
        ax.set_ylim([0, 4000])
    elif "throughput" == sim_type:
        plt.ylabel("Average Throughput bytes per Cycle")
        ax.set_ylim([0, 50])
    plt.xlabel("Injection Rate")
    temp_y_len = 100000
    for i in range(len(y)):
        if len(y[i]) != 0:
            if len(y[i]) < temp_y_len:
                temp_y_len = len(y[i])

    for i in range(len(y)):
        y_len = len(y[i])
        if y_len != 0:
            if i < 3:
                temp_buf_size = vc_buf_size[i % 3]
                plt.plot(
                    injection_rate[0:temp_y_len],
                    y[i][0:temp_y_len],
                    marker="o",
                    label=routing_paths[0] + " buf_size=" + str(temp_buf_size),
                )
            else:
                temp_buf_size = vc_buf_size[i % 3]
                plt.plot(
                    injection_rate[0:temp_y_len],
                    y[i][0:temp_y_len],
                    marker="x",
                    label=routing_paths[1] + " buf_size=" + str(temp_buf_size),
                )

    if injection_process == "on_off":
        name = (
            "num_vcs:"
            + str(num_vcs)
            + os.linesep
            + "internal_speedup:"
            + str(internal_speedup)
            + os.linesep
            + "bursty"
        )
    else:
        name = (
            "num_vcs:"
            + str(num_vcs)
            + os.linesep
            + "internal_speedup:"
            + str(internal_speedup)
            + os.linesep
            + injection_process
        )

    plt.title(name)
    plt.legend()
    # plt.yscale('log', basey=10)
    # plt.show()
    plt.savefig(
        "plots/"
        + sim_type
        + "num_vcs"
        + str(num_vcs)
        + "internal_speedup"
        + str(int(internal_speedup))
        + injection_process
        + ".pdf"
    )
    plt.clf()
    # exit(1)


def parsefile(file, sim_type):
    global x, y
    lines = file.readlines()[-30:-1]
    if "Simulation unstable," in lines[-1]:
        return

    if "latency" == sim_type:
        y[x].append(float(extract_network_latency(lines)))
        # print(extract_min_network_latency(lines))
        # print(extract_max_network_latency(lines))
    elif "throughput" == sim_type:
        y[x].append(float(extract_packet_rate(lines)) * 288 * 8)
        # print(extract_min_packet_rate(lines))
        # print(extract_max_packet_rate(lines))
    else:
        assert 0


# plot()
routing_paths_names = ["singlepath", "multipath"]
routing_paths = ["min", "multipath"]
num_vcs = [12, 32, 64]
vc_buf_size = [18, 144, 1024]
internal_speedup = [1.0, 2.0]
injection_process = ["bernoulli", "on_off"]
sim_type = ["latency", "throughput"]
for temp_sim_type in sim_type:
    for temp_num_vcs in num_vcs:
        for temp_internal_speedup in internal_speedup:
            for temp_injection_process in injection_process:
                for temp_routing_path in routing_paths:
                    for temp_vc_buff_size in vc_buf_size:
                        for temp_injection_rate in injection_rate:
                            output_file = (
                                "gxanth_results/"
                                + temp_sim_type
                                + str(temp_routing_path)
                                + str(temp_num_vcs)
                                + str(temp_vc_buff_size)
                                + str(int(temp_internal_speedup))
                                + str(temp_injection_rate)
                                + str(temp_injection_process)
                                + ".txt"
                            )

                            f = open(output_file, "r")
                            parsefile(f, temp_sim_type)
                            f.close()
                        x = x + 1
                        y.append([])
                plot(
                    normalized_injection_rate,
                    y,
                    temp_sim_type,
                    routing_paths_names,
                    vc_buf_size,
                    temp_num_vcs,
                    temp_internal_speedup,
                    temp_injection_process,
                )
                x = 0
                y = [[]]
