#!/bin/python3
import os

###
# run this with shift-custom1 and shift-custom2, where:
# shift-custom1: all fpgas of mezzanine 0 qfdb 0 send to all fpgas of mezzanine 1 qfdb 0
# shift-custom2: all fpgas of mezzanine 0 qfdb 0 send to a random fpga of mezzanine 1 (random destination every time)
###

injection_rate = [
    0.000277,
    0.002487,
    0.004697,
    0.006907,
    0.009117,
    0.011327,
    0.013537,
    0.015747,
    0.017957,
    0.020167,
    0.022377,
    0.024587,
    0.026797,
    0.029007,
    0.031217,
    0.033427,
    0.035637,
    0.037847,
    0.040057,
    0.042267,
    0.044477,
    0.046687,
    0.048897,
    0.051107,
    0.053317,
    0.055527,
    0.055556
]

routing_paths = ["min", "multipath"]
num_vcs = [12, 32, 64] #
vc_buf_size = [18, 144, 1024] #
internal_speedup = [1.0, 2.0] #
traffic_patterns = ["mez_unbalanced"]
mez_cmd = ["shift-custom2"]
sim_type = ["throughput", "latency"]
mezW = [0.0, 0.125, 0.25, 0.375, 0.5, 0.6125, 0.75, 0.875, 1.0]
for temp_sim_type in sim_type:
    for temp_routing_path in routing_paths:
        for temp_num_vcs in num_vcs:
            for temp_vc_buff_size in vc_buf_size:
                for temp_internal_speedup in internal_speedup:
                    for temp_traffic_patterns in traffic_patterns:
                        for temp_mezW in mezW:
	                        for temp_injection_rate in injection_rate:
		                        for temp_mez_cmd in mez_cmd:
		                            output_file = (
		                                "results/"
		                                + temp_sim_type
		                                + str(temp_routing_path)
		                                + str(temp_num_vcs)
		                                + str(temp_vc_buff_size)
		                                + str(int(temp_internal_speedup))
		                                + str(temp_injection_rate)
		                                + str(temp_mezW)
		                                + str(temp_traffic_patterns)
		                                + str(temp_mez_cmd)
		                                + ".txt"
		                            )
		                            command = (
		                                "./booksim "
		                                + "networks/mezzanine_config "
		                                + "injection_rate="
		                                + str(temp_injection_rate)
		                                + " routing_function="
		                                + temp_routing_path
		                                + " num_vcs="
		                                + str(temp_num_vcs)
		                                + " vc_buf_size="
		                                + str(temp_vc_buff_size)
		                                + " internal_speedup="
		                                + str(temp_internal_speedup)
		                                + " traffic="
		                                + temp_traffic_patterns
		                                + " mezPredefinedPatternType="
		                                + temp_mez_cmd
		                                + " mezW="
		                                + str(temp_mezW)
		                                + " sim_type="
		                                + temp_sim_type
		                                + " burst_alpha=0.0038461538 burst_beta=0.1 "
		                                + " > "
		                                + output_file
		                            )
		                            os.system(command)
