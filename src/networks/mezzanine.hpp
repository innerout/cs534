#ifndef _MEZANNINE_HPP_
#define _MEZANNINE_HPP_

#include "network.hpp"
#include "routefunc.hpp"
#include <cassert>
#include <string>
#include <map>
#include <list>

#define MEZZANINE_SIZE 4 //the number of QFDBs in a Mezzanine board
#define QFDB_SIZE 4 //num of nodes in a QFDB
#define SPINE_ROUTERS 4

class Mezzanine : public Network{
	int _n;

	//associtation between  nodes and routers
	map<int, int > node_list;
	//[link type][src router][conected router]=(port, latency)
	vector<map<int,  map<int, pair<int,int> > > > router_list;
	//stores minimal routing information from every router to every node
	//[router][dest_node]=port
	vector<map<int, int> > routing_table;
	vector<map<int, vector<int> > > multipath_routing_table;

	void _ComputeSize( const Configuration &config );
	void _BuildNet( const Configuration &config );
	void readFile();
	void buildRoutingTable(const Configuration &config);
	void multiroute(int r_start);
	void route(int r_start);

public:
	Mezzanine(const Configuration &config, const string & name);
	~Mezzanine();

	int GetN( ) const;
	int GetK( ) const{ return -1;}

  static void RegisterRoutingFunctions();
  double Capacity( ) const {return -1;}
  void InsertRandomFaults( const Configuration &config ){}

};

void min_mezzanine( const Router *r, const Flit *f, int in_channel,
		    OutputSet *outputs, bool inject );

void multipath_mezzanine(const Router *r, const Flit *f, int in_channel,
		    				OutputSet *outputs, bool inject);

bool intraMezzanineDest(const Router *r, const Flit *f);

#endif //_MEZANNINE_HPP_