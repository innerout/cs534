#include "mezzanine.hpp"
#include <fstream>
#include <sstream>
#include <limits>
#include <algorithm>
#include <random>
#include "random_utils.hpp"

#define BREAKPOINT asm volatile("int3;");
extern map<int, int>* global_routing_table;
map<int, vector<int> >* global_multipath_routing_table = NULL;
vector<map<int,  map<int, pair<int,int> > > >* to_router_list;
//map[router][packet_id] = port
vector<map<int,int> > multipath_packets_routes;
int * my_n;
Mezzanine::Mezzanine(const Configuration &config, const string & name) : Network(config, name){
	router_list.resize(2);
	to_router_list = &router_list;
	my_n = &_n;
	_ComputeSize( config );
	_Alloc( );
	_BuildNet( config );
}

Mezzanine::~Mezzanine(){
	for(int i = 0; i < 2; ++i) {
		for(map<int, map<int, pair<int,int> > >::iterator iter = router_list[i].begin();
		iter != router_list[i].end();
		++iter) {
			iter->second.clear();
		}
	}
}

void Mezzanine::_ComputeSize(const Configuration &config){
	int i, qfdb_iter, f_iter, my_index, opposite_index, f1_index, j;
	// num of mezzanines
	_n = config.GetInt( "n" );
	_channels =0;

	//init router's structures
	for(i = 0;i < _n*MEZZANINE_SIZE*QFDB_SIZE;i++){
		node_list[i] = i;
		router_list[0][i] = map<int, pair<int,int> >();//nodes
		router_list[0][i][i] = pair<int, int>(-1,1);
		router_list[1][i] = map<int, pair<int,int> >();//routers
	}

	for(i = _n*MEZZANINE_SIZE*QFDB_SIZE;i < _n*MEZZANINE_SIZE*QFDB_SIZE+SPINE_ROUTERS;i++){
		router_list[0][i] = map<int, pair<int,int> >();//nodes
		router_list[1][i] = map<int, pair<int,int> >();//routers
	}

	//cout<<"******************router listing**********************\n";
	for(i = 0;i < _n;i++){
		for(qfdb_iter = 0;qfdb_iter < MEZZANINE_SIZE;qfdb_iter++){
			for(f_iter = 0;f_iter < QFDB_SIZE;f_iter++){
				my_index = (i*MEZZANINE_SIZE*QFDB_SIZE) + (qfdb_iter*QFDB_SIZE)+f_iter;
				opposite_index = (i*MEZZANINE_SIZE*QFDB_SIZE) + (qfdb_iter*QFDB_SIZE);
				for(j = opposite_index;j < opposite_index + QFDB_SIZE;j++){//inter-QFDB
					if(my_index != j){
						router_list[1][my_index][j] = pair<int, int>(-1,1);
					}
				}
				opposite_index = (i*MEZZANINE_SIZE*QFDB_SIZE);
				if(my_index%4 == 0){
					for(j = opposite_index;j < opposite_index + MEZZANINE_SIZE*QFDB_SIZE;j = j + MEZZANINE_SIZE){//intra-Mezzanine
						if(my_index != j){
							router_list[1][my_index][j] = pair<int, int>(-1,1);
						}
					}
				}
			}
		}
	}

	//Spine routers
	for(i = 0;i < SPINE_ROUTERS;i++){
		for(j = 0;j < _n;j++){
			my_index = i + _n * MEZZANINE_SIZE * QFDB_SIZE;
			f1_index = (j * MEZZANINE_SIZE * QFDB_SIZE) + (i * QFDB_SIZE);
			router_list[1][my_index][f1_index] = pair<int, int>(-1,1);
			router_list[1][f1_index][my_index] = pair<int, int>(-1,1);
		}
	}


	//map verification, make sure the information contained in both maps
	//are the same
	assert(router_list[0].size() == router_list[1].size());

	//traffic generator assumes node list is sequential and starts at 0
	vector<int> node_check;
	for(map<int,int>::iterator i = node_list.begin();
	i!=node_list.end();
	i++){
		node_check.push_back(i->first);
	}
	sort(node_check.begin(), node_check.end());
	for(size_t i = 0; i<node_check.size(); i++){
		if((size_t)node_check[i] != i){
		//cout<<"Mezzanine:booksim trafficmanager assumes sequential node numbering starting at 0\n";
		assert(false);
		}
	}

	map<int,   map<int, pair<int,int> > >::iterator iter3;
	/*cout<<"========================Network File Parsed=================\n";
	cout<<"******************node listing**********************\n";
	map<int,  int >::iterator iter;
	for(iter = node_list.begin(); iter!=node_list.end(); iter++){
		cout<<"Node "<<iter->first;
		cout<<"\tRouter "<<iter->second<<endl;
	}

	cout<<"\n****************router to node listing*************\n";
	for(iter3 = router_list[0].begin(); iter3!=router_list[0].end(); iter3++){
		cout<<"Router "<<iter3->first<<endl;
		map<int, pair<int,int> >::iterator iter2;
		for(iter2 = iter3->second.begin();
		iter2!=iter3->second.end();
		iter2++){
			cout<<"\t Node "<<iter2->first<<" lat "<<iter2->second.second<<endl;
		}
	}
*/
	//cout<<"\n*****************router to router listing************\n";
	for(iter3 = router_list[1].begin(); iter3!=router_list[1].end(); iter3++){
		//cout<<"Router "<<iter3->first<<endl;
		map<int, pair<int,int> >::iterator iter2;
		if(iter3->second.size() == 0){
			//cout<<"Caution Router "<<iter3->first
			//<<" is not connected to any other Router\n"<<endl;
		}
		for(iter2 = iter3->second.begin();
		iter2!=iter3->second.end();
		iter2++){
			//cout<<"\t Router "<<iter2->first<<" lat "<<iter2->second.second<<endl;
			_channels++;
		}
	}

	_size = router_list[1].size();
	_nodes = node_list.size();
}

void Mezzanine::_BuildNet(const Configuration &config){

	//I need to keep track the output ports for each router during build
	int * outport = (int*)malloc(sizeof(int)*_size);
	for(int i = 0; i<_size; i++){outport[i] = 0;}

	//cout<<"==========================Node to Router =====================\n";
	//adding the injection/ejection chanenls first
	map<int,   map<int, pair<int,int> > >::iterator niter;
	for(niter = router_list[0].begin(); niter!=router_list[0].end(); niter++){
		map<int,   map<int, pair<int,int> > >::iterator riter = router_list[1].find(niter->first);
		//calculate radix
		int radix = niter->second.size()+riter->second.size();
		int node = niter->first;
		//cout<<"router "<<node<<" radix "<<radix<<endl;
		//decalre the routers
		ostringstream router_name;
		router_name << "router";
		router_name << "_" <<  node ;
		_routers[node] = Router::NewRouter( config, this, router_name.str( ),
							node, radix, radix );
		_timed_modules.push_back(_routers[node]);
		//add injeciton ejection channels
		map<int, pair<int,int> >::iterator nniter;
		for(nniter = niter->second.begin();nniter!=niter->second.end(); nniter++){
			int link = nniter->first;
			//add the outport port assined to the map
			(niter->second)[link].first = outport[node];
			outport[node]++;
			//cout<<"\t connected to node "<<link<<" at outport "<<nniter->second.first
			//<<" lat "<<nniter->second.second<<endl;
			_inject[link]->SetLatency(nniter->second.second);
			_inject_cred[link]->SetLatency(nniter->second.second);
			_eject[link]->SetLatency(nniter->second.second);
			_eject_cred[link]->SetLatency(nniter->second.second);

			_routers[node]->AddInputChannel( _inject[link], _inject_cred[link] );
			_routers[node]->AddOutputChannel( _eject[link], _eject_cred[link] );
		}
	}

	//cout<<"==========================Router to Router =====================\n";
	//add inter router channels
	//since there is no way to systematically number the channels we just start from 0
	//the map, is a mapping of output->input
	int channel_count = 0;
	for(niter = router_list[0].begin(); niter!=router_list[0].end(); niter++){
		map<int,   map<int, pair<int,int> > >::iterator riter = router_list[1].find(niter->first);
		int node = niter->first;
		map<int, pair<int,int> >::iterator rriter;
		//cout<<"router "<<node<<endl;
		for(rriter = riter->second.begin();rriter!=riter->second.end(); rriter++){
			int other_node = rriter->first;
			int link = channel_count;
			//add the outport port assined to the map
			(riter->second)[other_node].first = outport[node];
			outport[node]++;
			//cout<<"\t connected to router "<<other_node<<" using link "<<link
			//<<" at outport "<<rriter->second.first
			//<<" lat "<<rriter->second.second<<endl;

			_chan[link]->SetLatency(rriter->second.second);
			_chan_cred[link]->SetLatency(rriter->second.second);

			_routers[node]->AddOutputChannel( _chan[link], _chan_cred[link] );
			_routers[other_node]->AddInputChannel( _chan[link], _chan_cred[link]);
			channel_count++;
		}
	}
	buildRoutingTable(config);
	
}	

void Mezzanine::RegisterRoutingFunctions() {
	gRoutingFunctionMap["min_mezzanine"] = &min_mezzanine;
	gRoutingFunctionMap["multipath_mezzanine"] = &multipath_mezzanine;
}

void multipath_mezzanine(const Router *r, const Flit *f, int in_channel,
		    				OutputSet *outputs, bool inject){
	int out_port = -1;
	Flit *f_help = (Flit *)f;
	if(!inject){
		int choices_num = global_multipath_routing_table[r->GetID()][f->dest].size();
		std::default_random_engine generator;
		std::uniform_int_distribution<int> distribution(0, choices_num-1);
		//std::cout << "choices_num is: " << choices_num << std::endl;
		//per-packet multipath routing 

		if(f->head){
			if(!intraMezzanineDest(r, f)){
				if(f->shortest_path_flag){
					//out_port = global_routing_table[r->GetID()][f->dest];
					out_port = global_multipath_routing_table[r->GetID()][f->dest][0];
				} else {
					out_port = global_multipath_routing_table[r->GetID()][f->dest][distribution(generator)];
					f_help->shortest_path_flag = 1;
					//std::cout << "rand is: " << rand << std::endl;
					//std::cout << "outport is: " << out_port << std::endl;
				}
				multipath_packets_routes[r->GetID()][f->pid] = out_port;
			} else {
				//out_port = global_routing_table[r->GetID()][f->dest];
				out_port = global_multipath_routing_table[r->GetID()][f->dest][0];
				multipath_packets_routes[r->GetID()][f->pid] = out_port;
			}
		} else {
			//out_port = global_routing_table[r->GetID()][f->dest];
			out_port = multipath_packets_routes[r->GetID()][f->pid];
		}
	}

	int vcBegin = 0, vcEnd = gNumVCs-1;
	if ( f->type == Flit::READ_REQUEST ) {
		vcBegin = gReadReqBeginVC;
		vcEnd   = gReadReqEndVC;
	} else if ( f->type == Flit::WRITE_REQUEST ) {
		vcBegin = gWriteReqBeginVC;
		vcEnd   = gWriteReqEndVC;
	} else if ( f->type ==  Flit::READ_REPLY ) {
		vcBegin = gReadReplyBeginVC;
		vcEnd   = gReadReplyEndVC;
	} else if ( f->type ==  Flit::WRITE_REPLY ) {
		vcBegin = gWriteReplyBeginVC;
		vcEnd   = gWriteReplyEndVC;
	}

	outputs->Clear( );

	outputs->AddRange( out_port , vcBegin, vcEnd );
}

bool intraMezzanineDest(const Router *r, const Flit *f){
	int mezzanine_destination = f->dest / (MEZZANINE_SIZE * QFDB_SIZE);

	if(mezzanine_destination == (r->GetID() / (MEZZANINE_SIZE * QFDB_SIZE) ) ){
		return true;
	}
	return false;
}

void min_mezzanine( const Router *r, const Flit *f, int in_channel,
		    OutputSet *outputs, bool inject ){
	
	int out_port=-1;
	if(!inject){		////modify it for multipath implementation
		assert(global_routing_table[r->GetID()].count(f->dest)!=0);
		out_port=global_routing_table[r->GetID()][f->dest]; //r->GetID: returns the id of router
	}														//f->dest:

	int vcBegin = 0, vcEnd = gNumVCs-1;
	if ( f->type == Flit::READ_REQUEST ) {
		vcBegin = gReadReqBeginVC;
		vcEnd   = gReadReqEndVC;
	} else if ( f->type == Flit::WRITE_REQUEST ) {
		vcBegin = gWriteReqBeginVC;
		vcEnd   = gWriteReqEndVC;
	} else if ( f->type ==  Flit::READ_REPLY ) {
		vcBegin = gReadReplyBeginVC;
		vcEnd   = gReadReplyEndVC;
	} else if ( f->type ==  Flit::WRITE_REPLY ) {
		vcBegin = gWriteReplyBeginVC;
		vcEnd   = gWriteReplyEndVC;
	}

	outputs->Clear( );

	outputs->AddRange( out_port , vcBegin, vcEnd );
}


void Mezzanine::buildRoutingTable(const Configuration &config){
	cout<<"========================== Routing table  =====================\n";
	string r_funct = config.GetStr("routing_function");
	if(r_funct == "min"){
		routing_table.resize(_size);
		for(int i = 0; i<_size; i++){
			route(i);
		}
		global_routing_table = &routing_table[0];
	} else if(r_funct == "multipath"){
		multipath_routing_table.resize(_size);
		multipath_packets_routes.resize(_size);
		//routing_table.resize(_size);
		for(int i = 0; i<_size; i++){
			multipath_packets_routes[i] = map<int,int>();
			multiroute(i);
			//route(i);
		}
		global_multipath_routing_table = &multipath_routing_table[0];
		//global_routing_table = &routing_table[0];
	} else {
		cout << "Unknown routing function!!!!" << endl;
	}
}

void Mezzanine::multiroute(int r_start){
	int* dist = new int[_size];
	int* prev = new int[_size];
	set<int> rlist;

	for(int i = 0; i<_size; i++){
		dist[i] =  numeric_limits<int>::max();
		prev[i] = -1;
		rlist.insert(i);
	}

	dist[r_start] = 0;
	// cout << endl <<"Router " << r_start << endl;

	while(!rlist.empty()){
		//find min
		int min_dist = numeric_limits<int>::max();
		int min_cand = -1;
		for(set<int>::iterator i = rlist.begin();
		i!=rlist.end();
		i++){
			// cout  << "dist  " << dist[*i] << "  i " << *i << endl;
			if(dist[*i]<min_dist){
				min_dist = dist[*i];
				min_cand = *i;
			}
		}
		rlist.erase(min_cand);

		//neighbor
		for(map<int,pair<int,int> >::iterator i = router_list[1][min_cand].begin();
		    i!=router_list[1][min_cand].end();
		i++){
			int new_dist = dist[min_cand] + i->second.second;//distance is hops not cycles
			if(new_dist < dist[i->first]){
				dist[i->first] = new_dist;
				prev[i->first] = min_cand;
			}
		}
	}

	//multipath_routing_table[r_start][iter->first].resize(MEZZANINE_SIZE);
  //post process from the prev list

	for(int i = 0; i<_size; i++){
		// cout << "Router "<< i << "Count " << router_list[0][i].size() << "dist array [i]"<<dist[i] << endl ;

		if(prev[i] ==-1){ //self
			assert(i == r_start);
			for(map<int, pair<int, int> >::iterator iter = router_list[0][i].begin();
			iter!=router_list[0][i].end();
			iter++){
				multipath_routing_table[r_start][iter->first].push_back(iter->second.first);
				//cout<<"node "<<iter->first<<" port "<< iter->second.first<<endl;
			}
		} else {
			int distance=0;
			int neighbor=i;
			while(prev[neighbor]!=r_start){
				assert(router_list[1][neighbor].count(prev[neighbor])>0);
				distance+=router_list[1][prev[neighbor]][neighbor].second;//REVERSE lat
				neighbor= prev[neighbor];
			}
			distance+=router_list[1][prev[neighbor]][neighbor].second;//lat

			assert( router_list[1][r_start].count(neighbor)!=0);
			int port = router_list[1][r_start][neighbor].first;
			for(map<int, pair<int,int> >::iterator iter = router_list[0][i].begin();
			iter!=router_list[0][i].end();
			iter++){
				multipath_routing_table[r_start][iter->first].push_back(port);
				// cout<<"node "<<iter->first<<" port "<< port<<" dist "<<distance <<endl;
				if(r_start % QFDB_SIZE == 0 && (iter->first < r_start || iter->first >= r_start + QFDB_SIZE) ){
					for(map<int, pair<int,int> >::iterator iter2 = router_list[1][r_start].begin();
					iter2!=router_list[1][r_start].end();
					iter2++){
						if((iter2->first < r_start || (iter2->first >= r_start + QFDB_SIZE)) &&  neighbor != iter2->first)
							multipath_routing_table[r_start][iter->first]
							.push_back(router_list[1][r_start][iter2->first].first);
					}

				}
			}
		}
	}
}

void Mezzanine::route(int r_start){
	int* dist = new int[_size];
	int* prev = new int[_size];
	set<int> rlist;

	for(int i = 0; i<_size; i++){
		dist[i] =  numeric_limits<int>::max();
		prev[i] = -1;
		rlist.insert(i);
	}

	dist[r_start] = 0;
	// cout << endl <<"Router " << r_start << endl;

	while(!rlist.empty()){
		//find min
		int min_dist = numeric_limits<int>::max();
		int min_cand = -1;
		for(set<int>::iterator i = rlist.begin();
		i!=rlist.end();
		i++){
			// cout  << "dist  " << dist[*i] << "  i " << *i << endl;
			if(dist[*i]<min_dist){
				min_dist = dist[*i];
				min_cand = *i;
			}
		}
		rlist.erase(min_cand);

		//neighbor
		for(map<int,pair<int,int> >::iterator i = router_list[1][min_cand].begin();
		    i!=router_list[1][min_cand].end();
		i++){
			int new_dist = dist[min_cand] + i->second.second;//distance is hops not cycles
			if(new_dist < dist[i->first]){
				dist[i->first] = new_dist;
				prev[i->first] = min_cand;
			}
		}
	}

  //post process from the prev list

	for(int i = 0; i<_size; i++){
		// cout << "Router "<< i << "Count " << router_list[0][i].size() << "dist array [i]"<<dist[i] << endl ;

		if(prev[i] ==-1){ //self
			assert(i == r_start);
			for(map<int, pair<int, int> >::iterator iter = router_list[0][i].begin();
			iter!=router_list[0][i].end();
			iter++){
				routing_table[r_start][iter->first]=iter->second.first;
				//cout<<"node "<<iter->first<<" port "<< iter->second.first<<endl;
			}
		} else {
			int distance=0;
			int neighbor=i;
			while(prev[neighbor]!=r_start){
				assert(router_list[1][neighbor].count(prev[neighbor])>0);
				distance+=router_list[1][prev[neighbor]][neighbor].second;//REVERSE lat
				neighbor= prev[neighbor];
			}
			distance+=router_list[1][prev[neighbor]][neighbor].second;//lat

			assert( router_list[1][r_start].count(neighbor)!=0);
			int port = router_list[1][r_start][neighbor].first;
			for(map<int, pair<int,int> >::iterator iter = router_list[0][i].begin();
			iter!=router_list[0][i].end();
			iter++){
				routing_table[r_start][iter->first]=port;
				//cout<<"node "<<iter->first<<" port "<< port<<" dist "<<distance <<endl;
			}
		}
	}
	// exit(EXIT_FAILURE);
	}