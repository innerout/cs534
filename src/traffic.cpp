// $Id$

/*
 Copyright (c) 2007-2015, Trustees of The Leland Stanford Junior University
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 Redistributions of source code must retain the above copyright notice, this 
 list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <sstream>
#include <ctime>
#include "random_utils.hpp"
#include "traffic.hpp"

TrafficPattern::TrafficPattern(int nodes)
: _nodes(nodes)
{
  if(nodes <= 0) {
    cout << "Error: Traffic patterns require at least one node." << endl;
    exit(-1);
  }
}

void TrafficPattern::reset()
{

}

TrafficPattern * TrafficPattern::New(string const & pattern, int nodes, 
				     Configuration const * const config)
{
  string pattern_name;
  string param_str;
  size_t left = pattern.find_first_of('(');
  if(left == string::npos) {
    pattern_name = pattern;
  } else {
    pattern_name = pattern.substr(0, left);
    size_t right = pattern.find_last_of(')');
    if(right == string::npos) {
      param_str = pattern.substr(left+1);
    } else {
      param_str = pattern.substr(left+1, right-left-1);
    }
  }
  vector<string> params = tokenize_str(param_str);
  
  TrafficPattern * result = NULL;
  if(pattern_name == "bitcomp") {
    result = new BitCompTrafficPattern(nodes);
  } else if(pattern_name == "transpose") {
    result = new TransposeTrafficPattern(nodes);
  } else if(pattern_name == "bitrev") {
    result = new BitRevTrafficPattern(nodes);
  } else if(pattern_name == "shuffle") {
    result = new ShuffleTrafficPattern(nodes);
  } else if(pattern_name == "randperm") {
    int perm_seed = -1;
    if(params.empty()) {
      if(config) {
	if(config->GetStr("perm_seed") == "time") {
	  perm_seed = int(time(NULL));
	  cout << "SEED: perm_seed=" << perm_seed << endl;
	} else {
	  perm_seed = config->GetInt("perm_seed");
	}
      } else {
	cout << "Error: Missing parameter for random permutation traffic pattern: " << pattern << endl;
	exit(-1);
      }
    } else {
      perm_seed = atoi(params[0].c_str());
    }
    result = new RandomPermutationTrafficPattern(nodes, perm_seed);
  } else if(pattern_name == "uniform") {
    result = new UniformRandomTrafficPattern(nodes);
  } else if(pattern_name == "background") {
    vector<int> excludes = tokenize_int(params[0]);
    result = new UniformBackgroundTrafficPattern(nodes, excludes);
  } else if(pattern_name == "diagonal") {
    result = new DiagonalTrafficPattern(nodes);
  } else if(pattern_name == "asymmetric") {
    result = new AsymmetricTrafficPattern(nodes);
  } else if(pattern_name == "taper64") {
    result = new Taper64TrafficPattern(nodes);
  } else if(pattern_name == "bad_dragon") {
    bool missing_params = false;
    int k = -1;
    if(params.size() < 1) {
      if(config) {
	k = config->GetInt("k");
      } else {
	missing_params = true;
      }
    } else {
      k = atoi(params[0].c_str());
    }
    int n = -1;
    if(params.size() < 2) {
      if(config) {
	n = config->GetInt("n");
      } else {
	missing_params = true;
      }
    } else {
      n = atoi(params[1].c_str());
    }
    if(missing_params) {
      cout << "Error: Missing parameters for dragonfly bad permutation traffic pattern: " << pattern << endl;
      exit(-1);
    }
    result = new BadPermDFlyTrafficPattern(nodes, k, n);
  } else if((pattern_name == "tornado") || (pattern_name == "neighbor") ||
	    (pattern_name == "badperm_yarc")) {
    bool missing_params = false;
    int k = -1;
    if(params.size() < 1) {
      if(config) {
	k = config->GetInt("k");
      } else {
	missing_params = true;
      }
    } else {
      k = atoi(params[0].c_str());
    }
    int n = -1;
    if(params.size() < 2) {
      if(config) {
	n = config->GetInt("n");
      } else {
	missing_params = true;
      }
    } else {
      n = atoi(params[1].c_str());
    }
    int xr = -1;
    if(params.size() < 3) {
      if(config) {
	xr = config->GetInt("xr");
      } else {
	missing_params = true;
      }
    } else {
      xr = atoi(params[2].c_str());
    }
    if(missing_params) {
      cout << "Error: Missing parameters for digit permutation traffic pattern: " << pattern << endl;
      exit(-1);
    }
    if(pattern_name == "tornado") {
      result = new TornadoTrafficPattern(nodes, k, n, xr);
    } else if(pattern_name == "neighbor") {
      result = new NeighborTrafficPattern(nodes, k, n, xr);
    } else if(pattern_name == "badperm_yarc") {
      result = new BadPermYarcTrafficPattern(nodes, k, n, xr);
    }
  } else if(pattern_name == "hotspot") {
    if(params.empty()) {
      params.push_back("-1");
    } 
    vector<int> hotspots = tokenize_int(params[0]);
    for(size_t i = 0; i < hotspots.size(); ++i) {
      if(hotspots[i] < 0) {
	hotspots[i] = RandomInt(nodes - 1);
      }
    }
    vector<int> rates;
    if(params.size() >= 2) {
      rates = tokenize_int(params[1]);
      rates.resize(hotspots.size(), rates.back());
    } else {
      rates.resize(hotspots.size(), 1);
    }
    result = new HotSpotTrafficPattern(nodes, hotspots, rates);
  } else if (pattern_name == "mez_unbalanced") {
	int n = -1;
	double mezW = -1;
	if(config) {
		n = config->GetInt("n");
		mezW = config->GetFloat("mezW");
	}
	result = new MezUnbalancedTrafficPattern(nodes, n, mezW, config->GetStr("mezPredefinedPatternType"));
} else {
    cout << "Error: Unknown traffic pattern: " << pattern << endl;
    exit(-1);
  }
  return result;
}

PermutationTrafficPattern::PermutationTrafficPattern(int nodes)
  : TrafficPattern(nodes)
{
  
}

BitPermutationTrafficPattern::BitPermutationTrafficPattern(int nodes)
  : PermutationTrafficPattern(nodes)
{
  if((nodes & -nodes) != nodes) {
    cout << "Error: Bit permutation traffic patterns require the number of "
	 << "nodes to be a power of two." << endl;
    exit(-1);
  }
}

BitCompTrafficPattern::BitCompTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{
  
}

int BitCompTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const mask = _nodes - 1;
  return ~source & mask;
}

TransposeTrafficPattern::TransposeTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes), _shift(0)
{
  while(nodes >>= 1) {
    ++_shift;
  }
  if(_shift % 2) {
    cout << "Error: Transpose traffic pattern requires the number of nodes to "
	 << "be an even power of two." << endl;
    exit(-1);
  }
  _shift >>= 1;
}

int TransposeTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const mask_lo = (1 << _shift) - 1;
  int const mask_hi = mask_lo << _shift;
  return (((source >> _shift) & mask_lo) | ((source << _shift) & mask_hi));
}

BitRevTrafficPattern::BitRevTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{
  
}

int BitRevTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int result = 0;
  for(int n = _nodes; n > 1; n >>= 1) {
    result = (result << 1) | (source % 2);
    source >>= 1;
  }
  return result;
}

ShuffleTrafficPattern::ShuffleTrafficPattern(int nodes)
  : BitPermutationTrafficPattern(nodes)
{

}

int ShuffleTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const shifted = source << 1;
  return ((shifted & (_nodes - 1)) | bool(shifted & _nodes));
}

DigitPermutationTrafficPattern::DigitPermutationTrafficPattern(int nodes, int k,
							       int n, int xr)
  : PermutationTrafficPattern(nodes), _k(k), _n(n), _xr(xr)
{
  
}

TornadoTrafficPattern::TornadoTrafficPattern(int nodes, int k, int n, int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int TornadoTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int offset = 1;
  int result = 0;

  for(int n = 0; n < _n; ++n) {
    result += offset *
      (((source / offset) % (_xr * _k) + ((_xr * _k + 1) / 2 - 1)) % (_xr * _k));
    offset *= (_xr * _k);
  }
  return result;
}

NeighborTrafficPattern::NeighborTrafficPattern(int nodes, int k, int n, int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int NeighborTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int offset = 1;
  int result = 0;
  
  for(int n = 0; n < _n; ++n) {
    result += offset *
      (((source / offset) % (_xr * _k) + 1) % (_xr * _k));
    offset *= (_xr * _k);
  }
  return result;
}

RandomPermutationTrafficPattern::RandomPermutationTrafficPattern(int nodes, 
								 int seed)
  : TrafficPattern(nodes)
{
  _dest.resize(nodes);
  randomize(seed);
}

void RandomPermutationTrafficPattern::randomize(int seed)
{
  vector<long> save_x;
  vector<double> save_u;
  SaveRandomState(save_x, save_u);
  RandomSeed(seed);

  _dest.assign(_nodes, -1);

  for(int i = 0; i < _nodes; ++i) {
    int ind = RandomInt(_nodes - 1 - i);

    int j = 0;
    int cnt = 0;
    while((cnt < ind) || (_dest[j] != -1)) {
      if(_dest[j] == -1) {
	++cnt;
      }
      ++j;
      assert(j < _nodes);
    }

    _dest[j] = i;
  }

  RestoreRandomState(save_x, save_u); 
}

int RandomPermutationTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  assert((_dest[source] >= 0) && (_dest[source] < _nodes));
  return _dest[source];
}

RandomTrafficPattern::RandomTrafficPattern(int nodes)
  : TrafficPattern(nodes)
{

}

UniformRandomTrafficPattern::UniformRandomTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int UniformRandomTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  return RandomInt(_nodes - 1);
}

UniformBackgroundTrafficPattern::UniformBackgroundTrafficPattern(int nodes, vector<int> excluded_nodes)
  : RandomTrafficPattern(nodes)
{
  for(size_t i = 0; i < excluded_nodes.size(); ++i) {
    int const node = excluded_nodes[i];
    assert((node >= 0) && (node < _nodes));
    _excluded.insert(node);
  }
}

int UniformBackgroundTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int result;

  do {
    result = RandomInt(_nodes - 1);
  } while(_excluded.count(result) > 0);

  return result;
}

DiagonalTrafficPattern::DiagonalTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int DiagonalTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  return ((RandomInt(2) == 0) ? ((source + 1) % _nodes) : source);
}

AsymmetricTrafficPattern::AsymmetricTrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{

}

int AsymmetricTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const half = _nodes / 2;
  return (source % half) + (RandomInt(1) ? half : 0);
}

Taper64TrafficPattern::Taper64TrafficPattern(int nodes)
  : RandomTrafficPattern(nodes)
{
  if(nodes != 64) {
    cout << "Error: Tthe Taper64 traffic pattern requires the number of nodes "
	 << "to be exactly 64." << endl;
    exit(-1);
  }
}

int Taper64TrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  if(RandomInt(1)) {
    return ((64 + source + 8 * (RandomInt(2) - 1) + (RandomInt(2) - 1)) % 64);
  } else {
    return RandomInt(_nodes - 1);
  }
}

MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern(int nodes, int n, double w, string const &pattern) : RandomTrafficPattern(nodes) {
	 // store a local copy of the parameters
	_n = n; // n: mezzanine count
	_w = w; // w: w parameter of 2.2 ("unbalance factor")
	// pattern: parameter 'mezPredefinedPatternType' that defines what pattern to use as predefind destination 
	printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> n: %d, w: %f, pattern: %s\n", n, w, pattern.c_str());
	
	printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> [INFO] supported mezPredefinedPatternType types: 'fixed?_distinct?_shift-{none|first|mirror|inter|qfdb|mez|qfdbmez}?_random-{none|qfdb|mez|all}?_alt-{qfdb|mez|all}?'\n");
	
	ParsePattern(pattern);
	
	_dest.resize(nodes); // dest will include the preferred destination for each node
	if (_isFixed) {
		for (int i = 0; i < nodes; i++) {
			_dest[i] = ComputeDestination(i, nodes);
			
			if (_dest[i] == i) {
				printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> assigned node%3d -> %3d (=)\n", i, _dest[i]);
			} else {
				printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> assigned node%3d -> %3d\n", i, _dest[i]);
			}
		}
	}
	
	for (int i = 0; i < 20; i++) { // show 20 test maps for random nodes
		int index = RandomInt(nodes - 1);
		int des = dest(index);
		if (!_isFixed) {
			printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> test: node%3d -> %3d\n", index, des);
		} else if (des != _dest[index]) {
			printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> test: node%3d -> %3d [%3d is predefined]\n", index, des, _dest[index]);
		} else {
			printf("<MezUnbalancedTrafficPattern::MezUnbalancedTrafficPattern> test: node%3d ->     [%3d is predefined]\n", index, _dest[index]);
		}
	}
}

void MezUnbalancedTrafficPattern::ParsePattern(string const &pattern) {
	/*
	Parameters:
	isFixed = true; // contains "fixed" (shall the position be recalculated again every time? (makes sense only when random))
	allowSelf = true; // does not contain "distinct"? (can the destination be itself? (ignored by randomness when w))
	int shiftTargetArea = "none|first|mirror|inta|qfdb|mez|qfdbmez|custom1|custom2"; // contains shift-'value'? value else "none" (shift dstination by value)
	int randomRange = "none|qfdb|mez|all"; // contains random-'value'? value else "none" (choose a random node in the same value)
	int altRandomArea = "qfdb|mez|all"; // contains alt-'value'? value else "all" (when w choose a random node of the same value)
	*/
	
	if (pattern.find("fixed") != string::npos) {
		_isFixed = true;
	}
	if (pattern.find("distinct") != string::npos) {
		_allowSelf = false;
	}
	if (pattern.find("shift-") != string::npos) {
		if (pattern.find("shift-none") != string::npos) {
			_shiftTargetArea = "none";
		} else if (pattern.find("shift-mirror") != string::npos) {
			_shiftTargetArea = "mirror";
		} else if (pattern.find("shift-intra") != string::npos) {
			_shiftTargetArea = "intra";
		} else if (pattern.find("shift-qfdbmez") != string::npos) {
			_shiftTargetArea = "qfdbmez";
		} else if (pattern.find("shift-mez") != string::npos) {
			_shiftTargetArea = "mez";
		} else if (pattern.find("shift-qfdb") != string::npos) {
			_shiftTargetArea = "qfdb";
		} else if (pattern.find("shift-first") != string::npos) {
			_shiftTargetArea = "first";
		} else if (pattern.find("shift-custom1") != string::npos) {
			_shiftTargetArea = "custom1";
		} else if (pattern.find("shift-custom2") != string::npos) {
			_shiftTargetArea = "custom2";
		} else {
			printf("<MezUnbalancedTrafficPattern::ParsePattern> WARN: shaftTargetArea parameter not supported\n");
		}
	}
	if (pattern.find("random-") != string::npos) {
		if (pattern.find("random-none") != string::npos) {
			_randomRange = "none";
		} else if (pattern.find("random-qfdb") != string::npos) {
			_randomRange = "qfdb";
		} else if (pattern.find("random-mez") != string::npos) {
			_randomRange = "mez";
		} else if (pattern.find("random-all") != string::npos) {
			_randomRange = "all";
		} else {
			printf("<MezUnbalancedTrafficPattern::ParsePattern> WARN: randomRange parameter not supported\n");
		}
	}
	if (pattern.find("alt-") != string::npos) {
		if (pattern.find("alt-qfdb") != string::npos) {
			_altRandomArea = "qfdb";
		} else if (pattern.find("alt-mez") != string::npos) {
			_altRandomArea = "mez";
		} else if (pattern.find("alt-all") != string::npos) {
			_altRandomArea = "all";
		} else {
			printf("<MezUnbalancedTrafficPattern::ParsePattern> WARN: altRandomArea parameter not supported\n");
		}
	}
	
	if (_randomRange == "none") {
		_isFixed = true;
	}
	
	printf("<MezUnbalancedTrafficPattern::ParsePattern> Parsed: isFixed: %d, allowSelf: %d, shiftTargetArea: %s, randomRange: %s, altRandomArea: %s\n", _isFixed, _allowSelf, _shiftTargetArea.c_str(), _randomRange.c_str(), _altRandomArea.c_str());
}

int MezUnbalancedTrafficPattern::ComputeDestination(int index, int count) {
	int nodes_per_qfdb = 4;
	int nodes_per_mezzanine = nodes_per_qfdb * 4;
	
	int dest = index;
	
	// custom1: 0-3 -> 16-19
	if (_shiftTargetArea == "custom1") {
		if (dest <= 3) {
			dest += 16;
		} else {
			return dest;
		}
	}
	
	// custom2: 0-3 -> 16 - 31
	if (_shiftTargetArea == "custom2") {
		if (dest <= 3) {
			dest += 16;
			_randomRange = "mez";
		} else {
			return dest;
		}
	}

  // custom3: 0 -> 16
	if (_shiftTargetArea == "custom3") {
		if (dest == 0) {
			dest = 16;
			_randomRange = "none";
		} else {
			return -1;
		}
	}

  // custom3: 0 -> 20
	if (_shiftTargetArea == "custom4") {
		if (dest == 0) {
			dest = 20;
			_randomRange = "none";
		} else {
			return -1;
		}
	}
	
	// 1. shift (none|mirror|inter|qfdb|mez|qfdbmez)
	if (_shiftTargetArea == "mirror") {
		return count - 1 - index;
	} if (_shiftTargetArea == "intra") { // shift by 1, but stay in same qfdb
		if (dest % nodes_per_qfdb == nodes_per_qfdb - 1) {
			dest -= nodes_per_qfdb - 1;
		} else {
			dest += 1;
		}
	} else if (_shiftTargetArea == "qfdb") { // shift by qfdb, but stay in same mezzanine
		if (dest % nodes_per_mezzanine >= nodes_per_mezzanine - nodes_per_qfdb) {
			dest -= nodes_per_mezzanine - nodes_per_qfdb;
		} else {
			dest += nodes_per_qfdb;
		}
	} else if (_shiftTargetArea == "mez") {
		dest += nodes_per_mezzanine;
		dest = dest % count; // wrap around in case we are past last index
	} else if (_shiftTargetArea == "qfdbmez") { // shift by qfdb + mezzanine
		if (dest % nodes_per_mezzanine >= nodes_per_mezzanine - nodes_per_qfdb) {
			dest -= nodes_per_mezzanine - nodes_per_qfdb;
		} else {
			dest += nodes_per_qfdb;
		}
		dest += nodes_per_mezzanine;
		dest = dest % count; // wrap around in case we are past last index
	} else if (_shiftTargetArea == "first") { // set to first node when outside of random range
		if ((dest >= 1 && _randomRange == "none")
			|| (dest >= nodes_per_qfdb && _randomRange == "qfdb")
			|| (dest >= nodes_per_mezzanine && _randomRange == "mez")) {
			dest = 0;
		} else {
			return dest; // return identity otherwise
		}
	}
	
	// 2. random (none|qfdb|mez|all)
	bool done = false;
	while (!done) { // loop in case self is not allowed (then repeat while same)
		if (_randomRange == "qfdb") {
			dest -= dest % nodes_per_qfdb; // go to index of first node of qfdb
			dest += RandomInt(nodes_per_qfdb - 1);
		} else if (_randomRange == "mez") {
			dest -= dest % nodes_per_mezzanine; // go to index of first node of mez
			dest += RandomInt(nodes_per_mezzanine - 1);
		} else  if (_randomRange == "all") {
			dest = RandomInt(count - 1);
		}
		
		done = true;
		if (dest == index && !_allowSelf) {
			done = false;
		}
	}
	
	return dest;
}

int MezUnbalancedTrafficPattern::dest(int source)
{
	assert((source >= 0) && (source < _nodes));
	
	int nodes_per_qfdb = 4;
	int nodes_per_mezzanine = nodes_per_qfdb * 4;
	
	if (RandomFloat() < _w) { // choose predefined destination
		if (_isFixed) {
			return _dest[source];
		} else {
			return ComputeDestination(source, _nodes);
		}
	} else { // choose random destination
		int des = source;
		// take random destination
		if (_altRandomArea == "qfdb") {
			des -= des % nodes_per_qfdb; // go to index of first node of qfdb
			des += RandomInt(nodes_per_qfdb - 1);
		} else if (_altRandomArea == "mez") {
			des -= des % nodes_per_mezzanine; // go to index of first node of mez
			des += RandomInt(nodes_per_mezzanine - 1);
		} else  if (_altRandomArea == "all") {
			des = RandomInt(_nodes - 1);
		}
		
		return des;
	}
}

BadPermDFlyTrafficPattern::BadPermDFlyTrafficPattern(int nodes, int k, int n)
  : DigitPermutationTrafficPattern(nodes, k, n, 1)
{
  
}

int BadPermDFlyTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  int const grp_size_routers = 2 * _k;
  int const grp_size_nodes = grp_size_routers * _k;

  return ((RandomInt(grp_size_nodes - 1) + ((source / grp_size_nodes) + 1) * grp_size_nodes) % _nodes);
}

BadPermYarcTrafficPattern::BadPermYarcTrafficPattern(int nodes, int k, int n, 
						     int xr)
  : DigitPermutationTrafficPattern(nodes, k, n, xr)
{

}

int BadPermYarcTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));
  int const row = source / (_xr * _k);
  return RandomInt((_xr * _k) - 1) * (_xr * _k) + row;
}

HotSpotTrafficPattern::HotSpotTrafficPattern(int nodes, vector<int> hotspots, 
					     vector<int> rates)
  : TrafficPattern(nodes), _hotspots(hotspots), _rates(rates), _max_val(-1)
{
  assert(!_hotspots.empty());
  size_t const size = _hotspots.size();
  _rates.resize(size, _rates.empty() ? 1 : _rates.back());
  for(size_t i = 0; i < size; ++i) {
    int const hotspot = _hotspots[i];
    assert((hotspot >= 0) && (hotspot < _nodes));
    int const rate = _rates[i];
    assert(rate > 0);
    _max_val += rate;
  }
}

int HotSpotTrafficPattern::dest(int source)
{
  assert((source >= 0) && (source < _nodes));

  if(_hotspots.size() == 1) {
    return _hotspots[0];
  }

  int pct = RandomInt(_max_val);

  for(size_t i = 0; i < (_hotspots.size() - 1); ++i) {
    int const limit = _rates[i];
    if(limit > pct) {
      return _hotspots[i];
    } else {
      pct -= limit;
    }
  }
  assert(_rates.back() > pct);
  return _hotspots.back();
}
