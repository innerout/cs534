BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 239.564
	minimum = 22
	maximum = 727
Network latency average = 233.687
	minimum = 22
	maximum = 706
Slowest packet = 1239
Flit latency average = 203.844
	minimum = 5
	maximum = 689
Slowest flit = 22319
Fragmentation average = 42.7141
	minimum = 0
	maximum = 441
Injected packet rate average = 0.0246823
	minimum = 0.014 (at node 179)
	maximum = 0.039 (at node 160)
Accepted packet rate average = 0.0137708
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 51)
Injected flit rate average = 0.440411
	minimum = 0.237 (at node 179)
	maximum = 0.695 (at node 160)
Accepted flit rate average= 0.258276
	minimum = 0.089 (at node 174)
	maximum = 0.408 (at node 76)
Injected packet length average = 17.8432
Accepted packet length average = 18.7553
Total in-flight flits = 35713 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 431.143
	minimum = 22
	maximum = 1341
Network latency average = 424.762
	minimum = 22
	maximum = 1284
Slowest packet = 3253
Flit latency average = 391.904
	minimum = 5
	maximum = 1311
Slowest flit = 58701
Fragmentation average = 47.2745
	minimum = 0
	maximum = 663
Injected packet rate average = 0.0244792
	minimum = 0.016 (at node 17)
	maximum = 0.034 (at node 160)
Accepted packet rate average = 0.0144583
	minimum = 0.0075 (at node 153)
	maximum = 0.0215 (at node 154)
Injected flit rate average = 0.438792
	minimum = 0.288 (at node 17)
	maximum = 0.606 (at node 160)
Accepted flit rate average= 0.265352
	minimum = 0.139 (at node 153)
	maximum = 0.387 (at node 154)
Injected packet length average = 17.9251
Accepted packet length average = 18.3528
Total in-flight flits = 67305 (0 measured)
latency change    = 0.444352
throughput change = 0.0266647
Class 0:
Packet latency average = 958.741
	minimum = 22
	maximum = 1845
Network latency average = 951.701
	minimum = 22
	maximum = 1830
Slowest packet = 5518
Flit latency average = 920.847
	minimum = 5
	maximum = 1813
Slowest flit = 99341
Fragmentation average = 50.4982
	minimum = 0
	maximum = 446
Injected packet rate average = 0.024599
	minimum = 0.014 (at node 8)
	maximum = 0.037 (at node 48)
Accepted packet rate average = 0.0154948
	minimum = 0.006 (at node 149)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.442599
	minimum = 0.252 (at node 8)
	maximum = 0.649 (at node 48)
Accepted flit rate average= 0.276891
	minimum = 0.112 (at node 149)
	maximum = 0.505 (at node 34)
Injected packet length average = 17.9926
Accepted packet length average = 17.8699
Total in-flight flits = 99156 (0 measured)
latency change    = 0.550303
throughput change = 0.0416737
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 42.0534
	minimum = 22
	maximum = 111
Network latency average = 34.9186
	minimum = 22
	maximum = 83
Slowest packet = 14152
Flit latency average = 1253.84
	minimum = 5
	maximum = 2317
Slowest flit = 141850
Fragmentation average = 6.05852
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0249896
	minimum = 0.011 (at node 14)
	maximum = 0.041 (at node 91)
Accepted packet rate average = 0.0154896
	minimum = 0.007 (at node 17)
	maximum = 0.029 (at node 19)
Injected flit rate average = 0.449177
	minimum = 0.183 (at node 14)
	maximum = 0.741 (at node 91)
Accepted flit rate average= 0.281729
	minimum = 0.127 (at node 17)
	maximum = 0.497 (at node 19)
Injected packet length average = 17.9746
Accepted packet length average = 18.1883
Total in-flight flits = 131428 (79160 measured)
latency change    = 21.7982
throughput change = 0.0171744
Class 0:
Packet latency average = 414.938
	minimum = 22
	maximum = 1975
Network latency average = 408.204
	minimum = 22
	maximum = 1975
Slowest packet = 14201
Flit latency average = 1431.47
	minimum = 5
	maximum = 2766
Slowest flit = 187463
Fragmentation average = 14.6571
	minimum = 0
	maximum = 185
Injected packet rate average = 0.0245599
	minimum = 0.0175 (at node 14)
	maximum = 0.035 (at node 91)
Accepted packet rate average = 0.0155365
	minimum = 0.0095 (at node 89)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.442172
	minimum = 0.3075 (at node 14)
	maximum = 0.6255 (at node 91)
Accepted flit rate average= 0.280448
	minimum = 0.171 (at node 89)
	maximum = 0.4455 (at node 129)
Injected packet length average = 18.0038
Accepted packet length average = 18.051
Total in-flight flits = 161222 (150920 measured)
latency change    = 0.898651
throughput change = 0.00456858
Class 0:
Packet latency average = 1433.37
	minimum = 22
	maximum = 2983
Network latency average = 1426.72
	minimum = 22
	maximum = 2961
Slowest packet = 14202
Flit latency average = 1606.67
	minimum = 5
	maximum = 3484
Slowest flit = 210383
Fragmentation average = 41.1629
	minimum = 0
	maximum = 430
Injected packet rate average = 0.0246302
	minimum = 0.0183333 (at node 191)
	maximum = 0.0313333 (at node 123)
Accepted packet rate average = 0.0155747
	minimum = 0.0103333 (at node 36)
	maximum = 0.0226667 (at node 129)
Injected flit rate average = 0.443411
	minimum = 0.324333 (at node 191)
	maximum = 0.566667 (at node 123)
Accepted flit rate average= 0.281464
	minimum = 0.174333 (at node 36)
	maximum = 0.401333 (at node 129)
Injected packet length average = 18.0027
Accepted packet length average = 18.0719
Total in-flight flits = 192399 (191782 measured)
latency change    = 0.710517
throughput change = 0.00360837
Draining remaining packets ...
Class 0:
Remaining flits: 238500 238501 238502 238503 238504 238505 238506 238507 238508 238509 [...] (144949 flits)
Measured flits: 261632 261633 261634 261635 261636 261637 261638 261639 261640 261641 [...] (144859 flits)
Class 0:
Remaining flits: 238500 238501 238502 238503 238504 238505 238506 238507 238508 238509 [...] (97723 flits)
Measured flits: 289578 289579 289580 289581 289582 289583 291833 315090 315091 315092 [...] (97633 flits)
Class 0:
Remaining flits: 240044 240045 240046 240047 241470 241471 241472 241473 241474 241475 [...] (50722 flits)
Measured flits: 315090 315091 315092 315093 315094 315095 315096 315097 315098 315099 [...] (50664 flits)
Class 0:
Remaining flits: 315090 315091 315092 315093 315094 315095 315096 315097 315098 315099 [...] (8834 flits)
Measured flits: 315090 315091 315092 315093 315094 315095 315096 315097 315098 315099 [...] (8834 flits)
Time taken is 10963 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2879.49 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6950 (1 samples)
Network latency average = 2872.68 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6935 (1 samples)
Flit latency average = 2520.56 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6936 (1 samples)
Fragmentation average = 62.4238 (1 samples)
	minimum = 0 (1 samples)
	maximum = 585 (1 samples)
Injected packet rate average = 0.0246302 (1 samples)
	minimum = 0.0183333 (1 samples)
	maximum = 0.0313333 (1 samples)
Accepted packet rate average = 0.0155747 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.443411 (1 samples)
	minimum = 0.324333 (1 samples)
	maximum = 0.566667 (1 samples)
Accepted flit rate average = 0.281464 (1 samples)
	minimum = 0.174333 (1 samples)
	maximum = 0.401333 (1 samples)
Injected packet size average = 18.0027 (1 samples)
Accepted packet size average = 18.0719 (1 samples)
Hops average = 5.0652 (1 samples)
Total run time 9.97128
