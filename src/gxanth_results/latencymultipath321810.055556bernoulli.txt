BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 414.299
	minimum = 23
	maximum = 973
Network latency average = 360.114
	minimum = 23
	maximum = 973
Slowest packet = 139
Flit latency average = 285.445
	minimum = 6
	maximum = 968
Slowest flit = 1850
Fragmentation average = 172.013
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0219063
	minimum = 0.007 (at node 100)
	maximum = 0.035 (at node 131)
Accepted packet rate average = 0.0100625
	minimum = 0.003 (at node 150)
	maximum = 0.019 (at node 76)
Injected flit rate average = 0.386823
	minimum = 0.11 (at node 100)
	maximum = 0.63 (at node 131)
Accepted flit rate average= 0.207417
	minimum = 0.079 (at node 150)
	maximum = 0.395 (at node 76)
Injected packet length average = 17.6581
Accepted packet length average = 20.6128
Total in-flight flits = 36136 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 812.091
	minimum = 23
	maximum = 1978
Network latency average = 597.585
	minimum = 23
	maximum = 1951
Slowest packet = 254
Flit latency average = 492.037
	minimum = 6
	maximum = 1951
Slowest flit = 4634
Fragmentation average = 199.445
	minimum = 0
	maximum = 1842
Injected packet rate average = 0.0165208
	minimum = 0.006 (at node 100)
	maximum = 0.026 (at node 79)
Accepted packet rate average = 0.0106875
	minimum = 0.006 (at node 10)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.294036
	minimum = 0.104 (at node 100)
	maximum = 0.4655 (at node 79)
Accepted flit rate average= 0.204719
	minimum = 0.113 (at node 96)
	maximum = 0.327 (at node 152)
Injected packet length average = 17.7979
Accepted packet length average = 19.155
Total in-flight flits = 35922 (0 measured)
latency change    = 0.489837
throughput change = 0.0131786
Class 0:
Packet latency average = 1957.7
	minimum = 982
	maximum = 2827
Network latency average = 977.106
	minimum = 25
	maximum = 2799
Slowest packet = 1233
Flit latency average = 849.209
	minimum = 6
	maximum = 2890
Slowest flit = 15195
Fragmentation average = 211.545
	minimum = 1
	maximum = 2438
Injected packet rate average = 0.0112865
	minimum = 0 (at node 41)
	maximum = 0.032 (at node 125)
Accepted packet rate average = 0.010974
	minimum = 0.002 (at node 184)
	maximum = 0.02 (at node 34)
Injected flit rate average = 0.202141
	minimum = 0 (at node 55)
	maximum = 0.565 (at node 125)
Accepted flit rate average= 0.195974
	minimum = 0.057 (at node 184)
	maximum = 0.376 (at node 114)
Injected packet length average = 17.91
Accepted packet length average = 17.8581
Total in-flight flits = 37283 (0 measured)
latency change    = 0.585181
throughput change = 0.0446222
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2807.81
	minimum = 1928
	maximum = 3673
Network latency average = 351.881
	minimum = 27
	maximum = 958
Slowest packet = 8540
Flit latency average = 902.598
	minimum = 6
	maximum = 3736
Slowest flit = 35146
Fragmentation average = 131.281
	minimum = 0
	maximum = 587
Injected packet rate average = 0.0106146
	minimum = 0 (at node 23)
	maximum = 0.029 (at node 150)
Accepted packet rate average = 0.0108021
	minimum = 0.001 (at node 190)
	maximum = 0.018 (at node 21)
Injected flit rate average = 0.191484
	minimum = 0 (at node 54)
	maximum = 0.515 (at node 150)
Accepted flit rate average= 0.195854
	minimum = 0.033 (at node 190)
	maximum = 0.331 (at node 128)
Injected packet length average = 18.0397
Accepted packet length average = 18.1311
Total in-flight flits = 36273 (24862 measured)
latency change    = 0.302765
throughput change = 0.000611637
Class 0:
Packet latency average = 3346.39
	minimum = 1928
	maximum = 4546
Network latency average = 615.116
	minimum = 23
	maximum = 1924
Slowest packet = 8540
Flit latency average = 905.057
	minimum = 6
	maximum = 4711
Slowest flit = 17151
Fragmentation average = 159.242
	minimum = 0
	maximum = 1130
Injected packet rate average = 0.0107969
	minimum = 0 (at node 23)
	maximum = 0.0225 (at node 97)
Accepted packet rate average = 0.0109401
	minimum = 0.0045 (at node 4)
	maximum = 0.017 (at node 37)
Injected flit rate average = 0.194479
	minimum = 0.0005 (at node 23)
	maximum = 0.4 (at node 97)
Accepted flit rate average= 0.196901
	minimum = 0.0815 (at node 4)
	maximum = 0.3005 (at node 16)
Injected packet length average = 18.0125
Accepted packet length average = 17.9981
Total in-flight flits = 36247 (33081 measured)
latency change    = 0.160944
throughput change = 0.00531676
Class 0:
Packet latency average = 3812.41
	minimum = 1928
	maximum = 5347
Network latency average = 740.63
	minimum = 23
	maximum = 2921
Slowest packet = 8540
Flit latency average = 899.211
	minimum = 6
	maximum = 5857
Slowest flit = 18927
Fragmentation average = 174.416
	minimum = 0
	maximum = 1581
Injected packet rate average = 0.0108646
	minimum = 0 (at node 122)
	maximum = 0.022 (at node 97)
Accepted packet rate average = 0.0108663
	minimum = 0.00633333 (at node 36)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.195898
	minimum = 0.003 (at node 32)
	maximum = 0.394333 (at node 97)
Accepted flit rate average= 0.195712
	minimum = 0.111333 (at node 36)
	maximum = 0.275 (at node 16)
Injected packet length average = 18.0308
Accepted packet length average = 18.0109
Total in-flight flits = 37269 (36231 measured)
latency change    = 0.122239
throughput change = 0.00607647
Class 0:
Packet latency average = 4262.4
	minimum = 1928
	maximum = 6339
Network latency average = 833.467
	minimum = 23
	maximum = 3752
Slowest packet = 8540
Flit latency average = 910.074
	minimum = 6
	maximum = 6537
Slowest flit = 39241
Fragmentation average = 182.038
	minimum = 0
	maximum = 2635
Injected packet rate average = 0.0107435
	minimum = 0.0005 (at node 148)
	maximum = 0.01925 (at node 94)
Accepted packet rate average = 0.0108372
	minimum = 0.0065 (at node 4)
	maximum = 0.015 (at node 128)
Injected flit rate average = 0.193529
	minimum = 0.009 (at node 148)
	maximum = 0.347 (at node 94)
Accepted flit rate average= 0.194757
	minimum = 0.11925 (at node 4)
	maximum = 0.27475 (at node 128)
Injected packet length average = 18.0136
Accepted packet length average = 17.971
Total in-flight flits = 36282 (35917 measured)
latency change    = 0.105571
throughput change = 0.00490507
Class 0:
Packet latency average = 4697.01
	minimum = 1928
	maximum = 7366
Network latency average = 888.651
	minimum = 23
	maximum = 4768
Slowest packet = 8540
Flit latency average = 916.135
	minimum = 6
	maximum = 7358
Slowest flit = 51854
Fragmentation average = 188.203
	minimum = 0
	maximum = 3566
Injected packet rate average = 0.0108667
	minimum = 0.0014 (at node 148)
	maximum = 0.0186 (at node 161)
Accepted packet rate average = 0.0108063
	minimum = 0.0074 (at node 84)
	maximum = 0.0152 (at node 128)
Injected flit rate average = 0.195732
	minimum = 0.0238 (at node 148)
	maximum = 0.3326 (at node 161)
Accepted flit rate average= 0.194654
	minimum = 0.1336 (at node 84)
	maximum = 0.2704 (at node 128)
Injected packet length average = 18.0122
Accepted packet length average = 18.0131
Total in-flight flits = 38263 (38137 measured)
latency change    = 0.0925291
throughput change = 0.000525772
Class 0:
Packet latency average = 5117.47
	minimum = 1928
	maximum = 8172
Network latency average = 927.474
	minimum = 23
	maximum = 5246
Slowest packet = 8540
Flit latency average = 921.045
	minimum = 6
	maximum = 7650
Slowest flit = 70253
Fragmentation average = 191.488
	minimum = 0
	maximum = 3566
Injected packet rate average = 0.0107352
	minimum = 0.00316667 (at node 168)
	maximum = 0.0176667 (at node 161)
Accepted packet rate average = 0.0107908
	minimum = 0.00816667 (at node 36)
	maximum = 0.0143333 (at node 129)
Injected flit rate average = 0.193401
	minimum = 0.057 (at node 168)
	maximum = 0.317667 (at node 161)
Accepted flit rate average= 0.194141
	minimum = 0.141667 (at node 36)
	maximum = 0.256833 (at node 129)
Injected packet length average = 18.0155
Accepted packet length average = 17.9914
Total in-flight flits = 36418 (36363 measured)
latency change    = 0.0821607
throughput change = 0.00264072
Class 0:
Packet latency average = 5527.04
	minimum = 1928
	maximum = 9048
Network latency average = 956.882
	minimum = 23
	maximum = 5815
Slowest packet = 8540
Flit latency average = 927.109
	minimum = 6
	maximum = 7650
Slowest flit = 70253
Fragmentation average = 193.757
	minimum = 0
	maximum = 3566
Injected packet rate average = 0.0107083
	minimum = 0.004 (at node 163)
	maximum = 0.0174286 (at node 167)
Accepted packet rate average = 0.0107463
	minimum = 0.008 (at node 104)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.192879
	minimum = 0.0721429 (at node 163)
	maximum = 0.315429 (at node 167)
Accepted flit rate average= 0.193323
	minimum = 0.147571 (at node 104)
	maximum = 0.250143 (at node 128)
Injected packet length average = 18.012
Accepted packet length average = 17.9898
Total in-flight flits = 36639 (36618 measured)
latency change    = 0.0741042
throughput change = 0.00423424
Draining all recorded packets ...
Class 0:
Remaining flits: 121384 121385 121386 121387 121388 121389 121390 121391 196846 196847 [...] (36564 flits)
Measured flits: 196846 196847 204840 204841 204842 204843 204844 204845 204846 204847 [...] (36556 flits)
Class 0:
Remaining flits: 230825 230826 230827 230828 230829 230830 230831 291096 291097 291098 [...] (36372 flits)
Measured flits: 230825 230826 230827 230828 230829 230830 230831 291096 291097 291098 [...] (36372 flits)
Class 0:
Remaining flits: 291110 291111 291112 291113 296658 296659 296660 296661 296662 296663 [...] (36988 flits)
Measured flits: 291110 291111 291112 291113 296658 296659 296660 296661 296662 296663 [...] (36988 flits)
Class 0:
Remaining flits: 296662 296663 296664 296665 296666 296667 296668 296669 296670 296671 [...] (34327 flits)
Measured flits: 296662 296663 296664 296665 296666 296667 296668 296669 296670 296671 [...] (34327 flits)
Class 0:
Remaining flits: 298026 298027 298028 298029 298030 298031 298032 298033 298034 298035 [...] (35101 flits)
Measured flits: 298026 298027 298028 298029 298030 298031 298032 298033 298034 298035 [...] (35101 flits)
Class 0:
Remaining flits: 384274 384275 384276 384277 384278 384279 384280 384281 385056 385057 [...] (37497 flits)
Measured flits: 384274 384275 384276 384277 384278 384279 384280 384281 385056 385057 [...] (37497 flits)
Class 0:
Remaining flits: 385061 385062 385063 385064 385065 385066 385067 385068 385069 385070 [...] (35839 flits)
Measured flits: 385061 385062 385063 385064 385065 385066 385067 385068 385069 385070 [...] (35839 flits)
Class 0:
Remaining flits: 472410 472411 472412 472413 472414 472415 472416 472417 472418 472419 [...] (36726 flits)
Measured flits: 472410 472411 472412 472413 472414 472415 472416 472417 472418 472419 [...] (36726 flits)
Class 0:
Remaining flits: 491380 491381 495756 495757 495758 495759 495760 495761 495762 495763 [...] (36877 flits)
Measured flits: 491380 491381 495756 495757 495758 495759 495760 495761 495762 495763 [...] (36877 flits)
Class 0:
Remaining flits: 537642 537643 537644 537645 537646 537647 537648 537649 537650 537651 [...] (36944 flits)
Measured flits: 537642 537643 537644 537645 537646 537647 537648 537649 537650 537651 [...] (36944 flits)
Class 0:
Remaining flits: 564282 564283 564284 564285 564286 564287 564288 564289 564290 564291 [...] (35957 flits)
Measured flits: 564282 564283 564284 564285 564286 564287 564288 564289 564290 564291 [...] (35957 flits)
Class 0:
Remaining flits: 564295 564296 564297 564298 564299 642116 642117 642118 642119 642120 [...] (35397 flits)
Measured flits: 564295 564296 564297 564298 564299 642116 642117 642118 642119 642120 [...] (35397 flits)
Class 0:
Remaining flits: 658746 658747 658748 658749 658750 658751 658752 658753 658754 658755 [...] (36498 flits)
Measured flits: 658746 658747 658748 658749 658750 658751 658752 658753 658754 658755 [...] (36498 flits)
Class 0:
Remaining flits: 715025 715026 715027 715028 715029 715030 715031 721512 721513 721514 [...] (36376 flits)
Measured flits: 715025 715026 715027 715028 715029 715030 715031 721512 721513 721514 [...] (36376 flits)
Class 0:
Remaining flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36127 flits)
Measured flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36127 flits)
Class 0:
Remaining flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36568 flits)
Measured flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36568 flits)
Class 0:
Remaining flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36389 flits)
Measured flits: 721512 721513 721514 721515 721516 721517 721518 721519 721520 721521 [...] (36389 flits)
Class 0:
Remaining flits: 773244 773245 773246 773247 773248 773249 773250 773251 773252 773253 [...] (36595 flits)
Measured flits: 773244 773245 773246 773247 773248 773249 773250 773251 773252 773253 [...] (36595 flits)
Class 0:
Remaining flits: 773259 773260 773261 869814 869815 869816 869817 869818 869819 869820 [...] (38094 flits)
Measured flits: 773259 773260 773261 869814 869815 869816 869817 869818 869819 869820 [...] (38094 flits)
Class 0:
Remaining flits: 869818 869819 869820 869821 869822 869823 869824 869825 869826 869827 [...] (35935 flits)
Measured flits: 869818 869819 869820 869821 869822 869823 869824 869825 869826 869827 [...] (35935 flits)
Class 0:
Remaining flits: 869821 869822 869823 869824 869825 869826 869827 869828 869829 869830 [...] (36470 flits)
Measured flits: 869821 869822 869823 869824 869825 869826 869827 869828 869829 869830 [...] (36470 flits)
Class 0:
Remaining flits: 869826 869827 869828 869829 869830 869831 965502 965503 965504 965505 [...] (35645 flits)
Measured flits: 869826 869827 869828 869829 869830 869831 965502 965503 965504 965505 [...] (35645 flits)
Class 0:
Remaining flits: 989322 989323 989324 989325 989326 989327 989328 989329 989330 989331 [...] (36342 flits)
Measured flits: 989322 989323 989324 989325 989326 989327 989328 989329 989330 989331 [...] (36342 flits)
Class 0:
Remaining flits: 1039535 1059926 1059927 1059928 1059929 1077624 1077625 1077626 1077627 1077628 [...] (38160 flits)
Measured flits: 1039535 1059926 1059927 1059928 1059929 1077624 1077625 1077626 1077627 1077628 [...] (38160 flits)
Class 0:
Remaining flits: 1059926 1059927 1059928 1059929 1077641 1095714 1095715 1095716 1095717 1095718 [...] (38229 flits)
Measured flits: 1059926 1059927 1059928 1059929 1077641 1095714 1095715 1095716 1095717 1095718 [...] (38229 flits)
Class 0:
Remaining flits: 1122018 1122019 1122020 1122021 1122022 1122023 1122024 1122025 1122026 1122027 [...] (36878 flits)
Measured flits: 1122018 1122019 1122020 1122021 1122022 1122023 1122024 1122025 1122026 1122027 [...] (36878 flits)
Class 0:
Remaining flits: 1126134 1126135 1126136 1126137 1126138 1126139 1126140 1126141 1126142 1126143 [...] (37034 flits)
Measured flits: 1126134 1126135 1126136 1126137 1126138 1126139 1126140 1126141 1126142 1126143 [...] (37034 flits)
Class 0:
Remaining flits: 1195506 1195507 1195508 1195509 1195510 1195511 1195512 1195513 1195514 1195515 [...] (36445 flits)
Measured flits: 1195506 1195507 1195508 1195509 1195510 1195511 1195512 1195513 1195514 1195515 [...] (36109 flits)
Class 0:
Remaining flits: 1198240 1198241 1226342 1226343 1226344 1226345 1226346 1226347 1226348 1226349 [...] (37505 flits)
Measured flits: 1198240 1198241 1226342 1226343 1226344 1226345 1226346 1226347 1226348 1226349 [...] (36903 flits)
Class 0:
Remaining flits: 1226343 1226344 1226345 1226346 1226347 1226348 1226349 1226350 1226351 1226352 [...] (37947 flits)
Measured flits: 1226343 1226344 1226345 1226346 1226347 1226348 1226349 1226350 1226351 1226352 [...] (37170 flits)
Class 0:
Remaining flits: 1294848 1294849 1294850 1294851 1294852 1294853 1294854 1294855 1294856 1294857 [...] (36481 flits)
Measured flits: 1294848 1294849 1294850 1294851 1294852 1294853 1294854 1294855 1294856 1294857 [...] (35536 flits)
Class 0:
Remaining flits: 1317813 1317814 1317815 1331316 1331317 1331318 1331319 1331320 1331321 1331322 [...] (36179 flits)
Measured flits: 1317813 1317814 1317815 1331316 1331317 1331318 1331319 1331320 1331321 1331322 [...] (34735 flits)
Class 0:
Remaining flits: 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 1331325 [...] (36959 flits)
Measured flits: 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 1331325 [...] (33994 flits)
Class 0:
Remaining flits: 1388715 1388716 1388717 1396835 1397574 1397575 1397576 1397577 1397578 1397579 [...] (35654 flits)
Measured flits: 1388715 1388716 1388717 1396835 1397574 1397575 1397576 1397577 1397578 1397579 [...] (30896 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (36663 flits)
Measured flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (29281 flits)
Class 0:
Remaining flits: 1462716 1462717 1462718 1462719 1462720 1462721 1462722 1462723 1462724 1462725 [...] (35893 flits)
Measured flits: 1462716 1462717 1462718 1462719 1462720 1462721 1462722 1462723 1462724 1462725 [...] (27132 flits)
Class 0:
Remaining flits: 1494342 1494343 1494344 1494345 1494346 1494347 1494348 1494349 1494350 1494351 [...] (36458 flits)
Measured flits: 1494342 1494343 1494344 1494345 1494346 1494347 1494348 1494349 1494350 1494351 [...] (25386 flits)
Class 0:
Remaining flits: 1498920 1498921 1498922 1498923 1498924 1498925 1498926 1498927 1498928 1498929 [...] (38669 flits)
Measured flits: 1498920 1498921 1498922 1498923 1498924 1498925 1498926 1498927 1498928 1498929 [...] (24111 flits)
Class 0:
Remaining flits: 1542039 1542040 1542041 1567278 1567279 1567280 1567281 1567282 1567283 1567284 [...] (37020 flits)
Measured flits: 1542039 1542040 1542041 1567278 1567279 1567280 1567281 1567282 1567283 1567284 [...] (20836 flits)
Class 0:
Remaining flits: 1635910 1635911 1641222 1641223 1641224 1641225 1641226 1641227 1641228 1641229 [...] (36954 flits)
Measured flits: 1641222 1641223 1641224 1641225 1641226 1641227 1641228 1641229 1641230 1641231 [...] (16957 flits)
Class 0:
Remaining flits: 1652613 1652614 1652615 1669428 1669429 1669430 1669431 1669432 1669433 1669434 [...] (36408 flits)
Measured flits: 1652613 1652614 1652615 1669428 1669429 1669430 1669431 1669432 1669433 1669434 [...] (15211 flits)
Class 0:
Remaining flits: 1669428 1669429 1669430 1669431 1669432 1669433 1669434 1669435 1669436 1669437 [...] (36421 flits)
Measured flits: 1669428 1669429 1669430 1669431 1669432 1669433 1669434 1669435 1669436 1669437 [...] (12133 flits)
Class 0:
Remaining flits: 1669428 1669429 1669430 1669431 1669432 1669433 1669434 1669435 1669436 1669437 [...] (35394 flits)
Measured flits: 1669428 1669429 1669430 1669431 1669432 1669433 1669434 1669435 1669436 1669437 [...] (10646 flits)
Class 0:
Remaining flits: 1760649 1760650 1760651 1777230 1777231 1777232 1777233 1777234 1777235 1777236 [...] (36006 flits)
Measured flits: 1799125 1799126 1799127 1799128 1799129 1799130 1799131 1799132 1799133 1799134 [...] (8536 flits)
Class 0:
Remaining flits: 1760649 1760650 1760651 1777236 1777237 1777238 1777239 1777240 1777241 1777242 [...] (36620 flits)
Measured flits: 1814616 1814617 1814618 1814619 1814620 1814621 1814622 1814623 1814624 1814625 [...] (7763 flits)
Class 0:
Remaining flits: 1801238 1801239 1801240 1801241 1807866 1807867 1807868 1807869 1807870 1807871 [...] (37403 flits)
Measured flits: 1818234 1818235 1818236 1818237 1818238 1818239 1818240 1818241 1818242 1818243 [...] (6562 flits)
Class 0:
Remaining flits: 1829790 1829791 1829792 1829793 1829794 1829795 1829796 1829797 1829798 1829799 [...] (37350 flits)
Measured flits: 1932372 1932373 1932374 1932375 1932376 1932377 1932378 1932379 1932380 1932381 [...] (6151 flits)
Class 0:
Remaining flits: 1934514 1934515 1934516 1934517 1934518 1934519 1934520 1934521 1934522 1934523 [...] (37976 flits)
Measured flits: 1941606 1941607 1941608 1941609 1941610 1941611 1941612 1941613 1941614 1941615 [...] (5732 flits)
Class 0:
Remaining flits: 1941606 1941607 1941608 1941609 1941610 1941611 1941612 1941613 1941614 1941615 [...] (35701 flits)
Measured flits: 1941606 1941607 1941608 1941609 1941610 1941611 1941612 1941613 1941614 1941615 [...] (4729 flits)
Class 0:
Remaining flits: 1956726 1956727 1956728 1956729 1956730 1956731 1956732 1956733 1956734 1956735 [...] (37061 flits)
Measured flits: 2014308 2014309 2014310 2014311 2014312 2014313 2014314 2014315 2014316 2014317 [...] (3995 flits)
Class 0:
Remaining flits: 2014323 2014324 2014325 2040984 2040985 2040986 2040987 2040988 2040989 2040990 [...] (38086 flits)
Measured flits: 2014323 2014324 2014325 2066148 2066149 2066150 2066151 2066152 2066153 2066154 [...] (3947 flits)
Class 0:
Remaining flits: 2040984 2040985 2040986 2040987 2040988 2040989 2040990 2040991 2040992 2040993 [...] (38110 flits)
Measured flits: 2066154 2066155 2066156 2066157 2066158 2066159 2066160 2066161 2066162 2066163 [...] (3331 flits)
Class 0:
Remaining flits: 2064007 2064008 2064009 2064010 2064011 2064012 2064013 2064014 2064015 2064016 [...] (36522 flits)
Measured flits: 2104938 2104939 2104940 2104941 2104942 2104943 2104944 2104945 2104946 2104947 [...] (3152 flits)
Class 0:
Remaining flits: 2085660 2085661 2085662 2085663 2085664 2085665 2085666 2085667 2085668 2085669 [...] (36916 flits)
Measured flits: 2148606 2148607 2148608 2148609 2148610 2148611 2148612 2148613 2148614 2148615 [...] (2904 flits)
Class 0:
Remaining flits: 2085660 2085661 2085662 2085663 2085664 2085665 2085666 2085667 2085668 2085669 [...] (36723 flits)
Measured flits: 2148606 2148607 2148608 2148609 2148610 2148611 2148612 2148613 2148614 2148615 [...] (2527 flits)
Class 0:
Remaining flits: 2085660 2085661 2085662 2085663 2085664 2085665 2085666 2085667 2085668 2085669 [...] (37651 flits)
Measured flits: 2148609 2148610 2148611 2148612 2148613 2148614 2148615 2148616 2148617 2148618 [...] (2935 flits)
Class 0:
Remaining flits: 2095740 2095741 2095742 2095743 2095744 2095745 2095746 2095747 2095748 2095749 [...] (36210 flits)
Measured flits: 2283480 2283481 2283482 2283483 2283484 2283485 2283486 2283487 2283488 2283489 [...] (2793 flits)
Class 0:
Remaining flits: 2095754 2095755 2095756 2095757 2242836 2242837 2242838 2242839 2242840 2242841 [...] (36723 flits)
Measured flits: 2320812 2320813 2320814 2320815 2320816 2320817 2320818 2320819 2320820 2320821 [...] (2415 flits)
Class 0:
Remaining flits: 2243736 2243737 2243738 2243739 2243740 2243741 2243742 2243743 2243744 2243745 [...] (35948 flits)
Measured flits: 2320812 2320813 2320814 2320815 2320816 2320817 2320818 2320819 2320820 2320821 [...] (1924 flits)
Class 0:
Remaining flits: 2295900 2295901 2295902 2295903 2295904 2295905 2295906 2295907 2295908 2295909 [...] (37383 flits)
Measured flits: 2320812 2320813 2320814 2320815 2320816 2320817 2320818 2320819 2320820 2320821 [...] (1738 flits)
Class 0:
Remaining flits: 2297240 2297241 2297242 2297243 2297244 2297245 2297246 2297247 2297248 2297249 [...] (37150 flits)
Measured flits: 2413433 2413434 2413435 2413436 2413437 2413438 2413439 2434111 2434112 2434113 [...] (1319 flits)
Class 0:
Remaining flits: 2356524 2356525 2356526 2356527 2356528 2356529 2356530 2356531 2356532 2356533 [...] (35609 flits)
Measured flits: 2477842 2477843 2513517 2513518 2513519 2526786 2526787 2526788 2526789 2526790 [...] (613 flits)
Class 0:
Remaining flits: 2433366 2433367 2433368 2433369 2433370 2433371 2433372 2433373 2433374 2433375 [...] (36742 flits)
Measured flits: 2526790 2526791 2526792 2526793 2526794 2526795 2526796 2526797 2526798 2526799 [...] (729 flits)
Class 0:
Remaining flits: 2473272 2473273 2473274 2473275 2473276 2473277 2473278 2473279 2473280 2473281 [...] (36811 flits)
Measured flits: 2540994 2540995 2540996 2540997 2540998 2540999 2541000 2541001 2541002 2541003 [...] (530 flits)
Class 0:
Remaining flits: 2481318 2481319 2481320 2481321 2481322 2481323 2481324 2481325 2481326 2481327 [...] (36836 flits)
Measured flits: 2639751 2639752 2639753 2699370 2699371 2699372 2699373 2699374 2699375 2699376 [...] (482 flits)
Class 0:
Remaining flits: 2557207 2557208 2557209 2557210 2557211 2557212 2557213 2557214 2557215 2557216 [...] (36629 flits)
Measured flits: 2699370 2699371 2699372 2699373 2699374 2699375 2699376 2699377 2699378 2699379 [...] (275 flits)
Class 0:
Remaining flits: 2577024 2577025 2577026 2577027 2577028 2577029 2577030 2577031 2577032 2577033 [...] (36622 flits)
Measured flits: 2791811 2791812 2791813 2791814 2791815 2791816 2791817 2810719 2810720 2810721 [...] (187 flits)
Class 0:
Remaining flits: 2577041 2589405 2589406 2589407 2601360 2601361 2601362 2601363 2601364 2601365 [...] (37383 flits)
Measured flits: 2810728 2810729 2810730 2810731 2810732 2810733 2810734 2810735 2838834 2838835 [...] (216 flits)
Class 0:
Remaining flits: 2601368 2601369 2601370 2601371 2601372 2601373 2601374 2601375 2601376 2601377 [...] (36258 flits)
Measured flits: 2884212 2884213 2884214 2884215 2884216 2884217 2884218 2884219 2884220 2884221 [...] (54 flits)
Class 0:
Remaining flits: 2624521 2624522 2624523 2624524 2624525 2704639 2704640 2704641 2704642 2704643 [...] (35669 flits)
Measured flits: 2884212 2884213 2884214 2884215 2884216 2884217 2884218 2884219 2884220 2884221 [...] (36 flits)
Class 0:
Remaining flits: 2704639 2704640 2704641 2704642 2704643 2707578 2707579 2707580 2707581 2707582 [...] (33002 flits)
Measured flits: 2884212 2884213 2884214 2884215 2884216 2884217 2884218 2884219 2884220 2884221 [...] (36 flits)
Class 0:
Remaining flits: 2707585 2707586 2707587 2707588 2707589 2707590 2707591 2707592 2707593 2707594 [...] (35309 flits)
Measured flits: 3016530 3016531 3016532 3016533 3016534 3016535 3016536 3016537 3016538 3016539 [...] (36 flits)
Class 0:
Remaining flits: 2772702 2772703 2772704 2772705 2772706 2772707 2772708 2772709 2772710 2772711 [...] (37692 flits)
Measured flits: 3017304 3017305 3017306 3017307 3017308 3017309 3017310 3017311 3017312 3017313 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2836620 2836621 2836622 2836623 2836624 2836625 2836626 2836627 2836628 2836629 [...] (6793 flits)
Measured flits: (0 flits)
Time taken is 85586 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 23491.6 (1 samples)
	minimum = 1928 (1 samples)
	maximum = 73730 (1 samples)
Network latency average = 1073.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9950 (1 samples)
Flit latency average = 955.013 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11657 (1 samples)
Fragmentation average = 203.518 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6860 (1 samples)
Injected packet rate average = 0.0107083 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.0174286 (1 samples)
Accepted packet rate average = 0.0107463 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.192879 (1 samples)
	minimum = 0.0721429 (1 samples)
	maximum = 0.315429 (1 samples)
Accepted flit rate average = 0.193323 (1 samples)
	minimum = 0.147571 (1 samples)
	maximum = 0.250143 (1 samples)
Injected packet size average = 18.012 (1 samples)
Accepted packet size average = 17.9898 (1 samples)
Hops average = 5.0676 (1 samples)
Total run time 97.7845
