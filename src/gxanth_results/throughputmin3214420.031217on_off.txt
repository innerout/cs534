BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 337.527
	minimum = 24
	maximum = 980
Network latency average = 232.29
	minimum = 22
	maximum = 756
Slowest packet = 40
Flit latency average = 201.942
	minimum = 5
	maximum = 817
Slowest flit = 14667
Fragmentation average = 43.881
	minimum = 0
	maximum = 338
Injected packet rate average = 0.027625
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0136979
	minimum = 0.005 (at node 41)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.491703
	minimum = 0 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.257193
	minimum = 0.112 (at node 41)
	maximum = 0.414 (at node 48)
Injected packet length average = 17.7992
Accepted packet length average = 18.776
Total in-flight flits = 46091 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.9
	minimum = 24
	maximum = 1967
Network latency average = 423.747
	minimum = 22
	maximum = 1595
Slowest packet = 40
Flit latency average = 391.884
	minimum = 5
	maximum = 1594
Slowest flit = 32966
Fragmentation average = 60.7561
	minimum = 0
	maximum = 403
Injected packet rate average = 0.0292161
	minimum = 0.0035 (at node 37)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0146693
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.523701
	minimum = 0.063 (at node 37)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.272247
	minimum = 0.148 (at node 153)
	maximum = 0.39 (at node 63)
Injected packet length average = 17.925
Accepted packet length average = 18.559
Total in-flight flits = 97399 (0 measured)
latency change    = 0.405658
throughput change = 0.0552978
Class 0:
Packet latency average = 1272.56
	minimum = 25
	maximum = 2846
Network latency average = 1056.16
	minimum = 22
	maximum = 2236
Slowest packet = 2435
Flit latency average = 1020.51
	minimum = 5
	maximum = 2406
Slowest flit = 50169
Fragmentation average = 106.906
	minimum = 0
	maximum = 497
Injected packet rate average = 0.0318437
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.015849
	minimum = 0.007 (at node 185)
	maximum = 0.026 (at node 66)
Injected flit rate average = 0.572703
	minimum = 0 (at node 5)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287484
	minimum = 0.149 (at node 190)
	maximum = 0.499 (at node 99)
Injected packet length average = 17.9848
Accepted packet length average = 18.139
Total in-flight flits = 152254 (0 measured)
latency change    = 0.553735
throughput change = 0.0530011
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 292.171
	minimum = 25
	maximum = 1438
Network latency average = 40.9577
	minimum = 22
	maximum = 386
Slowest packet = 17333
Flit latency average = 1461.37
	minimum = 5
	maximum = 3154
Slowest flit = 76391
Fragmentation average = 6.1069
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0306146
	minimum = 0 (at node 94)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0159792
	minimum = 0.005 (at node 52)
	maximum = 0.028 (at node 39)
Injected flit rate average = 0.550995
	minimum = 0 (at node 94)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.286797
	minimum = 0.125 (at node 52)
	maximum = 0.475 (at node 39)
Injected packet length average = 17.9978
Accepted packet length average = 17.9482
Total in-flight flits = 203101 (97747 measured)
latency change    = 3.35553
throughput change = 0.00239717
Class 0:
Packet latency average = 498.721
	minimum = 25
	maximum = 2367
Network latency average = 244.832
	minimum = 22
	maximum = 1969
Slowest packet = 17333
Flit latency average = 1707.11
	minimum = 5
	maximum = 3937
Slowest flit = 101431
Fragmentation average = 16.1788
	minimum = 0
	maximum = 299
Injected packet rate average = 0.0296849
	minimum = 0.006 (at node 183)
	maximum = 0.0555 (at node 54)
Accepted packet rate average = 0.0157734
	minimum = 0.01 (at node 4)
	maximum = 0.0275 (at node 129)
Injected flit rate average = 0.53418
	minimum = 0.101 (at node 187)
	maximum = 1 (at node 54)
Accepted flit rate average= 0.283667
	minimum = 0.1715 (at node 4)
	maximum = 0.483 (at node 129)
Injected packet length average = 17.995
Accepted packet length average = 17.9838
Total in-flight flits = 248634 (187469 measured)
latency change    = 0.414159
throughput change = 0.0110348
Class 0:
Packet latency average = 1245.07
	minimum = 24
	maximum = 3757
Network latency average = 999.028
	minimum = 22
	maximum = 2974
Slowest packet = 17333
Flit latency average = 1959.89
	minimum = 5
	maximum = 4636
Slowest flit = 112337
Fragmentation average = 40.9346
	minimum = 0
	maximum = 399
Injected packet rate average = 0.0284462
	minimum = 0.008 (at node 0)
	maximum = 0.055 (at node 111)
Accepted packet rate average = 0.0157049
	minimum = 0.0106667 (at node 4)
	maximum = 0.0236667 (at node 129)
Injected flit rate average = 0.512094
	minimum = 0.144 (at node 0)
	maximum = 0.987 (at node 111)
Accepted flit rate average= 0.280995
	minimum = 0.188667 (at node 4)
	maximum = 0.414667 (at node 129)
Injected packet length average = 18.0022
Accepted packet length average = 17.8922
Total in-flight flits = 285637 (257205 measured)
latency change    = 0.599443
throughput change = 0.00950863
Draining remaining packets ...
Class 0:
Remaining flits: 148950 148951 148952 148953 148954 148955 148956 148957 148958 148959 [...] (236820 flits)
Measured flits: 312048 312049 312050 312051 312052 312053 312054 312055 312056 312057 [...] (224673 flits)
Class 0:
Remaining flits: 188417 188418 188419 188420 188421 188422 188423 190260 190261 190262 [...] (188464 flits)
Measured flits: 312048 312049 312050 312051 312052 312053 312054 312055 312056 312057 [...] (183566 flits)
Class 0:
Remaining flits: 199926 199927 199928 199929 199930 199931 199932 199933 199934 199935 [...] (140730 flits)
Measured flits: 312174 312175 312176 312177 312178 312179 312180 312181 312182 312183 [...] (138749 flits)
Class 0:
Remaining flits: 205488 205489 205490 205491 205492 205493 205494 205495 205496 205497 [...] (93571 flits)
Measured flits: 312174 312175 312176 312177 312178 312179 312180 312181 312182 312183 [...] (92907 flits)
Class 0:
Remaining flits: 279900 279901 279902 279903 279904 279905 279906 279907 279908 279909 [...] (49207 flits)
Measured flits: 312174 312175 312176 312177 312178 312179 312180 312181 312182 312183 [...] (49095 flits)
Class 0:
Remaining flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (15534 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (15534 flits)
Class 0:
Remaining flits: 424818 424819 424820 424821 424822 424823 424824 424825 424826 424827 [...] (1794 flits)
Measured flits: 424818 424819 424820 424821 424822 424823 424824 424825 424826 424827 [...] (1794 flits)
Time taken is 13938 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4588.5 (1 samples)
	minimum = 24 (1 samples)
	maximum = 10265 (1 samples)
Network latency average = 4279.51 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9463 (1 samples)
Flit latency average = 3699.04 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9446 (1 samples)
Fragmentation average = 60.1896 (1 samples)
	minimum = 0 (1 samples)
	maximum = 760 (1 samples)
Injected packet rate average = 0.0284462 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.055 (1 samples)
Accepted packet rate average = 0.0157049 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0236667 (1 samples)
Injected flit rate average = 0.512094 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.987 (1 samples)
Accepted flit rate average = 0.280995 (1 samples)
	minimum = 0.188667 (1 samples)
	maximum = 0.414667 (1 samples)
Injected packet size average = 18.0022 (1 samples)
Accepted packet size average = 17.8922 (1 samples)
Hops average = 5.13285 (1 samples)
Total run time 12.4803
