BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.439
	minimum = 23
	maximum = 937
Network latency average = 160.384
	minimum = 23
	maximum = 679
Slowest packet = 48
Flit latency average = 122.055
	minimum = 6
	maximum = 662
Slowest flit = 5489
Fragmentation average = 41.4047
	minimum = 0
	maximum = 108
Injected packet rate average = 0.0107865
	minimum = 0 (at node 52)
	maximum = 0.03 (at node 93)
Accepted packet rate average = 0.00811979
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 152)
Injected flit rate average = 0.19151
	minimum = 0 (at node 52)
	maximum = 0.529 (at node 93)
Accepted flit rate average= 0.151708
	minimum = 0.018 (at node 41)
	maximum = 0.278 (at node 152)
Injected packet length average = 17.7547
Accepted packet length average = 18.6838
Total in-flight flits = 8528 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 326.114
	minimum = 23
	maximum = 1742
Network latency average = 222.969
	minimum = 23
	maximum = 1031
Slowest packet = 48
Flit latency average = 179.501
	minimum = 6
	maximum = 996
Slowest flit = 29483
Fragmentation average = 46.8881
	minimum = 0
	maximum = 121
Injected packet rate average = 0.00998177
	minimum = 0.0005 (at node 111)
	maximum = 0.022 (at node 93)
Accepted packet rate average = 0.00847396
	minimum = 0.005 (at node 25)
	maximum = 0.014 (at node 44)
Injected flit rate average = 0.178211
	minimum = 0.009 (at node 111)
	maximum = 0.3955 (at node 93)
Accepted flit rate average= 0.155432
	minimum = 0.09 (at node 25)
	maximum = 0.252 (at node 44)
Injected packet length average = 17.8536
Accepted packet length average = 18.3423
Total in-flight flits = 9884 (0 measured)
latency change    = 0.33631
throughput change = 0.0239587
Class 0:
Packet latency average = 617.839
	minimum = 23
	maximum = 2274
Network latency average = 339.487
	minimum = 23
	maximum = 1646
Slowest packet = 2855
Flit latency average = 287.756
	minimum = 6
	maximum = 1660
Slowest flit = 48123
Fragmentation average = 54.1343
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0094375
	minimum = 0 (at node 53)
	maximum = 0.023 (at node 100)
Accepted packet rate average = 0.00895833
	minimum = 0.001 (at node 121)
	maximum = 0.018 (at node 179)
Injected flit rate average = 0.170167
	minimum = 0 (at node 53)
	maximum = 0.421 (at node 100)
Accepted flit rate average= 0.161312
	minimum = 0.018 (at node 121)
	maximum = 0.321 (at node 179)
Injected packet length average = 18.0309
Accepted packet length average = 18.007
Total in-flight flits = 11852 (0 measured)
latency change    = 0.472169
throughput change = 0.0364523
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 693.537
	minimum = 31
	maximum = 3023
Network latency average = 278.316
	minimum = 23
	maximum = 916
Slowest packet = 5715
Flit latency average = 334.316
	minimum = 6
	maximum = 1936
Slowest flit = 46331
Fragmentation average = 52.4606
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00924479
	minimum = 0 (at node 38)
	maximum = 0.022 (at node 148)
Accepted packet rate average = 0.00896875
	minimum = 0.002 (at node 146)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.166417
	minimum = 0 (at node 38)
	maximum = 0.403 (at node 148)
Accepted flit rate average= 0.161734
	minimum = 0.036 (at node 184)
	maximum = 0.306 (at node 56)
Injected packet length average = 18.0011
Accepted packet length average = 18.0331
Total in-flight flits = 13037 (12814 measured)
latency change    = 0.109148
throughput change = 0.00260844
Class 0:
Packet latency average = 888.525
	minimum = 31
	maximum = 3315
Network latency average = 358.098
	minimum = 23
	maximum = 1786
Slowest packet = 5715
Flit latency average = 348.605
	minimum = 6
	maximum = 2023
Slowest flit = 88703
Fragmentation average = 53.2939
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00910417
	minimum = 0.0015 (at node 48)
	maximum = 0.0165 (at node 35)
Accepted packet rate average = 0.00896354
	minimum = 0.0045 (at node 117)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.163667
	minimum = 0.0345 (at node 48)
	maximum = 0.303 (at node 35)
Accepted flit rate average= 0.161609
	minimum = 0.085 (at node 190)
	maximum = 0.299 (at node 16)
Injected packet length average = 17.9771
Accepted packet length average = 18.0296
Total in-flight flits = 13064 (13059 measured)
latency change    = 0.219451
throughput change = 0.00077347
Class 0:
Packet latency average = 1002.33
	minimum = 28
	maximum = 4235
Network latency average = 381.44
	minimum = 23
	maximum = 1881
Slowest packet = 5715
Flit latency average = 353.533
	minimum = 6
	maximum = 2023
Slowest flit = 88703
Fragmentation average = 53.2453
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00911806
	minimum = 0.00233333 (at node 43)
	maximum = 0.015 (at node 54)
Accepted packet rate average = 0.00904514
	minimum = 0.00566667 (at node 4)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.164135
	minimum = 0.042 (at node 43)
	maximum = 0.273 (at node 126)
Accepted flit rate average= 0.162622
	minimum = 0.0976667 (at node 113)
	maximum = 0.279 (at node 16)
Injected packet length average = 18.0011
Accepted packet length average = 17.9789
Total in-flight flits = 13078 (13078 measured)
latency change    = 0.113541
throughput change = 0.00622398
Class 0:
Packet latency average = 1090.67
	minimum = 23
	maximum = 4538
Network latency average = 395.844
	minimum = 23
	maximum = 2099
Slowest packet = 5715
Flit latency average = 358.96
	minimum = 6
	maximum = 2076
Slowest flit = 136691
Fragmentation average = 53.7695
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00915104
	minimum = 0.0035 (at node 121)
	maximum = 0.01625 (at node 54)
Accepted packet rate average = 0.00902214
	minimum = 0.00475 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.164634
	minimum = 0.063 (at node 121)
	maximum = 0.2925 (at node 54)
Accepted flit rate average= 0.162488
	minimum = 0.08425 (at node 4)
	maximum = 0.2535 (at node 16)
Injected packet length average = 17.9908
Accepted packet length average = 18.01
Total in-flight flits = 13961 (13961 measured)
latency change    = 0.0809937
throughput change = 0.000820038
Class 0:
Packet latency average = 1170.25
	minimum = 23
	maximum = 5207
Network latency average = 409.254
	minimum = 23
	maximum = 2099
Slowest packet = 5715
Flit latency average = 366.902
	minimum = 6
	maximum = 2076
Slowest flit = 136691
Fragmentation average = 54.478
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00909167
	minimum = 0.0048 (at node 175)
	maximum = 0.0172 (at node 54)
Accepted packet rate average = 0.00895313
	minimum = 0.006 (at node 36)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.163471
	minimum = 0.0862 (at node 175)
	maximum = 0.3066 (at node 54)
Accepted flit rate average= 0.161086
	minimum = 0.1066 (at node 36)
	maximum = 0.2328 (at node 16)
Injected packet length average = 17.9803
Accepted packet length average = 17.9922
Total in-flight flits = 14853 (14853 measured)
latency change    = 0.0680036
throughput change = 0.0087023
Class 0:
Packet latency average = 1259.38
	minimum = 23
	maximum = 5207
Network latency average = 417.39
	minimum = 23
	maximum = 2099
Slowest packet = 5715
Flit latency average = 372.46
	minimum = 6
	maximum = 2076
Slowest flit = 136691
Fragmentation average = 53.9765
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00903385
	minimum = 0.00483333 (at node 28)
	maximum = 0.016 (at node 54)
Accepted packet rate average = 0.00897569
	minimum = 0.006 (at node 36)
	maximum = 0.0123333 (at node 16)
Injected flit rate average = 0.162622
	minimum = 0.087 (at node 28)
	maximum = 0.287333 (at node 54)
Accepted flit rate average= 0.161425
	minimum = 0.105333 (at node 36)
	maximum = 0.221667 (at node 128)
Injected packet length average = 18.0013
Accepted packet length average = 17.9847
Total in-flight flits = 13864 (13864 measured)
latency change    = 0.0707715
throughput change = 0.00209935
Class 0:
Packet latency average = 1348.4
	minimum = 23
	maximum = 5364
Network latency average = 423.734
	minimum = 23
	maximum = 2278
Slowest packet = 5715
Flit latency average = 376.997
	minimum = 6
	maximum = 2467
Slowest flit = 233586
Fragmentation average = 53.9037
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00908408
	minimum = 0.00542857 (at node 28)
	maximum = 0.0155714 (at node 54)
Accepted packet rate average = 0.0089561
	minimum = 0.00614286 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.163394
	minimum = 0.0977143 (at node 28)
	maximum = 0.278571 (at node 54)
Accepted flit rate average= 0.161238
	minimum = 0.109143 (at node 36)
	maximum = 0.216571 (at node 128)
Injected packet length average = 17.9869
Accepted packet length average = 18.0032
Total in-flight flits = 15522 (15522 measured)
latency change    = 0.0660227
throughput change = 0.00116134
Draining all recorded packets ...
Class 0:
Remaining flits: 290902 290903 290904 290905 290906 290907 290908 290909 290910 290911 [...] (15345 flits)
Measured flits: 290902 290903 290904 290905 290906 290907 290908 290909 290910 290911 [...] (11522 flits)
Class 0:
Remaining flits: 300517 300518 300519 300520 300521 300522 300523 300524 300525 300526 [...] (15910 flits)
Measured flits: 300517 300518 300519 300520 300521 300522 300523 300524 300525 300526 [...] (8161 flits)
Class 0:
Remaining flits: 338850 338851 338852 338853 338854 338855 338856 338857 338858 338859 [...] (16404 flits)
Measured flits: 338850 338851 338852 338853 338854 338855 338856 338857 338858 338859 [...] (5336 flits)
Class 0:
Remaining flits: 384102 384103 384104 384105 384106 384107 384108 384109 384110 384111 [...] (16426 flits)
Measured flits: 384102 384103 384104 384105 384106 384107 384108 384109 384110 384111 [...] (3580 flits)
Class 0:
Remaining flits: 406314 406315 406316 406317 406318 406319 406320 406321 406322 406323 [...] (16635 flits)
Measured flits: 413712 413713 413714 413715 413716 413717 413718 413719 413720 413721 [...] (2239 flits)
Class 0:
Remaining flits: 427734 427735 427736 427737 427738 427739 427740 427741 427742 427743 [...] (16515 flits)
Measured flits: 455507 470249 470717 472374 472375 472376 472377 472378 472379 472380 [...] (1282 flits)
Class 0:
Remaining flits: 489780 489781 489782 489783 489784 489785 489786 489787 489788 489789 [...] (16396 flits)
Measured flits: 501264 501265 501266 501267 501268 501269 501270 501271 501272 501273 [...] (766 flits)
Class 0:
Remaining flits: 508230 508231 508232 508233 508234 508235 508236 508237 508238 508239 [...] (16759 flits)
Measured flits: 538308 538309 538310 538311 538312 538313 538314 538315 538316 538317 [...] (458 flits)
Class 0:
Remaining flits: 508247 508795 508796 508797 508798 508799 508800 508801 508802 508803 [...] (17127 flits)
Measured flits: 567936 567937 567938 567939 567940 567941 567942 567943 567944 567945 [...] (144 flits)
Class 0:
Remaining flits: 574719 574720 574721 578088 578089 578090 578091 578092 578093 578094 [...] (17043 flits)
Measured flits: 622926 622927 622928 622929 622930 622931 622932 622933 622934 622935 [...] (90 flits)
Class 0:
Remaining flits: 597964 597965 597966 597967 597968 597969 597970 597971 597972 597973 [...] (17337 flits)
Measured flits: 648540 648541 648542 648543 648544 648545 648546 648547 648548 648549 [...] (90 flits)
Class 0:
Remaining flits: 623053 623054 623055 623056 623057 623058 623059 623060 623061 623062 [...] (17263 flits)
Measured flits: 672213 672214 672215 672216 672217 672218 672219 672220 672221 672222 [...] (105 flits)
Class 0:
Remaining flits: 658342 658343 658344 658345 658346 658347 658348 658349 662958 662959 [...] (17592 flits)
Measured flits: 683244 683245 683246 683247 683248 683249 683250 683251 683252 683253 [...] (54 flits)
Draining remaining packets ...
Time taken is 24349 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2003.56 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13642 (1 samples)
Network latency average = 463.348 (1 samples)
	minimum = 23 (1 samples)
	maximum = 2724 (1 samples)
Flit latency average = 435.407 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3070 (1 samples)
Fragmentation average = 55.0719 (1 samples)
	minimum = 0 (1 samples)
	maximum = 147 (1 samples)
Injected packet rate average = 0.00908408 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.0155714 (1 samples)
Accepted packet rate average = 0.0089561 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.163394 (1 samples)
	minimum = 0.0977143 (1 samples)
	maximum = 0.278571 (1 samples)
Accepted flit rate average = 0.161238 (1 samples)
	minimum = 0.109143 (1 samples)
	maximum = 0.216571 (1 samples)
Injected packet size average = 17.9869 (1 samples)
Accepted packet size average = 18.0032 (1 samples)
Hops average = 5.07084 (1 samples)
Total run time 15.1842
