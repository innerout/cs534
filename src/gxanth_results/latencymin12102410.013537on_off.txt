BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 275.567
	minimum = 26
	maximum = 969
Network latency average = 223.663
	minimum = 25
	maximum = 809
Slowest packet = 73
Flit latency average = 183.307
	minimum = 6
	maximum = 826
Slowest flit = 10738
Fragmentation average = 52.7461
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00873958
	minimum = 0.003 (at node 172)
	maximum = 0.016 (at node 124)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.164536
	minimum = 0.054 (at node 172)
	maximum = 0.296 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 18.8266
Total in-flight flits = 16559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 395.876
	minimum = 26
	maximum = 1845
Network latency average = 340.363
	minimum = 25
	maximum = 1617
Slowest packet = 73
Flit latency average = 297.71
	minimum = 6
	maximum = 1600
Slowest flit = 16595
Fragmentation average = 60.165
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00907552
	minimum = 0.004 (at node 135)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.167273
	minimum = 0.072 (at node 135)
	maximum = 0.274 (at node 44)
Injected packet length average = 17.9095
Accepted packet length average = 18.4313
Total in-flight flits = 26847 (0 measured)
latency change    = 0.303905
throughput change = 0.0163623
Class 0:
Packet latency average = 742.212
	minimum = 23
	maximum = 2832
Network latency average = 678.761
	minimum = 23
	maximum = 2569
Slowest packet = 961
Flit latency average = 635.933
	minimum = 6
	maximum = 2552
Slowest flit = 17315
Fragmentation average = 70.1611
	minimum = 0
	maximum = 182
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.009375
	minimum = 0.003 (at node 88)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.16888
	minimum = 0.065 (at node 88)
	maximum = 0.306 (at node 152)
Injected packet length average = 18.0569
Accepted packet length average = 18.0139
Total in-flight flits = 39350 (0 measured)
latency change    = 0.466626
throughput change = 0.00951426
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 344.3
	minimum = 23
	maximum = 1012
Network latency average = 288.482
	minimum = 23
	maximum = 951
Slowest packet = 7572
Flit latency average = 844.153
	minimum = 6
	maximum = 3074
Slowest flit = 44027
Fragmentation average = 52.7429
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.00954167
	minimum = 0.001 (at node 138)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.171818
	minimum = 0.02 (at node 138)
	maximum = 0.299 (at node 56)
Injected packet length average = 17.981
Accepted packet length average = 18.0071
Total in-flight flits = 52729 (35467 measured)
latency change    = 1.15571
throughput change = 0.0170966
Class 0:
Packet latency average = 645.011
	minimum = 23
	maximum = 2087
Network latency average = 585.774
	minimum = 23
	maximum = 1962
Slowest packet = 7620
Flit latency average = 979.039
	minimum = 6
	maximum = 4076
Slowest flit = 42641
Fragmentation average = 60.995
	minimum = 0
	maximum = 159
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.00960417
	minimum = 0.0045 (at node 138)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.172823
	minimum = 0.08 (at node 153)
	maximum = 0.277 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 17.9946
Total in-flight flits = 67342 (60747 measured)
latency change    = 0.466211
throughput change = 0.00581641
Class 0:
Packet latency average = 947.575
	minimum = 23
	maximum = 2980
Network latency average = 886.908
	minimum = 23
	maximum = 2904
Slowest packet = 7620
Flit latency average = 1123.01
	minimum = 6
	maximum = 4699
Slowest flit = 55817
Fragmentation average = 65.8111
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.00953993
	minimum = 0.00566667 (at node 17)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.171783
	minimum = 0.104 (at node 31)
	maximum = 0.279 (at node 16)
Injected packet length average = 17.9913
Accepted packet length average = 18.0067
Total in-flight flits = 76933 (74503 measured)
latency change    = 0.319303
throughput change = 0.00605375
Class 0:
Packet latency average = 1202.59
	minimum = 23
	maximum = 4076
Network latency average = 1139.56
	minimum = 23
	maximum = 3880
Slowest packet = 7832
Flit latency average = 1261.47
	minimum = 6
	maximum = 5449
Slowest flit = 70363
Fragmentation average = 66.7264
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0133529
	minimum = 0.00175 (at node 78)
	maximum = 0.02675 (at node 97)
Accepted packet rate average = 0.00960807
	minimum = 0.00625 (at node 79)
	maximum = 0.01325 (at node 16)
Injected flit rate average = 0.24025
	minimum = 0.0315 (at node 78)
	maximum = 0.48225 (at node 97)
Accepted flit rate average= 0.173078
	minimum = 0.113 (at node 36)
	maximum = 0.238 (at node 159)
Injected packet length average = 17.9924
Accepted packet length average = 18.0138
Total in-flight flits = 91016 (89999 measured)
latency change    = 0.212057
throughput change = 0.00748297
Class 0:
Packet latency average = 1377.28
	minimum = 23
	maximum = 4841
Network latency average = 1313.45
	minimum = 23
	maximum = 4824
Slowest packet = 7907
Flit latency average = 1371.25
	minimum = 6
	maximum = 6158
Slowest flit = 52001
Fragmentation average = 68.4335
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0133448
	minimum = 0.004 (at node 78)
	maximum = 0.0256 (at node 109)
Accepted packet rate average = 0.00960417
	minimum = 0.0064 (at node 86)
	maximum = 0.0124 (at node 159)
Injected flit rate average = 0.240096
	minimum = 0.0712 (at node 119)
	maximum = 0.4608 (at node 109)
Accepted flit rate average= 0.172979
	minimum = 0.1142 (at node 86)
	maximum = 0.2248 (at node 182)
Injected packet length average = 17.9917
Accepted packet length average = 18.0108
Total in-flight flits = 103888 (103344 measured)
latency change    = 0.126836
throughput change = 0.000572082
Class 0:
Packet latency average = 1551.01
	minimum = 23
	maximum = 5977
Network latency average = 1486.68
	minimum = 23
	maximum = 5833
Slowest packet = 7910
Flit latency average = 1498.9
	minimum = 6
	maximum = 6518
Slowest flit = 89837
Fragmentation average = 67.8796
	minimum = 0
	maximum = 159
Injected packet rate average = 0.013237
	minimum = 0.00566667 (at node 80)
	maximum = 0.0251667 (at node 28)
Accepted packet rate average = 0.00964497
	minimum = 0.00683333 (at node 79)
	maximum = 0.0121667 (at node 103)
Injected flit rate average = 0.238133
	minimum = 0.102 (at node 80)
	maximum = 0.450333 (at node 28)
Accepted flit rate average= 0.173474
	minimum = 0.126167 (at node 79)
	maximum = 0.219 (at node 159)
Injected packet length average = 17.99
Accepted packet length average = 17.986
Total in-flight flits = 113990 (113716 measured)
latency change    = 0.11201
throughput change = 0.00285225
Class 0:
Packet latency average = 1707.21
	minimum = 23
	maximum = 6930
Network latency average = 1641.88
	minimum = 23
	maximum = 6772
Slowest packet = 7559
Flit latency average = 1624.58
	minimum = 6
	maximum = 7142
Slowest flit = 115307
Fragmentation average = 68.1127
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0134241
	minimum = 0.00642857 (at node 80)
	maximum = 0.025 (at node 28)
Accepted packet rate average = 0.00964435
	minimum = 0.00685714 (at node 79)
	maximum = 0.0122857 (at node 103)
Injected flit rate average = 0.241589
	minimum = 0.115 (at node 80)
	maximum = 0.448571 (at node 28)
Accepted flit rate average= 0.173501
	minimum = 0.124 (at node 79)
	maximum = 0.220143 (at node 103)
Injected packet length average = 17.9967
Accepted packet length average = 17.9899
Total in-flight flits = 130921 (130849 measured)
latency change    = 0.0914963
throughput change = 0.000154384
Draining all recorded packets ...
Class 0:
Remaining flits: 128359 128360 128361 128362 128363 128364 128365 128366 128367 128368 [...] (142933 flits)
Measured flits: 137196 137197 137198 137199 137200 137201 137202 137203 137204 137205 [...] (103830 flits)
Class 0:
Remaining flits: 156780 156781 156782 156783 156784 156785 156786 156787 156788 156789 [...] (156814 flits)
Measured flits: 156780 156781 156782 156783 156784 156785 156786 156787 156788 156789 [...] (78977 flits)
Class 0:
Remaining flits: 156780 156781 156782 156783 156784 156785 156786 156787 156788 156789 [...] (166937 flits)
Measured flits: 156780 156781 156782 156783 156784 156785 156786 156787 156788 156789 [...] (57932 flits)
Class 0:
Remaining flits: 158490 158491 158492 158493 158494 158495 158496 158497 158498 158499 [...] (181878 flits)
Measured flits: 158490 158491 158492 158493 158494 158495 158496 158497 158498 158499 [...] (40267 flits)
Class 0:
Remaining flits: 225381 225382 225383 225384 225385 225386 225387 225388 225389 225390 [...] (194610 flits)
Measured flits: 225381 225382 225383 225384 225385 225386 225387 225388 225389 225390 [...] (26345 flits)
Class 0:
Remaining flits: 232398 232399 232400 232401 232402 232403 232404 232405 232406 232407 [...] (206339 flits)
Measured flits: 232398 232399 232400 232401 232402 232403 232404 232405 232406 232407 [...] (16966 flits)
Class 0:
Remaining flits: 244908 244909 244910 244911 244912 244913 244914 244915 244916 244917 [...] (219745 flits)
Measured flits: 244908 244909 244910 244911 244912 244913 244914 244915 244916 244917 [...] (10363 flits)
Class 0:
Remaining flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (232167 flits)
Measured flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (6022 flits)
Class 0:
Remaining flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (242178 flits)
Measured flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (3140 flits)
Class 0:
Remaining flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (251697 flits)
Measured flits: 292536 292537 292538 292539 292540 292541 292542 292543 292544 292545 [...] (1906 flits)
Class 0:
Remaining flits: 337320 337321 337322 337323 337324 337325 337326 337327 337328 337329 [...] (264132 flits)
Measured flits: 337320 337321 337322 337323 337324 337325 337326 337327 337328 337329 [...] (1119 flits)
Class 0:
Remaining flits: 345510 345511 345512 345513 345514 345515 345516 345517 345518 345519 [...] (280150 flits)
Measured flits: 345510 345511 345512 345513 345514 345515 345516 345517 345518 345519 [...] (540 flits)
Class 0:
Remaining flits: 348984 348985 348986 348987 348988 348989 348990 348991 348992 348993 [...] (296055 flits)
Measured flits: 348984 348985 348986 348987 348988 348989 348990 348991 348992 348993 [...] (288 flits)
Class 0:
Remaining flits: 355284 355285 355286 355287 355288 355289 355290 355291 355292 355293 [...] (312061 flits)
Measured flits: 355284 355285 355286 355287 355288 355289 355290 355291 355292 355293 [...] (90 flits)
Class 0:
Remaining flits: 384534 384535 384536 384537 384538 384539 384540 384541 384542 384543 [...] (327113 flits)
Measured flits: 384534 384535 384536 384537 384538 384539 384540 384541 384542 384543 [...] (36 flits)
Class 0:
Remaining flits: 384534 384535 384536 384537 384538 384539 384540 384541 384542 384543 [...] (339784 flits)
Measured flits: 384534 384535 384536 384537 384538 384539 384540 384541 384542 384543 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 509454 509455 509456 509457 509458 509459 509460 509461 509462 509463 [...] (318447 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 564048 564049 564050 564051 564052 564053 564054 564055 564056 564057 [...] (288832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 594126 594127 594128 594129 594130 594131 594132 594133 594134 594135 [...] (259206 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 644730 644731 644732 644733 644734 644735 644736 644737 644738 644739 [...] (229832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 652770 652771 652772 652773 652774 652775 652776 652777 652778 652779 [...] (201089 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 665460 665461 665462 665463 665464 665465 665466 665467 665468 665469 [...] (172040 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 670680 670681 670682 670683 670684 670685 670686 670687 670688 670689 [...] (143580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 697644 697645 697646 697647 697648 697649 697650 697651 697652 697653 [...] (115513 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 776252 776253 776254 776255 776256 776257 776258 776259 776260 776261 [...] (87277 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 806652 806653 806654 806655 806656 806657 806658 806659 806660 806661 [...] (60843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 894816 894817 894818 894819 894820 894821 894822 894823 894824 894825 [...] (37096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 935388 935389 935390 935391 935392 935393 935394 935395 935396 935397 [...] (19151 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 960192 960193 960194 960195 960196 960197 960198 960199 960200 960201 [...] (7788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1081109 1081110 1081111 1081112 1081113 1081114 1081115 1119996 1119997 1119998 [...] (1297 flits)
Measured flits: (0 flits)
Time taken is 40993 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3036.82 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18163 (1 samples)
Network latency average = 2968.76 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18078 (1 samples)
Flit latency average = 5820.42 (1 samples)
	minimum = 6 (1 samples)
	maximum = 20150 (1 samples)
Fragmentation average = 71.9931 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.0134241 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.025 (1 samples)
Accepted packet rate average = 0.00964435 (1 samples)
	minimum = 0.00685714 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.241589 (1 samples)
	minimum = 0.115 (1 samples)
	maximum = 0.448571 (1 samples)
Accepted flit rate average = 0.173501 (1 samples)
	minimum = 0.124 (1 samples)
	maximum = 0.220143 (1 samples)
Injected packet size average = 17.9967 (1 samples)
Accepted packet size average = 17.9899 (1 samples)
Hops average = 5.07556 (1 samples)
Total run time 22.8734
