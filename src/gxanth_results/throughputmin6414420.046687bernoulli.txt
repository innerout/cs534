BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.658
	minimum = 24
	maximum = 909
Network latency average = 300.943
	minimum = 22
	maximum = 835
Slowest packet = 765
Flit latency average = 270.468
	minimum = 5
	maximum = 853
Slowest flit = 13066
Fragmentation average = 81.731
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0143073
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.285307
	minimum = 0.117 (at node 64)
	maximum = 0.474 (at node 22)
Injected packet length average = 17.8602
Accepted packet length average = 19.9414
Total in-flight flits = 99589 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 598.561
	minimum = 24
	maximum = 1722
Network latency average = 573.205
	minimum = 22
	maximum = 1671
Slowest packet = 765
Flit latency average = 528.386
	minimum = 5
	maximum = 1709
Slowest flit = 39652
Fragmentation average = 136.398
	minimum = 0
	maximum = 824
Injected packet rate average = 0.0457682
	minimum = 0.0315 (at node 10)
	maximum = 0.055 (at node 42)
Accepted packet rate average = 0.0155052
	minimum = 0.0095 (at node 83)
	maximum = 0.0225 (at node 70)
Injected flit rate average = 0.820263
	minimum = 0.567 (at node 10)
	maximum = 0.9855 (at node 135)
Accepted flit rate average= 0.29712
	minimum = 0.18 (at node 135)
	maximum = 0.421 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 19.1626
Total in-flight flits = 202256 (0 measured)
latency change    = 0.465955
throughput change = 0.0397567
Class 0:
Packet latency average = 1368.74
	minimum = 24
	maximum = 2574
Network latency average = 1331.33
	minimum = 23
	maximum = 2478
Slowest packet = 3195
Flit latency average = 1285.53
	minimum = 5
	maximum = 2507
Slowest flit = 71097
Fragmentation average = 236.539
	minimum = 0
	maximum = 963
Injected packet rate average = 0.0447344
	minimum = 0.024 (at node 126)
	maximum = 0.056 (at node 178)
Accepted packet rate average = 0.016901
	minimum = 0.008 (at node 22)
	maximum = 0.03 (at node 56)
Injected flit rate average = 0.804958
	minimum = 0.436 (at node 126)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.313333
	minimum = 0.159 (at node 136)
	maximum = 0.502 (at node 81)
Injected packet length average = 17.9942
Accepted packet length average = 18.5393
Total in-flight flits = 296698 (0 measured)
latency change    = 0.562693
throughput change = 0.0517453
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 133.908
	minimum = 23
	maximum = 1115
Network latency average = 41.7185
	minimum = 23
	maximum = 326
Slowest packet = 26194
Flit latency average = 1830.9
	minimum = 5
	maximum = 3489
Slowest flit = 73078
Fragmentation average = 6.42462
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0422187
	minimum = 0.019 (at node 156)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0166302
	minimum = 0.009 (at node 4)
	maximum = 0.034 (at node 120)
Injected flit rate average = 0.760005
	minimum = 0.353 (at node 156)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.308979
	minimum = 0.153 (at node 131)
	maximum = 0.527 (at node 120)
Injected packet length average = 18.0016
Accepted packet length average = 18.5794
Total in-flight flits = 383282 (134041 measured)
latency change    = 9.22153
throughput change = 0.0140921
Class 0:
Packet latency average = 176.033
	minimum = 23
	maximum = 1512
Network latency average = 50.4037
	minimum = 23
	maximum = 727
Slowest packet = 26194
Flit latency average = 2105.83
	minimum = 5
	maximum = 4411
Slowest flit = 85669
Fragmentation average = 6.58696
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0421536
	minimum = 0.0225 (at node 164)
	maximum = 0.056 (at node 61)
Accepted packet rate average = 0.0166693
	minimum = 0.0095 (at node 131)
	maximum = 0.025 (at node 118)
Injected flit rate average = 0.758602
	minimum = 0.4 (at node 164)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.30674
	minimum = 0.1755 (at node 36)
	maximum = 0.438 (at node 118)
Injected packet length average = 17.9961
Accepted packet length average = 18.4015
Total in-flight flits = 470276 (268035 measured)
latency change    = 0.239302
throughput change = 0.00730125
Class 0:
Packet latency average = 236.773
	minimum = 23
	maximum = 1946
Network latency average = 79.8375
	minimum = 23
	maximum = 1280
Slowest packet = 26194
Flit latency average = 2402.67
	minimum = 5
	maximum = 5184
Slowest flit = 123650
Fragmentation average = 6.8267
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0420226
	minimum = 0.022 (at node 84)
	maximum = 0.0556667 (at node 5)
Accepted packet rate average = 0.0165417
	minimum = 0.00933333 (at node 36)
	maximum = 0.0246667 (at node 118)
Injected flit rate average = 0.756312
	minimum = 0.392667 (at node 132)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.303524
	minimum = 0.178333 (at node 36)
	maximum = 0.447667 (at node 118)
Injected packet length average = 17.9978
Accepted packet length average = 18.3491
Total in-flight flits = 557558 (402157 measured)
latency change    = 0.256534
throughput change = 0.0105931
Draining remaining packets ...
Class 0:
Remaining flits: 143208 143209 143210 143211 143212 143213 143214 143215 143216 143217 [...] (507674 flits)
Measured flits: 470952 470953 470954 470955 470956 470957 470958 470959 470960 470961 [...] (398391 flits)
Class 0:
Remaining flits: 176091 176092 176093 199835 201370 201371 201372 201373 201374 201375 [...] (460480 flits)
Measured flits: 470952 470953 470954 470955 470956 470957 470958 470959 470960 470961 [...] (391764 flits)
Class 0:
Remaining flits: 255015 255016 255017 255018 255019 255020 255021 255022 255023 256299 [...] (413308 flits)
Measured flits: 470952 470953 470954 470955 470956 470957 470958 470959 470960 470961 [...] (378229 flits)
Class 0:
Remaining flits: 277704 277705 277706 277707 277708 277709 277710 277711 277712 277713 [...] (366454 flits)
Measured flits: 470952 470953 470954 470955 470956 470957 470958 470959 470960 470961 [...] (353460 flits)
Class 0:
Remaining flits: 340266 340267 340268 340269 340270 340271 358105 358106 358107 358108 [...] (319184 flits)
Measured flits: 470970 470971 470972 470973 470974 470975 470976 470977 470978 470979 [...] (316654 flits)
Class 0:
Remaining flits: 430560 430561 430562 430563 430564 430565 430566 430567 430568 430569 [...] (271608 flits)
Measured flits: 470985 470986 470987 471585 471586 471587 471588 471589 471590 471591 [...] (271367 flits)
Class 0:
Remaining flits: 455145 455146 455147 459360 459361 459362 459363 459364 459365 459366 [...] (224200 flits)
Measured flits: 476279 477446 477447 477448 477449 479304 479305 479306 479307 479308 [...] (224177 flits)
Class 0:
Remaining flits: 479320 479321 508280 508281 508282 508283 513036 513037 513038 513039 [...] (176576 flits)
Measured flits: 479320 479321 508280 508281 508282 508283 513036 513037 513038 513039 [...] (176576 flits)
Class 0:
Remaining flits: 516006 516007 516008 516009 516010 516011 516012 516013 516014 516015 [...] (129380 flits)
Measured flits: 516006 516007 516008 516009 516010 516011 516012 516013 516014 516015 [...] (129380 flits)
Class 0:
Remaining flits: 565415 567280 567281 567282 567283 567284 567285 567286 567287 583038 [...] (82339 flits)
Measured flits: 565415 567280 567281 567282 567283 567284 567285 567286 567287 583038 [...] (82339 flits)
Class 0:
Remaining flits: 583051 583052 583053 583054 583055 604404 604405 604406 604407 604408 [...] (35915 flits)
Measured flits: 583051 583052 583053 583054 583055 604404 604405 604406 604407 604408 [...] (35915 flits)
Class 0:
Remaining flits: 649900 649901 649902 649903 649904 649905 649906 649907 670692 670693 [...] (4919 flits)
Measured flits: 649900 649901 649902 649903 649904 649905 649906 649907 670692 670693 [...] (4919 flits)
Class 0:
Remaining flits: 804371 804372 804373 804374 804375 804376 804377 804378 804379 804380 [...] (49 flits)
Measured flits: 804371 804372 804373 804374 804375 804376 804377 804378 804379 804380 [...] (49 flits)
Time taken is 19099 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8527.81 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14058 (1 samples)
Network latency average = 8362.93 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14021 (1 samples)
Flit latency average = 6625.11 (1 samples)
	minimum = 5 (1 samples)
	maximum = 14004 (1 samples)
Fragmentation average = 311.176 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1422 (1 samples)
Injected packet rate average = 0.0420226 (1 samples)
	minimum = 0.022 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0165417 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0246667 (1 samples)
Injected flit rate average = 0.756312 (1 samples)
	minimum = 0.392667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.303524 (1 samples)
	minimum = 0.178333 (1 samples)
	maximum = 0.447667 (1 samples)
Injected packet size average = 17.9978 (1 samples)
Accepted packet size average = 18.3491 (1 samples)
Hops average = 5.14369 (1 samples)
Total run time 26.1085
