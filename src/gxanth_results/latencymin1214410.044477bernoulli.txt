BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 347.009
	minimum = 25
	maximum = 940
Network latency average = 328.994
	minimum = 23
	maximum = 940
Slowest packet = 181
Flit latency average = 298.226
	minimum = 6
	maximum = 936
Slowest flit = 4912
Fragmentation average = 58.0232
	minimum = 0
	maximum = 164
Injected packet rate average = 0.0378542
	minimum = 0.021 (at node 12)
	maximum = 0.05 (at node 33)
Accepted packet rate average = 0.0103385
	minimum = 0.005 (at node 41)
	maximum = 0.019 (at node 27)
Injected flit rate average = 0.674458
	minimum = 0.361 (at node 12)
	maximum = 0.893 (at node 101)
Accepted flit rate average= 0.19451
	minimum = 0.09 (at node 41)
	maximum = 0.342 (at node 27)
Injected packet length average = 17.8173
Accepted packet length average = 18.8141
Total in-flight flits = 93622 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 707.261
	minimum = 25
	maximum = 1847
Network latency average = 663.671
	minimum = 23
	maximum = 1780
Slowest packet = 1473
Flit latency average = 627.855
	minimum = 6
	maximum = 1815
Slowest flit = 22766
Fragmentation average = 64.0294
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0284453
	minimum = 0.016 (at node 112)
	maximum = 0.0365 (at node 77)
Accepted packet rate average = 0.0100833
	minimum = 0.0055 (at node 83)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.509164
	minimum = 0.287 (at node 112)
	maximum = 0.657 (at node 83)
Accepted flit rate average= 0.185401
	minimum = 0.099 (at node 115)
	maximum = 0.288 (at node 44)
Injected packet length average = 17.8998
Accepted packet length average = 18.3869
Total in-flight flits = 126518 (0 measured)
latency change    = 0.509363
throughput change = 0.0491334
Class 0:
Packet latency average = 1887.44
	minimum = 274
	maximum = 2783
Network latency average = 1717.62
	minimum = 27
	maximum = 2719
Slowest packet = 1895
Flit latency average = 1687
	minimum = 6
	maximum = 2730
Slowest flit = 35901
Fragmentation average = 71.1107
	minimum = 0
	maximum = 153
Injected packet rate average = 0.010625
	minimum = 0 (at node 7)
	maximum = 0.031 (at node 129)
Accepted packet rate average = 0.00940625
	minimum = 0.003 (at node 65)
	maximum = 0.017 (at node 73)
Injected flit rate average = 0.193557
	minimum = 0 (at node 7)
	maximum = 0.548 (at node 129)
Accepted flit rate average= 0.168271
	minimum = 0.061 (at node 31)
	maximum = 0.298 (at node 73)
Injected packet length average = 18.2172
Accepted packet length average = 17.8893
Total in-flight flits = 131992 (0 measured)
latency change    = 0.625281
throughput change = 0.101801
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2157.74
	minimum = 893
	maximum = 3093
Network latency average = 218.283
	minimum = 26
	maximum = 909
Slowest packet = 13156
Flit latency average = 2365.28
	minimum = 6
	maximum = 3672
Slowest flit = 38753
Fragmentation average = 31.1509
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00804167
	minimum = 0 (at node 41)
	maximum = 0.029 (at node 14)
Accepted packet rate average = 0.00899479
	minimum = 0.003 (at node 79)
	maximum = 0.017 (at node 3)
Injected flit rate average = 0.145156
	minimum = 0 (at node 41)
	maximum = 0.522 (at node 14)
Accepted flit rate average= 0.16163
	minimum = 0.058 (at node 79)
	maximum = 0.288 (at node 51)
Injected packet length average = 18.0505
Accepted packet length average = 17.9693
Total in-flight flits = 128877 (26925 measured)
latency change    = 0.125267
throughput change = 0.0410853
Class 0:
Packet latency average = 2887.21
	minimum = 893
	maximum = 4198
Network latency average = 592.124
	minimum = 25
	maximum = 1922
Slowest packet = 13156
Flit latency average = 2635.3
	minimum = 6
	maximum = 4446
Slowest flit = 74093
Fragmentation average = 44.6571
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00811198
	minimum = 0.0005 (at node 82)
	maximum = 0.025 (at node 55)
Accepted packet rate average = 0.00901302
	minimum = 0.004 (at node 167)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.146336
	minimum = 0.009 (at node 82)
	maximum = 0.448 (at node 114)
Accepted flit rate average= 0.162273
	minimum = 0.0755 (at node 141)
	maximum = 0.2815 (at node 16)
Injected packet length average = 18.0395
Accepted packet length average = 18.0043
Total in-flight flits = 125911 (52242 measured)
latency change    = 0.252658
throughput change = 0.00396386
Class 0:
Packet latency average = 3634.86
	minimum = 893
	maximum = 5074
Network latency average = 1113.93
	minimum = 25
	maximum = 2882
Slowest packet = 13156
Flit latency average = 2866.67
	minimum = 6
	maximum = 5483
Slowest flit = 69770
Fragmentation average = 55.3237
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00812847
	minimum = 0.000333333 (at node 82)
	maximum = 0.0206667 (at node 55)
Accepted packet rate average = 0.00894792
	minimum = 0.005 (at node 5)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.146563
	minimum = 0.006 (at node 82)
	maximum = 0.372 (at node 55)
Accepted flit rate average= 0.161319
	minimum = 0.09 (at node 5)
	maximum = 0.235333 (at node 16)
Injected packet length average = 18.0308
Accepted packet length average = 18.0287
Total in-flight flits = 123402 (73250 measured)
latency change    = 0.205688
throughput change = 0.00591369
Class 0:
Packet latency average = 4276.72
	minimum = 893
	maximum = 5918
Network latency average = 1548.26
	minimum = 25
	maximum = 3974
Slowest packet = 13156
Flit latency average = 3055.43
	minimum = 6
	maximum = 6370
Slowest flit = 83117
Fragmentation average = 59.0825
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00836719
	minimum = 0.00175 (at node 152)
	maximum = 0.0175 (at node 72)
Accepted packet rate average = 0.00899089
	minimum = 0.00475 (at node 79)
	maximum = 0.01275 (at node 103)
Injected flit rate average = 0.150635
	minimum = 0.0315 (at node 152)
	maximum = 0.31775 (at node 72)
Accepted flit rate average= 0.162042
	minimum = 0.08675 (at node 79)
	maximum = 0.22875 (at node 103)
Injected packet length average = 18.0031
Accepted packet length average = 18.0229
Total in-flight flits = 123176 (92581 measured)
latency change    = 0.150081
throughput change = 0.00445702
Class 0:
Packet latency average = 4865.17
	minimum = 893
	maximum = 7005
Network latency average = 2006.46
	minimum = 25
	maximum = 4980
Slowest packet = 13156
Flit latency average = 3235.76
	minimum = 6
	maximum = 7088
Slowest flit = 115919
Fragmentation average = 62.1677
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00848958
	minimum = 0.002 (at node 181)
	maximum = 0.0174 (at node 72)
Accepted packet rate average = 0.00897083
	minimum = 0.0046 (at node 79)
	maximum = 0.0126 (at node 128)
Injected flit rate average = 0.152894
	minimum = 0.036 (at node 181)
	maximum = 0.3154 (at node 72)
Accepted flit rate average= 0.16167
	minimum = 0.0828 (at node 79)
	maximum = 0.2264 (at node 128)
Injected packet length average = 18.0096
Accepted packet length average = 18.0217
Total in-flight flits = 123561 (107435 measured)
latency change    = 0.120952
throughput change = 0.00230021
Class 0:
Packet latency average = 5468.48
	minimum = 893
	maximum = 7924
Network latency average = 2394.75
	minimum = 23
	maximum = 5937
Slowest packet = 13156
Flit latency average = 3358.81
	minimum = 6
	maximum = 7976
Slowest flit = 132752
Fragmentation average = 65.8011
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00846441
	minimum = 0.003 (at node 181)
	maximum = 0.0163333 (at node 55)
Accepted packet rate average = 0.00893837
	minimum = 0.00516667 (at node 79)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.152534
	minimum = 0.0536667 (at node 181)
	maximum = 0.294 (at node 55)
Accepted flit rate average= 0.161126
	minimum = 0.0923333 (at node 79)
	maximum = 0.215333 (at node 128)
Injected packet length average = 18.0206
Accepted packet length average = 18.0263
Total in-flight flits = 121929 (114641 measured)
latency change    = 0.110325
throughput change = 0.00337577
Class 0:
Packet latency average = 6040.96
	minimum = 893
	maximum = 8825
Network latency average = 2712.17
	minimum = 23
	maximum = 6970
Slowest packet = 13156
Flit latency average = 3435.03
	minimum = 6
	maximum = 8987
Slowest flit = 126342
Fragmentation average = 67.9322
	minimum = 0
	maximum = 173
Injected packet rate average = 0.00839955
	minimum = 0.003 (at node 110)
	maximum = 0.0164286 (at node 65)
Accepted packet rate average = 0.0088683
	minimum = 0.00514286 (at node 79)
	maximum = 0.0114286 (at node 128)
Injected flit rate average = 0.151186
	minimum = 0.054 (at node 181)
	maximum = 0.295714 (at node 65)
Accepted flit rate average= 0.159693
	minimum = 0.0947143 (at node 79)
	maximum = 0.207 (at node 128)
Injected packet length average = 17.9993
Accepted packet length average = 18.0071
Total in-flight flits = 120639 (117303 measured)
latency change    = 0.094767
throughput change = 0.00897448
Draining all recorded packets ...
Class 0:
Remaining flits: 164826 164827 164828 164829 164830 164831 164832 164833 164834 164835 [...] (120252 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (118894 flits)
Class 0:
Remaining flits: 183996 183997 183998 183999 184000 184001 184002 184003 184004 184005 [...] (120900 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (120291 flits)
Class 0:
Remaining flits: 200592 200593 200594 200595 200596 200597 200598 200599 200600 200601 [...] (119244 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (119000 flits)
Class 0:
Remaining flits: 200592 200593 200594 200595 200596 200597 200598 200599 200600 200601 [...] (120724 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (120616 flits)
Class 0:
Remaining flits: 209376 209377 209378 209379 209380 209381 209382 209383 209384 209385 [...] (119427 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (119403 flits)
Class 0:
Remaining flits: 246654 246655 246656 246657 246658 246659 246660 246661 246662 246663 [...] (120986 flits)
Measured flits: 246654 246655 246656 246657 246658 246659 246660 246661 246662 246663 [...] (120986 flits)
Class 0:
Remaining flits: 252594 252595 252596 252597 252598 252599 252600 252601 252602 252603 [...] (120726 flits)
Measured flits: 252594 252595 252596 252597 252598 252599 252600 252601 252602 252603 [...] (120726 flits)
Class 0:
Remaining flits: 252594 252595 252596 252597 252598 252599 252600 252601 252602 252603 [...] (120502 flits)
Measured flits: 252594 252595 252596 252597 252598 252599 252600 252601 252602 252603 [...] (120502 flits)
Class 0:
Remaining flits: 262026 262027 262028 262029 262030 262031 262032 262033 262034 262035 [...] (120678 flits)
Measured flits: 262026 262027 262028 262029 262030 262031 262032 262033 262034 262035 [...] (120678 flits)
Class 0:
Remaining flits: 361836 361837 361838 361839 361840 361841 361842 361843 361844 361845 [...] (122017 flits)
Measured flits: 361836 361837 361838 361839 361840 361841 361842 361843 361844 361845 [...] (122017 flits)
Class 0:
Remaining flits: 426258 426259 426260 426261 426262 426263 426264 426265 426266 426267 [...] (120509 flits)
Measured flits: 426258 426259 426260 426261 426262 426263 426264 426265 426266 426267 [...] (120509 flits)
Class 0:
Remaining flits: 497484 497485 497486 497487 497488 497489 497490 497491 497492 497493 [...] (119915 flits)
Measured flits: 497484 497485 497486 497487 497488 497489 497490 497491 497492 497493 [...] (119915 flits)
Class 0:
Remaining flits: 536868 536869 536870 536871 536872 536873 536874 536875 536876 536877 [...] (119688 flits)
Measured flits: 536868 536869 536870 536871 536872 536873 536874 536875 536876 536877 [...] (119688 flits)
Class 0:
Remaining flits: 542484 542485 542486 542487 542488 542489 542490 542491 542492 542493 [...] (118856 flits)
Measured flits: 542484 542485 542486 542487 542488 542489 542490 542491 542492 542493 [...] (118856 flits)
Class 0:
Remaining flits: 542484 542485 542486 542487 542488 542489 542490 542491 542492 542493 [...] (119013 flits)
Measured flits: 542484 542485 542486 542487 542488 542489 542490 542491 542492 542493 [...] (119013 flits)
Class 0:
Remaining flits: 590634 590635 590636 590637 590638 590639 590640 590641 590642 590643 [...] (120124 flits)
Measured flits: 590634 590635 590636 590637 590638 590639 590640 590641 590642 590643 [...] (120124 flits)
Class 0:
Remaining flits: 602118 602119 602120 602121 602122 602123 602124 602125 602126 602127 [...] (117859 flits)
Measured flits: 602118 602119 602120 602121 602122 602123 602124 602125 602126 602127 [...] (117859 flits)
Class 0:
Remaining flits: 649275 649276 649277 649710 649711 649712 649713 649714 649715 649716 [...] (118145 flits)
Measured flits: 649275 649276 649277 649710 649711 649712 649713 649714 649715 649716 [...] (118145 flits)
Class 0:
Remaining flits: 661050 661051 661052 661053 661054 661055 661056 661057 661058 661059 [...] (118655 flits)
Measured flits: 661050 661051 661052 661053 661054 661055 661056 661057 661058 661059 [...] (118655 flits)
Class 0:
Remaining flits: 683046 683047 683048 683049 683050 683051 683052 683053 683054 683055 [...] (118804 flits)
Measured flits: 683046 683047 683048 683049 683050 683051 683052 683053 683054 683055 [...] (118804 flits)
Class 0:
Remaining flits: 720612 720613 720614 720615 720616 720617 720618 720619 720620 720621 [...] (119620 flits)
Measured flits: 720612 720613 720614 720615 720616 720617 720618 720619 720620 720621 [...] (119620 flits)
Class 0:
Remaining flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (119783 flits)
Measured flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (119783 flits)
Class 0:
Remaining flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (121143 flits)
Measured flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (120981 flits)
Class 0:
Remaining flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (122257 flits)
Measured flits: 771534 771535 771536 771537 771538 771539 771540 771541 771542 771543 [...] (121915 flits)
Class 0:
Remaining flits: 852858 852859 852860 852861 852862 852863 852864 852865 852866 852867 [...] (122004 flits)
Measured flits: 852858 852859 852860 852861 852862 852863 852864 852865 852866 852867 [...] (121194 flits)
Class 0:
Remaining flits: 897876 897877 897878 897879 897880 897881 897882 897883 897884 897885 [...] (122116 flits)
Measured flits: 897876 897877 897878 897879 897880 897881 897882 897883 897884 897885 [...] (120208 flits)
Class 0:
Remaining flits: 902304 902305 902306 902307 902308 902309 902310 902311 902312 902313 [...] (120688 flits)
Measured flits: 902304 902305 902306 902307 902308 902309 902310 902311 902312 902313 [...] (116602 flits)
Class 0:
Remaining flits: 945738 945739 945740 945741 945742 945743 945744 945745 945746 945747 [...] (119037 flits)
Measured flits: 945738 945739 945740 945741 945742 945743 945744 945745 945746 945747 [...] (113297 flits)
Class 0:
Remaining flits: 952812 952813 952814 952815 952816 952817 952818 952819 952820 952821 [...] (118476 flits)
Measured flits: 952812 952813 952814 952815 952816 952817 952818 952819 952820 952821 [...] (110910 flits)
Class 0:
Remaining flits: 1005570 1005571 1005572 1005573 1005574 1005575 1005576 1005577 1005578 1005579 [...] (121477 flits)
Measured flits: 1005570 1005571 1005572 1005573 1005574 1005575 1005576 1005577 1005578 1005579 [...] (109560 flits)
Class 0:
Remaining flits: 1027242 1027243 1027244 1027245 1027246 1027247 1027248 1027249 1027250 1027251 [...] (122536 flits)
Measured flits: 1027242 1027243 1027244 1027245 1027246 1027247 1027248 1027249 1027250 1027251 [...] (107335 flits)
Class 0:
Remaining flits: 1038636 1038637 1038638 1038639 1038640 1038641 1038642 1038643 1038644 1038645 [...] (121944 flits)
Measured flits: 1038636 1038637 1038638 1038639 1038640 1038641 1038642 1038643 1038644 1038645 [...] (101986 flits)
Class 0:
Remaining flits: 1070604 1070605 1070606 1070607 1070608 1070609 1070610 1070611 1070612 1070613 [...] (123463 flits)
Measured flits: 1070604 1070605 1070606 1070607 1070608 1070609 1070610 1070611 1070612 1070613 [...] (97140 flits)
Class 0:
Remaining flits: 1070615 1070616 1070617 1070618 1070619 1070620 1070621 1078596 1078597 1078598 [...] (123616 flits)
Measured flits: 1070615 1070616 1070617 1070618 1070619 1070620 1070621 1078596 1078597 1078598 [...] (91995 flits)
Class 0:
Remaining flits: 1097586 1097587 1097588 1097589 1097590 1097591 1097592 1097593 1097594 1097595 [...] (120359 flits)
Measured flits: 1097586 1097587 1097588 1097589 1097590 1097591 1097592 1097593 1097594 1097595 [...] (82322 flits)
Class 0:
Remaining flits: 1142820 1142821 1142822 1142823 1142824 1142825 1142826 1142827 1142828 1142829 [...] (120036 flits)
Measured flits: 1142820 1142821 1142822 1142823 1142824 1142825 1142826 1142827 1142828 1142829 [...] (73381 flits)
Class 0:
Remaining flits: 1194300 1194301 1194302 1194303 1194304 1194305 1194306 1194307 1194308 1194309 [...] (120506 flits)
Measured flits: 1194300 1194301 1194302 1194303 1194304 1194305 1194306 1194307 1194308 1194309 [...] (65645 flits)
Class 0:
Remaining flits: 1302048 1302049 1302050 1302051 1302052 1302053 1302054 1302055 1302056 1302057 [...] (119491 flits)
Measured flits: 1302048 1302049 1302050 1302051 1302052 1302053 1302054 1302055 1302056 1302057 [...] (57123 flits)
Class 0:
Remaining flits: 1328742 1328743 1328744 1328745 1328746 1328747 1328748 1328749 1328750 1328751 [...] (119641 flits)
Measured flits: 1328742 1328743 1328744 1328745 1328746 1328747 1328748 1328749 1328750 1328751 [...] (48887 flits)
Class 0:
Remaining flits: 1344852 1344853 1344854 1344855 1344856 1344857 1344858 1344859 1344860 1344861 [...] (120798 flits)
Measured flits: 1344852 1344853 1344854 1344855 1344856 1344857 1344858 1344859 1344860 1344861 [...] (39792 flits)
Class 0:
Remaining flits: 1379952 1379953 1379954 1379955 1379956 1379957 1379958 1379959 1379960 1379961 [...] (121399 flits)
Measured flits: 1379952 1379953 1379954 1379955 1379956 1379957 1379958 1379959 1379960 1379961 [...] (32161 flits)
Class 0:
Remaining flits: 1433051 1434150 1434151 1434152 1434153 1434154 1434155 1434156 1434157 1434158 [...] (120489 flits)
Measured flits: 1441131 1441132 1441133 1456830 1456831 1456832 1456833 1456834 1456835 1456836 [...] (25743 flits)
Class 0:
Remaining flits: 1434163 1434164 1434165 1434166 1434167 1435698 1435699 1435700 1435701 1435702 [...] (120149 flits)
Measured flits: 1466712 1466713 1466714 1466715 1466716 1466717 1466718 1466719 1466720 1466721 [...] (21517 flits)
Class 0:
Remaining flits: 1467810 1467811 1467812 1467813 1467814 1467815 1467816 1467817 1467818 1467819 [...] (119992 flits)
Measured flits: 1498878 1498879 1498880 1498881 1498882 1498883 1498884 1498885 1498886 1498887 [...] (16538 flits)
Class 0:
Remaining flits: 1473426 1473427 1473428 1473429 1473430 1473431 1473432 1473433 1473434 1473435 [...] (120213 flits)
Measured flits: 1504008 1504009 1504010 1504011 1504012 1504013 1504014 1504015 1504016 1504017 [...] (13621 flits)
Class 0:
Remaining flits: 1488708 1488709 1488710 1488711 1488712 1488713 1488714 1488715 1488716 1488717 [...] (120240 flits)
Measured flits: 1504008 1504009 1504010 1504011 1504012 1504013 1504014 1504015 1504016 1504017 [...] (10635 flits)
Class 0:
Remaining flits: 1490382 1490383 1490384 1490385 1490386 1490387 1490388 1490389 1490390 1490391 [...] (121179 flits)
Measured flits: 1575725 1575726 1575727 1575728 1575729 1575730 1575731 1575732 1575733 1575734 [...] (7725 flits)
Class 0:
Remaining flits: 1559844 1559845 1559846 1559847 1559848 1559849 1559850 1559851 1559852 1559853 [...] (120401 flits)
Measured flits: 1618613 1619335 1619336 1619337 1619338 1619339 1619340 1619341 1619342 1619343 [...] (5374 flits)
Class 0:
Remaining flits: 1599948 1599949 1599950 1599951 1599952 1599953 1599954 1599955 1599956 1599957 [...] (121218 flits)
Measured flits: 1646478 1646479 1646480 1646481 1646482 1646483 1646484 1646485 1646486 1646487 [...] (4298 flits)
Class 0:
Remaining flits: 1601226 1601227 1601228 1601229 1601230 1601231 1601232 1601233 1601234 1601235 [...] (122324 flits)
Measured flits: 1654308 1654309 1654310 1654311 1654312 1654313 1654314 1654315 1654316 1654317 [...] (3303 flits)
Class 0:
Remaining flits: 1604484 1604485 1604486 1604487 1604488 1604489 1604490 1604491 1604492 1604493 [...] (121332 flits)
Measured flits: 1680012 1680013 1680014 1680015 1680016 1680017 1680018 1680019 1680020 1680021 [...] (2773 flits)
Class 0:
Remaining flits: 1604484 1604485 1604486 1604487 1604488 1604489 1604490 1604491 1604492 1604493 [...] (121457 flits)
Measured flits: 1681638 1681639 1681640 1681641 1681642 1681643 1681644 1681645 1681646 1681647 [...] (2294 flits)
Class 0:
Remaining flits: 1651806 1651807 1651808 1651809 1651810 1651811 1651812 1651813 1651814 1651815 [...] (121588 flits)
Measured flits: 1712160 1712161 1712162 1712163 1712164 1712165 1712166 1712167 1712168 1712169 [...] (1949 flits)
Class 0:
Remaining flits: 1652868 1652869 1652870 1652871 1652872 1652873 1652874 1652875 1652876 1652877 [...] (120331 flits)
Measured flits: 1741320 1741321 1741322 1741323 1741324 1741325 1741326 1741327 1741328 1741329 [...] (1494 flits)
Class 0:
Remaining flits: 1652868 1652869 1652870 1652871 1652872 1652873 1652874 1652875 1652876 1652877 [...] (119733 flits)
Measured flits: 1837080 1837081 1837082 1837083 1837084 1837085 1837086 1837087 1837088 1837089 [...] (1107 flits)
Class 0:
Remaining flits: 1710000 1710001 1710002 1710003 1710004 1710005 1710006 1710007 1710008 1710009 [...] (121310 flits)
Measured flits: 1840734 1840735 1840736 1840737 1840738 1840739 1840740 1840741 1840742 1840743 [...] (720 flits)
Class 0:
Remaining flits: 1817748 1817749 1817750 1817751 1817752 1817753 1817754 1817755 1817756 1817757 [...] (120879 flits)
Measured flits: 1934100 1934101 1934102 1934103 1934104 1934105 1934106 1934107 1934108 1934109 [...] (411 flits)
Class 0:
Remaining flits: 1817748 1817749 1817750 1817751 1817752 1817753 1817754 1817755 1817756 1817757 [...] (121439 flits)
Measured flits: 1936980 1936981 1936982 1936983 1936984 1936985 1936986 1936987 1936988 1936989 [...] (129 flits)
Class 0:
Remaining flits: 1817748 1817749 1817750 1817751 1817752 1817753 1817754 1817755 1817756 1817757 [...] (120489 flits)
Measured flits: 2104002 2104003 2104004 2104005 2104006 2104007 2104008 2104009 2104010 2104011 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1917036 1917037 1917038 1917039 1917040 1917041 1917042 1917043 1917044 1917045 [...] (91168 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1948662 1948663 1948664 1948665 1948666 1948667 1948668 1948669 1948670 1948671 [...] (61676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2014020 2014021 2014022 2014023 2014024 2014025 2014026 2014027 2014028 2014029 [...] (33109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2049246 2049247 2049248 2049249 2049250 2049251 2049252 2049253 2049254 2049255 [...] (8965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2238145 2238146 2238147 2238148 2238149 2238150 2238151 2238152 2238153 2238154 [...] (157 flits)
Measured flits: (0 flits)
Time taken is 74492 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 23053.6 (1 samples)
	minimum = 893 (1 samples)
	maximum = 59359 (1 samples)
Network latency average = 3864.16 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15163 (1 samples)
Flit latency average = 3813.12 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15538 (1 samples)
Fragmentation average = 71.1903 (1 samples)
	minimum = 0 (1 samples)
	maximum = 187 (1 samples)
Injected packet rate average = 0.00839955 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.0164286 (1 samples)
Accepted packet rate average = 0.0088683 (1 samples)
	minimum = 0.00514286 (1 samples)
	maximum = 0.0114286 (1 samples)
Injected flit rate average = 0.151186 (1 samples)
	minimum = 0.054 (1 samples)
	maximum = 0.295714 (1 samples)
Accepted flit rate average = 0.159693 (1 samples)
	minimum = 0.0947143 (1 samples)
	maximum = 0.207 (1 samples)
Injected packet size average = 17.9993 (1 samples)
Accepted packet size average = 18.0071 (1 samples)
Hops average = 5.05388 (1 samples)
Total run time 56.9736
