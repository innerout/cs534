BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 252.228
	minimum = 22
	maximum = 855
Network latency average = 244.473
	minimum = 22
	maximum = 838
Slowest packet = 589
Flit latency average = 211.69
	minimum = 5
	maximum = 821
Slowest flit = 10619
Fragmentation average = 48.0823
	minimum = 0
	maximum = 274
Injected packet rate average = 0.0256771
	minimum = 0.013 (at node 50)
	maximum = 0.037 (at node 43)
Accepted packet rate average = 0.0139792
	minimum = 0.006 (at node 164)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.457781
	minimum = 0.234 (at node 50)
	maximum = 0.661 (at node 173)
Accepted flit rate average= 0.262937
	minimum = 0.131 (at node 164)
	maximum = 0.444 (at node 48)
Injected packet length average = 17.8284
Accepted packet length average = 18.8092
Total in-flight flits = 38526 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 466.955
	minimum = 22
	maximum = 1572
Network latency average = 443.889
	minimum = 22
	maximum = 1560
Slowest packet = 2000
Flit latency average = 404.955
	minimum = 5
	maximum = 1543
Slowest flit = 36017
Fragmentation average = 54.7243
	minimum = 0
	maximum = 329
Injected packet rate average = 0.0221146
	minimum = 0.013 (at node 76)
	maximum = 0.028 (at node 127)
Accepted packet rate average = 0.0146146
	minimum = 0.0085 (at node 10)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.395568
	minimum = 0.234 (at node 76)
	maximum = 0.504 (at node 127)
Accepted flit rate average= 0.267982
	minimum = 0.168 (at node 10)
	maximum = 0.4 (at node 152)
Injected packet length average = 17.8872
Accepted packet length average = 18.3366
Total in-flight flits = 50959 (0 measured)
latency change    = 0.459846
throughput change = 0.0188232
Class 0:
Packet latency average = 1104.46
	minimum = 22
	maximum = 2090
Network latency average = 899.93
	minimum = 22
	maximum = 2046
Slowest packet = 2141
Flit latency average = 853.811
	minimum = 5
	maximum = 2200
Slowest flit = 71349
Fragmentation average = 60.831
	minimum = 0
	maximum = 293
Injected packet rate average = 0.0152656
	minimum = 0.005 (at node 118)
	maximum = 0.029 (at node 142)
Accepted packet rate average = 0.0147604
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 3)
Injected flit rate average = 0.274719
	minimum = 0.09 (at node 118)
	maximum = 0.516 (at node 142)
Accepted flit rate average= 0.268073
	minimum = 0.104 (at node 163)
	maximum = 0.477 (at node 159)
Injected packet length average = 17.9959
Accepted packet length average = 18.1616
Total in-flight flits = 52427 (0 measured)
latency change    = 0.577209
throughput change = 0.000340004
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1315.61
	minimum = 31
	maximum = 2522
Network latency average = 359.409
	minimum = 22
	maximum = 969
Slowest packet = 11526
Flit latency average = 943.041
	minimum = 5
	maximum = 2596
Slowest flit = 109331
Fragmentation average = 32.6193
	minimum = 0
	maximum = 181
Injected packet rate average = 0.015125
	minimum = 0 (at node 126)
	maximum = 0.026 (at node 3)
Accepted packet rate average = 0.0151406
	minimum = 0.006 (at node 36)
	maximum = 0.035 (at node 66)
Injected flit rate average = 0.271234
	minimum = 0 (at node 126)
	maximum = 0.466 (at node 3)
Accepted flit rate average= 0.27088
	minimum = 0.093 (at node 141)
	maximum = 0.597 (at node 66)
Injected packet length average = 17.9329
Accepted packet length average = 17.891
Total in-flight flits = 52492 (41814 measured)
latency change    = 0.160496
throughput change = 0.0103636
Class 0:
Packet latency average = 1811.26
	minimum = 31
	maximum = 3100
Network latency average = 777.81
	minimum = 22
	maximum = 1884
Slowest packet = 11526
Flit latency average = 963.565
	minimum = 5
	maximum = 3014
Slowest flit = 117233
Fragmentation average = 56.5834
	minimum = 0
	maximum = 259
Injected packet rate average = 0.0150703
	minimum = 0.006 (at node 126)
	maximum = 0.022 (at node 75)
Accepted packet rate average = 0.014987
	minimum = 0.009 (at node 35)
	maximum = 0.0255 (at node 66)
Injected flit rate average = 0.270695
	minimum = 0.108 (at node 126)
	maximum = 0.4015 (at node 75)
Accepted flit rate average= 0.269542
	minimum = 0.1595 (at node 141)
	maximum = 0.4425 (at node 66)
Injected packet length average = 17.9622
Accepted packet length average = 17.9851
Total in-flight flits = 52819 (52458 measured)
latency change    = 0.273648
throughput change = 0.00496599
Class 0:
Packet latency average = 2118.68
	minimum = 31
	maximum = 3825
Network latency average = 900.913
	minimum = 22
	maximum = 2726
Slowest packet = 11526
Flit latency average = 963.808
	minimum = 5
	maximum = 3099
Slowest flit = 195322
Fragmentation average = 64.8168
	minimum = 0
	maximum = 284
Injected packet rate average = 0.0149878
	minimum = 0.00933333 (at node 48)
	maximum = 0.0216667 (at node 27)
Accepted packet rate average = 0.0149392
	minimum = 0.01 (at node 141)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.269521
	minimum = 0.169 (at node 48)
	maximum = 0.384333 (at node 27)
Accepted flit rate average= 0.268972
	minimum = 0.184333 (at node 141)
	maximum = 0.381333 (at node 128)
Injected packet length average = 17.9826
Accepted packet length average = 18.0044
Total in-flight flits = 52497 (52484 measured)
latency change    = 0.1451
throughput change = 0.00211711
Class 0:
Packet latency average = 2370.43
	minimum = 31
	maximum = 4428
Network latency average = 954.862
	minimum = 22
	maximum = 3495
Slowest packet = 11526
Flit latency average = 971.654
	minimum = 5
	maximum = 3461
Slowest flit = 211463
Fragmentation average = 68.6731
	minimum = 0
	maximum = 330
Injected packet rate average = 0.0148919
	minimum = 0.0095 (at node 48)
	maximum = 0.021 (at node 27)
Accepted packet rate average = 0.0148411
	minimum = 0.0105 (at node 79)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.267813
	minimum = 0.1705 (at node 48)
	maximum = 0.3745 (at node 27)
Accepted flit rate average= 0.267319
	minimum = 0.18925 (at node 79)
	maximum = 0.359 (at node 128)
Injected packet length average = 17.9837
Accepted packet length average = 18.012
Total in-flight flits = 52614 (52614 measured)
latency change    = 0.106206
throughput change = 0.00618442
Class 0:
Packet latency average = 2610.27
	minimum = 31
	maximum = 5264
Network latency average = 978.921
	minimum = 22
	maximum = 3495
Slowest packet = 11526
Flit latency average = 972.785
	minimum = 5
	maximum = 3461
Slowest flit = 211463
Fragmentation average = 71.1271
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0148604
	minimum = 0.0088 (at node 71)
	maximum = 0.021 (at node 17)
Accepted packet rate average = 0.0148354
	minimum = 0.011 (at node 79)
	maximum = 0.0188 (at node 129)
Injected flit rate average = 0.267416
	minimum = 0.1604 (at node 71)
	maximum = 0.3756 (at node 17)
Accepted flit rate average= 0.267416
	minimum = 0.199 (at node 79)
	maximum = 0.34 (at node 129)
Injected packet length average = 17.9952
Accepted packet length average = 18.0255
Total in-flight flits = 52226 (52226 measured)
latency change    = 0.0918837
throughput change = 0.00036129
Class 0:
Packet latency average = 2841.85
	minimum = 31
	maximum = 5877
Network latency average = 995.202
	minimum = 22
	maximum = 3975
Slowest packet = 11526
Flit latency average = 974.774
	minimum = 5
	maximum = 3866
Slowest flit = 275636
Fragmentation average = 73.2095
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0148576
	minimum = 0.00983333 (at node 4)
	maximum = 0.0196667 (at node 17)
Accepted packet rate average = 0.0148299
	minimum = 0.0115 (at node 171)
	maximum = 0.0186667 (at node 138)
Injected flit rate average = 0.267283
	minimum = 0.177 (at node 4)
	maximum = 0.3525 (at node 17)
Accepted flit rate average= 0.267187
	minimum = 0.2025 (at node 171)
	maximum = 0.339833 (at node 138)
Injected packet length average = 17.9896
Accepted packet length average = 18.0168
Total in-flight flits = 52320 (52320 measured)
latency change    = 0.0814872
throughput change = 0.000857053
Class 0:
Packet latency average = 3068.34
	minimum = 31
	maximum = 6405
Network latency average = 1000.78
	minimum = 22
	maximum = 5456
Slowest packet = 11526
Flit latency average = 972.046
	minimum = 5
	maximum = 5404
Slowest flit = 255275
Fragmentation average = 74.6347
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0148452
	minimum = 0.00857143 (at node 4)
	maximum = 0.0204286 (at node 17)
Accepted packet rate average = 0.0148475
	minimum = 0.0117143 (at node 79)
	maximum = 0.0187143 (at node 70)
Injected flit rate average = 0.267127
	minimum = 0.153571 (at node 4)
	maximum = 0.366143 (at node 17)
Accepted flit rate average= 0.267365
	minimum = 0.209857 (at node 79)
	maximum = 0.333286 (at node 70)
Injected packet length average = 17.9941
Accepted packet length average = 18.0075
Total in-flight flits = 51792 (51792 measured)
latency change    = 0.0738159
throughput change = 0.000668357
Draining all recorded packets ...
Class 0:
Remaining flits: 407682 407683 407684 407685 407686 407687 407688 407689 407690 407691 [...] (51278 flits)
Measured flits: 407682 407683 407684 407685 407686 407687 407688 407689 407690 407691 [...] (51278 flits)
Class 0:
Remaining flits: 462438 462439 462440 462441 462442 462443 462444 462445 462446 462447 [...] (50846 flits)
Measured flits: 462438 462439 462440 462441 462442 462443 462444 462445 462446 462447 [...] (50846 flits)
Class 0:
Remaining flits: 480059 528138 528139 528140 528141 528142 528143 528144 528145 528146 [...] (51746 flits)
Measured flits: 480059 528138 528139 528140 528141 528142 528143 528144 528145 528146 [...] (51584 flits)
Class 0:
Remaining flits: 564624 564625 564626 564627 564628 564629 564630 564631 564632 564633 [...] (50976 flits)
Measured flits: 564624 564625 564626 564627 564628 564629 564630 564631 564632 564633 [...] (50123 flits)
Class 0:
Remaining flits: 624204 624205 624206 624207 624208 624209 624210 624211 624212 624213 [...] (50165 flits)
Measured flits: 624204 624205 624206 624207 624208 624209 624210 624211 624212 624213 [...] (44869 flits)
Class 0:
Remaining flits: 640980 640981 640982 640983 640984 640985 640986 640987 640988 640989 [...] (50181 flits)
Measured flits: 640980 640981 640982 640983 640984 640985 640986 640987 640988 640989 [...] (38091 flits)
Class 0:
Remaining flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (49401 flits)
Measured flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (27079 flits)
Class 0:
Remaining flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (50407 flits)
Measured flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (15844 flits)
Class 0:
Remaining flits: 780750 780751 780752 780753 780754 780755 780756 780757 780758 780759 [...] (49336 flits)
Measured flits: 780750 780751 780752 780753 780754 780755 780756 780757 780758 780759 [...] (8356 flits)
Class 0:
Remaining flits: 866214 866215 866216 866217 866218 866219 866220 866221 866222 866223 [...] (48759 flits)
Measured flits: 874638 874639 874640 874641 874642 874643 874644 874645 874646 874647 [...] (5122 flits)
Class 0:
Remaining flits: 874638 874639 874640 874641 874642 874643 874644 874645 874646 874647 [...] (48927 flits)
Measured flits: 874638 874639 874640 874641 874642 874643 874644 874645 874646 874647 [...] (3691 flits)
Class 0:
Remaining flits: 945548 945549 945550 945551 945552 945553 945554 945555 945556 945557 [...] (48124 flits)
Measured flits: 987480 987481 987482 987483 987484 987485 987486 987487 987488 987489 [...] (2681 flits)
Class 0:
Remaining flits: 979758 979759 979760 979761 979762 979763 979764 979765 979766 979767 [...] (47975 flits)
Measured flits: 1017063 1017064 1017065 1017066 1017067 1017068 1017069 1017070 1017071 1018818 [...] (1748 flits)
Class 0:
Remaining flits: 1014239 1014240 1014241 1014242 1014243 1014244 1014245 1018830 1018831 1018832 [...] (48497 flits)
Measured flits: 1018830 1018831 1018832 1018833 1018834 1018835 1082353 1082354 1082355 1082356 [...] (754 flits)
Class 0:
Remaining flits: 1043691 1043692 1043693 1045476 1045477 1045478 1045479 1045480 1045481 1045482 [...] (48347 flits)
Measured flits: 1151460 1151461 1151462 1151463 1151464 1151465 1151466 1151467 1151468 1151469 [...] (207 flits)
Class 0:
Remaining flits: 1075502 1075503 1075504 1075505 1075506 1075507 1075508 1075509 1075510 1075511 [...] (48407 flits)
Measured flits: 1151474 1151475 1151476 1151477 1268388 1268389 1268390 1268391 1268392 1268393 [...] (22 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (8510 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1161846 1161847 1161848 1161849 1161850 1161851 1161852 1161853 1161854 1161855 [...] (150 flits)
Measured flits: (0 flits)
Time taken is 28401 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5138.37 (1 samples)
	minimum = 31 (1 samples)
	maximum = 16664 (1 samples)
Network latency average = 1025.15 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5476 (1 samples)
Flit latency average = 957.599 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6641 (1 samples)
Fragmentation average = 78.8374 (1 samples)
	minimum = 0 (1 samples)
	maximum = 356 (1 samples)
Injected packet rate average = 0.0148452 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0204286 (1 samples)
Accepted packet rate average = 0.0148475 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.267127 (1 samples)
	minimum = 0.153571 (1 samples)
	maximum = 0.366143 (1 samples)
Accepted flit rate average = 0.267365 (1 samples)
	minimum = 0.209857 (1 samples)
	maximum = 0.333286 (1 samples)
Injected packet size average = 17.9941 (1 samples)
Accepted packet size average = 18.0075 (1 samples)
Hops average = 5.06437 (1 samples)
Total run time 39.2889
