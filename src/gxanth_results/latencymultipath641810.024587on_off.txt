BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 355.9
	minimum = 27
	maximum = 986
Network latency average = 277.67
	minimum = 27
	maximum = 872
Slowest packet = 144
Flit latency average = 190.952
	minimum = 6
	maximum = 958
Slowest flit = 3019
Fragmentation average = 185.233
	minimum = 0
	maximum = 718
Injected packet rate average = 0.0194687
	minimum = 0 (at node 26)
	maximum = 0.052 (at node 190)
Accepted packet rate average = 0.00884375
	minimum = 0.002 (at node 41)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.346547
	minimum = 0 (at node 26)
	maximum = 0.919 (at node 190)
Accepted flit rate average= 0.192781
	minimum = 0.062 (at node 174)
	maximum = 0.333 (at node 44)
Injected packet length average = 17.8002
Accepted packet length average = 21.7986
Total in-flight flits = 30306 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 573.592
	minimum = 27
	maximum = 1978
Network latency average = 448.711
	minimum = 25
	maximum = 1877
Slowest packet = 144
Flit latency average = 333.829
	minimum = 6
	maximum = 1922
Slowest flit = 5113
Fragmentation average = 244.476
	minimum = 0
	maximum = 1640
Injected packet rate average = 0.0179297
	minimum = 0 (at node 116)
	maximum = 0.0395 (at node 177)
Accepted packet rate average = 0.0104609
	minimum = 0.0055 (at node 164)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.32024
	minimum = 0 (at node 116)
	maximum = 0.7055 (at node 177)
Accepted flit rate average= 0.206786
	minimum = 0.11 (at node 164)
	maximum = 0.35 (at node 152)
Injected packet length average = 17.8609
Accepted packet length average = 19.7675
Total in-flight flits = 44614 (0 measured)
latency change    = 0.379525
throughput change = 0.0677279
Class 0:
Packet latency average = 1067.53
	minimum = 33
	maximum = 2894
Network latency average = 782.51
	minimum = 27
	maximum = 2778
Slowest packet = 308
Flit latency average = 661.669
	minimum = 6
	maximum = 2838
Slowest flit = 10234
Fragmentation average = 293.166
	minimum = 0
	maximum = 2452
Injected packet rate average = 0.0128333
	minimum = 0 (at node 1)
	maximum = 0.038 (at node 93)
Accepted packet rate average = 0.0122708
	minimum = 0.003 (at node 40)
	maximum = 0.024 (at node 159)
Injected flit rate average = 0.229469
	minimum = 0 (at node 1)
	maximum = 0.685 (at node 139)
Accepted flit rate average= 0.220615
	minimum = 0.067 (at node 163)
	maximum = 0.432 (at node 159)
Injected packet length average = 17.8807
Accepted packet length average = 17.9788
Total in-flight flits = 46644 (0 measured)
latency change    = 0.462694
throughput change = 0.06268
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1050.54
	minimum = 28
	maximum = 3349
Network latency average = 328.568
	minimum = 26
	maximum = 948
Slowest packet = 9373
Flit latency average = 834.944
	minimum = 6
	maximum = 3911
Slowest flit = 6280
Fragmentation average = 160.656
	minimum = 1
	maximum = 762
Injected packet rate average = 0.0119635
	minimum = 0 (at node 1)
	maximum = 0.045 (at node 87)
Accepted packet rate average = 0.0121198
	minimum = 0.004 (at node 1)
	maximum = 0.024 (at node 182)
Injected flit rate average = 0.214646
	minimum = 0 (at node 4)
	maximum = 0.799 (at node 87)
Accepted flit rate average= 0.216229
	minimum = 0.089 (at node 163)
	maximum = 0.412 (at node 182)
Injected packet length average = 17.9417
Accepted packet length average = 17.841
Total in-flight flits = 46582 (24082 measured)
latency change    = 0.0161725
throughput change = 0.0202813
Class 0:
Packet latency average = 1479.98
	minimum = 28
	maximum = 4898
Network latency average = 503.537
	minimum = 26
	maximum = 1943
Slowest packet = 9373
Flit latency average = 872.353
	minimum = 6
	maximum = 4765
Slowest flit = 5091
Fragmentation average = 191.599
	minimum = 0
	maximum = 1502
Injected packet rate average = 0.0120391
	minimum = 0 (at node 1)
	maximum = 0.0335 (at node 127)
Accepted packet rate average = 0.0119818
	minimum = 0.007 (at node 31)
	maximum = 0.0205 (at node 182)
Injected flit rate average = 0.216404
	minimum = 0 (at node 4)
	maximum = 0.6015 (at node 127)
Accepted flit rate average= 0.215398
	minimum = 0.1145 (at node 31)
	maximum = 0.356 (at node 182)
Injected packet length average = 17.9751
Accepted packet length average = 17.9772
Total in-flight flits = 47289 (34051 measured)
latency change    = 0.290163
throughput change = 0.00385671
Class 0:
Packet latency average = 1816
	minimum = 28
	maximum = 5834
Network latency average = 627.344
	minimum = 25
	maximum = 2882
Slowest packet = 9373
Flit latency average = 897.752
	minimum = 6
	maximum = 5697
Slowest flit = 19007
Fragmentation average = 210.05
	minimum = 0
	maximum = 2116
Injected packet rate average = 0.0120694
	minimum = 0 (at node 4)
	maximum = 0.0266667 (at node 75)
Accepted packet rate average = 0.0119358
	minimum = 0.008 (at node 55)
	maximum = 0.017 (at node 129)
Injected flit rate average = 0.217224
	minimum = 0 (at node 4)
	maximum = 0.475 (at node 75)
Accepted flit rate average= 0.214642
	minimum = 0.131667 (at node 135)
	maximum = 0.304333 (at node 138)
Injected packet length average = 17.9978
Accepted packet length average = 17.9831
Total in-flight flits = 48326 (39434 measured)
latency change    = 0.185032
throughput change = 0.00352249
Class 0:
Packet latency average = 2086.74
	minimum = 28
	maximum = 6731
Network latency average = 713.389
	minimum = 23
	maximum = 3938
Slowest packet = 9373
Flit latency average = 918.004
	minimum = 6
	maximum = 6734
Slowest flit = 10439
Fragmentation average = 223.916
	minimum = 0
	maximum = 2357
Injected packet rate average = 0.0121107
	minimum = 0 (at node 18)
	maximum = 0.02475 (at node 191)
Accepted packet rate average = 0.0118763
	minimum = 0.00775 (at node 2)
	maximum = 0.01625 (at node 128)
Injected flit rate average = 0.217805
	minimum = 0 (at node 18)
	maximum = 0.442 (at node 191)
Accepted flit rate average= 0.213055
	minimum = 0.14075 (at node 135)
	maximum = 0.296 (at node 128)
Injected packet length average = 17.9845
Accepted packet length average = 17.9395
Total in-flight flits = 50526 (44004 measured)
latency change    = 0.129746
throughput change = 0.00745195
Class 0:
Packet latency average = 2380.36
	minimum = 28
	maximum = 7094
Network latency average = 781.989
	minimum = 23
	maximum = 4731
Slowest packet = 9373
Flit latency average = 944.599
	minimum = 6
	maximum = 7736
Slowest flit = 15551
Fragmentation average = 233.974
	minimum = 0
	maximum = 4089
Injected packet rate average = 0.0122229
	minimum = 0 (at node 18)
	maximum = 0.0234 (at node 145)
Accepted packet rate average = 0.0118625
	minimum = 0.008 (at node 2)
	maximum = 0.0168 (at node 128)
Injected flit rate average = 0.219876
	minimum = 0 (at node 18)
	maximum = 0.4232 (at node 145)
Accepted flit rate average= 0.213277
	minimum = 0.1456 (at node 2)
	maximum = 0.296 (at node 128)
Injected packet length average = 17.9888
Accepted packet length average = 17.9791
Total in-flight flits = 53218 (48435 measured)
latency change    = 0.12335
throughput change = 0.00104276
Class 0:
Packet latency average = 2655.4
	minimum = 28
	maximum = 8737
Network latency average = 851.009
	minimum = 23
	maximum = 5900
Slowest packet = 9373
Flit latency average = 982.447
	minimum = 6
	maximum = 8811
Slowest flit = 8719
Fragmentation average = 243.125
	minimum = 0
	maximum = 4278
Injected packet rate average = 0.0119635
	minimum = 0 (at node 18)
	maximum = 0.0236667 (at node 34)
Accepted packet rate average = 0.011829
	minimum = 0.00883333 (at node 2)
	maximum = 0.0161667 (at node 128)
Injected flit rate average = 0.215138
	minimum = 0 (at node 18)
	maximum = 0.427333 (at node 34)
Accepted flit rate average= 0.21201
	minimum = 0.156667 (at node 42)
	maximum = 0.2915 (at node 128)
Injected packet length average = 17.9828
Accepted packet length average = 17.9229
Total in-flight flits = 50718 (47311 measured)
latency change    = 0.103578
throughput change = 0.00597455
Class 0:
Packet latency average = 2902.69
	minimum = 28
	maximum = 9073
Network latency average = 911.769
	minimum = 23
	maximum = 6785
Slowest packet = 9373
Flit latency average = 1014.25
	minimum = 6
	maximum = 9366
Slowest flit = 41982
Fragmentation average = 246.658
	minimum = 0
	maximum = 4292
Injected packet rate average = 0.0118601
	minimum = 0 (at node 18)
	maximum = 0.0231429 (at node 109)
Accepted packet rate average = 0.0117656
	minimum = 0.009 (at node 42)
	maximum = 0.0154286 (at node 128)
Injected flit rate average = 0.213275
	minimum = 0 (at node 18)
	maximum = 0.417143 (at node 109)
Accepted flit rate average= 0.211252
	minimum = 0.150857 (at node 42)
	maximum = 0.275143 (at node 128)
Injected packet length average = 17.9826
Accepted packet length average = 17.955
Total in-flight flits = 49767 (47318 measured)
latency change    = 0.0851908
throughput change = 0.003589
Draining all recorded packets ...
Class 0:
Remaining flits: 14330 14331 14332 14333 14334 14335 14336 14337 14338 14339 [...] (48631 flits)
Measured flits: 169254 169255 169256 169257 169258 169259 169260 169261 169262 169263 [...] (45368 flits)
Class 0:
Remaining flits: 14330 14331 14332 14333 14334 14335 14336 14337 14338 14339 [...] (50347 flits)
Measured flits: 171966 171967 171968 171969 171970 171971 172314 172315 172316 172317 [...] (46100 flits)
Class 0:
Remaining flits: 14330 14331 14332 14333 14334 14335 14336 14337 14338 14339 [...] (54037 flits)
Measured flits: 174895 174896 174897 174898 174899 174900 174901 174902 174903 174904 [...] (47130 flits)
Class 0:
Remaining flits: 14330 14331 14332 14333 14334 14335 14336 14337 14338 14339 [...] (56359 flits)
Measured flits: 174900 174901 174902 174903 174904 174905 179622 179623 179624 179625 [...] (46608 flits)
Class 0:
Remaining flits: 14330 14331 14332 14333 14334 14335 14336 14337 14338 14339 [...] (58340 flits)
Measured flits: 179622 179623 179624 179625 179626 179627 179628 179629 179630 179631 [...] (45490 flits)
Class 0:
Remaining flits: 14334 14335 14336 14337 14338 14339 14340 14341 14342 14343 [...] (59901 flits)
Measured flits: 179622 179623 179624 179625 179626 179627 179628 179629 179630 179631 [...] (44270 flits)
Class 0:
Remaining flits: 14338 14339 14340 14341 14342 14343 14344 14345 17352 17353 [...] (57665 flits)
Measured flits: 179622 179623 179624 179625 179626 179627 179628 179629 179630 179631 [...] (41420 flits)
Class 0:
Remaining flits: 14345 17358 17359 17360 17361 17362 17363 17364 17365 17366 [...] (59727 flits)
Measured flits: 179631 179632 179633 179634 179635 179636 179637 179638 179639 190831 [...] (37033 flits)
Class 0:
Remaining flits: 17360 17361 17362 17363 17364 17365 17366 17367 17368 17369 [...] (61528 flits)
Measured flits: 190831 190832 190833 190834 190835 192334 192335 192336 192337 192338 [...] (35428 flits)
Class 0:
Remaining flits: 17365 17366 17367 17368 17369 20971 20972 20973 20974 20975 [...] (62384 flits)
Measured flits: 192334 192335 192336 192337 192338 192339 192340 192341 192342 192343 [...] (32715 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (63552 flits)
Measured flits: 192334 192335 192336 192337 192338 192339 192340 192341 192342 192343 [...] (29698 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (62442 flits)
Measured flits: 300996 300997 300998 300999 301000 301001 301002 301003 301004 301005 [...] (25156 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (61804 flits)
Measured flits: 388656 388657 388658 388659 388660 388661 388662 388663 388664 388665 [...] (20977 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (63121 flits)
Measured flits: 426403 426404 426405 426406 426407 426408 426409 426410 426411 426412 [...] (19133 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (62848 flits)
Measured flits: 427986 427987 427988 427989 427990 427991 427992 427993 427994 427995 [...] (16368 flits)
Class 0:
Remaining flits: 20971 20972 20973 20974 20975 20976 20977 20978 20979 20980 [...] (62563 flits)
Measured flits: 427986 427987 427988 427989 427990 427991 427992 427993 427994 427995 [...] (14229 flits)
Class 0:
Remaining flits: 20983 20984 20985 20986 20987 78102 78103 78104 78105 78106 [...] (62875 flits)
Measured flits: 427986 427987 427988 427989 427990 427991 427992 427993 427994 427995 [...] (12587 flits)
Class 0:
Remaining flits: 427986 427987 427988 427989 427990 427991 427992 427993 427994 427995 [...] (62366 flits)
Measured flits: 427986 427987 427988 427989 427990 427991 427992 427993 427994 427995 [...] (12102 flits)
Class 0:
Remaining flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (62530 flits)
Measured flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (11055 flits)
Class 0:
Remaining flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (61180 flits)
Measured flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (9596 flits)
Class 0:
Remaining flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (60812 flits)
Measured flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (7978 flits)
Class 0:
Remaining flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (60716 flits)
Measured flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (7744 flits)
Class 0:
Remaining flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (58834 flits)
Measured flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (7420 flits)
Class 0:
Remaining flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (61694 flits)
Measured flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (7127 flits)
Class 0:
Remaining flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (63198 flits)
Measured flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (6423 flits)
Class 0:
Remaining flits: 537926 537927 537928 537929 539568 539569 539570 539571 539572 539573 [...] (60707 flits)
Measured flits: 537926 537927 537928 537929 539568 539569 539570 539571 539572 539573 [...] (5110 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (61822 flits)
Measured flits: 664236 664237 664238 664239 664240 664241 664242 664243 664244 664245 [...] (4772 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (64110 flits)
Measured flits: 664237 664238 664239 664240 664241 664242 664243 664244 664245 664246 [...] (3966 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (65676 flits)
Measured flits: 664237 664238 664239 664240 664241 664242 664243 664244 664245 664246 [...] (3605 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (62814 flits)
Measured flits: 664237 664238 664239 664240 664241 664242 664243 664244 664245 664246 [...] (3153 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (63437 flits)
Measured flits: 664237 664238 664239 664240 664241 664242 664243 664244 664245 664246 [...] (2818 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (63124 flits)
Measured flits: 664251 664252 664253 771336 771337 771338 771339 771340 771341 771342 [...] (2556 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (64142 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (2228 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (64061 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1904 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (65012 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1764 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (65527 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1735 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (65700 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1857 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (61644 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1938 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (61141 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1872 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (62101 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1554 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (62265 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1533 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (61758 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1166 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (62566 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (1124 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (57851 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (965 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (57381 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (758 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (58621 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (623 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (59855 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (543 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (60167 flits)
Measured flits: 771336 771337 771338 771339 771340 771341 771342 771343 771344 771345 [...] (577 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (61741 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (586 flits)
Class 0:
Remaining flits: 599886 599887 599888 599889 599890 599891 599892 599893 599894 599895 [...] (59838 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (556 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (60878 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (686 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (63638 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (710 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (63330 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (569 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (62818 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (478 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (62983 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (338 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (60383 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (270 flits)
Class 0:
Remaining flits: 1099098 1099099 1099100 1099101 1099102 1099103 1099104 1099105 1099106 1099107 [...] (61724 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (270 flits)
Class 0:
Remaining flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (59276 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (234 flits)
Class 0:
Remaining flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (60313 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (234 flits)
Class 0:
Remaining flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (62423 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (216 flits)
Class 0:
Remaining flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (61975 flits)
Measured flits: 1111356 1111357 1111358 1111359 1111360 1111361 1111362 1111363 1111364 1111365 [...] (215 flits)
Class 0:
Remaining flits: 1111369 1111370 1111371 1111372 1111373 1304316 1304317 1304318 1304319 1304320 [...] (60226 flits)
Measured flits: 1111369 1111370 1111371 1111372 1111373 2069046 2069047 2069048 2069049 2069050 [...] (184 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (60781 flits)
Measured flits: 2069046 2069047 2069048 2069049 2069050 2069051 2069052 2069053 2069054 2069055 [...] (144 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (61704 flits)
Measured flits: 2069052 2069053 2069054 2069055 2069056 2069057 2069058 2069059 2069060 2069061 [...] (138 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (60874 flits)
Measured flits: 2106342 2106343 2106344 2106345 2106346 2106347 2106348 2106349 2106350 2106351 [...] (108 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (59857 flits)
Measured flits: 2106342 2106343 2106344 2106345 2106346 2106347 2106348 2106349 2106350 2106351 [...] (72 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (60356 flits)
Measured flits: 2106343 2106344 2106345 2106346 2106347 2106348 2106349 2106350 2106351 2106352 [...] (71 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (59616 flits)
Measured flits: 2106349 2106350 2106351 2106352 2106353 2106354 2106355 2106356 2106357 2106358 [...] (47 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (62417 flits)
Measured flits: 2106351 2106352 2106353 2106354 2106355 2106356 2106357 2106358 2106359 2240478 [...] (73 flits)
Class 0:
Remaining flits: 1304316 1304317 1304318 1304319 1304320 1304321 1304322 1304323 1304324 1304325 [...] (60295 flits)
Measured flits: 2106351 2106352 2106353 2106354 2106355 2106356 2106357 2106358 2106359 (9 flits)
Class 0:
Remaining flits: 1544760 1544761 1544762 1544763 1544764 1544765 1544766 1544767 1544768 1544769 [...] (60665 flits)
Measured flits: 2106352 2106353 2106354 2106355 2106356 2106357 2106358 2106359 (8 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1544768 1544769 1544770 1544771 1544772 1544773 1544774 1544775 1544776 1544777 [...] (36991 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1870290 1870291 1870292 1870293 1870294 1870295 1870296 1870297 1870298 1870299 [...] (25041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1870290 1870291 1870292 1870293 1870294 1870295 1870296 1870297 1870298 1870299 [...] (14883 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1870290 1870291 1870292 1870293 1870294 1870295 1870296 1870297 1870298 1870299 [...] (6208 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1870290 1870291 1870292 1870293 1870294 1870295 1870296 1870297 1870298 1870299 [...] (1626 flits)
Measured flits: (0 flits)
Time taken is 86904 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9392.9 (1 samples)
	minimum = 28 (1 samples)
	maximum = 73001 (1 samples)
Network latency average = 1529.98 (1 samples)
	minimum = 23 (1 samples)
	maximum = 44973 (1 samples)
Flit latency average = 1782.95 (1 samples)
	minimum = 6 (1 samples)
	maximum = 47482 (1 samples)
Fragmentation average = 285.053 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18034 (1 samples)
Injected packet rate average = 0.0118601 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0231429 (1 samples)
Accepted packet rate average = 0.0117656 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.213275 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.417143 (1 samples)
Accepted flit rate average = 0.211252 (1 samples)
	minimum = 0.150857 (1 samples)
	maximum = 0.275143 (1 samples)
Injected packet size average = 17.9826 (1 samples)
Accepted packet size average = 17.955 (1 samples)
Hops average = 5.05174 (1 samples)
Total run time 131.11
