BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 252.912
	minimum = 22
	maximum = 767
Network latency average = 246.297
	minimum = 22
	maximum = 741
Slowest packet = 1135
Flit latency average = 222.86
	minimum = 5
	maximum = 724
Slowest flit = 20447
Fragmentation average = 21.2238
	minimum = 0
	maximum = 88
Injected packet rate average = 0.0267917
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0140313
	minimum = 0.006 (at node 64)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.478505
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.258156
	minimum = 0.116 (at node 64)
	maximum = 0.432 (at node 131)
Injected packet length average = 17.8602
Accepted packet length average = 18.3987
Total in-flight flits = 43044 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 465.756
	minimum = 22
	maximum = 1385
Network latency average = 458.3
	minimum = 22
	maximum = 1385
Slowest packet = 2502
Flit latency average = 433.359
	minimum = 5
	maximum = 1368
Slowest flit = 45053
Fragmentation average = 22.0799
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0265156
	minimum = 0.015 (at node 190)
	maximum = 0.0355 (at node 10)
Accepted packet rate average = 0.0146276
	minimum = 0.008 (at node 116)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.475362
	minimum = 0.269 (at node 190)
	maximum = 0.639 (at node 70)
Accepted flit rate average= 0.265682
	minimum = 0.144 (at node 116)
	maximum = 0.3785 (at node 177)
Injected packet length average = 17.9276
Accepted packet length average = 18.1631
Total in-flight flits = 81362 (0 measured)
latency change    = 0.456986
throughput change = 0.0283272
Class 0:
Packet latency average = 1076.42
	minimum = 22
	maximum = 2033
Network latency average = 1066.27
	minimum = 22
	maximum = 2006
Slowest packet = 5022
Flit latency average = 1046.27
	minimum = 5
	maximum = 1989
Slowest flit = 90413
Fragmentation average = 20.6649
	minimum = 0
	maximum = 169
Injected packet rate average = 0.0241875
	minimum = 0.007 (at node 132)
	maximum = 0.04 (at node 53)
Accepted packet rate average = 0.0150781
	minimum = 0.007 (at node 132)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.435865
	minimum = 0.126 (at node 132)
	maximum = 0.72 (at node 53)
Accepted flit rate average= 0.271516
	minimum = 0.126 (at node 132)
	maximum = 0.547 (at node 16)
Injected packet length average = 18.0202
Accepted packet length average = 18.0073
Total in-flight flits = 113327 (0 measured)
latency change    = 0.56731
throughput change = 0.0214843
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 355.272
	minimum = 22
	maximum = 1740
Network latency average = 150.738
	minimum = 22
	maximum = 891
Slowest packet = 14915
Flit latency average = 1470.42
	minimum = 5
	maximum = 2495
Slowest flit = 114206
Fragmentation average = 5.90052
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0211094
	minimum = 0.002 (at node 8)
	maximum = 0.038 (at node 46)
Accepted packet rate average = 0.014901
	minimum = 0.006 (at node 113)
	maximum = 0.024 (at node 34)
Injected flit rate average = 0.37976
	minimum = 0.039 (at node 8)
	maximum = 0.698 (at node 46)
Accepted flit rate average= 0.268964
	minimum = 0.108 (at node 113)
	maximum = 0.459 (at node 66)
Injected packet length average = 17.9901
Accepted packet length average = 18.05
Total in-flight flits = 135432 (70221 measured)
latency change    = 2.02985
throughput change = 0.00948858
Class 0:
Packet latency average = 1066.04
	minimum = 22
	maximum = 2618
Network latency average = 856.091
	minimum = 22
	maximum = 1983
Slowest packet = 14915
Flit latency average = 1654.69
	minimum = 5
	maximum = 3222
Slowest flit = 161748
Fragmentation average = 12.4715
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0196094
	minimum = 0.005 (at node 12)
	maximum = 0.035 (at node 46)
Accepted packet rate average = 0.0148906
	minimum = 0.009 (at node 36)
	maximum = 0.0225 (at node 66)
Injected flit rate average = 0.353292
	minimum = 0.096 (at node 12)
	maximum = 0.631 (at node 46)
Accepted flit rate average= 0.268091
	minimum = 0.164 (at node 36)
	maximum = 0.405 (at node 66)
Injected packet length average = 18.0165
Accepted packet length average = 18.004
Total in-flight flits = 147108 (123126 measured)
latency change    = 0.666736
throughput change = 0.0032541
Class 0:
Packet latency average = 1831.98
	minimum = 22
	maximum = 3883
Network latency average = 1570.91
	minimum = 22
	maximum = 2993
Slowest packet = 14915
Flit latency average = 1824.89
	minimum = 5
	maximum = 3848
Slowest flit = 195732
Fragmentation average = 16.9365
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0183351
	minimum = 0.007 (at node 17)
	maximum = 0.0313333 (at node 46)
Accepted packet rate average = 0.0148385
	minimum = 0.01 (at node 79)
	maximum = 0.0203333 (at node 66)
Injected flit rate average = 0.330026
	minimum = 0.126 (at node 17)
	maximum = 0.563333 (at node 46)
Accepted flit rate average= 0.26709
	minimum = 0.18 (at node 79)
	maximum = 0.366 (at node 66)
Injected packet length average = 17.9997
Accepted packet length average = 17.9998
Total in-flight flits = 151147 (145358 measured)
latency change    = 0.418095
throughput change = 0.0037473
Class 0:
Packet latency average = 2285.92
	minimum = 22
	maximum = 4335
Network latency average = 1960.13
	minimum = 22
	maximum = 3941
Slowest packet = 14915
Flit latency average = 1981.44
	minimum = 5
	maximum = 4660
Slowest flit = 212098
Fragmentation average = 17.9884
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0175599
	minimum = 0.00825 (at node 8)
	maximum = 0.02725 (at node 46)
Accepted packet rate average = 0.0148503
	minimum = 0.01075 (at node 2)
	maximum = 0.0195 (at node 95)
Injected flit rate average = 0.316241
	minimum = 0.1485 (at node 21)
	maximum = 0.494 (at node 46)
Accepted flit rate average= 0.267374
	minimum = 0.188 (at node 2)
	maximum = 0.35475 (at node 95)
Injected packet length average = 18.0093
Accepted packet length average = 18.0046
Total in-flight flits = 152388 (151727 measured)
latency change    = 0.198581
throughput change = 0.00106002
Class 0:
Packet latency average = 2637.04
	minimum = 22
	maximum = 5085
Network latency average = 2213.21
	minimum = 22
	maximum = 4939
Slowest packet = 14915
Flit latency average = 2118.97
	minimum = 5
	maximum = 5294
Slowest flit = 242098
Fragmentation average = 18.7064
	minimum = 0
	maximum = 184
Injected packet rate average = 0.0169344
	minimum = 0.0094 (at node 21)
	maximum = 0.0248 (at node 147)
Accepted packet rate average = 0.0148344
	minimum = 0.0106 (at node 79)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.304702
	minimum = 0.1668 (at node 21)
	maximum = 0.4464 (at node 147)
Accepted flit rate average= 0.267078
	minimum = 0.1908 (at node 79)
	maximum = 0.342 (at node 103)
Injected packet length average = 17.9931
Accepted packet length average = 18.004
Total in-flight flits = 150944 (150867 measured)
latency change    = 0.133149
throughput change = 0.00110669
Class 0:
Packet latency average = 2930.03
	minimum = 22
	maximum = 5658
Network latency average = 2393.33
	minimum = 22
	maximum = 5387
Slowest packet = 14915
Flit latency average = 2244.08
	minimum = 5
	maximum = 5502
Slowest flit = 243701
Fragmentation average = 19.0147
	minimum = 0
	maximum = 184
Injected packet rate average = 0.0166293
	minimum = 0.0095 (at node 8)
	maximum = 0.0238333 (at node 93)
Accepted packet rate average = 0.0148351
	minimum = 0.0115 (at node 42)
	maximum = 0.0186667 (at node 157)
Injected flit rate average = 0.299312
	minimum = 0.1715 (at node 8)
	maximum = 0.427167 (at node 93)
Accepted flit rate average= 0.267004
	minimum = 0.2085 (at node 42)
	maximum = 0.334667 (at node 157)
Injected packet length average = 17.999
Accepted packet length average = 17.9982
Total in-flight flits = 152040 (152040 measured)
latency change    = 0.0999967
throughput change = 0.000276343
Class 0:
Packet latency average = 3186.22
	minimum = 22
	maximum = 6297
Network latency average = 2506.77
	minimum = 22
	maximum = 5862
Slowest packet = 14915
Flit latency average = 2338.39
	minimum = 5
	maximum = 5845
Slowest flit = 338165
Fragmentation average = 19.1812
	minimum = 0
	maximum = 206
Injected packet rate average = 0.0164167
	minimum = 0.00928571 (at node 21)
	maximum = 0.0237143 (at node 93)
Accepted packet rate average = 0.0148497
	minimum = 0.0124286 (at node 64)
	maximum = 0.0185714 (at node 138)
Injected flit rate average = 0.295446
	minimum = 0.167143 (at node 21)
	maximum = 0.425429 (at node 93)
Accepted flit rate average= 0.267373
	minimum = 0.219714 (at node 63)
	maximum = 0.334286 (at node 138)
Injected packet length average = 17.9967
Accepted packet length average = 18.0053
Total in-flight flits = 152534 (152534 measured)
latency change    = 0.0804036
throughput change = 0.00137795
Draining all recorded packets ...
Class 0:
Remaining flits: 406998 406999 407000 407001 407002 407003 407004 407005 407006 407007 [...] (152865 flits)
Measured flits: 406998 406999 407000 407001 407002 407003 407004 407005 407006 407007 [...] (152757 flits)
Class 0:
Remaining flits: 455994 455995 455996 455997 455998 455999 456000 456001 456002 456003 [...] (151114 flits)
Measured flits: 455994 455995 455996 455997 455998 455999 456000 456001 456002 456003 [...] (149926 flits)
Class 0:
Remaining flits: 514440 514441 514442 514443 514444 514445 514446 514447 514448 514449 [...] (153682 flits)
Measured flits: 514440 514441 514442 514443 514444 514445 514446 514447 514448 514449 [...] (147652 flits)
Class 0:
Remaining flits: 567108 567109 567110 567111 567112 567113 567114 567115 567116 567117 [...] (150844 flits)
Measured flits: 567108 567109 567110 567111 567112 567113 567114 567115 567116 567117 [...] (131458 flits)
Class 0:
Remaining flits: 600534 600535 600536 600537 600538 600539 600540 600541 600542 600543 [...] (150951 flits)
Measured flits: 600534 600535 600536 600537 600538 600539 600540 600541 600542 600543 [...] (110093 flits)
Class 0:
Remaining flits: 630756 630757 630758 630759 630760 630761 630762 630763 630764 630765 [...] (149668 flits)
Measured flits: 630756 630757 630758 630759 630760 630761 630762 630763 630764 630765 [...] (84685 flits)
Class 0:
Remaining flits: 686628 686629 686630 686631 686632 686633 686634 686635 686636 686637 [...] (147462 flits)
Measured flits: 686628 686629 686630 686631 686632 686633 686634 686635 686636 686637 [...] (61909 flits)
Class 0:
Remaining flits: 765414 765415 765416 765417 765418 765419 765420 765421 765422 765423 [...] (145377 flits)
Measured flits: 765414 765415 765416 765417 765418 765419 765420 765421 765422 765423 [...] (40121 flits)
Class 0:
Remaining flits: 779292 779293 779294 779295 779296 779297 779298 779299 779300 779301 [...] (146035 flits)
Measured flits: 779292 779293 779294 779295 779296 779297 779298 779299 779300 779301 [...] (23356 flits)
Class 0:
Remaining flits: 837288 837289 837290 837291 837292 837293 837294 837295 837296 837297 [...] (144702 flits)
Measured flits: 837288 837289 837290 837291 837292 837293 837294 837295 837296 837297 [...] (11645 flits)
Class 0:
Remaining flits: 844704 844705 844706 844707 844708 844709 844710 844711 844712 844713 [...] (144874 flits)
Measured flits: 844704 844705 844706 844707 844708 844709 844710 844711 844712 844713 [...] (5493 flits)
Class 0:
Remaining flits: 907110 907111 907112 907113 907114 907115 907116 907117 907118 907119 [...] (146883 flits)
Measured flits: 907110 907111 907112 907113 907114 907115 907116 907117 907118 907119 [...] (2007 flits)
Class 0:
Remaining flits: 960552 960553 960554 960555 960556 960557 960558 960559 960560 960561 [...] (144224 flits)
Measured flits: 1007967 1007968 1007969 1007970 1007971 1007972 1007973 1007974 1007975 1007976 [...] (708 flits)
Class 0:
Remaining flits: 989640 989641 989642 989643 989644 989645 989646 989647 989648 989649 [...] (143422 flits)
Measured flits: 1092816 1092817 1092818 1092819 1092820 1092821 1092822 1092823 1092824 1092825 [...] (234 flits)
Class 0:
Remaining flits: 1080162 1080163 1080164 1080165 1080166 1080167 1080168 1080169 1080170 1080171 [...] (143612 flits)
Measured flits: 1147716 1147717 1147718 1147719 1147720 1147721 1147722 1147723 1147724 1147725 [...] (72 flits)
Class 0:
Remaining flits: 1115982 1115983 1115984 1115985 1115986 1115987 1115988 1115989 1115990 1115991 [...] (146103 flits)
Measured flits: 1171530 1171531 1171532 1171533 1171534 1171535 1171536 1171537 1171538 1171539 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1175976 1175977 1175978 1175979 1175980 1175981 1175982 1175983 1175984 1175985 [...] (95403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1283886 1283887 1283888 1283889 1283890 1283891 1283892 1283893 1283894 1283895 [...] (46869 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1330812 1330813 1330814 1330815 1330816 1330817 1330818 1330819 1330820 1330821 [...] (7348 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494277 1494278 1494279 1494280 1494281 1494282 1494283 1494284 1494285 1494286 [...] (11 flits)
Measured flits: (0 flits)
Time taken is 30653 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5332.78 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16646 (1 samples)
Network latency average = 2834.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7950 (1 samples)
Flit latency average = 2731.87 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7923 (1 samples)
Fragmentation average = 20.101 (1 samples)
	minimum = 0 (1 samples)
	maximum = 206 (1 samples)
Injected packet rate average = 0.0164167 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0237143 (1 samples)
Accepted packet rate average = 0.0148497 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.295446 (1 samples)
	minimum = 0.167143 (1 samples)
	maximum = 0.425429 (1 samples)
Accepted flit rate average = 0.267373 (1 samples)
	minimum = 0.219714 (1 samples)
	maximum = 0.334286 (1 samples)
Injected packet size average = 17.9967 (1 samples)
Accepted packet size average = 18.0053 (1 samples)
Hops average = 5.06116 (1 samples)
Total run time 28.8287
