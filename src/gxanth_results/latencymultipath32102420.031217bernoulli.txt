BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 278.677
	minimum = 22
	maximum = 752
Network latency average = 269.845
	minimum = 22
	maximum = 728
Slowest packet = 811
Flit latency average = 237.255
	minimum = 5
	maximum = 758
Slowest flit = 22602
Fragmentation average = 54.1276
	minimum = 0
	maximum = 350
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.01425
	minimum = 0.006 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.269766
	minimum = 0.12 (at node 174)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8483
Accepted packet length average = 18.9309
Total in-flight flits = 54657 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 511.337
	minimum = 22
	maximum = 1547
Network latency average = 501.458
	minimum = 22
	maximum = 1479
Slowest packet = 2924
Flit latency average = 464.669
	minimum = 5
	maximum = 1533
Slowest flit = 46212
Fragmentation average = 71.2934
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0149453
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.27738
	minimum = 0.15 (at node 153)
	maximum = 0.379 (at node 152)
Injected packet length average = 17.9239
Accepted packet length average = 18.5597
Total in-flight flits = 108064 (0 measured)
latency change    = 0.455003
throughput change = 0.0274518
Class 0:
Packet latency average = 1168.72
	minimum = 23
	maximum = 2337
Network latency average = 1157.32
	minimum = 22
	maximum = 2284
Slowest packet = 3942
Flit latency average = 1121.82
	minimum = 5
	maximum = 2267
Slowest flit = 70973
Fragmentation average = 97.5345
	minimum = 0
	maximum = 492
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0159896
	minimum = 0.005 (at node 126)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.288854
	minimum = 0.108 (at node 96)
	maximum = 0.455 (at node 16)
Injected packet length average = 18.0012
Accepted packet length average = 18.0651
Total in-flight flits = 159524 (0 measured)
latency change    = 0.562481
throughput change = 0.0397223
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 46.5957
	minimum = 22
	maximum = 146
Network latency average = 36.3872
	minimum = 22
	maximum = 146
Slowest packet = 20551
Flit latency average = 1573.15
	minimum = 5
	maximum = 2809
Slowest flit = 123317
Fragmentation average = 5.8383
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0160469
	minimum = 0.006 (at node 134)
	maximum = 0.029 (at node 103)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.290719
	minimum = 0.097 (at node 134)
	maximum = 0.513 (at node 103)
Injected packet length average = 17.9869
Accepted packet length average = 18.1168
Total in-flight flits = 213686 (101417 measured)
latency change    = 24.0821
throughput change = 0.0064137
Class 0:
Packet latency average = 46.4216
	minimum = 22
	maximum = 241
Network latency average = 36.4164
	minimum = 22
	maximum = 210
Slowest packet = 26809
Flit latency average = 1790.36
	minimum = 5
	maximum = 3415
Slowest flit = 166607
Fragmentation average = 5.91381
	minimum = 0
	maximum = 40
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.016013
	minimum = 0.0095 (at node 5)
	maximum = 0.023 (at node 90)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.290008
	minimum = 0.1795 (at node 86)
	maximum = 0.417 (at node 90)
Injected packet length average = 17.9923
Accepted packet length average = 18.1107
Total in-flight flits = 264809 (199209 measured)
latency change    = 0.00375139
throughput change = 0.00245144
Class 0:
Packet latency average = 195.004
	minimum = 22
	maximum = 3029
Network latency average = 184.946
	minimum = 22
	maximum = 2980
Slowest packet = 17873
Flit latency average = 2010.87
	minimum = 5
	maximum = 4132
Slowest flit = 196685
Fragmentation average = 10.1809
	minimum = 0
	maximum = 263
Injected packet rate average = 0.0313576
	minimum = 0.024 (at node 15)
	maximum = 0.0396667 (at node 67)
Accepted packet rate average = 0.0160503
	minimum = 0.0106667 (at node 135)
	maximum = 0.0216667 (at node 157)
Injected flit rate average = 0.564382
	minimum = 0.435667 (at node 15)
	maximum = 0.715667 (at node 67)
Accepted flit rate average= 0.289773
	minimum = 0.193 (at node 135)
	maximum = 0.390333 (at node 157)
Injected packet length average = 17.9982
Accepted packet length average = 18.054
Total in-flight flits = 317731 (296934 measured)
latency change    = 0.761945
throughput change = 0.00081182
Class 0:
Packet latency average = 1490.04
	minimum = 22
	maximum = 3975
Network latency average = 1479.88
	minimum = 22
	maximum = 3955
Slowest packet = 17890
Flit latency average = 2233.5
	minimum = 5
	maximum = 4894
Slowest flit = 220175
Fragmentation average = 58.8656
	minimum = 0
	maximum = 342
Injected packet rate average = 0.0313737
	minimum = 0.024 (at node 15)
	maximum = 0.038 (at node 67)
Accepted packet rate average = 0.0160807
	minimum = 0.012 (at node 161)
	maximum = 0.0205 (at node 103)
Injected flit rate average = 0.564616
	minimum = 0.4315 (at node 15)
	maximum = 0.68525 (at node 67)
Accepted flit rate average= 0.290509
	minimum = 0.21425 (at node 86)
	maximum = 0.37625 (at node 157)
Injected packet length average = 17.9965
Accepted packet length average = 18.0657
Total in-flight flits = 370123 (368118 measured)
latency change    = 0.869129
throughput change = 0.00253536
Class 0:
Packet latency average = 2344.46
	minimum = 22
	maximum = 4909
Network latency average = 2333.66
	minimum = 22
	maximum = 4889
Slowest packet = 18134
Flit latency average = 2453.84
	minimum = 5
	maximum = 5579
Slowest flit = 251009
Fragmentation average = 91.879
	minimum = 0
	maximum = 471
Injected packet rate average = 0.031424
	minimum = 0.0256 (at node 15)
	maximum = 0.0378 (at node 64)
Accepted packet rate average = 0.0161375
	minimum = 0.0116 (at node 104)
	maximum = 0.0212 (at node 157)
Injected flit rate average = 0.565514
	minimum = 0.4608 (at node 97)
	maximum = 0.6776 (at node 64)
Accepted flit rate average= 0.29176
	minimum = 0.2126 (at node 104)
	maximum = 0.3804 (at node 157)
Injected packet length average = 17.9963
Accepted packet length average = 18.0797
Total in-flight flits = 422440 (422256 measured)
latency change    = 0.36444
throughput change = 0.0042888
Class 0:
Packet latency average = 2832.33
	minimum = 22
	maximum = 5675
Network latency average = 2821.5
	minimum = 22
	maximum = 5662
Slowest packet = 18192
Flit latency average = 2675.69
	minimum = 5
	maximum = 5933
Slowest flit = 284957
Fragmentation average = 105.869
	minimum = 0
	maximum = 471
Injected packet rate average = 0.0313828
	minimum = 0.0255 (at node 97)
	maximum = 0.0365 (at node 64)
Accepted packet rate average = 0.0161901
	minimum = 0.0121667 (at node 80)
	maximum = 0.0201667 (at node 40)
Injected flit rate average = 0.564931
	minimum = 0.459 (at node 97)
	maximum = 0.6545 (at node 64)
Accepted flit rate average= 0.291999
	minimum = 0.220833 (at node 80)
	maximum = 0.363833 (at node 103)
Injected packet length average = 18.0013
Accepted packet length average = 18.0357
Total in-flight flits = 473895 (473895 measured)
latency change    = 0.172253
throughput change = 0.000817521
Class 0:
Packet latency average = 3185.07
	minimum = 22
	maximum = 6435
Network latency average = 3174.37
	minimum = 22
	maximum = 6418
Slowest packet = 18869
Flit latency average = 2898.51
	minimum = 5
	maximum = 6436
Slowest flit = 381460
Fragmentation average = 113.497
	minimum = 0
	maximum = 471
Injected packet rate average = 0.0312999
	minimum = 0.0252857 (at node 15)
	maximum = 0.0361429 (at node 64)
Accepted packet rate average = 0.016128
	minimum = 0.0125714 (at node 80)
	maximum = 0.02 (at node 40)
Injected flit rate average = 0.563356
	minimum = 0.455143 (at node 97)
	maximum = 0.650571 (at node 64)
Accepted flit rate average= 0.291406
	minimum = 0.222286 (at node 80)
	maximum = 0.36 (at node 40)
Injected packet length average = 17.9987
Accepted packet length average = 18.0684
Total in-flight flits = 525080 (525080 measured)
latency change    = 0.110746
throughput change = 0.00203455
Draining all recorded packets ...
Class 0:
Remaining flits: 424080 424081 424082 424083 424084 424085 424086 424087 424088 424089 [...] (578805 flits)
Measured flits: 424080 424081 424082 424083 424084 424085 424086 424087 424088 424089 [...] (479129 flits)
Class 0:
Remaining flits: 474753 474754 474755 474756 474757 474758 474759 474760 474761 474762 [...] (628000 flits)
Measured flits: 474753 474754 474755 474756 474757 474758 474759 474760 474761 474762 [...] (432156 flits)
Class 0:
Remaining flits: 524466 524467 524468 524469 524470 524471 524472 524473 524474 524475 [...] (680557 flits)
Measured flits: 524466 524467 524468 524469 524470 524471 524472 524473 524474 524475 [...] (385572 flits)
Class 0:
Remaining flits: 536742 536743 536744 536745 536746 536747 536748 536749 536750 536751 [...] (733583 flits)
Measured flits: 536742 536743 536744 536745 536746 536747 536748 536749 536750 536751 [...] (338478 flits)
Class 0:
Remaining flits: 586356 586357 586358 586359 586360 586361 586362 586363 586364 586365 [...] (785741 flits)
Measured flits: 586356 586357 586358 586359 586360 586361 586362 586363 586364 586365 [...] (291103 flits)
Class 0:
Remaining flits: 653022 653023 653024 653025 653026 653027 653028 653029 653030 653031 [...] (840557 flits)
Measured flits: 653022 653023 653024 653025 653026 653027 653028 653029 653030 653031 [...] (243875 flits)
Class 0:
Remaining flits: 700632 700633 700634 700635 700636 700637 700638 700639 700640 700641 [...] (890597 flits)
Measured flits: 700632 700633 700634 700635 700636 700637 700638 700639 700640 700641 [...] (196475 flits)
Class 0:
Remaining flits: 743520 743521 743522 743523 743524 743525 754944 754945 754946 754947 [...] (941730 flits)
Measured flits: 743520 743521 743522 743523 743524 743525 754944 754945 754946 754947 [...] (148886 flits)
Class 0:
Remaining flits: 803988 803989 803990 803991 803992 803993 803994 803995 803996 803997 [...] (993215 flits)
Measured flits: 803988 803989 803990 803991 803992 803993 803994 803995 803996 803997 [...] (101403 flits)
Class 0:
Remaining flits: 860094 860095 860096 860097 860098 860099 860100 860101 860102 860103 [...] (1045833 flits)
Measured flits: 860094 860095 860096 860097 860098 860099 860100 860101 860102 860103 [...] (55599 flits)
Class 0:
Remaining flits: 879809 879810 879811 879812 879813 879814 879815 879816 879817 879818 [...] (1096178 flits)
Measured flits: 879809 879810 879811 879812 879813 879814 879815 879816 879817 879818 [...] (22719 flits)
Class 0:
Remaining flits: 903667 903668 903669 903670 903671 915264 915265 915266 915267 915268 [...] (1147582 flits)
Measured flits: 903667 903668 903669 903670 903671 915264 915265 915266 915267 915268 [...] (7532 flits)
Class 0:
Remaining flits: 944204 944205 944206 944207 968238 968239 968240 968241 968242 968243 [...] (1199285 flits)
Measured flits: 944204 944205 944206 944207 968238 968239 968240 968241 968242 968243 [...] (1857 flits)
Class 0:
Remaining flits: 990216 990217 990218 990219 990220 990221 990222 990223 990224 990225 [...] (1249822 flits)
Measured flits: 990216 990217 990218 990219 990220 990221 990222 990223 990224 990225 [...] (537 flits)
Class 0:
Remaining flits: 1030729 1030730 1030731 1030732 1030733 1034430 1034431 1034432 1034433 1034434 [...] (1300588 flits)
Measured flits: 1030729 1030730 1030731 1030732 1030733 1034430 1034431 1034432 1034433 1034434 [...] (60 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1132821 1132822 1132823 1132824 1132825 1132826 1132827 1132828 1132829 1134684 [...] (1266143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1134694 1134695 1134696 1134697 1134698 1134699 1134700 1134701 1136664 1136665 [...] (1218566 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1213374 1213375 1213376 1213377 1213378 1213379 1216332 1216333 1216334 1216335 [...] (1171223 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1232612 1232613 1232614 1232615 1232616 1232617 1232618 1232619 1232620 1232621 [...] (1124030 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1298975 1298976 1298977 1298978 1298979 1298980 1298981 1298982 1298983 1298984 [...] (1076606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1360116 1360117 1360118 1360119 1360120 1360121 1360122 1360123 1360124 1360125 [...] (1029130 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379304 1379305 1379306 1379307 1379308 1379309 1379310 1379311 1379312 1379313 [...] (981795 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1413047 1413048 1413049 1413050 1413051 1413052 1413053 1429771 1429772 1429773 [...] (934130 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1496276 1496277 1496278 1496279 1496280 1496281 1496282 1496283 1496284 1496285 [...] (886604 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1525230 1525231 1525232 1525233 1525234 1525235 1525236 1525237 1525238 1525239 [...] (839034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1561104 1561105 1561106 1561107 1561108 1561109 1561110 1561111 1561112 1561113 [...] (791367 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1561104 1561105 1561106 1561107 1561108 1561109 1561110 1561111 1561112 1561113 [...] (743779 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1581156 1581157 1581158 1581159 1581160 1581161 1581162 1581163 1581164 1581165 [...] (696075 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1673352 1673353 1673354 1673355 1673356 1673357 1673358 1673359 1673360 1673361 [...] (648437 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1749904 1749905 1756512 1756513 1756514 1756515 1756516 1756517 1756518 1756519 [...] (601010 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1780434 1780435 1780436 1780437 1780438 1780439 1780440 1780441 1780442 1780443 [...] (553058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1780434 1780435 1780436 1780437 1780438 1780439 1780440 1780441 1780442 1780443 [...] (505869 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1786286 1786287 1786288 1786289 1786290 1786291 1786292 1786293 1786294 1786295 [...] (458257 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1817046 1817047 1817048 1817049 1817050 1817051 1817052 1817053 1817054 1817055 [...] (410580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1817046 1817047 1817048 1817049 1817050 1817051 1817052 1817053 1817054 1817055 [...] (363396 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1890828 1890829 1890830 1890831 1890832 1890833 1890834 1890835 1890836 1890837 [...] (315434 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1952658 1952659 1952660 1952661 1952662 1952663 1952664 1952665 1952666 1952667 [...] (267829 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1952658 1952659 1952660 1952661 1952662 1952663 1952664 1952665 1952666 1952667 [...] (220133 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1977120 1977121 1977122 1977123 1977124 1977125 1977126 1977127 1977128 1977129 [...] (172469 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2019276 2019277 2019278 2019279 2019280 2019281 2019282 2019283 2019284 2019285 [...] (125210 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2055546 2055547 2055548 2055549 2055550 2055551 2055552 2055553 2055554 2055555 [...] (77510 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2055546 2055547 2055548 2055549 2055550 2055551 2055552 2055553 2055554 2055555 [...] (34196 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2055546 2055547 2055548 2055549 2055550 2055551 2055552 2055553 2055554 2055555 [...] (8975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2278098 2278099 2278100 2278101 2278102 2278103 2278104 2278105 2278106 2278107 [...] (2130 flits)
Measured flits: (0 flits)
Time taken is 55197 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6810.03 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15595 (1 samples)
Network latency average = 6799.29 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15577 (1 samples)
Flit latency average = 13555.8 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34396 (1 samples)
Fragmentation average = 156.614 (1 samples)
	minimum = 0 (1 samples)
	maximum = 471 (1 samples)
Injected packet rate average = 0.0312999 (1 samples)
	minimum = 0.0252857 (1 samples)
	maximum = 0.0361429 (1 samples)
Accepted packet rate average = 0.016128 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.563356 (1 samples)
	minimum = 0.455143 (1 samples)
	maximum = 0.650571 (1 samples)
Accepted flit rate average = 0.291406 (1 samples)
	minimum = 0.222286 (1 samples)
	maximum = 0.36 (1 samples)
Injected packet size average = 17.9987 (1 samples)
Accepted packet size average = 18.0684 (1 samples)
Hops average = 5.07428 (1 samples)
Total run time 57.3928
