BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 365.271
	minimum = 22
	maximum = 882
Network latency average = 328.524
	minimum = 22
	maximum = 861
Slowest packet = 649
Flit latency average = 284.602
	minimum = 5
	maximum = 906
Slowest flit = 11527
Fragmentation average = 88.57
	minimum = 0
	maximum = 355
Injected packet rate average = 0.0292708
	minimum = 0.016 (at node 76)
	maximum = 0.042 (at node 145)
Accepted packet rate average = 0.0143281
	minimum = 0.007 (at node 108)
	maximum = 0.027 (at node 44)
Injected flit rate average = 0.520911
	minimum = 0.281 (at node 156)
	maximum = 0.756 (at node 145)
Accepted flit rate average= 0.273536
	minimum = 0.144 (at node 135)
	maximum = 0.49 (at node 44)
Injected packet length average = 17.7963
Accepted packet length average = 19.0909
Total in-flight flits = 49325 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 719.391
	minimum = 22
	maximum = 1753
Network latency average = 580.542
	minimum = 22
	maximum = 1738
Slowest packet = 1463
Flit latency average = 525.466
	minimum = 5
	maximum = 1779
Slowest flit = 31417
Fragmentation average = 86.0096
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0222109
	minimum = 0.013 (at node 52)
	maximum = 0.0305 (at node 145)
Accepted packet rate average = 0.0146484
	minimum = 0.009 (at node 153)
	maximum = 0.0215 (at node 88)
Injected flit rate average = 0.396409
	minimum = 0.233 (at node 76)
	maximum = 0.5405 (at node 145)
Accepted flit rate average= 0.269857
	minimum = 0.165 (at node 153)
	maximum = 0.387 (at node 88)
Injected packet length average = 17.8475
Accepted packet length average = 18.4222
Total in-flight flits = 50581 (0 measured)
latency change    = 0.492249
throughput change = 0.0136357
Class 0:
Packet latency average = 1782.95
	minimum = 893
	maximum = 2552
Network latency average = 993
	minimum = 22
	maximum = 2469
Slowest packet = 2428
Flit latency average = 927.019
	minimum = 5
	maximum = 2629
Slowest flit = 50570
Fragmentation average = 73.4832
	minimum = 0
	maximum = 351
Injected packet rate average = 0.0152188
	minimum = 0.002 (at node 72)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.014724
	minimum = 0.006 (at node 180)
	maximum = 0.027 (at node 120)
Injected flit rate average = 0.27476
	minimum = 0.02 (at node 72)
	maximum = 0.53 (at node 43)
Accepted flit rate average= 0.265396
	minimum = 0.115 (at node 163)
	maximum = 0.486 (at node 120)
Injected packet length average = 18.0541
Accepted packet length average = 18.0248
Total in-flight flits = 52419 (0 measured)
latency change    = 0.596516
throughput change = 0.0168086
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2517.58
	minimum = 1574
	maximum = 3256
Network latency average = 350.015
	minimum = 22
	maximum = 986
Slowest packet = 11525
Flit latency average = 975.668
	minimum = 5
	maximum = 2907
Slowest flit = 98703
Fragmentation average = 33.5087
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0148646
	minimum = 0.004 (at node 160)
	maximum = 0.027 (at node 39)
Accepted packet rate average = 0.0149531
	minimum = 0.006 (at node 4)
	maximum = 0.028 (at node 129)
Injected flit rate average = 0.266995
	minimum = 0.082 (at node 160)
	maximum = 0.483 (at node 39)
Accepted flit rate average= 0.268807
	minimum = 0.108 (at node 4)
	maximum = 0.464 (at node 129)
Injected packet length average = 17.9618
Accepted packet length average = 17.9767
Total in-flight flits = 52198 (42151 measured)
latency change    = 0.291803
throughput change = 0.0126911
Class 0:
Packet latency average = 3106.57
	minimum = 1574
	maximum = 4096
Network latency average = 772.198
	minimum = 22
	maximum = 1934
Slowest packet = 11525
Flit latency average = 963.824
	minimum = 5
	maximum = 2907
Slowest flit = 98703
Fragmentation average = 58.8108
	minimum = 0
	maximum = 275
Injected packet rate average = 0.0148542
	minimum = 0.0065 (at node 92)
	maximum = 0.0215 (at node 53)
Accepted packet rate average = 0.0149453
	minimum = 0.0085 (at node 5)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.267594
	minimum = 0.117 (at node 112)
	maximum = 0.3875 (at node 53)
Accepted flit rate average= 0.268836
	minimum = 0.1545 (at node 5)
	maximum = 0.407 (at node 165)
Injected packet length average = 18.0147
Accepted packet length average = 17.988
Total in-flight flits = 52002 (51625 measured)
latency change    = 0.189592
throughput change = 0.000106555
Class 0:
Packet latency average = 3536.6
	minimum = 1574
	maximum = 4891
Network latency average = 902.455
	minimum = 22
	maximum = 2806
Slowest packet = 11525
Flit latency average = 965.786
	minimum = 5
	maximum = 3439
Slowest flit = 170207
Fragmentation average = 66.3912
	minimum = 0
	maximum = 294
Injected packet rate average = 0.0149115
	minimum = 0.008 (at node 92)
	maximum = 0.023 (at node 113)
Accepted packet rate average = 0.0149601
	minimum = 0.01 (at node 139)
	maximum = 0.0203333 (at node 66)
Injected flit rate average = 0.26851
	minimum = 0.148 (at node 92)
	maximum = 0.412667 (at node 113)
Accepted flit rate average= 0.268689
	minimum = 0.178333 (at node 139)
	maximum = 0.366 (at node 66)
Injected packet length average = 18.007
Accepted packet length average = 17.9604
Total in-flight flits = 52400 (52378 measured)
latency change    = 0.121595
throughput change = 0.000545989
Class 0:
Packet latency average = 3921.91
	minimum = 1574
	maximum = 5784
Network latency average = 944.68
	minimum = 22
	maximum = 3185
Slowest packet = 11525
Flit latency average = 964.357
	minimum = 5
	maximum = 3632
Slowest flit = 204911
Fragmentation average = 69.9601
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0149154
	minimum = 0.00925 (at node 52)
	maximum = 0.0205 (at node 113)
Accepted packet rate average = 0.0149271
	minimum = 0.01075 (at node 139)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.268358
	minimum = 0.16375 (at node 52)
	maximum = 0.366 (at node 113)
Accepted flit rate average= 0.268721
	minimum = 0.19225 (at node 139)
	maximum = 0.3515 (at node 118)
Injected packet length average = 17.9921
Accepted packet length average = 18.0023
Total in-flight flits = 52033 (52033 measured)
latency change    = 0.0982458
throughput change = 0.000119522
Class 0:
Packet latency average = 4293.44
	minimum = 1574
	maximum = 6692
Network latency average = 974.213
	minimum = 22
	maximum = 4127
Slowest packet = 11525
Flit latency average = 968.596
	minimum = 5
	maximum = 4110
Slowest flit = 250036
Fragmentation average = 71.5692
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0149521
	minimum = 0.0098 (at node 144)
	maximum = 0.0196 (at node 146)
Accepted packet rate average = 0.0149042
	minimum = 0.0114 (at node 89)
	maximum = 0.0192 (at node 129)
Injected flit rate average = 0.269077
	minimum = 0.1734 (at node 144)
	maximum = 0.3524 (at node 146)
Accepted flit rate average= 0.268502
	minimum = 0.2062 (at node 89)
	maximum = 0.3406 (at node 129)
Injected packet length average = 17.996
Accepted packet length average = 18.0152
Total in-flight flits = 52921 (52921 measured)
latency change    = 0.0865339
throughput change = 0.000816645
Class 0:
Packet latency average = 4661.63
	minimum = 1574
	maximum = 7499
Network latency average = 985.843
	minimum = 22
	maximum = 4127
Slowest packet = 11525
Flit latency average = 967.691
	minimum = 5
	maximum = 4268
Slowest flit = 294601
Fragmentation average = 73.9938
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0149219
	minimum = 0.00966667 (at node 188)
	maximum = 0.0193333 (at node 79)
Accepted packet rate average = 0.0149132
	minimum = 0.0121667 (at node 63)
	maximum = 0.0183333 (at node 70)
Injected flit rate average = 0.268492
	minimum = 0.171833 (at node 188)
	maximum = 0.349333 (at node 79)
Accepted flit rate average= 0.268785
	minimum = 0.221167 (at node 144)
	maximum = 0.338 (at node 138)
Injected packet length average = 17.9932
Accepted packet length average = 18.0233
Total in-flight flits = 52001 (52001 measured)
latency change    = 0.0789846
throughput change = 0.00105154
Class 0:
Packet latency average = 5025.34
	minimum = 1574
	maximum = 7984
Network latency average = 993.252
	minimum = 22
	maximum = 4347
Slowest packet = 11525
Flit latency average = 964.776
	minimum = 5
	maximum = 4330
Slowest flit = 294605
Fragmentation average = 75.992
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0149368
	minimum = 0.00914286 (at node 124)
	maximum = 0.019 (at node 127)
Accepted packet rate average = 0.0149226
	minimum = 0.0115714 (at node 190)
	maximum = 0.0185714 (at node 70)
Injected flit rate average = 0.268857
	minimum = 0.164571 (at node 124)
	maximum = 0.342857 (at node 127)
Accepted flit rate average= 0.269004
	minimum = 0.209571 (at node 190)
	maximum = 0.334 (at node 70)
Injected packet length average = 17.9997
Accepted packet length average = 18.0266
Total in-flight flits = 52048 (52048 measured)
latency change    = 0.0723738
throughput change = 0.000814108
Draining all recorded packets ...
Class 0:
Remaining flits: 361332 361333 361334 361335 361336 361337 361338 361339 361340 361341 [...] (51533 flits)
Measured flits: 361332 361333 361334 361335 361336 361337 361338 361339 361340 361341 [...] (51533 flits)
Class 0:
Remaining flits: 434196 434197 434198 434199 434200 434201 434202 434203 434204 434205 [...] (51241 flits)
Measured flits: 434196 434197 434198 434199 434200 434201 434202 434203 434204 434205 [...] (51241 flits)
Class 0:
Remaining flits: 475722 475723 475724 475725 475726 475727 475728 475729 475730 475731 [...] (50894 flits)
Measured flits: 475722 475723 475724 475725 475726 475727 475728 475729 475730 475731 [...] (50894 flits)
Class 0:
Remaining flits: 549558 549559 549560 549561 549562 549563 549564 549565 549566 549567 [...] (50936 flits)
Measured flits: 549558 549559 549560 549561 549562 549563 549564 549565 549566 549567 [...] (50936 flits)
Class 0:
Remaining flits: 610992 610993 610994 610995 610996 610997 610998 610999 611000 611001 [...] (50870 flits)
Measured flits: 610992 610993 610994 610995 610996 610997 610998 610999 611000 611001 [...] (50870 flits)
Class 0:
Remaining flits: 629082 629083 629084 629085 629086 629087 629088 629089 629090 629091 [...] (50718 flits)
Measured flits: 629082 629083 629084 629085 629086 629087 629088 629089 629090 629091 [...] (50718 flits)
Class 0:
Remaining flits: 629128 629129 629130 629131 629132 629133 629134 629135 685692 685693 [...] (50144 flits)
Measured flits: 629128 629129 629130 629131 629132 629133 629134 629135 685692 685693 [...] (50144 flits)
Class 0:
Remaining flits: 742176 742177 742178 742179 742180 742181 742182 742183 742184 742185 [...] (49680 flits)
Measured flits: 742176 742177 742178 742179 742180 742181 742182 742183 742184 742185 [...] (49680 flits)
Class 0:
Remaining flits: 788487 788488 788489 824236 824237 839214 839215 839216 839217 839218 [...] (49997 flits)
Measured flits: 788487 788488 788489 824236 824237 839214 839215 839216 839217 839218 [...] (49997 flits)
Class 0:
Remaining flits: 849158 849159 849160 849161 849162 849163 849164 849165 849166 849167 [...] (50431 flits)
Measured flits: 849158 849159 849160 849161 849162 849163 849164 849165 849166 849167 [...] (50431 flits)
Class 0:
Remaining flits: 889559 893129 893130 893131 893132 893133 893134 893135 893136 893137 [...] (49867 flits)
Measured flits: 889559 893129 893130 893131 893132 893133 893134 893135 893136 893137 [...] (49867 flits)
Class 0:
Remaining flits: 980010 980011 980012 980013 980014 980015 980016 980017 980018 980019 [...] (49710 flits)
Measured flits: 980010 980011 980012 980013 980014 980015 980016 980017 980018 980019 [...] (49710 flits)
Class 0:
Remaining flits: 980016 980017 980018 980019 980020 980021 980022 980023 980024 980025 [...] (50370 flits)
Measured flits: 980016 980017 980018 980019 980020 980021 980022 980023 980024 980025 [...] (50370 flits)
Class 0:
Remaining flits: 1036890 1036891 1036892 1036893 1036894 1036895 1036896 1036897 1036898 1036899 [...] (50098 flits)
Measured flits: 1036890 1036891 1036892 1036893 1036894 1036895 1036896 1036897 1036898 1036899 [...] (50098 flits)
Class 0:
Remaining flits: 1121112 1121113 1121114 1121115 1121116 1121117 1121118 1121119 1121120 1121121 [...] (50096 flits)
Measured flits: 1121112 1121113 1121114 1121115 1121116 1121117 1121118 1121119 1121120 1121121 [...] (50096 flits)
Class 0:
Remaining flits: 1158390 1158391 1158392 1158393 1158394 1158395 1158396 1158397 1158398 1158399 [...] (50453 flits)
Measured flits: 1158390 1158391 1158392 1158393 1158394 1158395 1158396 1158397 1158398 1158399 [...] (50453 flits)
Class 0:
Remaining flits: 1254272 1254273 1254274 1254275 1266426 1266427 1266428 1266429 1266430 1266431 [...] (50037 flits)
Measured flits: 1254272 1254273 1254274 1254275 1266426 1266427 1266428 1266429 1266430 1266431 [...] (49983 flits)
Class 0:
Remaining flits: 1278702 1278703 1278704 1278705 1278706 1278707 1278708 1278709 1278710 1278711 [...] (50986 flits)
Measured flits: 1278702 1278703 1278704 1278705 1278706 1278707 1278708 1278709 1278710 1278711 [...] (50623 flits)
Class 0:
Remaining flits: 1302406 1302407 1308672 1308673 1308674 1308675 1308676 1308677 1308678 1308679 [...] (49399 flits)
Measured flits: 1302406 1302407 1308672 1308673 1308674 1308675 1308676 1308677 1308678 1308679 [...] (48575 flits)
Class 0:
Remaining flits: 1308672 1308673 1308674 1308675 1308676 1308677 1308678 1308679 1308680 1308681 [...] (48965 flits)
Measured flits: 1308672 1308673 1308674 1308675 1308676 1308677 1308678 1308679 1308680 1308681 [...] (47038 flits)
Class 0:
Remaining flits: 1370736 1370737 1370738 1370739 1370740 1370741 1370742 1370743 1370744 1370745 [...] (49923 flits)
Measured flits: 1370736 1370737 1370738 1370739 1370740 1370741 1370742 1370743 1370744 1370745 [...] (45789 flits)
Class 0:
Remaining flits: 1437840 1437841 1437842 1437843 1437844 1437845 1437846 1437847 1437848 1437849 [...] (50051 flits)
Measured flits: 1437840 1437841 1437842 1437843 1437844 1437845 1437846 1437847 1437848 1437849 [...] (41017 flits)
Class 0:
Remaining flits: 1509354 1509355 1509356 1509357 1509358 1509359 1509360 1509361 1509362 1509363 [...] (50863 flits)
Measured flits: 1509354 1509355 1509356 1509357 1509358 1509359 1509360 1509361 1509362 1509363 [...] (34967 flits)
Class 0:
Remaining flits: 1536710 1536711 1536712 1536713 1562112 1562113 1562114 1562115 1562116 1562117 [...] (50273 flits)
Measured flits: 1536710 1536711 1536712 1536713 1569114 1569115 1569116 1569117 1569118 1569119 [...] (25586 flits)
Class 0:
Remaining flits: 1562112 1562113 1562114 1562115 1562116 1562117 1562118 1562119 1562120 1562121 [...] (49325 flits)
Measured flits: 1594836 1594837 1594838 1594839 1594840 1594841 1594842 1594843 1594844 1594845 [...] (18711 flits)
Class 0:
Remaining flits: 1590732 1590733 1590734 1590735 1590736 1590737 1590738 1590739 1590740 1590741 [...] (49514 flits)
Measured flits: 1642050 1642051 1642052 1642053 1642054 1642055 1642056 1642057 1642058 1642059 [...] (14124 flits)
Class 0:
Remaining flits: 1590732 1590733 1590734 1590735 1590736 1590737 1590738 1590739 1590740 1590741 [...] (50129 flits)
Measured flits: 1731125 1731126 1731127 1731128 1731129 1731130 1731131 1740888 1740889 1740890 [...] (10810 flits)
Class 0:
Remaining flits: 1811934 1811935 1811936 1811937 1811938 1811939 1811940 1811941 1811942 1811943 [...] (50334 flits)
Measured flits: 1826334 1826335 1826336 1826337 1826338 1826339 1826340 1826341 1826342 1826343 [...] (9082 flits)
Class 0:
Remaining flits: 1843005 1843006 1843007 1843008 1843009 1843010 1843011 1843012 1843013 1843014 [...] (49157 flits)
Measured flits: 1898100 1898101 1898102 1898103 1898104 1898105 1898106 1898107 1898108 1898109 [...] (7416 flits)
Class 0:
Remaining flits: 1888092 1888093 1888094 1888095 1888096 1888097 1888098 1888099 1888100 1888101 [...] (49137 flits)
Measured flits: 1925640 1925641 1925642 1925643 1925644 1925645 1925646 1925647 1925648 1925649 [...] (5354 flits)
Class 0:
Remaining flits: 1888092 1888093 1888094 1888095 1888096 1888097 1888098 1888099 1888100 1888101 [...] (49878 flits)
Measured flits: 1976922 1976923 1976924 1976925 1976926 1976927 1976928 1976929 1976930 1976931 [...] (4061 flits)
Class 0:
Remaining flits: 1975536 1975537 1975538 1975539 1975540 1975541 1975542 1975543 1975544 1975545 [...] (49632 flits)
Measured flits: 2030174 2030175 2030176 2030177 2030178 2030179 2030180 2030181 2030182 2030183 [...] (3515 flits)
Class 0:
Remaining flits: 1976652 1976653 1976654 1976655 1976656 1976657 1976658 1976659 1976660 1976661 [...] (50599 flits)
Measured flits: 2079112 2079113 2079114 2079115 2079116 2079117 2079118 2079119 2079120 2079121 [...] (2752 flits)
Class 0:
Remaining flits: 2040066 2040067 2040068 2040069 2040070 2040071 2040072 2040073 2040074 2040075 [...] (50933 flits)
Measured flits: 2196846 2196847 2196848 2196849 2196850 2196851 2196852 2196853 2196854 2196855 [...] (2349 flits)
Class 0:
Remaining flits: 2109726 2109727 2109728 2109729 2109730 2109731 2109732 2109733 2109734 2109735 [...] (50518 flits)
Measured flits: 2251350 2251351 2251352 2251353 2251354 2251355 2251356 2251357 2251358 2251359 [...] (1798 flits)
Class 0:
Remaining flits: 2209284 2209285 2209286 2209287 2209288 2209289 2209290 2209291 2209292 2209293 [...] (51576 flits)
Measured flits: 2303496 2303497 2303498 2303499 2303500 2303501 2303502 2303503 2303504 2303505 [...] (1062 flits)
Class 0:
Remaining flits: 2238234 2238235 2238236 2238237 2238238 2238239 2238240 2238241 2238242 2238243 [...] (50217 flits)
Measured flits: 2363164 2363165 2369448 2369449 2369450 2369451 2369452 2369453 2369454 2369455 [...] (631 flits)
Class 0:
Remaining flits: 2309436 2309437 2309438 2309439 2309440 2309441 2309442 2309443 2309444 2309445 [...] (49772 flits)
Measured flits: 2400300 2400301 2400302 2400303 2400304 2400305 2400306 2400307 2400308 2400309 [...] (387 flits)
Class 0:
Remaining flits: 2309436 2309437 2309438 2309439 2309440 2309441 2309442 2309443 2309444 2309445 [...] (49876 flits)
Measured flits: 2433258 2433259 2433260 2433261 2433262 2433263 2433264 2433265 2433266 2433267 [...] (204 flits)
Class 0:
Remaining flits: 2397690 2397691 2397692 2397693 2397694 2397695 2397696 2397697 2397698 2397699 [...] (49228 flits)
Measured flits: 2492082 2492083 2492084 2492085 2492086 2492087 2492088 2492089 2492090 2492091 [...] (128 flits)
Class 0:
Remaining flits: 2465028 2465029 2465030 2465031 2465032 2465033 2465034 2465035 2465036 2465037 [...] (49784 flits)
Measured flits: 2577326 2577327 2577328 2577329 2648852 2648853 2648854 2648855 2648856 2648857 [...] (32 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2517375 2517376 2517377 2517378 2517379 2517380 2517381 2517382 2517383 2517384 [...] (8730 flits)
Measured flits: (0 flits)
Time taken is 53537 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14676 (1 samples)
	minimum = 1574 (1 samples)
	maximum = 41664 (1 samples)
Network latency average = 1001.29 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6545 (1 samples)
Flit latency average = 946.006 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7783 (1 samples)
Fragmentation average = 78.3639 (1 samples)
	minimum = 0 (1 samples)
	maximum = 372 (1 samples)
Injected packet rate average = 0.0149368 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.019 (1 samples)
Accepted packet rate average = 0.0149226 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.268857 (1 samples)
	minimum = 0.164571 (1 samples)
	maximum = 0.342857 (1 samples)
Accepted flit rate average = 0.269004 (1 samples)
	minimum = 0.209571 (1 samples)
	maximum = 0.334 (1 samples)
Injected packet size average = 17.9997 (1 samples)
Accepted packet size average = 18.0266 (1 samples)
Hops average = 5.06376 (1 samples)
Total run time 77.7585
