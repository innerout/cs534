BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 350.98
	minimum = 23
	maximum = 903
Network latency average = 334.447
	minimum = 23
	maximum = 889
Slowest packet = 449
Flit latency average = 238.16
	minimum = 6
	maximum = 912
Slowest flit = 6705
Fragmentation average = 229.042
	minimum = 0
	maximum = 842
Injected packet rate average = 0.027599
	minimum = 0.016 (at node 160)
	maximum = 0.04 (at node 137)
Accepted packet rate average = 0.00973958
	minimum = 0.003 (at node 112)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.491849
	minimum = 0.283 (at node 160)
	maximum = 0.713 (at node 137)
Accepted flit rate average= 0.219635
	minimum = 0.104 (at node 41)
	maximum = 0.371 (at node 103)
Injected packet length average = 17.8213
Accepted packet length average = 22.5508
Total in-flight flits = 53248 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 662.498
	minimum = 23
	maximum = 1907
Network latency average = 590.993
	minimum = 23
	maximum = 1888
Slowest packet = 498
Flit latency average = 463.029
	minimum = 6
	maximum = 1872
Slowest flit = 11764
Fragmentation average = 299.89
	minimum = 0
	maximum = 1351
Injected packet rate average = 0.0219896
	minimum = 0.01 (at node 160)
	maximum = 0.03 (at node 165)
Accepted packet rate average = 0.0111823
	minimum = 0.0055 (at node 135)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.392115
	minimum = 0.173 (at node 160)
	maximum = 0.539 (at node 165)
Accepted flit rate average= 0.22369
	minimum = 0.1265 (at node 135)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.8318
Accepted packet length average = 20.004
Total in-flight flits = 66221 (0 measured)
latency change    = 0.470217
throughput change = 0.0181264
Class 0:
Packet latency average = 1438.19
	minimum = 40
	maximum = 2797
Network latency average = 1122.98
	minimum = 30
	maximum = 2761
Slowest packet = 1232
Flit latency average = 992.646
	minimum = 6
	maximum = 2829
Slowest flit = 15851
Fragmentation average = 322.381
	minimum = 3
	maximum = 2212
Injected packet rate average = 0.00864063
	minimum = 0 (at node 4)
	maximum = 0.042 (at node 45)
Accepted packet rate average = 0.0122656
	minimum = 0.005 (at node 70)
	maximum = 0.021 (at node 33)
Injected flit rate average = 0.155104
	minimum = 0 (at node 9)
	maximum = 0.752 (at node 45)
Accepted flit rate average= 0.217104
	minimum = 0.089 (at node 70)
	maximum = 0.373 (at node 34)
Injected packet length average = 17.9506
Accepted packet length average = 17.7002
Total in-flight flits = 54471 (0 measured)
latency change    = 0.539354
throughput change = 0.0303354
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1884.32
	minimum = 364
	maximum = 3281
Network latency average = 306.356
	minimum = 23
	maximum = 945
Slowest packet = 10134
Flit latency average = 1138.15
	minimum = 6
	maximum = 3870
Slowest flit = 12337
Fragmentation average = 151.287
	minimum = 0
	maximum = 826
Injected packet rate average = 0.0102917
	minimum = 0 (at node 1)
	maximum = 0.042 (at node 21)
Accepted packet rate average = 0.0120885
	minimum = 0.004 (at node 31)
	maximum = 0.022 (at node 151)
Injected flit rate average = 0.185266
	minimum = 0 (at node 3)
	maximum = 0.757 (at node 21)
Accepted flit rate average= 0.210292
	minimum = 0.092 (at node 162)
	maximum = 0.405 (at node 99)
Injected packet length average = 18.0015
Accepted packet length average = 17.396
Total in-flight flits = 49681 (22372 measured)
latency change    = 0.236758
throughput change = 0.0323955
Class 0:
Packet latency average = 2303.64
	minimum = 364
	maximum = 4226
Network latency average = 495.44
	minimum = 23
	maximum = 1914
Slowest packet = 10134
Flit latency average = 1107.73
	minimum = 6
	maximum = 4886
Slowest flit = 9070
Fragmentation average = 179.706
	minimum = 0
	maximum = 1226
Injected packet rate average = 0.0114115
	minimum = 0 (at node 3)
	maximum = 0.033 (at node 61)
Accepted packet rate average = 0.0118776
	minimum = 0.007 (at node 5)
	maximum = 0.018 (at node 182)
Injected flit rate average = 0.205305
	minimum = 0 (at node 3)
	maximum = 0.594 (at node 61)
Accepted flit rate average= 0.210687
	minimum = 0.1225 (at node 180)
	maximum = 0.3135 (at node 99)
Injected packet length average = 17.9911
Accepted packet length average = 17.7382
Total in-flight flits = 52515 (36258 measured)
latency change    = 0.182025
throughput change = 0.00187877
Class 0:
Packet latency average = 2658.47
	minimum = 364
	maximum = 5144
Network latency average = 651.569
	minimum = 23
	maximum = 2950
Slowest packet = 10134
Flit latency average = 1121.14
	minimum = 6
	maximum = 5894
Slowest flit = 9601
Fragmentation average = 201.693
	minimum = 0
	maximum = 1930
Injected packet rate average = 0.0112604
	minimum = 0 (at node 3)
	maximum = 0.033 (at node 57)
Accepted packet rate average = 0.011724
	minimum = 0.007 (at node 5)
	maximum = 0.0173333 (at node 99)
Injected flit rate average = 0.202738
	minimum = 0 (at node 33)
	maximum = 0.592667 (at node 57)
Accepted flit rate average= 0.20904
	minimum = 0.139 (at node 5)
	maximum = 0.309 (at node 99)
Injected packet length average = 18.0045
Accepted packet length average = 17.8301
Total in-flight flits = 50776 (40235 measured)
latency change    = 0.133473
throughput change = 0.0078816
Class 0:
Packet latency average = 3031.27
	minimum = 364
	maximum = 5966
Network latency average = 753.299
	minimum = 23
	maximum = 3842
Slowest packet = 10134
Flit latency average = 1130.38
	minimum = 6
	maximum = 6678
Slowest flit = 33438
Fragmentation average = 216.768
	minimum = 0
	maximum = 3273
Injected packet rate average = 0.0111224
	minimum = 0 (at node 3)
	maximum = 0.034 (at node 57)
Accepted packet rate average = 0.0117135
	minimum = 0.00825 (at node 135)
	maximum = 0.0165 (at node 118)
Injected flit rate average = 0.200191
	minimum = 0 (at node 54)
	maximum = 0.6095 (at node 57)
Accepted flit rate average= 0.208996
	minimum = 0.15175 (at node 84)
	maximum = 0.28775 (at node 118)
Injected packet length average = 17.9989
Accepted packet length average = 17.8423
Total in-flight flits = 47772 (40662 measured)
latency change    = 0.122984
throughput change = 0.000209749
Class 0:
Packet latency average = 3393.14
	minimum = 364
	maximum = 7225
Network latency average = 821.358
	minimum = 23
	maximum = 4926
Slowest packet = 10134
Flit latency average = 1130.71
	minimum = 6
	maximum = 7705
Slowest flit = 30574
Fragmentation average = 227.621
	minimum = 0
	maximum = 3999
Injected packet rate average = 0.0115771
	minimum = 0 (at node 3)
	maximum = 0.0286 (at node 57)
Accepted packet rate average = 0.0116885
	minimum = 0.0084 (at node 135)
	maximum = 0.0154 (at node 99)
Injected flit rate average = 0.208488
	minimum = 0 (at node 54)
	maximum = 0.514 (at node 57)
Accepted flit rate average= 0.209073
	minimum = 0.1572 (at node 162)
	maximum = 0.2744 (at node 128)
Injected packet length average = 18.0086
Accepted packet length average = 17.887
Total in-flight flits = 53921 (48967 measured)
latency change    = 0.106648
throughput change = 0.000367446
Class 0:
Packet latency average = 3726.99
	minimum = 364
	maximum = 8040
Network latency average = 878.391
	minimum = 23
	maximum = 5879
Slowest packet = 10134
Flit latency average = 1128.5
	minimum = 6
	maximum = 8642
Slowest flit = 35886
Fragmentation average = 234.812
	minimum = 0
	maximum = 3999
Injected packet rate average = 0.0115252
	minimum = 0 (at node 3)
	maximum = 0.0273333 (at node 61)
Accepted packet rate average = 0.0116597
	minimum = 0.00883333 (at node 104)
	maximum = 0.015 (at node 118)
Injected flit rate average = 0.207489
	minimum = 0 (at node 54)
	maximum = 0.491 (at node 61)
Accepted flit rate average= 0.208561
	minimum = 0.159167 (at node 104)
	maximum = 0.270167 (at node 128)
Injected packet length average = 18.0031
Accepted packet length average = 17.8873
Total in-flight flits = 53339 (49496 measured)
latency change    = 0.089576
throughput change = 0.00245565
Class 0:
Packet latency average = 4061.94
	minimum = 364
	maximum = 8893
Network latency average = 930.404
	minimum = 23
	maximum = 6692
Slowest packet = 10134
Flit latency average = 1134.68
	minimum = 6
	maximum = 9475
Slowest flit = 37169
Fragmentation average = 240.807
	minimum = 0
	maximum = 4212
Injected packet rate average = 0.0114799
	minimum = 0 (at node 54)
	maximum = 0.0261429 (at node 61)
Accepted packet rate average = 0.011593
	minimum = 0.00928571 (at node 57)
	maximum = 0.0147143 (at node 128)
Injected flit rate average = 0.206754
	minimum = 0 (at node 54)
	maximum = 0.47 (at node 61)
Accepted flit rate average= 0.207779
	minimum = 0.164429 (at node 57)
	maximum = 0.269571 (at node 128)
Injected packet length average = 18.0101
Accepted packet length average = 17.9228
Total in-flight flits = 53046 (50007 measured)
latency change    = 0.0824612
throughput change = 0.00376239
Draining all recorded packets ...
Class 0:
Remaining flits: 19368 19369 19370 19371 19372 19373 19374 19375 19376 19377 [...] (52668 flits)
Measured flits: 183204 183205 183206 183207 183208 183209 183210 183211 183212 183213 [...] (50224 flits)
Class 0:
Remaining flits: 19368 19369 19370 19371 19372 19373 19374 19375 19376 19377 [...] (55931 flits)
Measured flits: 183204 183205 183206 183207 183208 183209 183210 183211 183212 183213 [...] (53924 flits)
Class 0:
Remaining flits: 19368 19369 19370 19371 19372 19373 19374 19375 19376 19377 [...] (58596 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (56943 flits)
Class 0:
Remaining flits: 19368 19369 19370 19371 19372 19373 19374 19375 19376 19377 [...] (57763 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (56388 flits)
Class 0:
Remaining flits: 19368 19369 19370 19371 19372 19373 19374 19375 19376 19377 [...] (57811 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (56810 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (58027 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (56549 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (59984 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (58285 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (61157 flits)
Measured flits: 184320 184321 184322 184323 184324 184325 184326 184327 184328 184329 [...] (59477 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (63235 flits)
Measured flits: 227142 227143 227144 227145 227146 227147 227148 227149 227150 227151 [...] (61042 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (63431 flits)
Measured flits: 227142 227143 227144 227145 227146 227147 227148 227149 227150 227151 [...] (59596 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (64277 flits)
Measured flits: 245304 245305 245306 245307 245308 245309 245310 245311 245312 245313 [...] (58448 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (64009 flits)
Measured flits: 245304 245305 245306 245307 245308 245309 245310 245311 245312 245313 [...] (55613 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (62932 flits)
Measured flits: 245304 245305 245306 245307 245308 245309 245310 245311 245312 245313 [...] (51149 flits)
Class 0:
Remaining flits: 21654 21655 21656 21657 21658 21659 21660 21661 21662 21663 [...] (64954 flits)
Measured flits: 245304 245305 245306 245307 245308 245309 245310 245311 245312 245313 [...] (50532 flits)
Class 0:
Remaining flits: 28044 28045 28046 28047 28048 28049 28050 28051 28052 28053 [...] (65890 flits)
Measured flits: 245312 245313 245314 245315 245316 245317 245318 245319 245320 245321 [...] (47165 flits)
Class 0:
Remaining flits: 28050 28051 28052 28053 28054 28055 28056 28057 28058 28059 [...] (65946 flits)
Measured flits: 324072 324073 324074 324075 324076 324077 324078 324079 324080 324081 [...] (43184 flits)
Class 0:
Remaining flits: 64963 64964 64965 64966 64967 64968 64969 64970 64971 64972 [...] (64204 flits)
Measured flits: 324072 324073 324074 324075 324076 324077 324078 324079 324080 324081 [...] (38413 flits)
Class 0:
Remaining flits: 64963 64964 64965 64966 64967 64968 64969 64970 64971 64972 [...] (62997 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (34054 flits)
Class 0:
Remaining flits: 64963 64964 64965 64966 64967 64968 64969 64970 64971 64972 [...] (65843 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (32189 flits)
Class 0:
Remaining flits: 64963 64964 64965 64966 64967 64968 64969 64970 64971 64972 [...] (63864 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (29056 flits)
Class 0:
Remaining flits: 64963 64964 64965 64966 64967 64968 64969 64970 64971 64972 [...] (63683 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (26528 flits)
Class 0:
Remaining flits: 64976 64977 64978 64979 345582 345583 345584 345585 345586 345587 [...] (63528 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (23953 flits)
Class 0:
Remaining flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (66229 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (22435 flits)
Class 0:
Remaining flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (66312 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (20022 flits)
Class 0:
Remaining flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (66258 flits)
Measured flits: 345582 345583 345584 345585 345586 345587 345588 345589 345590 345591 [...] (18675 flits)
Class 0:
Remaining flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (67273 flits)
Measured flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (18126 flits)
Class 0:
Remaining flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (66592 flits)
Measured flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (15103 flits)
Class 0:
Remaining flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (67073 flits)
Measured flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (13728 flits)
Class 0:
Remaining flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (66679 flits)
Measured flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (12136 flits)
Class 0:
Remaining flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (67804 flits)
Measured flits: 531684 531685 531686 531687 531688 531689 531690 531691 531692 531693 [...] (10827 flits)
Class 0:
Remaining flits: 667926 667927 667928 667929 667930 667931 667932 667933 667934 667935 [...] (69434 flits)
Measured flits: 667926 667927 667928 667929 667930 667931 667932 667933 667934 667935 [...] (10073 flits)
Class 0:
Remaining flits: 681828 681829 681830 681831 681832 681833 681834 681835 681836 681837 [...] (66934 flits)
Measured flits: 681828 681829 681830 681831 681832 681833 681834 681835 681836 681837 [...] (8697 flits)
Class 0:
Remaining flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (66970 flits)
Measured flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (8162 flits)
Class 0:
Remaining flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (69454 flits)
Measured flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (7066 flits)
Class 0:
Remaining flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (66987 flits)
Measured flits: 695394 695395 695396 695397 695398 695399 695400 695401 695402 695403 [...] (6225 flits)
Class 0:
Remaining flits: 695407 695408 695409 695410 695411 706896 706897 706898 706899 706900 [...] (67364 flits)
Measured flits: 695407 695408 695409 695410 695411 706896 706897 706898 706899 706900 [...] (5922 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (66227 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (5111 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64280 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (4479 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (66356 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (3706 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64172 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (3107 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (62721 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2639 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (62263 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2400 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (62063 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2420 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (63053 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2211 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64130 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2249 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (63179 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (2060 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64028 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1847 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64367 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1649 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64762 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1704 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64809 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1750 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64013 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1333 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (63097 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1362 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (63324 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1384 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64445 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (1106 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (64257 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (992 flits)
Class 0:
Remaining flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (65220 flits)
Measured flits: 706896 706897 706898 706899 706900 706901 706902 706903 706904 706905 [...] (873 flits)
Class 0:
Remaining flits: 706900 706901 706902 706903 706904 706905 706906 706907 706908 706909 [...] (64229 flits)
Measured flits: 706900 706901 706902 706903 706904 706905 706906 706907 706908 706909 [...] (674 flits)
Class 0:
Remaining flits: 1174338 1174339 1174340 1174341 1174342 1174343 1174344 1174345 1174346 1174347 [...] (61582 flits)
Measured flits: 1174338 1174339 1174340 1174341 1174342 1174343 1174344 1174345 1174346 1174347 [...] (564 flits)
Class 0:
Remaining flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (62312 flits)
Measured flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (429 flits)
Class 0:
Remaining flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (61177 flits)
Measured flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (345 flits)
Class 0:
Remaining flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (61940 flits)
Measured flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (316 flits)
Class 0:
Remaining flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (61396 flits)
Measured flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (288 flits)
Class 0:
Remaining flits: 1480734 1480735 1480736 1480737 1480738 1480739 1480740 1480741 1480742 1480743 [...] (60658 flits)
Measured flits: 1835638 1835639 1971468 1971469 1971470 1971471 1971472 1971473 1971474 1971475 [...] (436 flits)
Class 0:
Remaining flits: 1480734 1480735 1480736 1480737 1480738 1480739 1480740 1480741 1480742 1480743 [...] (62748 flits)
Measured flits: 1971468 1971469 1971470 1971471 1971472 1971473 1971474 1971475 1971476 1971477 [...] (394 flits)
Class 0:
Remaining flits: 1480734 1480735 1480736 1480737 1480738 1480739 1480740 1480741 1480742 1480743 [...] (62696 flits)
Measured flits: 1971468 1971469 1971470 1971471 1971472 1971473 1971474 1971475 1971476 1971477 [...] (378 flits)
Class 0:
Remaining flits: 1480734 1480735 1480736 1480737 1480738 1480739 1480740 1480741 1480742 1480743 [...] (63397 flits)
Measured flits: 1971468 1971469 1971470 1971471 1971472 1971473 1971474 1971475 1971476 1971477 [...] (308 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (59934 flits)
Measured flits: 2101147 2101148 2101149 2101150 2101151 2101152 2101153 2101154 2101155 2101156 [...] (188 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (62641 flits)
Measured flits: 2101147 2101148 2101149 2101150 2101151 2101152 2101153 2101154 2101155 2101156 [...] (138 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (63733 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (108 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (60942 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (108 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (62898 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (54 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (62653 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (86 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (62810 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (54 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (63726 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (36 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (61729 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (36 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (63500 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (36 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (63322 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (18 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (64266 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (18 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (62869 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (18 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (61635 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (18 flits)
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (61290 flits)
Measured flits: 2164608 2164609 2164610 2164611 2164612 2164613 2164614 2164615 2164616 2164617 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1742202 1742203 1742204 1742205 1742206 1742207 1742208 1742209 1742210 1742211 [...] (38726 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1886723 2053800 2053801 2053802 2053803 2053804 2053805 2053806 2053807 2053808 [...] (25916 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2099057 2099058 2099059 2099060 2099061 2099062 2099063 2099064 2099065 2099066 [...] (14792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2282417 2388006 2388007 2388008 2388009 2388010 2388011 2388012 2388013 2388014 [...] (6999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2388006 2388007 2388008 2388009 2388010 2388011 2388012 2388013 2388014 2388015 [...] (3224 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2388006 2388007 2388008 2388009 2388010 2388011 2388012 2388013 2388014 2388015 [...] (1049 flits)
Measured flits: (0 flits)
Time taken is 98514 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13235.1 (1 samples)
	minimum = 364 (1 samples)
	maximum = 83012 (1 samples)
Network latency average = 1728.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 50916 (1 samples)
Flit latency average = 1891.55 (1 samples)
	minimum = 6 (1 samples)
	maximum = 50899 (1 samples)
Fragmentation average = 261.797 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18547 (1 samples)
Injected packet rate average = 0.0114799 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0261429 (1 samples)
Accepted packet rate average = 0.011593 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0147143 (1 samples)
Injected flit rate average = 0.206754 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.47 (1 samples)
Accepted flit rate average = 0.207779 (1 samples)
	minimum = 0.164429 (1 samples)
	maximum = 0.269571 (1 samples)
Injected packet size average = 18.0101 (1 samples)
Accepted packet size average = 17.9228 (1 samples)
Hops average = 5.06209 (1 samples)
Total run time 150.564
