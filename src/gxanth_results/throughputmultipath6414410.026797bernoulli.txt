BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.936
	minimum = 24
	maximum = 934
Network latency average = 292.454
	minimum = 23
	maximum = 924
Slowest packet = 342
Flit latency average = 212.59
	minimum = 6
	maximum = 907
Slowest flit = 6173
Fragmentation average = 177.345
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0100938
	minimum = 0.004 (at node 45)
	maximum = 0.019 (at node 91)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.215708
	minimum = 0.103 (at node 185)
	maximum = 0.358 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 21.3705
Total in-flight flits = 51194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 522.072
	minimum = 24
	maximum = 1915
Network latency average = 514.824
	minimum = 23
	maximum = 1906
Slowest packet = 228
Flit latency average = 416.903
	minimum = 6
	maximum = 1943
Slowest flit = 3944
Fragmentation average = 239.992
	minimum = 0
	maximum = 1570
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0114245
	minimum = 0.0065 (at node 96)
	maximum = 0.018 (at node 67)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.226555
	minimum = 0.122 (at node 96)
	maximum = 0.3305 (at node 44)
Injected packet length average = 17.9276
Accepted packet length average = 19.8306
Total in-flight flits = 97431 (0 measured)
latency change    = 0.427406
throughput change = 0.0478752
Class 0:
Packet latency average = 1081.48
	minimum = 24
	maximum = 2806
Network latency average = 1073.52
	minimum = 24
	maximum = 2806
Slowest packet = 858
Flit latency average = 969.129
	minimum = 6
	maximum = 2818
Slowest flit = 15122
Fragmentation average = 337.908
	minimum = 0
	maximum = 2692
Injected packet rate average = 0.0266667
	minimum = 0.015 (at node 7)
	maximum = 0.039 (at node 24)
Accepted packet rate average = 0.0132656
	minimum = 0.004 (at node 135)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.479432
	minimum = 0.26 (at node 7)
	maximum = 0.692 (at node 24)
Accepted flit rate average= 0.244286
	minimum = 0.092 (at node 135)
	maximum = 0.484 (at node 129)
Injected packet length average = 17.9787
Accepted packet length average = 18.415
Total in-flight flits = 142688 (0 measured)
latency change    = 0.517259
throughput change = 0.072586
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 160.488
	minimum = 25
	maximum = 989
Network latency average = 150.955
	minimum = 25
	maximum = 982
Slowest packet = 15423
Flit latency average = 1387.63
	minimum = 6
	maximum = 3846
Slowest flit = 8420
Fragmentation average = 77.6296
	minimum = 1
	maximum = 661
Injected packet rate average = 0.0259583
	minimum = 0.007 (at node 8)
	maximum = 0.041 (at node 55)
Accepted packet rate average = 0.013151
	minimum = 0.005 (at node 86)
	maximum = 0.023 (at node 68)
Injected flit rate average = 0.466656
	minimum = 0.141 (at node 8)
	maximum = 0.723 (at node 55)
Accepted flit rate average= 0.24226
	minimum = 0.105 (at node 86)
	maximum = 0.401 (at node 119)
Injected packet length average = 17.9771
Accepted packet length average = 18.4214
Total in-flight flits = 185886 (79905 measured)
latency change    = 5.73866
throughput change = 0.00836307
Class 0:
Packet latency average = 494.241
	minimum = 23
	maximum = 1924
Network latency average = 473.187
	minimum = 23
	maximum = 1924
Slowest packet = 15459
Flit latency average = 1640.9
	minimum = 6
	maximum = 4649
Slowest flit = 30005
Fragmentation average = 167.129
	minimum = 0
	maximum = 1257
Injected packet rate average = 0.0251432
	minimum = 0.0115 (at node 8)
	maximum = 0.039 (at node 150)
Accepted packet rate average = 0.0132057
	minimum = 0.0075 (at node 190)
	maximum = 0.021 (at node 103)
Injected flit rate average = 0.452581
	minimum = 0.2115 (at node 64)
	maximum = 0.696 (at node 150)
Accepted flit rate average= 0.240271
	minimum = 0.141 (at node 48)
	maximum = 0.365 (at node 83)
Injected packet length average = 18.0001
Accepted packet length average = 18.1944
Total in-flight flits = 224214 (148517 measured)
latency change    = 0.675284
throughput change = 0.00828059
Class 0:
Packet latency average = 907.262
	minimum = 23
	maximum = 2887
Network latency average = 873.869
	minimum = 23
	maximum = 2887
Slowest packet = 15794
Flit latency average = 1874.25
	minimum = 6
	maximum = 5788
Slowest flit = 16071
Fragmentation average = 219.983
	minimum = 0
	maximum = 2254
Injected packet rate average = 0.0247448
	minimum = 0.0116667 (at node 172)
	maximum = 0.0346667 (at node 150)
Accepted packet rate average = 0.013184
	minimum = 0.00833333 (at node 86)
	maximum = 0.0186667 (at node 119)
Injected flit rate average = 0.445309
	minimum = 0.209667 (at node 172)
	maximum = 0.624 (at node 150)
Accepted flit rate average= 0.238901
	minimum = 0.158333 (at node 64)
	maximum = 0.327333 (at node 119)
Injected packet length average = 17.9961
Accepted packet length average = 18.1205
Total in-flight flits = 261635 (209060 measured)
latency change    = 0.455239
throughput change = 0.00573372
Draining remaining packets ...
Class 0:
Remaining flits: 26714 26715 26716 26717 26718 26719 26720 26721 26722 26723 [...] (222608 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (187246 flits)
Class 0:
Remaining flits: 26727 26728 26729 34525 34526 34527 34528 34529 34530 34531 [...] (183997 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (159624 flits)
Class 0:
Remaining flits: 34525 34526 34527 34528 34529 34530 34531 34532 34533 34534 [...] (146180 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (128311 flits)
Class 0:
Remaining flits: 34530 34531 34532 34533 34534 34535 34536 34537 34538 34539 [...] (109261 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (95780 flits)
Class 0:
Remaining flits: 34530 34531 34532 34533 34534 34535 34536 34537 34538 34539 [...] (74326 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (64598 flits)
Class 0:
Remaining flits: 60182 60183 60184 60185 60186 60187 60188 60189 60190 60191 [...] (42361 flits)
Measured flits: 276624 276625 276626 276627 276628 276629 276630 276631 276632 276633 [...] (36754 flits)
Class 0:
Remaining flits: 73530 73531 73532 73533 73534 73535 73536 73537 73538 73539 [...] (14429 flits)
Measured flits: 278640 278641 278642 278643 278644 278645 278646 278647 278648 278649 [...] (12889 flits)
Class 0:
Remaining flits: 144474 144475 144476 144477 144478 144479 144480 144481 144482 144483 [...] (2206 flits)
Measured flits: 280139 280140 280141 280142 280143 280144 280145 280146 280147 280148 [...] (2034 flits)
Class 0:
Remaining flits: 243090 243091 243092 243093 243094 243095 243096 243097 243098 243099 [...] (291 flits)
Measured flits: 305135 380339 389757 389758 389759 389760 389761 389762 389763 389764 [...] (273 flits)
Time taken is 15295 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4606.83 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11747 (1 samples)
Network latency average = 4546.38 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11747 (1 samples)
Flit latency average = 4183.89 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12713 (1 samples)
Fragmentation average = 302.286 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6169 (1 samples)
Injected packet rate average = 0.0247448 (1 samples)
	minimum = 0.0116667 (1 samples)
	maximum = 0.0346667 (1 samples)
Accepted packet rate average = 0.013184 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.445309 (1 samples)
	minimum = 0.209667 (1 samples)
	maximum = 0.624 (1 samples)
Accepted flit rate average = 0.238901 (1 samples)
	minimum = 0.158333 (1 samples)
	maximum = 0.327333 (1 samples)
Injected packet size average = 17.9961 (1 samples)
Accepted packet size average = 18.1205 (1 samples)
Hops average = 5.14292 (1 samples)
Total run time 20.9404
