BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.26
	minimum = 23
	maximum = 790
Network latency average = 210.287
	minimum = 23
	maximum = 790
Slowest packet = 555
Flit latency average = 170.174
	minimum = 6
	maximum = 866
Slowest flit = 6948
Fragmentation average = 58.1749
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0158698
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00893229
	minimum = 0.003 (at node 41)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.282448
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.16876
	minimum = 0.063 (at node 174)
	maximum = 0.291 (at node 44)
Injected packet length average = 17.7978
Accepted packet length average = 18.8933
Total in-flight flits = 22462 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 376.483
	minimum = 23
	maximum = 1616
Network latency average = 373.318
	minimum = 23
	maximum = 1616
Slowest packet = 968
Flit latency average = 330.703
	minimum = 6
	maximum = 1676
Slowest flit = 17037
Fragmentation average = 65.0233
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0156094
	minimum = 0.009 (at node 91)
	maximum = 0.0225 (at node 68)
Accepted packet rate average = 0.00938542
	minimum = 0.0055 (at node 116)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.279844
	minimum = 0.162 (at node 91)
	maximum = 0.4045 (at node 68)
Accepted flit rate average= 0.172753
	minimum = 0.099 (at node 116)
	maximum = 0.27 (at node 44)
Injected packet length average = 17.9279
Accepted packet length average = 18.4065
Total in-flight flits = 41609 (0 measured)
latency change    = 0.433546
throughput change = 0.0231093
Class 0:
Packet latency average = 875.545
	minimum = 23
	maximum = 2353
Network latency average = 870.589
	minimum = 23
	maximum = 2309
Slowest packet = 2025
Flit latency average = 830.351
	minimum = 6
	maximum = 2347
Slowest flit = 34751
Fragmentation average = 66.1667
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0152604
	minimum = 0.002 (at node 136)
	maximum = 0.026 (at node 105)
Accepted packet rate average = 0.00978125
	minimum = 0.003 (at node 169)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.274807
	minimum = 0.023 (at node 136)
	maximum = 0.468 (at node 105)
Accepted flit rate average= 0.175422
	minimum = 0.051 (at node 169)
	maximum = 0.323 (at node 16)
Injected packet length average = 18.0078
Accepted packet length average = 17.9345
Total in-flight flits = 60740 (0 measured)
latency change    = 0.570002
throughput change = 0.0152163
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 269.311
	minimum = 23
	maximum = 1127
Network latency average = 240.547
	minimum = 23
	maximum = 945
Slowest packet = 8979
Flit latency average = 1221
	minimum = 6
	maximum = 3206
Slowest flit = 37637
Fragmentation average = 36.6047
	minimum = 0
	maximum = 133
Injected packet rate average = 0.014724
	minimum = 0 (at node 140)
	maximum = 0.026 (at node 161)
Accepted packet rate average = 0.00936979
	minimum = 0.003 (at node 74)
	maximum = 0.017 (at node 137)
Injected flit rate average = 0.264776
	minimum = 0 (at node 176)
	maximum = 0.468 (at node 161)
Accepted flit rate average= 0.168969
	minimum = 0.049 (at node 74)
	maximum = 0.292 (at node 5)
Injected packet length average = 17.9827
Accepted packet length average = 18.0334
Total in-flight flits = 79526 (45638 measured)
latency change    = 2.25106
throughput change = 0.0381912
Class 0:
Packet latency average = 758.016
	minimum = 23
	maximum = 2546
Network latency average = 713.379
	minimum = 23
	maximum = 1950
Slowest packet = 8979
Flit latency average = 1416.7
	minimum = 6
	maximum = 3711
Slowest flit = 63647
Fragmentation average = 52.5317
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0141172
	minimum = 0.0015 (at node 140)
	maximum = 0.022 (at node 190)
Accepted packet rate average = 0.00920573
	minimum = 0.004 (at node 79)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.25387
	minimum = 0.032 (at node 140)
	maximum = 0.39 (at node 190)
Accepted flit rate average= 0.166216
	minimum = 0.0665 (at node 79)
	maximum = 0.2725 (at node 152)
Injected packet length average = 17.983
Accepted packet length average = 18.0557
Total in-flight flits = 95211 (80141 measured)
latency change    = 0.644716
throughput change = 0.0165604
Class 0:
Packet latency average = 1267.39
	minimum = 23
	maximum = 3084
Network latency average = 1195.13
	minimum = 23
	maximum = 2956
Slowest packet = 8979
Flit latency average = 1584.05
	minimum = 6
	maximum = 4815
Slowest flit = 51623
Fragmentation average = 63.049
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0132899
	minimum = 0.00133333 (at node 96)
	maximum = 0.0203333 (at node 126)
Accepted packet rate average = 0.00909028
	minimum = 0.00433333 (at node 79)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.238884
	minimum = 0.0266667 (at node 140)
	maximum = 0.366 (at node 126)
Accepted flit rate average= 0.163816
	minimum = 0.0743333 (at node 79)
	maximum = 0.252 (at node 16)
Injected packet length average = 17.9748
Accepted packet length average = 18.021
Total in-flight flits = 105288 (98886 measured)
latency change    = 0.401909
throughput change = 0.0146516
Draining remaining packets ...
Class 0:
Remaining flits: 102390 102391 102392 102393 102394 102395 102396 102397 102398 102399 [...] (75631 flits)
Measured flits: 161406 161407 161408 161409 161410 161411 161412 161413 161414 161415 [...] (73430 flits)
Class 0:
Remaining flits: 124200 124201 124202 124203 124204 124205 124206 124207 124208 124209 [...] (45828 flits)
Measured flits: 161406 161407 161408 161409 161410 161411 161412 161413 161414 161415 [...] (45323 flits)
Class 0:
Remaining flits: 139248 139249 139250 139251 139252 139253 139254 139255 139256 139257 [...] (18407 flits)
Measured flits: 169086 169087 169088 169089 169090 169091 169694 169695 169696 169697 [...] (18317 flits)
Class 0:
Remaining flits: 210460 210461 210462 210463 210464 210465 210466 210467 210468 210469 [...] (581 flits)
Measured flits: 210460 210461 210462 210463 210464 210465 210466 210467 210468 210469 [...] (581 flits)
Time taken is 10201 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2760.07 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6788 (1 samples)
Network latency average = 2640.92 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6415 (1 samples)
Flit latency average = 2449.96 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6603 (1 samples)
Fragmentation average = 66.9734 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0132899 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0203333 (1 samples)
Accepted packet rate average = 0.00909028 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.238884 (1 samples)
	minimum = 0.0266667 (1 samples)
	maximum = 0.366 (1 samples)
Accepted flit rate average = 0.163816 (1 samples)
	minimum = 0.0743333 (1 samples)
	maximum = 0.252 (1 samples)
Injected packet size average = 17.9748 (1 samples)
Accepted packet size average = 18.021 (1 samples)
Hops average = 5.14811 (1 samples)
Total run time 8.80564
