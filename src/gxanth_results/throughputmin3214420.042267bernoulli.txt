BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 311.531
	minimum = 25
	maximum = 888
Network latency average = 295.137
	minimum = 22
	maximum = 830
Slowest packet = 726
Flit latency average = 260.967
	minimum = 5
	maximum = 844
Slowest flit = 18192
Fragmentation average = 76.0577
	minimum = 0
	maximum = 383
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0149063
	minimum = 0.007 (at node 135)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.288583
	minimum = 0.151 (at node 135)
	maximum = 0.462 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 19.3599
Total in-flight flits = 85964 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 572.411
	minimum = 22
	maximum = 1794
Network latency average = 551.624
	minimum = 22
	maximum = 1673
Slowest packet = 2065
Flit latency average = 510.297
	minimum = 5
	maximum = 1698
Slowest flit = 39712
Fragmentation average = 105.17
	minimum = 0
	maximum = 433
Injected packet rate average = 0.041526
	minimum = 0.0325 (at node 112)
	maximum = 0.0515 (at node 97)
Accepted packet rate average = 0.0158047
	minimum = 0.0085 (at node 135)
	maximum = 0.0225 (at node 70)
Injected flit rate average = 0.744372
	minimum = 0.585 (at node 112)
	maximum = 0.925 (at node 97)
Accepted flit rate average= 0.297318
	minimum = 0.1775 (at node 135)
	maximum = 0.418 (at node 152)
Injected packet length average = 17.9254
Accepted packet length average = 18.812
Total in-flight flits = 172858 (0 measured)
latency change    = 0.455757
throughput change = 0.0293772
Class 0:
Packet latency average = 1372.19
	minimum = 22
	maximum = 2588
Network latency average = 1342.6
	minimum = 22
	maximum = 2535
Slowest packet = 3208
Flit latency average = 1303.75
	minimum = 5
	maximum = 2518
Slowest flit = 57761
Fragmentation average = 152.066
	minimum = 0
	maximum = 483
Injected packet rate average = 0.0393906
	minimum = 0.018 (at node 108)
	maximum = 0.056 (at node 51)
Accepted packet rate average = 0.0165938
	minimum = 0.006 (at node 146)
	maximum = 0.027 (at node 99)
Injected flit rate average = 0.709229
	minimum = 0.326 (at node 108)
	maximum = 1 (at node 51)
Accepted flit rate average= 0.299391
	minimum = 0.121 (at node 4)
	maximum = 0.482 (at node 120)
Injected packet length average = 18.005
Accepted packet length average = 18.0424
Total in-flight flits = 251689 (0 measured)
latency change    = 0.582849
throughput change = 0.00692379
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 205.683
	minimum = 22
	maximum = 1135
Network latency average = 86.0098
	minimum = 22
	maximum = 913
Slowest packet = 23571
Flit latency average = 2004.87
	minimum = 5
	maximum = 3372
Slowest flit = 84203
Fragmentation average = 6.04575
	minimum = 0
	maximum = 31
Injected packet rate average = 0.035776
	minimum = 0.007 (at node 80)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0152865
	minimum = 0.007 (at node 144)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.644062
	minimum = 0.132 (at node 128)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.274917
	minimum = 0.145 (at node 80)
	maximum = 0.462 (at node 16)
Injected packet length average = 18.0026
Accepted packet length average = 17.9843
Total in-flight flits = 322619 (118198 measured)
latency change    = 5.67139
throughput change = 0.0890232
Class 0:
Packet latency average = 439.582
	minimum = 22
	maximum = 2065
Network latency average = 228.874
	minimum = 22
	maximum = 1695
Slowest packet = 23571
Flit latency average = 2320.62
	minimum = 5
	maximum = 4080
Slowest flit = 127636
Fragmentation average = 6.54371
	minimum = 0
	maximum = 31
Injected packet rate average = 0.0332839
	minimum = 0.004 (at node 80)
	maximum = 0.0495 (at node 185)
Accepted packet rate average = 0.015276
	minimum = 0.0085 (at node 132)
	maximum = 0.023 (at node 66)
Injected flit rate average = 0.59932
	minimum = 0.0745 (at node 80)
	maximum = 0.893 (at node 185)
Accepted flit rate average= 0.273711
	minimum = 0.1635 (at node 132)
	maximum = 0.401 (at node 128)
Injected packet length average = 18.0063
Accepted packet length average = 17.9177
Total in-flight flits = 376948 (219984 measured)
latency change    = 0.532094
throughput change = 0.00440512
Class 0:
Packet latency average = 731.569
	minimum = 22
	maximum = 2819
Network latency average = 437.006
	minimum = 22
	maximum = 2813
Slowest packet = 24236
Flit latency average = 2623.1
	minimum = 5
	maximum = 4729
Slowest flit = 162017
Fragmentation average = 6.97636
	minimum = 0
	maximum = 81
Injected packet rate average = 0.029375
	minimum = 0.00633333 (at node 92)
	maximum = 0.0463333 (at node 167)
Accepted packet rate average = 0.0153455
	minimum = 0.01 (at node 36)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.528627
	minimum = 0.114 (at node 92)
	maximum = 0.839667 (at node 167)
Accepted flit rate average= 0.273354
	minimum = 0.175 (at node 36)
	maximum = 0.417667 (at node 128)
Injected packet length average = 17.9958
Accepted packet length average = 17.8133
Total in-flight flits = 399355 (289825 measured)
latency change    = 0.399124
throughput change = 0.00130516
Class 0:
Packet latency average = 1430.21
	minimum = 22
	maximum = 4518
Network latency average = 1060.98
	minimum = 22
	maximum = 3943
Slowest packet = 23535
Flit latency average = 2926.29
	minimum = 5
	maximum = 5618
Slowest flit = 195411
Fragmentation average = 19.8172
	minimum = 0
	maximum = 377
Injected packet rate average = 0.0260833
	minimum = 0.00775 (at node 164)
	maximum = 0.04375 (at node 1)
Accepted packet rate average = 0.0152279
	minimum = 0.011 (at node 104)
	maximum = 0.02175 (at node 128)
Injected flit rate average = 0.469357
	minimum = 0.14275 (at node 164)
	maximum = 0.79075 (at node 1)
Accepted flit rate average= 0.272138
	minimum = 0.1995 (at node 104)
	maximum = 0.39475 (at node 128)
Injected packet length average = 17.9945
Accepted packet length average = 17.8711
Total in-flight flits = 403803 (337324 measured)
latency change    = 0.48849
throughput change = 0.00446886
Draining remaining packets ...
Class 0:
Remaining flits: 222333 222334 222335 237708 237709 237710 237711 237712 237713 237714 [...] (353921 flits)
Measured flits: 423342 423343 423344 423345 423346 423347 423348 423349 423350 423351 [...] (319819 flits)
Class 0:
Remaining flits: 264960 264961 264962 264963 264964 264965 264966 264967 264968 264969 [...] (304091 flits)
Measured flits: 423378 423379 423380 423381 423382 423383 423384 423385 423386 423387 [...] (287937 flits)
Class 0:
Remaining flits: 274518 274519 274520 274521 274522 274523 274524 274525 274526 274527 [...] (254533 flits)
Measured flits: 423378 423379 423380 423381 423382 423383 423384 423385 423386 423387 [...] (248428 flits)
Class 0:
Remaining flits: 275328 275329 275330 275331 275332 275333 275334 275335 275336 275337 [...] (205743 flits)
Measured flits: 423378 423379 423380 423381 423382 423383 423384 423385 423386 423387 [...] (203874 flits)
Class 0:
Remaining flits: 303066 303067 303068 303069 303070 303071 303072 303073 303074 303075 [...] (158228 flits)
Measured flits: 423384 423385 423386 423387 423388 423389 423390 423391 423392 423393 [...] (157717 flits)
Class 0:
Remaining flits: 394614 394615 394616 394617 394618 394619 394620 394621 394622 394623 [...] (110920 flits)
Measured flits: 425610 425611 425612 425613 425614 425615 425616 425617 425618 425619 [...] (110848 flits)
Class 0:
Remaining flits: 406476 406477 406478 406479 406480 406481 406482 406483 406484 406485 [...] (63766 flits)
Measured flits: 437566 437567 437568 437569 437570 437571 437572 437573 437574 437575 [...] (63748 flits)
Class 0:
Remaining flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (17615 flits)
Measured flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (17615 flits)
Class 0:
Remaining flits: 582426 582427 582428 582429 582430 582431 582432 582433 582434 582435 [...] (210 flits)
Measured flits: 582426 582427 582428 582429 582430 582431 582432 582433 582434 582435 [...] (210 flits)
Time taken is 16215 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7017.15 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12069 (1 samples)
Network latency average = 6624.82 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12047 (1 samples)
Flit latency average = 5450.63 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12030 (1 samples)
Fragmentation average = 61.3635 (1 samples)
	minimum = 0 (1 samples)
	maximum = 823 (1 samples)
Injected packet rate average = 0.0260833 (1 samples)
	minimum = 0.00775 (1 samples)
	maximum = 0.04375 (1 samples)
Accepted packet rate average = 0.0152279 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.02175 (1 samples)
Injected flit rate average = 0.469357 (1 samples)
	minimum = 0.14275 (1 samples)
	maximum = 0.79075 (1 samples)
Accepted flit rate average = 0.272138 (1 samples)
	minimum = 0.1995 (1 samples)
	maximum = 0.39475 (1 samples)
Injected packet size average = 17.9945 (1 samples)
Accepted packet size average = 17.8711 (1 samples)
Hops average = 5.17311 (1 samples)
Total run time 18.9621
