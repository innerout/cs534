BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.702
	minimum = 23
	maximum = 983
Network latency average = 274.59
	minimum = 23
	maximum = 948
Slowest packet = 244
Flit latency average = 202.605
	minimum = 6
	maximum = 954
Slowest flit = 4365
Fragmentation average = 163.076
	minimum = 0
	maximum = 812
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.00997396
	minimum = 0.004 (at node 23)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.214135
	minimum = 0.089 (at node 68)
	maximum = 0.369 (at node 152)
Injected packet length average = 17.8118
Accepted packet length average = 21.4695
Total in-flight flits = 59326 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 629.491
	minimum = 23
	maximum = 1985
Network latency average = 487.545
	minimum = 23
	maximum = 1869
Slowest packet = 114
Flit latency average = 396.389
	minimum = 6
	maximum = 1930
Slowest flit = 2842
Fragmentation average = 230.822
	minimum = 0
	maximum = 1508
Injected packet rate average = 0.0302422
	minimum = 0.0005 (at node 125)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0116198
	minimum = 0.006 (at node 96)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.541753
	minimum = 0.009 (at node 125)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.231432
	minimum = 0.1345 (at node 135)
	maximum = 0.364 (at node 34)
Injected packet length average = 17.9138
Accepted packet length average = 19.9171
Total in-flight flits = 120164 (0 measured)
latency change    = 0.40793
throughput change = 0.0747384
Class 0:
Packet latency average = 1278.15
	minimum = 29
	maximum = 2917
Network latency average = 1064.78
	minimum = 23
	maximum = 2679
Slowest packet = 1939
Flit latency average = 969.163
	minimum = 6
	maximum = 2877
Slowest flit = 9796
Fragmentation average = 336.066
	minimum = 0
	maximum = 1830
Injected packet rate average = 0.0306875
	minimum = 0 (at node 55)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0134792
	minimum = 0.003 (at node 163)
	maximum = 0.022 (at node 22)
Injected flit rate average = 0.552214
	minimum = 0 (at node 56)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.245464
	minimum = 0.092 (at node 163)
	maximum = 0.403 (at node 22)
Injected packet length average = 17.9947
Accepted packet length average = 18.2106
Total in-flight flits = 179091 (0 measured)
latency change    = 0.5075
throughput change = 0.0571623
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 435.772
	minimum = 25
	maximum = 2707
Network latency average = 150.416
	minimum = 25
	maximum = 884
Slowest packet = 17538
Flit latency average = 1473
	minimum = 6
	maximum = 3800
Slowest flit = 15583
Fragmentation average = 71.3518
	minimum = 1
	maximum = 740
Injected packet rate average = 0.0308802
	minimum = 0 (at node 90)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0132969
	minimum = 0.005 (at node 18)
	maximum = 0.022 (at node 103)
Injected flit rate average = 0.555599
	minimum = 0 (at node 90)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.243188
	minimum = 0.113 (at node 18)
	maximum = 0.421 (at node 103)
Injected packet length average = 17.9921
Accepted packet length average = 18.2891
Total in-flight flits = 239121 (97550 measured)
latency change    = 1.93308
throughput change = 0.00935921
Class 0:
Packet latency average = 731.112
	minimum = 25
	maximum = 3271
Network latency average = 453.043
	minimum = 25
	maximum = 1965
Slowest packet = 17538
Flit latency average = 1751.49
	minimum = 6
	maximum = 4822
Slowest flit = 14588
Fragmentation average = 144.599
	minimum = 1
	maximum = 1620
Injected packet rate average = 0.0290365
	minimum = 0.003 (at node 5)
	maximum = 0.056 (at node 87)
Accepted packet rate average = 0.0133646
	minimum = 0.0065 (at node 100)
	maximum = 0.0215 (at node 103)
Injected flit rate average = 0.522674
	minimum = 0.054 (at node 5)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.243466
	minimum = 0.1045 (at node 100)
	maximum = 0.381 (at node 181)
Injected packet length average = 18.0006
Accepted packet length average = 18.2173
Total in-flight flits = 286300 (176389 measured)
latency change    = 0.40396
throughput change = 0.0011445
Class 0:
Packet latency average = 1115.73
	minimum = 25
	maximum = 4406
Network latency average = 815.369
	minimum = 25
	maximum = 2975
Slowest packet = 17538
Flit latency average = 2052.82
	minimum = 6
	maximum = 5639
Slowest flit = 30214
Fragmentation average = 192.053
	minimum = 1
	maximum = 1832
Injected packet rate average = 0.0278576
	minimum = 0.009 (at node 53)
	maximum = 0.054 (at node 109)
Accepted packet rate average = 0.013283
	minimum = 0.00833333 (at node 18)
	maximum = 0.018 (at node 87)
Injected flit rate average = 0.501481
	minimum = 0.163333 (at node 53)
	maximum = 0.972 (at node 109)
Accepted flit rate average= 0.240946
	minimum = 0.148667 (at node 110)
	maximum = 0.332333 (at node 128)
Injected packet length average = 18.0016
Accepted packet length average = 18.1395
Total in-flight flits = 329134 (246571 measured)
latency change    = 0.344726
throughput change = 0.0104586
Draining remaining packets ...
Class 0:
Remaining flits: 11970 11971 11972 11973 11974 11975 11976 11977 11978 11979 [...] (288371 flits)
Measured flits: 315126 315127 315128 315129 315130 315131 315132 315133 315134 315135 [...] (227983 flits)
Class 0:
Remaining flits: 11971 11972 11973 11974 11975 11976 11977 11978 11979 11980 [...] (249116 flits)
Measured flits: 315127 315128 315129 315130 315131 315132 315133 315134 315135 315136 [...] (206096 flits)
Class 0:
Remaining flits: 11971 11972 11973 11974 11975 11976 11977 11978 11979 11980 [...] (210474 flits)
Measured flits: 315216 315217 315218 315219 315220 315221 315222 315223 315224 315225 [...] (179299 flits)
Class 0:
Remaining flits: 11984 11985 11986 11987 24066 24067 24068 24069 24070 24071 [...] (172791 flits)
Measured flits: 315216 315217 315218 315219 315220 315221 315222 315223 315224 315225 [...] (149582 flits)
Class 0:
Remaining flits: 32076 32077 32078 32079 32080 32081 32082 32083 32084 32085 [...] (136515 flits)
Measured flits: 315218 315219 315220 315221 315222 315223 315224 315225 315226 315227 [...] (118967 flits)
Class 0:
Remaining flits: 32078 32079 32080 32081 32082 32083 32084 32085 32086 32087 [...] (102265 flits)
Measured flits: 315270 315271 315272 315273 315274 315275 315276 315277 315278 315279 [...] (89127 flits)
Class 0:
Remaining flits: 32078 32079 32080 32081 32082 32083 32084 32085 32086 32087 [...] (72110 flits)
Measured flits: 315270 315271 315272 315273 315274 315275 315276 315277 315278 315279 [...] (62699 flits)
Class 0:
Remaining flits: 32083 32084 32085 32086 32087 32088 32089 32090 32091 32092 [...] (45014 flits)
Measured flits: 315270 315271 315272 315273 315274 315275 315276 315277 315278 315279 [...] (38529 flits)
Class 0:
Remaining flits: 66510 66511 66512 66513 66514 66515 66516 66517 66518 66519 [...] (21285 flits)
Measured flits: 316278 316279 316280 316281 316282 316283 316284 316285 316286 316287 [...] (18001 flits)
Class 0:
Remaining flits: 107370 107371 107372 107373 107374 107375 107376 107377 107378 107379 [...] (5821 flits)
Measured flits: 318582 318583 318584 318585 318586 318587 318588 318589 318590 318591 [...] (5130 flits)
Class 0:
Remaining flits: 121860 121861 121862 121863 121864 121865 121866 121867 121868 121869 [...] (1151 flits)
Measured flits: 324504 324505 324506 324507 324508 324509 324510 324511 324512 324513 [...] (1017 flits)
Class 0:
Remaining flits: 491638 491639 491640 491641 491642 491643 491644 491645 491646 491647 [...] (51 flits)
Measured flits: 491638 491639 491640 491641 491642 491643 491644 491645 491646 491647 [...] (51 flits)
Time taken is 18052 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6156.56 (1 samples)
	minimum = 25 (1 samples)
	maximum = 15486 (1 samples)
Network latency average = 5752.74 (1 samples)
	minimum = 25 (1 samples)
	maximum = 14643 (1 samples)
Flit latency average = 5189.48 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16347 (1 samples)
Fragmentation average = 286.65 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7760 (1 samples)
Injected packet rate average = 0.0278576 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.054 (1 samples)
Accepted packet rate average = 0.013283 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.501481 (1 samples)
	minimum = 0.163333 (1 samples)
	maximum = 0.972 (1 samples)
Accepted flit rate average = 0.240946 (1 samples)
	minimum = 0.148667 (1 samples)
	maximum = 0.332333 (1 samples)
Injected packet size average = 18.0016 (1 samples)
Accepted packet size average = 18.1395 (1 samples)
Hops average = 5.15699 (1 samples)
Total run time 25.1565
