BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 276.347
	minimum = 22
	maximum = 789
Network latency average = 267.577
	minimum = 22
	maximum = 756
Slowest packet = 1205
Flit latency average = 237.327
	minimum = 5
	maximum = 739
Slowest flit = 21707
Fragmentation average = 51.9427
	minimum = 0
	maximum = 440
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0141823
	minimum = 0.006 (at node 127)
	maximum = 0.023 (at node 131)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.269677
	minimum = 0.122 (at node 174)
	maximum = 0.446 (at node 44)
Injected packet length average = 17.8483
Accepted packet length average = 19.0151
Total in-flight flits = 54674 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 511.969
	minimum = 22
	maximum = 1524
Network latency average = 502.098
	minimum = 22
	maximum = 1456
Slowest packet = 2701
Flit latency average = 464.679
	minimum = 5
	maximum = 1631
Slowest flit = 38144
Fragmentation average = 74.5465
	minimum = 0
	maximum = 742
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.014901
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.276885
	minimum = 0.15 (at node 153)
	maximum = 0.3845 (at node 152)
Injected packet length average = 17.9239
Accepted packet length average = 18.5816
Total in-flight flits = 108254 (0 measured)
latency change    = 0.460228
throughput change = 0.0260336
Class 0:
Packet latency average = 1169.68
	minimum = 22
	maximum = 2368
Network latency average = 1158.61
	minimum = 22
	maximum = 2315
Slowest packet = 3942
Flit latency average = 1121.86
	minimum = 5
	maximum = 2298
Slowest flit = 70973
Fragmentation average = 109.766
	minimum = 0
	maximum = 730
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0158646
	minimum = 0.005 (at node 96)
	maximum = 0.026 (at node 159)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.289333
	minimum = 0.112 (at node 96)
	maximum = 0.476 (at node 159)
Injected packet length average = 18.0012
Accepted packet length average = 18.2377
Total in-flight flits = 159622 (0 measured)
latency change    = 0.562299
throughput change = 0.0430228
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.2915
	minimum = 22
	maximum = 114
Network latency average = 35.083
	minimum = 22
	maximum = 87
Slowest packet = 17866
Flit latency average = 1571.98
	minimum = 5
	maximum = 3102
Slowest flit = 86597
Fragmentation average = 6.33404
	minimum = 0
	maximum = 47
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0159323
	minimum = 0.005 (at node 134)
	maximum = 0.025 (at node 103)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.289818
	minimum = 0.098 (at node 134)
	maximum = 0.478 (at node 123)
Injected packet length average = 17.9869
Accepted packet length average = 18.1906
Total in-flight flits = 213957 (101407 measured)
latency change    = 24.8256
throughput change = 0.00167131
Class 0:
Packet latency average = 45.1776
	minimum = 22
	maximum = 119
Network latency average = 35.1423
	minimum = 22
	maximum = 87
Slowest packet = 17866
Flit latency average = 1789.62
	minimum = 5
	maximum = 3709
Slowest flit = 133694
Fragmentation average = 6.42056
	minimum = 0
	maximum = 47
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.0159219
	minimum = 0.0095 (at node 4)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.289792
	minimum = 0.1835 (at node 5)
	maximum = 0.418 (at node 129)
Injected packet length average = 17.9923
Accepted packet length average = 18.2009
Total in-flight flits = 264990 (199194 measured)
latency change    = 0.00252159
throughput change = 8.98634e-05
Class 0:
Packet latency average = 188.044
	minimum = 22
	maximum = 2999
Network latency average = 178.099
	minimum = 22
	maximum = 2986
Slowest packet = 17932
Flit latency average = 2009.13
	minimum = 5
	maximum = 4508
Slowest flit = 135611
Fragmentation average = 10.2789
	minimum = 0
	maximum = 244
Injected packet rate average = 0.0313576
	minimum = 0.024 (at node 15)
	maximum = 0.0396667 (at node 67)
Accepted packet rate average = 0.0159653
	minimum = 0.011 (at node 135)
	maximum = 0.022 (at node 157)
Injected flit rate average = 0.564382
	minimum = 0.435667 (at node 15)
	maximum = 0.715667 (at node 67)
Accepted flit rate average= 0.289748
	minimum = 0.190667 (at node 135)
	maximum = 0.397 (at node 157)
Injected packet length average = 17.9982
Accepted packet length average = 18.1487
Total in-flight flits = 317843 (296806 measured)
latency change    = 0.75975
throughput change = 0.000149795
Draining remaining packets ...
Class 0:
Remaining flits: 201658 201659 201660 201661 201662 201663 201664 201665 201666 201667 [...] (270321 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (267249 flits)
Class 0:
Remaining flits: 241405 241406 241407 241408 241409 241410 241411 241412 241413 241414 [...] (222586 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (222074 flits)
Class 0:
Remaining flits: 266633 298878 298879 298880 298881 298882 298883 298884 298885 298886 [...] (175343 flits)
Measured flits: 321532 321533 326034 326035 326036 326037 326038 326039 326040 326041 [...] (175277 flits)
Class 0:
Remaining flits: 349686 349687 349688 349689 349690 349691 349692 349693 349694 349695 [...] (128263 flits)
Measured flits: 349686 349687 349688 349689 349690 349691 349692 349693 349694 349695 [...] (128263 flits)
Class 0:
Remaining flits: 414528 414529 414530 414531 414532 414533 414534 414535 414536 414537 [...] (81462 flits)
Measured flits: 414528 414529 414530 414531 414532 414533 414534 414535 414536 414537 [...] (81462 flits)
Class 0:
Remaining flits: 420480 420481 420482 420483 420484 420485 420486 420487 420488 420489 [...] (34563 flits)
Measured flits: 420480 420481 420482 420483 420484 420485 420486 420487 420488 420489 [...] (34563 flits)
Class 0:
Remaining flits: 509706 509707 509708 509709 509710 509711 509712 509713 509714 509715 [...] (2395 flits)
Measured flits: 509706 509707 509708 509709 509710 509711 509712 509713 509714 509715 [...] (2395 flits)
Time taken is 13625 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4780.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9083 (1 samples)
Network latency average = 4769.56 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9032 (1 samples)
Flit latency average = 3947.32 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9015 (1 samples)
Fragmentation average = 245.439 (1 samples)
	minimum = 0 (1 samples)
	maximum = 845 (1 samples)
Injected packet rate average = 0.0313576 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.0396667 (1 samples)
Accepted packet rate average = 0.0159653 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.564382 (1 samples)
	minimum = 0.435667 (1 samples)
	maximum = 0.715667 (1 samples)
Accepted flit rate average = 0.289748 (1 samples)
	minimum = 0.190667 (1 samples)
	maximum = 0.397 (1 samples)
Injected packet size average = 17.9982 (1 samples)
Accepted packet size average = 18.1487 (1 samples)
Hops average = 5.07231 (1 samples)
Total run time 14.2128
