BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 339.891
	minimum = 22
	maximum = 973
Network latency average = 229.354
	minimum = 22
	maximum = 735
Slowest packet = 82
Flit latency average = 207.081
	minimum = 5
	maximum = 719
Slowest flit = 23408
Fragmentation average = 18.8167
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0139479
	minimum = 0.006 (at node 41)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.257375
	minimum = 0.109 (at node 41)
	maximum = 0.431 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.4526
Total in-flight flits = 51024 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 601.865
	minimum = 22
	maximum = 1928
Network latency average = 442.397
	minimum = 22
	maximum = 1455
Slowest packet = 82
Flit latency average = 420.185
	minimum = 5
	maximum = 1494
Slowest flit = 45702
Fragmentation average = 20.3051
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0150208
	minimum = 0.008 (at node 83)
	maximum = 0.0215 (at node 22)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.273284
	minimum = 0.146 (at node 83)
	maximum = 0.387 (at node 22)
Injected packet length average = 17.9205
Accepted packet length average = 18.1937
Total in-flight flits = 105803 (0 measured)
latency change    = 0.43527
throughput change = 0.0582137
Class 0:
Packet latency average = 1326.73
	minimum = 22
	maximum = 2771
Network latency average = 1077.81
	minimum = 22
	maximum = 2140
Slowest packet = 4570
Flit latency average = 1057.89
	minimum = 5
	maximum = 2199
Slowest flit = 75526
Fragmentation average = 22.7147
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0320521
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0161719
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.576651
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.29176
	minimum = 0.126 (at node 61)
	maximum = 0.522 (at node 16)
Injected packet length average = 17.9911
Accepted packet length average = 18.0412
Total in-flight flits = 160557 (0 measured)
latency change    = 0.546356
throughput change = 0.0633279
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 369.968
	minimum = 22
	maximum = 1200
Network latency average = 41.483
	minimum = 22
	maximum = 430
Slowest packet = 17864
Flit latency average = 1495.25
	minimum = 5
	maximum = 2862
Slowest flit = 116571
Fragmentation average = 5.20943
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0354375
	minimum = 0 (at node 55)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.016401
	minimum = 0.007 (at node 36)
	maximum = 0.028 (at node 151)
Injected flit rate average = 0.637641
	minimum = 0 (at node 55)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.294964
	minimum = 0.139 (at node 36)
	maximum = 0.517 (at node 151)
Injected packet length average = 17.9934
Accepted packet length average = 17.9844
Total in-flight flits = 226396 (112796 measured)
latency change    = 2.58608
throughput change = 0.0108594
Class 0:
Packet latency average = 439.819
	minimum = 22
	maximum = 2526
Network latency average = 109.101
	minimum = 22
	maximum = 1971
Slowest packet = 18181
Flit latency average = 1708.09
	minimum = 5
	maximum = 3550
Slowest flit = 146825
Fragmentation average = 5.58504
	minimum = 0
	maximum = 65
Injected packet rate average = 0.0344141
	minimum = 0.005 (at node 17)
	maximum = 0.056 (at node 187)
Accepted packet rate average = 0.0164141
	minimum = 0.01 (at node 4)
	maximum = 0.0235 (at node 128)
Injected flit rate average = 0.619701
	minimum = 0.094 (at node 17)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.295521
	minimum = 0.176 (at node 4)
	maximum = 0.423 (at node 128)
Injected packet length average = 18.0072
Accepted packet length average = 18.0041
Total in-flight flits = 284947 (217508 measured)
latency change    = 0.158818
throughput change = 0.00188579
Class 0:
Packet latency average = 1016.02
	minimum = 22
	maximum = 3600
Network latency average = 714.311
	minimum = 22
	maximum = 2982
Slowest packet = 18181
Flit latency average = 1931.71
	minimum = 5
	maximum = 4293
Slowest flit = 178412
Fragmentation average = 10.3551
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0337708
	minimum = 0.0113333 (at node 20)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0163906
	minimum = 0.0106667 (at node 52)
	maximum = 0.022 (at node 95)
Injected flit rate average = 0.608189
	minimum = 0.204 (at node 20)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.295036
	minimum = 0.193667 (at node 52)
	maximum = 0.393333 (at node 95)
Injected packet length average = 18.0093
Accepted packet length average = 18.0003
Total in-flight flits = 340752 (309268 measured)
latency change    = 0.567116
throughput change = 0.00164175
Draining remaining packets ...
Class 0:
Remaining flits: 208764 208765 208766 208767 208768 208769 208770 208771 208772 208773 [...] (292832 flits)
Measured flits: 321696 321697 321698 321699 321700 321701 321702 321703 321704 321705 [...] (280111 flits)
Class 0:
Remaining flits: 229122 229123 229124 229125 229126 229127 229128 229129 229130 229131 [...] (245502 flits)
Measured flits: 322110 322111 322112 322113 322114 322115 322116 322117 322118 322119 [...] (241041 flits)
Class 0:
Remaining flits: 260916 260917 260918 260919 260920 260921 260922 260923 260924 260925 [...] (198263 flits)
Measured flits: 322146 322147 322148 322149 322150 322151 322152 322153 322154 322155 [...] (197127 flits)
Class 0:
Remaining flits: 309222 309223 309224 309225 309226 309227 309228 309229 309230 309231 [...] (150970 flits)
Measured flits: 322146 322147 322148 322149 322150 322151 322152 322153 322154 322155 [...] (150808 flits)
Class 0:
Remaining flits: 338238 338239 338240 338241 338242 338243 338244 338245 338246 338247 [...] (104526 flits)
Measured flits: 338238 338239 338240 338241 338242 338243 338244 338245 338246 338247 [...] (104526 flits)
Class 0:
Remaining flits: 358344 358345 358346 358347 358348 358349 358350 358351 358352 358353 [...] (61611 flits)
Measured flits: 358344 358345 358346 358347 358348 358349 358350 358351 358352 358353 [...] (61611 flits)
Class 0:
Remaining flits: 411912 411913 411914 411915 411916 411917 411918 411919 411920 411921 [...] (29952 flits)
Measured flits: 411912 411913 411914 411915 411916 411917 411918 411919 411920 411921 [...] (29952 flits)
Class 0:
Remaining flits: 473004 473005 473006 473007 473008 473009 473010 473011 473012 473013 [...] (10564 flits)
Measured flits: 473004 473005 473006 473007 473008 473009 473010 473011 473012 473013 [...] (10564 flits)
Class 0:
Remaining flits: 526428 526429 526430 526431 526432 526433 526434 526435 526436 526437 [...] (2593 flits)
Measured flits: 526428 526429 526430 526431 526432 526433 526434 526435 526436 526437 [...] (2593 flits)
Time taken is 15949 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5278.66 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11392 (1 samples)
Network latency average = 4934.75 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10493 (1 samples)
Flit latency average = 4216.75 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10476 (1 samples)
Fragmentation average = 23.5016 (1 samples)
	minimum = 0 (1 samples)
	maximum = 106 (1 samples)
Injected packet rate average = 0.0337708 (1 samples)
	minimum = 0.0113333 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0163906 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.608189 (1 samples)
	minimum = 0.204 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.295036 (1 samples)
	minimum = 0.193667 (1 samples)
	maximum = 0.393333 (1 samples)
Injected packet size average = 18.0093 (1 samples)
Accepted packet size average = 18.0003 (1 samples)
Hops average = 5.07398 (1 samples)
Total run time 16.8972
