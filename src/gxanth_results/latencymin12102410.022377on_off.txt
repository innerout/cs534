BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.259
	minimum = 23
	maximum = 990
Network latency average = 239.514
	minimum = 23
	maximum = 890
Slowest packet = 7
Flit latency average = 203.065
	minimum = 6
	maximum = 873
Slowest flit = 8837
Fragmentation average = 53.2804
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00960417
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.180187
	minimum = 0.054 (at node 174)
	maximum = 0.323 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.7614
Total in-flight flits = 38178 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 519.3
	minimum = 23
	maximum = 1927
Network latency average = 430.826
	minimum = 23
	maximum = 1745
Slowest packet = 673
Flit latency average = 392.676
	minimum = 6
	maximum = 1783
Slowest flit = 14574
Fragmentation average = 59.3185
	minimum = 0
	maximum = 156
Injected packet rate average = 0.021651
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0101224
	minimum = 0.0055 (at node 96)
	maximum = 0.0165 (at node 44)
Injected flit rate average = 0.388193
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.185984
	minimum = 0.102 (at node 96)
	maximum = 0.299 (at node 44)
Injected packet length average = 17.9295
Accepted packet length average = 18.3736
Total in-flight flits = 78234 (0 measured)
latency change    = 0.41795
throughput change = 0.0311686
Class 0:
Packet latency average = 1172.9
	minimum = 27
	maximum = 2719
Network latency average = 1047.71
	minimum = 23
	maximum = 2605
Slowest packet = 1113
Flit latency average = 1019.03
	minimum = 6
	maximum = 2778
Slowest flit = 15376
Fragmentation average = 66.3241
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0221823
	minimum = 0 (at node 3)
	maximum = 0.056 (at node 73)
Accepted packet rate average = 0.0103333
	minimum = 0.003 (at node 94)
	maximum = 0.02 (at node 99)
Injected flit rate average = 0.398922
	minimum = 0 (at node 3)
	maximum = 1 (at node 73)
Accepted flit rate average= 0.186521
	minimum = 0.054 (at node 153)
	maximum = 0.371 (at node 99)
Injected packet length average = 17.9838
Accepted packet length average = 18.0504
Total in-flight flits = 119084 (0 measured)
latency change    = 0.557252
throughput change = 0.00287613
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 246.583
	minimum = 23
	maximum = 1120
Network latency average = 120.762
	minimum = 23
	maximum = 937
Slowest packet = 12573
Flit latency average = 1466.31
	minimum = 6
	maximum = 3409
Slowest flit = 37655
Fragmentation average = 18.2265
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0204219
	minimum = 0 (at node 24)
	maximum = 0.055 (at node 169)
Accepted packet rate average = 0.0104844
	minimum = 0.003 (at node 180)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.368016
	minimum = 0 (at node 24)
	maximum = 1 (at node 169)
Accepted flit rate average= 0.187281
	minimum = 0.059 (at node 147)
	maximum = 0.361 (at node 16)
Injected packet length average = 18.0207
Accepted packet length average = 17.8629
Total in-flight flits = 153704 (63912 measured)
latency change    = 3.75663
throughput change = 0.00406029
Class 0:
Packet latency average = 480.872
	minimum = 23
	maximum = 2092
Network latency average = 365.772
	minimum = 23
	maximum = 1914
Slowest packet = 12923
Flit latency average = 1715.32
	minimum = 6
	maximum = 4258
Slowest flit = 42911
Fragmentation average = 31.9874
	minimum = 0
	maximum = 127
Injected packet rate average = 0.020875
	minimum = 0.0015 (at node 64)
	maximum = 0.052 (at node 169)
Accepted packet rate average = 0.0101901
	minimum = 0.005 (at node 180)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.375513
	minimum = 0.027 (at node 64)
	maximum = 0.938 (at node 169)
Accepted flit rate average= 0.183904
	minimum = 0.0985 (at node 180)
	maximum = 0.3165 (at node 16)
Injected packet length average = 17.9886
Accepted packet length average = 18.0473
Total in-flight flits = 192753 (128180 measured)
latency change    = 0.487218
throughput change = 0.0183662
Class 0:
Packet latency average = 866.52
	minimum = 23
	maximum = 3087
Network latency average = 753.45
	minimum = 23
	maximum = 2920
Slowest packet = 12923
Flit latency average = 1946.62
	minimum = 6
	maximum = 5417
Slowest flit = 39438
Fragmentation average = 41.2525
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0213733
	minimum = 0.00366667 (at node 163)
	maximum = 0.0473333 (at node 19)
Accepted packet rate average = 0.0102639
	minimum = 0.00633333 (at node 64)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.384833
	minimum = 0.066 (at node 163)
	maximum = 0.852667 (at node 19)
Accepted flit rate average= 0.184549
	minimum = 0.109 (at node 64)
	maximum = 0.282 (at node 13)
Injected packet length average = 18.0054
Accepted packet length average = 17.9804
Total in-flight flits = 234382 (190449 measured)
latency change    = 0.445053
throughput change = 0.00349483
Class 0:
Packet latency average = 1325.16
	minimum = 23
	maximum = 4198
Network latency average = 1209.63
	minimum = 23
	maximum = 3939
Slowest packet = 12923
Flit latency average = 2177.74
	minimum = 6
	maximum = 6361
Slowest flit = 36719
Fragmentation average = 47.5304
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0213112
	minimum = 0.00625 (at node 59)
	maximum = 0.03925 (at node 19)
Accepted packet rate average = 0.0102747
	minimum = 0.007 (at node 121)
	maximum = 0.01425 (at node 151)
Injected flit rate average = 0.383283
	minimum = 0.1125 (at node 59)
	maximum = 0.7065 (at node 19)
Accepted flit rate average= 0.184932
	minimum = 0.12675 (at node 64)
	maximum = 0.25575 (at node 151)
Injected packet length average = 17.985
Accepted packet length average = 17.9987
Total in-flight flits = 271662 (242989 measured)
latency change    = 0.346103
throughput change = 0.00207471
Class 0:
Packet latency average = 1800.36
	minimum = 23
	maximum = 5552
Network latency average = 1678.76
	minimum = 23
	maximum = 4921
Slowest packet = 12923
Flit latency average = 2424.26
	minimum = 6
	maximum = 7123
Slowest flit = 50993
Fragmentation average = 52.2566
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0212573
	minimum = 0.0076 (at node 59)
	maximum = 0.0392 (at node 35)
Accepted packet rate average = 0.0102521
	minimum = 0.0066 (at node 132)
	maximum = 0.014 (at node 151)
Injected flit rate average = 0.382395
	minimum = 0.1356 (at node 59)
	maximum = 0.7076 (at node 35)
Accepted flit rate average= 0.184593
	minimum = 0.1212 (at node 132)
	maximum = 0.2504 (at node 151)
Injected packet length average = 17.9889
Accepted packet length average = 18.0054
Total in-flight flits = 309219 (291396 measured)
latency change    = 0.263944
throughput change = 0.00183964
Class 0:
Packet latency average = 2277.15
	minimum = 23
	maximum = 6560
Network latency average = 2146.8
	minimum = 23
	maximum = 5921
Slowest packet = 12923
Flit latency average = 2667.64
	minimum = 6
	maximum = 8124
Slowest flit = 62018
Fragmentation average = 56.5172
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0214141
	minimum = 0.0085 (at node 65)
	maximum = 0.036 (at node 23)
Accepted packet rate average = 0.0102014
	minimum = 0.00716667 (at node 49)
	maximum = 0.0133333 (at node 165)
Injected flit rate average = 0.385386
	minimum = 0.153 (at node 65)
	maximum = 0.645167 (at node 23)
Accepted flit rate average= 0.183785
	minimum = 0.1255 (at node 49)
	maximum = 0.237 (at node 165)
Injected packet length average = 17.9969
Accepted packet length average = 18.0157
Total in-flight flits = 351406 (340369 measured)
latency change    = 0.209381
throughput change = 0.00439637
Class 0:
Packet latency average = 2708.57
	minimum = 23
	maximum = 7270
Network latency average = 2571.28
	minimum = 23
	maximum = 6985
Slowest packet = 12923
Flit latency average = 2903.75
	minimum = 6
	maximum = 8560
Slowest flit = 100187
Fragmentation average = 59.5269
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0213884
	minimum = 0.009 (at node 65)
	maximum = 0.0364286 (at node 135)
Accepted packet rate average = 0.0101429
	minimum = 0.00742857 (at node 80)
	maximum = 0.0127143 (at node 13)
Injected flit rate average = 0.384906
	minimum = 0.162 (at node 65)
	maximum = 0.656714 (at node 135)
Accepted flit rate average= 0.182597
	minimum = 0.135 (at node 49)
	maximum = 0.228857 (at node 13)
Injected packet length average = 17.996
Accepted packet length average = 18.0026
Total in-flight flits = 391119 (384192 measured)
latency change    = 0.159281
throughput change = 0.00650202
Draining all recorded packets ...
Class 0:
Remaining flits: 118854 118855 118856 118857 118858 118859 118860 118861 118862 118863 [...] (428756 flits)
Measured flits: 226350 226351 226352 226353 226354 226355 226356 226357 226358 226359 [...] (371416 flits)
Class 0:
Remaining flits: 122310 122311 122312 122313 122314 122315 122316 122317 122318 122319 [...] (470268 flits)
Measured flits: 226350 226351 226352 226353 226354 226355 226356 226357 226358 226359 [...] (347693 flits)
Class 0:
Remaining flits: 136332 136333 136334 136335 136336 136337 136338 136339 136340 136341 [...] (510159 flits)
Measured flits: 226350 226351 226352 226353 226354 226355 226356 226357 226358 226359 [...] (322095 flits)
Class 0:
Remaining flits: 143388 143389 143390 143391 143392 143393 143394 143395 143396 143397 [...] (549836 flits)
Measured flits: 228132 228133 228134 228135 228136 228137 228138 228139 228140 228141 [...] (296236 flits)
Class 0:
Remaining flits: 191142 191143 191144 191145 191146 191147 191148 191149 191150 191151 [...] (585717 flits)
Measured flits: 228132 228133 228134 228135 228136 228137 228138 228139 228140 228141 [...] (268197 flits)
Class 0:
Remaining flits: 198828 198829 198830 198831 198832 198833 198834 198835 198836 198837 [...] (622129 flits)
Measured flits: 228132 228133 228134 228135 228136 228137 228138 228139 228140 228141 [...] (240551 flits)
Class 0:
Remaining flits: 198828 198829 198830 198831 198832 198833 198834 198835 198836 198837 [...] (651919 flits)
Measured flits: 228132 228133 228134 228135 228136 228137 228138 228139 228140 228141 [...] (214250 flits)
Class 0:
Remaining flits: 210420 210421 210422 210423 210424 210425 210426 210427 210428 210429 [...] (684414 flits)
Measured flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (188254 flits)
Class 0:
Remaining flits: 210420 210421 210422 210423 210424 210425 210426 210427 210428 210429 [...] (721035 flits)
Measured flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (163694 flits)
Class 0:
Remaining flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (755150 flits)
Measured flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (141090 flits)
Class 0:
Remaining flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (783404 flits)
Measured flits: 229968 229969 229970 229971 229972 229973 229974 229975 229976 229977 [...] (119981 flits)
Class 0:
Remaining flits: 234817 234818 234819 234820 234821 234822 234823 234824 234825 234826 [...] (809643 flits)
Measured flits: 234817 234818 234819 234820 234821 234822 234823 234824 234825 234826 [...] (100875 flits)
Class 0:
Remaining flits: 258786 258787 258788 258789 258790 258791 258792 258793 258794 258795 [...] (831133 flits)
Measured flits: 258786 258787 258788 258789 258790 258791 258792 258793 258794 258795 [...] (82896 flits)
Class 0:
Remaining flits: 258786 258787 258788 258789 258790 258791 258792 258793 258794 258795 [...] (857066 flits)
Measured flits: 258786 258787 258788 258789 258790 258791 258792 258793 258794 258795 [...] (67978 flits)
Class 0:
Remaining flits: 283680 283681 283682 283683 283684 283685 283686 283687 283688 283689 [...] (876736 flits)
Measured flits: 283680 283681 283682 283683 283684 283685 283686 283687 283688 283689 [...] (54881 flits)
Class 0:
Remaining flits: 283680 283681 283682 283683 283684 283685 283686 283687 283688 283689 [...] (887728 flits)
Measured flits: 283680 283681 283682 283683 283684 283685 283686 283687 283688 283689 [...] (44249 flits)
Class 0:
Remaining flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (903022 flits)
Measured flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (34863 flits)
Class 0:
Remaining flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (915466 flits)
Measured flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (27480 flits)
Class 0:
Remaining flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (921914 flits)
Measured flits: 315936 315937 315938 315939 315940 315941 315942 315943 315944 315945 [...] (21457 flits)
Class 0:
Remaining flits: 346464 346465 346466 346467 346468 346469 346470 346471 346472 346473 [...] (927919 flits)
Measured flits: 346464 346465 346466 346467 346468 346469 346470 346471 346472 346473 [...] (16144 flits)
Class 0:
Remaining flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (933322 flits)
Measured flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (12381 flits)
Class 0:
Remaining flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (935226 flits)
Measured flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (9203 flits)
Class 0:
Remaining flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (937460 flits)
Measured flits: 358686 358687 358688 358689 358690 358691 358692 358693 358694 358695 [...] (6767 flits)
Class 0:
Remaining flits: 528300 528301 528302 528303 528304 528305 528306 528307 528308 528309 [...] (940682 flits)
Measured flits: 528300 528301 528302 528303 528304 528305 528306 528307 528308 528309 [...] (4799 flits)
Class 0:
Remaining flits: 563922 563923 563924 563925 563926 563927 563928 563929 563930 563931 [...] (941932 flits)
Measured flits: 563922 563923 563924 563925 563926 563927 563928 563929 563930 563931 [...] (3197 flits)
Class 0:
Remaining flits: 563932 563933 563934 563935 563936 563937 563938 563939 593550 593551 [...] (941253 flits)
Measured flits: 563932 563933 563934 563935 563936 563937 563938 563939 593550 593551 [...] (2168 flits)
Class 0:
Remaining flits: 593550 593551 593552 593553 593554 593555 593556 593557 593558 593559 [...] (940560 flits)
Measured flits: 593550 593551 593552 593553 593554 593555 593556 593557 593558 593559 [...] (1418 flits)
Class 0:
Remaining flits: 614448 614449 614450 614451 614452 614453 614454 614455 614456 614457 [...] (937956 flits)
Measured flits: 614448 614449 614450 614451 614452 614453 614454 614455 614456 614457 [...] (874 flits)
Class 0:
Remaining flits: 654930 654931 654932 654933 654934 654935 654936 654937 654938 654939 [...] (938878 flits)
Measured flits: 654930 654931 654932 654933 654934 654935 654936 654937 654938 654939 [...] (486 flits)
Class 0:
Remaining flits: 654930 654931 654932 654933 654934 654935 654936 654937 654938 654939 [...] (938532 flits)
Measured flits: 654930 654931 654932 654933 654934 654935 654936 654937 654938 654939 [...] (282 flits)
Class 0:
Remaining flits: 661410 661411 661412 661413 661414 661415 661416 661417 661418 661419 [...] (937915 flits)
Measured flits: 661410 661411 661412 661413 661414 661415 661416 661417 661418 661419 [...] (166 flits)
Class 0:
Remaining flits: 678006 678007 678008 678009 678010 678011 678012 678013 678014 678015 [...] (936636 flits)
Measured flits: 678006 678007 678008 678009 678010 678011 678012 678013 678014 678015 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 775206 775207 775208 775209 775210 775211 775212 775213 775214 775215 [...] (905365 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 778986 778987 778988 778989 778990 778991 778992 778993 778994 778995 [...] (874228 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 795114 795115 795116 795117 795118 795119 795120 795121 795122 795123 [...] (843662 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 808074 808075 808076 808077 808078 808079 808080 808081 808082 808083 [...] (812523 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 808074 808075 808076 808077 808078 808079 808080 808081 808082 808083 [...] (780589 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 816660 816661 816662 816663 816664 816665 816666 816667 816668 816669 [...] (749748 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 816660 816661 816662 816663 816664 816665 816666 816667 816668 816669 [...] (718092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 816660 816661 816662 816663 816664 816665 816666 816667 816668 816669 [...] (686208 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 816660 816661 816662 816663 816664 816665 816666 816667 816668 816669 [...] (655312 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860742 860743 860744 860745 860746 860747 860748 860749 860750 860751 [...] (623934 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 863208 863209 863210 863211 863212 863213 863214 863215 863216 863217 [...] (591751 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 902538 902539 902540 902541 902542 902543 902544 902545 902546 902547 [...] (560070 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 902538 902539 902540 902541 902542 902543 902544 902545 902546 902547 [...] (529532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 938538 938539 938540 938541 938542 938543 938544 938545 938546 938547 [...] (499086 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1025316 1025317 1025318 1025319 1025320 1025321 1025322 1025323 1025324 1025325 [...] (468494 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1025316 1025317 1025318 1025319 1025320 1025321 1025322 1025323 1025324 1025325 [...] (437883 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1025316 1025317 1025318 1025319 1025320 1025321 1025322 1025323 1025324 1025325 [...] (407528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1055502 1055503 1055504 1055505 1055506 1055507 1055508 1055509 1055510 1055511 [...] (377652 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1103526 1103527 1103528 1103529 1103530 1103531 1103532 1103533 1103534 1103535 [...] (347149 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1120104 1120105 1120106 1120107 1120108 1120109 1120110 1120111 1120112 1120113 [...] (317672 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1128744 1128745 1128746 1128747 1128748 1128749 1128750 1128751 1128752 1128753 [...] (288341 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1128744 1128745 1128746 1128747 1128748 1128749 1128750 1128751 1128752 1128753 [...] (258905 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1199333 1199334 1199335 1199336 1199337 1199338 1199339 1205244 1205245 1205246 [...] (229066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1205244 1205245 1205246 1205247 1205248 1205249 1205250 1205251 1205252 1205253 [...] (199242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (170418 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (142852 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (115704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (88070 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1255698 1255699 1255700 1255701 1255702 1255703 1255704 1255705 1255706 1255707 [...] (59644 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1393038 1393039 1393040 1393041 1393042 1393043 1393044 1393045 1393046 1393047 [...] (34504 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1646743 1646744 1646745 1646746 1646747 1727027 1749744 1749745 1749746 1749747 [...] (14963 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1749758 1749759 1749760 1749761 1859130 1859131 1859132 1859133 1859134 1859135 [...] (3920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1869840 1869841 1869842 1869843 1869844 1869845 1869846 1869847 1869848 1869849 [...] (1063 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2205593 2211030 2211031 2211032 2211033 2211034 2211035 2211036 2211037 2211038 [...] (73 flits)
Measured flits: (0 flits)
Time taken is 76460 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9150.57 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33226 (1 samples)
Network latency average = 8830.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33111 (1 samples)
Flit latency average = 18801.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 54593 (1 samples)
Fragmentation average = 72.6859 (1 samples)
	minimum = 0 (1 samples)
	maximum = 187 (1 samples)
Injected packet rate average = 0.0213884 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0364286 (1 samples)
Accepted packet rate average = 0.0101429 (1 samples)
	minimum = 0.00742857 (1 samples)
	maximum = 0.0127143 (1 samples)
Injected flit rate average = 0.384906 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 0.656714 (1 samples)
Accepted flit rate average = 0.182597 (1 samples)
	minimum = 0.135 (1 samples)
	maximum = 0.228857 (1 samples)
Injected packet size average = 17.996 (1 samples)
Accepted packet size average = 18.0026 (1 samples)
Hops average = 5.06879 (1 samples)
Total run time 55.5416
