BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 339.523
	minimum = 23
	maximum = 875
Network latency average = 326.22
	minimum = 23
	maximum = 875
Slowest packet = 646
Flit latency average = 232.058
	minimum = 6
	maximum = 938
Slowest flit = 4457
Fragmentation average = 222.962
	minimum = 0
	maximum = 808
Injected packet rate average = 0.0268958
	minimum = 0.015 (at node 52)
	maximum = 0.039 (at node 3)
Accepted packet rate average = 0.00957812
	minimum = 0.001 (at node 174)
	maximum = 0.018 (at node 91)
Injected flit rate average = 0.479078
	minimum = 0.269 (at node 52)
	maximum = 0.697 (at node 3)
Accepted flit rate average= 0.215172
	minimum = 0.082 (at node 174)
	maximum = 0.356 (at node 104)
Injected packet length average = 17.8124
Accepted packet length average = 22.4649
Total in-flight flits = 51693 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 629.1
	minimum = 23
	maximum = 1813
Network latency average = 570.775
	minimum = 23
	maximum = 1807
Slowest packet = 624
Flit latency average = 448.384
	minimum = 6
	maximum = 1942
Slowest flit = 3805
Fragmentation average = 287.06
	minimum = 0
	maximum = 1271
Injected packet rate average = 0.0221484
	minimum = 0.008 (at node 108)
	maximum = 0.03 (at node 70)
Accepted packet rate average = 0.011151
	minimum = 0.005 (at node 96)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.395018
	minimum = 0.137 (at node 108)
	maximum = 0.537 (at node 70)
Accepted flit rate average= 0.222693
	minimum = 0.099 (at node 96)
	maximum = 0.3545 (at node 152)
Injected packet length average = 17.835
Accepted packet length average = 19.9706
Total in-flight flits = 67756 (0 measured)
latency change    = 0.460304
throughput change = 0.0337722
Class 0:
Packet latency average = 1379.27
	minimum = 33
	maximum = 2875
Network latency average = 1123.51
	minimum = 30
	maximum = 2853
Slowest packet = 552
Flit latency average = 1002.36
	minimum = 6
	maximum = 2836
Slowest flit = 9953
Fragmentation average = 323.926
	minimum = 3
	maximum = 2229
Injected packet rate average = 0.0089375
	minimum = 0 (at node 1)
	maximum = 0.032 (at node 65)
Accepted packet rate average = 0.0122656
	minimum = 0.002 (at node 127)
	maximum = 0.023 (at node 8)
Injected flit rate average = 0.160286
	minimum = 0 (at node 7)
	maximum = 0.583 (at node 65)
Accepted flit rate average= 0.216318
	minimum = 0.052 (at node 127)
	maximum = 0.39 (at node 183)
Injected packet length average = 17.9341
Accepted packet length average = 17.6361
Total in-flight flits = 57129 (0 measured)
latency change    = 0.543891
throughput change = 0.0294705
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1740.81
	minimum = 107
	maximum = 3137
Network latency average = 319.386
	minimum = 28
	maximum = 975
Slowest packet = 10241
Flit latency average = 1173.47
	minimum = 6
	maximum = 3858
Slowest flit = 12856
Fragmentation average = 147.104
	minimum = 1
	maximum = 653
Injected packet rate average = 0.00972396
	minimum = 0 (at node 2)
	maximum = 0.044 (at node 126)
Accepted packet rate average = 0.011724
	minimum = 0.005 (at node 110)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.174646
	minimum = 0 (at node 2)
	maximum = 0.782 (at node 126)
Accepted flit rate average= 0.206203
	minimum = 0.078 (at node 35)
	maximum = 0.434 (at node 129)
Injected packet length average = 17.9604
Accepted packet length average = 17.5882
Total in-flight flits = 51162 (21604 measured)
latency change    = 0.207684
throughput change = 0.0490516
Class 0:
Packet latency average = 2208.12
	minimum = 51
	maximum = 4093
Network latency average = 507.32
	minimum = 25
	maximum = 1892
Slowest packet = 10241
Flit latency average = 1165.25
	minimum = 6
	maximum = 4856
Slowest flit = 12944
Fragmentation average = 187.27
	minimum = 0
	maximum = 1425
Injected packet rate average = 0.0101484
	minimum = 0 (at node 2)
	maximum = 0.0335 (at node 117)
Accepted packet rate average = 0.0115885
	minimum = 0.0055 (at node 35)
	maximum = 0.0205 (at node 129)
Injected flit rate average = 0.182638
	minimum = 0 (at node 2)
	maximum = 0.61 (at node 117)
Accepted flit rate average= 0.206122
	minimum = 0.084 (at node 110)
	maximum = 0.378 (at node 129)
Injected packet length average = 17.9967
Accepted packet length average = 17.7867
Total in-flight flits = 48124 (30946 measured)
latency change    = 0.211632
throughput change = 0.000391656
Class 0:
Packet latency average = 2585.01
	minimum = 51
	maximum = 4810
Network latency average = 631.175
	minimum = 25
	maximum = 2920
Slowest packet = 10241
Flit latency average = 1147.53
	minimum = 6
	maximum = 5852
Slowest flit = 8189
Fragmentation average = 205.981
	minimum = 0
	maximum = 1880
Injected packet rate average = 0.0109184
	minimum = 0 (at node 2)
	maximum = 0.033 (at node 126)
Accepted packet rate average = 0.0116042
	minimum = 0.00666667 (at node 110)
	maximum = 0.017 (at node 128)
Injected flit rate average = 0.196583
	minimum = 0 (at node 2)
	maximum = 0.594 (at node 126)
Accepted flit rate average= 0.20703
	minimum = 0.122667 (at node 110)
	maximum = 0.314667 (at node 128)
Injected packet length average = 18.0048
Accepted packet length average = 17.841
Total in-flight flits = 51172 (40136 measured)
latency change    = 0.145796
throughput change = 0.00438159
Class 0:
Packet latency average = 2937.42
	minimum = 51
	maximum = 6020
Network latency average = 733.732
	minimum = 25
	maximum = 3797
Slowest packet = 10241
Flit latency average = 1146.95
	minimum = 6
	maximum = 6658
Slowest flit = 24187
Fragmentation average = 217.986
	minimum = 0
	maximum = 2988
Injected packet rate average = 0.0113477
	minimum = 0 (at node 2)
	maximum = 0.03175 (at node 126)
Accepted packet rate average = 0.0116055
	minimum = 0.008 (at node 110)
	maximum = 0.0155 (at node 128)
Injected flit rate average = 0.204221
	minimum = 0 (at node 2)
	maximum = 0.56925 (at node 126)
Accepted flit rate average= 0.207732
	minimum = 0.14025 (at node 64)
	maximum = 0.286 (at node 128)
Injected packet length average = 17.9968
Accepted packet length average = 17.8995
Total in-flight flits = 54515 (47197 measured)
latency change    = 0.119973
throughput change = 0.00338059
Class 0:
Packet latency average = 3259.28
	minimum = 51
	maximum = 7111
Network latency average = 809.983
	minimum = 24
	maximum = 4789
Slowest packet = 10241
Flit latency average = 1147.21
	minimum = 6
	maximum = 7744
Slowest flit = 23774
Fragmentation average = 231.997
	minimum = 0
	maximum = 3996
Injected packet rate average = 0.0113094
	minimum = 0 (at node 2)
	maximum = 0.0322 (at node 126)
Accepted packet rate average = 0.0116437
	minimum = 0.0084 (at node 64)
	maximum = 0.0156 (at node 128)
Injected flit rate average = 0.203518
	minimum = 0 (at node 2)
	maximum = 0.58 (at node 126)
Accepted flit rate average= 0.208511
	minimum = 0.1546 (at node 64)
	maximum = 0.2782 (at node 128)
Injected packet length average = 17.9955
Accepted packet length average = 17.9076
Total in-flight flits = 52438 (47227 measured)
latency change    = 0.0987517
throughput change = 0.0037393
Class 0:
Packet latency average = 3596
	minimum = 51
	maximum = 8251
Network latency average = 885.265
	minimum = 24
	maximum = 5781
Slowest packet = 10241
Flit latency average = 1160.73
	minimum = 6
	maximum = 8855
Slowest flit = 12590
Fragmentation average = 241.554
	minimum = 0
	maximum = 5075
Injected packet rate average = 0.0113932
	minimum = 0 (at node 10)
	maximum = 0.0278333 (at node 126)
Accepted packet rate average = 0.0116302
	minimum = 0.00866667 (at node 42)
	maximum = 0.015 (at node 128)
Injected flit rate average = 0.204989
	minimum = 0 (at node 10)
	maximum = 0.501833 (at node 126)
Accepted flit rate average= 0.208722
	minimum = 0.153833 (at node 42)
	maximum = 0.2735 (at node 129)
Injected packet length average = 17.9922
Accepted packet length average = 17.9466
Total in-flight flits = 52949 (49293 measured)
latency change    = 0.0936366
throughput change = 0.00100978
Class 0:
Packet latency average = 3951.15
	minimum = 51
	maximum = 8719
Network latency average = 946.804
	minimum = 23
	maximum = 6536
Slowest packet = 10241
Flit latency average = 1172.37
	minimum = 6
	maximum = 9688
Slowest flit = 31020
Fragmentation average = 249.845
	minimum = 0
	maximum = 5075
Injected packet rate average = 0.011407
	minimum = 0 (at node 47)
	maximum = 0.0238571 (at node 126)
Accepted packet rate average = 0.0116287
	minimum = 0.00857143 (at node 42)
	maximum = 0.0145714 (at node 72)
Injected flit rate average = 0.205391
	minimum = 0 (at node 47)
	maximum = 0.430143 (at node 126)
Accepted flit rate average= 0.208545
	minimum = 0.155714 (at node 42)
	maximum = 0.263143 (at node 129)
Injected packet length average = 18.0057
Accepted packet length average = 17.9336
Total in-flight flits = 52875 (50267 measured)
latency change    = 0.0898857
throughput change = 0.000851517
Draining all recorded packets ...
Class 0:
Remaining flits: 19674 19675 19676 19677 19678 19679 19680 19681 19682 19683 [...] (52494 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (50598 flits)
Class 0:
Remaining flits: 19674 19675 19676 19677 19678 19679 19680 19681 19682 19683 [...] (50582 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (49111 flits)
Class 0:
Remaining flits: 19674 19675 19676 19677 19678 19679 19680 19681 19682 19683 [...] (53928 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (52725 flits)
Class 0:
Remaining flits: 19674 19675 19676 19677 19678 19679 19680 19681 19682 19683 [...] (54884 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (53656 flits)
Class 0:
Remaining flits: 19684 19685 19686 19687 19688 19689 19690 19691 21780 21781 [...] (54111 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (52711 flits)
Class 0:
Remaining flits: 19684 19685 19686 19687 19688 19689 19690 19691 21780 21781 [...] (54408 flits)
Measured flits: 184914 184915 184916 184917 184918 184919 184920 184921 184922 184923 [...] (52698 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (56354 flits)
Measured flits: 191718 191719 191720 191721 191722 191723 191724 191725 191726 191727 [...] (53954 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (55599 flits)
Measured flits: 191723 191724 191725 191726 191727 191728 191729 191730 191731 191732 [...] (50883 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (59024 flits)
Measured flits: 208476 208477 208478 208479 208480 208481 208482 208483 208484 208485 [...] (51388 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (58455 flits)
Measured flits: 208476 208477 208478 208479 208480 208481 208482 208483 208484 208485 [...] (48635 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (58756 flits)
Measured flits: 208476 208477 208478 208479 208480 208481 208482 208483 208484 208485 [...] (47394 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (57565 flits)
Measured flits: 223326 223327 223328 223329 223330 223331 223332 223333 223334 223335 [...] (44205 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (57503 flits)
Measured flits: 223340 223341 223342 223343 242424 242425 242426 242427 242428 242429 [...] (42900 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (60102 flits)
Measured flits: 242429 242430 242431 242432 242433 242434 242435 242436 242437 242438 [...] (39997 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (63265 flits)
Measured flits: 262386 262387 262388 262389 262390 262391 262392 262393 262394 262395 [...] (35678 flits)
Class 0:
Remaining flits: 21780 21781 21782 21783 21784 21785 21786 21787 21788 21789 [...] (63661 flits)
Measured flits: 262386 262387 262388 262389 262390 262391 262392 262393 262394 262395 [...] (32834 flits)
Class 0:
Remaining flits: 23691 23692 23693 23694 23695 23696 23697 23698 23699 23700 [...] (64671 flits)
Measured flits: 262386 262387 262388 262389 262390 262391 262392 262393 262394 262395 [...] (30666 flits)
Class 0:
Remaining flits: 23700 23701 23702 23703 23704 23705 61884 61885 61886 61887 [...] (63591 flits)
Measured flits: 303624 303625 303626 303627 303628 303629 303630 303631 303632 303633 [...] (26500 flits)
Class 0:
Remaining flits: 303635 303636 303637 303638 303639 303640 303641 331139 331140 331141 [...] (61997 flits)
Measured flits: 303635 303636 303637 303638 303639 303640 303641 331139 331140 331141 [...] (22995 flits)
Class 0:
Remaining flits: 303635 303636 303637 303638 303639 303640 303641 331139 331140 331141 [...] (60115 flits)
Measured flits: 303635 303636 303637 303638 303639 303640 303641 331139 331140 331141 [...] (20332 flits)
Class 0:
Remaining flits: 303635 303636 303637 303638 303639 303640 303641 372150 372151 372152 [...] (62869 flits)
Measured flits: 303635 303636 303637 303638 303639 303640 303641 372150 372151 372152 [...] (18867 flits)
Class 0:
Remaining flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (66911 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (18053 flits)
Class 0:
Remaining flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (65277 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (15677 flits)
Class 0:
Remaining flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (67937 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (14373 flits)
Class 0:
Remaining flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (65892 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (12547 flits)
Class 0:
Remaining flits: 509058 509059 509060 509061 509062 509063 509064 509065 509066 509067 [...] (67275 flits)
Measured flits: 509058 509059 509060 509061 509062 509063 509064 509065 509066 509067 [...] (11202 flits)
Class 0:
Remaining flits: 509058 509059 509060 509061 509062 509063 509064 509065 509066 509067 [...] (66408 flits)
Measured flits: 509058 509059 509060 509061 509062 509063 509064 509065 509066 509067 [...] (9431 flits)
Class 0:
Remaining flits: 509066 509067 509068 509069 509070 509071 509072 509073 509074 509075 [...] (65079 flits)
Measured flits: 509066 509067 509068 509069 509070 509071 509072 509073 509074 509075 [...] (8477 flits)
Class 0:
Remaining flits: 565351 565352 565353 565354 565355 565356 565357 565358 565359 565360 [...] (65524 flits)
Measured flits: 565351 565352 565353 565354 565355 565356 565357 565358 565359 565360 [...] (7823 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (65497 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (6779 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (67681 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (6754 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (66232 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (6030 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (63861 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (5399 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (64170 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (5085 flits)
Class 0:
Remaining flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (65530 flits)
Measured flits: 767232 767233 767234 767235 767236 767237 767238 767239 767240 767241 [...] (4607 flits)
Class 0:
Remaining flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (66493 flits)
Measured flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (4298 flits)
Class 0:
Remaining flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (65498 flits)
Measured flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (3743 flits)
Class 0:
Remaining flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (62722 flits)
Measured flits: 885996 885997 885998 885999 886000 886001 886002 886003 886004 886005 [...] (3260 flits)
Class 0:
Remaining flits: 920592 920593 920594 920595 920596 920597 920598 920599 920600 920601 [...] (64184 flits)
Measured flits: 931896 931897 931898 931899 931900 931901 931902 931903 931904 931905 [...] (2686 flits)
Class 0:
Remaining flits: 920592 920593 920594 920595 920596 920597 920598 920599 920600 920601 [...] (65687 flits)
Measured flits: 931896 931897 931898 931899 931900 931901 931902 931903 931904 931905 [...] (2650 flits)
Class 0:
Remaining flits: 920592 920593 920594 920595 920596 920597 920598 920599 920600 920601 [...] (66457 flits)
Measured flits: 931896 931897 931898 931899 931900 931901 931902 931903 931904 931905 [...] (2375 flits)
Class 0:
Remaining flits: 931902 931903 931904 931905 931906 931907 931908 931909 931910 931911 [...] (64398 flits)
Measured flits: 931902 931903 931904 931905 931906 931907 931908 931909 931910 931911 [...] (1951 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (65357 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (1857 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (64812 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (1609 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (62463 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (1055 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (63836 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (915 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (63410 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (1083 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (64678 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (841 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (61761 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (711 flits)
Class 0:
Remaining flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (62072 flits)
Measured flits: 960624 960625 960626 960627 960628 960629 960630 960631 960632 960633 [...] (639 flits)
Class 0:
Remaining flits: 960639 960640 960641 1215594 1215595 1215596 1215597 1215598 1215599 1215600 [...] (60196 flits)
Measured flits: 960639 960640 960641 1331964 1331965 1331966 1331967 1331968 1331969 1331970 [...] (572 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (60431 flits)
Measured flits: 1331966 1331967 1331968 1331969 1331970 1331971 1331972 1331973 1331974 1331975 [...] (516 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (63118 flits)
Measured flits: 1331969 1331970 1331971 1331972 1331973 1331974 1331975 1331976 1331977 1331978 [...] (499 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (63131 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (467 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (62517 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (564 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (61860 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (565 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (61507 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (391 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (59706 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (313 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (60412 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (273 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (61685 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (244 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (64309 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (195 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (62153 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (326 flits)
Class 0:
Remaining flits: 1215594 1215595 1215596 1215597 1215598 1215599 1215600 1215601 1215602 1215603 [...] (61477 flits)
Measured flits: 1615842 1615843 1615844 1615845 1615846 1615847 1615848 1615849 1615850 1615851 [...] (221 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (62108 flits)
Measured flits: 1615847 1615848 1615849 1615850 1615851 1615852 1615853 1615854 1615855 1615856 [...] (175 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (60456 flits)
Measured flits: 1792332 1792333 1792334 1792335 1792336 1792337 1792338 1792339 1792340 1792341 [...] (162 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (61891 flits)
Measured flits: 1792332 1792333 1792334 1792335 1792336 1792337 1792338 1792339 1792340 1792341 [...] (162 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (63171 flits)
Measured flits: 1792332 1792333 1792334 1792335 1792336 1792337 1792338 1792339 1792340 1792341 [...] (162 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (64268 flits)
Measured flits: 2530152 2530153 2530154 2530155 2530156 2530157 2530158 2530159 2530160 2530161 [...] (111 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (63544 flits)
Measured flits: 2530152 2530153 2530154 2530155 2530156 2530157 2530158 2530159 2530160 2530161 [...] (108 flits)
Class 0:
Remaining flits: 1354410 1354411 1354412 1354413 1354414 1354415 1354416 1354417 1354418 1354419 [...] (66854 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (90 flits)
Class 0:
Remaining flits: 1354423 1354424 1354425 1354426 1354427 1393362 1393363 1393364 1393365 1393366 [...] (66756 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (90 flits)
Class 0:
Remaining flits: 1395342 1395343 1395344 1395345 1395346 1395347 1395348 1395349 1395350 1395351 [...] (66000 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (72 flits)
Class 0:
Remaining flits: 1871473 1871474 1871475 1871476 1871477 1926846 1926847 1926848 1926849 1926850 [...] (65236 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (72 flits)
Class 0:
Remaining flits: 1871473 1871474 1871475 1871476 1871477 1926846 1926847 1926848 1926849 1926850 [...] (63569 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (72 flits)
Class 0:
Remaining flits: 1926863 1983636 1983637 1983638 1983639 1983640 1983641 1983642 1983643 1983644 [...] (65510 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (60 flits)
Class 0:
Remaining flits: 1926863 2064420 2064421 2064422 2064423 2064424 2064425 2064426 2064427 2064428 [...] (64975 flits)
Measured flits: 2535912 2535913 2535914 2535915 2535916 2535917 2535918 2535919 2535920 2535921 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2064420 2064421 2064422 2064423 2064424 2064425 2064426 2064427 2064428 2064429 [...] (41904 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2064420 2064421 2064422 2064423 2064424 2064425 2064426 2064427 2064428 2064429 [...] (28772 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2064420 2064421 2064422 2064423 2064424 2064425 2064426 2064427 2064428 2064429 [...] (16695 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147652 2147653 2147654 2147655 2147656 2147657 2147658 2147659 2147660 2147661 [...] (7622 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147652 2147653 2147654 2147655 2147656 2147657 2147658 2147659 2147660 2147661 [...] (2411 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2593241 2648969 2704114 2704115 2704116 2704117 2704118 2704119 2704120 2704121 [...] (485 flits)
Measured flits: (0 flits)
Time taken is 92815 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11912 (1 samples)
	minimum = 51 (1 samples)
	maximum = 77116 (1 samples)
Network latency average = 1618.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 38152 (1 samples)
Flit latency average = 1785.17 (1 samples)
	minimum = 6 (1 samples)
	maximum = 47495 (1 samples)
Fragmentation average = 279.845 (1 samples)
	minimum = 0 (1 samples)
	maximum = 16814 (1 samples)
Injected packet rate average = 0.011407 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0238571 (1 samples)
Accepted packet rate average = 0.0116287 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0145714 (1 samples)
Injected flit rate average = 0.205391 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.430143 (1 samples)
Accepted flit rate average = 0.208545 (1 samples)
	minimum = 0.155714 (1 samples)
	maximum = 0.263143 (1 samples)
Injected packet size average = 18.0057 (1 samples)
Accepted packet size average = 17.9336 (1 samples)
Hops average = 5.05883 (1 samples)
Total run time 143.188
