BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.676
	minimum = 25
	maximum = 954
Network latency average = 310.324
	minimum = 23
	maximum = 926
Slowest packet = 243
Flit latency average = 240.076
	minimum = 6
	maximum = 942
Slowest flit = 4603
Fragmentation average = 186.346
	minimum = 0
	maximum = 717
Injected packet rate average = 0.042901
	minimum = 0.03 (at node 51)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0111927
	minimum = 0.004 (at node 41)
	maximum = 0.022 (at node 140)
Injected flit rate average = 0.76549
	minimum = 0.531 (at node 145)
	maximum = 0.992 (at node 55)
Accepted flit rate average= 0.240828
	minimum = 0.113 (at node 135)
	maximum = 0.432 (at node 114)
Injected packet length average = 17.8431
Accepted packet length average = 21.5165
Total in-flight flits = 102027 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.964
	minimum = 23
	maximum = 1896
Network latency average = 554.998
	minimum = 23
	maximum = 1863
Slowest packet = 590
Flit latency average = 468.823
	minimum = 6
	maximum = 1922
Slowest flit = 6217
Fragmentation average = 247.683
	minimum = 0
	maximum = 1413
Injected packet rate average = 0.0438047
	minimum = 0.029 (at node 51)
	maximum = 0.055 (at node 74)
Accepted packet rate average = 0.0127422
	minimum = 0.007 (at node 118)
	maximum = 0.019 (at node 114)
Injected flit rate average = 0.784995
	minimum = 0.5145 (at node 51)
	maximum = 0.984 (at node 74)
Accepted flit rate average= 0.25418
	minimum = 0.1455 (at node 118)
	maximum = 0.3795 (at node 114)
Injected packet length average = 17.9203
Accepted packet length average = 19.9479
Total in-flight flits = 205173 (0 measured)
latency change    = 0.433051
throughput change = 0.052528
Class 0:
Packet latency average = 1255.53
	minimum = 28
	maximum = 2856
Network latency average = 1225.57
	minimum = 24
	maximum = 2832
Slowest packet = 937
Flit latency average = 1160.94
	minimum = 6
	maximum = 2867
Slowest flit = 16762
Fragmentation average = 341.834
	minimum = 0
	maximum = 1957
Injected packet rate average = 0.0442917
	minimum = 0.032 (at node 35)
	maximum = 0.056 (at node 95)
Accepted packet rate average = 0.0147344
	minimum = 0.007 (at node 23)
	maximum = 0.026 (at node 0)
Injected flit rate average = 0.797573
	minimum = 0.562 (at node 35)
	maximum = 1 (at node 55)
Accepted flit rate average= 0.270479
	minimum = 0.121 (at node 23)
	maximum = 0.483 (at node 0)
Injected packet length average = 18.0073
Accepted packet length average = 18.357
Total in-flight flits = 306313 (0 measured)
latency change    = 0.539665
throughput change = 0.0602615
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 87.7003
	minimum = 23
	maximum = 621
Network latency average = 54.2068
	minimum = 23
	maximum = 621
Slowest packet = 25893
Flit latency average = 1718.47
	minimum = 6
	maximum = 3887
Slowest flit = 12249
Fragmentation average = 16.4723
	minimum = 0
	maximum = 90
Injected packet rate average = 0.0448646
	minimum = 0.027 (at node 173)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0146667
	minimum = 0.006 (at node 169)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.80726
	minimum = 0.486 (at node 173)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.264604
	minimum = 0.131 (at node 169)
	maximum = 0.441 (at node 16)
Injected packet length average = 17.9933
Accepted packet length average = 18.0412
Total in-flight flits = 410561 (143696 measured)
latency change    = 13.3161
throughput change = 0.022203
Class 0:
Packet latency average = 96.526
	minimum = 23
	maximum = 1772
Network latency average = 62.1524
	minimum = 23
	maximum = 1757
Slowest packet = 25576
Flit latency average = 1991.22
	minimum = 6
	maximum = 4793
Slowest flit = 20593
Fragmentation average = 18.2228
	minimum = 0
	maximum = 567
Injected packet rate average = 0.0447057
	minimum = 0.034 (at node 102)
	maximum = 0.056 (at node 143)
Accepted packet rate average = 0.0147344
	minimum = 0.008 (at node 36)
	maximum = 0.0225 (at node 16)
Injected flit rate average = 0.804466
	minimum = 0.6095 (at node 102)
	maximum = 1 (at node 143)
Accepted flit rate average= 0.264167
	minimum = 0.1635 (at node 36)
	maximum = 0.3985 (at node 16)
Injected packet length average = 17.9947
Accepted packet length average = 17.9286
Total in-flight flits = 513879 (285179 measured)
latency change    = 0.0914334
throughput change = 0.00165615
Class 0:
Packet latency average = 173.721
	minimum = 23
	maximum = 2995
Network latency average = 140.13
	minimum = 23
	maximum = 2977
Slowest packet = 25353
Flit latency average = 2265.07
	minimum = 6
	maximum = 5799
Slowest flit = 25871
Fragmentation average = 27.47
	minimum = 0
	maximum = 981
Injected packet rate average = 0.0445851
	minimum = 0.0343333 (at node 102)
	maximum = 0.0556667 (at node 143)
Accepted packet rate average = 0.0147431
	minimum = 0.0103333 (at node 64)
	maximum = 0.0203333 (at node 129)
Injected flit rate average = 0.802568
	minimum = 0.617333 (at node 102)
	maximum = 1 (at node 143)
Accepted flit rate average= 0.264505
	minimum = 0.192667 (at node 89)
	maximum = 0.359 (at node 128)
Injected packet length average = 18.0008
Accepted packet length average = 17.941
Total in-flight flits = 616216 (424116 measured)
latency change    = 0.444363
throughput change = 0.00127991
Draining remaining packets ...
Class 0:
Remaining flits: 19764 19765 19766 19767 19768 19769 19770 19771 19772 19773 [...] (577377 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (420439 flits)
Class 0:
Remaining flits: 19764 19765 19766 19767 19768 19769 19770 19771 19772 19773 [...] (539704 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (414216 flits)
Class 0:
Remaining flits: 23498 23499 23500 23501 23502 23503 23504 23505 23506 23507 [...] (502221 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (404284 flits)
Class 0:
Remaining flits: 23502 23503 23504 23505 23506 23507 33838 33839 40283 45414 [...] (465376 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (388465 flits)
Class 0:
Remaining flits: 45414 45415 45416 45417 45418 45419 45420 45421 45422 45423 [...] (429120 flits)
Measured flits: 455853 455854 455855 455856 455857 455858 455859 455860 455861 455862 [...] (368511 flits)
Class 0:
Remaining flits: 45414 45415 45416 45417 45418 45419 45420 45421 45422 45423 [...] (393780 flits)
Measured flits: 455853 455854 455855 455856 455857 455858 455859 455860 455861 455862 [...] (345317 flits)
Class 0:
Remaining flits: 45414 45415 45416 45417 45418 45419 45420 45421 45422 45423 [...] (358540 flits)
Measured flits: 455868 455869 455870 455871 455872 455873 455874 455875 455876 455877 [...] (319517 flits)
Class 0:
Remaining flits: 45424 45425 45426 45427 45428 45429 45430 45431 53190 53191 [...] (324034 flits)
Measured flits: 455880 455881 455882 455883 455884 455885 455886 455887 455888 455889 [...] (292625 flits)
Class 0:
Remaining flits: 45424 45425 45426 45427 45428 45429 45430 45431 53190 53191 [...] (289482 flits)
Measured flits: 455886 455887 455888 455889 455890 455891 455892 455893 455894 455895 [...] (263625 flits)
Class 0:
Remaining flits: 53190 53191 53192 53193 53194 53195 53196 53197 53198 53199 [...] (255548 flits)
Measured flits: 455886 455887 455888 455889 455890 455891 455892 455893 455894 455895 [...] (234238 flits)
Class 0:
Remaining flits: 53190 53191 53192 53193 53194 53195 53196 53197 53198 53199 [...] (221744 flits)
Measured flits: 455886 455887 455888 455889 455890 455891 455892 455893 455894 455895 [...] (204344 flits)
Class 0:
Remaining flits: 53190 53191 53192 53193 53194 53195 53196 53197 53198 53199 [...] (188527 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (174212 flits)
Class 0:
Remaining flits: 53190 53191 53192 53193 53194 53195 53196 53197 53198 53199 [...] (156007 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (145037 flits)
Class 0:
Remaining flits: 58986 58987 58988 58989 58990 58991 58992 58993 58994 58995 [...] (122957 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (114264 flits)
Class 0:
Remaining flits: 58986 58987 58988 58989 58990 58991 58992 58993 58994 58995 [...] (91175 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (85013 flits)
Class 0:
Remaining flits: 58986 58987 58988 58989 58990 58991 58992 58993 58994 58995 [...] (59821 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (55357 flits)
Class 0:
Remaining flits: 72676 72677 72678 72679 72680 72681 72682 72683 94248 94249 [...] (31391 flits)
Measured flits: 455958 455959 455960 455961 455962 455963 455964 455965 455966 455967 [...] (29220 flits)
Class 0:
Remaining flits: 94249 94250 94251 94252 94253 94254 94255 94256 94257 94258 [...] (10713 flits)
Measured flits: 456300 456301 456302 456303 456304 456305 456306 456307 456308 456309 [...] (9959 flits)
Class 0:
Remaining flits: 288738 288739 288740 288741 288742 288743 288744 288745 288746 288747 [...] (670 flits)
Measured flits: 479881 479882 479883 479884 479885 479886 479887 479888 479889 479890 [...] (629 flits)
Time taken is 25299 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11247.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22139 (1 samples)
Network latency average = 11214 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21928 (1 samples)
Flit latency average = 9178.43 (1 samples)
	minimum = 6 (1 samples)
	maximum = 23928 (1 samples)
Fragmentation average = 249.426 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8782 (1 samples)
Injected packet rate average = 0.0445851 (1 samples)
	minimum = 0.0343333 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0147431 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.802568 (1 samples)
	minimum = 0.617333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.264505 (1 samples)
	minimum = 0.192667 (1 samples)
	maximum = 0.359 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 17.941 (1 samples)
Hops average = 5.07219 (1 samples)
Total run time 37.0077
