BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.856
	minimum = 24
	maximum = 934
Network latency average = 292.372
	minimum = 23
	maximum = 924
Slowest packet = 342
Flit latency average = 212.659
	minimum = 6
	maximum = 907
Slowest flit = 6173
Fragmentation average = 177.397
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0100781
	minimum = 0.004 (at node 45)
	maximum = 0.019 (at node 91)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.21574
	minimum = 0.103 (at node 185)
	maximum = 0.358 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 21.4067
Total in-flight flits = 51188 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 524.575
	minimum = 24
	maximum = 1841
Network latency average = 517.317
	minimum = 23
	maximum = 1832
Slowest packet = 228
Flit latency average = 417.161
	minimum = 6
	maximum = 1926
Slowest flit = 3945
Fragmentation average = 239.703
	minimum = 0
	maximum = 1574
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0114609
	minimum = 0.006 (at node 174)
	maximum = 0.0175 (at node 67)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.226792
	minimum = 0.122 (at node 96)
	maximum = 0.3255 (at node 67)
Injected packet length average = 17.9276
Accepted packet length average = 19.7882
Total in-flight flits = 97340 (0 measured)
latency change    = 0.430289
throughput change = 0.0487323
Class 0:
Packet latency average = 1082.26
	minimum = 23
	maximum = 2667
Network latency average = 1074.18
	minimum = 23
	maximum = 2667
Slowest packet = 1372
Flit latency average = 971.416
	minimum = 6
	maximum = 2827
Slowest flit = 13327
Fragmentation average = 335.11
	minimum = 0
	maximum = 2355
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0131927
	minimum = 0.003 (at node 135)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.243344
	minimum = 0.07 (at node 135)
	maximum = 0.472 (at node 129)
Injected packet length average = 17.9863
Accepted packet length average = 18.4453
Total in-flight flits = 142904 (0 measured)
latency change    = 0.515298
throughput change = 0.0680193
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 175.197
	minimum = 24
	maximum = 964
Network latency average = 167.381
	minimum = 24
	maximum = 949
Slowest packet = 15422
Flit latency average = 1356.98
	minimum = 6
	maximum = 3823
Slowest flit = 12662
Fragmentation average = 90.2263
	minimum = 1
	maximum = 698
Injected packet rate average = 0.0271719
	minimum = 0.013 (at node 159)
	maximum = 0.046 (at node 26)
Accepted packet rate average = 0.0134427
	minimum = 0.004 (at node 64)
	maximum = 0.023 (at node 33)
Injected flit rate average = 0.488312
	minimum = 0.25 (at node 159)
	maximum = 0.822 (at node 26)
Accepted flit rate average= 0.245214
	minimum = 0.11 (at node 150)
	maximum = 0.401 (at node 51)
Injected packet length average = 17.9712
Accepted packet length average = 18.2414
Total in-flight flits = 189729 (83030 measured)
latency change    = 5.17739
throughput change = 0.00762516
Class 0:
Packet latency average = 456.378
	minimum = 23
	maximum = 1961
Network latency average = 448.178
	minimum = 23
	maximum = 1961
Slowest packet = 15450
Flit latency average = 1571.42
	minimum = 6
	maximum = 4725
Slowest flit = 12851
Fragmentation average = 167.224
	minimum = 0
	maximum = 1551
Injected packet rate average = 0.02675
	minimum = 0.0185 (at node 28)
	maximum = 0.038 (at node 87)
Accepted packet rate average = 0.0134271
	minimum = 0.005 (at node 150)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.481448
	minimum = 0.333 (at node 28)
	maximum = 0.692 (at node 87)
Accepted flit rate average= 0.244448
	minimum = 0.107 (at node 150)
	maximum = 0.355 (at node 177)
Injected packet length average = 17.9981
Accepted packet length average = 18.2056
Total in-flight flits = 233932 (156392 measured)
latency change    = 0.616113
throughput change = 0.00313206
Class 0:
Packet latency average = 819.523
	minimum = 23
	maximum = 2927
Network latency average = 811.427
	minimum = 23
	maximum = 2927
Slowest packet = 15675
Flit latency average = 1797.6
	minimum = 6
	maximum = 5705
Slowest flit = 16065
Fragmentation average = 207.615
	minimum = 0
	maximum = 2347
Injected packet rate average = 0.0267986
	minimum = 0.0193333 (at node 28)
	maximum = 0.034 (at node 26)
Accepted packet rate average = 0.0134045
	minimum = 0.00666667 (at node 35)
	maximum = 0.0176667 (at node 119)
Injected flit rate average = 0.48247
	minimum = 0.348 (at node 28)
	maximum = 0.611667 (at node 26)
Accepted flit rate average= 0.243946
	minimum = 0.123 (at node 35)
	maximum = 0.332333 (at node 177)
Injected packet length average = 18.0036
Accepted packet length average = 18.1988
Total in-flight flits = 280239 (225222 measured)
latency change    = 0.443118
throughput change = 0.00205675
Draining remaining packets ...
Class 0:
Remaining flits: 31599 31600 31601 31602 31603 31604 31605 31606 31607 35750 [...] (241285 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (203278 flits)
Class 0:
Remaining flits: 31599 31600 31601 31602 31603 31604 31605 31606 31607 35757 [...] (202864 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (176244 flits)
Class 0:
Remaining flits: 35757 35758 35759 35760 35761 35762 35763 35764 35765 42745 [...] (164961 flits)
Measured flits: 276737 276738 276739 276740 276741 276742 276743 276744 276745 276746 [...] (146406 flits)
Class 0:
Remaining flits: 35757 35758 35759 35760 35761 35762 35763 35764 35765 42745 [...] (127658 flits)
Measured flits: 276930 276931 276932 276933 276934 276935 276936 276937 276938 276939 [...] (114064 flits)
Class 0:
Remaining flits: 35757 35758 35759 35760 35761 35762 35763 35764 35765 42745 [...] (92341 flits)
Measured flits: 276930 276931 276932 276933 276934 276935 276936 276937 276938 276939 [...] (82363 flits)
Class 0:
Remaining flits: 54286 54287 61170 61171 61172 61173 61174 61175 61176 61177 [...] (58617 flits)
Measured flits: 277001 277103 277104 277105 277106 277107 277108 277109 277110 277111 [...] (52353 flits)
Class 0:
Remaining flits: 65664 65665 65666 65667 65668 65669 65670 65671 65672 65673 [...] (26903 flits)
Measured flits: 277113 277114 277115 277116 277117 277118 277119 277120 277121 277122 [...] (24196 flits)
Class 0:
Remaining flits: 104742 104743 104744 104745 104746 104747 104748 104749 104750 104751 [...] (4333 flits)
Measured flits: 277810 277811 280378 280379 280380 280381 280382 280383 280384 280385 [...] (3896 flits)
Class 0:
Remaining flits: 449759 449760 449761 449762 449763 449764 449765 512262 512263 512264 [...] (25 flits)
Measured flits: 449759 449760 449761 449762 449763 449764 449765 512262 512263 512264 [...] (25 flits)
Time taken is 15030 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4727.64 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11563 (1 samples)
Network latency average = 4719.56 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11563 (1 samples)
Flit latency average = 4338.07 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13312 (1 samples)
Fragmentation average = 300.866 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8002 (1 samples)
Injected packet rate average = 0.0267986 (1 samples)
	minimum = 0.0193333 (1 samples)
	maximum = 0.034 (1 samples)
Accepted packet rate average = 0.0134045 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.48247 (1 samples)
	minimum = 0.348 (1 samples)
	maximum = 0.611667 (1 samples)
Accepted flit rate average = 0.243946 (1 samples)
	minimum = 0.123 (1 samples)
	maximum = 0.332333 (1 samples)
Injected packet size average = 18.0036 (1 samples)
Accepted packet size average = 18.1988 (1 samples)
Hops average = 5.0837 (1 samples)
Total run time 20.9961
