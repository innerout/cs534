BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 395.009
	minimum = 23
	maximum = 974
Network latency average = 305.924
	minimum = 23
	maximum = 930
Slowest packet = 323
Flit latency average = 259.484
	minimum = 6
	maximum = 929
Slowest flit = 7424
Fragmentation average = 55.4872
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0141042
	minimum = 0.005 (at node 44)
	maximum = 0.024 (at node 39)
Accepted packet rate average = 0.00918229
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.249979
	minimum = 0.09 (at node 44)
	maximum = 0.417 (at node 39)
Accepted flit rate average= 0.171161
	minimum = 0.036 (at node 41)
	maximum = 0.307 (at node 70)
Injected packet length average = 17.7238
Accepted packet length average = 18.6404
Total in-flight flits = 17879 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 794.544
	minimum = 23
	maximum = 1825
Network latency average = 415.274
	minimum = 23
	maximum = 1695
Slowest packet = 730
Flit latency average = 359.918
	minimum = 6
	maximum = 1665
Slowest flit = 24569
Fragmentation average = 56.0801
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0116354
	minimum = 0.004 (at node 8)
	maximum = 0.02 (at node 85)
Accepted packet rate average = 0.00913542
	minimum = 0.005 (at node 62)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.207404
	minimum = 0.072 (at node 8)
	maximum = 0.355 (at node 85)
Accepted flit rate average= 0.167635
	minimum = 0.09 (at node 62)
	maximum = 0.27 (at node 22)
Injected packet length average = 17.8252
Accepted packet length average = 18.3501
Total in-flight flits = 17780 (0 measured)
latency change    = 0.502848
throughput change = 0.021034
Class 0:
Packet latency average = 2015.63
	minimum = 1240
	maximum = 2791
Network latency average = 542.112
	minimum = 23
	maximum = 2293
Slowest packet = 3733
Flit latency average = 480.856
	minimum = 6
	maximum = 2198
Slowest flit = 29447
Fragmentation average = 59.7194
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00921354
	minimum = 0.001 (at node 90)
	maximum = 0.02 (at node 4)
Accepted packet rate average = 0.00915104
	minimum = 0.003 (at node 153)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.166167
	minimum = 0.018 (at node 91)
	maximum = 0.359 (at node 4)
Accepted flit rate average= 0.165823
	minimum = 0.054 (at node 153)
	maximum = 0.344 (at node 63)
Injected packet length average = 18.035
Accepted packet length average = 18.1207
Total in-flight flits = 17910 (0 measured)
latency change    = 0.605808
throughput change = 0.0109303
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2958.35
	minimum = 2195
	maximum = 3660
Network latency average = 340.393
	minimum = 27
	maximum = 925
Slowest packet = 6354
Flit latency average = 480.35
	minimum = 6
	maximum = 2218
Slowest flit = 67553
Fragmentation average = 52.4974
	minimum = 0
	maximum = 126
Injected packet rate average = 0.00901042
	minimum = 0 (at node 26)
	maximum = 0.022 (at node 38)
Accepted packet rate average = 0.00916146
	minimum = 0.001 (at node 190)
	maximum = 0.018 (at node 187)
Injected flit rate average = 0.162495
	minimum = 0 (at node 26)
	maximum = 0.384 (at node 38)
Accepted flit rate average= 0.163562
	minimum = 0.026 (at node 190)
	maximum = 0.312 (at node 187)
Injected packet length average = 18.0341
Accepted packet length average = 17.8533
Total in-flight flits = 17736 (16509 measured)
latency change    = 0.318665
throughput change = 0.0138199
Class 0:
Packet latency average = 3427.75
	minimum = 2195
	maximum = 4472
Network latency average = 450.163
	minimum = 23
	maximum = 1747
Slowest packet = 6354
Flit latency average = 480.188
	minimum = 6
	maximum = 2218
Slowest flit = 67553
Fragmentation average = 54.9427
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0088151
	minimum = 0.003 (at node 47)
	maximum = 0.0205 (at node 177)
Accepted packet rate average = 0.00892969
	minimum = 0.0035 (at node 163)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.158487
	minimum = 0.0475 (at node 142)
	maximum = 0.369 (at node 177)
Accepted flit rate average= 0.159977
	minimum = 0.0655 (at node 163)
	maximum = 0.279 (at node 16)
Injected packet length average = 17.979
Accepted packet length average = 17.9151
Total in-flight flits = 17535 (17517 measured)
latency change    = 0.136942
throughput change = 0.0224154
Class 0:
Packet latency average = 3877.18
	minimum = 2195
	maximum = 5429
Network latency average = 494.18
	minimum = 23
	maximum = 2506
Slowest packet = 6354
Flit latency average = 486.209
	minimum = 6
	maximum = 2406
Slowest flit = 125744
Fragmentation average = 56.0186
	minimum = 0
	maximum = 128
Injected packet rate average = 0.00878472
	minimum = 0.00333333 (at node 68)
	maximum = 0.0163333 (at node 177)
Accepted packet rate average = 0.00888542
	minimum = 0.00533333 (at node 4)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.15824
	minimum = 0.0543333 (at node 144)
	maximum = 0.294 (at node 177)
Accepted flit rate average= 0.159326
	minimum = 0.093 (at node 17)
	maximum = 0.234 (at node 16)
Injected packet length average = 18.013
Accepted packet length average = 17.9312
Total in-flight flits = 17434 (17434 measured)
latency change    = 0.115915
throughput change = 0.00408077
Class 0:
Packet latency average = 4317.23
	minimum = 2195
	maximum = 6265
Network latency average = 510.694
	minimum = 23
	maximum = 2506
Slowest packet = 6354
Flit latency average = 486.35
	minimum = 6
	maximum = 2406
Slowest flit = 125744
Fragmentation average = 56.0514
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00886198
	minimum = 0.00375 (at node 68)
	maximum = 0.01575 (at node 177)
Accepted packet rate average = 0.00892839
	minimum = 0.00475 (at node 4)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.159538
	minimum = 0.06325 (at node 68)
	maximum = 0.2835 (at node 177)
Accepted flit rate average= 0.160219
	minimum = 0.086 (at node 4)
	maximum = 0.236 (at node 129)
Injected packet length average = 18.0025
Accepted packet length average = 17.9449
Total in-flight flits = 17586 (17586 measured)
latency change    = 0.101929
throughput change = 0.00556964
Class 0:
Packet latency average = 4737.36
	minimum = 2195
	maximum = 7134
Network latency average = 517.42
	minimum = 23
	maximum = 2506
Slowest packet = 6354
Flit latency average = 485.075
	minimum = 6
	maximum = 2406
Slowest flit = 125744
Fragmentation average = 56.0741
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00889688
	minimum = 0.0038 (at node 80)
	maximum = 0.0156 (at node 177)
Accepted packet rate average = 0.00894062
	minimum = 0.006 (at node 4)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.160104
	minimum = 0.0684 (at node 80)
	maximum = 0.2808 (at node 177)
Accepted flit rate average= 0.160623
	minimum = 0.111 (at node 36)
	maximum = 0.234 (at node 129)
Injected packet length average = 17.9956
Accepted packet length average = 17.9655
Total in-flight flits = 17540 (17540 measured)
latency change    = 0.0886854
throughput change = 0.00251625
Class 0:
Packet latency average = 5151.66
	minimum = 2195
	maximum = 8060
Network latency average = 519.159
	minimum = 23
	maximum = 2923
Slowest packet = 6354
Flit latency average = 481.971
	minimum = 6
	maximum = 2894
Slowest flit = 187344
Fragmentation average = 56.3248
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00891927
	minimum = 0.0045 (at node 80)
	maximum = 0.0145 (at node 177)
Accepted packet rate average = 0.0089401
	minimum = 0.00583333 (at node 4)
	maximum = 0.0123333 (at node 129)
Injected flit rate average = 0.160453
	minimum = 0.0793333 (at node 80)
	maximum = 0.261 (at node 177)
Accepted flit rate average= 0.16064
	minimum = 0.107833 (at node 4)
	maximum = 0.222 (at node 129)
Injected packet length average = 17.9895
Accepted packet length average = 17.9684
Total in-flight flits = 17695 (17695 measured)
latency change    = 0.0804199
throughput change = 0.000104833
Class 0:
Packet latency average = 5556.91
	minimum = 2195
	maximum = 8927
Network latency average = 528.023
	minimum = 23
	maximum = 3498
Slowest packet = 6354
Flit latency average = 486.334
	minimum = 6
	maximum = 3457
Slowest flit = 214683
Fragmentation average = 56.2685
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00888393
	minimum = 0.00442857 (at node 68)
	maximum = 0.0131429 (at node 160)
Accepted packet rate average = 0.00890179
	minimum = 0.00657143 (at node 4)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.15995
	minimum = 0.0787143 (at node 68)
	maximum = 0.236571 (at node 160)
Accepted flit rate average= 0.160011
	minimum = 0.115857 (at node 135)
	maximum = 0.218429 (at node 129)
Injected packet length average = 18.0044
Accepted packet length average = 17.9752
Total in-flight flits = 17793 (17793 measured)
latency change    = 0.0729277
throughput change = 0.00392845
Draining all recorded packets ...
Class 0:
Remaining flits: 289044 289045 289046 289047 289048 289049 289050 289051 289052 289053 [...] (17606 flits)
Measured flits: 289044 289045 289046 289047 289048 289049 289050 289051 289052 289053 [...] (17606 flits)
Class 0:
Remaining flits: 328320 328321 328322 328323 328324 328325 328326 328327 328328 328329 [...] (17772 flits)
Measured flits: 328320 328321 328322 328323 328324 328325 328326 328327 328328 328329 [...] (17772 flits)
Class 0:
Remaining flits: 345096 345097 345098 345099 345100 345101 345102 345103 345104 345105 [...] (17819 flits)
Measured flits: 345096 345097 345098 345099 345100 345101 345102 345103 345104 345105 [...] (17819 flits)
Class 0:
Remaining flits: 375387 375388 375389 377091 377092 377093 377094 377095 377096 377097 [...] (17647 flits)
Measured flits: 375387 375388 375389 377091 377092 377093 377094 377095 377096 377097 [...] (17647 flits)
Class 0:
Remaining flits: 403561 403562 403563 403564 403565 403566 403567 403568 403569 403570 [...] (17675 flits)
Measured flits: 403561 403562 403563 403564 403565 403566 403567 403568 403569 403570 [...] (17675 flits)
Class 0:
Remaining flits: 426726 426727 426728 426729 426730 426731 426732 426733 426734 426735 [...] (17711 flits)
Measured flits: 426726 426727 426728 426729 426730 426731 426732 426733 426734 426735 [...] (17711 flits)
Class 0:
Remaining flits: 472477 472478 472479 472480 472481 479700 479701 479702 479703 479704 [...] (17449 flits)
Measured flits: 472477 472478 472479 472480 472481 479700 479701 479702 479703 479704 [...] (17449 flits)
Class 0:
Remaining flits: 485604 485605 485606 485607 485608 485609 485610 485611 485612 485613 [...] (17670 flits)
Measured flits: 485604 485605 485606 485607 485608 485609 485610 485611 485612 485613 [...] (17670 flits)
Class 0:
Remaining flits: 530329 530330 530331 530332 530333 531990 531991 531992 531993 531994 [...] (17117 flits)
Measured flits: 530329 530330 530331 530332 530333 531990 531991 531992 531993 531994 [...] (17117 flits)
Class 0:
Remaining flits: 560394 560395 560396 560397 560398 560399 560400 560401 560402 560403 [...] (17668 flits)
Measured flits: 560394 560395 560396 560397 560398 560399 560400 560401 560402 560403 [...] (17668 flits)
Class 0:
Remaining flits: 578250 578251 578252 578253 578254 578255 578256 578257 578258 578259 [...] (17424 flits)
Measured flits: 578250 578251 578252 578253 578254 578255 578256 578257 578258 578259 [...] (17424 flits)
Class 0:
Remaining flits: 620658 620659 620660 620661 620662 620663 620664 620665 620666 620667 [...] (17664 flits)
Measured flits: 620658 620659 620660 620661 620662 620663 620664 620665 620666 620667 [...] (17664 flits)
Class 0:
Remaining flits: 630972 630973 630974 630975 630976 630977 630978 630979 630980 630981 [...] (17634 flits)
Measured flits: 630972 630973 630974 630975 630976 630977 630978 630979 630980 630981 [...] (17634 flits)
Class 0:
Remaining flits: 679338 679339 679340 679341 679342 679343 679344 679345 679346 679347 [...] (17591 flits)
Measured flits: 679338 679339 679340 679341 679342 679343 679344 679345 679346 679347 [...] (17591 flits)
Class 0:
Remaining flits: 705564 705565 705566 705567 705568 705569 705570 705571 705572 705573 [...] (17942 flits)
Measured flits: 705564 705565 705566 705567 705568 705569 705570 705571 705572 705573 [...] (17942 flits)
Class 0:
Remaining flits: 739063 739064 739065 739066 739067 739068 739069 739070 739071 739072 [...] (17980 flits)
Measured flits: 739063 739064 739065 739066 739067 739068 739069 739070 739071 739072 [...] (17980 flits)
Class 0:
Remaining flits: 781884 781885 781886 781887 781888 781889 781890 781891 781892 781893 [...] (17868 flits)
Measured flits: 781884 781885 781886 781887 781888 781889 781890 781891 781892 781893 [...] (17868 flits)
Class 0:
Remaining flits: 797994 797995 797996 797997 797998 797999 798000 798001 798002 798003 [...] (17768 flits)
Measured flits: 797994 797995 797996 797997 797998 797999 798000 798001 798002 798003 [...] (17768 flits)
Class 0:
Remaining flits: 821478 821479 821480 821481 821482 821483 832320 832321 832322 832323 [...] (17915 flits)
Measured flits: 821478 821479 821480 821481 821482 821483 832320 832321 832322 832323 [...] (17915 flits)
Class 0:
Remaining flits: 869811 869812 869813 881905 881906 881907 881908 881909 884268 884269 [...] (17436 flits)
Measured flits: 869811 869812 869813 881905 881906 881907 881908 881909 884268 884269 [...] (17436 flits)
Class 0:
Remaining flits: 899532 899533 899534 899535 899536 899537 899538 899539 899540 899541 [...] (17938 flits)
Measured flits: 899532 899533 899534 899535 899536 899537 899538 899539 899540 899541 [...] (17938 flits)
Class 0:
Remaining flits: 919762 919763 920914 920915 922716 922717 922718 922719 922720 922721 [...] (17938 flits)
Measured flits: 919762 919763 920914 920915 922716 922717 922718 922719 922720 922721 [...] (17938 flits)
Class 0:
Remaining flits: 947772 947773 947774 947775 947776 947777 947778 947779 947780 947781 [...] (17905 flits)
Measured flits: 947772 947773 947774 947775 947776 947777 947778 947779 947780 947781 [...] (17905 flits)
Class 0:
Remaining flits: 984870 984871 984872 984873 984874 984875 984876 984877 984878 984879 [...] (17447 flits)
Measured flits: 984870 984871 984872 984873 984874 984875 984876 984877 984878 984879 [...] (17447 flits)
Class 0:
Remaining flits: 1007370 1007371 1007372 1007373 1007374 1007375 1007376 1007377 1007378 1007379 [...] (17712 flits)
Measured flits: 1007370 1007371 1007372 1007373 1007374 1007375 1007376 1007377 1007378 1007379 [...] (17712 flits)
Class 0:
Remaining flits: 1067274 1067275 1067276 1067277 1067278 1067279 1067280 1067281 1067282 1067283 [...] (17739 flits)
Measured flits: 1067274 1067275 1067276 1067277 1067278 1067279 1067280 1067281 1067282 1067283 [...] (17739 flits)
Class 0:
Remaining flits: 1079676 1079677 1079678 1079679 1079680 1079681 1079682 1079683 1079684 1079685 [...] (17556 flits)
Measured flits: 1079676 1079677 1079678 1079679 1079680 1079681 1079682 1079683 1079684 1079685 [...] (17556 flits)
Class 0:
Remaining flits: 1109322 1109323 1109324 1109325 1109326 1109327 1109328 1109329 1109330 1109331 [...] (17726 flits)
Measured flits: 1109322 1109323 1109324 1109325 1109326 1109327 1109328 1109329 1109330 1109331 [...] (17726 flits)
Class 0:
Remaining flits: 1141182 1141183 1141184 1141185 1141186 1141187 1141188 1141189 1141190 1141191 [...] (17629 flits)
Measured flits: 1141182 1141183 1141184 1141185 1141186 1141187 1141188 1141189 1141190 1141191 [...] (17629 flits)
Class 0:
Remaining flits: 1175831 1176282 1176283 1176284 1176285 1176286 1176287 1176288 1176289 1176290 [...] (17644 flits)
Measured flits: 1175831 1176282 1176283 1176284 1176285 1176286 1176287 1176288 1176289 1176290 [...] (17644 flits)
Class 0:
Remaining flits: 1212498 1212499 1212500 1212501 1212502 1212503 1212504 1212505 1212506 1212507 [...] (17803 flits)
Measured flits: 1212498 1212499 1212500 1212501 1212502 1212503 1212504 1212505 1212506 1212507 [...] (17803 flits)
Class 0:
Remaining flits: 1232703 1232704 1232705 1232706 1232707 1232708 1232709 1232710 1232711 1238048 [...] (17232 flits)
Measured flits: 1232703 1232704 1232705 1232706 1232707 1232708 1232709 1232710 1232711 1238048 [...] (17232 flits)
Class 0:
Remaining flits: 1265556 1265557 1265558 1265559 1265560 1265561 1283652 1283653 1283654 1283655 [...] (17874 flits)
Measured flits: 1265556 1265557 1265558 1265559 1265560 1265561 1283652 1283653 1283654 1283655 [...] (17874 flits)
Class 0:
Remaining flits: 1292724 1292725 1292726 1292727 1292728 1292729 1292730 1292731 1292732 1292733 [...] (17627 flits)
Measured flits: 1292724 1292725 1292726 1292727 1292728 1292729 1292730 1292731 1292732 1292733 [...] (17627 flits)
Class 0:
Remaining flits: 1334862 1334863 1334864 1334865 1334866 1334867 1334868 1334869 1334870 1334871 [...] (17819 flits)
Measured flits: 1334862 1334863 1334864 1334865 1334866 1334867 1334868 1334869 1334870 1334871 [...] (17819 flits)
Class 0:
Remaining flits: 1352054 1352055 1352056 1352057 1352058 1352059 1352060 1352061 1352062 1352063 [...] (17917 flits)
Measured flits: 1352054 1352055 1352056 1352057 1352058 1352059 1352060 1352061 1352062 1352063 [...] (17845 flits)
Class 0:
Remaining flits: 1391023 1391024 1391025 1391026 1391027 1391028 1391029 1391030 1391031 1391032 [...] (17493 flits)
Measured flits: 1391023 1391024 1391025 1391026 1391027 1391028 1391029 1391030 1391031 1391032 [...] (17356 flits)
Class 0:
Remaining flits: 1433106 1433107 1433108 1433109 1433110 1433111 1433112 1433113 1433114 1433115 [...] (17634 flits)
Measured flits: 1433106 1433107 1433108 1433109 1433110 1433111 1433112 1433113 1433114 1433115 [...] (17286 flits)
Class 0:
Remaining flits: 1439090 1439091 1439092 1439093 1439094 1439095 1439096 1439097 1439098 1439099 [...] (17642 flits)
Measured flits: 1439090 1439091 1439092 1439093 1439094 1439095 1439096 1439097 1439098 1439099 [...] (17293 flits)
Class 0:
Remaining flits: 1471175 1494162 1494163 1494164 1494165 1494166 1494167 1494168 1494169 1494170 [...] (17894 flits)
Measured flits: 1471175 1494162 1494163 1494164 1494165 1494166 1494167 1494168 1494169 1494170 [...] (16777 flits)
Class 0:
Remaining flits: 1524114 1524115 1524116 1524117 1524118 1524119 1524120 1524121 1524122 1524123 [...] (17767 flits)
Measured flits: 1524114 1524115 1524116 1524117 1524118 1524119 1524120 1524121 1524122 1524123 [...] (16431 flits)
Class 0:
Remaining flits: 1548810 1548811 1548812 1548813 1548814 1548815 1548816 1548817 1548818 1548819 [...] (17905 flits)
Measured flits: 1548810 1548811 1548812 1548813 1548814 1548815 1548816 1548817 1548818 1548819 [...] (16174 flits)
Class 0:
Remaining flits: 1585494 1585495 1585496 1585497 1585498 1585499 1585500 1585501 1585502 1585503 [...] (18048 flits)
Measured flits: 1585494 1585495 1585496 1585497 1585498 1585499 1585500 1585501 1585502 1585503 [...] (15169 flits)
Class 0:
Remaining flits: 1605492 1605493 1605494 1605495 1605496 1605497 1605498 1605499 1605500 1605501 [...] (17528 flits)
Measured flits: 1605492 1605493 1605494 1605495 1605496 1605497 1605498 1605499 1605500 1605501 [...] (13585 flits)
Class 0:
Remaining flits: 1634022 1634023 1634024 1634025 1634026 1634027 1634028 1634029 1634030 1634031 [...] (17925 flits)
Measured flits: 1634022 1634023 1634024 1634025 1634026 1634027 1634028 1634029 1634030 1634031 [...] (12394 flits)
Class 0:
Remaining flits: 1658790 1658791 1658792 1658793 1658794 1658795 1658796 1658797 1658798 1658799 [...] (17609 flits)
Measured flits: 1658790 1658791 1658792 1658793 1658794 1658795 1658796 1658797 1658798 1658799 [...] (10825 flits)
Class 0:
Remaining flits: 1699732 1699733 1699734 1699735 1699736 1699737 1699738 1699739 1705104 1705105 [...] (18143 flits)
Measured flits: 1699732 1699733 1699734 1699735 1699736 1699737 1699738 1699739 1705104 1705105 [...] (8543 flits)
Class 0:
Remaining flits: 1747800 1747801 1747802 1747803 1747804 1747805 1747806 1747807 1747808 1747809 [...] (17735 flits)
Measured flits: 1751796 1751797 1751798 1751799 1751800 1751801 1751802 1751803 1751804 1751805 [...] (6473 flits)
Class 0:
Remaining flits: 1755954 1755955 1755956 1755957 1755958 1755959 1755960 1755961 1755962 1755963 [...] (17756 flits)
Measured flits: 1781676 1781677 1781678 1781679 1781680 1781681 1781682 1781683 1781684 1781685 [...] (5535 flits)
Class 0:
Remaining flits: 1771182 1771183 1771184 1771185 1771186 1771187 1771188 1771189 1771190 1771191 [...] (17780 flits)
Measured flits: 1813756 1813757 1813758 1813759 1813760 1813761 1813762 1813763 1813764 1813765 [...] (4401 flits)
Class 0:
Remaining flits: 1826208 1826209 1826210 1826211 1826212 1826213 1826214 1826215 1826216 1826217 [...] (17571 flits)
Measured flits: 1826208 1826209 1826210 1826211 1826212 1826213 1826214 1826215 1826216 1826217 [...] (3287 flits)
Class 0:
Remaining flits: 1877358 1877359 1877360 1877361 1877362 1877363 1878480 1878481 1878482 1878483 [...] (17826 flits)
Measured flits: 1882512 1882513 1882514 1882515 1882516 1882517 1882518 1882519 1882520 1882521 [...] (2347 flits)
Class 0:
Remaining flits: 1901736 1901737 1901738 1901739 1901740 1901741 1901742 1901743 1901744 1901745 [...] (17517 flits)
Measured flits: 1903932 1903933 1903934 1903935 1903936 1903937 1903938 1903939 1903940 1903941 [...] (1445 flits)
Class 0:
Remaining flits: 1934027 1935858 1935859 1935860 1935861 1935862 1935863 1935918 1935919 1935920 [...] (17731 flits)
Measured flits: 1948302 1948303 1948304 1948305 1948306 1948307 1948308 1948309 1948310 1948311 [...] (1021 flits)
Class 0:
Remaining flits: 1956024 1956025 1956026 1956027 1956028 1956029 1956030 1956031 1956032 1956033 [...] (17694 flits)
Measured flits: 1999056 1999057 1999058 1999059 1999060 1999061 2002746 2002747 2002748 2002749 [...] (720 flits)
Class 0:
Remaining flits: 1960002 1960003 1960004 1960005 1960006 1960007 1960008 1960009 1960010 1960011 [...] (17615 flits)
Measured flits: 2016162 2016163 2016164 2016165 2016166 2016167 2016168 2016169 2016170 2016171 [...] (537 flits)
Class 0:
Remaining flits: 1999620 1999621 1999622 1999623 1999624 1999625 1999626 1999627 1999628 1999629 [...] (17408 flits)
Measured flits: 2063051 2073295 2073296 2073297 2073298 2073299 2073300 2073301 2073302 2073303 [...] (162 flits)
Class 0:
Remaining flits: 2035962 2035963 2035964 2035965 2035966 2035967 2035968 2035969 2035970 2035971 [...] (17532 flits)
Measured flits: 2099368 2099369 2099370 2099371 2099372 2099373 2099374 2099375 2100384 2100385 [...] (26 flits)
Draining remaining packets ...
Time taken is 69134 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 25391.5 (1 samples)
	minimum = 2195 (1 samples)
	maximum = 58228 (1 samples)
Network latency average = 550.843 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3498 (1 samples)
Flit latency average = 490.208 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3457 (1 samples)
Fragmentation average = 58.1088 (1 samples)
	minimum = 0 (1 samples)
	maximum = 164 (1 samples)
Injected packet rate average = 0.00888393 (1 samples)
	minimum = 0.00442857 (1 samples)
	maximum = 0.0131429 (1 samples)
Accepted packet rate average = 0.00890179 (1 samples)
	minimum = 0.00657143 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.15995 (1 samples)
	minimum = 0.0787143 (1 samples)
	maximum = 0.236571 (1 samples)
Accepted flit rate average = 0.160011 (1 samples)
	minimum = 0.115857 (1 samples)
	maximum = 0.218429 (1 samples)
Injected packet size average = 18.0044 (1 samples)
Accepted packet size average = 17.9752 (1 samples)
Hops average = 5.07106 (1 samples)
Total run time 51.948
