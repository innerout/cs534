BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.362
	minimum = 22
	maximum = 844
Network latency average = 151.279
	minimum = 22
	maximum = 696
Slowest packet = 63
Flit latency average = 123.939
	minimum = 5
	maximum = 679
Slowest flit = 14921
Fragmentation average = 25.3605
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117604
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.217516
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.4956
Total in-flight flits = 12831 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 304.892
	minimum = 22
	maximum = 1529
Network latency average = 234.15
	minimum = 22
	maximum = 1034
Slowest packet = 63
Flit latency average = 204.338
	minimum = 5
	maximum = 1017
Slowest flit = 49535
Fragmentation average = 30.5992
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125859
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.229474
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 70)
Injected packet length average = 17.9137
Accepted packet length average = 18.2326
Total in-flight flits = 19522 (0 measured)
latency change    = 0.300204
throughput change = 0.0521119
Class 0:
Packet latency average = 497.788
	minimum = 22
	maximum = 1657
Network latency average = 422.468
	minimum = 22
	maximum = 1339
Slowest packet = 3307
Flit latency average = 388.476
	minimum = 5
	maximum = 1397
Slowest flit = 85763
Fragmentation average = 36.551
	minimum = 0
	maximum = 299
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140469
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.253979
	minimum = 0.114 (at node 4)
	maximum = 0.418 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.0808
Total in-flight flits = 26738 (0 measured)
latency change    = 0.387505
throughput change = 0.0964851
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.272
	minimum = 22
	maximum = 1180
Network latency average = 314.286
	minimum = 22
	maximum = 953
Slowest packet = 9091
Flit latency average = 505.857
	minimum = 5
	maximum = 1677
Slowest flit = 105947
Fragmentation average = 26.0841
	minimum = 0
	maximum = 221
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141875
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.254
	minimum = 0.09 (at node 36)
	maximum = 0.516 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9031
Total in-flight flits = 29882 (27950 measured)
latency change    = 0.282059
throughput change = 8.2021e-05
Class 0:
Packet latency average = 580.425
	minimum = 22
	maximum = 2029
Network latency average = 501.649
	minimum = 22
	maximum = 1739
Slowest packet = 9091
Flit latency average = 550.058
	minimum = 5
	maximum = 2109
Slowest flit = 153989
Fragmentation average = 34.5317
	minimum = 0
	maximum = 315
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0141771
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.255406
	minimum = 0.153 (at node 36)
	maximum = 0.375 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0154
Total in-flight flits = 32792 (32771 measured)
latency change    = 0.331056
throughput change = 0.00550593
Class 0:
Packet latency average = 652.025
	minimum = 22
	maximum = 2326
Network latency average = 573.269
	minimum = 22
	maximum = 2007
Slowest packet = 9091
Flit latency average = 580.25
	minimum = 5
	maximum = 2109
Slowest flit = 153989
Fragmentation average = 34.4773
	minimum = 0
	maximum = 315
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143264
	minimum = 0.00866667 (at node 36)
	maximum = 0.0196667 (at node 0)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257517
	minimum = 0.156 (at node 36)
	maximum = 0.355667 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.975
Total in-flight flits = 34234 (34234 measured)
latency change    = 0.109812
throughput change = 0.00819794
Class 0:
Packet latency average = 690.819
	minimum = 22
	maximum = 2541
Network latency average = 610.316
	minimum = 22
	maximum = 2210
Slowest packet = 9091
Flit latency average = 601.554
	minimum = 5
	maximum = 2193
Slowest flit = 246473
Fragmentation average = 36.0503
	minimum = 0
	maximum = 315
Injected packet rate average = 0.0150117
	minimum = 0.0055 (at node 35)
	maximum = 0.03025 (at node 158)
Accepted packet rate average = 0.014332
	minimum = 0.0095 (at node 36)
	maximum = 0.01925 (at node 128)
Injected flit rate average = 0.27023
	minimum = 0.099 (at node 35)
	maximum = 0.54075 (at node 158)
Accepted flit rate average= 0.257956
	minimum = 0.17475 (at node 36)
	maximum = 0.34575 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 17.9985
Total in-flight flits = 36150 (36150 measured)
latency change    = 0.0561557
throughput change = 0.00169939
Class 0:
Packet latency average = 718.004
	minimum = 22
	maximum = 2541
Network latency average = 638.269
	minimum = 22
	maximum = 2317
Slowest packet = 9091
Flit latency average = 621.039
	minimum = 5
	maximum = 2300
Slowest flit = 271367
Fragmentation average = 35.7905
	minimum = 0
	maximum = 349
Injected packet rate average = 0.0151635
	minimum = 0.005 (at node 68)
	maximum = 0.0292 (at node 96)
Accepted packet rate average = 0.014351
	minimum = 0.0106 (at node 86)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.272958
	minimum = 0.09 (at node 68)
	maximum = 0.5256 (at node 96)
Accepted flit rate average= 0.258303
	minimum = 0.1908 (at node 86)
	maximum = 0.3296 (at node 128)
Injected packet length average = 18.001
Accepted packet length average = 17.9989
Total in-flight flits = 40793 (40793 measured)
latency change    = 0.0378627
throughput change = 0.00134492
Class 0:
Packet latency average = 745.303
	minimum = 22
	maximum = 2541
Network latency average = 666.254
	minimum = 22
	maximum = 2317
Slowest packet = 9091
Flit latency average = 643.582
	minimum = 5
	maximum = 2300
Slowest flit = 271367
Fragmentation average = 36.3556
	minimum = 0
	maximum = 349
Injected packet rate average = 0.0151467
	minimum = 0.0055 (at node 68)
	maximum = 0.0271667 (at node 96)
Accepted packet rate average = 0.014388
	minimum = 0.0113333 (at node 86)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.272705
	minimum = 0.099 (at node 68)
	maximum = 0.489 (at node 96)
Accepted flit rate average= 0.258953
	minimum = 0.209167 (at node 149)
	maximum = 0.321333 (at node 128)
Injected packet length average = 18.0042
Accepted packet length average = 17.9978
Total in-flight flits = 42506 (42506 measured)
latency change    = 0.0366278
throughput change = 0.00251011
Class 0:
Packet latency average = 772.166
	minimum = 22
	maximum = 2736
Network latency average = 692.942
	minimum = 22
	maximum = 2611
Slowest packet = 9091
Flit latency average = 665.987
	minimum = 5
	maximum = 2594
Slowest flit = 368567
Fragmentation average = 36.6595
	minimum = 0
	maximum = 349
Injected packet rate average = 0.0152582
	minimum = 0.007 (at node 167)
	maximum = 0.0258571 (at node 2)
Accepted packet rate average = 0.0144189
	minimum = 0.0112857 (at node 79)
	maximum = 0.0178571 (at node 138)
Injected flit rate average = 0.274633
	minimum = 0.126 (at node 167)
	maximum = 0.466286 (at node 2)
Accepted flit rate average= 0.259595
	minimum = 0.205429 (at node 79)
	maximum = 0.323714 (at node 138)
Injected packet length average = 17.9991
Accepted packet length average = 18.0038
Total in-flight flits = 46968 (46968 measured)
latency change    = 0.0347896
throughput change = 0.00247352
Draining all recorded packets ...
Class 0:
Remaining flits: 423585 423586 423587 423588 423589 423590 423591 423592 423593 425489 [...] (49735 flits)
Measured flits: 423585 423586 423587 423588 423589 423590 423591 423592 423593 425489 [...] (12473 flits)
Class 0:
Remaining flits: 505575 505576 505577 505578 505579 505580 505581 505582 505583 506214 [...] (51527 flits)
Measured flits: 505575 505576 505577 505578 505579 505580 505581 505582 505583 506214 [...] (1534 flits)
Class 0:
Remaining flits: 521924 521925 521926 521927 522030 522031 522032 522033 522034 522035 [...] (52478 flits)
Measured flits: 521924 521925 521926 521927 522030 522031 522032 522033 522034 522035 [...] (315 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 588987 588988 588989 588990 588991 588992 588993 588994 588995 597456 [...] (15811 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 662741 663385 663386 663387 663388 663389 663600 663601 663602 663603 [...] (2658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 708673 708674 708675 708676 708677 708712 708713 710334 710335 710336 [...] (565 flits)
Measured flits: (0 flits)
Time taken is 17365 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 855.265 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3873 (1 samples)
Network latency average = 774.845 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3682 (1 samples)
Flit latency average = 830.454 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3896 (1 samples)
Fragmentation average = 38.512 (1 samples)
	minimum = 0 (1 samples)
	maximum = 424 (1 samples)
Injected packet rate average = 0.0152582 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0258571 (1 samples)
Accepted packet rate average = 0.0144189 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.274633 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.466286 (1 samples)
Accepted flit rate average = 0.259595 (1 samples)
	minimum = 0.205429 (1 samples)
	maximum = 0.323714 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0038 (1 samples)
Hops average = 5.06962 (1 samples)
Total run time 11.0123
