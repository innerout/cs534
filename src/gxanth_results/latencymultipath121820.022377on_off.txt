BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 270.138
	minimum = 25
	maximum = 963
Network latency average = 163.434
	minimum = 22
	maximum = 770
Slowest packet = 21
Flit latency average = 136.829
	minimum = 5
	maximum = 753
Slowest flit = 9888
Fragmentation average = 18.2189
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0162969
	minimum = 0 (at node 108)
	maximum = 0.04 (at node 32)
Accepted packet rate average = 0.0124219
	minimum = 0.004 (at node 41)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.289823
	minimum = 0 (at node 108)
	maximum = 0.72 (at node 32)
Accepted flit rate average= 0.229151
	minimum = 0.072 (at node 41)
	maximum = 0.377 (at node 48)
Injected packet length average = 17.784
Accepted packet length average = 18.4474
Total in-flight flits = 13189 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 456.616
	minimum = 25
	maximum = 1864
Network latency average = 223.155
	minimum = 22
	maximum = 1181
Slowest packet = 21
Flit latency average = 193.457
	minimum = 5
	maximum = 1164
Slowest flit = 46673
Fragmentation average = 19.6123
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0152865
	minimum = 0.0025 (at node 180)
	maximum = 0.032 (at node 89)
Accepted packet rate average = 0.013112
	minimum = 0.0065 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.273279
	minimum = 0.045 (at node 180)
	maximum = 0.57 (at node 89)
Accepted flit rate average= 0.238115
	minimum = 0.117 (at node 116)
	maximum = 0.351 (at node 22)
Injected packet length average = 17.8772
Accepted packet length average = 18.1601
Total in-flight flits = 15448 (0 measured)
latency change    = 0.408391
throughput change = 0.0376438
Class 0:
Packet latency average = 1007.75
	minimum = 25
	maximum = 2770
Network latency average = 314.62
	minimum = 22
	maximum = 1422
Slowest packet = 4748
Flit latency average = 280.671
	minimum = 5
	maximum = 1413
Slowest flit = 85737
Fragmentation average = 21.9767
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0138073
	minimum = 0 (at node 79)
	maximum = 0.031 (at node 51)
Accepted packet rate average = 0.013651
	minimum = 0.006 (at node 91)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.248656
	minimum = 0 (at node 79)
	maximum = 0.564 (at node 51)
Accepted flit rate average= 0.246208
	minimum = 0.098 (at node 91)
	maximum = 0.462 (at node 16)
Injected packet length average = 18.0091
Accepted packet length average = 18.0359
Total in-flight flits = 16308 (0 measured)
latency change    = 0.546895
throughput change = 0.0328736
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1311.07
	minimum = 25
	maximum = 3557
Network latency average = 266.674
	minimum = 22
	maximum = 873
Slowest packet = 8687
Flit latency average = 296.863
	minimum = 5
	maximum = 1686
Slowest flit = 86758
Fragmentation average = 20.2776
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0141302
	minimum = 0 (at node 160)
	maximum = 0.036 (at node 26)
Accepted packet rate average = 0.0138958
	minimum = 0.005 (at node 36)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.253865
	minimum = 0 (at node 160)
	maximum = 0.647 (at node 26)
Accepted flit rate average= 0.250375
	minimum = 0.09 (at node 36)
	maximum = 0.441 (at node 16)
Injected packet length average = 17.9661
Accepted packet length average = 18.018
Total in-flight flits = 16854 (16750 measured)
latency change    = 0.231352
throughput change = 0.0166417
Class 0:
Packet latency average = 1564.57
	minimum = 25
	maximum = 4457
Network latency average = 308.272
	minimum = 22
	maximum = 1458
Slowest packet = 8687
Flit latency average = 299.865
	minimum = 5
	maximum = 1686
Slowest flit = 86758
Fragmentation average = 21.8376
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0141641
	minimum = 0.004 (at node 87)
	maximum = 0.0265 (at node 26)
Accepted packet rate average = 0.014
	minimum = 0.0075 (at node 4)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.254951
	minimum = 0.0745 (at node 87)
	maximum = 0.477 (at node 26)
Accepted flit rate average= 0.252568
	minimum = 0.1405 (at node 36)
	maximum = 0.4105 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 18.0406
Total in-flight flits = 17170 (17170 measured)
latency change    = 0.162026
throughput change = 0.00868167
Class 0:
Packet latency average = 1773.05
	minimum = 25
	maximum = 5159
Network latency average = 319.507
	minimum = 22
	maximum = 1458
Slowest packet = 8687
Flit latency average = 300.464
	minimum = 5
	maximum = 1686
Slowest flit = 86758
Fragmentation average = 22.359
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0141563
	minimum = 0.00533333 (at node 87)
	maximum = 0.0226667 (at node 122)
Accepted packet rate average = 0.0140694
	minimum = 0.009 (at node 36)
	maximum = 0.0193333 (at node 129)
Injected flit rate average = 0.254799
	minimum = 0.096 (at node 144)
	maximum = 0.411667 (at node 122)
Accepted flit rate average= 0.253488
	minimum = 0.162 (at node 36)
	maximum = 0.357333 (at node 129)
Injected packet length average = 17.999
Accepted packet length average = 18.0169
Total in-flight flits = 17233 (17233 measured)
latency change    = 0.117583
throughput change = 0.00362991
Class 0:
Packet latency average = 1932.37
	minimum = 25
	maximum = 5967
Network latency average = 326.735
	minimum = 22
	maximum = 1694
Slowest packet = 8687
Flit latency average = 302.842
	minimum = 5
	maximum = 1686
Slowest flit = 86758
Fragmentation average = 22.387
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0141211
	minimum = 0.0055 (at node 87)
	maximum = 0.02175 (at node 37)
Accepted packet rate average = 0.0140117
	minimum = 0.00975 (at node 89)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.254202
	minimum = 0.099 (at node 144)
	maximum = 0.38925 (at node 37)
Accepted flit rate average= 0.252125
	minimum = 0.178 (at node 89)
	maximum = 0.346 (at node 129)
Injected packet length average = 18.0016
Accepted packet length average = 17.9939
Total in-flight flits = 18156 (18156 measured)
latency change    = 0.0824501
throughput change = 0.00540544
Class 0:
Packet latency average = 2104.06
	minimum = 25
	maximum = 6738
Network latency average = 333.747
	minimum = 22
	maximum = 1694
Slowest packet = 8687
Flit latency average = 306.866
	minimum = 5
	maximum = 1686
Slowest flit = 86758
Fragmentation average = 22.4871
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0141167
	minimum = 0.007 (at node 144)
	maximum = 0.0202 (at node 37)
Accepted packet rate average = 0.0139979
	minimum = 0.0102 (at node 89)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.254086
	minimum = 0.1232 (at node 144)
	maximum = 0.3634 (at node 37)
Accepted flit rate average= 0.252211
	minimum = 0.1852 (at node 89)
	maximum = 0.3364 (at node 128)
Injected packet length average = 17.999
Accepted packet length average = 18.0178
Total in-flight flits = 18391 (18391 measured)
latency change    = 0.0815981
throughput change = 0.000342801
Class 0:
Packet latency average = 2258.8
	minimum = 25
	maximum = 7495
Network latency average = 338.25
	minimum = 22
	maximum = 1716
Slowest packet = 8687
Flit latency average = 309.699
	minimum = 5
	maximum = 1696
Slowest flit = 343295
Fragmentation average = 22.6212
	minimum = 0
	maximum = 115
Injected packet rate average = 0.014059
	minimum = 0.0085 (at node 144)
	maximum = 0.0191667 (at node 84)
Accepted packet rate average = 0.0139991
	minimum = 0.0111667 (at node 42)
	maximum = 0.0175 (at node 129)
Injected flit rate average = 0.253069
	minimum = 0.152167 (at node 144)
	maximum = 0.343167 (at node 84)
Accepted flit rate average= 0.252084
	minimum = 0.195833 (at node 42)
	maximum = 0.315833 (at node 138)
Injected packet length average = 18.0005
Accepted packet length average = 18.0071
Total in-flight flits = 17615 (17615 measured)
latency change    = 0.0685069
throughput change = 0.000504819
Class 0:
Packet latency average = 2433.56
	minimum = 25
	maximum = 7966
Network latency average = 341.137
	minimum = 22
	maximum = 1716
Slowest packet = 8687
Flit latency average = 311.397
	minimum = 5
	maximum = 1696
Slowest flit = 343295
Fragmentation average = 22.5272
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0140067
	minimum = 0.00785714 (at node 144)
	maximum = 0.0185714 (at node 65)
Accepted packet rate average = 0.0139345
	minimum = 0.011 (at node 79)
	maximum = 0.0175714 (at node 138)
Injected flit rate average = 0.252094
	minimum = 0.141429 (at node 144)
	maximum = 0.333429 (at node 65)
Accepted flit rate average= 0.250914
	minimum = 0.199286 (at node 79)
	maximum = 0.318571 (at node 138)
Injected packet length average = 17.9981
Accepted packet length average = 18.0067
Total in-flight flits = 18163 (18163 measured)
latency change    = 0.0718103
throughput change = 0.00466202
Draining all recorded packets ...
Class 0:
Remaining flits: 454158 454159 454160 454161 454162 454163 454164 454165 454166 454167 [...] (18486 flits)
Measured flits: 454158 454159 454160 454161 454162 454163 454164 454165 454166 454167 [...] (17462 flits)
Class 0:
Remaining flits: 530046 530047 530048 530049 530050 530051 530052 530053 530054 530055 [...] (18022 flits)
Measured flits: 530046 530047 530048 530049 530050 530051 530052 530053 530054 530055 [...] (15506 flits)
Class 0:
Remaining flits: 556722 556723 556724 556725 556726 556727 556728 556729 556730 556731 [...] (18088 flits)
Measured flits: 556722 556723 556724 556725 556726 556727 556728 556729 556730 556731 [...] (13484 flits)
Class 0:
Remaining flits: 598158 598159 598160 598161 598162 598163 598164 598165 598166 598167 [...] (18684 flits)
Measured flits: 598158 598159 598160 598161 598162 598163 598164 598165 598166 598167 [...] (12861 flits)
Class 0:
Remaining flits: 659520 659521 659522 659523 659524 659525 659526 659527 659528 659529 [...] (18340 flits)
Measured flits: 659520 659521 659522 659523 659524 659525 659526 659527 659528 659529 [...] (10955 flits)
Class 0:
Remaining flits: 714510 714511 714512 714513 714514 714515 714516 714517 714518 714519 [...] (18124 flits)
Measured flits: 716004 716005 716006 716007 716008 716009 716010 716011 716012 716013 [...] (8856 flits)
Class 0:
Remaining flits: 757853 768220 768221 771291 771292 771293 771294 771295 771296 771297 [...] (18197 flits)
Measured flits: 768220 768221 771291 771292 771293 771294 771295 771296 771297 771298 [...] (7039 flits)
Class 0:
Remaining flits: 801522 801523 801524 801525 801526 801527 801528 801529 801530 801531 [...] (18472 flits)
Measured flits: 820980 820981 820982 820983 820984 820985 820986 820987 820988 820989 [...] (5666 flits)
Class 0:
Remaining flits: 839754 839755 839756 839757 839758 839759 839760 839761 839762 839763 [...] (18189 flits)
Measured flits: 886667 886668 886669 886670 886671 886672 886673 886674 886675 886676 [...] (4455 flits)
Class 0:
Remaining flits: 896976 896977 896978 896979 896980 896981 896982 896983 896984 896985 [...] (18137 flits)
Measured flits: 896976 896977 896978 896979 896980 896981 896982 896983 896984 896985 [...] (3032 flits)
Class 0:
Remaining flits: 904248 904249 904250 904251 904252 904253 904254 904255 904256 904257 [...] (18431 flits)
Measured flits: 904248 904249 904250 904251 904252 904253 904254 904255 904256 904257 [...] (1925 flits)
Class 0:
Remaining flits: 1000404 1000405 1000406 1000407 1000408 1000409 1000410 1000411 1000412 1000413 [...] (18428 flits)
Measured flits: 1029906 1029907 1029908 1029909 1029910 1029911 1029912 1029913 1029914 1029915 [...] (1009 flits)
Class 0:
Remaining flits: 1039302 1039303 1039304 1039305 1039306 1039307 1039308 1039309 1039310 1039311 [...] (18438 flits)
Measured flits: 1061694 1061695 1061696 1061697 1061698 1061699 1061700 1061701 1061702 1061703 [...] (823 flits)
Class 0:
Remaining flits: 1092632 1092633 1092634 1092635 1098846 1098847 1098848 1098849 1098850 1098851 [...] (17841 flits)
Measured flits: 1127286 1127287 1127288 1127289 1127290 1127291 1127292 1127293 1127294 1127295 [...] (621 flits)
Class 0:
Remaining flits: 1148112 1148113 1148114 1148115 1148116 1148117 1148118 1148119 1148120 1148121 [...] (18550 flits)
Measured flits: 1190790 1190791 1190792 1190793 1190794 1190795 1190796 1190797 1190798 1190799 [...] (357 flits)
Class 0:
Remaining flits: 1197954 1197955 1197956 1197957 1197958 1197959 1197960 1197961 1197962 1197963 [...] (18260 flits)
Measured flits: 1230804 1230805 1230806 1230807 1230808 1230809 1230810 1230811 1230812 1230813 [...] (180 flits)
Class 0:
Remaining flits: 1213569 1213570 1213571 1213572 1213573 1213574 1213575 1213576 1213577 1250064 [...] (18819 flits)
Measured flits: 1274742 1274743 1274744 1274745 1274746 1274747 1274748 1274749 1274750 1274751 [...] (90 flits)
Class 0:
Remaining flits: 1286753 1286754 1286755 1286756 1286757 1286758 1286759 1286760 1286761 1286762 [...] (18622 flits)
Measured flits: 1340406 1340407 1340408 1340409 1340410 1340411 1340412 1340413 1340414 1340415 [...] (126 flits)
Class 0:
Remaining flits: 1351327 1351328 1351329 1351330 1351331 1353762 1353763 1353764 1353765 1353766 [...] (18622 flits)
Measured flits: 1385364 1385365 1385366 1385367 1385368 1385369 1386216 1386217 1386218 1386219 [...] (96 flits)
Class 0:
Remaining flits: 1383002 1383003 1383004 1383005 1383006 1383007 1383008 1383009 1383010 1383011 [...] (18370 flits)
Measured flits: 1429148 1429149 1429150 1429151 1429152 1429153 1429154 1429155 1429156 1429157 [...] (106 flits)
Class 0:
Remaining flits: 1448622 1448623 1448624 1448625 1448626 1448627 1448628 1448629 1448630 1448631 [...] (18612 flits)
Measured flits: 1485540 1485541 1485542 1485543 1485544 1485545 1485546 1485547 1485548 1485549 [...] (108 flits)
Class 0:
Remaining flits: 1474575 1474576 1474577 1475226 1475227 1475228 1475229 1475230 1475231 1475232 [...] (18408 flits)
Measured flits: 1529496 1529497 1529498 1529499 1529500 1529501 1529502 1529503 1529504 1529505 [...] (90 flits)
Draining remaining packets ...
Time taken is 33656 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4508.23 (1 samples)
	minimum = 25 (1 samples)
	maximum = 23665 (1 samples)
Network latency average = 354.625 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2987 (1 samples)
Flit latency average = 325.391 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2970 (1 samples)
Fragmentation average = 22.5564 (1 samples)
	minimum = 0 (1 samples)
	maximum = 139 (1 samples)
Injected packet rate average = 0.0140067 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.0185714 (1 samples)
Accepted packet rate average = 0.0139345 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0175714 (1 samples)
Injected flit rate average = 0.252094 (1 samples)
	minimum = 0.141429 (1 samples)
	maximum = 0.333429 (1 samples)
Accepted flit rate average = 0.250914 (1 samples)
	minimum = 0.199286 (1 samples)
	maximum = 0.318571 (1 samples)
Injected packet size average = 17.9981 (1 samples)
Accepted packet size average = 18.0067 (1 samples)
Hops average = 5.05896 (1 samples)
Total run time 31.2326
