BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.6
	minimum = 22
	maximum = 864
Network latency average = 289.344
	minimum = 22
	maximum = 793
Slowest packet = 463
Flit latency average = 252.15
	minimum = 5
	maximum = 807
Slowest flit = 18358
Fragmentation average = 80.7835
	minimum = 0
	maximum = 524
Injected packet rate average = 0.0333177
	minimum = 0.021 (at node 76)
	maximum = 0.049 (at node 167)
Accepted packet rate average = 0.014
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.594911
	minimum = 0.376 (at node 76)
	maximum = 0.882 (at node 167)
Accepted flit rate average= 0.272693
	minimum = 0.072 (at node 174)
	maximum = 0.438 (at node 88)
Injected packet length average = 17.8557
Accepted packet length average = 19.4781
Total in-flight flits = 62987 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 570.58
	minimum = 22
	maximum = 1627
Network latency average = 545.787
	minimum = 22
	maximum = 1617
Slowest packet = 2392
Flit latency average = 498.11
	minimum = 5
	maximum = 1600
Slowest flit = 43073
Fragmentation average = 101.914
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0293802
	minimum = 0.0165 (at node 76)
	maximum = 0.0395 (at node 167)
Accepted packet rate average = 0.0148438
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 56)
Injected flit rate average = 0.525568
	minimum = 0.2925 (at node 76)
	maximum = 0.704 (at node 167)
Accepted flit rate average= 0.277781
	minimum = 0.1575 (at node 153)
	maximum = 0.4035 (at node 152)
Injected packet length average = 17.8885
Accepted packet length average = 18.7137
Total in-flight flits = 96768 (0 measured)
latency change    = 0.469663
throughput change = 0.0183185
Class 0:
Packet latency average = 1423.85
	minimum = 27
	maximum = 2310
Network latency average = 1311.06
	minimum = 22
	maximum = 2308
Slowest packet = 4169
Flit latency average = 1262.22
	minimum = 5
	maximum = 2398
Slowest flit = 70305
Fragmentation average = 128.166
	minimum = 0
	maximum = 724
Injected packet rate average = 0.0159948
	minimum = 0 (at node 55)
	maximum = 0.031 (at node 74)
Accepted packet rate average = 0.0150052
	minimum = 0.005 (at node 61)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.288073
	minimum = 0 (at node 55)
	maximum = 0.565 (at node 74)
Accepted flit rate average= 0.268844
	minimum = 0.099 (at node 132)
	maximum = 0.453 (at node 159)
Injected packet length average = 18.0104
Accepted packet length average = 17.9167
Total in-flight flits = 100770 (0 measured)
latency change    = 0.59927
throughput change = 0.0332442
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1169.53
	minimum = 105
	maximum = 2283
Network latency average = 77.2939
	minimum = 22
	maximum = 942
Slowest packet = 14404
Flit latency average = 1640.57
	minimum = 5
	maximum = 3045
Slowest flit = 84972
Fragmentation average = 8.39474
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0155417
	minimum = 0.001 (at node 66)
	maximum = 0.031 (at node 55)
Accepted packet rate average = 0.0149271
	minimum = 0.006 (at node 71)
	maximum = 0.027 (at node 78)
Injected flit rate average = 0.28026
	minimum = 0.022 (at node 66)
	maximum = 0.563 (at node 142)
Accepted flit rate average= 0.266953
	minimum = 0.112 (at node 4)
	maximum = 0.492 (at node 78)
Injected packet length average = 18.0328
Accepted packet length average = 17.8838
Total in-flight flits = 103047 (49333 measured)
latency change    = 0.217457
throughput change = 0.00708224
Class 0:
Packet latency average = 2063.63
	minimum = 105
	maximum = 3681
Network latency average = 684.418
	minimum = 22
	maximum = 1970
Slowest packet = 14404
Flit latency average = 1739.74
	minimum = 5
	maximum = 3786
Slowest flit = 96389
Fragmentation average = 50.2306
	minimum = 0
	maximum = 576
Injected packet rate average = 0.0150885
	minimum = 0.0025 (at node 131)
	maximum = 0.025 (at node 142)
Accepted packet rate average = 0.0148906
	minimum = 0.009 (at node 79)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.271773
	minimum = 0.0425 (at node 131)
	maximum = 0.452 (at node 142)
Accepted flit rate average= 0.267961
	minimum = 0.1715 (at node 122)
	maximum = 0.3935 (at node 128)
Injected packet length average = 18.0119
Accepted packet length average = 17.9953
Total in-flight flits = 102219 (86017 measured)
latency change    = 0.433266
throughput change = 0.00376104
Class 0:
Packet latency average = 2770.8
	minimum = 105
	maximum = 4385
Network latency average = 1348.68
	minimum = 22
	maximum = 2973
Slowest packet = 14404
Flit latency average = 1806.4
	minimum = 5
	maximum = 4429
Slowest flit = 106145
Fragmentation average = 85.6152
	minimum = 0
	maximum = 672
Injected packet rate average = 0.0153576
	minimum = 0.00833333 (at node 20)
	maximum = 0.0226667 (at node 46)
Accepted packet rate average = 0.0149774
	minimum = 0.0103333 (at node 64)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.276656
	minimum = 0.153 (at node 20)
	maximum = 0.412 (at node 46)
Accepted flit rate average= 0.268793
	minimum = 0.18 (at node 64)
	maximum = 0.368 (at node 128)
Injected packet length average = 18.0142
Accepted packet length average = 17.9466
Total in-flight flits = 105281 (103627 measured)
latency change    = 0.255223
throughput change = 0.00309705
Draining remaining packets ...
Class 0:
Remaining flits: 198612 198613 198614 198615 198616 198617 198618 198619 198620 198621 [...] (57591 flits)
Measured flits: 259668 259669 259670 259671 259672 259673 259674 259675 259676 259677 [...] (57414 flits)
Class 0:
Remaining flits: 258325 258326 258327 258328 258329 258330 258331 258332 258333 258334 [...] (10693 flits)
Measured flits: 285588 285589 285590 285591 285592 285593 285594 285595 285596 285597 [...] (10682 flits)
Time taken is 8649 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3685.87 (1 samples)
	minimum = 105 (1 samples)
	maximum = 6324 (1 samples)
Network latency average = 1981.26 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4895 (1 samples)
Flit latency average = 1978.79 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5769 (1 samples)
Fragmentation average = 101.009 (1 samples)
	minimum = 0 (1 samples)
	maximum = 726 (1 samples)
Injected packet rate average = 0.0153576 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.0226667 (1 samples)
Accepted packet rate average = 0.0149774 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.276656 (1 samples)
	minimum = 0.153 (1 samples)
	maximum = 0.412 (1 samples)
Accepted flit rate average = 0.268793 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.368 (1 samples)
Injected packet size average = 18.0142 (1 samples)
Accepted packet size average = 17.9466 (1 samples)
Hops average = 5.12528 (1 samples)
Total run time 13.7816
