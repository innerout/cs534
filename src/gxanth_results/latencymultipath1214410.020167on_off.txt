BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.755
	minimum = 27
	maximum = 972
Network latency average = 244.012
	minimum = 23
	maximum = 815
Slowest packet = 3
Flit latency average = 205.827
	minimum = 6
	maximum = 884
Slowest flit = 8464
Fragmentation average = 55.318
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0187604
	minimum = 0 (at node 114)
	maximum = 0.052 (at node 186)
Accepted packet rate average = 0.00905729
	minimum = 0.002 (at node 150)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.334646
	minimum = 0 (at node 114)
	maximum = 0.92 (at node 186)
Accepted flit rate average= 0.17099
	minimum = 0.051 (at node 150)
	maximum = 0.288 (at node 34)
Injected packet length average = 17.8379
Accepted packet length average = 18.8787
Total in-flight flits = 32042 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 523.997
	minimum = 25
	maximum = 1901
Network latency average = 442.818
	minimum = 23
	maximum = 1704
Slowest packet = 3
Flit latency average = 400.403
	minimum = 6
	maximum = 1753
Slowest flit = 16259
Fragmentation average = 62.9628
	minimum = 0
	maximum = 150
Injected packet rate average = 0.018151
	minimum = 0.0005 (at node 140)
	maximum = 0.04 (at node 150)
Accepted packet rate average = 0.00946094
	minimum = 0.0045 (at node 174)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.325409
	minimum = 0.009 (at node 140)
	maximum = 0.7185 (at node 150)
Accepted flit rate average= 0.173771
	minimum = 0.081 (at node 174)
	maximum = 0.261 (at node 22)
Injected packet length average = 17.9278
Accepted packet length average = 18.3672
Total in-flight flits = 58894 (0 measured)
latency change    = 0.42222
throughput change = 0.0160053
Class 0:
Packet latency average = 1151.6
	minimum = 29
	maximum = 2932
Network latency average = 1018.65
	minimum = 23
	maximum = 2673
Slowest packet = 2306
Flit latency average = 985.828
	minimum = 6
	maximum = 2715
Slowest flit = 18314
Fragmentation average = 70.7106
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0157552
	minimum = 0 (at node 27)
	maximum = 0.051 (at node 171)
Accepted packet rate average = 0.00944792
	minimum = 0.003 (at node 32)
	maximum = 0.017 (at node 102)
Injected flit rate average = 0.283469
	minimum = 0 (at node 27)
	maximum = 0.923 (at node 171)
Accepted flit rate average= 0.17101
	minimum = 0.064 (at node 190)
	maximum = 0.296 (at node 102)
Injected packet length average = 17.9921
Accepted packet length average = 18.1003
Total in-flight flits = 80870 (0 measured)
latency change    = 0.544982
throughput change = 0.0161418
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 424.208
	minimum = 27
	maximum = 2713
Network latency average = 234.284
	minimum = 23
	maximum = 984
Slowest packet = 10038
Flit latency average = 1427.9
	minimum = 6
	maximum = 3470
Slowest flit = 32542
Fragmentation average = 32.5127
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0145
	minimum = 0 (at node 31)
	maximum = 0.045 (at node 165)
Accepted packet rate average = 0.00948958
	minimum = 0.003 (at node 72)
	maximum = 0.018 (at node 99)
Injected flit rate average = 0.260562
	minimum = 0 (at node 63)
	maximum = 0.807 (at node 165)
Accepted flit rate average= 0.169687
	minimum = 0.054 (at node 117)
	maximum = 0.312 (at node 99)
Injected packet length average = 17.9698
Accepted packet length average = 17.8814
Total in-flight flits = 98870 (46078 measured)
latency change    = 1.7147
throughput change = 0.00779619
Class 0:
Packet latency average = 1000.98
	minimum = 27
	maximum = 3705
Network latency average = 667.272
	minimum = 23
	maximum = 1974
Slowest packet = 10038
Flit latency average = 1643.28
	minimum = 6
	maximum = 4352
Slowest flit = 33713
Fragmentation average = 52.068
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0134635
	minimum = 0 (at node 99)
	maximum = 0.0325 (at node 165)
Accepted packet rate average = 0.00938542
	minimum = 0.0055 (at node 58)
	maximum = 0.0165 (at node 120)
Injected flit rate average = 0.242284
	minimum = 0 (at node 99)
	maximum = 0.5895 (at node 165)
Accepted flit rate average= 0.168932
	minimum = 0.093 (at node 138)
	maximum = 0.2945 (at node 120)
Injected packet length average = 17.9956
Accepted packet length average = 17.9994
Total in-flight flits = 110032 (78549 measured)
latency change    = 0.576207
throughput change = 0.00447048
Class 0:
Packet latency average = 1522.9
	minimum = 27
	maximum = 5174
Network latency average = 1137.33
	minimum = 23
	maximum = 2956
Slowest packet = 10038
Flit latency average = 1847.01
	minimum = 6
	maximum = 5274
Slowest flit = 45431
Fragmentation average = 60.9241
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0124601
	minimum = 0.00166667 (at node 140)
	maximum = 0.025 (at node 130)
Accepted packet rate average = 0.00931771
	minimum = 0.00533333 (at node 2)
	maximum = 0.0136667 (at node 50)
Injected flit rate average = 0.224361
	minimum = 0.03 (at node 161)
	maximum = 0.45 (at node 130)
Accepted flit rate average= 0.167748
	minimum = 0.092 (at node 138)
	maximum = 0.245333 (at node 50)
Injected packet length average = 18.0064
Accepted packet length average = 18.0032
Total in-flight flits = 114819 (97539 measured)
latency change    = 0.342716
throughput change = 0.00705836
Class 0:
Packet latency average = 2031.77
	minimum = 25
	maximum = 5990
Network latency average = 1559.53
	minimum = 23
	maximum = 3910
Slowest packet = 10038
Flit latency average = 2060.37
	minimum = 6
	maximum = 6072
Slowest flit = 56861
Fragmentation average = 64.0411
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0116159
	minimum = 0.0015 (at node 168)
	maximum = 0.023 (at node 74)
Accepted packet rate average = 0.00923307
	minimum = 0.005 (at node 2)
	maximum = 0.0125 (at node 66)
Injected flit rate average = 0.209103
	minimum = 0.02775 (at node 168)
	maximum = 0.412 (at node 74)
Accepted flit rate average= 0.166293
	minimum = 0.09 (at node 2)
	maximum = 0.22975 (at node 66)
Injected packet length average = 18.0015
Accepted packet length average = 18.0106
Total in-flight flits = 115211 (106894 measured)
latency change    = 0.250458
throughput change = 0.00875139
Class 0:
Packet latency average = 2523.14
	minimum = 25
	maximum = 6889
Network latency average = 1942.54
	minimum = 23
	maximum = 4942
Slowest packet = 10038
Flit latency average = 2251.65
	minimum = 6
	maximum = 6889
Slowest flit = 61235
Fragmentation average = 66.5714
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0110417
	minimum = 0.0024 (at node 140)
	maximum = 0.019 (at node 74)
Accepted packet rate average = 0.00919896
	minimum = 0.006 (at node 2)
	maximum = 0.0124 (at node 152)
Injected flit rate average = 0.19875
	minimum = 0.0436 (at node 140)
	maximum = 0.3388 (at node 74)
Accepted flit rate average= 0.165444
	minimum = 0.108 (at node 2)
	maximum = 0.222 (at node 152)
Injected packet length average = 18
Accepted packet length average = 17.9851
Total in-flight flits = 114536 (110780 measured)
latency change    = 0.194743
throughput change = 0.00513298
Class 0:
Packet latency average = 2936.86
	minimum = 25
	maximum = 7968
Network latency average = 2226.96
	minimum = 23
	maximum = 5915
Slowest packet = 10038
Flit latency average = 2403.81
	minimum = 6
	maximum = 7494
Slowest flit = 85931
Fragmentation average = 67.8641
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0107092
	minimum = 0.00233333 (at node 140)
	maximum = 0.0188333 (at node 114)
Accepted packet rate average = 0.00911458
	minimum = 0.006 (at node 36)
	maximum = 0.0125 (at node 128)
Injected flit rate average = 0.192697
	minimum = 0.0423333 (at node 140)
	maximum = 0.337667 (at node 114)
Accepted flit rate average= 0.164174
	minimum = 0.107667 (at node 36)
	maximum = 0.224167 (at node 128)
Injected packet length average = 17.9936
Accepted packet length average = 18.0122
Total in-flight flits = 115212 (113567 measured)
latency change    = 0.140871
throughput change = 0.00773656
Class 0:
Packet latency average = 3338.89
	minimum = 25
	maximum = 8604
Network latency average = 2466.31
	minimum = 23
	maximum = 6886
Slowest packet = 10038
Flit latency average = 2544.54
	minimum = 6
	maximum = 8410
Slowest flit = 95651
Fragmentation average = 69.0313
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0105811
	minimum = 0.003 (at node 140)
	maximum = 0.0185714 (at node 114)
Accepted packet rate average = 0.00909152
	minimum = 0.00657143 (at node 2)
	maximum = 0.0124286 (at node 128)
Injected flit rate average = 0.190403
	minimum = 0.0542857 (at node 140)
	maximum = 0.334286 (at node 114)
Accepted flit rate average= 0.163702
	minimum = 0.119286 (at node 2)
	maximum = 0.223714 (at node 128)
Injected packet length average = 17.9947
Accepted packet length average = 18.006
Total in-flight flits = 118327 (117717 measured)
latency change    = 0.120411
throughput change = 0.00288314
Draining all recorded packets ...
Class 0:
Remaining flits: 115902 115903 115904 115905 115906 115907 115908 115909 115910 115911 [...] (118294 flits)
Measured flits: 184806 184807 184808 184809 184810 184811 184812 184813 184814 184815 [...] (115369 flits)
Class 0:
Remaining flits: 140112 140113 140114 140115 140116 140117 140118 140119 140120 140121 [...] (118282 flits)
Measured flits: 191946 191947 191948 191949 191950 191951 207432 207433 207434 207435 [...] (111955 flits)
Class 0:
Remaining flits: 210564 210565 210566 210567 210568 210569 210570 210571 210572 210573 [...] (120584 flits)
Measured flits: 210564 210565 210566 210567 210568 210569 210570 210571 210572 210573 [...] (109894 flits)
Class 0:
Remaining flits: 212058 212059 212060 212061 212062 212063 212064 212065 212066 212067 [...] (120864 flits)
Measured flits: 212058 212059 212060 212061 212062 212063 212064 212065 212066 212067 [...] (106087 flits)
Class 0:
Remaining flits: 236844 236845 236846 236847 236848 236849 236850 236851 236852 236853 [...] (120147 flits)
Measured flits: 236844 236845 236846 236847 236848 236849 236850 236851 236852 236853 [...] (99454 flits)
Class 0:
Remaining flits: 272898 272899 272900 272901 272902 272903 272904 272905 272906 272907 [...] (120439 flits)
Measured flits: 272898 272899 272900 272901 272902 272903 272904 272905 272906 272907 [...] (94705 flits)
Class 0:
Remaining flits: 302562 302563 302564 302565 302566 302567 302568 302569 302570 302571 [...] (119127 flits)
Measured flits: 302562 302563 302564 302565 302566 302567 302568 302569 302570 302571 [...] (86632 flits)
Class 0:
Remaining flits: 349218 349219 349220 349221 349222 349223 349224 349225 349226 349227 [...] (118595 flits)
Measured flits: 349218 349219 349220 349221 349222 349223 349224 349225 349226 349227 [...] (75603 flits)
Class 0:
Remaining flits: 363564 363565 363566 363567 363568 363569 363570 363571 363572 363573 [...] (118290 flits)
Measured flits: 363564 363565 363566 363567 363568 363569 363570 363571 363572 363573 [...] (67318 flits)
Class 0:
Remaining flits: 367344 367345 367346 367347 367348 367349 367350 367351 367352 367353 [...] (119332 flits)
Measured flits: 367344 367345 367346 367347 367348 367349 367350 367351 367352 367353 [...] (59968 flits)
Class 0:
Remaining flits: 387432 387433 387434 387435 387436 387437 387438 387439 387440 387441 [...] (119870 flits)
Measured flits: 387432 387433 387434 387435 387436 387437 387438 387439 387440 387441 [...] (53408 flits)
Class 0:
Remaining flits: 387432 387433 387434 387435 387436 387437 387438 387439 387440 387441 [...] (119428 flits)
Measured flits: 387432 387433 387434 387435 387436 387437 387438 387439 387440 387441 [...] (44786 flits)
Class 0:
Remaining flits: 473976 473977 473978 473979 473980 473981 473982 473983 473984 473985 [...] (118546 flits)
Measured flits: 473976 473977 473978 473979 473980 473981 473982 473983 473984 473985 [...] (37358 flits)
Class 0:
Remaining flits: 473976 473977 473978 473979 473980 473981 473982 473983 473984 473985 [...] (117650 flits)
Measured flits: 473976 473977 473978 473979 473980 473981 473982 473983 473984 473985 [...] (30211 flits)
Class 0:
Remaining flits: 474588 474589 474590 474591 474592 474593 474594 474595 474596 474597 [...] (117983 flits)
Measured flits: 474588 474589 474590 474591 474592 474593 474594 474595 474596 474597 [...] (24923 flits)
Class 0:
Remaining flits: 513450 513451 513452 513453 513454 513455 513456 513457 513458 513459 [...] (119741 flits)
Measured flits: 513450 513451 513452 513453 513454 513455 513456 513457 513458 513459 [...] (20401 flits)
Class 0:
Remaining flits: 513456 513457 513458 513459 513460 513461 513462 513463 513464 513465 [...] (118746 flits)
Measured flits: 513456 513457 513458 513459 513460 513461 513462 513463 513464 513465 [...] (17104 flits)
Class 0:
Remaining flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (120062 flits)
Measured flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (12668 flits)
Class 0:
Remaining flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (122055 flits)
Measured flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (9591 flits)
Class 0:
Remaining flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (119772 flits)
Measured flits: 615024 615025 615026 615027 615028 615029 615030 615031 615032 615033 [...] (6652 flits)
Class 0:
Remaining flits: 673920 673921 673922 673923 673924 673925 673926 673927 673928 673929 [...] (119049 flits)
Measured flits: 700362 700363 700364 700365 700366 700367 700368 700369 700370 700371 [...] (5091 flits)
Class 0:
Remaining flits: 727956 727957 727958 727959 727960 727961 727962 727963 727964 727965 [...] (117024 flits)
Measured flits: 763308 763309 763310 763311 763312 763313 763314 763315 763316 763317 [...] (3402 flits)
Class 0:
Remaining flits: 764388 764389 764390 764391 764392 764393 764394 764395 764396 764397 [...] (117830 flits)
Measured flits: 764388 764389 764390 764391 764392 764393 764394 764395 764396 764397 [...] (2208 flits)
Class 0:
Remaining flits: 766296 766297 766298 766299 766300 766301 766302 766303 766304 766305 [...] (117381 flits)
Measured flits: 766296 766297 766298 766299 766300 766301 766302 766303 766304 766305 [...] (1396 flits)
Class 0:
Remaining flits: 848862 848863 848864 848865 848866 848867 848868 848869 848870 848871 [...] (118574 flits)
Measured flits: 887706 887707 887708 887709 887710 887711 887712 887713 887714 887715 [...] (684 flits)
Class 0:
Remaining flits: 857502 857503 857504 857505 857506 857507 857508 857509 857510 857511 [...] (118138 flits)
Measured flits: 944154 944155 944156 944157 944158 944159 944160 944161 944162 944163 [...] (443 flits)
Class 0:
Remaining flits: 859626 859627 859628 859629 859630 859631 859632 859633 859634 859635 [...] (116924 flits)
Measured flits: 1019070 1019071 1019072 1019073 1019074 1019075 1019076 1019077 1019078 1019079 [...] (276 flits)
Class 0:
Remaining flits: 859626 859627 859628 859629 859630 859631 859632 859633 859634 859635 [...] (117832 flits)
Measured flits: 1059822 1059823 1059824 1059825 1059826 1059827 1059828 1059829 1059830 1059831 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 948510 948511 948512 948513 948514 948515 948516 948517 948518 948519 [...] (89271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951660 951661 951662 951663 951664 951665 951666 951667 951668 951669 [...] (60364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 992340 992341 992342 992343 992344 992345 992346 992347 992348 992349 [...] (33397 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1063098 1063099 1063100 1063101 1063102 1063103 1063104 1063105 1063106 1063107 [...] (10562 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1146510 1146511 1146512 1146513 1146514 1146515 1146516 1146517 1146518 1146519 [...] (545 flits)
Measured flits: (0 flits)
Time taken is 43828 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8448.82 (1 samples)
	minimum = 25 (1 samples)
	maximum = 28486 (1 samples)
Network latency average = 3662.09 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15138 (1 samples)
Flit latency average = 3632.4 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15118 (1 samples)
Fragmentation average = 72.1374 (1 samples)
	minimum = 0 (1 samples)
	maximum = 184 (1 samples)
Injected packet rate average = 0.0105811 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.0185714 (1 samples)
Accepted packet rate average = 0.00909152 (1 samples)
	minimum = 0.00657143 (1 samples)
	maximum = 0.0124286 (1 samples)
Injected flit rate average = 0.190403 (1 samples)
	minimum = 0.0542857 (1 samples)
	maximum = 0.334286 (1 samples)
Accepted flit rate average = 0.163702 (1 samples)
	minimum = 0.119286 (1 samples)
	maximum = 0.223714 (1 samples)
Injected packet size average = 17.9947 (1 samples)
Accepted packet size average = 18.006 (1 samples)
Hops average = 5.05485 (1 samples)
Total run time 34.0252
