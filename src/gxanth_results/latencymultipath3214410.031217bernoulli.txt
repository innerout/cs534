BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.61
	minimum = 23
	maximum = 969
Network latency average = 294.178
	minimum = 23
	maximum = 942
Slowest packet = 191
Flit latency average = 232.147
	minimum = 6
	maximum = 960
Slowest flit = 2812
Fragmentation average = 151.974
	minimum = 0
	maximum = 796
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.010599
	minimum = 0.004 (at node 174)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.219641
	minimum = 0.083 (at node 174)
	maximum = 0.371 (at node 152)
Injected packet length average = 17.8483
Accepted packet length average = 20.7229
Total in-flight flits = 64281 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 562.846
	minimum = 23
	maximum = 1822
Network latency average = 552.723
	minimum = 23
	maximum = 1822
Slowest packet = 937
Flit latency average = 474.814
	minimum = 6
	maximum = 1856
Slowest flit = 12353
Fragmentation average = 188.449
	minimum = 0
	maximum = 1697
Injected packet rate average = 0.0304427
	minimum = 0.0225 (at node 100)
	maximum = 0.045 (at node 83)
Accepted packet rate average = 0.0119063
	minimum = 0.0065 (at node 174)
	maximum = 0.0175 (at node 22)
Injected flit rate average = 0.545555
	minimum = 0.405 (at node 100)
	maximum = 0.8045 (at node 83)
Accepted flit rate average= 0.22725
	minimum = 0.13 (at node 115)
	maximum = 0.3305 (at node 152)
Injected packet length average = 17.9207
Accepted packet length average = 19.0866
Total in-flight flits = 123192 (0 measured)
latency change    = 0.462357
throughput change = 0.0334846
Class 0:
Packet latency average = 1315.07
	minimum = 26
	maximum = 2812
Network latency average = 1294.01
	minimum = 24
	maximum = 2804
Slowest packet = 638
Flit latency average = 1243.47
	minimum = 6
	maximum = 2872
Slowest flit = 10219
Fragmentation average = 222.749
	minimum = 1
	maximum = 1863
Injected packet rate average = 0.0273646
	minimum = 0.011 (at node 72)
	maximum = 0.053 (at node 139)
Accepted packet rate average = 0.0124635
	minimum = 0.005 (at node 6)
	maximum = 0.023 (at node 162)
Injected flit rate average = 0.491635
	minimum = 0.194 (at node 72)
	maximum = 0.955 (at node 139)
Accepted flit rate average= 0.226266
	minimum = 0.07 (at node 85)
	maximum = 0.404 (at node 44)
Injected packet length average = 17.9661
Accepted packet length average = 18.1542
Total in-flight flits = 174303 (0 measured)
latency change    = 0.572002
throughput change = 0.00435053
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 398.09
	minimum = 28
	maximum = 2282
Network latency average = 156.484
	minimum = 28
	maximum = 918
Slowest packet = 16959
Flit latency average = 1843.96
	minimum = 6
	maximum = 3747
Slowest flit = 24507
Fragmentation average = 15.7285
	minimum = 1
	maximum = 186
Injected packet rate average = 0.0274271
	minimum = 0 (at node 44)
	maximum = 0.048 (at node 145)
Accepted packet rate average = 0.0120573
	minimum = 0.004 (at node 14)
	maximum = 0.023 (at node 148)
Injected flit rate average = 0.493286
	minimum = 0.004 (at node 44)
	maximum = 0.868 (at node 145)
Accepted flit rate average= 0.216172
	minimum = 0.075 (at node 36)
	maximum = 0.366 (at node 24)
Injected packet length average = 17.9854
Accepted packet length average = 17.9287
Total in-flight flits = 227658 (90795 measured)
latency change    = 2.30343
throughput change = 0.0466932
Class 0:
Packet latency average = 714.38
	minimum = 28
	maximum = 3070
Network latency average = 410.331
	minimum = 28
	maximum = 1974
Slowest packet = 16959
Flit latency average = 2134.08
	minimum = 6
	maximum = 4743
Slowest flit = 25238
Fragmentation average = 31.6788
	minimum = 1
	maximum = 646
Injected packet rate average = 0.0269036
	minimum = 0.0035 (at node 44)
	maximum = 0.0385 (at node 29)
Accepted packet rate average = 0.0119271
	minimum = 0.0065 (at node 17)
	maximum = 0.0175 (at node 34)
Injected flit rate average = 0.484536
	minimum = 0.063 (at node 44)
	maximum = 0.6995 (at node 29)
Accepted flit rate average= 0.213276
	minimum = 0.107 (at node 17)
	maximum = 0.315 (at node 56)
Injected packet length average = 18.0101
Accepted packet length average = 17.8817
Total in-flight flits = 278435 (176845 measured)
latency change    = 0.442747
throughput change = 0.0135779
Class 0:
Packet latency average = 1365.66
	minimum = 28
	maximum = 3953
Network latency average = 952.064
	minimum = 28
	maximum = 2888
Slowest packet = 16959
Flit latency average = 2397.28
	minimum = 6
	maximum = 5595
Slowest flit = 41544
Fragmentation average = 67.524
	minimum = 1
	maximum = 1203
Injected packet rate average = 0.0254201
	minimum = 0.00333333 (at node 188)
	maximum = 0.0356667 (at node 29)
Accepted packet rate average = 0.0118021
	minimum = 0.00666667 (at node 64)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.457415
	minimum = 0.0596667 (at node 188)
	maximum = 0.644333 (at node 29)
Accepted flit rate average= 0.211528
	minimum = 0.120333 (at node 79)
	maximum = 0.304667 (at node 56)
Injected packet length average = 17.9942
Accepted packet length average = 17.9229
Total in-flight flits = 316289 (244692 measured)
latency change    = 0.476898
throughput change = 0.00826494
Class 0:
Packet latency average = 2143.23
	minimum = 28
	maximum = 5056
Network latency average = 1654.25
	minimum = 24
	maximum = 3961
Slowest packet = 16959
Flit latency average = 2650.02
	minimum = 6
	maximum = 6532
Slowest flit = 47863
Fragmentation average = 105.662
	minimum = 1
	maximum = 1251
Injected packet rate average = 0.0237396
	minimum = 0.00525 (at node 188)
	maximum = 0.03175 (at node 41)
Accepted packet rate average = 0.0117513
	minimum = 0.0075 (at node 36)
	maximum = 0.0165 (at node 56)
Injected flit rate average = 0.427178
	minimum = 0.09075 (at node 188)
	maximum = 0.57175 (at node 41)
Accepted flit rate average= 0.210664
	minimum = 0.12625 (at node 86)
	maximum = 0.29375 (at node 56)
Injected packet length average = 17.9944
Accepted packet length average = 17.9269
Total in-flight flits = 341031 (291547 measured)
latency change    = 0.362803
throughput change = 0.00409996
Class 0:
Packet latency average = 2778.54
	minimum = 26
	maximum = 5819
Network latency average = 2257.45
	minimum = 24
	maximum = 4956
Slowest packet = 16959
Flit latency average = 2901.92
	minimum = 6
	maximum = 7211
Slowest flit = 68723
Fragmentation average = 126.951
	minimum = 0
	maximum = 1480
Injected packet rate average = 0.0220906
	minimum = 0.0042 (at node 188)
	maximum = 0.03 (at node 189)
Accepted packet rate average = 0.0117115
	minimum = 0.008 (at node 86)
	maximum = 0.0152 (at node 56)
Injected flit rate average = 0.39772
	minimum = 0.0726 (at node 188)
	maximum = 0.5386 (at node 189)
Accepted flit rate average= 0.20966
	minimum = 0.1398 (at node 86)
	maximum = 0.2762 (at node 56)
Injected packet length average = 18.004
Accepted packet length average = 17.9022
Total in-flight flits = 355691 (321640 measured)
latency change    = 0.228649
throughput change = 0.00478701
Class 0:
Packet latency average = 3331.61
	minimum = 26
	maximum = 7093
Network latency average = 2778.5
	minimum = 24
	maximum = 5969
Slowest packet = 16959
Flit latency average = 3169.38
	minimum = 6
	maximum = 8472
Slowest flit = 55350
Fragmentation average = 138.91
	minimum = 0
	maximum = 2091
Injected packet rate average = 0.0204592
	minimum = 0.008 (at node 104)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.0115833
	minimum = 0.008 (at node 64)
	maximum = 0.0153333 (at node 56)
Injected flit rate average = 0.368284
	minimum = 0.144333 (at node 104)
	maximum = 0.5035 (at node 35)
Accepted flit rate average= 0.207589
	minimum = 0.140833 (at node 79)
	maximum = 0.2755 (at node 56)
Injected packet length average = 18.0009
Accepted packet length average = 17.9213
Total in-flight flits = 360483 (337655 measured)
latency change    = 0.166008
throughput change = 0.00998068
Class 0:
Packet latency average = 3842.3
	minimum = 26
	maximum = 7663
Network latency average = 3260.7
	minimum = 24
	maximum = 6936
Slowest packet = 16959
Flit latency average = 3436.45
	minimum = 6
	maximum = 9179
Slowest flit = 79469
Fragmentation average = 147.523
	minimum = 0
	maximum = 2485
Injected packet rate average = 0.0191205
	minimum = 0.00828571 (at node 104)
	maximum = 0.0271429 (at node 35)
Accepted packet rate average = 0.0114821
	minimum = 0.00814286 (at node 64)
	maximum = 0.0154286 (at node 56)
Injected flit rate average = 0.344294
	minimum = 0.149714 (at node 104)
	maximum = 0.488714 (at node 35)
Accepted flit rate average= 0.205804
	minimum = 0.140714 (at node 79)
	maximum = 0.276857 (at node 56)
Injected packet length average = 18.0065
Accepted packet length average = 17.9238
Total in-flight flits = 361599 (346758 measured)
latency change    = 0.132911
throughput change = 0.00867317
Draining all recorded packets ...
Class 0:
Remaining flits: 69246 69247 69248 69249 69250 69251 69252 69253 69254 69255 [...] (360962 flits)
Measured flits: 305082 305083 305084 305085 305086 305087 305088 305089 305090 305091 [...] (351517 flits)
Class 0:
Remaining flits: 89730 89731 89732 89733 89734 89735 89736 89737 89738 89739 [...] (357152 flits)
Measured flits: 305082 305083 305084 305085 305086 305087 305088 305089 305090 305091 [...] (351130 flits)
Class 0:
Remaining flits: 89730 89731 89732 89733 89734 89735 89736 89737 89738 89739 [...] (352582 flits)
Measured flits: 305082 305083 305084 305085 305086 305087 305088 305089 305090 305091 [...] (348070 flits)
Class 0:
Remaining flits: 89730 89731 89732 89733 89734 89735 89736 89737 89738 89739 [...] (348911 flits)
Measured flits: 305082 305083 305084 305085 305086 305087 305088 305089 305090 305091 [...] (342981 flits)
Class 0:
Remaining flits: 89730 89731 89732 89733 89734 89735 89736 89737 89738 89739 [...] (347750 flits)
Measured flits: 305082 305083 305084 305085 305086 305087 305088 305089 305090 305091 [...] (337589 flits)
Class 0:
Remaining flits: 157626 157627 157628 157629 157630 157631 157632 157633 157634 157635 [...] (349763 flits)
Measured flits: 305514 305515 305516 305517 305518 305519 305520 305521 305522 305523 [...] (331624 flits)
Class 0:
Remaining flits: 157626 157627 157628 157629 157630 157631 157632 157633 157634 157635 [...] (349336 flits)
Measured flits: 305514 305515 305516 305517 305518 305519 305520 305521 305522 305523 [...] (318070 flits)
Class 0:
Remaining flits: 193752 193753 193754 193755 193756 193757 193758 193759 193760 193761 [...] (346604 flits)
Measured flits: 305514 305515 305516 305517 305518 305519 305520 305521 305522 305523 [...] (298841 flits)
Class 0:
Remaining flits: 193752 193753 193754 193755 193756 193757 193758 193759 193760 193761 [...] (342737 flits)
Measured flits: 305514 305515 305516 305517 305518 305519 305520 305521 305522 305523 [...] (275647 flits)
Class 0:
Remaining flits: 193753 193754 193755 193756 193757 193758 193759 193760 193761 193762 [...] (338247 flits)
Measured flits: 305525 305526 305527 305528 305529 305530 305531 311636 311637 311638 [...] (247604 flits)
Class 0:
Remaining flits: 289638 289639 289640 289641 289642 289643 289644 289645 289646 289647 [...] (333396 flits)
Measured flits: 312012 312013 312014 312015 312016 312017 312018 312019 312020 312021 [...] (220716 flits)
Class 0:
Remaining flits: 315576 315577 315578 315579 315580 315581 315582 315583 315584 315585 [...] (334193 flits)
Measured flits: 315576 315577 315578 315579 315580 315581 315582 315583 315584 315585 [...] (193434 flits)
Class 0:
Remaining flits: 315584 315585 315586 315587 315588 315589 315590 315591 315592 315593 [...] (333875 flits)
Measured flits: 315584 315585 315586 315587 315588 315589 315590 315591 315592 315593 [...] (170159 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (333660 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (148562 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (331554 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (127047 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (326776 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (105832 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (324714 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (89445 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (323317 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (74851 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (320997 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (61815 flits)
Class 0:
Remaining flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (322979 flits)
Measured flits: 358326 358327 358328 358329 358330 358331 358332 358333 358334 358335 [...] (50790 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (321561 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (40555 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (321913 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (32916 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (322573 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (25742 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (319955 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (20194 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (317238 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (15547 flits)
Class 0:
Remaining flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (314796 flits)
Measured flits: 403920 403921 403922 403923 403924 403925 403926 403927 403928 403929 [...] (11952 flits)
Class 0:
Remaining flits: 436266 436267 436268 436269 436270 436271 436272 436273 436274 436275 [...] (311316 flits)
Measured flits: 436266 436267 436268 436269 436270 436271 436272 436273 436274 436275 [...] (9671 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (313216 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (8061 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (313450 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (6660 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (315554 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (5089 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (315387 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (3743 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (315724 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (2775 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (313333 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (1911 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (313722 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (1275 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (315296 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (972 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (318414 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (628 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (319006 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (452 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (319721 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (390 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (321006 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (262 flits)
Class 0:
Remaining flits: 922081 922082 922083 922084 922085 993748 993749 993750 993751 993752 [...] (319283 flits)
Measured flits: 922081 922082 922083 922084 922085 993748 993749 993750 993751 993752 [...] (181 flits)
Class 0:
Remaining flits: 1013490 1013491 1013492 1013493 1013494 1013495 1013496 1013497 1013498 1013499 [...] (319383 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (126 flits)
Class 0:
Remaining flits: 1103166 1103167 1103168 1103169 1103170 1103171 1103172 1103173 1103174 1103175 [...] (317313 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (88 flits)
Class 0:
Remaining flits: 1143954 1143955 1143956 1143957 1143958 1143959 1143960 1143961 1143962 1143963 [...] (317276 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (32 flits)
Class 0:
Remaining flits: 1143954 1143955 1143956 1143957 1143958 1143959 1143960 1143961 1143962 1143963 [...] (317790 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (18 flits)
Class 0:
Remaining flits: 1143954 1143955 1143956 1143957 1143958 1143959 1143960 1143961 1143962 1143963 [...] (317705 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (18 flits)
Class 0:
Remaining flits: 1143954 1143955 1143956 1143957 1143958 1143959 1143960 1143961 1143962 1143963 [...] (317627 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (18 flits)
Class 0:
Remaining flits: 1149228 1149229 1149230 1149231 1149232 1149233 1149234 1149235 1149236 1149237 [...] (315428 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (18 flits)
Class 0:
Remaining flits: 1149228 1149229 1149230 1149231 1149232 1149233 1149234 1149235 1149236 1149237 [...] (315551 flits)
Measured flits: 1157760 1157761 1157762 1157763 1157764 1157765 1157766 1157767 1157768 1157769 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1155510 1155511 1155512 1155513 1155514 1155515 1155516 1155517 1155518 1155519 [...] (281320 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1172736 1172737 1172738 1172739 1172740 1172741 1172742 1172743 1172744 1172745 [...] (247778 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1314648 1314649 1314650 1314651 1314652 1314653 1314654 1314655 1314656 1314657 [...] (215249 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1314648 1314649 1314650 1314651 1314652 1314653 1314654 1314655 1314656 1314657 [...] (183462 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355976 1355977 1355978 1355979 1355980 1355981 1355982 1355983 1355984 1355985 [...] (152699 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355976 1355977 1355978 1355979 1355980 1355981 1355982 1355983 1355984 1355985 [...] (123087 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1486674 1486675 1486676 1486677 1486678 1486679 1486680 1486681 1486682 1486683 [...] (94353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1486687 1486688 1486689 1486690 1486691 1630584 1630585 1630586 1630587 1630588 [...] (69106 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630584 1630585 1630586 1630587 1630588 1630589 1630590 1630591 1630592 1630593 [...] (47457 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630588 1630589 1630590 1630591 1630592 1630593 1630594 1630595 1630596 1630597 [...] (28877 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1800210 1800211 1800212 1800213 1800214 1800215 1809270 1809271 1809272 1809273 [...] (14333 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1854596 1854597 1854598 1854599 1854600 1854601 1854602 1854603 1854604 1854605 [...] (6451 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1861884 1861885 1861886 1861887 1861888 1861889 1861890 1861891 1861892 1861893 [...] (2515 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1950822 1950823 1950824 1950825 1950826 1950827 1950828 1950829 1950830 1950831 [...] (656 flits)
Measured flits: (0 flits)
Time taken is 72831 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11997.2 (1 samples)
	minimum = 26 (1 samples)
	maximum = 48830 (1 samples)
Network latency average = 8415.76 (1 samples)
	minimum = 24 (1 samples)
	maximum = 44760 (1 samples)
Flit latency average = 8286.34 (1 samples)
	minimum = 6 (1 samples)
	maximum = 44743 (1 samples)
Fragmentation average = 207.247 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4673 (1 samples)
Injected packet rate average = 0.0191205 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0271429 (1 samples)
Accepted packet rate average = 0.0114821 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.344294 (1 samples)
	minimum = 0.149714 (1 samples)
	maximum = 0.488714 (1 samples)
Accepted flit rate average = 0.205804 (1 samples)
	minimum = 0.140714 (1 samples)
	maximum = 0.276857 (1 samples)
Injected packet size average = 18.0065 (1 samples)
Accepted packet size average = 17.9238 (1 samples)
Hops average = 5.05613 (1 samples)
Total run time 93.7861
