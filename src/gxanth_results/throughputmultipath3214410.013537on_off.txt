BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 284.928
	minimum = 27
	maximum = 953
Network latency average = 232.25
	minimum = 23
	maximum = 912
Slowest packet = 77
Flit latency average = 162.881
	minimum = 6
	maximum = 958
Slowest flit = 2828
Fragmentation average = 124.56
	minimum = 0
	maximum = 864
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00879167
	minimum = 0.003 (at node 174)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.176979
	minimum = 0.06 (at node 174)
	maximum = 0.315 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.1303
Total in-flight flits = 14170 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 391.176
	minimum = 27
	maximum = 1932
Network latency average = 334.314
	minimum = 23
	maximum = 1792
Slowest packet = 355
Flit latency average = 248.184
	minimum = 6
	maximum = 1889
Slowest flit = 6820
Fragmentation average = 158.049
	minimum = 0
	maximum = 1500
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00958594
	minimum = 0.004 (at node 135)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.183714
	minimum = 0.0885 (at node 135)
	maximum = 0.284 (at node 22)
Injected packet length average = 17.9095
Accepted packet length average = 19.1649
Total in-flight flits = 20534 (0 measured)
latency change    = 0.271611
throughput change = 0.0366569
Class 0:
Packet latency average = 616.312
	minimum = 27
	maximum = 2838
Network latency average = 554.433
	minimum = 23
	maximum = 2582
Slowest packet = 1018
Flit latency average = 453.163
	minimum = 6
	maximum = 2606
Slowest flit = 20589
Fragmentation average = 213.3
	minimum = 0
	maximum = 2037
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0107969
	minimum = 0.004 (at node 146)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.195917
	minimum = 0.053 (at node 146)
	maximum = 0.369 (at node 24)
Injected packet length average = 18.0569
Accepted packet length average = 18.1457
Total in-flight flits = 27846 (0 measured)
latency change    = 0.365295
throughput change = 0.0622873
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 390.851
	minimum = 23
	maximum = 1144
Network latency average = 325.15
	minimum = 23
	maximum = 976
Slowest packet = 7597
Flit latency average = 574.714
	minimum = 6
	maximum = 3635
Slowest flit = 19667
Fragmentation average = 140.659
	minimum = 0
	maximum = 624
Injected packet rate average = 0.0140625
	minimum = 0 (at node 6)
	maximum = 0.056 (at node 145)
Accepted packet rate average = 0.0114062
	minimum = 0.004 (at node 49)
	maximum = 0.023 (at node 159)
Injected flit rate average = 0.252656
	minimum = 0 (at node 15)
	maximum = 1 (at node 145)
Accepted flit rate average= 0.205661
	minimum = 0.067 (at node 138)
	maximum = 0.366 (at node 102)
Injected packet length average = 17.9667
Accepted packet length average = 18.0306
Total in-flight flits = 36959 (28245 measured)
latency change    = 0.576846
throughput change = 0.0473827
Class 0:
Packet latency average = 573.337
	minimum = 23
	maximum = 2071
Network latency average = 509.292
	minimum = 23
	maximum = 1979
Slowest packet = 7597
Flit latency average = 652.38
	minimum = 6
	maximum = 4278
Slowest flit = 34857
Fragmentation average = 167.687
	minimum = 0
	maximum = 1677
Injected packet rate average = 0.0136797
	minimum = 0.0015 (at node 143)
	maximum = 0.036 (at node 145)
Accepted packet rate average = 0.011388
	minimum = 0.0055 (at node 71)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.245948
	minimum = 0.027 (at node 143)
	maximum = 0.6475 (at node 145)
Accepted flit rate average= 0.206401
	minimum = 0.094 (at node 71)
	maximum = 0.3415 (at node 159)
Injected packet length average = 17.9791
Accepted packet length average = 18.1244
Total in-flight flits = 43142 (39679 measured)
latency change    = 0.318287
throughput change = 0.00358323
Class 0:
Packet latency average = 708.872
	minimum = 23
	maximum = 2956
Network latency average = 641.142
	minimum = 23
	maximum = 2830
Slowest packet = 7635
Flit latency average = 711.352
	minimum = 6
	maximum = 4553
Slowest flit = 65785
Fragmentation average = 189.981
	minimum = 0
	maximum = 2063
Injected packet rate average = 0.0134618
	minimum = 0.00233333 (at node 57)
	maximum = 0.0286667 (at node 173)
Accepted packet rate average = 0.0114844
	minimum = 0.00633333 (at node 71)
	maximum = 0.0166667 (at node 16)
Injected flit rate average = 0.242297
	minimum = 0.042 (at node 57)
	maximum = 0.521333 (at node 173)
Accepted flit rate average= 0.20778
	minimum = 0.123667 (at node 71)
	maximum = 0.306 (at node 16)
Injected packet length average = 17.9988
Accepted packet length average = 18.0924
Total in-flight flits = 47737 (46199 measured)
latency change    = 0.191199
throughput change = 0.0066343
Draining remaining packets ...
Class 0:
Remaining flits: 47501 51984 51985 51986 51987 51988 51989 51990 51991 51992 [...] (16109 flits)
Measured flits: 136854 136855 136856 136857 136858 136859 136860 136861 136862 136863 [...] (15510 flits)
Class 0:
Remaining flits: 98816 98817 98818 98819 108162 108163 108164 108165 108166 108167 [...] (2041 flits)
Measured flits: 136854 136855 136856 136857 136858 136859 136860 136861 136862 136863 [...] (1857 flits)
Time taken is 8759 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1120.88 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5630 (1 samples)
Network latency average = 1048.62 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5414 (1 samples)
Flit latency average = 1021.78 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6442 (1 samples)
Fragmentation average = 192.192 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2098 (1 samples)
Injected packet rate average = 0.0134618 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.0114844 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.242297 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.521333 (1 samples)
Accepted flit rate average = 0.20778 (1 samples)
	minimum = 0.123667 (1 samples)
	maximum = 0.306 (1 samples)
Injected packet size average = 17.9988 (1 samples)
Accepted packet size average = 18.0924 (1 samples)
Hops average = 5.04643 (1 samples)
Total run time 6.39522
