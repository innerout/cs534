BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 136.759
	minimum = 22
	maximum = 530
Network latency average = 133.141
	minimum = 22
	maximum = 510
Slowest packet = 1055
Flit latency average = 102.428
	minimum = 5
	maximum = 493
Slowest flit = 19006
Fragmentation average = 29.9872
	minimum = 0
	maximum = 147
Injected packet rate average = 0.015875
	minimum = 0.008 (at node 61)
	maximum = 0.026 (at node 14)
Accepted packet rate average = 0.0126302
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.282516
	minimum = 0.144 (at node 61)
	maximum = 0.468 (at node 60)
Accepted flit rate average= 0.23401
	minimum = 0.09 (at node 174)
	maximum = 0.446 (at node 44)
Injected packet length average = 17.7963
Accepted packet length average = 18.5278
Total in-flight flits = 9934 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 195.996
	minimum = 22
	maximum = 893
Network latency average = 192.482
	minimum = 22
	maximum = 884
Slowest packet = 1986
Flit latency average = 159.407
	minimum = 5
	maximum = 867
Slowest flit = 35765
Fragmentation average = 34.9705
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0156406
	minimum = 0.0075 (at node 111)
	maximum = 0.025 (at node 104)
Accepted packet rate average = 0.0134245
	minimum = 0.0075 (at node 153)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.280315
	minimum = 0.135 (at node 111)
	maximum = 0.45 (at node 104)
Accepted flit rate average= 0.245354
	minimum = 0.135 (at node 153)
	maximum = 0.35 (at node 166)
Injected packet length average = 17.9222
Accepted packet length average = 18.2766
Total in-flight flits = 13892 (0 measured)
latency change    = 0.302234
throughput change = 0.0462342
Class 0:
Packet latency average = 324.428
	minimum = 22
	maximum = 1113
Network latency average = 320.358
	minimum = 22
	maximum = 1107
Slowest packet = 5615
Flit latency average = 279.178
	minimum = 5
	maximum = 1090
Slowest flit = 101087
Fragmentation average = 47.2118
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0155521
	minimum = 0.007 (at node 6)
	maximum = 0.027 (at node 17)
Accepted packet rate average = 0.0145573
	minimum = 0.007 (at node 163)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.279859
	minimum = 0.126 (at node 6)
	maximum = 0.48 (at node 17)
Accepted flit rate average= 0.262401
	minimum = 0.109 (at node 190)
	maximum = 0.473 (at node 34)
Injected packet length average = 17.995
Accepted packet length average = 18.0254
Total in-flight flits = 17295 (0 measured)
latency change    = 0.395873
throughput change = 0.064965
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 313.592
	minimum = 22
	maximum = 965
Network latency average = 302.204
	minimum = 22
	maximum = 946
Slowest packet = 9030
Flit latency average = 327.773
	minimum = 5
	maximum = 1243
Slowest flit = 147545
Fragmentation average = 43.6956
	minimum = 0
	maximum = 195
Injected packet rate average = 0.0161302
	minimum = 0.006 (at node 12)
	maximum = 0.029 (at node 117)
Accepted packet rate average = 0.0148646
	minimum = 0.005 (at node 134)
	maximum = 0.024 (at node 123)
Injected flit rate average = 0.290375
	minimum = 0.108 (at node 12)
	maximum = 0.523 (at node 117)
Accepted flit rate average= 0.269531
	minimum = 0.1 (at node 134)
	maximum = 0.438 (at node 129)
Injected packet length average = 18.0019
Accepted packet length average = 18.1324
Total in-flight flits = 21327 (21118 measured)
latency change    = 0.0345539
throughput change = 0.0264541
Class 0:
Packet latency average = 394.719
	minimum = 22
	maximum = 1816
Network latency average = 381.521
	minimum = 22
	maximum = 1815
Slowest packet = 9297
Flit latency average = 360.781
	minimum = 5
	maximum = 1843
Slowest flit = 152351
Fragmentation average = 49.7972
	minimum = 0
	maximum = 229
Injected packet rate average = 0.0157422
	minimum = 0.009 (at node 12)
	maximum = 0.023 (at node 54)
Accepted packet rate average = 0.0149167
	minimum = 0.009 (at node 86)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.283383
	minimum = 0.162 (at node 12)
	maximum = 0.414 (at node 54)
Accepted flit rate average= 0.269604
	minimum = 0.162 (at node 86)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0015
Accepted packet length average = 18.074
Total in-flight flits = 22649 (22631 measured)
latency change    = 0.205529
throughput change = 0.000270458
Class 0:
Packet latency average = 434.5
	minimum = 22
	maximum = 1947
Network latency average = 416.566
	minimum = 22
	maximum = 1947
Slowest packet = 11652
Flit latency average = 382.805
	minimum = 5
	maximum = 2205
Slowest flit = 155519
Fragmentation average = 52.4139
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0157344
	minimum = 0.01 (at node 12)
	maximum = 0.023 (at node 21)
Accepted packet rate average = 0.0149132
	minimum = 0.00966667 (at node 36)
	maximum = 0.022 (at node 128)
Injected flit rate average = 0.282964
	minimum = 0.18 (at node 12)
	maximum = 0.414667 (at node 21)
Accepted flit rate average= 0.269181
	minimum = 0.170667 (at node 36)
	maximum = 0.388333 (at node 128)
Injected packet length average = 17.9838
Accepted packet length average = 18.0498
Total in-flight flits = 25435 (25435 measured)
latency change    = 0.0915568
throughput change = 0.00157371
Class 0:
Packet latency average = 465.735
	minimum = 22
	maximum = 2209
Network latency average = 443.092
	minimum = 22
	maximum = 2164
Slowest packet = 12645
Flit latency average = 403.683
	minimum = 5
	maximum = 2236
Slowest flit = 258447
Fragmentation average = 54.7823
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0157057
	minimum = 0.011 (at node 136)
	maximum = 0.02125 (at node 21)
Accepted packet rate average = 0.0148698
	minimum = 0.0105 (at node 104)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.282561
	minimum = 0.19775 (at node 136)
	maximum = 0.383 (at node 21)
Accepted flit rate average= 0.268806
	minimum = 0.187 (at node 104)
	maximum = 0.37175 (at node 128)
Injected packet length average = 17.991
Accepted packet length average = 18.0773
Total in-flight flits = 28130 (28130 measured)
latency change    = 0.0670659
throughput change = 0.00139344
Class 0:
Packet latency average = 498.055
	minimum = 22
	maximum = 2322
Network latency average = 470.568
	minimum = 22
	maximum = 2293
Slowest packet = 14358
Flit latency average = 426.205
	minimum = 5
	maximum = 2276
Slowest flit = 301643
Fragmentation average = 57.1998
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0156479
	minimum = 0.011 (at node 136)
	maximum = 0.0204 (at node 21)
Accepted packet rate average = 0.0148885
	minimum = 0.0116 (at node 104)
	maximum = 0.0202 (at node 128)
Injected flit rate average = 0.281523
	minimum = 0.198 (at node 136)
	maximum = 0.3676 (at node 21)
Accepted flit rate average= 0.26888
	minimum = 0.2054 (at node 104)
	maximum = 0.3616 (at node 128)
Injected packet length average = 17.9911
Accepted packet length average = 18.0595
Total in-flight flits = 29674 (29674 measured)
latency change    = 0.0648918
throughput change = 0.000276029
Class 0:
Packet latency average = 527.812
	minimum = 22
	maximum = 2647
Network latency average = 494.205
	minimum = 22
	maximum = 2577
Slowest packet = 14358
Flit latency average = 446.847
	minimum = 5
	maximum = 2849
Slowest flit = 330832
Fragmentation average = 59.1217
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0156076
	minimum = 0.0111667 (at node 136)
	maximum = 0.0206667 (at node 21)
Accepted packet rate average = 0.0148733
	minimum = 0.0116667 (at node 171)
	maximum = 0.0191667 (at node 128)
Injected flit rate average = 0.280753
	minimum = 0.198333 (at node 136)
	maximum = 0.371833 (at node 21)
Accepted flit rate average= 0.268388
	minimum = 0.210667 (at node 171)
	maximum = 0.343333 (at node 128)
Injected packet length average = 17.9882
Accepted packet length average = 18.045
Total in-flight flits = 31914 (31914 measured)
latency change    = 0.056379
throughput change = 0.00183387
Class 0:
Packet latency average = 557.065
	minimum = 22
	maximum = 3318
Network latency average = 516.586
	minimum = 22
	maximum = 3306
Slowest packet = 17920
Flit latency average = 467.146
	minimum = 5
	maximum = 3289
Slowest flit = 322577
Fragmentation average = 60.7647
	minimum = 0
	maximum = 306
Injected packet rate average = 0.0155722
	minimum = 0.0115714 (at node 103)
	maximum = 0.0194286 (at node 21)
Accepted packet rate average = 0.014881
	minimum = 0.012 (at node 80)
	maximum = 0.0187143 (at node 128)
Injected flit rate average = 0.280254
	minimum = 0.209143 (at node 103)
	maximum = 0.35 (at node 21)
Accepted flit rate average= 0.268823
	minimum = 0.214714 (at node 80)
	maximum = 0.335 (at node 128)
Injected packet length average = 17.9971
Accepted packet length average = 18.0649
Total in-flight flits = 33007 (33007 measured)
latency change    = 0.0525125
throughput change = 0.00161778
Draining all recorded packets ...
Class 0:
Remaining flits: 433584 433585 433586 433587 433588 433589 433590 433591 433592 433593 [...] (34990 flits)
Measured flits: 433584 433585 433586 433587 433588 433589 433590 433591 433592 433593 [...] (5418 flits)
Class 0:
Remaining flits: 490608 490609 490610 490611 490612 490613 490614 490615 490616 490617 [...] (34681 flits)
Measured flits: 490608 490609 490610 490611 490612 490613 490614 490615 490616 490617 [...] (634 flits)
Class 0:
Remaining flits: 555426 555427 555428 555429 555430 555431 555432 555433 555434 555435 [...] (35112 flits)
Measured flits: 555426 555427 555428 555429 555430 555431 555432 555433 555434 555435 [...] (31 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 633852 633853 633854 633855 633856 633857 633858 633859 633860 633861 [...] (1808 flits)
Measured flits: (0 flits)
Time taken is 15699 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 614.866 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4161 (1 samples)
Network latency average = 560.319 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3678 (1 samples)
Flit latency average = 552.639 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3661 (1 samples)
Fragmentation average = 63.6207 (1 samples)
	minimum = 0 (1 samples)
	maximum = 306 (1 samples)
Injected packet rate average = 0.0155722 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0194286 (1 samples)
Accepted packet rate average = 0.014881 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.280254 (1 samples)
	minimum = 0.209143 (1 samples)
	maximum = 0.35 (1 samples)
Accepted flit rate average = 0.268823 (1 samples)
	minimum = 0.214714 (1 samples)
	maximum = 0.335 (1 samples)
Injected packet size average = 17.9971 (1 samples)
Accepted packet size average = 18.0649 (1 samples)
Hops average = 5.05719 (1 samples)
Total run time 15.1953
