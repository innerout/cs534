BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.691
	minimum = 22
	maximum = 845
Network latency average = 302.013
	minimum = 22
	maximum = 823
Slowest packet = 270
Flit latency average = 278.911
	minimum = 5
	maximum = 806
Slowest flit = 23399
Fragmentation average = 25.5091
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0152344
	minimum = 0.006 (at node 64)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.280906
	minimum = 0.108 (at node 64)
	maximum = 0.457 (at node 44)
Injected packet length average = 17.8602
Accepted packet length average = 18.439
Total in-flight flits = 100434 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 592.247
	minimum = 22
	maximum = 1682
Network latency average = 566.718
	minimum = 22
	maximum = 1543
Slowest packet = 270
Flit latency average = 543.523
	minimum = 5
	maximum = 1538
Slowest flit = 66380
Fragmentation average = 25.7087
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0457656
	minimum = 0.0315 (at node 10)
	maximum = 0.055 (at node 42)
Accepted packet rate average = 0.0161458
	minimum = 0.0085 (at node 83)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.820128
	minimum = 0.567 (at node 10)
	maximum = 0.9855 (at node 135)
Accepted flit rate average= 0.294156
	minimum = 0.155 (at node 83)
	maximum = 0.432 (at node 152)
Injected packet length average = 17.9202
Accepted packet length average = 18.2187
Total in-flight flits = 203376 (0 measured)
latency change    = 0.456829
throughput change = 0.0450441
Class 0:
Packet latency average = 1401.75
	minimum = 22
	maximum = 2331
Network latency average = 1364.64
	minimum = 22
	maximum = 2255
Slowest packet = 4309
Flit latency average = 1349.66
	minimum = 5
	maximum = 2279
Slowest flit = 110088
Fragmentation average = 25.35
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0460208
	minimum = 0.031 (at node 100)
	maximum = 0.056 (at node 71)
Accepted packet rate average = 0.0171875
	minimum = 0.008 (at node 4)
	maximum = 0.03 (at node 120)
Injected flit rate average = 0.82812
	minimum = 0.561 (at node 100)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.310276
	minimum = 0.144 (at node 4)
	maximum = 0.53 (at node 120)
Injected packet length average = 17.9945
Accepted packet length average = 18.0524
Total in-flight flits = 302851 (0 measured)
latency change    = 0.577495
throughput change = 0.0519531
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 111.756
	minimum = 22
	maximum = 834
Network latency average = 58.2548
	minimum = 22
	maximum = 590
Slowest packet = 26435
Flit latency average = 1907.3
	minimum = 5
	maximum = 3057
Slowest flit = 143826
Fragmentation average = 5.59194
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0457812
	minimum = 0.026 (at node 132)
	maximum = 0.056 (at node 63)
Accepted packet rate average = 0.0170677
	minimum = 0.008 (at node 36)
	maximum = 0.035 (at node 83)
Injected flit rate average = 0.824411
	minimum = 0.479 (at node 132)
	maximum = 1 (at node 63)
Accepted flit rate average= 0.306661
	minimum = 0.127 (at node 36)
	maximum = 0.635 (at node 83)
Injected packet length average = 18.0076
Accepted packet length average = 17.9673
Total in-flight flits = 402228 (146960 measured)
latency change    = 11.5429
throughput change = 0.0117869
Class 0:
Packet latency average = 175.872
	minimum = 22
	maximum = 1450
Network latency average = 104.62
	minimum = 22
	maximum = 1115
Slowest packet = 26435
Flit latency average = 2209.5
	minimum = 5
	maximum = 3742
Slowest flit = 195754
Fragmentation average = 5.50671
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0447474
	minimum = 0.0265 (at node 72)
	maximum = 0.0555 (at node 183)
Accepted packet rate average = 0.0167734
	minimum = 0.01 (at node 36)
	maximum = 0.0255 (at node 83)
Injected flit rate average = 0.805867
	minimum = 0.48 (at node 72)
	maximum = 1 (at node 191)
Accepted flit rate average= 0.301417
	minimum = 0.1725 (at node 36)
	maximum = 0.458 (at node 83)
Injected packet length average = 18.0093
Accepted packet length average = 17.9699
Total in-flight flits = 496401 (287682 measured)
latency change    = 0.364557
throughput change = 0.0174005
Class 0:
Packet latency average = 282.84
	minimum = 22
	maximum = 1560
Network latency average = 178.046
	minimum = 22
	maximum = 1397
Slowest packet = 26435
Flit latency average = 2519.75
	minimum = 5
	maximum = 4487
Slowest flit = 231461
Fragmentation average = 5.51977
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0439375
	minimum = 0.0276667 (at node 72)
	maximum = 0.0546667 (at node 118)
Accepted packet rate average = 0.0166059
	minimum = 0.0106667 (at node 36)
	maximum = 0.0226667 (at node 181)
Injected flit rate average = 0.790986
	minimum = 0.501 (at node 72)
	maximum = 0.984333 (at node 118)
Accepted flit rate average= 0.298441
	minimum = 0.192 (at node 36)
	maximum = 0.407 (at node 181)
Injected packet length average = 18.0025
Accepted packet length average = 17.972
Total in-flight flits = 586493 (424444 measured)
latency change    = 0.378194
throughput change = 0.0099708
Draining remaining packets ...
Class 0:
Remaining flits: 277796 277797 277798 277799 277800 277801 277802 277803 277804 277805 [...] (535234 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (419731 flits)
Class 0:
Remaining flits: 301140 301141 301142 301143 301144 301145 301146 301147 301148 301149 [...] (486473 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (417850 flits)
Class 0:
Remaining flits: 356328 356329 356330 356331 356332 356333 356334 356335 356336 356337 [...] (439024 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (411165 flits)
Class 0:
Remaining flits: 392063 392064 392065 392066 392067 392068 392069 392070 392071 392072 [...] (391992 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (386541 flits)
Class 0:
Remaining flits: 429474 429475 429476 429477 429478 429479 436104 436105 436106 436107 [...] (345083 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (344735 flits)
Class 0:
Remaining flits: 469438 469439 478098 478099 478100 478101 478102 478103 478104 478105 [...] (297829 flits)
Measured flits: 478098 478099 478100 478101 478102 478103 478104 478105 478106 478107 [...] (297827 flits)
Class 0:
Remaining flits: 531411 531412 531413 534168 534169 534170 534171 534172 534173 534174 [...] (250690 flits)
Measured flits: 531411 531412 531413 534168 534169 534170 534171 534172 534173 534174 [...] (250690 flits)
Class 0:
Remaining flits: 579582 579583 579584 579585 579586 579587 579588 579589 579590 579591 [...] (203461 flits)
Measured flits: 579582 579583 579584 579585 579586 579587 579588 579589 579590 579591 [...] (203461 flits)
Class 0:
Remaining flits: 608921 613188 613189 613190 613191 613192 613193 613194 613195 613196 [...] (156135 flits)
Measured flits: 608921 613188 613189 613190 613191 613192 613193 613194 613195 613196 [...] (156135 flits)
Class 0:
Remaining flits: 645048 645049 645050 645051 645052 645053 645054 645055 645056 645057 [...] (108951 flits)
Measured flits: 645048 645049 645050 645051 645052 645053 645054 645055 645056 645057 [...] (108951 flits)
Class 0:
Remaining flits: 699300 699301 699302 699303 699304 699305 699306 699307 699308 699309 [...] (61580 flits)
Measured flits: 699300 699301 699302 699303 699304 699305 699306 699307 699308 699309 [...] (61580 flits)
Class 0:
Remaining flits: 753552 753553 753554 753555 753556 753557 753558 753559 753560 753561 [...] (17845 flits)
Measured flits: 753552 753553 753554 753555 753556 753557 753558 753559 753560 753561 [...] (17845 flits)
Class 0:
Remaining flits: 806706 806707 806708 806709 806710 806711 806712 806713 806714 806715 [...] (1101 flits)
Measured flits: 806706 806707 806708 806709 806710 806711 806712 806713 806714 806715 [...] (1101 flits)
Time taken is 19527 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8761.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13917 (1 samples)
Network latency average = 8666.79 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13893 (1 samples)
Flit latency average = 6900.61 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13876 (1 samples)
Fragmentation average = 27.006 (1 samples)
	minimum = 0 (1 samples)
	maximum = 436 (1 samples)
Injected packet rate average = 0.0439375 (1 samples)
	minimum = 0.0276667 (1 samples)
	maximum = 0.0546667 (1 samples)
Accepted packet rate average = 0.0166059 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.790986 (1 samples)
	minimum = 0.501 (1 samples)
	maximum = 0.984333 (1 samples)
Accepted flit rate average = 0.298441 (1 samples)
	minimum = 0.192 (1 samples)
	maximum = 0.407 (1 samples)
Injected packet size average = 18.0025 (1 samples)
Accepted packet size average = 17.972 (1 samples)
Hops average = 5.11846 (1 samples)
Total run time 14.5292
