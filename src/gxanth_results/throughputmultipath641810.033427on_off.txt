BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 378.622
	minimum = 25
	maximum = 988
Network latency average = 284.203
	minimum = 25
	maximum = 889
Slowest packet = 16
Flit latency average = 197.225
	minimum = 6
	maximum = 955
Slowest flit = 3673
Fragmentation average = 189.183
	minimum = 0
	maximum = 740
Injected packet rate average = 0.0232552
	minimum = 0 (at node 17)
	maximum = 0.052 (at node 74)
Accepted packet rate average = 0.0098125
	minimum = 0.004 (at node 4)
	maximum = 0.018 (at node 114)
Injected flit rate average = 0.413901
	minimum = 0 (at node 17)
	maximum = 0.932 (at node 74)
Accepted flit rate average= 0.212135
	minimum = 0.08 (at node 135)
	maximum = 0.343 (at node 114)
Injected packet length average = 17.7982
Accepted packet length average = 21.6189
Total in-flight flits = 39676 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 658.607
	minimum = 25
	maximum = 1959
Network latency average = 492.427
	minimum = 25
	maximum = 1837
Slowest packet = 251
Flit latency average = 376.35
	minimum = 6
	maximum = 1820
Slowest flit = 11969
Fragmentation average = 258.232
	minimum = 0
	maximum = 1778
Injected packet rate average = 0.0198802
	minimum = 0 (at node 183)
	maximum = 0.0335 (at node 74)
Accepted packet rate average = 0.0110234
	minimum = 0.0055 (at node 153)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.354513
	minimum = 0 (at node 183)
	maximum = 0.6 (at node 74)
Accepted flit rate average= 0.218589
	minimum = 0.1135 (at node 153)
	maximum = 0.3345 (at node 152)
Injected packet length average = 17.8325
Accepted packet length average = 19.8294
Total in-flight flits = 53546 (0 measured)
latency change    = 0.425118
throughput change = 0.0295218
Class 0:
Packet latency average = 1387.28
	minimum = 27
	maximum = 2860
Network latency average = 915.088
	minimum = 27
	maximum = 2783
Slowest packet = 512
Flit latency average = 797.14
	minimum = 6
	maximum = 2842
Slowest flit = 10610
Fragmentation average = 305.429
	minimum = 2
	maximum = 2304
Injected packet rate average = 0.0124427
	minimum = 0 (at node 1)
	maximum = 0.041 (at node 162)
Accepted packet rate average = 0.012349
	minimum = 0.004 (at node 1)
	maximum = 0.023 (at node 99)
Injected flit rate average = 0.223891
	minimum = 0 (at node 1)
	maximum = 0.733 (at node 162)
Accepted flit rate average= 0.220885
	minimum = 0.095 (at node 101)
	maximum = 0.405 (at node 99)
Injected packet length average = 17.9937
Accepted packet length average = 17.887
Total in-flight flits = 54228 (0 measured)
latency change    = 0.525251
throughput change = 0.0103985
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1645.05
	minimum = 47
	maximum = 3794
Network latency average = 343.669
	minimum = 26
	maximum = 966
Slowest packet = 10042
Flit latency average = 978.491
	minimum = 6
	maximum = 3791
Slowest flit = 10601
Fragmentation average = 156.797
	minimum = 0
	maximum = 616
Injected packet rate average = 0.0108021
	minimum = 0 (at node 1)
	maximum = 0.038 (at node 54)
Accepted packet rate average = 0.0119844
	minimum = 0.002 (at node 191)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.193755
	minimum = 0 (at node 1)
	maximum = 0.697 (at node 54)
Accepted flit rate average= 0.212036
	minimum = 0.065 (at node 191)
	maximum = 0.38 (at node 182)
Injected packet length average = 17.9368
Accepted packet length average = 17.6927
Total in-flight flits = 50831 (23662 measured)
latency change    = 0.1567
throughput change = 0.0417332
Class 0:
Packet latency average = 2154.92
	minimum = 42
	maximum = 4726
Network latency average = 518.233
	minimum = 26
	maximum = 1947
Slowest packet = 10042
Flit latency average = 1003.47
	minimum = 6
	maximum = 4867
Slowest flit = 10553
Fragmentation average = 186.665
	minimum = 0
	maximum = 1684
Injected packet rate average = 0.0112187
	minimum = 0 (at node 4)
	maximum = 0.0365 (at node 30)
Accepted packet rate average = 0.0118255
	minimum = 0.005 (at node 191)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.201674
	minimum = 0 (at node 4)
	maximum = 0.656 (at node 30)
Accepted flit rate average= 0.211565
	minimum = 0.1015 (at node 191)
	maximum = 0.3205 (at node 129)
Injected packet length average = 17.9766
Accepted packet length average = 17.8906
Total in-flight flits = 50783 (34630 measured)
latency change    = 0.236607
throughput change = 0.00222794
Class 0:
Packet latency average = 2512.27
	minimum = 42
	maximum = 5848
Network latency average = 643.596
	minimum = 26
	maximum = 2868
Slowest packet = 10042
Flit latency average = 1026.96
	minimum = 6
	maximum = 5779
Slowest flit = 16558
Fragmentation average = 206.526
	minimum = 0
	maximum = 2118
Injected packet rate average = 0.011776
	minimum = 0 (at node 12)
	maximum = 0.0333333 (at node 30)
Accepted packet rate average = 0.0118438
	minimum = 0.006 (at node 36)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.211717
	minimum = 0 (at node 12)
	maximum = 0.602 (at node 30)
Accepted flit rate average= 0.212431
	minimum = 0.103 (at node 36)
	maximum = 0.291333 (at node 103)
Injected packet length average = 17.9786
Accepted packet length average = 17.9361
Total in-flight flits = 54052 (43703 measured)
latency change    = 0.142239
throughput change = 0.00407404
Draining remaining packets ...
Class 0:
Remaining flits: 12616 12617 22788 22789 22790 22791 22792 22793 22794 22795 [...] (20322 flits)
Measured flits: 180909 180910 180911 180912 180913 180914 180915 180916 180917 182322 [...] (15088 flits)
Class 0:
Remaining flits: 92301 92302 92303 103272 103273 103274 103275 103276 103277 103278 [...] (955 flits)
Measured flits: 184986 184987 184988 184989 184990 184991 184992 184993 184994 184995 [...] (873 flits)
Time taken is 8388 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3134.29 (1 samples)
	minimum = 42 (1 samples)
	maximum = 7602 (1 samples)
Network latency average = 1093.88 (1 samples)
	minimum = 26 (1 samples)
	maximum = 4974 (1 samples)
Flit latency average = 1431.69 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7372 (1 samples)
Fragmentation average = 226.41 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3547 (1 samples)
Injected packet rate average = 0.011776 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0333333 (1 samples)
Accepted packet rate average = 0.0118438 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.211717 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.602 (1 samples)
Accepted flit rate average = 0.212431 (1 samples)
	minimum = 0.103 (1 samples)
	maximum = 0.291333 (1 samples)
Injected packet size average = 17.9786 (1 samples)
Accepted packet size average = 17.9361 (1 samples)
Hops average = 5.14408 (1 samples)
Total run time 11.1095
