BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 235.266
	minimum = 27
	maximum = 962
Network latency average = 190.925
	minimum = 23
	maximum = 900
Slowest packet = 168
Flit latency average = 125.621
	minimum = 6
	maximum = 883
Slowest flit = 5147
Fragmentation average = 107.401
	minimum = 0
	maximum = 706
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00814063
	minimum = 0.002 (at node 41)
	maximum = 0.015 (at node 70)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.162182
	minimum = 0.036 (at node 174)
	maximum = 0.3 (at node 70)
Injected packet length average = 17.8411
Accepted packet length average = 19.9226
Total in-flight flits = 9991 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 322.133
	minimum = 27
	maximum = 1787
Network latency average = 273.109
	minimum = 23
	maximum = 1703
Slowest packet = 672
Flit latency average = 194.743
	minimum = 6
	maximum = 1786
Slowest flit = 5986
Fragmentation average = 138.944
	minimum = 0
	maximum = 1434
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00895313
	minimum = 0.004 (at node 115)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.170805
	minimum = 0.0785 (at node 115)
	maximum = 0.2805 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 19.0777
Total in-flight flits = 14007 (0 measured)
latency change    = 0.269663
throughput change = 0.050481
Class 0:
Packet latency average = 487.032
	minimum = 27
	maximum = 2356
Network latency average = 439.174
	minimum = 25
	maximum = 2262
Slowest packet = 332
Flit latency average = 335.316
	minimum = 6
	maximum = 2587
Slowest flit = 16540
Fragmentation average = 185.114
	minimum = 0
	maximum = 2065
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0106094
	minimum = 0.004 (at node 26)
	maximum = 0.02 (at node 177)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.191245
	minimum = 0.073 (at node 26)
	maximum = 0.327 (at node 68)
Injected packet length average = 17.9936
Accepted packet length average = 18.026
Total in-flight flits = 16384 (0 measured)
latency change    = 0.338581
throughput change = 0.106879
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 351.79
	minimum = 25
	maximum = 1112
Network latency average = 305.245
	minimum = 25
	maximum = 963
Slowest packet = 6604
Flit latency average = 399.511
	minimum = 6
	maximum = 3302
Slowest flit = 24569
Fragmentation average = 149.17
	minimum = 0
	maximum = 724
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.0106562
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 63)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.192599
	minimum = 0.066 (at node 40)
	maximum = 0.338 (at node 120)
Injected packet length average = 18.0247
Accepted packet length average = 18.0738
Total in-flight flits = 20157 (17040 measured)
latency change    = 0.384442
throughput change = 0.00703102
Class 0:
Packet latency average = 465.142
	minimum = 25
	maximum = 1910
Network latency average = 416.226
	minimum = 23
	maximum = 1798
Slowest packet = 6604
Flit latency average = 444.061
	minimum = 6
	maximum = 3580
Slowest flit = 47915
Fragmentation average = 170.533
	minimum = 0
	maximum = 1470
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.0106094
	minimum = 0.0045 (at node 17)
	maximum = 0.018 (at node 182)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.191987
	minimum = 0.076 (at node 17)
	maximum = 0.318 (at node 182)
Injected packet length average = 17.9755
Accepted packet length average = 18.096
Total in-flight flits = 22779 (21845 measured)
latency change    = 0.243694
throughput change = 0.00318761
Class 0:
Packet latency average = 549.111
	minimum = 25
	maximum = 3042
Network latency average = 498.368
	minimum = 23
	maximum = 2825
Slowest packet = 6751
Flit latency average = 481.733
	minimum = 6
	maximum = 4417
Slowest flit = 62296
Fragmentation average = 183.64
	minimum = 0
	maximum = 1769
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0106215
	minimum = 0.00566667 (at node 4)
	maximum = 0.016 (at node 129)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.191767
	minimum = 0.0996667 (at node 4)
	maximum = 0.279 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 18.0546
Total in-flight flits = 22836 (22496 measured)
latency change    = 0.152918
throughput change = 0.00114523
Draining remaining packets ...
Class 0:
Remaining flits: 114325 114326 114327 114328 114329 114330 114331 114332 114333 114334 [...] (2477 flits)
Measured flits: 121174 121175 126133 126134 126135 126136 126137 126138 126139 126140 [...] (2466 flits)
Class 0:
Remaining flits: 205233 205234 205235 232137 232138 232139 232140 232141 232142 232143 [...] (12 flits)
Measured flits: 205233 205234 205235 232137 232138 232139 232140 232141 232142 232143 [...] (12 flits)
Time taken is 8014 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 713.705 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4467 (1 samples)
Network latency average = 658.81 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4358 (1 samples)
Flit latency average = 605.933 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4889 (1 samples)
Fragmentation average = 187.776 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1800 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0106215 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.191767 (1 samples)
	minimum = 0.0996667 (1 samples)
	maximum = 0.279 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 18.0546 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 5.2286
