BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 198.778
	minimum = 22
	maximum = 610
Network latency average = 194.153
	minimum = 22
	maximum = 590
Slowest packet = 1533
Flit latency average = 164.052
	minimum = 5
	maximum = 573
Slowest flit = 27610
Fragmentation average = 33.5124
	minimum = 0
	maximum = 235
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.0134167
	minimum = 0.005 (at node 115)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.24813
	minimum = 0.09 (at node 115)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8234
Accepted packet length average = 18.4942
Total in-flight flits = 21965 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 334.921
	minimum = 22
	maximum = 1027
Network latency average = 330.05
	minimum = 22
	maximum = 1015
Slowest packet = 3685
Flit latency average = 298.915
	minimum = 5
	maximum = 1097
Slowest flit = 61648
Fragmentation average = 35.2311
	minimum = 0
	maximum = 336
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0141432
	minimum = 0.0075 (at node 153)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.258073
	minimum = 0.138 (at node 153)
	maximum = 0.378 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 18.2471
Total in-flight flits = 39104 (0 measured)
latency change    = 0.406494
throughput change = 0.0385267
Class 0:
Packet latency average = 698.831
	minimum = 22
	maximum = 1482
Network latency average = 694.172
	minimum = 22
	maximum = 1482
Slowest packet = 4974
Flit latency average = 665.352
	minimum = 5
	maximum = 1465
Slowest flit = 89549
Fragmentation average = 36.5496
	minimum = 0
	maximum = 395
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0150573
	minimum = 0.006 (at node 126)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.272229
	minimum = 0.108 (at node 126)
	maximum = 0.5 (at node 16)
Injected packet length average = 17.9811
Accepted packet length average = 18.0796
Total in-flight flits = 56514 (0 measured)
latency change    = 0.520742
throughput change = 0.0520012
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 187.601
	minimum = 22
	maximum = 1001
Network latency average = 182.504
	minimum = 22
	maximum = 981
Slowest packet = 11551
Flit latency average = 887.625
	minimum = 5
	maximum = 1700
Slowest flit = 158526
Fragmentation average = 10.4224
	minimum = 0
	maximum = 98
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0153594
	minimum = 0.007 (at node 48)
	maximum = 0.026 (at node 123)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.275828
	minimum = 0.126 (at node 48)
	maximum = 0.464 (at node 129)
Injected packet length average = 17.9618
Accepted packet length average = 17.9583
Total in-flight flits = 75177 (64242 measured)
latency change    = 2.7251
throughput change = 0.0130478
Class 0:
Packet latency average = 959.175
	minimum = 22
	maximum = 1927
Network latency average = 954.294
	minimum = 22
	maximum = 1927
Slowest packet = 11810
Flit latency average = 1008.09
	minimum = 5
	maximum = 1944
Slowest flit = 185705
Fragmentation average = 32.283
	minimum = 0
	maximum = 241
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0153411
	minimum = 0.009 (at node 113)
	maximum = 0.0235 (at node 66)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.275995
	minimum = 0.1685 (at node 113)
	maximum = 0.4095 (at node 66)
Injected packet length average = 18.0096
Accepted packet length average = 17.9905
Total in-flight flits = 90644 (90644 measured)
latency change    = 0.804415
throughput change = 0.000603876
Class 0:
Packet latency average = 1193.79
	minimum = 22
	maximum = 2308
Network latency average = 1188.74
	minimum = 22
	maximum = 2308
Slowest packet = 14056
Flit latency average = 1129.99
	minimum = 5
	maximum = 2291
Slowest flit = 253025
Fragmentation average = 35.4815
	minimum = 0
	maximum = 282
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0153333
	minimum = 0.01 (at node 135)
	maximum = 0.021 (at node 118)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.27608
	minimum = 0.178 (at node 135)
	maximum = 0.378 (at node 118)
Injected packet length average = 18.0047
Accepted packet length average = 18.0052
Total in-flight flits = 108272 (108272 measured)
latency change    = 0.196528
throughput change = 0.000308133
Class 0:
Packet latency average = 1347.65
	minimum = 22
	maximum = 2732
Network latency average = 1342.69
	minimum = 22
	maximum = 2732
Slowest packet = 15118
Flit latency average = 1248.45
	minimum = 5
	maximum = 2715
Slowest flit = 272141
Fragmentation average = 36.1654
	minimum = 0
	maximum = 286
Injected packet rate average = 0.0203372
	minimum = 0.01475 (at node 5)
	maximum = 0.026 (at node 156)
Accepted packet rate average = 0.0153177
	minimum = 0.011 (at node 135)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.366148
	minimum = 0.2655 (at node 5)
	maximum = 0.468 (at node 156)
Accepted flit rate average= 0.275594
	minimum = 0.1965 (at node 135)
	maximum = 0.36025 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 17.9918
Total in-flight flits = 126000 (126000 measured)
latency change    = 0.114169
throughput change = 0.00176387
Class 0:
Packet latency average = 1481.01
	minimum = 22
	maximum = 3050
Network latency average = 1476.09
	minimum = 22
	maximum = 3043
Slowest packet = 18293
Flit latency average = 1365.48
	minimum = 5
	maximum = 3050
Slowest flit = 343582
Fragmentation average = 37.5924
	minimum = 0
	maximum = 348
Injected packet rate average = 0.0203438
	minimum = 0.0154 (at node 139)
	maximum = 0.0248 (at node 113)
Accepted packet rate average = 0.015351
	minimum = 0.012 (at node 42)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.366196
	minimum = 0.2772 (at node 139)
	maximum = 0.4464 (at node 113)
Accepted flit rate average= 0.276122
	minimum = 0.2118 (at node 42)
	maximum = 0.3544 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 17.9872
Total in-flight flits = 142977 (142977 measured)
latency change    = 0.0900482
throughput change = 0.00191265
Class 0:
Packet latency average = 1611.25
	minimum = 22
	maximum = 3365
Network latency average = 1606.3
	minimum = 22
	maximum = 3363
Slowest packet = 21300
Flit latency average = 1485.25
	minimum = 5
	maximum = 3346
Slowest flit = 389554
Fragmentation average = 37.6702
	minimum = 0
	maximum = 348
Injected packet rate average = 0.0203151
	minimum = 0.016 (at node 155)
	maximum = 0.0245 (at node 96)
Accepted packet rate average = 0.0153238
	minimum = 0.0123333 (at node 42)
	maximum = 0.0191667 (at node 128)
Injected flit rate average = 0.365752
	minimum = 0.289333 (at node 155)
	maximum = 0.441 (at node 96)
Accepted flit rate average= 0.275755
	minimum = 0.221 (at node 42)
	maximum = 0.342167 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 17.9952
Total in-flight flits = 160098 (160098 measured)
latency change    = 0.0808311
throughput change = 0.00132968
Class 0:
Packet latency average = 1737.96
	minimum = 22
	maximum = 3796
Network latency average = 1732.97
	minimum = 22
	maximum = 3774
Slowest packet = 22750
Flit latency average = 1605.56
	minimum = 5
	maximum = 3757
Slowest flit = 424439
Fragmentation average = 37.8971
	minimum = 0
	maximum = 348
Injected packet rate average = 0.0202984
	minimum = 0.0158571 (at node 155)
	maximum = 0.0248571 (at node 165)
Accepted packet rate average = 0.0153251
	minimum = 0.012 (at node 42)
	maximum = 0.0192857 (at node 70)
Injected flit rate average = 0.365336
	minimum = 0.287286 (at node 155)
	maximum = 0.445429 (at node 165)
Accepted flit rate average= 0.27593
	minimum = 0.213 (at node 42)
	maximum = 0.350714 (at node 70)
Injected packet length average = 17.9983
Accepted packet length average = 18.005
Total in-flight flits = 176722 (176722 measured)
latency change    = 0.0729059
throughput change = 0.000633679
Draining all recorded packets ...
Class 0:
Remaining flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (193618 flits)
Measured flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (129930 flits)
Class 0:
Remaining flits: 521910 521911 521912 521913 521914 521915 521916 521917 521918 521919 [...] (208443 flits)
Measured flits: 521910 521911 521912 521913 521914 521915 521916 521917 521918 521919 [...] (82757 flits)
Class 0:
Remaining flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (224968 flits)
Measured flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (35994 flits)
Class 0:
Remaining flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (241555 flits)
Measured flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (4751 flits)
Class 0:
Remaining flits: 679536 679537 679538 679539 679540 679541 679542 679543 679544 679545 [...] (257364 flits)
Measured flits: 679536 679537 679538 679539 679540 679541 679542 679543 679544 679545 [...] (53 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 731725 731726 731727 731728 731729 731730 731731 731732 731733 731734 [...] (213660 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 780588 780589 780590 780591 780592 780593 780594 780595 780596 780597 [...] (166261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825534 825535 825536 825537 825538 825539 825540 825541 825542 825543 [...] (118586 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 893484 893485 893486 893487 893488 893489 893490 893491 893492 893493 [...] (71077 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944124 944125 944126 944127 944128 944129 944130 944131 944132 944133 [...] (25927 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 994140 994141 994142 994143 994144 994145 994146 994147 994148 994149 [...] (2638 flits)
Measured flits: (0 flits)
Time taken is 22051 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2316.72 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5307 (1 samples)
Network latency average = 2311.74 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5307 (1 samples)
Flit latency average = 2975.57 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7224 (1 samples)
Fragmentation average = 40.1072 (1 samples)
	minimum = 0 (1 samples)
	maximum = 380 (1 samples)
Injected packet rate average = 0.0202984 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0248571 (1 samples)
Accepted packet rate average = 0.0153251 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0192857 (1 samples)
Injected flit rate average = 0.365336 (1 samples)
	minimum = 0.287286 (1 samples)
	maximum = 0.445429 (1 samples)
Accepted flit rate average = 0.27593 (1 samples)
	minimum = 0.213 (1 samples)
	maximum = 0.350714 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 18.005 (1 samples)
Hops average = 5.07713 (1 samples)
Total run time 20.7091
