BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.869
	minimum = 23
	maximum = 967
Network latency average = 298.421
	minimum = 23
	maximum = 967
Slowest packet = 125
Flit latency average = 230.914
	minimum = 6
	maximum = 950
Slowest flit = 2267
Fragmentation average = 157.201
	minimum = 0
	maximum = 873
Injected packet rate average = 0.021401
	minimum = 0.008 (at node 36)
	maximum = 0.032 (at node 114)
Accepted packet rate average = 0.00986458
	minimum = 0.004 (at node 9)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.37799
	minimum = 0.14 (at node 36)
	maximum = 0.559 (at node 114)
Accepted flit rate average= 0.205187
	minimum = 0.085 (at node 153)
	maximum = 0.343 (at node 44)
Injected packet length average = 17.6622
Accepted packet length average = 20.8004
Total in-flight flits = 34710 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 612.779
	minimum = 23
	maximum = 1904
Network latency average = 523.924
	minimum = 23
	maximum = 1879
Slowest packet = 451
Flit latency average = 426.618
	minimum = 6
	maximum = 1898
Slowest flit = 7806
Fragmentation average = 193.09
	minimum = 0
	maximum = 1692
Injected packet rate average = 0.0163932
	minimum = 0.004 (at node 36)
	maximum = 0.0285 (at node 22)
Accepted packet rate average = 0.010513
	minimum = 0.0045 (at node 153)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.291591
	minimum = 0.0715 (at node 36)
	maximum = 0.5085 (at node 22)
Accepted flit rate average= 0.201909
	minimum = 0.0925 (at node 153)
	maximum = 0.3075 (at node 22)
Injected packet length average = 17.7873
Accepted packet length average = 19.2056
Total in-flight flits = 36101 (0 measured)
latency change    = 0.48453
throughput change = 0.0162382
Class 0:
Packet latency average = 1491.1
	minimum = 54
	maximum = 2808
Network latency average = 941.149
	minimum = 27
	maximum = 2808
Slowest packet = 435
Flit latency average = 812.952
	minimum = 6
	maximum = 2885
Slowest flit = 9490
Fragmentation average = 219.274
	minimum = 2
	maximum = 2280
Injected packet rate average = 0.0110208
	minimum = 0 (at node 5)
	maximum = 0.035 (at node 101)
Accepted packet rate average = 0.011
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 142)
Injected flit rate average = 0.198417
	minimum = 0 (at node 19)
	maximum = 0.643 (at node 101)
Accepted flit rate average= 0.195109
	minimum = 0.059 (at node 40)
	maximum = 0.332 (at node 142)
Injected packet length average = 18.0038
Accepted packet length average = 17.7372
Total in-flight flits = 36818 (0 measured)
latency change    = 0.589042
throughput change = 0.0348496
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2005.52
	minimum = 588
	maximum = 3227
Network latency average = 355.599
	minimum = 27
	maximum = 972
Slowest packet = 8441
Flit latency average = 904.971
	minimum = 6
	maximum = 3743
Slowest flit = 5777
Fragmentation average = 128.025
	minimum = 0
	maximum = 584
Injected packet rate average = 0.0108958
	minimum = 0 (at node 65)
	maximum = 0.033 (at node 150)
Accepted packet rate average = 0.010849
	minimum = 0.002 (at node 113)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.196406
	minimum = 0 (at node 88)
	maximum = 0.608 (at node 150)
Accepted flit rate average= 0.194495
	minimum = 0.07 (at node 190)
	maximum = 0.38 (at node 16)
Injected packet length average = 18.0258
Accepted packet length average = 17.9275
Total in-flight flits = 37095 (25942 measured)
latency change    = 0.256502
throughput change = 0.0031599
Class 0:
Packet latency average = 2476.48
	minimum = 588
	maximum = 4189
Network latency average = 620.007
	minimum = 27
	maximum = 1950
Slowest packet = 8441
Flit latency average = 913.228
	minimum = 6
	maximum = 4916
Slowest flit = 7005
Fragmentation average = 158.284
	minimum = 0
	maximum = 1221
Injected packet rate average = 0.0107422
	minimum = 0 (at node 77)
	maximum = 0.0235 (at node 181)
Accepted packet rate average = 0.0107865
	minimum = 0.0045 (at node 4)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.193198
	minimum = 0 (at node 88)
	maximum = 0.4235 (at node 181)
Accepted flit rate average= 0.194378
	minimum = 0.087 (at node 4)
	maximum = 0.3265 (at node 16)
Injected packet length average = 17.985
Accepted packet length average = 18.0205
Total in-flight flits = 36355 (33131 measured)
latency change    = 0.190174
throughput change = 0.000602886
Class 0:
Packet latency average = 2880
	minimum = 588
	maximum = 5245
Network latency average = 752.235
	minimum = 23
	maximum = 2854
Slowest packet = 8441
Flit latency average = 915.841
	minimum = 6
	maximum = 5503
Slowest flit = 43360
Fragmentation average = 176.129
	minimum = 0
	maximum = 1610
Injected packet rate average = 0.0109896
	minimum = 0 (at node 88)
	maximum = 0.022 (at node 19)
Accepted packet rate average = 0.0108767
	minimum = 0.00566667 (at node 113)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.197733
	minimum = 0 (at node 88)
	maximum = 0.395333 (at node 19)
Accepted flit rate average= 0.195998
	minimum = 0.111 (at node 113)
	maximum = 0.283 (at node 16)
Injected packet length average = 17.9927
Accepted packet length average = 18.02
Total in-flight flits = 37827 (36953 measured)
latency change    = 0.140112
throughput change = 0.00826875
Class 0:
Packet latency average = 3234.53
	minimum = 588
	maximum = 5955
Network latency average = 842.203
	minimum = 23
	maximum = 3693
Slowest packet = 8441
Flit latency average = 918.531
	minimum = 6
	maximum = 5790
Slowest flit = 54665
Fragmentation average = 183.741
	minimum = 0
	maximum = 2624
Injected packet rate average = 0.0109206
	minimum = 0.00025 (at node 88)
	maximum = 0.019 (at node 126)
Accepted packet rate average = 0.0108685
	minimum = 0.00675 (at node 180)
	maximum = 0.01575 (at node 129)
Injected flit rate average = 0.196543
	minimum = 0.00325 (at node 88)
	maximum = 0.3435 (at node 126)
Accepted flit rate average= 0.195546
	minimum = 0.1245 (at node 180)
	maximum = 0.2835 (at node 129)
Injected packet length average = 17.9975
Accepted packet length average = 17.992
Total in-flight flits = 37659 (37307 measured)
latency change    = 0.109609
throughput change = 0.00231502
Class 0:
Packet latency average = 3556.77
	minimum = 588
	maximum = 6843
Network latency average = 898.742
	minimum = 23
	maximum = 4732
Slowest packet = 8441
Flit latency average = 924.213
	minimum = 6
	maximum = 7414
Slowest flit = 18611
Fragmentation average = 187.994
	minimum = 0
	maximum = 2624
Injected packet rate average = 0.0107656
	minimum = 0.0014 (at node 88)
	maximum = 0.0186 (at node 150)
Accepted packet rate average = 0.0107906
	minimum = 0.0076 (at node 36)
	maximum = 0.0148 (at node 129)
Injected flit rate average = 0.193908
	minimum = 0.0252 (at node 88)
	maximum = 0.3348 (at node 150)
Accepted flit rate average= 0.194332
	minimum = 0.1374 (at node 89)
	maximum = 0.2694 (at node 129)
Injected packet length average = 18.0118
Accepted packet length average = 18.0094
Total in-flight flits = 36253 (36099 measured)
latency change    = 0.0905975
throughput change = 0.00624333
Class 0:
Packet latency average = 3891.12
	minimum = 588
	maximum = 7860
Network latency average = 930.629
	minimum = 23
	maximum = 5271
Slowest packet = 8441
Flit latency average = 923.5
	minimum = 6
	maximum = 8061
Slowest flit = 70748
Fragmentation average = 191.469
	minimum = 0
	maximum = 2895
Injected packet rate average = 0.0107674
	minimum = 0.004 (at node 88)
	maximum = 0.0183333 (at node 165)
Accepted packet rate average = 0.0107925
	minimum = 0.0075 (at node 162)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.193839
	minimum = 0.0703333 (at node 88)
	maximum = 0.329167 (at node 165)
Accepted flit rate average= 0.194195
	minimum = 0.136667 (at node 162)
	maximum = 0.256167 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 17.9935
Total in-flight flits = 36485 (36437 measured)
latency change    = 0.0859281
throughput change = 0.000705368
Class 0:
Packet latency average = 4234.49
	minimum = 588
	maximum = 8344
Network latency average = 958.073
	minimum = 23
	maximum = 6194
Slowest packet = 8441
Flit latency average = 926.678
	minimum = 6
	maximum = 8061
Slowest flit = 70748
Fragmentation average = 194.553
	minimum = 0
	maximum = 3664
Injected packet rate average = 0.0107701
	minimum = 0.00428571 (at node 100)
	maximum = 0.0171429 (at node 150)
Accepted packet rate average = 0.0107939
	minimum = 0.00771429 (at node 79)
	maximum = 0.0135714 (at node 128)
Injected flit rate average = 0.19388
	minimum = 0.0792857 (at node 100)
	maximum = 0.309571 (at node 150)
Accepted flit rate average= 0.194202
	minimum = 0.141143 (at node 79)
	maximum = 0.246286 (at node 129)
Injected packet length average = 18.0017
Accepted packet length average = 17.9919
Total in-flight flits = 36504 (36502 measured)
latency change    = 0.0810875
throughput change = 3.63974e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 111112 111113 201492 201493 201494 201495 201496 201497 201498 201499 [...] (35957 flits)
Measured flits: 201492 201493 201494 201495 201496 201497 201498 201499 201500 201501 [...] (35955 flits)
Class 0:
Remaining flits: 201500 201501 201502 201503 201504 201505 201506 201507 201508 201509 [...] (35388 flits)
Measured flits: 201500 201501 201502 201503 201504 201505 201506 201507 201508 201509 [...] (35388 flits)
Class 0:
Remaining flits: 201509 220284 220285 220286 220287 220288 220289 220290 220291 220292 [...] (36067 flits)
Measured flits: 201509 220284 220285 220286 220287 220288 220289 220290 220291 220292 [...] (36067 flits)
Class 0:
Remaining flits: 220284 220285 220286 220287 220288 220289 220290 220291 220292 220293 [...] (36086 flits)
Measured flits: 220284 220285 220286 220287 220288 220289 220290 220291 220292 220293 [...] (36086 flits)
Class 0:
Remaining flits: 337390 337391 346176 346177 346178 346179 346180 346181 346182 346183 [...] (36761 flits)
Measured flits: 337390 337391 346176 346177 346178 346179 346180 346181 346182 346183 [...] (36761 flits)
Class 0:
Remaining flits: 397918 397919 397920 397921 397922 397923 397924 397925 397983 397984 [...] (35688 flits)
Measured flits: 397918 397919 397920 397921 397922 397923 397924 397925 397983 397984 [...] (35670 flits)
Class 0:
Remaining flits: 403326 403327 403328 403329 403330 403331 403332 403333 403334 403335 [...] (35768 flits)
Measured flits: 403326 403327 403328 403329 403330 403331 403332 403333 403334 403335 [...] (35197 flits)
Class 0:
Remaining flits: 447386 447387 447388 447389 470115 470116 470117 470118 470119 470120 [...] (36379 flits)
Measured flits: 447386 447387 447388 447389 470115 470116 470117 470118 470119 470120 [...] (35700 flits)
Class 0:
Remaining flits: 495432 495433 495434 495435 495436 495437 495438 495439 495440 495441 [...] (35815 flits)
Measured flits: 495432 495433 495434 495435 495436 495437 495438 495439 495440 495441 [...] (34509 flits)
Class 0:
Remaining flits: 497830 497831 497832 497833 497834 497835 497836 497837 497838 497839 [...] (36600 flits)
Measured flits: 497830 497831 497832 497833 497834 497835 497836 497837 497838 497839 [...] (34279 flits)
Class 0:
Remaining flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (37574 flits)
Measured flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (34081 flits)
Class 0:
Remaining flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (37300 flits)
Measured flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (31583 flits)
Class 0:
Remaining flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (36658 flits)
Measured flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (28355 flits)
Class 0:
Remaining flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (37528 flits)
Measured flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (25140 flits)
Class 0:
Remaining flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (36066 flits)
Measured flits: 528192 528193 528194 528195 528196 528197 528198 528199 528200 528201 [...] (20900 flits)
Class 0:
Remaining flits: 528195 528196 528197 528198 528199 528200 528201 528202 528203 528204 [...] (36450 flits)
Measured flits: 528195 528196 528197 528198 528199 528200 528201 528202 528203 528204 [...] (16486 flits)
Class 0:
Remaining flits: 632106 632107 632108 632109 632110 632111 632112 632113 632114 632115 [...] (35520 flits)
Measured flits: 632106 632107 632108 632109 632110 632111 632112 632113 632114 632115 [...] (13326 flits)
Class 0:
Remaining flits: 632106 632107 632108 632109 632110 632111 632112 632113 632114 632115 [...] (35088 flits)
Measured flits: 632106 632107 632108 632109 632110 632111 632112 632113 632114 632115 [...] (11871 flits)
Class 0:
Remaining flits: 632122 632123 773298 773299 773300 773301 773302 773303 773304 773305 [...] (36181 flits)
Measured flits: 632122 632123 773298 773299 773300 773301 773302 773303 773304 773305 [...] (9575 flits)
Class 0:
Remaining flits: 799020 799021 799022 799023 799024 799025 799026 799027 799028 799029 [...] (35143 flits)
Measured flits: 799020 799021 799022 799023 799024 799025 799026 799027 799028 799029 [...] (7665 flits)
Class 0:
Remaining flits: 805446 805447 805448 805449 805450 805451 805452 805453 805454 805455 [...] (36624 flits)
Measured flits: 805446 805447 805448 805449 805450 805451 805452 805453 805454 805455 [...] (6213 flits)
Class 0:
Remaining flits: 874623 874624 874625 874626 874627 874628 874629 874630 874631 874632 [...] (36626 flits)
Measured flits: 874623 874624 874625 874626 874627 874628 874629 874630 874631 874632 [...] (5119 flits)
Class 0:
Remaining flits: 884376 884377 884378 884379 884380 884381 884382 884383 884384 884385 [...] (37653 flits)
Measured flits: 955586 955587 955588 955589 955590 955591 955592 955593 955594 955595 [...] (4713 flits)
Class 0:
Remaining flits: 955586 955587 955588 955589 955590 955591 955592 955593 955594 955595 [...] (34824 flits)
Measured flits: 955586 955587 955588 955589 955590 955591 955592 955593 955594 955595 [...] (3572 flits)
Class 0:
Remaining flits: 955598 955599 955600 955601 984096 984097 984098 984099 984100 984101 [...] (36017 flits)
Measured flits: 955598 955599 955600 955601 1007812 1007813 1007814 1007815 1007816 1007817 [...] (2881 flits)
Class 0:
Remaining flits: 1053450 1053451 1053452 1053453 1053454 1053455 1053456 1053457 1053458 1053459 [...] (36628 flits)
Measured flits: 1053450 1053451 1053452 1053453 1053454 1053455 1053456 1053457 1053458 1053459 [...] (2383 flits)
Class 0:
Remaining flits: 1062164 1062165 1062166 1062167 1062168 1062169 1062170 1062171 1062172 1062173 [...] (36753 flits)
Measured flits: 1230828 1230829 1230830 1230831 1230832 1230833 1230834 1230835 1230836 1230837 [...] (1605 flits)
Class 0:
Remaining flits: 1062166 1062167 1062168 1062169 1062170 1062171 1062172 1062173 1062174 1062175 [...] (36496 flits)
Measured flits: 1268838 1268839 1268840 1268841 1268842 1268843 1268844 1268845 1268846 1268847 [...] (1230 flits)
Class 0:
Remaining flits: 1062169 1062170 1062171 1062172 1062173 1062174 1062175 1062176 1062177 1062178 [...] (37436 flits)
Measured flits: 1268838 1268839 1268840 1268841 1268842 1268843 1268844 1268845 1268846 1268847 [...] (1046 flits)
Class 0:
Remaining flits: 1146596 1146597 1146598 1146599 1158092 1158093 1158094 1158095 1158096 1158097 [...] (36524 flits)
Measured flits: 1268838 1268839 1268840 1268841 1268842 1268843 1268844 1268845 1268846 1268847 [...] (830 flits)
Class 0:
Remaining flits: 1146599 1231704 1231705 1231706 1231707 1231708 1231709 1231710 1231711 1231712 [...] (37673 flits)
Measured flits: 1273950 1273951 1273952 1273953 1273954 1273955 1273956 1273957 1273958 1273959 [...] (582 flits)
Class 0:
Remaining flits: 1231704 1231705 1231706 1231707 1231708 1231709 1231710 1231711 1231712 1231713 [...] (37554 flits)
Measured flits: 1329696 1329697 1329698 1329699 1329700 1329701 1329702 1329703 1329704 1329705 [...] (429 flits)
Class 0:
Remaining flits: 1231704 1231705 1231706 1231707 1231708 1231709 1231710 1231711 1231712 1231713 [...] (36796 flits)
Measured flits: 1349102 1349103 1349104 1349105 1349106 1349107 1349108 1349109 1349110 1349111 [...] (226 flits)
Class 0:
Remaining flits: 1231721 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 [...] (37513 flits)
Measured flits: 1561338 1561339 1561340 1561341 1561342 1561343 1561344 1561345 1561346 1561347 [...] (177 flits)
Class 0:
Remaining flits: 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 1331325 [...] (36454 flits)
Measured flits: 1561338 1561339 1561340 1561341 1561342 1561343 1561344 1561345 1561346 1561347 [...] (164 flits)
Class 0:
Remaining flits: 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 1331325 [...] (38005 flits)
Measured flits: 1561338 1561339 1561340 1561341 1561342 1561343 1561344 1561345 1561346 1561347 [...] (96 flits)
Class 0:
Remaining flits: 1331319 1331320 1331321 1331322 1331323 1331324 1331325 1331326 1331327 1331328 [...] (36430 flits)
Measured flits: 1561338 1561339 1561340 1561341 1561342 1561343 1561344 1561345 1561346 1561347 [...] (36 flits)
Class 0:
Remaining flits: 1481148 1481149 1481150 1481151 1481152 1481153 1481154 1481155 1481156 1481157 [...] (35594 flits)
Measured flits: 1730106 1730107 1730108 1730109 1730110 1730111 1730112 1730113 1730114 1730115 [...] (18 flits)
Class 0:
Remaining flits: 1517562 1517563 1517564 1517565 1517566 1517567 1517568 1517569 1517570 1517571 [...] (36252 flits)
Measured flits: 1730106 1730107 1730108 1730109 1730110 1730111 1730112 1730113 1730114 1730115 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1517562 1517563 1517564 1517565 1517566 1517567 1517568 1517569 1517570 1517571 [...] (6996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1675548 1675549 1675550 1675551 1675552 1675553 1675554 1675555 1675556 1675557 [...] (631 flits)
Measured flits: (0 flits)
Time taken is 51932 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10566.6 (1 samples)
	minimum = 588 (1 samples)
	maximum = 39524 (1 samples)
Network latency average = 1069.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13057 (1 samples)
Flit latency average = 957.991 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12986 (1 samples)
Fragmentation average = 206.093 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5044 (1 samples)
Injected packet rate average = 0.0107701 (1 samples)
	minimum = 0.00428571 (1 samples)
	maximum = 0.0171429 (1 samples)
Accepted packet rate average = 0.0107939 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0135714 (1 samples)
Injected flit rate average = 0.19388 (1 samples)
	minimum = 0.0792857 (1 samples)
	maximum = 0.309571 (1 samples)
Accepted flit rate average = 0.194202 (1 samples)
	minimum = 0.141143 (1 samples)
	maximum = 0.246286 (1 samples)
Injected packet size average = 18.0017 (1 samples)
Accepted packet size average = 17.9919 (1 samples)
Hops average = 5.06011 (1 samples)
Total run time 57.1994
