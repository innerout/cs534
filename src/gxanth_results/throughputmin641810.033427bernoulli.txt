BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 350.98
	minimum = 23
	maximum = 903
Network latency average = 334.447
	minimum = 23
	maximum = 889
Slowest packet = 449
Flit latency average = 238.16
	minimum = 6
	maximum = 912
Slowest flit = 6705
Fragmentation average = 229.042
	minimum = 0
	maximum = 842
Injected packet rate average = 0.027599
	minimum = 0.016 (at node 160)
	maximum = 0.04 (at node 137)
Accepted packet rate average = 0.00973958
	minimum = 0.003 (at node 112)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.491849
	minimum = 0.283 (at node 160)
	maximum = 0.713 (at node 137)
Accepted flit rate average= 0.219635
	minimum = 0.104 (at node 41)
	maximum = 0.371 (at node 103)
Injected packet length average = 17.8213
Accepted packet length average = 22.5508
Total in-flight flits = 53248 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 662.498
	minimum = 23
	maximum = 1907
Network latency average = 590.993
	minimum = 23
	maximum = 1888
Slowest packet = 498
Flit latency average = 463.029
	minimum = 6
	maximum = 1872
Slowest flit = 11764
Fragmentation average = 299.89
	minimum = 0
	maximum = 1351
Injected packet rate average = 0.0219896
	minimum = 0.01 (at node 160)
	maximum = 0.03 (at node 165)
Accepted packet rate average = 0.0111823
	minimum = 0.0055 (at node 135)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.392115
	minimum = 0.173 (at node 160)
	maximum = 0.539 (at node 165)
Accepted flit rate average= 0.22369
	minimum = 0.1265 (at node 135)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.8318
Accepted packet length average = 20.004
Total in-flight flits = 66221 (0 measured)
latency change    = 0.470217
throughput change = 0.0181264
Class 0:
Packet latency average = 1438.19
	minimum = 40
	maximum = 2797
Network latency average = 1122.98
	minimum = 30
	maximum = 2761
Slowest packet = 1232
Flit latency average = 992.646
	minimum = 6
	maximum = 2829
Slowest flit = 15851
Fragmentation average = 322.381
	minimum = 3
	maximum = 2212
Injected packet rate average = 0.00864063
	minimum = 0 (at node 4)
	maximum = 0.042 (at node 45)
Accepted packet rate average = 0.0122656
	minimum = 0.005 (at node 70)
	maximum = 0.021 (at node 33)
Injected flit rate average = 0.155104
	minimum = 0 (at node 9)
	maximum = 0.752 (at node 45)
Accepted flit rate average= 0.217104
	minimum = 0.089 (at node 70)
	maximum = 0.373 (at node 34)
Injected packet length average = 17.9506
Accepted packet length average = 17.7002
Total in-flight flits = 54471 (0 measured)
latency change    = 0.539354
throughput change = 0.0303354
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1884.32
	minimum = 364
	maximum = 3281
Network latency average = 306.356
	minimum = 23
	maximum = 945
Slowest packet = 10134
Flit latency average = 1138.15
	minimum = 6
	maximum = 3870
Slowest flit = 12337
Fragmentation average = 151.287
	minimum = 0
	maximum = 826
Injected packet rate average = 0.0102917
	minimum = 0 (at node 1)
	maximum = 0.042 (at node 21)
Accepted packet rate average = 0.0120885
	minimum = 0.004 (at node 31)
	maximum = 0.022 (at node 151)
Injected flit rate average = 0.185266
	minimum = 0 (at node 3)
	maximum = 0.757 (at node 21)
Accepted flit rate average= 0.210292
	minimum = 0.092 (at node 162)
	maximum = 0.405 (at node 99)
Injected packet length average = 18.0015
Accepted packet length average = 17.396
Total in-flight flits = 49681 (22372 measured)
latency change    = 0.236758
throughput change = 0.0323955
Class 0:
Packet latency average = 2303.64
	minimum = 364
	maximum = 4226
Network latency average = 495.44
	minimum = 23
	maximum = 1914
Slowest packet = 10134
Flit latency average = 1107.73
	minimum = 6
	maximum = 4886
Slowest flit = 9070
Fragmentation average = 179.706
	minimum = 0
	maximum = 1226
Injected packet rate average = 0.0114115
	minimum = 0 (at node 3)
	maximum = 0.033 (at node 61)
Accepted packet rate average = 0.0118776
	minimum = 0.007 (at node 5)
	maximum = 0.018 (at node 182)
Injected flit rate average = 0.205305
	minimum = 0 (at node 3)
	maximum = 0.594 (at node 61)
Accepted flit rate average= 0.210687
	minimum = 0.1225 (at node 180)
	maximum = 0.3135 (at node 99)
Injected packet length average = 17.9911
Accepted packet length average = 17.7382
Total in-flight flits = 52515 (36258 measured)
latency change    = 0.182025
throughput change = 0.00187877
Class 0:
Packet latency average = 2658.47
	minimum = 364
	maximum = 5144
Network latency average = 651.569
	minimum = 23
	maximum = 2950
Slowest packet = 10134
Flit latency average = 1121.14
	minimum = 6
	maximum = 5894
Slowest flit = 9601
Fragmentation average = 201.693
	minimum = 0
	maximum = 1930
Injected packet rate average = 0.0112604
	minimum = 0 (at node 3)
	maximum = 0.033 (at node 57)
Accepted packet rate average = 0.011724
	minimum = 0.007 (at node 5)
	maximum = 0.0173333 (at node 99)
Injected flit rate average = 0.202738
	minimum = 0 (at node 33)
	maximum = 0.592667 (at node 57)
Accepted flit rate average= 0.20904
	minimum = 0.139 (at node 5)
	maximum = 0.309 (at node 99)
Injected packet length average = 18.0045
Accepted packet length average = 17.8301
Total in-flight flits = 50776 (40235 measured)
latency change    = 0.133473
throughput change = 0.0078816
Draining remaining packets ...
Class 0:
Remaining flits: 11844 11845 11846 11847 11848 11849 11850 11851 11852 11853 [...] (17967 flits)
Measured flits: 182142 182143 182144 182145 182146 182147 182148 182149 182150 182151 [...] (12721 flits)
Class 0:
Remaining flits: 45263 45264 45265 45266 45267 45268 45269 69977 69978 69979 [...] (242 flits)
Measured flits: 183562 183563 187506 187507 187508 187509 187510 187511 187512 187513 [...] (170 flits)
Time taken is 8097 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3256.31 (1 samples)
	minimum = 364 (1 samples)
	maximum = 6927 (1 samples)
Network latency average = 1065.17 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4955 (1 samples)
Flit latency average = 1503.03 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7646 (1 samples)
Fragmentation average = 222.916 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4179 (1 samples)
Injected packet rate average = 0.0112604 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.033 (1 samples)
Accepted packet rate average = 0.011724 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.202738 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.592667 (1 samples)
Accepted flit rate average = 0.20904 (1 samples)
	minimum = 0.139 (1 samples)
	maximum = 0.309 (1 samples)
Injected packet size average = 18.0045 (1 samples)
Accepted packet size average = 17.8301 (1 samples)
Hops average = 5.13927 (1 samples)
Total run time 11.2325
