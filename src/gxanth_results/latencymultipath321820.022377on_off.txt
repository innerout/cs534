BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 281.443
	minimum = 22
	maximum = 973
Network latency average = 196.969
	minimum = 22
	maximum = 819
Slowest packet = 69
Flit latency average = 164.671
	minimum = 5
	maximum = 802
Slowest flit = 11087
Fragmentation average = 41.909
	minimum = 0
	maximum = 322
Injected packet rate average = 0.0205938
	minimum = 0 (at node 42)
	maximum = 0.052 (at node 171)
Accepted packet rate average = 0.0128229
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.366797
	minimum = 0 (at node 42)
	maximum = 0.934 (at node 171)
Accepted flit rate average= 0.240531
	minimum = 0.097 (at node 174)
	maximum = 0.396 (at node 44)
Injected packet length average = 17.8111
Accepted packet length average = 18.7579
Total in-flight flits = 25152 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 468.976
	minimum = 22
	maximum = 1888
Network latency average = 345.251
	minimum = 22
	maximum = 1518
Slowest packet = 69
Flit latency average = 306.952
	minimum = 5
	maximum = 1501
Slowest flit = 33065
Fragmentation average = 50.6327
	minimum = 0
	maximum = 322
Injected packet rate average = 0.0186432
	minimum = 0.0005 (at node 100)
	maximum = 0.04 (at node 101)
Accepted packet rate average = 0.0137266
	minimum = 0.0075 (at node 116)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.333727
	minimum = 0.009 (at node 100)
	maximum = 0.72 (at node 101)
Accepted flit rate average= 0.251875
	minimum = 0.135 (at node 116)
	maximum = 0.3705 (at node 22)
Injected packet length average = 17.9007
Accepted packet length average = 18.3495
Total in-flight flits = 32484 (0 measured)
latency change    = 0.399878
throughput change = 0.0450372
Class 0:
Packet latency average = 933.849
	minimum = 22
	maximum = 2678
Network latency average = 632.667
	minimum = 22
	maximum = 2128
Slowest packet = 3375
Flit latency average = 583.88
	minimum = 5
	maximum = 2129
Slowest flit = 61817
Fragmentation average = 59.6981
	minimum = 0
	maximum = 304
Injected packet rate average = 0.0169115
	minimum = 0 (at node 143)
	maximum = 0.042 (at node 188)
Accepted packet rate average = 0.0147656
	minimum = 0.005 (at node 22)
	maximum = 0.028 (at node 187)
Injected flit rate average = 0.303724
	minimum = 0.005 (at node 143)
	maximum = 0.772 (at node 188)
Accepted flit rate average= 0.267375
	minimum = 0.1 (at node 22)
	maximum = 0.477 (at node 187)
Injected packet length average = 17.9597
Accepted packet length average = 18.1079
Total in-flight flits = 39684 (0 measured)
latency change    = 0.497803
throughput change = 0.057971
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 894.875
	minimum = 25
	maximum = 3236
Network latency average = 396.512
	minimum = 22
	maximum = 989
Slowest packet = 10431
Flit latency average = 714.675
	minimum = 5
	maximum = 3039
Slowest flit = 64523
Fragmentation average = 43.565
	minimum = 0
	maximum = 244
Injected packet rate average = 0.0163385
	minimum = 0 (at node 105)
	maximum = 0.04 (at node 77)
Accepted packet rate average = 0.0148542
	minimum = 0.006 (at node 158)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.293365
	minimum = 0.005 (at node 105)
	maximum = 0.725 (at node 77)
Accepted flit rate average= 0.268901
	minimum = 0.123 (at node 158)
	maximum = 0.485 (at node 129)
Injected packet length average = 17.9554
Accepted packet length average = 18.1027
Total in-flight flits = 44665 (39859 measured)
latency change    = 0.0435532
throughput change = 0.00567511
Class 0:
Packet latency average = 1347.32
	minimum = 25
	maximum = 4142
Network latency average = 678.916
	minimum = 22
	maximum = 1909
Slowest packet = 10431
Flit latency average = 755.353
	minimum = 5
	maximum = 3058
Slowest flit = 117449
Fragmentation average = 60.0291
	minimum = 0
	maximum = 282
Injected packet rate average = 0.0158646
	minimum = 0.0035 (at node 9)
	maximum = 0.029 (at node 77)
Accepted packet rate average = 0.0148828
	minimum = 0.01 (at node 31)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.285318
	minimum = 0.063 (at node 9)
	maximum = 0.523 (at node 77)
Accepted flit rate average= 0.268461
	minimum = 0.1825 (at node 139)
	maximum = 0.402 (at node 129)
Injected packet length average = 17.9846
Accepted packet length average = 18.0383
Total in-flight flits = 46539 (46392 measured)
latency change    = 0.335812
throughput change = 0.00163936
Class 0:
Packet latency average = 1591.34
	minimum = 24
	maximum = 4928
Network latency average = 778.836
	minimum = 22
	maximum = 2782
Slowest packet = 10431
Flit latency average = 784.309
	minimum = 5
	maximum = 3623
Slowest flit = 105240
Fragmentation average = 64.8436
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0157656
	minimum = 0.005 (at node 105)
	maximum = 0.025 (at node 77)
Accepted packet rate average = 0.0149323
	minimum = 0.0103333 (at node 79)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.283446
	minimum = 0.0903333 (at node 23)
	maximum = 0.449333 (at node 77)
Accepted flit rate average= 0.269047
	minimum = 0.189667 (at node 89)
	maximum = 0.38 (at node 128)
Injected packet length average = 17.9787
Accepted packet length average = 18.0178
Total in-flight flits = 48513 (48513 measured)
latency change    = 0.153341
throughput change = 0.00217783
Class 0:
Packet latency average = 1778.34
	minimum = 22
	maximum = 5875
Network latency average = 834.913
	minimum = 22
	maximum = 3174
Slowest packet = 10431
Flit latency average = 810.532
	minimum = 5
	maximum = 3623
Slowest flit = 105240
Fragmentation average = 68.5343
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0156914
	minimum = 0.00725 (at node 29)
	maximum = 0.024 (at node 134)
Accepted packet rate average = 0.0149284
	minimum = 0.01075 (at node 2)
	maximum = 0.02025 (at node 129)
Injected flit rate average = 0.282217
	minimum = 0.1265 (at node 29)
	maximum = 0.42975 (at node 134)
Accepted flit rate average= 0.269203
	minimum = 0.19325 (at node 104)
	maximum = 0.36825 (at node 129)
Injected packet length average = 17.9855
Accepted packet length average = 18.033
Total in-flight flits = 50232 (50232 measured)
latency change    = 0.105157
throughput change = 0.000580417
Class 0:
Packet latency average = 1943.29
	minimum = 22
	maximum = 5881
Network latency average = 872.138
	minimum = 22
	maximum = 3620
Slowest packet = 10431
Flit latency average = 833.156
	minimum = 5
	maximum = 3623
Slowest flit = 105240
Fragmentation average = 71.5676
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0155708
	minimum = 0.0076 (at node 116)
	maximum = 0.0228 (at node 147)
Accepted packet rate average = 0.0149323
	minimum = 0.0114 (at node 171)
	maximum = 0.0192 (at node 128)
Injected flit rate average = 0.280122
	minimum = 0.1368 (at node 116)
	maximum = 0.4118 (at node 147)
Accepted flit rate average= 0.269286
	minimum = 0.2084 (at node 64)
	maximum = 0.3454 (at node 128)
Injected packet length average = 17.9902
Accepted packet length average = 18.0338
Total in-flight flits = 50629 (50629 measured)
latency change    = 0.0848804
throughput change = 0.00030946
Class 0:
Packet latency average = 2107.18
	minimum = 22
	maximum = 6493
Network latency average = 896.537
	minimum = 22
	maximum = 3620
Slowest packet = 10431
Flit latency average = 848.109
	minimum = 5
	maximum = 3623
Slowest flit = 105240
Fragmentation average = 73.5136
	minimum = 0
	maximum = 355
Injected packet rate average = 0.0154809
	minimum = 0.00883333 (at node 135)
	maximum = 0.0246667 (at node 147)
Accepted packet rate average = 0.0149592
	minimum = 0.0118333 (at node 79)
	maximum = 0.019 (at node 72)
Injected flit rate average = 0.278414
	minimum = 0.159 (at node 135)
	maximum = 0.443833 (at node 147)
Accepted flit rate average= 0.269665
	minimum = 0.215167 (at node 79)
	maximum = 0.339333 (at node 72)
Injected packet length average = 17.9844
Accepted packet length average = 18.0267
Total in-flight flits = 50312 (50312 measured)
latency change    = 0.0777773
throughput change = 0.00140349
Class 0:
Packet latency average = 2256.53
	minimum = 22
	maximum = 6704
Network latency average = 911.091
	minimum = 22
	maximum = 3620
Slowest packet = 10431
Flit latency average = 858.031
	minimum = 5
	maximum = 3623
Slowest flit = 105240
Fragmentation average = 74.6504
	minimum = 0
	maximum = 355
Injected packet rate average = 0.0153698
	minimum = 0.00928571 (at node 116)
	maximum = 0.0225714 (at node 147)
Accepted packet rate average = 0.0149769
	minimum = 0.0118571 (at node 79)
	maximum = 0.0185714 (at node 70)
Injected flit rate average = 0.276524
	minimum = 0.166571 (at node 116)
	maximum = 0.406857 (at node 147)
Accepted flit rate average= 0.269743
	minimum = 0.214571 (at node 79)
	maximum = 0.334714 (at node 70)
Injected packet length average = 17.9914
Accepted packet length average = 18.0105
Total in-flight flits = 49282 (49282 measured)
latency change    = 0.0661833
throughput change = 0.000287789
Draining all recorded packets ...
Class 0:
Remaining flits: 383364 383365 383366 383367 383368 383369 383370 383371 383372 383373 [...] (51203 flits)
Measured flits: 383364 383365 383366 383367 383368 383369 383370 383371 383372 383373 [...] (46287 flits)
Class 0:
Remaining flits: 483372 483373 483374 483375 483376 483377 483378 483379 483380 483381 [...] (51225 flits)
Measured flits: 483372 483373 483374 483375 483376 483377 483378 483379 483380 483381 [...] (40530 flits)
Class 0:
Remaining flits: 515122 515123 535932 535933 535934 535935 535936 535937 535938 535939 [...] (50576 flits)
Measured flits: 515122 515123 535932 535933 535934 535935 535936 535937 535938 535939 [...] (31936 flits)
Class 0:
Remaining flits: 586440 586441 586442 586443 586444 586445 586446 586447 586448 586449 [...] (50537 flits)
Measured flits: 586440 586441 586442 586443 586444 586445 586446 586447 586448 586449 [...] (25479 flits)
Class 0:
Remaining flits: 623250 623251 623252 623253 623254 623255 623256 623257 623258 623259 [...] (51418 flits)
Measured flits: 623250 623251 623252 623253 623254 623255 623256 623257 623258 623259 [...] (20473 flits)
Class 0:
Remaining flits: 623257 623258 623259 623260 623261 623262 623263 623264 623265 623266 [...] (51232 flits)
Measured flits: 623257 623258 623259 623260 623261 623262 623263 623264 623265 623266 [...] (13716 flits)
Class 0:
Remaining flits: 690714 690715 690716 690717 690718 690719 690720 690721 690722 690723 [...] (50751 flits)
Measured flits: 690714 690715 690716 690717 690718 690719 690720 690721 690722 690723 [...] (10167 flits)
Class 0:
Remaining flits: 754286 754287 754288 754289 764064 764065 764066 764067 764068 764069 [...] (50919 flits)
Measured flits: 754286 754287 754288 754289 764064 764065 764066 764067 764068 764069 [...] (6489 flits)
Class 0:
Remaining flits: 853362 853363 853364 853365 853366 853367 853368 853369 853370 853371 [...] (50352 flits)
Measured flits: 879428 879429 879430 879431 879432 879433 879434 879435 879436 879437 [...] (3064 flits)
Class 0:
Remaining flits: 861678 861679 861680 861681 861682 861683 861684 861685 861686 861687 [...] (49613 flits)
Measured flits: 902939 902940 902941 902942 902943 902944 902945 902946 902947 902948 [...] (2066 flits)
Class 0:
Remaining flits: 907182 907183 907184 907185 907186 907187 907188 907189 907190 907191 [...] (48575 flits)
Measured flits: 955090 955091 955092 955093 955094 955095 955096 955097 985140 985141 [...] (1745 flits)
Class 0:
Remaining flits: 965880 965881 965882 965883 965884 965885 965886 965887 965888 965889 [...] (48246 flits)
Measured flits: 1056371 1056372 1056373 1056374 1056375 1056376 1056377 1056378 1056379 1056380 [...] (956 flits)
Class 0:
Remaining flits: 994464 994465 994466 994467 994468 994469 994470 994471 994472 994473 [...] (48646 flits)
Measured flits: 1127394 1127395 1127396 1127397 1127398 1127399 1127400 1127401 1127402 1127403 [...] (705 flits)
Class 0:
Remaining flits: 1033452 1033453 1033454 1033455 1033456 1033457 1033458 1033459 1033460 1033461 [...] (50121 flits)
Measured flits: 1156338 1156339 1156340 1156341 1156342 1156343 1156344 1156345 1156346 1156347 [...] (393 flits)
Class 0:
Remaining flits: 1115674 1115675 1129966 1129967 1140408 1140409 1140410 1140411 1140412 1140413 [...] (50213 flits)
Measured flits: 1220616 1220617 1220618 1220619 1220620 1220621 1220622 1220623 1220624 1220625 [...] (354 flits)
Class 0:
Remaining flits: 1171656 1171657 1171658 1171659 1171660 1171661 1171662 1171663 1171664 1171665 [...] (50450 flits)
Measured flits: 1274904 1274905 1274906 1274907 1274908 1274909 1274910 1274911 1274912 1274913 [...] (144 flits)
Class 0:
Remaining flits: 1195020 1195021 1195022 1195023 1195024 1195025 1195026 1195027 1195028 1195029 [...] (51994 flits)
Measured flits: 1380132 1380133 1380134 1380135 1380136 1380137 1380138 1380139 1380140 1380141 [...] (112 flits)
Class 0:
Remaining flits: 1223816 1223817 1223818 1223819 1254510 1254511 1254512 1254513 1254514 1254515 [...] (50633 flits)
Measured flits: 1428678 1428679 1428680 1428681 1428682 1428683 1428684 1428685 1428686 1428687 [...] (36 flits)
Class 0:
Remaining flits: 1271861 1338066 1338067 1338068 1338069 1338070 1338071 1338072 1338073 1338074 [...] (50854 flits)
Measured flits: 1451605 1451606 1451607 1451608 1451609 1498014 1498015 1498016 1498017 1498018 [...] (59 flits)
Class 0:
Remaining flits: 1354266 1354267 1354268 1354269 1354270 1354271 1354272 1354273 1354274 1354275 [...] (50191 flits)
Measured flits: 1499850 1499851 1499852 1499853 1499854 1499855 1499856 1499857 1499858 1499859 [...] (162 flits)
Class 0:
Remaining flits: 1430550 1430551 1430552 1430553 1430554 1430555 1430556 1430557 1430558 1430559 [...] (49221 flits)
Measured flits: 1613790 1613791 1613792 1613793 1613794 1613795 1613796 1613797 1613798 1613799 [...] (126 flits)
Class 0:
Remaining flits: 1448118 1448119 1448120 1448121 1448122 1448123 1448124 1448125 1448126 1448127 [...] (48916 flits)
Measured flits: 1615716 1615717 1615718 1615719 1615720 1615721 1615722 1615723 1615724 1615725 [...] (140 flits)
Class 0:
Remaining flits: 1506510 1506511 1506512 1506513 1506514 1506515 1506516 1506517 1506518 1506519 [...] (50227 flits)
Measured flits: 1705968 1705969 1705970 1705971 1705972 1705973 1705974 1705975 1705976 1705977 [...] (144 flits)
Class 0:
Remaining flits: 1586358 1586359 1586360 1586361 1586362 1586363 1586364 1586365 1586366 1586367 [...] (49823 flits)
Measured flits: 1705968 1705969 1705970 1705971 1705972 1705973 1705974 1705975 1705976 1705977 [...] (144 flits)
Class 0:
Remaining flits: 1586358 1586359 1586360 1586361 1586362 1586363 1586364 1586365 1586366 1586367 [...] (49649 flits)
Measured flits: 1783944 1783945 1783946 1783947 1783948 1783949 1783950 1783951 1783952 1783953 [...] (127 flits)
Class 0:
Remaining flits: 1597482 1597483 1597484 1597485 1597486 1597487 1597488 1597489 1597490 1597491 [...] (50157 flits)
Measured flits: 1802934 1802935 1802936 1802937 1802938 1802939 1802940 1802941 1802942 1802943 [...] (90 flits)
Class 0:
Remaining flits: 1676736 1676737 1676738 1676739 1676740 1676741 1676742 1676743 1676744 1676745 [...] (50975 flits)
Measured flits: 1802934 1802935 1802936 1802937 1802938 1802939 1802940 1802941 1802942 1802943 [...] (90 flits)
Class 0:
Remaining flits: 1741086 1741087 1741088 1741089 1741090 1741091 1741092 1741093 1741094 1741095 [...] (50462 flits)
Measured flits: 1869858 1869859 1869860 1869861 1869862 1869863 1869864 1869865 1869866 1869867 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1908534 1908535 1908536 1908537 1908538 1908539 1912498 1912499 1915938 1915939 [...] (9102 flits)
Measured flits: (0 flits)
Time taken is 40334 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3774.58 (1 samples)
	minimum = 22 (1 samples)
	maximum = 28619 (1 samples)
Network latency average = 976.456 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4949 (1 samples)
Flit latency average = 926.905 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6688 (1 samples)
Fragmentation average = 78.9053 (1 samples)
	minimum = 0 (1 samples)
	maximum = 355 (1 samples)
Injected packet rate average = 0.0153698 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0225714 (1 samples)
Accepted packet rate average = 0.0149769 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.276524 (1 samples)
	minimum = 0.166571 (1 samples)
	maximum = 0.406857 (1 samples)
Accepted flit rate average = 0.269743 (1 samples)
	minimum = 0.214571 (1 samples)
	maximum = 0.334714 (1 samples)
Injected packet size average = 17.9914 (1 samples)
Accepted packet size average = 18.0105 (1 samples)
Hops average = 5.06293 (1 samples)
Total run time 56.1793
