BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 258
	minimum = 23
	maximum = 904
Network latency average = 253.733
	minimum = 23
	maximum = 873
Slowest packet = 431
Flit latency average = 178.983
	minimum = 6
	maximum = 927
Slowest flit = 4131
Fragmentation average = 152.856
	minimum = 0
	maximum = 773
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00961458
	minimum = 0.002 (at node 127)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.204323
	minimum = 0.072 (at node 174)
	maximum = 0.354 (at node 167)
Injected packet length average = 17.8234
Accepted packet length average = 21.2514
Total in-flight flits = 30376 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 436.456
	minimum = 23
	maximum = 1744
Network latency average = 431.68
	minimum = 23
	maximum = 1744
Slowest packet = 643
Flit latency average = 330.434
	minimum = 6
	maximum = 1892
Slowest flit = 5305
Fragmentation average = 219.947
	minimum = 0
	maximum = 1647
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0110469
	minimum = 0.006 (at node 81)
	maximum = 0.017 (at node 98)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.217799
	minimum = 0.115 (at node 81)
	maximum = 0.3285 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.7159
Total in-flight flits = 54569 (0 measured)
latency change    = 0.408876
throughput change = 0.061876
Class 0:
Packet latency average = 865.356
	minimum = 25
	maximum = 2757
Network latency average = 860.763
	minimum = 25
	maximum = 2752
Slowest packet = 823
Flit latency average = 741.868
	minimum = 6
	maximum = 2802
Slowest flit = 8699
Fragmentation average = 311.45
	minimum = 0
	maximum = 2456
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0124792
	minimum = 0.006 (at node 138)
	maximum = 0.024 (at node 145)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.231802
	minimum = 0.097 (at node 31)
	maximum = 0.384 (at node 16)
Injected packet length average = 17.9811
Accepted packet length average = 18.5751
Total in-flight flits = 79741 (0 measured)
latency change    = 0.495633
throughput change = 0.0604076
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 268.481
	minimum = 24
	maximum = 966
Network latency average = 263.556
	minimum = 24
	maximum = 960
Slowest packet = 11575
Flit latency average = 1031.25
	minimum = 6
	maximum = 3830
Slowest flit = 8705
Fragmentation average = 134.217
	minimum = 1
	maximum = 633
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0130677
	minimum = 0.003 (at node 41)
	maximum = 0.024 (at node 78)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.236807
	minimum = 0.103 (at node 158)
	maximum = 0.427 (at node 182)
Injected packet length average = 17.9618
Accepted packet length average = 18.1216
Total in-flight flits = 105896 (58957 measured)
latency change    = 2.22316
throughput change = 0.0211362
Class 0:
Packet latency average = 608.682
	minimum = 24
	maximum = 1970
Network latency average = 603.714
	minimum = 24
	maximum = 1970
Slowest packet = 11588
Flit latency average = 1187.86
	minimum = 6
	maximum = 4698
Slowest flit = 17027
Fragmentation average = 225.246
	minimum = 1
	maximum = 1751
Injected packet rate average = 0.020237
	minimum = 0.0125 (at node 116)
	maximum = 0.027 (at node 157)
Accepted packet rate average = 0.0130729
	minimum = 0.007 (at node 139)
	maximum = 0.0225 (at node 0)
Injected flit rate average = 0.364182
	minimum = 0.225 (at node 116)
	maximum = 0.4895 (at node 157)
Accepted flit rate average= 0.237984
	minimum = 0.132 (at node 41)
	maximum = 0.427 (at node 0)
Injected packet length average = 17.9959
Accepted packet length average = 18.2044
Total in-flight flits = 128233 (100900 measured)
latency change    = 0.558914
throughput change = 0.00494605
Class 0:
Packet latency average = 926.662
	minimum = 24
	maximum = 2970
Network latency average = 921.051
	minimum = 24
	maximum = 2970
Slowest packet = 11573
Flit latency average = 1342.06
	minimum = 6
	maximum = 5660
Slowest flit = 22601
Fragmentation average = 264.818
	minimum = 0
	maximum = 2132
Injected packet rate average = 0.0202205
	minimum = 0.0133333 (at node 116)
	maximum = 0.027 (at node 57)
Accepted packet rate average = 0.0130729
	minimum = 0.00766667 (at node 139)
	maximum = 0.019 (at node 0)
Injected flit rate average = 0.364168
	minimum = 0.24 (at node 116)
	maximum = 0.487 (at node 57)
Accepted flit rate average= 0.237028
	minimum = 0.139333 (at node 139)
	maximum = 0.343 (at node 0)
Injected packet length average = 18.0099
Accepted packet length average = 18.1312
Total in-flight flits = 152859 (136369 measured)
latency change    = 0.343146
throughput change = 0.0040358
Class 0:
Packet latency average = 1228.75
	minimum = 24
	maximum = 3892
Network latency average = 1221.86
	minimum = 24
	maximum = 3883
Slowest packet = 11917
Flit latency average = 1497.96
	minimum = 6
	maximum = 6712
Slowest flit = 12896
Fragmentation average = 289.665
	minimum = 0
	maximum = 2709
Injected packet rate average = 0.0200833
	minimum = 0.01475 (at node 155)
	maximum = 0.0255 (at node 37)
Accepted packet rate average = 0.0130664
	minimum = 0.0085 (at node 139)
	maximum = 0.01875 (at node 103)
Injected flit rate average = 0.361273
	minimum = 0.26875 (at node 155)
	maximum = 0.459 (at node 37)
Accepted flit rate average= 0.236221
	minimum = 0.155 (at node 139)
	maximum = 0.3245 (at node 103)
Injected packet length average = 17.9887
Accepted packet length average = 18.0785
Total in-flight flits = 175955 (165274 measured)
latency change    = 0.245849
throughput change = 0.00341385
Class 0:
Packet latency average = 1488.17
	minimum = 23
	maximum = 4911
Network latency average = 1478.91
	minimum = 23
	maximum = 4887
Slowest packet = 11865
Flit latency average = 1659.19
	minimum = 6
	maximum = 7437
Slowest flit = 35582
Fragmentation average = 307.044
	minimum = 0
	maximum = 3127
Injected packet rate average = 0.0200052
	minimum = 0.0156 (at node 116)
	maximum = 0.0254 (at node 89)
Accepted packet rate average = 0.013001
	minimum = 0.0084 (at node 139)
	maximum = 0.0174 (at node 0)
Injected flit rate average = 0.359968
	minimum = 0.2776 (at node 116)
	maximum = 0.4574 (at node 89)
Accepted flit rate average= 0.234984
	minimum = 0.1568 (at node 139)
	maximum = 0.3172 (at node 0)
Injected packet length average = 17.9937
Accepted packet length average = 18.0743
Total in-flight flits = 199846 (192855 measured)
latency change    = 0.174321
throughput change = 0.00526409
Class 0:
Packet latency average = 1742.66
	minimum = 23
	maximum = 5919
Network latency average = 1729.84
	minimum = 23
	maximum = 5919
Slowest packet = 11586
Flit latency average = 1821.51
	minimum = 6
	maximum = 8328
Slowest flit = 43000
Fragmentation average = 320.514
	minimum = 0
	maximum = 4953
Injected packet rate average = 0.0198559
	minimum = 0.0158333 (at node 152)
	maximum = 0.025 (at node 73)
Accepted packet rate average = 0.0129488
	minimum = 0.00883333 (at node 104)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.357253
	minimum = 0.283833 (at node 152)
	maximum = 0.452833 (at node 73)
Accepted flit rate average= 0.233554
	minimum = 0.168 (at node 104)
	maximum = 0.301667 (at node 103)
Injected packet length average = 17.9923
Accepted packet length average = 18.0367
Total in-flight flits = 222419 (217629 measured)
latency change    = 0.146039
throughput change = 0.00612516
Class 0:
Packet latency average = 1970.39
	minimum = 23
	maximum = 6791
Network latency average = 1950.15
	minimum = 23
	maximum = 6791
Slowest packet = 11830
Flit latency average = 1972.49
	minimum = 6
	maximum = 9244
Slowest flit = 12905
Fragmentation average = 329.159
	minimum = 0
	maximum = 4953
Injected packet rate average = 0.0196845
	minimum = 0.0158571 (at node 152)
	maximum = 0.0245714 (at node 73)
Accepted packet rate average = 0.0128847
	minimum = 0.00942857 (at node 190)
	maximum = 0.0165714 (at node 103)
Injected flit rate average = 0.354221
	minimum = 0.284 (at node 152)
	maximum = 0.442286 (at node 73)
Accepted flit rate average= 0.232176
	minimum = 0.174429 (at node 104)
	maximum = 0.299 (at node 103)
Injected packet length average = 17.9949
Accepted packet length average = 18.0196
Total in-flight flits = 243904 (240202 measured)
latency change    = 0.115575
throughput change = 0.0059329
Draining all recorded packets ...
Class 0:
Remaining flits: 49147 49148 49149 49150 49151 49152 49153 49154 49155 49156 [...] (267698 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (212553 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (289876 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (183216 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (315527 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (153782 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (340195 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (128212 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (367088 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (106568 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (392352 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (89977 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (418194 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (76231 flits)
Class 0:
Remaining flits: 50388 50389 50390 50391 50392 50393 50394 50395 50396 50397 [...] (443205 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (64911 flits)
Class 0:
Remaining flits: 71766 71767 71768 71769 71770 71771 71772 71773 71774 71775 [...] (470578 flits)
Measured flits: 208116 208117 208118 208119 208120 208121 208122 208123 208124 208125 [...] (54984 flits)
Class 0:
Remaining flits: 71766 71767 71768 71769 71770 71771 71772 71773 71774 71775 [...] (497954 flits)
Measured flits: 210312 210313 210314 210315 210316 210317 210318 210319 210320 210321 [...] (46225 flits)
Class 0:
Remaining flits: 120402 120403 120404 120405 120406 120407 120408 120409 120410 120411 [...] (523344 flits)
Measured flits: 210326 210327 210328 210329 211230 211231 211232 211233 211234 211235 [...] (38762 flits)
Class 0:
Remaining flits: 137916 137917 137918 137919 137920 137921 137922 137923 137924 137925 [...] (546446 flits)
Measured flits: 211230 211231 211232 211233 211234 211235 211236 211237 211238 211239 [...] (32929 flits)
Class 0:
Remaining flits: 137916 137917 137918 137919 137920 137921 137922 137923 137924 137925 [...] (569513 flits)
Measured flits: 211230 211231 211232 211233 211234 211235 211236 211237 211238 211239 [...] (27851 flits)
Class 0:
Remaining flits: 137916 137917 137918 137919 137920 137921 137922 137923 137924 137925 [...] (592573 flits)
Measured flits: 217674 217675 217676 217677 217678 217679 217680 217681 217682 217683 [...] (23273 flits)
Class 0:
Remaining flits: 137933 188928 188929 188930 188931 188932 188933 188934 188935 188936 [...] (613763 flits)
Measured flits: 217674 217675 217676 217677 217678 217679 217680 217681 217682 217683 [...] (19480 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (636576 flits)
Measured flits: 218178 218179 218180 218181 218182 218183 218184 218185 218186 218187 [...] (16199 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (656286 flits)
Measured flits: 218178 218179 218180 218181 218182 218183 218184 218185 218186 218187 [...] (13649 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (674200 flits)
Measured flits: 218178 218179 218180 218181 218182 218183 218184 218185 218186 218187 [...] (11725 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (688102 flits)
Measured flits: 218178 218179 218180 218181 218182 218183 218184 218185 218186 218187 [...] (10004 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (695655 flits)
Measured flits: 220950 220951 220952 220953 220954 220955 220956 220957 220958 220959 [...] (8579 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (701336 flits)
Measured flits: 223722 223723 223724 223725 223726 223727 223728 223729 223730 223731 [...] (7264 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (703608 flits)
Measured flits: 223725 223726 223727 223728 223729 223730 223731 223732 223733 223734 [...] (6074 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (709228 flits)
Measured flits: 223725 223726 223727 223728 223729 223730 223731 223732 223733 223734 [...] (5145 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (710622 flits)
Measured flits: 223725 223726 223727 223728 223729 223730 223731 223732 223733 223734 [...] (4404 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (713431 flits)
Measured flits: 223725 223726 223727 223728 223729 223730 223731 223732 223733 223734 [...] (3824 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (720497 flits)
Measured flits: 295614 295615 295616 295617 295618 295619 295620 295621 295622 295623 [...] (3193 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (721691 flits)
Measured flits: 297234 297235 297236 297237 297238 297239 297240 297241 297242 297243 [...] (2721 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (718838 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 314856 314857 [...] (2285 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (712189 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 314856 314857 [...] (2019 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (708929 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 314856 314857 [...] (1673 flits)
Class 0:
Remaining flits: 188928 188929 188930 188931 188932 188933 188934 188935 188936 188937 [...] (715217 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 314856 314857 [...] (1285 flits)
Class 0:
Remaining flits: 297244 297245 297246 297247 297248 297249 297250 297251 372870 372871 [...] (711179 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 372870 372871 [...] (1060 flits)
Class 0:
Remaining flits: 297244 297245 297246 297247 297248 297249 297250 297251 372870 372871 [...] (711542 flits)
Measured flits: 297244 297245 297246 297247 297248 297249 297250 297251 372870 372871 [...] (985 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (707823 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (792 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (707177 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (750 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (708852 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (682 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (711241 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (600 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (714744 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (558 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (710666 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (499 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (711379 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (459 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (711839 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (376 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (708971 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (376 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (708370 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (316 flits)
Class 0:
Remaining flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (704054 flits)
Measured flits: 372870 372871 372872 372873 372874 372875 372876 372877 372878 372879 [...] (244 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (701152 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (101 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (702179 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (90 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (699656 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (86 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (699464 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (77 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (699422 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (54 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (698570 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (36 flits)
Class 0:
Remaining flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (701528 flits)
Measured flits: 510372 510373 510374 510375 510376 510377 510378 510379 510380 510381 [...] (36 flits)
Class 0:
Remaining flits: 510389 665604 665605 665606 665607 665608 665609 665610 665611 665612 [...] (698935 flits)
Measured flits: 510389 665604 665605 665606 665607 665608 665609 665610 665611 665612 [...] (19 flits)
Class 0:
Remaining flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (695892 flits)
Measured flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (18 flits)
Class 0:
Remaining flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (692671 flits)
Measured flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (18 flits)
Class 0:
Remaining flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (693612 flits)
Measured flits: 665604 665605 665606 665607 665608 665609 665610 665611 665612 665613 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 713700 713701 713702 713703 713704 713705 713706 713707 713708 713709 [...] (656065 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 713700 713701 713702 713703 713704 713705 713706 713707 713708 713709 [...] (622611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 713700 713701 713702 713703 713704 713705 713706 713707 713708 713709 [...] (588731 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 713700 713701 713702 713703 713704 713705 713706 713707 713708 713709 [...] (553961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (519594 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (484443 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (451922 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (419366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (386494 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (353741 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 809802 809803 809804 809805 809806 809807 809808 809809 809810 809811 [...] (320182 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (288244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (257212 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (225404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (194621 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (165907 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 920556 920557 920558 920559 920560 920561 920562 920563 920564 920565 [...] (137888 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 920556 920557 920558 920559 920560 920561 920562 920563 920564 920565 [...] (111203 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 952812 952813 952814 952815 952816 952817 952818 952819 952820 952821 [...] (87577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 952812 952813 952814 952815 952816 952817 952818 952819 952820 952821 [...] (65358 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 952822 952823 952824 952825 952826 952827 952828 952829 1345950 1345951 [...] (44748 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379160 1379161 1379162 1379163 1379164 1379165 1379166 1379167 1379168 1379169 [...] (26775 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1437823 1437824 1437825 1437826 1437827 1437828 1437829 1437830 1437831 1437832 [...] (12580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1437823 1437824 1437825 1437826 1437827 1437828 1437829 1437830 1437831 1437832 [...] (5934 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692702 1692703 1692704 1692705 1692706 1692707 1692708 1692709 1692710 1692711 [...] (2721 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692702 1692703 1692704 1692705 1692706 1692707 1692708 1692709 1692710 1692711 [...] (1723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692702 1692703 1692704 1692705 1692706 1692707 1692708 1692709 1692710 1692711 [...] (724 flits)
Measured flits: (0 flits)
Time taken is 93385 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5331.84 (1 samples)
	minimum = 23 (1 samples)
	maximum = 55948 (1 samples)
Network latency average = 5238.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 55948 (1 samples)
Flit latency average = 14383.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 73073 (1 samples)
Fragmentation average = 362.617 (1 samples)
	minimum = 0 (1 samples)
	maximum = 15495 (1 samples)
Injected packet rate average = 0.0196845 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0245714 (1 samples)
Accepted packet rate average = 0.0128847 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0165714 (1 samples)
Injected flit rate average = 0.354221 (1 samples)
	minimum = 0.284 (1 samples)
	maximum = 0.442286 (1 samples)
Accepted flit rate average = 0.232176 (1 samples)
	minimum = 0.174429 (1 samples)
	maximum = 0.299 (1 samples)
Injected packet size average = 17.9949 (1 samples)
Accepted packet size average = 18.0196 (1 samples)
Hops average = 5.0733 (1 samples)
Total run time 173.698
