BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 369.688
	minimum = 27
	maximum = 981
Network latency average = 275.277
	minimum = 23
	maximum = 951
Slowest packet = 64
Flit latency average = 210.079
	minimum = 6
	maximum = 951
Slowest flit = 3789
Fragmentation average = 142.311
	minimum = 0
	maximum = 854
Injected packet rate average = 0.027625
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0101302
	minimum = 0.004 (at node 79)
	maximum = 0.019 (at node 48)
Injected flit rate average = 0.491703
	minimum = 0 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.206953
	minimum = 0.078 (at node 83)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.7992
Accepted packet length average = 20.4293
Total in-flight flits = 55737 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.064
	minimum = 23
	maximum = 1972
Network latency average = 491.252
	minimum = 23
	maximum = 1823
Slowest packet = 64
Flit latency average = 418.951
	minimum = 6
	maximum = 1885
Slowest flit = 7809
Fragmentation average = 179.566
	minimum = 0
	maximum = 1469
Injected packet rate average = 0.028987
	minimum = 0.001 (at node 138)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0113802
	minimum = 0.0055 (at node 10)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.519185
	minimum = 0.018 (at node 138)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.219055
	minimum = 0.1135 (at node 116)
	maximum = 0.3175 (at node 56)
Injected packet length average = 17.911
Accepted packet length average = 19.2487
Total in-flight flits = 116241 (0 measured)
latency change    = 0.400892
throughput change = 0.0552445
Class 0:
Packet latency average = 1294.11
	minimum = 30
	maximum = 2915
Network latency average = 1097.74
	minimum = 25
	maximum = 2698
Slowest packet = 173
Flit latency average = 1035.71
	minimum = 6
	maximum = 2762
Slowest flit = 19020
Fragmentation average = 213.133
	minimum = 0
	maximum = 2312
Injected packet rate average = 0.0320208
	minimum = 0 (at node 172)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0132344
	minimum = 0.004 (at node 164)
	maximum = 0.025 (at node 11)
Injected flit rate average = 0.577255
	minimum = 0 (at node 172)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.238484
	minimum = 0.1 (at node 164)
	maximum = 0.487 (at node 82)
Injected packet length average = 18.0275
Accepted packet length average = 18.0201
Total in-flight flits = 181116 (0 measured)
latency change    = 0.523176
throughput change = 0.0814715
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 396.361
	minimum = 29
	maximum = 1994
Network latency average = 88.6734
	minimum = 23
	maximum = 905
Slowest packet = 17305
Flit latency average = 1517.62
	minimum = 6
	maximum = 3670
Slowest flit = 27695
Fragmentation average = 29.0081
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0309271
	minimum = 0.002 (at node 27)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0131198
	minimum = 0.004 (at node 126)
	maximum = 0.022 (at node 137)
Injected flit rate average = 0.555724
	minimum = 0.036 (at node 27)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.235146
	minimum = 0.085 (at node 126)
	maximum = 0.396 (at node 137)
Injected packet length average = 17.9688
Accepted packet length average = 17.923
Total in-flight flits = 242852 (97546 measured)
latency change    = 2.26499
throughput change = 0.0141977
Class 0:
Packet latency average = 545.229
	minimum = 29
	maximum = 2237
Network latency average = 275.612
	minimum = 23
	maximum = 1957
Slowest packet = 17305
Flit latency average = 1722.35
	minimum = 6
	maximum = 4867
Slowest flit = 10072
Fragmentation average = 55.674
	minimum = 0
	maximum = 642
Injected packet rate average = 0.0306641
	minimum = 0.0055 (at node 59)
	maximum = 0.056 (at node 24)
Accepted packet rate average = 0.0132083
	minimum = 0.0065 (at node 126)
	maximum = 0.0185 (at node 73)
Injected flit rate average = 0.551911
	minimum = 0.095 (at node 59)
	maximum = 1 (at node 24)
Accepted flit rate average= 0.23612
	minimum = 0.1135 (at node 126)
	maximum = 0.3415 (at node 73)
Injected packet length average = 17.9986
Accepted packet length average = 17.8766
Total in-flight flits = 302396 (188153 measured)
latency change    = 0.273038
throughput change = 0.00412485
Class 0:
Packet latency average = 812.754
	minimum = 25
	maximum = 3572
Network latency average = 569.673
	minimum = 23
	maximum = 2952
Slowest packet = 17305
Flit latency average = 1961.76
	minimum = 6
	maximum = 5566
Slowest flit = 36400
Fragmentation average = 77.8605
	minimum = 0
	maximum = 1181
Injected packet rate average = 0.0303785
	minimum = 0.008 (at node 39)
	maximum = 0.0556667 (at node 24)
Accepted packet rate average = 0.013059
	minimum = 0.00766667 (at node 36)
	maximum = 0.0176667 (at node 6)
Injected flit rate average = 0.546731
	minimum = 0.144 (at node 39)
	maximum = 1 (at node 24)
Accepted flit rate average= 0.234061
	minimum = 0.135 (at node 36)
	maximum = 0.334 (at node 73)
Injected packet length average = 17.9973
Accepted packet length average = 17.9233
Total in-flight flits = 361261 (273217 measured)
latency change    = 0.329159
throughput change = 0.00879698
Class 0:
Packet latency average = 1151.23
	minimum = 25
	maximum = 4643
Network latency average = 921.082
	minimum = 23
	maximum = 3962
Slowest packet = 17305
Flit latency average = 2199.78
	minimum = 6
	maximum = 6803
Slowest flit = 14110
Fragmentation average = 95.5381
	minimum = 0
	maximum = 1330
Injected packet rate average = 0.0302917
	minimum = 0.007 (at node 189)
	maximum = 0.0535 (at node 24)
Accepted packet rate average = 0.0130078
	minimum = 0.0085 (at node 36)
	maximum = 0.017 (at node 13)
Injected flit rate average = 0.545156
	minimum = 0.1295 (at node 189)
	maximum = 0.962 (at node 24)
Accepted flit rate average= 0.233607
	minimum = 0.151 (at node 36)
	maximum = 0.31675 (at node 73)
Injected packet length average = 17.9969
Accepted packet length average = 17.959
Total in-flight flits = 420458 (353976 measured)
latency change    = 0.294014
throughput change = 0.00194341
Class 0:
Packet latency average = 1581.08
	minimum = 25
	maximum = 5750
Network latency average = 1357.09
	minimum = 23
	maximum = 4899
Slowest packet = 17305
Flit latency average = 2434.74
	minimum = 6
	maximum = 7196
Slowest flit = 62801
Fragmentation average = 111.983
	minimum = 0
	maximum = 1637
Injected packet rate average = 0.0303833
	minimum = 0.0088 (at node 22)
	maximum = 0.0522 (at node 179)
Accepted packet rate average = 0.0129823
	minimum = 0.0094 (at node 31)
	maximum = 0.0168 (at node 73)
Injected flit rate average = 0.546739
	minimum = 0.1584 (at node 22)
	maximum = 0.9364 (at node 179)
Accepted flit rate average= 0.2328
	minimum = 0.1662 (at node 31)
	maximum = 0.3036 (at node 73)
Injected packet length average = 17.9947
Accepted packet length average = 17.9321
Total in-flight flits = 482652 (432041 measured)
latency change    = 0.271868
throughput change = 0.00346551
Class 0:
Packet latency average = 2000.43
	minimum = 25
	maximum = 6959
Network latency average = 1774.07
	minimum = 23
	maximum = 5976
Slowest packet = 17305
Flit latency average = 2674.61
	minimum = 6
	maximum = 8323
Slowest flit = 58002
Fragmentation average = 124.137
	minimum = 0
	maximum = 1927
Injected packet rate average = 0.0306406
	minimum = 0.0101667 (at node 22)
	maximum = 0.052 (at node 134)
Accepted packet rate average = 0.0129262
	minimum = 0.00883333 (at node 31)
	maximum = 0.016 (at node 103)
Injected flit rate average = 0.551582
	minimum = 0.183 (at node 22)
	maximum = 0.936 (at node 179)
Accepted flit rate average= 0.231752
	minimum = 0.155333 (at node 31)
	maximum = 0.292833 (at node 188)
Injected packet length average = 18.0016
Accepted packet length average = 17.9288
Total in-flight flits = 549502 (511369 measured)
latency change    = 0.209632
throughput change = 0.00452322
Class 0:
Packet latency average = 2411.6
	minimum = 25
	maximum = 7865
Network latency average = 2181.42
	minimum = 23
	maximum = 6936
Slowest packet = 17305
Flit latency average = 2926
	minimum = 6
	maximum = 9094
Slowest flit = 83332
Fragmentation average = 131.915
	minimum = 0
	maximum = 2124
Injected packet rate average = 0.0306243
	minimum = 0.00985714 (at node 22)
	maximum = 0.0507143 (at node 47)
Accepted packet rate average = 0.0128363
	minimum = 0.00942857 (at node 31)
	maximum = 0.016 (at node 95)
Injected flit rate average = 0.551158
	minimum = 0.176143 (at node 22)
	maximum = 0.912857 (at node 47)
Accepted flit rate average= 0.230459
	minimum = 0.171857 (at node 31)
	maximum = 0.293857 (at node 95)
Injected packet length average = 17.9974
Accepted packet length average = 17.9537
Total in-flight flits = 612241 (583926 measured)
latency change    = 0.170495
throughput change = 0.00560906
Draining all recorded packets ...
Class 0:
Remaining flits: 94212 94213 94214 94215 94216 94217 94218 94219 94220 94221 [...] (677995 flits)
Measured flits: 311094 311095 311096 311097 311098 311099 311100 311101 311102 311103 [...] (586126 flits)
Class 0:
Remaining flits: 94212 94213 94214 94215 94216 94217 94218 94219 94220 94221 [...] (739179 flits)
Measured flits: 311094 311095 311096 311097 311098 311099 311100 311101 311102 311103 [...] (559219 flits)
Class 0:
Remaining flits: 100908 100909 100910 100911 100912 100913 100914 100915 100916 100917 [...] (797452 flits)
Measured flits: 311094 311095 311096 311097 311098 311099 311100 311101 311102 311103 [...] (529796 flits)
Class 0:
Remaining flits: 117792 117793 117794 117795 117796 117797 117798 117799 117800 117801 [...] (853893 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (499465 flits)
Class 0:
Remaining flits: 117792 117793 117794 117795 117796 117797 117798 117799 117800 117801 [...] (907229 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (469111 flits)
Class 0:
Remaining flits: 117792 117793 117794 117795 117796 117797 117798 117799 117800 117801 [...] (970399 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (438681 flits)
Class 0:
Remaining flits: 117792 117793 117794 117795 117796 117797 117798 117799 117800 117801 [...] (1022564 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (408005 flits)
Class 0:
Remaining flits: 120171 120172 120173 120174 120175 120176 120177 120178 120179 120180 [...] (1074501 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (377473 flits)
Class 0:
Remaining flits: 148407 148408 148409 175338 175339 175340 175341 175342 175343 175344 [...] (1122954 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (347586 flits)
Class 0:
Remaining flits: 175338 175339 175340 175341 175342 175343 175344 175345 175346 175347 [...] (1178017 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (318529 flits)
Class 0:
Remaining flits: 175338 175339 175340 175341 175342 175343 175344 175345 175346 175347 [...] (1233027 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (290790 flits)
Class 0:
Remaining flits: 225324 225325 225326 225327 225328 225329 225330 225331 225332 225333 [...] (1285897 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (263824 flits)
Class 0:
Remaining flits: 225324 225325 225326 225327 225328 225329 225330 225331 225332 225333 [...] (1338358 flits)
Measured flits: 311185 311186 311187 311188 311189 311190 311191 311192 311193 311194 [...] (238653 flits)
Class 0:
Remaining flits: 262925 297635 297636 297637 297638 297639 297640 297641 297642 297643 [...] (1389404 flits)
Measured flits: 314585 319590 319591 319592 319593 319594 319595 319596 319597 319598 [...] (214598 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1447093 flits)
Measured flits: 319590 319591 319592 319593 319594 319595 319596 319597 319598 319599 [...] (191711 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1504552 flits)
Measured flits: 319590 319591 319592 319593 319594 319595 319596 319597 319598 319599 [...] (171219 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1560223 flits)
Measured flits: 319590 319591 319592 319593 319594 319595 319596 319597 319598 319599 [...] (152483 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1614155 flits)
Measured flits: 319590 319591 319592 319593 319594 319595 319596 319597 319598 319599 [...] (135658 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1660373 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (119513 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1714103 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (105293 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1765054 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (93291 flits)
Class 0:
Remaining flits: 299628 299629 299630 299631 299632 299633 299634 299635 299636 299637 [...] (1820167 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (82002 flits)
Class 0:
Remaining flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (1868349 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (71471 flits)
Class 0:
Remaining flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (1916390 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (62069 flits)
Class 0:
Remaining flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (1959968 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (54124 flits)
Class 0:
Remaining flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (2005718 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (46982 flits)
Class 0:
Remaining flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (2049296 flits)
Measured flits: 337932 337933 337934 337935 337936 337937 337938 337939 337940 337941 [...] (40711 flits)
Class 0:
Remaining flits: 395694 395695 395696 395697 395698 395699 395700 395701 395702 395703 [...] (2089806 flits)
Measured flits: 395694 395695 395696 395697 395698 395699 395700 395701 395702 395703 [...] (35118 flits)
Class 0:
Remaining flits: 395694 395695 395696 395697 395698 395699 395700 395701 395702 395703 [...] (2123810 flits)
Measured flits: 395694 395695 395696 395697 395698 395699 395700 395701 395702 395703 [...] (30545 flits)
Class 0:
Remaining flits: 454955 454956 454957 454958 454959 454960 454961 454962 454963 454964 [...] (2161473 flits)
Measured flits: 454955 454956 454957 454958 454959 454960 454961 454962 454963 454964 [...] (26727 flits)
Class 0:
Remaining flits: 478890 478891 478892 478893 478894 478895 478896 478897 478898 478899 [...] (2196789 flits)
Measured flits: 478890 478891 478892 478893 478894 478895 478896 478897 478898 478899 [...] (23043 flits)
Class 0:
Remaining flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (2229004 flits)
Measured flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (19824 flits)
Class 0:
Remaining flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (2261811 flits)
Measured flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (17031 flits)
Class 0:
Remaining flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (2292047 flits)
Measured flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (14702 flits)
Class 0:
Remaining flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (2315641 flits)
Measured flits: 491094 491095 491096 491097 491098 491099 491100 491101 491102 491103 [...] (12547 flits)
Class 0:
Remaining flits: 491110 491111 527238 527239 527240 527241 527242 527243 527244 527245 [...] (2343698 flits)
Measured flits: 491110 491111 527238 527239 527240 527241 527242 527243 527244 527245 [...] (10517 flits)
Class 0:
Remaining flits: 527238 527239 527240 527241 527242 527243 527244 527245 527246 527247 [...] (2363488 flits)
Measured flits: 527238 527239 527240 527241 527242 527243 527244 527245 527246 527247 [...] (9022 flits)
Class 0:
Remaining flits: 527241 527242 527243 527244 527245 527246 527247 527248 527249 527250 [...] (2381859 flits)
Measured flits: 527241 527242 527243 527244 527245 527246 527247 527248 527249 527250 [...] (7655 flits)
Class 0:
Remaining flits: 584910 584911 584912 584913 584914 584915 584916 584917 584918 584919 [...] (2399854 flits)
Measured flits: 584910 584911 584912 584913 584914 584915 584916 584917 584918 584919 [...] (6520 flits)
Class 0:
Remaining flits: 584910 584911 584912 584913 584914 584915 584916 584917 584918 584919 [...] (2414758 flits)
Measured flits: 584910 584911 584912 584913 584914 584915 584916 584917 584918 584919 [...] (5435 flits)
Class 0:
Remaining flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (2426205 flits)
Measured flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (4022 flits)
Class 0:
Remaining flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (2439535 flits)
Measured flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (3292 flits)
Class 0:
Remaining flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (2455240 flits)
Measured flits: 590922 590923 590924 590925 590926 590927 590928 590929 590930 590931 [...] (2877 flits)
Class 0:
Remaining flits: 668790 668791 668792 668793 668794 668795 668796 668797 668798 668799 [...] (2465262 flits)
Measured flits: 668790 668791 668792 668793 668794 668795 668796 668797 668798 668799 [...] (2490 flits)
Class 0:
Remaining flits: 668802 668803 668804 668805 668806 668807 760122 760123 760124 760125 [...] (2476213 flits)
Measured flits: 668802 668803 668804 668805 668806 668807 760122 760123 760124 760125 [...] (2078 flits)
Class 0:
Remaining flits: 760122 760123 760124 760125 760126 760127 760128 760129 760130 760131 [...] (2482275 flits)
Measured flits: 760122 760123 760124 760125 760126 760127 760128 760129 760130 760131 [...] (1728 flits)
Class 0:
Remaining flits: 760122 760123 760124 760125 760126 760127 760128 760129 760130 760131 [...] (2486783 flits)
Measured flits: 760122 760123 760124 760125 760126 760127 760128 760129 760130 760131 [...] (1494 flits)
Class 0:
Remaining flits: 760135 760136 760137 760138 760139 780480 780481 780482 780483 780484 [...] (2494041 flits)
Measured flits: 760135 760136 760137 760138 760139 780480 780481 780482 780483 780484 [...] (1220 flits)
Class 0:
Remaining flits: 780485 780486 780487 780488 780489 780490 780491 780492 780493 780494 [...] (2494197 flits)
Measured flits: 780485 780486 780487 780488 780489 780490 780491 780492 780493 780494 [...] (868 flits)
Class 0:
Remaining flits: 848307 848308 848309 848310 848311 848312 848313 848314 848315 848316 [...] (2490310 flits)
Measured flits: 848307 848308 848309 848310 848311 848312 848313 848314 848315 848316 [...] (609 flits)
Class 0:
Remaining flits: 848320 848321 857410 857411 861714 861715 861716 861717 861718 861719 [...] (2488966 flits)
Measured flits: 848320 848321 857410 857411 861714 861715 861716 861717 861718 861719 [...] (490 flits)
Class 0:
Remaining flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (2487234 flits)
Measured flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (395 flits)
Class 0:
Remaining flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (2480912 flits)
Measured flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (261 flits)
Class 0:
Remaining flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (2474230 flits)
Measured flits: 863550 863551 863552 863553 863554 863555 863556 863557 863558 863559 [...] (183 flits)
Class 0:
Remaining flits: 863553 863554 863555 863556 863557 863558 863559 863560 863561 863562 [...] (2471173 flits)
Measured flits: 863553 863554 863555 863556 863557 863558 863559 863560 863561 863562 [...] (159 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2465868 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (144 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2458473 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (90 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2451316 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (90 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2448050 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (90 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2443548 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (54 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2436954 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (48 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2428206 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (36 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2426598 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (20 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2417668 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (18 flits)
Class 0:
Remaining flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (2411434 flits)
Measured flits: 910980 910981 910982 910983 910984 910985 910986 910987 910988 910989 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1115802 1115803 1115804 1115805 1115806 1115807 1115808 1115809 1115810 1115811 [...] (2371962 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1115802 1115803 1115804 1115805 1115806 1115807 1115808 1115809 1115810 1115811 [...] (2336645 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1135062 1135063 1135064 1135065 1135066 1135067 1135068 1135069 1135070 1135071 [...] (2300887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2265148 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2230204 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2195171 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2160145 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2124901 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2089726 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1233990 1233991 1233992 1233993 1233994 1233995 1233996 1233997 1233998 1233999 [...] (2053475 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1330038 1330039 1330040 1330041 1330042 1330043 1330044 1330045 1330046 1330047 [...] (2017448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1330038 1330039 1330040 1330041 1330042 1330043 1330044 1330045 1330046 1330047 [...] (1981972 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1460700 1460701 1460702 1460703 1460704 1460705 1460706 1460707 1460708 1460709 [...] (1946374 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487106 1487107 1487108 1487109 1487110 1487111 1487112 1487113 1487114 1487115 [...] (1911069 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487106 1487107 1487108 1487109 1487110 1487111 1487112 1487113 1487114 1487115 [...] (1875912 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487106 1487107 1487108 1487109 1487110 1487111 1487112 1487113 1487114 1487115 [...] (1840900 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487106 1487107 1487108 1487109 1487110 1487111 1487112 1487113 1487114 1487115 [...] (1805975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1551994 1551995 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 [...] (1770521 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (1735570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (1700923 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (1665415 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (1629907 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (1594973 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1559920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1524850 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1489794 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1454335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1418996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1643130 1643131 1643132 1643133 1643134 1643135 1643136 1643137 1643138 1643139 [...] (1384036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866438 1866439 1866440 1866441 1866442 1866443 1866444 1866445 1866446 1866447 [...] (1349690 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866438 1866439 1866440 1866441 1866442 1866443 1866444 1866445 1866446 1866447 [...] (1315268 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866438 1866439 1866440 1866441 1866442 1866443 1866444 1866445 1866446 1866447 [...] (1280922 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866438 1866439 1866440 1866441 1866442 1866443 1866444 1866445 1866446 1866447 [...] (1246878 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866446 1866447 1866448 1866449 1866450 1866451 1866452 1866453 1866454 1866455 [...] (1212944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1902474 1902475 1902476 1902477 1902478 1902479 1902480 1902481 1902482 1902483 [...] (1178951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1902474 1902475 1902476 1902477 1902478 1902479 1902480 1902481 1902482 1902483 [...] (1145230 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1902474 1902475 1902476 1902477 1902478 1902479 1902480 1902481 1902482 1902483 [...] (1111462 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1935792 1935793 1935794 1935795 1935796 1935797 1935798 1935799 1935800 1935801 [...] (1078183 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1935792 1935793 1935794 1935795 1935796 1935797 1935798 1935799 1935800 1935801 [...] (1043904 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1935792 1935793 1935794 1935795 1935796 1935797 1935798 1935799 1935800 1935801 [...] (1010596 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1935792 1935793 1935794 1935795 1935796 1935797 1935798 1935799 1935800 1935801 [...] (977198 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2185974 2185975 2185976 2185977 2185978 2185979 2185980 2185981 2185982 2185983 [...] (943457 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2259954 2259955 2259956 2259957 2259958 2259959 2259960 2259961 2259962 2259963 [...] (910417 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2259954 2259955 2259956 2259957 2259958 2259959 2259960 2259961 2259962 2259963 [...] (876500 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2259954 2259955 2259956 2259957 2259958 2259959 2259960 2259961 2259962 2259963 [...] (842404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2268234 2268235 2268236 2268237 2268238 2268239 2268240 2268241 2268242 2268243 [...] (808702 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2353392 2353393 2353394 2353395 2353396 2353397 2353398 2353399 2353400 2353401 [...] (775001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2427966 2427967 2427968 2427969 2427970 2427971 2427972 2427973 2427974 2427975 [...] (741763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2451618 2451619 2451620 2451621 2451622 2451623 2451624 2451625 2451626 2451627 [...] (708021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2451618 2451619 2451620 2451621 2451622 2451623 2451624 2451625 2451626 2451627 [...] (673157 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2451618 2451619 2451620 2451621 2451622 2451623 2451624 2451625 2451626 2451627 [...] (639041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2451618 2451619 2451620 2451621 2451622 2451623 2451624 2451625 2451626 2451627 [...] (604775 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2480670 2480671 2480672 2480673 2480674 2480675 2480676 2480677 2480678 2480679 [...] (570876 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2480670 2480671 2480672 2480673 2480674 2480675 2480676 2480677 2480678 2480679 [...] (536334 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2480670 2480671 2480672 2480673 2480674 2480675 2480676 2480677 2480678 2480679 [...] (501498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2480672 2480673 2480674 2480675 2480676 2480677 2480678 2480679 2480680 2480681 [...] (467609 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575296 2575297 2575298 2575299 2575300 2575301 2575302 2575303 2575304 2575305 [...] (433159 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575296 2575297 2575298 2575299 2575300 2575301 2575302 2575303 2575304 2575305 [...] (398220 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575296 2575297 2575298 2575299 2575300 2575301 2575302 2575303 2575304 2575305 [...] (363913 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575296 2575297 2575298 2575299 2575300 2575301 2575302 2575303 2575304 2575305 [...] (329666 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575309 2575310 2575311 2575312 2575313 2664144 2664145 2664146 2664147 2664148 [...] (295774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2664144 2664145 2664146 2664147 2664148 2664149 2664150 2664151 2664152 2664153 [...] (262817 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2664144 2664145 2664146 2664147 2664148 2664149 2664150 2664151 2664152 2664153 [...] (230723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2746152 2746153 2746154 2746155 2746156 2746157 2746158 2746159 2746160 2746161 [...] (199161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (168498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (138237 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (109086 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (82760 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (59899 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2849328 2849329 2849330 2849331 2849332 2849333 2849334 2849335 2849336 2849337 [...] (40750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3178872 3178873 3178874 3178875 3178876 3178877 3178878 3178879 3178880 3178881 [...] (25884 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3337974 3337975 3337976 3337977 3337978 3337979 3337980 3337981 3337982 3337983 [...] (15005 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3397428 3397429 3397430 3397431 3397432 3397433 3397434 3397435 3397436 3397437 [...] (7323 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3397428 3397429 3397430 3397431 3397432 3397433 3397434 3397435 3397436 3397437 [...] (2527 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3417048 3417049 3417050 3417051 3417052 3417053 3417054 3417055 3417056 3417057 [...] (631 flits)
Measured flits: (0 flits)
Time taken is 151181 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12710.3 (1 samples)
	minimum = 25 (1 samples)
	maximum = 67406 (1 samples)
Network latency average = 12408.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 66855 (1 samples)
Flit latency average = 41497.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 116853 (1 samples)
Fragmentation average = 196.853 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4364 (1 samples)
Injected packet rate average = 0.0306243 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0507143 (1 samples)
Accepted packet rate average = 0.0128363 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.551158 (1 samples)
	minimum = 0.176143 (1 samples)
	maximum = 0.912857 (1 samples)
Accepted flit rate average = 0.230459 (1 samples)
	minimum = 0.171857 (1 samples)
	maximum = 0.293857 (1 samples)
Injected packet size average = 17.9974 (1 samples)
Accepted packet size average = 17.9537 (1 samples)
Hops average = 5.07393 (1 samples)
Total run time 194.6
