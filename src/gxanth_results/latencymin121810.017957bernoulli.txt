BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 238.558
	minimum = 23
	maximum = 755
Network latency average = 220.588
	minimum = 23
	maximum = 750
Slowest packet = 432
Flit latency average = 180.201
	minimum = 6
	maximum = 803
Slowest flit = 11160
Fragmentation average = 51.6438
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0137083
	minimum = 0.005 (at node 36)
	maximum = 0.024 (at node 115)
Accepted packet rate average = 0.00910938
	minimum = 0.001 (at node 174)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.242984
	minimum = 0.085 (at node 36)
	maximum = 0.425 (at node 115)
Accepted flit rate average= 0.170094
	minimum = 0.018 (at node 174)
	maximum = 0.288 (at node 44)
Injected packet length average = 17.7253
Accepted packet length average = 18.6724
Total in-flight flits = 16230 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 456.54
	minimum = 23
	maximum = 1649
Network latency average = 345.841
	minimum = 23
	maximum = 1596
Slowest packet = 1101
Flit latency average = 297.098
	minimum = 6
	maximum = 1509
Slowest flit = 19832
Fragmentation average = 54.7827
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0115417
	minimum = 0.005 (at node 40)
	maximum = 0.018 (at node 23)
Accepted packet rate average = 0.00911979
	minimum = 0.005 (at node 10)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.205956
	minimum = 0.09 (at node 40)
	maximum = 0.324 (at node 23)
Accepted flit rate average= 0.167323
	minimum = 0.0915 (at node 116)
	maximum = 0.27 (at node 22)
Injected packet length average = 17.8445
Accepted packet length average = 18.3472
Total in-flight flits = 17450 (0 measured)
latency change    = 0.477465
throughput change = 0.0165598
Class 0:
Packet latency average = 1138.78
	minimum = 23
	maximum = 2620
Network latency average = 534.512
	minimum = 23
	maximum = 1926
Slowest packet = 1258
Flit latency average = 471.791
	minimum = 6
	maximum = 1920
Slowest flit = 47214
Fragmentation average = 58.6011
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00905208
	minimum = 0 (at node 48)
	maximum = 0.02 (at node 54)
Accepted packet rate average = 0.00886458
	minimum = 0.002 (at node 153)
	maximum = 0.019 (at node 113)
Injected flit rate average = 0.161755
	minimum = 0 (at node 48)
	maximum = 0.35 (at node 54)
Accepted flit rate average= 0.159828
	minimum = 0.04 (at node 153)
	maximum = 0.338 (at node 113)
Injected packet length average = 17.8694
Accepted packet length average = 18.03
Total in-flight flits = 17777 (0 measured)
latency change    = 0.599098
throughput change = 0.0468928
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1621.21
	minimum = 30
	maximum = 3224
Network latency average = 325.012
	minimum = 23
	maximum = 932
Slowest packet = 6310
Flit latency average = 482.075
	minimum = 6
	maximum = 2587
Slowest flit = 59183
Fragmentation average = 54.1875
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0089375
	minimum = 0 (at node 22)
	maximum = 0.017 (at node 153)
Accepted packet rate average = 0.00911979
	minimum = 0.003 (at node 132)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.162177
	minimum = 0 (at node 169)
	maximum = 0.316 (at node 189)
Accepted flit rate average= 0.163661
	minimum = 0.044 (at node 163)
	maximum = 0.32 (at node 56)
Injected packet length average = 18.1457
Accepted packet length average = 17.9457
Total in-flight flits = 17512 (16306 measured)
latency change    = 0.297571
throughput change = 0.0234223
Class 0:
Packet latency average = 1963.75
	minimum = 30
	maximum = 3898
Network latency average = 456.142
	minimum = 23
	maximum = 1780
Slowest packet = 6310
Flit latency average = 488.08
	minimum = 6
	maximum = 2587
Slowest flit = 59183
Fragmentation average = 56.7561
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00895052
	minimum = 0.0025 (at node 77)
	maximum = 0.016 (at node 78)
Accepted packet rate average = 0.00901302
	minimum = 0.0035 (at node 163)
	maximum = 0.0155 (at node 56)
Injected flit rate average = 0.161721
	minimum = 0.045 (at node 77)
	maximum = 0.284 (at node 78)
Accepted flit rate average= 0.162214
	minimum = 0.058 (at node 163)
	maximum = 0.283 (at node 56)
Injected packet length average = 18.0684
Accepted packet length average = 17.9977
Total in-flight flits = 17605 (17560 measured)
latency change    = 0.174433
throughput change = 0.00892599
Class 0:
Packet latency average = 2241.94
	minimum = 30
	maximum = 4325
Network latency average = 489.964
	minimum = 23
	maximum = 2483
Slowest packet = 6310
Flit latency average = 483.435
	minimum = 6
	maximum = 2587
Slowest flit = 59183
Fragmentation average = 57.3985
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00892708
	minimum = 0.00333333 (at node 62)
	maximum = 0.015 (at node 123)
Accepted packet rate average = 0.00896875
	minimum = 0.005 (at node 45)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.16126
	minimum = 0.06 (at node 62)
	maximum = 0.269333 (at node 123)
Accepted flit rate average= 0.161153
	minimum = 0.09 (at node 45)
	maximum = 0.276667 (at node 16)
Injected packet length average = 18.0642
Accepted packet length average = 17.9683
Total in-flight flits = 18067 (18067 measured)
latency change    = 0.124084
throughput change = 0.00658235
Class 0:
Packet latency average = 2508.75
	minimum = 30
	maximum = 4902
Network latency average = 509.354
	minimum = 23
	maximum = 2715
Slowest packet = 6310
Flit latency average = 485.245
	minimum = 6
	maximum = 2683
Slowest flit = 144107
Fragmentation average = 57.4356
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00894401
	minimum = 0.00425 (at node 62)
	maximum = 0.016 (at node 123)
Accepted packet rate average = 0.00898177
	minimum = 0.00475 (at node 4)
	maximum = 0.01325 (at node 16)
Injected flit rate average = 0.161253
	minimum = 0.0765 (at node 62)
	maximum = 0.29125 (at node 123)
Accepted flit rate average= 0.161604
	minimum = 0.08625 (at node 4)
	maximum = 0.241 (at node 16)
Injected packet length average = 18.0291
Accepted packet length average = 17.9925
Total in-flight flits = 17595 (17595 measured)
latency change    = 0.106353
throughput change = 0.00279318
Class 0:
Packet latency average = 2762.92
	minimum = 30
	maximum = 5660
Network latency average = 518.899
	minimum = 23
	maximum = 2715
Slowest packet = 6310
Flit latency average = 485.957
	minimum = 6
	maximum = 2683
Slowest flit = 144107
Fragmentation average = 57.728
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00897187
	minimum = 0.004 (at node 62)
	maximum = 0.015 (at node 140)
Accepted packet rate average = 0.00899479
	minimum = 0.006 (at node 163)
	maximum = 0.0128 (at node 16)
Injected flit rate average = 0.161604
	minimum = 0.072 (at node 62)
	maximum = 0.27 (at node 140)
Accepted flit rate average= 0.161796
	minimum = 0.1068 (at node 163)
	maximum = 0.2316 (at node 16)
Injected packet length average = 18.0123
Accepted packet length average = 17.9877
Total in-flight flits = 17685 (17685 measured)
latency change    = 0.0919919
throughput change = 0.00118462
Class 0:
Packet latency average = 3027.19
	minimum = 30
	maximum = 6151
Network latency average = 523.854
	minimum = 23
	maximum = 2715
Slowest packet = 6310
Flit latency average = 485.891
	minimum = 6
	maximum = 2683
Slowest flit = 144107
Fragmentation average = 57.7611
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00898698
	minimum = 0.00433333 (at node 16)
	maximum = 0.0141667 (at node 140)
Accepted packet rate average = 0.00900781
	minimum = 0.0065 (at node 36)
	maximum = 0.0128333 (at node 128)
Injected flit rate average = 0.161979
	minimum = 0.078 (at node 16)
	maximum = 0.255 (at node 140)
Accepted flit rate average= 0.162098
	minimum = 0.114667 (at node 36)
	maximum = 0.231167 (at node 128)
Injected packet length average = 18.0238
Accepted packet length average = 17.9953
Total in-flight flits = 17736 (17736 measured)
latency change    = 0.087299
throughput change = 0.00186465
Class 0:
Packet latency average = 3277.82
	minimum = 30
	maximum = 7058
Network latency average = 524.008
	minimum = 23
	maximum = 2715
Slowest packet = 6310
Flit latency average = 482.406
	minimum = 6
	maximum = 2683
Slowest flit = 144107
Fragmentation average = 57.4621
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00894792
	minimum = 0.00485714 (at node 82)
	maximum = 0.0137143 (at node 123)
Accepted packet rate average = 0.0089814
	minimum = 0.006 (at node 163)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.161196
	minimum = 0.0861429 (at node 82)
	maximum = 0.248714 (at node 123)
Accepted flit rate average= 0.161609
	minimum = 0.106571 (at node 163)
	maximum = 0.219 (at node 128)
Injected packet length average = 18.0149
Accepted packet length average = 17.9937
Total in-flight flits = 17421 (17421 measured)
latency change    = 0.0764629
throughput change = 0.00302867
Draining all recorded packets ...
Class 0:
Remaining flits: 273060 273061 273062 273063 273064 273065 273066 273067 273068 273069 [...] (17833 flits)
Measured flits: 273060 273061 273062 273063 273064 273065 273066 273067 273068 273069 [...] (17833 flits)
Class 0:
Remaining flits: 312216 312217 312218 312219 312220 312221 312222 312223 312224 312225 [...] (17917 flits)
Measured flits: 312216 312217 312218 312219 312220 312221 312222 312223 312224 312225 [...] (17917 flits)
Class 0:
Remaining flits: 351630 351631 351632 351633 351634 351635 351636 351637 351638 351639 [...] (17762 flits)
Measured flits: 351630 351631 351632 351633 351634 351635 351636 351637 351638 351639 [...] (17726 flits)
Class 0:
Remaining flits: 354924 354925 354926 354927 354928 354929 354930 354931 354932 354933 [...] (17798 flits)
Measured flits: 354924 354925 354926 354927 354928 354929 354930 354931 354932 354933 [...] (17733 flits)
Class 0:
Remaining flits: 416790 416791 416792 416793 416794 416795 416796 416797 416798 416799 [...] (17873 flits)
Measured flits: 416790 416791 416792 416793 416794 416795 416796 416797 416798 416799 [...] (17420 flits)
Class 0:
Remaining flits: 440711 445500 445501 445502 445503 445504 445505 445506 445507 445508 [...] (17853 flits)
Measured flits: 440711 445500 445501 445502 445503 445504 445505 445506 445507 445508 [...] (16694 flits)
Class 0:
Remaining flits: 474930 474931 474932 474933 474934 474935 474936 474937 474938 474939 [...] (17398 flits)
Measured flits: 474930 474931 474932 474933 474934 474935 474936 474937 474938 474939 [...] (14994 flits)
Class 0:
Remaining flits: 514815 514816 514817 514944 514945 514946 514947 514948 514949 514950 [...] (17655 flits)
Measured flits: 514815 514816 514817 514944 514945 514946 514947 514948 514949 514950 [...] (13145 flits)
Class 0:
Remaining flits: 541890 541891 541892 541893 541894 541895 541896 541897 541898 541899 [...] (17758 flits)
Measured flits: 541890 541891 541892 541893 541894 541895 541896 541897 541898 541899 [...] (10572 flits)
Class 0:
Remaining flits: 575457 575458 575459 577692 577693 577694 577695 577696 577697 577698 [...] (17799 flits)
Measured flits: 575457 575458 575459 580905 580906 580907 580908 580909 580910 580911 [...] (7755 flits)
Class 0:
Remaining flits: 596736 596737 596738 596739 596740 596741 596742 596743 596744 596745 [...] (17839 flits)
Measured flits: 624654 624655 624656 624657 624658 624659 624660 624661 624662 624663 [...] (4694 flits)
Class 0:
Remaining flits: 625307 625308 625309 625310 625311 625312 625313 625314 625315 625316 [...] (17526 flits)
Measured flits: 625307 625308 625309 625310 625311 625312 625313 625314 625315 625316 [...] (2809 flits)
Class 0:
Remaining flits: 653004 653005 653006 653007 653008 653009 653010 653011 653012 653013 [...] (17789 flits)
Measured flits: 696546 696547 696548 696549 696550 696551 696552 696553 696554 696555 [...] (1508 flits)
Class 0:
Remaining flits: 661104 661105 661106 661107 661108 661109 661110 661111 661112 661113 [...] (18082 flits)
Measured flits: 699190 699191 716730 716731 716732 716733 716734 716735 716736 716737 [...] (1235 flits)
Class 0:
Remaining flits: 724662 724663 724664 724665 724666 724667 724668 724669 724670 724671 [...] (17891 flits)
Measured flits: 746028 746029 746030 746031 746032 746033 746034 746035 746036 746037 [...] (719 flits)
Class 0:
Remaining flits: 760208 760209 760210 760211 760929 760930 760931 763398 763399 763400 [...] (17840 flits)
Measured flits: 803136 803137 803138 803139 803140 803141 805914 805915 805916 805917 [...] (240 flits)
Draining remaining packets ...
Time taken is 27529 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6099.62 (1 samples)
	minimum = 30 (1 samples)
	maximum = 16813 (1 samples)
Network latency average = 545.06 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3567 (1 samples)
Flit latency average = 490.123 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3639 (1 samples)
Fragmentation average = 58.5226 (1 samples)
	minimum = 0 (1 samples)
	maximum = 147 (1 samples)
Injected packet rate average = 0.00894792 (1 samples)
	minimum = 0.00485714 (1 samples)
	maximum = 0.0137143 (1 samples)
Accepted packet rate average = 0.0089814 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.161196 (1 samples)
	minimum = 0.0861429 (1 samples)
	maximum = 0.248714 (1 samples)
Accepted flit rate average = 0.161609 (1 samples)
	minimum = 0.106571 (1 samples)
	maximum = 0.219 (1 samples)
Injected packet size average = 18.0149 (1 samples)
Accepted packet size average = 17.9937 (1 samples)
Hops average = 5.06116 (1 samples)
Total run time 18.5346
