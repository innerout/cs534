BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 329.365
	minimum = 22
	maximum = 903
Network latency average = 302.749
	minimum = 22
	maximum = 851
Slowest packet = 236
Flit latency average = 268.975
	minimum = 5
	maximum = 868
Slowest flit = 18221
Fragmentation average = 86.2543
	minimum = 0
	maximum = 392
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0151354
	minimum = 0.008 (at node 25)
	maximum = 0.026 (at node 22)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.296146
	minimum = 0.16 (at node 30)
	maximum = 0.491 (at node 44)
Injected packet length average = 17.8388
Accepted packet length average = 19.5664
Total in-flight flits = 112124 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 620.211
	minimum = 22
	maximum = 1765
Network latency average = 578.201
	minimum = 22
	maximum = 1701
Slowest packet = 1493
Flit latency average = 536.676
	minimum = 5
	maximum = 1749
Slowest flit = 37629
Fragmentation average = 115.565
	minimum = 0
	maximum = 506
Injected packet rate average = 0.0490651
	minimum = 0.0375 (at node 128)
	maximum = 0.056 (at node 115)
Accepted packet rate average = 0.0162448
	minimum = 0.009 (at node 83)
	maximum = 0.0235 (at node 22)
Injected flit rate average = 0.879138
	minimum = 0.6745 (at node 128)
	maximum = 1 (at node 161)
Accepted flit rate average= 0.306555
	minimum = 0.1625 (at node 83)
	maximum = 0.4355 (at node 22)
Injected packet length average = 17.9178
Accepted packet length average = 18.871
Total in-flight flits = 221439 (0 measured)
latency change    = 0.468948
throughput change = 0.0339543
Class 0:
Packet latency average = 1577.66
	minimum = 24
	maximum = 2727
Network latency average = 1496.27
	minimum = 22
	maximum = 2637
Slowest packet = 2596
Flit latency average = 1462.03
	minimum = 5
	maximum = 2620
Slowest flit = 46745
Fragmentation average = 163.286
	minimum = 0
	maximum = 435
Injected packet rate average = 0.0459271
	minimum = 0.009 (at node 120)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0163073
	minimum = 0.005 (at node 174)
	maximum = 0.03 (at node 102)
Injected flit rate average = 0.827333
	minimum = 0.165 (at node 120)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.292953
	minimum = 0.099 (at node 40)
	maximum = 0.544 (at node 159)
Injected packet length average = 18.0141
Accepted packet length average = 17.9645
Total in-flight flits = 324060 (0 measured)
latency change    = 0.606879
throughput change = 0.0464291
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 551.846
	minimum = 27
	maximum = 2006
Network latency average = 81.4083
	minimum = 22
	maximum = 845
Slowest packet = 27735
Flit latency average = 2251.46
	minimum = 5
	maximum = 3307
Slowest flit = 111502
Fragmentation average = 5.01183
	minimum = 0
	maximum = 19
Injected packet rate average = 0.0346875
	minimum = 0.001 (at node 40)
	maximum = 0.056 (at node 21)
Accepted packet rate average = 0.0149635
	minimum = 0.007 (at node 88)
	maximum = 0.025 (at node 30)
Injected flit rate average = 0.62526
	minimum = 0.016 (at node 40)
	maximum = 1 (at node 21)
Accepted flit rate average= 0.270234
	minimum = 0.115 (at node 36)
	maximum = 0.414 (at node 121)
Injected packet length average = 18.0255
Accepted packet length average = 18.0595
Total in-flight flits = 392415 (117194 measured)
latency change    = 1.85888
throughput change = 0.0840705
Class 0:
Packet latency average = 961.255
	minimum = 27
	maximum = 2583
Network latency average = 346.899
	minimum = 22
	maximum = 1943
Slowest packet = 27735
Flit latency average = 2599.13
	minimum = 5
	maximum = 4043
Slowest flit = 136673
Fragmentation average = 5.70435
	minimum = 0
	maximum = 25
Injected packet rate average = 0.0257266
	minimum = 0.0055 (at node 25)
	maximum = 0.044 (at node 34)
Accepted packet rate average = 0.015026
	minimum = 0.009 (at node 45)
	maximum = 0.023 (at node 81)
Injected flit rate average = 0.464047
	minimum = 0.098 (at node 25)
	maximum = 0.792 (at node 34)
Accepted flit rate average= 0.269445
	minimum = 0.1475 (at node 36)
	maximum = 0.3885 (at node 129)
Injected packet length average = 18.0377
Accepted packet length average = 17.9319
Total in-flight flits = 398865 (172003 measured)
latency change    = 0.425911
throughput change = 0.00292847
Class 0:
Packet latency average = 1428.04
	minimum = 27
	maximum = 3489
Network latency average = 626.906
	minimum = 22
	maximum = 2883
Slowest packet = 27735
Flit latency average = 2928.12
	minimum = 5
	maximum = 4950
Slowest flit = 170657
Fragmentation average = 6.53417
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0226233
	minimum = 0.0103333 (at node 136)
	maximum = 0.034 (at node 138)
Accepted packet rate average = 0.0151823
	minimum = 0.009 (at node 36)
	maximum = 0.0196667 (at node 157)
Injected flit rate average = 0.4075
	minimum = 0.186 (at node 152)
	maximum = 0.615 (at node 138)
Accepted flit rate average= 0.269703
	minimum = 0.146333 (at node 36)
	maximum = 0.356333 (at node 157)
Injected packet length average = 18.0124
Accepted packet length average = 17.7643
Total in-flight flits = 403827 (225074 measured)
latency change    = 0.326872
throughput change = 0.000955912
Class 0:
Packet latency average = 2084.6
	minimum = 27
	maximum = 5133
Network latency average = 1136.78
	minimum = 22
	maximum = 3946
Slowest packet = 27735
Flit latency average = 3249.57
	minimum = 5
	maximum = 5780
Slowest flit = 188279
Fragmentation average = 11.0667
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0206159
	minimum = 0.00775 (at node 136)
	maximum = 0.03025 (at node 1)
Accepted packet rate average = 0.0151628
	minimum = 0.01075 (at node 36)
	maximum = 0.0205 (at node 157)
Injected flit rate average = 0.371552
	minimum = 0.13975 (at node 136)
	maximum = 0.54475 (at node 1)
Accepted flit rate average= 0.270181
	minimum = 0.18175 (at node 36)
	maximum = 0.35575 (at node 157)
Injected packet length average = 18.0226
Accepted packet length average = 17.8187
Total in-flight flits = 402419 (269998 measured)
latency change    = 0.314958
throughput change = 0.00176868
Draining remaining packets ...
Class 0:
Remaining flits: 185112 185113 185114 185115 185116 185117 185118 185119 185120 185121 [...] (352324 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (260753 flits)
Class 0:
Remaining flits: 224712 224713 224714 224715 224716 224717 224718 224719 224720 224721 [...] (302272 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (245644 flits)
Class 0:
Remaining flits: 247320 247321 247322 247323 247324 247325 247326 247327 247328 247329 [...] (252322 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (220807 flits)
Class 0:
Remaining flits: 268704 268705 268706 268707 268708 268709 268710 268711 268712 268713 [...] (203065 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (188272 flits)
Class 0:
Remaining flits: 337248 337249 337250 337251 337252 337253 337254 337255 337256 337257 [...] (155394 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (149761 flits)
Class 0:
Remaining flits: 385812 385813 385814 385815 385816 385817 385818 385819 385820 385821 [...] (108000 flits)
Measured flits: 498024 498025 498026 498027 498028 498029 498030 498031 498032 498033 [...] (106170 flits)
Class 0:
Remaining flits: 402768 402769 402770 402771 402772 402773 402774 402775 402776 402777 [...] (61026 flits)
Measured flits: 498204 498205 498206 498207 498208 498209 498210 498211 498212 498213 [...] (60491 flits)
Class 0:
Remaining flits: 434649 434650 434651 434652 434653 434654 434655 434656 434657 434658 [...] (15209 flits)
Measured flits: 506844 506845 506846 506847 506848 506849 506850 506851 506852 506853 [...] (15104 flits)
Class 0:
Remaining flits: 623034 623035 623036 623037 623038 623039 623040 623041 623042 623043 [...] (561 flits)
Measured flits: 623034 623035 623036 623037 623038 623039 623040 623041 623042 623043 [...] (561 flits)
Time taken is 16502 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8206.71 (1 samples)
	minimum = 27 (1 samples)
	maximum = 12884 (1 samples)
Network latency average = 7168.67 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12347 (1 samples)
Flit latency average = 5826.15 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13238 (1 samples)
Fragmentation average = 57.9203 (1 samples)
	minimum = 0 (1 samples)
	maximum = 791 (1 samples)
Injected packet rate average = 0.0206159 (1 samples)
	minimum = 0.00775 (1 samples)
	maximum = 0.03025 (1 samples)
Accepted packet rate average = 0.0151628 (1 samples)
	minimum = 0.01075 (1 samples)
	maximum = 0.0205 (1 samples)
Injected flit rate average = 0.371552 (1 samples)
	minimum = 0.13975 (1 samples)
	maximum = 0.54475 (1 samples)
Accepted flit rate average = 0.270181 (1 samples)
	minimum = 0.18175 (1 samples)
	maximum = 0.35575 (1 samples)
Injected packet size average = 18.0226 (1 samples)
Accepted packet size average = 17.8187 (1 samples)
Hops average = 5.13526 (1 samples)
Total run time 19.3293
