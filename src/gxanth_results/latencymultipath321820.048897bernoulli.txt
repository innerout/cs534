BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 356.903
	minimum = 24
	maximum = 875
Network latency average = 326.584
	minimum = 22
	maximum = 867
Slowest packet = 1010
Flit latency average = 282.586
	minimum = 5
	maximum = 874
Slowest flit = 15471
Fragmentation average = 83.9846
	minimum = 0
	maximum = 360
Injected packet rate average = 0.0293021
	minimum = 0.014 (at node 8)
	maximum = 0.041 (at node 137)
Accepted packet rate average = 0.0142083
	minimum = 0.005 (at node 135)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.5215
	minimum = 0.252 (at node 8)
	maximum = 0.732 (at node 137)
Accepted flit rate average= 0.269875
	minimum = 0.093 (at node 174)
	maximum = 0.416 (at node 44)
Injected packet length average = 17.7974
Accepted packet length average = 18.9941
Total in-flight flits = 50208 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 703.352
	minimum = 24
	maximum = 1824
Network latency average = 583.113
	minimum = 22
	maximum = 1719
Slowest packet = 1663
Flit latency average = 529.756
	minimum = 5
	maximum = 1780
Slowest flit = 27180
Fragmentation average = 82.6133
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0221042
	minimum = 0.009 (at node 8)
	maximum = 0.0305 (at node 134)
Accepted packet rate average = 0.0145
	minimum = 0.0075 (at node 116)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.394682
	minimum = 0.162 (at node 8)
	maximum = 0.549 (at node 134)
Accepted flit rate average= 0.266951
	minimum = 0.135 (at node 116)
	maximum = 0.3825 (at node 22)
Injected packet length average = 17.8556
Accepted packet length average = 18.4104
Total in-flight flits = 51067 (0 measured)
latency change    = 0.492568
throughput change = 0.0109551
Class 0:
Packet latency average = 1728.61
	minimum = 883
	maximum = 2748
Network latency average = 996.913
	minimum = 22
	maximum = 2673
Slowest packet = 2493
Flit latency average = 931.298
	minimum = 5
	maximum = 2656
Slowest flit = 44891
Fragmentation average = 71.0042
	minimum = 0
	maximum = 344
Injected packet rate average = 0.015349
	minimum = 0.003 (at node 174)
	maximum = 0.028 (at node 58)
Accepted packet rate average = 0.0148854
	minimum = 0.006 (at node 163)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.276979
	minimum = 0.053 (at node 174)
	maximum = 0.505 (at node 58)
Accepted flit rate average= 0.268521
	minimum = 0.111 (at node 163)
	maximum = 0.48 (at node 16)
Injected packet length average = 18.0455
Accepted packet length average = 18.0392
Total in-flight flits = 52773 (0 measured)
latency change    = 0.593112
throughput change = 0.00584801
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2408.61
	minimum = 1342
	maximum = 3221
Network latency average = 358.911
	minimum = 22
	maximum = 978
Slowest packet = 11540
Flit latency average = 961.654
	minimum = 5
	maximum = 3371
Slowest flit = 78009
Fragmentation average = 34.5467
	minimum = 0
	maximum = 202
Injected packet rate average = 0.0148438
	minimum = 0.006 (at node 171)
	maximum = 0.032 (at node 174)
Accepted packet rate average = 0.0149688
	minimum = 0.006 (at node 139)
	maximum = 0.025 (at node 0)
Injected flit rate average = 0.266458
	minimum = 0.102 (at node 172)
	maximum = 0.573 (at node 174)
Accepted flit rate average= 0.268729
	minimum = 0.126 (at node 71)
	maximum = 0.432 (at node 34)
Injected packet length average = 17.9509
Accepted packet length average = 17.9527
Total in-flight flits = 52297 (41165 measured)
latency change    = 0.282319
throughput change = 0.000775254
Class 0:
Packet latency average = 2988.11
	minimum = 1342
	maximum = 4146
Network latency average = 768.712
	minimum = 22
	maximum = 1872
Slowest packet = 11540
Flit latency average = 970.263
	minimum = 5
	maximum = 3510
Slowest flit = 106036
Fragmentation average = 60.0553
	minimum = 0
	maximum = 293
Injected packet rate average = 0.0147734
	minimum = 0.007 (at node 9)
	maximum = 0.0265 (at node 174)
Accepted packet rate average = 0.0148125
	minimum = 0.0085 (at node 122)
	maximum = 0.0235 (at node 0)
Injected flit rate average = 0.265977
	minimum = 0.129 (at node 9)
	maximum = 0.4725 (at node 174)
Accepted flit rate average= 0.266513
	minimum = 0.1575 (at node 122)
	maximum = 0.4155 (at node 0)
Injected packet length average = 18.0037
Accepted packet length average = 17.9924
Total in-flight flits = 52456 (51987 measured)
latency change    = 0.193935
throughput change = 0.00831534
Class 0:
Packet latency average = 3414.57
	minimum = 1342
	maximum = 5079
Network latency average = 902.652
	minimum = 22
	maximum = 2901
Slowest packet = 11540
Flit latency average = 971.954
	minimum = 5
	maximum = 3654
Slowest flit = 151019
Fragmentation average = 66.4065
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0147483
	minimum = 0.009 (at node 36)
	maximum = 0.0223333 (at node 174)
Accepted packet rate average = 0.0148455
	minimum = 0.01 (at node 36)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.265467
	minimum = 0.159667 (at node 36)
	maximum = 0.397 (at node 174)
Accepted flit rate average= 0.267097
	minimum = 0.179667 (at node 36)
	maximum = 0.371 (at node 138)
Injected packet length average = 17.9999
Accepted packet length average = 17.9918
Total in-flight flits = 51655 (51655 measured)
latency change    = 0.124895
throughput change = 0.00218722
Class 0:
Packet latency average = 3793.98
	minimum = 1342
	maximum = 5688
Network latency average = 946.224
	minimum = 22
	maximum = 3440
Slowest packet = 11540
Flit latency average = 968.785
	minimum = 5
	maximum = 3801
Slowest flit = 214879
Fragmentation average = 70.2964
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148568
	minimum = 0.0085 (at node 152)
	maximum = 0.021 (at node 174)
Accepted packet rate average = 0.0148958
	minimum = 0.0105 (at node 36)
	maximum = 0.01975 (at node 103)
Injected flit rate average = 0.267328
	minimum = 0.15125 (at node 152)
	maximum = 0.37825 (at node 174)
Accepted flit rate average= 0.268201
	minimum = 0.191 (at node 64)
	maximum = 0.35225 (at node 103)
Injected packet length average = 17.9937
Accepted packet length average = 18.0051
Total in-flight flits = 52031 (52031 measured)
latency change    = 0.100002
throughput change = 0.00411371
Class 0:
Packet latency average = 4153.61
	minimum = 1342
	maximum = 6494
Network latency average = 968.17
	minimum = 22
	maximum = 3909
Slowest packet = 11540
Flit latency average = 967.207
	minimum = 5
	maximum = 3892
Slowest flit = 214883
Fragmentation average = 72.4937
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148771
	minimum = 0.0088 (at node 160)
	maximum = 0.021 (at node 174)
Accepted packet rate average = 0.0149052
	minimum = 0.0112 (at node 64)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.267631
	minimum = 0.1584 (at node 160)
	maximum = 0.3748 (at node 174)
Accepted flit rate average= 0.268608
	minimum = 0.2038 (at node 64)
	maximum = 0.347 (at node 138)
Injected packet length average = 17.9895
Accepted packet length average = 18.0211
Total in-flight flits = 51733 (51733 measured)
latency change    = 0.0865834
throughput change = 0.00151824
Class 0:
Packet latency average = 4503.89
	minimum = 1342
	maximum = 7317
Network latency average = 981.454
	minimum = 22
	maximum = 3909
Slowest packet = 11540
Flit latency average = 966.08
	minimum = 5
	maximum = 3892
Slowest flit = 214883
Fragmentation average = 73.646
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0148872
	minimum = 0.00933333 (at node 160)
	maximum = 0.0206667 (at node 177)
Accepted packet rate average = 0.0149089
	minimum = 0.0118333 (at node 64)
	maximum = 0.0193333 (at node 138)
Injected flit rate average = 0.267861
	minimum = 0.167 (at node 160)
	maximum = 0.3695 (at node 177)
Accepted flit rate average= 0.268368
	minimum = 0.211667 (at node 149)
	maximum = 0.350667 (at node 138)
Injected packet length average = 17.9928
Accepted packet length average = 18.0006
Total in-flight flits = 52205 (52205 measured)
latency change    = 0.0777729
throughput change = 0.000895329
Class 0:
Packet latency average = 4855.92
	minimum = 1342
	maximum = 8161
Network latency average = 990.994
	minimum = 22
	maximum = 3988
Slowest packet = 11540
Flit latency average = 966.471
	minimum = 5
	maximum = 3917
Slowest flit = 357739
Fragmentation average = 74.4977
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0148594
	minimum = 0.00942857 (at node 36)
	maximum = 0.0198571 (at node 174)
Accepted packet rate average = 0.0149159
	minimum = 0.0118571 (at node 67)
	maximum = 0.0187143 (at node 128)
Injected flit rate average = 0.267527
	minimum = 0.169 (at node 36)
	maximum = 0.356571 (at node 174)
Accepted flit rate average= 0.268536
	minimum = 0.216143 (at node 67)
	maximum = 0.340714 (at node 181)
Injected packet length average = 18.0039
Accepted packet length average = 18.0033
Total in-flight flits = 51249 (51249 measured)
latency change    = 0.072495
throughput change = 0.000624344
Draining all recorded packets ...
Class 0:
Remaining flits: 417510 417511 417512 417513 417514 417515 417516 417517 417518 417519 [...] (51081 flits)
Measured flits: 417510 417511 417512 417513 417514 417515 417516 417517 417518 417519 [...] (51081 flits)
Class 0:
Remaining flits: 417513 417514 417515 417516 417517 417518 417519 417520 417521 417522 [...] (51277 flits)
Measured flits: 417513 417514 417515 417516 417517 417518 417519 417520 417521 417522 [...] (51277 flits)
Class 0:
Remaining flits: 519232 519233 519234 519235 519236 519237 519238 519239 519240 519241 [...] (50538 flits)
Measured flits: 519232 519233 519234 519235 519236 519237 519238 519239 519240 519241 [...] (50538 flits)
Class 0:
Remaining flits: 564678 564679 564680 564681 564682 564683 564684 564685 564686 564687 [...] (49531 flits)
Measured flits: 564678 564679 564680 564681 564682 564683 564684 564685 564686 564687 [...] (49531 flits)
Class 0:
Remaining flits: 608166 608167 608168 608169 608170 608171 608172 608173 608174 608175 [...] (49346 flits)
Measured flits: 608166 608167 608168 608169 608170 608171 608172 608173 608174 608175 [...] (49346 flits)
Class 0:
Remaining flits: 655194 655195 655196 655197 655198 655199 656874 656875 656876 656877 [...] (50171 flits)
Measured flits: 655194 655195 655196 655197 655198 655199 656874 656875 656876 656877 [...] (50171 flits)
Class 0:
Remaining flits: 683318 683319 683320 683321 683322 683323 683324 683325 683326 683327 [...] (50784 flits)
Measured flits: 683318 683319 683320 683321 683322 683323 683324 683325 683326 683327 [...] (50784 flits)
Class 0:
Remaining flits: 716058 716059 716060 716061 716062 716063 716064 716065 716066 716067 [...] (49209 flits)
Measured flits: 716058 716059 716060 716061 716062 716063 716064 716065 716066 716067 [...] (49209 flits)
Class 0:
Remaining flits: 747126 747127 747128 747129 747130 747131 747132 747133 747134 747135 [...] (49804 flits)
Measured flits: 747126 747127 747128 747129 747130 747131 747132 747133 747134 747135 [...] (49804 flits)
Class 0:
Remaining flits: 747126 747127 747128 747129 747130 747131 747132 747133 747134 747135 [...] (49271 flits)
Measured flits: 747126 747127 747128 747129 747130 747131 747132 747133 747134 747135 [...] (49271 flits)
Class 0:
Remaining flits: 747142 747143 845532 845533 845534 845535 845536 845537 845538 845539 [...] (48619 flits)
Measured flits: 747142 747143 845532 845533 845534 845535 845536 845537 845538 845539 [...] (48619 flits)
Class 0:
Remaining flits: 862920 862921 862922 862923 862924 862925 862926 862927 862928 862929 [...] (49178 flits)
Measured flits: 862920 862921 862922 862923 862924 862925 862926 862927 862928 862929 [...] (49178 flits)
Class 0:
Remaining flits: 862920 862921 862922 862923 862924 862925 862926 862927 862928 862929 [...] (47657 flits)
Measured flits: 862920 862921 862922 862923 862924 862925 862926 862927 862928 862929 [...] (47657 flits)
Class 0:
Remaining flits: 979668 979669 979670 979671 979672 979673 979674 979675 979676 979677 [...] (48522 flits)
Measured flits: 979668 979669 979670 979671 979672 979673 979674 979675 979676 979677 [...] (48522 flits)
Class 0:
Remaining flits: 1020654 1020655 1020656 1020657 1020658 1020659 1020660 1020661 1020662 1020663 [...] (47926 flits)
Measured flits: 1020654 1020655 1020656 1020657 1020658 1020659 1020660 1020661 1020662 1020663 [...] (47890 flits)
Class 0:
Remaining flits: 1074883 1074884 1074885 1074886 1074887 1114560 1114561 1114562 1114563 1114564 [...] (48286 flits)
Measured flits: 1074883 1074884 1074885 1074886 1074887 1114560 1114561 1114562 1114563 1114564 [...] (47603 flits)
Class 0:
Remaining flits: 1114560 1114561 1114562 1114563 1114564 1114565 1114566 1114567 1114568 1114569 [...] (49132 flits)
Measured flits: 1114560 1114561 1114562 1114563 1114564 1114565 1114566 1114567 1114568 1114569 [...] (47884 flits)
Class 0:
Remaining flits: 1168200 1168201 1168202 1168203 1168204 1168205 1168206 1168207 1168208 1168209 [...] (49300 flits)
Measured flits: 1168200 1168201 1168202 1168203 1168204 1168205 1168206 1168207 1168208 1168209 [...] (46139 flits)
Class 0:
Remaining flits: 1283922 1283923 1283924 1283925 1283926 1283927 1283928 1283929 1283930 1283931 [...] (49602 flits)
Measured flits: 1283922 1283923 1283924 1283925 1283926 1283927 1283928 1283929 1283930 1283931 [...] (41980 flits)
Class 0:
Remaining flits: 1323360 1323361 1323362 1323363 1323364 1323365 1323366 1323367 1323368 1323369 [...] (48516 flits)
Measured flits: 1323360 1323361 1323362 1323363 1323364 1323365 1323366 1323367 1323368 1323369 [...] (34870 flits)
Class 0:
Remaining flits: 1323360 1323361 1323362 1323363 1323364 1323365 1323366 1323367 1323368 1323369 [...] (48357 flits)
Measured flits: 1323360 1323361 1323362 1323363 1323364 1323365 1323366 1323367 1323368 1323369 [...] (26900 flits)
Class 0:
Remaining flits: 1440486 1440487 1440488 1440489 1440490 1440491 1440492 1440493 1440494 1440495 [...] (46893 flits)
Measured flits: 1440486 1440487 1440488 1440489 1440490 1440491 1440492 1440493 1440494 1440495 [...] (19979 flits)
Class 0:
Remaining flits: 1475280 1475281 1475282 1475283 1475284 1475285 1475286 1475287 1475288 1475289 [...] (47970 flits)
Measured flits: 1475280 1475281 1475282 1475283 1475284 1475285 1475286 1475287 1475288 1475289 [...] (14374 flits)
Class 0:
Remaining flits: 1511406 1511407 1511408 1511409 1511410 1511411 1511412 1511413 1511414 1511415 [...] (47315 flits)
Measured flits: 1511406 1511407 1511408 1511409 1511410 1511411 1511412 1511413 1511414 1511415 [...] (10532 flits)
Class 0:
Remaining flits: 1511406 1511407 1511408 1511409 1511410 1511411 1511412 1511413 1511414 1511415 [...] (48191 flits)
Measured flits: 1511406 1511407 1511408 1511409 1511410 1511411 1511412 1511413 1511414 1511415 [...] (8565 flits)
Class 0:
Remaining flits: 1602291 1602292 1602293 1602294 1602295 1602296 1602297 1602298 1602299 1602300 [...] (48726 flits)
Measured flits: 1624446 1624447 1624448 1624449 1624450 1624451 1624452 1624453 1624454 1624455 [...] (5452 flits)
Class 0:
Remaining flits: 1647389 1647390 1647391 1647392 1647393 1647394 1647395 1655496 1655497 1655498 [...] (48618 flits)
Measured flits: 1734426 1734427 1734428 1734429 1734430 1734431 1734432 1734433 1734434 1734435 [...] (4908 flits)
Class 0:
Remaining flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (48689 flits)
Measured flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (3816 flits)
Class 0:
Remaining flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (48497 flits)
Measured flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (2975 flits)
Class 0:
Remaining flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (48828 flits)
Measured flits: 1758546 1758547 1758548 1758549 1758550 1758551 1758552 1758553 1758554 1758555 [...] (2741 flits)
Class 0:
Remaining flits: 1874394 1874395 1874396 1874397 1874398 1874399 1874400 1874401 1874402 1874403 [...] (48272 flits)
Measured flits: 1943244 1943245 1943246 1943247 1943248 1943249 1943250 1943251 1943252 1943253 [...] (1768 flits)
Class 0:
Remaining flits: 1944684 1944685 1944686 1944687 1944688 1944689 1944690 1944691 1944692 1944693 [...] (47569 flits)
Measured flits: 2005865 2091731 2091732 2091733 2091734 2091735 2091736 2091737 2091738 2091739 [...] (1170 flits)
Class 0:
Remaining flits: 1970460 1970461 1970462 1970463 1970464 1970465 1970466 1970467 1970468 1970469 [...] (47607 flits)
Measured flits: 2092248 2092249 2092250 2092251 2092252 2092253 2092254 2092255 2092256 2092257 [...] (688 flits)
Class 0:
Remaining flits: 1970460 1970461 1970462 1970463 1970464 1970465 1970466 1970467 1970468 1970469 [...] (47267 flits)
Measured flits: 2120595 2120596 2120597 2126037 2126038 2126039 2126040 2126041 2126042 2126043 [...] (546 flits)
Class 0:
Remaining flits: 2065716 2065717 2065718 2065719 2065720 2065721 2065722 2065723 2065724 2065725 [...] (48629 flits)
Measured flits: 2228796 2228797 2228798 2228799 2228800 2228801 2228802 2228803 2228804 2228805 [...] (215 flits)
Class 0:
Remaining flits: 2125818 2125819 2125820 2125821 2125822 2125823 2125824 2125825 2125826 2125827 [...] (48395 flits)
Measured flits: 2228796 2228797 2228798 2228799 2228800 2228801 2228802 2228803 2228804 2228805 [...] (324 flits)
Class 0:
Remaining flits: 2137536 2137537 2137538 2137539 2137540 2137541 2137542 2137543 2137544 2137545 [...] (48312 flits)
Measured flits: 2301984 2301985 2301986 2301987 2301988 2301989 2301990 2301991 2301992 2301993 [...] (156 flits)
Class 0:
Remaining flits: 2137542 2137543 2137544 2137545 2137546 2137547 2137548 2137549 2137550 2137551 [...] (47942 flits)
Measured flits: 2301984 2301985 2301986 2301987 2301988 2301989 2301990 2301991 2301992 2301993 [...] (19 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2323062 2323063 2323064 2323065 2323066 2323067 2323068 2323069 2323070 2323071 [...] (8200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2412414 2412415 2412416 2412417 2412418 2412419 2412420 2412421 2412422 2412423 [...] (212 flits)
Measured flits: (0 flits)
Time taken is 50760 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13175.6 (1 samples)
	minimum = 1342 (1 samples)
	maximum = 38916 (1 samples)
Network latency average = 987.276 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7778 (1 samples)
Flit latency average = 928.53 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7743 (1 samples)
Fragmentation average = 75.253 (1 samples)
	minimum = 0 (1 samples)
	maximum = 370 (1 samples)
Injected packet rate average = 0.0148594 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.0149159 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.267527 (1 samples)
	minimum = 0.169 (1 samples)
	maximum = 0.356571 (1 samples)
Accepted flit rate average = 0.268536 (1 samples)
	minimum = 0.216143 (1 samples)
	maximum = 0.340714 (1 samples)
Injected packet size average = 18.0039 (1 samples)
Accepted packet size average = 18.0033 (1 samples)
Hops average = 5.06208 (1 samples)
Total run time 73.3235
