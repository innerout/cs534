BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 340.997
	minimum = 27
	maximum = 974
Network latency average = 263.304
	minimum = 23
	maximum = 941
Slowest packet = 85
Flit latency average = 196.258
	minimum = 6
	maximum = 951
Slowest flit = 3313
Fragmentation average = 139.837
	minimum = 0
	maximum = 874
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00966146
	minimum = 0.002 (at node 41)
	maximum = 0.019 (at node 167)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.199422
	minimum = 0.075 (at node 41)
	maximum = 0.354 (at node 44)
Injected packet length average = 17.8416
Accepted packet length average = 20.641
Total in-flight flits = 41379 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 561.993
	minimum = 27
	maximum = 1943
Network latency average = 458.763
	minimum = 23
	maximum = 1809
Slowest packet = 99
Flit latency average = 383.339
	minimum = 6
	maximum = 1867
Slowest flit = 9106
Fragmentation average = 177.175
	minimum = 0
	maximum = 1534
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0109271
	minimum = 0.005 (at node 41)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.210664
	minimum = 0.117 (at node 174)
	maximum = 0.3585 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 19.2791
Total in-flight flits = 81429 (0 measured)
latency change    = 0.393237
throughput change = 0.0533655
Class 0:
Packet latency average = 1160.61
	minimum = 29
	maximum = 2943
Network latency average = 1023.49
	minimum = 23
	maximum = 2810
Slowest packet = 505
Flit latency average = 943.786
	minimum = 6
	maximum = 2793
Slowest flit = 10979
Fragmentation average = 219.67
	minimum = 0
	maximum = 2009
Injected packet rate average = 0.0248021
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0127135
	minimum = 0.004 (at node 172)
	maximum = 0.025 (at node 56)
Injected flit rate average = 0.4465
	minimum = 0 (at node 0)
	maximum = 1 (at node 60)
Accepted flit rate average= 0.227547
	minimum = 0.101 (at node 52)
	maximum = 0.443 (at node 56)
Injected packet length average = 18.0025
Accepted packet length average = 17.898
Total in-flight flits = 123456 (0 measured)
latency change    = 0.51578
throughput change = 0.0741949
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 295.201
	minimum = 28
	maximum = 1163
Network latency average = 141.327
	minimum = 23
	maximum = 867
Slowest packet = 13795
Flit latency average = 1340.42
	minimum = 6
	maximum = 3620
Slowest flit = 19331
Fragmentation average = 46.158
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0253698
	minimum = 0 (at node 40)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0125885
	minimum = 0.006 (at node 176)
	maximum = 0.022 (at node 107)
Injected flit rate average = 0.456693
	minimum = 0 (at node 40)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.227807
	minimum = 0.117 (at node 85)
	maximum = 0.375 (at node 152)
Injected packet length average = 18.0014
Accepted packet length average = 18.0964
Total in-flight flits = 167395 (78427 measured)
latency change    = 2.9316
throughput change = 0.00114314
Class 0:
Packet latency average = 592.221
	minimum = 27
	maximum = 2237
Network latency average = 458.39
	minimum = 23
	maximum = 1970
Slowest packet = 13795
Flit latency average = 1554.46
	minimum = 6
	maximum = 4558
Slowest flit = 18666
Fragmentation average = 92.1088
	minimum = 0
	maximum = 1054
Injected packet rate average = 0.0246979
	minimum = 0.002 (at node 86)
	maximum = 0.0545 (at node 117)
Accepted packet rate average = 0.0125339
	minimum = 0.007 (at node 134)
	maximum = 0.0205 (at node 95)
Injected flit rate average = 0.444807
	minimum = 0.0405 (at node 86)
	maximum = 0.9805 (at node 117)
Accepted flit rate average= 0.22681
	minimum = 0.1215 (at node 134)
	maximum = 0.355 (at node 95)
Injected packet length average = 18.0099
Accepted packet length average = 18.0958
Total in-flight flits = 207073 (145448 measured)
latency change    = 0.501535
throughput change = 0.0043975
Class 0:
Packet latency average = 959.886
	minimum = 23
	maximum = 3620
Network latency average = 831.425
	minimum = 23
	maximum = 2964
Slowest packet = 13795
Flit latency average = 1743.48
	minimum = 6
	maximum = 5370
Slowest flit = 18683
Fragmentation average = 122.82
	minimum = 0
	maximum = 1415
Injected packet rate average = 0.0242535
	minimum = 0.00566667 (at node 171)
	maximum = 0.052 (at node 88)
Accepted packet rate average = 0.0125035
	minimum = 0.007 (at node 134)
	maximum = 0.0193333 (at node 95)
Injected flit rate average = 0.436701
	minimum = 0.102 (at node 171)
	maximum = 0.935 (at node 88)
Accepted flit rate average= 0.224781
	minimum = 0.129667 (at node 134)
	maximum = 0.340333 (at node 95)
Injected packet length average = 18.0057
Accepted packet length average = 17.9775
Total in-flight flits = 245442 (202604 measured)
latency change    = 0.38303
throughput change = 0.00902498
Draining remaining packets ...
Class 0:
Remaining flits: 18612 18613 18614 18615 18616 18617 18618 18619 18620 18621 [...] (209527 flits)
Measured flits: 248040 248041 248042 248043 248044 248045 248046 248047 248048 248049 [...] (180235 flits)
Class 0:
Remaining flits: 36936 36937 36938 36939 36940 36941 36942 36943 36944 36945 [...] (174760 flits)
Measured flits: 248202 248203 248204 248205 248206 248207 248208 248209 248210 248211 [...] (155072 flits)
Class 0:
Remaining flits: 49554 49555 49556 49557 49558 49559 49560 49561 49562 49563 [...] (140893 flits)
Measured flits: 248213 248214 248215 248216 248217 248218 248219 248274 248275 248276 [...] (127283 flits)
Class 0:
Remaining flits: 54559 54560 54561 54562 54563 54564 54565 54566 54567 54568 [...] (107808 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (98483 flits)
Class 0:
Remaining flits: 70668 70669 70670 70671 70672 70673 70674 70675 70676 70677 [...] (76389 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (70445 flits)
Class 0:
Remaining flits: 70668 70669 70670 70671 70672 70673 70674 70675 70676 70677 [...] (47550 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (43679 flits)
Class 0:
Remaining flits: 97525 97526 97527 97528 97529 97530 97531 97532 97533 97534 [...] (24463 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (22869 flits)
Class 0:
Remaining flits: 128250 128251 128252 128253 128254 128255 128256 128257 128258 128259 [...] (9498 flits)
Measured flits: 248976 248977 248978 248979 248980 248981 248982 248983 248984 248985 [...] (9115 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (1307 flits)
Measured flits: 264474 264475 264476 264477 264478 264479 264480 264481 264482 264483 [...] (1235 flits)
Time taken is 15774 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4781.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12546 (1 samples)
Network latency average = 4613.37 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12316 (1 samples)
Flit latency average = 4210.81 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13904 (1 samples)
Fragmentation average = 183.082 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3419 (1 samples)
Injected packet rate average = 0.0242535 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.052 (1 samples)
Accepted packet rate average = 0.0125035 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.436701 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.935 (1 samples)
Accepted flit rate average = 0.224781 (1 samples)
	minimum = 0.129667 (1 samples)
	maximum = 0.340333 (1 samples)
Injected packet size average = 18.0057 (1 samples)
Accepted packet size average = 17.9775 (1 samples)
Hops average = 5.06256 (1 samples)
Total run time 13.512
