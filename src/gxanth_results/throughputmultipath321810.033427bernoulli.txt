BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.782
	minimum = 23
	maximum = 967
Network latency average = 318.871
	minimum = 23
	maximum = 954
Slowest packet = 155
Flit latency average = 243.779
	minimum = 6
	maximum = 938
Slowest flit = 4873
Fragmentation average = 162.634
	minimum = 0
	maximum = 903
Injected packet rate average = 0.0219115
	minimum = 0.009 (at node 124)
	maximum = 0.033 (at node 170)
Accepted packet rate average = 0.010125
	minimum = 0.003 (at node 98)
	maximum = 0.018 (at node 104)
Injected flit rate average = 0.386823
	minimum = 0.152 (at node 124)
	maximum = 0.577 (at node 170)
Accepted flit rate average= 0.207839
	minimum = 0.089 (at node 98)
	maximum = 0.335 (at node 104)
Injected packet length average = 17.6539
Accepted packet length average = 20.5273
Total in-flight flits = 36037 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 665.329
	minimum = 23
	maximum = 1871
Network latency average = 548.022
	minimum = 23
	maximum = 1871
Slowest packet = 310
Flit latency average = 449.659
	minimum = 6
	maximum = 1922
Slowest flit = 7202
Fragmentation average = 195.278
	minimum = 0
	maximum = 1794
Injected packet rate average = 0.0164036
	minimum = 0.006 (at node 100)
	maximum = 0.026 (at node 166)
Accepted packet rate average = 0.0105964
	minimum = 0.006 (at node 120)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.291721
	minimum = 0.1075 (at node 100)
	maximum = 0.461 (at node 166)
Accepted flit rate average= 0.202438
	minimum = 0.1235 (at node 153)
	maximum = 0.288 (at node 152)
Injected packet length average = 17.7839
Accepted packet length average = 19.1044
Total in-flight flits = 36024 (0 measured)
latency change    = 0.480283
throughput change = 0.02668
Class 0:
Packet latency average = 1627.51
	minimum = 203
	maximum = 2649
Network latency average = 941.711
	minimum = 25
	maximum = 2648
Slowest packet = 1310
Flit latency average = 809.935
	minimum = 6
	maximum = 2759
Slowest flit = 17419
Fragmentation average = 207.217
	minimum = 0
	maximum = 1973
Injected packet rate average = 0.0110052
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 61)
Accepted packet rate average = 0.0109635
	minimum = 0.004 (at node 101)
	maximum = 0.022 (at node 159)
Injected flit rate average = 0.198266
	minimum = 0 (at node 2)
	maximum = 0.564 (at node 61)
Accepted flit rate average= 0.197078
	minimum = 0.088 (at node 101)
	maximum = 0.388 (at node 159)
Injected packet length average = 18.0156
Accepted packet length average = 17.9758
Total in-flight flits = 36201 (0 measured)
latency change    = 0.591199
throughput change = 0.0271942
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2229.41
	minimum = 933
	maximum = 3289
Network latency average = 350.037
	minimum = 23
	maximum = 930
Slowest packet = 8434
Flit latency average = 871.933
	minimum = 6
	maximum = 3790
Slowest flit = 18377
Fragmentation average = 126.002
	minimum = 0
	maximum = 578
Injected packet rate average = 0.0110156
	minimum = 0 (at node 10)
	maximum = 0.034 (at node 9)
Accepted packet rate average = 0.0109219
	minimum = 0.004 (at node 89)
	maximum = 0.021 (at node 78)
Injected flit rate average = 0.198172
	minimum = 0 (at node 22)
	maximum = 0.614 (at node 9)
Accepted flit rate average= 0.195786
	minimum = 0.075 (at node 89)
	maximum = 0.418 (at node 128)
Injected packet length average = 17.9901
Accepted packet length average = 17.9261
Total in-flight flits = 36770 (24757 measured)
latency change    = 0.269982
throughput change = 0.00659732
Class 0:
Packet latency average = 2689.58
	minimum = 933
	maximum = 4204
Network latency average = 596.159
	minimum = 23
	maximum = 1943
Slowest packet = 8434
Flit latency average = 904.361
	minimum = 6
	maximum = 4544
Slowest flit = 21959
Fragmentation average = 157.677
	minimum = 0
	maximum = 976
Injected packet rate average = 0.0109479
	minimum = 0.001 (at node 183)
	maximum = 0.0235 (at node 9)
Accepted packet rate average = 0.0108411
	minimum = 0.006 (at node 36)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.197232
	minimum = 0.014 (at node 183)
	maximum = 0.4255 (at node 9)
Accepted flit rate average= 0.194937
	minimum = 0.101 (at node 36)
	maximum = 0.344 (at node 128)
Injected packet length average = 18.0155
Accepted packet length average = 17.9813
Total in-flight flits = 37269 (33710 measured)
latency change    = 0.171092
throughput change = 0.00435503
Class 0:
Packet latency average = 3100.62
	minimum = 933
	maximum = 5193
Network latency average = 743.694
	minimum = 23
	maximum = 2800
Slowest packet = 8434
Flit latency average = 918.797
	minimum = 6
	maximum = 5374
Slowest flit = 52565
Fragmentation average = 170.63
	minimum = 0
	maximum = 2303
Injected packet rate average = 0.0107413
	minimum = 0.00166667 (at node 52)
	maximum = 0.0233333 (at node 9)
Accepted packet rate average = 0.0107569
	minimum = 0.006 (at node 36)
	maximum = 0.0173333 (at node 128)
Injected flit rate average = 0.193444
	minimum = 0.0283333 (at node 148)
	maximum = 0.417333 (at node 9)
Accepted flit rate average= 0.19326
	minimum = 0.106667 (at node 36)
	maximum = 0.321667 (at node 128)
Injected packet length average = 18.0094
Accepted packet length average = 17.9661
Total in-flight flits = 36393 (35270 measured)
latency change    = 0.132566
throughput change = 0.00867784
Draining remaining packets ...
Class 0:
Remaining flits: 76599 76600 76601 76602 76603 76604 76605 76606 76607 82822 [...] (5445 flits)
Measured flits: 153702 153703 153704 153705 153706 153707 153708 153709 153710 153711 [...] (5251 flits)
Time taken is 7592 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3561.81 (1 samples)
	minimum = 933 (1 samples)
	maximum = 6549 (1 samples)
Network latency average = 974.409 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4408 (1 samples)
Flit latency average = 1030.84 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6580 (1 samples)
Fragmentation average = 175.67 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2552 (1 samples)
Injected packet rate average = 0.0107413 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.0233333 (1 samples)
Accepted packet rate average = 0.0107569 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.193444 (1 samples)
	minimum = 0.0283333 (1 samples)
	maximum = 0.417333 (1 samples)
Accepted flit rate average = 0.19326 (1 samples)
	minimum = 0.106667 (1 samples)
	maximum = 0.321667 (1 samples)
Injected packet size average = 18.0094 (1 samples)
Accepted packet size average = 17.9661 (1 samples)
Hops average = 5.13963 (1 samples)
Total run time 7.63276
