BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 335.546
	minimum = 22
	maximum = 857
Network latency average = 310.178
	minimum = 22
	maximum = 789
Slowest packet = 433
Flit latency average = 287.475
	minimum = 5
	maximum = 809
Slowest flit = 25914
Fragmentation average = 26.4043
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0451406
	minimum = 0.031 (at node 88)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.015125
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.805219
	minimum = 0.541 (at node 88)
	maximum = 0.995 (at node 15)
Accepted flit rate average= 0.279406
	minimum = 0.118 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.838
Accepted packet length average = 18.4731
Total in-flight flits = 102630 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 668.481
	minimum = 22
	maximum = 1682
Network latency average = 628.885
	minimum = 22
	maximum = 1630
Slowest packet = 433
Flit latency average = 604.9
	minimum = 5
	maximum = 1595
Slowest flit = 53927
Fragmentation average = 27.0564
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0365313
	minimum = 0.0205 (at node 88)
	maximum = 0.051 (at node 171)
Accepted packet rate average = 0.0151094
	minimum = 0.009 (at node 30)
	maximum = 0.021 (at node 173)
Injected flit rate average = 0.656068
	minimum = 0.369 (at node 88)
	maximum = 0.918 (at node 171)
Accepted flit rate average= 0.274956
	minimum = 0.167 (at node 30)
	maximum = 0.378 (at node 173)
Injected packet length average = 17.9591
Accepted packet length average = 18.1977
Total in-flight flits = 149027 (0 measured)
latency change    = 0.498046
throughput change = 0.0161863
Class 0:
Packet latency average = 1721.31
	minimum = 182
	maximum = 2618
Network latency average = 1597.7
	minimum = 22
	maximum = 2502
Slowest packet = 4010
Flit latency average = 1580.12
	minimum = 5
	maximum = 2485
Slowest flit = 69424
Fragmentation average = 21.7261
	minimum = 0
	maximum = 219
Injected packet rate average = 0.0148438
	minimum = 0.002 (at node 7)
	maximum = 0.034 (at node 173)
Accepted packet rate average = 0.0148333
	minimum = 0.007 (at node 126)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.267141
	minimum = 0.036 (at node 7)
	maximum = 0.616 (at node 173)
Accepted flit rate average= 0.266021
	minimum = 0.126 (at node 132)
	maximum = 0.486 (at node 16)
Injected packet length average = 17.9968
Accepted packet length average = 17.934
Total in-flight flits = 149431 (0 measured)
latency change    = 0.611643
throughput change = 0.0335872
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1760.89
	minimum = 853
	maximum = 2443
Network latency average = 45.0517
	minimum = 22
	maximum = 538
Slowest packet = 17028
Flit latency average = 2113.77
	minimum = 5
	maximum = 3209
Slowest flit = 123267
Fragmentation average = 5.12069
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0151094
	minimum = 0.002 (at node 62)
	maximum = 0.036 (at node 86)
Accepted packet rate average = 0.0146927
	minimum = 0.007 (at node 85)
	maximum = 0.025 (at node 181)
Injected flit rate average = 0.272448
	minimum = 0.036 (at node 62)
	maximum = 0.646 (at node 86)
Accepted flit rate average= 0.263604
	minimum = 0.143 (at node 85)
	maximum = 0.436 (at node 181)
Injected packet length average = 18.0317
Accepted packet length average = 17.9412
Total in-flight flits = 151235 (50307 measured)
latency change    = 0.0224779
throughput change = 0.00916779
Class 0:
Packet latency average = 2600.05
	minimum = 853
	maximum = 3782
Network latency average = 740.308
	minimum = 22
	maximum = 1911
Slowest packet = 17028
Flit latency average = 2329.98
	minimum = 5
	maximum = 4182
Slowest flit = 113523
Fragmentation average = 11.2293
	minimum = 0
	maximum = 93
Injected packet rate average = 0.014901
	minimum = 0.0045 (at node 153)
	maximum = 0.0275 (at node 111)
Accepted packet rate average = 0.0147682
	minimum = 0.0085 (at node 36)
	maximum = 0.0215 (at node 11)
Injected flit rate average = 0.268349
	minimum = 0.081 (at node 153)
	maximum = 0.495 (at node 111)
Accepted flit rate average= 0.265404
	minimum = 0.153 (at node 36)
	maximum = 0.3955 (at node 56)
Injected packet length average = 18.0087
Accepted packet length average = 17.9713
Total in-flight flits = 150530 (94383 measured)
latency change    = 0.322749
throughput change = 0.00678016
Class 0:
Packet latency average = 3328.47
	minimum = 853
	maximum = 4774
Network latency average = 1370.95
	minimum = 22
	maximum = 2985
Slowest packet = 17028
Flit latency average = 2485.46
	minimum = 5
	maximum = 4808
Slowest flit = 181241
Fragmentation average = 14.7395
	minimum = 0
	maximum = 104
Injected packet rate average = 0.0150469
	minimum = 0.007 (at node 110)
	maximum = 0.025 (at node 21)
Accepted packet rate average = 0.0148125
	minimum = 0.00933333 (at node 161)
	maximum = 0.02 (at node 11)
Injected flit rate average = 0.270859
	minimum = 0.126 (at node 110)
	maximum = 0.45 (at node 86)
Accepted flit rate average= 0.266604
	minimum = 0.172 (at node 161)
	maximum = 0.357333 (at node 11)
Injected packet length average = 18.001
Accepted packet length average = 17.9986
Total in-flight flits = 151909 (128643 measured)
latency change    = 0.218846
throughput change = 0.00450301
Class 0:
Packet latency average = 3944.33
	minimum = 853
	maximum = 5671
Network latency average = 1885.5
	minimum = 22
	maximum = 3961
Slowest packet = 17028
Flit latency average = 2601.7
	minimum = 5
	maximum = 5452
Slowest flit = 223685
Fragmentation average = 16.9825
	minimum = 0
	maximum = 108
Injected packet rate average = 0.0150612
	minimum = 0.00825 (at node 36)
	maximum = 0.02575 (at node 45)
Accepted packet rate average = 0.0148255
	minimum = 0.01125 (at node 89)
	maximum = 0.0195 (at node 11)
Injected flit rate average = 0.271077
	minimum = 0.14825 (at node 180)
	maximum = 0.461 (at node 45)
Accepted flit rate average= 0.266876
	minimum = 0.2025 (at node 89)
	maximum = 0.35425 (at node 128)
Injected packet length average = 17.9984
Accepted packet length average = 18.0011
Total in-flight flits = 152748 (148798 measured)
latency change    = 0.156137
throughput change = 0.00101971
Class 0:
Packet latency average = 4522.4
	minimum = 853
	maximum = 6429
Network latency average = 2283.49
	minimum = 22
	maximum = 4903
Slowest packet = 17028
Flit latency average = 2659.14
	minimum = 5
	maximum = 6281
Slowest flit = 230202
Fragmentation average = 17.856
	minimum = 0
	maximum = 178
Injected packet rate average = 0.0149833
	minimum = 0.0086 (at node 36)
	maximum = 0.0238 (at node 18)
Accepted packet rate average = 0.0148469
	minimum = 0.0112 (at node 31)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.269757
	minimum = 0.1548 (at node 36)
	maximum = 0.4284 (at node 18)
Accepted flit rate average= 0.267293
	minimum = 0.2016 (at node 31)
	maximum = 0.3594 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 18.0033
Total in-flight flits = 151796 (151562 measured)
latency change    = 0.127824
throughput change = 0.00155787
Class 0:
Packet latency average = 4997.11
	minimum = 853
	maximum = 7198
Network latency average = 2499.1
	minimum = 22
	maximum = 5700
Slowest packet = 17028
Flit latency average = 2703.67
	minimum = 5
	maximum = 6281
Slowest flit = 230202
Fragmentation average = 18.393
	minimum = 0
	maximum = 178
Injected packet rate average = 0.0149028
	minimum = 0.00816667 (at node 36)
	maximum = 0.0216667 (at node 18)
Accepted packet rate average = 0.0148481
	minimum = 0.0111667 (at node 161)
	maximum = 0.0188333 (at node 11)
Injected flit rate average = 0.26824
	minimum = 0.1455 (at node 36)
	maximum = 0.39 (at node 18)
Accepted flit rate average= 0.267264
	minimum = 0.201 (at node 161)
	maximum = 0.337667 (at node 11)
Injected packet length average = 17.9993
Accepted packet length average = 17.9999
Total in-flight flits = 150495 (150495 measured)
latency change    = 0.0949963
throughput change = 0.000107831
Class 0:
Packet latency average = 5414.03
	minimum = 853
	maximum = 8162
Network latency average = 2600.35
	minimum = 22
	maximum = 5700
Slowest packet = 17028
Flit latency average = 2729.4
	minimum = 5
	maximum = 6281
Slowest flit = 230202
Fragmentation average = 18.6481
	minimum = 0
	maximum = 178
Injected packet rate average = 0.0147999
	minimum = 0.00871429 (at node 36)
	maximum = 0.0221429 (at node 18)
Accepted packet rate average = 0.0148296
	minimum = 0.0114286 (at node 161)
	maximum = 0.0188571 (at node 181)
Injected flit rate average = 0.266362
	minimum = 0.156857 (at node 36)
	maximum = 0.397429 (at node 18)
Accepted flit rate average= 0.266992
	minimum = 0.206571 (at node 161)
	maximum = 0.337429 (at node 181)
Injected packet length average = 17.9976
Accepted packet length average = 18.004
Total in-flight flits = 148560 (148560 measured)
latency change    = 0.0770071
throughput change = 0.00101903
Draining all recorded packets ...
Class 0:
Remaining flits: 408168 408169 408170 408171 408172 408173 408174 408175 408176 408177 [...] (150040 flits)
Measured flits: 408168 408169 408170 408171 408172 408173 408174 408175 408176 408177 [...] (150040 flits)
Class 0:
Remaining flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (149509 flits)
Measured flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (149509 flits)
Class 0:
Remaining flits: 484038 484039 484040 484041 484042 484043 484044 484045 484046 484047 [...] (148924 flits)
Measured flits: 484038 484039 484040 484041 484042 484043 484044 484045 484046 484047 [...] (148924 flits)
Class 0:
Remaining flits: 545580 545581 545582 545583 545584 545585 545586 545587 545588 545589 [...] (149742 flits)
Measured flits: 545580 545581 545582 545583 545584 545585 545586 545587 545588 545589 [...] (149742 flits)
Class 0:
Remaining flits: 586746 586747 586748 586749 586750 586751 586752 586753 586754 586755 [...] (148620 flits)
Measured flits: 586746 586747 586748 586749 586750 586751 586752 586753 586754 586755 [...] (148620 flits)
Class 0:
Remaining flits: 652680 652681 652682 652683 652684 652685 652686 652687 652688 652689 [...] (148317 flits)
Measured flits: 652680 652681 652682 652683 652684 652685 652686 652687 652688 652689 [...] (148317 flits)
Class 0:
Remaining flits: 665992 665993 665994 665995 665996 665997 665998 665999 710532 710533 [...] (145502 flits)
Measured flits: 665992 665993 665994 665995 665996 665997 665998 665999 710532 710533 [...] (145502 flits)
Class 0:
Remaining flits: 770382 770383 770384 770385 770386 770387 770388 770389 770390 770391 [...] (147712 flits)
Measured flits: 770382 770383 770384 770385 770386 770387 770388 770389 770390 770391 [...] (147712 flits)
Class 0:
Remaining flits: 770382 770383 770384 770385 770386 770387 770388 770389 770390 770391 [...] (145844 flits)
Measured flits: 770382 770383 770384 770385 770386 770387 770388 770389 770390 770391 [...] (145844 flits)
Class 0:
Remaining flits: 869832 869833 869834 869835 869836 869837 869838 869839 869840 869841 [...] (144111 flits)
Measured flits: 869832 869833 869834 869835 869836 869837 869838 869839 869840 869841 [...] (144111 flits)
Class 0:
Remaining flits: 886212 886213 886214 886215 886216 886217 886218 886219 886220 886221 [...] (143693 flits)
Measured flits: 886212 886213 886214 886215 886216 886217 886218 886219 886220 886221 [...] (143693 flits)
Class 0:
Remaining flits: 952146 952147 952148 952149 952150 952151 952152 952153 952154 952155 [...] (141609 flits)
Measured flits: 952146 952147 952148 952149 952150 952151 952152 952153 952154 952155 [...] (141609 flits)
Class 0:
Remaining flits: 998460 998461 998462 998463 998464 998465 998466 998467 998468 998469 [...] (144256 flits)
Measured flits: 998460 998461 998462 998463 998464 998465 998466 998467 998468 998469 [...] (144256 flits)
Class 0:
Remaining flits: 1005570 1005571 1005572 1005573 1005574 1005575 1005576 1005577 1005578 1005579 [...] (142644 flits)
Measured flits: 1005570 1005571 1005572 1005573 1005574 1005575 1005576 1005577 1005578 1005579 [...] (142644 flits)
Class 0:
Remaining flits: 1022778 1022779 1022780 1022781 1022782 1022783 1022784 1022785 1022786 1022787 [...] (145739 flits)
Measured flits: 1022778 1022779 1022780 1022781 1022782 1022783 1022784 1022785 1022786 1022787 [...] (145577 flits)
Class 0:
Remaining flits: 1110365 1130256 1130257 1130258 1130259 1130260 1130261 1130262 1130263 1130264 [...] (147085 flits)
Measured flits: 1110365 1130256 1130257 1130258 1130259 1130260 1130261 1130262 1130263 1130264 [...] (146005 flits)
Class 0:
Remaining flits: 1173006 1173007 1173008 1173009 1173010 1173011 1173012 1173013 1173014 1173015 [...] (144121 flits)
Measured flits: 1173006 1173007 1173008 1173009 1173010 1173011 1173012 1173013 1173014 1173015 [...] (141122 flits)
Class 0:
Remaining flits: 1252044 1252045 1252046 1252047 1252048 1252049 1252050 1252051 1252052 1252053 [...] (143302 flits)
Measured flits: 1252044 1252045 1252046 1252047 1252048 1252049 1252050 1252051 1252052 1252053 [...] (136074 flits)
Class 0:
Remaining flits: 1280988 1280989 1280990 1280991 1280992 1280993 1280994 1280995 1280996 1280997 [...] (143919 flits)
Measured flits: 1280988 1280989 1280990 1280991 1280992 1280993 1280994 1280995 1280996 1280997 [...] (130716 flits)
Class 0:
Remaining flits: 1344888 1344889 1344890 1344891 1344892 1344893 1344894 1344895 1344896 1344897 [...] (144337 flits)
Measured flits: 1344888 1344889 1344890 1344891 1344892 1344893 1344894 1344895 1344896 1344897 [...] (123384 flits)
Class 0:
Remaining flits: 1402344 1402345 1402346 1402347 1402348 1402349 1402350 1402351 1402352 1402353 [...] (145488 flits)
Measured flits: 1402344 1402345 1402346 1402347 1402348 1402349 1402350 1402351 1402352 1402353 [...] (108891 flits)
Class 0:
Remaining flits: 1463940 1463941 1463942 1463943 1463944 1463945 1463946 1463947 1463948 1463949 [...] (141300 flits)
Measured flits: 1463940 1463941 1463942 1463943 1463944 1463945 1463946 1463947 1463948 1463949 [...] (89778 flits)
Class 0:
Remaining flits: 1463940 1463941 1463942 1463943 1463944 1463945 1463946 1463947 1463948 1463949 [...] (142861 flits)
Measured flits: 1463940 1463941 1463942 1463943 1463944 1463945 1463946 1463947 1463948 1463949 [...] (73497 flits)
Class 0:
Remaining flits: 1475856 1475857 1475858 1475859 1475860 1475861 1475862 1475863 1475864 1475865 [...] (145013 flits)
Measured flits: 1475856 1475857 1475858 1475859 1475860 1475861 1475862 1475863 1475864 1475865 [...] (55660 flits)
Class 0:
Remaining flits: 1518462 1518463 1518464 1518465 1518466 1518467 1518468 1518469 1518470 1518471 [...] (144902 flits)
Measured flits: 1518462 1518463 1518464 1518465 1518466 1518467 1518468 1518469 1518470 1518471 [...] (38419 flits)
Class 0:
Remaining flits: 1613232 1613233 1613234 1613235 1613236 1613237 1613238 1613239 1613240 1613241 [...] (144151 flits)
Measured flits: 1613232 1613233 1613234 1613235 1613236 1613237 1613238 1613239 1613240 1613241 [...] (25868 flits)
Class 0:
Remaining flits: 1613232 1613233 1613234 1613235 1613236 1613237 1613238 1613239 1613240 1613241 [...] (143121 flits)
Measured flits: 1613232 1613233 1613234 1613235 1613236 1613237 1613238 1613239 1613240 1613241 [...] (15720 flits)
Class 0:
Remaining flits: 1766574 1766575 1766576 1766577 1766578 1766579 1766580 1766581 1766582 1766583 [...] (143212 flits)
Measured flits: 1766574 1766575 1766576 1766577 1766578 1766579 1766580 1766581 1766582 1766583 [...] (10045 flits)
Class 0:
Remaining flits: 1782594 1782595 1782596 1782597 1782598 1782599 1782600 1782601 1782602 1782603 [...] (145794 flits)
Measured flits: 1782594 1782595 1782596 1782597 1782598 1782599 1782600 1782601 1782602 1782603 [...] (6424 flits)
Class 0:
Remaining flits: 1836414 1836415 1836416 1836417 1836418 1836419 1836420 1836421 1836422 1836423 [...] (145409 flits)
Measured flits: 1836414 1836415 1836416 1836417 1836418 1836419 1836420 1836421 1836422 1836423 [...] (3158 flits)
Class 0:
Remaining flits: 1886292 1886293 1886294 1886295 1886296 1886297 1886298 1886299 1886300 1886301 [...] (142426 flits)
Measured flits: 1886292 1886293 1886294 1886295 1886296 1886297 1886298 1886299 1886300 1886301 [...] (1746 flits)
Class 0:
Remaining flits: 1966950 1966951 1966952 1966953 1966954 1966955 1966956 1966957 1966958 1966959 [...] (143403 flits)
Measured flits: 2079635 2079636 2079637 2079638 2079639 2079640 2079641 2079642 2079643 2079644 [...] (1128 flits)
Class 0:
Remaining flits: 2026548 2026549 2026550 2026551 2026552 2026553 2026554 2026555 2026556 2026557 [...] (144351 flits)
Measured flits: 2082510 2082511 2082512 2082513 2082514 2082515 2082516 2082517 2082518 2082519 [...] (864 flits)
Class 0:
Remaining flits: 2059434 2059435 2059436 2059437 2059438 2059439 2059440 2059441 2059442 2059443 [...] (141074 flits)
Measured flits: 2188980 2188981 2188982 2188983 2188984 2188985 2188986 2188987 2188988 2188989 [...] (558 flits)
Class 0:
Remaining flits: 2117772 2117773 2117774 2117775 2117776 2117777 2117778 2117779 2117780 2117781 [...] (142483 flits)
Measured flits: 2226366 2226367 2226368 2226369 2226370 2226371 2226372 2226373 2226374 2226375 [...] (360 flits)
Class 0:
Remaining flits: 2166516 2166517 2166518 2166519 2166520 2166521 2166522 2166523 2166524 2166525 [...] (142423 flits)
Measured flits: 2227680 2227681 2227682 2227683 2227684 2227685 2227686 2227687 2227688 2227689 [...] (216 flits)
Class 0:
Remaining flits: 2252106 2252107 2252108 2252109 2252110 2252111 2252112 2252113 2252114 2252115 [...] (136343 flits)
Measured flits: 2289733 2289734 2289735 2289736 2289737 2289738 2289739 2289740 2289741 2289742 [...] (137 flits)
Class 0:
Remaining flits: 2252106 2252107 2252108 2252109 2252110 2252111 2252112 2252113 2252114 2252115 [...] (134926 flits)
Measured flits: 2298902 2298903 2298904 2298905 2302794 2302795 2302796 2302797 2302798 2302799 [...] (22 flits)
Class 0:
Remaining flits: 2267568 2267569 2267570 2267571 2267572 2267573 2267574 2267575 2267576 2267577 [...] (135424 flits)
Measured flits: 2302794 2302795 2302796 2302797 2302798 2302799 2302800 2302801 2302802 2302803 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2314332 2314333 2314334 2314335 2314336 2314337 2314338 2314339 2314340 2314341 [...] (87246 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2335104 2335105 2335106 2335107 2335108 2335109 2335110 2335111 2335112 2335113 [...] (40674 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2374650 2374651 2374652 2374653 2374654 2374655 2374656 2374657 2374658 2374659 [...] (5908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2597364 2597365 2597366 2597367 2597368 2597369 2597370 2597371 2597372 2597373 [...] (98 flits)
Measured flits: (0 flits)
Time taken is 53179 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14480.3 (1 samples)
	minimum = 853 (1 samples)
	maximum = 39176 (1 samples)
Network latency average = 2832.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8774 (1 samples)
Flit latency average = 2788.03 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8925 (1 samples)
Fragmentation average = 20.4724 (1 samples)
	minimum = 0 (1 samples)
	maximum = 289 (1 samples)
Injected packet rate average = 0.0147999 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.0221429 (1 samples)
Accepted packet rate average = 0.0148296 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.266362 (1 samples)
	minimum = 0.156857 (1 samples)
	maximum = 0.397429 (1 samples)
Accepted flit rate average = 0.266992 (1 samples)
	minimum = 0.206571 (1 samples)
	maximum = 0.337429 (1 samples)
Injected packet size average = 17.9976 (1 samples)
Accepted packet size average = 18.004 (1 samples)
Hops average = 5.06122 (1 samples)
Total run time 52.8978
