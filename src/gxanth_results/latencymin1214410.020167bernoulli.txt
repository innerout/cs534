BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 247.977
	minimum = 23
	maximum = 935
Network latency average = 244.095
	minimum = 23
	maximum = 914
Slowest packet = 239
Flit latency average = 205.485
	minimum = 6
	maximum = 897
Slowest flit = 4319
Fragmentation average = 58.0747
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0200625
	minimum = 0.009 (at node 91)
	maximum = 0.033 (at node 106)
Accepted packet rate average = 0.00941146
	minimum = 0.003 (at node 174)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.357859
	minimum = 0.162 (at node 91)
	maximum = 0.586 (at node 106)
Accepted flit rate average= 0.176792
	minimum = 0.056 (at node 178)
	maximum = 0.357 (at node 70)
Injected packet length average = 17.8372
Accepted packet length average = 18.7847
Total in-flight flits = 35392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 463.442
	minimum = 23
	maximum = 1727
Network latency average = 458.048
	minimum = 23
	maximum = 1727
Slowest packet = 809
Flit latency average = 418.232
	minimum = 6
	maximum = 1783
Slowest flit = 13855
Fragmentation average = 62.8901
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0196406
	minimum = 0.013 (at node 127)
	maximum = 0.029 (at node 13)
Accepted packet rate average = 0.00973958
	minimum = 0.0045 (at node 108)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.351849
	minimum = 0.234 (at node 127)
	maximum = 0.522 (at node 123)
Accepted flit rate average= 0.179211
	minimum = 0.0815 (at node 108)
	maximum = 0.316 (at node 22)
Injected packet length average = 17.9143
Accepted packet length average = 18.4003
Total in-flight flits = 67173 (0 measured)
latency change    = 0.464924
throughput change = 0.0134996
Class 0:
Packet latency average = 1142.84
	minimum = 23
	maximum = 2581
Network latency average = 1126.24
	minimum = 23
	maximum = 2550
Slowest packet = 1215
Flit latency average = 1098.52
	minimum = 6
	maximum = 2533
Slowest flit = 21887
Fragmentation average = 67.4917
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0175573
	minimum = 0.001 (at node 28)
	maximum = 0.032 (at node 46)
Accepted packet rate average = 0.00972396
	minimum = 0.003 (at node 83)
	maximum = 0.019 (at node 151)
Injected flit rate average = 0.316964
	minimum = 0.012 (at node 28)
	maximum = 0.576 (at node 46)
Accepted flit rate average= 0.174479
	minimum = 0.062 (at node 61)
	maximum = 0.344 (at node 151)
Injected packet length average = 18.0531
Accepted packet length average = 17.9432
Total in-flight flits = 94927 (0 measured)
latency change    = 0.594482
throughput change = 0.0271194
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 329.046
	minimum = 23
	maximum = 2011
Network latency average = 203.138
	minimum = 23
	maximum = 901
Slowest packet = 11011
Flit latency average = 1622.04
	minimum = 6
	maximum = 3426
Slowest flit = 36989
Fragmentation average = 22.8077
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0132604
	minimum = 0 (at node 12)
	maximum = 0.031 (at node 90)
Accepted packet rate average = 0.00881771
	minimum = 0.003 (at node 123)
	maximum = 0.017 (at node 160)
Injected flit rate average = 0.238651
	minimum = 0 (at node 12)
	maximum = 0.548 (at node 90)
Accepted flit rate average= 0.158667
	minimum = 0.061 (at node 135)
	maximum = 0.298 (at node 27)
Injected packet length average = 17.9973
Accepted packet length average = 17.9941
Total in-flight flits = 110993 (44110 measured)
latency change    = 2.47319
throughput change = 0.0996586
Class 0:
Packet latency average = 1041.46
	minimum = 23
	maximum = 2851
Network latency average = 693.787
	minimum = 23
	maximum = 1893
Slowest packet = 11011
Flit latency average = 1831.39
	minimum = 6
	maximum = 4403
Slowest flit = 40099
Fragmentation average = 45.0466
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0123047
	minimum = 0 (at node 160)
	maximum = 0.0275 (at node 121)
Accepted packet rate average = 0.0089349
	minimum = 0.004 (at node 36)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.221117
	minimum = 0 (at node 160)
	maximum = 0.491 (at node 121)
Accepted flit rate average= 0.161529
	minimum = 0.0715 (at node 36)
	maximum = 0.2575 (at node 136)
Injected packet length average = 17.9702
Accepted packet length average = 18.0784
Total in-flight flits = 119030 (76727 measured)
latency change    = 0.684052
throughput change = 0.0177181
Class 0:
Packet latency average = 1637.18
	minimum = 23
	maximum = 4048
Network latency average = 1200.05
	minimum = 23
	maximum = 2893
Slowest packet = 11011
Flit latency average = 2057.48
	minimum = 6
	maximum = 5045
Slowest flit = 33750
Fragmentation average = 56.3596
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0115295
	minimum = 0.00133333 (at node 64)
	maximum = 0.0236667 (at node 93)
Accepted packet rate average = 0.00901563
	minimum = 0.00433333 (at node 17)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.207663
	minimum = 0.024 (at node 160)
	maximum = 0.426 (at node 93)
Accepted flit rate average= 0.162366
	minimum = 0.0763333 (at node 17)
	maximum = 0.255667 (at node 136)
Injected packet length average = 18.0114
Accepted packet length average = 18.0094
Total in-flight flits = 122508 (98731 measured)
latency change    = 0.36387
throughput change = 0.00515916
Class 0:
Packet latency average = 2232.59
	minimum = 23
	maximum = 4994
Network latency average = 1716.85
	minimum = 23
	maximum = 3954
Slowest packet = 11011
Flit latency average = 2272.93
	minimum = 6
	maximum = 5655
Slowest flit = 56033
Fragmentation average = 62.5522
	minimum = 0
	maximum = 155
Injected packet rate average = 0.01075
	minimum = 0.001 (at node 160)
	maximum = 0.022 (at node 138)
Accepted packet rate average = 0.00894271
	minimum = 0.005 (at node 135)
	maximum = 0.01325 (at node 16)
Injected flit rate average = 0.193534
	minimum = 0.018 (at node 160)
	maximum = 0.39775 (at node 138)
Accepted flit rate average= 0.161307
	minimum = 0.09325 (at node 135)
	maximum = 0.2375 (at node 16)
Injected packet length average = 18.0031
Accepted packet length average = 18.0379
Total in-flight flits = 121289 (108488 measured)
latency change    = 0.266692
throughput change = 0.00656528
Class 0:
Packet latency average = 2748.77
	minimum = 23
	maximum = 6042
Network latency average = 2126.76
	minimum = 23
	maximum = 4908
Slowest packet = 11011
Flit latency average = 2466.08
	minimum = 6
	maximum = 6861
Slowest flit = 78277
Fragmentation average = 65.4311
	minimum = 0
	maximum = 155
Injected packet rate average = 0.010301
	minimum = 0.0032 (at node 123)
	maximum = 0.0196 (at node 89)
Accepted packet rate average = 0.0089125
	minimum = 0.0054 (at node 135)
	maximum = 0.0126 (at node 16)
Injected flit rate average = 0.185286
	minimum = 0.0576 (at node 123)
	maximum = 0.3528 (at node 89)
Accepted flit rate average= 0.160383
	minimum = 0.0988 (at node 135)
	maximum = 0.225 (at node 33)
Injected packet length average = 17.9872
Accepted packet length average = 17.9953
Total in-flight flits = 120347 (114022 measured)
latency change    = 0.187786
throughput change = 0.00576094
Class 0:
Packet latency average = 3198.44
	minimum = 23
	maximum = 6675
Network latency average = 2465.7
	minimum = 23
	maximum = 5893
Slowest packet = 11011
Flit latency average = 2645.06
	minimum = 6
	maximum = 7213
Slowest flit = 113921
Fragmentation average = 67.426
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0100503
	minimum = 0.00333333 (at node 123)
	maximum = 0.0178333 (at node 61)
Accepted packet rate average = 0.00885938
	minimum = 0.00566667 (at node 135)
	maximum = 0.0116667 (at node 33)
Injected flit rate average = 0.180686
	minimum = 0.0578333 (at node 123)
	maximum = 0.321 (at node 61)
Accepted flit rate average= 0.159591
	minimum = 0.1015 (at node 135)
	maximum = 0.2085 (at node 33)
Injected packet length average = 17.9781
Accepted packet length average = 18.0138
Total in-flight flits = 120832 (118116 measured)
latency change    = 0.140589
throughput change = 0.00496386
Class 0:
Packet latency average = 3623.82
	minimum = 23
	maximum = 7520
Network latency average = 2741.93
	minimum = 23
	maximum = 6933
Slowest packet = 11011
Flit latency average = 2806.53
	minimum = 6
	maximum = 8335
Slowest flit = 94067
Fragmentation average = 68.9588
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00981176
	minimum = 0.004 (at node 123)
	maximum = 0.0172857 (at node 61)
Accepted packet rate average = 0.00883631
	minimum = 0.00571429 (at node 135)
	maximum = 0.0112857 (at node 128)
Injected flit rate average = 0.176413
	minimum = 0.071 (at node 123)
	maximum = 0.311143 (at node 61)
Accepted flit rate average= 0.159078
	minimum = 0.105429 (at node 135)
	maximum = 0.205143 (at node 128)
Injected packet length average = 17.9798
Accepted packet length average = 18.0028
Total in-flight flits = 119788 (118991 measured)
latency change    = 0.117385
throughput change = 0.00322496
Draining all recorded packets ...
Class 0:
Remaining flits: 128628 128629 128630 128631 128632 128633 128634 128635 128636 128637 [...] (120264 flits)
Measured flits: 197658 197659 197660 197661 197662 197663 197664 197665 197666 197667 [...] (120113 flits)
Class 0:
Remaining flits: 136836 136837 136838 136839 136840 136841 136842 136843 136844 136845 [...] (123780 flits)
Measured flits: 201582 201583 201584 201585 201586 201587 201588 201589 201590 201591 [...] (123438 flits)
Class 0:
Remaining flits: 150390 150391 150392 150393 150394 150395 150396 150397 150398 150399 [...] (122334 flits)
Measured flits: 213030 213031 213032 213033 213034 213035 213036 213037 213038 213039 [...] (121344 flits)
Class 0:
Remaining flits: 237600 237601 237602 237603 237604 237605 237606 237607 237608 237609 [...] (120439 flits)
Measured flits: 237600 237601 237602 237603 237604 237605 237606 237607 237608 237609 [...] (117955 flits)
Class 0:
Remaining flits: 275166 275167 275168 275169 275170 275171 275172 275173 275174 275175 [...] (122675 flits)
Measured flits: 275166 275167 275168 275169 275170 275171 275172 275173 275174 275175 [...] (116704 flits)
Class 0:
Remaining flits: 290508 290509 290510 290511 290512 290513 290514 290515 290516 290517 [...] (121338 flits)
Measured flits: 290508 290509 290510 290511 290512 290513 290514 290515 290516 290517 [...] (109256 flits)
Class 0:
Remaining flits: 318654 318655 318656 318657 318658 318659 318660 318661 318662 318663 [...] (121222 flits)
Measured flits: 318654 318655 318656 318657 318658 318659 318660 318661 318662 318663 [...] (100334 flits)
Class 0:
Remaining flits: 334440 334441 334442 334443 334444 334445 334446 334447 334448 334449 [...] (122113 flits)
Measured flits: 334440 334441 334442 334443 334444 334445 334446 334447 334448 334449 [...] (90189 flits)
Class 0:
Remaining flits: 334440 334441 334442 334443 334444 334445 334446 334447 334448 334449 [...] (123615 flits)
Measured flits: 334440 334441 334442 334443 334444 334445 334446 334447 334448 334449 [...] (79319 flits)
Class 0:
Remaining flits: 375912 375913 375914 375915 375916 375917 375918 375919 375920 375921 [...] (125949 flits)
Measured flits: 375912 375913 375914 375915 375916 375917 375918 375919 375920 375921 [...] (64637 flits)
Class 0:
Remaining flits: 417024 417025 417026 417027 417028 417029 417030 417031 417032 417033 [...] (126348 flits)
Measured flits: 417024 417025 417026 417027 417028 417029 417030 417031 417032 417033 [...] (52741 flits)
Class 0:
Remaining flits: 417024 417025 417026 417027 417028 417029 417030 417031 417032 417033 [...] (122607 flits)
Measured flits: 417024 417025 417026 417027 417028 417029 417030 417031 417032 417033 [...] (41589 flits)
Class 0:
Remaining flits: 485892 485893 485894 485895 485896 485897 485898 485899 485900 485901 [...] (123539 flits)
Measured flits: 485892 485893 485894 485895 485896 485897 485898 485899 485900 485901 [...] (30783 flits)
Class 0:
Remaining flits: 485898 485899 485900 485901 485902 485903 485904 485905 485906 485907 [...] (123725 flits)
Measured flits: 485898 485899 485900 485901 485902 485903 485904 485905 485906 485907 [...] (21473 flits)
Class 0:
Remaining flits: 550208 550209 550210 550211 550212 550213 550214 550215 550216 550217 [...] (122464 flits)
Measured flits: 550208 550209 550210 550211 550212 550213 550214 550215 550216 550217 [...] (14249 flits)
Class 0:
Remaining flits: 576288 576289 576290 576291 576292 576293 576294 576295 576296 576297 [...] (120994 flits)
Measured flits: 576288 576289 576290 576291 576292 576293 576294 576295 576296 576297 [...] (9041 flits)
Class 0:
Remaining flits: 612018 612019 612020 612021 612022 612023 612024 612025 612026 612027 [...] (119547 flits)
Measured flits: 612018 612019 612020 612021 612022 612023 612024 612025 612026 612027 [...] (6199 flits)
Class 0:
Remaining flits: 649188 649189 649190 649191 649192 649193 649194 649195 649196 649197 [...] (122177 flits)
Measured flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (4347 flits)
Class 0:
Remaining flits: 656568 656569 656570 656571 656572 656573 656574 656575 656576 656577 [...] (121708 flits)
Measured flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (2892 flits)
Class 0:
Remaining flits: 678132 678133 678134 678135 678136 678137 678138 678139 678140 678141 [...] (120698 flits)
Measured flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (1838 flits)
Class 0:
Remaining flits: 744228 744229 744230 744231 744232 744233 744234 744235 744236 744237 [...] (118429 flits)
Measured flits: 782174 782175 782176 782177 782178 782179 782180 782181 782182 782183 [...] (1327 flits)
Class 0:
Remaining flits: 744228 744229 744230 744231 744232 744233 744234 744235 744236 744237 [...] (117464 flits)
Measured flits: 831006 831007 831008 831009 831010 831011 831012 831013 831014 831015 [...] (901 flits)
Class 0:
Remaining flits: 758358 758359 758360 758361 758362 758363 758364 758365 758366 758367 [...] (117754 flits)
Measured flits: 831006 831007 831008 831009 831010 831011 831012 831013 831014 831015 [...] (612 flits)
Class 0:
Remaining flits: 808578 808579 808580 808581 808582 808583 808584 808585 808586 808587 [...] (118163 flits)
Measured flits: 851670 851671 851672 851673 851674 851675 851676 851677 851678 851679 [...] (480 flits)
Class 0:
Remaining flits: 855180 855181 855182 855183 855184 855185 855186 855187 855188 855189 [...] (117783 flits)
Measured flits: 873036 873037 873038 873039 873040 873041 873042 873043 873044 873045 [...] (324 flits)
Class 0:
Remaining flits: 887148 887149 887150 887151 887152 887153 887154 887155 887156 887157 [...] (118591 flits)
Measured flits: 987300 987301 987302 987303 987304 987305 987306 987307 987308 987309 [...] (136 flits)
Class 0:
Remaining flits: 948798 948799 948800 948801 948802 948803 948804 948805 948806 948807 [...] (122833 flits)
Measured flits: 1125150 1125151 1125152 1125153 1125154 1125155 1125156 1125157 1125158 1125159 [...] (30 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 971928 971929 971930 971931 971932 971933 971934 971935 971936 971937 [...] (91841 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1015380 1015381 1015382 1015383 1015384 1015385 1015386 1015387 1015388 1015389 [...] (62847 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1033554 1033555 1033556 1033557 1033558 1033559 1053324 1053325 1053326 1053327 [...] (34335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1053338 1053339 1053340 1053341 1065366 1065367 1065368 1065369 1065370 1065371 [...] (10092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1172610 1172611 1172612 1172613 1172614 1172615 1172616 1172617 1172618 1172619 [...] (351 flits)
Measured flits: (0 flits)
Time taken is 42694 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8177.87 (1 samples)
	minimum = 23 (1 samples)
	maximum = 27364 (1 samples)
Network latency average = 3792.56 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13338 (1 samples)
Flit latency average = 3718.07 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13280 (1 samples)
Fragmentation average = 72.7417 (1 samples)
	minimum = 0 (1 samples)
	maximum = 163 (1 samples)
Injected packet rate average = 0.00981176 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.0172857 (1 samples)
Accepted packet rate average = 0.00883631 (1 samples)
	minimum = 0.00571429 (1 samples)
	maximum = 0.0112857 (1 samples)
Injected flit rate average = 0.176413 (1 samples)
	minimum = 0.071 (1 samples)
	maximum = 0.311143 (1 samples)
Accepted flit rate average = 0.159078 (1 samples)
	minimum = 0.105429 (1 samples)
	maximum = 0.205143 (1 samples)
Injected packet size average = 17.9798 (1 samples)
Accepted packet size average = 18.0028 (1 samples)
Hops average = 5.05229 (1 samples)
Total run time 31.4253
