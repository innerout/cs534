BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 224.082
	minimum = 23
	maximum = 816
Network latency average = 221.016
	minimum = 23
	maximum = 801
Slowest packet = 304
Flit latency average = 150.324
	minimum = 6
	maximum = 965
Slowest flit = 1161
Fragmentation average = 130.516
	minimum = 0
	maximum = 749
Injected packet rate average = 0.0158542
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00930208
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.282297
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.18824
	minimum = 0.048 (at node 174)
	maximum = 0.315 (at node 48)
Injected packet length average = 17.8058
Accepted packet length average = 20.2363
Total in-flight flits = 18650 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 356.952
	minimum = 23
	maximum = 1808
Network latency average = 353.736
	minimum = 23
	maximum = 1808
Slowest packet = 445
Flit latency average = 266.202
	minimum = 6
	maximum = 1791
Slowest flit = 8027
Fragmentation average = 169.455
	minimum = 0
	maximum = 1661
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 164)
	maximum = 0.0235 (at node 44)
Accepted packet rate average = 0.010375
	minimum = 0.005 (at node 116)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.280391
	minimum = 0.162 (at node 164)
	maximum = 0.423 (at node 44)
Accepted flit rate average= 0.199396
	minimum = 0.1055 (at node 164)
	maximum = 0.287 (at node 22)
Injected packet length average = 17.9211
Accepted packet length average = 19.2189
Total in-flight flits = 31576 (0 measured)
latency change    = 0.372235
throughput change = 0.0559503
Class 0:
Packet latency average = 658.108
	minimum = 27
	maximum = 2785
Network latency average = 654.971
	minimum = 27
	maximum = 2785
Slowest packet = 107
Flit latency average = 556.113
	minimum = 6
	maximum = 2768
Slowest flit = 1943
Fragmentation average = 224.709
	minimum = 0
	maximum = 2143
Injected packet rate average = 0.0156458
	minimum = 0.005 (at node 67)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.0116615
	minimum = 0.004 (at node 4)
	maximum = 0.023 (at node 47)
Injected flit rate average = 0.281401
	minimum = 0.101 (at node 67)
	maximum = 0.504 (at node 35)
Accepted flit rate average= 0.21275
	minimum = 0.078 (at node 4)
	maximum = 0.373 (at node 119)
Injected packet length average = 17.9857
Accepted packet length average = 18.2439
Total in-flight flits = 44800 (0 measured)
latency change    = 0.457609
throughput change = 0.0627693
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 321.293
	minimum = 25
	maximum = 967
Network latency average = 318.154
	minimum = 25
	maximum = 967
Slowest packet = 9023
Flit latency average = 762.818
	minimum = 6
	maximum = 3596
Slowest flit = 13383
Fragmentation average = 129.818
	minimum = 0
	maximum = 638
Injected packet rate average = 0.016276
	minimum = 0.008 (at node 154)
	maximum = 0.027 (at node 155)
Accepted packet rate average = 0.0120313
	minimum = 0.005 (at node 54)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.292349
	minimum = 0.144 (at node 154)
	maximum = 0.486 (at node 155)
Accepted flit rate average= 0.215542
	minimum = 0.093 (at node 48)
	maximum = 0.438 (at node 152)
Injected packet length average = 17.9619
Accepted packet length average = 17.9152
Total in-flight flits = 59666 (40160 measured)
latency change    = 1.04831
throughput change = 0.0129519
Class 0:
Packet latency average = 590.215
	minimum = 23
	maximum = 1988
Network latency average = 586.808
	minimum = 23
	maximum = 1971
Slowest packet = 9068
Flit latency average = 873.253
	minimum = 6
	maximum = 4537
Slowest flit = 13391
Fragmentation average = 164.097
	minimum = 0
	maximum = 1268
Injected packet rate average = 0.0159089
	minimum = 0.008 (at node 154)
	maximum = 0.023 (at node 150)
Accepted packet rate average = 0.0119792
	minimum = 0.006 (at node 54)
	maximum = 0.022 (at node 0)
Injected flit rate average = 0.286482
	minimum = 0.144 (at node 154)
	maximum = 0.414 (at node 150)
Accepted flit rate average= 0.214057
	minimum = 0.12 (at node 26)
	maximum = 0.384 (at node 0)
Injected packet length average = 18.0077
Accepted packet length average = 17.8691
Total in-flight flits = 72564 (62492 measured)
latency change    = 0.455634
throughput change = 0.00693448
Class 0:
Packet latency average = 784.208
	minimum = 23
	maximum = 2975
Network latency average = 780.601
	minimum = 23
	maximum = 2958
Slowest packet = 9108
Flit latency average = 983.244
	minimum = 6
	maximum = 5261
Slowest flit = 39982
Fragmentation average = 175.538
	minimum = 0
	maximum = 1780
Injected packet rate average = 0.0158715
	minimum = 0.00933333 (at node 138)
	maximum = 0.0236667 (at node 3)
Accepted packet rate average = 0.0118507
	minimum = 0.00733333 (at node 54)
	maximum = 0.017 (at node 0)
Injected flit rate average = 0.285753
	minimum = 0.170333 (at node 138)
	maximum = 0.421 (at node 3)
Accepted flit rate average= 0.211884
	minimum = 0.125333 (at node 26)
	maximum = 0.307667 (at node 152)
Injected packet length average = 18.0042
Accepted packet length average = 17.8794
Total in-flight flits = 87311 (82030 measured)
latency change    = 0.247375
throughput change = 0.0102585
Class 0:
Packet latency average = 962.263
	minimum = 23
	maximum = 3974
Network latency average = 958.537
	minimum = 23
	maximum = 3955
Slowest packet = 9056
Flit latency average = 1097.28
	minimum = 6
	maximum = 5701
Slowest flit = 68742
Fragmentation average = 185.116
	minimum = 0
	maximum = 2135
Injected packet rate average = 0.0158646
	minimum = 0.01025 (at node 76)
	maximum = 0.0215 (at node 3)
Accepted packet rate average = 0.0117474
	minimum = 0.008 (at node 36)
	maximum = 0.0165 (at node 123)
Injected flit rate average = 0.285605
	minimum = 0.1845 (at node 76)
	maximum = 0.387 (at node 3)
Accepted flit rate average= 0.210493
	minimum = 0.14025 (at node 36)
	maximum = 0.2935 (at node 123)
Injected packet length average = 18.0027
Accepted packet length average = 17.9183
Total in-flight flits = 102453 (99790 measured)
latency change    = 0.185038
throughput change = 0.00660444
Class 0:
Packet latency average = 1137.34
	minimum = 23
	maximum = 4883
Network latency average = 1133.61
	minimum = 23
	maximum = 4869
Slowest packet = 9280
Flit latency average = 1214.52
	minimum = 6
	maximum = 6651
Slowest flit = 68754
Fragmentation average = 189.131
	minimum = 0
	maximum = 2313
Injected packet rate average = 0.0157615
	minimum = 0.0112 (at node 45)
	maximum = 0.0208 (at node 3)
Accepted packet rate average = 0.0116573
	minimum = 0.0078 (at node 36)
	maximum = 0.0148 (at node 78)
Injected flit rate average = 0.283838
	minimum = 0.2016 (at node 45)
	maximum = 0.3744 (at node 3)
Accepted flit rate average= 0.208729
	minimum = 0.1416 (at node 17)
	maximum = 0.274 (at node 181)
Injected packet length average = 18.0083
Accepted packet length average = 17.9055
Total in-flight flits = 116832 (115390 measured)
latency change    = 0.153936
throughput change = 0.00845269
Class 0:
Packet latency average = 1315.1
	minimum = 23
	maximum = 5817
Network latency average = 1311.01
	minimum = 23
	maximum = 5813
Slowest packet = 9121
Flit latency average = 1342.55
	minimum = 6
	maximum = 6900
Slowest flit = 68759
Fragmentation average = 190.501
	minimum = 0
	maximum = 2984
Injected packet rate average = 0.0157144
	minimum = 0.0095 (at node 40)
	maximum = 0.0198333 (at node 151)
Accepted packet rate average = 0.0115582
	minimum = 0.00783333 (at node 79)
	maximum = 0.015 (at node 123)
Injected flit rate average = 0.2828
	minimum = 0.171 (at node 40)
	maximum = 0.354167 (at node 151)
Accepted flit rate average= 0.207103
	minimum = 0.140667 (at node 79)
	maximum = 0.270333 (at node 123)
Injected packet length average = 17.9962
Accepted packet length average = 17.9184
Total in-flight flits = 132161 (131307 measured)
latency change    = 0.135168
throughput change = 0.00785052
Class 0:
Packet latency average = 1476.79
	minimum = 23
	maximum = 6874
Network latency average = 1471.8
	minimum = 23
	maximum = 6874
Slowest packet = 9388
Flit latency average = 1468.45
	minimum = 6
	maximum = 8276
Slowest flit = 62751
Fragmentation average = 190.644
	minimum = 0
	maximum = 2984
Injected packet rate average = 0.015631
	minimum = 0.0102857 (at node 40)
	maximum = 0.0195714 (at node 3)
Accepted packet rate average = 0.0114963
	minimum = 0.00828571 (at node 79)
	maximum = 0.0144286 (at node 181)
Injected flit rate average = 0.281272
	minimum = 0.183714 (at node 40)
	maximum = 0.352143 (at node 3)
Accepted flit rate average= 0.206222
	minimum = 0.150857 (at node 79)
	maximum = 0.266857 (at node 181)
Injected packet length average = 17.9946
Accepted packet length average = 17.9382
Total in-flight flits = 145925 (145365 measured)
latency change    = 0.109489
throughput change = 0.00427125
Draining all recorded packets ...
Class 0:
Remaining flits: 84906 84907 84908 84909 84910 84911 84912 84913 84914 84915 [...] (161122 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (114917 flits)
Class 0:
Remaining flits: 119340 119341 119342 119343 119344 119345 119346 119347 119348 119349 [...] (173285 flits)
Measured flits: 164304 164305 164306 164307 164308 164309 164310 164311 164312 164313 [...] (87970 flits)
Class 0:
Remaining flits: 119340 119341 119342 119343 119344 119345 119346 119347 119348 119349 [...] (186170 flits)
Measured flits: 164304 164305 164306 164307 164308 164309 164310 164311 164312 164313 [...] (64873 flits)
Class 0:
Remaining flits: 140119 140120 140121 140122 140123 140124 140125 140126 140127 140128 [...] (196165 flits)
Measured flits: 164304 164305 164306 164307 164308 164309 164310 164311 164312 164313 [...] (46915 flits)
Class 0:
Remaining flits: 144010 144011 144012 144013 144014 144015 144016 144017 164304 164305 [...] (206137 flits)
Measured flits: 164304 164305 164306 164307 164308 164309 164310 164311 164312 164313 [...] (33441 flits)
Class 0:
Remaining flits: 213840 213841 213842 213843 213844 213845 213846 213847 213848 213849 [...] (217398 flits)
Measured flits: 213840 213841 213842 213843 213844 213845 213846 213847 213848 213849 [...] (23413 flits)
Class 0:
Remaining flits: 226081 226082 226083 226084 226085 226086 226087 226088 226089 226090 [...] (227304 flits)
Measured flits: 226081 226082 226083 226084 226085 226086 226087 226088 226089 226090 [...] (15993 flits)
Class 0:
Remaining flits: 238086 238087 238088 238089 238090 238091 238092 238093 238094 238095 [...] (237659 flits)
Measured flits: 238086 238087 238088 238089 238090 238091 238092 238093 238094 238095 [...] (10945 flits)
Class 0:
Remaining flits: 244260 244261 244262 244263 244264 244265 244266 244267 244268 244269 [...] (248333 flits)
Measured flits: 244260 244261 244262 244263 244264 244265 244266 244267 244268 244269 [...] (7618 flits)
Class 0:
Remaining flits: 244260 244261 244262 244263 244264 244265 244266 244267 244268 244269 [...] (262824 flits)
Measured flits: 244260 244261 244262 244263 244264 244265 244266 244267 244268 244269 [...] (5055 flits)
Class 0:
Remaining flits: 301464 301465 301466 301467 301468 301469 301470 301471 301472 301473 [...] (271120 flits)
Measured flits: 301464 301465 301466 301467 301468 301469 301470 301471 301472 301473 [...] (3180 flits)
Class 0:
Remaining flits: 301480 301481 305208 305209 305210 305211 305212 305213 305214 305215 [...] (278645 flits)
Measured flits: 301480 301481 305208 305209 305210 305211 305212 305213 305214 305215 [...] (1909 flits)
Class 0:
Remaining flits: 305208 305209 305210 305211 305212 305213 305214 305215 305216 305217 [...] (287210 flits)
Measured flits: 305208 305209 305210 305211 305212 305213 305214 305215 305216 305217 [...] (1250 flits)
Class 0:
Remaining flits: 305208 305209 305210 305211 305212 305213 305214 305215 305216 305217 [...] (294662 flits)
Measured flits: 305208 305209 305210 305211 305212 305213 305214 305215 305216 305217 [...] (803 flits)
Class 0:
Remaining flits: 352170 352171 352172 352173 352174 352175 352176 352177 352178 352179 [...] (298816 flits)
Measured flits: 352170 352171 352172 352173 352174 352175 352176 352177 352178 352179 [...] (503 flits)
Class 0:
Remaining flits: 383886 383887 383888 383889 383890 383891 383892 383893 383894 383895 [...] (302206 flits)
Measured flits: 383886 383887 383888 383889 383890 383891 383892 383893 383894 383895 [...] (270 flits)
Class 0:
Remaining flits: 383888 383889 383890 383891 383892 383893 383894 383895 383896 383897 [...] (307722 flits)
Measured flits: 383888 383889 383890 383891 383892 383893 383894 383895 383896 383897 [...] (160 flits)
Class 0:
Remaining flits: 421362 421363 421364 421365 421366 421367 421368 421369 421370 421371 [...] (310396 flits)
Measured flits: 421362 421363 421364 421365 421366 421367 421368 421369 421370 421371 [...] (54 flits)
Class 0:
Remaining flits: 421362 421363 421364 421365 421366 421367 421368 421369 421370 421371 [...] (315876 flits)
Measured flits: 421362 421363 421364 421365 421366 421367 421368 421369 421370 421371 [...] (54 flits)
Class 0:
Remaining flits: 532944 532945 532946 532947 532948 532949 532950 532951 532952 532953 [...] (317139 flits)
Measured flits: 532944 532945 532946 532947 532948 532949 532950 532951 532952 532953 [...] (18 flits)
Class 0:
Remaining flits: 532959 532960 532961 583992 583993 583994 583995 583996 583997 583998 [...] (320627 flits)
Measured flits: 532959 532960 532961 (3 flits)
Class 0:
Remaining flits: 532959 532960 532961 583992 583993 583994 583995 583996 583997 583998 [...] (319861 flits)
Measured flits: 532959 532960 532961 (3 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 583992 583993 583994 583995 583996 583997 583998 583999 584000 584001 [...] (284528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 583992 583993 583994 583995 583996 583997 583998 583999 584000 584001 [...] (249188 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 587808 587809 587810 587811 587812 587813 587814 587815 587816 587817 [...] (214586 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 587808 587809 587810 587811 587812 587813 587814 587815 587816 587817 [...] (180695 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 587808 587809 587810 587811 587812 587813 587814 587815 587816 587817 [...] (146942 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 587810 587811 587812 587813 587814 587815 587816 587817 587818 587819 [...] (113548 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 657180 657181 657182 657183 657184 657185 657186 657187 657188 657189 [...] (82046 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 657180 657181 657182 657183 657184 657185 657186 657187 657188 657189 [...] (53339 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 818622 818623 818624 818625 818626 818627 818628 818629 818630 818631 [...] (28717 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 958356 958357 958358 958359 958360 958361 958362 958363 958364 958365 [...] (10797 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1115513 1190448 1190449 1190450 1190451 1190452 1190453 1190454 1190455 1190456 [...] (1868 flits)
Measured flits: (0 flits)
Time taken is 44008 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3020.41 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22340 (1 samples)
Network latency average = 2984.35 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22340 (1 samples)
Flit latency average = 5506.59 (1 samples)
	minimum = 6 (1 samples)
	maximum = 28732 (1 samples)
Fragmentation average = 213.49 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4409 (1 samples)
Injected packet rate average = 0.015631 (1 samples)
	minimum = 0.0102857 (1 samples)
	maximum = 0.0195714 (1 samples)
Accepted packet rate average = 0.0114963 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0144286 (1 samples)
Injected flit rate average = 0.281272 (1 samples)
	minimum = 0.183714 (1 samples)
	maximum = 0.352143 (1 samples)
Accepted flit rate average = 0.206222 (1 samples)
	minimum = 0.150857 (1 samples)
	maximum = 0.266857 (1 samples)
Injected packet size average = 17.9946 (1 samples)
Accepted packet size average = 17.9382 (1 samples)
Hops average = 5.07792 (1 samples)
Total run time 49.4654
