BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 44.0523
	minimum = 22
	maximum = 104
Network latency average = 43.1449
	minimum = 22
	maximum = 103
Slowest packet = 63
Flit latency average = 23.0331
	minimum = 5
	maximum = 86
Slowest flit = 10133
Fragmentation average = 5.55819
	minimum = 0
	maximum = 46
Injected packet rate average = 0.00458333
	minimum = 0 (at node 33)
	maximum = 0.01 (at node 39)
Accepted packet rate average = 0.00438542
	minimum = 0.001 (at node 4)
	maximum = 0.011 (at node 140)
Injected flit rate average = 0.0817552
	minimum = 0 (at node 33)
	maximum = 0.18 (at node 39)
Accepted flit rate average= 0.0798594
	minimum = 0.018 (at node 4)
	maximum = 0.198 (at node 140)
Injected packet length average = 17.8375
Accepted packet length average = 18.2102
Total in-flight flits = 507 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 43.9489
	minimum = 22
	maximum = 143
Network latency average = 43.1943
	minimum = 22
	maximum = 121
Slowest packet = 986
Flit latency average = 23.2002
	minimum = 5
	maximum = 104
Slowest flit = 17753
Fragmentation average = 5.34403
	minimum = 0
	maximum = 47
Injected packet rate average = 0.00447656
	minimum = 0.0005 (at node 82)
	maximum = 0.0085 (at node 132)
Accepted packet rate average = 0.00438281
	minimum = 0.0005 (at node 41)
	maximum = 0.008 (at node 48)
Injected flit rate average = 0.080276
	minimum = 0.009 (at node 82)
	maximum = 0.153 (at node 132)
Accepted flit rate average= 0.0794349
	minimum = 0.009 (at node 41)
	maximum = 0.144 (at node 48)
Injected packet length average = 17.9325
Accepted packet length average = 18.1242
Total in-flight flits = 439 (0 measured)
latency change    = 0.00235173
throughput change = 0.00534374
Class 0:
Packet latency average = 43.6118
	minimum = 22
	maximum = 119
Network latency average = 42.7697
	minimum = 22
	maximum = 119
Slowest packet = 2233
Flit latency average = 22.7255
	minimum = 5
	maximum = 102
Slowest flit = 40211
Fragmentation average = 5.40241
	minimum = 0
	maximum = 42
Injected packet rate average = 0.00477604
	minimum = 0 (at node 40)
	maximum = 0.013 (at node 121)
Accepted packet rate average = 0.00475
	minimum = 0 (at node 17)
	maximum = 0.011 (at node 168)
Injected flit rate average = 0.0858698
	minimum = 0 (at node 40)
	maximum = 0.234 (at node 121)
Accepted flit rate average= 0.0854896
	minimum = 0 (at node 17)
	maximum = 0.206 (at node 173)
Injected packet length average = 17.9793
Accepted packet length average = 17.9978
Total in-flight flits = 531 (0 measured)
latency change    = 0.0077286
throughput change = 0.0708237
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.1509
	minimum = 22
	maximum = 120
Network latency average = 44.4155
	minimum = 22
	maximum = 114
Slowest packet = 2890
Flit latency average = 24.1717
	minimum = 5
	maximum = 97
Slowest flit = 60047
Fragmentation average = 5.94595
	minimum = 0
	maximum = 43
Injected packet rate average = 0.00483854
	minimum = 0.001 (at node 0)
	maximum = 0.011 (at node 128)
Accepted packet rate average = 0.00483854
	minimum = 0 (at node 18)
	maximum = 0.013 (at node 22)
Injected flit rate average = 0.087026
	minimum = 0.018 (at node 0)
	maximum = 0.198 (at node 128)
Accepted flit rate average= 0.0870781
	minimum = 0 (at node 18)
	maximum = 0.234 (at node 22)
Injected packet length average = 17.986
Accepted packet length average = 17.9968
Total in-flight flits = 534 (534 measured)
latency change    = 0.034087
throughput change = 0.0182427
Class 0:
Packet latency average = 44.3862
	minimum = 22
	maximum = 120
Network latency average = 43.6382
	minimum = 22
	maximum = 114
Slowest packet = 2890
Flit latency average = 23.6015
	minimum = 5
	maximum = 97
Slowest flit = 60047
Fragmentation average = 5.52162
	minimum = 0
	maximum = 43
Injected packet rate average = 0.00467187
	minimum = 0.001 (at node 0)
	maximum = 0.01 (at node 128)
Accepted packet rate average = 0.0046849
	minimum = 0.001 (at node 24)
	maximum = 0.0085 (at node 22)
Injected flit rate average = 0.0840313
	minimum = 0.0095 (at node 0)
	maximum = 0.18 (at node 128)
Accepted flit rate average= 0.0841901
	minimum = 0.018 (at node 24)
	maximum = 0.153 (at node 22)
Injected packet length average = 17.9866
Accepted packet length average = 17.9705
Total in-flight flits = 494 (494 measured)
latency change    = 0.0172276
throughput change = 0.0343036
Class 0:
Packet latency average = 44.0557
	minimum = 22
	maximum = 120
Network latency average = 43.3785
	minimum = 22
	maximum = 120
Slowest packet = 2890
Flit latency average = 23.4002
	minimum = 5
	maximum = 103
Slowest flit = 87119
Fragmentation average = 5.46378
	minimum = 0
	maximum = 45
Injected packet rate average = 0.00464583
	minimum = 0.000666667 (at node 0)
	maximum = 0.008 (at node 128)
Accepted packet rate average = 0.00464931
	minimum = 0.00133333 (at node 153)
	maximum = 0.00733333 (at node 97)
Injected flit rate average = 0.0837483
	minimum = 0.012 (at node 0)
	maximum = 0.144 (at node 128)
Accepted flit rate average= 0.0837517
	minimum = 0.024 (at node 153)
	maximum = 0.132 (at node 97)
Injected packet length average = 18.0265
Accepted packet length average = 18.0138
Total in-flight flits = 458 (458 measured)
latency change    = 0.00750161
throughput change = 0.00523414
Draining remaining packets ...
Time taken is 6058 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 44.1962 (1 samples)
	minimum = 22 (1 samples)
	maximum = 120 (1 samples)
Network latency average = 43.4929 (1 samples)
	minimum = 22 (1 samples)
	maximum = 120 (1 samples)
Flit latency average = 23.4858 (1 samples)
	minimum = 5 (1 samples)
	maximum = 103 (1 samples)
Fragmentation average = 5.46413 (1 samples)
	minimum = 0 (1 samples)
	maximum = 45 (1 samples)
Injected packet rate average = 0.00464583 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.008 (1 samples)
Accepted packet rate average = 0.00464931 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.00733333 (1 samples)
Injected flit rate average = 0.0837483 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.144 (1 samples)
Accepted flit rate average = 0.0837517 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.132 (1 samples)
Injected packet size average = 18.0265 (1 samples)
Accepted packet size average = 18.0138 (1 samples)
Hops average = 5.09193 (1 samples)
Total run time 1.5267
