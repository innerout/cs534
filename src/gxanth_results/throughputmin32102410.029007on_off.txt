BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 357.314
	minimum = 23
	maximum = 989
Network latency average = 268.449
	minimum = 23
	maximum = 927
Slowest packet = 3
Flit latency average = 202.717
	minimum = 6
	maximum = 956
Slowest flit = 3803
Fragmentation average = 142.742
	minimum = 0
	maximum = 806
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0101146
	minimum = 0.004 (at node 178)
	maximum = 0.017 (at node 132)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.207573
	minimum = 0.09 (at node 25)
	maximum = 0.336 (at node 121)
Injected packet length average = 17.8192
Accepted packet length average = 20.5221
Total in-flight flits = 48364 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 593.026
	minimum = 23
	maximum = 1929
Network latency average = 468.941
	minimum = 23
	maximum = 1797
Slowest packet = 170
Flit latency average = 396.484
	minimum = 6
	maximum = 1858
Slowest flit = 8236
Fragmentation average = 177.225
	minimum = 0
	maximum = 1322
Injected packet rate average = 0.0268229
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.011263
	minimum = 0.007 (at node 101)
	maximum = 0.017 (at node 13)
Injected flit rate average = 0.480539
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.21738
	minimum = 0.1295 (at node 135)
	maximum = 0.322 (at node 13)
Injected packet length average = 17.9152
Accepted packet length average = 19.3003
Total in-flight flits = 101926 (0 measured)
latency change    = 0.397474
throughput change = 0.0451158
Class 0:
Packet latency average = 1243.92
	minimum = 25
	maximum = 2897
Network latency average = 1059.22
	minimum = 25
	maximum = 2797
Slowest packet = 3487
Flit latency average = 989.717
	minimum = 6
	maximum = 2780
Slowest flit = 9917
Fragmentation average = 216.248
	minimum = 0
	maximum = 1905
Injected packet rate average = 0.0294115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0130833
	minimum = 0.004 (at node 117)
	maximum = 0.024 (at node 90)
Injected flit rate average = 0.529307
	minimum = 0 (at node 8)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.234984
	minimum = 0.072 (at node 117)
	maximum = 0.411 (at node 155)
Injected packet length average = 17.9966
Accepted packet length average = 17.9606
Total in-flight flits = 158455 (0 measured)
latency change    = 0.523261
throughput change = 0.0749163
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 333.44
	minimum = 25
	maximum = 1432
Network latency average = 122.41
	minimum = 24
	maximum = 928
Slowest packet = 16006
Flit latency average = 1403.53
	minimum = 6
	maximum = 3721
Slowest flit = 21375
Fragmentation average = 37.6698
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0308802
	minimum = 0.001 (at node 58)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0130417
	minimum = 0.005 (at node 100)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.555734
	minimum = 0.018 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.233245
	minimum = 0.098 (at node 145)
	maximum = 0.395 (at node 129)
Injected packet length average = 17.9965
Accepted packet length average = 17.8846
Total in-flight flits = 220394 (96717 measured)
latency change    = 2.73057
throughput change = 0.00745819
Class 0:
Packet latency average = 563.545
	minimum = 25
	maximum = 2246
Network latency average = 364.717
	minimum = 23
	maximum = 1941
Slowest packet = 16006
Flit latency average = 1628.85
	minimum = 6
	maximum = 4482
Slowest flit = 40649
Fragmentation average = 75.621
	minimum = 0
	maximum = 1052
Injected packet rate average = 0.0296953
	minimum = 0.003 (at node 58)
	maximum = 0.0555 (at node 6)
Accepted packet rate average = 0.0130365
	minimum = 0.008 (at node 79)
	maximum = 0.0205 (at node 123)
Injected flit rate average = 0.53482
	minimum = 0.054 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.232888
	minimum = 0.125 (at node 110)
	maximum = 0.3805 (at node 123)
Injected packet length average = 18.0103
Accepted packet length average = 17.8644
Total in-flight flits = 274280 (179700 measured)
latency change    = 0.408316
throughput change = 0.00153194
Class 0:
Packet latency average = 886.934
	minimum = 23
	maximum = 3744
Network latency average = 690.447
	minimum = 23
	maximum = 2977
Slowest packet = 16006
Flit latency average = 1849.37
	minimum = 6
	maximum = 5470
Slowest flit = 42555
Fragmentation average = 95.6495
	minimum = 0
	maximum = 1052
Injected packet rate average = 0.0294514
	minimum = 0.00466667 (at node 58)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0129844
	minimum = 0.008 (at node 79)
	maximum = 0.0216667 (at node 123)
Injected flit rate average = 0.530122
	minimum = 0.084 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.232148
	minimum = 0.138 (at node 155)
	maximum = 0.388333 (at node 123)
Injected packet length average = 17.9999
Accepted packet length average = 17.879
Total in-flight flits = 330090 (258410 measured)
latency change    = 0.364615
throughput change = 0.00318957
Draining remaining packets ...
Class 0:
Remaining flits: 21492 21493 21494 21495 21496 21497 21498 21499 21500 21501 [...] (294606 flits)
Measured flits: 287046 287047 287048 287049 287050 287051 287052 287053 287054 287055 [...] (241275 flits)
Class 0:
Remaining flits: 45756 45757 45758 45759 45760 45761 45762 45763 45764 45765 [...] (259724 flits)
Measured flits: 287046 287047 287048 287049 287050 287051 287052 287053 287054 287055 [...] (221059 flits)
Class 0:
Remaining flits: 62222 62223 62224 62225 63972 63973 63974 63975 63976 63977 [...] (224878 flits)
Measured flits: 287046 287047 287048 287049 287050 287051 287052 287053 287054 287055 [...] (197162 flits)
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (190183 flits)
Measured flits: 287046 287047 287048 287049 287050 287051 287052 287053 287054 287055 [...] (170012 flits)
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (155844 flits)
Measured flits: 287226 287227 287228 287229 287230 287231 287232 287233 287234 287235 [...] (141619 flits)
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (122508 flits)
Measured flits: 287226 287227 287228 287229 287230 287231 287232 287233 287234 287235 [...] (112590 flits)
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (89362 flits)
Measured flits: 287243 287568 287569 287570 287571 287572 287573 287574 287575 287576 [...] (83175 flits)
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (59069 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (54914 flits)
Class 0:
Remaining flits: 63978 63979 63980 63981 63982 63983 63984 63985 63986 63987 [...] (34056 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (31643 flits)
Class 0:
Remaining flits: 63986 63987 63988 63989 120888 120889 120890 120891 120892 120893 [...] (15400 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (14176 flits)
Class 0:
Remaining flits: 63986 63987 63988 63989 120888 120889 120890 120891 120892 120893 [...] (6862 flits)
Measured flits: 289800 289801 289802 289803 289804 289805 289806 289807 289808 289809 [...] (6084 flits)
Class 0:
Remaining flits: 63986 63987 63988 63989 120888 120889 120890 120891 120892 120893 [...] (4242 flits)
Measured flits: 289800 289801 289802 289803 289804 289805 289806 289807 289808 289809 [...] (3728 flits)
Class 0:
Remaining flits: 120888 120889 120890 120891 120892 120893 120894 120895 120896 120897 [...] (2467 flits)
Measured flits: 290034 290035 290036 290037 290038 290039 290040 290041 290042 290043 [...] (2173 flits)
Class 0:
Remaining flits: 239004 239005 239006 239007 239008 239009 239010 239011 239012 239013 [...] (1115 flits)
Measured flits: 290034 290035 290036 290037 290038 290039 290040 290041 290042 290043 [...] (971 flits)
Class 0:
Remaining flits: 471996 471997 471998 471999 472000 472001 472002 472003 472004 472005 [...] (69 flits)
Measured flits: 471996 471997 471998 471999 472000 472001 472002 472003 472004 472005 [...] (69 flits)
Time taken is 21077 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6299 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18925 (1 samples)
Network latency average = 6049.23 (1 samples)
	minimum = 23 (1 samples)
	maximum = 17411 (1 samples)
Flit latency average = 5404.72 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18019 (1 samples)
Fragmentation average = 183.003 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5214 (1 samples)
Injected packet rate average = 0.0294514 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0129844 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.530122 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.232148 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 0.388333 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 17.879 (1 samples)
Hops average = 5.0514 (1 samples)
Total run time 16.4351
