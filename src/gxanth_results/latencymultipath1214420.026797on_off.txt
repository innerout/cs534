BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 316.525
	minimum = 22
	maximum = 962
Network latency average = 216.98
	minimum = 22
	maximum = 805
Slowest packet = 8
Flit latency average = 193.621
	minimum = 5
	maximum = 788
Slowest flit = 15043
Fragmentation average = 17.7246
	minimum = 0
	maximum = 86
Injected packet rate average = 0.0242552
	minimum = 0 (at node 122)
	maximum = 0.056 (at node 85)
Accepted packet rate average = 0.013599
	minimum = 0.004 (at node 64)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.432646
	minimum = 0 (at node 122)
	maximum = 1 (at node 85)
Accepted flit rate average= 0.249781
	minimum = 0.072 (at node 64)
	maximum = 0.432 (at node 152)
Injected packet length average = 17.8372
Accepted packet length average = 18.3677
Total in-flight flits = 35886 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.302
	minimum = 22
	maximum = 1918
Network latency average = 395.309
	minimum = 22
	maximum = 1417
Slowest packet = 8
Flit latency average = 371.561
	minimum = 5
	maximum = 1400
Slowest flit = 47231
Fragmentation average = 19.5426
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0246224
	minimum = 0.002 (at node 177)
	maximum = 0.056 (at node 91)
Accepted packet rate average = 0.0143411
	minimum = 0.006 (at node 79)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.441047
	minimum = 0.036 (at node 177)
	maximum = 0.9995 (at node 91)
Accepted flit rate average= 0.261109
	minimum = 0.116 (at node 79)
	maximum = 0.38 (at node 78)
Injected packet length average = 17.9124
Accepted packet length average = 18.207
Total in-flight flits = 70122 (0 measured)
latency change    = 0.399726
throughput change = 0.0433846
Class 0:
Packet latency average = 1098.58
	minimum = 22
	maximum = 2696
Network latency average = 924.658
	minimum = 22
	maximum = 2080
Slowest packet = 3483
Flit latency average = 906.336
	minimum = 5
	maximum = 2175
Slowest flit = 67226
Fragmentation average = 20.4524
	minimum = 0
	maximum = 137
Injected packet rate average = 0.023026
	minimum = 0 (at node 1)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0152188
	minimum = 0.005 (at node 11)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.415437
	minimum = 0 (at node 1)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.273229
	minimum = 0.09 (at node 11)
	maximum = 0.476 (at node 120)
Injected packet length average = 18.0421
Accepted packet length average = 17.9535
Total in-flight flits = 97690 (0 measured)
latency change    = 0.520015
throughput change = 0.0443576
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 485.579
	minimum = 27
	maximum = 2162
Network latency average = 187.309
	minimum = 22
	maximum = 964
Slowest packet = 13940
Flit latency average = 1277.95
	minimum = 5
	maximum = 3006
Slowest flit = 81360
Fragmentation average = 7.87018
	minimum = 0
	maximum = 55
Injected packet rate average = 0.0198177
	minimum = 0 (at node 78)
	maximum = 0.054 (at node 169)
Accepted packet rate average = 0.0150573
	minimum = 0.006 (at node 31)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.356661
	minimum = 0 (at node 78)
	maximum = 0.955 (at node 169)
Accepted flit rate average= 0.271224
	minimum = 0.108 (at node 31)
	maximum = 0.49 (at node 16)
Injected packet length average = 17.9971
Accepted packet length average = 18.0128
Total in-flight flits = 114537 (63589 measured)
latency change    = 1.26241
throughput change = 0.00739318
Class 0:
Packet latency average = 1100.29
	minimum = 23
	maximum = 3398
Network latency average = 816.361
	minimum = 22
	maximum = 1973
Slowest packet = 13940
Flit latency average = 1460.3
	minimum = 5
	maximum = 3875
Slowest flit = 91525
Fragmentation average = 15.6326
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0195807
	minimum = 0.003 (at node 94)
	maximum = 0.042 (at node 26)
Accepted packet rate average = 0.0150286
	minimum = 0.0085 (at node 2)
	maximum = 0.023 (at node 181)
Injected flit rate average = 0.352237
	minimum = 0.048 (at node 94)
	maximum = 0.7555 (at node 50)
Accepted flit rate average= 0.270427
	minimum = 0.153 (at node 2)
	maximum = 0.4215 (at node 181)
Injected packet length average = 17.989
Accepted packet length average = 17.9941
Total in-flight flits = 129602 (110932 measured)
latency change    = 0.558683
throughput change = 0.00294673
Class 0:
Packet latency average = 1666.58
	minimum = 23
	maximum = 4466
Network latency average = 1313.94
	minimum = 22
	maximum = 2936
Slowest packet = 13940
Flit latency average = 1623.92
	minimum = 5
	maximum = 4579
Slowest flit = 97848
Fragmentation average = 17.0948
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0184549
	minimum = 0.00433333 (at node 94)
	maximum = 0.0376667 (at node 93)
Accepted packet rate average = 0.0149323
	minimum = 0.00933333 (at node 64)
	maximum = 0.0216667 (at node 128)
Injected flit rate average = 0.332285
	minimum = 0.078 (at node 94)
	maximum = 0.678 (at node 93)
Accepted flit rate average= 0.26858
	minimum = 0.172333 (at node 64)
	maximum = 0.391333 (at node 128)
Injected packet length average = 18.0053
Accepted packet length average = 17.9865
Total in-flight flits = 135354 (130443 measured)
latency change    = 0.339789
throughput change = 0.00687774
Class 0:
Packet latency average = 2110.65
	minimum = 23
	maximum = 5395
Network latency average = 1657.15
	minimum = 22
	maximum = 3973
Slowest packet = 13940
Flit latency average = 1774.13
	minimum = 5
	maximum = 4914
Slowest flit = 102036
Fragmentation average = 18.3659
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0179935
	minimum = 0.006 (at node 94)
	maximum = 0.03175 (at node 93)
Accepted packet rate average = 0.0148906
	minimum = 0.0095 (at node 64)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.323758
	minimum = 0.108 (at node 94)
	maximum = 0.5715 (at node 93)
Accepted flit rate average= 0.268029
	minimum = 0.171 (at node 64)
	maximum = 0.378 (at node 128)
Injected packet length average = 17.9931
Accepted packet length average = 17.9998
Total in-flight flits = 141720 (141137 measured)
latency change    = 0.210395
throughput change = 0.00205655
Class 0:
Packet latency average = 2457.92
	minimum = 23
	maximum = 6348
Network latency average = 1919.55
	minimum = 22
	maximum = 4696
Slowest packet = 13940
Flit latency average = 1913.54
	minimum = 5
	maximum = 4954
Slowest flit = 187811
Fragmentation average = 18.5162
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0174688
	minimum = 0.0082 (at node 143)
	maximum = 0.0292 (at node 57)
Accepted packet rate average = 0.0148677
	minimum = 0.0108 (at node 104)
	maximum = 0.0194 (at node 128)
Injected flit rate average = 0.314474
	minimum = 0.1476 (at node 143)
	maximum = 0.5272 (at node 57)
Accepted flit rate average= 0.267548
	minimum = 0.195 (at node 104)
	maximum = 0.3506 (at node 128)
Injected packet length average = 18.0021
Accepted packet length average = 17.9952
Total in-flight flits = 143892 (143892 measured)
latency change    = 0.141283
throughput change = 0.0017968
Class 0:
Packet latency average = 2730.7
	minimum = 23
	maximum = 6912
Network latency average = 2083.28
	minimum = 22
	maximum = 5181
Slowest packet = 13940
Flit latency average = 2021.63
	minimum = 5
	maximum = 5164
Slowest flit = 288108
Fragmentation average = 18.8924
	minimum = 0
	maximum = 191
Injected packet rate average = 0.0171632
	minimum = 0.009 (at node 94)
	maximum = 0.0275 (at node 26)
Accepted packet rate average = 0.0148594
	minimum = 0.0113333 (at node 79)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.309075
	minimum = 0.162 (at node 94)
	maximum = 0.495167 (at node 26)
Accepted flit rate average= 0.267586
	minimum = 0.2025 (at node 79)
	maximum = 0.344667 (at node 128)
Injected packet length average = 18.008
Accepted packet length average = 18.0079
Total in-flight flits = 146839 (146839 measured)
latency change    = 0.0998958
throughput change = 0.000142088
Class 0:
Packet latency average = 2984.48
	minimum = 23
	maximum = 7684
Network latency average = 2202.67
	minimum = 22
	maximum = 5192
Slowest packet = 13940
Flit latency average = 2112.95
	minimum = 5
	maximum = 5175
Slowest flit = 369672
Fragmentation average = 19.164
	minimum = 0
	maximum = 191
Injected packet rate average = 0.0167545
	minimum = 0.00971429 (at node 41)
	maximum = 0.0274286 (at node 57)
Accepted packet rate average = 0.0148638
	minimum = 0.0115714 (at node 64)
	maximum = 0.0187143 (at node 70)
Injected flit rate average = 0.301596
	minimum = 0.174857 (at node 41)
	maximum = 0.494 (at node 57)
Accepted flit rate average= 0.267501
	minimum = 0.208571 (at node 64)
	maximum = 0.338571 (at node 128)
Injected packet length average = 18.0009
Accepted packet length average = 17.9968
Total in-flight flits = 144986 (144986 measured)
latency change    = 0.085032
throughput change = 0.000315697
Draining all recorded packets ...
Class 0:
Remaining flits: 382842 382843 382844 382845 382846 382847 382848 382849 382850 382851 [...] (149907 flits)
Measured flits: 382842 382843 382844 382845 382846 382847 382848 382849 382850 382851 [...] (143026 flits)
Class 0:
Remaining flits: 473490 473491 473492 473493 473494 473495 473496 473497 473498 473499 [...] (147374 flits)
Measured flits: 473490 473491 473492 473493 473494 473495 473496 473497 473498 473499 [...] (131543 flits)
Class 0:
Remaining flits: 474318 474319 474320 474321 474322 474323 474324 474325 474326 474327 [...] (149367 flits)
Measured flits: 474318 474319 474320 474321 474322 474323 474324 474325 474326 474327 [...] (121993 flits)
Class 0:
Remaining flits: 562122 562123 562124 562125 562126 562127 562128 562129 562130 562131 [...] (146264 flits)
Measured flits: 562122 562123 562124 562125 562126 562127 562128 562129 562130 562131 [...] (107840 flits)
Class 0:
Remaining flits: 576774 576775 576776 576777 576778 576779 576780 576781 576782 576783 [...] (147378 flits)
Measured flits: 576774 576775 576776 576777 576778 576779 576780 576781 576782 576783 [...] (94472 flits)
Class 0:
Remaining flits: 618498 618499 618500 618501 618502 618503 618504 618505 618506 618507 [...] (144731 flits)
Measured flits: 618498 618499 618500 618501 618502 618503 618504 618505 618506 618507 [...] (76812 flits)
Class 0:
Remaining flits: 638608 638609 638610 638611 638612 638613 638614 638615 638616 638617 [...] (145478 flits)
Measured flits: 638608 638609 638610 638611 638612 638613 638614 638615 638616 638617 [...] (62130 flits)
Class 0:
Remaining flits: 693162 693163 693164 693165 693166 693167 693168 693169 693170 693171 [...] (144859 flits)
Measured flits: 693162 693163 693164 693165 693166 693167 693168 693169 693170 693171 [...] (48842 flits)
Class 0:
Remaining flits: 693162 693163 693164 693165 693166 693167 693168 693169 693170 693171 [...] (143621 flits)
Measured flits: 693162 693163 693164 693165 693166 693167 693168 693169 693170 693171 [...] (38765 flits)
Class 0:
Remaining flits: 793602 793603 793604 793605 793606 793607 793608 793609 793610 793611 [...] (144581 flits)
Measured flits: 793602 793603 793604 793605 793606 793607 793608 793609 793610 793611 [...] (30315 flits)
Class 0:
Remaining flits: 860868 860869 860870 860871 860872 860873 860874 860875 860876 860877 [...] (143977 flits)
Measured flits: 860868 860869 860870 860871 860872 860873 860874 860875 860876 860877 [...] (22669 flits)
Class 0:
Remaining flits: 862794 862795 862796 862797 862798 862799 862800 862801 862802 862803 [...] (144821 flits)
Measured flits: 862794 862795 862796 862797 862798 862799 862800 862801 862802 862803 [...] (16430 flits)
Class 0:
Remaining flits: 923670 923671 923672 923673 923674 923675 923676 923677 923678 923679 [...] (144215 flits)
Measured flits: 923670 923671 923672 923673 923674 923675 923676 923677 923678 923679 [...] (11455 flits)
Class 0:
Remaining flits: 982836 982837 982838 982839 982840 982841 982842 982843 982844 982845 [...] (145633 flits)
Measured flits: 982836 982837 982838 982839 982840 982841 982842 982843 982844 982845 [...] (7532 flits)
Class 0:
Remaining flits: 1033866 1033867 1033868 1033869 1033870 1033871 1033872 1033873 1033874 1033875 [...] (145913 flits)
Measured flits: 1101593 1101594 1101595 1101596 1101597 1101598 1101599 1152846 1152847 1152848 [...] (4272 flits)
Class 0:
Remaining flits: 1111914 1111915 1111916 1111917 1111918 1111919 1111920 1111921 1111922 1111923 [...] (147124 flits)
Measured flits: 1166994 1166995 1166996 1166997 1166998 1166999 1167000 1167001 1167002 1167003 [...] (2485 flits)
Class 0:
Remaining flits: 1190700 1190701 1190702 1190703 1190704 1190705 1190706 1190707 1190708 1190709 [...] (144815 flits)
Measured flits: 1244736 1244737 1244738 1244739 1244740 1244741 1244742 1244743 1244744 1244745 [...] (1597 flits)
Class 0:
Remaining flits: 1257239 1257240 1257241 1257242 1257243 1257244 1257245 1271430 1271431 1271432 [...] (145715 flits)
Measured flits: 1276164 1276165 1276166 1276167 1276168 1276169 1276170 1276171 1276172 1276173 [...] (828 flits)
Class 0:
Remaining flits: 1292004 1292005 1292006 1292007 1292008 1292009 1292010 1292011 1292012 1292013 [...] (143415 flits)
Measured flits: 1465758 1465759 1465760 1465761 1465762 1465763 1465764 1465765 1465766 1465767 [...] (378 flits)
Class 0:
Remaining flits: 1335672 1335673 1335674 1335675 1335676 1335677 1335678 1335679 1335680 1335681 [...] (142390 flits)
Measured flits: 1469376 1469377 1469378 1469379 1469380 1469381 1469382 1469383 1469384 1469385 [...] (360 flits)
Class 0:
Remaining flits: 1392930 1392931 1392932 1392933 1392934 1392935 1392936 1392937 1392938 1392939 [...] (144107 flits)
Measured flits: 1469376 1469377 1469378 1469379 1469380 1469381 1469382 1469383 1469384 1469385 [...] (324 flits)
Class 0:
Remaining flits: 1425636 1425637 1425638 1425639 1425640 1425641 1425642 1425643 1425644 1425645 [...] (143738 flits)
Measured flits: 1505322 1505323 1505324 1505325 1505326 1505327 1505328 1505329 1505330 1505331 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1485846 1485847 1485848 1485849 1485850 1485851 1485852 1485853 1485854 1485855 [...] (93421 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1500858 1500859 1500860 1500861 1500862 1500863 1500864 1500865 1500866 1500867 [...] (45125 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1610406 1610407 1610408 1610409 1610410 1610411 1610412 1610413 1610414 1610415 [...] (5956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1668870 1668871 1668872 1668873 1668874 1668875 1668876 1668877 1668878 1668879 [...] (40 flits)
Measured flits: (0 flits)
Time taken is 36231 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5660.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22894 (1 samples)
Network latency average = 2696.36 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8433 (1 samples)
Flit latency average = 2665.24 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8416 (1 samples)
Fragmentation average = 19.9764 (1 samples)
	minimum = 0 (1 samples)
	maximum = 260 (1 samples)
Injected packet rate average = 0.0167545 (1 samples)
	minimum = 0.00971429 (1 samples)
	maximum = 0.0274286 (1 samples)
Accepted packet rate average = 0.0148638 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.301596 (1 samples)
	minimum = 0.174857 (1 samples)
	maximum = 0.494 (1 samples)
Accepted flit rate average = 0.267501 (1 samples)
	minimum = 0.208571 (1 samples)
	maximum = 0.338571 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 17.9968 (1 samples)
Hops average = 5.07851 (1 samples)
Total run time 36.4563
