BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.667
	minimum = 22
	maximum = 868
Network latency average = 296.615
	minimum = 22
	maximum = 815
Slowest packet = 433
Flit latency average = 273.907
	minimum = 5
	maximum = 798
Slowest flit = 25918
Fragmentation average = 25.5321
	minimum = 0
	maximum = 95
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0158073
	minimum = 0.006 (at node 174)
	maximum = 0.025 (at node 152)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.291469
	minimum = 0.117 (at node 174)
	maximum = 0.454 (at node 152)
Injected packet length average = 17.8433
Accepted packet length average = 18.4389
Total in-flight flits = 107874 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 599.306
	minimum = 22
	maximum = 1686
Network latency average = 562.577
	minimum = 22
	maximum = 1542
Slowest packet = 433
Flit latency average = 539.639
	minimum = 5
	maximum = 1582
Slowest flit = 64499
Fragmentation average = 25.4333
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0490573
	minimum = 0.039 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.016763
	minimum = 0.0105 (at node 83)
	maximum = 0.025 (at node 177)
Injected flit rate average = 0.878964
	minimum = 0.698 (at node 43)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.305115
	minimum = 0.189 (at node 83)
	maximum = 0.453 (at node 177)
Injected packet length average = 17.9171
Accepted packet length average = 18.2016
Total in-flight flits = 221920 (0 measured)
latency change    = 0.463267
throughput change = 0.0447236
Class 0:
Packet latency average = 1397.79
	minimum = 22
	maximum = 2395
Network latency average = 1338.94
	minimum = 22
	maximum = 2332
Slowest packet = 5080
Flit latency average = 1321.07
	minimum = 5
	maximum = 2315
Slowest flit = 103823
Fragmentation average = 24.5894
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0501719
	minimum = 0.038 (at node 166)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0176719
	minimum = 0.005 (at node 190)
	maximum = 0.032 (at node 187)
Injected flit rate average = 0.90325
	minimum = 0.684 (at node 166)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.318323
	minimum = 0.087 (at node 190)
	maximum = 0.582 (at node 187)
Injected packet length average = 18.0031
Accepted packet length average = 18.013
Total in-flight flits = 334196 (0 measured)
latency change    = 0.571247
throughput change = 0.0414935
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 202.795
	minimum = 25
	maximum = 1124
Network latency average = 96.3503
	minimum = 22
	maximum = 795
Slowest packet = 28522
Flit latency average = 1978.85
	minimum = 5
	maximum = 3056
Slowest flit = 148122
Fragmentation average = 5.7303
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0474375
	minimum = 0.027 (at node 60)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.016901
	minimum = 0.009 (at node 5)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.853948
	minimum = 0.498 (at node 60)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.303245
	minimum = 0.162 (at node 5)
	maximum = 0.558 (at node 129)
Injected packet length average = 18.0015
Accepted packet length average = 17.9424
Total in-flight flits = 439953 (153605 measured)
latency change    = 5.89262
throughput change = 0.0497226
Class 0:
Packet latency average = 337.93
	minimum = 22
	maximum = 1439
Network latency average = 180.584
	minimum = 22
	maximum = 1264
Slowest packet = 28522
Flit latency average = 2317.42
	minimum = 5
	maximum = 3808
Slowest flit = 190871
Fragmentation average = 5.58263
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0467708
	minimum = 0.0285 (at node 52)
	maximum = 0.056 (at node 174)
Accepted packet rate average = 0.0165026
	minimum = 0.0095 (at node 86)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.841807
	minimum = 0.512 (at node 52)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.297365
	minimum = 0.1785 (at node 86)
	maximum = 0.4605 (at node 90)
Injected packet length average = 17.9986
Accepted packet length average = 18.0193
Total in-flight flits = 543288 (303880 measured)
latency change    = 0.39989
throughput change = 0.0197744
Class 0:
Packet latency average = 503.019
	minimum = 22
	maximum = 1797
Network latency average = 294.621
	minimum = 22
	maximum = 1581
Slowest packet = 28522
Flit latency average = 2633.34
	minimum = 5
	maximum = 4572
Slowest flit = 231929
Fragmentation average = 5.61649
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0462969
	minimum = 0.0283333 (at node 116)
	maximum = 0.0556667 (at node 29)
Accepted packet rate average = 0.0164132
	minimum = 0.011 (at node 36)
	maximum = 0.0236667 (at node 129)
Injected flit rate average = 0.833335
	minimum = 0.510333 (at node 116)
	maximum = 1 (at node 13)
Accepted flit rate average= 0.295493
	minimum = 0.196667 (at node 36)
	maximum = 0.430333 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 18.0034
Total in-flight flits = 643998 (451109 measured)
latency change    = 0.328197
throughput change = 0.00633358
Class 0:
Packet latency average = 661.54
	minimum = 22
	maximum = 2354
Network latency average = 407.524
	minimum = 22
	maximum = 2093
Slowest packet = 28522
Flit latency average = 2952.16
	minimum = 5
	maximum = 5314
Slowest flit = 269891
Fragmentation average = 5.58768
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0461602
	minimum = 0.02825 (at node 116)
	maximum = 0.05575 (at node 174)
Accepted packet rate average = 0.0164115
	minimum = 0.012 (at node 169)
	maximum = 0.02225 (at node 129)
Injected flit rate average = 0.831051
	minimum = 0.5085 (at node 116)
	maximum = 1 (at node 13)
Accepted flit rate average= 0.295488
	minimum = 0.21825 (at node 169)
	maximum = 0.40375 (at node 129)
Injected packet length average = 18.0036
Accepted packet length average = 18.005
Total in-flight flits = 745379 (599668 measured)
latency change    = 0.239623
throughput change = 1.61573e-05
Class 0:
Packet latency average = 830.085
	minimum = 22
	maximum = 3110
Network latency average = 537.28
	minimum = 22
	maximum = 2553
Slowest packet = 28522
Flit latency average = 3261.81
	minimum = 5
	maximum = 6002
Slowest flit = 327327
Fragmentation average = 5.63653
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0462156
	minimum = 0.0284 (at node 52)
	maximum = 0.0556 (at node 135)
Accepted packet rate average = 0.0164656
	minimum = 0.012 (at node 86)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.832018
	minimum = 0.5136 (at node 52)
	maximum = 1 (at node 13)
Accepted flit rate average= 0.296336
	minimum = 0.2176 (at node 144)
	maximum = 0.378 (at node 129)
Injected packet length average = 18.003
Accepted packet length average = 17.9973
Total in-flight flits = 848373 (749767 measured)
latency change    = 0.203045
throughput change = 0.00286221
Class 0:
Packet latency average = 1067.64
	minimum = 22
	maximum = 6018
Network latency average = 731.331
	minimum = 22
	maximum = 5938
Slowest packet = 28814
Flit latency average = 3577.43
	minimum = 5
	maximum = 6846
Slowest flit = 362894
Fragmentation average = 6.20262
	minimum = 0
	maximum = 76
Injected packet rate average = 0.0461484
	minimum = 0.0278333 (at node 52)
	maximum = 0.0555 (at node 9)
Accepted packet rate average = 0.0164401
	minimum = 0.0125 (at node 86)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.830694
	minimum = 0.5005 (at node 52)
	maximum = 1 (at node 13)
Accepted flit rate average= 0.295955
	minimum = 0.226333 (at node 86)
	maximum = 0.378 (at node 129)
Injected packet length average = 18.0005
Accepted packet length average = 18.002
Total in-flight flits = 950190 (897573 measured)
latency change    = 0.222508
throughput change = 0.00128938
Class 0:
Packet latency average = 1843.29
	minimum = 22
	maximum = 7267
Network latency average = 1517.08
	minimum = 22
	maximum = 6978
Slowest packet = 28601
Flit latency average = 3903.18
	minimum = 5
	maximum = 7586
Slowest flit = 394595
Fragmentation average = 9.35125
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0452396
	minimum = 0.0251429 (at node 52)
	maximum = 0.0552857 (at node 13)
Accepted packet rate average = 0.0163631
	minimum = 0.0131429 (at node 63)
	maximum = 0.0208571 (at node 103)
Injected flit rate average = 0.8146
	minimum = 0.454 (at node 52)
	maximum = 0.996714 (at node 13)
Accepted flit rate average= 0.294474
	minimum = 0.235857 (at node 79)
	maximum = 0.375143 (at node 103)
Injected packet length average = 18.0063
Accepted packet length average = 17.9962
Total in-flight flits = 1032859 (1017403 measured)
latency change    = 0.420793
throughput change = 0.00502898
Draining all recorded packets ...
Class 0:
Remaining flits: 455570 455571 455572 455573 455574 455575 455576 455577 455578 455579 [...] (1069026 flits)
Measured flits: 512478 512479 512480 512481 512482 512483 512484 512485 512486 512487 [...] (1008020 flits)
Class 0:
Remaining flits: 499284 499285 499286 499287 499288 499289 499290 499291 499292 499293 [...] (1070902 flits)
Measured flits: 514638 514639 514640 514641 514642 514643 514644 514645 514646 514647 [...] (974548 flits)
Class 0:
Remaining flits: 540234 540235 540236 540237 540238 540239 540240 540241 540242 540243 [...] (1069098 flits)
Measured flits: 540234 540235 540236 540237 540238 540239 540240 540241 540242 540243 [...] (938544 flits)
Class 0:
Remaining flits: 589842 589843 589844 589845 589846 589847 589848 589849 589850 589851 [...] (1067274 flits)
Measured flits: 589842 589843 589844 589845 589846 589847 589848 589849 589850 589851 [...] (900774 flits)
Class 0:
Remaining flits: 619196 619197 619198 619199 628056 628057 628058 628059 628060 628061 [...] (1064778 flits)
Measured flits: 619196 619197 619198 619199 628056 628057 628058 628059 628060 628061 [...] (860785 flits)
Class 0:
Remaining flits: 653328 653329 653330 653331 653332 653333 653334 653335 653336 653337 [...] (1063719 flits)
Measured flits: 653328 653329 653330 653331 653332 653333 653334 653335 653336 653337 [...] (821367 flits)
Class 0:
Remaining flits: 723294 723295 723296 723297 723298 723299 723300 723301 723302 723303 [...] (1063135 flits)
Measured flits: 723294 723295 723296 723297 723298 723299 723300 723301 723302 723303 [...] (783490 flits)
Class 0:
Remaining flits: 766620 766621 766622 766623 766624 766625 766626 766627 766628 766629 [...] (1066738 flits)
Measured flits: 766620 766621 766622 766623 766624 766625 766626 766627 766628 766629 [...] (744376 flits)
Class 0:
Remaining flits: 810054 810055 810056 810057 810058 810059 810060 810061 810062 810063 [...] (1066392 flits)
Measured flits: 810054 810055 810056 810057 810058 810059 810060 810061 810062 810063 [...] (706672 flits)
Class 0:
Remaining flits: 851832 851833 851834 851835 851836 851837 851838 851839 851840 851841 [...] (1062292 flits)
Measured flits: 851832 851833 851834 851835 851836 851837 851838 851839 851840 851841 [...] (666110 flits)
Class 0:
Remaining flits: 869346 869347 869348 869349 869350 869351 869352 869353 869354 869355 [...] (1062651 flits)
Measured flits: 869346 869347 869348 869349 869350 869351 869352 869353 869354 869355 [...] (624653 flits)
Class 0:
Remaining flits: 872028 872029 872030 872031 872032 872033 872034 872035 872036 872037 [...] (1065840 flits)
Measured flits: 872028 872029 872030 872031 872032 872033 872034 872035 872036 872037 [...] (583143 flits)
Class 0:
Remaining flits: 931374 931375 931376 931377 931378 931379 931380 931381 931382 931383 [...] (1064822 flits)
Measured flits: 931374 931375 931376 931377 931378 931379 931380 931381 931382 931383 [...] (538624 flits)
Class 0:
Remaining flits: 951134 951135 951136 951137 981630 981631 981632 981633 981634 981635 [...] (1069089 flits)
Measured flits: 951134 951135 951136 951137 981630 981631 981632 981633 981634 981635 [...] (492910 flits)
Class 0:
Remaining flits: 1011654 1011655 1011656 1011657 1011658 1011659 1011660 1011661 1011662 1011663 [...] (1071032 flits)
Measured flits: 1011654 1011655 1011656 1011657 1011658 1011659 1011660 1011661 1011662 1011663 [...] (446364 flits)
Class 0:
Remaining flits: 1070424 1070425 1070426 1070427 1070428 1070429 1070430 1070431 1070432 1070433 [...] (1073197 flits)
Measured flits: 1070424 1070425 1070426 1070427 1070428 1070429 1070430 1070431 1070432 1070433 [...] (399701 flits)
Class 0:
Remaining flits: 1107864 1107865 1107866 1107867 1107868 1107869 1107870 1107871 1107872 1107873 [...] (1072068 flits)
Measured flits: 1107864 1107865 1107866 1107867 1107868 1107869 1107870 1107871 1107872 1107873 [...] (352218 flits)
Class 0:
Remaining flits: 1154745 1154746 1154747 1154748 1154749 1154750 1154751 1154752 1154753 1158696 [...] (1069811 flits)
Measured flits: 1154745 1154746 1154747 1154748 1154749 1154750 1154751 1154752 1154753 1158696 [...] (305299 flits)
Class 0:
Remaining flits: 1185390 1185391 1185392 1185393 1185394 1185395 1185396 1185397 1185398 1185399 [...] (1070741 flits)
Measured flits: 1185390 1185391 1185392 1185393 1185394 1185395 1185396 1185397 1185398 1185399 [...] (258627 flits)
Class 0:
Remaining flits: 1228932 1228933 1228934 1228935 1228936 1228937 1228938 1228939 1228940 1228941 [...] (1070933 flits)
Measured flits: 1228932 1228933 1228934 1228935 1228936 1228937 1228938 1228939 1228940 1228941 [...] (212560 flits)
Class 0:
Remaining flits: 1270368 1270369 1270370 1270371 1270372 1270373 1270374 1270375 1270376 1270377 [...] (1069128 flits)
Measured flits: 1270368 1270369 1270370 1270371 1270372 1270373 1270374 1270375 1270376 1270377 [...] (168370 flits)
Class 0:
Remaining flits: 1270368 1270369 1270370 1270371 1270372 1270373 1270374 1270375 1270376 1270377 [...] (1065538 flits)
Measured flits: 1270368 1270369 1270370 1270371 1270372 1270373 1270374 1270375 1270376 1270377 [...] (127580 flits)
Class 0:
Remaining flits: 1339596 1339597 1339598 1339599 1339600 1339601 1339602 1339603 1339604 1339605 [...] (1066422 flits)
Measured flits: 1339596 1339597 1339598 1339599 1339600 1339601 1339602 1339603 1339604 1339605 [...] (91237 flits)
Class 0:
Remaining flits: 1380690 1380691 1380692 1380693 1380694 1380695 1380696 1380697 1380698 1380699 [...] (1066915 flits)
Measured flits: 1380690 1380691 1380692 1380693 1380694 1380695 1380696 1380697 1380698 1380699 [...] (61609 flits)
Class 0:
Remaining flits: 1400400 1400401 1400402 1400403 1400404 1400405 1400406 1400407 1400408 1400409 [...] (1063055 flits)
Measured flits: 1400400 1400401 1400402 1400403 1400404 1400405 1400406 1400407 1400408 1400409 [...] (37774 flits)
Class 0:
Remaining flits: 1465290 1465291 1465292 1465293 1465294 1465295 1465296 1465297 1465298 1465299 [...] (1068487 flits)
Measured flits: 1465290 1465291 1465292 1465293 1465294 1465295 1465296 1465297 1465298 1465299 [...] (21990 flits)
Class 0:
Remaining flits: 1506384 1506385 1506386 1506387 1506388 1506389 1506390 1506391 1506392 1506393 [...] (1072097 flits)
Measured flits: 1506384 1506385 1506386 1506387 1506388 1506389 1506390 1506391 1506392 1506393 [...] (10836 flits)
Class 0:
Remaining flits: 1522224 1522225 1522226 1522227 1522228 1522229 1522230 1522231 1522232 1522233 [...] (1074855 flits)
Measured flits: 1522224 1522225 1522226 1522227 1522228 1522229 1522230 1522231 1522232 1522233 [...] (5065 flits)
Class 0:
Remaining flits: 1571652 1571653 1571654 1571655 1571656 1571657 1571658 1571659 1571660 1571661 [...] (1073615 flits)
Measured flits: 1571652 1571653 1571654 1571655 1571656 1571657 1571658 1571659 1571660 1571661 [...] (2426 flits)
Class 0:
Remaining flits: 1605510 1605511 1605512 1605513 1605514 1605515 1605516 1605517 1605518 1605519 [...] (1070222 flits)
Measured flits: 1605510 1605511 1605512 1605513 1605514 1605515 1605516 1605517 1605518 1605519 [...] (1105 flits)
Class 0:
Remaining flits: 1639764 1639765 1639766 1639767 1639768 1639769 1639770 1639771 1639772 1639773 [...] (1066703 flits)
Measured flits: 1784448 1784449 1784450 1784451 1784452 1784453 1784454 1784455 1784456 1784457 [...] (347 flits)
Class 0:
Remaining flits: 1659330 1659331 1659332 1659333 1659334 1659335 1659336 1659337 1659338 1659339 [...] (1068011 flits)
Measured flits: 2373318 2373319 2373320 2373321 2373322 2373323 2373324 2373325 2373326 2373327 [...] (90 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1714644 1714645 1714646 1714647 1714648 1714649 1714650 1714651 1714652 1714653 [...] (1017279 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1790946 1790947 1790948 1790949 1790950 1790951 1790952 1790953 1790954 1790955 [...] (966801 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1821978 1821979 1821980 1821981 1821982 1821983 1821984 1821985 1821986 1821987 [...] (917188 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1918908 1918909 1918910 1918911 1918912 1918913 1918914 1918915 1918916 1918917 [...] (866944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1951380 1951381 1951382 1951383 1951384 1951385 1951386 1951387 1951388 1951389 [...] (816375 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2009646 2009647 2009648 2009649 2009650 2009651 2009652 2009653 2009654 2009655 [...] (765511 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2056824 2056825 2056826 2056827 2056828 2056829 2056830 2056831 2056832 2056833 [...] (714299 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2059974 2059975 2059976 2059977 2059978 2059979 2059980 2059981 2059982 2059983 [...] (663704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2162880 2162881 2162882 2162883 2162884 2162885 2162886 2162887 2162888 2162889 [...] (614354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2164716 2164717 2164718 2164719 2164720 2164721 2164722 2164723 2164724 2164725 [...] (565406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2302630 2302631 2315466 2315467 2315468 2315469 2315470 2315471 2315472 2315473 [...] (516985 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2359356 2359357 2359358 2359359 2359360 2359361 2359362 2359363 2359364 2359365 [...] (468763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2399454 2399455 2399456 2399457 2399458 2399459 2399460 2399461 2399462 2399463 [...] (421056 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2442690 2442691 2442692 2442693 2442694 2442695 2442696 2442697 2442698 2442699 [...] (373537 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2520772 2520773 2523105 2523106 2523107 2523108 2523109 2523110 2523111 2523112 [...] (325389 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2545650 2545651 2545652 2545653 2545654 2545655 2545656 2545657 2545658 2545659 [...] (277517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2575962 2575963 2575964 2575965 2575966 2575967 2575968 2575969 2575970 2575971 [...] (229680 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2630934 2630935 2630936 2630937 2630938 2630939 2630940 2630941 2630942 2630943 [...] (181992 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2788902 2788903 2788904 2788905 2788906 2788907 2788908 2788909 2788910 2788911 [...] (134011 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2805750 2805751 2805752 2805753 2805754 2805755 2805756 2805757 2805758 2805759 [...] (86826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836206 2836207 2836208 2836209 2836210 2836211 2836212 2836213 2836214 2836215 [...] (40289 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2994858 2994859 2994860 2994861 2994862 2994863 2994864 2994865 2994866 2994867 [...] (7438 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3155400 3155401 3155402 3155403 3155404 3155405 3155406 3155407 3155408 3155409 [...] (302 flits)
Measured flits: (0 flits)
Time taken is 66009 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14933.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 32705 (1 samples)
Network latency average = 13836.5 (1 samples)
	minimum = 22 (1 samples)
	maximum = 30176 (1 samples)
Flit latency average = 16357.3 (1 samples)
	minimum = 5 (1 samples)
	maximum = 32964 (1 samples)
Fragmentation average = 27.9518 (1 samples)
	minimum = 0 (1 samples)
	maximum = 938 (1 samples)
Injected packet rate average = 0.0452396 (1 samples)
	minimum = 0.0251429 (1 samples)
	maximum = 0.0552857 (1 samples)
Accepted packet rate average = 0.0163631 (1 samples)
	minimum = 0.0131429 (1 samples)
	maximum = 0.0208571 (1 samples)
Injected flit rate average = 0.8146 (1 samples)
	minimum = 0.454 (1 samples)
	maximum = 0.996714 (1 samples)
Accepted flit rate average = 0.294474 (1 samples)
	minimum = 0.235857 (1 samples)
	maximum = 0.375143 (1 samples)
Injected packet size average = 18.0063 (1 samples)
Accepted packet size average = 17.9962 (1 samples)
Hops average = 5.07051 (1 samples)
Total run time 65.4453
