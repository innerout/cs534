BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 396.642
	minimum = 27
	maximum = 938
Network latency average = 365.26
	minimum = 23
	maximum = 935
Slowest packet = 278
Flit latency average = 267.98
	minimum = 6
	maximum = 976
Slowest flit = 1790
Fragmentation average = 255.804
	minimum = 0
	maximum = 777
Injected packet rate average = 0.030474
	minimum = 0.012 (at node 156)
	maximum = 0.04 (at node 46)
Accepted packet rate average = 0.01
	minimum = 0.003 (at node 25)
	maximum = 0.019 (at node 114)
Injected flit rate average = 0.543151
	minimum = 0.203 (at node 156)
	maximum = 0.714 (at node 135)
Accepted flit rate average= 0.225661
	minimum = 0.083 (at node 25)
	maximum = 0.392 (at node 152)
Injected packet length average = 17.8234
Accepted packet length average = 22.5661
Total in-flight flits = 62027 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 772.46
	minimum = 27
	maximum = 1918
Network latency average = 659.486
	minimum = 23
	maximum = 1885
Slowest packet = 801
Flit latency average = 529.017
	minimum = 6
	maximum = 1908
Slowest flit = 9090
Fragmentation average = 316.833
	minimum = 0
	maximum = 1471
Injected packet rate average = 0.0220964
	minimum = 0.006 (at node 156)
	maximum = 0.03 (at node 159)
Accepted packet rate average = 0.0112161
	minimum = 0.006 (at node 25)
	maximum = 0.0165 (at node 152)
Injected flit rate average = 0.39432
	minimum = 0.102 (at node 156)
	maximum = 0.5335 (at node 159)
Accepted flit rate average= 0.22557
	minimum = 0.1305 (at node 164)
	maximum = 0.3285 (at node 63)
Injected packet length average = 17.8455
Accepted packet length average = 20.1112
Total in-flight flits = 66309 (0 measured)
latency change    = 0.486521
throughput change = 0.000404068
Class 0:
Packet latency average = 1748.71
	minimum = 327
	maximum = 2888
Network latency average = 1220.86
	minimum = 25
	maximum = 2851
Slowest packet = 1078
Flit latency average = 1081.64
	minimum = 6
	maximum = 2899
Slowest flit = 10379
Fragmentation average = 341.82
	minimum = 2
	maximum = 2476
Injected packet rate average = 0.00961979
	minimum = 0 (at node 0)
	maximum = 0.036 (at node 35)
Accepted packet rate average = 0.012526
	minimum = 0.004 (at node 10)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.171937
	minimum = 0 (at node 0)
	maximum = 0.644 (at node 35)
Accepted flit rate average= 0.220161
	minimum = 0.095 (at node 10)
	maximum = 0.453 (at node 16)
Injected packet length average = 17.8733
Accepted packet length average = 17.5763
Total in-flight flits = 57212 (0 measured)
latency change    = 0.558268
throughput change = 0.0245677
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2387.73
	minimum = 1313
	maximum = 3473
Network latency average = 333.617
	minimum = 29
	maximum = 930
Slowest packet = 10352
Flit latency average = 1151.43
	minimum = 6
	maximum = 3899
Slowest flit = 10645
Fragmentation average = 145.383
	minimum = 2
	maximum = 639
Injected packet rate average = 0.00992188
	minimum = 0 (at node 0)
	maximum = 0.041 (at node 43)
Accepted packet rate average = 0.0119115
	minimum = 0.004 (at node 79)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.178932
	minimum = 0 (at node 0)
	maximum = 0.748 (at node 43)
Accepted flit rate average= 0.211146
	minimum = 0.099 (at node 134)
	maximum = 0.422 (at node 129)
Injected packet length average = 18.0341
Accepted packet length average = 17.7263
Total in-flight flits = 51016 (22679 measured)
latency change    = 0.267626
throughput change = 0.0426986
Class 0:
Packet latency average = 2880.07
	minimum = 1313
	maximum = 4537
Network latency average = 537.157
	minimum = 25
	maximum = 1945
Slowest packet = 10352
Flit latency average = 1163.8
	minimum = 6
	maximum = 4841
Slowest flit = 10655
Fragmentation average = 182.683
	minimum = 2
	maximum = 1461
Injected packet rate average = 0.0102422
	minimum = 0 (at node 18)
	maximum = 0.0325 (at node 150)
Accepted packet rate average = 0.0117396
	minimum = 0.006 (at node 105)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.184596
	minimum = 0 (at node 18)
	maximum = 0.582 (at node 150)
Accepted flit rate average= 0.20831
	minimum = 0.1125 (at node 105)
	maximum = 0.333 (at node 129)
Injected packet length average = 18.0231
Accepted packet length average = 17.7442
Total in-flight flits = 48087 (32226 measured)
latency change    = 0.17095
throughput change = 0.013614
Class 0:
Packet latency average = 3323.97
	minimum = 1313
	maximum = 5470
Network latency average = 659.263
	minimum = 25
	maximum = 2929
Slowest packet = 10352
Flit latency average = 1133.91
	minimum = 6
	maximum = 5867
Slowest flit = 13463
Fragmentation average = 205.319
	minimum = 1
	maximum = 2224
Injected packet rate average = 0.010875
	minimum = 0 (at node 34)
	maximum = 0.028 (at node 43)
Accepted packet rate average = 0.0116858
	minimum = 0.00666667 (at node 116)
	maximum = 0.018 (at node 177)
Injected flit rate average = 0.19592
	minimum = 0 (at node 92)
	maximum = 0.503667 (at node 43)
Accepted flit rate average= 0.208613
	minimum = 0.126667 (at node 116)
	maximum = 0.331 (at node 177)
Injected packet length average = 18.0156
Accepted packet length average = 17.8519
Total in-flight flits = 49803 (39362 measured)
latency change    = 0.133545
throughput change = 0.00145222
Draining remaining packets ...
Class 0:
Remaining flits: 6202 6203 6204 6205 6206 6207 6208 6209 9579 9580 [...] (15812 flits)
Measured flits: 186444 186445 186446 186447 186448 186449 186450 186451 186452 186453 [...] (11895 flits)
Class 0:
Remaining flits: 78124 78125 78126 78127 78128 78129 78130 78131 78132 78133 [...] (393 flits)
Measured flits: 200592 200593 200594 200595 200596 200597 200598 200599 200600 200601 [...] (352 flits)
Time taken is 8347 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3951.58 (1 samples)
	minimum = 1313 (1 samples)
	maximum = 7073 (1 samples)
Network latency average = 1061.51 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4884 (1 samples)
Flit latency average = 1500.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7640 (1 samples)
Fragmentation average = 220.178 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3458 (1 samples)
Injected packet rate average = 0.010875 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.028 (1 samples)
Accepted packet rate average = 0.0116858 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.19592 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.503667 (1 samples)
Accepted flit rate average = 0.208613 (1 samples)
	minimum = 0.126667 (1 samples)
	maximum = 0.331 (1 samples)
Injected packet size average = 18.0156 (1 samples)
Accepted packet size average = 17.8519 (1 samples)
Hops average = 5.12261 (1 samples)
Total run time 11.6985
