BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.7
	minimum = 23
	maximum = 914
Network latency average = 352.422
	minimum = 23
	maximum = 913
Slowest packet = 346
Flit latency average = 256.649
	minimum = 6
	maximum = 955
Slowest flit = 2930
Fragmentation average = 244.022
	minimum = 0
	maximum = 730
Injected packet rate average = 0.0294479
	minimum = 0.014 (at node 124)
	maximum = 0.041 (at node 62)
Accepted packet rate average = 0.00981771
	minimum = 0.003 (at node 77)
	maximum = 0.019 (at node 51)
Injected flit rate average = 0.524323
	minimum = 0.244 (at node 124)
	maximum = 0.733 (at node 62)
Accepted flit rate average= 0.222677
	minimum = 0.097 (at node 30)
	maximum = 0.373 (at node 152)
Injected packet length average = 17.8051
Accepted packet length average = 22.6812
Total in-flight flits = 59036 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 728.598
	minimum = 23
	maximum = 1918
Network latency average = 637.073
	minimum = 23
	maximum = 1894
Slowest packet = 222
Flit latency average = 506.116
	minimum = 6
	maximum = 1931
Slowest flit = 6286
Fragmentation average = 312.051
	minimum = 0
	maximum = 1749
Injected packet rate average = 0.0222161
	minimum = 0.009 (at node 188)
	maximum = 0.0285 (at node 66)
Accepted packet rate average = 0.0111901
	minimum = 0.005 (at node 164)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.396253
	minimum = 0.158 (at node 188)
	maximum = 0.5075 (at node 66)
Accepted flit rate average= 0.224745
	minimum = 0.106 (at node 164)
	maximum = 0.344 (at node 152)
Injected packet length average = 17.8362
Accepted packet length average = 20.0842
Total in-flight flits = 67436 (0 measured)
latency change    = 0.484352
throughput change = 0.00920025
Class 0:
Packet latency average = 1621.38
	minimum = 435
	maximum = 2883
Network latency average = 1193.76
	minimum = 30
	maximum = 2871
Slowest packet = 802
Flit latency average = 1061.82
	minimum = 6
	maximum = 2861
Slowest flit = 15667
Fragmentation average = 330.404
	minimum = 3
	maximum = 2231
Injected packet rate average = 0.0100313
	minimum = 0 (at node 2)
	maximum = 0.036 (at node 21)
Accepted packet rate average = 0.0125
	minimum = 0.002 (at node 153)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.180224
	minimum = 0 (at node 2)
	maximum = 0.652 (at node 21)
Accepted flit rate average= 0.22
	minimum = 0.03 (at node 153)
	maximum = 0.418 (at node 16)
Injected packet length average = 17.9663
Accepted packet length average = 17.6
Total in-flight flits = 59900 (0 measured)
latency change    = 0.55063
throughput change = 0.0215672
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2149.12
	minimum = 992
	maximum = 3329
Network latency average = 317.018
	minimum = 26
	maximum = 933
Slowest packet = 10471
Flit latency average = 1223.44
	minimum = 6
	maximum = 3886
Slowest flit = 11033
Fragmentation average = 152.267
	minimum = 0
	maximum = 836
Injected packet rate average = 0.00883333
	minimum = 0 (at node 0)
	maximum = 0.042 (at node 7)
Accepted packet rate average = 0.0116979
	minimum = 0.004 (at node 4)
	maximum = 0.02 (at node 66)
Injected flit rate average = 0.158708
	minimum = 0 (at node 1)
	maximum = 0.764 (at node 7)
Accepted flit rate average= 0.207135
	minimum = 0.073 (at node 174)
	maximum = 0.361 (at node 8)
Injected packet length average = 17.967
Accepted packet length average = 17.707
Total in-flight flits = 50604 (20323 measured)
latency change    = 0.245562
throughput change = 0.0621071
Class 0:
Packet latency average = 2606.84
	minimum = 992
	maximum = 4283
Network latency average = 510.785
	minimum = 26
	maximum = 1906
Slowest packet = 10471
Flit latency average = 1207.45
	minimum = 6
	maximum = 4740
Slowest flit = 17423
Fragmentation average = 178.037
	minimum = 0
	maximum = 1225
Injected packet rate average = 0.00985677
	minimum = 0 (at node 1)
	maximum = 0.035 (at node 7)
Accepted packet rate average = 0.0117604
	minimum = 0.004 (at node 36)
	maximum = 0.019 (at node 186)
Injected flit rate average = 0.177312
	minimum = 0 (at node 1)
	maximum = 0.631 (at node 7)
Accepted flit rate average= 0.208362
	minimum = 0.0695 (at node 36)
	maximum = 0.3045 (at node 186)
Injected packet length average = 17.9889
Accepted packet length average = 17.7172
Total in-flight flits = 48091 (31510 measured)
latency change    = 0.175586
throughput change = 0.00588669
Class 0:
Packet latency average = 3033.41
	minimum = 992
	maximum = 5241
Network latency average = 622.595
	minimum = 25
	maximum = 2901
Slowest packet = 10471
Flit latency average = 1177.28
	minimum = 6
	maximum = 5833
Slowest flit = 19535
Fragmentation average = 198.223
	minimum = 0
	maximum = 1715
Injected packet rate average = 0.0105052
	minimum = 0 (at node 1)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0117483
	minimum = 0.00633333 (at node 36)
	maximum = 0.017 (at node 136)
Injected flit rate average = 0.188953
	minimum = 0 (at node 1)
	maximum = 0.591 (at node 59)
Accepted flit rate average= 0.209267
	minimum = 0.108 (at node 36)
	maximum = 0.286 (at node 136)
Injected packet length average = 17.9866
Accepted packet length average = 17.8126
Total in-flight flits = 48298 (38333 measured)
latency change    = 0.140623
throughput change = 0.00432644
Class 0:
Packet latency average = 3439.76
	minimum = 992
	maximum = 6301
Network latency average = 736.704
	minimum = 25
	maximum = 3868
Slowest packet = 10471
Flit latency average = 1162.94
	minimum = 6
	maximum = 6874
Slowest flit = 10493
Fragmentation average = 220.257
	minimum = 0
	maximum = 2959
Injected packet rate average = 0.0107357
	minimum = 0 (at node 1)
	maximum = 0.03175 (at node 2)
Accepted packet rate average = 0.0116901
	minimum = 0.00725 (at node 116)
	maximum = 0.01625 (at node 129)
Injected flit rate average = 0.193228
	minimum = 0 (at node 1)
	maximum = 0.57175 (at node 2)
Accepted flit rate average= 0.208832
	minimum = 0.12675 (at node 36)
	maximum = 0.278 (at node 177)
Injected packet length average = 17.9987
Accepted packet length average = 17.864
Total in-flight flits = 47981 (41171 measured)
latency change    = 0.118132
throughput change = 0.00208459
Draining remaining packets ...
Class 0:
Remaining flits: 7128 7129 7130 7131 7132 7133 7134 7135 7136 7137 [...] (15340 flits)
Measured flits: 189310 189311 189312 189313 189314 189315 189316 189317 189318 189319 [...] (12796 flits)
Class 0:
Remaining flits: 177785 217962 217963 217964 217965 217966 217967 217968 217969 217970 [...] (209 flits)
Measured flits: 217962 217963 217964 217965 217966 217967 217968 217969 217970 217971 [...] (208 flits)
Time taken is 9153 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4015.03 (1 samples)
	minimum = 992 (1 samples)
	maximum = 7988 (1 samples)
Network latency average = 1077.31 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5713 (1 samples)
Flit latency average = 1443.68 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8322 (1 samples)
Fragmentation average = 230.699 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3901 (1 samples)
Injected packet rate average = 0.0107357 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.03175 (1 samples)
Accepted packet rate average = 0.0116901 (1 samples)
	minimum = 0.00725 (1 samples)
	maximum = 0.01625 (1 samples)
Injected flit rate average = 0.193228 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.57175 (1 samples)
Accepted flit rate average = 0.208832 (1 samples)
	minimum = 0.12675 (1 samples)
	maximum = 0.278 (1 samples)
Injected packet size average = 17.9987 (1 samples)
Accepted packet size average = 17.864 (1 samples)
Hops average = 5.11542 (1 samples)
Total run time 13.0466
