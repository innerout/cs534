BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.112
	minimum = 23
	maximum = 910
Network latency average = 298.559
	minimum = 23
	maximum = 897
Slowest packet = 463
Flit latency average = 267.986
	minimum = 6
	maximum = 910
Slowest flit = 9034
Fragmentation average = 53.3956
	minimum = 0
	maximum = 151
Injected packet rate average = 0.042901
	minimum = 0.03 (at node 51)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0114531
	minimum = 0.004 (at node 41)
	maximum = 0.021 (at node 34)
Injected flit rate average = 0.76549
	minimum = 0.531 (at node 145)
	maximum = 0.992 (at node 55)
Accepted flit rate average= 0.214281
	minimum = 0.077 (at node 41)
	maximum = 0.378 (at node 34)
Injected packet length average = 17.8431
Accepted packet length average = 18.7094
Total in-flight flits = 107124 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 587.007
	minimum = 23
	maximum = 1889
Network latency average = 565.487
	minimum = 23
	maximum = 1838
Slowest packet = 979
Flit latency average = 531.874
	minimum = 6
	maximum = 1852
Slowest flit = 17919
Fragmentation average = 55.1345
	minimum = 0
	maximum = 154
Injected packet rate average = 0.043625
	minimum = 0.034 (at node 51)
	maximum = 0.0545 (at node 31)
Accepted packet rate average = 0.0118333
	minimum = 0.0065 (at node 120)
	maximum = 0.0185 (at node 63)
Injected flit rate average = 0.78187
	minimum = 0.61 (at node 51)
	maximum = 0.981 (at node 74)
Accepted flit rate average= 0.216474
	minimum = 0.117 (at node 120)
	maximum = 0.3345 (at node 63)
Injected packet length average = 17.9225
Accepted packet length average = 18.2936
Total in-flight flits = 218410 (0 measured)
latency change    = 0.463189
throughput change = 0.0101292
Class 0:
Packet latency average = 1536.48
	minimum = 23
	maximum = 2754
Network latency average = 1501.93
	minimum = 23
	maximum = 2754
Slowest packet = 1592
Flit latency average = 1490.86
	minimum = 6
	maximum = 2737
Slowest flit = 28673
Fragmentation average = 62.3167
	minimum = 0
	maximum = 151
Injected packet rate average = 0.038974
	minimum = 0.013 (at node 36)
	maximum = 0.056 (at node 142)
Accepted packet rate average = 0.010625
	minimum = 0.004 (at node 38)
	maximum = 0.018 (at node 51)
Injected flit rate average = 0.701036
	minimum = 0.22 (at node 36)
	maximum = 1 (at node 31)
Accepted flit rate average= 0.193193
	minimum = 0.066 (at node 109)
	maximum = 0.321 (at node 51)
Injected packet length average = 17.9873
Accepted packet length average = 18.1828
Total in-flight flits = 316065 (0 measured)
latency change    = 0.617954
throughput change = 0.120508
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 400.235
	minimum = 27
	maximum = 1700
Network latency average = 80.3296
	minimum = 23
	maximum = 857
Slowest packet = 24360
Flit latency average = 2274.28
	minimum = 6
	maximum = 3674
Slowest flit = 43840
Fragmentation average = 9.95531
	minimum = 0
	maximum = 46
Injected packet rate average = 0.0373177
	minimum = 0.012 (at node 128)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0100521
	minimum = 0.002 (at node 96)
	maximum = 0.018 (at node 8)
Injected flit rate average = 0.671328
	minimum = 0.222 (at node 152)
	maximum = 1 (at node 18)
Accepted flit rate average= 0.180172
	minimum = 0.053 (at node 96)
	maximum = 0.316 (at node 8)
Injected packet length average = 17.9895
Accepted packet length average = 17.9238
Total in-flight flits = 410388 (125611 measured)
latency change    = 2.83896
throughput change = 0.072269
Class 0:
Packet latency average = 640.631
	minimum = 26
	maximum = 2417
Network latency average = 286.421
	minimum = 23
	maximum = 1917
Slowest packet = 24360
Flit latency average = 2652.3
	minimum = 6
	maximum = 4512
Slowest flit = 67084
Fragmentation average = 10.0856
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0373802
	minimum = 0.013 (at node 36)
	maximum = 0.0555 (at node 154)
Accepted packet rate average = 0.0100495
	minimum = 0.0055 (at node 1)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.672982
	minimum = 0.2405 (at node 8)
	maximum = 0.9955 (at node 154)
Accepted flit rate average= 0.180951
	minimum = 0.1025 (at node 1)
	maximum = 0.29 (at node 34)
Injected packet length average = 18.0037
Accepted packet length average = 18.006
Total in-flight flits = 504952 (250894 measured)
latency change    = 0.375249
throughput change = 0.00430309
Class 0:
Packet latency average = 937.759
	minimum = 26
	maximum = 2918
Network latency average = 545.475
	minimum = 23
	maximum = 2720
Slowest packet = 24373
Flit latency average = 3016.04
	minimum = 6
	maximum = 5491
Slowest flit = 71912
Fragmentation average = 10.0942
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0372031
	minimum = 0.013 (at node 36)
	maximum = 0.052 (at node 145)
Accepted packet rate average = 0.0101649
	minimum = 0.006 (at node 54)
	maximum = 0.0146667 (at node 78)
Injected flit rate average = 0.669484
	minimum = 0.237667 (at node 36)
	maximum = 0.939 (at node 145)
Accepted flit rate average= 0.182481
	minimum = 0.105667 (at node 163)
	maximum = 0.264 (at node 78)
Injected packet length average = 17.9954
Accepted packet length average = 17.952
Total in-flight flits = 596642 (372805 measured)
latency change    = 0.31685
throughput change = 0.00838653
Class 0:
Packet latency average = 1220.64
	minimum = 26
	maximum = 3788
Network latency average = 780.641
	minimum = 23
	maximum = 3423
Slowest packet = 24373
Flit latency average = 3373.08
	minimum = 6
	maximum = 6351
Slowest flit = 87929
Fragmentation average = 10.0468
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0371094
	minimum = 0.01325 (at node 36)
	maximum = 0.052 (at node 145)
Accepted packet rate average = 0.0101563
	minimum = 0.0055 (at node 163)
	maximum = 0.01425 (at node 99)
Injected flit rate average = 0.668008
	minimum = 0.2415 (at node 36)
	maximum = 0.936 (at node 145)
Accepted flit rate average= 0.182635
	minimum = 0.09675 (at node 163)
	maximum = 0.25725 (at node 99)
Injected packet length average = 18.0011
Accepted packet length average = 17.9826
Total in-flight flits = 688765 (494422 measured)
latency change    = 0.231747
throughput change = 0.000846024
Draining remaining packets ...
Class 0:
Remaining flits: 88524 88525 88526 88527 88528 88529 88530 88531 88532 88533 [...] (657000 flits)
Measured flits: 436302 436303 436304 436305 436306 436307 436308 436309 436310 436311 [...] (491756 flits)
Class 0:
Remaining flits: 108090 108091 108092 108093 108094 108095 108096 108097 108098 108099 [...] (624695 flits)
Measured flits: 436302 436303 436304 436305 436306 436307 436308 436309 436310 436311 [...] (488264 flits)
Class 0:
Remaining flits: 120510 120511 120512 120513 120514 120515 120516 120517 120518 120519 [...] (592826 flits)
Measured flits: 436302 436303 436304 436305 436306 436307 436308 436309 436310 436311 [...] (483443 flits)
Class 0:
Remaining flits: 139860 139861 139862 139863 139864 139865 139866 139867 139868 139869 [...] (560571 flits)
Measured flits: 436302 436303 436304 436305 436306 436307 436308 436309 436310 436311 [...] (475990 flits)
Class 0:
Remaining flits: 159604 159605 169524 169525 169526 169527 169528 169529 169530 169531 [...] (527650 flits)
Measured flits: 436320 436321 436322 436323 436324 436325 436326 436327 436328 436329 [...] (464626 flits)
Class 0:
Remaining flits: 206676 206677 206678 206679 206680 206681 206682 206683 206684 206685 [...] (494305 flits)
Measured flits: 436320 436321 436322 436323 436324 436325 436326 436327 436328 436329 [...] (449464 flits)
Class 0:
Remaining flits: 236461 236462 236463 236464 236465 237600 237601 237602 237603 237604 [...] (461518 flits)
Measured flits: 436320 436321 436322 436323 436324 436325 436326 436327 436328 436329 [...] (430716 flits)
Class 0:
Remaining flits: 239202 239203 239204 239205 239206 239207 239208 239209 239210 239211 [...] (428663 flits)
Measured flits: 436320 436321 436322 436323 436324 436325 436326 436327 436328 436329 [...] (408609 flits)
Class 0:
Remaining flits: 250398 250399 250400 250401 250402 250403 250404 250405 250406 250407 [...] (397516 flits)
Measured flits: 436320 436321 436322 436323 436324 436325 436326 436327 436328 436329 [...] (385325 flits)
Class 0:
Remaining flits: 250398 250399 250400 250401 250402 250403 250404 250405 250406 250407 [...] (368008 flits)
Measured flits: 436626 436627 436628 436629 436630 436631 436632 436633 436634 436635 [...] (360822 flits)
Class 0:
Remaining flits: 253350 253351 253352 253353 253354 253355 253356 253357 253358 253359 [...] (339115 flits)
Measured flits: 436752 436753 436754 436755 436756 436757 436758 436759 436760 436761 [...] (335080 flits)
Class 0:
Remaining flits: 327924 327925 327926 327927 327928 327929 327930 327931 327932 327933 [...] (309532 flits)
Measured flits: 436752 436753 436754 436755 436756 436757 436758 436759 436760 436761 [...] (307235 flits)
Class 0:
Remaining flits: 336348 336349 336350 336351 336352 336353 336354 336355 336356 336357 [...] (279822 flits)
Measured flits: 436752 436753 436754 436755 436756 436757 436758 436759 436760 436761 [...] (278620 flits)
Class 0:
Remaining flits: 350154 350155 350156 350157 350158 350159 350160 350161 350162 350163 [...] (250828 flits)
Measured flits: 438606 438607 438608 438609 438610 438611 438612 438613 438614 438615 [...] (250198 flits)
Class 0:
Remaining flits: 365993 370278 370279 370280 370281 370282 370283 370284 370285 370286 [...] (221606 flits)
Measured flits: 438606 438607 438608 438609 438610 438611 438612 438613 438614 438615 [...] (221250 flits)
Class 0:
Remaining flits: 377154 377155 377156 377157 377158 377159 377160 377161 377162 377163 [...] (192410 flits)
Measured flits: 438606 438607 438608 438609 438610 438611 438612 438613 438614 438615 [...] (192248 flits)
Class 0:
Remaining flits: 377154 377155 377156 377157 377158 377159 377160 377161 377162 377163 [...] (162730 flits)
Measured flits: 439992 439993 439994 439995 439996 439997 439998 439999 440000 440001 [...] (162653 flits)
Class 0:
Remaining flits: 428418 428419 428420 428421 428422 428423 428424 428425 428426 428427 [...] (133640 flits)
Measured flits: 439992 439993 439994 439995 439996 439997 439998 439999 440000 440001 [...] (133622 flits)
Class 0:
Remaining flits: 428418 428419 428420 428421 428422 428423 428424 428425 428426 428427 [...] (104933 flits)
Measured flits: 443286 443287 443288 443289 443290 443291 443292 443293 443294 443295 [...] (104915 flits)
Class 0:
Remaining flits: 479394 479395 479396 479397 479398 479399 479400 479401 479402 479403 [...] (76212 flits)
Measured flits: 479394 479395 479396 479397 479398 479399 479400 479401 479402 479403 [...] (76212 flits)
Class 0:
Remaining flits: 535932 535933 535934 535935 535936 535937 535938 535939 535940 535941 [...] (47196 flits)
Measured flits: 535932 535933 535934 535935 535936 535937 535938 535939 535940 535941 [...] (47196 flits)
Class 0:
Remaining flits: 582343 582344 582345 582346 582347 582348 582349 582350 582351 582352 [...] (20976 flits)
Measured flits: 582343 582344 582345 582346 582347 582348 582349 582350 582351 582352 [...] (20976 flits)
Class 0:
Remaining flits: 704520 704521 704522 704523 704524 704525 704526 704527 704528 704529 [...] (3585 flits)
Measured flits: 704520 704521 704522 704523 704524 704525 704526 704527 704528 704529 [...] (3585 flits)
Time taken is 30908 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15499.4 (1 samples)
	minimum = 26 (1 samples)
	maximum = 25090 (1 samples)
Network latency average = 15280.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25090 (1 samples)
Flit latency average = 12044.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 25073 (1 samples)
Fragmentation average = 67.9433 (1 samples)
	minimum = 0 (1 samples)
	maximum = 189 (1 samples)
Injected packet rate average = 0.0371094 (1 samples)
	minimum = 0.01325 (1 samples)
	maximum = 0.052 (1 samples)
Accepted packet rate average = 0.0101563 (1 samples)
	minimum = 0.0055 (1 samples)
	maximum = 0.01425 (1 samples)
Injected flit rate average = 0.668008 (1 samples)
	minimum = 0.2415 (1 samples)
	maximum = 0.936 (1 samples)
Accepted flit rate average = 0.182635 (1 samples)
	minimum = 0.09675 (1 samples)
	maximum = 0.25725 (1 samples)
Injected packet size average = 18.0011 (1 samples)
Accepted packet size average = 17.9826 (1 samples)
Hops average = 5.20517 (1 samples)
Total run time 30.5883
