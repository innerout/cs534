BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 385.489
	minimum = 23
	maximum = 993
Network latency average = 284.93
	minimum = 23
	maximum = 976
Slowest packet = 21
Flit latency average = 213.142
	minimum = 6
	maximum = 959
Slowest flit = 3293
Fragmentation average = 150.214
	minimum = 0
	maximum = 876
Injected packet rate average = 0.0197031
	minimum = 0 (at node 66)
	maximum = 0.041 (at node 87)
Accepted packet rate average = 0.00988542
	minimum = 0.002 (at node 174)
	maximum = 0.019 (at node 114)
Injected flit rate average = 0.348594
	minimum = 0 (at node 66)
	maximum = 0.736 (at node 87)
Accepted flit rate average= 0.202937
	minimum = 0.052 (at node 174)
	maximum = 0.35 (at node 152)
Injected packet length average = 17.6923
Accepted packet length average = 20.529
Total in-flight flits = 29202 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 680.342
	minimum = 23
	maximum = 1966
Network latency average = 473.053
	minimum = 23
	maximum = 1866
Slowest packet = 21
Flit latency average = 378.503
	minimum = 6
	maximum = 1849
Slowest flit = 8117
Fragmentation average = 185.761
	minimum = 0
	maximum = 1614
Injected packet rate average = 0.0162214
	minimum = 0.005 (at node 5)
	maximum = 0.029 (at node 87)
Accepted packet rate average = 0.0105599
	minimum = 0.0045 (at node 174)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.288635
	minimum = 0.09 (at node 5)
	maximum = 0.5195 (at node 87)
Accepted flit rate average= 0.202323
	minimum = 0.1035 (at node 174)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.7935
Accepted packet length average = 19.1596
Total in-flight flits = 34754 (0 measured)
latency change    = 0.433389
throughput change = 0.00303764
Class 0:
Packet latency average = 1614.49
	minimum = 27
	maximum = 2951
Network latency average = 877.815
	minimum = 25
	maximum = 2796
Slowest packet = 181
Flit latency average = 755.466
	minimum = 6
	maximum = 2878
Slowest flit = 10641
Fragmentation average = 223.42
	minimum = 0
	maximum = 2329
Injected packet rate average = 0.0112344
	minimum = 0 (at node 11)
	maximum = 0.025 (at node 65)
Accepted packet rate average = 0.0108542
	minimum = 0.005 (at node 60)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.202135
	minimum = 0 (at node 11)
	maximum = 0.455 (at node 82)
Accepted flit rate average= 0.193781
	minimum = 0.08 (at node 185)
	maximum = 0.353 (at node 137)
Injected packet length average = 17.9926
Accepted packet length average = 17.8532
Total in-flight flits = 36410 (0 measured)
latency change    = 0.578602
throughput change = 0.0440789
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2164.9
	minimum = 41
	maximum = 3722
Network latency average = 378.773
	minimum = 25
	maximum = 936
Slowest packet = 8428
Flit latency average = 845.528
	minimum = 6
	maximum = 3916
Slowest flit = 6281
Fragmentation average = 132.126
	minimum = 2
	maximum = 721
Injected packet rate average = 0.0106094
	minimum = 0 (at node 15)
	maximum = 0.029 (at node 22)
Accepted packet rate average = 0.0108021
	minimum = 0.003 (at node 132)
	maximum = 0.021 (at node 56)
Injected flit rate average = 0.191047
	minimum = 0 (at node 20)
	maximum = 0.533 (at node 22)
Accepted flit rate average= 0.193062
	minimum = 0.039 (at node 132)
	maximum = 0.345 (at node 56)
Injected packet length average = 18.0074
Accepted packet length average = 17.8727
Total in-flight flits = 36116 (24181 measured)
latency change    = 0.254246
throughput change = 0.00372289
Class 0:
Packet latency average = 2678.86
	minimum = 41
	maximum = 4750
Network latency average = 589.627
	minimum = 25
	maximum = 1948
Slowest packet = 8428
Flit latency average = 878.382
	minimum = 6
	maximum = 4631
Slowest flit = 24792
Fragmentation average = 161.163
	minimum = 0
	maximum = 1394
Injected packet rate average = 0.0108073
	minimum = 0 (at node 67)
	maximum = 0.0225 (at node 94)
Accepted packet rate average = 0.0106823
	minimum = 0.0055 (at node 36)
	maximum = 0.0165 (at node 6)
Injected flit rate average = 0.194302
	minimum = 0.0005 (at node 67)
	maximum = 0.4045 (at node 94)
Accepted flit rate average= 0.192497
	minimum = 0.099 (at node 36)
	maximum = 0.291 (at node 16)
Injected packet length average = 17.9788
Accepted packet length average = 18.0202
Total in-flight flits = 37173 (33517 measured)
latency change    = 0.191856
throughput change = 0.00293565
Class 0:
Packet latency average = 3134.29
	minimum = 41
	maximum = 5766
Network latency average = 743.933
	minimum = 25
	maximum = 2769
Slowest packet = 8428
Flit latency average = 900.522
	minimum = 6
	maximum = 5092
Slowest flit = 54197
Fragmentation average = 177.749
	minimum = 0
	maximum = 1934
Injected packet rate average = 0.0107639
	minimum = 0.000666667 (at node 77)
	maximum = 0.02 (at node 65)
Accepted packet rate average = 0.0107153
	minimum = 0.005 (at node 36)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.193628
	minimum = 0.0136667 (at node 77)
	maximum = 0.355667 (at node 65)
Accepted flit rate average= 0.193099
	minimum = 0.095 (at node 36)
	maximum = 0.298667 (at node 129)
Injected packet length average = 17.9887
Accepted packet length average = 18.0209
Total in-flight flits = 36821 (35698 measured)
latency change    = 0.145305
throughput change = 0.00311531
Draining remaining packets ...
Class 0:
Remaining flits: 57455 71478 71479 71480 71481 71482 71483 71484 71485 71486 [...] (5419 flits)
Measured flits: 158040 158041 158042 158043 158044 158045 158046 158047 158048 158049 [...] (5243 flits)
Time taken is 7505 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3596.33 (1 samples)
	minimum = 41 (1 samples)
	maximum = 6878 (1 samples)
Network latency average = 978.605 (1 samples)
	minimum = 25 (1 samples)
	maximum = 3976 (1 samples)
Flit latency average = 1018.47 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6181 (1 samples)
Fragmentation average = 181.196 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2069 (1 samples)
Injected packet rate average = 0.0107639 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.02 (1 samples)
Accepted packet rate average = 0.0107153 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.193628 (1 samples)
	minimum = 0.0136667 (1 samples)
	maximum = 0.355667 (1 samples)
Accepted flit rate average = 0.193099 (1 samples)
	minimum = 0.095 (1 samples)
	maximum = 0.298667 (1 samples)
Injected packet size average = 17.9887 (1 samples)
Accepted packet size average = 18.0209 (1 samples)
Hops average = 5.14286 (1 samples)
Total run time 12.0024
