BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 364.882
	minimum = 23
	maximum = 949
Network latency average = 342.261
	minimum = 23
	maximum = 921
Slowest packet = 340
Flit latency average = 247.164
	minimum = 6
	maximum = 975
Slowest flit = 1947
Fragmentation average = 237.535
	minimum = 0
	maximum = 822
Injected packet rate average = 0.0294323
	minimum = 0.015 (at node 72)
	maximum = 0.041 (at node 134)
Accepted packet rate average = 0.00996354
	minimum = 0.003 (at node 174)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.524021
	minimum = 0.27 (at node 72)
	maximum = 0.724 (at node 134)
Accepted flit rate average= 0.224042
	minimum = 0.089 (at node 174)
	maximum = 0.363 (at node 167)
Injected packet length average = 17.8043
Accepted packet length average = 22.4861
Total in-flight flits = 58774 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 702.118
	minimum = 23
	maximum = 1870
Network latency average = 618.973
	minimum = 23
	maximum = 1817
Slowest packet = 770
Flit latency average = 493.107
	minimum = 6
	maximum = 1901
Slowest flit = 9368
Fragmentation average = 302.269
	minimum = 0
	maximum = 1293
Injected packet rate average = 0.0221719
	minimum = 0.008 (at node 72)
	maximum = 0.031 (at node 126)
Accepted packet rate average = 0.0112266
	minimum = 0.006 (at node 62)
	maximum = 0.0165 (at node 63)
Injected flit rate average = 0.395862
	minimum = 0.136 (at node 72)
	maximum = 0.555 (at node 126)
Accepted flit rate average= 0.22537
	minimum = 0.132 (at node 62)
	maximum = 0.3465 (at node 78)
Injected packet length average = 17.8542
Accepted packet length average = 20.0747
Total in-flight flits = 66962 (0 measured)
latency change    = 0.480313
throughput change = 0.00589309
Class 0:
Packet latency average = 1577.53
	minimum = 349
	maximum = 2769
Network latency average = 1191
	minimum = 28
	maximum = 2759
Slowest packet = 921
Flit latency average = 1057.97
	minimum = 6
	maximum = 2838
Slowest flit = 16611
Fragmentation average = 328.327
	minimum = 1
	maximum = 2127
Injected packet rate average = 0.0100469
	minimum = 0 (at node 1)
	maximum = 0.039 (at node 170)
Accepted packet rate average = 0.012651
	minimum = 0.004 (at node 190)
	maximum = 0.023 (at node 120)
Injected flit rate average = 0.179391
	minimum = 0 (at node 2)
	maximum = 0.698 (at node 170)
Accepted flit rate average= 0.221297
	minimum = 0.077 (at node 0)
	maximum = 0.385 (at node 120)
Injected packet length average = 17.8554
Accepted packet length average = 17.4924
Total in-flight flits = 59177 (0 measured)
latency change    = 0.554925
throughput change = 0.0184048
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2025.58
	minimum = 757
	maximum = 3087
Network latency average = 328.829
	minimum = 27
	maximum = 940
Slowest packet = 10479
Flit latency average = 1194.34
	minimum = 6
	maximum = 3825
Slowest flit = 18627
Fragmentation average = 146.257
	minimum = 0
	maximum = 869
Injected packet rate average = 0.00978646
	minimum = 0 (at node 0)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0118594
	minimum = 0.005 (at node 86)
	maximum = 0.02 (at node 24)
Injected flit rate average = 0.176849
	minimum = 0 (at node 0)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.210318
	minimum = 0.097 (at node 71)
	maximum = 0.349 (at node 187)
Injected packet length average = 18.0708
Accepted packet length average = 17.7343
Total in-flight flits = 52510 (22200 measured)
latency change    = 0.221197
throughput change = 0.0522028
Class 0:
Packet latency average = 2451.16
	minimum = 757
	maximum = 4491
Network latency average = 518.469
	minimum = 27
	maximum = 1929
Slowest packet = 10479
Flit latency average = 1174.57
	minimum = 6
	maximum = 4836
Slowest flit = 18275
Fragmentation average = 176.345
	minimum = 0
	maximum = 1449
Injected packet rate average = 0.0104375
	minimum = 0 (at node 0)
	maximum = 0.0345 (at node 7)
Accepted packet rate average = 0.0117917
	minimum = 0.007 (at node 20)
	maximum = 0.018 (at node 38)
Injected flit rate average = 0.187992
	minimum = 0 (at node 0)
	maximum = 0.626 (at node 181)
Accepted flit rate average= 0.208737
	minimum = 0.1255 (at node 4)
	maximum = 0.3205 (at node 38)
Injected packet length average = 18.0112
Accepted packet length average = 17.7021
Total in-flight flits = 51130 (33803 measured)
latency change    = 0.173624
throughput change = 0.00757283
Class 0:
Packet latency average = 2900.47
	minimum = 757
	maximum = 5298
Network latency average = 646.463
	minimum = 26
	maximum = 2927
Slowest packet = 10479
Flit latency average = 1161.51
	minimum = 6
	maximum = 5809
Slowest flit = 18629
Fragmentation average = 196.856
	minimum = 0
	maximum = 1890
Injected packet rate average = 0.0109236
	minimum = 0 (at node 0)
	maximum = 0.0306667 (at node 109)
Accepted packet rate average = 0.0116806
	minimum = 0.00666667 (at node 185)
	maximum = 0.018 (at node 165)
Injected flit rate average = 0.196552
	minimum = 0 (at node 0)
	maximum = 0.550333 (at node 109)
Accepted flit rate average= 0.20799
	minimum = 0.129 (at node 5)
	maximum = 0.326333 (at node 165)
Injected packet length average = 17.9933
Accepted packet length average = 17.8065
Total in-flight flits = 52595 (41565 measured)
latency change    = 0.154909
throughput change = 0.00359343
Class 0:
Packet latency average = 3298.02
	minimum = 757
	maximum = 6112
Network latency average = 765.524
	minimum = 26
	maximum = 3825
Slowest packet = 10479
Flit latency average = 1166.73
	minimum = 6
	maximum = 6737
Slowest flit = 18485
Fragmentation average = 214.438
	minimum = 0
	maximum = 3228
Injected packet rate average = 0.010819
	minimum = 0 (at node 0)
	maximum = 0.0265 (at node 87)
Accepted packet rate average = 0.0116055
	minimum = 0.0075 (at node 135)
	maximum = 0.017 (at node 165)
Injected flit rate average = 0.19466
	minimum = 0 (at node 0)
	maximum = 0.477 (at node 87)
Accepted flit rate average= 0.207453
	minimum = 0.13625 (at node 135)
	maximum = 0.309 (at node 165)
Injected packet length average = 17.9924
Accepted packet length average = 17.8755
Total in-flight flits = 49343 (41576 measured)
latency change    = 0.120543
throughput change = 0.00258593
Class 0:
Packet latency average = 3685.5
	minimum = 757
	maximum = 6905
Network latency average = 836.768
	minimum = 26
	maximum = 4827
Slowest packet = 10479
Flit latency average = 1171.19
	minimum = 6
	maximum = 7722
Slowest flit = 33034
Fragmentation average = 224.448
	minimum = 0
	maximum = 3228
Injected packet rate average = 0.0108802
	minimum = 0 (at node 0)
	maximum = 0.0262 (at node 190)
Accepted packet rate average = 0.0115396
	minimum = 0.0076 (at node 135)
	maximum = 0.0166 (at node 128)
Injected flit rate average = 0.195928
	minimum = 0 (at node 0)
	maximum = 0.4746 (at node 190)
Accepted flit rate average= 0.206872
	minimum = 0.1372 (at node 135)
	maximum = 0.2908 (at node 128)
Injected packet length average = 18.0078
Accepted packet length average = 17.9272
Total in-flight flits = 48590 (43189 measured)
latency change    = 0.105137
throughput change = 0.00280971
Class 0:
Packet latency average = 4074.63
	minimum = 757
	maximum = 7921
Network latency average = 899.723
	minimum = 26
	maximum = 5905
Slowest packet = 10479
Flit latency average = 1161.64
	minimum = 6
	maximum = 8715
Slowest flit = 33453
Fragmentation average = 235.79
	minimum = 0
	maximum = 4429
Injected packet rate average = 0.0109618
	minimum = 0 (at node 14)
	maximum = 0.0251667 (at node 190)
Accepted packet rate average = 0.0115668
	minimum = 0.00833333 (at node 135)
	maximum = 0.0153333 (at node 128)
Injected flit rate average = 0.197319
	minimum = 0 (at node 14)
	maximum = 0.4545 (at node 190)
Accepted flit rate average= 0.207247
	minimum = 0.148833 (at node 45)
	maximum = 0.271333 (at node 128)
Injected packet length average = 18.0006
Accepted packet length average = 17.9173
Total in-flight flits = 47733 (43519 measured)
latency change    = 0.0954995
throughput change = 0.00180776
Class 0:
Packet latency average = 4436.17
	minimum = 757
	maximum = 9283
Network latency average = 930.306
	minimum = 26
	maximum = 6681
Slowest packet = 10479
Flit latency average = 1156.2
	minimum = 6
	maximum = 9611
Slowest flit = 43286
Fragmentation average = 241.757
	minimum = 0
	maximum = 4444
Injected packet rate average = 0.0111644
	minimum = 0 (at node 14)
	maximum = 0.026 (at node 190)
Accepted packet rate average = 0.0115714
	minimum = 0.00842857 (at node 45)
	maximum = 0.0154286 (at node 128)
Injected flit rate average = 0.201032
	minimum = 0 (at node 14)
	maximum = 0.468429 (at node 190)
Accepted flit rate average= 0.207533
	minimum = 0.150429 (at node 45)
	maximum = 0.271571 (at node 128)
Injected packet length average = 18.0065
Accepted packet length average = 17.935
Total in-flight flits = 50396 (47371 measured)
latency change    = 0.0814982
throughput change = 0.00138269
Draining all recorded packets ...
Class 0:
Remaining flits: 12265 12266 12267 12268 12269 12270 12271 12272 12273 12274 [...] (53880 flits)
Measured flits: 188417 188418 188419 188420 188421 188422 188423 189737 190782 190783 [...] (51799 flits)
Class 0:
Remaining flits: 26416 26417 26418 26419 26420 26421 26422 26423 27774 27775 [...] (56345 flits)
Measured flits: 190797 190798 190799 191574 191575 191576 191577 191578 191579 191580 [...] (54998 flits)
Class 0:
Remaining flits: 26421 26422 26423 27774 27775 27776 27777 27778 27779 27780 [...] (54547 flits)
Measured flits: 191574 191575 191576 191577 191578 191579 191580 191581 191582 191583 [...] (53567 flits)
Class 0:
Remaining flits: 44136 44137 44138 44139 44140 44141 44142 44143 44144 44145 [...] (53982 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (53350 flits)
Class 0:
Remaining flits: 44138 44139 44140 44141 44142 44143 44144 44145 44146 44147 [...] (53150 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (52690 flits)
Class 0:
Remaining flits: 56782 56783 56784 56785 56786 56787 56788 56789 62114 62115 [...] (55087 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (54885 flits)
Class 0:
Remaining flits: 56782 56783 56784 56785 56786 56787 56788 56789 68594 68595 [...] (56489 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (56355 flits)
Class 0:
Remaining flits: 56782 56783 56784 56785 56786 56787 56788 56789 68594 68595 [...] (55069 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (54995 flits)
Class 0:
Remaining flits: 56788 56789 74337 74338 74339 98310 98311 98312 98313 98314 [...] (56171 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (56035 flits)
Class 0:
Remaining flits: 74337 74338 74339 98310 98311 98312 98313 98314 98315 143779 [...] (58297 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (57274 flits)
Class 0:
Remaining flits: 74337 74338 74339 98310 98311 98312 98313 98314 98315 157482 [...] (56970 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (54965 flits)
Class 0:
Remaining flits: 98310 98311 98312 98313 98314 98315 157482 157483 157484 157485 [...] (59036 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (55933 flits)
Class 0:
Remaining flits: 98310 98311 98312 98313 98314 98315 157482 157483 157484 157485 [...] (60015 flits)
Measured flits: 192240 192241 192242 192243 192244 192245 192246 192247 192248 192249 [...] (55809 flits)
Class 0:
Remaining flits: 157484 157485 157486 157487 157488 157489 157490 157491 157492 157493 [...] (59952 flits)
Measured flits: 206253 206254 206255 206256 206257 206258 206259 206260 206261 212000 [...] (53154 flits)
Class 0:
Remaining flits: 157484 157485 157486 157487 157488 157489 157490 157491 157492 157493 [...] (61262 flits)
Measured flits: 206253 206254 206255 206256 206257 206258 206259 206260 206261 259787 [...] (51149 flits)
Class 0:
Remaining flits: 157484 157485 157486 157487 157488 157489 157490 157491 157492 157493 [...] (61249 flits)
Measured flits: 206253 206254 206255 206256 206257 206258 206259 206260 206261 284526 [...] (48102 flits)
Class 0:
Remaining flits: 206253 206254 206255 206256 206257 206258 206259 206260 206261 284526 [...] (61623 flits)
Measured flits: 206253 206254 206255 206256 206257 206258 206259 206260 206261 284526 [...] (46178 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (60413 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (43367 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (61295 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (39905 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62171 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (37701 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (64429 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (35938 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62898 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (32386 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62773 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (30062 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62101 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (27581 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (64925 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (26084 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (63661 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (24322 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (63123 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (23210 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62558 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (19831 flits)
Class 0:
Remaining flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (62875 flits)
Measured flits: 284526 284527 284528 284529 284530 284531 284532 284533 284534 284535 [...] (17852 flits)
Class 0:
Remaining flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (63346 flits)
Measured flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (16902 flits)
Class 0:
Remaining flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (62533 flits)
Measured flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (14924 flits)
Class 0:
Remaining flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (62822 flits)
Measured flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (14291 flits)
Class 0:
Remaining flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (64290 flits)
Measured flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (13029 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (63222 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (12076 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (64434 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (11349 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (64246 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (10456 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (66427 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (8759 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (65638 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (7547 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (64824 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (6360 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (63013 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (5649 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (65105 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (5792 flits)
Class 0:
Remaining flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (64907 flits)
Measured flits: 706716 706717 706718 706719 706720 706721 706722 706723 706724 706725 [...] (5526 flits)
Class 0:
Remaining flits: 819918 819919 819920 819921 819922 819923 819924 819925 819926 819927 [...] (63933 flits)
Measured flits: 819918 819919 819920 819921 819922 819923 819924 819925 819926 819927 [...] (5170 flits)
Class 0:
Remaining flits: 983160 983161 983162 983163 983164 983165 983166 983167 983168 983169 [...] (64131 flits)
Measured flits: 983160 983161 983162 983163 983164 983165 983166 983167 983168 983169 [...] (4362 flits)
Class 0:
Remaining flits: 1059014 1059015 1059016 1059017 1059018 1059019 1059020 1059021 1059022 1059023 [...] (65868 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (4351 flits)
Class 0:
Remaining flits: 1106964 1106965 1106966 1106967 1106968 1106969 1106970 1106971 1106972 1106973 [...] (66321 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (3887 flits)
Class 0:
Remaining flits: 1106964 1106965 1106966 1106967 1106968 1106969 1106970 1106971 1106972 1106973 [...] (64105 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (3681 flits)
Class 0:
Remaining flits: 1106970 1106971 1106972 1106973 1106974 1106975 1106976 1106977 1106978 1106979 [...] (62441 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (3168 flits)
Class 0:
Remaining flits: 1159596 1159597 1159598 1159599 1159600 1159601 1159602 1159603 1159604 1159605 [...] (63032 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (2887 flits)
Class 0:
Remaining flits: 1159596 1159597 1159598 1159599 1159600 1159601 1159602 1159603 1159604 1159605 [...] (59549 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (2670 flits)
Class 0:
Remaining flits: 1159596 1159597 1159598 1159599 1159600 1159601 1159602 1159603 1159604 1159605 [...] (60388 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (2370 flits)
Class 0:
Remaining flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (60890 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (2209 flits)
Class 0:
Remaining flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (61985 flits)
Measured flits: 1230300 1230301 1230302 1230303 1230304 1230305 1230306 1230307 1230308 1230309 [...] (2322 flits)
Class 0:
Remaining flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (62182 flits)
Measured flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (1691 flits)
Class 0:
Remaining flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (60563 flits)
Measured flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (1315 flits)
Class 0:
Remaining flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (60486 flits)
Measured flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (1701 flits)
Class 0:
Remaining flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (62516 flits)
Measured flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (1434 flits)
Class 0:
Remaining flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (62224 flits)
Measured flits: 1243314 1243315 1243316 1243317 1243318 1243319 1243320 1243321 1243322 1243323 [...] (1429 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (64841 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (1133 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (62181 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (963 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (63239 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (817 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (65453 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (735 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (63493 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (703 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (64640 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (442 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (63807 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (311 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (62924 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (201 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (63409 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (180 flits)
Class 0:
Remaining flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (61823 flits)
Measured flits: 1376280 1376281 1376282 1376283 1376284 1376285 1376286 1376287 1376288 1376289 [...] (162 flits)
Class 0:
Remaining flits: 1376297 1678338 1678339 1678340 1678341 1678342 1678343 1678344 1678345 1678346 [...] (61936 flits)
Measured flits: 1376297 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 [...] (145 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (61416 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (108 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (60182 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (90 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (62467 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (90 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (60178 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (90 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (62412 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (90 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (61819 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (72 flits)
Class 0:
Remaining flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (62703 flits)
Measured flits: 1725228 1725229 1725230 1725231 1725232 1725233 1725234 1725235 1725236 1725237 [...] (72 flits)
Class 0:
Remaining flits: 1725242 1725243 1725244 1725245 1781397 1781398 1781399 1781400 1781401 1781402 [...] (60567 flits)
Measured flits: 1725242 1725243 1725244 1725245 2261808 2261809 2261810 2261811 2261812 2261813 [...] (58 flits)
Class 0:
Remaining flits: 1725245 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 [...] (59498 flits)
Measured flits: 1725245 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 [...] (37 flits)
Class 0:
Remaining flits: 1725245 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 [...] (60384 flits)
Measured flits: 1725245 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 [...] (37 flits)
Class 0:
Remaining flits: 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 1959903 [...] (61485 flits)
Measured flits: 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 2402361 [...] (36 flits)
Class 0:
Remaining flits: 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 1959903 [...] (61100 flits)
Measured flits: 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 2402361 [...] (36 flits)
Class 0:
Remaining flits: 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 1959903 [...] (61363 flits)
Measured flits: 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 2402361 [...] (36 flits)
Class 0:
Remaining flits: 1959894 1959895 1959896 1959897 1959898 1959899 1959900 1959901 1959902 1959903 [...] (61358 flits)
Measured flits: 2412756 2412757 2412758 2412759 2412760 2412761 2412762 2412763 2412764 2412765 [...] (18 flits)
Class 0:
Remaining flits: 2230470 2230471 2230472 2230473 2230474 2230475 2230476 2230477 2230478 2230479 [...] (63284 flits)
Measured flits: 2412756 2412757 2412758 2412759 2412760 2412761 2412762 2412763 2412764 2412765 [...] (18 flits)
Class 0:
Remaining flits: 2230470 2230471 2230472 2230473 2230474 2230475 2230476 2230477 2230478 2230479 [...] (62906 flits)
Measured flits: 2412756 2412757 2412758 2412759 2412760 2412761 2412762 2412763 2412764 2412765 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2230470 2230471 2230472 2230473 2230474 2230475 2230476 2230477 2230478 2230479 [...] (40796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230470 2230471 2230472 2230473 2230474 2230475 2230476 2230477 2230478 2230479 [...] (28860 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230470 2230471 2230472 2230473 2230474 2230475 2230476 2230477 2230478 2230479 [...] (18232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2449206 2449207 2449208 2449209 2449210 2449211 2449212 2449213 2449214 2449215 [...] (7660 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478060 2478061 2478062 2478063 2478064 2478065 2478066 2478067 2478068 2478069 [...] (2163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3057860 3057861 3057862 3057863 3057864 3057865 3057866 3057867 3057868 3057869 [...] (69 flits)
Measured flits: (0 flits)
Time taken is 101444 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15451 (1 samples)
	minimum = 757 (1 samples)
	maximum = 85896 (1 samples)
Network latency average = 1659.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 44261 (1 samples)
Flit latency average = 1807.58 (1 samples)
	minimum = 6 (1 samples)
	maximum = 44244 (1 samples)
Fragmentation average = 267.671 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18784 (1 samples)
Injected packet rate average = 0.0111644 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0115714 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.201032 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.468429 (1 samples)
Accepted flit rate average = 0.207533 (1 samples)
	minimum = 0.150429 (1 samples)
	maximum = 0.271571 (1 samples)
Injected packet size average = 18.0065 (1 samples)
Accepted packet size average = 17.935 (1 samples)
Hops average = 5.05521 (1 samples)
Total run time 149.386
