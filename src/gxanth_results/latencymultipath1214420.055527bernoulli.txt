BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.134
	minimum = 22
	maximum = 886
Network latency average = 312.921
	minimum = 22
	maximum = 812
Slowest packet = 87
Flit latency average = 290.116
	minimum = 5
	maximum = 795
Slowest flit = 23884
Fragmentation average = 25.5024
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0471615
	minimum = 0.032 (at node 92)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.0154479
	minimum = 0.006 (at node 108)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.841599
	minimum = 0.576 (at node 92)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.28438
	minimum = 0.108 (at node 108)
	maximum = 0.466 (at node 44)
Injected packet length average = 17.8451
Accepted packet length average = 18.409
Total in-flight flits = 108695 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 690.303
	minimum = 22
	maximum = 1689
Network latency average = 641.303
	minimum = 22
	maximum = 1640
Slowest packet = 87
Flit latency average = 617.182
	minimum = 5
	maximum = 1623
Slowest flit = 55349
Fragmentation average = 26.5279
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0363203
	minimum = 0.0205 (at node 64)
	maximum = 0.0515 (at node 183)
Accepted packet rate average = 0.0152135
	minimum = 0.0085 (at node 153)
	maximum = 0.0215 (at node 56)
Injected flit rate average = 0.652229
	minimum = 0.369 (at node 64)
	maximum = 0.9185 (at node 183)
Accepted flit rate average= 0.276625
	minimum = 0.153 (at node 153)
	maximum = 0.3935 (at node 152)
Injected packet length average = 17.9577
Accepted packet length average = 18.1828
Total in-flight flits = 147072 (0 measured)
latency change    = 0.502923
throughput change = 0.0280351
Class 0:
Packet latency average = 1779.72
	minimum = 397
	maximum = 2578
Network latency average = 1634.37
	minimum = 22
	maximum = 2452
Slowest packet = 4254
Flit latency average = 1615.03
	minimum = 5
	maximum = 2487
Slowest flit = 82913
Fragmentation average = 21.2309
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0150729
	minimum = 0.001 (at node 156)
	maximum = 0.036 (at node 141)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.271526
	minimum = 0.029 (at node 156)
	maximum = 0.662 (at node 141)
Accepted flit rate average= 0.263917
	minimum = 0.109 (at node 167)
	maximum = 0.452 (at node 137)
Injected packet length average = 18.0142
Accepted packet length average = 17.9752
Total in-flight flits = 148384 (0 measured)
latency change    = 0.612128
throughput change = 0.0481528
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2003.63
	minimum = 701
	maximum = 2804
Network latency average = 55.2336
	minimum = 22
	maximum = 797
Slowest packet = 16981
Flit latency average = 2158.21
	minimum = 5
	maximum = 3199
Slowest flit = 128068
Fragmentation average = 4.42056
	minimum = 0
	maximum = 23
Injected packet rate average = 0.0154219
	minimum = 0.003 (at node 63)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0148125
	minimum = 0.007 (at node 127)
	maximum = 0.025 (at node 81)
Injected flit rate average = 0.277458
	minimum = 0.054 (at node 63)
	maximum = 0.712 (at node 158)
Accepted flit rate average= 0.266542
	minimum = 0.126 (at node 127)
	maximum = 0.447 (at node 81)
Injected packet length average = 17.9912
Accepted packet length average = 17.9944
Total in-flight flits = 150596 (51411 measured)
latency change    = 0.11175
throughput change = 0.00984837
Class 0:
Packet latency average = 2762.49
	minimum = 701
	maximum = 3993
Network latency average = 789.77
	minimum = 22
	maximum = 1939
Slowest packet = 16981
Flit latency average = 2360.05
	minimum = 5
	maximum = 4025
Slowest flit = 151989
Fragmentation average = 10.9799
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0153073
	minimum = 0.005 (at node 175)
	maximum = 0.031 (at node 158)
Accepted packet rate average = 0.0148229
	minimum = 0.008 (at node 116)
	maximum = 0.024 (at node 0)
Injected flit rate average = 0.275198
	minimum = 0.084 (at node 175)
	maximum = 0.563 (at node 158)
Accepted flit rate average= 0.266708
	minimum = 0.148 (at node 116)
	maximum = 0.4315 (at node 0)
Injected packet length average = 17.9782
Accepted packet length average = 17.993
Total in-flight flits = 151754 (97454 measured)
latency change    = 0.274703
throughput change = 0.000624902
Class 0:
Packet latency average = 3495.54
	minimum = 701
	maximum = 4866
Network latency average = 1391.3
	minimum = 22
	maximum = 2954
Slowest packet = 16981
Flit latency average = 2514.71
	minimum = 5
	maximum = 5006
Slowest flit = 162378
Fragmentation average = 15.2912
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0151944
	minimum = 0.00733333 (at node 32)
	maximum = 0.026 (at node 186)
Accepted packet rate average = 0.0147986
	minimum = 0.009 (at node 36)
	maximum = 0.021 (at node 0)
Injected flit rate average = 0.273257
	minimum = 0.129 (at node 127)
	maximum = 0.463333 (at node 186)
Accepted flit rate average= 0.266344
	minimum = 0.162 (at node 36)
	maximum = 0.381333 (at node 0)
Injected packet length average = 17.984
Accepted packet length average = 17.9979
Total in-flight flits = 152470 (131461 measured)
latency change    = 0.20971
throughput change = 0.00136885
Class 0:
Packet latency average = 4152.79
	minimum = 701
	maximum = 5707
Network latency average = 1941.29
	minimum = 22
	maximum = 3982
Slowest packet = 16981
Flit latency average = 2605.37
	minimum = 5
	maximum = 5896
Slowest flit = 179425
Fragmentation average = 17.1115
	minimum = 0
	maximum = 158
Injected packet rate average = 0.015237
	minimum = 0.00775 (at node 175)
	maximum = 0.02375 (at node 186)
Accepted packet rate average = 0.0148776
	minimum = 0.0105 (at node 104)
	maximum = 0.01975 (at node 138)
Injected flit rate average = 0.27416
	minimum = 0.14075 (at node 175)
	maximum = 0.42525 (at node 186)
Accepted flit rate average= 0.267615
	minimum = 0.1875 (at node 104)
	maximum = 0.353 (at node 138)
Injected packet length average = 17.9931
Accepted packet length average = 17.9877
Total in-flight flits = 153510 (150070 measured)
latency change    = 0.158266
throughput change = 0.00474874
Class 0:
Packet latency average = 4727.11
	minimum = 701
	maximum = 6572
Network latency average = 2318.55
	minimum = 22
	maximum = 4792
Slowest packet = 16981
Flit latency average = 2661.49
	minimum = 5
	maximum = 6006
Slowest flit = 242548
Fragmentation average = 18.0258
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0152219
	minimum = 0.0078 (at node 175)
	maximum = 0.0232 (at node 186)
Accepted packet rate average = 0.0148854
	minimum = 0.0106 (at node 126)
	maximum = 0.0194 (at node 138)
Injected flit rate average = 0.27401
	minimum = 0.1414 (at node 175)
	maximum = 0.4176 (at node 186)
Accepted flit rate average= 0.267912
	minimum = 0.1908 (at node 126)
	maximum = 0.3472 (at node 138)
Injected packet length average = 18.0011
Accepted packet length average = 17.9983
Total in-flight flits = 154384 (154217 measured)
latency change    = 0.121496
throughput change = 0.00111199
Class 0:
Packet latency average = 5201.19
	minimum = 701
	maximum = 7529
Network latency average = 2507.18
	minimum = 22
	maximum = 5881
Slowest packet = 16981
Flit latency average = 2698.93
	minimum = 5
	maximum = 6251
Slowest flit = 258893
Fragmentation average = 18.5536
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0150339
	minimum = 0.008 (at node 175)
	maximum = 0.0216667 (at node 171)
Accepted packet rate average = 0.0149036
	minimum = 0.0111667 (at node 104)
	maximum = 0.0193333 (at node 138)
Injected flit rate average = 0.270666
	minimum = 0.144833 (at node 175)
	maximum = 0.391333 (at node 171)
Accepted flit rate average= 0.268148
	minimum = 0.200333 (at node 104)
	maximum = 0.349167 (at node 138)
Injected packet length average = 18.0038
Accepted packet length average = 17.9921
Total in-flight flits = 151471 (151471 measured)
latency change    = 0.0911481
throughput change = 0.000879876
Class 0:
Packet latency average = 5621.63
	minimum = 701
	maximum = 8457
Network latency average = 2621.46
	minimum = 22
	maximum = 5992
Slowest packet = 16981
Flit latency average = 2734.01
	minimum = 5
	maximum = 6251
Slowest flit = 258893
Fragmentation average = 18.7285
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0149859
	minimum = 0.00871429 (at node 48)
	maximum = 0.0211429 (at node 150)
Accepted packet rate average = 0.0148743
	minimum = 0.0117143 (at node 79)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.269694
	minimum = 0.156857 (at node 48)
	maximum = 0.38 (at node 150)
Accepted flit rate average= 0.267767
	minimum = 0.210429 (at node 79)
	maximum = 0.340571 (at node 138)
Injected packet length average = 17.9966
Accepted packet length average = 18.0021
Total in-flight flits = 151007 (151007 measured)
latency change    = 0.0747895
throughput change = 0.00142409
Draining all recorded packets ...
Class 0:
Remaining flits: 425988 425989 425990 425991 425992 425993 425994 425995 425996 425997 [...] (153575 flits)
Measured flits: 425988 425989 425990 425991 425992 425993 425994 425995 425996 425997 [...] (153575 flits)
Class 0:
Remaining flits: 458370 458371 458372 458373 458374 458375 458376 458377 458378 458379 [...] (148414 flits)
Measured flits: 458370 458371 458372 458373 458374 458375 458376 458377 458378 458379 [...] (148414 flits)
Class 0:
Remaining flits: 509580 509581 509582 509583 509584 509585 509586 509587 509588 509589 [...] (150429 flits)
Measured flits: 509580 509581 509582 509583 509584 509585 509586 509587 509588 509589 [...] (150429 flits)
Class 0:
Remaining flits: 511038 511039 511040 511041 511042 511043 511044 511045 511046 511047 [...] (147601 flits)
Measured flits: 511038 511039 511040 511041 511042 511043 511044 511045 511046 511047 [...] (147601 flits)
Class 0:
Remaining flits: 631044 631045 631046 631047 631048 631049 631050 631051 631052 631053 [...] (147129 flits)
Measured flits: 631044 631045 631046 631047 631048 631049 631050 631051 631052 631053 [...] (147129 flits)
Class 0:
Remaining flits: 643464 643465 643466 643467 643468 643469 643470 643471 643472 643473 [...] (146230 flits)
Measured flits: 643464 643465 643466 643467 643468 643469 643470 643471 643472 643473 [...] (146230 flits)
Class 0:
Remaining flits: 722484 722485 722486 722487 722488 722489 722490 722491 722492 722493 [...] (142865 flits)
Measured flits: 722484 722485 722486 722487 722488 722489 722490 722491 722492 722493 [...] (142865 flits)
Class 0:
Remaining flits: 759654 759655 759656 759657 759658 759659 759660 759661 759662 759663 [...] (139763 flits)
Measured flits: 759654 759655 759656 759657 759658 759659 759660 759661 759662 759663 [...] (139763 flits)
Class 0:
Remaining flits: 789570 789571 789572 789573 789574 789575 789576 789577 789578 789579 [...] (139998 flits)
Measured flits: 789570 789571 789572 789573 789574 789575 789576 789577 789578 789579 [...] (139998 flits)
Class 0:
Remaining flits: 796176 796177 796178 796179 796180 796181 796182 796183 796184 796185 [...] (139985 flits)
Measured flits: 796176 796177 796178 796179 796180 796181 796182 796183 796184 796185 [...] (139985 flits)
Class 0:
Remaining flits: 860850 860851 860852 860853 860854 860855 860856 860857 860858 860859 [...] (141768 flits)
Measured flits: 860850 860851 860852 860853 860854 860855 860856 860857 860858 860859 [...] (141768 flits)
Class 0:
Remaining flits: 860850 860851 860852 860853 860854 860855 860856 860857 860858 860859 [...] (141288 flits)
Measured flits: 860850 860851 860852 860853 860854 860855 860856 860857 860858 860859 [...] (141288 flits)
Class 0:
Remaining flits: 969185 969186 969187 969188 969189 969190 969191 984870 984871 984872 [...] (138930 flits)
Measured flits: 969185 969186 969187 969188 969189 969190 969191 984870 984871 984872 [...] (138930 flits)
Class 0:
Remaining flits: 989802 989803 989804 989805 989806 989807 989808 989809 989810 989811 [...] (138303 flits)
Measured flits: 989802 989803 989804 989805 989806 989807 989808 989809 989810 989811 [...] (138303 flits)
Class 0:
Remaining flits: 1081008 1081009 1081010 1081011 1081012 1081013 1081014 1081015 1081016 1081017 [...] (140834 flits)
Measured flits: 1081008 1081009 1081010 1081011 1081012 1081013 1081014 1081015 1081016 1081017 [...] (140834 flits)
Class 0:
Remaining flits: 1085742 1085743 1085744 1085745 1085746 1085747 1085748 1085749 1085750 1085751 [...] (140468 flits)
Measured flits: 1085742 1085743 1085744 1085745 1085746 1085747 1085748 1085749 1085750 1085751 [...] (140432 flits)
Class 0:
Remaining flits: 1171324 1171325 1171326 1171327 1171328 1171329 1171330 1171331 1197504 1197505 [...] (139710 flits)
Measured flits: 1171324 1171325 1171326 1171327 1171328 1171329 1171330 1171331 1197504 1197505 [...] (139584 flits)
Class 0:
Remaining flits: 1233072 1233073 1233074 1233075 1233076 1233077 1233078 1233079 1233080 1233081 [...] (138761 flits)
Measured flits: 1233072 1233073 1233074 1233075 1233076 1233077 1233078 1233079 1233080 1233081 [...] (138024 flits)
Class 0:
Remaining flits: 1284390 1284391 1284392 1284393 1284394 1284395 1284396 1284397 1284398 1284399 [...] (140088 flits)
Measured flits: 1284390 1284391 1284392 1284393 1284394 1284395 1284396 1284397 1284398 1284399 [...] (138756 flits)
Class 0:
Remaining flits: 1320318 1320319 1320320 1320321 1320322 1320323 1320324 1320325 1320326 1320327 [...] (138497 flits)
Measured flits: 1320318 1320319 1320320 1320321 1320322 1320323 1320324 1320325 1320326 1320327 [...] (135185 flits)
Class 0:
Remaining flits: 1326780 1326781 1326782 1326783 1326784 1326785 1326786 1326787 1326788 1326789 [...] (145004 flits)
Measured flits: 1326780 1326781 1326782 1326783 1326784 1326785 1326786 1326787 1326788 1326789 [...] (137261 flits)
Class 0:
Remaining flits: 1374570 1374571 1374572 1374573 1374574 1374575 1374576 1374577 1374578 1374579 [...] (140923 flits)
Measured flits: 1374570 1374571 1374572 1374573 1374574 1374575 1374576 1374577 1374578 1374579 [...] (126802 flits)
Class 0:
Remaining flits: 1433796 1433797 1433798 1433799 1433800 1433801 1433802 1433803 1433804 1433805 [...] (141183 flits)
Measured flits: 1433796 1433797 1433798 1433799 1433800 1433801 1433802 1433803 1433804 1433805 [...] (118106 flits)
Class 0:
Remaining flits: 1520910 1520911 1520912 1520913 1520914 1520915 1520916 1520917 1520918 1520919 [...] (141596 flits)
Measured flits: 1520910 1520911 1520912 1520913 1520914 1520915 1520916 1520917 1520918 1520919 [...] (106420 flits)
Class 0:
Remaining flits: 1527336 1527337 1527338 1527339 1527340 1527341 1527342 1527343 1527344 1527345 [...] (142162 flits)
Measured flits: 1527336 1527337 1527338 1527339 1527340 1527341 1527342 1527343 1527344 1527345 [...] (90462 flits)
Class 0:
Remaining flits: 1631178 1631179 1631180 1631181 1631182 1631183 1631184 1631185 1631186 1631187 [...] (142676 flits)
Measured flits: 1631178 1631179 1631180 1631181 1631182 1631183 1631184 1631185 1631186 1631187 [...] (74121 flits)
Class 0:
Remaining flits: 1673496 1673497 1673498 1673499 1673500 1673501 1673502 1673503 1673504 1673505 [...] (144300 flits)
Measured flits: 1673496 1673497 1673498 1673499 1673500 1673501 1673502 1673503 1673504 1673505 [...] (56508 flits)
Class 0:
Remaining flits: 1700982 1700983 1700984 1700985 1700986 1700987 1700988 1700989 1700990 1700991 [...] (143156 flits)
Measured flits: 1700982 1700983 1700984 1700985 1700986 1700987 1700988 1700989 1700990 1700991 [...] (42193 flits)
Class 0:
Remaining flits: 1815066 1815067 1815068 1815069 1815070 1815071 1815072 1815073 1815074 1815075 [...] (143623 flits)
Measured flits: 1815066 1815067 1815068 1815069 1815070 1815071 1815072 1815073 1815074 1815075 [...] (29596 flits)
Class 0:
Remaining flits: 1816956 1816957 1816958 1816959 1816960 1816961 1816962 1816963 1816964 1816965 [...] (142734 flits)
Measured flits: 1816956 1816957 1816958 1816959 1816960 1816961 1816962 1816963 1816964 1816965 [...] (17605 flits)
Class 0:
Remaining flits: 1823652 1823653 1823654 1823655 1823656 1823657 1823658 1823659 1823660 1823661 [...] (143909 flits)
Measured flits: 1823652 1823653 1823654 1823655 1823656 1823657 1823658 1823659 1823660 1823661 [...] (12549 flits)
Class 0:
Remaining flits: 1934910 1934911 1934912 1934913 1934914 1934915 1934916 1934917 1934918 1934919 [...] (145355 flits)
Measured flits: 1934910 1934911 1934912 1934913 1934914 1934915 1934916 1934917 1934918 1934919 [...] (8379 flits)
Class 0:
Remaining flits: 1959878 1959879 1959880 1959881 1959882 1959883 1959884 1959885 1959886 1959887 [...] (145369 flits)
Measured flits: 2009826 2009827 2009828 2009829 2009830 2009831 2009832 2009833 2009834 2009835 [...] (4031 flits)
Class 0:
Remaining flits: 1993968 1993969 1993970 1993971 1993972 1993973 1993974 1993975 1993976 1993977 [...] (145527 flits)
Measured flits: 2091924 2091925 2091926 2091927 2091928 2091929 2091930 2091931 2091932 2091933 [...] (1821 flits)
Class 0:
Remaining flits: 2090970 2090971 2090972 2090973 2090974 2090975 2090976 2090977 2090978 2090979 [...] (144214 flits)
Measured flits: 2094425 2182986 2182987 2182988 2182989 2182990 2182991 2182992 2182993 2182994 [...] (970 flits)
Class 0:
Remaining flits: 2115630 2115631 2115632 2115633 2115634 2115635 2115636 2115637 2115638 2115639 [...] (145277 flits)
Measured flits: 2199276 2199277 2199278 2199279 2199280 2199281 2199282 2199283 2199284 2199285 [...] (547 flits)
Class 0:
Remaining flits: 2215668 2215669 2215670 2215671 2215672 2215673 2225394 2225395 2225396 2225397 [...] (144221 flits)
Measured flits: 2246814 2246815 2246816 2246817 2246818 2246819 2246820 2246821 2246822 2246823 [...] (234 flits)
Class 0:
Remaining flits: 2264184 2264185 2264186 2264187 2264188 2264189 2264190 2264191 2264192 2264193 [...] (143636 flits)
Measured flits: 2389698 2389699 2389700 2389701 2389702 2389703 2389704 2389705 2389706 2389707 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2333142 2333143 2333144 2333145 2333146 2333147 2333148 2333149 2333150 2333151 [...] (93004 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2343942 2343943 2343944 2343945 2343946 2343947 2343948 2343949 2343950 2343951 [...] (44611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2421216 2421217 2421218 2421219 2421220 2421221 2421222 2421223 2421224 2421225 [...] (7057 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2577402 2577403 2577404 2577405 2577406 2577407 2577408 2577409 2577410 2577411 [...] (142 flits)
Measured flits: (0 flits)
Time taken is 53003 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16091 (1 samples)
	minimum = 701 (1 samples)
	maximum = 38873 (1 samples)
Network latency average = 2796.7 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8594 (1 samples)
Flit latency average = 2773.76 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8577 (1 samples)
Fragmentation average = 19.9258 (1 samples)
	minimum = 0 (1 samples)
	maximum = 257 (1 samples)
Injected packet rate average = 0.0149859 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.0211429 (1 samples)
Accepted packet rate average = 0.0148743 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.269694 (1 samples)
	minimum = 0.156857 (1 samples)
	maximum = 0.38 (1 samples)
Accepted flit rate average = 0.267767 (1 samples)
	minimum = 0.210429 (1 samples)
	maximum = 0.340571 (1 samples)
Injected packet size average = 17.9966 (1 samples)
Accepted packet size average = 18.0021 (1 samples)
Hops average = 5.05776 (1 samples)
Total run time 57.0301
