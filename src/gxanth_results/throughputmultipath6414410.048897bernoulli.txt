BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 332.751
	minimum = 27
	maximum = 950
Network latency average = 310.377
	minimum = 24
	maximum = 950
Slowest packet = 201
Flit latency average = 240.067
	minimum = 6
	maximum = 964
Slowest flit = 3011
Fragmentation average = 188.586
	minimum = 0
	maximum = 780
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0114167
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 122)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.245703
	minimum = 0.104 (at node 174)
	maximum = 0.408 (at node 122)
Injected packet length average = 17.8227
Accepted packet length average = 21.5214
Total in-flight flits = 113349 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 604.542
	minimum = 27
	maximum = 1926
Network latency average = 572.156
	minimum = 24
	maximum = 1915
Slowest packet = 244
Flit latency average = 482.384
	minimum = 6
	maximum = 1898
Slowest flit = 9521
Fragmentation average = 261.257
	minimum = 0
	maximum = 1444
Injected packet rate average = 0.0457161
	minimum = 0.0315 (at node 96)
	maximum = 0.0555 (at node 39)
Accepted packet rate average = 0.0127422
	minimum = 0.0065 (at node 174)
	maximum = 0.0195 (at node 14)
Injected flit rate average = 0.819302
	minimum = 0.5665 (at node 96)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.255177
	minimum = 0.135 (at node 174)
	maximum = 0.3745 (at node 14)
Injected packet length average = 17.9215
Accepted packet length average = 20.0262
Total in-flight flits = 218020 (0 measured)
latency change    = 0.449581
throughput change = 0.037127
Class 0:
Packet latency average = 1437.43
	minimum = 30
	maximum = 2931
Network latency average = 1378.12
	minimum = 28
	maximum = 2915
Slowest packet = 627
Flit latency average = 1330.01
	minimum = 6
	maximum = 2898
Slowest flit = 11303
Fragmentation average = 375.282
	minimum = 3
	maximum = 2198
Injected packet rate average = 0.0399635
	minimum = 0.006 (at node 88)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0137969
	minimum = 0.005 (at node 1)
	maximum = 0.024 (at node 37)
Injected flit rate average = 0.719536
	minimum = 0.124 (at node 88)
	maximum = 1 (at node 21)
Accepted flit rate average= 0.250521
	minimum = 0.108 (at node 1)
	maximum = 0.41 (at node 37)
Injected packet length average = 18.0048
Accepted packet length average = 18.1578
Total in-flight flits = 308034 (0 measured)
latency change    = 0.579428
throughput change = 0.0185863
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 544.432
	minimum = 31
	maximum = 2164
Network latency average = 134.083
	minimum = 28
	maximum = 929
Slowest packet = 25302
Flit latency average = 2133.58
	minimum = 6
	maximum = 3794
Slowest flit = 23327
Fragmentation average = 13.0388
	minimum = 4
	maximum = 46
Injected packet rate average = 0.0400885
	minimum = 0.008 (at node 84)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0131667
	minimum = 0.005 (at node 158)
	maximum = 0.022 (at node 58)
Injected flit rate average = 0.720906
	minimum = 0.156 (at node 84)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.237042
	minimum = 0.103 (at node 36)
	maximum = 0.386 (at node 166)
Injected packet length average = 17.9829
Accepted packet length average = 18.0032
Total in-flight flits = 401050 (134735 measured)
latency change    = 1.64023
throughput change = 0.0568641
Class 0:
Packet latency average = 843.293
	minimum = 31
	maximum = 3278
Network latency average = 353.427
	minimum = 28
	maximum = 1927
Slowest packet = 25302
Flit latency average = 2501.62
	minimum = 6
	maximum = 4865
Slowest flit = 17056
Fragmentation average = 18.0866
	minimum = 2
	maximum = 566
Injected packet rate average = 0.0393125
	minimum = 0.009 (at node 140)
	maximum = 0.055 (at node 59)
Accepted packet rate average = 0.013112
	minimum = 0.0065 (at node 79)
	maximum = 0.021 (at node 166)
Injected flit rate average = 0.707474
	minimum = 0.165 (at node 140)
	maximum = 0.9855 (at node 119)
Accepted flit rate average= 0.236797
	minimum = 0.1255 (at node 79)
	maximum = 0.371 (at node 166)
Injected packet length average = 17.9962
Accepted packet length average = 18.0596
Total in-flight flits = 488868 (262755 measured)
latency change    = 0.354397
throughput change = 0.00103376
Class 0:
Packet latency average = 1347.2
	minimum = 28
	maximum = 4406
Network latency average = 678.706
	minimum = 28
	maximum = 2900
Slowest packet = 25302
Flit latency average = 2861.04
	minimum = 6
	maximum = 5811
Slowest flit = 18737
Fragmentation average = 52.8254
	minimum = 2
	maximum = 2134
Injected packet rate average = 0.0342795
	minimum = 0.0103333 (at node 84)
	maximum = 0.0503333 (at node 170)
Accepted packet rate average = 0.0129549
	minimum = 0.00766667 (at node 79)
	maximum = 0.0176667 (at node 114)
Injected flit rate average = 0.617188
	minimum = 0.188 (at node 84)
	maximum = 0.905 (at node 170)
Accepted flit rate average= 0.23334
	minimum = 0.151667 (at node 79)
	maximum = 0.311667 (at node 29)
Injected packet length average = 18.0046
Accepted packet length average = 18.0118
Total in-flight flits = 529202 (340489 measured)
latency change    = 0.374041
throughput change = 0.0148135
Class 0:
Packet latency average = 2028.34
	minimum = 28
	maximum = 5247
Network latency average = 1121.67
	minimum = 28
	maximum = 3964
Slowest packet = 25302
Flit latency average = 3212.91
	minimum = 6
	maximum = 6754
Slowest flit = 30545
Fragmentation average = 91.6169
	minimum = 1
	maximum = 2134
Injected packet rate average = 0.0298242
	minimum = 0.0115 (at node 140)
	maximum = 0.0445 (at node 185)
Accepted packet rate average = 0.0128828
	minimum = 0.008 (at node 79)
	maximum = 0.0165 (at node 29)
Injected flit rate average = 0.536909
	minimum = 0.209 (at node 140)
	maximum = 0.79875 (at node 185)
Accepted flit rate average= 0.231443
	minimum = 0.15225 (at node 79)
	maximum = 0.29475 (at node 29)
Injected packet length average = 18.0024
Accepted packet length average = 17.9652
Total in-flight flits = 542684 (389190 measured)
latency change    = 0.335811
throughput change = 0.00819887
Draining remaining packets ...
Class 0:
Remaining flits: 15133 15134 15135 15136 15137 23272 23273 33448 33449 33450 [...] (502157 flits)
Measured flits: 454122 454123 454124 454125 454126 454127 454128 454129 454130 454131 [...] (381051 flits)
Class 0:
Remaining flits: 33448 33449 33450 33451 33452 33453 33454 33455 33456 33457 [...] (461761 flits)
Measured flits: 454122 454123 454124 454125 454126 454127 454128 454129 454130 454131 [...] (369404 flits)
Class 0:
Remaining flits: 34272 34273 34274 34275 34276 34277 34278 34279 34280 34281 [...] (421491 flits)
Measured flits: 454122 454123 454124 454125 454126 454127 454128 454129 454130 454131 [...] (351761 flits)
Class 0:
Remaining flits: 34272 34273 34274 34275 34276 34277 34278 34279 34280 34281 [...] (381693 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (327902 flits)
Class 0:
Remaining flits: 35712 35713 35714 35715 35716 35717 35718 35719 35720 35721 [...] (342706 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (299688 flits)
Class 0:
Remaining flits: 35712 35713 35714 35715 35716 35717 35718 35719 35720 35721 [...] (305210 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (270689 flits)
Class 0:
Remaining flits: 49896 49897 49898 49899 49900 49901 49902 49903 49904 49905 [...] (270059 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (241438 flits)
Class 0:
Remaining flits: 61249 61250 61251 61252 61253 67674 67675 67676 67677 67678 [...] (235813 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (212215 flits)
Class 0:
Remaining flits: 61249 61250 61251 61252 61253 69354 69355 69356 69357 69358 [...] (201155 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (181969 flits)
Class 0:
Remaining flits: 69354 69355 69356 69357 69358 69359 69360 69361 69362 69363 [...] (167722 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (152258 flits)
Class 0:
Remaining flits: 69354 69355 69356 69357 69358 69359 69360 69361 69362 69363 [...] (135453 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (123602 flits)
Class 0:
Remaining flits: 83934 83935 83936 83937 83938 83939 83940 83941 83942 83943 [...] (103678 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (95275 flits)
Class 0:
Remaining flits: 83934 83935 83936 83937 83938 83939 83940 83941 83942 83943 [...] (72639 flits)
Measured flits: 454284 454285 454286 454287 454288 454289 454290 454291 454292 454293 [...] (66970 flits)
Class 0:
Remaining flits: 83934 83935 83936 83937 83938 83939 83940 83941 83942 83943 [...] (44420 flits)
Measured flits: 454860 454861 454862 454863 454864 454865 454866 454867 454868 454869 [...] (40844 flits)
Class 0:
Remaining flits: 83934 83935 83936 83937 83938 83939 83940 83941 83942 83943 [...] (22684 flits)
Measured flits: 454874 454875 454876 454877 455058 455059 455060 455061 455062 455063 [...] (20729 flits)
Class 0:
Remaining flits: 108496 108497 108498 108499 108500 108501 108502 108503 183391 183392 [...] (6900 flits)
Measured flits: 455832 455833 455834 455835 455836 455837 455838 455839 455840 455841 [...] (6395 flits)
Class 0:
Remaining flits: 475596 475597 475598 475599 475600 475601 475602 475603 475604 475605 [...] (970 flits)
Measured flits: 475596 475597 475598 475599 475600 475601 475602 475603 475604 475605 [...] (970 flits)
Time taken is 24978 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10947.1 (1 samples)
	minimum = 28 (1 samples)
	maximum = 21338 (1 samples)
Network latency average = 10502.2 (1 samples)
	minimum = 28 (1 samples)
	maximum = 21199 (1 samples)
Flit latency average = 8552.04 (1 samples)
	minimum = 6 (1 samples)
	maximum = 22364 (1 samples)
Fragmentation average = 242.72 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8275 (1 samples)
Injected packet rate average = 0.0298242 (1 samples)
	minimum = 0.0115 (1 samples)
	maximum = 0.0445 (1 samples)
Accepted packet rate average = 0.0128828 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0165 (1 samples)
Injected flit rate average = 0.536909 (1 samples)
	minimum = 0.209 (1 samples)
	maximum = 0.79875 (1 samples)
Accepted flit rate average = 0.231443 (1 samples)
	minimum = 0.15225 (1 samples)
	maximum = 0.29475 (1 samples)
Injected packet size average = 18.0024 (1 samples)
Accepted packet size average = 17.9652 (1 samples)
Hops average = 5.19737 (1 samples)
Total run time 41.3036
