BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 270.544
	minimum = 22
	maximum = 781
Network latency average = 262.438
	minimum = 22
	maximum = 762
Slowest packet = 1150
Flit latency average = 238.652
	minimum = 5
	maximum = 745
Slowest flit = 20484
Fragmentation average = 21.8091
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0141042
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.260042
	minimum = 0.105 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.4372
Total in-flight flits = 49720 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 493.897
	minimum = 22
	maximum = 1469
Network latency average = 484.989
	minimum = 22
	maximum = 1369
Slowest packet = 2856
Flit latency average = 460.776
	minimum = 5
	maximum = 1352
Slowest flit = 51425
Fragmentation average = 22.8489
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.014875
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.270604
	minimum = 0.1565 (at node 153)
	maximum = 0.3975 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 18.1919
Total in-flight flits = 95150 (0 measured)
latency change    = 0.452226
throughput change = 0.039033
Class 0:
Packet latency average = 1111.95
	minimum = 22
	maximum = 1906
Network latency average = 1103.23
	minimum = 22
	maximum = 1889
Slowest packet = 5482
Flit latency average = 1082.53
	minimum = 5
	maximum = 1923
Slowest flit = 107082
Fragmentation average = 23.6114
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0287708
	minimum = 0.016 (at node 65)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0159635
	minimum = 0.007 (at node 113)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.518208
	minimum = 0.288 (at node 65)
	maximum = 0.818 (at node 69)
Accepted flit rate average= 0.287859
	minimum = 0.134 (at node 113)
	maximum = 0.497 (at node 34)
Injected packet length average = 18.0116
Accepted packet length average = 18.0323
Total in-flight flits = 139313 (0 measured)
latency change    = 0.555826
throughput change = 0.0599432
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 49.4592
	minimum = 22
	maximum = 138
Network latency average = 39.4989
	minimum = 22
	maximum = 128
Slowest packet = 17612
Flit latency average = 1487.49
	minimum = 5
	maximum = 2517
Slowest flit = 145815
Fragmentation average = 5.22737
	minimum = 0
	maximum = 29
Injected packet rate average = 0.029625
	minimum = 0.013 (at node 119)
	maximum = 0.045 (at node 150)
Accepted packet rate average = 0.0159896
	minimum = 0.008 (at node 4)
	maximum = 0.026 (at node 78)
Injected flit rate average = 0.532229
	minimum = 0.234 (at node 119)
	maximum = 0.815 (at node 150)
Accepted flit rate average= 0.28837
	minimum = 0.144 (at node 4)
	maximum = 0.453 (at node 16)
Injected packet length average = 17.9655
Accepted packet length average = 18.0349
Total in-flight flits = 186330 (94078 measured)
latency change    = 21.4821
throughput change = 0.00177001
Class 0:
Packet latency average = 55.3879
	minimum = 22
	maximum = 1921
Network latency average = 45.9507
	minimum = 22
	maximum = 1921
Slowest packet = 16727
Flit latency average = 1701.49
	minimum = 5
	maximum = 3171
Slowest flit = 180466
Fragmentation average = 5.24888
	minimum = 0
	maximum = 72
Injected packet rate average = 0.0291536
	minimum = 0.0185 (at node 23)
	maximum = 0.0385 (at node 13)
Accepted packet rate average = 0.0158568
	minimum = 0.009 (at node 88)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.524307
	minimum = 0.333 (at node 109)
	maximum = 0.689 (at node 22)
Accepted flit rate average= 0.285513
	minimum = 0.162 (at node 88)
	maximum = 0.4295 (at node 128)
Injected packet length average = 17.9843
Accepted packet length average = 18.0057
Total in-flight flits = 231186 (185337 measured)
latency change    = 0.10704
throughput change = 0.0100057
Class 0:
Packet latency average = 651.128
	minimum = 22
	maximum = 3000
Network latency average = 642.034
	minimum = 22
	maximum = 2989
Slowest packet = 16646
Flit latency average = 1908.25
	minimum = 5
	maximum = 3750
Slowest flit = 205181
Fragmentation average = 9.38964
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0291406
	minimum = 0.0196667 (at node 115)
	maximum = 0.0373333 (at node 136)
Accepted packet rate average = 0.0159375
	minimum = 0.01 (at node 88)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.524429
	minimum = 0.354 (at node 115)
	maximum = 0.672 (at node 136)
Accepted flit rate average= 0.286698
	minimum = 0.18 (at node 88)
	maximum = 0.400333 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 17.9889
Total in-flight flits = 276305 (269557 measured)
latency change    = 0.914936
throughput change = 0.00413291
Draining remaining packets ...
Class 0:
Remaining flits: 282384 282385 282386 282387 282388 282389 282390 282391 282392 282393 [...] (229006 flits)
Measured flits: 299556 299557 299558 299559 299560 299561 299562 299563 299564 299565 [...] (228833 flits)
Class 0:
Remaining flits: 333252 333253 333254 333255 333256 333257 333258 333259 333260 333261 [...] (182210 flits)
Measured flits: 333252 333253 333254 333255 333256 333257 333258 333259 333260 333261 [...] (182210 flits)
Class 0:
Remaining flits: 386910 386911 386912 386913 386914 386915 386916 386917 386918 386919 [...] (134930 flits)
Measured flits: 386910 386911 386912 386913 386914 386915 386916 386917 386918 386919 [...] (134930 flits)
Class 0:
Remaining flits: 420012 420013 420014 420015 420016 420017 420018 420019 420020 420021 [...] (87682 flits)
Measured flits: 420012 420013 420014 420015 420016 420017 420018 420019 420020 420021 [...] (87682 flits)
Class 0:
Remaining flits: 453600 453601 453602 453603 453604 453605 453606 453607 453608 453609 [...] (40754 flits)
Measured flits: 453600 453601 453602 453603 453604 453605 453606 453607 453608 453609 [...] (40754 flits)
Class 0:
Remaining flits: 514754 514755 514756 514757 514758 514759 514760 514761 514762 514763 [...] (3419 flits)
Measured flits: 514754 514755 514756 514757 514758 514759 514760 514761 514762 514763 [...] (3419 flits)
Time taken is 12811 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4089.89 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7166 (1 samples)
Network latency average = 4080.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7149 (1 samples)
Flit latency average = 3489.51 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7132 (1 samples)
Fragmentation average = 25.0447 (1 samples)
	minimum = 0 (1 samples)
	maximum = 102 (1 samples)
Injected packet rate average = 0.0291406 (1 samples)
	minimum = 0.0196667 (1 samples)
	maximum = 0.0373333 (1 samples)
Accepted packet rate average = 0.0159375 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0223333 (1 samples)
Injected flit rate average = 0.524429 (1 samples)
	minimum = 0.354 (1 samples)
	maximum = 0.672 (1 samples)
Accepted flit rate average = 0.286698 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.400333 (1 samples)
Injected packet size average = 17.9965 (1 samples)
Accepted packet size average = 17.9889 (1 samples)
Hops average = 5.06661 (1 samples)
Total run time 13.8615
