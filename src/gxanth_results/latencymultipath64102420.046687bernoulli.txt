BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.276
	minimum = 24
	maximum = 909
Network latency average = 300.536
	minimum = 22
	maximum = 835
Slowest packet = 765
Flit latency average = 270.241
	minimum = 5
	maximum = 853
Slowest flit = 13066
Fragmentation average = 81.6687
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0143229
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.285401
	minimum = 0.117 (at node 64)
	maximum = 0.474 (at node 22)
Injected packet length average = 17.8602
Accepted packet length average = 19.9262
Total in-flight flits = 99571 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 598.982
	minimum = 24
	maximum = 1852
Network latency average = 573.508
	minimum = 22
	maximum = 1713
Slowest packet = 765
Flit latency average = 528.029
	minimum = 5
	maximum = 1696
Slowest flit = 41201
Fragmentation average = 134.2
	minimum = 0
	maximum = 824
Injected packet rate average = 0.0457656
	minimum = 0.0315 (at node 10)
	maximum = 0.055 (at node 42)
Accepted packet rate average = 0.0155495
	minimum = 0.009 (at node 164)
	maximum = 0.0225 (at node 70)
Injected flit rate average = 0.820128
	minimum = 0.567 (at node 10)
	maximum = 0.9855 (at node 135)
Accepted flit rate average= 0.297234
	minimum = 0.1855 (at node 83)
	maximum = 0.4195 (at node 152)
Injected packet length average = 17.9202
Accepted packet length average = 19.1154
Total in-flight flits = 202194 (0 measured)
latency change    = 0.466968
throughput change = 0.0398115
Class 0:
Packet latency average = 1377.12
	minimum = 22
	maximum = 2565
Network latency average = 1340.73
	minimum = 22
	maximum = 2443
Slowest packet = 3195
Flit latency average = 1291.97
	minimum = 5
	maximum = 2519
Slowest flit = 71154
Fragmentation average = 233.821
	minimum = 0
	maximum = 951
Injected packet rate average = 0.0462552
	minimum = 0.029 (at node 100)
	maximum = 0.056 (at node 71)
Accepted packet rate average = 0.0167865
	minimum = 0.008 (at node 146)
	maximum = 0.028 (at node 119)
Injected flit rate average = 0.832729
	minimum = 0.527 (at node 100)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.311276
	minimum = 0.152 (at node 167)
	maximum = 0.545 (at node 119)
Injected packet length average = 18.0029
Accepted packet length average = 18.5433
Total in-flight flits = 302287 (0 measured)
latency change    = 0.565047
throughput change = 0.04511
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 83.7355
	minimum = 23
	maximum = 492
Network latency average = 37.8206
	minimum = 23
	maximum = 188
Slowest packet = 26469
Flit latency average = 1756.57
	minimum = 5
	maximum = 3462
Slowest flit = 78682
Fragmentation average = 7.05548
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0473802
	minimum = 0.031 (at node 118)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.017349
	minimum = 0.005 (at node 4)
	maximum = 0.033 (at node 83)
Injected flit rate average = 0.85249
	minimum = 0.541 (at node 158)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.320573
	minimum = 0.109 (at node 176)
	maximum = 0.649 (at node 83)
Injected packet length average = 17.9925
Accepted packet length average = 18.4779
Total in-flight flits = 404483 (149615 measured)
latency change    = 15.4461
throughput change = 0.0290008
Class 0:
Packet latency average = 85.3734
	minimum = 23
	maximum = 492
Network latency average = 39.0386
	minimum = 23
	maximum = 315
Slowest packet = 26469
Flit latency average = 2018.76
	minimum = 5
	maximum = 4423
Slowest flit = 84462
Fragmentation average = 6.53985
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0469505
	minimum = 0.035 (at node 31)
	maximum = 0.0555 (at node 92)
Accepted packet rate average = 0.0173464
	minimum = 0.0095 (at node 4)
	maximum = 0.0255 (at node 83)
Injected flit rate average = 0.845125
	minimum = 0.628 (at node 31)
	maximum = 1 (at node 109)
Accepted flit rate average= 0.319836
	minimum = 0.187 (at node 4)
	maximum = 0.4965 (at node 83)
Injected packet length average = 18.0003
Accepted packet length average = 18.4382
Total in-flight flits = 503992 (296370 measured)
latency change    = 0.0191852
throughput change = 0.00230424
Class 0:
Packet latency average = 85.7546
	minimum = 23
	maximum = 492
Network latency average = 40.1856
	minimum = 23
	maximum = 352
Slowest packet = 26469
Flit latency average = 2279.05
	minimum = 5
	maximum = 5142
Slowest flit = 129486
Fragmentation average = 6.58586
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0468837
	minimum = 0.036 (at node 31)
	maximum = 0.0553333 (at node 171)
Accepted packet rate average = 0.0173212
	minimum = 0.012 (at node 4)
	maximum = 0.0246667 (at node 83)
Injected flit rate average = 0.843799
	minimum = 0.644 (at node 31)
	maximum = 0.997333 (at node 171)
Accepted flit rate average= 0.318179
	minimum = 0.226 (at node 101)
	maximum = 0.454667 (at node 83)
Injected packet length average = 17.9977
Accepted packet length average = 18.3693
Total in-flight flits = 605106 (444344 measured)
latency change    = 0.00444478
throughput change = 0.00520813
Class 0:
Packet latency average = 139.676
	minimum = 23
	maximum = 4087
Network latency average = 92.9704
	minimum = 23
	maximum = 3949
Slowest packet = 26727
Flit latency average = 2552.17
	minimum = 5
	maximum = 6302
Slowest flit = 103239
Fragmentation average = 11.6938
	minimum = 0
	maximum = 654
Injected packet rate average = 0.0468867
	minimum = 0.03725 (at node 31)
	maximum = 0.0555 (at node 171)
Accepted packet rate average = 0.0173633
	minimum = 0.01175 (at node 36)
	maximum = 0.02325 (at node 118)
Injected flit rate average = 0.843893
	minimum = 0.6705 (at node 31)
	maximum = 0.998 (at node 171)
Accepted flit rate average= 0.317501
	minimum = 0.219 (at node 36)
	maximum = 0.41975 (at node 118)
Injected packet length average = 17.9986
Accepted packet length average = 18.2858
Total in-flight flits = 706608 (591252 measured)
latency change    = 0.386047
throughput change = 0.0021339
Class 0:
Packet latency average = 458.387
	minimum = 23
	maximum = 5075
Network latency average = 411.259
	minimum = 22
	maximum = 4961
Slowest packet = 26850
Flit latency average = 2834.97
	minimum = 5
	maximum = 6866
Slowest flit = 158489
Fragmentation average = 41.0373
	minimum = 0
	maximum = 745
Injected packet rate average = 0.0469854
	minimum = 0.0372 (at node 31)
	maximum = 0.0542 (at node 23)
Accepted packet rate average = 0.0173729
	minimum = 0.0134 (at node 23)
	maximum = 0.0228 (at node 129)
Injected flit rate average = 0.845727
	minimum = 0.6694 (at node 31)
	maximum = 0.9744 (at node 23)
Accepted flit rate average= 0.316656
	minimum = 0.2444 (at node 23)
	maximum = 0.401 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 18.227
Total in-flight flits = 810205 (734681 measured)
latency change    = 0.695288
throughput change = 0.00266867
Class 0:
Packet latency average = 994.566
	minimum = 23
	maximum = 5996
Network latency average = 949.119
	minimum = 22
	maximum = 5995
Slowest packet = 26850
Flit latency average = 3114.79
	minimum = 5
	maximum = 7647
Slowest flit = 206971
Fragmentation average = 82.7447
	minimum = 0
	maximum = 847
Injected packet rate average = 0.0469505
	minimum = 0.0378333 (at node 31)
	maximum = 0.0541667 (at node 23)
Accepted packet rate average = 0.0173715
	minimum = 0.0135 (at node 172)
	maximum = 0.022 (at node 118)
Injected flit rate average = 0.845043
	minimum = 0.6795 (at node 31)
	maximum = 0.973333 (at node 23)
Accepted flit rate average= 0.316168
	minimum = 0.248167 (at node 172)
	maximum = 0.3965 (at node 118)
Injected packet length average = 17.9986
Accepted packet length average = 18.2003
Total in-flight flits = 911628 (868674 measured)
latency change    = 0.539109
throughput change = 0.00154575
Class 0:
Packet latency average = 1711.98
	minimum = 23
	maximum = 7226
Network latency average = 1667.8
	minimum = 22
	maximum = 6912
Slowest packet = 26653
Flit latency average = 3388.71
	minimum = 5
	maximum = 8597
Slowest flit = 215967
Fragmentation average = 131.665
	minimum = 0
	maximum = 847
Injected packet rate average = 0.046849
	minimum = 0.0394286 (at node 31)
	maximum = 0.0534286 (at node 34)
Accepted packet rate average = 0.0174077
	minimum = 0.0138571 (at node 80)
	maximum = 0.0212857 (at node 118)
Injected flit rate average = 0.84326
	minimum = 0.708857 (at node 31)
	maximum = 0.960286 (at node 34)
Accepted flit rate average= 0.31644
	minimum = 0.254714 (at node 49)
	maximum = 0.389 (at node 118)
Injected packet length average = 17.9995
Accepted packet length average = 18.1781
Total in-flight flits = 1010362 (990441 measured)
latency change    = 0.419054
throughput change = 0.000860187
Draining all recorded packets ...
Class 0:
Remaining flits: 231426 231427 231428 231429 231430 231431 231432 231433 231434 231435 [...] (1109599 flits)
Measured flits: 476205 476206 476207 476334 476335 476336 476337 476338 476339 476340 [...] (962458 flits)
Class 0:
Remaining flits: 348912 348913 348914 348915 348916 348917 348918 348919 348920 348921 [...] (1205866 flits)
Measured flits: 476568 476569 476570 476571 476572 476573 476574 476575 476576 476577 [...] (919802 flits)
Class 0:
Remaining flits: 403362 403363 403364 403365 403366 403367 403368 403369 403370 403371 [...] (1307857 flits)
Measured flits: 476580 476581 476582 476583 476584 476585 479016 479017 479018 479019 [...] (873690 flits)
Class 0:
Remaining flits: 432423 432424 432425 432426 432427 432428 432429 432430 432431 458514 [...] (1409751 flits)
Measured flits: 479016 479017 479018 479019 479020 479021 479022 479023 479024 479025 [...] (826516 flits)
Class 0:
Remaining flits: 458527 458528 458529 458530 458531 461358 461359 461360 461361 461362 [...] (1510576 flits)
Measured flits: 479029 479030 479031 479032 479033 484902 484903 484904 484905 484906 [...] (779389 flits)
Class 0:
Remaining flits: 464384 464385 464386 464387 464388 464389 464390 464391 464392 464393 [...] (1614345 flits)
Measured flits: 484902 484903 484904 484905 484906 484907 484908 484909 484910 484911 [...] (731980 flits)
Class 0:
Remaining flits: 484910 484911 484912 484913 484914 484915 484916 484917 484918 484919 [...] (1710370 flits)
Measured flits: 484910 484911 484912 484913 484914 484915 484916 484917 484918 484919 [...] (684648 flits)
Class 0:
Remaining flits: 534718 534719 534720 534721 534722 534723 534724 534725 546450 546451 [...] (1807071 flits)
Measured flits: 534718 534719 534720 534721 534722 534723 534724 534725 546450 546451 [...] (637580 flits)
Class 0:
Remaining flits: 572645 572646 572647 572648 572649 572650 572651 575550 575551 575552 [...] (1899193 flits)
Measured flits: 572645 572646 572647 572648 572649 572650 572651 575550 575551 575552 [...] (590248 flits)
Class 0:
Remaining flits: 658458 658459 658460 658461 658462 658463 658464 658465 658466 658467 [...] (1987900 flits)
Measured flits: 658458 658459 658460 658461 658462 658463 658464 658465 658466 658467 [...] (542663 flits)
Class 0:
Remaining flits: 672689 672690 672691 672692 672693 672694 672695 710099 719460 719461 [...] (2070331 flits)
Measured flits: 672689 672690 672691 672692 672693 672694 672695 710099 719460 719461 [...] (494813 flits)
Class 0:
Remaining flits: 719460 719461 719462 719463 719464 719465 719466 719467 719468 719469 [...] (2153289 flits)
Measured flits: 719460 719461 719462 719463 719464 719465 719466 719467 719468 719469 [...] (446933 flits)
Class 0:
Remaining flits: 736853 736854 736855 736856 736857 736858 736859 736860 736861 736862 [...] (2236292 flits)
Measured flits: 736853 736854 736855 736856 736857 736858 736859 736860 736861 736862 [...] (399569 flits)
Class 0:
Remaining flits: 754487 792588 792589 792590 792591 792592 792593 830664 830665 830666 [...] (2319519 flits)
Measured flits: 754487 792588 792589 792590 792591 792592 792593 830664 830665 830666 [...] (352814 flits)
Class 0:
Remaining flits: 849328 849329 862214 862215 862216 862217 865092 865093 865094 865095 [...] (2401026 flits)
Measured flits: 849328 849329 862214 862215 862216 862217 865092 865093 865094 865095 [...] (305936 flits)
Class 0:
Remaining flits: 882900 882901 882902 882903 882904 882905 882906 882907 882908 882909 [...] (2485939 flits)
Measured flits: 882900 882901 882902 882903 882904 882905 882906 882907 882908 882909 [...] (260544 flits)
Class 0:
Remaining flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (2569447 flits)
Measured flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (217177 flits)
Class 0:
Remaining flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (2653155 flits)
Measured flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (177267 flits)
Class 0:
Remaining flits: 943102 943103 943104 943105 943106 943107 943108 943109 979236 979237 [...] (2735806 flits)
Measured flits: 943102 943103 943104 943105 943106 943107 943108 943109 979236 979237 [...] (141766 flits)
Class 0:
Remaining flits: 1040076 1040077 1040078 1040079 1040080 1040081 1040082 1040083 1040084 1040085 [...] (2817769 flits)
Measured flits: 1040076 1040077 1040078 1040079 1040080 1040081 1040082 1040083 1040084 1040085 [...] (111437 flits)
Class 0:
Remaining flits: 1071936 1071937 1071938 1071939 1071940 1071941 1071942 1071943 1071944 1071945 [...] (2900991 flits)
Measured flits: 1071936 1071937 1071938 1071939 1071940 1071941 1071942 1071943 1071944 1071945 [...] (85610 flits)
Class 0:
Remaining flits: 1071953 1086498 1086499 1086500 1086501 1086502 1086503 1086504 1086505 1086506 [...] (2987160 flits)
Measured flits: 1071953 1086498 1086499 1086500 1086501 1086502 1086503 1086504 1086505 1086506 [...] (64230 flits)
Class 0:
Remaining flits: 1086498 1086499 1086500 1086501 1086502 1086503 1086504 1086505 1086506 1086507 [...] (3073921 flits)
Measured flits: 1086498 1086499 1086500 1086501 1086502 1086503 1086504 1086505 1086506 1086507 [...] (46490 flits)
Class 0:
Remaining flits: 1110479 1110480 1110481 1110482 1110483 1110484 1110485 1110486 1110487 1110488 [...] (3158078 flits)
Measured flits: 1110479 1110480 1110481 1110482 1110483 1110484 1110485 1110486 1110487 1110488 [...] (32058 flits)
Class 0:
Remaining flits: 1166666 1166667 1166668 1166669 1190583 1190584 1190585 1190586 1190587 1190588 [...] (3244216 flits)
Measured flits: 1166666 1166667 1166668 1166669 1190583 1190584 1190585 1190586 1190587 1190588 [...] (20799 flits)
Class 0:
Remaining flits: 1237140 1237141 1237142 1237143 1237144 1237145 1237146 1237147 1237148 1237149 [...] (3332163 flits)
Measured flits: 1237140 1237141 1237142 1237143 1237144 1237145 1237146 1237147 1237148 1237149 [...] (13190 flits)
Class 0:
Remaining flits: 1281924 1281925 1281926 1281927 1281928 1281929 1281930 1281931 1281932 1281933 [...] (3418814 flits)
Measured flits: 1281924 1281925 1281926 1281927 1281928 1281929 1281930 1281931 1281932 1281933 [...] (7783 flits)
Class 0:
Remaining flits: 1281936 1281937 1281938 1281939 1281940 1281941 1305036 1305037 1305038 1305039 [...] (3505945 flits)
Measured flits: 1281936 1281937 1281938 1281939 1281940 1281941 1305036 1305037 1305038 1305039 [...] (4201 flits)
Class 0:
Remaining flits: 1362506 1362507 1362508 1362509 1385478 1385479 1385480 1385481 1385482 1385483 [...] (3593006 flits)
Measured flits: 1362506 1362507 1362508 1362509 1385478 1385479 1385480 1385481 1385482 1385483 [...] (2353 flits)
Class 0:
Remaining flits: 1409058 1409059 1409060 1409061 1409062 1409063 1409064 1409065 1409066 1409067 [...] (3683379 flits)
Measured flits: 1409058 1409059 1409060 1409061 1409062 1409063 1409064 1409065 1409066 1409067 [...] (1433 flits)
Class 0:
Remaining flits: 1409058 1409059 1409060 1409061 1409062 1409063 1409064 1409065 1409066 1409067 [...] (3774209 flits)
Measured flits: 1409058 1409059 1409060 1409061 1409062 1409063 1409064 1409065 1409066 1409067 [...] (785 flits)
Class 0:
Remaining flits: 1474506 1474507 1474508 1474509 1474510 1474511 1474512 1474513 1474514 1474515 [...] (3866042 flits)
Measured flits: 1474506 1474507 1474508 1474509 1474510 1474511 1474512 1474513 1474514 1474515 [...] (537 flits)
Class 0:
Remaining flits: 1513908 1513909 1513910 1513911 1513912 1513913 1513914 1513915 1513916 1513917 [...] (3953821 flits)
Measured flits: 1513908 1513909 1513910 1513911 1513912 1513913 1513914 1513915 1513916 1513917 [...] (331 flits)
Class 0:
Remaining flits: 1513908 1513909 1513910 1513911 1513912 1513913 1513914 1513915 1513916 1513917 [...] (4039069 flits)
Measured flits: 1513908 1513909 1513910 1513911 1513912 1513913 1513914 1513915 1513916 1513917 [...] (153 flits)
Class 0:
Remaining flits: 1556352 1556353 1556354 1556355 1556356 1556357 1556358 1556359 1556360 1556361 [...] (4126918 flits)
Measured flits: 1556352 1556353 1556354 1556355 1556356 1556357 1556358 1556359 1556360 1556361 [...] (57 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1628583 1628584 1628585 1656666 1656667 1656668 1656669 1656670 1656671 1656672 [...] (4150333 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1656666 1656667 1656668 1656669 1656670 1656671 1656672 1656673 1656674 1656675 [...] (4096832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1708794 1708795 1708796 1708797 1708798 1708799 1708800 1708801 1708802 1708803 [...] (4043731 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1796547 1796548 1796549 1796550 1796551 1796552 1796553 1796554 1796555 1796556 [...] (3991952 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1860112 1860113 1860114 1860115 1860116 1860117 1860118 1860119 1868706 1868707 [...] (3940214 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1875549 1875550 1875551 1875552 1875553 1875554 1875555 1875556 1875557 1875558 [...] (3889471 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1897228 1897229 1897230 1897231 1897232 1897233 1897234 1897235 1902672 1902673 [...] (3838466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1933493 1933494 1933495 1933496 1933497 1933498 1933499 1933500 1933501 1933502 [...] (3788009 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1955967 1955968 1955969 1962558 1962559 1962560 1962561 1962562 1962563 1962564 [...] (3738040 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1974114 1974115 1974116 1974117 1974118 1974119 1974120 1974121 1974122 1974123 [...] (3688548 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2003791 2003792 2003793 2003794 2003795 2037690 2037691 2037692 2037693 2037694 [...] (3639600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2049544 2049545 2049546 2049547 2049548 2049549 2049550 2049551 2124145 2124146 [...] (3591034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147467 2147468 2147469 2147470 2147471 2161926 2161927 2161928 2161929 2161930 [...] (3542653 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2203056 2203057 2203058 2203059 2203060 2203061 2203062 2203063 2203064 2203065 [...] (3494621 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2282068 2282069 2282070 2282071 2282072 2282073 2282074 2282075 2288034 2288035 [...] (3446566 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2288050 2288051 2299788 2299789 2299790 2299791 2299792 2299793 2299794 2299795 [...] (3398428 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366820 2366821 2366822 2366823 2366824 2366825 2366826 2366827 2366828 2366829 [...] (3350465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366820 2366821 2366822 2366823 2366824 2366825 2366826 2366827 2366828 2366829 [...] (3302706 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366826 2366827 2366828 2366829 2366830 2366831 2366832 2366833 2366834 2366835 [...] (3254695 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2496995 2537010 2537011 2537012 2537013 2537014 2537015 2537016 2537017 2537018 [...] (3207189 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2552453 2566944 2566945 2566946 2566947 2566948 2566949 2566950 2566951 2566952 [...] (3159374 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2566958 2566959 2566960 2566961 2578385 2578386 2578387 2578388 2578389 2578390 [...] (3111499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2714157 2714158 2714159 2714160 2714161 2714162 2714163 2714164 2714165 2716218 [...] (3063739 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2716218 2716219 2716220 2716221 2716222 2716223 2716224 2716225 2716226 2716227 [...] (3016391 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2783464 2783465 2785354 2785355 2835698 2835699 2835700 2835701 2853803 2853804 [...] (2968721 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2879856 2879857 2879858 2879859 2879860 2879861 2879862 2879863 2879864 2879865 [...] (2920859 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2918898 2918899 2918900 2918901 2918902 2918903 2918904 2918905 2918906 2918907 [...] (2873146 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3040038 3040039 3040040 3040041 3040042 3040043 3040044 3040045 3040046 3040047 [...] (2825368 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3040047 3040048 3040049 3040050 3040051 3040052 3040053 3040054 3040055 3089194 [...] (2777843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3111346 3111347 3111348 3111349 3111350 3111351 3111352 3111353 3129271 3129272 [...] (2730114 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3180731 3180732 3180733 3180734 3180735 3180736 3180737 3180738 3180739 3180740 [...] (2682393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3193354 3193355 3193356 3193357 3193358 3193359 3193360 3193361 3241062 3241063 [...] (2634545 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3241074 3241075 3241076 3241077 3241078 3241079 3302352 3302353 3302354 3302355 [...] (2586851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3390837 3390838 3390839 3403816 3403817 3409486 3409487 3419082 3419083 3419084 [...] (2538994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3432311 3464545 3464546 3464547 3464548 3464549 3466199 3466200 3466201 3466202 [...] (2491087 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3472090 3472091 3485376 3485377 3485378 3485379 3485380 3485381 3485382 3485383 [...] (2443332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3485390 3485391 3485392 3485393 3511960 3511961 3516936 3516937 3516938 3516939 [...] (2395820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3602846 3602847 3602848 3602849 3602850 3602851 3602852 3602853 3602854 3602855 [...] (2347926 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3602859 3602860 3602861 3642498 3642499 3642500 3642501 3642502 3642503 3642504 [...] (2300058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3712318 3712319 3721390 3721391 3753234 3753235 3753236 3753237 3753238 3753239 [...] (2252219 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3834458 3834459 3834460 3834461 3834462 3834463 3834464 3834465 3834466 3834467 [...] (2204386 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3855076 3855077 3856086 3856087 3856088 3856089 3856090 3856091 3856092 3856093 [...] (2156516 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3872932 3872933 3885154 3885155 3893506 3893507 3905352 3905353 3905354 3905355 [...] (2108631 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3905367 3905368 3905369 3914380 3914381 3914382 3914383 3914384 3914385 3914386 [...] (2060666 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3914386 3914387 3972234 3972235 3972236 3972237 3972238 3972239 3980358 3980359 [...] (2012960 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4015865 4015866 4015867 4015868 4015869 4015870 4015871 4019794 4019795 4035994 [...] (1965014 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4015870 4015871 4037976 4037977 4037978 4037979 4037980 4037981 4037982 4037983 [...] (1917321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4037976 4037977 4037978 4037979 4037980 4037981 4037982 4037983 4037984 4037985 [...] (1869631 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4056298 4056299 4094154 4094155 4094156 4094157 4094158 4094159 4094160 4094161 [...] (1821859 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101892 4101893 4174542 4174543 4174544 4174545 4174546 4174547 4174548 4174549 [...] (1774427 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4174542 4174543 4174544 4174545 4174546 4174547 4174548 4174549 4174550 4174551 [...] (1726848 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4174542 4174543 4174544 4174545 4174546 4174547 4174548 4174549 4174550 4174551 [...] (1679199 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4174542 4174543 4174544 4174545 4174546 4174547 4174548 4174549 4174550 4174551 [...] (1631610 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4299598 4299599 4299600 4299601 4299602 4299603 4299604 4299605 4317786 4317787 [...] (1583835 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4338947 4338948 4338949 4338950 4338951 4338952 4338953 4354578 4354579 4354580 [...] (1536185 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4415976 4415977 4415978 4415979 4415980 4415981 4415982 4415983 4415984 4415985 [...] (1488083 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4425172 4425173 4472674 4472675 4489848 4489849 4489850 4489851 4489852 4489853 [...] (1440388 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4489860 4489861 4489862 4489863 4489864 4489865 4525884 4525885 4525886 4525887 [...] (1393239 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4489864 4489865 4525884 4525885 4525886 4525887 4525888 4525889 4525890 4525891 [...] (1345599 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4566550 4566551 4566552 4566553 4566554 4566555 4566556 4566557 4566558 4566559 [...] (1297987 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4625959 4625960 4625961 4625962 4625963 4644376 4644377 4652944 4652945 4667508 [...] (1250184 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4674958 4674959 4696306 4696307 4698882 4698883 4698884 4698885 4698886 4698887 [...] (1202462 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4704064 4704065 4755438 4755439 4755440 4755441 4755442 4755443 4755444 4755445 [...] (1154209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4770414 4770415 4770416 4770417 4770418 4770419 4770420 4770421 4770422 4770423 [...] (1106220 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4770414 4770415 4770416 4770417 4770418 4770419 4770420 4770421 4770422 4770423 [...] (1058785 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4839372 4839373 4839374 4839375 4839376 4839377 4839378 4839379 4839380 4839381 [...] (1010665 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4839388 4839389 4903997 4903998 4903999 4904000 4904001 4904002 4904003 4904004 [...] (963198 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4943232 4943233 4943234 4943235 4943236 4943237 4943238 4943239 4943240 4943241 [...] (915805 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4943232 4943233 4943234 4943235 4943236 4943237 4943238 4943239 4943240 4943241 [...] (867845 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4943246 4943247 4943248 4943249 4947750 4947751 4947752 4947753 4947754 4947755 [...] (820429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4947750 4947751 4947752 4947753 4947754 4947755 4947756 4947757 4947758 4947759 [...] (772304 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4947766 4947767 5068044 5068045 5068046 5068047 5068048 5068049 5068050 5068051 [...] (724737 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5068044 5068045 5068046 5068047 5068048 5068049 5068050 5068051 5068052 5068053 [...] (676979 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5068060 5068061 5131776 5131777 5131778 5131779 5131780 5131781 5155686 5155687 [...] (629088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5185512 5185513 5185514 5185515 5185516 5185517 5185518 5185519 5185520 5185521 [...] (581296 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5207182 5207183 5251788 5251789 5251790 5251791 5251792 5251793 5251794 5251795 [...] (533492 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5207182 5207183 5251788 5251789 5251790 5251791 5251792 5251793 5251794 5251795 [...] (486021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5207182 5207183 5251801 5251802 5251803 5251804 5251805 5320890 5320891 5320892 [...] (438128 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5207182 5207183 5320890 5320891 5320892 5320893 5320894 5320895 5320896 5320897 [...] (390417 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5320906 5320907 5346000 5346001 5346002 5346003 5346004 5346005 5346006 5346007 [...] (342464 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5346000 5346001 5346002 5346003 5346004 5346005 5346006 5346007 5346008 5346009 [...] (294741 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5346000 5346001 5346002 5346003 5346004 5346005 5346006 5346007 5346008 5346009 [...] (247201 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5444136 5444137 5444138 5444139 5444140 5444141 5444142 5444143 5444144 5444145 [...] (199335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5483392 5483393 5500912 5500913 5500914 5500915 5500916 5500917 5500918 5500919 [...] (151637 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5517576 5517577 5517578 5517579 5517580 5517581 5517582 5517583 5517584 5517585 [...] (104833 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5645176 5645177 5652000 5652001 5652002 5652003 5652004 5652005 5652006 5652007 [...] (61178 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5652000 5652001 5652002 5652003 5652004 5652005 5652006 5652007 5652008 5652009 [...] (28134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5652000 5652001 5652002 5652003 5652004 5652005 5652006 5652007 5652008 5652009 [...] (11012 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6006186 6006187 6006188 6006189 6006190 6006191 6006192 6006193 6006194 6006195 [...] (3177 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6143760 6143761 6143762 6143763 6143764 6143765 6143766 6143767 6143768 6143769 [...] (565 flits)
Measured flits: (0 flits)
Time taken is 136522 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13275.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 36027 (1 samples)
Network latency average = 13228.3 (1 samples)
	minimum = 22 (1 samples)
	maximum = 35988 (1 samples)
Flit latency average = 41778.2 (1 samples)
	minimum = 5 (1 samples)
	maximum = 97344 (1 samples)
Fragmentation average = 430.408 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1018 (1 samples)
Injected packet rate average = 0.046849 (1 samples)
	minimum = 0.0394286 (1 samples)
	maximum = 0.0534286 (1 samples)
Accepted packet rate average = 0.0174077 (1 samples)
	minimum = 0.0138571 (1 samples)
	maximum = 0.0212857 (1 samples)
Injected flit rate average = 0.84326 (1 samples)
	minimum = 0.708857 (1 samples)
	maximum = 0.960286 (1 samples)
Accepted flit rate average = 0.31644 (1 samples)
	minimum = 0.254714 (1 samples)
	maximum = 0.389 (1 samples)
Injected packet size average = 17.9995 (1 samples)
Accepted packet size average = 18.1781 (1 samples)
Hops average = 5.06423 (1 samples)
Total run time 233.831
