BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 337.527
	minimum = 24
	maximum = 980
Network latency average = 232.29
	minimum = 22
	maximum = 756
Slowest packet = 40
Flit latency average = 201.942
	minimum = 5
	maximum = 817
Slowest flit = 14667
Fragmentation average = 43.881
	minimum = 0
	maximum = 338
Injected packet rate average = 0.027625
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0136979
	minimum = 0.005 (at node 41)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.491703
	minimum = 0 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.257193
	minimum = 0.112 (at node 41)
	maximum = 0.414 (at node 48)
Injected packet length average = 17.7992
Accepted packet length average = 18.776
Total in-flight flits = 46091 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.9
	minimum = 24
	maximum = 1967
Network latency average = 423.747
	minimum = 22
	maximum = 1595
Slowest packet = 40
Flit latency average = 391.884
	minimum = 5
	maximum = 1594
Slowest flit = 32966
Fragmentation average = 60.7561
	minimum = 0
	maximum = 403
Injected packet rate average = 0.0292161
	minimum = 0.0035 (at node 37)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0146693
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.523701
	minimum = 0.063 (at node 37)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.272247
	minimum = 0.148 (at node 153)
	maximum = 0.39 (at node 63)
Injected packet length average = 17.925
Accepted packet length average = 18.559
Total in-flight flits = 97399 (0 measured)
latency change    = 0.405658
throughput change = 0.0552978
Class 0:
Packet latency average = 1272.56
	minimum = 25
	maximum = 2846
Network latency average = 1056.16
	minimum = 22
	maximum = 2236
Slowest packet = 2435
Flit latency average = 1020.51
	minimum = 5
	maximum = 2406
Slowest flit = 50169
Fragmentation average = 106.906
	minimum = 0
	maximum = 497
Injected packet rate average = 0.0318437
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.015849
	minimum = 0.007 (at node 185)
	maximum = 0.026 (at node 66)
Injected flit rate average = 0.572703
	minimum = 0 (at node 5)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287484
	minimum = 0.149 (at node 190)
	maximum = 0.499 (at node 99)
Injected packet length average = 17.9848
Accepted packet length average = 18.139
Total in-flight flits = 152254 (0 measured)
latency change    = 0.553735
throughput change = 0.0530011
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 292.171
	minimum = 25
	maximum = 1438
Network latency average = 40.9577
	minimum = 22
	maximum = 386
Slowest packet = 17333
Flit latency average = 1461.37
	minimum = 5
	maximum = 3154
Slowest flit = 76391
Fragmentation average = 6.1069
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0306146
	minimum = 0 (at node 94)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0159792
	minimum = 0.005 (at node 52)
	maximum = 0.028 (at node 39)
Injected flit rate average = 0.550995
	minimum = 0 (at node 94)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.286797
	minimum = 0.125 (at node 52)
	maximum = 0.475 (at node 39)
Injected packet length average = 17.9978
Accepted packet length average = 17.9482
Total in-flight flits = 203101 (97747 measured)
latency change    = 3.35553
throughput change = 0.00239717
Class 0:
Packet latency average = 498.721
	minimum = 25
	maximum = 2367
Network latency average = 244.832
	minimum = 22
	maximum = 1969
Slowest packet = 17333
Flit latency average = 1707.11
	minimum = 5
	maximum = 3937
Slowest flit = 101431
Fragmentation average = 16.1788
	minimum = 0
	maximum = 299
Injected packet rate average = 0.0296849
	minimum = 0.006 (at node 183)
	maximum = 0.0555 (at node 54)
Accepted packet rate average = 0.0157734
	minimum = 0.01 (at node 4)
	maximum = 0.0275 (at node 129)
Injected flit rate average = 0.53418
	minimum = 0.101 (at node 187)
	maximum = 1 (at node 54)
Accepted flit rate average= 0.283667
	minimum = 0.1715 (at node 4)
	maximum = 0.483 (at node 129)
Injected packet length average = 17.995
Accepted packet length average = 17.9838
Total in-flight flits = 248634 (187469 measured)
latency change    = 0.414159
throughput change = 0.0110348
Class 0:
Packet latency average = 1245.07
	minimum = 24
	maximum = 3757
Network latency average = 999.028
	minimum = 22
	maximum = 2974
Slowest packet = 17333
Flit latency average = 1959.89
	minimum = 5
	maximum = 4636
Slowest flit = 112337
Fragmentation average = 40.9346
	minimum = 0
	maximum = 399
Injected packet rate average = 0.0284462
	minimum = 0.008 (at node 0)
	maximum = 0.055 (at node 111)
Accepted packet rate average = 0.0157049
	minimum = 0.0106667 (at node 4)
	maximum = 0.0236667 (at node 129)
Injected flit rate average = 0.512094
	minimum = 0.144 (at node 0)
	maximum = 0.987 (at node 111)
Accepted flit rate average= 0.280995
	minimum = 0.188667 (at node 4)
	maximum = 0.414667 (at node 129)
Injected packet length average = 18.0022
Accepted packet length average = 17.8922
Total in-flight flits = 285637 (257205 measured)
latency change    = 0.599443
throughput change = 0.00950863
Class 0:
Packet latency average = 2029.77
	minimum = 22
	maximum = 4576
Network latency average = 1779.63
	minimum = 22
	maximum = 3953
Slowest packet = 17333
Flit latency average = 2205.11
	minimum = 5
	maximum = 5291
Slowest flit = 154601
Fragmentation average = 55.5427
	minimum = 0
	maximum = 510
Injected packet rate average = 0.0270937
	minimum = 0.009 (at node 96)
	maximum = 0.04425 (at node 111)
Accepted packet rate average = 0.0156185
	minimum = 0.01175 (at node 4)
	maximum = 0.02175 (at node 128)
Injected flit rate average = 0.487677
	minimum = 0.15975 (at node 96)
	maximum = 0.7965 (at node 111)
Accepted flit rate average= 0.279901
	minimum = 0.21125 (at node 4)
	maximum = 0.39025 (at node 128)
Injected packet length average = 17.9996
Accepted packet length average = 17.9211
Total in-flight flits = 312284 (300127 measured)
latency change    = 0.386597
throughput change = 0.00390763
Class 0:
Packet latency average = 2586.25
	minimum = 22
	maximum = 5959
Network latency average = 2327.34
	minimum = 22
	maximum = 4970
Slowest packet = 17333
Flit latency average = 2440.18
	minimum = 5
	maximum = 6086
Slowest flit = 188416
Fragmentation average = 62.1255
	minimum = 0
	maximum = 581
Injected packet rate average = 0.0264312
	minimum = 0.012 (at node 133)
	maximum = 0.0386 (at node 111)
Accepted packet rate average = 0.0155323
	minimum = 0.012 (at node 80)
	maximum = 0.021 (at node 181)
Injected flit rate average = 0.475742
	minimum = 0.2162 (at node 133)
	maximum = 0.6922 (at node 111)
Accepted flit rate average= 0.278504
	minimum = 0.2098 (at node 80)
	maximum = 0.3736 (at node 181)
Injected packet length average = 17.9992
Accepted packet length average = 17.9307
Total in-flight flits = 342108 (337209 measured)
latency change    = 0.215166
throughput change = 0.00501563
Class 0:
Packet latency average = 3034.07
	minimum = 22
	maximum = 6939
Network latency average = 2760.69
	minimum = 22
	maximum = 5971
Slowest packet = 17333
Flit latency average = 2673.24
	minimum = 5
	maximum = 6948
Slowest flit = 190277
Fragmentation average = 64.3812
	minimum = 0
	maximum = 760
Injected packet rate average = 0.0254826
	minimum = 0.0101667 (at node 133)
	maximum = 0.0361667 (at node 138)
Accepted packet rate average = 0.0154748
	minimum = 0.012 (at node 52)
	maximum = 0.0198333 (at node 128)
Injected flit rate average = 0.458782
	minimum = 0.183 (at node 133)
	maximum = 0.651167 (at node 138)
Accepted flit rate average= 0.277235
	minimum = 0.213667 (at node 52)
	maximum = 0.357833 (at node 128)
Injected packet length average = 18.0037
Accepted packet length average = 17.9152
Total in-flight flits = 362061 (360072 measured)
latency change    = 0.147598
throughput change = 0.00457706
Class 0:
Packet latency average = 3411.39
	minimum = 22
	maximum = 7841
Network latency average = 3106.66
	minimum = 22
	maximum = 6823
Slowest packet = 17333
Flit latency average = 2897.46
	minimum = 5
	maximum = 7621
Slowest flit = 243272
Fragmentation average = 64.1949
	minimum = 0
	maximum = 809
Injected packet rate average = 0.0247374
	minimum = 0.0124286 (at node 148)
	maximum = 0.0361429 (at node 69)
Accepted packet rate average = 0.0154182
	minimum = 0.0127143 (at node 104)
	maximum = 0.0197143 (at node 157)
Injected flit rate average = 0.44536
	minimum = 0.223143 (at node 148)
	maximum = 0.650571 (at node 69)
Accepted flit rate average= 0.276517
	minimum = 0.226 (at node 79)
	maximum = 0.350143 (at node 128)
Injected packet length average = 18.0035
Accepted packet length average = 17.9345
Total in-flight flits = 380195 (379380 measured)
latency change    = 0.110606
throughput change = 0.00259705
Draining all recorded packets ...
Class 0:
Remaining flits: 279900 279901 279902 279903 279904 279905 279906 279907 279908 279909 [...] (387190 flits)
Measured flits: 312174 312175 312176 312177 312178 312179 312180 312181 312182 312183 [...] (367842 flits)
Class 0:
Remaining flits: 300168 300169 300170 300171 300172 300173 300174 300175 300176 300177 [...] (394994 flits)
Measured flits: 312182 312183 312184 312185 312186 312187 312188 312189 312190 312191 [...] (348585 flits)
Class 0:
Remaining flits: 331416 331417 331418 331419 331420 331421 331422 331423 331424 331425 [...] (404840 flits)
Measured flits: 331416 331417 331418 331419 331420 331421 331422 331423 331424 331425 [...] (324289 flits)
Class 0:
Remaining flits: 339228 339229 339230 339231 339232 339233 339234 339235 339236 339237 [...] (409587 flits)
Measured flits: 339228 339229 339230 339231 339232 339233 339234 339235 339236 339237 [...] (295344 flits)
Class 0:
Remaining flits: 392904 392905 392906 392907 392908 392909 392910 392911 392912 392913 [...] (410089 flits)
Measured flits: 392904 392905 392906 392907 392908 392909 392910 392911 392912 392913 [...] (266379 flits)
Class 0:
Remaining flits: 409058 409059 409060 409061 409062 409063 409064 409065 409066 409067 [...] (411507 flits)
Measured flits: 409058 409059 409060 409061 409062 409063 409064 409065 409066 409067 [...] (235360 flits)
Class 0:
Remaining flits: 425304 425305 425306 425307 425308 425309 425310 425311 425312 425313 [...] (414125 flits)
Measured flits: 425304 425305 425306 425307 425308 425309 425310 425311 425312 425313 [...] (204933 flits)
Class 0:
Remaining flits: 429912 429913 429914 429915 429916 429917 429918 429919 429920 429921 [...] (415820 flits)
Measured flits: 429912 429913 429914 429915 429916 429917 429918 429919 429920 429921 [...] (176548 flits)
Class 0:
Remaining flits: 512010 512011 512012 512013 512014 512015 512016 512017 512018 512019 [...] (414202 flits)
Measured flits: 512010 512011 512012 512013 512014 512015 512016 512017 512018 512019 [...] (146431 flits)
Class 0:
Remaining flits: 571086 571087 571088 571089 571090 571091 571092 571093 571094 571095 [...] (414968 flits)
Measured flits: 571086 571087 571088 571089 571090 571091 571092 571093 571094 571095 [...] (117674 flits)
Class 0:
Remaining flits: 649638 649639 649640 649641 649642 649643 649644 649645 649646 649647 [...] (419421 flits)
Measured flits: 649638 649639 649640 649641 649642 649643 649644 649645 649646 649647 [...] (92600 flits)
Class 0:
Remaining flits: 658008 658009 658010 658011 658012 658013 658014 658015 658016 658017 [...] (419324 flits)
Measured flits: 658008 658009 658010 658011 658012 658013 658014 658015 658016 658017 [...] (70574 flits)
Class 0:
Remaining flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (417968 flits)
Measured flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (52964 flits)
Class 0:
Remaining flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (419698 flits)
Measured flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (39488 flits)
Class 0:
Remaining flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (418823 flits)
Measured flits: 702180 702181 702182 702183 702184 702185 702186 702187 702188 702189 [...] (28346 flits)
Class 0:
Remaining flits: 931176 931177 931178 931179 931180 931181 931182 931183 931184 931185 [...] (418921 flits)
Measured flits: 931176 931177 931178 931179 931180 931181 931182 931183 931184 931185 [...] (18186 flits)
Class 0:
Remaining flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (422189 flits)
Measured flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (11547 flits)
Class 0:
Remaining flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (420956 flits)
Measured flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (7099 flits)
Class 0:
Remaining flits: 1010718 1010719 1010720 1010721 1010722 1010723 1010724 1010725 1010726 1010727 [...] (421038 flits)
Measured flits: 1010718 1010719 1010720 1010721 1010722 1010723 1010724 1010725 1010726 1010727 [...] (4084 flits)
Class 0:
Remaining flits: 1010722 1010723 1010724 1010725 1010726 1010727 1010728 1010729 1010730 1010731 [...] (421256 flits)
Measured flits: 1010722 1010723 1010724 1010725 1010726 1010727 1010728 1010729 1010730 1010731 [...] (2440 flits)
Class 0:
Remaining flits: 1140390 1140391 1140392 1140393 1140394 1140395 1140396 1140397 1140398 1140399 [...] (422132 flits)
Measured flits: 1169964 1169965 1169966 1169967 1169968 1169969 1169970 1169971 1169972 1169973 [...] (1348 flits)
Class 0:
Remaining flits: 1165752 1165753 1165754 1165755 1165756 1165757 1165758 1165759 1165760 1165761 [...] (423039 flits)
Measured flits: 1357146 1357147 1357148 1357149 1357150 1357151 1357152 1357153 1357154 1357155 [...] (664 flits)
Class 0:
Remaining flits: 1165752 1165753 1165754 1165755 1165756 1165757 1165758 1165759 1165760 1165761 [...] (423518 flits)
Measured flits: 1427528 1427529 1427530 1427531 1427532 1427533 1427534 1427535 1427536 1427537 [...] (423 flits)
Class 0:
Remaining flits: 1254222 1254223 1254224 1254225 1254226 1254227 1254228 1254229 1254230 1254231 [...] (422483 flits)
Measured flits: 1446804 1446805 1446806 1446807 1446808 1446809 1446810 1446811 1446812 1446813 [...] (220 flits)
Class 0:
Remaining flits: 1254222 1254223 1254224 1254225 1254226 1254227 1254228 1254229 1254230 1254231 [...] (425286 flits)
Measured flits: 1446804 1446805 1446806 1446807 1446808 1446809 1446810 1446811 1446812 1446813 [...] (144 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1495692 1495693 1495694 1495695 1495696 1495697 1495698 1495699 1495700 1495701 [...] (372783 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1495692 1495693 1495694 1495695 1495696 1495697 1495698 1495699 1495700 1495701 [...] (322628 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1495692 1495693 1495694 1495695 1495696 1495697 1495698 1495699 1495700 1495701 [...] (272364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1575918 1575919 1575920 1575921 1575922 1575923 1575924 1575925 1575926 1575927 [...] (223520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1675098 1675099 1675100 1675101 1675102 1675103 1675104 1675105 1675106 1675107 [...] (175512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1736201 1736202 1736203 1736204 1736205 1736206 1736207 1762173 1762174 1762175 [...] (127873 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805094 1805095 1805096 1805097 1805098 1805099 1805100 1805101 1805102 1805103 [...] (80381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1885914 1885915 1885916 1885917 1885918 1885919 1885920 1885921 1885922 1885923 [...] (33353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2006586 2006587 2006588 2006589 2006590 2006591 2006592 2006593 2006594 2006595 [...] (4566 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2116728 2116729 2116730 2116731 2116732 2116733 2116734 2116735 2116736 2116737 [...] (150 flits)
Measured flits: (0 flits)
Time taken is 45716 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7723.98 (1 samples)
	minimum = 22 (1 samples)
	maximum = 26213 (1 samples)
Network latency average = 5986.38 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18695 (1 samples)
Flit latency average = 6838.59 (1 samples)
	minimum = 5 (1 samples)
	maximum = 19238 (1 samples)
Fragmentation average = 56.8087 (1 samples)
	minimum = 0 (1 samples)
	maximum = 809 (1 samples)
Injected packet rate average = 0.0247374 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0361429 (1 samples)
Accepted packet rate average = 0.0154182 (1 samples)
	minimum = 0.0127143 (1 samples)
	maximum = 0.0197143 (1 samples)
Injected flit rate average = 0.44536 (1 samples)
	minimum = 0.223143 (1 samples)
	maximum = 0.650571 (1 samples)
Accepted flit rate average = 0.276517 (1 samples)
	minimum = 0.226 (1 samples)
	maximum = 0.350143 (1 samples)
Injected packet size average = 18.0035 (1 samples)
Accepted packet size average = 17.9345 (1 samples)
Hops average = 5.06364 (1 samples)
Total run time 62.7988
