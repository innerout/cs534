BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 306.985
	minimum = 22
	maximum = 829
Network latency average = 290.466
	minimum = 22
	maximum = 792
Slowest packet = 236
Flit latency average = 268.119
	minimum = 5
	maximum = 785
Slowest flit = 27868
Fragmentation average = 23.7782
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0153073
	minimum = 0.008 (at node 54)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.283021
	minimum = 0.151 (at node 138)
	maximum = 0.473 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 18.4893
Total in-flight flits = 87032 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 573.24
	minimum = 22
	maximum = 1645
Network latency average = 553.014
	minimum = 22
	maximum = 1559
Slowest packet = 2757
Flit latency average = 529.732
	minimum = 5
	maximum = 1578
Slowest flit = 55937
Fragmentation average = 24.8476
	minimum = 0
	maximum = 109
Injected packet rate average = 0.0418438
	minimum = 0.032 (at node 50)
	maximum = 0.052 (at node 154)
Accepted packet rate average = 0.0160078
	minimum = 0.01 (at node 83)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.74988
	minimum = 0.5705 (at node 50)
	maximum = 0.93 (at node 154)
Accepted flit rate average= 0.291352
	minimum = 0.183 (at node 83)
	maximum = 0.4205 (at node 152)
Injected packet length average = 17.921
Accepted packet length average = 18.2006
Total in-flight flits = 177345 (0 measured)
latency change    = 0.464473
throughput change = 0.0285934
Class 0:
Packet latency average = 1323.06
	minimum = 22
	maximum = 2349
Network latency average = 1297.23
	minimum = 22
	maximum = 2223
Slowest packet = 4059
Flit latency average = 1277.05
	minimum = 5
	maximum = 2317
Slowest flit = 94399
Fragmentation average = 25.6597
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0418542
	minimum = 0.024 (at node 17)
	maximum = 0.056 (at node 48)
Accepted packet rate average = 0.0170521
	minimum = 0.008 (at node 49)
	maximum = 0.027 (at node 120)
Injected flit rate average = 0.753583
	minimum = 0.42 (at node 17)
	maximum = 1 (at node 48)
Accepted flit rate average= 0.306354
	minimum = 0.153 (at node 49)
	maximum = 0.49 (at node 120)
Injected packet length average = 18.005
Accepted packet length average = 17.9658
Total in-flight flits = 263173 (0 measured)
latency change    = 0.566733
throughput change = 0.0489714
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 70.2492
	minimum = 22
	maximum = 259
Network latency average = 42.473
	minimum = 22
	maximum = 259
Slowest packet = 28189
Flit latency average = 1817.86
	minimum = 5
	maximum = 2976
Slowest flit = 142698
Fragmentation average = 5.0873
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0425104
	minimum = 0.03 (at node 37)
	maximum = 0.056 (at node 135)
Accepted packet rate average = 0.0169688
	minimum = 0.007 (at node 4)
	maximum = 0.027 (at node 90)
Injected flit rate average = 0.764432
	minimum = 0.53 (at node 127)
	maximum = 1 (at node 49)
Accepted flit rate average= 0.306583
	minimum = 0.119 (at node 4)
	maximum = 0.488 (at node 90)
Injected packet length average = 17.9822
Accepted packet length average = 18.0675
Total in-flight flits = 351225 (135380 measured)
latency change    = 17.8339
throughput change = 0.000747486
Class 0:
Packet latency average = 79.2147
	minimum = 22
	maximum = 737
Network latency average = 48.2194
	minimum = 22
	maximum = 710
Slowest packet = 34053
Flit latency average = 2083.8
	minimum = 5
	maximum = 3814
Slowest flit = 168612
Fragmentation average = 5.08129
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0417786
	minimum = 0.032 (at node 67)
	maximum = 0.0515 (at node 54)
Accepted packet rate average = 0.0168672
	minimum = 0.012 (at node 4)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.752005
	minimum = 0.576 (at node 67)
	maximum = 0.9265 (at node 190)
Accepted flit rate average= 0.304411
	minimum = 0.2125 (at node 4)
	maximum = 0.446 (at node 47)
Injected packet length average = 17.9998
Accepted packet length average = 18.0476
Total in-flight flits = 435053 (265805 measured)
latency change    = 0.113179
throughput change = 0.00713467
Class 0:
Packet latency average = 109.896
	minimum = 22
	maximum = 1009
Network latency average = 65.2349
	minimum = 22
	maximum = 1009
Slowest packet = 37296
Flit latency average = 2361.14
	minimum = 5
	maximum = 4476
Slowest flit = 216641
Fragmentation average = 4.99627
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0413038
	minimum = 0.0306667 (at node 28)
	maximum = 0.0516667 (at node 49)
Accepted packet rate average = 0.0168455
	minimum = 0.0123333 (at node 54)
	maximum = 0.0236667 (at node 138)
Injected flit rate average = 0.743198
	minimum = 0.556667 (at node 28)
	maximum = 0.931667 (at node 49)
Accepted flit rate average= 0.303377
	minimum = 0.215 (at node 54)
	maximum = 0.424 (at node 138)
Injected packet length average = 17.9934
Accepted packet length average = 18.0094
Total in-flight flits = 516684 (394385 measured)
latency change    = 0.279186
throughput change = 0.00341068
Draining remaining packets ...
Class 0:
Remaining flits: 266665 266666 266667 266668 266669 269298 269299 269300 269301 269302 [...] (467955 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (392500 flits)
Class 0:
Remaining flits: 313776 313777 313778 313779 313780 313781 313782 313783 313784 313785 [...] (420612 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (388645 flits)
Class 0:
Remaining flits: 347724 347725 347726 347727 347728 347729 347730 347731 347732 347733 [...] (373154 flits)
Measured flits: 433908 433909 433910 433911 433912 433913 433914 433915 433916 433917 [...] (366048 flits)
Class 0:
Remaining flits: 375768 375769 375770 375771 375772 375773 375774 375775 375776 375777 [...] (325832 flits)
Measured flits: 434466 434467 434468 434469 434470 434471 434472 434473 434474 434475 [...] (325166 flits)
Class 0:
Remaining flits: 432612 432613 432614 432615 432616 432617 432618 432619 432620 432621 [...] (278914 flits)
Measured flits: 437850 437851 437852 437853 437854 437855 437856 437857 437858 437859 [...] (278878 flits)
Class 0:
Remaining flits: 473436 473437 473438 473439 473440 473441 473442 473443 473444 473445 [...] (232080 flits)
Measured flits: 473436 473437 473438 473439 473440 473441 473442 473443 473444 473445 [...] (232080 flits)
Class 0:
Remaining flits: 500004 500005 500006 500007 500008 500009 500010 500011 500012 500013 [...] (184295 flits)
Measured flits: 500004 500005 500006 500007 500008 500009 500010 500011 500012 500013 [...] (184295 flits)
Class 0:
Remaining flits: 571318 571319 574272 574273 574274 574275 574276 574277 574278 574279 [...] (137046 flits)
Measured flits: 571318 571319 574272 574273 574274 574275 574276 574277 574278 574279 [...] (137046 flits)
Class 0:
Remaining flits: 602640 602641 602642 602643 602644 602645 602646 602647 602648 602649 [...] (89399 flits)
Measured flits: 602640 602641 602642 602643 602644 602645 602646 602647 602648 602649 [...] (89399 flits)
Class 0:
Remaining flits: 642762 642763 642764 642765 642766 642767 642768 642769 642770 642771 [...] (42121 flits)
Measured flits: 642762 642763 642764 642765 642766 642767 642768 642769 642770 642771 [...] (42121 flits)
Class 0:
Remaining flits: 720576 720577 720578 720579 720580 720581 720582 720583 720584 720585 [...] (5743 flits)
Measured flits: 720576 720577 720578 720579 720580 720581 720582 720583 720584 720585 [...] (5743 flits)
Time taken is 17817 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7626.84 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12540 (1 samples)
Network latency average = 7583.74 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12528 (1 samples)
Flit latency average = 6136.58 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12511 (1 samples)
Fragmentation average = 27.1843 (1 samples)
	minimum = 0 (1 samples)
	maximum = 329 (1 samples)
Injected packet rate average = 0.0413038 (1 samples)
	minimum = 0.0306667 (1 samples)
	maximum = 0.0516667 (1 samples)
Accepted packet rate average = 0.0168455 (1 samples)
	minimum = 0.0123333 (1 samples)
	maximum = 0.0236667 (1 samples)
Injected flit rate average = 0.743198 (1 samples)
	minimum = 0.556667 (1 samples)
	maximum = 0.931667 (1 samples)
Accepted flit rate average = 0.303377 (1 samples)
	minimum = 0.215 (1 samples)
	maximum = 0.424 (1 samples)
Injected packet size average = 17.9934 (1 samples)
Accepted packet size average = 18.0094 (1 samples)
Hops average = 5.08431 (1 samples)
Total run time 16.5342
