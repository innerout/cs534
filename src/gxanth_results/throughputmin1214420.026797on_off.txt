BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 316.525
	minimum = 22
	maximum = 962
Network latency average = 216.98
	minimum = 22
	maximum = 805
Slowest packet = 8
Flit latency average = 193.621
	minimum = 5
	maximum = 788
Slowest flit = 15043
Fragmentation average = 17.7246
	minimum = 0
	maximum = 86
Injected packet rate average = 0.0242552
	minimum = 0 (at node 122)
	maximum = 0.056 (at node 85)
Accepted packet rate average = 0.013599
	minimum = 0.004 (at node 64)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.432646
	minimum = 0 (at node 122)
	maximum = 1 (at node 85)
Accepted flit rate average= 0.249781
	minimum = 0.072 (at node 64)
	maximum = 0.432 (at node 152)
Injected packet length average = 17.8372
Accepted packet length average = 18.3677
Total in-flight flits = 35886 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.302
	minimum = 22
	maximum = 1918
Network latency average = 395.309
	minimum = 22
	maximum = 1417
Slowest packet = 8
Flit latency average = 371.561
	minimum = 5
	maximum = 1400
Slowest flit = 47231
Fragmentation average = 19.5426
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0246224
	minimum = 0.002 (at node 177)
	maximum = 0.056 (at node 91)
Accepted packet rate average = 0.0143411
	minimum = 0.006 (at node 79)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.441047
	minimum = 0.036 (at node 177)
	maximum = 0.9995 (at node 91)
Accepted flit rate average= 0.261109
	minimum = 0.116 (at node 79)
	maximum = 0.38 (at node 78)
Injected packet length average = 17.9124
Accepted packet length average = 18.207
Total in-flight flits = 70122 (0 measured)
latency change    = 0.399726
throughput change = 0.0433846
Class 0:
Packet latency average = 1098.58
	minimum = 22
	maximum = 2696
Network latency average = 924.658
	minimum = 22
	maximum = 2080
Slowest packet = 3483
Flit latency average = 906.336
	minimum = 5
	maximum = 2175
Slowest flit = 67226
Fragmentation average = 20.4524
	minimum = 0
	maximum = 137
Injected packet rate average = 0.023026
	minimum = 0 (at node 1)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0152188
	minimum = 0.005 (at node 11)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.415437
	minimum = 0 (at node 1)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.273229
	minimum = 0.09 (at node 11)
	maximum = 0.476 (at node 120)
Injected packet length average = 18.0421
Accepted packet length average = 17.9535
Total in-flight flits = 97690 (0 measured)
latency change    = 0.520015
throughput change = 0.0443576
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 485.579
	minimum = 27
	maximum = 2162
Network latency average = 187.309
	minimum = 22
	maximum = 964
Slowest packet = 13940
Flit latency average = 1277.95
	minimum = 5
	maximum = 3006
Slowest flit = 81360
Fragmentation average = 7.87018
	minimum = 0
	maximum = 55
Injected packet rate average = 0.0198177
	minimum = 0 (at node 78)
	maximum = 0.054 (at node 169)
Accepted packet rate average = 0.0150573
	minimum = 0.006 (at node 31)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.356661
	minimum = 0 (at node 78)
	maximum = 0.955 (at node 169)
Accepted flit rate average= 0.271224
	minimum = 0.108 (at node 31)
	maximum = 0.49 (at node 16)
Injected packet length average = 17.9971
Accepted packet length average = 18.0128
Total in-flight flits = 114537 (63589 measured)
latency change    = 1.26241
throughput change = 0.00739318
Class 0:
Packet latency average = 1100.29
	minimum = 23
	maximum = 3398
Network latency average = 816.361
	minimum = 22
	maximum = 1973
Slowest packet = 13940
Flit latency average = 1460.3
	minimum = 5
	maximum = 3875
Slowest flit = 91525
Fragmentation average = 15.6326
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0195807
	minimum = 0.003 (at node 94)
	maximum = 0.042 (at node 26)
Accepted packet rate average = 0.0150286
	minimum = 0.0085 (at node 2)
	maximum = 0.023 (at node 181)
Injected flit rate average = 0.352237
	minimum = 0.048 (at node 94)
	maximum = 0.7555 (at node 50)
Accepted flit rate average= 0.270427
	minimum = 0.153 (at node 2)
	maximum = 0.4215 (at node 181)
Injected packet length average = 17.989
Accepted packet length average = 17.9941
Total in-flight flits = 129602 (110932 measured)
latency change    = 0.558683
throughput change = 0.00294673
Class 0:
Packet latency average = 1666.58
	minimum = 23
	maximum = 4466
Network latency average = 1313.94
	minimum = 22
	maximum = 2936
Slowest packet = 13940
Flit latency average = 1623.92
	minimum = 5
	maximum = 4579
Slowest flit = 97848
Fragmentation average = 17.0948
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0184549
	minimum = 0.00433333 (at node 94)
	maximum = 0.0376667 (at node 93)
Accepted packet rate average = 0.0149323
	minimum = 0.00933333 (at node 64)
	maximum = 0.0216667 (at node 128)
Injected flit rate average = 0.332285
	minimum = 0.078 (at node 94)
	maximum = 0.678 (at node 93)
Accepted flit rate average= 0.26858
	minimum = 0.172333 (at node 64)
	maximum = 0.391333 (at node 128)
Injected packet length average = 18.0053
Accepted packet length average = 17.9865
Total in-flight flits = 135354 (130443 measured)
latency change    = 0.339789
throughput change = 0.00687774
Draining remaining packets ...
Class 0:
Remaining flits: 187794 187795 187796 187797 187798 187799 187800 187801 187802 187803 [...] (86317 flits)
Measured flits: 252396 252397 252398 252399 252400 252401 252402 252403 252404 252405 [...] (85739 flits)
Class 0:
Remaining flits: 280836 280837 280838 280839 280840 280841 280842 280843 280844 280845 [...] (39021 flits)
Measured flits: 280836 280837 280838 280839 280840 280841 280842 280843 280844 280845 [...] (39021 flits)
Class 0:
Remaining flits: 356940 356941 356942 356943 356944 356945 356946 356947 356948 356949 [...] (2530 flits)
Measured flits: 356940 356941 356942 356943 356944 356945 356946 356947 356948 356949 [...] (2530 flits)
Time taken is 9499 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2846.55 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6866 (1 samples)
Network latency average = 2253.89 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5018 (1 samples)
Flit latency average = 2130.35 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5001 (1 samples)
Fragmentation average = 18.0128 (1 samples)
	minimum = 0 (1 samples)
	maximum = 161 (1 samples)
Injected packet rate average = 0.0184549 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0376667 (1 samples)
Accepted packet rate average = 0.0149323 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.332285 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.678 (1 samples)
Accepted flit rate average = 0.26858 (1 samples)
	minimum = 0.172333 (1 samples)
	maximum = 0.391333 (1 samples)
Injected packet size average = 18.0053 (1 samples)
Accepted packet size average = 17.9865 (1 samples)
Hops average = 5.09395 (1 samples)
Total run time 9.95046
