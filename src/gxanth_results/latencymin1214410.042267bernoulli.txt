BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.793
	minimum = 27
	maximum = 941
Network latency average = 330.501
	minimum = 23
	maximum = 919
Slowest packet = 501
Flit latency average = 298.757
	minimum = 6
	maximum = 916
Slowest flit = 8685
Fragmentation average = 57.1658
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0367396
	minimum = 0.02 (at node 112)
	maximum = 0.051 (at node 51)
Accepted packet rate average = 0.0100833
	minimum = 0.005 (at node 4)
	maximum = 0.018 (at node 114)
Injected flit rate average = 0.654854
	minimum = 0.36 (at node 112)
	maximum = 0.916 (at node 77)
Accepted flit rate average= 0.18937
	minimum = 0.09 (at node 30)
	maximum = 0.34 (at node 114)
Injected packet length average = 17.8242
Accepted packet length average = 18.7805
Total in-flight flits = 90739 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 702.338
	minimum = 27
	maximum = 1849
Network latency average = 662.031
	minimum = 23
	maximum = 1826
Slowest packet = 572
Flit latency average = 627.86
	minimum = 6
	maximum = 1913
Slowest flit = 7970
Fragmentation average = 63.9031
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0283646
	minimum = 0.016 (at node 60)
	maximum = 0.0375 (at node 110)
Accepted packet rate average = 0.0100286
	minimum = 0.0055 (at node 96)
	maximum = 0.0145 (at node 177)
Injected flit rate average = 0.507971
	minimum = 0.288 (at node 60)
	maximum = 0.6705 (at node 110)
Accepted flit rate average= 0.184857
	minimum = 0.099 (at node 96)
	maximum = 0.264 (at node 177)
Injected packet length average = 17.9086
Accepted packet length average = 18.4329
Total in-flight flits = 126223 (0 measured)
latency change    = 0.507654
throughput change = 0.0244136
Class 0:
Packet latency average = 1843.33
	minimum = 309
	maximum = 2761
Network latency average = 1687.13
	minimum = 23
	maximum = 2700
Slowest packet = 1578
Flit latency average = 1656.04
	minimum = 6
	maximum = 2683
Slowest flit = 28799
Fragmentation average = 69.0695
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0109115
	minimum = 0 (at node 32)
	maximum = 0.036 (at node 115)
Accepted packet rate average = 0.00967187
	minimum = 0.003 (at node 9)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.198599
	minimum = 0 (at node 32)
	maximum = 0.646 (at node 115)
Accepted flit rate average= 0.172724
	minimum = 0.046 (at node 9)
	maximum = 0.336 (at node 152)
Injected packet length average = 18.201
Accepted packet length average = 17.8584
Total in-flight flits = 131976 (0 measured)
latency change    = 0.618985
throughput change = 0.0702439
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2130.07
	minimum = 728
	maximum = 3018
Network latency average = 197.483
	minimum = 31
	maximum = 808
Slowest packet = 13219
Flit latency average = 2312.28
	minimum = 6
	maximum = 3636
Slowest flit = 48637
Fragmentation average = 32.8667
	minimum = 1
	maximum = 130
Injected packet rate average = 0.00804167
	minimum = 0 (at node 3)
	maximum = 0.039 (at node 1)
Accepted packet rate average = 0.00892708
	minimum = 0.003 (at node 91)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.145016
	minimum = 0 (at node 3)
	maximum = 0.692 (at node 1)
Accepted flit rate average= 0.161245
	minimum = 0.057 (at node 91)
	maximum = 0.297 (at node 112)
Injected packet length average = 18.033
Accepted packet length average = 18.0624
Total in-flight flits = 128629 (26490 measured)
latency change    = 0.134613
throughput change = 0.0711909
Class 0:
Packet latency average = 2707.9
	minimum = 728
	maximum = 4113
Network latency average = 557.977
	minimum = 27
	maximum = 1938
Slowest packet = 13219
Flit latency average = 2609.1
	minimum = 6
	maximum = 4764
Slowest flit = 30124
Fragmentation average = 44.6326
	minimum = 0
	maximum = 136
Injected packet rate average = 0.00794531
	minimum = 0.0005 (at node 5)
	maximum = 0.026 (at node 1)
Accepted packet rate average = 0.00901042
	minimum = 0.0045 (at node 104)
	maximum = 0.016 (at node 78)
Injected flit rate average = 0.143211
	minimum = 0.0075 (at node 8)
	maximum = 0.463 (at node 1)
Accepted flit rate average= 0.161997
	minimum = 0.081 (at node 104)
	maximum = 0.286 (at node 78)
Injected packet length average = 18.0246
Accepted packet length average = 17.9789
Total in-flight flits = 124687 (50902 measured)
latency change    = 0.213387
throughput change = 0.00464578
Class 0:
Packet latency average = 3446.65
	minimum = 728
	maximum = 5013
Network latency average = 1062.19
	minimum = 23
	maximum = 2948
Slowest packet = 13219
Flit latency average = 2861.82
	minimum = 6
	maximum = 5540
Slowest flit = 59359
Fragmentation average = 53.3081
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00817535
	minimum = 0.000666667 (at node 191)
	maximum = 0.0203333 (at node 1)
Accepted packet rate average = 0.00900694
	minimum = 0.00533333 (at node 132)
	maximum = 0.0136667 (at node 16)
Injected flit rate average = 0.147038
	minimum = 0.012 (at node 191)
	maximum = 0.364 (at node 112)
Accepted flit rate average= 0.162241
	minimum = 0.098 (at node 132)
	maximum = 0.244667 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.0129
Total in-flight flits = 123017 (74144 measured)
latency change    = 0.214338
throughput change = 0.00150346
Class 0:
Packet latency average = 4144.9
	minimum = 728
	maximum = 5995
Network latency average = 1508.24
	minimum = 23
	maximum = 3892
Slowest packet = 13219
Flit latency average = 3031.2
	minimum = 6
	maximum = 6337
Slowest flit = 84453
Fragmentation average = 60.3535
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00833854
	minimum = 0.0015 (at node 191)
	maximum = 0.01875 (at node 112)
Accepted packet rate average = 0.00895573
	minimum = 0.00625 (at node 35)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.150253
	minimum = 0.027 (at node 191)
	maximum = 0.3375 (at node 112)
Accepted flit rate average= 0.161376
	minimum = 0.113 (at node 35)
	maximum = 0.23075 (at node 16)
Injected packet length average = 18.0191
Accepted packet length average = 18.0193
Total in-flight flits = 123401 (93253 measured)
latency change    = 0.168461
throughput change = 0.00536025
Class 0:
Packet latency average = 4787.95
	minimum = 728
	maximum = 7040
Network latency average = 1930.39
	minimum = 23
	maximum = 4874
Slowest packet = 13219
Flit latency average = 3179.98
	minimum = 6
	maximum = 7208
Slowest flit = 100418
Fragmentation average = 62.9602
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00835833
	minimum = 0.0016 (at node 54)
	maximum = 0.0164 (at node 24)
Accepted packet rate average = 0.00893958
	minimum = 0.0062 (at node 64)
	maximum = 0.012 (at node 16)
Injected flit rate average = 0.150445
	minimum = 0.0288 (at node 54)
	maximum = 0.2958 (at node 24)
Accepted flit rate average= 0.161064
	minimum = 0.1096 (at node 64)
	maximum = 0.2156 (at node 157)
Injected packet length average = 17.9994
Accepted packet length average = 18.0169
Total in-flight flits = 121553 (105203 measured)
latency change    = 0.134306
throughput change = 0.00194184
Class 0:
Packet latency average = 5408.23
	minimum = 728
	maximum = 8126
Network latency average = 2361.68
	minimum = 23
	maximum = 5813
Slowest packet = 13219
Flit latency average = 3299.94
	minimum = 6
	maximum = 8326
Slowest flit = 70613
Fragmentation average = 64.5154
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00832986
	minimum = 0.0015 (at node 54)
	maximum = 0.015 (at node 166)
Accepted packet rate average = 0.00896094
	minimum = 0.00583333 (at node 64)
	maximum = 0.0118333 (at node 128)
Injected flit rate average = 0.149926
	minimum = 0.027 (at node 54)
	maximum = 0.269333 (at node 166)
Accepted flit rate average= 0.161216
	minimum = 0.105667 (at node 64)
	maximum = 0.213 (at node 128)
Injected packet length average = 17.9986
Accepted packet length average = 17.991
Total in-flight flits = 118893 (110794 measured)
latency change    = 0.114692
throughput change = 0.000946581
Class 0:
Packet latency average = 5950.73
	minimum = 728
	maximum = 8963
Network latency average = 2657.71
	minimum = 23
	maximum = 6843
Slowest packet = 13219
Flit latency average = 3383.14
	minimum = 6
	maximum = 8819
Slowest flit = 118673
Fragmentation average = 65.051
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00831622
	minimum = 0.00128571 (at node 54)
	maximum = 0.0145714 (at node 174)
Accepted packet rate average = 0.00894568
	minimum = 0.00628571 (at node 126)
	maximum = 0.0115714 (at node 129)
Injected flit rate average = 0.149717
	minimum = 0.0231429 (at node 54)
	maximum = 0.259857 (at node 174)
Accepted flit rate average= 0.160989
	minimum = 0.111143 (at node 126)
	maximum = 0.207143 (at node 72)
Injected packet length average = 18.003
Accepted packet length average = 17.9963
Total in-flight flits = 116865 (113377 measured)
latency change    = 0.0911663
throughput change = 0.00141194
Draining all recorded packets ...
Class 0:
Remaining flits: 129870 129871 129872 129873 129874 129875 129876 129877 129878 129879 [...] (118215 flits)
Measured flits: 236286 236287 236288 236289 236290 236291 236292 236293 236294 236295 [...] (116710 flits)
Class 0:
Remaining flits: 129870 129871 129872 129873 129874 129875 129876 129877 129878 129879 [...] (121664 flits)
Measured flits: 238698 238699 238700 238701 238702 238703 238704 238705 238706 238707 [...] (120992 flits)
Class 0:
Remaining flits: 129870 129871 129872 129873 129874 129875 129876 129877 129878 129879 [...] (121968 flits)
Measured flits: 242604 242605 242606 242607 242608 242609 242610 242611 242612 242613 [...] (121698 flits)
Class 0:
Remaining flits: 129886 129887 169310 169311 169312 169313 169314 169315 169316 169317 [...] (120977 flits)
Measured flits: 248238 248239 248240 248241 248242 248243 248244 248245 248246 248247 [...] (120797 flits)
Class 0:
Remaining flits: 217206 217207 217208 217209 217210 217211 217212 217213 217214 217215 [...] (121927 flits)
Measured flits: 258714 258715 258716 258717 258718 258719 258720 258721 258722 258723 [...] (121837 flits)
Class 0:
Remaining flits: 217206 217207 217208 217209 217210 217211 217212 217213 217214 217215 [...] (122029 flits)
Measured flits: 298962 298963 298964 298965 298966 298967 298968 298969 298970 298971 [...] (122011 flits)
Class 0:
Remaining flits: 299466 299467 299468 299469 299470 299471 299472 299473 299474 299475 [...] (120691 flits)
Measured flits: 299466 299467 299468 299469 299470 299471 299472 299473 299474 299475 [...] (120691 flits)
Class 0:
Remaining flits: 324900 324901 324902 324903 324904 324905 324906 324907 324908 324909 [...] (120589 flits)
Measured flits: 324900 324901 324902 324903 324904 324905 324906 324907 324908 324909 [...] (120589 flits)
Class 0:
Remaining flits: 324900 324901 324902 324903 324904 324905 324906 324907 324908 324909 [...] (121954 flits)
Measured flits: 324900 324901 324902 324903 324904 324905 324906 324907 324908 324909 [...] (121954 flits)
Class 0:
Remaining flits: 380304 380305 380306 380307 380308 380309 380310 380311 380312 380313 [...] (121824 flits)
Measured flits: 380304 380305 380306 380307 380308 380309 380310 380311 380312 380313 [...] (121824 flits)
Class 0:
Remaining flits: 410364 410365 410366 410367 410368 410369 410370 410371 410372 410373 [...] (119474 flits)
Measured flits: 410364 410365 410366 410367 410368 410369 410370 410371 410372 410373 [...] (119474 flits)
Class 0:
Remaining flits: 482058 482059 482060 482061 482062 482063 482064 482065 482066 482067 [...] (119786 flits)
Measured flits: 482058 482059 482060 482061 482062 482063 482064 482065 482066 482067 [...] (119786 flits)
Class 0:
Remaining flits: 515574 515575 515576 515577 515578 515579 515580 515581 515582 515583 [...] (118169 flits)
Measured flits: 515574 515575 515576 515577 515578 515579 515580 515581 515582 515583 [...] (118169 flits)
Class 0:
Remaining flits: 518274 518275 518276 518277 518278 518279 518280 518281 518282 518283 [...] (121577 flits)
Measured flits: 518274 518275 518276 518277 518278 518279 518280 518281 518282 518283 [...] (121577 flits)
Class 0:
Remaining flits: 575838 575839 575840 575841 575842 575843 575844 575845 575846 575847 [...] (122011 flits)
Measured flits: 575838 575839 575840 575841 575842 575843 575844 575845 575846 575847 [...] (122011 flits)
Class 0:
Remaining flits: 594018 594019 594020 594021 594022 594023 594024 594025 594026 594027 [...] (121987 flits)
Measured flits: 594018 594019 594020 594021 594022 594023 594024 594025 594026 594027 [...] (121987 flits)
Class 0:
Remaining flits: 606942 606943 606944 606945 606946 606947 606948 606949 606950 606951 [...] (120873 flits)
Measured flits: 606942 606943 606944 606945 606946 606947 606948 606949 606950 606951 [...] (120873 flits)
Class 0:
Remaining flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (121266 flits)
Measured flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (121266 flits)
Class 0:
Remaining flits: 686448 686449 686450 686451 686452 686453 686454 686455 686456 686457 [...] (121524 flits)
Measured flits: 686448 686449 686450 686451 686452 686453 686454 686455 686456 686457 [...] (121290 flits)
Class 0:
Remaining flits: 727020 727021 727022 727023 727024 727025 727026 727027 727028 727029 [...] (122048 flits)
Measured flits: 727020 727021 727022 727023 727024 727025 727026 727027 727028 727029 [...] (121580 flits)
Class 0:
Remaining flits: 747234 747235 747236 747237 747238 747239 747240 747241 747242 747243 [...] (120646 flits)
Measured flits: 747234 747235 747236 747237 747238 747239 747240 747241 747242 747243 [...] (119620 flits)
Class 0:
Remaining flits: 782891 785970 785971 785972 785973 785974 785975 785976 785977 785978 [...] (117969 flits)
Measured flits: 782891 785970 785971 785972 785973 785974 785975 785976 785977 785978 [...] (116432 flits)
Class 0:
Remaining flits: 785975 785976 785977 785978 785979 785980 785981 785982 785983 785984 [...] (117984 flits)
Measured flits: 785975 785976 785977 785978 785979 785980 785981 785982 785983 785984 [...] (115460 flits)
Class 0:
Remaining flits: 881298 881299 881300 881301 881302 881303 881304 881305 881306 881307 [...] (121024 flits)
Measured flits: 881298 881299 881300 881301 881302 881303 881304 881305 881306 881307 [...] (117920 flits)
Class 0:
Remaining flits: 885474 885475 885476 885477 885478 885479 885480 885481 885482 885483 [...] (118967 flits)
Measured flits: 885474 885475 885476 885477 885478 885479 885480 885481 885482 885483 [...] (114773 flits)
Class 0:
Remaining flits: 896634 896635 896636 896637 896638 896639 896640 896641 896642 896643 [...] (119929 flits)
Measured flits: 896634 896635 896636 896637 896638 896639 896640 896641 896642 896643 [...] (114644 flits)
Class 0:
Remaining flits: 898650 898651 898652 898653 898654 898655 898656 898657 898658 898659 [...] (120325 flits)
Measured flits: 898650 898651 898652 898653 898654 898655 898656 898657 898658 898659 [...] (113126 flits)
Class 0:
Remaining flits: 910926 910927 910928 910929 910930 910931 910932 910933 910934 910935 [...] (121325 flits)
Measured flits: 910926 910927 910928 910929 910930 910931 910932 910933 910934 910935 [...] (111556 flits)
Class 0:
Remaining flits: 957924 957925 957926 957927 957928 957929 957930 957931 957932 957933 [...] (121499 flits)
Measured flits: 957924 957925 957926 957927 957928 957929 957930 957931 957932 957933 [...] (108289 flits)
Class 0:
Remaining flits: 1020726 1020727 1020728 1020729 1020730 1020731 1020732 1020733 1020734 1020735 [...] (121424 flits)
Measured flits: 1020726 1020727 1020728 1020729 1020730 1020731 1020732 1020733 1020734 1020735 [...] (103317 flits)
Class 0:
Remaining flits: 1054584 1054585 1054586 1054587 1054588 1054589 1054590 1054591 1054592 1054593 [...] (120195 flits)
Measured flits: 1054584 1054585 1054586 1054587 1054588 1054589 1054590 1054591 1054592 1054593 [...] (95792 flits)
Class 0:
Remaining flits: 1073027 1073028 1073029 1073030 1073031 1073032 1073033 1089792 1089793 1089794 [...] (121957 flits)
Measured flits: 1073027 1073028 1073029 1073030 1073031 1073032 1073033 1089792 1089793 1089794 [...] (88562 flits)
Class 0:
Remaining flits: 1089792 1089793 1089794 1089795 1089796 1089797 1089798 1089799 1089800 1089801 [...] (121265 flits)
Measured flits: 1089792 1089793 1089794 1089795 1089796 1089797 1089798 1089799 1089800 1089801 [...] (81285 flits)
Class 0:
Remaining flits: 1152018 1152019 1152020 1152021 1152022 1152023 1152024 1152025 1152026 1152027 [...] (120126 flits)
Measured flits: 1152018 1152019 1152020 1152021 1152022 1152023 1152024 1152025 1152026 1152027 [...] (70604 flits)
Class 0:
Remaining flits: 1152018 1152019 1152020 1152021 1152022 1152023 1152024 1152025 1152026 1152027 [...] (121779 flits)
Measured flits: 1152018 1152019 1152020 1152021 1152022 1152023 1152024 1152025 1152026 1152027 [...] (61752 flits)
Class 0:
Remaining flits: 1186380 1186381 1186382 1186383 1186384 1186385 1186386 1186387 1186388 1186389 [...] (122897 flits)
Measured flits: 1186380 1186381 1186382 1186383 1186384 1186385 1186386 1186387 1186388 1186389 [...] (52901 flits)
Class 0:
Remaining flits: 1186380 1186381 1186382 1186383 1186384 1186385 1186386 1186387 1186388 1186389 [...] (123936 flits)
Measured flits: 1186380 1186381 1186382 1186383 1186384 1186385 1186386 1186387 1186388 1186389 [...] (43585 flits)
Class 0:
Remaining flits: 1282553 1283922 1283923 1283924 1283925 1283926 1283927 1283928 1283929 1283930 [...] (121205 flits)
Measured flits: 1282553 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 [...] (34194 flits)
Class 0:
Remaining flits: 1332630 1332631 1332632 1332633 1332634 1332635 1332636 1332637 1332638 1332639 [...] (121100 flits)
Measured flits: 1332630 1332631 1332632 1332633 1332634 1332635 1332636 1332637 1332638 1332639 [...] (26513 flits)
Class 0:
Remaining flits: 1336734 1336735 1336736 1336737 1336738 1336739 1336740 1336741 1336742 1336743 [...] (119127 flits)
Measured flits: 1336734 1336735 1336736 1336737 1336738 1336739 1336740 1336741 1336742 1336743 [...] (20901 flits)
Class 0:
Remaining flits: 1340226 1340227 1340228 1340229 1340230 1340231 1340232 1340233 1340234 1340235 [...] (118122 flits)
Measured flits: 1395396 1395397 1395398 1395399 1395400 1395401 1395402 1395403 1395404 1395405 [...] (15756 flits)
Class 0:
Remaining flits: 1341396 1341397 1341398 1341399 1341400 1341401 1341402 1341403 1341404 1341405 [...] (118377 flits)
Measured flits: 1406249 1414098 1414099 1414100 1414101 1414102 1414103 1414104 1414105 1414106 [...] (12170 flits)
Class 0:
Remaining flits: 1407726 1407727 1407728 1407729 1407730 1407731 1407732 1407733 1407734 1407735 [...] (120112 flits)
Measured flits: 1437156 1437157 1437158 1437159 1437160 1437161 1437162 1437163 1437164 1437165 [...] (9348 flits)
Class 0:
Remaining flits: 1498950 1498951 1498952 1498953 1498954 1498955 1498956 1498957 1498958 1498959 [...] (119999 flits)
Measured flits: 1517688 1517689 1517690 1517691 1517692 1517693 1517694 1517695 1517696 1517697 [...] (7412 flits)
Class 0:
Remaining flits: 1522314 1522315 1522316 1522317 1522318 1522319 1522320 1522321 1522322 1522323 [...] (118987 flits)
Measured flits: 1522314 1522315 1522316 1522317 1522318 1522319 1522320 1522321 1522322 1522323 [...] (5657 flits)
Class 0:
Remaining flits: 1544598 1544599 1544600 1544601 1544602 1544603 1544604 1544605 1544606 1544607 [...] (119845 flits)
Measured flits: 1547640 1547641 1547642 1547643 1547644 1547645 1547646 1547647 1547648 1547649 [...] (4313 flits)
Class 0:
Remaining flits: 1562958 1562959 1562960 1562961 1562962 1562963 1562964 1562965 1562966 1562967 [...] (119423 flits)
Measured flits: 1595304 1595305 1595306 1595307 1595308 1595309 1595310 1595311 1595312 1595313 [...] (3130 flits)
Class 0:
Remaining flits: 1574460 1574461 1574462 1574463 1574464 1574465 1574466 1574467 1574468 1574469 [...] (118648 flits)
Measured flits: 1611180 1611181 1611182 1611183 1611184 1611185 1611186 1611187 1611188 1611189 [...] (2220 flits)
Class 0:
Remaining flits: 1627380 1627381 1627382 1627383 1627384 1627385 1627386 1627387 1627388 1627389 [...] (119415 flits)
Measured flits: 1655010 1655011 1655012 1655013 1655014 1655015 1655016 1655017 1655018 1655019 [...] (1559 flits)
Class 0:
Remaining flits: 1668474 1668475 1668476 1668477 1668478 1668479 1668480 1668481 1668482 1668483 [...] (118891 flits)
Measured flits: 1707884 1707885 1707886 1707887 1707888 1707889 1707890 1707891 1707892 1707893 [...] (1000 flits)
Class 0:
Remaining flits: 1673082 1673083 1673084 1673085 1673086 1673087 1673088 1673089 1673090 1673091 [...] (118704 flits)
Measured flits: 1833126 1833127 1833128 1833129 1833130 1833131 1833132 1833133 1833134 1833135 [...] (500 flits)
Class 0:
Remaining flits: 1698516 1698517 1698518 1698519 1698520 1698521 1698522 1698523 1698524 1698525 [...] (120471 flits)
Measured flits: 1882188 1882189 1882190 1882191 1882192 1882193 1882194 1882195 1882196 1882197 [...] (237 flits)
Class 0:
Remaining flits: 1700190 1700191 1700192 1700193 1700194 1700195 1700196 1700197 1700198 1700199 [...] (120015 flits)
Measured flits: 1882188 1882189 1882190 1882191 1882192 1882193 1882194 1882195 1882196 1882197 [...] (81 flits)
Class 0:
Remaining flits: 1724364 1724365 1724366 1724367 1724368 1724369 1724370 1724371 1724372 1724373 [...] (119281 flits)
Measured flits: 1882203 1882204 1882205 1950894 1950895 1950896 1950897 1950898 1950899 1950900 [...] (21 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1739322 1739323 1739324 1739325 1739326 1739327 1739328 1739329 1739330 1739331 [...] (90253 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1786932 1786933 1786934 1786935 1786936 1786937 1786938 1786939 1786940 1786941 [...] (61133 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1786935 1786936 1786937 1786938 1786939 1786940 1786941 1786942 1786943 1786944 [...] (32977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1893820 1893821 1893822 1893823 1893824 1893825 1893826 1893827 1893828 1893829 [...] (9421 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1978956 1978957 1978958 1978959 1978960 1978961 1978962 1978963 1978964 1978965 [...] (255 flits)
Measured flits: (0 flits)
Time taken is 69649 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 21692.8 (1 samples)
	minimum = 728 (1 samples)
	maximum = 54568 (1 samples)
Network latency average = 3855.99 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12883 (1 samples)
Flit latency average = 3815.35 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13964 (1 samples)
Fragmentation average = 71.2936 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.00831622 (1 samples)
	minimum = 0.00128571 (1 samples)
	maximum = 0.0145714 (1 samples)
Accepted packet rate average = 0.00894568 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0115714 (1 samples)
Injected flit rate average = 0.149717 (1 samples)
	minimum = 0.0231429 (1 samples)
	maximum = 0.259857 (1 samples)
Accepted flit rate average = 0.160989 (1 samples)
	minimum = 0.111143 (1 samples)
	maximum = 0.207143 (1 samples)
Injected packet size average = 18.003 (1 samples)
Accepted packet size average = 17.9963 (1 samples)
Hops average = 5.05498 (1 samples)
Total run time 53.313
