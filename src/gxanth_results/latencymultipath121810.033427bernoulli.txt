BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 344.293
	minimum = 23
	maximum = 949
Network latency average = 281.249
	minimum = 23
	maximum = 928
Slowest packet = 227
Flit latency average = 236.272
	minimum = 6
	maximum = 915
Slowest flit = 6919
Fragmentation average = 55.7334
	minimum = 0
	maximum = 129
Injected packet rate average = 0.014125
	minimum = 0.005 (at node 140)
	maximum = 0.03 (at node 97)
Accepted packet rate average = 0.00904688
	minimum = 0.001 (at node 174)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.249417
	minimum = 0.09 (at node 140)
	maximum = 0.53 (at node 97)
Accepted flit rate average= 0.168807
	minimum = 0.024 (at node 174)
	maximum = 0.306 (at node 152)
Injected packet length average = 17.6578
Accepted packet length average = 18.6592
Total in-flight flits = 18025 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 698.462
	minimum = 23
	maximum = 1836
Network latency average = 412.652
	minimum = 23
	maximum = 1791
Slowest packet = 783
Flit latency average = 357.973
	minimum = 6
	maximum = 1774
Slowest flit = 14111
Fragmentation average = 58.4072
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0114635
	minimum = 0.0045 (at node 167)
	maximum = 0.0205 (at node 135)
Accepted packet rate average = 0.00899219
	minimum = 0.0045 (at node 25)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.204471
	minimum = 0.081 (at node 167)
	maximum = 0.369 (at node 135)
Accepted flit rate average= 0.165128
	minimum = 0.0875 (at node 25)
	maximum = 0.27 (at node 22)
Injected packet length average = 17.8367
Accepted packet length average = 18.3635
Total in-flight flits = 17699 (0 measured)
latency change    = 0.50707
throughput change = 0.0222839
Class 0:
Packet latency average = 1791.09
	minimum = 788
	maximum = 2642
Network latency average = 545.444
	minimum = 26
	maximum = 2079
Slowest packet = 3712
Flit latency average = 482.608
	minimum = 6
	maximum = 2029
Slowest flit = 32039
Fragmentation average = 58.703
	minimum = 0
	maximum = 130
Injected packet rate average = 0.00895313
	minimum = 0 (at node 112)
	maximum = 0.022 (at node 61)
Accepted packet rate average = 0.00899479
	minimum = 0.001 (at node 153)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.161401
	minimum = 0 (at node 112)
	maximum = 0.409 (at node 61)
Accepted flit rate average= 0.161427
	minimum = 0.018 (at node 153)
	maximum = 0.278 (at node 98)
Injected packet length average = 18.0273
Accepted packet length average = 17.9467
Total in-flight flits = 17665 (0 measured)
latency change    = 0.610035
throughput change = 0.0229238
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2591.62
	minimum = 1553
	maximum = 3576
Network latency average = 339.567
	minimum = 27
	maximum = 939
Slowest packet = 6254
Flit latency average = 483.648
	minimum = 6
	maximum = 2544
Slowest flit = 60092
Fragmentation average = 53.6245
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00899479
	minimum = 0 (at node 91)
	maximum = 0.023 (at node 59)
Accepted packet rate average = 0.00897917
	minimum = 0.002 (at node 96)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.161812
	minimum = 0 (at node 91)
	maximum = 0.421 (at node 59)
Accepted flit rate average= 0.161016
	minimum = 0.046 (at node 96)
	maximum = 0.306 (at node 56)
Injected packet length average = 17.9896
Accepted packet length average = 17.9321
Total in-flight flits = 17944 (16612 measured)
latency change    = 0.308893
throughput change = 0.00255539
Class 0:
Packet latency average = 3027.46
	minimum = 1553
	maximum = 4426
Network latency average = 454.828
	minimum = 23
	maximum = 1710
Slowest packet = 6254
Flit latency average = 484.618
	minimum = 6
	maximum = 2544
Slowest flit = 60092
Fragmentation average = 54.5899
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00896094
	minimum = 0.001 (at node 119)
	maximum = 0.0185 (at node 117)
Accepted packet rate average = 0.00891667
	minimum = 0.004 (at node 163)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.161068
	minimum = 0.018 (at node 119)
	maximum = 0.335 (at node 117)
Accepted flit rate average= 0.161117
	minimum = 0.0655 (at node 163)
	maximum = 0.292 (at node 16)
Injected packet length average = 17.9744
Accepted packet length average = 18.0692
Total in-flight flits = 17644 (17520 measured)
latency change    = 0.143962
throughput change = 0.000630364
Class 0:
Packet latency average = 3421.42
	minimum = 1553
	maximum = 5259
Network latency average = 493.718
	minimum = 23
	maximum = 2204
Slowest packet = 6254
Flit latency average = 487.568
	minimum = 6
	maximum = 2726
Slowest flit = 102654
Fragmentation average = 56.0456
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00898611
	minimum = 0.003 (at node 119)
	maximum = 0.0166667 (at node 18)
Accepted packet rate average = 0.00896701
	minimum = 0.005 (at node 134)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.161556
	minimum = 0.054 (at node 119)
	maximum = 0.298333 (at node 59)
Accepted flit rate average= 0.161457
	minimum = 0.094 (at node 134)
	maximum = 0.272667 (at node 16)
Injected packet length average = 17.9784
Accepted packet length average = 18.0056
Total in-flight flits = 17762 (17762 measured)
latency change    = 0.115144
throughput change = 0.00210217
Class 0:
Packet latency average = 3800.16
	minimum = 1553
	maximum = 6096
Network latency average = 508.285
	minimum = 23
	maximum = 2204
Slowest packet = 6254
Flit latency average = 485.703
	minimum = 6
	maximum = 2726
Slowest flit = 102654
Fragmentation average = 56.6009
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00892188
	minimum = 0.00375 (at node 127)
	maximum = 0.01425 (at node 38)
Accepted packet rate average = 0.0089349
	minimum = 0.00475 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.160492
	minimum = 0.066 (at node 127)
	maximum = 0.255 (at node 38)
Accepted flit rate average= 0.160953
	minimum = 0.0825 (at node 4)
	maximum = 0.253 (at node 16)
Injected packet length average = 17.9886
Accepted packet length average = 18.014
Total in-flight flits = 17317 (17317 measured)
latency change    = 0.099665
throughput change = 0.00312807
Class 0:
Packet latency average = 4170.02
	minimum = 1553
	maximum = 6901
Network latency average = 518.665
	minimum = 23
	maximum = 2828
Slowest packet = 6254
Flit latency average = 486.059
	minimum = 6
	maximum = 2802
Slowest flit = 178937
Fragmentation average = 57.4544
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00889792
	minimum = 0.0044 (at node 63)
	maximum = 0.014 (at node 124)
Accepted packet rate average = 0.00890625
	minimum = 0.0062 (at node 86)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.160076
	minimum = 0.0766 (at node 127)
	maximum = 0.2514 (at node 124)
Accepted flit rate average= 0.160367
	minimum = 0.109 (at node 163)
	maximum = 0.2334 (at node 16)
Injected packet length average = 17.9903
Accepted packet length average = 18.0061
Total in-flight flits = 17559 (17559 measured)
latency change    = 0.088694
throughput change = 0.00365698
Class 0:
Packet latency average = 4532.11
	minimum = 1553
	maximum = 7532
Network latency average = 524.222
	minimum = 23
	maximum = 2828
Slowest packet = 6254
Flit latency average = 486.915
	minimum = 6
	maximum = 2802
Slowest flit = 178937
Fragmentation average = 57.4837
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00882899
	minimum = 0.004 (at node 127)
	maximum = 0.0128333 (at node 7)
Accepted packet rate average = 0.00884635
	minimum = 0.00616667 (at node 163)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.158811
	minimum = 0.072 (at node 127)
	maximum = 0.233833 (at node 69)
Accepted flit rate average= 0.159108
	minimum = 0.1105 (at node 163)
	maximum = 0.218333 (at node 128)
Injected packet length average = 17.9874
Accepted packet length average = 17.9857
Total in-flight flits = 17451 (17451 measured)
latency change    = 0.0798964
throughput change = 0.00791306
Class 0:
Packet latency average = 4911.68
	minimum = 1553
	maximum = 8610
Network latency average = 531.331
	minimum = 23
	maximum = 3130
Slowest packet = 6254
Flit latency average = 489.698
	minimum = 6
	maximum = 3113
Slowest flit = 229571
Fragmentation average = 57.7092
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00886161
	minimum = 0.00485714 (at node 127)
	maximum = 0.0128571 (at node 124)
Accepted packet rate average = 0.00886905
	minimum = 0.00614286 (at node 163)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.159521
	minimum = 0.0861429 (at node 127)
	maximum = 0.231286 (at node 124)
Accepted flit rate average= 0.159603
	minimum = 0.111714 (at node 163)
	maximum = 0.217143 (at node 128)
Injected packet length average = 18.0013
Accepted packet length average = 17.9956
Total in-flight flits = 17718 (17718 measured)
latency change    = 0.0772775
throughput change = 0.00310635
Draining all recorded packets ...
Class 0:
Remaining flits: 274073 274074 274075 274076 274077 274078 274079 274080 274081 274082 [...] (17944 flits)
Measured flits: 274073 274074 274075 274076 274077 274078 274079 274080 274081 274082 [...] (17944 flits)
Class 0:
Remaining flits: 311112 311113 311114 311115 311116 311117 311118 311119 311120 311121 [...] (17462 flits)
Measured flits: 311112 311113 311114 311115 311116 311117 311118 311119 311120 311121 [...] (17462 flits)
Class 0:
Remaining flits: 332676 332677 332678 332679 332680 332681 332682 332683 332684 332685 [...] (17950 flits)
Measured flits: 332676 332677 332678 332679 332680 332681 332682 332683 332684 332685 [...] (17950 flits)
Class 0:
Remaining flits: 361602 361603 361604 361605 361606 361607 361608 361609 361610 361611 [...] (18011 flits)
Measured flits: 361602 361603 361604 361605 361606 361607 361608 361609 361610 361611 [...] (18011 flits)
Class 0:
Remaining flits: 413064 413065 413066 413067 413068 413069 413070 413071 413072 413073 [...] (17462 flits)
Measured flits: 413064 413065 413066 413067 413068 413069 413070 413071 413072 413073 [...] (17462 flits)
Class 0:
Remaining flits: 441648 441649 441650 441651 441652 441653 441654 441655 441656 441657 [...] (17819 flits)
Measured flits: 441648 441649 441650 441651 441652 441653 441654 441655 441656 441657 [...] (17819 flits)
Class 0:
Remaining flits: 472812 472813 472814 472815 472816 472817 472818 472819 472820 472821 [...] (17642 flits)
Measured flits: 472812 472813 472814 472815 472816 472817 472818 472819 472820 472821 [...] (17642 flits)
Class 0:
Remaining flits: 508392 508393 508394 508395 508396 508397 508398 508399 508400 508401 [...] (17347 flits)
Measured flits: 508392 508393 508394 508395 508396 508397 508398 508399 508400 508401 [...] (17347 flits)
Class 0:
Remaining flits: 530982 530983 530984 530985 530986 530987 530988 530989 530990 530991 [...] (17227 flits)
Measured flits: 530982 530983 530984 530985 530986 530987 530988 530989 530990 530991 [...] (17227 flits)
Class 0:
Remaining flits: 558900 558901 558902 558903 558904 558905 558906 558907 558908 558909 [...] (17695 flits)
Measured flits: 558900 558901 558902 558903 558904 558905 558906 558907 558908 558909 [...] (17695 flits)
Class 0:
Remaining flits: 585072 585073 585074 585075 585076 585077 585078 585079 585080 585081 [...] (17545 flits)
Measured flits: 585072 585073 585074 585075 585076 585077 585078 585079 585080 585081 [...] (17545 flits)
Class 0:
Remaining flits: 597096 597097 597098 597099 597100 597101 597102 597103 597104 597105 [...] (17635 flits)
Measured flits: 597096 597097 597098 597099 597100 597101 597102 597103 597104 597105 [...] (17635 flits)
Class 0:
Remaining flits: 635318 635319 635320 635321 635322 635323 635324 635325 635326 635327 [...] (17272 flits)
Measured flits: 635318 635319 635320 635321 635322 635323 635324 635325 635326 635327 [...] (17272 flits)
Class 0:
Remaining flits: 667026 667027 667028 667029 667030 667031 667032 667033 667034 667035 [...] (17705 flits)
Measured flits: 667026 667027 667028 667029 667030 667031 667032 667033 667034 667035 [...] (17705 flits)
Class 0:
Remaining flits: 668723 668724 668725 668726 668727 668728 668729 668730 668731 668732 [...] (17457 flits)
Measured flits: 668723 668724 668725 668726 668727 668728 668729 668730 668731 668732 [...] (17457 flits)
Class 0:
Remaining flits: 750150 750151 750152 750153 750154 750155 750156 750157 750158 750159 [...] (17835 flits)
Measured flits: 750150 750151 750152 750153 750154 750155 750156 750157 750158 750159 [...] (17835 flits)
Class 0:
Remaining flits: 762372 762373 762374 762375 762376 762377 762378 762379 762380 762381 [...] (17764 flits)
Measured flits: 762372 762373 762374 762375 762376 762377 762378 762379 762380 762381 [...] (17764 flits)
Class 0:
Remaining flits: 764169 764170 764171 765756 765757 765758 765759 765760 765761 765762 [...] (17556 flits)
Measured flits: 764169 764170 764171 765756 765757 765758 765759 765760 765761 765762 [...] (17556 flits)
Class 0:
Remaining flits: 840060 840061 840062 840063 840064 840065 840066 840067 840068 840069 [...] (17872 flits)
Measured flits: 840060 840061 840062 840063 840064 840065 840066 840067 840068 840069 [...] (17782 flits)
Class 0:
Remaining flits: 879873 879874 879875 882540 882541 882542 882543 882544 882545 882546 [...] (17784 flits)
Measured flits: 879873 879874 879875 882540 882541 882542 882543 882544 882545 882546 [...] (17658 flits)
Class 0:
Remaining flits: 905938 905939 908766 908767 908768 908769 908770 908771 908772 908773 [...] (17786 flits)
Measured flits: 905938 905939 908766 908767 908768 908769 908770 908771 908772 908773 [...] (17124 flits)
Class 0:
Remaining flits: 944838 944839 944840 944841 944842 944843 944844 944845 944846 944847 [...] (17674 flits)
Measured flits: 944838 944839 944840 944841 944842 944843 944844 944845 944846 944847 [...] (16741 flits)
Class 0:
Remaining flits: 959353 959354 959355 959356 959357 959358 959359 959360 959361 959362 [...] (17977 flits)
Measured flits: 959353 959354 959355 959356 959357 959358 959359 959360 959361 959362 [...] (16171 flits)
Class 0:
Remaining flits: 999216 999217 999218 999219 999220 999221 999222 999223 999224 999225 [...] (17385 flits)
Measured flits: 999216 999217 999218 999219 999220 999221 999222 999223 999224 999225 [...] (14090 flits)
Class 0:
Remaining flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (17729 flits)
Measured flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (12872 flits)
Class 0:
Remaining flits: 1061190 1061191 1061192 1061193 1061194 1061195 1061196 1061197 1061198 1061199 [...] (17689 flits)
Measured flits: 1061190 1061191 1061192 1061193 1061194 1061195 1061196 1061197 1061198 1061199 [...] (11647 flits)
Class 0:
Remaining flits: 1092330 1092331 1092332 1092333 1092334 1092335 1092336 1092337 1092338 1092339 [...] (17975 flits)
Measured flits: 1092330 1092331 1092332 1092333 1092334 1092335 1092336 1092337 1092338 1092339 [...] (9461 flits)
Class 0:
Remaining flits: 1112886 1112887 1112888 1112889 1112890 1112891 1112892 1112893 1112894 1112895 [...] (17932 flits)
Measured flits: 1112886 1112887 1112888 1112889 1112890 1112891 1112892 1112893 1112894 1112895 [...] (7233 flits)
Class 0:
Remaining flits: 1154322 1154323 1154324 1154325 1154326 1154327 1154328 1154329 1154330 1154331 [...] (17823 flits)
Measured flits: 1159346 1159347 1159348 1159349 1159350 1159351 1159352 1159353 1159354 1159355 [...] (5054 flits)
Class 0:
Remaining flits: 1189278 1189279 1189280 1189281 1189282 1189283 1189284 1189285 1189286 1189287 [...] (17847 flits)
Measured flits: 1190289 1190290 1190291 1190292 1190293 1190294 1190295 1190296 1190297 1190298 [...] (3860 flits)
Class 0:
Remaining flits: 1204506 1204507 1204508 1204509 1204510 1204511 1204512 1204513 1204514 1204515 [...] (17609 flits)
Measured flits: 1222249 1222250 1222251 1222252 1222253 1232514 1232515 1232516 1232517 1232518 [...] (2403 flits)
Class 0:
Remaining flits: 1243962 1243963 1243964 1243965 1243966 1243967 1243968 1243969 1243970 1243971 [...] (17595 flits)
Measured flits: 1278252 1278253 1278254 1278255 1278256 1278257 1278258 1278259 1278260 1278261 [...] (1335 flits)
Class 0:
Remaining flits: 1275174 1275175 1275176 1275177 1275178 1275179 1275180 1275181 1275182 1275183 [...] (18156 flits)
Measured flits: 1287468 1287469 1287470 1287471 1287472 1287473 1287474 1287475 1287476 1287477 [...] (529 flits)
Class 0:
Remaining flits: 1301418 1301419 1301420 1301421 1301422 1301423 1301424 1301425 1301426 1301427 [...] (17486 flits)
Measured flits: 1301418 1301419 1301420 1301421 1301422 1301423 1301424 1301425 1301426 1301427 [...] (273 flits)
Class 0:
Remaining flits: 1340856 1340857 1340858 1340859 1340860 1340861 1340862 1340863 1340864 1340865 [...] (17647 flits)
Measured flits: 1355616 1355617 1355618 1355619 1355620 1355621 1355622 1355623 1355624 1355625 [...] (341 flits)
Class 0:
Remaining flits: 1358550 1358551 1358552 1358553 1358554 1358555 1358556 1358557 1358558 1358559 [...] (17546 flits)
Measured flits: 1401318 1401319 1401320 1401321 1401322 1401323 1401324 1401325 1401326 1401327 [...] (270 flits)
Class 0:
Remaining flits: 1382004 1382005 1382006 1382007 1382008 1382009 1382010 1382011 1382012 1382013 [...] (17377 flits)
Measured flits: 1429643 1429644 1429645 1429646 1429647 1429648 1429649 1443762 1443763 1443764 [...] (61 flits)
Draining remaining packets ...
Time taken is 48159 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15250.4 (1 samples)
	minimum = 1553 (1 samples)
	maximum = 37385 (1 samples)
Network latency average = 551.04 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3825 (1 samples)
Flit latency average = 490.514 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3795 (1 samples)
Fragmentation average = 58.4241 (1 samples)
	minimum = 0 (1 samples)
	maximum = 172 (1 samples)
Injected packet rate average = 0.00886161 (1 samples)
	minimum = 0.00485714 (1 samples)
	maximum = 0.0128571 (1 samples)
Accepted packet rate average = 0.00886905 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.159521 (1 samples)
	minimum = 0.0861429 (1 samples)
	maximum = 0.231286 (1 samples)
Accepted flit rate average = 0.159603 (1 samples)
	minimum = 0.111714 (1 samples)
	maximum = 0.217143 (1 samples)
Injected packet size average = 18.0013 (1 samples)
Accepted packet size average = 17.9956 (1 samples)
Hops average = 5.06971 (1 samples)
Total run time 35.7065
