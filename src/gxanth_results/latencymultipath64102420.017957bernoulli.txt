BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 171.187
	minimum = 22
	maximum = 510
Network latency average = 167.448
	minimum = 22
	maximum = 509
Slowest packet = 1296
Flit latency average = 138.137
	minimum = 5
	maximum = 539
Slowest flit = 28314
Fragmentation average = 29.5004
	minimum = 0
	maximum = 185
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.0129896
	minimum = 0.004 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.240182
	minimum = 0.072 (at node 41)
	maximum = 0.446 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 18.4904
Total in-flight flits = 15949 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 271.228
	minimum = 22
	maximum = 912
Network latency average = 267.344
	minimum = 22
	maximum = 912
Slowest packet = 3679
Flit latency average = 237.534
	minimum = 5
	maximum = 924
Slowest flit = 65658
Fragmentation average = 30.7983
	minimum = 0
	maximum = 191
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.013737
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.251135
	minimum = 0.1385 (at node 153)
	maximum = 0.36 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 18.2817
Total in-flight flits = 26270 (0 measured)
latency change    = 0.368843
throughput change = 0.0436144
Class 0:
Packet latency average = 526.403
	minimum = 25
	maximum = 1192
Network latency average = 522.369
	minimum = 22
	maximum = 1176
Slowest packet = 6141
Flit latency average = 493.331
	minimum = 5
	maximum = 1163
Slowest flit = 112689
Fragmentation average = 32.0444
	minimum = 0
	maximum = 237
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0147865
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.265547
	minimum = 0.108 (at node 163)
	maximum = 0.479 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 17.9588
Total in-flight flits = 37079 (0 measured)
latency change    = 0.484753
throughput change = 0.0542709
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 455.464
	minimum = 22
	maximum = 989
Network latency average = 451.504
	minimum = 22
	maximum = 989
Slowest packet = 10256
Flit latency average = 649.691
	minimum = 5
	maximum = 1465
Slowest flit = 152081
Fragmentation average = 24.3733
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0151302
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.272182
	minimum = 0.14 (at node 154)
	maximum = 0.494 (at node 90)
Injected packet length average = 17.9626
Accepted packet length average = 17.9893
Total in-flight flits = 48810 (46763 measured)
latency change    = 0.155751
throughput change = 0.0243786
Class 0:
Packet latency average = 739.389
	minimum = 22
	maximum = 1896
Network latency average = 735.235
	minimum = 22
	maximum = 1876
Slowest packet = 10293
Flit latency average = 733.452
	minimum = 5
	maximum = 1897
Slowest flit = 189718
Fragmentation average = 31.5991
	minimum = 0
	maximum = 232
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.0151042
	minimum = 0.0105 (at node 2)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.271617
	minimum = 0.1775 (at node 2)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 17.9829
Total in-flight flits = 58166 (58166 measured)
latency change    = 0.383999
throughput change = 0.00208052
Class 0:
Packet latency average = 867.911
	minimum = 22
	maximum = 2103
Network latency average = 863.659
	minimum = 22
	maximum = 2100
Slowest packet = 13109
Flit latency average = 821.389
	minimum = 5
	maximum = 2083
Slowest flit = 235979
Fragmentation average = 33.1853
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0150833
	minimum = 0.0106667 (at node 2)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.271663
	minimum = 0.185 (at node 2)
	maximum = 0.380667 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.0108
Total in-flight flits = 68665 (68665 measured)
latency change    = 0.148083
throughput change = 0.000169353
Class 0:
Packet latency average = 963.065
	minimum = 22
	maximum = 2322
Network latency average = 958.791
	minimum = 22
	maximum = 2285
Slowest packet = 15848
Flit latency average = 901.916
	minimum = 5
	maximum = 2268
Slowest flit = 285281
Fragmentation average = 33.8022
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0181263
	minimum = 0.0125 (at node 55)
	maximum = 0.02475 (at node 104)
Accepted packet rate average = 0.0150534
	minimum = 0.011 (at node 104)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.326395
	minimum = 0.225 (at node 55)
	maximum = 0.4425 (at node 104)
Accepted flit rate average= 0.271176
	minimum = 0.19825 (at node 104)
	maximum = 0.35775 (at node 128)
Injected packet length average = 18.0067
Accepted packet length average = 18.0143
Total in-flight flits = 79394 (79394 measured)
latency change    = 0.0988031
throughput change = 0.00179741
Class 0:
Packet latency average = 1053.38
	minimum = 22
	maximum = 2533
Network latency average = 1049.15
	minimum = 22
	maximum = 2533
Slowest packet = 18561
Flit latency average = 983.738
	minimum = 5
	maximum = 2516
Slowest flit = 334115
Fragmentation average = 34.2572
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0180781
	minimum = 0.0132 (at node 121)
	maximum = 0.0226 (at node 104)
Accepted packet rate average = 0.0150948
	minimum = 0.0114 (at node 42)
	maximum = 0.0196 (at node 95)
Injected flit rate average = 0.325454
	minimum = 0.2376 (at node 121)
	maximum = 0.4068 (at node 104)
Accepted flit rate average= 0.271508
	minimum = 0.2052 (at node 42)
	maximum = 0.3526 (at node 95)
Injected packet length average = 18.0027
Accepted packet length average = 17.9869
Total in-flight flits = 88821 (88821 measured)
latency change    = 0.0857402
throughput change = 0.00122483
Class 0:
Packet latency average = 1137.74
	minimum = 22
	maximum = 2655
Network latency average = 1133.55
	minimum = 22
	maximum = 2655
Slowest packet = 21685
Flit latency average = 1064.15
	minimum = 5
	maximum = 2663
Slowest flit = 392416
Fragmentation average = 34.157
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0180833
	minimum = 0.0145 (at node 69)
	maximum = 0.0225 (at node 104)
Accepted packet rate average = 0.0151094
	minimum = 0.0118333 (at node 42)
	maximum = 0.0195 (at node 103)
Injected flit rate average = 0.325551
	minimum = 0.261 (at node 69)
	maximum = 0.405 (at node 104)
Accepted flit rate average= 0.271986
	minimum = 0.213 (at node 42)
	maximum = 0.351 (at node 103)
Injected packet length average = 18.0028
Accepted packet length average = 18.0011
Total in-flight flits = 98727 (98727 measured)
latency change    = 0.0741434
throughput change = 0.00175663
Class 0:
Packet latency average = 1220.09
	minimum = 22
	maximum = 2911
Network latency average = 1215.92
	minimum = 22
	maximum = 2911
Slowest packet = 23754
Flit latency average = 1143.06
	minimum = 5
	maximum = 2894
Slowest flit = 427589
Fragmentation average = 34.5436
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0180737
	minimum = 0.0144286 (at node 23)
	maximum = 0.022 (at node 104)
Accepted packet rate average = 0.0151436
	minimum = 0.0124286 (at node 63)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.325296
	minimum = 0.260571 (at node 55)
	maximum = 0.396 (at node 104)
Accepted flit rate average= 0.272465
	minimum = 0.223714 (at node 63)
	maximum = 0.339429 (at node 70)
Injected packet length average = 17.9984
Accepted packet length average = 17.9921
Total in-flight flits = 108124 (108124 measured)
latency change    = 0.067494
throughput change = 0.00175773
Draining all recorded packets ...
Class 0:
Remaining flits: 493542 493543 493544 493545 493546 493547 493548 493549 493550 493551 [...] (118420 flits)
Measured flits: 493542 493543 493544 493545 493546 493547 493548 493549 493550 493551 [...] (60881 flits)
Class 0:
Remaining flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (126145 flits)
Measured flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (17643 flits)
Class 0:
Remaining flits: 588546 588547 588548 588549 588550 588551 588552 588553 588554 588555 [...] (134843 flits)
Measured flits: 588546 588547 588548 588549 588550 588551 588552 588553 588554 588555 [...] (847 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 670119 670120 670121 672390 672391 672392 672393 672394 672395 672396 [...] (92565 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 711794 711795 711796 711797 711798 711799 711800 711801 711802 711803 [...] (45298 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 761202 761203 761204 761205 761206 761207 761208 761209 761210 761211 [...] (6920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 821429 833976 833977 833978 833979 833980 833981 833982 833983 833984 [...] (37 flits)
Measured flits: (0 flits)
Time taken is 17630 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1466.66 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3732 (1 samples)
Network latency average = 1462.54 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3732 (1 samples)
Flit latency average = 1698.66 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4707 (1 samples)
Fragmentation average = 35.5981 (1 samples)
	minimum = 0 (1 samples)
	maximum = 343 (1 samples)
Injected packet rate average = 0.0180737 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0151436 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.325296 (1 samples)
	minimum = 0.260571 (1 samples)
	maximum = 0.396 (1 samples)
Accepted flit rate average = 0.272465 (1 samples)
	minimum = 0.223714 (1 samples)
	maximum = 0.339429 (1 samples)
Injected packet size average = 17.9984 (1 samples)
Accepted packet size average = 17.9921 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 16.5407
