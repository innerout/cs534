BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 399.804
	minimum = 27
	maximum = 951
Network latency average = 355.181
	minimum = 25
	maximum = 932
Slowest packet = 502
Flit latency average = 285.228
	minimum = 6
	maximum = 936
Slowest flit = 6097
Fragmentation average = 168.663
	minimum = 0
	maximum = 836
Injected packet rate average = 0.021901
	minimum = 0.008 (at node 116)
	maximum = 0.033 (at node 39)
Accepted packet rate average = 0.00997917
	minimum = 0.004 (at node 41)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.38713
	minimum = 0.144 (at node 116)
	maximum = 0.586 (at node 39)
Accepted flit rate average= 0.205625
	minimum = 0.086 (at node 150)
	maximum = 0.359 (at node 22)
Injected packet length average = 17.6763
Accepted packet length average = 20.6054
Total in-flight flits = 36462 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 781.434
	minimum = 27
	maximum = 1923
Network latency average = 592.379
	minimum = 25
	maximum = 1902
Slowest packet = 606
Flit latency average = 493.48
	minimum = 6
	maximum = 1932
Slowest flit = 7173
Fragmentation average = 199.169
	minimum = 0
	maximum = 1833
Injected packet rate average = 0.0164922
	minimum = 0.0055 (at node 154)
	maximum = 0.026 (at node 45)
Accepted packet rate average = 0.010599
	minimum = 0.0055 (at node 153)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.293526
	minimum = 0.094 (at node 154)
	maximum = 0.4625 (at node 45)
Accepted flit rate average= 0.203357
	minimum = 0.101 (at node 153)
	maximum = 0.3085 (at node 22)
Injected packet length average = 17.7979
Accepted packet length average = 19.1865
Total in-flight flits = 36211 (0 measured)
latency change    = 0.488371
throughput change = 0.0111539
Class 0:
Packet latency average = 1901.24
	minimum = 925
	maximum = 2891
Network latency average = 979.312
	minimum = 25
	maximum = 2888
Slowest packet = 209
Flit latency average = 830.545
	minimum = 6
	maximum = 2871
Slowest flit = 3779
Fragmentation average = 218.687
	minimum = 0
	maximum = 2657
Injected packet rate average = 0.0109844
	minimum = 0 (at node 8)
	maximum = 0.037 (at node 11)
Accepted packet rate average = 0.011026
	minimum = 0.002 (at node 105)
	maximum = 0.021 (at node 106)
Injected flit rate average = 0.197906
	minimum = 0 (at node 8)
	maximum = 0.661 (at node 11)
Accepted flit rate average= 0.195604
	minimum = 0.062 (at node 105)
	maximum = 0.382 (at node 119)
Injected packet length average = 18.0171
Accepted packet length average = 17.7402
Total in-flight flits = 36761 (0 measured)
latency change    = 0.588988
throughput change = 0.0396341
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2693.81
	minimum = 1686
	maximum = 3650
Network latency average = 360.199
	minimum = 27
	maximum = 968
Slowest packet = 8477
Flit latency average = 905.621
	minimum = 6
	maximum = 3926
Slowest flit = 5273
Fragmentation average = 130.988
	minimum = 0
	maximum = 743
Injected packet rate average = 0.0108281
	minimum = 0 (at node 26)
	maximum = 0.034 (at node 147)
Accepted packet rate average = 0.0108594
	minimum = 0.004 (at node 85)
	maximum = 0.019 (at node 63)
Injected flit rate average = 0.194359
	minimum = 0 (at node 26)
	maximum = 0.596 (at node 147)
Accepted flit rate average= 0.194016
	minimum = 0.077 (at node 146)
	maximum = 0.323 (at node 182)
Injected packet length average = 17.9495
Accepted packet length average = 17.8662
Total in-flight flits = 36932 (25252 measured)
latency change    = 0.294218
throughput change = 0.0081877
Class 0:
Packet latency average = 3211.65
	minimum = 1686
	maximum = 4597
Network latency average = 605.712
	minimum = 24
	maximum = 1975
Slowest packet = 8477
Flit latency average = 903.198
	minimum = 6
	maximum = 4575
Slowest flit = 41543
Fragmentation average = 162.931
	minimum = 0
	maximum = 1373
Injected packet rate average = 0.0105469
	minimum = 0 (at node 31)
	maximum = 0.0275 (at node 147)
Accepted packet rate average = 0.0107292
	minimum = 0.0065 (at node 4)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.189375
	minimum = 0.003 (at node 74)
	maximum = 0.49 (at node 147)
Accepted flit rate average= 0.193128
	minimum = 0.1075 (at node 93)
	maximum = 0.3095 (at node 129)
Injected packet length average = 17.9556
Accepted packet length average = 18.0002
Total in-flight flits = 35428 (31765 measured)
latency change    = 0.161238
throughput change = 0.0045981
Class 0:
Packet latency average = 3670.22
	minimum = 1686
	maximum = 5483
Network latency average = 754.033
	minimum = 24
	maximum = 2920
Slowest packet = 8477
Flit latency average = 913.942
	minimum = 6
	maximum = 5804
Slowest flit = 25295
Fragmentation average = 172.075
	minimum = 0
	maximum = 1373
Injected packet rate average = 0.0105764
	minimum = 0 (at node 86)
	maximum = 0.0223333 (at node 23)
Accepted packet rate average = 0.0106753
	minimum = 0.00566667 (at node 36)
	maximum = 0.0183333 (at node 129)
Injected flit rate average = 0.190198
	minimum = 0.003 (at node 86)
	maximum = 0.399333 (at node 23)
Accepted flit rate average= 0.192498
	minimum = 0.107 (at node 36)
	maximum = 0.314 (at node 129)
Injected packet length average = 17.9833
Accepted packet length average = 18.032
Total in-flight flits = 35502 (34233 measured)
latency change    = 0.124943
throughput change = 0.00326933
Class 0:
Packet latency average = 4116.8
	minimum = 1686
	maximum = 6356
Network latency average = 827.818
	minimum = 24
	maximum = 3755
Slowest packet = 8477
Flit latency average = 914.993
	minimum = 6
	maximum = 6212
Slowest flit = 56446
Fragmentation average = 182.713
	minimum = 0
	maximum = 2928
Injected packet rate average = 0.010724
	minimum = 0.00025 (at node 41)
	maximum = 0.02075 (at node 25)
Accepted packet rate average = 0.0107617
	minimum = 0.0065 (at node 36)
	maximum = 0.016 (at node 128)
Injected flit rate average = 0.19297
	minimum = 0.005 (at node 41)
	maximum = 0.37175 (at node 25)
Accepted flit rate average= 0.193421
	minimum = 0.12025 (at node 36)
	maximum = 0.28425 (at node 128)
Injected packet length average = 17.9943
Accepted packet length average = 17.973
Total in-flight flits = 36390 (36065 measured)
latency change    = 0.108478
throughput change = 0.00476841
Class 0:
Packet latency average = 4528.98
	minimum = 1686
	maximum = 7287
Network latency average = 874.645
	minimum = 24
	maximum = 4531
Slowest packet = 8477
Flit latency average = 912.316
	minimum = 6
	maximum = 6868
Slowest flit = 61685
Fragmentation average = 187.069
	minimum = 0
	maximum = 2928
Injected packet rate average = 0.0106844
	minimum = 0.0016 (at node 18)
	maximum = 0.0206 (at node 25)
Accepted packet rate average = 0.0107281
	minimum = 0.0066 (at node 36)
	maximum = 0.015 (at node 128)
Injected flit rate average = 0.192179
	minimum = 0.0278 (at node 18)
	maximum = 0.3714 (at node 25)
Accepted flit rate average= 0.193132
	minimum = 0.1196 (at node 36)
	maximum = 0.272 (at node 128)
Injected packet length average = 17.9869
Accepted packet length average = 18.0024
Total in-flight flits = 35872 (35797 measured)
latency change    = 0.0910096
throughput change = 0.00149266
Class 0:
Packet latency average = 4945.38
	minimum = 1686
	maximum = 8063
Network latency average = 919.223
	minimum = 24
	maximum = 5551
Slowest packet = 8477
Flit latency average = 918.274
	minimum = 6
	maximum = 6868
Slowest flit = 61685
Fragmentation average = 193.841
	minimum = 0
	maximum = 4523
Injected packet rate average = 0.0107778
	minimum = 0.00366667 (at node 32)
	maximum = 0.0181667 (at node 17)
Accepted packet rate average = 0.0107509
	minimum = 0.00716667 (at node 2)
	maximum = 0.0146667 (at node 129)
Injected flit rate average = 0.193895
	minimum = 0.066 (at node 32)
	maximum = 0.327667 (at node 25)
Accepted flit rate average= 0.193253
	minimum = 0.132 (at node 2)
	maximum = 0.257333 (at node 129)
Injected packet length average = 17.9903
Accepted packet length average = 17.9755
Total in-flight flits = 37784 (37766 measured)
latency change    = 0.0841994
throughput change = 0.000622566
Class 0:
Packet latency average = 5340.91
	minimum = 1686
	maximum = 8874
Network latency average = 943.279
	minimum = 23
	maximum = 6139
Slowest packet = 8477
Flit latency average = 919.358
	minimum = 6
	maximum = 6868
Slowest flit = 61685
Fragmentation average = 194.835
	minimum = 0
	maximum = 4523
Injected packet rate average = 0.010689
	minimum = 0.00328571 (at node 32)
	maximum = 0.0174286 (at node 25)
Accepted packet rate average = 0.0107158
	minimum = 0.008 (at node 2)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.192383
	minimum = 0.057 (at node 32)
	maximum = 0.314857 (at node 25)
Accepted flit rate average= 0.192731
	minimum = 0.143 (at node 79)
	maximum = 0.255714 (at node 128)
Injected packet length average = 17.9983
Accepted packet length average = 17.9858
Total in-flight flits = 36282 (36274 measured)
latency change    = 0.0740569
throughput change = 0.00270431
Draining all recorded packets ...
Class 0:
Remaining flits: 131122 131123 131124 131125 131126 131127 131128 131129 244746 244747 [...] (37307 flits)
Measured flits: 244746 244747 244748 244749 244750 244751 244752 244753 244754 244755 [...] (37299 flits)
Class 0:
Remaining flits: 251692 251693 253884 253885 253886 253887 253888 253889 260172 260173 [...] (36715 flits)
Measured flits: 251692 251693 253884 253885 253886 253887 253888 253889 260172 260173 [...] (36715 flits)
Class 0:
Remaining flits: 306630 306631 306632 306633 306634 306635 306636 306637 306638 306639 [...] (36611 flits)
Measured flits: 306630 306631 306632 306633 306634 306635 306636 306637 306638 306639 [...] (36611 flits)
Class 0:
Remaining flits: 306634 306635 306636 306637 306638 306639 306640 306641 306642 306643 [...] (36901 flits)
Measured flits: 306634 306635 306636 306637 306638 306639 306640 306641 306642 306643 [...] (36901 flits)
Class 0:
Remaining flits: 352044 352045 352046 352047 352048 352049 352050 352051 352052 352053 [...] (37018 flits)
Measured flits: 352044 352045 352046 352047 352048 352049 352050 352051 352052 352053 [...] (37018 flits)
Class 0:
Remaining flits: 352061 364893 364894 364895 368694 368695 368696 368697 368698 368699 [...] (35258 flits)
Measured flits: 352061 364893 364894 364895 368694 368695 368696 368697 368698 368699 [...] (35258 flits)
Class 0:
Remaining flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (35474 flits)
Measured flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (35474 flits)
Class 0:
Remaining flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (36398 flits)
Measured flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (36398 flits)
Class 0:
Remaining flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (36612 flits)
Measured flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (36612 flits)
Class 0:
Remaining flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (35389 flits)
Measured flits: 390960 390961 390962 390963 390964 390965 390966 390967 390968 390969 [...] (35389 flits)
Class 0:
Remaining flits: 458838 458839 458840 458841 458842 458843 458844 458845 458846 458847 [...] (36284 flits)
Measured flits: 458838 458839 458840 458841 458842 458843 458844 458845 458846 458847 [...] (36284 flits)
Class 0:
Remaining flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (37252 flits)
Measured flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (37252 flits)
Class 0:
Remaining flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36639 flits)
Measured flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36639 flits)
Class 0:
Remaining flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36119 flits)
Measured flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36119 flits)
Class 0:
Remaining flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36048 flits)
Measured flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (36048 flits)
Class 0:
Remaining flits: 641880 641881 641882 641883 641884 641885 641886 641887 641888 641889 [...] (35320 flits)
Measured flits: 641880 641881 641882 641883 641884 641885 641886 641887 641888 641889 [...] (35320 flits)
Class 0:
Remaining flits: 736319 736320 736321 736322 736323 736324 736325 749718 749719 749720 [...] (35937 flits)
Measured flits: 736319 736320 736321 736322 736323 736324 736325 749718 749719 749720 [...] (35937 flits)
Class 0:
Remaining flits: 749720 749721 749722 749723 749724 749725 749726 749727 749728 749729 [...] (36239 flits)
Measured flits: 749720 749721 749722 749723 749724 749725 749726 749727 749728 749729 [...] (36221 flits)
Class 0:
Remaining flits: 805356 805357 805358 805359 805360 805361 805362 805363 805364 805365 [...] (36990 flits)
Measured flits: 805356 805357 805358 805359 805360 805361 805362 805363 805364 805365 [...] (36918 flits)
Class 0:
Remaining flits: 856957 856958 856959 856960 856961 890162 890163 890164 890165 890166 [...] (35418 flits)
Measured flits: 856957 856958 856959 856960 856961 890162 890163 890164 890165 890166 [...] (35186 flits)
Class 0:
Remaining flits: 902358 902359 902360 902361 902362 902363 902364 902365 902366 902367 [...] (35039 flits)
Measured flits: 902358 902359 902360 902361 902362 902363 902364 902365 902366 902367 [...] (34735 flits)
Class 0:
Remaining flits: 906552 906553 906554 906555 906556 906557 906558 906559 906560 906561 [...] (36615 flits)
Measured flits: 906552 906553 906554 906555 906556 906557 906558 906559 906560 906561 [...] (36251 flits)
Class 0:
Remaining flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (36139 flits)
Measured flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (35696 flits)
Class 0:
Remaining flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (34624 flits)
Measured flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (34108 flits)
Class 0:
Remaining flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (35212 flits)
Measured flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (33944 flits)
Class 0:
Remaining flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (37354 flits)
Measured flits: 914238 914239 914240 914241 914242 914243 914244 914245 914246 914247 [...] (35866 flits)
Class 0:
Remaining flits: 1036764 1036765 1036766 1036767 1036768 1036769 1036770 1036771 1036772 1036773 [...] (35307 flits)
Measured flits: 1036764 1036765 1036766 1036767 1036768 1036769 1036770 1036771 1036772 1036773 [...] (32441 flits)
Class 0:
Remaining flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (35638 flits)
Measured flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (31181 flits)
Class 0:
Remaining flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (36215 flits)
Measured flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (28881 flits)
Class 0:
Remaining flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (36046 flits)
Measured flits: 1046538 1046539 1046540 1046541 1046542 1046543 1046544 1046545 1046546 1046547 [...] (27825 flits)
Class 0:
Remaining flits: 1059699 1059700 1059701 1059702 1059703 1059704 1059705 1059706 1059707 1059708 [...] (35953 flits)
Measured flits: 1059699 1059700 1059701 1059702 1059703 1059704 1059705 1059706 1059707 1059708 [...] (24549 flits)
Class 0:
Remaining flits: 1126008 1126009 1126010 1126011 1126012 1126013 1126014 1126015 1126016 1126017 [...] (36267 flits)
Measured flits: 1126008 1126009 1126010 1126011 1126012 1126013 1126014 1126015 1126016 1126017 [...] (22460 flits)
Class 0:
Remaining flits: 1198944 1198945 1198946 1198947 1198948 1198949 1198950 1198951 1198952 1198953 [...] (36042 flits)
Measured flits: 1198944 1198945 1198946 1198947 1198948 1198949 1198950 1198951 1198952 1198953 [...] (18800 flits)
Class 0:
Remaining flits: 1206901 1206902 1206903 1206904 1206905 1206906 1206907 1206908 1206909 1206910 [...] (36280 flits)
Measured flits: 1206901 1206902 1206903 1206904 1206905 1206906 1206907 1206908 1206909 1206910 [...] (16908 flits)
Class 0:
Remaining flits: 1243656 1243657 1243658 1243659 1243660 1243661 1243662 1243663 1243664 1243665 [...] (36110 flits)
Measured flits: 1243656 1243657 1243658 1243659 1243660 1243661 1243662 1243663 1243664 1243665 [...] (14440 flits)
Class 0:
Remaining flits: 1243656 1243657 1243658 1243659 1243660 1243661 1243662 1243663 1243664 1243665 [...] (35816 flits)
Measured flits: 1243656 1243657 1243658 1243659 1243660 1243661 1243662 1243663 1243664 1243665 [...] (12194 flits)
Class 0:
Remaining flits: 1387210 1387211 1387212 1387213 1387214 1387215 1387216 1387217 1387218 1387219 [...] (36099 flits)
Measured flits: 1387210 1387211 1387212 1387213 1387214 1387215 1387216 1387217 1387218 1387219 [...] (11817 flits)
Class 0:
Remaining flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (36476 flits)
Measured flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (10296 flits)
Class 0:
Remaining flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (36411 flits)
Measured flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (8874 flits)
Class 0:
Remaining flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (35688 flits)
Measured flits: 1458954 1458955 1458956 1458957 1458958 1458959 1458960 1458961 1458962 1458963 [...] (7694 flits)
Class 0:
Remaining flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (36276 flits)
Measured flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (6897 flits)
Class 0:
Remaining flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (36708 flits)
Measured flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (6351 flits)
Class 0:
Remaining flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (37886 flits)
Measured flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (5803 flits)
Class 0:
Remaining flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (36978 flits)
Measured flits: 1486404 1486405 1486406 1486407 1486408 1486409 1486410 1486411 1486412 1486413 [...] (5088 flits)
Class 0:
Remaining flits: 1729980 1729981 1729982 1729983 1729984 1729985 1729986 1729987 1729988 1729989 [...] (37189 flits)
Measured flits: 1792116 1792117 1792118 1792119 1792120 1792121 1792122 1792123 1792124 1792125 [...] (4233 flits)
Class 0:
Remaining flits: 1792116 1792117 1792118 1792119 1792120 1792121 1792122 1792123 1792124 1792125 [...] (36482 flits)
Measured flits: 1792116 1792117 1792118 1792119 1792120 1792121 1792122 1792123 1792124 1792125 [...] (3696 flits)
Class 0:
Remaining flits: 1825956 1825957 1825958 1825959 1825960 1825961 1825962 1825963 1825964 1825965 [...] (38769 flits)
Measured flits: 1825956 1825957 1825958 1825959 1825960 1825961 1825962 1825963 1825964 1825965 [...] (3925 flits)
Class 0:
Remaining flits: 1851892 1851893 1872630 1872631 1872632 1872633 1872634 1872635 1872636 1872637 [...] (37822 flits)
Measured flits: 1909260 1909261 1909262 1909263 1909264 1909265 1909266 1909267 1909268 1909269 [...] (3314 flits)
Class 0:
Remaining flits: 1916495 1921066 1921067 1931145 1931146 1931147 1943802 1943803 1943804 1943805 [...] (36105 flits)
Measured flits: 1916495 1921066 1921067 1952773 1952774 1952775 1952776 1952777 1952778 1952779 [...] (3281 flits)
Class 0:
Remaining flits: 1943802 1943803 1943804 1943805 1943806 1943807 1943808 1943809 1943810 1943811 [...] (38704 flits)
Measured flits: 1962414 1962415 1962416 1962417 1962418 1962419 1962420 1962421 1962422 1962423 [...] (2611 flits)
Class 0:
Remaining flits: 1960196 1960197 1960198 1960199 1962415 1962416 1962417 1962418 1962419 1962420 [...] (36006 flits)
Measured flits: 1962415 1962416 1962417 1962418 1962419 1962420 1962421 1962422 1962423 1962424 [...] (2302 flits)
Class 0:
Remaining flits: 1975500 1975501 1975502 1975503 1975504 1975505 1975506 1975507 1975508 1975509 [...] (36226 flits)
Measured flits: 1975500 1975501 1975502 1975503 1975504 1975505 1975506 1975507 1975508 1975509 [...] (2442 flits)
Class 0:
Remaining flits: 1975514 1975515 1975516 1975517 2030895 2030896 2030897 2030898 2030899 2030900 [...] (35280 flits)
Measured flits: 1975514 1975515 1975516 1975517 2170420 2170421 2170962 2170963 2170964 2170965 [...] (1622 flits)
Class 0:
Remaining flits: 2038446 2038447 2038448 2038449 2038450 2038451 2038452 2038453 2038454 2038455 [...] (35640 flits)
Measured flits: 2206194 2206195 2206196 2206197 2206198 2206199 2206200 2206201 2206202 2206203 [...] (1159 flits)
Class 0:
Remaining flits: 2038461 2038462 2038463 2081140 2081141 2120580 2120581 2120582 2120583 2120584 [...] (35678 flits)
Measured flits: 2217132 2217133 2217134 2217135 2217136 2217137 2217138 2217139 2217140 2217141 [...] (957 flits)
Class 0:
Remaining flits: 2173877 2195460 2195461 2195462 2195463 2195464 2195465 2195466 2195467 2195468 [...] (37290 flits)
Measured flits: 2278529 2291085 2291086 2291087 2291088 2291089 2291090 2291091 2291092 2291093 [...] (927 flits)
Class 0:
Remaining flits: 2207556 2207557 2207558 2207559 2207560 2207561 2207562 2207563 2207564 2207565 [...] (35659 flits)
Measured flits: 2351466 2351467 2351468 2351469 2351470 2351471 2351472 2351473 2351474 2351475 [...] (857 flits)
Class 0:
Remaining flits: 2207556 2207557 2207558 2207559 2207560 2207561 2207562 2207563 2207564 2207565 [...] (37252 flits)
Measured flits: 2351466 2351467 2351468 2351469 2351470 2351471 2351472 2351473 2351474 2351475 [...] (541 flits)
Class 0:
Remaining flits: 2243042 2243043 2243044 2243045 2243046 2243047 2243048 2243049 2243050 2243051 [...] (34908 flits)
Measured flits: 2351480 2351481 2351482 2351483 2416392 2416393 2416394 2416395 2416396 2416397 [...] (291 flits)
Class 0:
Remaining flits: 2268898 2268899 2270322 2270323 2270324 2270325 2270326 2270327 2270328 2270329 [...] (37070 flits)
Measured flits: 2416392 2416393 2416394 2416395 2416396 2416397 2416398 2416399 2416400 2416401 [...] (224 flits)
Class 0:
Remaining flits: 2336202 2336203 2336204 2336205 2336206 2336207 2336208 2336209 2336210 2336211 [...] (37174 flits)
Measured flits: 2565756 2565757 2565758 2565759 2565760 2565761 2565762 2565763 2565764 2565765 [...] (211 flits)
Class 0:
Remaining flits: 2336202 2336203 2336204 2336205 2336206 2336207 2336208 2336209 2336210 2336211 [...] (36726 flits)
Measured flits: 2595384 2595385 2595386 2595387 2595388 2595389 2595390 2595391 2595392 2595393 [...] (235 flits)
Class 0:
Remaining flits: 2336202 2336203 2336204 2336205 2336206 2336207 2336208 2336209 2336210 2336211 [...] (37210 flits)
Measured flits: 2595384 2595385 2595386 2595387 2595388 2595389 2595390 2595391 2595392 2595393 [...] (186 flits)
Class 0:
Remaining flits: 2336202 2336203 2336204 2336205 2336206 2336207 2336208 2336209 2336210 2336211 [...] (38270 flits)
Measured flits: 2595384 2595385 2595386 2595387 2595388 2595389 2595390 2595391 2595392 2595393 [...] (224 flits)
Class 0:
Remaining flits: 2336202 2336203 2336204 2336205 2336206 2336207 2336208 2336209 2336210 2336211 [...] (36830 flits)
Measured flits: 2677701 2677702 2677703 2677704 2677705 2677706 2677707 2677708 2677709 2677710 [...] (176 flits)
Class 0:
Remaining flits: 2452698 2452699 2452700 2452701 2452702 2452703 2452704 2452705 2452706 2452707 [...] (35136 flits)
Measured flits: 2677714 2677715 2743938 2743939 2743940 2743941 2743942 2743943 2743944 2743945 [...] (74 flits)
Class 0:
Remaining flits: 2452698 2452699 2452700 2452701 2452702 2452703 2452704 2452705 2452706 2452707 [...] (35331 flits)
Measured flits: 2768328 2768329 2768330 2768331 2768332 2768333 2768334 2768335 2768336 2768337 [...] (36 flits)
Class 0:
Remaining flits: 2452714 2452715 2456438 2456439 2456440 2456441 2500182 2500183 2500184 2500185 [...] (35331 flits)
Measured flits: 2781795 2781796 2781797 2781798 2781799 2781800 2781801 2781802 2781803 2781804 [...] (69 flits)
Class 0:
Remaining flits: 2501297 2558178 2558179 2558180 2558181 2558182 2558183 2558184 2558185 2558186 [...] (36048 flits)
Measured flits: 2781806 2781807 2781808 2781809 2839403 2839404 2839405 2839406 2839407 2839408 [...] (63 flits)
Class 0:
Remaining flits: 2558178 2558179 2558180 2558181 2558182 2558183 2558184 2558185 2558186 2558187 [...] (35148 flits)
Measured flits: 2781806 2781807 2781808 2781809 2918880 2918881 2918882 2918883 2918884 2918885 [...] (22 flits)
Class 0:
Remaining flits: 2585564 2585565 2585566 2585567 2585568 2585569 2585570 2585571 2585572 2585573 [...] (36085 flits)
Measured flits: 2918882 2918883 2918884 2918885 2918886 2918887 2918888 2918889 2918890 2918891 [...] (16 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2764449 2764450 2764451 2764452 2764453 2764454 2764455 2764456 2764457 2788596 [...] (6854 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972401 2972402 2972403 2972404 2972405 2972406 2972407 2972408 2972409 2972410 [...] (27 flits)
Measured flits: (0 flits)
Time taken is 83185 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 20743 (1 samples)
	minimum = 1686 (1 samples)
	maximum = 71192 (1 samples)
Network latency average = 1070.09 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14510 (1 samples)
Flit latency average = 957.072 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14493 (1 samples)
Fragmentation average = 202.657 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5864 (1 samples)
Injected packet rate average = 0.010689 (1 samples)
	minimum = 0.00328571 (1 samples)
	maximum = 0.0174286 (1 samples)
Accepted packet rate average = 0.0107158 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.192383 (1 samples)
	minimum = 0.057 (1 samples)
	maximum = 0.314857 (1 samples)
Accepted flit rate average = 0.192731 (1 samples)
	minimum = 0.143 (1 samples)
	maximum = 0.255714 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 17.9858 (1 samples)
Hops average = 5.06283 (1 samples)
Total run time 94.4074
