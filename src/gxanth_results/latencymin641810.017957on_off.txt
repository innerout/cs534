BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.825
	minimum = 23
	maximum = 989
Network latency average = 233.986
	minimum = 23
	maximum = 972
Slowest packet = 104
Flit latency average = 158.536
	minimum = 6
	maximum = 955
Slowest flit = 1889
Fragmentation average = 149.772
	minimum = 0
	maximum = 927
Injected packet rate average = 0.0166927
	minimum = 0 (at node 5)
	maximum = 0.049 (at node 117)
Accepted packet rate average = 0.00892188
	minimum = 0.001 (at node 174)
	maximum = 0.018 (at node 132)
Injected flit rate average = 0.297625
	minimum = 0 (at node 5)
	maximum = 0.871 (at node 117)
Accepted flit rate average= 0.187724
	minimum = 0.047 (at node 174)
	maximum = 0.333 (at node 132)
Injected packet length average = 17.8296
Accepted packet length average = 21.0409
Total in-flight flits = 21647 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.649
	minimum = 23
	maximum = 1831
Network latency average = 375.415
	minimum = 23
	maximum = 1779
Slowest packet = 305
Flit latency average = 273.977
	minimum = 6
	maximum = 1820
Slowest flit = 10686
Fragmentation average = 209.137
	minimum = 0
	maximum = 1607
Injected packet rate average = 0.0159036
	minimum = 0.001 (at node 184)
	maximum = 0.0345 (at node 117)
Accepted packet rate average = 0.0102109
	minimum = 0.006 (at node 81)
	maximum = 0.015 (at node 131)
Injected flit rate average = 0.284445
	minimum = 0.018 (at node 184)
	maximum = 0.6125 (at node 117)
Accepted flit rate average= 0.200604
	minimum = 0.119 (at node 81)
	maximum = 0.292 (at node 98)
Injected packet length average = 17.8855
Accepted packet length average = 19.646
Total in-flight flits = 32912 (0 measured)
latency change    = 0.361184
throughput change = 0.0642071
Class 0:
Packet latency average = 824.871
	minimum = 28
	maximum = 2888
Network latency average = 647.611
	minimum = 27
	maximum = 2632
Slowest packet = 573
Flit latency average = 525.286
	minimum = 6
	maximum = 2842
Slowest flit = 8228
Fragmentation average = 270.07
	minimum = 0
	maximum = 2398
Injected packet rate average = 0.0139271
	minimum = 0 (at node 26)
	maximum = 0.04 (at node 145)
Accepted packet rate average = 0.0121875
	minimum = 0.006 (at node 77)
	maximum = 0.024 (at node 42)
Injected flit rate average = 0.250245
	minimum = 0 (at node 26)
	maximum = 0.713 (at node 145)
Accepted flit rate average= 0.222417
	minimum = 0.083 (at node 98)
	maximum = 0.404 (at node 42)
Injected packet length average = 17.9682
Accepted packet length average = 18.2496
Total in-flight flits = 38484 (0 measured)
latency change    = 0.436702
throughput change = 0.0980704
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 636.098
	minimum = 31
	maximum = 3162
Network latency average = 320.043
	minimum = 25
	maximum = 945
Slowest packet = 8797
Flit latency average = 695.97
	minimum = 6
	maximum = 3851
Slowest flit = 8242
Fragmentation average = 165.136
	minimum = 0
	maximum = 612
Injected packet rate average = 0.0112812
	minimum = 0 (at node 7)
	maximum = 0.04 (at node 85)
Accepted packet rate average = 0.0120625
	minimum = 0.005 (at node 32)
	maximum = 0.022 (at node 37)
Injected flit rate average = 0.201521
	minimum = 0 (at node 7)
	maximum = 0.716 (at node 85)
Accepted flit rate average= 0.215021
	minimum = 0.089 (at node 35)
	maximum = 0.373 (at node 37)
Injected packet length average = 17.8633
Accepted packet length average = 17.8256
Total in-flight flits = 36170 (20174 measured)
latency change    = 0.296768
throughput change = 0.0343959
Class 0:
Packet latency average = 889.849
	minimum = 29
	maximum = 3945
Network latency average = 459.276
	minimum = 25
	maximum = 1855
Slowest packet = 8797
Flit latency average = 725.821
	minimum = 6
	maximum = 4723
Slowest flit = 9588
Fragmentation average = 196.168
	minimum = 0
	maximum = 1463
Injected packet rate average = 0.011776
	minimum = 0 (at node 7)
	maximum = 0.0325 (at node 143)
Accepted packet rate average = 0.0118672
	minimum = 0.006 (at node 36)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.211388
	minimum = 0 (at node 35)
	maximum = 0.5865 (at node 143)
Accepted flit rate average= 0.211922
	minimum = 0.129 (at node 36)
	maximum = 0.323 (at node 129)
Injected packet length average = 17.9507
Accepted packet length average = 17.8578
Total in-flight flits = 38538 (29851 measured)
latency change    = 0.285162
throughput change = 0.0146231
Class 0:
Packet latency average = 1095.71
	minimum = 29
	maximum = 4703
Network latency average = 553.039
	minimum = 25
	maximum = 2939
Slowest packet = 8797
Flit latency average = 732.898
	minimum = 6
	maximum = 5828
Slowest flit = 5561
Fragmentation average = 213.153
	minimum = 0
	maximum = 2557
Injected packet rate average = 0.0120556
	minimum = 0 (at node 83)
	maximum = 0.0296667 (at node 143)
Accepted packet rate average = 0.0117309
	minimum = 0.007 (at node 2)
	maximum = 0.0166667 (at node 128)
Injected flit rate average = 0.216354
	minimum = 0 (at node 140)
	maximum = 0.534 (at node 143)
Accepted flit rate average= 0.209878
	minimum = 0.138667 (at node 146)
	maximum = 0.310333 (at node 128)
Injected packet length average = 17.9464
Accepted packet length average = 17.8911
Total in-flight flits = 42568 (36685 measured)
latency change    = 0.187882
throughput change = 0.00973612
Class 0:
Packet latency average = 1317.9
	minimum = 29
	maximum = 5926
Network latency average = 634.903
	minimum = 24
	maximum = 3833
Slowest packet = 8797
Flit latency average = 763.441
	minimum = 6
	maximum = 6568
Slowest flit = 10997
Fragmentation average = 224.918
	minimum = 0
	maximum = 2557
Injected packet rate average = 0.0122852
	minimum = 0 (at node 140)
	maximum = 0.0265 (at node 143)
Accepted packet rate average = 0.0117318
	minimum = 0.00775 (at node 36)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.220574
	minimum = 0 (at node 140)
	maximum = 0.475 (at node 143)
Accepted flit rate average= 0.210072
	minimum = 0.14925 (at node 2)
	maximum = 0.30625 (at node 128)
Injected packet length average = 17.9545
Accepted packet length average = 17.9062
Total in-flight flits = 47033 (42915 measured)
latency change    = 0.168592
throughput change = 0.000919412
Class 0:
Packet latency average = 1529.96
	minimum = 29
	maximum = 7100
Network latency average = 719.169
	minimum = 23
	maximum = 4663
Slowest packet = 8797
Flit latency average = 812.901
	minimum = 6
	maximum = 7560
Slowest flit = 23219
Fragmentation average = 237.225
	minimum = 0
	maximum = 4461
Injected packet rate average = 0.0120458
	minimum = 0 (at node 140)
	maximum = 0.0234 (at node 11)
Accepted packet rate average = 0.0116927
	minimum = 0.0084 (at node 89)
	maximum = 0.0164 (at node 128)
Injected flit rate average = 0.216357
	minimum = 0 (at node 140)
	maximum = 0.4192 (at node 11)
Accepted flit rate average= 0.209628
	minimum = 0.1512 (at node 146)
	maximum = 0.289 (at node 128)
Injected packet length average = 17.9612
Accepted packet length average = 17.9281
Total in-flight flits = 45501 (42829 measured)
latency change    = 0.138602
throughput change = 0.0021156
Class 0:
Packet latency average = 1734.57
	minimum = 28
	maximum = 7488
Network latency average = 790.584
	minimum = 23
	maximum = 5613
Slowest packet = 8797
Flit latency average = 845.97
	minimum = 6
	maximum = 8691
Slowest flit = 17146
Fragmentation average = 245.665
	minimum = 0
	maximum = 5529
Injected packet rate average = 0.0120451
	minimum = 0 (at node 140)
	maximum = 0.0226667 (at node 11)
Accepted packet rate average = 0.011691
	minimum = 0.00816667 (at node 42)
	maximum = 0.0161667 (at node 128)
Injected flit rate average = 0.216385
	minimum = 0 (at node 140)
	maximum = 0.407667 (at node 11)
Accepted flit rate average= 0.209876
	minimum = 0.141 (at node 42)
	maximum = 0.283833 (at node 128)
Injected packet length average = 17.9645
Accepted packet length average = 17.952
Total in-flight flits = 46583 (44670 measured)
latency change    = 0.117962
throughput change = 0.00118043
Class 0:
Packet latency average = 1913.11
	minimum = 28
	maximum = 8532
Network latency average = 844.317
	minimum = 23
	maximum = 6758
Slowest packet = 8797
Flit latency average = 878.193
	minimum = 6
	maximum = 9413
Slowest flit = 18160
Fragmentation average = 252.044
	minimum = 0
	maximum = 5828
Injected packet rate average = 0.0121019
	minimum = 0 (at node 184)
	maximum = 0.022 (at node 13)
Accepted packet rate average = 0.0116696
	minimum = 0.00857143 (at node 42)
	maximum = 0.0151429 (at node 128)
Injected flit rate average = 0.217552
	minimum = 0 (at node 184)
	maximum = 0.398286 (at node 13)
Accepted flit rate average= 0.209473
	minimum = 0.152571 (at node 42)
	maximum = 0.270571 (at node 128)
Injected packet length average = 17.9766
Accepted packet length average = 17.9503
Total in-flight flits = 49920 (48665 measured)
latency change    = 0.0933242
throughput change = 0.00192222
Draining all recorded packets ...
Class 0:
Remaining flits: 14310 14311 14312 14313 14314 14315 14316 14317 14318 14319 [...] (47423 flits)
Measured flits: 162810 162811 162812 162813 162814 162815 162816 162817 162818 162819 [...] (40887 flits)
Class 0:
Remaining flits: 14310 14311 14312 14313 14314 14315 14316 14317 14318 14319 [...] (50635 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (38376 flits)
Class 0:
Remaining flits: 14310 14311 14312 14313 14314 14315 14316 14317 14318 14319 [...] (52047 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (34411 flits)
Class 0:
Remaining flits: 18161 21388 21389 21390 21391 21392 21393 21394 21395 21396 [...] (53059 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (27919 flits)
Class 0:
Remaining flits: 21388 21389 21390 21391 21392 21393 21394 21395 21396 21397 [...] (55227 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (25494 flits)
Class 0:
Remaining flits: 22234 22235 22236 22237 22238 22239 22240 22241 22242 22243 [...] (57032 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (22045 flits)
Class 0:
Remaining flits: 56556 56557 56558 56559 56560 56561 56562 56563 56564 56565 [...] (59119 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (18922 flits)
Class 0:
Remaining flits: 56556 56557 56558 56559 56560 56561 56562 56563 56564 56565 [...] (54555 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (15557 flits)
Class 0:
Remaining flits: 56558 56559 56560 56561 56562 56563 56564 56565 56566 56567 [...] (55458 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (12587 flits)
Class 0:
Remaining flits: 56559 56560 56561 56562 56563 56564 56565 56566 56567 56568 [...] (59423 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (10678 flits)
Class 0:
Remaining flits: 56560 56561 56562 56563 56564 56565 56566 56567 56568 56569 [...] (58654 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (9127 flits)
Class 0:
Remaining flits: 56573 62697 62698 62699 62700 62701 62702 62703 62704 62705 [...] (57177 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (6732 flits)
Class 0:
Remaining flits: 62708 62709 62710 62711 85608 85609 85610 85611 85612 85613 [...] (57861 flits)
Measured flits: 165384 165385 165386 165387 165388 165389 165390 165391 165392 165393 [...] (5466 flits)
Class 0:
Remaining flits: 85608 85609 85610 85611 85612 85613 85614 85615 85616 85617 [...] (56309 flits)
Measured flits: 166968 166969 166970 166971 166972 166973 166974 166975 166976 166977 [...] (4433 flits)
Class 0:
Remaining flits: 85608 85609 85610 85611 85612 85613 85614 85615 85616 85617 [...] (56489 flits)
Measured flits: 166968 166969 166970 166971 166972 166973 166974 166975 166976 166977 [...] (3201 flits)
Class 0:
Remaining flits: 85608 85609 85610 85611 85612 85613 85614 85615 85616 85617 [...] (56589 flits)
Measured flits: 166968 166969 166970 166971 166972 166973 166974 166975 166976 166977 [...] (2990 flits)
Class 0:
Remaining flits: 166968 166969 166970 166971 166972 166973 166974 166975 166976 166977 [...] (58478 flits)
Measured flits: 166968 166969 166970 166971 166972 166973 166974 166975 166976 166977 [...] (2591 flits)
Class 0:
Remaining flits: 250470 250471 250472 250473 250474 250475 250476 250477 250478 250479 [...] (60998 flits)
Measured flits: 250470 250471 250472 250473 250474 250475 250476 250477 250478 250479 [...] (1993 flits)
Class 0:
Remaining flits: 250487 366120 366121 366122 366123 366124 366125 366126 366127 366128 [...] (61047 flits)
Measured flits: 250487 366120 366121 366122 366123 366124 366125 366126 366127 366128 [...] (1477 flits)
Class 0:
Remaining flits: 250487 439434 439435 439436 439437 439438 439439 439440 439441 439442 [...] (61861 flits)
Measured flits: 250487 439434 439435 439436 439437 439438 439439 439440 439441 439442 [...] (1192 flits)
Class 0:
Remaining flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (60468 flits)
Measured flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (985 flits)
Class 0:
Remaining flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (59227 flits)
Measured flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (812 flits)
Class 0:
Remaining flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (59594 flits)
Measured flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (641 flits)
Class 0:
Remaining flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (61856 flits)
Measured flits: 451782 451783 451784 451785 451786 451787 451788 451789 451790 451791 [...] (538 flits)
Class 0:
Remaining flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (62823 flits)
Measured flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (369 flits)
Class 0:
Remaining flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (62786 flits)
Measured flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (318 flits)
Class 0:
Remaining flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (61516 flits)
Measured flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (306 flits)
Class 0:
Remaining flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (62177 flits)
Measured flits: 548316 548317 548318 548319 548320 548321 548322 548323 548324 548325 [...] (260 flits)
Class 0:
Remaining flits: 548329 548330 548331 548332 548333 579330 579331 579332 579333 579334 [...] (61038 flits)
Measured flits: 548329 548330 548331 548332 548333 620010 620011 620012 620013 620014 [...] (266 flits)
Class 0:
Remaining flits: 607212 607213 607214 607215 607216 607217 607218 607219 607220 607221 [...] (58922 flits)
Measured flits: 620010 620011 620012 620013 620014 620015 620016 620017 620018 620019 [...] (182 flits)
Class 0:
Remaining flits: 607212 607213 607214 607215 607216 607217 607218 607219 607220 607221 [...] (61285 flits)
Measured flits: 620010 620011 620012 620013 620014 620015 620016 620017 620018 620019 [...] (110 flits)
Class 0:
Remaining flits: 744570 744571 744572 744573 744574 744575 744576 744577 744578 744579 [...] (61896 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 829127 829128 829129 829130 829131 829132 829133 833292 833293 833294 [...] (61937 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 833292 833293 833294 833295 833296 833297 833298 833299 833300 833301 [...] (61395 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 833292 833293 833294 833295 833296 833297 833298 833299 833300 833301 [...] (61806 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 833292 833293 833294 833295 833296 833297 833298 833299 833300 833301 [...] (60518 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 833292 833293 833294 833295 833296 833297 833298 833299 833300 833301 [...] (61193 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Class 0:
Remaining flits: 833292 833293 833294 833295 833296 833297 833298 833299 833300 833301 [...] (62460 flits)
Measured flits: 1082394 1082395 1082396 1082397 1082398 1082399 1082400 1082401 1082402 1082403 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 833300 833301 833302 833303 833304 833305 833306 833307 833308 833309 [...] (37323 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 858285 858286 858287 858288 858289 858290 858291 858292 858293 1036440 [...] (24294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1036440 1036441 1036442 1036443 1036444 1036445 1036446 1036447 1036448 1036449 [...] (14000 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1060699 1060700 1060701 1060702 1060703 1085120 1085121 1085122 1085123 1085124 [...] (6301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1228083 1228084 1228085 1379808 1379809 1379810 1379811 1379812 1379813 1379814 [...] (1701 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379809 1379810 1379811 1379812 1379813 1379814 1379815 1379816 1379817 1379818 [...] (153 flits)
Measured flits: (0 flits)
Time taken is 54695 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4457.5 (1 samples)
	minimum = 28 (1 samples)
	maximum = 39331 (1 samples)
Network latency average = 1380.71 (1 samples)
	minimum = 23 (1 samples)
	maximum = 27603 (1 samples)
Flit latency average = 1537.36 (1 samples)
	minimum = 6 (1 samples)
	maximum = 30074 (1 samples)
Fragmentation average = 305.883 (1 samples)
	minimum = 0 (1 samples)
	maximum = 13559 (1 samples)
Injected packet rate average = 0.0121019 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0116696 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0151429 (1 samples)
Injected flit rate average = 0.217552 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.398286 (1 samples)
Accepted flit rate average = 0.209473 (1 samples)
	minimum = 0.152571 (1 samples)
	maximum = 0.270571 (1 samples)
Injected packet size average = 17.9766 (1 samples)
Accepted packet size average = 17.9503 (1 samples)
Hops average = 5.07458 (1 samples)
Total run time 75.967
