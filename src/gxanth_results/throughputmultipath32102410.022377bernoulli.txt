BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 277.255
	minimum = 23
	maximum = 968
Network latency average = 272.327
	minimum = 23
	maximum = 945
Slowest packet = 212
Flit latency average = 200.692
	minimum = 6
	maximum = 928
Slowest flit = 3833
Fragmentation average = 151.226
	minimum = 0
	maximum = 840
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.00986979
	minimum = 0.004 (at node 30)
	maximum = 0.018 (at node 91)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.203698
	minimum = 0.077 (at node 174)
	maximum = 0.338 (at node 91)
Injected packet length average = 17.8322
Accepted packet length average = 20.6385
Total in-flight flits = 38236 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 478.887
	minimum = 23
	maximum = 1860
Network latency average = 473.53
	minimum = 23
	maximum = 1860
Slowest packet = 482
Flit latency average = 389.453
	minimum = 6
	maximum = 1843
Slowest flit = 8693
Fragmentation average = 191.401
	minimum = 0
	maximum = 1518
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0110156
	minimum = 0.0055 (at node 62)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.212112
	minimum = 0.108 (at node 116)
	maximum = 0.3205 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 19.2556
Total in-flight flits = 72305 (0 measured)
latency change    = 0.421042
throughput change = 0.039668
Class 0:
Packet latency average = 1042.8
	minimum = 25
	maximum = 2762
Network latency average = 1036.71
	minimum = 23
	maximum = 2742
Slowest packet = 965
Flit latency average = 963.884
	minimum = 6
	maximum = 2725
Slowest flit = 17837
Fragmentation average = 224.741
	minimum = 0
	maximum = 2068
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0123542
	minimum = 0.004 (at node 77)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.22351
	minimum = 0.086 (at node 77)
	maximum = 0.374 (at node 156)
Injected packet length average = 18.0068
Accepted packet length average = 18.0919
Total in-flight flits = 106305 (0 measured)
latency change    = 0.540767
throughput change = 0.0509973
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 114.941
	minimum = 26
	maximum = 963
Network latency average = 109.864
	minimum = 23
	maximum = 957
Slowest packet = 12992
Flit latency average = 1345.49
	minimum = 6
	maximum = 3660
Slowest flit = 23093
Fragmentation average = 30.6133
	minimum = 0
	maximum = 251
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0126354
	minimum = 0.004 (at node 69)
	maximum = 0.022 (at node 14)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.226401
	minimum = 0.079 (at node 69)
	maximum = 0.406 (at node 33)
Injected packet length average = 17.9469
Accepted packet length average = 17.918
Total in-flight flits = 142180 (72027 measured)
latency change    = 8.07243
throughput change = 0.0127677
Class 0:
Packet latency average = 535.296
	minimum = 23
	maximum = 1995
Network latency average = 529.871
	minimum = 23
	maximum = 1980
Slowest packet = 12839
Flit latency average = 1530
	minimum = 6
	maximum = 4621
Slowest flit = 28912
Fragmentation average = 89.8847
	minimum = 0
	maximum = 704
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.0124635
	minimum = 0.006 (at node 36)
	maximum = 0.0195 (at node 137)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.223466
	minimum = 0.1045 (at node 36)
	maximum = 0.3505 (at node 128)
Injected packet length average = 17.9983
Accepted packet length average = 17.9296
Total in-flight flits = 175888 (132743 measured)
latency change    = 0.785275
throughput change = 0.0131335
Class 0:
Packet latency average = 1026.56
	minimum = 23
	maximum = 2929
Network latency average = 1021
	minimum = 23
	maximum = 2929
Slowest packet = 12910
Flit latency average = 1723.62
	minimum = 6
	maximum = 5317
Slowest flit = 52245
Fragmentation average = 125.316
	minimum = 0
	maximum = 1681
Injected packet rate average = 0.0225382
	minimum = 0.0146667 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0124115
	minimum = 0.00666667 (at node 36)
	maximum = 0.018 (at node 90)
Injected flit rate average = 0.405694
	minimum = 0.264 (at node 171)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.222182
	minimum = 0.119 (at node 36)
	maximum = 0.321667 (at node 78)
Injected packet length average = 18.0003
Accepted packet length average = 17.9014
Total in-flight flits = 212004 (185571 measured)
latency change    = 0.478555
throughput change = 0.00577838
Draining remaining packets ...
Class 0:
Remaining flits: 45306 45307 45308 45309 45310 45311 45312 45313 45314 45315 [...] (176315 flits)
Measured flits: 230688 230689 230690 230691 230692 230693 230694 230695 230696 230697 [...] (159947 flits)
Class 0:
Remaining flits: 45316 45317 45318 45319 45320 45321 45322 45323 47034 47035 [...] (140671 flits)
Measured flits: 230688 230689 230690 230691 230692 230693 230694 230695 230696 230697 [...] (130238 flits)
Class 0:
Remaining flits: 81667 81668 81669 81670 81671 81672 81673 81674 81675 81676 [...] (105914 flits)
Measured flits: 230706 230707 230708 230709 230710 230711 230712 230713 230714 230715 [...] (99477 flits)
Class 0:
Remaining flits: 87310 87311 87312 87313 87314 87315 87316 87317 95112 95113 [...] (71972 flits)
Measured flits: 230706 230707 230708 230709 230710 230711 230712 230713 230714 230715 [...] (68613 flits)
Class 0:
Remaining flits: 109735 109736 109737 109738 109739 109740 109741 109742 109743 109744 [...] (39576 flits)
Measured flits: 231030 231031 231032 231033 231034 231035 231036 231037 231038 231039 [...] (38189 flits)
Class 0:
Remaining flits: 159172 159173 185058 185059 185060 185061 185062 185063 185064 185065 [...] (11996 flits)
Measured flits: 231966 231967 231968 231969 231970 231971 231972 231973 231974 231975 [...] (11719 flits)
Class 0:
Remaining flits: 338015 338016 338017 338018 338019 338020 338021 341190 341191 341192 [...] (447 flits)
Measured flits: 338015 338016 338017 338018 338019 338020 338021 341190 341191 341192 [...] (447 flits)
Time taken is 13391 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3949.53 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9745 (1 samples)
Network latency average = 3943.79 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9743 (1 samples)
Flit latency average = 3572.92 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10361 (1 samples)
Fragmentation average = 177.394 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2231 (1 samples)
Injected packet rate average = 0.0225382 (1 samples)
	minimum = 0.0146667 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0124115 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.405694 (1 samples)
	minimum = 0.264 (1 samples)
	maximum = 0.541333 (1 samples)
Accepted flit rate average = 0.222182 (1 samples)
	minimum = 0.119 (1 samples)
	maximum = 0.321667 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.9014 (1 samples)
Hops average = 5.08088 (1 samples)
Total run time 12.6496
