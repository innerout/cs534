BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 308.419
	minimum = 22
	maximum = 850
Network latency average = 293.447
	minimum = 22
	maximum = 797
Slowest packet = 1047
Flit latency average = 259.648
	minimum = 5
	maximum = 824
Slowest flit = 20531
Fragmentation average = 73.4197
	minimum = 0
	maximum = 454
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0146198
	minimum = 0.005 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.281995
	minimum = 0.105 (at node 174)
	maximum = 0.487 (at node 44)
Injected packet length average = 17.8489
Accepted packet length average = 19.2886
Total in-flight flits = 81145 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 564.486
	minimum = 22
	maximum = 1678
Network latency average = 546.644
	minimum = 22
	maximum = 1651
Slowest packet = 2363
Flit latency average = 507.394
	minimum = 5
	maximum = 1701
Slowest flit = 37077
Fragmentation average = 101.037
	minimum = 0
	maximum = 454
Injected packet rate average = 0.0397656
	minimum = 0.0285 (at node 88)
	maximum = 0.0485 (at node 100)
Accepted packet rate average = 0.0155964
	minimum = 0.0085 (at node 153)
	maximum = 0.0225 (at node 63)
Injected flit rate average = 0.712951
	minimum = 0.5055 (at node 88)
	maximum = 0.8645 (at node 100)
Accepted flit rate average= 0.293461
	minimum = 0.172 (at node 116)
	maximum = 0.4115 (at node 63)
Injected packet length average = 17.9288
Accepted packet length average = 18.816
Total in-flight flits = 162171 (0 measured)
latency change    = 0.45363
throughput change = 0.0390721
Class 0:
Packet latency average = 1332.75
	minimum = 22
	maximum = 2477
Network latency average = 1311.3
	minimum = 22
	maximum = 2428
Slowest packet = 3860
Flit latency average = 1265.84
	minimum = 5
	maximum = 2466
Slowest flit = 68307
Fragmentation average = 148.314
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0396615
	minimum = 0.026 (at node 91)
	maximum = 0.054 (at node 36)
Accepted packet rate average = 0.016651
	minimum = 0.008 (at node 42)
	maximum = 0.03 (at node 137)
Injected flit rate average = 0.713333
	minimum = 0.465 (at node 91)
	maximum = 0.969 (at node 139)
Accepted flit rate average= 0.30074
	minimum = 0.163 (at node 17)
	maximum = 0.523 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.0613
Total in-flight flits = 241499 (0 measured)
latency change    = 0.576449
throughput change = 0.0242025
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 59.6306
	minimum = 23
	maximum = 269
Network latency average = 39.1385
	minimum = 22
	maximum = 269
Slowest packet = 27104
Flit latency average = 1754.5
	minimum = 5
	maximum = 3250
Slowest flit = 98314
Fragmentation average = 6.30096
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0406562
	minimum = 0.027 (at node 70)
	maximum = 0.056 (at node 66)
Accepted packet rate average = 0.0169427
	minimum = 0.008 (at node 4)
	maximum = 0.028 (at node 175)
Injected flit rate average = 0.732083
	minimum = 0.478 (at node 70)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.305016
	minimum = 0.134 (at node 36)
	maximum = 0.505 (at node 90)
Injected packet length average = 18.0067
Accepted packet length average = 18.0028
Total in-flight flits = 323444 (129062 measured)
latency change    = 21.3501
throughput change = 0.0140191
Class 0:
Packet latency average = 60.4205
	minimum = 22
	maximum = 275
Network latency average = 38.8435
	minimum = 22
	maximum = 269
Slowest packet = 27104
Flit latency average = 2025.4
	minimum = 5
	maximum = 3856
Slowest flit = 143279
Fragmentation average = 6.04645
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0401901
	minimum = 0.0295 (at node 92)
	maximum = 0.051 (at node 117)
Accepted packet rate average = 0.0167891
	minimum = 0.01 (at node 36)
	maximum = 0.0245 (at node 90)
Injected flit rate average = 0.723805
	minimum = 0.532 (at node 92)
	maximum = 0.923 (at node 117)
Accepted flit rate average= 0.30262
	minimum = 0.1875 (at node 36)
	maximum = 0.4355 (at node 90)
Injected packet length average = 18.0095
Accepted packet length average = 18.0248
Total in-flight flits = 403087 (255566 measured)
latency change    = 0.0130744
throughput change = 0.00791698
Class 0:
Packet latency average = 60.7436
	minimum = 22
	maximum = 305
Network latency average = 39.0623
	minimum = 22
	maximum = 290
Slowest packet = 42161
Flit latency average = 2284.2
	minimum = 5
	maximum = 4638
Slowest flit = 172529
Fragmentation average = 5.87315
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0401163
	minimum = 0.0286667 (at node 92)
	maximum = 0.0493333 (at node 117)
Accepted packet rate average = 0.0167587
	minimum = 0.0103333 (at node 64)
	maximum = 0.0223333 (at node 66)
Injected flit rate average = 0.722292
	minimum = 0.514667 (at node 92)
	maximum = 0.886667 (at node 117)
Accepted flit rate average= 0.301741
	minimum = 0.188667 (at node 64)
	maximum = 0.391667 (at node 66)
Injected packet length average = 18.0049
Accepted packet length average = 18.0051
Total in-flight flits = 483622 (382779 measured)
latency change    = 0.00531806
throughput change = 0.00291134
Draining remaining packets ...
Class 0:
Remaining flits: 214914 214915 214916 214917 214918 214919 223424 223425 223426 223427 [...] (436127 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (380945 flits)
Class 0:
Remaining flits: 270247 270248 270249 270250 270251 273534 273535 273536 273537 273538 [...] (389026 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (367846 flits)
Class 0:
Remaining flits: 320540 320541 320542 320543 322841 322842 322843 322844 322845 322846 [...] (341939 flits)
Measured flits: 412097 412098 412099 412100 412101 412102 412103 412104 412105 412106 [...] (337289 flits)
Class 0:
Remaining flits: 358664 358665 358666 358667 361134 361135 361136 361137 361138 361139 [...] (294447 flits)
Measured flits: 412551 412552 412553 412554 412555 412556 412557 412558 412559 412668 [...] (293916 flits)
Class 0:
Remaining flits: 384012 384013 384014 384015 384016 384017 384018 384019 384020 384021 [...] (247045 flits)
Measured flits: 425425 425426 425427 425428 425429 433616 433617 433618 433619 435531 [...] (247027 flits)
Class 0:
Remaining flits: 472752 472753 472754 472755 472756 472757 472758 472759 472760 472761 [...] (199850 flits)
Measured flits: 472752 472753 472754 472755 472756 472757 472758 472759 472760 472761 [...] (199850 flits)
Class 0:
Remaining flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (152841 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (152841 flits)
Class 0:
Remaining flits: 522901 522902 522903 522904 522905 522906 522907 522908 522909 522910 [...] (105460 flits)
Measured flits: 522901 522902 522903 522904 522905 522906 522907 522908 522909 522910 [...] (105460 flits)
Class 0:
Remaining flits: 588690 588691 588692 588693 588694 588695 588696 588697 588698 588699 [...] (58047 flits)
Measured flits: 588690 588691 588692 588693 588694 588695 588696 588697 588698 588699 [...] (58047 flits)
Class 0:
Remaining flits: 626964 626965 626966 626967 626968 626969 626970 626971 626972 626973 [...] (14351 flits)
Measured flits: 626964 626965 626966 626967 626968 626969 626970 626971 626972 626973 [...] (14351 flits)
Class 0:
Remaining flits: 746406 746407 746408 746409 746410 746411 746412 746413 746414 746415 [...] (110 flits)
Measured flits: 746406 746407 746408 746409 746410 746411 746412 746413 746414 746415 [...] (110 flits)
Time taken is 17121 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7146.39 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11979 (1 samples)
Network latency average = 7124.51 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11961 (1 samples)
Flit latency average = 5769.37 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11944 (1 samples)
Fragmentation average = 161.218 (1 samples)
	minimum = 0 (1 samples)
	maximum = 537 (1 samples)
Injected packet rate average = 0.0401163 (1 samples)
	minimum = 0.0286667 (1 samples)
	maximum = 0.0493333 (1 samples)
Accepted packet rate average = 0.0167587 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0223333 (1 samples)
Injected flit rate average = 0.722292 (1 samples)
	minimum = 0.514667 (1 samples)
	maximum = 0.886667 (1 samples)
Accepted flit rate average = 0.301741 (1 samples)
	minimum = 0.188667 (1 samples)
	maximum = 0.391667 (1 samples)
Injected packet size average = 18.0049 (1 samples)
Accepted packet size average = 18.0051 (1 samples)
Hops average = 5.06838 (1 samples)
Total run time 16.7228
