BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.053
	minimum = 23
	maximum = 985
Network latency average = 279.723
	minimum = 23
	maximum = 953
Slowest packet = 21
Flit latency average = 215.743
	minimum = 6
	maximum = 949
Slowest flit = 4657
Fragmentation average = 146.517
	minimum = 0
	maximum = 821
Injected packet rate average = 0.0288229
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.01025
	minimum = 0.002 (at node 41)
	maximum = 0.018 (at node 88)
Injected flit rate average = 0.514172
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.211495
	minimum = 0.073 (at node 41)
	maximum = 0.345 (at node 166)
Injected packet length average = 17.839
Accepted packet length average = 20.6336
Total in-flight flits = 59005 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 649.317
	minimum = 23
	maximum = 1969
Network latency average = 515.233
	minimum = 23
	maximum = 1851
Slowest packet = 21
Flit latency average = 437.528
	minimum = 6
	maximum = 1912
Slowest flit = 8015
Fragmentation average = 181.84
	minimum = 0
	maximum = 1741
Injected packet rate average = 0.030112
	minimum = 0.0015 (at node 34)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0114271
	minimum = 0.006 (at node 4)
	maximum = 0.0165 (at node 76)
Injected flit rate average = 0.539141
	minimum = 0.027 (at node 34)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.218352
	minimum = 0.117 (at node 4)
	maximum = 0.312 (at node 22)
Injected packet length average = 17.9045
Accepted packet length average = 19.1082
Total in-flight flits = 124287 (0 measured)
latency change    = 0.422388
throughput change = 0.0314024
Class 0:
Packet latency average = 1433.05
	minimum = 32
	maximum = 2981
Network latency average = 1217.04
	minimum = 25
	maximum = 2712
Slowest packet = 653
Flit latency average = 1156.45
	minimum = 6
	maximum = 2820
Slowest flit = 14506
Fragmentation average = 228.398
	minimum = 0
	maximum = 2286
Injected packet rate average = 0.0301562
	minimum = 0 (at node 21)
	maximum = 0.056 (at node 42)
Accepted packet rate average = 0.0123385
	minimum = 0.005 (at node 17)
	maximum = 0.024 (at node 47)
Injected flit rate average = 0.543021
	minimum = 0 (at node 191)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.224318
	minimum = 0.092 (at node 17)
	maximum = 0.379 (at node 47)
Injected packet length average = 18.0069
Accepted packet length average = 18.1802
Total in-flight flits = 185474 (0 measured)
latency change    = 0.5469
throughput change = 0.0265969
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 550.779
	minimum = 29
	maximum = 2872
Network latency average = 154.654
	minimum = 27
	maximum = 881
Slowest packet = 17389
Flit latency average = 1704.47
	minimum = 6
	maximum = 3713
Slowest flit = 18719
Fragmentation average = 45.8566
	minimum = 0
	maximum = 300
Injected packet rate average = 0.0252813
	minimum = 0 (at node 115)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0123281
	minimum = 0.004 (at node 53)
	maximum = 0.02 (at node 3)
Injected flit rate average = 0.455599
	minimum = 0 (at node 115)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.219833
	minimum = 0.093 (at node 15)
	maximum = 0.39 (at node 3)
Injected packet length average = 18.0212
Accepted packet length average = 17.8319
Total in-flight flits = 230656 (82179 measured)
latency change    = 1.60187
throughput change = 0.020399
Class 0:
Packet latency average = 864.819
	minimum = 27
	maximum = 3897
Network latency average = 438.63
	minimum = 27
	maximum = 1961
Slowest packet = 17389
Flit latency average = 1985.56
	minimum = 6
	maximum = 4715
Slowest flit = 25175
Fragmentation average = 75.5942
	minimum = 0
	maximum = 1220
Injected packet rate average = 0.0240807
	minimum = 0.004 (at node 111)
	maximum = 0.05 (at node 39)
Accepted packet rate average = 0.0120234
	minimum = 0.006 (at node 98)
	maximum = 0.0185 (at node 3)
Injected flit rate average = 0.433411
	minimum = 0.073 (at node 111)
	maximum = 0.902 (at node 39)
Accepted flit rate average= 0.214385
	minimum = 0.1095 (at node 98)
	maximum = 0.3295 (at node 3)
Injected packet length average = 17.9983
Accepted packet length average = 17.8306
Total in-flight flits = 269668 (153342 measured)
latency change    = 0.363127
throughput change = 0.0254118
Class 0:
Packet latency average = 1448.31
	minimum = 27
	maximum = 5507
Network latency average = 932.875
	minimum = 26
	maximum = 2973
Slowest packet = 17389
Flit latency average = 2263.77
	minimum = 6
	maximum = 5497
Slowest flit = 44919
Fragmentation average = 99.7287
	minimum = 0
	maximum = 1309
Injected packet rate average = 0.0227691
	minimum = 0.00733333 (at node 112)
	maximum = 0.0453333 (at node 78)
Accepted packet rate average = 0.0118264
	minimum = 0.00766667 (at node 98)
	maximum = 0.0163333 (at node 3)
Injected flit rate average = 0.409833
	minimum = 0.132333 (at node 112)
	maximum = 0.814 (at node 78)
Accepted flit rate average= 0.211234
	minimum = 0.142667 (at node 10)
	maximum = 0.292667 (at node 3)
Injected packet length average = 17.9995
Accepted packet length average = 17.8613
Total in-flight flits = 300143 (210868 measured)
latency change    = 0.402879
throughput change = 0.0149173
Draining remaining packets ...
Class 0:
Remaining flits: 25308 25309 25310 25311 25312 25313 25314 25315 25316 25317 [...] (263838 flits)
Measured flits: 312408 312409 312410 312411 312412 312413 312414 312415 312416 312417 [...] (195441 flits)
Class 0:
Remaining flits: 25560 25561 25562 25563 25564 25565 25566 25567 25568 25569 [...] (227270 flits)
Measured flits: 312408 312409 312410 312411 312412 312413 312414 312415 312416 312417 [...] (176375 flits)
Class 0:
Remaining flits: 25560 25561 25562 25563 25564 25565 25566 25567 25568 25569 [...] (191354 flits)
Measured flits: 312460 312461 312462 312463 312464 312465 312466 312467 312468 312469 [...] (153776 flits)
Class 0:
Remaining flits: 77552 77553 77554 77555 77556 77557 77558 77559 77560 77561 [...] (156804 flits)
Measured flits: 312462 312463 312464 312465 312466 312467 312468 312469 312470 312471 [...] (130516 flits)
Class 0:
Remaining flits: 79663 79664 79665 79666 79667 82782 82783 82784 82785 82786 [...] (122773 flits)
Measured flits: 312474 312475 312476 312477 312478 312479 312480 312481 312482 312483 [...] (105440 flits)
Class 0:
Remaining flits: 82784 82785 82786 82787 82788 82789 82790 82791 82792 82793 [...] (88854 flits)
Measured flits: 312606 312607 312608 312609 312610 312611 312612 312613 312614 312615 [...] (77806 flits)
Class 0:
Remaining flits: 98190 98191 98192 98193 98194 98195 98196 98197 98198 98199 [...] (56606 flits)
Measured flits: 312606 312607 312608 312609 312610 312611 312612 312613 312614 312615 [...] (50080 flits)
Class 0:
Remaining flits: 98190 98191 98192 98193 98194 98195 98196 98197 98198 98199 [...] (27669 flits)
Measured flits: 312642 312643 312644 312645 312646 312647 312648 312649 312650 312651 [...] (24560 flits)
Class 0:
Remaining flits: 145080 145081 145082 145083 145084 145085 145086 145087 145088 145089 [...] (6596 flits)
Measured flits: 313164 313165 313166 313167 313168 313169 313170 313171 313172 313173 [...] (5870 flits)
Class 0:
Remaining flits: 260814 260815 260816 260817 260818 260819 262286 262287 262288 262289 [...] (645 flits)
Measured flits: 323674 323675 326340 326341 326342 326343 326344 326345 326346 326347 [...] (611 flits)
Time taken is 16437 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6570.37 (1 samples)
	minimum = 27 (1 samples)
	maximum = 15609 (1 samples)
Network latency average = 5890.06 (1 samples)
	minimum = 26 (1 samples)
	maximum = 13096 (1 samples)
Flit latency average = 5258.65 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14374 (1 samples)
Fragmentation average = 172.116 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2970 (1 samples)
Injected packet rate average = 0.0227691 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0453333 (1 samples)
Accepted packet rate average = 0.0118264 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.409833 (1 samples)
	minimum = 0.132333 (1 samples)
	maximum = 0.814 (1 samples)
Accepted flit rate average = 0.211234 (1 samples)
	minimum = 0.142667 (1 samples)
	maximum = 0.292667 (1 samples)
Injected packet size average = 17.9995 (1 samples)
Accepted packet size average = 17.8613 (1 samples)
Hops average = 5.14196 (1 samples)
Total run time 17.0777
