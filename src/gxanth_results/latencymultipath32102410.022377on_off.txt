BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.13
	minimum = 25
	maximum = 981
Network latency average = 252.532
	minimum = 25
	maximum = 947
Slowest packet = 140
Flit latency average = 187.92
	minimum = 6
	maximum = 979
Slowest flit = 1258
Fragmentation average = 135.299
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00939583
	minimum = 0.004 (at node 108)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.193828
	minimum = 0.087 (at node 108)
	maximum = 0.339 (at node 44)
Injected packet length average = 17.8417
Accepted packet length average = 20.6292
Total in-flight flits = 35559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.382
	minimum = 23
	maximum = 1920
Network latency average = 435.747
	minimum = 23
	maximum = 1838
Slowest packet = 438
Flit latency average = 356.202
	minimum = 6
	maximum = 1874
Slowest flit = 6706
Fragmentation average = 175.996
	minimum = 0
	maximum = 1671
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0107786
	minimum = 0.006 (at node 83)
	maximum = 0.017 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.208141
	minimum = 0.1175 (at node 96)
	maximum = 0.324 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 19.3105
Total in-flight flits = 70482 (0 measured)
latency change    = 0.394878
throughput change = 0.0687636
Class 0:
Packet latency average = 1084.14
	minimum = 27
	maximum = 2898
Network latency average = 946.909
	minimum = 23
	maximum = 2825
Slowest packet = 372
Flit latency average = 869.103
	minimum = 6
	maximum = 2864
Slowest flit = 8997
Fragmentation average = 225.292
	minimum = 0
	maximum = 1908
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0125208
	minimum = 0.004 (at node 190)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.22451
	minimum = 0.071 (at node 190)
	maximum = 0.403 (at node 9)
Injected packet length average = 18
Accepted packet length average = 17.9309
Total in-flight flits = 105280 (0 measured)
latency change    = 0.51355
throughput change = 0.0729133
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 303.616
	minimum = 23
	maximum = 1135
Network latency average = 187.798
	minimum = 23
	maximum = 940
Slowest packet = 12702
Flit latency average = 1258.12
	minimum = 6
	maximum = 3634
Slowest flit = 21149
Fragmentation average = 66.7566
	minimum = 0
	maximum = 522
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0124167
	minimum = 0.004 (at node 180)
	maximum = 0.025 (at node 128)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.222484
	minimum = 0.096 (at node 180)
	maximum = 0.483 (at node 128)
Injected packet length average = 17.9946
Accepted packet length average = 17.9182
Total in-flight flits = 138829 (67149 measured)
latency change    = 2.57077
throughput change = 0.00910644
Class 0:
Packet latency average = 607.776
	minimum = 23
	maximum = 2299
Network latency average = 498.142
	minimum = 23
	maximum = 1960
Slowest packet = 12702
Flit latency average = 1450.13
	minimum = 6
	maximum = 4657
Slowest flit = 19603
Fragmentation average = 111.149
	minimum = 0
	maximum = 1034
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0123854
	minimum = 0.006 (at node 91)
	maximum = 0.0195 (at node 175)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.223297
	minimum = 0.112 (at node 91)
	maximum = 0.3395 (at node 175)
Injected packet length average = 17.9936
Accepted packet length average = 18.029
Total in-flight flits = 172444 (125718 measured)
latency change    = 0.500447
throughput change = 0.00363865
Class 0:
Packet latency average = 1001.07
	minimum = 23
	maximum = 3200
Network latency average = 889.198
	minimum = 23
	maximum = 2930
Slowest packet = 12702
Flit latency average = 1631.68
	minimum = 6
	maximum = 5141
Slowest flit = 43798
Fragmentation average = 140.521
	minimum = 0
	maximum = 1481
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0123299
	minimum = 0.00766667 (at node 146)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.221319
	minimum = 0.136333 (at node 31)
	maximum = 0.337667 (at node 128)
Injected packet length average = 18.0055
Accepted packet length average = 17.9499
Total in-flight flits = 204852 (173855 measured)
latency change    = 0.392873
throughput change = 0.00893473
Class 0:
Packet latency average = 1347.97
	minimum = 23
	maximum = 4165
Network latency average = 1231.04
	minimum = 23
	maximum = 3935
Slowest packet = 12702
Flit latency average = 1813.37
	minimum = 6
	maximum = 6412
Slowest flit = 39748
Fragmentation average = 155.522
	minimum = 0
	maximum = 1893
Injected packet rate average = 0.0222187
	minimum = 0.00625 (at node 90)
	maximum = 0.04875 (at node 9)
Accepted packet rate average = 0.0122799
	minimum = 0.008 (at node 79)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.39987
	minimum = 0.1125 (at node 90)
	maximum = 0.878 (at node 9)
Accepted flit rate average= 0.219947
	minimum = 0.14575 (at node 31)
	maximum = 0.317 (at node 128)
Injected packet length average = 17.997
Accepted packet length average = 17.911
Total in-flight flits = 243513 (223078 measured)
latency change    = 0.257348
throughput change = 0.00624165
Class 0:
Packet latency average = 1662.11
	minimum = 23
	maximum = 5205
Network latency average = 1542.75
	minimum = 23
	maximum = 4937
Slowest packet = 12702
Flit latency average = 1995.57
	minimum = 6
	maximum = 7128
Slowest flit = 61943
Fragmentation average = 163.074
	minimum = 0
	maximum = 2267
Injected packet rate average = 0.0221542
	minimum = 0.008 (at node 136)
	maximum = 0.044 (at node 117)
Accepted packet rate average = 0.0122188
	minimum = 0.008 (at node 52)
	maximum = 0.0158 (at node 81)
Injected flit rate average = 0.398649
	minimum = 0.144 (at node 136)
	maximum = 0.7894 (at node 117)
Accepted flit rate average= 0.219668
	minimum = 0.146 (at node 52)
	maximum = 0.2934 (at node 138)
Injected packet length average = 17.9943
Accepted packet length average = 17.9779
Total in-flight flits = 277223 (263563 measured)
latency change    = 0.189004
throughput change = 0.00126967
Class 0:
Packet latency average = 2003.25
	minimum = 23
	maximum = 6255
Network latency average = 1880.49
	minimum = 23
	maximum = 5918
Slowest packet = 12702
Flit latency average = 2199.29
	minimum = 6
	maximum = 8481
Slowest flit = 21257
Fragmentation average = 169.498
	minimum = 0
	maximum = 2267
Injected packet rate average = 0.0222352
	minimum = 0.00816667 (at node 136)
	maximum = 0.0435 (at node 117)
Accepted packet rate average = 0.0122075
	minimum = 0.00866667 (at node 49)
	maximum = 0.0161667 (at node 128)
Injected flit rate average = 0.400199
	minimum = 0.147 (at node 136)
	maximum = 0.7815 (at node 117)
Accepted flit rate average= 0.219099
	minimum = 0.1565 (at node 49)
	maximum = 0.295333 (at node 128)
Injected packet length average = 17.9984
Accepted packet length average = 17.9479
Total in-flight flits = 313948 (304979 measured)
latency change    = 0.170291
throughput change = 0.00259586
Class 0:
Packet latency average = 2291.52
	minimum = 23
	maximum = 7277
Network latency average = 2167.07
	minimum = 23
	maximum = 6983
Slowest packet = 12702
Flit latency average = 2385.19
	minimum = 6
	maximum = 9740
Slowest flit = 16915
Fragmentation average = 172.829
	minimum = 0
	maximum = 2267
Injected packet rate average = 0.0224353
	minimum = 0.00985714 (at node 90)
	maximum = 0.04 (at node 117)
Accepted packet rate average = 0.0121882
	minimum = 0.00885714 (at node 49)
	maximum = 0.0152857 (at node 70)
Injected flit rate average = 0.403841
	minimum = 0.176143 (at node 90)
	maximum = 0.717571 (at node 117)
Accepted flit rate average= 0.218711
	minimum = 0.155 (at node 49)
	maximum = 0.274143 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 17.9444
Total in-flight flits = 354086 (348129 measured)
latency change    = 0.125801
throughput change = 0.00177242
Draining all recorded packets ...
Class 0:
Remaining flits: 36715 36716 36717 36718 36719 62010 62011 62012 62013 62014 [...] (393067 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (326626 flits)
Class 0:
Remaining flits: 62010 62011 62012 62013 62014 62015 62016 62017 62018 62019 [...] (430682 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (293898 flits)
Class 0:
Remaining flits: 98622 98623 98624 98625 98626 98627 98628 98629 98630 98631 [...] (460851 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (261920 flits)
Class 0:
Remaining flits: 98622 98623 98624 98625 98626 98627 98628 98629 98630 98631 [...] (497343 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (231638 flits)
Class 0:
Remaining flits: 98622 98623 98624 98625 98626 98627 98628 98629 98630 98631 [...] (534752 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (203657 flits)
Class 0:
Remaining flits: 98630 98631 98632 98633 98634 98635 98636 98637 98638 98639 [...] (568017 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (178073 flits)
Class 0:
Remaining flits: 107452 107453 107454 107455 107456 107457 107458 107459 119736 119737 [...] (597845 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (154427 flits)
Class 0:
Remaining flits: 107452 107453 107454 107455 107456 107457 107458 107459 119751 119752 [...] (634761 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (132977 flits)
Class 0:
Remaining flits: 143568 143569 143570 143571 143572 143573 143574 143575 143576 143577 [...] (669022 flits)
Measured flits: 228834 228835 228836 228837 228838 228839 228840 228841 228842 228843 [...] (114171 flits)
Class 0:
Remaining flits: 143568 143569 143570 143571 143572 143573 143574 143575 143576 143577 [...] (702149 flits)
Measured flits: 233460 233461 233462 233463 233464 233465 233466 233467 233468 233469 [...] (97164 flits)
Class 0:
Remaining flits: 143568 143569 143570 143571 143572 143573 143574 143575 143576 143577 [...] (734517 flits)
Measured flits: 233460 233461 233462 233463 233464 233465 233466 233467 233468 233469 [...] (82633 flits)
Class 0:
Remaining flits: 143568 143569 143570 143571 143572 143573 143574 143575 143576 143577 [...] (765235 flits)
Measured flits: 233460 233461 233462 233463 233464 233465 233466 233467 233468 233469 [...] (69625 flits)
Class 0:
Remaining flits: 143568 143569 143570 143571 143572 143573 143574 143575 143576 143577 [...] (803503 flits)
Measured flits: 233461 233462 233463 233464 233465 233466 233467 233468 233469 233470 [...] (58205 flits)
Class 0:
Remaining flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (843182 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (48893 flits)
Class 0:
Remaining flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (882666 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (40605 flits)
Class 0:
Remaining flits: 272718 272719 272720 272721 272722 272723 272724 272725 272726 272727 [...] (921754 flits)
Measured flits: 272718 272719 272720 272721 272722 272723 272724 272725 272726 272727 [...] (33703 flits)
Class 0:
Remaining flits: 273422 273423 273424 273425 273426 273427 273428 273429 273430 273431 [...] (954238 flits)
Measured flits: 273422 273423 273424 273425 273426 273427 273428 273429 273430 273431 [...] (28104 flits)
Class 0:
Remaining flits: 349434 349435 349436 349437 349438 349439 349440 349441 349442 349443 [...] (991589 flits)
Measured flits: 349434 349435 349436 349437 349438 349439 349440 349441 349442 349443 [...] (23075 flits)
Class 0:
Remaining flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (1027807 flits)
Measured flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (19051 flits)
Class 0:
Remaining flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (1062937 flits)
Measured flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (15883 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1100843 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (13013 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1134353 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (10920 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1169552 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (9060 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1207147 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (7530 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1242129 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (6239 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1276352 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (5451 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1308721 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (4431 flits)
Class 0:
Remaining flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (1341793 flits)
Measured flits: 361080 361081 361082 361083 361084 361085 361086 361087 361088 361089 [...] (3563 flits)
Class 0:
Remaining flits: 361097 401760 401761 401762 401763 401764 401765 401766 401767 401768 [...] (1375446 flits)
Measured flits: 361097 401760 401761 401762 401763 401764 401765 401766 401767 401768 [...] (2943 flits)
Class 0:
Remaining flits: 401772 401773 401774 401775 401776 401777 406908 406909 406910 406911 [...] (1410254 flits)
Measured flits: 401772 401773 401774 401775 401776 401777 406908 406909 406910 406911 [...] (2500 flits)
Class 0:
Remaining flits: 434970 434971 434972 434973 434974 434975 434976 434977 434978 434979 [...] (1446743 flits)
Measured flits: 434970 434971 434972 434973 434974 434975 434976 434977 434978 434979 [...] (1936 flits)
Class 0:
Remaining flits: 434970 434971 434972 434973 434974 434975 434976 434977 434978 434979 [...] (1486934 flits)
Measured flits: 434970 434971 434972 434973 434974 434975 434976 434977 434978 434979 [...] (1505 flits)
Class 0:
Remaining flits: 434981 434982 434983 434984 434985 434986 434987 466182 466183 466184 [...] (1521409 flits)
Measured flits: 434981 434982 434983 434984 434985 434986 434987 466182 466183 466184 [...] (1252 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1560332 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1010 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1593708 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (858 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1621629 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (744 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1654136 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (610 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1684971 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (467 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1714435 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (393 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1744527 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (343 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1776813 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (306 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1804459 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (274 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1834232 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (239 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1867633 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (198 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1892036 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (180 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1925680 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (180 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1961459 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (162 flits)
Class 0:
Remaining flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (1985866 flits)
Measured flits: 466182 466183 466184 466185 466186 466187 466188 466189 466190 466191 [...] (93 flits)
Class 0:
Remaining flits: 466183 466184 466185 466186 466187 466188 466189 466190 466191 466192 [...] (2008239 flits)
Measured flits: 466183 466184 466185 466186 466187 466188 466189 466190 466191 466192 [...] (71 flits)
Class 0:
Remaining flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (2033527 flits)
Measured flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (36 flits)
Class 0:
Remaining flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (2061323 flits)
Measured flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (36 flits)
Class 0:
Remaining flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (2084666 flits)
Measured flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (36 flits)
Class 0:
Remaining flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (2105661 flits)
Measured flits: 558216 558217 558218 558219 558220 558221 558222 558223 558224 558225 [...] (35 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (2082941 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (2047528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (2013338 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1978607 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1943830 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1909008 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1873817 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1838460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825876 825877 825878 825879 825880 825881 825882 825883 825884 825885 [...] (1803313 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1061550 1061551 1061552 1061553 1061554 1061555 1061556 1061557 1061558 1061559 [...] (1768637 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1061550 1061551 1061552 1061553 1061554 1061555 1061556 1061557 1061558 1061559 [...] (1733419 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1112926 1112927 1112928 1112929 1112930 1112931 1112932 1112933 1112934 1112935 [...] (1698233 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1124730 1124731 1124732 1124733 1124734 1124735 1124736 1124737 1124738 1124739 [...] (1663733 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1218312 1218313 1218314 1218315 1218316 1218317 1218318 1218319 1218320 1218321 [...] (1629170 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254906 1254907 1254908 1254909 1254910 1254911 1254912 1254913 1254914 1254915 [...] (1595207 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254906 1254907 1254908 1254909 1254910 1254911 1254912 1254913 1254914 1254915 [...] (1560513 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254906 1254907 1254908 1254909 1254910 1254911 1254912 1254913 1254914 1254915 [...] (1526341 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254906 1254907 1254908 1254909 1254910 1254911 1254912 1254913 1254914 1254915 [...] (1492364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254906 1254907 1254908 1254909 1254910 1254911 1254912 1254913 1254914 1254915 [...] (1458369 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1354374 1354375 1354376 1354377 1354378 1354379 1354380 1354381 1354382 1354383 [...] (1424635 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1354374 1354375 1354376 1354377 1354378 1354379 1354380 1354381 1354382 1354383 [...] (1390006 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1367083 1367084 1367085 1367086 1367087 1367088 1367089 1367090 1367091 1367092 [...] (1355416 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426950 1426951 1426952 1426953 1426954 1426955 1426956 1426957 1426958 1426959 [...] (1321550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426950 1426951 1426952 1426953 1426954 1426955 1426956 1426957 1426958 1426959 [...] (1287216 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426950 1426951 1426952 1426953 1426954 1426955 1426956 1426957 1426958 1426959 [...] (1252681 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426950 1426951 1426952 1426953 1426954 1426955 1426956 1426957 1426958 1426959 [...] (1218990 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1663632 1663633 1663634 1663635 1663636 1663637 1663638 1663639 1663640 1663641 [...] (1184723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1705662 1705663 1705664 1705665 1705666 1705667 1705668 1705669 1705670 1705671 [...] (1150343 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1705662 1705663 1705664 1705665 1705666 1705667 1705668 1705669 1705670 1705671 [...] (1116605 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1791540 1791541 1791542 1791543 1791544 1791545 1791546 1791547 1791548 1791549 [...] (1083123 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1806300 1806301 1806302 1806303 1806304 1806305 1806306 1806307 1806308 1806309 [...] (1049616 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1806300 1806301 1806302 1806303 1806304 1806305 1806306 1806307 1806308 1806309 [...] (1017283 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1806307 1806308 1806309 1806310 1806311 1806312 1806313 1806314 1806315 1806316 [...] (984089 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (949589 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (915484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (881793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (848110 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (814042 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845738 1845739 1845740 1845741 1845742 1845743 1845744 1845745 1845746 1845747 [...] (780184 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2011086 2011087 2011088 2011089 2011090 2011091 2011092 2011093 2011094 2011095 [...] (745781 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (711967 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (677695 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (643358 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (609447 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (575177 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (540897 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (507092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (473372 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (439269 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (405593 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (372075 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (339675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (307632 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (275345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (244105 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (213732 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (183718 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (154915 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (127622 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (101332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147994 2147995 2147996 2147997 2147998 2147999 2148000 2148001 2148002 2148003 [...] (76410 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2436894 2436895 2436896 2436897 2436898 2436899 2436900 2436901 2436902 2436903 [...] (55746 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2749500 2749501 2749502 2749503 2749504 2749505 2749506 2749507 2749508 2749509 [...] (38664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2766348 2766349 2766350 2766351 2766352 2766353 2766354 2766355 2766356 2766357 [...] (25321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2866230 2866231 2866232 2866233 2866234 2866235 2866236 2866237 2866238 2866239 [...] (15243 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2894191 2894192 2894193 2894194 2894195 2894196 2894197 2894198 2894199 2894200 [...] (7403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3113093 3113094 3113095 3113096 3113097 3113098 3113099 3125232 3125233 3125234 [...] (1803 flits)
Measured flits: (0 flits)
Time taken is 131367 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7476.34 (1 samples)
	minimum = 23 (1 samples)
	maximum = 56334 (1 samples)
Network latency average = 7325.34 (1 samples)
	minimum = 23 (1 samples)
	maximum = 56317 (1 samples)
Flit latency average = 30353.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 96811 (1 samples)
Fragmentation average = 208.795 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4736 (1 samples)
Injected packet rate average = 0.0224353 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0121882 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0152857 (1 samples)
Injected flit rate average = 0.403841 (1 samples)
	minimum = 0.176143 (1 samples)
	maximum = 0.717571 (1 samples)
Accepted flit rate average = 0.218711 (1 samples)
	minimum = 0.155 (1 samples)
	maximum = 0.274143 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.9444 (1 samples)
Hops average = 5.07806 (1 samples)
Total run time 157.414
