BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.111
	minimum = 22
	maximum = 834
Network latency average = 297.183
	minimum = 22
	maximum = 779
Slowest packet = 389
Flit latency average = 274.923
	minimum = 5
	maximum = 793
Slowest flit = 29926
Fragmentation average = 24.1841
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0162656
	minimum = 0.009 (at node 6)
	maximum = 0.026 (at node 48)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.299781
	minimum = 0.165 (at node 6)
	maximum = 0.468 (at node 48)
Injected packet length average = 17.8419
Accepted packet length average = 18.4304
Total in-flight flits = 115710 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 612.98
	minimum = 22
	maximum = 1694
Network latency average = 565.312
	minimum = 22
	maximum = 1564
Slowest packet = 389
Flit latency average = 543.503
	minimum = 5
	maximum = 1572
Slowest flit = 69714
Fragmentation average = 24.6344
	minimum = 0
	maximum = 120
Injected packet rate average = 0.0517865
	minimum = 0.0425 (at node 37)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0170156
	minimum = 0.011 (at node 96)
	maximum = 0.0265 (at node 152)
Injected flit rate average = 0.928016
	minimum = 0.76 (at node 37)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.309844
	minimum = 0.205 (at node 96)
	maximum = 0.477 (at node 152)
Injected packet length average = 17.92
Accepted packet length average = 18.2094
Total in-flight flits = 238968 (0 measured)
latency change    = 0.466359
throughput change = 0.032476
Class 0:
Packet latency average = 1449.8
	minimum = 22
	maximum = 2493
Network latency average = 1363.4
	minimum = 22
	maximum = 2296
Slowest packet = 4100
Flit latency average = 1348.95
	minimum = 5
	maximum = 2290
Slowest flit = 119004
Fragmentation average = 25.0987
	minimum = 0
	maximum = 120
Injected packet rate average = 0.0521615
	minimum = 0.04 (at node 164)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0175729
	minimum = 0.007 (at node 154)
	maximum = 0.034 (at node 16)
Injected flit rate average = 0.938974
	minimum = 0.71 (at node 164)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.316833
	minimum = 0.135 (at node 154)
	maximum = 0.607 (at node 16)
Injected packet length average = 18.0013
Accepted packet length average = 18.0296
Total in-flight flits = 358406 (0 measured)
latency change    = 0.577195
throughput change = 0.0220608
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 382.021
	minimum = 23
	maximum = 1249
Network latency average = 158.172
	minimum = 22
	maximum = 810
Slowest packet = 29955
Flit latency average = 2048
	minimum = 5
	maximum = 3108
Slowest flit = 153469
Fragmentation average = 5.65794
	minimum = 0
	maximum = 33
Injected packet rate average = 0.048474
	minimum = 0.026 (at node 148)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0167813
	minimum = 0.009 (at node 5)
	maximum = 0.028 (at node 3)
Injected flit rate average = 0.873318
	minimum = 0.478 (at node 148)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.30037
	minimum = 0.159 (at node 5)
	maximum = 0.495 (at node 176)
Injected packet length average = 18.0162
Accepted packet length average = 17.8991
Total in-flight flits = 468261 (157799 measured)
latency change    = 2.79507
throughput change = 0.0548109
Class 0:
Packet latency average = 567.095
	minimum = 22
	maximum = 1885
Network latency average = 280.527
	minimum = 22
	maximum = 1344
Slowest packet = 29955
Flit latency average = 2378.11
	minimum = 5
	maximum = 3845
Slowest flit = 190061
Fragmentation average = 5.78875
	minimum = 0
	maximum = 39
Injected packet rate average = 0.0485
	minimum = 0.0275 (at node 148)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0165599
	minimum = 0.0105 (at node 86)
	maximum = 0.023 (at node 118)
Injected flit rate average = 0.87293
	minimum = 0.5015 (at node 148)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.297687
	minimum = 0.191 (at node 86)
	maximum = 0.417 (at node 118)
Injected packet length average = 17.9986
Accepted packet length average = 17.9764
Total in-flight flits = 579344 (315584 measured)
latency change    = 0.326355
throughput change = 0.00901043
Class 0:
Packet latency average = 758.387
	minimum = 22
	maximum = 2392
Network latency average = 412.72
	minimum = 22
	maximum = 1734
Slowest packet = 29955
Flit latency average = 2702.84
	minimum = 5
	maximum = 4574
Slowest flit = 249457
Fragmentation average = 5.72351
	minimum = 0
	maximum = 39
Injected packet rate average = 0.0484618
	minimum = 0.0276667 (at node 72)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0165347
	minimum = 0.0116667 (at node 146)
	maximum = 0.0243333 (at node 128)
Injected flit rate average = 0.872259
	minimum = 0.498667 (at node 72)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.297365
	minimum = 0.207667 (at node 146)
	maximum = 0.435 (at node 128)
Injected packet length average = 17.9989
Accepted packet length average = 17.9843
Total in-flight flits = 689594 (472833 measured)
latency change    = 0.252235
throughput change = 0.00108593
Class 0:
Packet latency average = 917.339
	minimum = 22
	maximum = 2476
Network latency average = 513.333
	minimum = 22
	maximum = 2175
Slowest packet = 29955
Flit latency average = 3032.34
	minimum = 5
	maximum = 5287
Slowest flit = 291203
Fragmentation average = 5.63075
	minimum = 0
	maximum = 39
Injected packet rate average = 0.0484271
	minimum = 0.02825 (at node 72)
	maximum = 0.05575 (at node 7)
Accepted packet rate average = 0.0165078
	minimum = 0.01175 (at node 146)
	maximum = 0.0225 (at node 128)
Injected flit rate average = 0.871702
	minimum = 0.50875 (at node 72)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.296836
	minimum = 0.21125 (at node 146)
	maximum = 0.40425 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 17.9815
Total in-flight flits = 799946 (630645 measured)
latency change    = 0.173275
throughput change = 0.00178094
Draining remaining packets ...
Class 0:
Remaining flits: 344772 344773 344774 344775 344776 344777 344778 344779 344780 344781 [...] (746824 flits)
Measured flits: 538218 538219 538220 538221 538222 538223 538224 538225 538226 538227 [...] (625005 flits)
Class 0:
Remaining flits: 388206 388207 388208 388209 388210 388211 388212 388213 388214 388215 [...] (694883 flits)
Measured flits: 538218 538219 538220 538221 538222 538223 538224 538225 538226 538227 [...] (620299 flits)
Class 0:
Remaining flits: 440856 440857 440858 440859 440860 440861 440862 440863 440864 440865 [...] (643339 flits)
Measured flits: 538236 538237 538238 538239 538240 538241 538242 538243 538244 538245 [...] (614707 flits)
Class 0:
Remaining flits: 478338 478339 478340 478341 478342 478343 478344 478345 478346 478347 [...] (593686 flits)
Measured flits: 538470 538471 538472 538473 538474 538475 538476 538477 538478 538479 [...] (589879 flits)
Class 0:
Remaining flits: 533574 533575 533576 533577 533578 533579 533580 533581 533582 533583 [...] (545122 flits)
Measured flits: 539136 539137 539138 539139 539140 539141 539142 539143 539144 539145 [...] (545050 flits)
Class 0:
Remaining flits: 571320 571321 571322 571323 571324 571325 571326 571327 571328 571329 [...] (498005 flits)
Measured flits: 571320 571321 571322 571323 571324 571325 571326 571327 571328 571329 [...] (498005 flits)
Class 0:
Remaining flits: 604207 604208 604209 604210 604211 604212 604213 604214 604215 604216 [...] (450517 flits)
Measured flits: 604207 604208 604209 604210 604211 604212 604213 604214 604215 604216 [...] (450517 flits)
Class 0:
Remaining flits: 635184 635185 635186 635187 635188 635189 635190 635191 635192 635193 [...] (403373 flits)
Measured flits: 635184 635185 635186 635187 635188 635189 635190 635191 635192 635193 [...] (403373 flits)
Class 0:
Remaining flits: 677538 677539 677540 677541 677542 677543 677544 677545 677546 677547 [...] (356251 flits)
Measured flits: 677538 677539 677540 677541 677542 677543 677544 677545 677546 677547 [...] (356251 flits)
Class 0:
Remaining flits: 719550 719551 719552 719553 719554 719555 719556 719557 719558 719559 [...] (308749 flits)
Measured flits: 719550 719551 719552 719553 719554 719555 719556 719557 719558 719559 [...] (308749 flits)
Class 0:
Remaining flits: 750168 750169 750170 750171 750172 750173 750174 750175 750176 750177 [...] (261164 flits)
Measured flits: 750168 750169 750170 750171 750172 750173 750174 750175 750176 750177 [...] (261164 flits)
Class 0:
Remaining flits: 797814 797815 797816 797817 797818 797819 797820 797821 797822 797823 [...] (213691 flits)
Measured flits: 797814 797815 797816 797817 797818 797819 797820 797821 797822 797823 [...] (213691 flits)
Class 0:
Remaining flits: 839088 839089 839090 839091 839092 839093 839094 839095 839096 839097 [...] (166641 flits)
Measured flits: 839088 839089 839090 839091 839092 839093 839094 839095 839096 839097 [...] (166641 flits)
Class 0:
Remaining flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (120006 flits)
Measured flits: 866538 866539 866540 866541 866542 866543 866544 866545 866546 866547 [...] (120006 flits)
Class 0:
Remaining flits: 930366 930367 930368 930369 930370 930371 930372 930373 930374 930375 [...] (73691 flits)
Measured flits: 930366 930367 930368 930369 930370 930371 930372 930373 930374 930375 [...] (73691 flits)
Class 0:
Remaining flits: 962712 962713 962714 962715 962716 962717 962718 962719 962720 962721 [...] (26579 flits)
Measured flits: 962712 962713 962714 962715 962716 962717 962718 962719 962720 962721 [...] (26579 flits)
Class 0:
Remaining flits: 1065942 1065943 1065944 1065945 1065946 1065947 1065948 1065949 1065950 1065951 [...] (2713 flits)
Measured flits: 1065942 1065943 1065944 1065945 1065946 1065947 1065948 1065949 1065950 1065951 [...] (2713 flits)
Time taken is 24875 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11515.2 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18771 (1 samples)
Network latency average = 11154.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18261 (1 samples)
Flit latency average = 8942.93 (1 samples)
	minimum = 5 (1 samples)
	maximum = 18244 (1 samples)
Fragmentation average = 26.324 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1355 (1 samples)
Injected packet rate average = 0.0484271 (1 samples)
	minimum = 0.02825 (1 samples)
	maximum = 0.05575 (1 samples)
Accepted packet rate average = 0.0165078 (1 samples)
	minimum = 0.01175 (1 samples)
	maximum = 0.0225 (1 samples)
Injected flit rate average = 0.871702 (1 samples)
	minimum = 0.50875 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.296836 (1 samples)
	minimum = 0.21125 (1 samples)
	maximum = 0.40425 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.9815 (1 samples)
Hops average = 5.1576 (1 samples)
Total run time 48.1752
