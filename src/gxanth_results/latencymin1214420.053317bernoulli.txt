BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 341.583
	minimum = 22
	maximum = 836
Network latency average = 314.946
	minimum = 22
	maximum = 795
Slowest packet = 491
Flit latency average = 292.115
	minimum = 5
	maximum = 799
Slowest flit = 29344
Fragmentation average = 25.4639
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0463333
	minimum = 0.032 (at node 92)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0152135
	minimum = 0.008 (at node 135)
	maximum = 0.023 (at node 2)
Injected flit rate average = 0.827083
	minimum = 0.576 (at node 92)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.280401
	minimum = 0.153 (at node 135)
	maximum = 0.429 (at node 76)
Injected packet length average = 17.8507
Accepted packet length average = 18.431
Total in-flight flits = 106525 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 684.51
	minimum = 22
	maximum = 1657
Network latency average = 640.917
	minimum = 22
	maximum = 1560
Slowest packet = 491
Flit latency average = 616.319
	minimum = 5
	maximum = 1543
Slowest flit = 69858
Fragmentation average = 26.9687
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0363177
	minimum = 0.0215 (at node 80)
	maximum = 0.048 (at node 155)
Accepted packet rate average = 0.0150495
	minimum = 0.008 (at node 153)
	maximum = 0.022 (at node 71)
Injected flit rate average = 0.652503
	minimum = 0.387 (at node 80)
	maximum = 0.864 (at node 155)
Accepted flit rate average= 0.273846
	minimum = 0.152 (at node 153)
	maximum = 0.396 (at node 71)
Injected packet length average = 17.9665
Accepted packet length average = 18.1964
Total in-flight flits = 148211 (0 measured)
latency change    = 0.500982
throughput change = 0.0239356
Class 0:
Packet latency average = 1756.83
	minimum = 67
	maximum = 2529
Network latency average = 1616.55
	minimum = 22
	maximum = 2335
Slowest packet = 3176
Flit latency average = 1600.81
	minimum = 5
	maximum = 2377
Slowest flit = 98218
Fragmentation average = 21.4368
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0154531
	minimum = 0.003 (at node 3)
	maximum = 0.036 (at node 126)
Accepted packet rate average = 0.0148802
	minimum = 0.007 (at node 71)
	maximum = 0.026 (at node 34)
Injected flit rate average = 0.277896
	minimum = 0.065 (at node 3)
	maximum = 0.648 (at node 126)
Accepted flit rate average= 0.267557
	minimum = 0.126 (at node 71)
	maximum = 0.454 (at node 34)
Injected packet length average = 17.9831
Accepted packet length average = 17.9807
Total in-flight flits = 150192 (0 measured)
latency change    = 0.610372
throughput change = 0.0235055
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1932.79
	minimum = 814
	maximum = 2692
Network latency average = 73.0388
	minimum = 22
	maximum = 971
Slowest packet = 17067
Flit latency average = 2136.46
	minimum = 5
	maximum = 3142
Slowest flit = 136284
Fragmentation average = 5.25243
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0149688
	minimum = 0.003 (at node 129)
	maximum = 0.034 (at node 17)
Accepted packet rate average = 0.0148281
	minimum = 0.006 (at node 20)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.269135
	minimum = 0.054 (at node 129)
	maximum = 0.612 (at node 17)
Accepted flit rate average= 0.266583
	minimum = 0.111 (at node 20)
	maximum = 0.459 (at node 16)
Injected packet length average = 17.9798
Accepted packet length average = 17.9782
Total in-flight flits = 150722 (49820 measured)
latency change    = 0.0910372
throughput change = 0.00365349
Class 0:
Packet latency average = 2688
	minimum = 814
	maximum = 3994
Network latency average = 695.933
	minimum = 22
	maximum = 1961
Slowest packet = 17067
Flit latency average = 2373.31
	minimum = 5
	maximum = 3963
Slowest flit = 157085
Fragmentation average = 10.1
	minimum = 0
	maximum = 63
Injected packet rate average = 0.0147057
	minimum = 0.005 (at node 186)
	maximum = 0.0285 (at node 46)
Accepted packet rate average = 0.0147865
	minimum = 0.0075 (at node 36)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.264768
	minimum = 0.093 (at node 186)
	maximum = 0.5095 (at node 46)
Accepted flit rate average= 0.265536
	minimum = 0.1345 (at node 36)
	maximum = 0.388 (at node 129)
Injected packet length average = 18.0044
Accepted packet length average = 17.9581
Total in-flight flits = 149908 (94466 measured)
latency change    = 0.280957
throughput change = 0.00394249
Class 0:
Packet latency average = 3498.28
	minimum = 814
	maximum = 4844
Network latency average = 1383.54
	minimum = 22
	maximum = 2976
Slowest packet = 17067
Flit latency average = 2512.81
	minimum = 5
	maximum = 4868
Slowest flit = 178288
Fragmentation average = 14.4219
	minimum = 0
	maximum = 84
Injected packet rate average = 0.0149705
	minimum = 0.00666667 (at node 191)
	maximum = 0.0276667 (at node 189)
Accepted packet rate average = 0.0147743
	minimum = 0.00933333 (at node 36)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.269113
	minimum = 0.12 (at node 191)
	maximum = 0.500667 (at node 189)
Accepted flit rate average= 0.26572
	minimum = 0.164667 (at node 36)
	maximum = 0.388333 (at node 128)
Injected packet length average = 17.9762
Accepted packet length average = 17.9853
Total in-flight flits = 152099 (129800 measured)
latency change    = 0.231623
throughput change = 0.000692561
Class 0:
Packet latency average = 4125.86
	minimum = 814
	maximum = 5687
Network latency average = 1956.62
	minimum = 22
	maximum = 3987
Slowest packet = 17067
Flit latency average = 2610.02
	minimum = 5
	maximum = 5414
Slowest flit = 212741
Fragmentation average = 16.3907
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0149896
	minimum = 0.007 (at node 191)
	maximum = 0.03 (at node 189)
Accepted packet rate average = 0.0147865
	minimum = 0.01 (at node 64)
	maximum = 0.01975 (at node 103)
Injected flit rate average = 0.269634
	minimum = 0.126 (at node 191)
	maximum = 0.54325 (at node 189)
Accepted flit rate average= 0.2661
	minimum = 0.18 (at node 64)
	maximum = 0.35525 (at node 128)
Injected packet length average = 17.9881
Accepted packet length average = 17.9962
Total in-flight flits = 152971 (148525 measured)
latency change    = 0.15211
throughput change = 0.00142719
Class 0:
Packet latency average = 4675.76
	minimum = 814
	maximum = 6502
Network latency average = 2310.65
	minimum = 22
	maximum = 4796
Slowest packet = 17067
Flit latency average = 2659.45
	minimum = 5
	maximum = 6166
Slowest flit = 200696
Fragmentation average = 17.7604
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0149646
	minimum = 0.0062 (at node 191)
	maximum = 0.026 (at node 189)
Accepted packet rate average = 0.0148573
	minimum = 0.0112 (at node 52)
	maximum = 0.019 (at node 95)
Injected flit rate average = 0.269389
	minimum = 0.1116 (at node 191)
	maximum = 0.4706 (at node 189)
Accepted flit rate average= 0.267272
	minimum = 0.2026 (at node 36)
	maximum = 0.3464 (at node 95)
Injected packet length average = 18.0017
Accepted packet length average = 17.9893
Total in-flight flits = 152181 (151585 measured)
latency change    = 0.117605
throughput change = 0.00438361
Class 0:
Packet latency average = 5131.95
	minimum = 814
	maximum = 7404
Network latency average = 2486.06
	minimum = 22
	maximum = 5535
Slowest packet = 17067
Flit latency average = 2692.34
	minimum = 5
	maximum = 6166
Slowest flit = 200696
Fragmentation average = 18.132
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0148394
	minimum = 0.00816667 (at node 191)
	maximum = 0.0261667 (at node 189)
Accepted packet rate average = 0.0148602
	minimum = 0.0116667 (at node 79)
	maximum = 0.0188333 (at node 95)
Injected flit rate average = 0.267043
	minimum = 0.147 (at node 191)
	maximum = 0.472833 (at node 189)
Accepted flit rate average= 0.267467
	minimum = 0.21 (at node 79)
	maximum = 0.341167 (at node 95)
Injected packet length average = 17.9955
Accepted packet length average = 17.9988
Total in-flight flits = 149690 (149666 measured)
latency change    = 0.0888922
throughput change = 0.000729581
Class 0:
Packet latency average = 5543.75
	minimum = 814
	maximum = 8240
Network latency average = 2579.89
	minimum = 22
	maximum = 6270
Slowest packet = 17067
Flit latency average = 2712.59
	minimum = 5
	maximum = 6253
Slowest flit = 319391
Fragmentation average = 18.392
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0148408
	minimum = 0.00871429 (at node 191)
	maximum = 0.0242857 (at node 189)
Accepted packet rate average = 0.0148676
	minimum = 0.0114286 (at node 112)
	maximum = 0.0188571 (at node 128)
Injected flit rate average = 0.267048
	minimum = 0.156857 (at node 191)
	maximum = 0.437857 (at node 189)
Accepted flit rate average= 0.267672
	minimum = 0.207571 (at node 112)
	maximum = 0.339286 (at node 128)
Injected packet length average = 17.9942
Accepted packet length average = 18.0038
Total in-flight flits = 149289 (149289 measured)
latency change    = 0.0742827
throughput change = 0.000765344
Draining all recorded packets ...
Class 0:
Remaining flits: 375786 375787 375788 375789 375790 375791 375792 375793 375794 375795 [...] (147549 flits)
Measured flits: 375786 375787 375788 375789 375790 375791 375792 375793 375794 375795 [...] (147549 flits)
Class 0:
Remaining flits: 394416 394417 394418 394419 394420 394421 394422 394423 394424 394425 [...] (149127 flits)
Measured flits: 394416 394417 394418 394419 394420 394421 394422 394423 394424 394425 [...] (149127 flits)
Class 0:
Remaining flits: 457596 457597 457598 457599 457600 457601 457602 457603 457604 457605 [...] (146322 flits)
Measured flits: 457596 457597 457598 457599 457600 457601 457602 457603 457604 457605 [...] (146322 flits)
Class 0:
Remaining flits: 508986 508987 508988 508989 508990 508991 508992 508993 508994 508995 [...] (147167 flits)
Measured flits: 508986 508987 508988 508989 508990 508991 508992 508993 508994 508995 [...] (147167 flits)
Class 0:
Remaining flits: 613062 613063 613064 613065 613066 613067 613068 613069 613070 613071 [...] (147294 flits)
Measured flits: 613062 613063 613064 613065 613066 613067 613068 613069 613070 613071 [...] (147294 flits)
Class 0:
Remaining flits: 655452 655453 655454 655455 655456 655457 655458 655459 655460 655461 [...] (149372 flits)
Measured flits: 655452 655453 655454 655455 655456 655457 655458 655459 655460 655461 [...] (149372 flits)
Class 0:
Remaining flits: 687744 687745 687746 687747 687748 687749 687750 687751 687752 687753 [...] (147901 flits)
Measured flits: 687744 687745 687746 687747 687748 687749 687750 687751 687752 687753 [...] (147901 flits)
Class 0:
Remaining flits: 759940 759941 781146 781147 781148 781149 781150 781151 781152 781153 [...] (148426 flits)
Measured flits: 759940 759941 781146 781147 781148 781149 781150 781151 781152 781153 [...] (148426 flits)
Class 0:
Remaining flits: 801216 801217 801218 801219 801220 801221 801222 801223 801224 801225 [...] (148116 flits)
Measured flits: 801216 801217 801218 801219 801220 801221 801222 801223 801224 801225 [...] (148116 flits)
Class 0:
Remaining flits: 820512 820513 820514 820515 820516 820517 820518 820519 820520 820521 [...] (142250 flits)
Measured flits: 820512 820513 820514 820515 820516 820517 820518 820519 820520 820521 [...] (142250 flits)
Class 0:
Remaining flits: 866736 866737 866738 866739 866740 866741 866742 866743 866744 866745 [...] (138444 flits)
Measured flits: 866736 866737 866738 866739 866740 866741 866742 866743 866744 866745 [...] (138444 flits)
Class 0:
Remaining flits: 956466 956467 956468 956469 956470 956471 956472 956473 956474 956475 [...] (142075 flits)
Measured flits: 956466 956467 956468 956469 956470 956471 956472 956473 956474 956475 [...] (142075 flits)
Class 0:
Remaining flits: 981306 981307 981308 981309 981310 981311 981312 981313 981314 981315 [...] (142501 flits)
Measured flits: 981306 981307 981308 981309 981310 981311 981312 981313 981314 981315 [...] (142501 flits)
Class 0:
Remaining flits: 1070226 1070227 1070228 1070229 1070230 1070231 1070232 1070233 1070234 1070235 [...] (144771 flits)
Measured flits: 1070226 1070227 1070228 1070229 1070230 1070231 1070232 1070233 1070234 1070235 [...] (144663 flits)
Class 0:
Remaining flits: 1117098 1117099 1117100 1117101 1117102 1117103 1117104 1117105 1117106 1117107 [...] (145705 flits)
Measured flits: 1117098 1117099 1117100 1117101 1117102 1117103 1117104 1117105 1117106 1117107 [...] (145201 flits)
Class 0:
Remaining flits: 1121202 1121203 1121204 1121205 1121206 1121207 1121208 1121209 1121210 1121211 [...] (145281 flits)
Measured flits: 1121202 1121203 1121204 1121205 1121206 1121207 1121208 1121209 1121210 1121211 [...] (144327 flits)
Class 0:
Remaining flits: 1136304 1136305 1136306 1136307 1136308 1136309 1136310 1136311 1136312 1136313 [...] (142084 flits)
Measured flits: 1136304 1136305 1136306 1136307 1136308 1136309 1136310 1136311 1136312 1136313 [...] (140215 flits)
Class 0:
Remaining flits: 1235286 1235287 1235288 1235289 1235290 1235291 1235292 1235293 1235294 1235295 [...] (142717 flits)
Measured flits: 1235286 1235287 1235288 1235289 1235290 1235291 1235292 1235293 1235294 1235295 [...] (139145 flits)
Class 0:
Remaining flits: 1278624 1278625 1278626 1278627 1278628 1278629 1303524 1303525 1303526 1303527 [...] (145640 flits)
Measured flits: 1278624 1278625 1278626 1278627 1278628 1278629 1303524 1303525 1303526 1303527 [...] (138164 flits)
Class 0:
Remaining flits: 1360296 1360297 1360298 1360299 1360300 1360301 1360302 1360303 1360304 1360305 [...] (142077 flits)
Measured flits: 1360296 1360297 1360298 1360299 1360300 1360301 1360302 1360303 1360304 1360305 [...] (129796 flits)
Class 0:
Remaining flits: 1401192 1401193 1401194 1401195 1401196 1401197 1401198 1401199 1401200 1401201 [...] (144122 flits)
Measured flits: 1401192 1401193 1401194 1401195 1401196 1401197 1401198 1401199 1401200 1401201 [...] (123213 flits)
Class 0:
Remaining flits: 1401208 1401209 1485684 1485685 1485686 1485687 1485688 1485689 1485690 1485691 [...] (144904 flits)
Measured flits: 1401208 1401209 1485684 1485685 1485686 1485687 1485688 1485689 1485690 1485691 [...] (111806 flits)
Class 0:
Remaining flits: 1530180 1530181 1530182 1530183 1530184 1530185 1530186 1530187 1530188 1530189 [...] (143752 flits)
Measured flits: 1530180 1530181 1530182 1530183 1530184 1530185 1530186 1530187 1530188 1530189 [...] (96990 flits)
Class 0:
Remaining flits: 1569654 1569655 1569656 1569657 1569658 1569659 1569660 1569661 1569662 1569663 [...] (143652 flits)
Measured flits: 1569654 1569655 1569656 1569657 1569658 1569659 1569660 1569661 1569662 1569663 [...] (79847 flits)
Class 0:
Remaining flits: 1620234 1620235 1620236 1620237 1620238 1620239 1620240 1620241 1620242 1620243 [...] (142138 flits)
Measured flits: 1625688 1625689 1625690 1625691 1625692 1625693 1625694 1625695 1625696 1625697 [...] (60896 flits)
Class 0:
Remaining flits: 1682514 1682515 1682516 1682517 1682518 1682519 1682520 1682521 1682522 1682523 [...] (140780 flits)
Measured flits: 1682514 1682515 1682516 1682517 1682518 1682519 1682520 1682521 1682522 1682523 [...] (44574 flits)
Class 0:
Remaining flits: 1709334 1709335 1709336 1709337 1709338 1709339 1709340 1709341 1709342 1709343 [...] (139768 flits)
Measured flits: 1709334 1709335 1709336 1709337 1709338 1709339 1709340 1709341 1709342 1709343 [...] (31560 flits)
Class 0:
Remaining flits: 1734300 1734301 1734302 1734303 1734304 1734305 1734306 1734307 1734308 1734309 [...] (136209 flits)
Measured flits: 1734300 1734301 1734302 1734303 1734304 1734305 1734306 1734307 1734308 1734309 [...] (21592 flits)
Class 0:
Remaining flits: 1789776 1789777 1789778 1789779 1789780 1789781 1789782 1789783 1789784 1789785 [...] (138724 flits)
Measured flits: 1789776 1789777 1789778 1789779 1789780 1789781 1789782 1789783 1789784 1789785 [...] (14331 flits)
Class 0:
Remaining flits: 1797012 1797013 1797014 1797015 1797016 1797017 1797018 1797019 1797020 1797021 [...] (137284 flits)
Measured flits: 1797012 1797013 1797014 1797015 1797016 1797017 1797018 1797019 1797020 1797021 [...] (7998 flits)
Class 0:
Remaining flits: 1868328 1868329 1868330 1868331 1868332 1868333 1868334 1868335 1868336 1868337 [...] (141528 flits)
Measured flits: 1901898 1901899 1901900 1901901 1901902 1901903 1901904 1901905 1901906 1901907 [...] (3885 flits)
Class 0:
Remaining flits: 1945008 1945009 1945010 1945011 1945012 1945013 1945014 1945015 1945016 1945017 [...] (142007 flits)
Measured flits: 1962972 1962973 1962974 1962975 1962976 1962977 1962978 1962979 1962980 1962981 [...] (1774 flits)
Class 0:
Remaining flits: 2019780 2019781 2019782 2019783 2019784 2019785 2019786 2019787 2019788 2019789 [...] (139914 flits)
Measured flits: 2081376 2081377 2081378 2081379 2081380 2081381 2081382 2081383 2081384 2081385 [...] (811 flits)
Class 0:
Remaining flits: 2019780 2019781 2019782 2019783 2019784 2019785 2019786 2019787 2019788 2019789 [...] (139620 flits)
Measured flits: 2120256 2120257 2120258 2120259 2120260 2120261 2120262 2120263 2120264 2120265 [...] (384 flits)
Class 0:
Remaining flits: 2019780 2019781 2019782 2019783 2019784 2019785 2019786 2019787 2019788 2019789 [...] (140196 flits)
Measured flits: 2357460 2357461 2357462 2357463 2357464 2357465 2357466 2357467 2357468 2357469 [...] (324 flits)
Class 0:
Remaining flits: 2174400 2174401 2174402 2174403 2174404 2174405 2174406 2174407 2174408 2174409 [...] (141131 flits)
Measured flits: 2372922 2372923 2372924 2372925 2372926 2372927 2372928 2372929 2372930 2372931 [...] (174 flits)
Class 0:
Remaining flits: 2186658 2186659 2186660 2186661 2186662 2186663 2186664 2186665 2186666 2186667 [...] (142516 flits)
Measured flits: 2406222 2406223 2406224 2406225 2406226 2406227 2406228 2406229 2406230 2406231 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2252772 2252773 2252774 2252775 2252776 2252777 2252778 2252779 2252780 2252781 [...] (93936 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2301912 2301913 2301914 2301915 2301916 2301917 2301918 2301919 2301920 2301921 [...] (45841 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2359206 2359207 2359208 2359209 2359210 2359211 2359212 2359213 2359214 2359215 [...] (6134 flits)
Measured flits: (0 flits)
Time taken is 51106 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15222.2 (1 samples)
	minimum = 814 (1 samples)
	maximum = 37373 (1 samples)
Network latency average = 2806.34 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8661 (1 samples)
Flit latency average = 2764.98 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8644 (1 samples)
Fragmentation average = 20.2076 (1 samples)
	minimum = 0 (1 samples)
	maximum = 252 (1 samples)
Injected packet rate average = 0.0148408 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.0242857 (1 samples)
Accepted packet rate average = 0.0148676 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.267048 (1 samples)
	minimum = 0.156857 (1 samples)
	maximum = 0.437857 (1 samples)
Accepted flit rate average = 0.267672 (1 samples)
	minimum = 0.207571 (1 samples)
	maximum = 0.339286 (1 samples)
Injected packet size average = 17.9942 (1 samples)
Accepted packet size average = 18.0038 (1 samples)
Hops average = 5.06016 (1 samples)
Total run time 51.0341
