BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.267
	minimum = 25
	maximum = 933
Network latency average = 304.6
	minimum = 22
	maximum = 871
Slowest packet = 918
Flit latency average = 267.598
	minimum = 5
	maximum = 870
Slowest flit = 17874
Fragmentation average = 85.3913
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0150938
	minimum = 0.002 (at node 174)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.293349
	minimum = 0.074 (at node 174)
	maximum = 0.471 (at node 44)
Injected packet length average = 17.8227
Accepted packet length average = 19.4351
Total in-flight flits = 104201 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.914
	minimum = 24
	maximum = 1804
Network latency average = 571.715
	minimum = 22
	maximum = 1708
Slowest packet = 993
Flit latency average = 528.159
	minimum = 5
	maximum = 1714
Slowest flit = 41943
Fragmentation average = 114.269
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0464349
	minimum = 0.037 (at node 129)
	maximum = 0.0555 (at node 134)
Accepted packet rate average = 0.0160859
	minimum = 0.0085 (at node 135)
	maximum = 0.0225 (at node 63)
Injected flit rate average = 0.832029
	minimum = 0.666 (at node 129)
	maximum = 0.9955 (at node 134)
Accepted flit rate average= 0.30307
	minimum = 0.161 (at node 135)
	maximum = 0.429 (at node 63)
Injected packet length average = 17.9182
Accepted packet length average = 18.8407
Total in-flight flits = 204597 (0 measured)
latency change    = 0.458089
throughput change = 0.0320762
Class 0:
Packet latency average = 1484.59
	minimum = 22
	maximum = 2589
Network latency average = 1431.38
	minimum = 22
	maximum = 2518
Slowest packet = 3735
Flit latency average = 1399.01
	minimum = 5
	maximum = 2654
Slowest flit = 50374
Fragmentation average = 156.931
	minimum = 0
	maximum = 485
Injected packet rate average = 0.0434792
	minimum = 0.017 (at node 128)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0164688
	minimum = 0.008 (at node 70)
	maximum = 0.031 (at node 159)
Injected flit rate average = 0.783458
	minimum = 0.309 (at node 128)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.297609
	minimum = 0.169 (at node 113)
	maximum = 0.512 (at node 16)
Injected packet length average = 18.0192
Accepted packet length average = 18.0712
Total in-flight flits = 297882 (0 measured)
latency change    = 0.593212
throughput change = 0.0183493
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 383.561
	minimum = 23
	maximum = 1611
Network latency average = 127.884
	minimum = 22
	maximum = 922
Slowest packet = 26276
Flit latency average = 2171.72
	minimum = 5
	maximum = 3297
Slowest flit = 103589
Fragmentation average = 5.93434
	minimum = 0
	maximum = 20
Injected packet rate average = 0.037
	minimum = 0 (at node 52)
	maximum = 0.056 (at node 63)
Accepted packet rate average = 0.0152292
	minimum = 0.005 (at node 180)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.666161
	minimum = 0 (at node 52)
	maximum = 1 (at node 63)
Accepted flit rate average= 0.273813
	minimum = 0.091 (at node 180)
	maximum = 0.515 (at node 129)
Injected packet length average = 18.0044
Accepted packet length average = 17.9795
Total in-flight flits = 373398 (124427 measured)
latency change    = 2.87055
throughput change = 0.0869094
Class 0:
Packet latency average = 804.83
	minimum = 23
	maximum = 2460
Network latency average = 370.048
	minimum = 22
	maximum = 1933
Slowest packet = 26276
Flit latency average = 2504.89
	minimum = 5
	maximum = 4010
Slowest flit = 156395
Fragmentation average = 6.49545
	minimum = 0
	maximum = 23
Injected packet rate average = 0.0294609
	minimum = 0.0055 (at node 72)
	maximum = 0.0475 (at node 129)
Accepted packet rate average = 0.0152474
	minimum = 0.0085 (at node 48)
	maximum = 0.0225 (at node 66)
Injected flit rate average = 0.530854
	minimum = 0.099 (at node 128)
	maximum = 0.8575 (at node 129)
Accepted flit rate average= 0.271833
	minimum = 0.16 (at node 48)
	maximum = 0.4015 (at node 129)
Injected packet length average = 18.0189
Accepted packet length average = 17.8282
Total in-flight flits = 397726 (196245 measured)
latency change    = 0.523426
throughput change = 0.00728081
Class 0:
Packet latency average = 1202.23
	minimum = 23
	maximum = 3498
Network latency average = 635.147
	minimum = 22
	maximum = 2902
Slowest packet = 26276
Flit latency average = 2827.59
	minimum = 5
	maximum = 4957
Slowest flit = 165376
Fragmentation average = 7.0531
	minimum = 0
	maximum = 57
Injected packet rate average = 0.0250069
	minimum = 0.00633333 (at node 128)
	maximum = 0.0396667 (at node 111)
Accepted packet rate average = 0.0152431
	minimum = 0.0103333 (at node 139)
	maximum = 0.022 (at node 66)
Injected flit rate average = 0.450569
	minimum = 0.114 (at node 128)
	maximum = 0.713 (at node 111)
Accepted flit rate average= 0.270927
	minimum = 0.186 (at node 180)
	maximum = 0.395333 (at node 129)
Injected packet length average = 18.0178
Accepted packet length average = 17.7738
Total in-flight flits = 401910 (247822 measured)
latency change    = 0.330554
throughput change = 0.003345
Class 0:
Packet latency average = 1696.57
	minimum = 23
	maximum = 4886
Network latency average = 991.105
	minimum = 22
	maximum = 3974
Slowest packet = 26276
Flit latency average = 3153.21
	minimum = 5
	maximum = 5639
Slowest flit = 171377
Fragmentation average = 12.1335
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0225951
	minimum = 0.00675 (at node 176)
	maximum = 0.0365 (at node 129)
Accepted packet rate average = 0.0151823
	minimum = 0.011 (at node 139)
	maximum = 0.02075 (at node 129)
Injected flit rate average = 0.406986
	minimum = 0.11825 (at node 176)
	maximum = 0.657 (at node 129)
Accepted flit rate average= 0.270245
	minimum = 0.1955 (at node 162)
	maximum = 0.37375 (at node 129)
Injected packet length average = 18.0122
Accepted packet length average = 17.8
Total in-flight flits = 403408 (295582 measured)
latency change    = 0.291374
throughput change = 0.00252472
Class 0:
Packet latency average = 2651.48
	minimum = 23
	maximum = 5987
Network latency average = 1799.65
	minimum = 22
	maximum = 4958
Slowest packet = 26276
Flit latency average = 3465.04
	minimum = 5
	maximum = 6617
Slowest flit = 205181
Fragmentation average = 26.2944
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0210302
	minimum = 0.0058 (at node 112)
	maximum = 0.0336 (at node 129)
Accepted packet rate average = 0.0151469
	minimum = 0.0116 (at node 162)
	maximum = 0.0198 (at node 129)
Injected flit rate average = 0.378694
	minimum = 0.1058 (at node 112)
	maximum = 0.6052 (at node 129)
Accepted flit rate average= 0.27029
	minimum = 0.2066 (at node 162)
	maximum = 0.3526 (at node 129)
Injected packet length average = 18.0071
Accepted packet length average = 17.8446
Total in-flight flits = 402688 (335978 measured)
latency change    = 0.360143
throughput change = 0.000165717
Class 0:
Packet latency average = 3754.81
	minimum = 23
	maximum = 6976
Network latency average = 2855.54
	minimum = 22
	maximum = 5960
Slowest packet = 26276
Flit latency average = 3763.59
	minimum = 5
	maximum = 7303
Slowest flit = 271112
Fragmentation average = 41.2157
	minimum = 0
	maximum = 643
Injected packet rate average = 0.020151
	minimum = 0.00866667 (at node 112)
	maximum = 0.0295 (at node 177)
Accepted packet rate average = 0.0151528
	minimum = 0.0115 (at node 79)
	maximum = 0.019 (at node 157)
Injected flit rate average = 0.363025
	minimum = 0.157167 (at node 112)
	maximum = 0.532333 (at node 177)
Accepted flit rate average= 0.270239
	minimum = 0.201 (at node 79)
	maximum = 0.339667 (at node 157)
Injected packet length average = 18.0152
Accepted packet length average = 17.8343
Total in-flight flits = 405499 (369488 measured)
latency change    = 0.293845
throughput change = 0.000188234
Class 0:
Packet latency average = 4699.12
	minimum = 23
	maximum = 7877
Network latency average = 3781.38
	minimum = 22
	maximum = 6942
Slowest packet = 26276
Flit latency average = 4051.12
	minimum = 5
	maximum = 8160
Slowest flit = 282347
Fragmentation average = 50.6828
	minimum = 0
	maximum = 643
Injected packet rate average = 0.0194241
	minimum = 0.00914286 (at node 8)
	maximum = 0.0278571 (at node 101)
Accepted packet rate average = 0.0151153
	minimum = 0.0125714 (at node 64)
	maximum = 0.0187143 (at node 40)
Injected flit rate average = 0.349827
	minimum = 0.164571 (at node 8)
	maximum = 0.501429 (at node 101)
Accepted flit rate average= 0.269932
	minimum = 0.221143 (at node 64)
	maximum = 0.333 (at node 40)
Injected packet length average = 18.0099
Accepted packet length average = 17.8582
Total in-flight flits = 405973 (389211 measured)
latency change    = 0.200953
throughput change = 0.00113519
Draining all recorded packets ...
Class 0:
Remaining flits: 281764 281765 281766 281767 281768 281769 281770 281771 288789 288790 [...] (407489 flits)
Measured flits: 471510 471511 471512 471513 471514 471515 471516 471517 471518 471519 [...] (399838 flits)
Class 0:
Remaining flits: 342576 342577 342578 342579 342580 342581 342582 342583 342584 342585 [...] (410555 flits)
Measured flits: 471510 471511 471512 471513 471514 471515 471516 471517 471518 471519 [...] (407453 flits)
Class 0:
Remaining flits: 345384 345385 345386 345387 345388 345389 345390 345391 345392 345393 [...] (410316 flits)
Measured flits: 471672 471673 471674 471675 471676 471677 471678 471679 471680 471681 [...] (409119 flits)
Class 0:
Remaining flits: 345384 345385 345386 345387 345388 345389 345390 345391 345392 345393 [...] (410993 flits)
Measured flits: 472374 472375 472376 472377 472378 472379 472380 472381 472382 472383 [...] (410616 flits)
Class 0:
Remaining flits: 386672 386673 386674 386675 416466 416467 416468 416469 416470 416471 [...] (409811 flits)
Measured flits: 472374 472375 472376 472377 472378 472379 472380 472381 472382 472383 [...] (409735 flits)
Class 0:
Remaining flits: 451422 451423 451424 451425 451426 451427 451428 451429 451430 451431 [...] (410705 flits)
Measured flits: 472374 472375 472376 472377 472378 472379 472380 472381 472382 472383 [...] (410687 flits)
Class 0:
Remaining flits: 472374 472375 472376 472377 472378 472379 472380 472381 472382 472383 [...] (411455 flits)
Measured flits: 472374 472375 472376 472377 472378 472379 472380 472381 472382 472383 [...] (411455 flits)
Class 0:
Remaining flits: 506538 506539 506540 506541 506542 506543 506544 506545 506546 506547 [...] (415537 flits)
Measured flits: 506538 506539 506540 506541 506542 506543 506544 506545 506546 506547 [...] (415537 flits)
Class 0:
Remaining flits: 544536 544537 544538 544539 544540 544541 544542 544543 544544 544545 [...] (415028 flits)
Measured flits: 544536 544537 544538 544539 544540 544541 544542 544543 544544 544545 [...] (414290 flits)
Class 0:
Remaining flits: 599328 599329 599330 599331 599332 599333 599334 599335 599336 599337 [...] (415358 flits)
Measured flits: 599328 599329 599330 599331 599332 599333 599334 599335 599336 599337 [...] (412406 flits)
Class 0:
Remaining flits: 655362 655363 655364 655365 655366 655367 655368 655369 655370 655371 [...] (416000 flits)
Measured flits: 655362 655363 655364 655365 655366 655367 655368 655369 655370 655371 [...] (407275 flits)
Class 0:
Remaining flits: 655362 655363 655364 655365 655366 655367 655368 655369 655370 655371 [...] (418188 flits)
Measured flits: 655362 655363 655364 655365 655366 655367 655368 655369 655370 655371 [...] (398388 flits)
Class 0:
Remaining flits: 655363 655364 655365 655366 655367 655368 655369 655370 655371 655372 [...] (417652 flits)
Measured flits: 655363 655364 655365 655366 655367 655368 655369 655370 655371 655372 [...] (380760 flits)
Class 0:
Remaining flits: 779070 779071 779072 779073 779074 779075 816804 816805 816806 816807 [...] (417565 flits)
Measured flits: 779070 779071 779072 779073 779074 779075 816804 816805 816806 816807 [...] (356628 flits)
Class 0:
Remaining flits: 871650 871651 871652 871653 871654 871655 871656 871657 871658 871659 [...] (417321 flits)
Measured flits: 871650 871651 871652 871653 871654 871655 871656 871657 871658 871659 [...] (327181 flits)
Class 0:
Remaining flits: 955728 955729 955730 955731 955732 955733 955734 955735 955736 955737 [...] (418284 flits)
Measured flits: 955728 955729 955730 955731 955732 955733 955734 955735 955736 955737 [...] (292884 flits)
Class 0:
Remaining flits: 961940 961941 961942 961943 961944 961945 961946 961947 961948 961949 [...] (420869 flits)
Measured flits: 961940 961941 961942 961943 961944 961945 961946 961947 961948 961949 [...] (257400 flits)
Class 0:
Remaining flits: 973476 973477 973478 973479 973480 973481 973482 973483 973484 973485 [...] (421980 flits)
Measured flits: 973476 973477 973478 973479 973480 973481 973482 973483 973484 973485 [...] (218345 flits)
Class 0:
Remaining flits: 973476 973477 973478 973479 973480 973481 973482 973483 973484 973485 [...] (422209 flits)
Measured flits: 973476 973477 973478 973479 973480 973481 973482 973483 973484 973485 [...] (177714 flits)
Class 0:
Remaining flits: 1078794 1078795 1078796 1078797 1078798 1078799 1078800 1078801 1078802 1078803 [...] (422880 flits)
Measured flits: 1078794 1078795 1078796 1078797 1078798 1078799 1078800 1078801 1078802 1078803 [...] (139680 flits)
Class 0:
Remaining flits: 1188414 1188415 1188416 1188417 1188418 1188419 1188420 1188421 1188422 1188423 [...] (425395 flits)
Measured flits: 1188414 1188415 1188416 1188417 1188418 1188419 1188420 1188421 1188422 1188423 [...] (103386 flits)
Class 0:
Remaining flits: 1232376 1232377 1232378 1232379 1232380 1232381 1232382 1232383 1232384 1232385 [...] (426303 flits)
Measured flits: 1232376 1232377 1232378 1232379 1232380 1232381 1232382 1232383 1232384 1232385 [...] (72510 flits)
Class 0:
Remaining flits: 1289250 1289251 1289252 1289253 1289254 1289255 1289256 1289257 1289258 1289259 [...] (424835 flits)
Measured flits: 1289250 1289251 1289252 1289253 1289254 1289255 1289256 1289257 1289258 1289259 [...] (47981 flits)
Class 0:
Remaining flits: 1317674 1317675 1317676 1317677 1317678 1317679 1317680 1317681 1317682 1317683 [...] (426878 flits)
Measured flits: 1317674 1317675 1317676 1317677 1317678 1317679 1317680 1317681 1317682 1317683 [...] (29142 flits)
Class 0:
Remaining flits: 1372374 1372375 1372376 1372377 1372378 1372379 1372380 1372381 1372382 1372383 [...] (425120 flits)
Measured flits: 1372374 1372375 1372376 1372377 1372378 1372379 1372380 1372381 1372382 1372383 [...] (16404 flits)
Class 0:
Remaining flits: 1444752 1444753 1444754 1444755 1444756 1444757 1444758 1444759 1444760 1444761 [...] (424227 flits)
Measured flits: 1444752 1444753 1444754 1444755 1444756 1444757 1444758 1444759 1444760 1444761 [...] (9059 flits)
Class 0:
Remaining flits: 1481508 1481509 1481510 1481511 1481512 1481513 1481514 1481515 1481516 1481517 [...] (424735 flits)
Measured flits: 1481508 1481509 1481510 1481511 1481512 1481513 1481514 1481515 1481516 1481517 [...] (4926 flits)
Class 0:
Remaining flits: 1481508 1481509 1481510 1481511 1481512 1481513 1481514 1481515 1481516 1481517 [...] (424999 flits)
Measured flits: 1481508 1481509 1481510 1481511 1481512 1481513 1481514 1481515 1481516 1481517 [...] (2249 flits)
Class 0:
Remaining flits: 1529650 1529651 1529652 1529653 1529654 1529655 1529656 1529657 1620054 1620055 [...] (425413 flits)
Measured flits: 1629882 1629883 1629884 1629885 1629886 1629887 1629888 1629889 1629890 1629891 [...] (872 flits)
Class 0:
Remaining flits: 1660446 1660447 1660448 1660449 1660450 1660451 1660452 1660453 1660454 1660455 [...] (421688 flits)
Measured flits: 1709856 1709857 1709858 1709859 1709860 1709861 1709862 1709863 1709864 1709865 [...] (345 flits)
Class 0:
Remaining flits: 1660463 1703772 1703773 1703774 1703775 1703776 1703777 1703778 1703779 1703780 [...] (422373 flits)
Measured flits: 1765854 1765855 1765856 1765857 1765858 1765859 1765860 1765861 1765862 1765863 [...] (144 flits)
Class 0:
Remaining flits: 1733958 1733959 1733960 1733961 1733962 1733963 1733964 1733965 1733966 1733967 [...] (424898 flits)
Measured flits: 1765854 1765855 1765856 1765857 1765858 1765859 1765860 1765861 1765862 1765863 [...] (54 flits)
Class 0:
Remaining flits: 1765854 1765855 1765856 1765857 1765858 1765859 1765860 1765861 1765862 1765863 [...] (426910 flits)
Measured flits: 1765854 1765855 1765856 1765857 1765858 1765859 1765860 1765861 1765862 1765863 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1885860 1885861 1885862 1885863 1885864 1885865 1885866 1885867 1885868 1885869 [...] (376518 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1948698 1948699 1948700 1948701 1948702 1948703 1948704 1948705 1948706 1948707 [...] (325920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1948698 1948699 1948700 1948701 1948702 1948703 1948704 1948705 1948706 1948707 [...] (275971 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2011608 2011609 2011610 2011611 2011612 2011613 2011614 2011615 2011616 2011617 [...] (226940 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2011614 2011615 2011616 2011617 2011618 2011619 2011620 2011621 2011622 2011623 [...] (178812 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2073870 2073871 2073872 2073873 2073874 2073875 2073876 2073877 2073878 2073879 [...] (131025 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2241450 2241451 2241452 2241453 2241454 2241455 2241456 2241457 2241458 2241459 [...] (83339 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2268224 2268225 2268226 2268227 2268228 2268229 2268230 2268231 2268232 2268233 [...] (36256 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2406597 2406598 2406599 2412666 2412667 2412668 2412669 2412670 2412671 2412672 [...] (8159 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2433186 2433187 2433188 2433189 2433190 2433191 2433192 2433193 2433194 2433195 [...] (656 flits)
Measured flits: (0 flits)
Time taken is 54475 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14292.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33916 (1 samples)
Network latency average = 7645.25 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18923 (1 samples)
Flit latency average = 7468.97 (1 samples)
	minimum = 5 (1 samples)
	maximum = 18901 (1 samples)
Fragmentation average = 51.4353 (1 samples)
	minimum = 0 (1 samples)
	maximum = 915 (1 samples)
Injected packet rate average = 0.0194241 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.0278571 (1 samples)
Accepted packet rate average = 0.0151153 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.349827 (1 samples)
	minimum = 0.164571 (1 samples)
	maximum = 0.501429 (1 samples)
Accepted flit rate average = 0.269932 (1 samples)
	minimum = 0.221143 (1 samples)
	maximum = 0.333 (1 samples)
Injected packet size average = 18.0099 (1 samples)
Accepted packet size average = 17.8582 (1 samples)
Hops average = 5.05625 (1 samples)
Total run time 80.6088
