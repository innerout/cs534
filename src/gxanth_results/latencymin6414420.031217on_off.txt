BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 335.165
	minimum = 25
	maximum = 961
Network latency average = 230.752
	minimum = 22
	maximum = 823
Slowest packet = 40
Flit latency average = 202.199
	minimum = 5
	maximum = 810
Slowest flit = 14775
Fragmentation average = 43.3538
	minimum = 0
	maximum = 603
Injected packet rate average = 0.027625
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0136042
	minimum = 0.005 (at node 41)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.491703
	minimum = 0 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.256932
	minimum = 0.111 (at node 41)
	maximum = 0.416 (at node 48)
Injected packet length average = 17.7992
Accepted packet length average = 18.8863
Total in-flight flits = 46141 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 573.632
	minimum = 23
	maximum = 1865
Network latency average = 429.953
	minimum = 22
	maximum = 1570
Slowest packet = 40
Flit latency average = 397.286
	minimum = 5
	maximum = 1671
Slowest flit = 26483
Fragmentation average = 64.5539
	minimum = 0
	maximum = 754
Injected packet rate average = 0.028987
	minimum = 0.001 (at node 138)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0144818
	minimum = 0.008 (at node 153)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.519185
	minimum = 0.018 (at node 138)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.269378
	minimum = 0.147 (at node 153)
	maximum = 0.397 (at node 152)
Injected packet length average = 17.911
Accepted packet length average = 18.6012
Total in-flight flits = 96917 (0 measured)
latency change    = 0.415714
throughput change = 0.0462002
Class 0:
Packet latency average = 1252.53
	minimum = 25
	maximum = 2803
Network latency average = 1035.54
	minimum = 22
	maximum = 2369
Slowest packet = 3190
Flit latency average = 995.902
	minimum = 5
	maximum = 2527
Slowest flit = 40177
Fragmentation average = 124.717
	minimum = 0
	maximum = 750
Injected packet rate average = 0.0312187
	minimum = 0 (at node 126)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.015724
	minimum = 0.007 (at node 32)
	maximum = 0.026 (at node 136)
Injected flit rate average = 0.562458
	minimum = 0 (at node 126)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.287411
	minimum = 0.147 (at node 146)
	maximum = 0.442 (at node 103)
Injected packet length average = 18.0167
Accepted packet length average = 18.2786
Total in-flight flits = 149626 (0 measured)
latency change    = 0.542021
throughput change = 0.0627458
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 336.043
	minimum = 25
	maximum = 1837
Network latency average = 55.4866
	minimum = 23
	maximum = 684
Slowest packet = 17141
Flit latency average = 1419.84
	minimum = 5
	maximum = 3285
Slowest flit = 60982
Fragmentation average = 7.3299
	minimum = 0
	maximum = 77
Injected packet rate average = 0.0321406
	minimum = 0 (at node 180)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0158177
	minimum = 0.004 (at node 36)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.57812
	minimum = 0 (at node 180)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.288646
	minimum = 0.075 (at node 36)
	maximum = 0.502 (at node 129)
Injected packet length average = 17.9872
Accepted packet length average = 18.2483
Total in-flight flits = 205284 (102248 measured)
latency change    = 2.72729
throughput change = 0.00427643
Class 0:
Packet latency average = 550.152
	minimum = 25
	maximum = 2480
Network latency average = 299.299
	minimum = 22
	maximum = 1956
Slowest packet = 17141
Flit latency average = 1639.71
	minimum = 5
	maximum = 4233
Slowest flit = 66596
Fragmentation average = 25.0441
	minimum = 0
	maximum = 618
Injected packet rate average = 0.0312344
	minimum = 0.008 (at node 163)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0159375
	minimum = 0.008 (at node 36)
	maximum = 0.026 (at node 0)
Injected flit rate average = 0.562453
	minimum = 0.144 (at node 163)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.290346
	minimum = 0.1425 (at node 36)
	maximum = 0.442 (at node 0)
Injected packet length average = 18.0075
Accepted packet length average = 18.2178
Total in-flight flits = 254025 (193584 measured)
latency change    = 0.389181
throughput change = 0.00585687
Class 0:
Packet latency average = 1079.22
	minimum = 25
	maximum = 3672
Network latency average = 839.875
	minimum = 22
	maximum = 2982
Slowest packet = 17141
Flit latency average = 1861.55
	minimum = 5
	maximum = 5027
Slowest flit = 90165
Fragmentation average = 51.6153
	minimum = 0
	maximum = 808
Injected packet rate average = 0.0305816
	minimum = 0.0106667 (at node 148)
	maximum = 0.0556667 (at node 7)
Accepted packet rate average = 0.0159097
	minimum = 0.00866667 (at node 36)
	maximum = 0.0233333 (at node 129)
Injected flit rate average = 0.550436
	minimum = 0.192 (at node 148)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.28951
	minimum = 0.173 (at node 36)
	maximum = 0.403333 (at node 129)
Injected packet length average = 17.9989
Accepted packet length average = 18.1971
Total in-flight flits = 299956 (271379 measured)
latency change    = 0.49023
throughput change = 0.00288742
Class 0:
Packet latency average = 1741.99
	minimum = 24
	maximum = 4897
Network latency average = 1505.02
	minimum = 22
	maximum = 3939
Slowest packet = 17141
Flit latency average = 2083.01
	minimum = 5
	maximum = 5745
Slowest flit = 120174
Fragmentation average = 91.5622
	minimum = 0
	maximum = 810
Injected packet rate average = 0.0308724
	minimum = 0.012 (at node 119)
	maximum = 0.0555 (at node 102)
Accepted packet rate average = 0.0158919
	minimum = 0.0105 (at node 36)
	maximum = 0.02125 (at node 138)
Injected flit rate average = 0.555465
	minimum = 0.216 (at node 119)
	maximum = 1 (at node 102)
Accepted flit rate average= 0.28971
	minimum = 0.19525 (at node 36)
	maximum = 0.3825 (at node 138)
Injected packet length average = 17.9923
Accepted packet length average = 18.23
Total in-flight flits = 353963 (340863 measured)
latency change    = 0.38047
throughput change = 0.00068765
Class 0:
Packet latency average = 2264.85
	minimum = 24
	maximum = 5796
Network latency average = 2027.4
	minimum = 22
	maximum = 4946
Slowest packet = 17141
Flit latency average = 2301.98
	minimum = 5
	maximum = 6401
Slowest flit = 155600
Fragmentation average = 126.361
	minimum = 0
	maximum = 810
Injected packet rate average = 0.0306802
	minimum = 0.0108 (at node 119)
	maximum = 0.0546 (at node 14)
Accepted packet rate average = 0.015924
	minimum = 0.0116 (at node 88)
	maximum = 0.0204 (at node 177)
Injected flit rate average = 0.552159
	minimum = 0.1944 (at node 119)
	maximum = 0.9852 (at node 14)
Accepted flit rate average= 0.289736
	minimum = 0.2182 (at node 88)
	maximum = 0.3754 (at node 177)
Injected packet length average = 17.9972
Accepted packet length average = 18.195
Total in-flight flits = 401669 (396043 measured)
latency change    = 0.230858
throughput change = 9.25769e-05
Class 0:
Packet latency average = 2709.96
	minimum = 23
	maximum = 6950
Network latency average = 2471.54
	minimum = 22
	maximum = 5939
Slowest packet = 17141
Flit latency average = 2534.94
	minimum = 5
	maximum = 7273
Slowest flit = 169971
Fragmentation average = 153.808
	minimum = 0
	maximum = 933
Injected packet rate average = 0.0303385
	minimum = 0.0113333 (at node 119)
	maximum = 0.0536667 (at node 14)
Accepted packet rate average = 0.015875
	minimum = 0.012 (at node 42)
	maximum = 0.0206667 (at node 177)
Injected flit rate average = 0.54607
	minimum = 0.204 (at node 119)
	maximum = 0.968 (at node 14)
Accepted flit rate average= 0.28867
	minimum = 0.2215 (at node 42)
	maximum = 0.3755 (at node 177)
Injected packet length average = 17.9992
Accepted packet length average = 18.1839
Total in-flight flits = 446250 (444305 measured)
latency change    = 0.164249
throughput change = 0.0036939
Class 0:
Packet latency average = 3100.57
	minimum = 23
	maximum = 7943
Network latency average = 2851.91
	minimum = 22
	maximum = 6838
Slowest packet = 17141
Flit latency average = 2761.23
	minimum = 5
	maximum = 7601
Slowest flit = 239273
Fragmentation average = 172.895
	minimum = 0
	maximum = 1279
Injected packet rate average = 0.0302277
	minimum = 0.0111429 (at node 119)
	maximum = 0.0528571 (at node 14)
Accepted packet rate average = 0.0159048
	minimum = 0.0121429 (at node 42)
	maximum = 0.0198571 (at node 138)
Injected flit rate average = 0.543947
	minimum = 0.200571 (at node 119)
	maximum = 0.951714 (at node 14)
Accepted flit rate average= 0.288624
	minimum = 0.223286 (at node 42)
	maximum = 0.370571 (at node 138)
Injected packet length average = 17.995
Accepted packet length average = 18.147
Total in-flight flits = 493110 (492581 measured)
latency change    = 0.125979
throughput change = 0.00016155
Draining all recorded packets ...
Class 0:
Remaining flits: 260140 260141 260142 260143 260144 260145 260146 260147 260148 260149 [...] (539926 flits)
Measured flits: 310368 310369 310370 310371 310372 310373 310948 310949 313512 313513 [...] (472784 flits)
Class 0:
Remaining flits: 268624 268625 268626 268627 268628 268629 268630 268631 303084 303085 [...] (581384 flits)
Measured flits: 315108 315109 315110 315111 315112 315113 315114 315115 315116 315117 [...] (429619 flits)
Class 0:
Remaining flits: 303093 303094 303095 303096 303097 303098 303099 303100 303101 315108 [...] (620243 flits)
Measured flits: 315108 315109 315110 315111 315112 315113 315114 315115 315116 315117 [...] (387386 flits)
Class 0:
Remaining flits: 366732 366733 366734 366735 366736 366737 366738 366739 366740 366741 [...] (649547 flits)
Measured flits: 366732 366733 366734 366735 366736 366737 366738 366739 366740 366741 [...] (342339 flits)
Class 0:
Remaining flits: 366740 366741 366742 366743 366744 366745 366746 366747 366748 366749 [...] (676730 flits)
Measured flits: 366740 366741 366742 366743 366744 366745 366746 366747 366748 366749 [...] (296314 flits)
Class 0:
Remaining flits: 396487 396488 396489 396490 396491 396492 396493 396494 396495 396496 [...] (703698 flits)
Measured flits: 396487 396488 396489 396490 396491 396492 396493 396494 396495 396496 [...] (251078 flits)
Class 0:
Remaining flits: 441126 441127 441128 441129 441130 441131 441132 441133 441134 441135 [...] (733225 flits)
Measured flits: 441126 441127 441128 441129 441130 441131 441132 441133 441134 441135 [...] (207699 flits)
Class 0:
Remaining flits: 479149 479150 479151 479152 479153 479154 479155 479156 479157 479158 [...] (749049 flits)
Measured flits: 479149 479150 479151 479152 479153 479154 479155 479156 479157 479158 [...] (168587 flits)
Class 0:
Remaining flits: 484758 484759 484760 484761 484762 484763 484764 484765 484766 484767 [...] (766800 flits)
Measured flits: 484758 484759 484760 484761 484762 484763 484764 484765 484766 484767 [...] (134376 flits)
Class 0:
Remaining flits: 484758 484759 484760 484761 484762 484763 484764 484765 484766 484767 [...] (785420 flits)
Measured flits: 484758 484759 484760 484761 484762 484763 484764 484765 484766 484767 [...] (103446 flits)
Class 0:
Remaining flits: 493641 493642 493643 493644 493645 493646 493647 493648 493649 522756 [...] (802521 flits)
Measured flits: 493641 493642 493643 493644 493645 493646 493647 493648 493649 522756 [...] (77594 flits)
Class 0:
Remaining flits: 552708 552709 552710 552711 552712 552713 552714 552715 552716 552717 [...] (812215 flits)
Measured flits: 552708 552709 552710 552711 552712 552713 552714 552715 552716 552717 [...] (56841 flits)
Class 0:
Remaining flits: 552708 552709 552710 552711 552712 552713 552714 552715 552716 552717 [...] (820156 flits)
Measured flits: 552708 552709 552710 552711 552712 552713 552714 552715 552716 552717 [...] (40747 flits)
Class 0:
Remaining flits: 573162 573163 573164 573165 573166 573167 573168 573169 573170 573171 [...] (826904 flits)
Measured flits: 573162 573163 573164 573165 573166 573167 573168 573169 573170 573171 [...] (28007 flits)
Class 0:
Remaining flits: 668538 668539 668540 668541 668542 668543 668544 668545 668546 668547 [...] (830998 flits)
Measured flits: 668538 668539 668540 668541 668542 668543 668544 668545 668546 668547 [...] (19496 flits)
Class 0:
Remaining flits: 703368 703369 703370 703371 703372 703373 703374 703375 703376 703377 [...] (834146 flits)
Measured flits: 703368 703369 703370 703371 703372 703373 703374 703375 703376 703377 [...] (13247 flits)
Class 0:
Remaining flits: 703368 703369 703370 703371 703372 703373 703374 703375 703376 703377 [...] (840390 flits)
Measured flits: 703368 703369 703370 703371 703372 703373 703374 703375 703376 703377 [...] (8924 flits)
Class 0:
Remaining flits: 769680 769681 769682 769683 769684 769685 769686 769687 769688 769689 [...] (849883 flits)
Measured flits: 769680 769681 769682 769683 769684 769685 769686 769687 769688 769689 [...] (6054 flits)
Class 0:
Remaining flits: 769680 769681 769682 769683 769684 769685 769686 769687 769688 769689 [...] (849943 flits)
Measured flits: 769680 769681 769682 769683 769684 769685 769686 769687 769688 769689 [...] (4033 flits)
Class 0:
Remaining flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (847668 flits)
Measured flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (2718 flits)
Class 0:
Remaining flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (849533 flits)
Measured flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (1579 flits)
Class 0:
Remaining flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (852425 flits)
Measured flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (1040 flits)
Class 0:
Remaining flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (851930 flits)
Measured flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (659 flits)
Class 0:
Remaining flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (854364 flits)
Measured flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (376 flits)
Class 0:
Remaining flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (857612 flits)
Measured flits: 906336 906337 906338 906339 906340 906341 906342 906343 906344 906345 [...] (216 flits)
Class 0:
Remaining flits: 982080 982081 982082 982083 982084 982085 982086 982087 982088 982089 [...] (857021 flits)
Measured flits: 982080 982081 982082 982083 982084 982085 982086 982087 982088 982089 [...] (127 flits)
Class 0:
Remaining flits: 1009260 1009261 1009262 1009263 1009264 1009265 1009266 1009267 1009268 1009269 [...] (859228 flits)
Measured flits: 1009260 1009261 1009262 1009263 1009264 1009265 1009266 1009267 1009268 1009269 [...] (54 flits)
Class 0:
Remaining flits: 1009260 1009261 1009262 1009263 1009264 1009265 1009266 1009267 1009268 1009269 [...] (859276 flits)
Measured flits: 1009260 1009261 1009262 1009263 1009264 1009265 1009266 1009267 1009268 1009269 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1193330 1193331 1193332 1193333 1193334 1193335 1193336 1193337 1193338 1193339 [...] (808996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1198008 1198009 1198010 1198011 1198012 1198013 1198014 1198015 1198016 1198017 [...] (758652 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355976 1355977 1355978 1355979 1355980 1355981 1355982 1355983 1355984 1355985 [...] (708692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355976 1355977 1355978 1355979 1355980 1355981 1355982 1355983 1355984 1355985 [...] (658398 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355985 1355986 1355987 1355988 1355989 1355990 1355991 1355992 1355993 1406700 [...] (608457 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1434564 1434565 1434566 1434567 1434568 1434569 1434570 1434571 1434572 1434573 [...] (558999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1434564 1434565 1434566 1434567 1434568 1434569 1434570 1434571 1434572 1434573 [...] (509852 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510974 1510975 1510976 1510977 1510978 1510979 1510980 1510981 1510982 1510983 [...] (461770 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1557828 1557829 1557830 1557831 1557832 1557833 1557834 1557835 1557836 1557837 [...] (413969 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1656540 1656541 1656542 1656543 1656544 1656545 1656546 1656547 1656548 1656549 [...] (366394 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692396 1692397 1692398 1692399 1692400 1692401 1692402 1692403 1692404 1692405 [...] (319119 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1829592 1829593 1829594 1829595 1829596 1829597 1829598 1829599 1829600 1829601 [...] (271825 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1829592 1829593 1829594 1829595 1829596 1829597 1829598 1829599 1829600 1829601 [...] (224301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1862694 1862695 1862696 1862697 1862698 1862699 1862700 1862701 1862702 1862703 [...] (176646 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1862694 1862695 1862696 1862697 1862698 1862699 1862700 1862701 1862702 1862703 [...] (128953 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1951064 1951065 1951066 1951067 1951068 1951069 1951070 1951071 1951072 1951073 [...] (81512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2159712 2159713 2159714 2159715 2159716 2159717 2159718 2159719 2159720 2159721 [...] (35764 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2370762 2370763 2370764 2370765 2370766 2370767 2370768 2370769 2370770 2370771 [...] (8062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2400712 2400713 2434140 2434141 2434142 2434143 2434144 2434145 2434146 2434147 [...] (599 flits)
Measured flits: (0 flits)
Time taken is 58215 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7354.38 (1 samples)
	minimum = 23 (1 samples)
	maximum = 29312 (1 samples)
Network latency average = 6939.36 (1 samples)
	minimum = 22 (1 samples)
	maximum = 29247 (1 samples)
Flit latency average = 11767.5 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34464 (1 samples)
Fragmentation average = 218.312 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1696 (1 samples)
Injected packet rate average = 0.0302277 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0528571 (1 samples)
Accepted packet rate average = 0.0159048 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0198571 (1 samples)
Injected flit rate average = 0.543947 (1 samples)
	minimum = 0.200571 (1 samples)
	maximum = 0.951714 (1 samples)
Accepted flit rate average = 0.288624 (1 samples)
	minimum = 0.223286 (1 samples)
	maximum = 0.370571 (1 samples)
Injected packet size average = 17.995 (1 samples)
Accepted packet size average = 18.147 (1 samples)
Hops average = 5.07466 (1 samples)
Total run time 106.398
