BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 284.928
	minimum = 27
	maximum = 953
Network latency average = 232.25
	minimum = 23
	maximum = 912
Slowest packet = 77
Flit latency average = 162.881
	minimum = 6
	maximum = 958
Slowest flit = 2828
Fragmentation average = 124.56
	minimum = 0
	maximum = 864
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00879167
	minimum = 0.003 (at node 174)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.176979
	minimum = 0.06 (at node 174)
	maximum = 0.315 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.1303
Total in-flight flits = 14170 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 391.176
	minimum = 27
	maximum = 1932
Network latency average = 334.314
	minimum = 23
	maximum = 1792
Slowest packet = 355
Flit latency average = 248.184
	minimum = 6
	maximum = 1889
Slowest flit = 6820
Fragmentation average = 158.049
	minimum = 0
	maximum = 1500
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00958594
	minimum = 0.004 (at node 135)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.183714
	minimum = 0.0885 (at node 135)
	maximum = 0.284 (at node 22)
Injected packet length average = 17.9095
Accepted packet length average = 19.1649
Total in-flight flits = 20534 (0 measured)
latency change    = 0.271611
throughput change = 0.0366569
Class 0:
Packet latency average = 613.024
	minimum = 27
	maximum = 2838
Network latency average = 551.267
	minimum = 25
	maximum = 2582
Slowest packet = 1018
Flit latency average = 452.635
	minimum = 6
	maximum = 2606
Slowest flit = 20589
Fragmentation average = 213.119
	minimum = 0
	maximum = 2037
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0107708
	minimum = 0.004 (at node 146)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.196094
	minimum = 0.052 (at node 146)
	maximum = 0.358 (at node 24)
Injected packet length average = 18.0569
Accepted packet length average = 18.206
Total in-flight flits = 27812 (0 measured)
latency change    = 0.361891
throughput change = 0.0631341
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 389.212
	minimum = 25
	maximum = 1103
Network latency average = 333.214
	minimum = 23
	maximum = 978
Slowest packet = 7597
Flit latency average = 588.045
	minimum = 6
	maximum = 3647
Slowest flit = 19658
Fragmentation average = 144.915
	minimum = 0
	maximum = 644
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0112656
	minimum = 0.004 (at node 138)
	maximum = 0.021 (at node 34)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.202672
	minimum = 0.06 (at node 138)
	maximum = 0.387 (at node 34)
Injected packet length average = 17.981
Accepted packet length average = 17.9903
Total in-flight flits = 35267 (26690 measured)
latency change    = 0.575041
throughput change = 0.032457
Class 0:
Packet latency average = 571.309
	minimum = 25
	maximum = 2033
Network latency average = 511.779
	minimum = 23
	maximum = 1946
Slowest packet = 7597
Flit latency average = 650.711
	minimum = 6
	maximum = 4685
Slowest flit = 17900
Fragmentation average = 168.384
	minimum = 0
	maximum = 1370
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.011263
	minimum = 0.0045 (at node 4)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.204536
	minimum = 0.089 (at node 4)
	maximum = 0.3305 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.16
Total in-flight flits = 43626 (39955 measured)
latency change    = 0.318737
throughput change = 0.00911614
Class 0:
Packet latency average = 714.543
	minimum = 25
	maximum = 2953
Network latency average = 651.446
	minimum = 23
	maximum = 2936
Slowest packet = 7667
Flit latency average = 718.813
	minimum = 6
	maximum = 5136
Slowest flit = 41029
Fragmentation average = 187.954
	minimum = 0
	maximum = 2065
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0113229
	minimum = 0.00766667 (at node 17)
	maximum = 0.0156667 (at node 0)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.204193
	minimum = 0.137333 (at node 36)
	maximum = 0.274667 (at node 44)
Injected packet length average = 17.9913
Accepted packet length average = 18.0336
Total in-flight flits = 46727 (45028 measured)
latency change    = 0.200456
throughput change = 0.00168346
Draining remaining packets ...
Class 0:
Remaining flits: 47499 47500 47501 51984 51985 51986 51987 51988 51989 51990 [...] (15169 flits)
Measured flits: 136854 136855 136856 136857 136858 136859 136860 136861 136862 136863 [...] (14544 flits)
Class 0:
Remaining flits: 121446 121447 121448 121449 121450 121451 121452 121453 121454 121455 [...] (742 flits)
Measured flits: 137715 137716 137717 142812 142813 142814 142815 142816 142817 142818 [...] (706 flits)
Time taken is 8473 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1113.95 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5223 (1 samples)
Network latency average = 1045.51 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5062 (1 samples)
Flit latency average = 1022.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6177 (1 samples)
Fragmentation average = 192.454 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2340 (1 samples)
Injected packet rate average = 0.0131684 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.0113229 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0156667 (1 samples)
Injected flit rate average = 0.236917 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.517 (1 samples)
Accepted flit rate average = 0.204193 (1 samples)
	minimum = 0.137333 (1 samples)
	maximum = 0.274667 (1 samples)
Injected packet size average = 17.9913 (1 samples)
Accepted packet size average = 18.0336 (1 samples)
Hops average = 5.07673 (1 samples)
Total run time 6.2966
