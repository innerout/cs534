BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.002487
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 44.0961
	minimum = 23
	maximum = 88
Network latency average = 43.9085
	minimum = 23
	maximum = 82
Slowest packet = 167
Flit latency average = 24.7574
	minimum = 6
	maximum = 65
Slowest flit = 3221
Fragmentation average = 4.07551
	minimum = 0
	maximum = 37
Injected packet rate average = 0.00238542
	minimum = 0 (at node 8)
	maximum = 0.007 (at node 124)
Accepted packet rate average = 0.00227604
	minimum = 0 (at node 11)
	maximum = 0.008 (at node 140)
Injected flit rate average = 0.0425625
	minimum = 0 (at node 8)
	maximum = 0.126 (at node 124)
Accepted flit rate average= 0.0413542
	minimum = 0 (at node 11)
	maximum = 0.144 (at node 140)
Injected packet length average = 17.8428
Accepted packet length average = 18.1693
Total in-flight flits = 304 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 44.8047
	minimum = 23
	maximum = 118
Network latency average = 44.5405
	minimum = 23
	maximum = 118
Slowest packet = 513
Flit latency average = 25.1986
	minimum = 6
	maximum = 101
Slowest flit = 9251
Fragmentation average = 4.36737
	minimum = 0
	maximum = 39
Injected packet rate average = 0.00238021
	minimum = 0 (at node 81)
	maximum = 0.0055 (at node 161)
Accepted packet rate average = 0.00234635
	minimum = 0.0005 (at node 35)
	maximum = 0.0055 (at node 140)
Injected flit rate average = 0.0427266
	minimum = 0 (at node 81)
	maximum = 0.099 (at node 161)
Accepted flit rate average= 0.0424401
	minimum = 0.009 (at node 41)
	maximum = 0.099 (at node 140)
Injected packet length average = 17.9508
Accepted packet length average = 18.0877
Total in-flight flits = 155 (0 measured)
latency change    = 0.0158142
throughput change = 0.0255875
Class 0:
Packet latency average = 45.3445
	minimum = 23
	maximum = 99
Network latency average = 44.8205
	minimum = 23
	maximum = 89
Slowest packet = 1337
Flit latency average = 25.2607
	minimum = 6
	maximum = 72
Slowest flit = 24083
Fragmentation average = 4.82881
	minimum = 0
	maximum = 34
Injected packet rate average = 0.00256771
	minimum = 0 (at node 0)
	maximum = 0.006 (at node 18)
Accepted packet rate average = 0.00249479
	minimum = 0 (at node 5)
	maximum = 0.007 (at node 22)
Injected flit rate average = 0.0460937
	minimum = 0 (at node 0)
	maximum = 0.108 (at node 18)
Accepted flit rate average= 0.0450104
	minimum = 0 (at node 5)
	maximum = 0.126 (at node 22)
Injected packet length average = 17.9513
Accepted packet length average = 18.0418
Total in-flight flits = 387 (0 measured)
latency change    = 0.0119046
throughput change = 0.0571048
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.7222
	minimum = 23
	maximum = 89
Network latency average = 45.406
	minimum = 23
	maximum = 89
Slowest packet = 1528
Flit latency average = 26.0287
	minimum = 6
	maximum = 72
Slowest flit = 27521
Fragmentation average = 4.80769
	minimum = 0
	maximum = 32
Injected packet rate average = 0.00255208
	minimum = 0 (at node 13)
	maximum = 0.008 (at node 120)
Accepted packet rate average = 0.00257813
	minimum = 0 (at node 32)
	maximum = 0.007 (at node 45)
Injected flit rate average = 0.045849
	minimum = 0 (at node 13)
	maximum = 0.148 (at node 177)
Accepted flit rate average= 0.0465781
	minimum = 0 (at node 32)
	maximum = 0.126 (at node 45)
Injected packet length average = 17.9653
Accepted packet length average = 18.0667
Total in-flight flits = 264 (264 measured)
latency change    = 0.00826195
throughput change = 0.0336576
Class 0:
Packet latency average = 45.5456
	minimum = 23
	maximum = 97
Network latency average = 45.2401
	minimum = 23
	maximum = 97
Slowest packet = 2074
Flit latency average = 25.8205
	minimum = 6
	maximum = 80
Slowest flit = 37338
Fragmentation average = 4.75456
	minimum = 0
	maximum = 34
Injected packet rate average = 0.00248177
	minimum = 0 (at node 134)
	maximum = 0.0065 (at node 120)
Accepted packet rate average = 0.0025
	minimum = 0 (at node 116)
	maximum = 0.006 (at node 65)
Injected flit rate average = 0.0446901
	minimum = 0 (at node 134)
	maximum = 0.117 (at node 120)
Accepted flit rate average= 0.0450208
	minimum = 0 (at node 116)
	maximum = 0.108 (at node 65)
Injected packet length average = 18.0073
Accepted packet length average = 18.0083
Total in-flight flits = 253 (253 measured)
latency change    = 0.00387898
throughput change = 0.0345905
Class 0:
Packet latency average = 45.6515
	minimum = 23
	maximum = 109
Network latency average = 45.33
	minimum = 23
	maximum = 109
Slowest packet = 2652
Flit latency average = 25.7677
	minimum = 6
	maximum = 92
Slowest flit = 47753
Fragmentation average = 4.87482
	minimum = 0
	maximum = 34
Injected packet rate average = 0.00246875
	minimum = 0.000333333 (at node 134)
	maximum = 0.00566667 (at node 55)
Accepted packet rate average = 0.00248785
	minimum = 0.000333333 (at node 116)
	maximum = 0.00566667 (at node 44)
Injected flit rate average = 0.0444896
	minimum = 0.006 (at node 134)
	maximum = 0.102 (at node 55)
Accepted flit rate average= 0.0448125
	minimum = 0.006 (at node 116)
	maximum = 0.101667 (at node 44)
Injected packet length average = 18.0211
Accepted packet length average = 18.0126
Total in-flight flits = 171 (171 measured)
latency change    = 0.00232066
throughput change = 0.004649
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6082 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 45.616 (1 samples)
	minimum = 23 (1 samples)
	maximum = 109 (1 samples)
Network latency average = 45.2876 (1 samples)
	minimum = 23 (1 samples)
	maximum = 109 (1 samples)
Flit latency average = 25.7339 (1 samples)
	minimum = 6 (1 samples)
	maximum = 92 (1 samples)
Fragmentation average = 4.85162 (1 samples)
	minimum = 0 (1 samples)
	maximum = 34 (1 samples)
Injected packet rate average = 0.00246875 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Accepted packet rate average = 0.00248785 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Injected flit rate average = 0.0444896 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.102 (1 samples)
Accepted flit rate average = 0.0448125 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.101667 (1 samples)
Injected packet size average = 18.0211 (1 samples)
Accepted packet size average = 18.0126 (1 samples)
Hops average = 5.06751 (1 samples)
Total run time 0.976196
