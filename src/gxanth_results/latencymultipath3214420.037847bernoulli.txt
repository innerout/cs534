BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.989
	minimum = 22
	maximum = 862
Network latency average = 286.145
	minimum = 22
	maximum = 811
Slowest packet = 907
Flit latency average = 253.848
	minimum = 5
	maximum = 818
Slowest flit = 19257
Fragmentation average = 70.9081
	minimum = 0
	maximum = 396
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 64)
	maximum = 0.024 (at node 88)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.283109
	minimum = 0.141 (at node 64)
	maximum = 0.451 (at node 22)
Injected packet length average = 17.8477
Accepted packet length average = 19.2824
Total in-flight flits = 74469 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 557.409
	minimum = 22
	maximum = 1675
Network latency average = 542.186
	minimum = 22
	maximum = 1637
Slowest packet = 2147
Flit latency average = 501.27
	minimum = 5
	maximum = 1693
Slowest flit = 36806
Fragmentation average = 97.1652
	minimum = 0
	maximum = 406
Injected packet rate average = 0.037651
	minimum = 0.0285 (at node 16)
	maximum = 0.0475 (at node 94)
Accepted packet rate average = 0.0155573
	minimum = 0.0095 (at node 135)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.675104
	minimum = 0.513 (at node 16)
	maximum = 0.855 (at node 94)
Accepted flit rate average= 0.291406
	minimum = 0.189 (at node 83)
	maximum = 0.4105 (at node 152)
Injected packet length average = 17.9306
Accepted packet length average = 18.7312
Total in-flight flits = 148344 (0 measured)
latency change    = 0.46361
throughput change = 0.0284718
Class 0:
Packet latency average = 1293.88
	minimum = 22
	maximum = 2438
Network latency average = 1275.1
	minimum = 22
	maximum = 2364
Slowest packet = 3143
Flit latency average = 1236.17
	minimum = 5
	maximum = 2416
Slowest flit = 71792
Fragmentation average = 141.37
	minimum = 0
	maximum = 544
Injected packet rate average = 0.0364583
	minimum = 0.012 (at node 120)
	maximum = 0.05 (at node 81)
Accepted packet rate average = 0.0162708
	minimum = 0.009 (at node 53)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.655635
	minimum = 0.218 (at node 120)
	maximum = 0.89 (at node 81)
Accepted flit rate average= 0.297026
	minimum = 0.148 (at node 134)
	maximum = 0.54 (at node 16)
Injected packet length average = 17.9831
Accepted packet length average = 18.2551
Total in-flight flits = 217423 (0 measured)
latency change    = 0.569197
throughput change = 0.0189202
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 124.034
	minimum = 23
	maximum = 1236
Network latency average = 56.6207
	minimum = 22
	maximum = 823
Slowest packet = 21503
Flit latency average = 1835.09
	minimum = 5
	maximum = 3134
Slowest flit = 109810
Fragmentation average = 6.14532
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0340312
	minimum = 0.004 (at node 44)
	maximum = 0.054 (at node 46)
Accepted packet rate average = 0.0156406
	minimum = 0.007 (at node 35)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.612604
	minimum = 0.072 (at node 44)
	maximum = 0.973 (at node 46)
Accepted flit rate average= 0.281536
	minimum = 0.121 (at node 105)
	maximum = 0.463 (at node 123)
Injected packet length average = 18.0012
Accepted packet length average = 18.0003
Total in-flight flits = 281070 (110323 measured)
latency change    = 9.43164
throughput change = 0.055018
Class 0:
Packet latency average = 264.43
	minimum = 23
	maximum = 2022
Network latency average = 134.253
	minimum = 22
	maximum = 1357
Slowest packet = 21503
Flit latency average = 2150.28
	minimum = 5
	maximum = 3875
Slowest flit = 131435
Fragmentation average = 6.45857
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0326406
	minimum = 0.0045 (at node 164)
	maximum = 0.048 (at node 103)
Accepted packet rate average = 0.0155781
	minimum = 0.009 (at node 4)
	maximum = 0.0225 (at node 9)
Injected flit rate average = 0.587786
	minimum = 0.0885 (at node 164)
	maximum = 0.864 (at node 103)
Accepted flit rate average= 0.278354
	minimum = 0.167 (at node 135)
	maximum = 0.38 (at node 9)
Injected packet length average = 18.0078
Accepted packet length average = 17.8683
Total in-flight flits = 336381 (213191 measured)
latency change    = 0.530936
throughput change = 0.0114325
Class 0:
Packet latency average = 539.302
	minimum = 22
	maximum = 3681
Network latency average = 350.396
	minimum = 22
	maximum = 2945
Slowest packet = 21540
Flit latency average = 2430.01
	minimum = 5
	maximum = 4808
Slowest flit = 151469
Fragmentation average = 8.09873
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0309653
	minimum = 0.009 (at node 180)
	maximum = 0.044 (at node 15)
Accepted packet rate average = 0.0155486
	minimum = 0.0103333 (at node 4)
	maximum = 0.0213333 (at node 182)
Injected flit rate average = 0.557491
	minimum = 0.16 (at node 180)
	maximum = 0.791333 (at node 15)
Accepted flit rate average= 0.276688
	minimum = 0.182333 (at node 4)
	maximum = 0.368333 (at node 129)
Injected packet length average = 18.0038
Accepted packet length average = 17.795
Total in-flight flits = 379567 (302926 measured)
latency change    = 0.509681
throughput change = 0.00602364
Class 0:
Packet latency average = 1511.55
	minimum = 22
	maximum = 4301
Network latency average = 1312.71
	minimum = 22
	maximum = 3977
Slowest packet = 21540
Flit latency average = 2711.67
	minimum = 5
	maximum = 5386
Slowest flit = 206287
Fragmentation average = 27.0166
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0282956
	minimum = 0.00825 (at node 132)
	maximum = 0.039 (at node 21)
Accepted packet rate average = 0.0153984
	minimum = 0.01025 (at node 4)
	maximum = 0.01925 (at node 103)
Injected flit rate average = 0.509326
	minimum = 0.15275 (at node 132)
	maximum = 0.70575 (at node 21)
Accepted flit rate average= 0.27466
	minimum = 0.18475 (at node 4)
	maximum = 0.3425 (at node 61)
Injected packet length average = 18.0002
Accepted packet length average = 17.8369
Total in-flight flits = 398506 (358371 measured)
latency change    = 0.643213
throughput change = 0.00738128
Class 0:
Packet latency average = 2597.4
	minimum = 22
	maximum = 5370
Network latency average = 2389.58
	minimum = 22
	maximum = 4981
Slowest packet = 21540
Flit latency average = 2998.12
	minimum = 5
	maximum = 6386
Slowest flit = 206888
Fragmentation average = 50.4841
	minimum = 0
	maximum = 612
Injected packet rate average = 0.0258937
	minimum = 0.0084 (at node 168)
	maximum = 0.0372 (at node 142)
Accepted packet rate average = 0.0152865
	minimum = 0.0112 (at node 4)
	maximum = 0.0192 (at node 157)
Injected flit rate average = 0.466196
	minimum = 0.1538 (at node 168)
	maximum = 0.669 (at node 118)
Accepted flit rate average= 0.272782
	minimum = 0.2008 (at node 4)
	maximum = 0.3408 (at node 129)
Injected packet length average = 18.0042
Accepted packet length average = 17.8447
Total in-flight flits = 404094 (386867 measured)
latency change    = 0.418051
throughput change = 0.00688411
Class 0:
Packet latency average = 3448.76
	minimum = 22
	maximum = 6317
Network latency average = 3217.82
	minimum = 22
	maximum = 5981
Slowest packet = 21540
Flit latency average = 3279.12
	minimum = 5
	maximum = 7167
Slowest flit = 227483
Fragmentation average = 59.978
	minimum = 0
	maximum = 774
Injected packet rate average = 0.0241814
	minimum = 0.00983333 (at node 84)
	maximum = 0.0345 (at node 21)
Accepted packet rate average = 0.0152405
	minimum = 0.0115 (at node 4)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.435452
	minimum = 0.179167 (at node 84)
	maximum = 0.6235 (at node 21)
Accepted flit rate average= 0.27188
	minimum = 0.199 (at node 80)
	maximum = 0.3365 (at node 70)
Injected packet length average = 18.0077
Accepted packet length average = 17.8394
Total in-flight flits = 406939 (400767 measured)
latency change    = 0.24686
throughput change = 0.00331794
Class 0:
Packet latency average = 4086.06
	minimum = 22
	maximum = 7295
Network latency average = 3804.47
	minimum = 22
	maximum = 6948
Slowest packet = 21540
Flit latency average = 3542.39
	minimum = 5
	maximum = 8028
Slowest flit = 255696
Fragmentation average = 63.2025
	minimum = 0
	maximum = 774
Injected packet rate average = 0.0229836
	minimum = 0.00957143 (at node 84)
	maximum = 0.032 (at node 142)
Accepted packet rate average = 0.0151801
	minimum = 0.012 (at node 80)
	maximum = 0.0194286 (at node 70)
Injected flit rate average = 0.413827
	minimum = 0.172429 (at node 84)
	maximum = 0.577571 (at node 142)
Accepted flit rate average= 0.271155
	minimum = 0.210143 (at node 80)
	maximum = 0.344143 (at node 70)
Injected packet length average = 18.0053
Accepted packet length average = 17.8626
Total in-flight flits = 410253 (408025 measured)
latency change    = 0.15597
throughput change = 0.0026754
Draining all recorded packets ...
Class 0:
Remaining flits: 333594 333595 333596 333597 333598 333599 333600 333601 333602 333603 [...] (410957 flits)
Measured flits: 387531 387532 387533 387534 387535 387536 387537 387538 387539 387990 [...] (410461 flits)
Class 0:
Remaining flits: 333594 333595 333596 333597 333598 333599 333600 333601 333602 333603 [...] (407779 flits)
Measured flits: 390060 390061 390062 390063 390064 390065 390066 390067 390068 390069 [...] (407545 flits)
Class 0:
Remaining flits: 354060 354061 354062 354063 354064 354065 354066 354067 354068 354069 [...] (410650 flits)
Measured flits: 397080 397081 397082 397083 397084 397085 397086 397087 397088 397089 [...] (410182 flits)
Class 0:
Remaining flits: 397080 397081 397082 397083 397084 397085 397086 397087 397088 397089 [...] (412485 flits)
Measured flits: 397080 397081 397082 397083 397084 397085 397086 397087 397088 397089 [...] (408597 flits)
Class 0:
Remaining flits: 397080 397081 397082 397083 397084 397085 397086 397087 397088 397089 [...] (412692 flits)
Measured flits: 397080 397081 397082 397083 397084 397085 397086 397087 397088 397089 [...] (400398 flits)
Class 0:
Remaining flits: 413649 413650 413651 413652 413653 413654 413655 413656 413657 447084 [...] (413531 flits)
Measured flits: 413649 413650 413651 413652 413653 413654 413655 413656 413657 447084 [...] (384959 flits)
Class 0:
Remaining flits: 478026 478027 478028 478029 478030 478031 478032 478033 478034 478035 [...] (415337 flits)
Measured flits: 478026 478027 478028 478029 478030 478031 478032 478033 478034 478035 [...] (359537 flits)
Class 0:
Remaining flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (415801 flits)
Measured flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (328112 flits)
Class 0:
Remaining flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (417189 flits)
Measured flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (291249 flits)
Class 0:
Remaining flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (417262 flits)
Measured flits: 505602 505603 505604 505605 505606 505607 505608 505609 505610 505611 [...] (250690 flits)
Class 0:
Remaining flits: 645768 645769 645770 645771 645772 645773 645774 645775 645776 645777 [...] (417752 flits)
Measured flits: 645768 645769 645770 645771 645772 645773 645774 645775 645776 645777 [...] (210357 flits)
Class 0:
Remaining flits: 645779 645780 645781 645782 645783 645784 645785 666972 666973 666974 [...] (416918 flits)
Measured flits: 645779 645780 645781 645782 645783 645784 645785 666972 666973 666974 [...] (169567 flits)
Class 0:
Remaining flits: 693288 693289 693290 693291 693292 693293 693294 693295 693296 693297 [...] (419358 flits)
Measured flits: 693288 693289 693290 693291 693292 693293 693294 693295 693296 693297 [...] (128317 flits)
Class 0:
Remaining flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (421085 flits)
Measured flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (92197 flits)
Class 0:
Remaining flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (419832 flits)
Measured flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (61551 flits)
Class 0:
Remaining flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (419830 flits)
Measured flits: 809442 809443 809444 809445 809446 809447 809448 809449 809450 809451 [...] (38567 flits)
Class 0:
Remaining flits: 909774 909775 909776 909777 909778 909779 909780 909781 909782 909783 [...] (420059 flits)
Measured flits: 909774 909775 909776 909777 909778 909779 909780 909781 909782 909783 [...] (21893 flits)
Class 0:
Remaining flits: 909774 909775 909776 909777 909778 909779 909780 909781 909782 909783 [...] (419948 flits)
Measured flits: 909774 909775 909776 909777 909778 909779 909780 909781 909782 909783 [...] (11732 flits)
Class 0:
Remaining flits: 1042236 1042237 1042238 1042239 1042240 1042241 1042242 1042243 1042244 1042245 [...] (422251 flits)
Measured flits: 1042236 1042237 1042238 1042239 1042240 1042241 1042242 1042243 1042244 1042245 [...] (6145 flits)
Class 0:
Remaining flits: 1110906 1110907 1110908 1110909 1110910 1110911 1110912 1110913 1110914 1110915 [...] (422422 flits)
Measured flits: 1110906 1110907 1110908 1110909 1110910 1110911 1110912 1110913 1110914 1110915 [...] (3009 flits)
Class 0:
Remaining flits: 1143276 1143277 1143278 1143279 1143280 1143281 1143282 1143283 1143284 1143285 [...] (423583 flits)
Measured flits: 1143276 1143277 1143278 1143279 1143280 1143281 1143282 1143283 1143284 1143285 [...] (1178 flits)
Class 0:
Remaining flits: 1167282 1167283 1167284 1167285 1167286 1167287 1167288 1167289 1167290 1167291 [...] (424736 flits)
Measured flits: 1167282 1167283 1167284 1167285 1167286 1167287 1167288 1167289 1167290 1167291 [...] (576 flits)
Class 0:
Remaining flits: 1251198 1251199 1251200 1251201 1251202 1251203 1251204 1251205 1251206 1251207 [...] (424503 flits)
Measured flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (360 flits)
Class 0:
Remaining flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (424256 flits)
Measured flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (162 flits)
Class 0:
Remaining flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (424903 flits)
Measured flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (126 flits)
Class 0:
Remaining flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (426319 flits)
Measured flits: 1258614 1258615 1258616 1258617 1258618 1258619 1258620 1258621 1258622 1258623 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1550826 1550827 1550828 1550829 1550830 1550831 1550832 1550833 1550834 1550835 [...] (375511 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1550826 1550827 1550828 1550829 1550830 1550831 1550832 1550833 1550834 1550835 [...] (324982 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1595142 1595143 1595144 1595145 1595146 1595147 1595148 1595149 1595150 1595151 [...] (275200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1641708 1641709 1641710 1641711 1641712 1641713 1641714 1641715 1641716 1641717 [...] (226352 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1763280 1763281 1763282 1763283 1763284 1763285 1763286 1763287 1763288 1763289 [...] (178154 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1823019 1823020 1823021 1853370 1853371 1853372 1853373 1853374 1853375 1853376 [...] (130825 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1869714 1869715 1869716 1869717 1869718 1869719 1869720 1869721 1869722 1869723 [...] (83562 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1869714 1869715 1869716 1869717 1869718 1869719 1869720 1869721 1869722 1869723 [...] (37136 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1952172 1952173 1952174 1952175 1952176 1952177 1952178 1952179 1952180 1952181 [...] (8450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2115630 2115631 2115632 2115633 2115634 2115635 2115636 2115637 2115638 2115639 [...] (897 flits)
Measured flits: (0 flits)
Time taken is 47526 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9827.57 (1 samples)
	minimum = 22 (1 samples)
	maximum = 27736 (1 samples)
Network latency average = 7149.96 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20023 (1 samples)
Flit latency average = 7218.59 (1 samples)
	minimum = 5 (1 samples)
	maximum = 19962 (1 samples)
Fragmentation average = 55.6272 (1 samples)
	minimum = 0 (1 samples)
	maximum = 899 (1 samples)
Injected packet rate average = 0.0229836 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.032 (1 samples)
Accepted packet rate average = 0.0151801 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0194286 (1 samples)
Injected flit rate average = 0.413827 (1 samples)
	minimum = 0.172429 (1 samples)
	maximum = 0.577571 (1 samples)
Accepted flit rate average = 0.271155 (1 samples)
	minimum = 0.210143 (1 samples)
	maximum = 0.344143 (1 samples)
Injected packet size average = 18.0053 (1 samples)
Accepted packet size average = 17.8626 (1 samples)
Hops average = 5.07802 (1 samples)
Total run time 68.4534
