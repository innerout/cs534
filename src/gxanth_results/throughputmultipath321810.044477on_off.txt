BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 386.986
	minimum = 23
	maximum = 988
Network latency average = 286.389
	minimum = 23
	maximum = 954
Slowest packet = 46
Flit latency average = 212.888
	minimum = 6
	maximum = 962
Slowest flit = 2872
Fragmentation average = 150.835
	minimum = 0
	maximum = 903
Injected packet rate average = 0.0193594
	minimum = 0 (at node 16)
	maximum = 0.044 (at node 19)
Accepted packet rate average = 0.00976042
	minimum = 0.003 (at node 41)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.342906
	minimum = 0 (at node 16)
	maximum = 0.792 (at node 19)
Accepted flit rate average= 0.20125
	minimum = 0.081 (at node 174)
	maximum = 0.353 (at node 88)
Injected packet length average = 17.7127
Accepted packet length average = 20.619
Total in-flight flits = 28392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 677.608
	minimum = 23
	maximum = 1953
Network latency average = 471.486
	minimum = 23
	maximum = 1870
Slowest packet = 160
Flit latency average = 378.339
	minimum = 6
	maximum = 1928
Slowest flit = 5230
Fragmentation average = 184.888
	minimum = 0
	maximum = 1467
Injected packet rate average = 0.0161068
	minimum = 0 (at node 60)
	maximum = 0.0335 (at node 19)
Accepted packet rate average = 0.0105078
	minimum = 0.0065 (at node 30)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.286615
	minimum = 0 (at node 60)
	maximum = 0.5985 (at node 19)
Accepted flit rate average= 0.20175
	minimum = 0.1235 (at node 79)
	maximum = 0.301 (at node 22)
Injected packet length average = 17.7947
Accepted packet length average = 19.2
Total in-flight flits = 34146 (0 measured)
latency change    = 0.428894
throughput change = 0.00247831
Class 0:
Packet latency average = 1594.51
	minimum = 40
	maximum = 2925
Network latency average = 889.963
	minimum = 26
	maximum = 2803
Slowest packet = 4559
Flit latency average = 761.665
	minimum = 6
	maximum = 2944
Slowest flit = 4180
Fragmentation average = 223.036
	minimum = 0
	maximum = 2285
Injected packet rate average = 0.0113177
	minimum = 0 (at node 0)
	maximum = 0.033 (at node 122)
Accepted packet rate average = 0.0111562
	minimum = 0.004 (at node 40)
	maximum = 0.019 (at node 6)
Injected flit rate average = 0.203771
	minimum = 0 (at node 67)
	maximum = 0.584 (at node 122)
Accepted flit rate average= 0.198229
	minimum = 0.066 (at node 184)
	maximum = 0.347 (at node 56)
Injected packet length average = 18.0046
Accepted packet length average = 17.7684
Total in-flight flits = 35182 (0 measured)
latency change    = 0.575037
throughput change = 0.0177614
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2155.59
	minimum = 47
	maximum = 3822
Network latency average = 352.297
	minimum = 27
	maximum = 965
Slowest packet = 8374
Flit latency average = 834.098
	minimum = 6
	maximum = 3696
Slowest flit = 8730
Fragmentation average = 128.303
	minimum = 1
	maximum = 428
Injected packet rate average = 0.0120208
	minimum = 0 (at node 5)
	maximum = 0.029 (at node 109)
Accepted packet rate average = 0.0110781
	minimum = 0.003 (at node 36)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.216063
	minimum = 0 (at node 71)
	maximum = 0.514 (at node 109)
Accepted flit rate average= 0.198328
	minimum = 0.072 (at node 36)
	maximum = 0.398 (at node 16)
Injected packet length average = 17.974
Accepted packet length average = 17.9027
Total in-flight flits = 38827 (28259 measured)
latency change    = 0.260291
throughput change = 0.000498963
Class 0:
Packet latency average = 2608.65
	minimum = 43
	maximum = 4818
Network latency average = 621.06
	minimum = 27
	maximum = 1968
Slowest packet = 8374
Flit latency average = 875.111
	minimum = 6
	maximum = 4630
Slowest flit = 8738
Fragmentation average = 161.006
	minimum = 1
	maximum = 1440
Injected packet rate average = 0.0111042
	minimum = 0.0005 (at node 108)
	maximum = 0.023 (at node 109)
Accepted packet rate average = 0.0108464
	minimum = 0.005 (at node 36)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.199424
	minimum = 0.0145 (at node 108)
	maximum = 0.4105 (at node 109)
Accepted flit rate average= 0.195552
	minimum = 0.1005 (at node 36)
	maximum = 0.325 (at node 16)
Injected packet length average = 17.9594
Accepted packet length average = 18.0293
Total in-flight flits = 36878 (34151 measured)
latency change    = 0.173676
throughput change = 0.0141959
Class 0:
Packet latency average = 3068.9
	minimum = 43
	maximum = 5686
Network latency average = 782.775
	minimum = 26
	maximum = 2836
Slowest packet = 8374
Flit latency average = 904.48
	minimum = 6
	maximum = 5317
Slowest flit = 8747
Fragmentation average = 176.941
	minimum = 0
	maximum = 1810
Injected packet rate average = 0.0109774
	minimum = 0.000333333 (at node 108)
	maximum = 0.021 (at node 109)
Accepted packet rate average = 0.0108073
	minimum = 0.00633333 (at node 36)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.197413
	minimum = 0.00966667 (at node 108)
	maximum = 0.373667 (at node 109)
Accepted flit rate average= 0.194637
	minimum = 0.127 (at node 36)
	maximum = 0.303 (at node 16)
Injected packet length average = 17.9836
Accepted packet length average = 18.0098
Total in-flight flits = 37101 (36586 measured)
latency change    = 0.149972
throughput change = 0.0047007
Draining remaining packets ...
Class 0:
Remaining flits: 60732 60733 60734 60735 60736 60737 60738 60739 60740 60741 [...] (5342 flits)
Measured flits: 154384 154385 156823 156824 156825 156826 156827 156828 156829 156830 [...] (5216 flits)
Time taken is 7579 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3557.16 (1 samples)
	minimum = 43 (1 samples)
	maximum = 7064 (1 samples)
Network latency average = 1013.75 (1 samples)
	minimum = 26 (1 samples)
	maximum = 3999 (1 samples)
Flit latency average = 1015.17 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6326 (1 samples)
Fragmentation average = 183.145 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2192 (1 samples)
Injected packet rate average = 0.0109774 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.021 (1 samples)
Accepted packet rate average = 0.0108073 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.197413 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.373667 (1 samples)
Accepted flit rate average = 0.194637 (1 samples)
	minimum = 0.127 (1 samples)
	maximum = 0.303 (1 samples)
Injected packet size average = 17.9836 (1 samples)
Accepted packet size average = 18.0098 (1 samples)
Hops average = 5.13828 (1 samples)
Total run time 7.55551
