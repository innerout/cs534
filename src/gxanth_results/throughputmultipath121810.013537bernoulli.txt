BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 176.731
	minimum = 23
	maximum = 851
Network latency average = 169.352
	minimum = 23
	maximum = 851
Slowest packet = 342
Flit latency average = 131.751
	minimum = 6
	maximum = 897
Slowest flit = 4234
Fragmentation average = 45.9725
	minimum = 0
	maximum = 124
Injected packet rate average = 0.012276
	minimum = 0.005 (at node 45)
	maximum = 0.022 (at node 113)
Accepted packet rate average = 0.00870833
	minimum = 0.002 (at node 93)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.218151
	minimum = 0.09 (at node 45)
	maximum = 0.382 (at node 113)
Accepted flit rate average= 0.163557
	minimum = 0.036 (at node 93)
	maximum = 0.305 (at node 165)
Injected packet length average = 17.7705
Accepted packet length average = 18.7817
Total in-flight flits = 11743 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 315.479
	minimum = 23
	maximum = 1456
Network latency average = 272.356
	minimum = 23
	maximum = 1430
Slowest packet = 541
Flit latency average = 227.83
	minimum = 6
	maximum = 1407
Slowest flit = 9754
Fragmentation average = 51.4392
	minimum = 0
	maximum = 143
Injected packet rate average = 0.011
	minimum = 0.005 (at node 40)
	maximum = 0.0165 (at node 109)
Accepted packet rate average = 0.00888802
	minimum = 0.0055 (at node 30)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.196688
	minimum = 0.084 (at node 40)
	maximum = 0.297 (at node 109)
Accepted flit rate average= 0.162964
	minimum = 0.099 (at node 30)
	maximum = 0.266 (at node 22)
Injected packet length average = 17.8807
Accepted packet length average = 18.3352
Total in-flight flits = 14894 (0 measured)
latency change    = 0.4398
throughput change = 0.00364345
Class 0:
Packet latency average = 725.099
	minimum = 23
	maximum = 2257
Network latency average = 452.754
	minimum = 23
	maximum = 1879
Slowest packet = 1352
Flit latency average = 397.197
	minimum = 6
	maximum = 1840
Slowest flit = 36144
Fragmentation average = 58.1094
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00914062
	minimum = 0 (at node 12)
	maximum = 0.022 (at node 59)
Accepted packet rate average = 0.00885417
	minimum = 0.001 (at node 116)
	maximum = 0.016 (at node 71)
Injected flit rate average = 0.163958
	minimum = 0 (at node 12)
	maximum = 0.395 (at node 120)
Accepted flit rate average= 0.159818
	minimum = 0.018 (at node 116)
	maximum = 0.305 (at node 71)
Injected packet length average = 17.9373
Accepted packet length average = 18.05
Total in-flight flits = 16123 (0 measured)
latency change    = 0.564916
throughput change = 0.0196839
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 941.48
	minimum = 31
	maximum = 2678
Network latency average = 319.759
	minimum = 23
	maximum = 907
Slowest packet = 6164
Flit latency average = 454.494
	minimum = 6
	maximum = 2640
Slowest flit = 47951
Fragmentation average = 55.5054
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0090625
	minimum = 0 (at node 188)
	maximum = 0.019 (at node 141)
Accepted packet rate average = 0.00881771
	minimum = 0.001 (at node 184)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.162286
	minimum = 0 (at node 188)
	maximum = 0.328 (at node 141)
Accepted flit rate average= 0.158099
	minimum = 0.002 (at node 184)
	maximum = 0.333 (at node 56)
Injected packet length average = 17.9075
Accepted packet length average = 17.9297
Total in-flight flits = 16926 (15115 measured)
latency change    = 0.229831
throughput change = 0.0108714
Class 0:
Packet latency average = 1237.05
	minimum = 23
	maximum = 3472
Network latency average = 430.795
	minimum = 23
	maximum = 1877
Slowest packet = 6164
Flit latency average = 470.079
	minimum = 6
	maximum = 2817
Slowest flit = 65933
Fragmentation average = 56.502
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00904688
	minimum = 0.0005 (at node 188)
	maximum = 0.018 (at node 141)
Accepted packet rate average = 0.00890365
	minimum = 0.004 (at node 85)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.16288
	minimum = 0.009 (at node 188)
	maximum = 0.319 (at node 141)
Accepted flit rate average= 0.160294
	minimum = 0.065 (at node 163)
	maximum = 0.288 (at node 34)
Injected packet length average = 18.004
Accepted packet length average = 18.0032
Total in-flight flits = 17156 (17069 measured)
latency change    = 0.238931
throughput change = 0.0136955
Class 0:
Packet latency average = 1454.01
	minimum = 23
	maximum = 4226
Network latency average = 467.35
	minimum = 23
	maximum = 2648
Slowest packet = 6164
Flit latency average = 466.978
	minimum = 6
	maximum = 2817
Slowest flit = 65933
Fragmentation average = 56.5781
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00905208
	minimum = 0.00166667 (at node 188)
	maximum = 0.0186667 (at node 141)
Accepted packet rate average = 0.00893576
	minimum = 0.00566667 (at node 17)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.162642
	minimum = 0.03 (at node 188)
	maximum = 0.336 (at node 141)
Accepted flit rate average= 0.160795
	minimum = 0.0996667 (at node 163)
	maximum = 0.264 (at node 16)
Injected packet length average = 17.9674
Accepted packet length average = 17.9946
Total in-flight flits = 17411 (17411 measured)
latency change    = 0.149214
throughput change = 0.00311495
Draining remaining packets ...
Time taken is 6937 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1596.78 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4702 (1 samples)
Network latency average = 508.362 (1 samples)
	minimum = 23 (1 samples)
	maximum = 2721 (1 samples)
Flit latency average = 492.388 (1 samples)
	minimum = 6 (1 samples)
	maximum = 2817 (1 samples)
Fragmentation average = 55.2833 (1 samples)
	minimum = 0 (1 samples)
	maximum = 139 (1 samples)
Injected packet rate average = 0.00905208 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.0186667 (1 samples)
Accepted packet rate average = 0.00893576 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0146667 (1 samples)
Injected flit rate average = 0.162642 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.336 (1 samples)
Accepted flit rate average = 0.160795 (1 samples)
	minimum = 0.0996667 (1 samples)
	maximum = 0.264 (1 samples)
Injected packet size average = 17.9674 (1 samples)
Accepted packet size average = 17.9946 (1 samples)
Hops average = 5.07859 (1 samples)
Total run time 4.4301
