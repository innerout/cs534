BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.782
	minimum = 23
	maximum = 967
Network latency average = 318.871
	minimum = 23
	maximum = 954
Slowest packet = 155
Flit latency average = 243.779
	minimum = 6
	maximum = 938
Slowest flit = 4873
Fragmentation average = 162.634
	minimum = 0
	maximum = 903
Injected packet rate average = 0.0219115
	minimum = 0.009 (at node 124)
	maximum = 0.033 (at node 170)
Accepted packet rate average = 0.010125
	minimum = 0.003 (at node 98)
	maximum = 0.018 (at node 104)
Injected flit rate average = 0.386823
	minimum = 0.152 (at node 124)
	maximum = 0.577 (at node 170)
Accepted flit rate average= 0.207839
	minimum = 0.089 (at node 98)
	maximum = 0.335 (at node 104)
Injected packet length average = 17.6539
Accepted packet length average = 20.5273
Total in-flight flits = 36037 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 665.329
	minimum = 23
	maximum = 1871
Network latency average = 548.022
	minimum = 23
	maximum = 1871
Slowest packet = 310
Flit latency average = 449.659
	minimum = 6
	maximum = 1922
Slowest flit = 7202
Fragmentation average = 195.278
	minimum = 0
	maximum = 1794
Injected packet rate average = 0.0164036
	minimum = 0.006 (at node 100)
	maximum = 0.026 (at node 166)
Accepted packet rate average = 0.0105964
	minimum = 0.006 (at node 120)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.291721
	minimum = 0.1075 (at node 100)
	maximum = 0.461 (at node 166)
Accepted flit rate average= 0.202438
	minimum = 0.1235 (at node 153)
	maximum = 0.288 (at node 152)
Injected packet length average = 17.7839
Accepted packet length average = 19.1044
Total in-flight flits = 36024 (0 measured)
latency change    = 0.480283
throughput change = 0.02668
Class 0:
Packet latency average = 1627.51
	minimum = 203
	maximum = 2649
Network latency average = 941.711
	minimum = 25
	maximum = 2648
Slowest packet = 1310
Flit latency average = 809.935
	minimum = 6
	maximum = 2759
Slowest flit = 17419
Fragmentation average = 207.217
	minimum = 0
	maximum = 1973
Injected packet rate average = 0.0110052
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 61)
Accepted packet rate average = 0.0109635
	minimum = 0.004 (at node 101)
	maximum = 0.022 (at node 159)
Injected flit rate average = 0.198266
	minimum = 0 (at node 2)
	maximum = 0.564 (at node 61)
Accepted flit rate average= 0.197078
	minimum = 0.088 (at node 101)
	maximum = 0.388 (at node 159)
Injected packet length average = 18.0156
Accepted packet length average = 17.9758
Total in-flight flits = 36201 (0 measured)
latency change    = 0.591199
throughput change = 0.0271942
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2229.41
	minimum = 933
	maximum = 3289
Network latency average = 350.037
	minimum = 23
	maximum = 930
Slowest packet = 8434
Flit latency average = 871.933
	minimum = 6
	maximum = 3790
Slowest flit = 18377
Fragmentation average = 126.002
	minimum = 0
	maximum = 578
Injected packet rate average = 0.0110156
	minimum = 0 (at node 10)
	maximum = 0.034 (at node 9)
Accepted packet rate average = 0.0109219
	minimum = 0.004 (at node 89)
	maximum = 0.021 (at node 78)
Injected flit rate average = 0.198172
	minimum = 0 (at node 22)
	maximum = 0.614 (at node 9)
Accepted flit rate average= 0.195786
	minimum = 0.075 (at node 89)
	maximum = 0.418 (at node 128)
Injected packet length average = 17.9901
Accepted packet length average = 17.9261
Total in-flight flits = 36770 (24757 measured)
latency change    = 0.269982
throughput change = 0.00659732
Class 0:
Packet latency average = 2689.58
	minimum = 933
	maximum = 4204
Network latency average = 596.159
	minimum = 23
	maximum = 1943
Slowest packet = 8434
Flit latency average = 904.361
	minimum = 6
	maximum = 4544
Slowest flit = 21959
Fragmentation average = 157.677
	minimum = 0
	maximum = 976
Injected packet rate average = 0.0109479
	minimum = 0.001 (at node 183)
	maximum = 0.0235 (at node 9)
Accepted packet rate average = 0.0108411
	minimum = 0.006 (at node 36)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.197232
	minimum = 0.014 (at node 183)
	maximum = 0.4255 (at node 9)
Accepted flit rate average= 0.194937
	minimum = 0.101 (at node 36)
	maximum = 0.344 (at node 128)
Injected packet length average = 18.0155
Accepted packet length average = 17.9813
Total in-flight flits = 37269 (33710 measured)
latency change    = 0.171092
throughput change = 0.00435503
Class 0:
Packet latency average = 3100.62
	minimum = 933
	maximum = 5193
Network latency average = 743.694
	minimum = 23
	maximum = 2800
Slowest packet = 8434
Flit latency average = 918.797
	minimum = 6
	maximum = 5374
Slowest flit = 52565
Fragmentation average = 170.63
	minimum = 0
	maximum = 2303
Injected packet rate average = 0.0107413
	minimum = 0.00166667 (at node 52)
	maximum = 0.0233333 (at node 9)
Accepted packet rate average = 0.0107569
	minimum = 0.006 (at node 36)
	maximum = 0.0173333 (at node 128)
Injected flit rate average = 0.193444
	minimum = 0.0283333 (at node 148)
	maximum = 0.417333 (at node 9)
Accepted flit rate average= 0.19326
	minimum = 0.106667 (at node 36)
	maximum = 0.321667 (at node 128)
Injected packet length average = 18.0094
Accepted packet length average = 17.9661
Total in-flight flits = 36393 (35270 measured)
latency change    = 0.132566
throughput change = 0.00867784
Class 0:
Packet latency average = 3490.49
	minimum = 933
	maximum = 5872
Network latency average = 824.762
	minimum = 23
	maximum = 3797
Slowest packet = 8434
Flit latency average = 913.328
	minimum = 6
	maximum = 6250
Slowest flit = 31427
Fragmentation average = 180.019
	minimum = 0
	maximum = 2574
Injected packet rate average = 0.0109271
	minimum = 0.00275 (at node 52)
	maximum = 0.01975 (at node 149)
Accepted packet rate average = 0.010776
	minimum = 0.0065 (at node 116)
	maximum = 0.01725 (at node 128)
Injected flit rate average = 0.196628
	minimum = 0.04875 (at node 52)
	maximum = 0.35575 (at node 149)
Accepted flit rate average= 0.194134
	minimum = 0.11525 (at node 116)
	maximum = 0.318 (at node 128)
Injected packet length average = 17.9945
Accepted packet length average = 18.0153
Total in-flight flits = 38180 (37751 measured)
latency change    = 0.111696
throughput change = 0.00450049
Class 0:
Packet latency average = 3869.84
	minimum = 933
	maximum = 6855
Network latency average = 879.689
	minimum = 23
	maximum = 4693
Slowest packet = 8434
Flit latency average = 919.886
	minimum = 6
	maximum = 6425
Slowest flit = 82830
Fragmentation average = 186.762
	minimum = 0
	maximum = 3109
Injected packet rate average = 0.0108458
	minimum = 0.0028 (at node 52)
	maximum = 0.0184 (at node 46)
Accepted packet rate average = 0.0108073
	minimum = 0.0068 (at node 36)
	maximum = 0.0168 (at node 128)
Injected flit rate average = 0.195267
	minimum = 0.0504 (at node 52)
	maximum = 0.3308 (at node 46)
Accepted flit rate average= 0.193973
	minimum = 0.1196 (at node 36)
	maximum = 0.308 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 17.9483
Total in-flight flits = 37565 (37465 measured)
latency change    = 0.0980277
throughput change = 0.000831033
Class 0:
Packet latency average = 4222.57
	minimum = 933
	maximum = 7503
Network latency average = 917.994
	minimum = 23
	maximum = 5221
Slowest packet = 8434
Flit latency average = 923.822
	minimum = 6
	maximum = 7616
Slowest flit = 70234
Fragmentation average = 189.142
	minimum = 0
	maximum = 3109
Injected packet rate average = 0.0107231
	minimum = 0.00283333 (at node 16)
	maximum = 0.0173333 (at node 9)
Accepted packet rate average = 0.010763
	minimum = 0.008 (at node 116)
	maximum = 0.0158333 (at node 128)
Injected flit rate average = 0.19301
	minimum = 0.052 (at node 52)
	maximum = 0.311 (at node 9)
Accepted flit rate average= 0.193463
	minimum = 0.1395 (at node 116)
	maximum = 0.286167 (at node 128)
Injected packet length average = 17.9995
Accepted packet length average = 17.9748
Total in-flight flits = 35812 (35767 measured)
latency change    = 0.083534
throughput change = 0.00263742
Class 0:
Packet latency average = 4570.57
	minimum = 933
	maximum = 8498
Network latency average = 948.155
	minimum = 23
	maximum = 6041
Slowest packet = 8434
Flit latency average = 926.273
	minimum = 6
	maximum = 8220
Slowest flit = 70235
Fragmentation average = 192.108
	minimum = 0
	maximum = 3272
Injected packet rate average = 0.0107046
	minimum = 0.004 (at node 52)
	maximum = 0.0168571 (at node 9)
Accepted packet rate average = 0.0107277
	minimum = 0.00814286 (at node 79)
	maximum = 0.0152857 (at node 128)
Injected flit rate average = 0.19266
	minimum = 0.072 (at node 52)
	maximum = 0.304286 (at node 9)
Accepted flit rate average= 0.192966
	minimum = 0.146286 (at node 144)
	maximum = 0.278714 (at node 128)
Injected packet length average = 17.9978
Accepted packet length average = 17.9877
Total in-flight flits = 35839 (35834 measured)
latency change    = 0.0761394
throughput change = 0.00257507
Draining all recorded packets ...
Class 0:
Remaining flits: 177903 177904 177905 177906 177907 177908 177909 177910 177911 199332 [...] (35364 flits)
Measured flits: 177903 177904 177905 177906 177907 177908 177909 177910 177911 199332 [...] (35364 flits)
Class 0:
Remaining flits: 199348 199349 246292 246293 268852 268853 268854 268855 268856 268857 [...] (34735 flits)
Measured flits: 199348 199349 246292 246293 268852 268853 268854 268855 268856 268857 [...] (34735 flits)
Class 0:
Remaining flits: 268864 268865 286990 286991 289944 289945 289946 289947 289948 289949 [...] (35558 flits)
Measured flits: 268864 268865 286990 286991 289944 289945 289946 289947 289948 289949 [...] (35558 flits)
Class 0:
Remaining flits: 289944 289945 289946 289947 289948 289949 289950 289951 289952 289953 [...] (36719 flits)
Measured flits: 289944 289945 289946 289947 289948 289949 289950 289951 289952 289953 [...] (36719 flits)
Class 0:
Remaining flits: 289946 289947 289948 289949 289950 289951 289952 289953 289954 289955 [...] (35865 flits)
Measured flits: 289946 289947 289948 289949 289950 289951 289952 289953 289954 289955 [...] (35865 flits)
Class 0:
Remaining flits: 420587 426690 426691 426692 426693 426694 426695 426696 426697 426698 [...] (37220 flits)
Measured flits: 420587 426690 426691 426692 426693 426694 426695 426696 426697 426698 [...] (37220 flits)
Class 0:
Remaining flits: 451242 451243 451244 451245 451246 451247 451248 451249 451250 451251 [...] (35533 flits)
Measured flits: 451242 451243 451244 451245 451246 451247 451248 451249 451250 451251 [...] (35533 flits)
Class 0:
Remaining flits: 454439 454440 454441 454442 454443 454444 454445 463050 463051 463052 [...] (37067 flits)
Measured flits: 454439 454440 454441 454442 454443 454444 454445 463050 463051 463052 [...] (37067 flits)
Class 0:
Remaining flits: 479415 479416 479417 479418 479419 479420 479421 479422 479423 479424 [...] (35732 flits)
Measured flits: 479415 479416 479417 479418 479419 479420 479421 479422 479423 479424 [...] (35732 flits)
Class 0:
Remaining flits: 479418 479419 479420 479421 479422 479423 479424 479425 479426 479427 [...] (36844 flits)
Measured flits: 479418 479419 479420 479421 479422 479423 479424 479425 479426 479427 [...] (36646 flits)
Class 0:
Remaining flits: 531341 577018 577019 577020 577021 577022 577023 577024 577025 589148 [...] (37979 flits)
Measured flits: 531341 577018 577019 577020 577021 577022 577023 577024 577025 589148 [...] (37530 flits)
Class 0:
Remaining flits: 607840 607841 614943 614944 614945 614946 614947 614948 614949 614950 [...] (37937 flits)
Measured flits: 607840 607841 614943 614944 614945 614946 614947 614948 614949 614950 [...] (37150 flits)
Class 0:
Remaining flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (36557 flits)
Measured flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (35150 flits)
Class 0:
Remaining flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (36021 flits)
Measured flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (33337 flits)
Class 0:
Remaining flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (35910 flits)
Measured flits: 623988 623989 623990 623991 623992 623993 623994 623995 623996 623997 [...] (30915 flits)
Class 0:
Remaining flits: 716922 716923 716924 716925 716926 716927 716928 716929 716930 716931 [...] (37957 flits)
Measured flits: 716922 716923 716924 716925 716926 716927 716928 716929 716930 716931 [...] (29789 flits)
Class 0:
Remaining flits: 803448 803449 803450 803451 803452 803453 803454 803455 803456 803457 [...] (35699 flits)
Measured flits: 803448 803449 803450 803451 803452 803453 803454 803455 803456 803457 [...] (25130 flits)
Class 0:
Remaining flits: 825822 825823 825824 825825 825826 825827 825828 825829 825830 825831 [...] (35056 flits)
Measured flits: 825822 825823 825824 825825 825826 825827 825828 825829 825830 825831 [...] (22234 flits)
Class 0:
Remaining flits: 825824 825825 825826 825827 825828 825829 825830 825831 825832 825833 [...] (35967 flits)
Measured flits: 825824 825825 825826 825827 825828 825829 825830 825831 825832 825833 [...] (20337 flits)
Class 0:
Remaining flits: 825827 825828 825829 825830 825831 825832 825833 825834 825835 825836 [...] (36517 flits)
Measured flits: 825827 825828 825829 825830 825831 825832 825833 825834 825835 825836 [...] (17586 flits)
Class 0:
Remaining flits: 894512 894513 894514 894515 894516 894517 894518 894519 894520 894521 [...] (37678 flits)
Measured flits: 894512 894513 894514 894515 894516 894517 894518 894519 894520 894521 [...] (15346 flits)
Class 0:
Remaining flits: 913790 913791 913792 913793 913794 913795 913796 913797 913798 913799 [...] (37370 flits)
Measured flits: 913790 913791 913792 913793 913794 913795 913796 913797 913798 913799 [...] (12651 flits)
Class 0:
Remaining flits: 928800 928801 928802 928803 928804 928805 928806 928807 928808 928809 [...] (36989 flits)
Measured flits: 928800 928801 928802 928803 928804 928805 928806 928807 928808 928809 [...] (10339 flits)
Class 0:
Remaining flits: 939150 939151 939152 939153 939154 939155 939156 939157 939158 939159 [...] (37068 flits)
Measured flits: 976410 976411 976412 976413 976414 976415 976416 976417 976418 976419 [...] (7611 flits)
Class 0:
Remaining flits: 939150 939151 939152 939153 939154 939155 939156 939157 939158 939159 [...] (34199 flits)
Measured flits: 976410 976411 976412 976413 976414 976415 976416 976417 976418 976419 [...] (5958 flits)
Class 0:
Remaining flits: 976427 1032678 1032679 1032680 1032681 1032682 1032683 1032684 1032685 1032686 [...] (35153 flits)
Measured flits: 976427 1032678 1032679 1032680 1032681 1032682 1032683 1032684 1032685 1032686 [...] (4971 flits)
Class 0:
Remaining flits: 1117188 1117189 1117190 1117191 1117192 1117193 1117194 1117195 1117196 1117197 [...] (36663 flits)
Measured flits: 1117188 1117189 1117190 1117191 1117192 1117193 1117194 1117195 1117196 1117197 [...] (4502 flits)
Class 0:
Remaining flits: 1117191 1117192 1117193 1117194 1117195 1117196 1117197 1117198 1117199 1117200 [...] (33585 flits)
Measured flits: 1117191 1117192 1117193 1117194 1117195 1117196 1117197 1117198 1117199 1117200 [...] (3069 flits)
Class 0:
Remaining flits: 1117205 1198944 1198945 1198946 1198947 1198948 1198949 1198950 1198951 1198952 [...] (34837 flits)
Measured flits: 1117205 1198944 1198945 1198946 1198947 1198948 1198949 1198950 1198951 1198952 [...] (3054 flits)
Class 0:
Remaining flits: 1224681 1224682 1224683 1232188 1232189 1264998 1264999 1265000 1265001 1265002 [...] (36512 flits)
Measured flits: 1224681 1224682 1224683 1264998 1264999 1265000 1265001 1265002 1265003 1309266 [...] (2682 flits)
Class 0:
Remaining flits: 1308730 1308731 1308732 1308733 1308734 1308735 1308736 1308737 1308738 1308739 [...] (33904 flits)
Measured flits: 1352934 1352935 1352936 1352937 1352938 1352939 1352940 1352941 1352942 1352943 [...] (2076 flits)
Class 0:
Remaining flits: 1309986 1309987 1309988 1309989 1309990 1309991 1309992 1309993 1309994 1309995 [...] (34048 flits)
Measured flits: 1352934 1352935 1352936 1352937 1352938 1352939 1352940 1352941 1352942 1352943 [...] (1887 flits)
Class 0:
Remaining flits: 1352949 1352950 1352951 1368776 1368777 1368778 1368779 1368780 1368781 1368782 [...] (35272 flits)
Measured flits: 1352949 1352950 1352951 1368776 1368777 1368778 1368779 1368780 1368781 1368782 [...] (1387 flits)
Class 0:
Remaining flits: 1411848 1411849 1411850 1411851 1411852 1411853 1411854 1411855 1411856 1411857 [...] (35845 flits)
Measured flits: 1448915 1448916 1448917 1448918 1448919 1448920 1448921 1448922 1448923 1448924 [...] (1289 flits)
Class 0:
Remaining flits: 1411855 1411856 1411857 1411858 1411859 1411860 1411861 1411862 1411863 1411864 [...] (36832 flits)
Measured flits: 1576170 1576171 1576172 1576173 1576174 1576175 1576176 1576177 1576178 1576179 [...] (890 flits)
Class 0:
Remaining flits: 1428085 1428086 1428087 1428088 1428089 1428090 1428091 1428092 1428093 1428094 [...] (37542 flits)
Measured flits: 1576170 1576171 1576172 1576173 1576174 1576175 1576176 1576177 1576178 1576179 [...] (762 flits)
Class 0:
Remaining flits: 1527120 1527121 1527122 1527123 1527124 1527125 1527126 1527127 1527128 1527129 [...] (35202 flits)
Measured flits: 1632781 1632782 1632783 1632784 1632785 1632786 1632787 1632788 1632789 1632790 [...] (427 flits)
Class 0:
Remaining flits: 1560403 1560404 1560405 1560406 1560407 1560408 1560409 1560410 1560411 1560412 [...] (35288 flits)
Measured flits: 1632781 1632782 1632783 1632784 1632785 1632786 1632787 1632788 1632789 1632790 [...] (192 flits)
Class 0:
Remaining flits: 1581521 1581522 1581523 1581524 1581525 1581526 1581527 1581528 1581529 1581530 [...] (36366 flits)
Measured flits: 1632790 1632791 1632792 1632793 1632794 1632795 1632796 1632797 1737536 1737537 [...] (156 flits)
Class 0:
Remaining flits: 1641078 1641079 1641080 1641081 1641082 1641083 1641084 1641085 1641086 1641087 [...] (36348 flits)
Measured flits: 1763706 1763707 1763708 1763709 1763710 1763711 1847808 1847809 1847810 1847811 [...] (88 flits)
Class 0:
Remaining flits: 1641080 1641081 1641082 1641083 1641084 1641085 1641086 1641087 1641088 1641089 [...] (37019 flits)
Measured flits: 1847815 1847816 1847817 1847818 1847819 1847820 1847821 1847822 1847823 1847824 [...] (189 flits)
Class 0:
Remaining flits: 1646602 1646603 1650715 1650716 1650717 1650718 1650719 1650720 1650721 1650722 [...] (34997 flits)
Measured flits: 1847823 1847824 1847825 1918368 1918369 1918370 1918371 1918372 1918373 1918374 [...] (85 flits)
Class 0:
Remaining flits: 1698391 1698392 1698393 1698394 1698395 1698396 1698397 1698398 1698399 1698400 [...] (35556 flits)
Measured flits: 1918378 1918379 1918380 1918381 1918382 1918383 1918384 1918385 1926180 1926181 [...] (168 flits)
Class 0:
Remaining flits: 1759464 1759465 1759466 1759467 1759468 1759469 1759470 1759471 1759472 1759473 [...] (38013 flits)
Measured flits: 1983378 1983379 1983380 1983381 1983382 1983383 1983969 1983970 1983971 1983972 [...] (52 flits)
Class 0:
Remaining flits: 1775298 1775299 1775300 1775301 1775302 1775303 1805094 1805095 1805096 1805097 [...] (37022 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1879360 1879361 1880208 1880209 1880210 1880211 1880212 1880213 1880214 1880215 [...] (37896 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1879360 1879361 1881396 1881397 1881398 1881399 1881400 1881401 1881402 1881403 [...] (35786 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1930266 1930267 1930268 1930269 1930270 1930271 1930272 1930273 1930274 1930275 [...] (36719 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1936602 1936603 1936604 1936605 1936606 1936607 1936608 1936609 1936610 1936611 [...] (36159 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1966068 1966069 1966070 1966071 1966072 1966073 1966074 1966075 1966076 1966077 [...] (38674 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1995372 1995373 1995374 1995375 1995376 1995377 1995378 1995379 1995380 1995381 [...] (36678 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1995372 1995373 1995374 1995375 1995376 1995377 1995378 1995379 1995380 1995381 [...] (36320 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1995372 1995373 1995374 1995375 1995376 1995377 1995378 1995379 1995380 1995381 [...] (38103 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 1995376 1995377 1995378 1995379 1995380 1995381 1995382 1995383 1995384 1995385 [...] (37937 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (36240 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (37327 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Class 0:
Remaining flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (35197 flits)
Measured flits: 2005272 2005273 2005274 2005275 2005276 2005277 2005278 2005279 2005280 2005281 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2215206 2215207 2215208 2215209 2215210 2215211 2215212 2215213 2215214 2215215 [...] (8021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2249640 2249641 2249642 2249643 2249644 2249645 2249646 2249647 2249648 2249649 [...] (627 flits)
Measured flits: (0 flits)
Time taken is 69522 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12707.4 (1 samples)
	minimum = 933 (1 samples)
	maximum = 57421 (1 samples)
Network latency average = 1067.42 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13768 (1 samples)
Flit latency average = 950.423 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13751 (1 samples)
Fragmentation average = 206.716 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5006 (1 samples)
Injected packet rate average = 0.0107046 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.0168571 (1 samples)
Accepted packet rate average = 0.0107277 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0152857 (1 samples)
Injected flit rate average = 0.19266 (1 samples)
	minimum = 0.072 (1 samples)
	maximum = 0.304286 (1 samples)
Accepted flit rate average = 0.192966 (1 samples)
	minimum = 0.146286 (1 samples)
	maximum = 0.278714 (1 samples)
Injected packet size average = 17.9978 (1 samples)
Accepted packet size average = 17.9877 (1 samples)
Hops average = 5.05611 (1 samples)
Total run time 73.9935
