BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 331.906
	minimum = 22
	maximum = 920
Network latency average = 301.772
	minimum = 22
	maximum = 866
Slowest packet = 656
Flit latency average = 271.412
	minimum = 5
	maximum = 854
Slowest flit = 18718
Fragmentation average = 86.0849
	minimum = 0
	maximum = 714
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0147865
	minimum = 0.004 (at node 174)
	maximum = 0.026 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.295792
	minimum = 0.128 (at node 174)
	maximum = 0.481 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 20.0042
Total in-flight flits = 116314 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 620.658
	minimum = 22
	maximum = 1772
Network latency average = 570.836
	minimum = 22
	maximum = 1703
Slowest packet = 656
Flit latency average = 529.754
	minimum = 5
	maximum = 1714
Slowest flit = 42386
Fragmentation average = 145.988
	minimum = 0
	maximum = 876
Injected packet rate average = 0.0514271
	minimum = 0.0405 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0158958
	minimum = 0.01 (at node 116)
	maximum = 0.0225 (at node 70)
Injected flit rate average = 0.921885
	minimum = 0.723 (at node 188)
	maximum = 0.997 (at node 91)
Accepted flit rate average= 0.307594
	minimum = 0.2 (at node 135)
	maximum = 0.416 (at node 154)
Injected packet length average = 17.9261
Accepted packet length average = 19.3506
Total in-flight flits = 237348 (0 measured)
latency change    = 0.465235
throughput change = 0.0383691
Class 0:
Packet latency average = 1456.77
	minimum = 31
	maximum = 2604
Network latency average = 1360.38
	minimum = 22
	maximum = 2541
Slowest packet = 3185
Flit latency average = 1318.79
	minimum = 5
	maximum = 2541
Slowest flit = 72952
Fragmentation average = 266.205
	minimum = 0
	maximum = 925
Injected packet rate average = 0.0479219
	minimum = 0.018 (at node 12)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0169375
	minimum = 0.007 (at node 153)
	maximum = 0.024 (at node 25)
Injected flit rate average = 0.861984
	minimum = 0.331 (at node 12)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.312958
	minimum = 0.159 (at node 11)
	maximum = 0.484 (at node 8)
Injected packet length average = 17.9873
Accepted packet length average = 18.4772
Total in-flight flits = 342878 (0 measured)
latency change    = 0.57395
throughput change = 0.0171415
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 349.401
	minimum = 23
	maximum = 1539
Network latency average = 68.4884
	minimum = 23
	maximum = 590
Slowest packet = 28951
Flit latency average = 1904.27
	minimum = 5
	maximum = 3509
Slowest flit = 79278
Fragmentation average = 7.53775
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0472917
	minimum = 0.016 (at node 76)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0166875
	minimum = 0.008 (at node 158)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.851917
	minimum = 0.295 (at node 76)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.310062
	minimum = 0.142 (at node 96)
	maximum = 0.486 (at node 123)
Injected packet length average = 18.0141
Accepted packet length average = 18.5805
Total in-flight flits = 446786 (151608 measured)
latency change    = 3.16935
throughput change = 0.00933951
Class 0:
Packet latency average = 486.447
	minimum = 23
	maximum = 2298
Network latency average = 144.576
	minimum = 23
	maximum = 1257
Slowest packet = 28951
Flit latency average = 2231.8
	minimum = 5
	maximum = 4429
Slowest flit = 89763
Fragmentation average = 7.27646
	minimum = 0
	maximum = 50
Injected packet rate average = 0.047362
	minimum = 0.02 (at node 36)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0164714
	minimum = 0.009 (at node 96)
	maximum = 0.0235 (at node 46)
Injected flit rate average = 0.852576
	minimum = 0.365 (at node 36)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.302849
	minimum = 0.1625 (at node 96)
	maximum = 0.434 (at node 78)
Injected packet length average = 18.0013
Accepted packet length average = 18.3864
Total in-flight flits = 553950 (305286 measured)
latency change    = 0.281729
throughput change = 0.0238189
Class 0:
Packet latency average = 647.973
	minimum = 23
	maximum = 2751
Network latency average = 246.264
	minimum = 22
	maximum = 1679
Slowest packet = 28951
Flit latency average = 2573.31
	minimum = 5
	maximum = 5276
Slowest flit = 120808
Fragmentation average = 7.32524
	minimum = 0
	maximum = 50
Injected packet rate average = 0.0474618
	minimum = 0.0196667 (at node 88)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0164635
	minimum = 0.0106667 (at node 158)
	maximum = 0.0216667 (at node 24)
Injected flit rate average = 0.854332
	minimum = 0.358667 (at node 88)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.300969
	minimum = 0.226333 (at node 130)
	maximum = 0.390667 (at node 123)
Injected packet length average = 18.0004
Accepted packet length average = 18.2809
Total in-flight flits = 661604 (460063 measured)
latency change    = 0.249279
throughput change = 0.00624719
Class 0:
Packet latency average = 819.73
	minimum = 23
	maximum = 4704
Network latency average = 353.376
	minimum = 22
	maximum = 3891
Slowest packet = 29778
Flit latency average = 2930.05
	minimum = 5
	maximum = 6122
Slowest flit = 128627
Fragmentation average = 8.52564
	minimum = 0
	maximum = 653
Injected packet rate average = 0.0460833
	minimum = 0.02025 (at node 88)
	maximum = 0.05575 (at node 6)
Accepted packet rate average = 0.0164167
	minimum = 0.01275 (at node 130)
	maximum = 0.02075 (at node 128)
Injected flit rate average = 0.82966
	minimum = 0.36675 (at node 88)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.298448
	minimum = 0.232 (at node 96)
	maximum = 0.376 (at node 128)
Injected packet length average = 18.0035
Accepted packet length average = 18.1796
Total in-flight flits = 750834 (596267 measured)
latency change    = 0.209529
throughput change = 0.00844648
Class 0:
Packet latency average = 1057.61
	minimum = 23
	maximum = 5895
Network latency average = 536.675
	minimum = 22
	maximum = 4979
Slowest packet = 29778
Flit latency average = 3304.83
	minimum = 5
	maximum = 6793
Slowest flit = 203542
Fragmentation average = 19.5506
	minimum = 0
	maximum = 669
Injected packet rate average = 0.0420229
	minimum = 0.0204 (at node 128)
	maximum = 0.054 (at node 123)
Accepted packet rate average = 0.0161375
	minimum = 0.0122 (at node 96)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.756731
	minimum = 0.3682 (at node 128)
	maximum = 0.9692 (at node 123)
Accepted flit rate average= 0.293206
	minimum = 0.2246 (at node 96)
	maximum = 0.3624 (at node 128)
Injected packet length average = 18.0076
Accepted packet length average = 18.1692
Total in-flight flits = 787862 (678352 measured)
latency change    = 0.224924
throughput change = 0.0178771
Class 0:
Packet latency average = 1614
	minimum = 23
	maximum = 6914
Network latency average = 1032.67
	minimum = 22
	maximum = 5962
Slowest packet = 29778
Flit latency average = 3671.87
	minimum = 5
	maximum = 7519
Slowest flit = 255004
Fragmentation average = 53.1518
	minimum = 0
	maximum = 748
Injected packet rate average = 0.0380998
	minimum = 0.0176667 (at node 124)
	maximum = 0.0481667 (at node 6)
Accepted packet rate average = 0.0159826
	minimum = 0.012 (at node 80)
	maximum = 0.0196667 (at node 128)
Injected flit rate average = 0.686015
	minimum = 0.316167 (at node 124)
	maximum = 0.867333 (at node 95)
Accepted flit rate average= 0.289808
	minimum = 0.213 (at node 80)
	maximum = 0.358333 (at node 11)
Injected packet length average = 18.0057
Accepted packet length average = 18.1327
Total in-flight flits = 799381 (730927 measured)
latency change    = 0.344727
throughput change = 0.0117253
Class 0:
Packet latency average = 2511.1
	minimum = 23
	maximum = 7837
Network latency average = 1863.88
	minimum = 22
	maximum = 6983
Slowest packet = 29778
Flit latency average = 4024.56
	minimum = 5
	maximum = 8248
Slowest flit = 304466
Fragmentation average = 108.626
	minimum = 0
	maximum = 885
Injected packet rate average = 0.0350193
	minimum = 0.0172857 (at node 128)
	maximum = 0.0437143 (at node 123)
Accepted packet rate average = 0.0158452
	minimum = 0.012 (at node 80)
	maximum = 0.019 (at node 157)
Injected flit rate average = 0.630655
	minimum = 0.312143 (at node 128)
	maximum = 0.787571 (at node 165)
Accepted flit rate average= 0.286918
	minimum = 0.216429 (at node 80)
	maximum = 0.349286 (at node 11)
Injected packet length average = 18.0088
Accepted packet length average = 18.1075
Total in-flight flits = 804826 (770696 measured)
latency change    = 0.357253
throughput change = 0.0100726
Draining all recorded packets ...
Class 0:
Remaining flits: 407517 407518 407519 415510 415511 423206 423207 423208 423209 423210 [...] (810429 flits)
Measured flits: 521154 521155 521156 521157 521158 521159 521160 521161 521162 521163 [...] (801161 flits)
Class 0:
Remaining flits: 430827 430828 430829 442143 442144 442145 442146 442147 442148 442149 [...] (811418 flits)
Measured flits: 521160 521161 521162 521163 521164 521165 521166 521167 521168 521169 [...] (810397 flits)
Class 0:
Remaining flits: 472152 472153 472154 472155 472156 472157 478242 478243 478244 478245 [...] (809293 flits)
Measured flits: 521313 521314 521315 523578 523579 523580 523581 523582 523583 523800 [...] (809108 flits)
Class 0:
Remaining flits: 478242 478243 478244 478245 478246 478247 478248 478249 478250 478251 [...] (807572 flits)
Measured flits: 526948 526949 532345 532346 532347 532348 532349 535895 538236 538237 [...] (807153 flits)
Class 0:
Remaining flits: 492688 492689 492690 492691 492692 492693 492694 492695 544833 544834 [...] (814645 flits)
Measured flits: 544833 544834 544835 544836 544837 544838 544839 544840 544841 561784 [...] (813125 flits)
Class 0:
Remaining flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (819994 flits)
Measured flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (814054 flits)
Class 0:
Remaining flits: 569409 569410 569411 599562 599563 599564 599565 599566 599567 599568 [...] (825401 flits)
Measured flits: 569409 569410 569411 599562 599563 599564 599565 599566 599567 599568 [...] (808365 flits)
Class 0:
Remaining flits: 617328 617329 617330 617331 617332 617333 617334 617335 617336 617337 [...] (827992 flits)
Measured flits: 617328 617329 617330 617331 617332 617333 617334 617335 617336 617337 [...] (790879 flits)
Class 0:
Remaining flits: 617343 617344 617345 621357 621358 621359 628272 628273 628274 628275 [...] (830211 flits)
Measured flits: 617343 617344 617345 621357 621358 621359 628272 628273 628274 628275 [...] (764170 flits)
Class 0:
Remaining flits: 628272 628273 628274 628275 628276 628277 628278 628279 628280 628281 [...] (827855 flits)
Measured flits: 628272 628273 628274 628275 628276 628277 628278 628279 628280 628281 [...] (731110 flits)
Class 0:
Remaining flits: 676548 676549 676550 676551 676552 676553 676554 676555 676556 676557 [...] (826748 flits)
Measured flits: 676548 676549 676550 676551 676552 676553 676554 676555 676556 676557 [...] (695159 flits)
Class 0:
Remaining flits: 676559 676560 676561 676562 676563 676564 676565 679482 679483 679484 [...] (836333 flits)
Measured flits: 676559 676560 676561 676562 676563 676564 676565 679482 679483 679484 [...] (658705 flits)
Class 0:
Remaining flits: 736020 736021 736022 736023 736024 736025 736026 736027 736028 736029 [...] (838387 flits)
Measured flits: 736020 736021 736022 736023 736024 736025 736026 736027 736028 736029 [...] (620963 flits)
Class 0:
Remaining flits: 736020 736021 736022 736023 736024 736025 736026 736027 736028 736029 [...] (841303 flits)
Measured flits: 736020 736021 736022 736023 736024 736025 736026 736027 736028 736029 [...] (582974 flits)
Class 0:
Remaining flits: 784246 784247 784248 784249 784250 784251 784252 784253 784254 784255 [...] (842633 flits)
Measured flits: 784246 784247 784248 784249 784250 784251 784252 784253 784254 784255 [...] (545255 flits)
Class 0:
Remaining flits: 803358 803359 803360 803361 803362 803363 803364 803365 803366 803367 [...] (840286 flits)
Measured flits: 803358 803359 803360 803361 803362 803363 803364 803365 803366 803367 [...] (505590 flits)
Class 0:
Remaining flits: 803366 803367 803368 803369 803370 803371 803372 803373 803374 803375 [...] (836490 flits)
Measured flits: 803366 803367 803368 803369 803370 803371 803372 803373 803374 803375 [...] (466811 flits)
Class 0:
Remaining flits: 862884 862885 862886 862887 862888 862889 862890 862891 862892 862893 [...] (839733 flits)
Measured flits: 862884 862885 862886 862887 862888 862889 862890 862891 862892 862893 [...] (427530 flits)
Class 0:
Remaining flits: 862892 862893 862894 862895 862896 862897 862898 862899 862900 862901 [...] (839147 flits)
Measured flits: 862892 862893 862894 862895 862896 862897 862898 862899 862900 862901 [...] (386624 flits)
Class 0:
Remaining flits: 906480 906481 906482 906483 906484 906485 906486 906487 906488 906489 [...] (843751 flits)
Measured flits: 906480 906481 906482 906483 906484 906485 906486 906487 906488 906489 [...] (343769 flits)
Class 0:
Remaining flits: 906480 906481 906482 906483 906484 906485 906486 906487 906488 906489 [...] (846063 flits)
Measured flits: 906480 906481 906482 906483 906484 906485 906486 906487 906488 906489 [...] (301119 flits)
Class 0:
Remaining flits: 906496 906497 914364 914365 914366 914367 914368 914369 914370 914371 [...] (845822 flits)
Measured flits: 906496 906497 914364 914365 914366 914367 914368 914369 914370 914371 [...] (257827 flits)
Class 0:
Remaining flits: 914364 914365 914366 914367 914368 914369 914370 914371 914372 914373 [...] (843997 flits)
Measured flits: 914364 914365 914366 914367 914368 914369 914370 914371 914372 914373 [...] (215748 flits)
Class 0:
Remaining flits: 1059786 1059787 1059788 1059789 1059790 1059791 1059792 1059793 1059794 1059795 [...] (839831 flits)
Measured flits: 1059786 1059787 1059788 1059789 1059790 1059791 1059792 1059793 1059794 1059795 [...] (176272 flits)
Class 0:
Remaining flits: 1059788 1059789 1059790 1059791 1059792 1059793 1059794 1059795 1059796 1059797 [...] (844705 flits)
Measured flits: 1059788 1059789 1059790 1059791 1059792 1059793 1059794 1059795 1059796 1059797 [...] (140293 flits)
Class 0:
Remaining flits: 1089427 1089428 1089429 1089430 1089431 1133622 1133623 1133624 1133625 1133626 [...] (846442 flits)
Measured flits: 1089427 1089428 1089429 1089430 1089431 1133622 1133623 1133624 1133625 1133626 [...] (107616 flits)
Class 0:
Remaining flits: 1166472 1166473 1166474 1166475 1166476 1166477 1166478 1166479 1166480 1166481 [...] (847717 flits)
Measured flits: 1166472 1166473 1166474 1166475 1166476 1166477 1166478 1166479 1166480 1166481 [...] (80483 flits)
Class 0:
Remaining flits: 1166472 1166473 1166474 1166475 1166476 1166477 1166478 1166479 1166480 1166481 [...] (852446 flits)
Measured flits: 1166472 1166473 1166474 1166475 1166476 1166477 1166478 1166479 1166480 1166481 [...] (57594 flits)
Class 0:
Remaining flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (850747 flits)
Measured flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (39642 flits)
Class 0:
Remaining flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (847897 flits)
Measured flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (26974 flits)
Class 0:
Remaining flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (847014 flits)
Measured flits: 1301688 1301689 1301690 1301691 1301692 1301693 1301694 1301695 1301696 1301697 [...] (17661 flits)
Class 0:
Remaining flits: 1309518 1309519 1309520 1309521 1309522 1309523 1309524 1309525 1309526 1309527 [...] (851911 flits)
Measured flits: 1309518 1309519 1309520 1309521 1309522 1309523 1309524 1309525 1309526 1309527 [...] (11358 flits)
Class 0:
Remaining flits: 1451934 1451935 1451936 1451937 1451938 1451939 1451940 1451941 1451942 1451943 [...] (854539 flits)
Measured flits: 1451934 1451935 1451936 1451937 1451938 1451939 1451940 1451941 1451942 1451943 [...] (7227 flits)
Class 0:
Remaining flits: 1451934 1451935 1451936 1451937 1451938 1451939 1451940 1451941 1451942 1451943 [...] (857140 flits)
Measured flits: 1451934 1451935 1451936 1451937 1451938 1451939 1451940 1451941 1451942 1451943 [...] (4394 flits)
Class 0:
Remaining flits: 1461906 1461907 1461908 1461909 1461910 1461911 1461912 1461913 1461914 1461915 [...] (857408 flits)
Measured flits: 1461906 1461907 1461908 1461909 1461910 1461911 1461912 1461913 1461914 1461915 [...] (2470 flits)
Class 0:
Remaining flits: 1461906 1461907 1461908 1461909 1461910 1461911 1461912 1461913 1461914 1461915 [...] (859765 flits)
Measured flits: 1461906 1461907 1461908 1461909 1461910 1461911 1461912 1461913 1461914 1461915 [...] (1646 flits)
Class 0:
Remaining flits: 1670418 1670419 1670420 1670421 1670422 1670423 1670424 1670425 1670426 1670427 [...] (858398 flits)
Measured flits: 1670418 1670419 1670420 1670421 1670422 1670423 1670424 1670425 1670426 1670427 [...] (783 flits)
Class 0:
Remaining flits: 1670418 1670419 1670420 1670421 1670422 1670423 1670424 1670425 1670426 1670427 [...] (859720 flits)
Measured flits: 1670418 1670419 1670420 1670421 1670422 1670423 1670424 1670425 1670426 1670427 [...] (431 flits)
Class 0:
Remaining flits: 1710666 1710667 1710668 1710669 1710670 1710671 1710672 1710673 1710674 1710675 [...] (868140 flits)
Measured flits: 1710666 1710667 1710668 1710669 1710670 1710671 1710672 1710673 1710674 1710675 [...] (253 flits)
Class 0:
Remaining flits: 1710666 1710667 1710668 1710669 1710670 1710671 1710672 1710673 1710674 1710675 [...] (868352 flits)
Measured flits: 1710666 1710667 1710668 1710669 1710670 1710671 1710672 1710673 1710674 1710675 [...] (144 flits)
Class 0:
Remaining flits: 1858068 1858069 1858070 1858071 1858072 1858073 1858074 1858075 1858076 1858077 [...] (869814 flits)
Measured flits: 2815560 2815561 2815562 2815563 2815564 2815565 2815566 2815567 2815568 2815569 [...] (54 flits)
Class 0:
Remaining flits: 1858068 1858069 1858070 1858071 1858072 1858073 1858074 1858075 1858076 1858077 [...] (873884 flits)
Measured flits: 2815560 2815561 2815562 2815563 2815564 2815565 2815566 2815567 2815568 2815569 [...] (18 flits)
Class 0:
Remaining flits: 1858068 1858069 1858070 1858071 1858072 1858073 1858074 1858075 1858076 1858077 [...] (872517 flits)
Measured flits: 2815560 2815561 2815562 2815563 2815564 2815565 2815566 2815567 2815568 2815569 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (816063 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (765827 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (715013 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (664334 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (613826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2040408 2040409 2040410 2040411 2040412 2040413 2040414 2040415 2040416 2040417 [...] (563750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2387646 2387647 2387648 2387649 2387650 2387651 2387652 2387653 2387654 2387655 [...] (513807 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2387646 2387647 2387648 2387649 2387650 2387651 2387652 2387653 2387654 2387655 [...] (465326 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2387646 2387647 2387648 2387649 2387650 2387651 2387652 2387653 2387654 2387655 [...] (417654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2387646 2387647 2387648 2387649 2387650 2387651 2387652 2387653 2387654 2387655 [...] (369822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2530026 2530027 2530028 2530029 2530030 2530031 2530032 2530033 2530034 2530035 [...] (322335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2530026 2530027 2530028 2530029 2530030 2530031 2530032 2530033 2530034 2530035 [...] (274600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2530026 2530027 2530028 2530029 2530030 2530031 2530032 2530033 2530034 2530035 [...] (226902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2550366 2550367 2550368 2550369 2550370 2550371 2550372 2550373 2550374 2550375 [...] (178955 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2665152 2665153 2665154 2665155 2665156 2665157 2665158 2665159 2665160 2665161 [...] (131197 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2904390 2904391 2904392 2904393 2904394 2904395 2904396 2904397 2904398 2904399 [...] (83350 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3097224 3097225 3097226 3097227 3097228 3097229 3097230 3097231 3097232 3097233 [...] (36431 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3097238 3097239 3097240 3097241 3205440 3205441 3205442 3205443 3205444 3205445 [...] (6564 flits)
Measured flits: (0 flits)
Time taken is 72668 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16879.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 43963 (1 samples)
Network latency average = 13189.5 (1 samples)
	minimum = 22 (1 samples)
	maximum = 34185 (1 samples)
Flit latency average = 14010.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 37612 (1 samples)
Fragmentation average = 246.653 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1647 (1 samples)
Injected packet rate average = 0.0350193 (1 samples)
	minimum = 0.0172857 (1 samples)
	maximum = 0.0437143 (1 samples)
Accepted packet rate average = 0.0158452 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.630655 (1 samples)
	minimum = 0.312143 (1 samples)
	maximum = 0.787571 (1 samples)
Accepted flit rate average = 0.286918 (1 samples)
	minimum = 0.216429 (1 samples)
	maximum = 0.349286 (1 samples)
Injected packet size average = 18.0088 (1 samples)
Accepted packet size average = 18.1075 (1 samples)
Hops average = 5.06149 (1 samples)
Total run time 156.554
