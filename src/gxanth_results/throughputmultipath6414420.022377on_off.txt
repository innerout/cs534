BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 273.123
	minimum = 22
	maximum = 939
Network latency average = 195.614
	minimum = 22
	maximum = 747
Slowest packet = 33
Flit latency average = 166.286
	minimum = 5
	maximum = 747
Slowest flit = 16512
Fragmentation average = 35.7056
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0127396
	minimum = 0.003 (at node 174)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.237979
	minimum = 0.081 (at node 174)
	maximum = 0.395 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6803
Total in-flight flits = 27082 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 441.869
	minimum = 22
	maximum = 1738
Network latency average = 337.603
	minimum = 22
	maximum = 1429
Slowest packet = 33
Flit latency average = 306.943
	minimum = 5
	maximum = 1451
Slowest flit = 36176
Fragmentation average = 49.6617
	minimum = 0
	maximum = 627
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0136875
	minimum = 0.008 (at node 81)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253125
	minimum = 0.144 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 18.4932
Total in-flight flits = 53208 (0 measured)
latency change    = 0.381891
throughput change = 0.0598354
Class 0:
Packet latency average = 959.866
	minimum = 23
	maximum = 2718
Network latency average = 800.503
	minimum = 22
	maximum = 2174
Slowest packet = 2009
Flit latency average = 757.773
	minimum = 5
	maximum = 2201
Slowest flit = 56593
Fragmentation average = 77.5604
	minimum = 0
	maximum = 698
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0153177
	minimum = 0.005 (at node 135)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.274719
	minimum = 0.098 (at node 135)
	maximum = 0.499 (at node 16)
Injected packet length average = 18
Accepted packet length average = 17.9347
Total in-flight flits = 78366 (0 measured)
latency change    = 0.539655
throughput change = 0.0786031
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 359.092
	minimum = 22
	maximum = 1304
Network latency average = 229.33
	minimum = 22
	maximum = 940
Slowest packet = 12702
Flit latency average = 1057.34
	minimum = 5
	maximum = 2775
Slowest flit = 88857
Fragmentation average = 18.476
	minimum = 0
	maximum = 214
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0153385
	minimum = 0.007 (at node 179)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.275286
	minimum = 0.126 (at node 179)
	maximum = 0.475 (at node 16)
Injected packet length average = 17.9946
Accepted packet length average = 17.9474
Total in-flight flits = 101777 (67246 measured)
latency change    = 1.67304
throughput change = 0.00206225
Class 0:
Packet latency average = 841.806
	minimum = 22
	maximum = 2149
Network latency average = 723.491
	minimum = 22
	maximum = 1958
Slowest packet = 12702
Flit latency average = 1205.31
	minimum = 5
	maximum = 3615
Slowest flit = 101634
Fragmentation average = 34.6582
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0153099
	minimum = 0.0085 (at node 22)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.275779
	minimum = 0.163 (at node 22)
	maximum = 0.423 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0131
Total in-flight flits = 125377 (114700 measured)
latency change    = 0.573427
throughput change = 0.00178472
Class 0:
Packet latency average = 1246.39
	minimum = 22
	maximum = 3231
Network latency average = 1125.67
	minimum = 22
	maximum = 2930
Slowest packet = 12702
Flit latency average = 1360.69
	minimum = 5
	maximum = 4268
Slowest flit = 125832
Fragmentation average = 51.0658
	minimum = 0
	maximum = 536
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0152431
	minimum = 0.0103333 (at node 146)
	maximum = 0.0213333 (at node 138)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.27583
	minimum = 0.183333 (at node 146)
	maximum = 0.394 (at node 138)
Injected packet length average = 18.0055
Accepted packet length average = 18.0954
Total in-flight flits = 146540 (144793 measured)
latency change    = 0.324604
throughput change = 0.000185677
Draining remaining packets ...
Class 0:
Remaining flits: 158305 158306 158307 158308 158309 179931 179932 179933 179934 179935 [...] (99622 flits)
Measured flits: 228444 228445 228446 228447 228448 228449 228450 228451 228452 228453 [...] (99396 flits)
Class 0:
Remaining flits: 200614 200615 200616 200617 200618 200619 200620 200621 200622 200623 [...] (53883 flits)
Measured flits: 228837 228838 228839 228840 228841 228842 228843 228844 228845 228846 [...] (53847 flits)
Class 0:
Remaining flits: 203128 203129 280548 280549 280550 280551 280552 280553 280554 280555 [...] (18049 flits)
Measured flits: 280548 280549 280550 280551 280552 280553 280554 280555 280556 280557 [...] (18047 flits)
Class 0:
Remaining flits: 295506 295507 295508 295509 295510 295511 295512 295513 295514 295515 [...] (2610 flits)
Measured flits: 295506 295507 295508 295509 295510 295511 295512 295513 295514 295515 [...] (2610 flits)
Class 0:
Remaining flits: 388416 388417 388418 388419 388420 388421 438722 438723 438724 438725 [...] (16 flits)
Measured flits: 388416 388417 388418 388419 388420 388421 438722 438723 438724 438725 [...] (16 flits)
Time taken is 11017 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2405.14 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6954 (1 samples)
Network latency average = 2261.79 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6794 (1 samples)
Flit latency average = 2065.57 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6777 (1 samples)
Fragmentation average = 87.1271 (1 samples)
	minimum = 0 (1 samples)
	maximum = 918 (1 samples)
Injected packet rate average = 0.0218993 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0152431 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.394309 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.926 (1 samples)
Accepted flit rate average = 0.27583 (1 samples)
	minimum = 0.183333 (1 samples)
	maximum = 0.394 (1 samples)
Injected packet size average = 18.0055 (1 samples)
Accepted packet size average = 18.0954 (1 samples)
Hops average = 5.08776 (1 samples)
Total run time 9.37318
