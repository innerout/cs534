BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 274.84
	minimum = 22
	maximum = 937
Network latency average = 196.988
	minimum = 22
	maximum = 784
Slowest packet = 69
Flit latency average = 166.613
	minimum = 5
	maximum = 767
Slowest flit = 12131
Fragmentation average = 36.1574
	minimum = 0
	maximum = 292
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.012776
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 48)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.238
	minimum = 0.076 (at node 174)
	maximum = 0.396 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6286
Total in-flight flits = 27078 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 444.105
	minimum = 22
	maximum = 1723
Network latency average = 339.312
	minimum = 22
	maximum = 1411
Slowest packet = 69
Flit latency average = 307.422
	minimum = 5
	maximum = 1486
Slowest flit = 33853
Fragmentation average = 47.9795
	minimum = 0
	maximum = 350
Injected packet rate average = 0.0217734
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.01375
	minimum = 0.0075 (at node 153)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.390318
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253206
	minimum = 0.14 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9263
Accepted packet length average = 18.415
Total in-flight flits = 53267 (0 measured)
latency change    = 0.381137
throughput change = 0.0600529
Class 0:
Packet latency average = 960.167
	minimum = 24
	maximum = 2563
Network latency average = 799.821
	minimum = 22
	maximum = 2088
Slowest packet = 3212
Flit latency average = 762.667
	minimum = 5
	maximum = 2077
Slowest flit = 64868
Fragmentation average = 66.4145
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0219635
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.0151667
	minimum = 0.007 (at node 134)
	maximum = 0.031 (at node 16)
Injected flit rate average = 0.39499
	minimum = 0 (at node 5)
	maximum = 1 (at node 40)
Accepted flit rate average= 0.271891
	minimum = 0.126 (at node 190)
	maximum = 0.551 (at node 16)
Injected packet length average = 17.9839
Accepted packet length average = 17.9269
Total in-flight flits = 76970 (0 measured)
latency change    = 0.537471
throughput change = 0.0687221
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 295.106
	minimum = 22
	maximum = 1128
Network latency average = 185.144
	minimum = 22
	maximum = 951
Slowest packet = 12589
Flit latency average = 1052.29
	minimum = 5
	maximum = 2708
Slowest flit = 94361
Fragmentation average = 15.4113
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0220104
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 156)
Accepted packet rate average = 0.0150938
	minimum = 0.006 (at node 138)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.396823
	minimum = 0 (at node 32)
	maximum = 0.999 (at node 156)
Accepted flit rate average= 0.273844
	minimum = 0.119 (at node 84)
	maximum = 0.481 (at node 115)
Injected packet length average = 18.0289
Accepted packet length average = 18.1429
Total in-flight flits = 100478 (65573 measured)
latency change    = 2.25363
throughput change = 0.00713226
Class 0:
Packet latency average = 804.949
	minimum = 22
	maximum = 2789
Network latency average = 684.309
	minimum = 22
	maximum = 1941
Slowest packet = 12589
Flit latency average = 1207.34
	minimum = 5
	maximum = 3615
Slowest flit = 100200
Fragmentation average = 37.0212
	minimum = 0
	maximum = 337
Injected packet rate average = 0.021763
	minimum = 0.001 (at node 120)
	maximum = 0.045 (at node 92)
Accepted packet rate average = 0.0151328
	minimum = 0.0095 (at node 132)
	maximum = 0.023 (at node 6)
Injected flit rate average = 0.391966
	minimum = 0.018 (at node 120)
	maximum = 0.8145 (at node 188)
Accepted flit rate average= 0.273602
	minimum = 0.1855 (at node 2)
	maximum = 0.408 (at node 6)
Injected packet length average = 18.0106
Accepted packet length average = 18.08
Total in-flight flits = 122369 (113145 measured)
latency change    = 0.633385
throughput change = 0.000885183
Class 0:
Packet latency average = 1252.54
	minimum = 22
	maximum = 3545
Network latency average = 1130.24
	minimum = 22
	maximum = 2940
Slowest packet = 12589
Flit latency average = 1351.36
	minimum = 5
	maximum = 4201
Slowest flit = 106001
Fragmentation average = 51.7272
	minimum = 0
	maximum = 403
Injected packet rate average = 0.0214427
	minimum = 0.00466667 (at node 120)
	maximum = 0.045 (at node 90)
Accepted packet rate average = 0.0151354
	minimum = 0.00966667 (at node 64)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.386125
	minimum = 0.084 (at node 120)
	maximum = 0.807 (at node 90)
Accepted flit rate average= 0.272995
	minimum = 0.171667 (at node 36)
	maximum = 0.391 (at node 123)
Injected packet length average = 18.0073
Accepted packet length average = 18.0368
Total in-flight flits = 142043 (140223 measured)
latency change    = 0.357348
throughput change = 0.00222265
Draining remaining packets ...
Class 0:
Remaining flits: 163566 163567 163568 163569 163570 163571 163572 163573 163574 163575 [...] (95136 flits)
Measured flits: 227052 227053 227054 227055 227056 227057 227058 227059 227060 227061 [...] (94665 flits)
Class 0:
Remaining flits: 174693 174694 174695 174696 174697 174698 174699 174700 174701 174702 [...] (50998 flits)
Measured flits: 227069 227844 227845 227846 227847 227848 227849 227850 227851 227852 [...] (50893 flits)
Class 0:
Remaining flits: 225536 225537 225538 225539 235096 235097 239166 239167 239168 239169 [...] (18327 flits)
Measured flits: 235096 235097 239166 239167 239168 239169 239170 239171 239172 239173 [...] (18323 flits)
Class 0:
Remaining flits: 309420 309421 309422 309423 309424 309425 309426 309427 309428 309429 [...] (3428 flits)
Measured flits: 309420 309421 309422 309423 309424 309425 309426 309427 309428 309429 [...] (3428 flits)
Class 0:
Remaining flits: 328604 328605 328606 328607 349668 349669 349670 349671 349672 349673 [...] (388 flits)
Measured flits: 328604 328605 328606 328607 349668 349669 349670 349671 349672 349673 [...] (388 flits)
Time taken is 11395 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2369.34 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7199 (1 samples)
Network latency average = 2225.41 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6689 (1 samples)
Flit latency average = 2051.03 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6672 (1 samples)
Fragmentation average = 56.0104 (1 samples)
	minimum = 0 (1 samples)
	maximum = 593 (1 samples)
Injected packet rate average = 0.0214427 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.045 (1 samples)
Accepted packet rate average = 0.0151354 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.386125 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.807 (1 samples)
Accepted flit rate average = 0.272995 (1 samples)
	minimum = 0.171667 (1 samples)
	maximum = 0.391 (1 samples)
Injected packet size average = 18.0073 (1 samples)
Accepted packet size average = 18.0368 (1 samples)
Hops average = 5.08097 (1 samples)
Total run time 7.59543
