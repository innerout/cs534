BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 307.257
	minimum = 25
	maximum = 850
Network latency average = 291.346
	minimum = 22
	maximum = 813
Slowest packet = 771
Flit latency average = 261.226
	minimum = 5
	maximum = 846
Slowest flit = 13084
Fragmentation average = 74.9082
	minimum = 0
	maximum = 651
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0145781
	minimum = 0.007 (at node 150)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.287161
	minimum = 0.151 (at node 135)
	maximum = 0.456 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 19.6981
Total in-flight flits = 86237 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.767
	minimum = 23
	maximum = 1719
Network latency average = 557.834
	minimum = 22
	maximum = 1649
Slowest packet = 2040
Flit latency average = 515.311
	minimum = 5
	maximum = 1632
Slowest flit = 44927
Fragmentation average = 123.785
	minimum = 0
	maximum = 889
Injected packet rate average = 0.0417969
	minimum = 0.031 (at node 50)
	maximum = 0.052 (at node 154)
Accepted packet rate average = 0.0153984
	minimum = 0.0075 (at node 135)
	maximum = 0.025 (at node 152)
Injected flit rate average = 0.74924
	minimum = 0.556 (at node 50)
	maximum = 0.9345 (at node 154)
Accepted flit rate average= 0.294229
	minimum = 0.159 (at node 135)
	maximum = 0.4655 (at node 152)
Injected packet length average = 17.9257
Accepted packet length average = 19.1077
Total in-flight flits = 175916 (0 measured)
latency change    = 0.468198
throughput change = 0.0240211
Class 0:
Packet latency average = 1344.67
	minimum = 24
	maximum = 2568
Network latency average = 1319.09
	minimum = 22
	maximum = 2479
Slowest packet = 2938
Flit latency average = 1273.09
	minimum = 5
	maximum = 2490
Slowest flit = 67911
Fragmentation average = 229.598
	minimum = 0
	maximum = 870
Injected packet rate average = 0.0414219
	minimum = 0.024 (at node 124)
	maximum = 0.055 (at node 58)
Accepted packet rate average = 0.0162969
	minimum = 0.008 (at node 52)
	maximum = 0.027 (at node 63)
Injected flit rate average = 0.745422
	minimum = 0.428 (at node 124)
	maximum = 1 (at node 58)
Accepted flit rate average= 0.301552
	minimum = 0.155 (at node 118)
	maximum = 0.474 (at node 120)
Injected packet length average = 17.9959
Accepted packet length average = 18.5037
Total in-flight flits = 261172 (0 measured)
latency change    = 0.570329
throughput change = 0.0242841
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 87.6268
	minimum = 23
	maximum = 782
Network latency average = 38.4593
	minimum = 23
	maximum = 347
Slowest packet = 24028
Flit latency average = 1778.12
	minimum = 5
	maximum = 3378
Slowest flit = 82256
Fragmentation average = 6.32057
	minimum = 0
	maximum = 30
Injected packet rate average = 0.0401354
	minimum = 0.016 (at node 0)
	maximum = 0.055 (at node 161)
Accepted packet rate average = 0.0165938
	minimum = 0.009 (at node 53)
	maximum = 0.03 (at node 178)
Injected flit rate average = 0.722635
	minimum = 0.3 (at node 0)
	maximum = 0.984 (at node 161)
Accepted flit rate average= 0.304979
	minimum = 0.163 (at node 53)
	maximum = 0.581 (at node 178)
Injected packet length average = 18.0049
Accepted packet length average = 18.3792
Total in-flight flits = 341342 (127375 measured)
latency change    = 14.3455
throughput change = 0.0112371
Class 0:
Packet latency average = 117.417
	minimum = 23
	maximum = 1317
Network latency average = 40.2885
	minimum = 22
	maximum = 443
Slowest packet = 24028
Flit latency average = 2045.4
	minimum = 5
	maximum = 4291
Slowest flit = 90305
Fragmentation average = 6.48947
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0394036
	minimum = 0.0215 (at node 0)
	maximum = 0.0555 (at node 161)
Accepted packet rate average = 0.0165729
	minimum = 0.0105 (at node 82)
	maximum = 0.025 (at node 178)
Injected flit rate average = 0.709271
	minimum = 0.3915 (at node 0)
	maximum = 0.992 (at node 161)
Accepted flit rate average= 0.304331
	minimum = 0.1905 (at node 141)
	maximum = 0.4575 (at node 178)
Injected packet length average = 18.0001
Accepted packet length average = 18.3631
Total in-flight flits = 416667 (250053 measured)
latency change    = 0.25371
throughput change = 0.0021307
Class 0:
Packet latency average = 154.068
	minimum = 23
	maximum = 1573
Network latency average = 44.6537
	minimum = 22
	maximum = 839
Slowest packet = 24028
Flit latency average = 2317.12
	minimum = 5
	maximum = 5104
Slowest flit = 124016
Fragmentation average = 6.57127
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0393785
	minimum = 0.0243333 (at node 56)
	maximum = 0.05 (at node 29)
Accepted packet rate average = 0.0164427
	minimum = 0.012 (at node 80)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.708637
	minimum = 0.435 (at node 56)
	maximum = 0.905333 (at node 29)
Accepted flit rate average= 0.301769
	minimum = 0.213 (at node 80)
	maximum = 0.399333 (at node 178)
Injected packet length average = 17.9955
Accepted packet length average = 18.3528
Total in-flight flits = 495647 (375858 measured)
latency change    = 0.237891
throughput change = 0.00848872
Class 0:
Packet latency average = 343.703
	minimum = 23
	maximum = 4016
Network latency average = 219.705
	minimum = 22
	maximum = 3949
Slowest packet = 24106
Flit latency average = 2597.41
	minimum = 5
	maximum = 5831
Slowest flit = 163690
Fragmentation average = 22.5511
	minimum = 0
	maximum = 650
Injected packet rate average = 0.0391003
	minimum = 0.02275 (at node 136)
	maximum = 0.04925 (at node 29)
Accepted packet rate average = 0.0164661
	minimum = 0.01175 (at node 36)
	maximum = 0.02125 (at node 99)
Injected flit rate average = 0.703652
	minimum = 0.40675 (at node 136)
	maximum = 0.8905 (at node 29)
Accepted flit rate average= 0.301243
	minimum = 0.23175 (at node 88)
	maximum = 0.3905 (at node 178)
Injected packet length average = 17.9961
Accepted packet length average = 18.2947
Total in-flight flits = 570375 (494476 measured)
latency change    = 0.551742
throughput change = 0.00174479
Class 0:
Packet latency average = 1080.39
	minimum = 23
	maximum = 5293
Network latency average = 950.144
	minimum = 22
	maximum = 4985
Slowest packet = 24106
Flit latency average = 2909.62
	minimum = 5
	maximum = 6635
Slowest flit = 189568
Fragmentation average = 91.8809
	minimum = 0
	maximum = 747
Injected packet rate average = 0.0386667
	minimum = 0.0188 (at node 136)
	maximum = 0.0482 (at node 29)
Accepted packet rate average = 0.0163208
	minimum = 0.0124 (at node 144)
	maximum = 0.0204 (at node 128)
Injected flit rate average = 0.696028
	minimum = 0.337 (at node 136)
	maximum = 0.8682 (at node 29)
Accepted flit rate average= 0.297698
	minimum = 0.216 (at node 171)
	maximum = 0.3748 (at node 178)
Injected packet length average = 18.0007
Accepted packet length average = 18.2404
Total in-flight flits = 643596 (601327 measured)
latency change    = 0.681872
throughput change = 0.01191
Class 0:
Packet latency average = 2061.26
	minimum = 23
	maximum = 6244
Network latency average = 1927.5
	minimum = 22
	maximum = 5977
Slowest packet = 24106
Flit latency average = 3235.19
	minimum = 5
	maximum = 7255
Slowest flit = 247819
Fragmentation average = 171.096
	minimum = 0
	maximum = 777
Injected packet rate average = 0.0379661
	minimum = 0.0188333 (at node 136)
	maximum = 0.0476667 (at node 157)
Accepted packet rate average = 0.0161502
	minimum = 0.0125 (at node 171)
	maximum = 0.0198333 (at node 115)
Injected flit rate average = 0.683365
	minimum = 0.337333 (at node 136)
	maximum = 0.859833 (at node 157)
Accepted flit rate average= 0.294275
	minimum = 0.2245 (at node 171)
	maximum = 0.36 (at node 115)
Injected packet length average = 17.9993
Accepted packet length average = 18.2212
Total in-flight flits = 709559 (690819 measured)
latency change    = 0.475858
throughput change = 0.0116311
Class 0:
Packet latency average = 3023.1
	minimum = 23
	maximum = 7255
Network latency average = 2887.21
	minimum = 22
	maximum = 6973
Slowest packet = 24106
Flit latency average = 3554.23
	minimum = 5
	maximum = 7992
Slowest flit = 285532
Fragmentation average = 233.026
	minimum = 0
	maximum = 789
Injected packet rate average = 0.036933
	minimum = 0.0188571 (at node 136)
	maximum = 0.0458571 (at node 175)
Accepted packet rate average = 0.0160432
	minimum = 0.013 (at node 88)
	maximum = 0.0195714 (at node 115)
Injected flit rate average = 0.66488
	minimum = 0.340143 (at node 136)
	maximum = 0.826143 (at node 175)
Accepted flit rate average= 0.291556
	minimum = 0.237286 (at node 171)
	maximum = 0.352429 (at node 138)
Injected packet length average = 18.0023
Accepted packet length average = 18.1732
Total in-flight flits = 763003 (757858 measured)
latency change    = 0.318163
throughput change = 0.0093271
Draining all recorded packets ...
Class 0:
Remaining flits: 334878 334879 334880 334881 334882 334883 334884 334885 334886 334887 [...] (797762 flits)
Measured flits: 432610 432611 434069 434165 434166 434167 434168 434169 434170 434171 [...] (740788 flits)
Class 0:
Remaining flits: 384238 384239 384240 384241 384242 384243 384244 384245 421632 421633 [...] (806471 flits)
Measured flits: 436788 436789 436790 436791 436792 436793 436794 436795 436796 436797 [...] (705447 flits)
Class 0:
Remaining flits: 456206 456207 456208 456209 456640 456641 458622 458623 458624 458625 [...] (809364 flits)
Measured flits: 456206 456207 456208 456209 456640 456641 458622 458623 458624 458625 [...] (667982 flits)
Class 0:
Remaining flits: 460404 460405 460406 460407 460408 460409 460410 460411 460412 460413 [...] (810558 flits)
Measured flits: 460404 460405 460406 460407 460408 460409 460410 460411 460412 460413 [...] (628443 flits)
Class 0:
Remaining flits: 460420 460421 472263 472264 472265 486774 486775 486776 486777 486778 [...] (815901 flits)
Measured flits: 460420 460421 472263 472264 472265 486774 486775 486776 486777 486778 [...] (590055 flits)
Class 0:
Remaining flits: 541116 541117 541118 541119 541120 541121 541122 541123 541124 541125 [...] (818058 flits)
Measured flits: 541116 541117 541118 541119 541120 541121 541122 541123 541124 541125 [...] (552124 flits)
Class 0:
Remaining flits: 562194 562195 562196 562197 562198 562199 562200 562201 562202 562203 [...] (825074 flits)
Measured flits: 562194 562195 562196 562197 562198 562199 562200 562201 562202 562203 [...] (515955 flits)
Class 0:
Remaining flits: 597975 597976 597977 625995 625996 625997 625998 625999 626000 626001 [...] (827589 flits)
Measured flits: 597975 597976 597977 625995 625996 625997 625998 625999 626000 626001 [...] (477424 flits)
Class 0:
Remaining flits: 640435 640436 640437 640438 640439 642284 642285 642286 642287 642288 [...] (825360 flits)
Measured flits: 640435 640436 640437 640438 640439 642284 642285 642286 642287 642288 [...] (436799 flits)
Class 0:
Remaining flits: 667368 667369 667370 667371 667372 667373 667374 667375 667376 667377 [...] (823980 flits)
Measured flits: 667368 667369 667370 667371 667372 667373 667374 667375 667376 667377 [...] (394683 flits)
Class 0:
Remaining flits: 705060 705061 705062 705063 705064 705065 705066 705067 705068 705069 [...] (831586 flits)
Measured flits: 705060 705061 705062 705063 705064 705065 705066 705067 705068 705069 [...] (351686 flits)
Class 0:
Remaining flits: 707580 707581 707582 707583 707584 707585 707586 707587 707588 707589 [...] (837068 flits)
Measured flits: 707580 707581 707582 707583 707584 707585 707586 707587 707588 707589 [...] (306900 flits)
Class 0:
Remaining flits: 741887 743148 743149 743150 743151 743152 743153 743154 743155 743156 [...] (843083 flits)
Measured flits: 741887 743148 743149 743150 743151 743152 743153 743154 743155 743156 [...] (260925 flits)
Class 0:
Remaining flits: 794502 794503 794504 794505 794506 794507 794508 794509 794510 794511 [...] (842978 flits)
Measured flits: 794502 794503 794504 794505 794506 794507 794508 794509 794510 794511 [...] (215674 flits)
Class 0:
Remaining flits: 850590 850591 850592 850593 850594 850595 850596 850597 850598 850599 [...] (842339 flits)
Measured flits: 850590 850591 850592 850593 850594 850595 850596 850597 850598 850599 [...] (173161 flits)
Class 0:
Remaining flits: 850590 850591 850592 850593 850594 850595 850596 850597 850598 850599 [...] (838382 flits)
Measured flits: 850590 850591 850592 850593 850594 850595 850596 850597 850598 850599 [...] (134653 flits)
Class 0:
Remaining flits: 850594 850595 850596 850597 850598 850599 850600 850601 850602 850603 [...] (836894 flits)
Measured flits: 850594 850595 850596 850597 850598 850599 850600 850601 850602 850603 [...] (100673 flits)
Class 0:
Remaining flits: 888984 888985 888986 888987 888988 888989 888990 888991 888992 888993 [...] (836758 flits)
Measured flits: 888984 888985 888986 888987 888988 888989 888990 888991 888992 888993 [...] (72504 flits)
Class 0:
Remaining flits: 888984 888985 888986 888987 888988 888989 888990 888991 888992 888993 [...] (839166 flits)
Measured flits: 888984 888985 888986 888987 888988 888989 888990 888991 888992 888993 [...] (49792 flits)
Class 0:
Remaining flits: 929556 929557 929558 929559 929560 929561 929562 929563 929564 929565 [...] (844633 flits)
Measured flits: 929556 929557 929558 929559 929560 929561 929562 929563 929564 929565 [...] (33316 flits)
Class 0:
Remaining flits: 929556 929557 929558 929559 929560 929561 929562 929563 929564 929565 [...] (847775 flits)
Measured flits: 929556 929557 929558 929559 929560 929561 929562 929563 929564 929565 [...] (21641 flits)
Class 0:
Remaining flits: 1000746 1000747 1000748 1000749 1000750 1000751 1000752 1000753 1000754 1000755 [...] (850038 flits)
Measured flits: 1000746 1000747 1000748 1000749 1000750 1000751 1000752 1000753 1000754 1000755 [...] (13104 flits)
Class 0:
Remaining flits: 1008054 1008055 1008056 1008057 1008058 1008059 1008060 1008061 1008062 1008063 [...] (850442 flits)
Measured flits: 1008054 1008055 1008056 1008057 1008058 1008059 1008060 1008061 1008062 1008063 [...] (8288 flits)
Class 0:
Remaining flits: 1008054 1008055 1008056 1008057 1008058 1008059 1008060 1008061 1008062 1008063 [...] (852153 flits)
Measured flits: 1008054 1008055 1008056 1008057 1008058 1008059 1008060 1008061 1008062 1008063 [...] (4252 flits)
Class 0:
Remaining flits: 1068552 1068553 1068554 1068555 1068556 1068557 1068558 1068559 1068560 1068561 [...] (853727 flits)
Measured flits: 1068552 1068553 1068554 1068555 1068556 1068557 1068558 1068559 1068560 1068561 [...] (2184 flits)
Class 0:
Remaining flits: 1068552 1068553 1068554 1068555 1068556 1068557 1068558 1068559 1068560 1068561 [...] (855522 flits)
Measured flits: 1068552 1068553 1068554 1068555 1068556 1068557 1068558 1068559 1068560 1068561 [...] (1105 flits)
Class 0:
Remaining flits: 1227474 1227475 1227476 1227477 1227478 1227479 1227480 1227481 1227482 1227483 [...] (854525 flits)
Measured flits: 1227474 1227475 1227476 1227477 1227478 1227479 1227480 1227481 1227482 1227483 [...] (588 flits)
Class 0:
Remaining flits: 1255734 1255735 1255736 1255737 1255738 1255739 1255740 1255741 1255742 1255743 [...] (852744 flits)
Measured flits: 1255734 1255735 1255736 1255737 1255738 1255739 1255740 1255741 1255742 1255743 [...] (172 flits)
Class 0:
Remaining flits: 1369044 1369045 1369046 1369047 1369048 1369049 1369050 1369051 1369052 1369053 [...] (847405 flits)
Measured flits: 1400058 1400059 1400060 1400061 1400062 1400063 1400064 1400065 1400066 1400067 [...] (72 flits)
Class 0:
Remaining flits: 1390662 1390663 1390664 1390665 1390666 1390667 1390668 1390669 1390670 1390671 [...] (845607 flits)
Measured flits: 1524078 1524079 1524080 1524081 1524082 1524083 1524084 1524085 1524086 1524087 [...] (18 flits)
Class 0:
Remaining flits: 1390662 1390663 1390664 1390665 1390666 1390667 1390668 1390669 1390670 1390671 [...] (846523 flits)
Measured flits: 1524086 1524087 1524088 1524089 1524090 1524091 1524092 1524093 1524094 1524095 (10 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1437840 1437841 1437842 1437843 1437844 1437845 1437846 1437847 1437848 1437849 [...] (796158 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1485342 1485343 1485344 1485345 1485346 1485347 1485348 1485349 1485350 1485351 [...] (746285 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1485342 1485343 1485344 1485345 1485346 1485347 1485348 1485349 1485350 1485351 [...] (696164 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1485342 1485343 1485344 1485345 1485346 1485347 1485348 1485349 1485350 1485351 [...] (645561 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1485354 1485355 1485356 1485357 1485358 1485359 1499958 1499959 1499960 1499961 [...] (595529 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1499958 1499959 1499960 1499961 1499962 1499963 1499964 1499965 1499966 1499967 [...] (546031 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1560204 1560205 1560206 1560207 1560208 1560209 1560210 1560211 1560212 1560213 [...] (497096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805160 1805161 1805162 1805163 1805164 1805165 1863954 1863955 1863956 1863957 [...] (448887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1863954 1863955 1863956 1863957 1863958 1863959 1863960 1863961 1863962 1863963 [...] (401225 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1863954 1863955 1863956 1863957 1863958 1863959 1863960 1863961 1863962 1863963 [...] (353597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1948212 1948213 1948214 1948215 1948216 1948217 1948218 1948219 1948220 1948221 [...] (305959 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1956118 1956119 1956120 1956121 1956122 1956123 1956124 1956125 1956126 1956127 [...] (258285 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2006586 2006587 2006588 2006589 2006590 2006591 2006592 2006593 2006594 2006595 [...] (210419 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2205432 2205433 2205434 2205435 2205436 2205437 2205438 2205439 2205440 2205441 [...] (162841 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2222244 2222245 2222246 2222247 2222248 2222249 2222250 2222251 2222252 2222253 [...] (115228 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2231568 2231569 2231570 2231571 2231572 2231573 2231574 2231575 2231576 2231577 [...] (67946 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2402964 2402965 2402966 2402967 2402968 2402969 2402970 2402971 2402972 2402973 [...] (24140 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2430584 2430585 2430586 2430587 2430588 2430589 2430590 2430591 2430592 2430593 [...] (3124 flits)
Measured flits: (0 flits)
Time taken is 59900 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11544.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 31259 (1 samples)
Network latency average = 10617.5 (1 samples)
	minimum = 22 (1 samples)
	maximum = 29424 (1 samples)
Flit latency average = 13033.9 (1 samples)
	minimum = 5 (1 samples)
	maximum = 35379 (1 samples)
Fragmentation average = 289.926 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1694 (1 samples)
Injected packet rate average = 0.036933 (1 samples)
	minimum = 0.0188571 (1 samples)
	maximum = 0.0458571 (1 samples)
Accepted packet rate average = 0.0160432 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.0195714 (1 samples)
Injected flit rate average = 0.66488 (1 samples)
	minimum = 0.340143 (1 samples)
	maximum = 0.826143 (1 samples)
Accepted flit rate average = 0.291556 (1 samples)
	minimum = 0.237286 (1 samples)
	maximum = 0.352429 (1 samples)
Injected packet size average = 18.0023 (1 samples)
Accepted packet size average = 18.1732 (1 samples)
Hops average = 5.07283 (1 samples)
Total run time 230.891
