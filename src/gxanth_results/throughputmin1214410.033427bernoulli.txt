BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 307.118
	minimum = 23
	maximum = 901
Network latency average = 296.694
	minimum = 23
	maximum = 879
Slowest packet = 391
Flit latency average = 264.657
	minimum = 6
	maximum = 925
Slowest flit = 6347
Fragmentation average = 55.2106
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0305833
	minimum = 0.02 (at node 52)
	maximum = 0.046 (at node 145)
Accepted packet rate average = 0.0101667
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 105)
Injected flit rate average = 0.545266
	minimum = 0.352 (at node 52)
	maximum = 0.828 (at node 145)
Accepted flit rate average= 0.190932
	minimum = 0.057 (at node 174)
	maximum = 0.333 (at node 105)
Injected packet length average = 17.8288
Accepted packet length average = 18.7802
Total in-flight flits = 69163 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 626.235
	minimum = 23
	maximum = 1747
Network latency average = 602.209
	minimum = 23
	maximum = 1715
Slowest packet = 998
Flit latency average = 566.869
	minimum = 6
	maximum = 1774
Slowest flit = 23404
Fragmentation average = 60.3363
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0278099
	minimum = 0.0145 (at node 172)
	maximum = 0.0355 (at node 53)
Accepted packet rate average = 0.0100443
	minimum = 0.0055 (at node 174)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.497802
	minimum = 0.261 (at node 172)
	maximum = 0.639 (at node 179)
Accepted flit rate average= 0.184768
	minimum = 0.1085 (at node 174)
	maximum = 0.2745 (at node 44)
Injected packet length average = 17.9002
Accepted packet length average = 18.3954
Total in-flight flits = 122171 (0 measured)
latency change    = 0.50958
throughput change = 0.0333611
Class 0:
Packet latency average = 1653.86
	minimum = 29
	maximum = 2725
Network latency average = 1554.11
	minimum = 26
	maximum = 2688
Slowest packet = 1478
Flit latency average = 1524.79
	minimum = 6
	maximum = 2671
Slowest flit = 32183
Fragmentation average = 71.2901
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0115052
	minimum = 0 (at node 18)
	maximum = 0.034 (at node 187)
Accepted packet rate average = 0.00960417
	minimum = 0.001 (at node 149)
	maximum = 0.019 (at node 14)
Injected flit rate average = 0.209937
	minimum = 0 (at node 18)
	maximum = 0.625 (at node 187)
Accepted flit rate average= 0.172516
	minimum = 0.018 (at node 149)
	maximum = 0.313 (at node 14)
Injected packet length average = 18.2472
Accepted packet length average = 17.9626
Total in-flight flits = 130232 (0 measured)
latency change    = 0.621349
throughput change = 0.0710232
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1526.23
	minimum = 33
	maximum = 2823
Network latency average = 126.262
	minimum = 25
	maximum = 952
Slowest packet = 13021
Flit latency average = 2187.75
	minimum = 6
	maximum = 3565
Slowest flit = 41075
Fragmentation average = 21.8
	minimum = 0
	maximum = 106
Injected packet rate average = 0.00816146
	minimum = 0 (at node 3)
	maximum = 0.045 (at node 17)
Accepted packet rate average = 0.009125
	minimum = 0.002 (at node 117)
	maximum = 0.016 (at node 95)
Injected flit rate average = 0.146594
	minimum = 0 (at node 3)
	maximum = 0.812 (at node 17)
Accepted flit rate average= 0.164161
	minimum = 0.036 (at node 138)
	maximum = 0.291 (at node 95)
Injected packet length average = 17.9617
Accepted packet length average = 17.9903
Total in-flight flits = 126793 (26843 measured)
latency change    = 0.0836232
throughput change = 0.0508899
Class 0:
Packet latency average = 2233.63
	minimum = 27
	maximum = 4172
Network latency average = 602.743
	minimum = 25
	maximum = 1915
Slowest packet = 13021
Flit latency average = 2460.86
	minimum = 6
	maximum = 4590
Slowest flit = 44788
Fragmentation average = 44.7344
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00860417
	minimum = 0.0015 (at node 33)
	maximum = 0.0385 (at node 17)
Accepted packet rate average = 0.00911719
	minimum = 0.005 (at node 126)
	maximum = 0.0155 (at node 120)
Injected flit rate average = 0.154333
	minimum = 0.027 (at node 33)
	maximum = 0.6905 (at node 17)
Accepted flit rate average= 0.163857
	minimum = 0.094 (at node 91)
	maximum = 0.279 (at node 120)
Injected packet length average = 17.937
Accepted packet length average = 17.9723
Total in-flight flits = 126693 (54968 measured)
latency change    = 0.316704
throughput change = 0.00185947
Class 0:
Packet latency average = 2971.04
	minimum = 26
	maximum = 4835
Network latency average = 1062.03
	minimum = 25
	maximum = 2953
Slowest packet = 13021
Flit latency average = 2721.78
	minimum = 6
	maximum = 5462
Slowest flit = 57179
Fragmentation average = 52.4691
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00857292
	minimum = 0.00133333 (at node 33)
	maximum = 0.0283333 (at node 17)
Accepted packet rate average = 0.00912847
	minimum = 0.00466667 (at node 86)
	maximum = 0.0136667 (at node 120)
Injected flit rate average = 0.154078
	minimum = 0.024 (at node 33)
	maximum = 0.511333 (at node 17)
Accepted flit rate average= 0.164156
	minimum = 0.084 (at node 86)
	maximum = 0.243 (at node 120)
Injected packet length average = 17.9727
Accepted packet length average = 17.9829
Total in-flight flits = 124364 (77637 measured)
latency change    = 0.248198
throughput change = 0.00182435
Class 0:
Packet latency average = 3684.16
	minimum = 26
	maximum = 5781
Network latency average = 1554.49
	minimum = 25
	maximum = 3876
Slowest packet = 13021
Flit latency average = 2926.99
	minimum = 6
	maximum = 6032
Slowest flit = 101924
Fragmentation average = 59.5385
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00855729
	minimum = 0.00125 (at node 36)
	maximum = 0.02125 (at node 17)
Accepted packet rate average = 0.00908984
	minimum = 0.00525 (at node 86)
	maximum = 0.013 (at node 76)
Injected flit rate average = 0.153992
	minimum = 0.0225 (at node 36)
	maximum = 0.3835 (at node 17)
Accepted flit rate average= 0.16374
	minimum = 0.0945 (at node 86)
	maximum = 0.232 (at node 76)
Injected packet length average = 17.9954
Accepted packet length average = 18.0135
Total in-flight flits = 122758 (95199 measured)
latency change    = 0.193564
throughput change = 0.00254469
Draining remaining packets ...
Class 0:
Remaining flits: 101610 101611 101612 101613 101614 101615 101616 101617 101618 101619 [...] (92615 flits)
Measured flits: 234347 234348 234349 234350 234351 234352 234353 234354 234355 234356 [...] (78585 flits)
Class 0:
Remaining flits: 119034 119035 119036 119037 119038 119039 119040 119041 119042 119043 [...] (62459 flits)
Measured flits: 234684 234685 234686 234687 234688 234689 234690 234691 234692 234693 [...] (56508 flits)
Class 0:
Remaining flits: 119034 119035 119036 119037 119038 119039 119040 119041 119042 119043 [...] (33401 flits)
Measured flits: 234702 234703 234704 234705 234706 234707 234708 234709 234710 234711 [...] (31364 flits)
Class 0:
Remaining flits: 147479 147480 147481 147482 147483 147484 147485 147486 147487 147488 [...] (7748 flits)
Measured flits: 237872 237873 237874 237875 237876 237877 237878 237879 237880 237881 [...] (7507 flits)
Time taken is 11800 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6037.53 (1 samples)
	minimum = 26 (1 samples)
	maximum = 9643 (1 samples)
Network latency average = 3581.68 (1 samples)
	minimum = 25 (1 samples)
	maximum = 8391 (1 samples)
Flit latency average = 3679.45 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9662 (1 samples)
Fragmentation average = 65.921 (1 samples)
	minimum = 0 (1 samples)
	maximum = 166 (1 samples)
Injected packet rate average = 0.00855729 (1 samples)
	minimum = 0.00125 (1 samples)
	maximum = 0.02125 (1 samples)
Accepted packet rate average = 0.00908984 (1 samples)
	minimum = 0.00525 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.153992 (1 samples)
	minimum = 0.0225 (1 samples)
	maximum = 0.3835 (1 samples)
Accepted flit rate average = 0.16374 (1 samples)
	minimum = 0.0945 (1 samples)
	maximum = 0.232 (1 samples)
Injected packet size average = 17.9954 (1 samples)
Accepted packet size average = 18.0135 (1 samples)
Hops average = 5.05463 (1 samples)
Total run time 15.1679
