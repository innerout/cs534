BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 347.009
	minimum = 25
	maximum = 940
Network latency average = 328.994
	minimum = 23
	maximum = 940
Slowest packet = 181
Flit latency average = 298.226
	minimum = 6
	maximum = 936
Slowest flit = 4912
Fragmentation average = 58.0232
	minimum = 0
	maximum = 164
Injected packet rate average = 0.0378542
	minimum = 0.021 (at node 12)
	maximum = 0.05 (at node 33)
Accepted packet rate average = 0.0103385
	minimum = 0.005 (at node 41)
	maximum = 0.019 (at node 27)
Injected flit rate average = 0.674458
	minimum = 0.361 (at node 12)
	maximum = 0.893 (at node 101)
Accepted flit rate average= 0.19451
	minimum = 0.09 (at node 41)
	maximum = 0.342 (at node 27)
Injected packet length average = 17.8173
Accepted packet length average = 18.8141
Total in-flight flits = 93622 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 707.261
	minimum = 25
	maximum = 1847
Network latency average = 663.671
	minimum = 23
	maximum = 1780
Slowest packet = 1473
Flit latency average = 627.855
	minimum = 6
	maximum = 1815
Slowest flit = 22766
Fragmentation average = 64.0294
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0284453
	minimum = 0.016 (at node 112)
	maximum = 0.0365 (at node 77)
Accepted packet rate average = 0.0100833
	minimum = 0.0055 (at node 83)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.509164
	minimum = 0.287 (at node 112)
	maximum = 0.657 (at node 83)
Accepted flit rate average= 0.185401
	minimum = 0.099 (at node 115)
	maximum = 0.288 (at node 44)
Injected packet length average = 17.8998
Accepted packet length average = 18.3869
Total in-flight flits = 126518 (0 measured)
latency change    = 0.509363
throughput change = 0.0491334
Class 0:
Packet latency average = 1887.44
	minimum = 274
	maximum = 2783
Network latency average = 1717.62
	minimum = 27
	maximum = 2719
Slowest packet = 1895
Flit latency average = 1687
	minimum = 6
	maximum = 2730
Slowest flit = 35901
Fragmentation average = 71.1107
	minimum = 0
	maximum = 153
Injected packet rate average = 0.010625
	minimum = 0 (at node 7)
	maximum = 0.031 (at node 129)
Accepted packet rate average = 0.00940625
	minimum = 0.003 (at node 65)
	maximum = 0.017 (at node 73)
Injected flit rate average = 0.193557
	minimum = 0 (at node 7)
	maximum = 0.548 (at node 129)
Accepted flit rate average= 0.168271
	minimum = 0.061 (at node 31)
	maximum = 0.298 (at node 73)
Injected packet length average = 18.2172
Accepted packet length average = 17.8893
Total in-flight flits = 131992 (0 measured)
latency change    = 0.625281
throughput change = 0.101801
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2157.74
	minimum = 893
	maximum = 3093
Network latency average = 218.283
	minimum = 26
	maximum = 909
Slowest packet = 13156
Flit latency average = 2365.28
	minimum = 6
	maximum = 3672
Slowest flit = 38753
Fragmentation average = 31.1509
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00804167
	minimum = 0 (at node 41)
	maximum = 0.029 (at node 14)
Accepted packet rate average = 0.00899479
	minimum = 0.003 (at node 79)
	maximum = 0.017 (at node 3)
Injected flit rate average = 0.145156
	minimum = 0 (at node 41)
	maximum = 0.522 (at node 14)
Accepted flit rate average= 0.16163
	minimum = 0.058 (at node 79)
	maximum = 0.288 (at node 51)
Injected packet length average = 18.0505
Accepted packet length average = 17.9693
Total in-flight flits = 128877 (26925 measured)
latency change    = 0.125267
throughput change = 0.0410853
Class 0:
Packet latency average = 2887.21
	minimum = 893
	maximum = 4198
Network latency average = 592.124
	minimum = 25
	maximum = 1922
Slowest packet = 13156
Flit latency average = 2635.3
	minimum = 6
	maximum = 4446
Slowest flit = 74093
Fragmentation average = 44.6571
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00811198
	minimum = 0.0005 (at node 82)
	maximum = 0.025 (at node 55)
Accepted packet rate average = 0.00901302
	minimum = 0.004 (at node 167)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.146336
	minimum = 0.009 (at node 82)
	maximum = 0.448 (at node 114)
Accepted flit rate average= 0.162273
	minimum = 0.0755 (at node 141)
	maximum = 0.2815 (at node 16)
Injected packet length average = 18.0395
Accepted packet length average = 18.0043
Total in-flight flits = 125911 (52242 measured)
latency change    = 0.252658
throughput change = 0.00396386
Class 0:
Packet latency average = 3634.86
	minimum = 893
	maximum = 5074
Network latency average = 1113.93
	minimum = 25
	maximum = 2882
Slowest packet = 13156
Flit latency average = 2866.67
	minimum = 6
	maximum = 5483
Slowest flit = 69770
Fragmentation average = 55.3237
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00812847
	minimum = 0.000333333 (at node 82)
	maximum = 0.0206667 (at node 55)
Accepted packet rate average = 0.00894792
	minimum = 0.005 (at node 5)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.146563
	minimum = 0.006 (at node 82)
	maximum = 0.372 (at node 55)
Accepted flit rate average= 0.161319
	minimum = 0.09 (at node 5)
	maximum = 0.235333 (at node 16)
Injected packet length average = 18.0308
Accepted packet length average = 18.0287
Total in-flight flits = 123402 (73250 measured)
latency change    = 0.205688
throughput change = 0.00591369
Draining remaining packets ...
Class 0:
Remaining flits: 83106 83107 83108 83109 83110 83111 83112 83113 83114 83115 [...] (92906 flits)
Measured flits: 235530 235531 235532 235533 235534 235535 235536 235537 235538 235539 [...] (62567 flits)
Class 0:
Remaining flits: 113382 113383 113384 113385 113386 113387 113388 113389 113390 113391 [...] (63194 flits)
Measured flits: 235530 235531 235532 235533 235534 235535 235536 235537 235538 235539 [...] (48242 flits)
Class 0:
Remaining flits: 126342 126343 126344 126345 126346 126347 126348 126349 126350 126351 [...] (34337 flits)
Measured flits: 235620 235621 235622 235623 235624 235625 235626 235627 235628 235629 [...] (29151 flits)
Class 0:
Remaining flits: 165474 165475 165476 165477 165478 165479 165480 165481 165482 165483 [...] (9305 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (8263 flits)
Class 0:
Remaining flits: 246654 246655 246656 246657 246658 246659 246660 246661 246662 246663 [...] (165 flits)
Measured flits: 246654 246655 246656 246657 246658 246659 246660 246661 246662 246663 [...] (165 flits)
Time taken is 11169 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6236.44 (1 samples)
	minimum = 893 (1 samples)
	maximum = 9513 (1 samples)
Network latency average = 3519.78 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7671 (1 samples)
Flit latency average = 3793.51 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8779 (1 samples)
Fragmentation average = 62.7812 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.00812847 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.0206667 (1 samples)
Accepted packet rate average = 0.00894792 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.146563 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.372 (1 samples)
Accepted flit rate average = 0.161319 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.235333 (1 samples)
Injected packet size average = 18.0308 (1 samples)
Accepted packet size average = 18.0287 (1 samples)
Hops average = 5.05422 (1 samples)
Total run time 7.47926
