BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 316.1
	minimum = 22
	maximum = 876
Network latency average = 291.399
	minimum = 22
	maximum = 829
Slowest packet = 728
Flit latency average = 263.326
	minimum = 5
	maximum = 830
Slowest flit = 22113
Fragmentation average = 82.7804
	minimum = 0
	maximum = 660
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0146823
	minimum = 0.005 (at node 150)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.295245
	minimum = 0.113 (at node 150)
	maximum = 0.45 (at node 88)
Injected packet length average = 17.8433
Accepted packet length average = 20.1089
Total in-flight flits = 107149 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 604.003
	minimum = 22
	maximum = 1760
Network latency average = 567.298
	minimum = 22
	maximum = 1672
Slowest packet = 728
Flit latency average = 523.284
	minimum = 5
	maximum = 1707
Slowest flit = 39488
Fragmentation average = 143.755
	minimum = 0
	maximum = 901
Injected packet rate average = 0.0489141
	minimum = 0.0375 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.015974
	minimum = 0.0095 (at node 81)
	maximum = 0.0245 (at node 152)
Injected flit rate average = 0.876638
	minimum = 0.675 (at node 43)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.307781
	minimum = 0.193 (at node 153)
	maximum = 0.472 (at node 152)
Injected packet length average = 17.922
Accepted packet length average = 19.2677
Total in-flight flits = 219906 (0 measured)
latency change    = 0.476658
throughput change = 0.0407317
Class 0:
Packet latency average = 1394.89
	minimum = 27
	maximum = 2563
Network latency average = 1333.21
	minimum = 24
	maximum = 2523
Slowest packet = 2360
Flit latency average = 1291.54
	minimum = 5
	maximum = 2548
Slowest flit = 66441
Fragmentation average = 251.55
	minimum = 0
	maximum = 865
Injected packet rate average = 0.0469375
	minimum = 0.02 (at node 16)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0169219
	minimum = 0.006 (at node 154)
	maximum = 0.027 (at node 89)
Injected flit rate average = 0.845208
	minimum = 0.363 (at node 16)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.313359
	minimum = 0.143 (at node 154)
	maximum = 0.486 (at node 3)
Injected packet length average = 18.0071
Accepted packet length average = 18.518
Total in-flight flits = 321957 (0 measured)
latency change    = 0.566989
throughput change = 0.017801
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 226.921
	minimum = 23
	maximum = 1293
Network latency average = 47.8994
	minimum = 23
	maximum = 473
Slowest packet = 27859
Flit latency average = 1866.72
	minimum = 5
	maximum = 3468
Slowest flit = 84114
Fragmentation average = 6.7783
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0448281
	minimum = 0.017 (at node 56)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0166563
	minimum = 0.004 (at node 104)
	maximum = 0.027 (at node 112)
Injected flit rate average = 0.806167
	minimum = 0.32 (at node 56)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.30737
	minimum = 0.143 (at node 104)
	maximum = 0.477 (at node 120)
Injected packet length average = 17.9835
Accepted packet length average = 18.4537
Total in-flight flits = 417868 (143255 measured)
latency change    = 5.14702
throughput change = 0.0194866
Class 0:
Packet latency average = 324.304
	minimum = 23
	maximum = 1818
Network latency average = 82.4084
	minimum = 23
	maximum = 927
Slowest packet = 27859
Flit latency average = 2160.81
	minimum = 5
	maximum = 4361
Slowest flit = 99477
Fragmentation average = 7.23646
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0449531
	minimum = 0.0215 (at node 64)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0165521
	minimum = 0.0095 (at node 104)
	maximum = 0.0235 (at node 73)
Injected flit rate average = 0.809031
	minimum = 0.3865 (at node 128)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.304529
	minimum = 0.189 (at node 86)
	maximum = 0.419 (at node 128)
Injected packet length average = 17.9972
Accepted packet length average = 18.3982
Total in-flight flits = 515734 (287972 measured)
latency change    = 0.300282
throughput change = 0.00932965
Class 0:
Packet latency average = 435.601
	minimum = 22
	maximum = 2456
Network latency average = 157.477
	minimum = 22
	maximum = 1506
Slowest packet = 27859
Flit latency average = 2488.3
	minimum = 5
	maximum = 5201
Slowest flit = 126412
Fragmentation average = 7.20778
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0450295
	minimum = 0.0223333 (at node 48)
	maximum = 0.0556667 (at node 27)
Accepted packet rate average = 0.0164601
	minimum = 0.011 (at node 79)
	maximum = 0.0223333 (at node 118)
Injected flit rate average = 0.810344
	minimum = 0.4 (at node 48)
	maximum = 1 (at node 27)
Accepted flit rate average= 0.301339
	minimum = 0.201667 (at node 5)
	maximum = 0.418667 (at node 118)
Injected packet length average = 17.9958
Accepted packet length average = 18.3072
Total in-flight flits = 615252 (434346 measured)
latency change    = 0.255501
throughput change = 0.0105864
Class 0:
Packet latency average = 569.214
	minimum = 22
	maximum = 4780
Network latency average = 245.803
	minimum = 22
	maximum = 3991
Slowest packet = 27796
Flit latency average = 2827.96
	minimum = 5
	maximum = 5979
Slowest flit = 165048
Fragmentation average = 9.67041
	minimum = 0
	maximum = 736
Injected packet rate average = 0.0445143
	minimum = 0.0225 (at node 52)
	maximum = 0.05575 (at node 53)
Accepted packet rate average = 0.0164414
	minimum = 0.012 (at node 79)
	maximum = 0.0215 (at node 118)
Injected flit rate average = 0.801188
	minimum = 0.403 (at node 52)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.299426
	minimum = 0.214 (at node 79)
	maximum = 0.3895 (at node 118)
Injected packet length average = 17.9984
Accepted packet length average = 18.2117
Total in-flight flits = 707436 (573415 measured)
latency change    = 0.234734
throughput change = 0.0063881
Class 0:
Packet latency average = 870.3
	minimum = 22
	maximum = 5785
Network latency average = 499.86
	minimum = 22
	maximum = 4934
Slowest packet = 27796
Flit latency average = 3190.51
	minimum = 5
	maximum = 6817
Slowest flit = 186780
Fragmentation average = 29.1159
	minimum = 0
	maximum = 736
Injected packet rate average = 0.0422219
	minimum = 0.0204 (at node 124)
	maximum = 0.0522 (at node 94)
Accepted packet rate average = 0.0162479
	minimum = 0.0114 (at node 79)
	maximum = 0.0212 (at node 103)
Injected flit rate average = 0.760051
	minimum = 0.367 (at node 124)
	maximum = 0.9392 (at node 175)
Accepted flit rate average= 0.295324
	minimum = 0.2076 (at node 79)
	maximum = 0.3704 (at node 103)
Injected packet length average = 18.0014
Accepted packet length average = 18.1761
Total in-flight flits = 768292 (678582 measured)
latency change    = 0.345956
throughput change = 0.0138892
Class 0:
Packet latency average = 1507.74
	minimum = 22
	maximum = 6586
Network latency average = 1088.13
	minimum = 22
	maximum = 5977
Slowest packet = 27796
Flit latency average = 3550.69
	minimum = 5
	maximum = 7434
Slowest flit = 241415
Fragmentation average = 71.9707
	minimum = 0
	maximum = 746
Injected packet rate average = 0.0388524
	minimum = 0.0193333 (at node 124)
	maximum = 0.0496667 (at node 43)
Accepted packet rate average = 0.0160686
	minimum = 0.0123333 (at node 67)
	maximum = 0.02 (at node 123)
Injected flit rate average = 0.699425
	minimum = 0.348667 (at node 124)
	maximum = 0.891667 (at node 43)
Accepted flit rate average= 0.291439
	minimum = 0.222667 (at node 67)
	maximum = 0.3745 (at node 123)
Injected packet length average = 18.0021
Accepted packet length average = 18.1372
Total in-flight flits = 792187 (740909 measured)
latency change    = 0.422777
throughput change = 0.0133294
Class 0:
Packet latency average = 2524.6
	minimum = 22
	maximum = 7701
Network latency average = 2061.51
	minimum = 22
	maximum = 6993
Slowest packet = 27796
Flit latency average = 3897.22
	minimum = 5
	maximum = 8064
Slowest flit = 321226
Fragmentation average = 134.225
	minimum = 0
	maximum = 821
Injected packet rate average = 0.0359189
	minimum = 0.0175714 (at node 116)
	maximum = 0.0474286 (at node 43)
Accepted packet rate average = 0.0159457
	minimum = 0.012 (at node 67)
	maximum = 0.02 (at node 123)
Injected flit rate average = 0.64677
	minimum = 0.317857 (at node 116)
	maximum = 0.853 (at node 43)
Accepted flit rate average= 0.288872
	minimum = 0.219714 (at node 140)
	maximum = 0.36 (at node 123)
Injected packet length average = 18.0064
Accepted packet length average = 18.116
Total in-flight flits = 803167 (781433 measured)
latency change    = 0.402782
throughput change = 0.00888702
Draining all recorded packets ...
Class 0:
Remaining flits: 377945 414139 414140 414141 414142 414143 417750 417751 417752 417753 [...] (806953 flits)
Measured flits: 500398 500399 500416 500417 500454 500455 500456 500457 500458 500459 [...] (802339 flits)
Class 0:
Remaining flits: 441090 441091 441092 441093 441094 441095 441096 441097 441098 441099 [...] (802752 flits)
Measured flits: 500454 500455 500456 500457 500458 500459 500460 500461 500462 500463 [...] (801860 flits)
Class 0:
Remaining flits: 463869 463870 463871 463872 463873 463874 463875 463876 463877 475192 [...] (801353 flits)
Measured flits: 500462 500463 500464 500465 500466 500467 500468 500469 500470 500471 [...] (798139 flits)
Class 0:
Remaining flits: 500866 500867 503244 503245 503246 503247 503248 503249 503250 503251 [...] (801242 flits)
Measured flits: 500866 500867 503244 503245 503246 503247 503248 503249 503250 503251 [...] (790856 flits)
Class 0:
Remaining flits: 503244 503245 503246 503247 503248 503249 503250 503251 503252 503253 [...] (810517 flits)
Measured flits: 503244 503245 503246 503247 503248 503249 503250 503251 503252 503253 [...] (783067 flits)
Class 0:
Remaining flits: 503246 503247 503248 503249 503250 503251 503252 503253 503254 503255 [...] (818216 flits)
Measured flits: 503246 503247 503248 503249 503250 503251 503252 503253 503254 503255 [...] (759608 flits)
Class 0:
Remaining flits: 579024 579025 579026 579027 579028 579029 579030 579031 579032 579033 [...] (824748 flits)
Measured flits: 579024 579025 579026 579027 579028 579029 579030 579031 579032 579033 [...] (729492 flits)
Class 0:
Remaining flits: 579031 579032 579033 579034 579035 579036 579037 579038 579039 579040 [...] (830395 flits)
Measured flits: 579031 579032 579033 579034 579035 579036 579037 579038 579039 579040 [...] (693241 flits)
Class 0:
Remaining flits: 606235 606236 606237 606238 606239 646592 646593 646594 646595 646650 [...] (832840 flits)
Measured flits: 606235 606236 606237 606238 606239 646592 646593 646594 646595 646650 [...] (655038 flits)
Class 0:
Remaining flits: 646650 646651 646652 646653 646654 646655 646656 646657 646658 646659 [...] (829941 flits)
Measured flits: 646650 646651 646652 646653 646654 646655 646656 646657 646658 646659 [...] (616730 flits)
Class 0:
Remaining flits: 673857 673858 673859 673860 673861 673862 673863 673864 673865 685962 [...] (833440 flits)
Measured flits: 673857 673858 673859 673860 673861 673862 673863 673864 673865 685962 [...] (578397 flits)
Class 0:
Remaining flits: 685962 685963 685964 685965 685966 685967 685968 685969 685970 685971 [...] (836654 flits)
Measured flits: 685962 685963 685964 685965 685966 685967 685968 685969 685970 685971 [...] (540435 flits)
Class 0:
Remaining flits: 736825 736826 736827 736828 736829 748332 748333 748334 748335 748336 [...] (839362 flits)
Measured flits: 736825 736826 736827 736828 736829 748332 748333 748334 748335 748336 [...] (503191 flits)
Class 0:
Remaining flits: 748332 748333 748334 748335 748336 748337 748338 748339 748340 748341 [...] (841150 flits)
Measured flits: 748332 748333 748334 748335 748336 748337 748338 748339 748340 748341 [...] (462368 flits)
Class 0:
Remaining flits: 781271 782964 782965 782966 782967 782968 782969 782970 782971 782972 [...] (841785 flits)
Measured flits: 781271 782964 782965 782966 782967 782968 782969 782970 782971 782972 [...] (423232 flits)
Class 0:
Remaining flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (838295 flits)
Measured flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (381060 flits)
Class 0:
Remaining flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (837193 flits)
Measured flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (337955 flits)
Class 0:
Remaining flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (839688 flits)
Measured flits: 859698 859699 859700 859701 859702 859703 859704 859705 859706 859707 [...] (294881 flits)
Class 0:
Remaining flits: 907164 907165 907166 907167 907168 907169 907170 907171 907172 907173 [...] (842471 flits)
Measured flits: 907164 907165 907166 907167 907168 907169 907170 907171 907172 907173 [...] (253254 flits)
Class 0:
Remaining flits: 907164 907165 907166 907167 907168 907169 907170 907171 907172 907173 [...] (847307 flits)
Measured flits: 907164 907165 907166 907167 907168 907169 907170 907171 907172 907173 [...] (211973 flits)
Class 0:
Remaining flits: 957042 957043 957044 957045 957046 957047 957048 957049 957050 957051 [...] (850176 flits)
Measured flits: 957042 957043 957044 957045 957046 957047 957048 957049 957050 957051 [...] (172764 flits)
Class 0:
Remaining flits: 986472 986473 986474 986475 986476 986477 986478 986479 986480 986481 [...] (848095 flits)
Measured flits: 986472 986473 986474 986475 986476 986477 986478 986479 986480 986481 [...] (136217 flits)
Class 0:
Remaining flits: 1061370 1061371 1061372 1061373 1061374 1061375 1061376 1061377 1061378 1061379 [...] (845515 flits)
Measured flits: 1061370 1061371 1061372 1061373 1061374 1061375 1061376 1061377 1061378 1061379 [...] (104307 flits)
Class 0:
Remaining flits: 1118034 1118035 1118036 1118037 1118038 1118039 1118040 1118041 1118042 1118043 [...] (844626 flits)
Measured flits: 1118034 1118035 1118036 1118037 1118038 1118039 1118040 1118041 1118042 1118043 [...] (76888 flits)
Class 0:
Remaining flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (849292 flits)
Measured flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (54685 flits)
Class 0:
Remaining flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (853882 flits)
Measured flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (36959 flits)
Class 0:
Remaining flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (853627 flits)
Measured flits: 1120248 1120249 1120250 1120251 1120252 1120253 1120254 1120255 1120256 1120257 [...] (24457 flits)
Class 0:
Remaining flits: 1120265 1325808 1325809 1325810 1325811 1325812 1325813 1325814 1325815 1325816 [...] (854901 flits)
Measured flits: 1120265 1325808 1325809 1325810 1325811 1325812 1325813 1325814 1325815 1325816 [...] (15504 flits)
Class 0:
Remaining flits: 1325808 1325809 1325810 1325811 1325812 1325813 1325814 1325815 1325816 1325817 [...] (853381 flits)
Measured flits: 1325808 1325809 1325810 1325811 1325812 1325813 1325814 1325815 1325816 1325817 [...] (9432 flits)
Class 0:
Remaining flits: 1327572 1327573 1327574 1327575 1327576 1327577 1327578 1327579 1327580 1327581 [...] (851918 flits)
Measured flits: 1327572 1327573 1327574 1327575 1327576 1327577 1327578 1327579 1327580 1327581 [...] (5804 flits)
Class 0:
Remaining flits: 1327585 1327586 1327587 1327588 1327589 1328418 1328419 1328420 1328421 1328422 [...] (852236 flits)
Measured flits: 1327585 1327586 1327587 1327588 1327589 1328418 1328419 1328420 1328421 1328422 [...] (3130 flits)
Class 0:
Remaining flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (857284 flits)
Measured flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (1743 flits)
Class 0:
Remaining flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (857466 flits)
Measured flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (931 flits)
Class 0:
Remaining flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (859386 flits)
Measured flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (433 flits)
Class 0:
Remaining flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (859941 flits)
Measured flits: 1381626 1381627 1381628 1381629 1381630 1381631 1381632 1381633 1381634 1381635 [...] (155 flits)
Class 0:
Remaining flits: 1684458 1684459 1684460 1684461 1684462 1684463 1684464 1684465 1684466 1684467 [...] (856593 flits)
Measured flits: 1730808 1730809 1730810 1730811 1730812 1730813 1730814 1730815 1730816 1730817 [...] (108 flits)
Class 0:
Remaining flits: 1684458 1684459 1684460 1684461 1684462 1684463 1684464 1684465 1684466 1684467 [...] (853857 flits)
Measured flits: 1730808 1730809 1730810 1730811 1730812 1730813 1730814 1730815 1730816 1730817 [...] (72 flits)
Class 0:
Remaining flits: 1730808 1730809 1730810 1730811 1730812 1730813 1730814 1730815 1730816 1730817 [...] (855884 flits)
Measured flits: 1730808 1730809 1730810 1730811 1730812 1730813 1730814 1730815 1730816 1730817 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1824534 1824535 1824536 1824537 1824538 1824539 1824540 1824541 1824542 1824543 [...] (806808 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1824534 1824535 1824536 1824537 1824538 1824539 1824540 1824541 1824542 1824543 [...] (756053 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1867104 1867105 1867106 1867107 1867108 1867109 1867110 1867111 1867112 1867113 [...] (705511 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1867119 1867120 1867121 1991376 1991377 1991378 1991379 1991380 1991381 1991382 [...] (655007 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1991378 1991379 1991380 1991381 1991382 1991383 1991384 1991385 1991386 1991387 [...] (605094 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1992420 1992421 1992422 1992423 1992424 1992425 1992426 1992427 1992428 1992429 [...] (555790 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2053188 2053189 2053190 2053191 2053192 2053193 2053194 2053195 2053196 2053197 [...] (507022 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2053188 2053189 2053190 2053191 2053192 2053193 2053194 2053195 2053196 2053197 [...] (458479 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2218302 2218303 2218304 2218305 2218306 2218307 2218308 2218309 2218310 2218311 [...] (410768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2218302 2218303 2218304 2218305 2218306 2218307 2218308 2218309 2218310 2218311 [...] (363112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2270502 2270503 2270504 2270505 2270506 2270507 2270508 2270509 2270510 2270511 [...] (315406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2270502 2270503 2270504 2270505 2270506 2270507 2270508 2270509 2270510 2270511 [...] (267488 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2397705 2397706 2397707 2398716 2398717 2398718 2398719 2398720 2398721 2398722 [...] (219615 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2425116 2425117 2425118 2425119 2425120 2425121 2490228 2490229 2490230 2490231 [...] (171592 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2490228 2490229 2490230 2490231 2490232 2490233 2490234 2490235 2490236 2490237 [...] (124002 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2490228 2490229 2490230 2490231 2490232 2490233 2490234 2490235 2490236 2490237 [...] (76651 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2580588 2580589 2580590 2580591 2580592 2580593 2580594 2580595 2580596 2580597 [...] (31271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2869578 2869579 2869580 2869581 2869582 2869583 2869584 2869585 2869586 2869587 [...] (5642 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3139681 3139682 3139683 3139684 3139685 3199824 3199825 3199826 3199827 3199828 [...] (307 flits)
Measured flits: (0 flits)
Time taken is 67641 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15074 (1 samples)
	minimum = 22 (1 samples)
	maximum = 38544 (1 samples)
Network latency average = 12544.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 34989 (1 samples)
Flit latency average = 13694 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34972 (1 samples)
Fragmentation average = 258.604 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1629 (1 samples)
Injected packet rate average = 0.0359189 (1 samples)
	minimum = 0.0175714 (1 samples)
	maximum = 0.0474286 (1 samples)
Accepted packet rate average = 0.0159457 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.64677 (1 samples)
	minimum = 0.317857 (1 samples)
	maximum = 0.853 (1 samples)
Accepted flit rate average = 0.288872 (1 samples)
	minimum = 0.219714 (1 samples)
	maximum = 0.36 (1 samples)
Injected packet size average = 18.0064 (1 samples)
Accepted packet size average = 18.116 (1 samples)
Hops average = 5.06053 (1 samples)
Total run time 143.703
