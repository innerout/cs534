BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.351
	minimum = 22
	maximum = 915
Network latency average = 253.259
	minimum = 22
	maximum = 839
Slowest packet = 627
Flit latency average = 223.276
	minimum = 5
	maximum = 822
Slowest flit = 15191
Fragmentation average = 22.5237
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0188646
	minimum = 0.01 (at node 120)
	maximum = 0.032 (at node 107)
Accepted packet rate average = 0.0136146
	minimum = 0.004 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.336203
	minimum = 0.18 (at node 120)
	maximum = 0.56 (at node 107)
Accepted flit rate average= 0.250896
	minimum = 0.087 (at node 174)
	maximum = 0.45 (at node 44)
Injected packet length average = 17.8219
Accepted packet length average = 18.4285
Total in-flight flits = 19004 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 662.002
	minimum = 22
	maximum = 1754
Network latency average = 308.475
	minimum = 22
	maximum = 1270
Slowest packet = 983
Flit latency average = 274.815
	minimum = 5
	maximum = 1210
Slowest flit = 49161
Fragmentation average = 22.7752
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0164297
	minimum = 0.009 (at node 116)
	maximum = 0.0255 (at node 103)
Accepted packet rate average = 0.013875
	minimum = 0.0075 (at node 153)
	maximum = 0.02 (at node 98)
Injected flit rate average = 0.293685
	minimum = 0.1545 (at node 116)
	maximum = 0.459 (at node 103)
Accepted flit rate average= 0.252055
	minimum = 0.135 (at node 153)
	maximum = 0.3615 (at node 22)
Injected packet length average = 17.8753
Accepted packet length average = 18.1661
Total in-flight flits = 18699 (0 measured)
latency change    = 0.488897
throughput change = 0.00459763
Class 0:
Packet latency average = 1641.69
	minimum = 688
	maximum = 2581
Network latency average = 362.725
	minimum = 22
	maximum = 1694
Slowest packet = 4386
Flit latency average = 326.709
	minimum = 5
	maximum = 1672
Slowest flit = 60335
Fragmentation average = 22.6321
	minimum = 0
	maximum = 118
Injected packet rate average = 0.013849
	minimum = 0.002 (at node 154)
	maximum = 0.032 (at node 37)
Accepted packet rate average = 0.0140313
	minimum = 0.005 (at node 190)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.250542
	minimum = 0.036 (at node 154)
	maximum = 0.576 (at node 37)
Accepted flit rate average= 0.252844
	minimum = 0.09 (at node 190)
	maximum = 0.462 (at node 16)
Injected packet length average = 18.091
Accepted packet length average = 18.02
Total in-flight flits = 18285 (0 measured)
latency change    = 0.596756
throughput change = 0.00312075
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2404.76
	minimum = 1258
	maximum = 3331
Network latency average = 278.43
	minimum = 22
	maximum = 929
Slowest packet = 9092
Flit latency average = 331.228
	minimum = 5
	maximum = 1793
Slowest flit = 114425
Fragmentation average = 21.3533
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0141563
	minimum = 0.001 (at node 114)
	maximum = 0.029 (at node 101)
Accepted packet rate average = 0.0139063
	minimum = 0.006 (at node 71)
	maximum = 0.024 (at node 181)
Injected flit rate average = 0.254344
	minimum = 0.018 (at node 114)
	maximum = 0.522 (at node 101)
Accepted flit rate average= 0.25001
	minimum = 0.108 (at node 71)
	maximum = 0.424 (at node 181)
Injected packet length average = 17.9669
Accepted packet length average = 17.9783
Total in-flight flits = 19135 (18829 measured)
latency change    = 0.317315
throughput change = 0.0113329
Class 0:
Packet latency average = 2767.88
	minimum = 1258
	maximum = 4208
Network latency average = 327.89
	minimum = 22
	maximum = 1739
Slowest packet = 9092
Flit latency average = 329.22
	minimum = 5
	maximum = 2120
Slowest flit = 126575
Fragmentation average = 22.1302
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0140651
	minimum = 0.0035 (at node 114)
	maximum = 0.023 (at node 190)
Accepted packet rate average = 0.0140026
	minimum = 0.0095 (at node 4)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.252544
	minimum = 0.0565 (at node 114)
	maximum = 0.414 (at node 190)
Accepted flit rate average= 0.25193
	minimum = 0.168 (at node 84)
	maximum = 0.378 (at node 129)
Injected packet length average = 17.9554
Accepted packet length average = 17.9916
Total in-flight flits = 18366 (18348 measured)
latency change    = 0.131192
throughput change = 0.00761828
Class 0:
Packet latency average = 3108.55
	minimum = 1258
	maximum = 4817
Network latency average = 340.591
	minimum = 22
	maximum = 1739
Slowest packet = 9092
Flit latency average = 328.35
	minimum = 5
	maximum = 2540
Slowest flit = 135664
Fragmentation average = 22.1216
	minimum = 0
	maximum = 124
Injected packet rate average = 0.013941
	minimum = 0.005 (at node 162)
	maximum = 0.0213333 (at node 84)
Accepted packet rate average = 0.0138993
	minimum = 0.00966667 (at node 36)
	maximum = 0.0196667 (at node 128)
Injected flit rate average = 0.250778
	minimum = 0.09 (at node 162)
	maximum = 0.384 (at node 84)
Accepted flit rate average= 0.250561
	minimum = 0.170667 (at node 36)
	maximum = 0.357667 (at node 128)
Injected packet length average = 17.9885
Accepted packet length average = 18.0269
Total in-flight flits = 18340 (18340 measured)
latency change    = 0.109591
throughput change = 0.00546344
Class 0:
Packet latency average = 3444.67
	minimum = 1258
	maximum = 5474
Network latency average = 346.017
	minimum = 22
	maximum = 1910
Slowest packet = 9092
Flit latency average = 327.798
	minimum = 5
	maximum = 2540
Slowest flit = 135664
Fragmentation average = 21.8079
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0138789
	minimum = 0.00425 (at node 162)
	maximum = 0.02 (at node 84)
Accepted packet rate average = 0.0138477
	minimum = 0.00975 (at node 36)
	maximum = 0.01825 (at node 118)
Injected flit rate average = 0.249618
	minimum = 0.0765 (at node 162)
	maximum = 0.3575 (at node 125)
Accepted flit rate average= 0.249314
	minimum = 0.173 (at node 36)
	maximum = 0.33225 (at node 118)
Injected packet length average = 17.9855
Accepted packet length average = 18.004
Total in-flight flits = 18476 (18476 measured)
latency change    = 0.0975769
throughput change = 0.00500158
Class 0:
Packet latency average = 3783.6
	minimum = 1258
	maximum = 6497
Network latency average = 350.492
	minimum = 22
	maximum = 2287
Slowest packet = 9092
Flit latency average = 328.426
	minimum = 5
	maximum = 2540
Slowest flit = 135664
Fragmentation average = 21.7845
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0138531
	minimum = 0.0058 (at node 114)
	maximum = 0.0198 (at node 84)
Accepted packet rate average = 0.0138427
	minimum = 0.0102 (at node 104)
	maximum = 0.0178 (at node 128)
Injected flit rate average = 0.249196
	minimum = 0.1044 (at node 114)
	maximum = 0.3556 (at node 84)
Accepted flit rate average= 0.249099
	minimum = 0.182 (at node 104)
	maximum = 0.3188 (at node 128)
Injected packet length average = 17.9884
Accepted packet length average = 17.995
Total in-flight flits = 18280 (18280 measured)
latency change    = 0.0895787
throughput change = 0.000862484
Class 0:
Packet latency average = 4129.01
	minimum = 1258
	maximum = 7252
Network latency average = 353.624
	minimum = 22
	maximum = 2383
Slowest packet = 9092
Flit latency average = 328.873
	minimum = 5
	maximum = 2540
Slowest flit = 135664
Fragmentation average = 22.0572
	minimum = 0
	maximum = 133
Injected packet rate average = 0.0139497
	minimum = 0.00666667 (at node 114)
	maximum = 0.0195 (at node 84)
Accepted packet rate average = 0.0139132
	minimum = 0.0111667 (at node 42)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.250974
	minimum = 0.118833 (at node 114)
	maximum = 0.351 (at node 84)
Accepted flit rate average= 0.250593
	minimum = 0.201 (at node 42)
	maximum = 0.315333 (at node 128)
Injected packet length average = 17.9914
Accepted packet length average = 18.0112
Total in-flight flits = 18628 (18628 measured)
latency change    = 0.0836548
throughput change = 0.00596156
Class 0:
Packet latency average = 4462.33
	minimum = 1258
	maximum = 7969
Network latency average = 354.758
	minimum = 22
	maximum = 2383
Slowest packet = 9092
Flit latency average = 328.298
	minimum = 5
	maximum = 2540
Slowest flit = 135664
Fragmentation average = 22.0928
	minimum = 0
	maximum = 133
Injected packet rate average = 0.0139531
	minimum = 0.00814286 (at node 31)
	maximum = 0.0192857 (at node 84)
Accepted packet rate average = 0.0139301
	minimum = 0.0114286 (at node 104)
	maximum = 0.0181429 (at node 138)
Injected flit rate average = 0.251051
	minimum = 0.146143 (at node 31)
	maximum = 0.347143 (at node 84)
Accepted flit rate average= 0.250778
	minimum = 0.204429 (at node 104)
	maximum = 0.322571 (at node 138)
Injected packet length average = 17.9925
Accepted packet length average = 18.0026
Total in-flight flits = 18524 (18524 measured)
latency change    = 0.074696
throughput change = 0.000736301
Draining all recorded packets ...
Class 0:
Remaining flits: 450564 450565 450566 450567 450568 450569 450570 450571 450572 450573 [...] (18744 flits)
Measured flits: 450564 450565 450566 450567 450568 450569 450570 450571 450572 450573 [...] (18744 flits)
Class 0:
Remaining flits: 518292 518293 518294 518295 518296 518297 518298 518299 518300 518301 [...] (18237 flits)
Measured flits: 518292 518293 518294 518295 518296 518297 518298 518299 518300 518301 [...] (18237 flits)
Class 0:
Remaining flits: 573606 573607 573608 573609 573610 573611 573612 573613 573614 573615 [...] (18420 flits)
Measured flits: 573606 573607 573608 573609 573610 573611 573612 573613 573614 573615 [...] (18420 flits)
Class 0:
Remaining flits: 606240 606241 606242 606243 606244 606245 606246 606247 606248 606249 [...] (18698 flits)
Measured flits: 606240 606241 606242 606243 606244 606245 606246 606247 606248 606249 [...] (18698 flits)
Class 0:
Remaining flits: 681858 681859 681860 681861 681862 681863 681864 681865 681866 681867 [...] (18641 flits)
Measured flits: 681858 681859 681860 681861 681862 681863 681864 681865 681866 681867 [...] (18641 flits)
Class 0:
Remaining flits: 713034 713035 713036 713037 713038 713039 713040 713041 713042 713043 [...] (18355 flits)
Measured flits: 713034 713035 713036 713037 713038 713039 713040 713041 713042 713043 [...] (18355 flits)
Class 0:
Remaining flits: 751194 751195 751196 751197 751198 751199 751200 751201 751202 751203 [...] (18540 flits)
Measured flits: 751194 751195 751196 751197 751198 751199 751200 751201 751202 751203 [...] (18540 flits)
Class 0:
Remaining flits: 816804 816805 816806 816807 816808 816809 816810 816811 816812 816813 [...] (18981 flits)
Measured flits: 816804 816805 816806 816807 816808 816809 816810 816811 816812 816813 [...] (18981 flits)
Class 0:
Remaining flits: 851974 851975 852732 852733 852734 852735 852736 852737 852738 852739 [...] (18520 flits)
Measured flits: 851974 851975 852732 852733 852734 852735 852736 852737 852738 852739 [...] (18520 flits)
Class 0:
Remaining flits: 908730 908731 908732 908733 908734 908735 908736 908737 908738 908739 [...] (18578 flits)
Measured flits: 908730 908731 908732 908733 908734 908735 908736 908737 908738 908739 [...] (18578 flits)
Class 0:
Remaining flits: 946224 946225 946226 946227 946228 946229 946230 946231 946232 946233 [...] (18098 flits)
Measured flits: 946224 946225 946226 946227 946228 946229 946230 946231 946232 946233 [...] (18098 flits)
Class 0:
Remaining flits: 989262 989263 989264 989265 989266 989267 989268 989269 989270 989271 [...] (18082 flits)
Measured flits: 989262 989263 989264 989265 989266 989267 989268 989269 989270 989271 [...] (18082 flits)
Class 0:
Remaining flits: 1025555 1025556 1025557 1025558 1025559 1025560 1025561 1025562 1025563 1025564 [...] (18356 flits)
Measured flits: 1025555 1025556 1025557 1025558 1025559 1025560 1025561 1025562 1025563 1025564 [...] (18356 flits)
Class 0:
Remaining flits: 1095678 1095679 1095680 1095681 1095682 1095683 1095684 1095685 1095686 1095687 [...] (18587 flits)
Measured flits: 1095678 1095679 1095680 1095681 1095682 1095683 1095684 1095685 1095686 1095687 [...] (18515 flits)
Class 0:
Remaining flits: 1115784 1115785 1115786 1115787 1115788 1115789 1115790 1115791 1115792 1115793 [...] (18091 flits)
Measured flits: 1115784 1115785 1115786 1115787 1115788 1115789 1115790 1115791 1115792 1115793 [...] (17877 flits)
Class 0:
Remaining flits: 1185894 1185895 1185896 1185897 1185898 1185899 1185900 1185901 1185902 1185903 [...] (18515 flits)
Measured flits: 1185894 1185895 1185896 1185897 1185898 1185899 1185900 1185901 1185902 1185903 [...] (17651 flits)
Class 0:
Remaining flits: 1227024 1227025 1227026 1227027 1227028 1227029 1227030 1227031 1227032 1227033 [...] (18359 flits)
Measured flits: 1227024 1227025 1227026 1227027 1227028 1227029 1227030 1227031 1227032 1227033 [...] (16931 flits)
Class 0:
Remaining flits: 1299654 1299655 1299656 1299657 1299658 1299659 1299660 1299661 1299662 1299663 [...] (18963 flits)
Measured flits: 1299654 1299655 1299656 1299657 1299658 1299659 1299660 1299661 1299662 1299663 [...] (16335 flits)
Class 0:
Remaining flits: 1323954 1323955 1323956 1323957 1323958 1323959 1323960 1323961 1323962 1323963 [...] (18112 flits)
Measured flits: 1323954 1323955 1323956 1323957 1323958 1323959 1323960 1323961 1323962 1323963 [...] (12729 flits)
Class 0:
Remaining flits: 1400201 1400616 1400617 1400618 1400619 1400620 1400621 1400622 1400623 1400624 [...] (18642 flits)
Measured flits: 1400616 1400617 1400618 1400619 1400620 1400621 1400622 1400623 1400624 1400625 [...] (10449 flits)
Class 0:
Remaining flits: 1435032 1435033 1435034 1435035 1435036 1435037 1435038 1435039 1435040 1435041 [...] (18069 flits)
Measured flits: 1435032 1435033 1435034 1435035 1435036 1435037 1435038 1435039 1435040 1435041 [...] (7263 flits)
Class 0:
Remaining flits: 1481544 1481545 1481546 1481547 1481548 1481549 1481550 1481551 1481552 1481553 [...] (18227 flits)
Measured flits: 1488780 1488781 1488782 1488783 1488784 1488785 1488786 1488787 1488788 1488789 [...] (4329 flits)
Class 0:
Remaining flits: 1527418 1527419 1527420 1527421 1527422 1527423 1527424 1527425 1531890 1531891 [...] (18374 flits)
Measured flits: 1547426 1547427 1547428 1547429 1547430 1547431 1547432 1547433 1547434 1547435 [...] (2290 flits)
Class 0:
Remaining flits: 1586879 1589968 1589969 1589970 1589971 1589972 1589973 1589974 1589975 1591848 [...] (18592 flits)
Measured flits: 1604070 1604071 1604072 1604073 1604074 1604075 1604076 1604077 1604078 1604079 [...] (1327 flits)
Class 0:
Remaining flits: 1632690 1632691 1632692 1632693 1632694 1632695 1632696 1632697 1632698 1632699 [...] (19049 flits)
Measured flits: 1660140 1660141 1660142 1660143 1660144 1660145 1660146 1660147 1660148 1660149 [...] (814 flits)
Class 0:
Remaining flits: 1675108 1675109 1675110 1675111 1675112 1675113 1675114 1675115 1679940 1679941 [...] (18667 flits)
Measured flits: 1687225 1687226 1687227 1687228 1687229 1688771 1688772 1688773 1688774 1688775 [...] (409 flits)
Class 0:
Remaining flits: 1734480 1734481 1734482 1734483 1734484 1734485 1734486 1734487 1734488 1734489 [...] (17968 flits)
Measured flits: 1760416 1760417 1780956 1780957 1780958 1780959 1780960 1780961 1780962 1780963 [...] (226 flits)
Draining remaining packets ...
Time taken is 38304 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11512.2 (1 samples)
	minimum = 1258 (1 samples)
	maximum = 27711 (1 samples)
Network latency average = 365.054 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2383 (1 samples)
Flit latency average = 330.181 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2540 (1 samples)
Fragmentation average = 22.46 (1 samples)
	minimum = 0 (1 samples)
	maximum = 138 (1 samples)
Injected packet rate average = 0.0139531 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0192857 (1 samples)
Accepted packet rate average = 0.0139301 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0181429 (1 samples)
Injected flit rate average = 0.251051 (1 samples)
	minimum = 0.146143 (1 samples)
	maximum = 0.347143 (1 samples)
Accepted flit rate average = 0.250778 (1 samples)
	minimum = 0.204429 (1 samples)
	maximum = 0.322571 (1 samples)
Injected packet size average = 17.9925 (1 samples)
Accepted packet size average = 18.0026 (1 samples)
Hops average = 5.0695 (1 samples)
Total run time 33.9599
