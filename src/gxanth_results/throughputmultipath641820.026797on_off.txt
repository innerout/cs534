BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 325.489
	minimum = 22
	maximum = 951
Network latency average = 222.939
	minimum = 22
	maximum = 815
Slowest packet = 74
Flit latency average = 188.898
	minimum = 5
	maximum = 803
Slowest flit = 14631
Fragmentation average = 55.4176
	minimum = 0
	maximum = 438
Injected packet rate average = 0.0245625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 46)
Accepted packet rate average = 0.0133958
	minimum = 0.006 (at node 135)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.437453
	minimum = 0 (at node 5)
	maximum = 1 (at node 46)
Accepted flit rate average= 0.255385
	minimum = 0.108 (at node 135)
	maximum = 0.426 (at node 48)
Injected packet length average = 17.8098
Accepted packet length average = 19.0645
Total in-flight flits = 35908 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 551.36
	minimum = 22
	maximum = 1934
Network latency average = 405.299
	minimum = 22
	maximum = 1570
Slowest packet = 74
Flit latency average = 364.302
	minimum = 5
	maximum = 1553
Slowest flit = 34037
Fragmentation average = 76.0439
	minimum = 0
	maximum = 707
Injected packet rate average = 0.0230443
	minimum = 0.001 (at node 66)
	maximum = 0.046 (at node 46)
Accepted packet rate average = 0.0141302
	minimum = 0.008 (at node 164)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.41249
	minimum = 0.018 (at node 66)
	maximum = 0.827 (at node 46)
Accepted flit rate average= 0.263227
	minimum = 0.1495 (at node 164)
	maximum = 0.3945 (at node 152)
Injected packet length average = 17.8999
Accepted packet length average = 18.6286
Total in-flight flits = 58473 (0 measured)
latency change    = 0.409662
throughput change = 0.0297886
Class 0:
Packet latency average = 1136.8
	minimum = 22
	maximum = 2744
Network latency average = 897.452
	minimum = 22
	maximum = 2191
Slowest packet = 4179
Flit latency average = 841.755
	minimum = 5
	maximum = 2174
Slowest flit = 53315
Fragmentation average = 102.148
	minimum = 0
	maximum = 646
Injected packet rate average = 0.0201615
	minimum = 0 (at node 19)
	maximum = 0.043 (at node 77)
Accepted packet rate average = 0.0153854
	minimum = 0.004 (at node 138)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.362812
	minimum = 0 (at node 19)
	maximum = 0.78 (at node 77)
Accepted flit rate average= 0.275724
	minimum = 0.072 (at node 138)
	maximum = 0.491 (at node 182)
Injected packet length average = 17.9954
Accepted packet length average = 17.9211
Total in-flight flits = 75248 (0 measured)
latency change    = 0.514988
throughput change = 0.0453258
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 691.185
	minimum = 23
	maximum = 2671
Network latency average = 199.772
	minimum = 22
	maximum = 971
Slowest packet = 12762
Flit latency average = 1142.71
	minimum = 5
	maximum = 2839
Slowest flit = 97966
Fragmentation average = 21.1432
	minimum = 0
	maximum = 312
Injected packet rate average = 0.0195625
	minimum = 0.003 (at node 73)
	maximum = 0.042 (at node 37)
Accepted packet rate average = 0.015224
	minimum = 0.006 (at node 149)
	maximum = 0.033 (at node 129)
Injected flit rate average = 0.351375
	minimum = 0.037 (at node 73)
	maximum = 0.75 (at node 37)
Accepted flit rate average= 0.274938
	minimum = 0.119 (at node 149)
	maximum = 0.569 (at node 129)
Injected packet length average = 17.9617
Accepted packet length average = 18.0595
Total in-flight flits = 90284 (59611 measured)
latency change    = 0.644706
throughput change = 0.0028605
Class 0:
Packet latency average = 1362.61
	minimum = 23
	maximum = 3844
Network latency average = 820.542
	minimum = 22
	maximum = 1980
Slowest packet = 12762
Flit latency average = 1276.17
	minimum = 5
	maximum = 3263
Slowest flit = 139451
Fragmentation average = 70.6328
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0180703
	minimum = 0.0055 (at node 53)
	maximum = 0.033 (at node 187)
Accepted packet rate average = 0.015026
	minimum = 0.0095 (at node 2)
	maximum = 0.025 (at node 0)
Injected flit rate average = 0.325219
	minimum = 0.099 (at node 53)
	maximum = 0.589 (at node 187)
Accepted flit rate average= 0.271615
	minimum = 0.172 (at node 26)
	maximum = 0.4485 (at node 0)
Injected packet length average = 17.9974
Accepted packet length average = 18.0763
Total in-flight flits = 96066 (90147 measured)
latency change    = 0.492749
throughput change = 0.0122339
Class 0:
Packet latency average = 1901.15
	minimum = 23
	maximum = 4900
Network latency average = 1245.09
	minimum = 22
	maximum = 2954
Slowest packet = 12762
Flit latency average = 1388.6
	minimum = 5
	maximum = 3801
Slowest flit = 170225
Fragmentation average = 87.9333
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0173872
	minimum = 0.00533333 (at node 168)
	maximum = 0.0303333 (at node 2)
Accepted packet rate average = 0.0150781
	minimum = 0.00966667 (at node 36)
	maximum = 0.021 (at node 0)
Injected flit rate average = 0.312748
	minimum = 0.101667 (at node 168)
	maximum = 0.546667 (at node 2)
Accepted flit rate average= 0.27172
	minimum = 0.180333 (at node 36)
	maximum = 0.380667 (at node 128)
Injected packet length average = 17.9873
Accepted packet length average = 18.0208
Total in-flight flits = 99313 (98871 measured)
latency change    = 0.28327
throughput change = 0.000389749
Draining remaining packets ...
Class 0:
Remaining flits: 220716 220717 220718 220719 220720 220721 220722 220723 220724 220725 [...] (51771 flits)
Measured flits: 231858 231859 231860 231861 231862 231863 231864 231865 231866 231867 [...] (51753 flits)
Class 0:
Remaining flits: 291515 291516 291517 291518 291519 291520 291521 291522 291523 291524 [...] (6676 flits)
Measured flits: 291515 291516 291517 291518 291519 291520 291521 291522 291523 291524 [...] (6676 flits)
Time taken is 8515 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2663.41 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7025 (1 samples)
Network latency average = 1757.1 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4499 (1 samples)
Flit latency average = 1656.88 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4438 (1 samples)
Fragmentation average = 98.6137 (1 samples)
	minimum = 0 (1 samples)
	maximum = 696 (1 samples)
Injected packet rate average = 0.0173872 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0303333 (1 samples)
Accepted packet rate average = 0.0150781 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.312748 (1 samples)
	minimum = 0.101667 (1 samples)
	maximum = 0.546667 (1 samples)
Accepted flit rate average = 0.27172 (1 samples)
	minimum = 0.180333 (1 samples)
	maximum = 0.380667 (1 samples)
Injected packet size average = 17.9873 (1 samples)
Accepted packet size average = 18.0208 (1 samples)
Hops average = 5.13128 (1 samples)
Total run time 12.226
