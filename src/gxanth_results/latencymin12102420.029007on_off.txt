BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 323.057
	minimum = 22
	maximum = 972
Network latency average = 217.352
	minimum = 22
	maximum = 712
Slowest packet = 4
Flit latency average = 195.058
	minimum = 5
	maximum = 726
Slowest flit = 21638
Fragmentation average = 18.3655
	minimum = 0
	maximum = 99
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0137083
	minimum = 0.007 (at node 25)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.252354
	minimum = 0.126 (at node 25)
	maximum = 0.424 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 18.4088
Total in-flight flits = 39766 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 548.451
	minimum = 22
	maximum = 1804
Network latency average = 403.949
	minimum = 22
	maximum = 1410
Slowest packet = 4
Flit latency average = 380.993
	minimum = 5
	maximum = 1393
Slowest flit = 41003
Fragmentation average = 19.4083
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0268229
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.0146563
	minimum = 0.008 (at node 153)
	maximum = 0.02 (at node 166)
Injected flit rate average = 0.480539
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.26662
	minimum = 0.144 (at node 153)
	maximum = 0.36 (at node 166)
Injected packet length average = 17.9152
Accepted packet length average = 18.1915
Total in-flight flits = 83018 (0 measured)
latency change    = 0.410965
throughput change = 0.0535055
Class 0:
Packet latency average = 1142.06
	minimum = 25
	maximum = 2739
Network latency average = 937.75
	minimum = 22
	maximum = 2113
Slowest packet = 4062
Flit latency average = 918.714
	minimum = 5
	maximum = 2096
Slowest flit = 69155
Fragmentation average = 21.5302
	minimum = 0
	maximum = 111
Injected packet rate average = 0.0293333
	minimum = 0 (at node 90)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0159531
	minimum = 0.008 (at node 174)
	maximum = 0.025 (at node 152)
Injected flit rate average = 0.527948
	minimum = 0 (at node 90)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.28751
	minimum = 0.145 (at node 4)
	maximum = 0.448 (at node 51)
Injected packet length average = 17.9982
Accepted packet length average = 18.0222
Total in-flight flits = 129192 (0 measured)
latency change    = 0.519769
throughput change = 0.0726604
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 344.623
	minimum = 25
	maximum = 2065
Network latency average = 95.8312
	minimum = 22
	maximum = 927
Slowest packet = 15950
Flit latency average = 1353.89
	minimum = 5
	maximum = 2774
Slowest flit = 108465
Fragmentation average = 5.45671
	minimum = 0
	maximum = 56
Injected packet rate average = 0.0292083
	minimum = 0 (at node 92)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0158177
	minimum = 0.006 (at node 35)
	maximum = 0.028 (at node 28)
Injected flit rate average = 0.525432
	minimum = 0 (at node 92)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.284714
	minimum = 0.108 (at node 35)
	maximum = 0.495 (at node 28)
Injected packet length average = 17.9891
Accepted packet length average = 17.9997
Total in-flight flits = 175471 (92532 measured)
latency change    = 2.31393
throughput change = 0.00982347
Class 0:
Packet latency average = 650.784
	minimum = 25
	maximum = 2911
Network latency average = 432.748
	minimum = 22
	maximum = 1973
Slowest packet = 15950
Flit latency average = 1560.34
	minimum = 5
	maximum = 3468
Slowest flit = 136961
Fragmentation average = 9.78815
	minimum = 0
	maximum = 94
Injected packet rate average = 0.0295286
	minimum = 0.004 (at node 92)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0157734
	minimum = 0.0075 (at node 35)
	maximum = 0.0235 (at node 128)
Injected flit rate average = 0.531664
	minimum = 0.072 (at node 92)
	maximum = 1 (at node 13)
Accepted flit rate average= 0.283555
	minimum = 0.1385 (at node 35)
	maximum = 0.4215 (at node 128)
Injected packet length average = 18.005
Accepted packet length average = 17.9767
Total in-flight flits = 224409 (181651 measured)
latency change    = 0.470449
throughput change = 0.00408688
Class 0:
Packet latency average = 1249.67
	minimum = 22
	maximum = 3529
Network latency average = 1044.7
	minimum = 22
	maximum = 2977
Slowest packet = 15950
Flit latency average = 1759.31
	minimum = 5
	maximum = 4178
Slowest flit = 156381
Fragmentation average = 14.7994
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0292361
	minimum = 0.004 (at node 92)
	maximum = 0.0553333 (at node 63)
Accepted packet rate average = 0.0158247
	minimum = 0.00933333 (at node 35)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.526403
	minimum = 0.072 (at node 92)
	maximum = 1 (at node 63)
Accepted flit rate average= 0.284845
	minimum = 0.173 (at node 139)
	maximum = 0.399333 (at node 128)
Injected packet length average = 18.0052
Accepted packet length average = 18.0001
Total in-flight flits = 268241 (251539 measured)
latency change    = 0.479235
throughput change = 0.00453157
Class 0:
Packet latency average = 1862.44
	minimum = 22
	maximum = 4602
Network latency average = 1658.49
	minimum = 22
	maximum = 3967
Slowest packet = 15950
Flit latency average = 1977.51
	minimum = 5
	maximum = 5042
Slowest flit = 179941
Fragmentation average = 18.2834
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0287188
	minimum = 0.004 (at node 92)
	maximum = 0.0555 (at node 110)
Accepted packet rate average = 0.0157904
	minimum = 0.01075 (at node 162)
	maximum = 0.02075 (at node 118)
Injected flit rate average = 0.51702
	minimum = 0.072 (at node 92)
	maximum = 0.998 (at node 110)
Accepted flit rate average= 0.283979
	minimum = 0.1965 (at node 162)
	maximum = 0.3755 (at node 128)
Injected packet length average = 18.0029
Accepted packet length average = 17.9843
Total in-flight flits = 308104 (302226 measured)
latency change    = 0.329017
throughput change = 0.00305064
Class 0:
Packet latency average = 2337.09
	minimum = 22
	maximum = 5499
Network latency average = 2130.52
	minimum = 22
	maximum = 4934
Slowest packet = 15950
Flit latency average = 2200.43
	minimum = 5
	maximum = 5736
Slowest flit = 209616
Fragmentation average = 19.8732
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0286208
	minimum = 0.0078 (at node 92)
	maximum = 0.0528 (at node 110)
Accepted packet rate average = 0.0157875
	minimum = 0.0116 (at node 173)
	maximum = 0.0194 (at node 115)
Injected flit rate average = 0.51515
	minimum = 0.1404 (at node 92)
	maximum = 0.952 (at node 110)
Accepted flit rate average= 0.284143
	minimum = 0.2078 (at node 173)
	maximum = 0.3506 (at node 115)
Injected packet length average = 17.9991
Accepted packet length average = 17.998
Total in-flight flits = 350983 (348444 measured)
latency change    = 0.203094
throughput change = 0.000575562
Class 0:
Packet latency average = 2687.1
	minimum = 22
	maximum = 7361
Network latency average = 2474.38
	minimum = 22
	maximum = 5839
Slowest packet = 15950
Flit latency average = 2410.25
	minimum = 5
	maximum = 6499
Slowest flit = 229463
Fragmentation average = 20.6319
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0288342
	minimum = 0.012 (at node 89)
	maximum = 0.0503333 (at node 114)
Accepted packet rate average = 0.0158411
	minimum = 0.012 (at node 86)
	maximum = 0.0195 (at node 118)
Injected flit rate average = 0.518957
	minimum = 0.215167 (at node 89)
	maximum = 0.904167 (at node 114)
Accepted flit rate average= 0.285253
	minimum = 0.218833 (at node 86)
	maximum = 0.3465 (at node 118)
Injected packet length average = 17.998
Accepted packet length average = 18.0071
Total in-flight flits = 398486 (397349 measured)
latency change    = 0.130253
throughput change = 0.00389395
Class 0:
Packet latency average = 2976.14
	minimum = 22
	maximum = 8329
Network latency average = 2758.23
	minimum = 22
	maximum = 6974
Slowest packet = 15950
Flit latency average = 2612.91
	minimum = 5
	maximum = 7285
Slowest flit = 255524
Fragmentation average = 20.9973
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0289129
	minimum = 0.0122857 (at node 89)
	maximum = 0.0474286 (at node 179)
Accepted packet rate average = 0.0159152
	minimum = 0.0122857 (at node 190)
	maximum = 0.0191429 (at node 115)
Injected flit rate average = 0.520361
	minimum = 0.221143 (at node 89)
	maximum = 0.853714 (at node 179)
Accepted flit rate average= 0.286506
	minimum = 0.221143 (at node 190)
	maximum = 0.344571 (at node 115)
Injected packet length average = 17.9975
Accepted packet length average = 18.0021
Total in-flight flits = 443590 (443215 measured)
latency change    = 0.0971215
throughput change = 0.00437157
Draining all recorded packets ...
Class 0:
Remaining flits: 297162 297163 297164 297165 297166 297167 297168 297169 297170 297171 [...] (493423 flits)
Measured flits: 297162 297163 297164 297165 297166 297167 297168 297169 297170 297171 [...] (419548 flits)
Class 0:
Remaining flits: 329058 329059 329060 329061 329062 329063 329064 329065 329066 329067 [...] (537324 flits)
Measured flits: 329058 329059 329060 329061 329062 329063 329064 329065 329066 329067 [...] (372770 flits)
Class 0:
Remaining flits: 334512 334513 334514 334515 334516 334517 334518 334519 334520 334521 [...] (579413 flits)
Measured flits: 334512 334513 334514 334515 334516 334517 334518 334519 334520 334521 [...] (325612 flits)
Class 0:
Remaining flits: 355500 355501 355502 355503 355504 355505 355506 355507 355508 355509 [...] (615886 flits)
Measured flits: 355500 355501 355502 355503 355504 355505 355506 355507 355508 355509 [...] (278297 flits)
Class 0:
Remaining flits: 404478 404479 404480 404481 404482 404483 404484 404485 404486 404487 [...] (650056 flits)
Measured flits: 404478 404479 404480 404481 404482 404483 404484 404485 404486 404487 [...] (231269 flits)
Class 0:
Remaining flits: 410454 410455 410456 410457 410458 410459 410460 410461 410462 410463 [...] (687683 flits)
Measured flits: 410454 410455 410456 410457 410458 410459 410460 410461 410462 410463 [...] (184535 flits)
Class 0:
Remaining flits: 481428 481429 481430 481431 481432 481433 481434 481435 481436 481437 [...] (716106 flits)
Measured flits: 481428 481429 481430 481431 481432 481433 481434 481435 481436 481437 [...] (142000 flits)
Class 0:
Remaining flits: 522324 522325 522326 522327 522328 522329 522330 522331 522332 522333 [...] (750246 flits)
Measured flits: 522324 522325 522326 522327 522328 522329 522330 522331 522332 522333 [...] (105057 flits)
Class 0:
Remaining flits: 547920 547921 547922 547923 547924 547925 547926 547927 547928 547929 [...] (786629 flits)
Measured flits: 547920 547921 547922 547923 547924 547925 547926 547927 547928 547929 [...] (74734 flits)
Class 0:
Remaining flits: 566152 566153 579546 579547 579548 579549 579550 579551 579552 579553 [...] (811659 flits)
Measured flits: 566152 566153 579546 579547 579548 579549 579550 579551 579552 579553 [...] (51082 flits)
Class 0:
Remaining flits: 621432 621433 621434 621435 621436 621437 621438 621439 621440 621441 [...] (836571 flits)
Measured flits: 621432 621433 621434 621435 621436 621437 621438 621439 621440 621441 [...] (35636 flits)
Class 0:
Remaining flits: 642008 642009 642010 642011 642012 642013 642014 642015 642016 642017 [...] (865003 flits)
Measured flits: 642008 642009 642010 642011 642012 642013 642014 642015 642016 642017 [...] (24410 flits)
Class 0:
Remaining flits: 674316 674317 674318 674319 674320 674321 674322 674323 674324 674325 [...] (892314 flits)
Measured flits: 674316 674317 674318 674319 674320 674321 674322 674323 674324 674325 [...] (15759 flits)
Class 0:
Remaining flits: 715536 715537 715538 715539 715540 715541 715542 715543 715544 715545 [...] (922540 flits)
Measured flits: 715536 715537 715538 715539 715540 715541 715542 715543 715544 715545 [...] (9616 flits)
Class 0:
Remaining flits: 719280 719281 719282 719283 719284 719285 719286 719287 719288 719289 [...] (948745 flits)
Measured flits: 719280 719281 719282 719283 719284 719285 719286 719287 719288 719289 [...] (5349 flits)
Class 0:
Remaining flits: 799326 799327 799328 799329 799330 799331 799332 799333 799334 799335 [...] (968630 flits)
Measured flits: 799326 799327 799328 799329 799330 799331 799332 799333 799334 799335 [...] (2814 flits)
Class 0:
Remaining flits: 812972 812973 812974 812975 812976 812977 812978 812979 812980 812981 [...] (989823 flits)
Measured flits: 812972 812973 812974 812975 812976 812977 812978 812979 812980 812981 [...] (1258 flits)
Class 0:
Remaining flits: 862938 862939 862940 862941 862942 862943 862944 862945 862946 862947 [...] (1011330 flits)
Measured flits: 862938 862939 862940 862941 862942 862943 862944 862945 862946 862947 [...] (403 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1012158 1012159 1012160 1012161 1012162 1012163 1012164 1012165 1012166 1012167 [...] (978020 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1067508 1067509 1067510 1067511 1067512 1067513 1067514 1067515 1067516 1067517 [...] (928196 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1097964 1097965 1097966 1097967 1097968 1097969 1097970 1097971 1097972 1097973 [...] (878240 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1194084 1194085 1194086 1194087 1194088 1194089 1194090 1194091 1194092 1194093 [...] (827999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1205316 1205317 1205318 1205319 1205320 1205321 1205322 1205323 1205324 1205325 [...] (777228 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1210608 1210609 1210610 1210611 1210612 1210613 1210614 1210615 1210616 1210617 [...] (726448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1281438 1281439 1281440 1281441 1281442 1281443 1281444 1281445 1281446 1281447 [...] (675141 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284930 1284931 1284932 1284933 1284934 1284935 1284936 1284937 1284938 1284939 [...] (624890 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1314018 1314019 1314020 1314021 1314022 1314023 1314024 1314025 1314026 1314027 [...] (575740 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1340172 1340173 1340174 1340175 1340176 1340177 1340178 1340179 1340180 1340181 [...] (527014 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1373061 1373062 1373063 1373064 1373065 1373066 1373067 1373068 1373069 1373070 [...] (478854 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473858 1473859 1473860 1473861 1473862 1473863 1473864 1473865 1473866 1473867 [...] (431051 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1511820 1511821 1511822 1511823 1511824 1511825 1511826 1511827 1511828 1511829 [...] (384019 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1511828 1511829 1511830 1511831 1511832 1511833 1511834 1511835 1511836 1511837 [...] (336506 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1586916 1586917 1586918 1586919 1586920 1586921 1586922 1586923 1586924 1586925 [...] (289402 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1670652 1670653 1670654 1670655 1670656 1670657 1670658 1670659 1670660 1670661 [...] (241550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1672164 1672165 1672166 1672167 1672168 1672169 1672170 1672171 1672172 1672173 [...] (193901 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1697958 1697959 1697960 1697961 1697962 1697963 1697964 1697965 1697966 1697967 [...] (146546 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1865472 1865473 1865474 1865475 1865476 1865477 1865478 1865479 1865480 1865481 [...] (99553 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1981062 1981063 1981064 1981065 1981066 1981067 1981068 1981069 1981070 1981071 [...] (52840 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2027844 2027845 2027846 2027847 2027848 2027849 2027850 2027851 2027852 2027853 [...] (15479 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2364318 2364319 2364320 2364321 2364322 2364323 2364324 2364325 2364326 2364327 [...] (707 flits)
Measured flits: (0 flits)
Time taken is 51274 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6206.39 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19484 (1 samples)
Network latency average = 5956.39 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19467 (1 samples)
Flit latency average = 11403.4 (1 samples)
	minimum = 5 (1 samples)
	maximum = 29618 (1 samples)
Fragmentation average = 25.0553 (1 samples)
	minimum = 0 (1 samples)
	maximum = 670 (1 samples)
Injected packet rate average = 0.0289129 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0474286 (1 samples)
Accepted packet rate average = 0.0159152 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.520361 (1 samples)
	minimum = 0.221143 (1 samples)
	maximum = 0.853714 (1 samples)
Accepted flit rate average = 0.286506 (1 samples)
	minimum = 0.221143 (1 samples)
	maximum = 0.344571 (1 samples)
Injected packet size average = 17.9975 (1 samples)
Accepted packet size average = 18.0021 (1 samples)
Hops average = 5.07233 (1 samples)
Total run time 44.156
