BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 359.636
	minimum = 25
	maximum = 901
Network latency average = 254.032
	minimum = 22
	maximum = 857
Slowest packet = 580
Flit latency average = 223.54
	minimum = 5
	maximum = 840
Slowest flit = 16163
Fragmentation average = 22.9639
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0187396
	minimum = 0.009 (at node 123)
	maximum = 0.031 (at node 99)
Accepted packet rate average = 0.0135781
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.333219
	minimum = 0.147 (at node 188)
	maximum = 0.558 (at node 99)
Accepted flit rate average= 0.249464
	minimum = 0.09 (at node 174)
	maximum = 0.445 (at node 44)
Injected packet length average = 17.7815
Accepted packet length average = 18.3725
Total in-flight flits = 18721 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 709.66
	minimum = 25
	maximum = 1748
Network latency average = 309.334
	minimum = 22
	maximum = 1521
Slowest packet = 882
Flit latency average = 275.764
	minimum = 5
	maximum = 1503
Slowest flit = 39635
Fragmentation average = 22.2994
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0161484
	minimum = 0.008 (at node 55)
	maximum = 0.025 (at node 66)
Accepted packet rate average = 0.0136536
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 22)
Injected flit rate average = 0.288771
	minimum = 0.144 (at node 55)
	maximum = 0.444 (at node 66)
Accepted flit rate average= 0.248544
	minimum = 0.126 (at node 153)
	maximum = 0.36 (at node 22)
Injected packet length average = 17.8823
Accepted packet length average = 18.2035
Total in-flight flits = 18139 (0 measured)
latency change    = 0.493228
throughput change = 0.00369862
Class 0:
Packet latency average = 1768.09
	minimum = 1001
	maximum = 2621
Network latency average = 360.29
	minimum = 22
	maximum = 1696
Slowest packet = 5849
Flit latency average = 324.818
	minimum = 5
	maximum = 1667
Slowest flit = 38015
Fragmentation average = 22.7826
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0142865
	minimum = 0.004 (at node 85)
	maximum = 0.026 (at node 118)
Accepted packet rate average = 0.0142292
	minimum = 0.005 (at node 91)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.257125
	minimum = 0.072 (at node 85)
	maximum = 0.469 (at node 118)
Accepted flit rate average= 0.255896
	minimum = 0.098 (at node 91)
	maximum = 0.438 (at node 16)
Injected packet length average = 17.9978
Accepted packet length average = 17.9839
Total in-flight flits = 18291 (0 measured)
latency change    = 0.598629
throughput change = 0.0287287
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2556.01
	minimum = 1644
	maximum = 3416
Network latency average = 284.145
	minimum = 22
	maximum = 961
Slowest packet = 9072
Flit latency average = 320.348
	minimum = 5
	maximum = 1321
Slowest flit = 124116
Fragmentation average = 21.8644
	minimum = 0
	maximum = 98
Injected packet rate average = 0.014599
	minimum = 0.002 (at node 16)
	maximum = 0.031 (at node 151)
Accepted packet rate average = 0.0144167
	minimum = 0.006 (at node 48)
	maximum = 0.025 (at node 19)
Injected flit rate average = 0.262464
	minimum = 0.036 (at node 16)
	maximum = 0.567 (at node 151)
Accepted flit rate average= 0.259854
	minimum = 0.108 (at node 48)
	maximum = 0.45 (at node 19)
Injected packet length average = 17.9782
Accepted packet length average = 18.0246
Total in-flight flits = 18871 (18743 measured)
latency change    = 0.308261
throughput change = 0.0152329
Class 0:
Packet latency average = 2939.9
	minimum = 1644
	maximum = 4115
Network latency average = 328.052
	minimum = 22
	maximum = 1702
Slowest packet = 9072
Flit latency average = 321.078
	minimum = 5
	maximum = 1658
Slowest flit = 173681
Fragmentation average = 22.5199
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0141536
	minimum = 0.0055 (at node 153)
	maximum = 0.024 (at node 87)
Accepted packet rate average = 0.0142005
	minimum = 0.0095 (at node 36)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.254958
	minimum = 0.0965 (at node 171)
	maximum = 0.438 (at node 87)
Accepted flit rate average= 0.255453
	minimum = 0.1655 (at node 36)
	maximum = 0.416 (at node 129)
Injected packet length average = 18.0136
Accepted packet length average = 17.989
Total in-flight flits = 18243 (18243 measured)
latency change    = 0.13058
throughput change = 0.0172284
Class 0:
Packet latency average = 3296.22
	minimum = 1644
	maximum = 5020
Network latency average = 340.627
	minimum = 22
	maximum = 1778
Slowest packet = 9072
Flit latency average = 322.894
	minimum = 5
	maximum = 1761
Slowest flit = 175842
Fragmentation average = 22.0757
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0140313
	minimum = 0.00566667 (at node 57)
	maximum = 0.0233333 (at node 72)
Accepted packet rate average = 0.0140694
	minimum = 0.00866667 (at node 36)
	maximum = 0.0203333 (at node 129)
Injected flit rate average = 0.252661
	minimum = 0.102 (at node 57)
	maximum = 0.423 (at node 72)
Accepted flit rate average= 0.253125
	minimum = 0.156 (at node 36)
	maximum = 0.361333 (at node 129)
Injected packet length average = 18.0071
Accepted packet length average = 17.9911
Total in-flight flits = 18219 (18219 measured)
latency change    = 0.108101
throughput change = 0.00919753
Class 0:
Packet latency average = 3662.72
	minimum = 1644
	maximum = 5843
Network latency average = 346.928
	minimum = 22
	maximum = 1778
Slowest packet = 9072
Flit latency average = 323.988
	minimum = 5
	maximum = 1761
Slowest flit = 175842
Fragmentation average = 22.187
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0139688
	minimum = 0.0065 (at node 57)
	maximum = 0.0215 (at node 72)
Accepted packet rate average = 0.0139831
	minimum = 0.00975 (at node 36)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.251607
	minimum = 0.1165 (at node 57)
	maximum = 0.39075 (at node 72)
Accepted flit rate average= 0.251655
	minimum = 0.17275 (at node 36)
	maximum = 0.342 (at node 128)
Injected packet length average = 18.0121
Accepted packet length average = 17.9971
Total in-flight flits = 18304 (18304 measured)
latency change    = 0.100061
throughput change = 0.00584154
Class 0:
Packet latency average = 4023.45
	minimum = 1644
	maximum = 6762
Network latency average = 351.463
	minimum = 22
	maximum = 1807
Slowest packet = 9072
Flit latency average = 325.863
	minimum = 5
	maximum = 1790
Slowest flit = 301355
Fragmentation average = 22.1854
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0139448
	minimum = 0.008 (at node 167)
	maximum = 0.0198 (at node 72)
Accepted packet rate average = 0.0139375
	minimum = 0.0106 (at node 104)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.251082
	minimum = 0.1464 (at node 167)
	maximum = 0.3566 (at node 72)
Accepted flit rate average= 0.250831
	minimum = 0.1908 (at node 104)
	maximum = 0.3312 (at node 128)
Injected packet length average = 18.0055
Accepted packet length average = 17.9969
Total in-flight flits = 18423 (18423 measured)
latency change    = 0.0896572
throughput change = 0.00328387
Class 0:
Packet latency average = 4389.51
	minimum = 1644
	maximum = 7475
Network latency average = 352.835
	minimum = 22
	maximum = 1807
Slowest packet = 9072
Flit latency average = 325.333
	minimum = 5
	maximum = 1790
Slowest flit = 301355
Fragmentation average = 22.3363
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0139618
	minimum = 0.00833333 (at node 153)
	maximum = 0.02 (at node 72)
Accepted packet rate average = 0.0139557
	minimum = 0.0113333 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.251249
	minimum = 0.150667 (at node 153)
	maximum = 0.362167 (at node 72)
Accepted flit rate average= 0.251099
	minimum = 0.204 (at node 79)
	maximum = 0.324167 (at node 138)
Injected packet length average = 17.9955
Accepted packet length average = 17.9925
Total in-flight flits = 18483 (18483 measured)
latency change    = 0.0833949
throughput change = 0.00106615
Class 0:
Packet latency average = 4752.63
	minimum = 1644
	maximum = 8158
Network latency average = 354.358
	minimum = 22
	maximum = 1907
Slowest packet = 9072
Flit latency average = 325.464
	minimum = 5
	maximum = 1889
Slowest flit = 383343
Fragmentation average = 22.4583
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0139903
	minimum = 0.00928571 (at node 153)
	maximum = 0.0192857 (at node 72)
Accepted packet rate average = 0.0139777
	minimum = 0.0107143 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.251781
	minimum = 0.168429 (at node 153)
	maximum = 0.349286 (at node 72)
Accepted flit rate average= 0.251568
	minimum = 0.192857 (at node 79)
	maximum = 0.324 (at node 138)
Injected packet length average = 17.9968
Accepted packet length average = 17.9978
Total in-flight flits = 18530 (18530 measured)
latency change    = 0.0764031
throughput change = 0.00186332
Draining all recorded packets ...
Class 0:
Remaining flits: 473688 473689 473690 473691 473692 473693 473694 473695 473696 473697 [...] (18474 flits)
Measured flits: 473688 473689 473690 473691 473692 473693 473694 473695 473696 473697 [...] (18474 flits)
Class 0:
Remaining flits: 526250 526251 526252 526253 526254 526255 526256 526257 526258 526259 [...] (18180 flits)
Measured flits: 526250 526251 526252 526253 526254 526255 526256 526257 526258 526259 [...] (18180 flits)
Class 0:
Remaining flits: 573228 573229 573230 573231 573232 573233 573234 573235 573236 573237 [...] (18439 flits)
Measured flits: 573228 573229 573230 573231 573232 573233 573234 573235 573236 573237 [...] (18439 flits)
Class 0:
Remaining flits: 634734 634735 634736 634737 634738 634739 634740 634741 634742 634743 [...] (18373 flits)
Measured flits: 634734 634735 634736 634737 634738 634739 634740 634741 634742 634743 [...] (18373 flits)
Class 0:
Remaining flits: 668934 668935 668936 668937 668938 668939 668940 668941 668942 668943 [...] (18199 flits)
Measured flits: 668934 668935 668936 668937 668938 668939 668940 668941 668942 668943 [...] (18199 flits)
Class 0:
Remaining flits: 675972 675973 675974 675975 675976 675977 675978 675979 675980 675981 [...] (18142 flits)
Measured flits: 675972 675973 675974 675975 675976 675977 675978 675979 675980 675981 [...] (18142 flits)
Class 0:
Remaining flits: 743868 743869 743870 743871 743872 743873 743874 743875 743876 743877 [...] (17934 flits)
Measured flits: 743868 743869 743870 743871 743872 743873 743874 743875 743876 743877 [...] (17934 flits)
Class 0:
Remaining flits: 787572 787573 787574 787575 787576 787577 787578 787579 787580 787581 [...] (18236 flits)
Measured flits: 787572 787573 787574 787575 787576 787577 787578 787579 787580 787581 [...] (18236 flits)
Class 0:
Remaining flits: 850896 850897 850898 850899 850900 850901 850902 850903 850904 850905 [...] (18403 flits)
Measured flits: 850896 850897 850898 850899 850900 850901 850902 850903 850904 850905 [...] (18403 flits)
Class 0:
Remaining flits: 915570 915571 915572 915573 915574 915575 915576 915577 915578 915579 [...] (18281 flits)
Measured flits: 915570 915571 915572 915573 915574 915575 915576 915577 915578 915579 [...] (18281 flits)
Class 0:
Remaining flits: 930654 930655 930656 930657 930658 930659 930660 930661 930662 930663 [...] (18217 flits)
Measured flits: 930654 930655 930656 930657 930658 930659 930660 930661 930662 930663 [...] (18217 flits)
Class 0:
Remaining flits: 980787 980788 980789 980790 980791 980792 980793 980794 980795 980796 [...] (18596 flits)
Measured flits: 980787 980788 980789 980790 980791 980792 980793 980794 980795 980796 [...] (18596 flits)
Class 0:
Remaining flits: 1050398 1050399 1050400 1050401 1050402 1050403 1050404 1050405 1050406 1050407 [...] (18749 flits)
Measured flits: 1050398 1050399 1050400 1050401 1050402 1050403 1050404 1050405 1050406 1050407 [...] (18749 flits)
Class 0:
Remaining flits: 1102086 1102087 1102088 1102089 1102090 1102091 1102092 1102093 1102094 1102095 [...] (18710 flits)
Measured flits: 1102086 1102087 1102088 1102089 1102090 1102091 1102092 1102093 1102094 1102095 [...] (18710 flits)
Class 0:
Remaining flits: 1153098 1153099 1153100 1153101 1153102 1153103 1153104 1153105 1153106 1153107 [...] (18786 flits)
Measured flits: 1153098 1153099 1153100 1153101 1153102 1153103 1153104 1153105 1153106 1153107 [...] (18786 flits)
Class 0:
Remaining flits: 1207728 1207729 1207730 1207731 1207732 1207733 1207734 1207735 1207736 1207737 [...] (18268 flits)
Measured flits: 1207728 1207729 1207730 1207731 1207732 1207733 1207734 1207735 1207736 1207737 [...] (18268 flits)
Class 0:
Remaining flits: 1251666 1251667 1251668 1251669 1251670 1251671 1251672 1251673 1251674 1251675 [...] (18606 flits)
Measured flits: 1251666 1251667 1251668 1251669 1251670 1251671 1251672 1251673 1251674 1251675 [...] (18606 flits)
Class 0:
Remaining flits: 1297530 1297531 1297532 1297533 1297534 1297535 1297536 1297537 1297538 1297539 [...] (18511 flits)
Measured flits: 1297530 1297531 1297532 1297533 1297534 1297535 1297536 1297537 1297538 1297539 [...] (18511 flits)
Class 0:
Remaining flits: 1338984 1338985 1338986 1338987 1338988 1338989 1338990 1338991 1338992 1338993 [...] (18454 flits)
Measured flits: 1338984 1338985 1338986 1338987 1338988 1338989 1338990 1338991 1338992 1338993 [...] (18219 flits)
Class 0:
Remaining flits: 1381572 1381573 1381574 1381575 1381576 1381577 1381578 1381579 1381580 1381581 [...] (18632 flits)
Measured flits: 1381572 1381573 1381574 1381575 1381576 1381577 1381578 1381579 1381580 1381581 [...] (18165 flits)
Class 0:
Remaining flits: 1426058 1426059 1426060 1426061 1426062 1426063 1426064 1426065 1426066 1426067 [...] (18128 flits)
Measured flits: 1426058 1426059 1426060 1426061 1426062 1426063 1426064 1426065 1426066 1426067 [...] (17528 flits)
Class 0:
Remaining flits: 1472674 1472675 1472676 1472677 1472678 1472679 1472680 1472681 1472682 1472683 [...] (18518 flits)
Measured flits: 1472674 1472675 1472676 1472677 1472678 1472679 1472680 1472681 1472682 1472683 [...] (16313 flits)
Class 0:
Remaining flits: 1552176 1552177 1552178 1552179 1552180 1552181 1552182 1552183 1552184 1552185 [...] (18062 flits)
Measured flits: 1552176 1552177 1552178 1552179 1552180 1552181 1552182 1552183 1552184 1552185 [...] (13821 flits)
Class 0:
Remaining flits: 1597580 1597581 1597582 1597583 1597584 1597585 1597586 1597587 1597588 1597589 [...] (18657 flits)
Measured flits: 1597580 1597581 1597582 1597583 1597584 1597585 1597586 1597587 1597588 1597589 [...] (12072 flits)
Class 0:
Remaining flits: 1640988 1640989 1640990 1640991 1640992 1640993 1640994 1640995 1640996 1640997 [...] (18514 flits)
Measured flits: 1642974 1642975 1642976 1642977 1642978 1642979 1642980 1642981 1642982 1642983 [...] (8736 flits)
Class 0:
Remaining flits: 1689912 1689913 1689914 1689915 1689916 1689917 1689918 1689919 1689920 1689921 [...] (18371 flits)
Measured flits: 1689912 1689913 1689914 1689915 1689916 1689917 1689918 1689919 1689920 1689921 [...] (6156 flits)
Class 0:
Remaining flits: 1737882 1737883 1737884 1737885 1737886 1737887 1737888 1737889 1737890 1737891 [...] (18570 flits)
Measured flits: 1749042 1749043 1749044 1749045 1749046 1749047 1749048 1749049 1749050 1749051 [...] (4285 flits)
Class 0:
Remaining flits: 1772370 1772371 1772372 1772373 1772374 1772375 1772376 1772377 1772378 1772379 [...] (18471 flits)
Measured flits: 1772370 1772371 1772372 1772373 1772374 1772375 1772376 1772377 1772378 1772379 [...] (2892 flits)
Class 0:
Remaining flits: 1820412 1820413 1820414 1820415 1820416 1820417 1820418 1820419 1820420 1820421 [...] (18069 flits)
Measured flits: 1820412 1820413 1820414 1820415 1820416 1820417 1820418 1820419 1820420 1820421 [...] (1612 flits)
Class 0:
Remaining flits: 1880472 1880473 1880474 1880475 1880476 1880477 1881288 1881289 1881290 1881291 [...] (18370 flits)
Measured flits: 1880472 1880473 1880474 1880475 1880476 1880477 1904490 1904491 1904492 1904493 [...] (1044 flits)
Class 0:
Remaining flits: 1929884 1929885 1929886 1929887 1930806 1930807 1930808 1930809 1930810 1930811 [...] (18583 flits)
Measured flits: 1965204 1965205 1965206 1965207 1965208 1965209 1965210 1965211 1965212 1965213 [...] (366 flits)
Class 0:
Remaining flits: 1966860 1966861 1966862 1966863 1966864 1966865 1966866 1966867 1966868 1966869 [...] (18367 flits)
Measured flits: 2017380 2017381 2017382 2017383 2017384 2017385 2019816 2019817 2019818 2019819 [...] (96 flits)
Class 0:
Remaining flits: 2034810 2034811 2034812 2034813 2034814 2034815 2034816 2034817 2034818 2034819 [...] (18373 flits)
Measured flits: 2041722 2041723 2041724 2041725 2041726 2041727 2041728 2041729 2041730 2041731 [...] (18 flits)
Draining remaining packets ...
Time taken is 43771 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13927.9 (1 samples)
	minimum = 1644 (1 samples)
	maximum = 33202 (1 samples)
Network latency average = 364.053 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2597 (1 samples)
Flit latency average = 328.655 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2548 (1 samples)
Fragmentation average = 22.5307 (1 samples)
	minimum = 0 (1 samples)
	maximum = 152 (1 samples)
Injected packet rate average = 0.0139903 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0192857 (1 samples)
Accepted packet rate average = 0.0139777 (1 samples)
	minimum = 0.0107143 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.251781 (1 samples)
	minimum = 0.168429 (1 samples)
	maximum = 0.349286 (1 samples)
Accepted flit rate average = 0.251568 (1 samples)
	minimum = 0.192857 (1 samples)
	maximum = 0.324 (1 samples)
Injected packet size average = 17.9968 (1 samples)
Accepted packet size average = 17.9978 (1 samples)
Hops average = 5.06579 (1 samples)
Total run time 38.883
