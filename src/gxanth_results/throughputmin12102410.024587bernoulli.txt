BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 272.024
	minimum = 23
	maximum = 923
Network latency average = 266.727
	minimum = 23
	maximum = 918
Slowest packet = 228
Flit latency average = 232.685
	minimum = 6
	maximum = 901
Slowest flit = 4121
Fragmentation average = 56.9437
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0246823
	minimum = 0.014 (at node 179)
	maximum = 0.039 (at node 160)
Accepted packet rate average = 0.00990625
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 95)
Injected flit rate average = 0.440411
	minimum = 0.237 (at node 179)
	maximum = 0.695 (at node 160)
Accepted flit rate average= 0.186214
	minimum = 0.04 (at node 174)
	maximum = 0.329 (at node 131)
Injected packet length average = 17.8432
Accepted packet length average = 18.7976
Total in-flight flits = 49549 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 515.167
	minimum = 23
	maximum = 1779
Network latency average = 509.209
	minimum = 23
	maximum = 1739
Slowest packet = 918
Flit latency average = 473.177
	minimum = 6
	maximum = 1772
Slowest flit = 18384
Fragmentation average = 61.247
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0244792
	minimum = 0.016 (at node 17)
	maximum = 0.034 (at node 160)
Accepted packet rate average = 0.0101745
	minimum = 0.0055 (at node 164)
	maximum = 0.016 (at node 119)
Injected flit rate average = 0.438792
	minimum = 0.288 (at node 17)
	maximum = 0.606 (at node 160)
Accepted flit rate average= 0.18712
	minimum = 0.099 (at node 164)
	maximum = 0.288 (at node 119)
Injected packet length average = 17.9251
Accepted packet length average = 18.3911
Total in-flight flits = 97346 (0 measured)
latency change    = 0.47197
throughput change = 0.00484315
Class 0:
Packet latency average = 1219.18
	minimum = 23
	maximum = 2626
Network latency average = 1212.13
	minimum = 23
	maximum = 2584
Slowest packet = 1025
Flit latency average = 1186.3
	minimum = 6
	maximum = 2620
Slowest flit = 31720
Fragmentation average = 64.6083
	minimum = 0
	maximum = 141
Injected packet rate average = 0.024599
	minimum = 0.014 (at node 8)
	maximum = 0.037 (at node 48)
Accepted packet rate average = 0.0105052
	minimum = 0.003 (at node 20)
	maximum = 0.02 (at node 47)
Injected flit rate average = 0.442599
	minimum = 0.252 (at node 8)
	maximum = 0.649 (at node 48)
Accepted flit rate average= 0.189078
	minimum = 0.054 (at node 20)
	maximum = 0.354 (at node 137)
Injected packet length average = 17.9926
Accepted packet length average = 17.9985
Total in-flight flits = 146057 (0 measured)
latency change    = 0.577449
throughput change = 0.0103573
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 66.7632
	minimum = 23
	maximum = 349
Network latency average = 59.6368
	minimum = 23
	maximum = 334
Slowest packet = 16363
Flit latency average = 1692.47
	minimum = 6
	maximum = 3431
Slowest flit = 45809
Fragmentation average = 9.87105
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0249844
	minimum = 0.012 (at node 14)
	maximum = 0.041 (at node 91)
Accepted packet rate average = 0.0106562
	minimum = 0.004 (at node 154)
	maximum = 0.02 (at node 78)
Injected flit rate average = 0.449016
	minimum = 0.215 (at node 14)
	maximum = 0.741 (at node 91)
Accepted flit rate average= 0.192219
	minimum = 0.072 (at node 154)
	maximum = 0.358 (at node 78)
Injected packet length average = 17.9719
Accepted packet length average = 18.0381
Total in-flight flits = 195497 (79328 measured)
latency change    = 17.2613
throughput change = 0.0163388
Class 0:
Packet latency average = 112.416
	minimum = 23
	maximum = 1952
Network latency average = 103.388
	minimum = 23
	maximum = 1944
Slowest packet = 14241
Flit latency average = 1937.51
	minimum = 6
	maximum = 4287
Slowest flit = 60321
Fragmentation average = 12.0139
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0245052
	minimum = 0.017 (at node 34)
	maximum = 0.036 (at node 10)
Accepted packet rate average = 0.0106146
	minimum = 0.005 (at node 96)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.440969
	minimum = 0.304 (at node 185)
	maximum = 0.65 (at node 10)
Accepted flit rate average= 0.190654
	minimum = 0.09 (at node 154)
	maximum = 0.323 (at node 90)
Injected packet length average = 17.9949
Accepted packet length average = 17.9615
Total in-flight flits = 242226 (154902 measured)
latency change    = 0.406104
throughput change = 0.00820915
Class 0:
Packet latency average = 362.151
	minimum = 23
	maximum = 2959
Network latency average = 351.44
	minimum = 23
	maximum = 2959
Slowest packet = 14142
Flit latency average = 2188.02
	minimum = 6
	maximum = 5036
Slowest flit = 79505
Fragmentation average = 18.5251
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0243628
	minimum = 0.018 (at node 14)
	maximum = 0.0323333 (at node 10)
Accepted packet rate average = 0.0105712
	minimum = 0.00666667 (at node 4)
	maximum = 0.0163333 (at node 90)
Injected flit rate average = 0.438215
	minimum = 0.324 (at node 14)
	maximum = 0.587 (at node 10)
Accepted flit rate average= 0.190458
	minimum = 0.12 (at node 4)
	maximum = 0.290667 (at node 90)
Injected packet length average = 17.987
Accepted packet length average = 18.0168
Total in-flight flits = 288965 (228321 measured)
latency change    = 0.689589
throughput change = 0.00102549
Draining remaining packets ...
Class 0:
Remaining flits: 92340 92341 92342 92343 92344 92345 92346 92347 92348 92349 [...] (259247 flits)
Measured flits: 254214 254215 254216 254217 254218 254219 254220 254221 254222 254223 [...] (220250 flits)
Class 0:
Remaining flits: 92340 92341 92342 92343 92344 92345 92346 92347 92348 92349 [...] (230432 flits)
Measured flits: 254214 254215 254216 254217 254218 254219 254220 254221 254222 254223 [...] (207116 flits)
Class 0:
Remaining flits: 111816 111817 111818 111819 111820 111821 111822 111823 111824 111825 [...] (201097 flits)
Measured flits: 254214 254215 254216 254217 254218 254219 254220 254221 254222 254223 [...] (188586 flits)
Class 0:
Remaining flits: 156222 156223 156224 156225 156226 156227 156228 156229 156230 156231 [...] (170872 flits)
Measured flits: 254340 254341 254342 254343 254344 254345 254346 254347 254348 254349 [...] (164822 flits)
Class 0:
Remaining flits: 164790 164791 164792 164793 164794 164795 164796 164797 164798 164799 [...] (141432 flits)
Measured flits: 254346 254347 254348 254349 254350 254351 254352 254353 254354 254355 [...] (138672 flits)
Class 0:
Remaining flits: 181997 183582 183583 183584 183585 183586 183587 183588 183589 183590 [...] (111643 flits)
Measured flits: 254466 254467 254468 254469 254470 254471 254472 254473 254474 254475 [...] (110441 flits)
Class 0:
Remaining flits: 207288 207289 207290 207291 207292 207293 207294 207295 207296 207297 [...] (82170 flits)
Measured flits: 257364 257365 257366 257367 257368 257369 257370 257371 257372 257373 [...] (81778 flits)
Class 0:
Remaining flits: 240174 240175 240176 240177 240178 240179 240180 240181 240182 240183 [...] (53489 flits)
Measured flits: 260406 260407 260408 260409 260410 260411 260412 260413 260414 260415 [...] (53399 flits)
Class 0:
Remaining flits: 277812 277813 277814 277815 277816 277817 277818 277819 277820 277821 [...] (26330 flits)
Measured flits: 277812 277813 277814 277815 277816 277817 277818 277819 277820 277821 [...] (26330 flits)
Class 0:
Remaining flits: 321444 321445 321446 321447 321448 321449 321450 321451 321452 321453 [...] (4488 flits)
Measured flits: 321444 321445 321446 321447 321448 321449 321450 321451 321452 321453 [...] (4488 flits)
Time taken is 16765 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6620.19 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12352 (1 samples)
Network latency average = 6609.54 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12322 (1 samples)
Flit latency average = 5562.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12305 (1 samples)
Fragmentation average = 66.368 (1 samples)
	minimum = 0 (1 samples)
	maximum = 164 (1 samples)
Injected packet rate average = 0.0243628 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0105712 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.438215 (1 samples)
	minimum = 0.324 (1 samples)
	maximum = 0.587 (1 samples)
Accepted flit rate average = 0.190458 (1 samples)
	minimum = 0.12 (1 samples)
	maximum = 0.290667 (1 samples)
Injected packet size average = 17.987 (1 samples)
Accepted packet size average = 18.0168 (1 samples)
Hops average = 5.07232 (1 samples)
Total run time 14.4103
