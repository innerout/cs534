BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 243.391
	minimum = 23
	maximum = 870
Network latency average = 239.788
	minimum = 23
	maximum = 870
Slowest packet = 236
Flit latency average = 169.146
	minimum = 6
	maximum = 949
Slowest flit = 2545
Fragmentation average = 136.609
	minimum = 0
	maximum = 720
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.00952604
	minimum = 0 (at node 41)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.194484
	minimum = 0.028 (at node 41)
	maximum = 0.328 (at node 152)
Injected packet length average = 17.7947
Accepted packet length average = 20.4161
Total in-flight flits = 24723 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 401.547
	minimum = 23
	maximum = 1780
Network latency average = 397.733
	minimum = 23
	maximum = 1780
Slowest packet = 586
Flit latency average = 309.979
	minimum = 6
	maximum = 1780
Slowest flit = 12156
Fragmentation average = 177.631
	minimum = 0
	maximum = 1484
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.0105078
	minimum = 0.005 (at node 83)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.202477
	minimum = 0.1085 (at node 83)
	maximum = 0.311 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 19.2691
Total in-flight flits = 44955 (0 measured)
latency change    = 0.393866
throughput change = 0.0394722
Class 0:
Packet latency average = 824.213
	minimum = 23
	maximum = 2653
Network latency average = 820.264
	minimum = 23
	maximum = 2653
Slowest packet = 1076
Flit latency average = 731.318
	minimum = 6
	maximum = 2722
Slowest flit = 15830
Fragmentation average = 235.617
	minimum = 0
	maximum = 2260
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0120833
	minimum = 0.004 (at node 121)
	maximum = 0.022 (at node 159)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.217609
	minimum = 0.072 (at node 121)
	maximum = 0.385 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 18.0091
Total in-flight flits = 64968 (0 measured)
latency change    = 0.512811
throughput change = 0.0695412
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 299.982
	minimum = 26
	maximum = 968
Network latency average = 296.25
	minimum = 23
	maximum = 963
Slowest packet = 10273
Flit latency average = 992.406
	minimum = 6
	maximum = 3679
Slowest flit = 18735
Fragmentation average = 108.241
	minimum = 0
	maximum = 479
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0120781
	minimum = 0.004 (at node 100)
	maximum = 0.025 (at node 160)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.21812
	minimum = 0.082 (at node 100)
	maximum = 0.452 (at node 151)
Injected packet length average = 17.9626
Accepted packet length average = 18.0591
Total in-flight flits = 87079 (52582 measured)
latency change    = 1.74754
throughput change = 0.00234007
Class 0:
Packet latency average = 645.921
	minimum = 24
	maximum = 1966
Network latency average = 641.886
	minimum = 23
	maximum = 1966
Slowest packet = 10299
Flit latency average = 1140.78
	minimum = 6
	maximum = 4418
Slowest flit = 29751
Fragmentation average = 145.346
	minimum = 0
	maximum = 1060
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.0120781
	minimum = 0.0055 (at node 17)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.216924
	minimum = 0.1165 (at node 4)
	maximum = 0.356 (at node 16)
Injected packet length average = 18.0138
Accepted packet length average = 17.9601
Total in-flight flits = 107057 (88557 measured)
latency change    = 0.535575
throughput change = 0.00551027
Class 0:
Packet latency average = 942.233
	minimum = 24
	maximum = 2985
Network latency average = 938.099
	minimum = 23
	maximum = 2985
Slowest packet = 10277
Flit latency average = 1288.11
	minimum = 6
	maximum = 5170
Slowest flit = 50609
Fragmentation average = 162.454
	minimum = 0
	maximum = 1886
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0121076
	minimum = 0.00733333 (at node 2)
	maximum = 0.0186667 (at node 16)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.217519
	minimum = 0.135667 (at node 4)
	maximum = 0.326333 (at node 16)
Injected packet length average = 18.0024
Accepted packet length average = 17.9654
Total in-flight flits = 127741 (117883 measured)
latency change    = 0.314478
throughput change = 0.00273364
Class 0:
Packet latency average = 1200.18
	minimum = 24
	maximum = 3962
Network latency average = 1196.06
	minimum = 23
	maximum = 3951
Slowest packet = 10384
Flit latency average = 1433.04
	minimum = 6
	maximum = 5850
Slowest flit = 69656
Fragmentation average = 175.717
	minimum = 0
	maximum = 2144
Injected packet rate average = 0.0181263
	minimum = 0.0125 (at node 55)
	maximum = 0.02475 (at node 104)
Accepted packet rate average = 0.0120456
	minimum = 0.0075 (at node 4)
	maximum = 0.01625 (at node 90)
Injected flit rate average = 0.326395
	minimum = 0.225 (at node 55)
	maximum = 0.4425 (at node 104)
Accepted flit rate average= 0.216271
	minimum = 0.138 (at node 4)
	maximum = 0.29975 (at node 90)
Injected packet length average = 18.0067
Accepted packet length average = 17.9544
Total in-flight flits = 149450 (144201 measured)
latency change    = 0.214921
throughput change = 0.00577176
Class 0:
Packet latency average = 1427.73
	minimum = 23
	maximum = 4888
Network latency average = 1423.51
	minimum = 23
	maximum = 4888
Slowest packet = 10584
Flit latency average = 1577.72
	minimum = 6
	maximum = 6365
Slowest flit = 98421
Fragmentation average = 181.309
	minimum = 0
	maximum = 2144
Injected packet rate average = 0.0180781
	minimum = 0.0132 (at node 121)
	maximum = 0.0226 (at node 104)
Accepted packet rate average = 0.0119896
	minimum = 0.008 (at node 2)
	maximum = 0.0164 (at node 157)
Injected flit rate average = 0.325454
	minimum = 0.2376 (at node 121)
	maximum = 0.4068 (at node 104)
Accepted flit rate average= 0.215543
	minimum = 0.1428 (at node 4)
	maximum = 0.294 (at node 90)
Injected packet length average = 18.0027
Accepted packet length average = 17.9775
Total in-flight flits = 170437 (167692 measured)
latency change    = 0.159384
throughput change = 0.0033781
Class 0:
Packet latency average = 1638.05
	minimum = 23
	maximum = 5901
Network latency average = 1633.84
	minimum = 23
	maximum = 5889
Slowest packet = 10490
Flit latency average = 1721.83
	minimum = 6
	maximum = 6865
Slowest flit = 112607
Fragmentation average = 186.054
	minimum = 0
	maximum = 2326
Injected packet rate average = 0.0180833
	minimum = 0.0145 (at node 69)
	maximum = 0.0225 (at node 104)
Accepted packet rate average = 0.0119427
	minimum = 0.00783333 (at node 4)
	maximum = 0.0158333 (at node 118)
Injected flit rate average = 0.325551
	minimum = 0.261 (at node 69)
	maximum = 0.405 (at node 104)
Accepted flit rate average= 0.21448
	minimum = 0.143167 (at node 4)
	maximum = 0.283667 (at node 118)
Injected packet length average = 18.0028
Accepted packet length average = 17.9591
Total in-flight flits = 192863 (191542 measured)
latency change    = 0.128397
throughput change = 0.00495465
Class 0:
Packet latency average = 1843.43
	minimum = 23
	maximum = 6983
Network latency average = 1839.2
	minimum = 23
	maximum = 6971
Slowest packet = 10322
Flit latency average = 1865.45
	minimum = 6
	maximum = 8450
Slowest flit = 94291
Fragmentation average = 187.607
	minimum = 0
	maximum = 2835
Injected packet rate average = 0.0180737
	minimum = 0.0144286 (at node 23)
	maximum = 0.022 (at node 104)
Accepted packet rate average = 0.0119063
	minimum = 0.00842857 (at node 4)
	maximum = 0.0158571 (at node 118)
Injected flit rate average = 0.325296
	minimum = 0.260571 (at node 55)
	maximum = 0.396 (at node 104)
Accepted flit rate average= 0.213701
	minimum = 0.152143 (at node 4)
	maximum = 0.284429 (at node 118)
Injected packet length average = 17.9984
Accepted packet length average = 17.9486
Total in-flight flits = 214992 (214245 measured)
latency change    = 0.111412
throughput change = 0.00364595
Draining all recorded packets ...
Class 0:
Remaining flits: 107164 107165 107166 107167 107168 107169 107170 107171 149022 149023 [...] (237952 flits)
Measured flits: 185348 185349 185350 185351 185352 185353 185354 185355 185356 185357 [...] (180374 flits)
Class 0:
Remaining flits: 149022 149023 149024 149025 149026 149027 149028 149029 149030 149031 [...] (258443 flits)
Measured flits: 188586 188587 188588 188589 188590 188591 188592 188593 188594 188595 [...] (148465 flits)
Class 0:
Remaining flits: 149022 149023 149024 149025 149026 149027 149028 149029 149030 149031 [...] (280043 flits)
Measured flits: 190188 190189 190190 190191 190192 190193 190194 190195 190196 190197 [...] (120335 flits)
Class 0:
Remaining flits: 182604 182605 182606 182607 182608 182609 206856 206857 206858 206859 [...] (302313 flits)
Measured flits: 206856 206857 206858 206859 206860 206861 206862 206863 206864 206865 [...] (96809 flits)
Class 0:
Remaining flits: 214722 214723 214724 214725 214726 214727 214728 214729 214730 214731 [...] (325372 flits)
Measured flits: 214722 214723 214724 214725 214726 214727 214728 214729 214730 214731 [...] (77743 flits)
Class 0:
Remaining flits: 214737 214738 214739 218808 218809 218810 218811 218812 218813 218814 [...] (350794 flits)
Measured flits: 214737 214738 214739 218808 218809 218810 218811 218812 218813 218814 [...] (61076 flits)
Class 0:
Remaining flits: 214737 214738 214739 218808 218809 218810 218811 218812 218813 218814 [...] (372072 flits)
Measured flits: 214737 214738 214739 218808 218809 218810 218811 218812 218813 218814 [...] (47749 flits)
Class 0:
Remaining flits: 214737 214738 214739 244602 244603 244604 244605 244606 244607 244608 [...] (396283 flits)
Measured flits: 214737 214738 214739 244602 244603 244604 244605 244606 244607 244608 [...] (36607 flits)
Class 0:
Remaining flits: 244612 244613 244614 244615 244616 244617 244618 244619 258552 258553 [...] (420466 flits)
Measured flits: 244612 244613 244614 244615 244616 244617 244618 244619 258552 258553 [...] (27674 flits)
Class 0:
Remaining flits: 258552 258553 258554 258555 258556 258557 258558 258559 258560 258561 [...] (444826 flits)
Measured flits: 258552 258553 258554 258555 258556 258557 258558 258559 258560 258561 [...] (20933 flits)
Class 0:
Remaining flits: 258552 258553 258554 258555 258556 258557 258558 258559 258560 258561 [...] (468199 flits)
Measured flits: 258552 258553 258554 258555 258556 258557 258558 258559 258560 258561 [...] (15430 flits)
Class 0:
Remaining flits: 334098 334099 334100 334101 334102 334103 334104 334105 334106 334107 [...] (492278 flits)
Measured flits: 334098 334099 334100 334101 334102 334103 334104 334105 334106 334107 [...] (11332 flits)
Class 0:
Remaining flits: 334098 334099 334100 334101 334102 334103 334104 334105 334106 334107 [...] (516561 flits)
Measured flits: 334098 334099 334100 334101 334102 334103 334104 334105 334106 334107 [...] (8495 flits)
Class 0:
Remaining flits: 338292 338293 338294 338295 338296 338297 338298 338299 338300 338301 [...] (539861 flits)
Measured flits: 338292 338293 338294 338295 338296 338297 338298 338299 338300 338301 [...] (6387 flits)
Class 0:
Remaining flits: 338292 338293 338294 338295 338296 338297 338298 338299 338300 338301 [...] (563140 flits)
Measured flits: 338292 338293 338294 338295 338296 338297 338298 338299 338300 338301 [...] (4739 flits)
Class 0:
Remaining flits: 357966 357967 357968 357969 357970 357971 357972 357973 357974 357975 [...] (586240 flits)
Measured flits: 357966 357967 357968 357969 357970 357971 357972 357973 357974 357975 [...] (3605 flits)
Class 0:
Remaining flits: 357966 357967 357968 357969 357970 357971 357972 357973 357974 357975 [...] (610762 flits)
Measured flits: 357966 357967 357968 357969 357970 357971 357972 357973 357974 357975 [...] (2798 flits)
Class 0:
Remaining flits: 396228 396229 396230 396231 396232 396233 423936 423937 423938 423939 [...] (633465 flits)
Measured flits: 396228 396229 396230 396231 396232 396233 423936 423937 423938 423939 [...] (2028 flits)
Class 0:
Remaining flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (656651 flits)
Measured flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (1535 flits)
Class 0:
Remaining flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (681142 flits)
Measured flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (1204 flits)
Class 0:
Remaining flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (705100 flits)
Measured flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (830 flits)
Class 0:
Remaining flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (728300 flits)
Measured flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (600 flits)
Class 0:
Remaining flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (751820 flits)
Measured flits: 457758 457759 457760 457761 457762 457763 457764 457765 457766 457767 [...] (485 flits)
Class 0:
Remaining flits: 512622 512623 512624 512625 512626 512627 512628 512629 512630 512631 [...] (777406 flits)
Measured flits: 512622 512623 512624 512625 512626 512627 512628 512629 512630 512631 [...] (368 flits)
Class 0:
Remaining flits: 521838 521839 521840 521841 521842 521843 521844 521845 521846 521847 [...] (801657 flits)
Measured flits: 521838 521839 521840 521841 521842 521843 521844 521845 521846 521847 [...] (217 flits)
Class 0:
Remaining flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (825689 flits)
Measured flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (126 flits)
Class 0:
Remaining flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (849448 flits)
Measured flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (90 flits)
Class 0:
Remaining flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (874167 flits)
Measured flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (54 flits)
Class 0:
Remaining flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (899515 flits)
Measured flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (54 flits)
Class 0:
Remaining flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (922459 flits)
Measured flits: 551970 551971 551972 551973 551974 551975 551976 551977 551978 551979 [...] (36 flits)
Class 0:
Remaining flits: 608292 608293 608294 608295 608296 608297 608298 608299 608300 608301 [...] (945422 flits)
Measured flits: 608292 608293 608294 608295 608296 608297 608298 608299 608300 608301 [...] (18 flits)
Class 0:
Remaining flits: 608297 608298 608299 608300 608301 608302 608303 608304 608305 608306 [...] (970213 flits)
Measured flits: 608297 608298 608299 608300 608301 608302 608303 608304 608305 608306 [...] (13 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (939132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (906574 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (873847 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (840891 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (807039 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722340 722341 722342 722343 722344 722345 722346 722347 722348 722349 [...] (773328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 722341 722342 722343 722344 722345 722346 722347 722348 722349 722350 [...] (739614 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 836550 836551 836552 836553 836554 836555 836556 836557 836558 836559 [...] (705878 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 836565 836566 836567 846108 846109 846110 846111 846112 846113 846114 [...] (672487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 846108 846109 846110 846111 846112 846113 846114 846115 846116 846117 [...] (639811 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 901069 901070 901071 901072 901073 901074 901075 901076 901077 901078 [...] (606891 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 978948 978949 978950 978951 978952 978953 978954 978955 978956 978957 [...] (573568 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 978948 978949 978950 978951 978952 978953 978954 978955 978956 978957 [...] (540570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 978948 978949 978950 978951 978952 978953 978954 978955 978956 978957 [...] (507199 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1023180 1023181 1023182 1023183 1023184 1023185 1023186 1023187 1023188 1023189 [...] (474173 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091304 1091305 1091306 1091307 1091308 1091309 1091310 1091311 1091312 1091313 [...] (440638 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091304 1091305 1091306 1091307 1091308 1091309 1091310 1091311 1091312 1091313 [...] (407230 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091304 1091305 1091306 1091307 1091308 1091309 1091310 1091311 1091312 1091313 [...] (374094 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091304 1091305 1091306 1091307 1091308 1091309 1091310 1091311 1091312 1091313 [...] (340482 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091310 1091311 1091312 1091313 1091314 1091315 1091316 1091317 1091318 1091319 [...] (307027 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1189566 1189567 1189568 1189569 1189570 1189571 1189572 1189573 1189574 1189575 [...] (273883 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1189566 1189567 1189568 1189569 1189570 1189571 1189572 1189573 1189574 1189575 [...] (239970 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1189566 1189567 1189568 1189569 1189570 1189571 1189572 1189573 1189574 1189575 [...] (205760 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1189566 1189567 1189568 1189569 1189570 1189571 1189572 1189573 1189574 1189575 [...] (171703 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1189566 1189567 1189568 1189569 1189570 1189571 1189572 1189573 1189574 1189575 [...] (139108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310004 1310005 1310006 1310007 1310008 1310009 1310010 1310011 1310012 1310013 [...] (107121 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388988 1388989 1388990 1388991 1388992 1388993 1388994 1388995 1388996 1388997 [...] (76970 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1416744 1416745 1416746 1416747 1416748 1416749 1416750 1416751 1416752 1416753 [...] (48931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1416744 1416745 1416746 1416747 1416748 1416749 1416750 1416751 1416752 1416753 [...] (25988 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1633554 1633555 1633556 1633557 1633558 1633559 1633560 1633561 1633562 1633563 [...] (9196 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1962414 1962415 1962416 1962417 1962418 1962419 1962420 1962421 1962422 1962423 [...] (1129 flits)
Measured flits: (0 flits)
Time taken is 73707 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4358.74 (1 samples)
	minimum = 23 (1 samples)
	maximum = 32330 (1 samples)
Network latency average = 4354.62 (1 samples)
	minimum = 23 (1 samples)
	maximum = 32330 (1 samples)
Flit latency average = 13691.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 48847 (1 samples)
Fragmentation average = 211.157 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3909 (1 samples)
Injected packet rate average = 0.0180737 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0119063 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0158571 (1 samples)
Injected flit rate average = 0.325296 (1 samples)
	minimum = 0.260571 (1 samples)
	maximum = 0.396 (1 samples)
Accepted flit rate average = 0.213701 (1 samples)
	minimum = 0.152143 (1 samples)
	maximum = 0.284429 (1 samples)
Injected packet size average = 17.9984 (1 samples)
Accepted packet size average = 17.9486 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 78.073
