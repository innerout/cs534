BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 272.444
	minimum = 22
	maximum = 812
Network latency average = 262.227
	minimum = 22
	maximum = 791
Slowest packet = 849
Flit latency average = 227.973
	minimum = 5
	maximum = 808
Slowest flit = 17049
Fragmentation average = 53.4439
	minimum = 0
	maximum = 291
Injected packet rate average = 0.0266146
	minimum = 0.018 (at node 108)
	maximum = 0.038 (at node 163)
Accepted packet rate average = 0.0138333
	minimum = 0.006 (at node 64)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.47412
	minimum = 0.308 (at node 108)
	maximum = 0.684 (at node 163)
Accepted flit rate average= 0.260922
	minimum = 0.125 (at node 172)
	maximum = 0.465 (at node 44)
Injected packet length average = 17.8143
Accepted packet length average = 18.8618
Total in-flight flits = 42207 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 506.882
	minimum = 22
	maximum = 1614
Network latency average = 469.467
	minimum = 22
	maximum = 1614
Slowest packet = 1467
Flit latency average = 429.474
	minimum = 5
	maximum = 1597
Slowest flit = 26423
Fragmentation average = 58.8456
	minimum = 0
	maximum = 291
Injected packet rate average = 0.0220937
	minimum = 0.0125 (at node 108)
	maximum = 0.029 (at node 37)
Accepted packet rate average = 0.0144922
	minimum = 0.009 (at node 116)
	maximum = 0.0205 (at node 177)
Injected flit rate average = 0.394805
	minimum = 0.225 (at node 108)
	maximum = 0.522 (at node 146)
Accepted flit rate average= 0.266047
	minimum = 0.162 (at node 153)
	maximum = 0.38 (at node 177)
Injected packet length average = 17.8695
Accepted packet length average = 18.358
Total in-flight flits = 51486 (0 measured)
latency change    = 0.462511
throughput change = 0.0192635
Class 0:
Packet latency average = 1209.04
	minimum = 28
	maximum = 2238
Network latency average = 939.769
	minimum = 22
	maximum = 2131
Slowest packet = 4111
Flit latency average = 887.024
	minimum = 5
	maximum = 2114
Slowest flit = 74015
Fragmentation average = 65.7954
	minimum = 0
	maximum = 293
Injected packet rate average = 0.0148333
	minimum = 0.002 (at node 52)
	maximum = 0.028 (at node 141)
Accepted packet rate average = 0.0147656
	minimum = 0.005 (at node 21)
	maximum = 0.028 (at node 59)
Injected flit rate average = 0.267568
	minimum = 0.036 (at node 52)
	maximum = 0.504 (at node 141)
Accepted flit rate average= 0.26751
	minimum = 0.09 (at node 21)
	maximum = 0.507 (at node 59)
Injected packet length average = 18.0383
Accepted packet length average = 18.1171
Total in-flight flits = 51622 (0 measured)
latency change    = 0.580756
throughput change = 0.00547097
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1517.78
	minimum = 32
	maximum = 2860
Network latency average = 353.008
	minimum = 22
	maximum = 965
Slowest packet = 11424
Flit latency average = 958.603
	minimum = 5
	maximum = 3102
Slowest flit = 83228
Fragmentation average = 35.5543
	minimum = 0
	maximum = 217
Injected packet rate average = 0.0151615
	minimum = 0.002 (at node 71)
	maximum = 0.027 (at node 77)
Accepted packet rate average = 0.0148229
	minimum = 0.005 (at node 36)
	maximum = 0.023 (at node 3)
Injected flit rate average = 0.272057
	minimum = 0.036 (at node 71)
	maximum = 0.484 (at node 149)
Accepted flit rate average= 0.267167
	minimum = 0.095 (at node 36)
	maximum = 0.432 (at node 3)
Injected packet length average = 17.944
Accepted packet length average = 18.0239
Total in-flight flits = 52400 (41675 measured)
latency change    = 0.203418
throughput change = 0.00128665
Class 0:
Packet latency average = 2024.96
	minimum = 32
	maximum = 3698
Network latency average = 773.562
	minimum = 22
	maximum = 1889
Slowest packet = 11424
Flit latency average = 965.569
	minimum = 5
	maximum = 3132
Slowest flit = 83231
Fragmentation average = 58.9986
	minimum = 0
	maximum = 331
Injected packet rate average = 0.0150964
	minimum = 0.007 (at node 181)
	maximum = 0.024 (at node 14)
Accepted packet rate average = 0.014888
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271214
	minimum = 0.129 (at node 181)
	maximum = 0.4275 (at node 14)
Accepted flit rate average= 0.268036
	minimum = 0.152 (at node 36)
	maximum = 0.3765 (at node 165)
Injected packet length average = 17.9655
Accepted packet length average = 18.0035
Total in-flight flits = 52646 (51967 measured)
latency change    = 0.250465
throughput change = 0.00324505
Class 0:
Packet latency average = 2334.38
	minimum = 32
	maximum = 4175
Network latency average = 901.12
	minimum = 22
	maximum = 2944
Slowest packet = 11424
Flit latency average = 971.369
	minimum = 5
	maximum = 3132
Slowest flit = 83231
Fragmentation average = 65.0924
	minimum = 0
	maximum = 331
Injected packet rate average = 0.0150122
	minimum = 0.008 (at node 181)
	maximum = 0.0223333 (at node 14)
Accepted packet rate average = 0.0148368
	minimum = 0.0106667 (at node 2)
	maximum = 0.0213333 (at node 165)
Injected flit rate average = 0.269911
	minimum = 0.145667 (at node 181)
	maximum = 0.400333 (at node 14)
Accepted flit rate average= 0.267708
	minimum = 0.188667 (at node 2)
	maximum = 0.379 (at node 165)
Injected packet length average = 17.9795
Accepted packet length average = 18.0435
Total in-flight flits = 52654 (52582 measured)
latency change    = 0.132548
throughput change = 0.00122568
Class 0:
Packet latency average = 2604.86
	minimum = 32
	maximum = 4736
Network latency average = 943.203
	minimum = 22
	maximum = 3207
Slowest packet = 11424
Flit latency average = 968.012
	minimum = 5
	maximum = 4419
Slowest flit = 169973
Fragmentation average = 68.3511
	minimum = 0
	maximum = 331
Injected packet rate average = 0.0149141
	minimum = 0.00875 (at node 181)
	maximum = 0.02275 (at node 23)
Accepted packet rate average = 0.014862
	minimum = 0.011 (at node 135)
	maximum = 0.01975 (at node 128)
Injected flit rate average = 0.268197
	minimum = 0.157 (at node 181)
	maximum = 0.4085 (at node 23)
Accepted flit rate average= 0.267977
	minimum = 0.2 (at node 2)
	maximum = 0.3575 (at node 128)
Injected packet length average = 17.9828
Accepted packet length average = 18.031
Total in-flight flits = 51556 (51556 measured)
latency change    = 0.103836
throughput change = 0.00100094
Class 0:
Packet latency average = 2867.27
	minimum = 32
	maximum = 5907
Network latency average = 966.604
	minimum = 22
	maximum = 3807
Slowest packet = 11424
Flit latency average = 967.543
	minimum = 5
	maximum = 4419
Slowest flit = 169973
Fragmentation average = 71.089
	minimum = 0
	maximum = 331
Injected packet rate average = 0.0149333
	minimum = 0.0098 (at node 72)
	maximum = 0.0222 (at node 23)
Accepted packet rate average = 0.0148906
	minimum = 0.0114 (at node 2)
	maximum = 0.0188 (at node 128)
Injected flit rate average = 0.268639
	minimum = 0.1742 (at node 72)
	maximum = 0.4008 (at node 23)
Accepted flit rate average= 0.268352
	minimum = 0.207 (at node 2)
	maximum = 0.34 (at node 128)
Injected packet length average = 17.9892
Accepted packet length average = 18.0215
Total in-flight flits = 51656 (51656 measured)
latency change    = 0.0915195
throughput change = 0.00139936
Class 0:
Packet latency average = 3114.81
	minimum = 32
	maximum = 6097
Network latency average = 979.686
	minimum = 22
	maximum = 4391
Slowest packet = 11424
Flit latency average = 965.734
	minimum = 5
	maximum = 4419
Slowest flit = 169973
Fragmentation average = 73.0831
	minimum = 0
	maximum = 345
Injected packet rate average = 0.0149323
	minimum = 0.0103333 (at node 181)
	maximum = 0.0205 (at node 23)
Accepted packet rate average = 0.0149028
	minimum = 0.0115 (at node 79)
	maximum = 0.0188333 (at node 157)
Injected flit rate average = 0.268612
	minimum = 0.1875 (at node 181)
	maximum = 0.3695 (at node 23)
Accepted flit rate average= 0.268708
	minimum = 0.208167 (at node 79)
	maximum = 0.341333 (at node 157)
Injected packet length average = 17.9887
Accepted packet length average = 18.0308
Total in-flight flits = 51148 (51148 measured)
latency change    = 0.0794716
throughput change = 0.00132579
Class 0:
Packet latency average = 3357.13
	minimum = 32
	maximum = 7056
Network latency average = 987.182
	minimum = 22
	maximum = 4767
Slowest packet = 11424
Flit latency average = 962.928
	minimum = 5
	maximum = 4684
Slowest flit = 300833
Fragmentation average = 74.6283
	minimum = 0
	maximum = 366
Injected packet rate average = 0.0148847
	minimum = 0.00985714 (at node 176)
	maximum = 0.0195714 (at node 14)
Accepted packet rate average = 0.014904
	minimum = 0.0117143 (at node 79)
	maximum = 0.0185714 (at node 181)
Injected flit rate average = 0.267824
	minimum = 0.175143 (at node 176)
	maximum = 0.350429 (at node 183)
Accepted flit rate average= 0.268686
	minimum = 0.21 (at node 79)
	maximum = 0.329714 (at node 157)
Injected packet length average = 17.9933
Accepted packet length average = 18.0278
Total in-flight flits = 50256 (50256 measured)
latency change    = 0.0721814
throughput change = 8.30763e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 424846 424847 424848 424849 424850 424851 424852 424853 426762 426763 [...] (51358 flits)
Measured flits: 424846 424847 424848 424849 424850 424851 424852 424853 426762 426763 [...] (51358 flits)
Class 0:
Remaining flits: 454464 454465 454466 454467 454468 454469 454470 454471 454472 454473 [...] (51007 flits)
Measured flits: 454464 454465 454466 454467 454468 454469 454470 454471 454472 454473 [...] (51007 flits)
Class 0:
Remaining flits: 454464 454465 454466 454467 454468 454469 454470 454471 454472 454473 [...] (51919 flits)
Measured flits: 454464 454465 454466 454467 454468 454469 454470 454471 454472 454473 [...] (51919 flits)
Class 0:
Remaining flits: 454468 454469 454470 454471 454472 454473 454474 454475 454476 454477 [...] (51083 flits)
Measured flits: 454468 454469 454470 454471 454472 454473 454474 454475 454476 454477 [...] (51083 flits)
Class 0:
Remaining flits: 561186 561187 561188 561189 561190 561191 561192 561193 561194 561195 [...] (49655 flits)
Measured flits: 561186 561187 561188 561189 561190 561191 561192 561193 561194 561195 [...] (48917 flits)
Class 0:
Remaining flits: 627012 627013 627014 627015 627016 627017 627018 627019 627020 627021 [...] (48851 flits)
Measured flits: 627012 627013 627014 627015 627016 627017 627018 627019 627020 627021 [...] (45125 flits)
Class 0:
Remaining flits: 697590 697591 697592 697593 697594 697595 697596 697597 697598 697599 [...] (49734 flits)
Measured flits: 697590 697591 697592 697593 697594 697595 697596 697597 697598 697599 [...] (39245 flits)
Class 0:
Remaining flits: 720648 720649 720650 720651 720652 720653 720654 720655 720656 720657 [...] (50103 flits)
Measured flits: 720648 720649 720650 720651 720652 720653 720654 720655 720656 720657 [...] (30218 flits)
Class 0:
Remaining flits: 772035 772036 772037 773820 773821 773822 773823 773824 773825 773826 [...] (49120 flits)
Measured flits: 772035 772036 772037 773820 773821 773822 773823 773824 773825 773826 [...] (20296 flits)
Class 0:
Remaining flits: 843840 843841 843842 843843 843844 843845 843846 843847 843848 843849 [...] (50386 flits)
Measured flits: 843840 843841 843842 843843 843844 843845 843846 843847 843848 843849 [...] (13228 flits)
Class 0:
Remaining flits: 843840 843841 843842 843843 843844 843845 843846 843847 843848 843849 [...] (48824 flits)
Measured flits: 843840 843841 843842 843843 843844 843845 843846 843847 843848 843849 [...] (8392 flits)
Class 0:
Remaining flits: 941256 941257 941258 941259 941260 941261 941262 941263 941264 941265 [...] (50656 flits)
Measured flits: 941256 941257 941258 941259 941260 941261 941262 941263 941264 941265 [...] (5187 flits)
Class 0:
Remaining flits: 954288 954289 954290 954291 954292 954293 954294 954295 954296 954297 [...] (50162 flits)
Measured flits: 954288 954289 954290 954291 954292 954293 954294 954295 954296 954297 [...] (3306 flits)
Class 0:
Remaining flits: 1040832 1040833 1040834 1040835 1040836 1040837 1040838 1040839 1040840 1040841 [...] (50459 flits)
Measured flits: 1065276 1065277 1065278 1065279 1065280 1065281 1065282 1065283 1065284 1065285 [...] (2334 flits)
Class 0:
Remaining flits: 1040832 1040833 1040834 1040835 1040836 1040837 1040838 1040839 1040840 1040841 [...] (49410 flits)
Measured flits: 1186668 1186669 1186670 1186671 1186672 1186673 1186674 1186675 1186676 1186677 [...] (1253 flits)
Class 0:
Remaining flits: 1056870 1056871 1056872 1056873 1056874 1056875 1056876 1056877 1056878 1056879 [...] (49412 flits)
Measured flits: 1186668 1186669 1186670 1186671 1186672 1186673 1186674 1186675 1186676 1186677 [...] (981 flits)
Class 0:
Remaining flits: 1093440 1093441 1093442 1093443 1093444 1093445 1226070 1226071 1226072 1226073 [...] (49471 flits)
Measured flits: 1253407 1253408 1253409 1253410 1253411 1258591 1258592 1258593 1258594 1258595 [...] (589 flits)
Class 0:
Remaining flits: 1244826 1244827 1244828 1244829 1244830 1244831 1244832 1244833 1244834 1244835 [...] (48814 flits)
Measured flits: 1374902 1374903 1374904 1374905 1374906 1374907 1374908 1374909 1374910 1374911 [...] (145 flits)
Class 0:
Remaining flits: 1252836 1252837 1252838 1252839 1252840 1252841 1252842 1252843 1252844 1252845 [...] (48427 flits)
Measured flits: 1426266 1426267 1426268 1426269 1426270 1426271 1426272 1426273 1426274 1426275 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1372284 1372285 1372286 1372287 1372288 1372289 1372290 1372291 1372292 1372293 [...] (8109 flits)
Measured flits: (0 flits)
Time taken is 31243 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6011.57 (1 samples)
	minimum = 32 (1 samples)
	maximum = 19476 (1 samples)
Network latency average = 1008.24 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6154 (1 samples)
Flit latency average = 952.25 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6714 (1 samples)
Fragmentation average = 79.2035 (1 samples)
	minimum = 0 (1 samples)
	maximum = 366 (1 samples)
Injected packet rate average = 0.0148847 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0195714 (1 samples)
Accepted packet rate average = 0.014904 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.267824 (1 samples)
	minimum = 0.175143 (1 samples)
	maximum = 0.350429 (1 samples)
Accepted flit rate average = 0.268686 (1 samples)
	minimum = 0.21 (1 samples)
	maximum = 0.329714 (1 samples)
Injected packet size average = 17.9933 (1 samples)
Accepted packet size average = 18.0278 (1 samples)
Hops average = 5.05433 (1 samples)
Total run time 44.0781
