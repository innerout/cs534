BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 294.504
	minimum = 22
	maximum = 828
Network latency average = 282.596
	minimum = 22
	maximum = 806
Slowest packet = 1061
Flit latency average = 258.82
	minimum = 5
	maximum = 789
Slowest flit = 19115
Fragmentation average = 23.8867
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.0146615
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.270411
	minimum = 0.09 (at node 174)
	maximum = 0.432 (at node 103)
Injected packet length average = 17.8324
Accepted packet length average = 18.4437
Total in-flight flits = 69851 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 542.324
	minimum = 22
	maximum = 1523
Network latency average = 528.328
	minimum = 22
	maximum = 1509
Slowest packet = 2486
Flit latency average = 504.57
	minimum = 5
	maximum = 1492
Slowest flit = 54971
Fragmentation average = 24.6064
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0354844
	minimum = 0.0215 (at node 120)
	maximum = 0.0485 (at node 160)
Accepted packet rate average = 0.0155964
	minimum = 0.009 (at node 153)
	maximum = 0.021 (at node 67)
Injected flit rate average = 0.636125
	minimum = 0.387 (at node 120)
	maximum = 0.8695 (at node 160)
Accepted flit rate average= 0.283951
	minimum = 0.162 (at node 153)
	maximum = 0.3815 (at node 152)
Injected packet length average = 17.9269
Accepted packet length average = 18.2062
Total in-flight flits = 136231 (0 measured)
latency change    = 0.456958
throughput change = 0.0476811
Class 0:
Packet latency average = 1263.56
	minimum = 22
	maximum = 2225
Network latency average = 1248.91
	minimum = 22
	maximum = 2210
Slowest packet = 5312
Flit latency average = 1232
	minimum = 5
	maximum = 2193
Slowest flit = 96227
Fragmentation average = 26.6314
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0353229
	minimum = 0.02 (at node 116)
	maximum = 0.051 (at node 86)
Accepted packet rate average = 0.0161094
	minimum = 0.007 (at node 52)
	maximum = 0.029 (at node 159)
Injected flit rate average = 0.635583
	minimum = 0.36 (at node 190)
	maximum = 0.927 (at node 86)
Accepted flit rate average= 0.291245
	minimum = 0.126 (at node 52)
	maximum = 0.513 (at node 159)
Injected packet length average = 17.9935
Accepted packet length average = 18.0792
Total in-flight flits = 202388 (0 measured)
latency change    = 0.570797
throughput change = 0.0250452
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 53.5144
	minimum = 22
	maximum = 174
Network latency average = 38.9904
	minimum = 22
	maximum = 136
Slowest packet = 21177
Flit latency average = 1714.32
	minimum = 5
	maximum = 2859
Slowest flit = 135557
Fragmentation average = 4.66027
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0362344
	minimum = 0.019 (at node 168)
	maximum = 0.052 (at node 30)
Accepted packet rate average = 0.0163438
	minimum = 0.007 (at node 36)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.652
	minimum = 0.342 (at node 168)
	maximum = 0.936 (at node 30)
Accepted flit rate average= 0.293953
	minimum = 0.137 (at node 36)
	maximum = 0.53 (at node 129)
Injected packet length average = 17.994
Accepted packet length average = 17.9857
Total in-flight flits = 271175 (115753 measured)
latency change    = 22.6116
throughput change = 0.00921349
Class 0:
Packet latency average = 55.1086
	minimum = 22
	maximum = 205
Network latency average = 41.015
	minimum = 22
	maximum = 181
Slowest packet = 21177
Flit latency average = 1933.5
	minimum = 5
	maximum = 3655
Slowest flit = 147329
Fragmentation average = 4.78641
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0358438
	minimum = 0.0265 (at node 168)
	maximum = 0.0495 (at node 187)
Accepted packet rate average = 0.0165104
	minimum = 0.011 (at node 145)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.645065
	minimum = 0.477 (at node 168)
	maximum = 0.8905 (at node 187)
Accepted flit rate average= 0.296734
	minimum = 0.194 (at node 145)
	maximum = 0.4845 (at node 129)
Injected packet length average = 17.9966
Accepted packet length average = 17.9726
Total in-flight flits = 336194 (227195 measured)
latency change    = 0.0289277
throughput change = 0.00937286
Class 0:
Packet latency average = 55.5367
	minimum = 22
	maximum = 220
Network latency average = 41.1568
	minimum = 22
	maximum = 181
Slowest packet = 21177
Flit latency average = 2172.65
	minimum = 5
	maximum = 4324
Slowest flit = 195767
Fragmentation average = 4.73135
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0357135
	minimum = 0.0286667 (at node 132)
	maximum = 0.0486667 (at node 187)
Accepted packet rate average = 0.0165399
	minimum = 0.011 (at node 36)
	maximum = 0.0256667 (at node 129)
Injected flit rate average = 0.642964
	minimum = 0.517 (at node 132)
	maximum = 0.871 (at node 187)
Accepted flit rate average= 0.297557
	minimum = 0.198 (at node 36)
	maximum = 0.463333 (at node 129)
Injected packet length average = 18.0034
Accepted packet length average = 17.9902
Total in-flight flits = 401273 (339250 measured)
latency change    = 0.00770935
throughput change = 0.00276557
Draining remaining packets ...
Class 0:
Remaining flits: 247410 247411 247412 247413 247414 247415 247416 247417 247418 247419 [...] (353609 flits)
Measured flits: 367344 367345 367346 367347 367348 367349 367350 367351 367352 367353 [...] (332290 flits)
Class 0:
Remaining flits: 279576 279577 279578 279579 279580 279581 279582 279583 279584 279585 [...] (306271 flits)
Measured flits: 367866 367867 367868 367869 367870 367871 367872 367873 367874 367875 [...] (303262 flits)
Class 0:
Remaining flits: 310194 310195 310196 310197 310198 310199 310200 310201 310202 310203 [...] (259164 flits)
Measured flits: 368244 368245 368246 368247 368248 368249 368250 368251 368252 368253 [...] (258992 flits)
Class 0:
Remaining flits: 388314 388315 388316 388317 388318 388319 388320 388321 388322 388323 [...] (212645 flits)
Measured flits: 388314 388315 388316 388317 388318 388319 388320 388321 388322 388323 [...] (212645 flits)
Class 0:
Remaining flits: 459648 459649 459650 459651 459652 459653 459654 459655 459656 459657 [...] (164941 flits)
Measured flits: 459648 459649 459650 459651 459652 459653 459654 459655 459656 459657 [...] (164941 flits)
Class 0:
Remaining flits: 484496 484497 484498 484499 484500 484501 484502 484503 484504 484505 [...] (117461 flits)
Measured flits: 484496 484497 484498 484499 484500 484501 484502 484503 484504 484505 [...] (117461 flits)
Class 0:
Remaining flits: 528197 528198 528199 528200 528201 528202 528203 528204 528205 528206 [...] (70333 flits)
Measured flits: 528197 528198 528199 528200 528201 528202 528203 528204 528205 528206 [...] (70333 flits)
Class 0:
Remaining flits: 581328 581329 581330 581331 581332 581333 581334 581335 581336 581337 [...] (24489 flits)
Measured flits: 581328 581329 581330 581331 581332 581333 581334 581335 581336 581337 [...] (24489 flits)
Class 0:
Remaining flits: 653328 653329 653330 653331 653332 653333 653334 653335 653336 653337 [...] (1675 flits)
Measured flits: 653328 653329 653330 653331 653332 653333 653334 653335 653336 653337 [...] (1675 flits)
Time taken is 15496 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5893.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9987 (1 samples)
Network latency average = 5878.76 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9953 (1 samples)
Flit latency average = 4879.07 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9936 (1 samples)
Fragmentation average = 27.2404 (1 samples)
	minimum = 0 (1 samples)
	maximum = 104 (1 samples)
Injected packet rate average = 0.0357135 (1 samples)
	minimum = 0.0286667 (1 samples)
	maximum = 0.0486667 (1 samples)
Accepted packet rate average = 0.0165399 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0256667 (1 samples)
Injected flit rate average = 0.642964 (1 samples)
	minimum = 0.517 (1 samples)
	maximum = 0.871 (1 samples)
Accepted flit rate average = 0.297557 (1 samples)
	minimum = 0.198 (1 samples)
	maximum = 0.463333 (1 samples)
Injected packet size average = 18.0034 (1 samples)
Accepted packet size average = 17.9902 (1 samples)
Hops average = 5.07224 (1 samples)
Total run time 10.732
