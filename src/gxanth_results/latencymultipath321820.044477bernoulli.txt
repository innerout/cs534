BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 342.219
	minimum = 22
	maximum = 890
Network latency average = 316.504
	minimum = 22
	maximum = 848
Slowest packet = 923
Flit latency average = 273.636
	minimum = 5
	maximum = 852
Slowest flit = 16953
Fragmentation average = 78.838
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0292396
	minimum = 0.012 (at node 100)
	maximum = 0.042 (at node 158)
Accepted packet rate average = 0.0142448
	minimum = 0.006 (at node 115)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.519526
	minimum = 0.216 (at node 100)
	maximum = 0.752 (at node 158)
Accepted flit rate average= 0.269557
	minimum = 0.13 (at node 41)
	maximum = 0.468 (at node 44)
Injected packet length average = 17.7679
Accepted packet length average = 18.9232
Total in-flight flits = 49819 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 667.929
	minimum = 22
	maximum = 1726
Network latency average = 561.122
	minimum = 22
	maximum = 1705
Slowest packet = 1948
Flit latency average = 510.373
	minimum = 5
	maximum = 1814
Slowest flit = 24430
Fragmentation average = 78.9241
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0221224
	minimum = 0.012 (at node 184)
	maximum = 0.0305 (at node 29)
Accepted packet rate average = 0.0144844
	minimum = 0.0075 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.395292
	minimum = 0.216 (at node 184)
	maximum = 0.5435 (at node 29)
Accepted flit rate average= 0.266549
	minimum = 0.1435 (at node 153)
	maximum = 0.387 (at node 152)
Injected packet length average = 17.8684
Accepted packet length average = 18.4026
Total in-flight flits = 51545 (0 measured)
latency change    = 0.487642
throughput change = 0.0112843
Class 0:
Packet latency average = 1650.62
	minimum = 603
	maximum = 2549
Network latency average = 1001.37
	minimum = 22
	maximum = 2485
Slowest packet = 2563
Flit latency average = 938.111
	minimum = 5
	maximum = 2575
Slowest flit = 56597
Fragmentation average = 69.8713
	minimum = 0
	maximum = 298
Injected packet rate average = 0.015125
	minimum = 0 (at node 110)
	maximum = 0.027 (at node 65)
Accepted packet rate average = 0.0148073
	minimum = 0.004 (at node 38)
	maximum = 0.026 (at node 137)
Injected flit rate average = 0.27151
	minimum = 0 (at node 110)
	maximum = 0.491 (at node 65)
Accepted flit rate average= 0.266901
	minimum = 0.118 (at node 38)
	maximum = 0.479 (at node 137)
Injected packet length average = 17.9511
Accepted packet length average = 18.025
Total in-flight flits = 52428 (0 measured)
latency change    = 0.595348
throughput change = 0.0013172
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2284.92
	minimum = 1093
	maximum = 3161
Network latency average = 351.069
	minimum = 22
	maximum = 974
Slowest packet = 11462
Flit latency average = 954.322
	minimum = 5
	maximum = 2863
Slowest flit = 91742
Fragmentation average = 33.8
	minimum = 0
	maximum = 208
Injected packet rate average = 0.0148802
	minimum = 0.006 (at node 24)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0149792
	minimum = 0.006 (at node 4)
	maximum = 0.027 (at node 103)
Injected flit rate average = 0.269219
	minimum = 0.103 (at node 132)
	maximum = 0.48 (at node 115)
Accepted flit rate average= 0.269422
	minimum = 0.108 (at node 79)
	maximum = 0.473 (at node 103)
Injected packet length average = 18.0924
Accepted packet length average = 17.9864
Total in-flight flits = 52521 (41268 measured)
latency change    = 0.277601
throughput change = 0.00935645
Class 0:
Packet latency average = 2839.88
	minimum = 1093
	maximum = 4025
Network latency average = 759.303
	minimum = 22
	maximum = 1898
Slowest packet = 11462
Flit latency average = 963.011
	minimum = 5
	maximum = 3904
Slowest flit = 100392
Fragmentation average = 58.1429
	minimum = 0
	maximum = 311
Injected packet rate average = 0.0150495
	minimum = 0.0075 (at node 63)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0149583
	minimum = 0.0075 (at node 2)
	maximum = 0.022 (at node 83)
Injected flit rate average = 0.271495
	minimum = 0.1365 (at node 63)
	maximum = 0.4695 (at node 115)
Accepted flit rate average= 0.269703
	minimum = 0.151 (at node 2)
	maximum = 0.392 (at node 128)
Injected packet length average = 18.0401
Accepted packet length average = 18.0303
Total in-flight flits = 52956 (52147 measured)
latency change    = 0.195417
throughput change = 0.00104281
Class 0:
Packet latency average = 3243.83
	minimum = 1093
	maximum = 4841
Network latency average = 875.258
	minimum = 22
	maximum = 2601
Slowest packet = 11462
Flit latency average = 958.255
	minimum = 5
	maximum = 3928
Slowest flit = 100403
Fragmentation average = 66.4853
	minimum = 0
	maximum = 311
Injected packet rate average = 0.0148646
	minimum = 0.00833333 (at node 36)
	maximum = 0.0236667 (at node 115)
Accepted packet rate average = 0.0149132
	minimum = 0.00966667 (at node 36)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.267649
	minimum = 0.153667 (at node 36)
	maximum = 0.427667 (at node 115)
Accepted flit rate average= 0.269148
	minimum = 0.180333 (at node 2)
	maximum = 0.391333 (at node 128)
Injected packet length average = 18.0058
Accepted packet length average = 18.0476
Total in-flight flits = 51461 (51370 measured)
latency change    = 0.124528
throughput change = 0.00206413
Class 0:
Packet latency average = 3602.23
	minimum = 1093
	maximum = 5688
Network latency average = 928.101
	minimum = 22
	maximum = 3320
Slowest packet = 11462
Flit latency average = 959.414
	minimum = 5
	maximum = 4341
Slowest flit = 180629
Fragmentation average = 69.7808
	minimum = 0
	maximum = 311
Injected packet rate average = 0.014849
	minimum = 0.00725 (at node 92)
	maximum = 0.02175 (at node 137)
Accepted packet rate average = 0.0149297
	minimum = 0.01025 (at node 79)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.267449
	minimum = 0.12875 (at node 92)
	maximum = 0.38925 (at node 137)
Accepted flit rate average= 0.269046
	minimum = 0.18575 (at node 79)
	maximum = 0.356 (at node 128)
Injected packet length average = 18.0113
Accepted packet length average = 18.0208
Total in-flight flits = 51127 (51109 measured)
latency change    = 0.0994938
throughput change = 0.000379105
Class 0:
Packet latency average = 3948.11
	minimum = 1093
	maximum = 6404
Network latency average = 955.25
	minimum = 22
	maximum = 4041
Slowest packet = 11462
Flit latency average = 960.565
	minimum = 5
	maximum = 4451
Slowest flit = 201095
Fragmentation average = 72.8248
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0149625
	minimum = 0.0076 (at node 132)
	maximum = 0.021 (at node 137)
Accepted packet rate average = 0.0149354
	minimum = 0.0114 (at node 64)
	maximum = 0.0194 (at node 128)
Injected flit rate average = 0.269458
	minimum = 0.1348 (at node 132)
	maximum = 0.3786 (at node 137)
Accepted flit rate average= 0.269459
	minimum = 0.211 (at node 64)
	maximum = 0.3478 (at node 128)
Injected packet length average = 18.0089
Accepted packet length average = 18.0416
Total in-flight flits = 52209 (52209 measured)
latency change    = 0.0876073
throughput change = 0.00153568
Class 0:
Packet latency average = 4287.87
	minimum = 1093
	maximum = 7135
Network latency average = 968.063
	minimum = 22
	maximum = 4041
Slowest packet = 11462
Flit latency average = 958.003
	minimum = 5
	maximum = 4451
Slowest flit = 201095
Fragmentation average = 74.7674
	minimum = 0
	maximum = 352
Injected packet rate average = 0.0149714
	minimum = 0.00866667 (at node 36)
	maximum = 0.0203333 (at node 137)
Accepted packet rate average = 0.0149653
	minimum = 0.0115 (at node 79)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.269591
	minimum = 0.157667 (at node 36)
	maximum = 0.366 (at node 137)
Accepted flit rate average= 0.269964
	minimum = 0.207667 (at node 144)
	maximum = 0.350333 (at node 138)
Injected packet length average = 18.0071
Accepted packet length average = 18.0394
Total in-flight flits = 51713 (51713 measured)
latency change    = 0.0792377
throughput change = 0.00187075
Class 0:
Packet latency average = 4623.37
	minimum = 1093
	maximum = 7890
Network latency average = 976.719
	minimum = 22
	maximum = 4041
Slowest packet = 11462
Flit latency average = 954.909
	minimum = 5
	maximum = 4505
Slowest flit = 333057
Fragmentation average = 76.6606
	minimum = 0
	maximum = 352
Injected packet rate average = 0.0149948
	minimum = 0.00928571 (at node 164)
	maximum = 0.0212857 (at node 115)
Accepted packet rate average = 0.014997
	minimum = 0.0117143 (at node 190)
	maximum = 0.0188571 (at node 181)
Injected flit rate average = 0.270028
	minimum = 0.167143 (at node 164)
	maximum = 0.384714 (at node 115)
Accepted flit rate average= 0.270356
	minimum = 0.210286 (at node 190)
	maximum = 0.342143 (at node 181)
Injected packet length average = 18.0081
Accepted packet length average = 18.0273
Total in-flight flits = 51788 (51788 measured)
latency change    = 0.0725661
throughput change = 0.00144715
Draining all recorded packets ...
Class 0:
Remaining flits: 404010 404011 404012 404013 404014 404015 404016 404017 404018 404019 [...] (52073 flits)
Measured flits: 404010 404011 404012 404013 404014 404015 404016 404017 404018 404019 [...] (52073 flits)
Class 0:
Remaining flits: 449388 449389 449390 449391 449392 449393 449394 449395 449396 449397 [...] (51216 flits)
Measured flits: 449388 449389 449390 449391 449392 449393 449394 449395 449396 449397 [...] (51216 flits)
Class 0:
Remaining flits: 449388 449389 449390 449391 449392 449393 449394 449395 449396 449397 [...] (51394 flits)
Measured flits: 449388 449389 449390 449391 449392 449393 449394 449395 449396 449397 [...] (51394 flits)
Class 0:
Remaining flits: 557064 557065 557066 557067 557068 557069 557070 557071 557072 557073 [...] (50316 flits)
Measured flits: 557064 557065 557066 557067 557068 557069 557070 557071 557072 557073 [...] (50316 flits)
Class 0:
Remaining flits: 611658 611659 611660 611661 611662 611663 611664 611665 611666 611667 [...] (50834 flits)
Measured flits: 611658 611659 611660 611661 611662 611663 611664 611665 611666 611667 [...] (50834 flits)
Class 0:
Remaining flits: 638524 638525 638526 638527 638528 638529 638530 638531 648612 648613 [...] (50308 flits)
Measured flits: 638524 638525 638526 638527 638528 638529 638530 638531 648612 648613 [...] (50308 flits)
Class 0:
Remaining flits: 684784 684785 684786 684787 684788 684789 684790 684791 698818 698819 [...] (50675 flits)
Measured flits: 684784 684785 684786 684787 684788 684789 684790 684791 698818 698819 [...] (50675 flits)
Class 0:
Remaining flits: 741288 741289 741290 741291 741292 741293 760716 760717 760718 760719 [...] (50539 flits)
Measured flits: 741288 741289 741290 741291 741292 741293 760716 760717 760718 760719 [...] (50539 flits)
Class 0:
Remaining flits: 811638 811639 811640 811641 811642 811643 811644 811645 811646 811647 [...] (50396 flits)
Measured flits: 811638 811639 811640 811641 811642 811643 811644 811645 811646 811647 [...] (50396 flits)
Class 0:
Remaining flits: 857250 857251 857252 857253 857254 857255 857256 857257 857258 857259 [...] (50405 flits)
Measured flits: 857250 857251 857252 857253 857254 857255 857256 857257 857258 857259 [...] (50405 flits)
Class 0:
Remaining flits: 893016 893017 893018 893019 893020 893021 893022 893023 893024 893025 [...] (50042 flits)
Measured flits: 893016 893017 893018 893019 893020 893021 893022 893023 893024 893025 [...] (50042 flits)
Class 0:
Remaining flits: 973696 973697 973698 973699 973700 973701 973702 973703 973704 973705 [...] (49154 flits)
Measured flits: 973696 973697 973698 973699 973700 973701 973702 973703 973704 973705 [...] (49154 flits)
Class 0:
Remaining flits: 1027476 1027477 1027478 1027479 1027480 1027481 1027482 1027483 1027484 1027485 [...] (49408 flits)
Measured flits: 1027476 1027477 1027478 1027479 1027480 1027481 1027482 1027483 1027484 1027485 [...] (49300 flits)
Class 0:
Remaining flits: 1052010 1052011 1052012 1052013 1052014 1052015 1052016 1052017 1052018 1052019 [...] (48890 flits)
Measured flits: 1052010 1052011 1052012 1052013 1052014 1052015 1052016 1052017 1052018 1052019 [...] (48222 flits)
Class 0:
Remaining flits: 1052946 1052947 1052948 1052949 1052950 1052951 1052952 1052953 1052954 1052955 [...] (47546 flits)
Measured flits: 1052946 1052947 1052948 1052949 1052950 1052951 1052952 1052953 1052954 1052955 [...] (46040 flits)
Class 0:
Remaining flits: 1139976 1139977 1139978 1139979 1139980 1139981 1139982 1139983 1139984 1139985 [...] (49109 flits)
Measured flits: 1139976 1139977 1139978 1139979 1139980 1139981 1139982 1139983 1139984 1139985 [...] (42546 flits)
Class 0:
Remaining flits: 1223674 1223675 1233918 1233919 1233920 1233921 1233922 1233923 1233924 1233925 [...] (49927 flits)
Measured flits: 1223674 1223675 1233918 1233919 1233920 1233921 1233922 1233923 1233924 1233925 [...] (34634 flits)
Class 0:
Remaining flits: 1233931 1233932 1233933 1233934 1233935 1241928 1241929 1241930 1241931 1241932 [...] (49934 flits)
Measured flits: 1233931 1233932 1233933 1233934 1233935 1241928 1241929 1241930 1241931 1241932 [...] (27637 flits)
Class 0:
Remaining flits: 1346616 1346617 1346618 1346619 1346620 1346621 1346622 1346623 1346624 1346625 [...] (49933 flits)
Measured flits: 1346616 1346617 1346618 1346619 1346620 1346621 1346622 1346623 1346624 1346625 [...] (20502 flits)
Class 0:
Remaining flits: 1390212 1390213 1390214 1390215 1390216 1390217 1390218 1390219 1390220 1390221 [...] (49818 flits)
Measured flits: 1390212 1390213 1390214 1390215 1390216 1390217 1390218 1390219 1390220 1390221 [...] (14680 flits)
Class 0:
Remaining flits: 1390229 1408284 1408285 1408286 1408287 1408288 1408289 1408290 1408291 1408292 [...] (49705 flits)
Measured flits: 1390229 1408284 1408285 1408286 1408287 1408288 1408289 1408290 1408291 1408292 [...] (9531 flits)
Class 0:
Remaining flits: 1408284 1408285 1408286 1408287 1408288 1408289 1408290 1408291 1408292 1408293 [...] (50120 flits)
Measured flits: 1408284 1408285 1408286 1408287 1408288 1408289 1408290 1408291 1408292 1408293 [...] (8085 flits)
Class 0:
Remaining flits: 1464095 1464096 1464097 1464098 1464099 1464100 1464101 1528524 1528525 1528526 [...] (49950 flits)
Measured flits: 1464095 1464096 1464097 1464098 1464099 1464100 1464101 1543788 1543789 1543790 [...] (6222 flits)
Class 0:
Remaining flits: 1532322 1532323 1532324 1532325 1532326 1532327 1532328 1532329 1532330 1532331 [...] (49147 flits)
Measured flits: 1602477 1602478 1602479 1602480 1602481 1602482 1602483 1602484 1602485 1603440 [...] (4361 flits)
Class 0:
Remaining flits: 1640556 1640557 1640558 1640559 1640560 1640561 1640562 1640563 1640564 1640565 [...] (50090 flits)
Measured flits: 1656990 1656991 1656992 1656993 1656994 1656995 1656996 1656997 1656998 1656999 [...] (3025 flits)
Class 0:
Remaining flits: 1691856 1691857 1691858 1691859 1691860 1691861 1691862 1691863 1691864 1691865 [...] (49451 flits)
Measured flits: 1792098 1792099 1792100 1792101 1792102 1792103 1792104 1792105 1792106 1792107 [...] (2171 flits)
Class 0:
Remaining flits: 1728359 1733472 1733473 1733474 1733475 1733476 1733477 1733478 1733479 1733480 [...] (49464 flits)
Measured flits: 1827486 1827487 1827488 1827489 1827490 1827491 1827492 1827493 1827494 1827495 [...] (1742 flits)
Class 0:
Remaining flits: 1733472 1733473 1733474 1733475 1733476 1733477 1733478 1733479 1733480 1733481 [...] (49551 flits)
Measured flits: 1827486 1827487 1827488 1827489 1827490 1827491 1827492 1827493 1827494 1827495 [...] (1109 flits)
Class 0:
Remaining flits: 1733488 1733489 1799316 1799317 1799318 1799319 1799320 1799321 1799322 1799323 [...] (48023 flits)
Measured flits: 1932077 1932078 1932079 1932080 1932081 1932082 1932083 1973286 1973287 1973288 [...] (533 flits)
Class 0:
Remaining flits: 1817496 1817497 1817498 1817499 1817500 1817501 1817502 1817503 1817504 1817505 [...] (47649 flits)
Measured flits: 2007414 2007415 2007416 2007417 2007418 2007419 2007420 2007421 2007422 2007423 [...] (180 flits)
Class 0:
Remaining flits: 1817496 1817497 1817498 1817499 1817500 1817501 1817502 1817503 1817504 1817505 [...] (47552 flits)
Measured flits: 2065194 2065195 2065196 2065197 2065198 2065199 2065200 2065201 2065202 2065203 [...] (180 flits)
Class 0:
Remaining flits: 1909195 1909196 1909197 1909198 1909199 1909200 1909201 1909202 1909203 1909204 [...] (47547 flits)
Measured flits: 2142350 2142351 2142352 2142353 2142354 2142355 2142356 2142357 2142358 2142359 [...] (167 flits)
Class 0:
Remaining flits: 1944684 1944685 1944686 1944687 1944688 1944689 1944690 1944691 1944692 1944693 [...] (47318 flits)
Measured flits: 2183832 2183833 2183834 2183835 2183836 2183837 2183838 2183839 2183840 2183841 [...] (54 flits)
Class 0:
Remaining flits: 1944684 1944685 1944686 1944687 1944688 1944689 1944690 1944691 1944692 1944693 [...] (48160 flits)
Measured flits: 2183841 2183842 2183843 2183844 2183845 2183846 2183847 2183848 2183849 2208024 [...] (45 flits)
Class 0:
Remaining flits: 2057364 2057365 2057366 2057367 2057368 2057369 2057370 2057371 2057372 2057373 [...] (46798 flits)
Measured flits: 2285208 2285209 2285210 2285211 2285212 2285213 2285214 2285215 2285216 2285217 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2146644 2146645 2146646 2146647 2146648 2146649 2146650 2146651 2146652 2146653 [...] (10078 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2242869 2242870 2242871 2277973 2277974 2277975 2277976 2277977 2277978 2277979 [...] (227 flits)
Measured flits: (0 flits)
Time taken is 47409 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11518.5 (1 samples)
	minimum = 1093 (1 samples)
	maximum = 35301 (1 samples)
Network latency average = 994.159 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6109 (1 samples)
Flit latency average = 939.649 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7439 (1 samples)
Fragmentation average = 80.2003 (1 samples)
	minimum = 0 (1 samples)
	maximum = 363 (1 samples)
Injected packet rate average = 0.0149948 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0212857 (1 samples)
Accepted packet rate average = 0.014997 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.270028 (1 samples)
	minimum = 0.167143 (1 samples)
	maximum = 0.384714 (1 samples)
Accepted flit rate average = 0.270356 (1 samples)
	minimum = 0.210286 (1 samples)
	maximum = 0.342143 (1 samples)
Injected packet size average = 18.0081 (1 samples)
Accepted packet size average = 18.0273 (1 samples)
Hops average = 5.06072 (1 samples)
Total run time 68.3366
