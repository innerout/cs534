BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 255.616
	minimum = 24
	maximum = 936
Network latency average = 250.75
	minimum = 24
	maximum = 913
Slowest packet = 302
Flit latency average = 171.686
	minimum = 6
	maximum = 940
Slowest flit = 3335
Fragmentation average = 163.493
	minimum = 0
	maximum = 820
Injected packet rate average = 0.0195938
	minimum = 0.01 (at node 116)
	maximum = 0.034 (at node 17)
Accepted packet rate average = 0.00959896
	minimum = 0.002 (at node 127)
	maximum = 0.017 (at node 167)
Injected flit rate average = 0.348906
	minimum = 0.18 (at node 116)
	maximum = 0.597 (at node 66)
Accepted flit rate average= 0.206052
	minimum = 0.08 (at node 174)
	maximum = 0.33 (at node 167)
Injected packet length average = 17.807
Accepted packet length average = 21.4661
Total in-flight flits = 28154 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 431.093
	minimum = 24
	maximum = 1912
Network latency average = 414.963
	minimum = 24
	maximum = 1906
Slowest packet = 229
Flit latency average = 305.771
	minimum = 6
	maximum = 1889
Slowest flit = 4139
Fragmentation average = 228.917
	minimum = 0
	maximum = 1424
Injected packet rate average = 0.0185651
	minimum = 0.0095 (at node 58)
	maximum = 0.027 (at node 117)
Accepted packet rate average = 0.0110807
	minimum = 0.0055 (at node 108)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.331495
	minimum = 0.171 (at node 58)
	maximum = 0.483 (at node 117)
Accepted flit rate average= 0.217333
	minimum = 0.113 (at node 108)
	maximum = 0.3245 (at node 48)
Injected packet length average = 17.8558
Accepted packet length average = 19.6136
Total in-flight flits = 44956 (0 measured)
latency change    = 0.407051
throughput change = 0.0519076
Class 0:
Packet latency average = 836.31
	minimum = 25
	maximum = 2802
Network latency average = 757.452
	minimum = 25
	maximum = 2802
Slowest packet = 545
Flit latency average = 641.01
	minimum = 6
	maximum = 2937
Slowest flit = 3412
Fragmentation average = 287.111
	minimum = 1
	maximum = 2117
Injected packet rate average = 0.0142031
	minimum = 0 (at node 17)
	maximum = 0.033 (at node 161)
Accepted packet rate average = 0.0122656
	minimum = 0.005 (at node 132)
	maximum = 0.022 (at node 63)
Injected flit rate average = 0.25525
	minimum = 0 (at node 45)
	maximum = 0.599 (at node 161)
Accepted flit rate average= 0.224042
	minimum = 0.093 (at node 105)
	maximum = 0.438 (at node 63)
Injected packet length average = 17.9714
Accepted packet length average = 18.2658
Total in-flight flits = 51188 (0 measured)
latency change    = 0.48453
throughput change = 0.0299423
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 660.789
	minimum = 28
	maximum = 2424
Network latency average = 327.03
	minimum = 25
	maximum = 991
Slowest packet = 9938
Flit latency average = 833.485
	minimum = 6
	maximum = 3718
Slowest flit = 15839
Fragmentation average = 151.702
	minimum = 1
	maximum = 616
Injected packet rate average = 0.0116198
	minimum = 0 (at node 8)
	maximum = 0.035 (at node 45)
Accepted packet rate average = 0.0121667
	minimum = 0.005 (at node 162)
	maximum = 0.024 (at node 182)
Injected flit rate average = 0.208068
	minimum = 0 (at node 8)
	maximum = 0.634 (at node 45)
Accepted flit rate average= 0.214453
	minimum = 0.114 (at node 4)
	maximum = 0.374 (at node 182)
Injected packet length average = 17.9063
Accepted packet length average = 17.6263
Total in-flight flits = 50261 (24270 measured)
latency change    = 0.265624
throughput change = 0.0447116
Class 0:
Packet latency average = 1036.09
	minimum = 24
	maximum = 3422
Network latency average = 504.425
	minimum = 24
	maximum = 1941
Slowest packet = 9938
Flit latency average = 904.299
	minimum = 6
	maximum = 4599
Slowest flit = 19475
Fragmentation average = 188.221
	minimum = 1
	maximum = 1723
Injected packet rate average = 0.0118724
	minimum = 0 (at node 8)
	maximum = 0.027 (at node 23)
Accepted packet rate average = 0.0120026
	minimum = 0.006 (at node 131)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.212745
	minimum = 0 (at node 8)
	maximum = 0.48 (at node 23)
Accepted flit rate average= 0.213807
	minimum = 0.117 (at node 141)
	maximum = 0.306 (at node 173)
Injected packet length average = 17.9193
Accepted packet length average = 17.8134
Total in-flight flits = 51166 (35971 measured)
latency change    = 0.362226
throughput change = 0.00302063
Class 0:
Packet latency average = 1333.07
	minimum = 24
	maximum = 4245
Network latency average = 639.266
	minimum = 23
	maximum = 2991
Slowest packet = 9938
Flit latency average = 949.988
	minimum = 6
	maximum = 5691
Slowest flit = 12359
Fragmentation average = 206.937
	minimum = 0
	maximum = 2597
Injected packet rate average = 0.0123247
	minimum = 0 (at node 21)
	maximum = 0.026 (at node 35)
Accepted packet rate average = 0.0119931
	minimum = 0.007 (at node 48)
	maximum = 0.017 (at node 40)
Injected flit rate average = 0.22142
	minimum = 0 (at node 21)
	maximum = 0.465667 (at node 35)
Accepted flit rate average= 0.21424
	minimum = 0.123333 (at node 36)
	maximum = 0.312333 (at node 40)
Injected packet length average = 17.9656
Accepted packet length average = 17.8636
Total in-flight flits = 55676 (45862 measured)
latency change    = 0.222782
throughput change = 0.0020178
Class 0:
Packet latency average = 1583.35
	minimum = 24
	maximum = 5496
Network latency average = 752.07
	minimum = 23
	maximum = 3831
Slowest packet = 9938
Flit latency average = 1001.34
	minimum = 6
	maximum = 6534
Slowest flit = 30794
Fragmentation average = 221.345
	minimum = 0
	maximum = 3162
Injected packet rate average = 0.0119766
	minimum = 0 (at node 21)
	maximum = 0.02625 (at node 23)
Accepted packet rate average = 0.0118997
	minimum = 0.0075 (at node 131)
	maximum = 0.0155 (at node 40)
Injected flit rate average = 0.215171
	minimum = 0 (at node 21)
	maximum = 0.47375 (at node 23)
Accepted flit rate average= 0.212768
	minimum = 0.13875 (at node 135)
	maximum = 0.28025 (at node 177)
Injected packet length average = 17.966
Accepted packet length average = 17.8801
Total in-flight flits = 53346 (46881 measured)
latency change    = 0.158069
throughput change = 0.00691529
Class 0:
Packet latency average = 1825.62
	minimum = 24
	maximum = 6365
Network latency average = 846.975
	minimum = 23
	maximum = 4827
Slowest packet = 9938
Flit latency average = 1042.34
	minimum = 6
	maximum = 7427
Slowest flit = 22427
Fragmentation average = 229.638
	minimum = 0
	maximum = 4332
Injected packet rate average = 0.0118719
	minimum = 0 (at node 21)
	maximum = 0.0256 (at node 23)
Accepted packet rate average = 0.0117979
	minimum = 0.008 (at node 79)
	maximum = 0.015 (at node 95)
Injected flit rate average = 0.213293
	minimum = 0.0002 (at node 88)
	maximum = 0.4618 (at node 23)
Accepted flit rate average= 0.211457
	minimum = 0.1428 (at node 79)
	maximum = 0.2808 (at node 95)
Injected packet length average = 17.9662
Accepted packet length average = 17.9233
Total in-flight flits = 53389 (48917 measured)
latency change    = 0.132707
throughput change = 0.00619954
Class 0:
Packet latency average = 2080.03
	minimum = 24
	maximum = 6781
Network latency average = 919.486
	minimum = 23
	maximum = 5569
Slowest packet = 9938
Flit latency average = 1070.58
	minimum = 6
	maximum = 8363
Slowest flit = 33317
Fragmentation average = 238.654
	minimum = 0
	maximum = 4341
Injected packet rate average = 0.011842
	minimum = 0.000166667 (at node 176)
	maximum = 0.0241667 (at node 23)
Accepted packet rate average = 0.0117457
	minimum = 0.00833333 (at node 79)
	maximum = 0.0153333 (at node 95)
Injected flit rate average = 0.212877
	minimum = 0.000333333 (at node 176)
	maximum = 0.435833 (at node 23)
Accepted flit rate average= 0.210504
	minimum = 0.152333 (at node 79)
	maximum = 0.280167 (at node 95)
Injected packet length average = 17.9764
Accepted packet length average = 17.9219
Total in-flight flits = 54261 (51040 measured)
latency change    = 0.12231
throughput change = 0.00452699
Class 0:
Packet latency average = 2309.26
	minimum = 24
	maximum = 7913
Network latency average = 976.004
	minimum = 23
	maximum = 6720
Slowest packet = 9938
Flit latency average = 1093.81
	minimum = 6
	maximum = 9412
Slowest flit = 31283
Fragmentation average = 244.562
	minimum = 0
	maximum = 4991
Injected packet rate average = 0.0118185
	minimum = 0.000571429 (at node 119)
	maximum = 0.0225714 (at node 95)
Accepted packet rate average = 0.0117113
	minimum = 0.00857143 (at node 79)
	maximum = 0.0151429 (at node 95)
Injected flit rate average = 0.212626
	minimum = 0.0121429 (at node 119)
	maximum = 0.405714 (at node 95)
Accepted flit rate average= 0.210109
	minimum = 0.155143 (at node 79)
	maximum = 0.268429 (at node 138)
Injected packet length average = 17.991
Accepted packet length average = 17.9407
Total in-flight flits = 54839 (52564 measured)
latency change    = 0.0992658
throughput change = 0.00187981
Draining all recorded packets ...
Class 0:
Remaining flits: 29430 29431 29432 29433 29434 29435 29436 29437 29438 29439 [...] (51572 flits)
Measured flits: 179172 179173 179174 179175 179176 179177 179178 179179 179180 179181 [...] (48165 flits)
Class 0:
Remaining flits: 29443 29444 29445 29446 29447 42980 42981 42982 42983 52158 [...] (54887 flits)
Measured flits: 179181 179182 179183 179184 179185 179186 179187 179188 179189 179892 [...] (48168 flits)
Class 0:
Remaining flits: 29447 42980 42981 42982 42983 53748 53749 53750 53751 53752 [...] (53172 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (43236 flits)
Class 0:
Remaining flits: 53765 66258 66259 66260 66261 66262 66263 66264 66265 66266 [...] (53901 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (39485 flits)
Class 0:
Remaining flits: 66258 66259 66260 66261 66262 66263 66264 66265 66266 66267 [...] (55225 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (35617 flits)
Class 0:
Remaining flits: 66258 66259 66260 66261 66262 66263 66264 66265 66266 66267 [...] (56681 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (30350 flits)
Class 0:
Remaining flits: 66272 66273 66274 66275 76390 76391 80479 80480 80481 80482 [...] (58564 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (28626 flits)
Class 0:
Remaining flits: 80479 80480 80481 80482 80483 80484 80485 80486 80487 80488 [...] (58313 flits)
Measured flits: 180504 180505 180506 180507 180508 180509 180510 180511 180512 180513 [...] (25717 flits)
Class 0:
Remaining flits: 102751 102752 102753 102754 102755 102756 102757 102758 102759 102760 [...] (58789 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (21947 flits)
Class 0:
Remaining flits: 102751 102752 102753 102754 102755 102756 102757 102758 102759 102760 [...] (60508 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (18481 flits)
Class 0:
Remaining flits: 110358 110359 110360 110361 110362 110363 110364 110365 110366 110367 [...] (59493 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (15100 flits)
Class 0:
Remaining flits: 110369 110370 110371 110372 110373 110374 110375 181764 181765 181766 [...] (58063 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (12840 flits)
Class 0:
Remaining flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (58864 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (10667 flits)
Class 0:
Remaining flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (61443 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (9329 flits)
Class 0:
Remaining flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (61218 flits)
Measured flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (7958 flits)
Class 0:
Remaining flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (62131 flits)
Measured flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (6989 flits)
Class 0:
Remaining flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (64815 flits)
Measured flits: 191790 191791 191792 191793 191794 191795 191796 191797 191798 191799 [...] (6091 flits)
Class 0:
Remaining flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (64545 flits)
Measured flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (5139 flits)
Class 0:
Remaining flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (64742 flits)
Measured flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (4557 flits)
Class 0:
Remaining flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (64865 flits)
Measured flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (3864 flits)
Class 0:
Remaining flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (64373 flits)
Measured flits: 204732 204733 204734 204735 204736 204737 204738 204739 204740 204741 [...] (3486 flits)
Class 0:
Remaining flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (64721 flits)
Measured flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (3346 flits)
Class 0:
Remaining flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (65430 flits)
Measured flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (2993 flits)
Class 0:
Remaining flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (65428 flits)
Measured flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (2676 flits)
Class 0:
Remaining flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (65846 flits)
Measured flits: 392976 392977 392978 392979 392980 392981 392982 392983 392984 392985 [...] (2183 flits)
Class 0:
Remaining flits: 437040 437041 437042 437043 437044 437045 437046 437047 437048 437049 [...] (64078 flits)
Measured flits: 437040 437041 437042 437043 437044 437045 437046 437047 437048 437049 [...] (1789 flits)
Class 0:
Remaining flits: 437043 437044 437045 437046 437047 437048 437049 437050 437051 437052 [...] (64386 flits)
Measured flits: 437043 437044 437045 437046 437047 437048 437049 437050 437051 437052 [...] (1583 flits)
Class 0:
Remaining flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (63694 flits)
Measured flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (1261 flits)
Class 0:
Remaining flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (63762 flits)
Measured flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (1289 flits)
Class 0:
Remaining flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (63275 flits)
Measured flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (873 flits)
Class 0:
Remaining flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (62166 flits)
Measured flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (671 flits)
Class 0:
Remaining flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (63472 flits)
Measured flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (651 flits)
Class 0:
Remaining flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (64447 flits)
Measured flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (512 flits)
Class 0:
Remaining flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (63681 flits)
Measured flits: 636156 636157 636158 636159 636160 636161 636162 636163 636164 636165 [...] (418 flits)
Class 0:
Remaining flits: 690246 690247 690248 690249 690250 690251 690252 690253 690254 690255 [...] (63032 flits)
Measured flits: 690246 690247 690248 690249 690250 690251 690252 690253 690254 690255 [...] (342 flits)
Class 0:
Remaining flits: 780858 780859 780860 780861 780862 780863 780864 780865 780866 780867 [...] (63522 flits)
Measured flits: 784170 784171 784172 784173 784174 784175 784176 784177 784178 784179 [...] (281 flits)
Class 0:
Remaining flits: 780858 780859 780860 780861 780862 780863 780864 780865 780866 780867 [...] (64053 flits)
Measured flits: 793890 793891 793892 793893 793894 793895 793896 793897 793898 793899 [...] (198 flits)
Class 0:
Remaining flits: 780858 780859 780860 780861 780862 780863 780864 780865 780866 780867 [...] (63616 flits)
Measured flits: 793890 793891 793892 793893 793894 793895 793896 793897 793898 793899 [...] (108 flits)
Class 0:
Remaining flits: 780858 780859 780860 780861 780862 780863 780864 780865 780866 780867 [...] (64135 flits)
Measured flits: 793890 793891 793892 793893 793894 793895 793896 793897 793898 793899 [...] (108 flits)
Class 0:
Remaining flits: 780858 780859 780860 780861 780862 780863 780864 780865 780866 780867 [...] (60683 flits)
Measured flits: 820674 820675 820676 820677 820678 820679 820680 820681 820682 820683 [...] (54 flits)
Class 0:
Remaining flits: 790794 790795 790796 790797 790798 790799 790800 790801 790802 790803 [...] (62469 flits)
Measured flits: 820674 820675 820676 820677 820678 820679 820680 820681 820682 820683 [...] (54 flits)
Class 0:
Remaining flits: 790794 790795 790796 790797 790798 790799 790800 790801 790802 790803 [...] (61775 flits)
Measured flits: 820674 820675 820676 820677 820678 820679 820680 820681 820682 820683 [...] (36 flits)
Class 0:
Remaining flits: 790806 790807 790808 790809 790810 790811 820674 820675 820676 820677 [...] (61651 flits)
Measured flits: 820674 820675 820676 820677 820678 820679 820680 820681 820682 820683 [...] (36 flits)
Class 0:
Remaining flits: 820689 820690 820691 958698 958699 958700 958701 958702 958703 958704 [...] (61329 flits)
Measured flits: 820689 820690 820691 1528398 1528399 1528400 1528401 1528402 1528403 1528404 [...] (21 flits)
Class 0:
Remaining flits: 958698 958699 958700 958701 958702 958703 958704 958705 958706 958707 [...] (61944 flits)
Measured flits: 1528398 1528399 1528400 1528401 1528402 1528403 1528404 1528405 1528406 1528407 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 958705 958706 958707 958708 958709 958710 958711 958712 958713 958714 [...] (38067 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1012824 1012825 1012826 1012827 1012828 1012829 1012830 1012831 1012832 1012833 [...] (25815 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1012824 1012825 1012826 1012827 1012828 1012829 1012830 1012831 1012832 1012833 [...] (15156 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1066565 1066566 1066567 1066568 1066569 1066570 1066571 1126008 1126009 1126010 [...] (5906 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1126008 1126009 1126010 1126011 1126012 1126013 1126014 1126015 1126016 1126017 [...] (1431 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1607709 1607710 1607711 1607712 1607713 1607714 1607715 1607716 1607717 1607718 [...] (149 flits)
Measured flits: (0 flits)
Time taken is 61549 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6008.44 (1 samples)
	minimum = 24 (1 samples)
	maximum = 45728 (1 samples)
Network latency average = 1581.17 (1 samples)
	minimum = 23 (1 samples)
	maximum = 34991 (1 samples)
Flit latency average = 1762.06 (1 samples)
	minimum = 6 (1 samples)
	maximum = 34974 (1 samples)
Fragmentation average = 286.237 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14736 (1 samples)
Injected packet rate average = 0.0118185 (1 samples)
	minimum = 0.000571429 (1 samples)
	maximum = 0.0225714 (1 samples)
Accepted packet rate average = 0.0117113 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0151429 (1 samples)
Injected flit rate average = 0.212626 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.405714 (1 samples)
Accepted flit rate average = 0.210109 (1 samples)
	minimum = 0.155143 (1 samples)
	maximum = 0.268429 (1 samples)
Injected packet size average = 17.991 (1 samples)
Accepted packet size average = 17.9407 (1 samples)
Hops average = 5.06236 (1 samples)
Total run time 87.5671
