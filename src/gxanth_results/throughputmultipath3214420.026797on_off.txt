BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.429
	minimum = 22
	maximum = 967
Network latency average = 220.817
	minimum = 22
	maximum = 764
Slowest packet = 8
Flit latency average = 190.858
	minimum = 5
	maximum = 751
Slowest flit = 18159
Fragmentation average = 41.0479
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0133802
	minimum = 0.006 (at node 64)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.250219
	minimum = 0.119 (at node 64)
	maximum = 0.396 (at node 44)
Injected packet length average = 17.8353
Accepted packet length average = 18.7007
Total in-flight flits = 35658 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 528.17
	minimum = 22
	maximum = 1940
Network latency average = 399.487
	minimum = 22
	maximum = 1615
Slowest packet = 8
Flit latency average = 366.512
	minimum = 5
	maximum = 1598
Slowest flit = 31229
Fragmentation average = 52.5164
	minimum = 0
	maximum = 394
Injected packet rate average = 0.024125
	minimum = 0 (at node 30)
	maximum = 0.0545 (at node 91)
Accepted packet rate average = 0.0142005
	minimum = 0.007 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.432654
	minimum = 0 (at node 30)
	maximum = 0.974 (at node 91)
Accepted flit rate average= 0.261688
	minimum = 0.132 (at node 30)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.9338
Accepted packet length average = 18.428
Total in-flight flits = 66264 (0 measured)
latency change    = 0.391429
throughput change = 0.0438261
Class 0:
Packet latency average = 1057.15
	minimum = 25
	maximum = 2887
Network latency average = 892.939
	minimum = 22
	maximum = 2227
Slowest packet = 4621
Flit latency average = 858.431
	minimum = 5
	maximum = 2324
Slowest flit = 54693
Fragmentation average = 73.7834
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0248958
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0152656
	minimum = 0.005 (at node 163)
	maximum = 0.027 (at node 97)
Injected flit rate average = 0.44726
	minimum = 0 (at node 7)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.276583
	minimum = 0.093 (at node 117)
	maximum = 0.483 (at node 97)
Injected packet length average = 17.9653
Accepted packet length average = 18.118
Total in-flight flits = 99200 (0 measured)
latency change    = 0.500382
throughput change = 0.0538566
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 331.68
	minimum = 22
	maximum = 1528
Network latency average = 161.862
	minimum = 22
	maximum = 964
Slowest packet = 14045
Flit latency average = 1177.61
	minimum = 5
	maximum = 2824
Slowest flit = 96686
Fragmentation average = 14.5398
	minimum = 0
	maximum = 183
Injected packet rate average = 0.026526
	minimum = 0 (at node 49)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0157656
	minimum = 0.006 (at node 84)
	maximum = 0.031 (at node 59)
Injected flit rate average = 0.477812
	minimum = 0 (at node 49)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.283609
	minimum = 0.118 (at node 84)
	maximum = 0.545 (at node 59)
Injected packet length average = 18.013
Accepted packet length average = 17.9891
Total in-flight flits = 136475 (81003 measured)
latency change    = 2.18725
throughput change = 0.0247737
Class 0:
Packet latency average = 762.887
	minimum = 22
	maximum = 2500
Network latency average = 592.978
	minimum = 22
	maximum = 1968
Slowest packet = 14045
Flit latency average = 1363.95
	minimum = 5
	maximum = 3504
Slowest flit = 120671
Fragmentation average = 35.4442
	minimum = 0
	maximum = 315
Injected packet rate average = 0.0260964
	minimum = 0.0005 (at node 191)
	maximum = 0.0555 (at node 3)
Accepted packet rate average = 0.0158151
	minimum = 0.0095 (at node 4)
	maximum = 0.0225 (at node 128)
Injected flit rate average = 0.469693
	minimum = 0.009 (at node 191)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.283544
	minimum = 0.177 (at node 135)
	maximum = 0.4125 (at node 128)
Injected packet length average = 17.9984
Accepted packet length average = 17.9287
Total in-flight flits = 170787 (148786 measured)
latency change    = 0.56523
throughput change = 0.000229608
Class 0:
Packet latency average = 1319.81
	minimum = 22
	maximum = 3811
Network latency average = 1147.61
	minimum = 22
	maximum = 2963
Slowest packet = 14045
Flit latency average = 1550.24
	minimum = 5
	maximum = 4344
Slowest flit = 134045
Fragmentation average = 49.6576
	minimum = 0
	maximum = 429
Injected packet rate average = 0.0255104
	minimum = 0.005 (at node 160)
	maximum = 0.0526667 (at node 135)
Accepted packet rate average = 0.0157431
	minimum = 0.0113333 (at node 52)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.459259
	minimum = 0.09 (at node 160)
	maximum = 0.945333 (at node 135)
Accepted flit rate average= 0.282938
	minimum = 0.204 (at node 52)
	maximum = 0.384 (at node 129)
Injected packet length average = 18.0028
Accepted packet length average = 17.9722
Total in-flight flits = 200864 (194507 measured)
latency change    = 0.421974
throughput change = 0.00214454
Draining remaining packets ...
Class 0:
Remaining flits: 152804 152805 152806 152807 152808 152809 152810 152811 152812 152813 [...] (153239 flits)
Measured flits: 253134 253135 253136 253137 253138 253139 253140 253141 253142 253143 [...] (151486 flits)
Class 0:
Remaining flits: 179389 179390 179391 179392 179393 179394 179395 179396 179397 179398 [...] (105737 flits)
Measured flits: 253998 253999 254000 254001 254002 254003 254004 254005 254006 254007 [...] (105434 flits)
Class 0:
Remaining flits: 229041 229042 229043 229044 229045 229046 229047 229048 229049 230778 [...] (60056 flits)
Measured flits: 255384 255385 255386 255387 255388 255389 255390 255391 255392 255393 [...] (59993 flits)
Class 0:
Remaining flits: 255384 255385 255386 255387 255388 255389 255390 255391 255392 255393 [...] (26213 flits)
Measured flits: 255384 255385 255386 255387 255388 255389 255390 255391 255392 255393 [...] (26213 flits)
Class 0:
Remaining flits: 320652 320653 320654 320655 320656 320657 320658 320659 320660 320661 [...] (8503 flits)
Measured flits: 320652 320653 320654 320655 320656 320657 320658 320659 320660 320661 [...] (8503 flits)
Class 0:
Remaining flits: 325620 325621 325622 325623 325624 325625 325626 325627 325628 325629 [...] (1589 flits)
Measured flits: 325620 325621 325622 325623 325624 325625 325626 325627 325628 325629 [...] (1589 flits)
Time taken is 12797 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3185.95 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9113 (1 samples)
Network latency average = 2978.23 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8612 (1 samples)
Flit latency average = 2679.41 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8595 (1 samples)
Fragmentation average = 55.8525 (1 samples)
	minimum = 0 (1 samples)
	maximum = 633 (1 samples)
Injected packet rate average = 0.0255104 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0526667 (1 samples)
Accepted packet rate average = 0.0157431 (1 samples)
	minimum = 0.0113333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.459259 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.945333 (1 samples)
Accepted flit rate average = 0.282938 (1 samples)
	minimum = 0.204 (1 samples)
	maximum = 0.384 (1 samples)
Injected packet size average = 18.0028 (1 samples)
Accepted packet size average = 17.9722 (1 samples)
Hops average = 5.08747 (1 samples)
Total run time 9.52889
