BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 286.53
	minimum = 22
	maximum = 823
Network latency average = 274.053
	minimum = 22
	maximum = 818
Slowest packet = 758
Flit latency average = 240.175
	minimum = 5
	maximum = 814
Slowest flit = 18203
Fragmentation average = 56.2302
	minimum = 0
	maximum = 305
Injected packet rate average = 0.0285938
	minimum = 0.013 (at node 52)
	maximum = 0.04 (at node 53)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 115)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.508839
	minimum = 0.227 (at node 52)
	maximum = 0.719 (at node 66)
Accepted flit rate average= 0.268604
	minimum = 0.111 (at node 115)
	maximum = 0.443 (at node 152)
Injected packet length average = 17.7954
Accepted packet length average = 18.877
Total in-flight flits = 47716 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 555.103
	minimum = 22
	maximum = 1754
Network latency average = 503.074
	minimum = 22
	maximum = 1754
Slowest packet = 1082
Flit latency average = 461.173
	minimum = 5
	maximum = 1737
Slowest flit = 19493
Fragmentation average = 60.4769
	minimum = 0
	maximum = 305
Injected packet rate average = 0.0223464
	minimum = 0.0125 (at node 32)
	maximum = 0.0305 (at node 122)
Accepted packet rate average = 0.0146354
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.399458
	minimum = 0.222 (at node 32)
	maximum = 0.548 (at node 122)
Accepted flit rate average= 0.268328
	minimum = 0.1495 (at node 153)
	maximum = 0.388 (at node 152)
Injected packet length average = 17.8758
Accepted packet length average = 18.3342
Total in-flight flits = 52518 (0 measured)
latency change    = 0.483826
throughput change = 0.00102875
Class 0:
Packet latency average = 1370.16
	minimum = 115
	maximum = 2482
Network latency average = 987.104
	minimum = 22
	maximum = 2441
Slowest packet = 3267
Flit latency average = 932.889
	minimum = 5
	maximum = 2424
Slowest flit = 58823
Fragmentation average = 61.5478
	minimum = 0
	maximum = 311
Injected packet rate average = 0.014875
	minimum = 0.004 (at node 168)
	maximum = 0.027 (at node 55)
Accepted packet rate average = 0.0145938
	minimum = 0.006 (at node 147)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.266698
	minimum = 0.076 (at node 168)
	maximum = 0.488 (at node 55)
Accepted flit rate average= 0.264359
	minimum = 0.108 (at node 190)
	maximum = 0.467 (at node 16)
Injected packet length average = 17.9293
Accepted packet length average = 18.1146
Total in-flight flits = 52917 (0 measured)
latency change    = 0.594864
throughput change = 0.0150127
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1801.27
	minimum = 625
	maximum = 2914
Network latency average = 356.383
	minimum = 22
	maximum = 974
Slowest packet = 11531
Flit latency average = 976.841
	minimum = 5
	maximum = 3038
Slowest flit = 87717
Fragmentation average = 30.6512
	minimum = 0
	maximum = 186
Injected packet rate average = 0.0147031
	minimum = 0.002 (at node 17)
	maximum = 0.03 (at node 186)
Accepted packet rate average = 0.0149792
	minimum = 0.005 (at node 135)
	maximum = 0.026 (at node 90)
Injected flit rate average = 0.265172
	minimum = 0.047 (at node 17)
	maximum = 0.544 (at node 186)
Accepted flit rate average= 0.27001
	minimum = 0.104 (at node 135)
	maximum = 0.454 (at node 90)
Injected packet length average = 18.0351
Accepted packet length average = 18.0257
Total in-flight flits = 51979 (41190 measured)
latency change    = 0.239337
throughput change = 0.020929
Class 0:
Packet latency average = 2319.55
	minimum = 625
	maximum = 3511
Network latency average = 756.777
	minimum = 22
	maximum = 1958
Slowest packet = 11531
Flit latency average = 969.136
	minimum = 5
	maximum = 3584
Slowest flit = 111797
Fragmentation average = 56.3193
	minimum = 0
	maximum = 289
Injected packet rate average = 0.014888
	minimum = 0.005 (at node 0)
	maximum = 0.0235 (at node 154)
Accepted packet rate average = 0.0149714
	minimum = 0.006 (at node 135)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.268164
	minimum = 0.093 (at node 0)
	maximum = 0.421 (at node 186)
Accepted flit rate average= 0.269667
	minimum = 0.1225 (at node 135)
	maximum = 0.3855 (at node 129)
Injected packet length average = 18.0121
Accepted packet length average = 18.0122
Total in-flight flits = 52235 (51677 measured)
latency change    = 0.223439
throughput change = 0.00127472
Class 0:
Packet latency average = 2674.22
	minimum = 625
	maximum = 4424
Network latency average = 888.145
	minimum = 22
	maximum = 2757
Slowest packet = 11531
Flit latency average = 968.86
	minimum = 5
	maximum = 3984
Slowest flit = 152261
Fragmentation average = 62.7348
	minimum = 0
	maximum = 289
Injected packet rate average = 0.0149236
	minimum = 0.00566667 (at node 0)
	maximum = 0.0243333 (at node 2)
Accepted packet rate average = 0.0149253
	minimum = 0.009 (at node 135)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.269066
	minimum = 0.104 (at node 0)
	maximum = 0.442 (at node 2)
Accepted flit rate average= 0.26905
	minimum = 0.169 (at node 135)
	maximum = 0.37 (at node 129)
Injected packet length average = 18.0295
Accepted packet length average = 18.0264
Total in-flight flits = 52960 (52904 measured)
latency change    = 0.132626
throughput change = 0.00229072
Draining remaining packets ...
Class 0:
Remaining flits: 225576 225577 225578 225579 225580 225581 225582 225583 225584 225585 [...] (7491 flits)
Measured flits: 225576 225577 225578 225579 225580 225581 225582 225583 225584 225585 [...] (7491 flits)
Time taken is 7894 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3035.03 (1 samples)
	minimum = 625 (1 samples)
	maximum = 6411 (1 samples)
Network latency average = 1017.25 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4080 (1 samples)
Flit latency average = 1024.57 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4065 (1 samples)
Fragmentation average = 67 (1 samples)
	minimum = 0 (1 samples)
	maximum = 307 (1 samples)
Injected packet rate average = 0.0149236 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0149253 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.269066 (1 samples)
	minimum = 0.104 (1 samples)
	maximum = 0.442 (1 samples)
Accepted flit rate average = 0.26905 (1 samples)
	minimum = 0.169 (1 samples)
	maximum = 0.37 (1 samples)
Injected packet size average = 18.0295 (1 samples)
Accepted packet size average = 18.0264 (1 samples)
Hops average = 5.09452 (1 samples)
Total run time 9.38456
