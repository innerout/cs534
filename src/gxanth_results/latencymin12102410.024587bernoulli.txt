BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 272.024
	minimum = 23
	maximum = 923
Network latency average = 266.727
	minimum = 23
	maximum = 918
Slowest packet = 228
Flit latency average = 232.685
	minimum = 6
	maximum = 901
Slowest flit = 4121
Fragmentation average = 56.9437
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0246823
	minimum = 0.014 (at node 179)
	maximum = 0.039 (at node 160)
Accepted packet rate average = 0.00990625
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 95)
Injected flit rate average = 0.440411
	minimum = 0.237 (at node 179)
	maximum = 0.695 (at node 160)
Accepted flit rate average= 0.186214
	minimum = 0.04 (at node 174)
	maximum = 0.329 (at node 131)
Injected packet length average = 17.8432
Accepted packet length average = 18.7976
Total in-flight flits = 49549 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 515.167
	minimum = 23
	maximum = 1779
Network latency average = 509.209
	minimum = 23
	maximum = 1739
Slowest packet = 918
Flit latency average = 473.177
	minimum = 6
	maximum = 1772
Slowest flit = 18384
Fragmentation average = 61.247
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0244792
	minimum = 0.016 (at node 17)
	maximum = 0.034 (at node 160)
Accepted packet rate average = 0.0101745
	minimum = 0.0055 (at node 164)
	maximum = 0.016 (at node 119)
Injected flit rate average = 0.438792
	minimum = 0.288 (at node 17)
	maximum = 0.606 (at node 160)
Accepted flit rate average= 0.18712
	minimum = 0.099 (at node 164)
	maximum = 0.288 (at node 119)
Injected packet length average = 17.9251
Accepted packet length average = 18.3911
Total in-flight flits = 97346 (0 measured)
latency change    = 0.47197
throughput change = 0.00484315
Class 0:
Packet latency average = 1219.18
	minimum = 23
	maximum = 2626
Network latency average = 1212.13
	minimum = 23
	maximum = 2584
Slowest packet = 1025
Flit latency average = 1186.3
	minimum = 6
	maximum = 2620
Slowest flit = 31720
Fragmentation average = 64.6083
	minimum = 0
	maximum = 141
Injected packet rate average = 0.024599
	minimum = 0.014 (at node 8)
	maximum = 0.037 (at node 48)
Accepted packet rate average = 0.0105052
	minimum = 0.003 (at node 20)
	maximum = 0.02 (at node 47)
Injected flit rate average = 0.442599
	minimum = 0.252 (at node 8)
	maximum = 0.649 (at node 48)
Accepted flit rate average= 0.189078
	minimum = 0.054 (at node 20)
	maximum = 0.354 (at node 137)
Injected packet length average = 17.9926
Accepted packet length average = 17.9985
Total in-flight flits = 146057 (0 measured)
latency change    = 0.577449
throughput change = 0.0103573
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 66.7632
	minimum = 23
	maximum = 349
Network latency average = 59.6368
	minimum = 23
	maximum = 334
Slowest packet = 16363
Flit latency average = 1692.47
	minimum = 6
	maximum = 3431
Slowest flit = 45809
Fragmentation average = 9.87105
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0249844
	minimum = 0.012 (at node 14)
	maximum = 0.041 (at node 91)
Accepted packet rate average = 0.0106562
	minimum = 0.004 (at node 154)
	maximum = 0.02 (at node 78)
Injected flit rate average = 0.449016
	minimum = 0.215 (at node 14)
	maximum = 0.741 (at node 91)
Accepted flit rate average= 0.192219
	minimum = 0.072 (at node 154)
	maximum = 0.358 (at node 78)
Injected packet length average = 17.9719
Accepted packet length average = 18.0381
Total in-flight flits = 195497 (79328 measured)
latency change    = 17.2613
throughput change = 0.0163388
Class 0:
Packet latency average = 112.416
	minimum = 23
	maximum = 1952
Network latency average = 103.388
	minimum = 23
	maximum = 1944
Slowest packet = 14241
Flit latency average = 1937.51
	minimum = 6
	maximum = 4287
Slowest flit = 60321
Fragmentation average = 12.0139
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0245052
	minimum = 0.017 (at node 34)
	maximum = 0.036 (at node 10)
Accepted packet rate average = 0.0106146
	minimum = 0.005 (at node 96)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.440969
	minimum = 0.304 (at node 185)
	maximum = 0.65 (at node 10)
Accepted flit rate average= 0.190654
	minimum = 0.09 (at node 154)
	maximum = 0.323 (at node 90)
Injected packet length average = 17.9949
Accepted packet length average = 17.9615
Total in-flight flits = 242226 (154902 measured)
latency change    = 0.406104
throughput change = 0.00820915
Class 0:
Packet latency average = 362.151
	minimum = 23
	maximum = 2959
Network latency average = 351.44
	minimum = 23
	maximum = 2959
Slowest packet = 14142
Flit latency average = 2188.02
	minimum = 6
	maximum = 5036
Slowest flit = 79505
Fragmentation average = 18.5251
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0243628
	minimum = 0.018 (at node 14)
	maximum = 0.0323333 (at node 10)
Accepted packet rate average = 0.0105712
	minimum = 0.00666667 (at node 4)
	maximum = 0.0163333 (at node 90)
Injected flit rate average = 0.438215
	minimum = 0.324 (at node 14)
	maximum = 0.587 (at node 10)
Accepted flit rate average= 0.190458
	minimum = 0.12 (at node 4)
	maximum = 0.290667 (at node 90)
Injected packet length average = 17.987
Accepted packet length average = 18.0168
Total in-flight flits = 288965 (228321 measured)
latency change    = 0.689589
throughput change = 0.00102549
Class 0:
Packet latency average = 860.25
	minimum = 23
	maximum = 3979
Network latency average = 845.51
	minimum = 23
	maximum = 3961
Slowest packet = 14170
Flit latency average = 2446.82
	minimum = 6
	maximum = 5725
Slowest flit = 104507
Fragmentation average = 28.7737
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0240273
	minimum = 0.0175 (at node 160)
	maximum = 0.0305 (at node 26)
Accepted packet rate average = 0.0104753
	minimum = 0.0065 (at node 31)
	maximum = 0.0145 (at node 90)
Injected flit rate average = 0.432177
	minimum = 0.31875 (at node 160)
	maximum = 0.54875 (at node 26)
Accepted flit rate average= 0.188449
	minimum = 0.11775 (at node 31)
	maximum = 0.262 (at node 90)
Injected packet length average = 17.9869
Accepted packet length average = 17.9899
Total in-flight flits = 333500 (294475 measured)
latency change    = 0.579017
throughput change = 0.0106613
Class 0:
Packet latency average = 1521.13
	minimum = 23
	maximum = 4993
Network latency average = 1500.25
	minimum = 23
	maximum = 4992
Slowest packet = 14135
Flit latency average = 2722.96
	minimum = 6
	maximum = 6373
Slowest flit = 136615
Fragmentation average = 38.6551
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0238531
	minimum = 0.0166 (at node 160)
	maximum = 0.0312 (at node 26)
Accepted packet rate average = 0.0103375
	minimum = 0.0076 (at node 161)
	maximum = 0.0138 (at node 90)
Injected flit rate average = 0.429143
	minimum = 0.3014 (at node 160)
	maximum = 0.5616 (at node 26)
Accepted flit rate average= 0.18607
	minimum = 0.1364 (at node 161)
	maximum = 0.2516 (at node 90)
Injected packet length average = 17.991
Accepted packet length average = 17.9995
Total in-flight flits = 379630 (356449 measured)
latency change    = 0.434467
throughput change = 0.0127878
Class 0:
Packet latency average = 2242.19
	minimum = 23
	maximum = 5934
Network latency average = 2212.52
	minimum = 23
	maximum = 5920
Slowest packet = 14227
Flit latency average = 3010.93
	minimum = 6
	maximum = 7481
Slowest flit = 123767
Fragmentation average = 46.2192
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0235521
	minimum = 0.0168333 (at node 160)
	maximum = 0.0311667 (at node 26)
Accepted packet rate average = 0.0102769
	minimum = 0.0075 (at node 48)
	maximum = 0.014 (at node 90)
Injected flit rate average = 0.42362
	minimum = 0.303 (at node 160)
	maximum = 0.5595 (at node 26)
Accepted flit rate average= 0.18489
	minimum = 0.135333 (at node 48)
	maximum = 0.253167 (at node 90)
Injected packet length average = 17.9865
Accepted packet length average = 17.9908
Total in-flight flits = 421458 (409054 measured)
latency change    = 0.321587
throughput change = 0.00638237
Class 0:
Packet latency average = 2922.48
	minimum = 23
	maximum = 6994
Network latency average = 2889.3
	minimum = 23
	maximum = 6994
Slowest packet = 14145
Flit latency average = 3309.18
	minimum = 6
	maximum = 8292
Slowest flit = 136997
Fragmentation average = 51.0064
	minimum = 0
	maximum = 149
Injected packet rate average = 0.023314
	minimum = 0.0164286 (at node 160)
	maximum = 0.0297143 (at node 26)
Accepted packet rate average = 0.0102708
	minimum = 0.00771429 (at node 190)
	maximum = 0.0141429 (at node 90)
Injected flit rate average = 0.419568
	minimum = 0.296 (at node 160)
	maximum = 0.534857 (at node 26)
Accepted flit rate average= 0.184846
	minimum = 0.138143 (at node 190)
	maximum = 0.253143 (at node 90)
Injected packet length average = 17.9964
Accepted packet length average = 17.9972
Total in-flight flits = 461636 (455694 measured)
latency change    = 0.232776
throughput change = 0.000236818
Draining all recorded packets ...
Class 0:
Remaining flits: 164790 164791 164792 164793 164794 164795 164796 164797 164798 164799 [...] (504887 flits)
Measured flits: 254448 254449 254450 254451 254452 254453 254454 254455 254456 254457 [...] (439414 flits)
Class 0:
Remaining flits: 180666 180667 180668 180669 180670 180671 180672 180673 180674 180675 [...] (547209 flits)
Measured flits: 254448 254449 254450 254451 254452 254453 254454 254455 254456 254457 [...] (418725 flits)
Class 0:
Remaining flits: 181980 181981 181982 181983 181984 181985 181986 181987 181988 181989 [...] (590007 flits)
Measured flits: 256194 256195 256196 256197 256198 256199 256200 256201 256202 256203 [...] (393658 flits)
Class 0:
Remaining flits: 189432 189433 189434 189435 189436 189437 189438 189439 189440 189441 [...] (631343 flits)
Measured flits: 260406 260407 260408 260409 260410 260411 260412 260413 260414 260415 [...] (365986 flits)
Class 0:
Remaining flits: 227160 227161 227162 227163 227164 227165 227166 227167 227168 227169 [...] (674410 flits)
Measured flits: 260406 260407 260408 260409 260410 260411 260412 260413 260414 260415 [...] (336737 flits)
Class 0:
Remaining flits: 250884 250885 250886 250887 250888 250889 250890 250891 250892 250893 [...] (716695 flits)
Measured flits: 275886 275887 275888 275889 275890 275891 275892 275893 275894 275895 [...] (307989 flits)
Class 0:
Remaining flits: 276262 276263 277812 277813 277814 277815 277816 277817 277818 277819 [...] (760303 flits)
Measured flits: 276262 276263 277812 277813 277814 277815 277816 277817 277818 277819 [...] (278272 flits)
Class 0:
Remaining flits: 349542 349543 349544 349545 349546 349547 349548 349549 349550 349551 [...] (802054 flits)
Measured flits: 349542 349543 349544 349545 349546 349547 349548 349549 349550 349551 [...] (248545 flits)
Class 0:
Remaining flits: 375480 375481 375482 375483 375484 375485 375486 375487 375488 375489 [...] (844123 flits)
Measured flits: 375480 375481 375482 375483 375484 375485 375486 375487 375488 375489 [...] (219594 flits)
Class 0:
Remaining flits: 395964 395965 395966 395967 395968 395969 395970 395971 395972 395973 [...] (884905 flits)
Measured flits: 395964 395965 395966 395967 395968 395969 395970 395971 395972 395973 [...] (190957 flits)
Class 0:
Remaining flits: 395964 395965 395966 395967 395968 395969 395970 395971 395972 395973 [...] (920748 flits)
Measured flits: 395964 395965 395966 395967 395968 395969 395970 395971 395972 395973 [...] (163064 flits)
Class 0:
Remaining flits: 401724 401725 401726 401727 401728 401729 401730 401731 401732 401733 [...] (950140 flits)
Measured flits: 401724 401725 401726 401727 401728 401729 401730 401731 401732 401733 [...] (136024 flits)
Class 0:
Remaining flits: 457200 457201 457202 457203 457204 457205 457206 457207 457208 457209 [...] (965697 flits)
Measured flits: 457200 457201 457202 457203 457204 457205 457206 457207 457208 457209 [...] (110255 flits)
Class 0:
Remaining flits: 470016 470017 470018 470019 470020 470021 470022 470023 470024 470025 [...] (976246 flits)
Measured flits: 470016 470017 470018 470019 470020 470021 470022 470023 470024 470025 [...] (87774 flits)
Class 0:
Remaining flits: 470026 470027 470028 470029 470030 470031 470032 470033 499194 499195 [...] (978805 flits)
Measured flits: 470026 470027 470028 470029 470030 470031 470032 470033 499194 499195 [...] (68214 flits)
Class 0:
Remaining flits: 503514 503515 503516 503517 503518 503519 503520 503521 503522 503523 [...] (979299 flits)
Measured flits: 503514 503515 503516 503517 503518 503519 503520 503521 503522 503523 [...] (50821 flits)
Class 0:
Remaining flits: 512586 512587 512588 512589 512590 512591 512592 512593 512594 512595 [...] (977502 flits)
Measured flits: 512586 512587 512588 512589 512590 512591 512592 512593 512594 512595 [...] (37421 flits)
Class 0:
Remaining flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (974837 flits)
Measured flits: 537912 537913 537914 537915 537916 537917 537918 537919 537920 537921 [...] (26117 flits)
Class 0:
Remaining flits: 558288 558289 558290 558291 558292 558293 558294 558295 558296 558297 [...] (970705 flits)
Measured flits: 558288 558289 558290 558291 558292 558293 558294 558295 558296 558297 [...] (17751 flits)
Class 0:
Remaining flits: 558288 558289 558290 558291 558292 558293 558294 558295 558296 558297 [...] (965770 flits)
Measured flits: 558288 558289 558290 558291 558292 558293 558294 558295 558296 558297 [...] (11785 flits)
Class 0:
Remaining flits: 589374 589375 589376 589377 589378 589379 589380 589381 589382 589383 [...] (960982 flits)
Measured flits: 589374 589375 589376 589377 589378 589379 589380 589381 589382 589383 [...] (7458 flits)
Class 0:
Remaining flits: 624294 624295 624296 624297 624298 624299 624300 624301 624302 624303 [...] (958028 flits)
Measured flits: 624294 624295 624296 624297 624298 624299 624300 624301 624302 624303 [...] (4638 flits)
Class 0:
Remaining flits: 673272 673273 673274 673275 673276 673277 673278 673279 673280 673281 [...] (955106 flits)
Measured flits: 673272 673273 673274 673275 673276 673277 673278 673279 673280 673281 [...] (2768 flits)
Class 0:
Remaining flits: 683154 683155 683156 683157 683158 683159 683160 683161 683162 683163 [...] (953365 flits)
Measured flits: 683154 683155 683156 683157 683158 683159 683160 683161 683162 683163 [...] (1580 flits)
Class 0:
Remaining flits: 728634 728635 728636 728637 728638 728639 734184 734185 734186 734187 [...] (948686 flits)
Measured flits: 728634 728635 728636 728637 728638 728639 734184 734185 734186 734187 [...] (763 flits)
Class 0:
Remaining flits: 773082 773083 773084 773085 773086 773087 773088 773089 773090 773091 [...] (948062 flits)
Measured flits: 773082 773083 773084 773085 773086 773087 773088 773089 773090 773091 [...] (423 flits)
Class 0:
Remaining flits: 787716 787717 787718 787719 787720 787721 787722 787723 787724 787725 [...] (948923 flits)
Measured flits: 787716 787717 787718 787719 787720 787721 787722 787723 787724 787725 [...] (180 flits)
Class 0:
Remaining flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (947943 flits)
Measured flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (108 flits)
Class 0:
Remaining flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (945656 flits)
Measured flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (90 flits)
Class 0:
Remaining flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (942964 flits)
Measured flits: 816588 816589 816590 816591 816592 816593 816594 816595 816596 816597 [...] (72 flits)
Class 0:
Remaining flits: 861462 861463 861464 861465 861466 861467 861468 861469 861470 861471 [...] (940961 flits)
Measured flits: 1065204 1065205 1065206 1065207 1065208 1065209 1065210 1065211 1065212 1065213 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 917550 917551 917552 917553 917554 917555 917556 917557 917558 917559 [...] (907322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 917550 917551 917552 917553 917554 917555 917556 917557 917558 917559 [...] (877272 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933822 933823 933824 933825 933826 933827 933828 933829 933830 933831 [...] (846173 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933822 933823 933824 933825 933826 933827 933828 933829 933830 933831 [...] (815404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933822 933823 933824 933825 933826 933827 933828 933829 933830 933831 [...] (784104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 968904 968905 968906 968907 968908 968909 968910 968911 968912 968913 [...] (753112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 968904 968905 968906 968907 968908 968909 968910 968911 968912 968913 [...] (722824 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1002078 1002079 1002080 1002081 1002082 1002083 1002084 1002085 1002086 1002087 [...] (691964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1019934 1019935 1019936 1019937 1019938 1019939 1019940 1019941 1019942 1019943 [...] (660473 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1019934 1019935 1019936 1019937 1019938 1019939 1019940 1019941 1019942 1019943 [...] (628897 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1019934 1019935 1019936 1019937 1019938 1019939 1019940 1019941 1019942 1019943 [...] (596801 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1019934 1019935 1019936 1019937 1019938 1019939 1019940 1019941 1019942 1019943 [...] (566195 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1068462 1068463 1068464 1068465 1068466 1068467 1068468 1068469 1068470 1068471 [...] (534509 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1068462 1068463 1068464 1068465 1068466 1068467 1068468 1068469 1068470 1068471 [...] (503758 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (472738 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (442244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (411707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (381809 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (352492 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (322913 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174698 1174699 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 [...] (293138 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1217070 1217071 1217072 1217073 1217074 1217075 1217076 1217077 1217078 1217079 [...] (263744 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1221066 1221067 1221068 1221069 1221070 1221071 1221072 1221073 1221074 1221075 [...] (234555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1306854 1306855 1306856 1306857 1306858 1306859 1306860 1306861 1306862 1306863 [...] (205351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1306854 1306855 1306856 1306857 1306858 1306859 1306860 1306861 1306862 1306863 [...] (176767 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1306854 1306855 1306856 1306857 1306858 1306859 1306860 1306861 1306862 1306863 [...] (148502 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1334790 1334791 1334792 1334793 1334794 1334795 1334796 1334797 1334798 1334799 [...] (119977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1334790 1334791 1334792 1334793 1334794 1334795 1334796 1334797 1334798 1334799 [...] (92215 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1486854 1486855 1486856 1486857 1486858 1486859 1486860 1486861 1486862 1486863 [...] (66158 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1520129 1520130 1520131 1520132 1520133 1520134 1520135 1545696 1545697 1545698 [...] (39495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1658124 1658125 1658126 1658127 1658128 1658129 1658130 1658131 1658132 1658133 [...] (17413 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1936932 1936933 1936934 1936935 1936936 1936937 1936938 1936939 1936940 1936941 [...] (3934 flits)
Measured flits: (0 flits)
Time taken is 74807 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10154.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 32308 (1 samples)
Network latency average = 9964.19 (1 samples)
	minimum = 23 (1 samples)
	maximum = 30185 (1 samples)
Flit latency average = 19850.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 53429 (1 samples)
Fragmentation average = 71.3263 (1 samples)
	minimum = 0 (1 samples)
	maximum = 187 (1 samples)
Injected packet rate average = 0.023314 (1 samples)
	minimum = 0.0164286 (1 samples)
	maximum = 0.0297143 (1 samples)
Accepted packet rate average = 0.0102708 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0141429 (1 samples)
Injected flit rate average = 0.419568 (1 samples)
	minimum = 0.296 (1 samples)
	maximum = 0.534857 (1 samples)
Accepted flit rate average = 0.184846 (1 samples)
	minimum = 0.138143 (1 samples)
	maximum = 0.253143 (1 samples)
Injected packet size average = 17.9964 (1 samples)
Accepted packet size average = 17.9972 (1 samples)
Hops average = 5.06237 (1 samples)
Total run time 55.9886
