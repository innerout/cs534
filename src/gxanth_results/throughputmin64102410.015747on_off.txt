BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 280.868
	minimum = 23
	maximum = 966
Network latency average = 228.184
	minimum = 23
	maximum = 916
Slowest packet = 270
Flit latency average = 155.962
	minimum = 6
	maximum = 908
Slowest flit = 6228
Fragmentation average = 134.976
	minimum = 0
	maximum = 714
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.00896354
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.182984
	minimum = 0.036 (at node 174)
	maximum = 0.327 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 20.4143
Total in-flight flits = 19461 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 417.233
	minimum = 23
	maximum = 1808
Network latency average = 354.109
	minimum = 23
	maximum = 1764
Slowest packet = 210
Flit latency average = 263.734
	minimum = 6
	maximum = 1787
Slowest flit = 11867
Fragmentation average = 180.303
	minimum = 0
	maximum = 1604
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0100339
	minimum = 0.0045 (at node 153)
	maximum = 0.0155 (at node 71)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.195284
	minimum = 0.098 (at node 153)
	maximum = 0.29 (at node 71)
Injected packet length average = 17.9137
Accepted packet length average = 19.4625
Total in-flight flits = 32651 (0 measured)
latency change    = 0.326833
throughput change = 0.0629826
Class 0:
Packet latency average = 721.58
	minimum = 25
	maximum = 2739
Network latency average = 647.472
	minimum = 25
	maximum = 2661
Slowest packet = 288
Flit latency average = 533.811
	minimum = 6
	maximum = 2806
Slowest flit = 11094
Fragmentation average = 267.961
	minimum = 1
	maximum = 2158
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0119063
	minimum = 0.004 (at node 138)
	maximum = 0.021 (at node 23)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.220427
	minimum = 0.056 (at node 138)
	maximum = 0.385 (at node 99)
Injected packet length average = 18.0167
Accepted packet length average = 18.5136
Total in-flight flits = 46309 (0 measured)
latency change    = 0.421779
throughput change = 0.114066
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.39
	minimum = 27
	maximum = 1174
Network latency average = 323.461
	minimum = 23
	maximum = 968
Slowest packet = 9091
Flit latency average = 716.195
	minimum = 6
	maximum = 3802
Slowest flit = 11082
Fragmentation average = 171.867
	minimum = 0
	maximum = 741
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0122448
	minimum = 0.004 (at node 51)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.222422
	minimum = 0.085 (at node 166)
	maximum = 0.393 (at node 103)
Injected packet length average = 17.9896
Accepted packet length average = 18.1646
Total in-flight flits = 55516 (33669 measured)
latency change    = 0.857875
throughput change = 0.0089685
Class 0:
Packet latency average = 605.307
	minimum = 27
	maximum = 2151
Network latency average = 540.037
	minimum = 23
	maximum = 1989
Slowest packet = 9091
Flit latency average = 818.275
	minimum = 6
	maximum = 4894
Slowest flit = 4365
Fragmentation average = 218.941
	minimum = 0
	maximum = 1368
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0122135
	minimum = 0.0055 (at node 79)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.222693
	minimum = 0.1115 (at node 23)
	maximum = 0.3525 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.2333
Total in-flight flits = 64925 (52772 measured)
latency change    = 0.358358
throughput change = 0.00121618
Class 0:
Packet latency average = 788.473
	minimum = 27
	maximum = 3060
Network latency average = 716.717
	minimum = 23
	maximum = 2792
Slowest packet = 9091
Flit latency average = 900.67
	minimum = 6
	maximum = 5472
Slowest flit = 28605
Fragmentation average = 243.212
	minimum = 0
	maximum = 1917
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0123021
	minimum = 0.007 (at node 79)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.223538
	minimum = 0.138 (at node 79)
	maximum = 0.325333 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 18.1708
Total in-flight flits = 73377 (65672 measured)
latency change    = 0.232305
throughput change = 0.00378229
Draining remaining packets ...
Class 0:
Remaining flits: 5725 5726 5727 5728 5729 5730 5731 5732 5733 5734 [...] (36516 flits)
Measured flits: 163800 163801 163802 163803 163804 163805 163806 163807 163808 163809 [...] (31834 flits)
Class 0:
Remaining flits: 5741 19077 19078 19079 35672 35673 35674 35675 43236 43237 [...] (9877 flits)
Measured flits: 164211 164212 164213 165060 165061 165062 165063 165064 165065 165066 [...] (8673 flits)
Time taken is 8840 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1464.55 (1 samples)
	minimum = 27 (1 samples)
	maximum = 5869 (1 samples)
Network latency average = 1382.88 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5596 (1 samples)
Flit latency average = 1449.85 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7917 (1 samples)
Fragmentation average = 265.983 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4695 (1 samples)
Injected packet rate average = 0.0150295 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0123021 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.270549 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.581667 (1 samples)
Accepted flit rate average = 0.223538 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 0.325333 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 18.1708 (1 samples)
Hops average = 5.05776 (1 samples)
Total run time 9.72482
