BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 265.182
	minimum = 22
	maximum = 957
Network latency average = 186.627
	minimum = 22
	maximum = 706
Slowest packet = 72
Flit latency average = 156.164
	minimum = 5
	maximum = 689
Slowest flit = 20123
Fragmentation average = 36.2427
	minimum = 0
	maximum = 277
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.0126406
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.235604
	minimum = 0.108 (at node 93)
	maximum = 0.406 (at node 44)
Injected packet length average = 17.8591
Accepted packet length average = 18.6386
Total in-flight flits = 22102 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 409.192
	minimum = 22
	maximum = 1692
Network latency average = 310.293
	minimum = 22
	maximum = 1316
Slowest packet = 72
Flit latency average = 276.775
	minimum = 5
	maximum = 1393
Slowest flit = 38289
Fragmentation average = 43.7817
	minimum = 0
	maximum = 401
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0135964
	minimum = 0.0075 (at node 116)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.249255
	minimum = 0.135 (at node 116)
	maximum = 0.3535 (at node 166)
Injected packet length average = 17.9339
Accepted packet length average = 18.3325
Total in-flight flits = 38728 (0 measured)
latency change    = 0.351937
throughput change = 0.0547673
Class 0:
Packet latency average = 793.473
	minimum = 22
	maximum = 2476
Network latency average = 662.499
	minimum = 22
	maximum = 1941
Slowest packet = 3545
Flit latency average = 629.498
	minimum = 5
	maximum = 1963
Slowest flit = 69434
Fragmentation average = 53.3696
	minimum = 0
	maximum = 383
Injected packet rate average = 0.0207448
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0148229
	minimum = 0.005 (at node 36)
	maximum = 0.025 (at node 92)
Injected flit rate average = 0.372271
	minimum = 0 (at node 7)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.268214
	minimum = 0.086 (at node 61)
	maximum = 0.458 (at node 92)
Injected packet length average = 17.9453
Accepted packet length average = 18.0945
Total in-flight flits = 58925 (0 measured)
latency change    = 0.484303
throughput change = 0.0706837
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 411.684
	minimum = 25
	maximum = 1268
Network latency average = 310.279
	minimum = 22
	maximum = 989
Slowest packet = 11456
Flit latency average = 883.613
	minimum = 5
	maximum = 2281
Slowest flit = 109835
Fragmentation average = 28.9837
	minimum = 0
	maximum = 287
Injected packet rate average = 0.020276
	minimum = 0 (at node 77)
	maximum = 0.055 (at node 34)
Accepted packet rate average = 0.0152292
	minimum = 0.006 (at node 79)
	maximum = 0.025 (at node 66)
Injected flit rate average = 0.36649
	minimum = 0 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.273708
	minimum = 0.114 (at node 79)
	maximum = 0.469 (at node 66)
Injected packet length average = 18.075
Accepted packet length average = 17.9726
Total in-flight flits = 76447 (57367 measured)
latency change    = 0.927381
throughput change = 0.0200754
Class 0:
Packet latency average = 845.585
	minimum = 23
	maximum = 2594
Network latency average = 738.014
	minimum = 22
	maximum = 1955
Slowest packet = 11456
Flit latency average = 1004.01
	minimum = 5
	maximum = 2791
Slowest flit = 149311
Fragmentation average = 38.4359
	minimum = 0
	maximum = 287
Injected packet rate average = 0.0198438
	minimum = 0.001 (at node 191)
	maximum = 0.051 (at node 11)
Accepted packet rate average = 0.0152422
	minimum = 0.0085 (at node 139)
	maximum = 0.023 (at node 136)
Injected flit rate average = 0.357513
	minimum = 0.018 (at node 191)
	maximum = 0.918 (at node 11)
Accepted flit rate average= 0.273779
	minimum = 0.1595 (at node 139)
	maximum = 0.412 (at node 136)
Injected packet length average = 18.0164
Accepted packet length average = 17.9619
Total in-flight flits = 90954 (87240 measured)
latency change    = 0.513136
throughput change = 0.000256822
Class 0:
Packet latency average = 1150.11
	minimum = 23
	maximum = 3582
Network latency average = 1038.2
	minimum = 22
	maximum = 2931
Slowest packet = 11456
Flit latency average = 1130.38
	minimum = 5
	maximum = 3363
Slowest flit = 175265
Fragmentation average = 45.7927
	minimum = 0
	maximum = 409
Injected packet rate average = 0.0198142
	minimum = 0.00166667 (at node 131)
	maximum = 0.0473333 (at node 55)
Accepted packet rate average = 0.0152066
	minimum = 0.00966667 (at node 141)
	maximum = 0.021 (at node 90)
Injected flit rate average = 0.357101
	minimum = 0.03 (at node 131)
	maximum = 0.847667 (at node 55)
Accepted flit rate average= 0.273293
	minimum = 0.177333 (at node 141)
	maximum = 0.384333 (at node 90)
Injected packet length average = 18.0224
Accepted packet length average = 17.972
Total in-flight flits = 106960 (106575 measured)
latency change    = 0.264779
throughput change = 0.00177554
Draining remaining packets ...
Class 0:
Remaining flits: 184994 184995 184996 184997 184998 184999 185000 185001 185002 185003 [...] (60242 flits)
Measured flits: 224748 224749 224750 224751 224752 224753 224754 224755 224756 224757 [...] (60232 flits)
Class 0:
Remaining flits: 251080 251081 253980 253981 253982 253983 253984 253985 253986 253987 [...] (21030 flits)
Measured flits: 251080 251081 253980 253981 253982 253983 253984 253985 253986 253987 [...] (21030 flits)
Class 0:
Remaining flits: 276390 276391 276392 276393 276394 276395 276396 276397 276398 276399 [...] (4946 flits)
Measured flits: 276390 276391 276392 276393 276394 276395 276396 276397 276398 276399 [...] (4946 flits)
Class 0:
Remaining flits: 307422 307423 307424 307425 307426 307427 307428 307429 307430 307431 [...] (455 flits)
Measured flits: 307422 307423 307424 307425 307426 307427 307428 307429 307430 307431 [...] (455 flits)
Time taken is 10354 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1807.59 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6211 (1 samples)
Network latency average = 1685.08 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5714 (1 samples)
Flit latency average = 1577.53 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5697 (1 samples)
Fragmentation average = 49.1641 (1 samples)
	minimum = 0 (1 samples)
	maximum = 474 (1 samples)
Injected packet rate average = 0.0198142 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.0473333 (1 samples)
Accepted packet rate average = 0.0152066 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.357101 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.847667 (1 samples)
Accepted flit rate average = 0.273293 (1 samples)
	minimum = 0.177333 (1 samples)
	maximum = 0.384333 (1 samples)
Injected packet size average = 18.0224 (1 samples)
Accepted packet size average = 17.972 (1 samples)
Hops average = 5.08209 (1 samples)
Total run time 6.89336
