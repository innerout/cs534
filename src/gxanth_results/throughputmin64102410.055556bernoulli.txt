BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.894
	minimum = 23
	maximum = 943
Network latency average = 307.628
	minimum = 23
	maximum = 928
Slowest packet = 178
Flit latency average = 238.945
	minimum = 6
	maximum = 954
Slowest flit = 1581
Fragmentation average = 184.717
	minimum = 0
	maximum = 838
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0119427
	minimum = 0.004 (at node 45)
	maximum = 0.02 (at node 63)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.254943
	minimum = 0.132 (at node 61)
	maximum = 0.4 (at node 136)
Injected packet length average = 17.8419
Accepted packet length average = 21.3471
Total in-flight flits = 124319 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 600.308
	minimum = 23
	maximum = 1955
Network latency average = 548.838
	minimum = 23
	maximum = 1922
Slowest packet = 578
Flit latency average = 468.658
	minimum = 6
	maximum = 1919
Slowest flit = 6977
Fragmentation average = 246.256
	minimum = 0
	maximum = 1546
Injected packet rate average = 0.0517865
	minimum = 0.0425 (at node 37)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0133646
	minimum = 0.008 (at node 8)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.928016
	minimum = 0.76 (at node 37)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.266674
	minimum = 0.1635 (at node 96)
	maximum = 0.402 (at node 152)
Injected packet length average = 17.92
Accepted packet length average = 19.9538
Total in-flight flits = 255545 (0 measured)
latency change    = 0.435466
throughput change = 0.0439929
Class 0:
Packet latency average = 1301.02
	minimum = 27
	maximum = 2908
Network latency average = 1208.09
	minimum = 23
	maximum = 2850
Slowest packet = 908
Flit latency average = 1155.66
	minimum = 6
	maximum = 2929
Slowest flit = 7180
Fragmentation average = 331.834
	minimum = 0
	maximum = 2205
Injected packet rate average = 0.0535417
	minimum = 0.039 (at node 140)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.015776
	minimum = 0.007 (at node 143)
	maximum = 0.03 (at node 131)
Injected flit rate average = 0.964151
	minimum = 0.707 (at node 140)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287495
	minimum = 0.145 (at node 64)
	maximum = 0.557 (at node 131)
Injected packet length average = 18.0075
Accepted packet length average = 18.2235
Total in-flight flits = 385386 (0 measured)
latency change    = 0.538585
throughput change = 0.0724198
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 229.366
	minimum = 25
	maximum = 899
Network latency average = 55.4542
	minimum = 23
	maximum = 477
Slowest packet = 30203
Flit latency average = 1702.46
	minimum = 6
	maximum = 3822
Slowest flit = 25423
Fragmentation average = 17.7599
	minimum = 0
	maximum = 79
Injected packet rate average = 0.0539115
	minimum = 0.039 (at node 162)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.015776
	minimum = 0.005 (at node 186)
	maximum = 0.025 (at node 3)
Injected flit rate average = 0.969901
	minimum = 0.691 (at node 162)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.283807
	minimum = 0.084 (at node 186)
	maximum = 0.443 (at node 106)
Injected packet length average = 17.9906
Accepted packet length average = 17.9898
Total in-flight flits = 517213 (171496 measured)
latency change    = 4.67222
throughput change = 0.012993
Class 0:
Packet latency average = 249.304
	minimum = 25
	maximum = 899
Network latency average = 58.3237
	minimum = 23
	maximum = 620
Slowest packet = 30203
Flit latency average = 1998.53
	minimum = 6
	maximum = 4851
Slowest flit = 12383
Fragmentation average = 18.7663
	minimum = 0
	maximum = 80
Injected packet rate average = 0.0541901
	minimum = 0.045 (at node 73)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.015763
	minimum = 0.0075 (at node 186)
	maximum = 0.025 (at node 119)
Injected flit rate average = 0.975346
	minimum = 0.808 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.284628
	minimum = 0.158 (at node 7)
	maximum = 0.4555 (at node 119)
Injected packet length average = 17.9986
Accepted packet length average = 18.0567
Total in-flight flits = 650651 (344455 measured)
latency change    = 0.0799724
throughput change = 0.00288206
Class 0:
Packet latency average = 270.866
	minimum = 25
	maximum = 2913
Network latency average = 64.5896
	minimum = 23
	maximum = 2830
Slowest packet = 30167
Flit latency average = 2289.72
	minimum = 6
	maximum = 5836
Slowest flit = 22327
Fragmentation average = 19.1107
	minimum = 0
	maximum = 365
Injected packet rate average = 0.0543368
	minimum = 0.045 (at node 73)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0158125
	minimum = 0.01 (at node 7)
	maximum = 0.0226667 (at node 22)
Injected flit rate average = 0.977953
	minimum = 0.811 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.285276
	minimum = 0.174333 (at node 7)
	maximum = 0.433667 (at node 119)
Injected packet length average = 17.998
Accepted packet length average = 18.0412
Total in-flight flits = 784431 (517514 measured)
latency change    = 0.0796047
throughput change = 0.00227302
Draining remaining packets ...
Class 0:
Remaining flits: 15624 15625 15626 15627 15628 15629 15630 15631 15632 15633 [...] (744596 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (515892 flits)
Class 0:
Remaining flits: 15624 15625 15626 15627 15628 15629 15630 15631 15632 15633 [...] (706090 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (514069 flits)
Class 0:
Remaining flits: 22329 22330 22331 22332 22333 22334 22335 22336 22337 30266 [...] (668031 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (509397 flits)
Class 0:
Remaining flits: 22329 22330 22331 22332 22333 22334 22335 22336 22337 30275 [...] (630235 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (500398 flits)
Class 0:
Remaining flits: 30924 30925 30926 30927 30928 30929 30930 30931 30932 30933 [...] (593800 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (487662 flits)
Class 0:
Remaining flits: 36738 36739 36740 36741 36742 36743 36744 36745 36746 36747 [...] (557933 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (471920 flits)
Class 0:
Remaining flits: 36738 36739 36740 36741 36742 36743 36744 36745 36746 36747 [...] (522600 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (452685 flits)
Class 0:
Remaining flits: 36738 36739 36740 36741 36742 36743 36744 36745 36746 36747 [...] (487446 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (430394 flits)
Class 0:
Remaining flits: 36751 36752 36753 36754 36755 54846 54847 54848 54849 54850 [...] (453339 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (406184 flits)
Class 0:
Remaining flits: 54846 54847 54848 54849 54850 54851 54852 54853 54854 54855 [...] (419026 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (379471 flits)
Class 0:
Remaining flits: 54846 54847 54848 54849 54850 54851 54852 54853 54854 54855 [...] (384471 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (351674 flits)
Class 0:
Remaining flits: 54860 54861 54862 54863 55746 55747 55748 55749 55750 55751 [...] (350648 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (323083 flits)
Class 0:
Remaining flits: 55746 55747 55748 55749 55750 55751 55752 55753 55754 55755 [...] (316817 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (293420 flits)
Class 0:
Remaining flits: 55746 55747 55748 55749 55750 55751 55752 55753 55754 55755 [...] (283479 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (263431 flits)
Class 0:
Remaining flits: 55748 55749 55750 55751 55752 55753 55754 55755 55756 55757 [...] (250634 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (233908 flits)
Class 0:
Remaining flits: 84132 84133 84134 84135 84136 84137 84138 84139 84140 84141 [...] (218763 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (204506 flits)
Class 0:
Remaining flits: 84132 84133 84134 84135 84136 84137 84138 84139 84140 84141 [...] (187918 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (176554 flits)
Class 0:
Remaining flits: 84132 84133 84134 84135 84136 84137 84138 84139 84140 84141 [...] (157270 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (148238 flits)
Class 0:
Remaining flits: 84132 84133 84134 84135 84136 84137 84138 84139 84140 84141 [...] (125489 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (118412 flits)
Class 0:
Remaining flits: 108756 108757 108758 108759 108760 108761 108762 108763 108764 108765 [...] (94462 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (89111 flits)
Class 0:
Remaining flits: 147423 147424 147425 147426 147427 147428 147429 147430 147431 147432 [...] (63709 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (60067 flits)
Class 0:
Remaining flits: 151272 151273 151274 151275 151276 151277 151278 151279 151280 151281 [...] (35373 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (33458 flits)
Class 0:
Remaining flits: 151272 151273 151274 151275 151276 151277 151278 151279 151280 151281 [...] (15558 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (14497 flits)
Class 0:
Remaining flits: 219688 219689 314010 314011 314012 314013 314014 314015 314016 314017 [...] (2869 flits)
Measured flits: 546480 546481 546482 546483 546484 546485 546486 546487 546488 546489 [...] (2736 flits)
Time taken is 30690 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14560.1 (1 samples)
	minimum = 25 (1 samples)
	maximum = 27436 (1 samples)
Network latency average = 14354.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 27347 (1 samples)
Flit latency average = 11448.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 28741 (1 samples)
Fragmentation average = 238.178 (1 samples)
	minimum = 0 (1 samples)
	maximum = 12947 (1 samples)
Injected packet rate average = 0.0543368 (1 samples)
	minimum = 0.045 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0158125 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.977953 (1 samples)
	minimum = 0.811 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.285276 (1 samples)
	minimum = 0.174333 (1 samples)
	maximum = 0.433667 (1 samples)
Injected packet size average = 17.998 (1 samples)
Accepted packet size average = 18.0412 (1 samples)
Hops average = 5.07163 (1 samples)
Total run time 45.4949
