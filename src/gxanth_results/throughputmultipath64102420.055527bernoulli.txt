BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 331.568
	minimum = 22
	maximum = 920
Network latency average = 301.406
	minimum = 22
	maximum = 866
Slowest packet = 656
Flit latency average = 270.861
	minimum = 5
	maximum = 854
Slowest flit = 18718
Fragmentation average = 86.0539
	minimum = 0
	maximum = 714
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0147917
	minimum = 0.004 (at node 174)
	maximum = 0.026 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.296078
	minimum = 0.128 (at node 174)
	maximum = 0.481 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 20.0165
Total in-flight flits = 116259 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 615.071
	minimum = 22
	maximum = 1764
Network latency average = 564.432
	minimum = 22
	maximum = 1715
Slowest packet = 656
Flit latency average = 524.19
	minimum = 5
	maximum = 1721
Slowest flit = 40429
Fragmentation average = 141.324
	minimum = 0
	maximum = 868
Injected packet rate average = 0.051625
	minimum = 0.04 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0160807
	minimum = 0.01 (at node 30)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.925266
	minimum = 0.713 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.309823
	minimum = 0.189 (at node 116)
	maximum = 0.433 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 19.2667
Total in-flight flits = 237860 (0 measured)
latency change    = 0.460927
throughput change = 0.0443634
Class 0:
Packet latency average = 1414.11
	minimum = 22
	maximum = 2615
Network latency average = 1325.89
	minimum = 22
	maximum = 2540
Slowest packet = 2414
Flit latency average = 1282.15
	minimum = 5
	maximum = 2616
Slowest flit = 60467
Fragmentation average = 249.06
	minimum = 0
	maximum = 933
Injected packet rate average = 0.0538177
	minimum = 0.043 (at node 63)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0173802
	minimum = 0.005 (at node 88)
	maximum = 0.029 (at node 65)
Injected flit rate average = 0.968359
	minimum = 0.78 (at node 63)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.320479
	minimum = 0.159 (at node 133)
	maximum = 0.511 (at node 65)
Injected packet length average = 17.9933
Accepted packet length average = 18.4393
Total in-flight flits = 362322 (0 measured)
latency change    = 0.565046
throughput change = 0.033251
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 223.85
	minimum = 24
	maximum = 724
Network latency average = 39.2078
	minimum = 22
	maximum = 496
Slowest packet = 30185
Flit latency average = 1804.46
	minimum = 5
	maximum = 3466
Slowest flit = 85797
Fragmentation average = 6.65892
	minimum = 0
	maximum = 40
Injected packet rate average = 0.0543333
	minimum = 0.045 (at node 18)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0174531
	minimum = 0.007 (at node 84)
	maximum = 0.029 (at node 37)
Injected flit rate average = 0.978068
	minimum = 0.808 (at node 18)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.324266
	minimum = 0.143 (at node 84)
	maximum = 0.491 (at node 37)
Injected packet length average = 18.0012
Accepted packet length average = 18.5792
Total in-flight flits = 487839 (172924 measured)
latency change    = 5.31722
throughput change = 0.011677
Class 0:
Packet latency average = 237.415
	minimum = 24
	maximum = 846
Network latency average = 42.1084
	minimum = 22
	maximum = 514
Slowest packet = 30185
Flit latency average = 2060.75
	minimum = 5
	maximum = 4422
Slowest flit = 92896
Fragmentation average = 6.4967
	minimum = 0
	maximum = 40
Injected packet rate average = 0.054349
	minimum = 0.0465 (at node 18)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0175703
	minimum = 0.0085 (at node 42)
	maximum = 0.0255 (at node 72)
Injected flit rate average = 0.978453
	minimum = 0.8385 (at node 18)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.32351
	minimum = 0.1575 (at node 42)
	maximum = 0.4745 (at node 72)
Injected packet length average = 18.0032
Accepted packet length average = 18.4123
Total in-flight flits = 613754 (345440 measured)
latency change    = 0.0571387
throughput change = 0.00233442
Class 0:
Packet latency average = 249.375
	minimum = 24
	maximum = 905
Network latency average = 43.8355
	minimum = 22
	maximum = 636
Slowest packet = 30185
Flit latency average = 2324.45
	minimum = 5
	maximum = 5365
Slowest flit = 104441
Fragmentation average = 6.61443
	minimum = 0
	maximum = 40
Injected packet rate average = 0.0542639
	minimum = 0.047 (at node 162)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0178142
	minimum = 0.0106667 (at node 42)
	maximum = 0.026 (at node 72)
Injected flit rate average = 0.976606
	minimum = 0.846333 (at node 162)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.325898
	minimum = 0.215333 (at node 42)
	maximum = 0.473333 (at node 72)
Injected packet length average = 17.9973
Accepted packet length average = 18.2942
Total in-flight flits = 737213 (516041 measured)
latency change    = 0.0479594
throughput change = 0.00732486
Draining remaining packets ...
Class 0:
Remaining flits: 140198 140199 140200 140201 141012 141013 141014 141015 141016 141017 [...] (689273 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (515395 flits)
Class 0:
Remaining flits: 146743 146744 146745 146746 146747 146748 146749 146750 146751 146752 [...] (641917 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (514582 flits)
Class 0:
Remaining flits: 196902 196903 196904 196905 196906 196907 196908 196909 196910 196911 [...] (594500 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (510500 flits)
Class 0:
Remaining flits: 212494 212495 212496 212497 212498 212499 212500 212501 212502 212503 [...] (547161 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (499480 flits)
Class 0:
Remaining flits: 262105 262106 262107 262108 262109 262110 262111 262112 262113 262114 [...] (499876 flits)
Measured flits: 542835 542836 542837 542838 542839 542840 542841 542842 542843 542844 [...] (477422 flits)
Class 0:
Remaining flits: 409694 409695 409696 409697 409986 409987 409988 409989 409990 409991 [...] (452881 flits)
Measured flits: 542844 542845 542846 542847 542848 542849 542850 542851 542852 542853 [...] (443780 flits)
Class 0:
Remaining flits: 460314 460315 460316 460317 460318 460319 460320 460321 460322 460323 [...] (405816 flits)
Measured flits: 542898 542899 542900 542901 542902 542903 542904 542905 542906 542907 [...] (402698 flits)
Class 0:
Remaining flits: 472013 492314 492315 492316 492317 494547 494548 494549 497498 497499 [...] (358712 flits)
Measured flits: 544153 544154 544155 544156 544157 544518 544519 544520 544521 544522 [...] (358017 flits)
Class 0:
Remaining flits: 524329 524330 524331 524332 524333 524334 524335 524336 524337 524338 [...] (311716 flits)
Measured flits: 547360 547361 550749 550750 550751 550752 550753 550754 550755 550756 [...] (311649 flits)
Class 0:
Remaining flits: 528971 528972 528973 528974 528975 528976 528977 528978 528979 528980 [...] (264588 flits)
Measured flits: 567285 567286 567287 572087 572088 572089 572090 572091 572092 572093 [...] (264575 flits)
Class 0:
Remaining flits: 576843 576844 576845 592675 592676 592677 592678 592679 592680 592681 [...] (217381 flits)
Measured flits: 576843 576844 576845 592675 592676 592677 592678 592679 592680 592681 [...] (217381 flits)
Class 0:
Remaining flits: 603450 603451 603452 603453 603454 603455 603456 603457 603458 603459 [...] (170108 flits)
Measured flits: 603450 603451 603452 603453 603454 603455 603456 603457 603458 603459 [...] (170108 flits)
Class 0:
Remaining flits: 669498 669499 669500 669501 669502 669503 669504 669505 669506 669507 [...] (122820 flits)
Measured flits: 669498 669499 669500 669501 669502 669503 669504 669505 669506 669507 [...] (122820 flits)
Class 0:
Remaining flits: 730306 730307 730308 730309 730310 730311 730312 730313 747342 747343 [...] (74994 flits)
Measured flits: 730306 730307 730308 730309 730310 730311 730312 730313 747342 747343 [...] (74994 flits)
Class 0:
Remaining flits: 760815 760816 760817 760818 760819 760820 760821 760822 760823 808110 [...] (27937 flits)
Measured flits: 760815 760816 760817 760818 760819 760820 760821 760822 760823 808110 [...] (27937 flits)
Class 0:
Remaining flits: 882112 882113 882114 882115 882116 882117 882118 882119 882120 882121 [...] (1728 flits)
Measured flits: 882112 882113 882114 882115 882116 882117 882118 882119 882120 882121 [...] (1728 flits)
Time taken is 22851 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10976.7 (1 samples)
	minimum = 24 (1 samples)
	maximum = 18061 (1 samples)
Network latency average = 10775.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17379 (1 samples)
Flit latency average = 8411.26 (1 samples)
	minimum = 5 (1 samples)
	maximum = 17362 (1 samples)
Fragmentation average = 376.671 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1046 (1 samples)
Injected packet rate average = 0.0542639 (1 samples)
	minimum = 0.047 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0178142 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.026 (1 samples)
Injected flit rate average = 0.976606 (1 samples)
	minimum = 0.846333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.325898 (1 samples)
	minimum = 0.215333 (1 samples)
	maximum = 0.473333 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 18.2942 (1 samples)
Hops average = 5.06434 (1 samples)
Total run time 31.5301
