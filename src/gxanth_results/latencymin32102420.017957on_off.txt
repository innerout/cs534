BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.046
	minimum = 24
	maximum = 880
Network latency average = 163.819
	minimum = 22
	maximum = 727
Slowest packet = 21
Flit latency average = 136.43
	minimum = 5
	maximum = 747
Slowest flit = 14990
Fragmentation average = 28.6859
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0122552
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.227964
	minimum = 0.108 (at node 41)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6014
Total in-flight flits = 18241 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 357.986
	minimum = 22
	maximum = 1822
Network latency average = 276.668
	minimum = 22
	maximum = 1439
Slowest packet = 21
Flit latency average = 245.593
	minimum = 5
	maximum = 1422
Slowest flit = 26189
Fragmentation average = 35.585
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0132214
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241727
	minimum = 0.153 (at node 116)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.283
Total in-flight flits = 26769 (0 measured)
latency change    = 0.360182
throughput change = 0.0569363
Class 0:
Packet latency average = 628.356
	minimum = 22
	maximum = 2515
Network latency average = 526.822
	minimum = 22
	maximum = 2067
Slowest packet = 2278
Flit latency average = 492.131
	minimum = 5
	maximum = 2050
Slowest flit = 52991
Fragmentation average = 42.6003
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145156
	minimum = 0.004 (at node 118)
	maximum = 0.025 (at node 57)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.26126
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9986
Total in-flight flits = 38977 (0 measured)
latency change    = 0.430282
throughput change = 0.0747678
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 409.814
	minimum = 25
	maximum = 1432
Network latency average = 315.59
	minimum = 22
	maximum = 989
Slowest packet = 10126
Flit latency average = 639.03
	minimum = 5
	maximum = 2439
Slowest flit = 81017
Fragmentation average = 27.9478
	minimum = 0
	maximum = 214
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0147917
	minimum = 0.007 (at node 57)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.26749
	minimum = 0.126 (at node 57)
	maximum = 0.468 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 18.0838
Total in-flight flits = 49557 (41434 measured)
latency change    = 0.533272
throughput change = 0.0232875
Class 0:
Packet latency average = 672.4
	minimum = 22
	maximum = 2125
Network latency average = 574.879
	minimum = 22
	maximum = 1927
Slowest packet = 10126
Flit latency average = 721.397
	minimum = 5
	maximum = 2478
Slowest flit = 150155
Fragmentation average = 38.7303
	minimum = 0
	maximum = 361
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0147917
	minimum = 0.0095 (at node 5)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.267216
	minimum = 0.171 (at node 36)
	maximum = 0.4145 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 18.0653
Total in-flight flits = 56012 (54958 measured)
latency change    = 0.39052
throughput change = 0.00102328
Class 0:
Packet latency average = 837.693
	minimum = 22
	maximum = 3387
Network latency average = 740.126
	minimum = 22
	maximum = 2976
Slowest packet = 10126
Flit latency average = 796.824
	minimum = 5
	maximum = 3162
Slowest flit = 162989
Fragmentation average = 41.2257
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0148351
	minimum = 0.0103333 (at node 135)
	maximum = 0.021 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267569
	minimum = 0.190667 (at node 144)
	maximum = 0.380333 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.0363
Total in-flight flits = 65991 (65990 measured)
latency change    = 0.197319
throughput change = 0.0013204
Class 0:
Packet latency average = 963.505
	minimum = 22
	maximum = 3795
Network latency average = 864.46
	minimum = 22
	maximum = 3570
Slowest packet = 10126
Flit latency average = 872.777
	minimum = 5
	maximum = 3578
Slowest flit = 207256
Fragmentation average = 44.3629
	minimum = 0
	maximum = 373
Injected packet rate average = 0.017625
	minimum = 0.0025 (at node 120)
	maximum = 0.038 (at node 17)
Accepted packet rate average = 0.014832
	minimum = 0.011 (at node 144)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.317267
	minimum = 0.045 (at node 120)
	maximum = 0.683 (at node 17)
Accepted flit rate average= 0.267143
	minimum = 0.1995 (at node 144)
	maximum = 0.3495 (at node 118)
Injected packet length average = 18.001
Accepted packet length average = 18.0112
Total in-flight flits = 77459 (77459 measured)
latency change    = 0.130578
throughput change = 0.00159546
Class 0:
Packet latency average = 1075.39
	minimum = 22
	maximum = 4056
Network latency average = 975.62
	minimum = 22
	maximum = 3913
Slowest packet = 10126
Flit latency average = 957.446
	minimum = 5
	maximum = 3926
Slowest flit = 246614
Fragmentation average = 45.0576
	minimum = 0
	maximum = 381
Injected packet rate average = 0.0175719
	minimum = 0.0032 (at node 89)
	maximum = 0.0388 (at node 23)
Accepted packet rate average = 0.0148406
	minimum = 0.0112 (at node 86)
	maximum = 0.0192 (at node 118)
Injected flit rate average = 0.31629
	minimum = 0.0576 (at node 89)
	maximum = 0.6994 (at node 23)
Accepted flit rate average= 0.267321
	minimum = 0.2016 (at node 86)
	maximum = 0.3456 (at node 118)
Injected packet length average = 17.9998
Accepted packet length average = 18.0128
Total in-flight flits = 85991 (85991 measured)
latency change    = 0.104044
throughput change = 0.000664386
Class 0:
Packet latency average = 1177.06
	minimum = 22
	maximum = 4166
Network latency average = 1075.54
	minimum = 22
	maximum = 4056
Slowest packet = 10126
Flit latency average = 1040.82
	minimum = 5
	maximum = 4039
Slowest flit = 246617
Fragmentation average = 45.7207
	minimum = 0
	maximum = 381
Injected packet rate average = 0.0177665
	minimum = 0.00516667 (at node 89)
	maximum = 0.0361667 (at node 23)
Accepted packet rate average = 0.0148767
	minimum = 0.0116667 (at node 80)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.319868
	minimum = 0.093 (at node 89)
	maximum = 0.651 (at node 23)
Accepted flit rate average= 0.268035
	minimum = 0.211 (at node 80)
	maximum = 0.344333 (at node 118)
Injected packet length average = 18.004
Accepted packet length average = 18.017
Total in-flight flits = 98607 (98607 measured)
latency change    = 0.0863713
throughput change = 0.00266342
Class 0:
Packet latency average = 1258.85
	minimum = 22
	maximum = 4192
Network latency average = 1158.64
	minimum = 22
	maximum = 4056
Slowest packet = 10126
Flit latency average = 1113.92
	minimum = 5
	maximum = 4039
Slowest flit = 246617
Fragmentation average = 46.0241
	minimum = 0
	maximum = 381
Injected packet rate average = 0.017744
	minimum = 0.00557143 (at node 89)
	maximum = 0.0318571 (at node 23)
Accepted packet rate average = 0.014904
	minimum = 0.012 (at node 64)
	maximum = 0.0187143 (at node 118)
Injected flit rate average = 0.319408
	minimum = 0.100286 (at node 89)
	maximum = 0.575429 (at node 23)
Accepted flit rate average= 0.268499
	minimum = 0.217571 (at node 64)
	maximum = 0.336857 (at node 118)
Injected packet length average = 18.0009
Accepted packet length average = 18.0152
Total in-flight flits = 107379 (107379 measured)
latency change    = 0.0649717
throughput change = 0.00172735
Draining all recorded packets ...
Class 0:
Remaining flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (120225 flits)
Measured flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (68151 flits)
Class 0:
Remaining flits: 476997 476998 476999 478263 478264 478265 478266 478267 478268 478269 [...] (129447 flits)
Measured flits: 476997 476998 476999 478263 478264 478265 478266 478267 478268 478269 [...] (31626 flits)
Class 0:
Remaining flits: 496356 496357 496358 496359 496360 496361 496362 496363 496364 496365 [...] (138395 flits)
Measured flits: 496356 496357 496358 496359 496360 496361 496362 496363 496364 496365 [...] (10566 flits)
Class 0:
Remaining flits: 546768 546769 546770 546771 546772 546773 546774 546775 546776 546777 [...] (149564 flits)
Measured flits: 546768 546769 546770 546771 546772 546773 546774 546775 546776 546777 [...] (2063 flits)
Class 0:
Remaining flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (160800 flits)
Measured flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (116 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 668592 668593 668594 668595 668596 668597 668598 668599 668600 668601 [...] (116843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 725180 725181 725182 725183 725303 725304 725305 725306 725307 725308 [...] (71903 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 767793 767794 767795 767796 767797 767798 767799 767800 767801 767802 [...] (35234 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 799578 799579 799580 799581 799582 799583 799584 799585 799586 799587 [...] (13648 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 872107 872108 872109 872110 872111 872112 872113 872114 872115 872116 [...] (4337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 920030 920031 920032 920033 922030 922031 923222 923223 923224 923225 [...] (760 flits)
Measured flits: (0 flits)
Time taken is 21571 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1617.26 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5636 (1 samples)
Network latency average = 1515.26 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5445 (1 samples)
Flit latency average = 1940.44 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6381 (1 samples)
Fragmentation average = 50.3556 (1 samples)
	minimum = 0 (1 samples)
	maximum = 381 (1 samples)
Injected packet rate average = 0.017744 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0318571 (1 samples)
Accepted packet rate average = 0.014904 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.319408 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.575429 (1 samples)
Accepted flit rate average = 0.268499 (1 samples)
	minimum = 0.217571 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.0152 (1 samples)
Hops average = 5.0647 (1 samples)
Total run time 14.8487
