BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.04
	minimum = 22
	maximum = 920
Network latency average = 296.167
	minimum = 22
	maximum = 874
Slowest packet = 820
Flit latency average = 264.938
	minimum = 5
	maximum = 860
Slowest flit = 17673
Fragmentation average = 86.7665
	minimum = 0
	maximum = 707
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.015276
	minimum = 0.006 (at node 108)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.304339
	minimum = 0.167 (at node 164)
	maximum = 0.465 (at node 48)
Injected packet length average = 17.8419
Accepted packet length average = 19.9226
Total in-flight flits = 114835 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.233
	minimum = 22
	maximum = 1776
Network latency average = 566.479
	minimum = 22
	maximum = 1686
Slowest packet = 981
Flit latency average = 524.029
	minimum = 5
	maximum = 1732
Slowest flit = 38986
Fragmentation average = 142.257
	minimum = 0
	maximum = 852
Injected packet rate average = 0.0517865
	minimum = 0.0425 (at node 37)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0163047
	minimum = 0.009 (at node 96)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.928016
	minimum = 0.76 (at node 37)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.313544
	minimum = 0.1725 (at node 96)
	maximum = 0.4715 (at node 152)
Injected packet length average = 17.92
Accepted packet length average = 19.2303
Total in-flight flits = 237547 (0 measured)
latency change    = 0.469192
throughput change = 0.0293602
Class 0:
Packet latency average = 1381.23
	minimum = 24
	maximum = 2616
Network latency average = 1291.24
	minimum = 23
	maximum = 2511
Slowest packet = 2474
Flit latency average = 1249.69
	minimum = 5
	maximum = 2582
Slowest flit = 67753
Fragmentation average = 243.141
	minimum = 0
	maximum = 891
Injected packet rate average = 0.0535417
	minimum = 0.039 (at node 140)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0176875
	minimum = 0.009 (at node 117)
	maximum = 0.029 (at node 182)
Injected flit rate average = 0.964151
	minimum = 0.707 (at node 140)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.327823
	minimum = 0.177 (at node 190)
	maximum = 0.549 (at node 182)
Injected packet length average = 18.0075
Accepted packet length average = 18.5342
Total in-flight flits = 359645 (0 measured)
latency change    = 0.555299
throughput change = 0.043556
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 213.248
	minimum = 24
	maximum = 660
Network latency average = 38.5985
	minimum = 22
	maximum = 255
Slowest packet = 30203
Flit latency average = 1793.39
	minimum = 5
	maximum = 3455
Slowest flit = 87083
Fragmentation average = 6.72871
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0539115
	minimum = 0.039 (at node 162)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0173802
	minimum = 0.008 (at node 46)
	maximum = 0.03 (at node 19)
Injected flit rate average = 0.969901
	minimum = 0.691 (at node 162)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.322198
	minimum = 0.17 (at node 80)
	maximum = 0.531 (at node 3)
Injected packet length average = 17.9906
Accepted packet length average = 18.5382
Total in-flight flits = 484101 (171319 measured)
latency change    = 5.47708
throughput change = 0.0174582
Class 0:
Packet latency average = 233.054
	minimum = 24
	maximum = 761
Network latency average = 41.3914
	minimum = 22
	maximum = 557
Slowest packet = 30203
Flit latency average = 2059.12
	minimum = 5
	maximum = 4405
Slowest flit = 93704
Fragmentation average = 6.74403
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0541901
	minimum = 0.045 (at node 73)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0175859
	minimum = 0.0115 (at node 46)
	maximum = 0.025 (at node 3)
Injected flit rate average = 0.975346
	minimum = 0.808 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.323216
	minimum = 0.215 (at node 26)
	maximum = 0.4645 (at node 3)
Injected packet length average = 17.9986
Accepted packet length average = 18.3792
Total in-flight flits = 610092 (344230 measured)
latency change    = 0.084985
throughput change = 0.0031503
Class 0:
Packet latency average = 249.018
	minimum = 24
	maximum = 933
Network latency average = 42.4263
	minimum = 22
	maximum = 557
Slowest packet = 30203
Flit latency average = 2329.56
	minimum = 5
	maximum = 5387
Slowest flit = 96803
Fragmentation average = 6.85647
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0543368
	minimum = 0.045 (at node 73)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0176649
	minimum = 0.0126667 (at node 49)
	maximum = 0.025 (at node 188)
Injected flit rate average = 0.977953
	minimum = 0.811 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.323389
	minimum = 0.236 (at node 36)
	maximum = 0.441667 (at node 188)
Injected packet length average = 17.998
Accepted packet length average = 18.3068
Total in-flight flits = 736737 (517505 measured)
latency change    = 0.0641071
throughput change = 0.000534165
Draining remaining packets ...
Class 0:
Remaining flits: 141276 141277 141278 141279 141280 141281 141605 141932 141933 141934 [...] (688934 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (516944 flits)
Class 0:
Remaining flits: 145204 145205 147808 147809 147810 147811 147812 147813 147814 147815 [...] (641508 flits)
Measured flits: 542988 542989 542990 542991 542992 542993 542994 542995 542996 542997 [...] (516046 flits)
Class 0:
Remaining flits: 180740 180741 180742 180743 180744 180745 180746 180747 180748 180749 [...] (594070 flits)
Measured flits: 543005 543006 543007 543008 543009 543010 543011 543012 543013 543014 [...] (511770 flits)
Class 0:
Remaining flits: 200028 200029 200030 200031 200032 200033 258228 258229 258230 258231 [...] (546444 flits)
Measured flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (500238 flits)
Class 0:
Remaining flits: 258241 258242 258243 258244 258245 279306 279307 279308 279309 279310 [...] (499577 flits)
Measured flits: 543027 543028 543029 543030 543031 543032 543033 543034 543035 543036 [...] (477625 flits)
Class 0:
Remaining flits: 321782 321783 321784 321785 329296 329297 329298 329299 329300 329301 [...] (453025 flits)
Measured flits: 543146 543147 543148 543149 543168 543169 543170 543171 543172 543173 [...] (444627 flits)
Class 0:
Remaining flits: 332708 332709 332710 332711 386315 442534 442535 442536 442537 442538 [...] (405394 flits)
Measured flits: 543172 543173 543174 543175 543176 543177 543178 543179 543180 543181 [...] (402746 flits)
Class 0:
Remaining flits: 473666 473667 473668 473669 490516 490517 500562 500563 500564 500565 [...] (358067 flits)
Measured flits: 544676 544677 544678 544679 544734 544735 544736 544737 544738 544739 [...] (357509 flits)
Class 0:
Remaining flits: 519061 519062 519063 519064 519065 521059 521060 521061 521062 521063 [...] (310960 flits)
Measured flits: 547614 547615 547616 547617 547618 547619 547620 547621 547622 547623 [...] (310893 flits)
Class 0:
Remaining flits: 553262 553263 553264 553265 553968 553969 553970 553971 553972 553973 [...] (263860 flits)
Measured flits: 553262 553263 553264 553265 553968 553969 553970 553971 553972 553973 [...] (263860 flits)
Class 0:
Remaining flits: 553968 553969 553970 553971 553972 553973 553974 553975 553976 553977 [...] (216800 flits)
Measured flits: 553968 553969 553970 553971 553972 553973 553974 553975 553976 553977 [...] (216800 flits)
Class 0:
Remaining flits: 617873 617874 617875 617876 617877 617878 617879 617880 617881 617882 [...] (169688 flits)
Measured flits: 617873 617874 617875 617876 617877 617878 617879 617880 617881 617882 [...] (169688 flits)
Class 0:
Remaining flits: 654719 654720 654721 654722 654723 654724 654725 654726 654727 654728 [...] (122506 flits)
Measured flits: 654719 654720 654721 654722 654723 654724 654725 654726 654727 654728 [...] (122506 flits)
Class 0:
Remaining flits: 756162 756163 756164 756165 756166 756167 756168 756169 756170 756171 [...] (75486 flits)
Measured flits: 756162 756163 756164 756165 756166 756167 756168 756169 756170 756171 [...] (75486 flits)
Class 0:
Remaining flits: 785953 785954 785955 785956 785957 785958 785959 785960 785961 785962 [...] (28649 flits)
Measured flits: 785953 785954 785955 785956 785957 785958 785959 785960 785961 785962 [...] (28649 flits)
Class 0:
Remaining flits: 915858 915859 915860 915861 915862 915863 915864 915865 915866 915867 [...] (3724 flits)
Measured flits: 915858 915859 915860 915861 915862 915863 915864 915865 915866 915867 [...] (3724 flits)
Time taken is 22714 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10977 (1 samples)
	minimum = 24 (1 samples)
	maximum = 17886 (1 samples)
Network latency average = 10771 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17438 (1 samples)
Flit latency average = 8412.81 (1 samples)
	minimum = 5 (1 samples)
	maximum = 17421 (1 samples)
Fragmentation average = 377.167 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1114 (1 samples)
Injected packet rate average = 0.0543368 (1 samples)
	minimum = 0.045 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0176649 (1 samples)
	minimum = 0.0126667 (1 samples)
	maximum = 0.025 (1 samples)
Injected flit rate average = 0.977953 (1 samples)
	minimum = 0.811 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.323389 (1 samples)
	minimum = 0.236 (1 samples)
	maximum = 0.441667 (1 samples)
Injected packet size average = 17.998 (1 samples)
Accepted packet size average = 18.3068 (1 samples)
Hops average = 5.07163 (1 samples)
Total run time 30.9902
