BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 318.268
	minimum = 24
	maximum = 860
Network latency average = 271.92
	minimum = 23
	maximum = 845
Slowest packet = 176
Flit latency average = 229.867
	minimum = 6
	maximum = 848
Slowest flit = 11175
Fragmentation average = 51.6946
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0139115
	minimum = 0.007 (at node 41)
	maximum = 0.025 (at node 187)
Accepted packet rate average = 0.00900521
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.246891
	minimum = 0.111 (at node 160)
	maximum = 0.437 (at node 187)
Accepted flit rate average= 0.168057
	minimum = 0.054 (at node 178)
	maximum = 0.306 (at node 91)
Injected packet length average = 17.7473
Accepted packet length average = 18.6622
Total in-flight flits = 17791 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 633.375
	minimum = 24
	maximum = 1689
Network latency average = 391.547
	minimum = 23
	maximum = 1689
Slowest packet = 893
Flit latency average = 339.57
	minimum = 6
	maximum = 1752
Slowest flit = 19072
Fragmentation average = 55.1513
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0114141
	minimum = 0.0055 (at node 41)
	maximum = 0.02 (at node 123)
Accepted packet rate average = 0.00894792
	minimum = 0.0045 (at node 135)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.203901
	minimum = 0.099 (at node 41)
	maximum = 0.3525 (at node 123)
Accepted flit rate average= 0.164326
	minimum = 0.081 (at node 135)
	maximum = 0.266 (at node 22)
Injected packet length average = 17.864
Accepted packet length average = 18.3647
Total in-flight flits = 17809 (0 measured)
latency change    = 0.497505
throughput change = 0.0227096
Class 0:
Packet latency average = 1626.71
	minimum = 626
	maximum = 2549
Network latency average = 558.543
	minimum = 23
	maximum = 2455
Slowest packet = 1360
Flit latency average = 491.469
	minimum = 6
	maximum = 2405
Slowest flit = 34054
Fragmentation average = 60.2732
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00926562
	minimum = 0 (at node 20)
	maximum = 0.022 (at node 87)
Accepted packet rate average = 0.0091875
	minimum = 0.002 (at node 153)
	maximum = 0.017 (at node 6)
Injected flit rate average = 0.165911
	minimum = 0 (at node 20)
	maximum = 0.386 (at node 87)
Accepted flit rate average= 0.164521
	minimum = 0.044 (at node 153)
	maximum = 0.318 (at node 6)
Injected packet length average = 17.9061
Accepted packet length average = 17.907
Total in-flight flits = 18117 (0 measured)
latency change    = 0.61064
throughput change = 0.00118716
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2343.09
	minimum = 1085
	maximum = 3341
Network latency average = 333.456
	minimum = 23
	maximum = 936
Slowest packet = 6274
Flit latency average = 478.404
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 52.4582
	minimum = 0
	maximum = 127
Injected packet rate average = 0.00903646
	minimum = 0 (at node 64)
	maximum = 0.02 (at node 178)
Accepted packet rate average = 0.00903125
	minimum = 0.002 (at node 184)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.162292
	minimum = 0 (at node 64)
	maximum = 0.36 (at node 178)
Accepted flit rate average= 0.162615
	minimum = 0.036 (at node 184)
	maximum = 0.317 (at node 56)
Injected packet length average = 17.9597
Accepted packet length average = 18.0058
Total in-flight flits = 18035 (16609 measured)
latency change    = 0.305743
throughput change = 0.0117225
Class 0:
Packet latency average = 2757.32
	minimum = 1085
	maximum = 4002
Network latency average = 461.393
	minimum = 23
	maximum = 1777
Slowest packet = 6274
Flit latency average = 488.965
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 55.7264
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00898958
	minimum = 0.001 (at node 64)
	maximum = 0.02 (at node 160)
Accepted packet rate average = 0.00897917
	minimum = 0.0045 (at node 91)
	maximum = 0.0155 (at node 59)
Injected flit rate average = 0.161651
	minimum = 0.018 (at node 64)
	maximum = 0.3575 (at node 160)
Accepted flit rate average= 0.162107
	minimum = 0.0805 (at node 91)
	maximum = 0.2755 (at node 59)
Injected packet length average = 17.982
Accepted packet length average = 18.0537
Total in-flight flits = 17914 (17891 measured)
latency change    = 0.150229
throughput change = 0.00313258
Class 0:
Packet latency average = 3103.75
	minimum = 1085
	maximum = 4780
Network latency average = 494.884
	minimum = 23
	maximum = 2102
Slowest packet = 6274
Flit latency average = 486.005
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 56.6291
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00893403
	minimum = 0.00333333 (at node 64)
	maximum = 0.0173333 (at node 160)
Accepted packet rate average = 0.0089809
	minimum = 0.005 (at node 71)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.160825
	minimum = 0.06 (at node 64)
	maximum = 0.312 (at node 160)
Accepted flit rate average= 0.161646
	minimum = 0.0953333 (at node 71)
	maximum = 0.263333 (at node 16)
Injected packet length average = 18.0014
Accepted packet length average = 17.9988
Total in-flight flits = 17763 (17763 measured)
latency change    = 0.111615
throughput change = 0.00285153
Draining remaining packets ...
Class 0:
Remaining flits: 199088 199089 199090 199091 199092 199093 199094 199095 199096 199097 [...] (34 flits)
Measured flits: 199088 199089 199090 199091 199092 199093 199094 199095 199096 199097 [...] (34 flits)
Time taken is 7038 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3324.63 (1 samples)
	minimum = 1085 (1 samples)
	maximum = 5494 (1 samples)
Network latency average = 533.098 (1 samples)
	minimum = 23 (1 samples)
	maximum = 2113 (1 samples)
Flit latency average = 509.296 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3039 (1 samples)
Fragmentation average = 55.3573 (1 samples)
	minimum = 0 (1 samples)
	maximum = 148 (1 samples)
Injected packet rate average = 0.00893403 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.0173333 (1 samples)
Accepted packet rate average = 0.0089809 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0146667 (1 samples)
Injected flit rate average = 0.160825 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.312 (1 samples)
Accepted flit rate average = 0.161646 (1 samples)
	minimum = 0.0953333 (1 samples)
	maximum = 0.263333 (1 samples)
Injected packet size average = 18.0014 (1 samples)
Accepted packet size average = 17.9988 (1 samples)
Hops average = 5.06229 (1 samples)
Total run time 4.67662
