BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 369.931
	minimum = 27
	maximum = 980
Network latency average = 279.074
	minimum = 23
	maximum = 837
Slowest packet = 24
Flit latency average = 244.079
	minimum = 6
	maximum = 857
Slowest flit = 11388
Fragmentation average = 53.2459
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0273594
	minimum = 0 (at node 2)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.00982813
	minimum = 0.004 (at node 23)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.487703
	minimum = 0 (at node 2)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.184224
	minimum = 0.079 (at node 115)
	maximum = 0.318 (at node 152)
Injected packet length average = 17.8258
Accepted packet length average = 18.7446
Total in-flight flits = 59273 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 669.926
	minimum = 23
	maximum = 1971
Network latency average = 536.631
	minimum = 23
	maximum = 1682
Slowest packet = 24
Flit latency average = 499.548
	minimum = 6
	maximum = 1823
Slowest flit = 14138
Fragmentation average = 60.7975
	minimum = 0
	maximum = 166
Injected packet rate average = 0.024099
	minimum = 0.003 (at node 106)
	maximum = 0.045 (at node 61)
Accepted packet rate average = 0.00988802
	minimum = 0.005 (at node 96)
	maximum = 0.0165 (at node 44)
Injected flit rate average = 0.431641
	minimum = 0.047 (at node 106)
	maximum = 0.8055 (at node 61)
Accepted flit rate average= 0.181932
	minimum = 0.09 (at node 108)
	maximum = 0.3045 (at node 44)
Injected packet length average = 17.9112
Accepted packet length average = 18.3993
Total in-flight flits = 97466 (0 measured)
latency change    = 0.447804
throughput change = 0.0125963
Class 0:
Packet latency average = 1604.16
	minimum = 23
	maximum = 2977
Network latency average = 1355.4
	minimum = 23
	maximum = 2703
Slowest packet = 852
Flit latency average = 1332.82
	minimum = 6
	maximum = 2652
Slowest flit = 21185
Fragmentation average = 67.5331
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0159427
	minimum = 0 (at node 20)
	maximum = 0.047 (at node 177)
Accepted packet rate average = 0.00975
	minimum = 0.002 (at node 18)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.288167
	minimum = 0 (at node 20)
	maximum = 0.846 (at node 177)
Accepted flit rate average= 0.175755
	minimum = 0.062 (at node 18)
	maximum = 0.315 (at node 16)
Injected packet length average = 18.0751
Accepted packet length average = 18.0262
Total in-flight flits = 119737 (0 measured)
latency change    = 0.582382
throughput change = 0.0351459
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1016.16
	minimum = 27
	maximum = 2807
Network latency average = 143.942
	minimum = 23
	maximum = 862
Slowest packet = 12425
Flit latency average = 1972.07
	minimum = 6
	maximum = 3654
Slowest flit = 23921
Fragmentation average = 20.7093
	minimum = 0
	maximum = 117
Injected packet rate average = 0.0105156
	minimum = 0 (at node 32)
	maximum = 0.037 (at node 119)
Accepted packet rate average = 0.00917708
	minimum = 0.004 (at node 17)
	maximum = 0.021 (at node 6)
Injected flit rate average = 0.189771
	minimum = 0 (at node 32)
	maximum = 0.662 (at node 119)
Accepted flit rate average= 0.16399
	minimum = 0.053 (at node 71)
	maximum = 0.368 (at node 6)
Injected packet length average = 18.0466
Accepted packet length average = 17.8695
Total in-flight flits = 125079 (35207 measured)
latency change    = 0.578642
throughput change = 0.0717462
Class 0:
Packet latency average = 1828.45
	minimum = 27
	maximum = 4494
Network latency average = 620.668
	minimum = 23
	maximum = 1934
Slowest packet = 12425
Flit latency average = 2249.19
	minimum = 6
	maximum = 4553
Slowest flit = 39909
Fragmentation average = 41.9772
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00991927
	minimum = 0.0005 (at node 129)
	maximum = 0.0295 (at node 86)
Accepted packet rate average = 0.00916146
	minimum = 0.0045 (at node 86)
	maximum = 0.016 (at node 6)
Injected flit rate average = 0.178536
	minimum = 0.009 (at node 129)
	maximum = 0.531 (at node 86)
Accepted flit rate average= 0.164456
	minimum = 0.079 (at node 86)
	maximum = 0.283 (at node 6)
Injected packet length average = 17.9989
Accepted packet length average = 17.9508
Total in-flight flits = 125706 (63411 measured)
latency change    = 0.444248
throughput change = 0.00283448
Class 0:
Packet latency average = 2511.1
	minimum = 27
	maximum = 5306
Network latency average = 1171.86
	minimum = 23
	maximum = 2923
Slowest packet = 12425
Flit latency average = 2492.33
	minimum = 6
	maximum = 5231
Slowest flit = 58193
Fragmentation average = 53.2618
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00961111
	minimum = 0.000666667 (at node 128)
	maximum = 0.0233333 (at node 86)
Accepted packet rate average = 0.00915278
	minimum = 0.00466667 (at node 158)
	maximum = 0.013 (at node 6)
Injected flit rate average = 0.173257
	minimum = 0.012 (at node 128)
	maximum = 0.42 (at node 86)
Accepted flit rate average= 0.1648
	minimum = 0.0786667 (at node 158)
	maximum = 0.23 (at node 6)
Injected packet length average = 18.0267
Accepted packet length average = 18.0055
Total in-flight flits = 125162 (86074 measured)
latency change    = 0.271854
throughput change = 0.00209112
Class 0:
Packet latency average = 3177.48
	minimum = 27
	maximum = 6363
Network latency average = 1644.18
	minimum = 23
	maximum = 3994
Slowest packet = 12425
Flit latency average = 2696.8
	minimum = 6
	maximum = 6116
Slowest flit = 67175
Fragmentation average = 60.767
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00943099
	minimum = 0.00125 (at node 128)
	maximum = 0.0195 (at node 119)
Accepted packet rate average = 0.00911458
	minimum = 0.005 (at node 31)
	maximum = 0.012 (at node 6)
Injected flit rate average = 0.169803
	minimum = 0.02225 (at node 128)
	maximum = 0.351 (at node 119)
Accepted flit rate average= 0.163983
	minimum = 0.09 (at node 31)
	maximum = 0.218 (at node 107)
Injected packet length average = 18.0048
Accepted packet length average = 17.9913
Total in-flight flits = 124802 (102942 measured)
latency change    = 0.20972
throughput change = 0.00498389
Draining remaining packets ...
Class 0:
Remaining flits: 68436 68437 68438 68439 68440 68441 68442 68443 68444 68445 [...] (94559 flits)
Measured flits: 223362 223363 223364 223365 223366 223367 223368 223369 223370 223371 [...] (84698 flits)
Class 0:
Remaining flits: 105839 119610 119611 119612 119613 119614 119615 119616 119617 119618 [...] (65278 flits)
Measured flits: 223371 223372 223373 223374 223375 223376 223377 223378 223379 223434 [...] (61382 flits)
Class 0:
Remaining flits: 125061 125062 125063 148392 148393 148394 148395 148396 148397 148398 [...] (37339 flits)
Measured flits: 223884 223885 223886 223887 223888 223889 223890 223891 223892 223893 [...] (35961 flits)
Class 0:
Remaining flits: 154890 154891 154892 154893 154894 154895 154896 154897 154898 154899 [...] (11922 flits)
Measured flits: 225180 225181 225182 225183 225184 225185 225186 225187 225188 225189 [...] (11626 flits)
Class 0:
Remaining flits: 214312 214313 214314 214315 214316 214317 214318 214319 214320 214321 [...] (214 flits)
Measured flits: 259722 259723 259724 259725 259726 259727 259728 259729 259730 259731 [...] (200 flits)
Time taken is 12219 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5796.69 (1 samples)
	minimum = 27 (1 samples)
	maximum = 11125 (1 samples)
Network latency average = 3657.54 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8509 (1 samples)
Flit latency average = 3552.81 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9355 (1 samples)
Fragmentation average = 65.7286 (1 samples)
	minimum = 0 (1 samples)
	maximum = 160 (1 samples)
Injected packet rate average = 0.00943099 (1 samples)
	minimum = 0.00125 (1 samples)
	maximum = 0.0195 (1 samples)
Accepted packet rate average = 0.00911458 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.169803 (1 samples)
	minimum = 0.02225 (1 samples)
	maximum = 0.351 (1 samples)
Accepted flit rate average = 0.163983 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.218 (1 samples)
Injected packet size average = 18.0048 (1 samples)
Accepted packet size average = 17.9913 (1 samples)
Hops average = 5.09398 (1 samples)
Total run time 11.2076
