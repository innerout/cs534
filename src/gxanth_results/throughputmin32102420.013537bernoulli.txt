BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 100.386
	minimum = 22
	maximum = 409
Network latency average = 97.8453
	minimum = 22
	maximum = 403
Slowest packet = 1471
Flit latency average = 69.8083
	minimum = 5
	maximum = 386
Slowest flit = 26491
Fragmentation average = 22.3798
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0136094
	minimum = 0.006 (at node 87)
	maximum = 0.022 (at node 123)
Accepted packet rate average = 0.011849
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.242297
	minimum = 0.1 (at node 87)
	maximum = 0.396 (at node 123)
Accepted flit rate average= 0.217688
	minimum = 0.072 (at node 64)
	maximum = 0.369 (at node 132)
Injected packet length average = 17.8037
Accepted packet length average = 18.3719
Total in-flight flits = 5238 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 123.623
	minimum = 22
	maximum = 528
Network latency average = 120.985
	minimum = 22
	maximum = 528
Slowest packet = 3706
Flit latency average = 92.0033
	minimum = 5
	maximum = 511
Slowest flit = 66725
Fragmentation average = 23.4956
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0133906
	minimum = 0.0075 (at node 161)
	maximum = 0.019 (at node 77)
Accepted packet rate average = 0.0124323
	minimum = 0.007 (at node 30)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.239911
	minimum = 0.135 (at node 161)
	maximum = 0.342 (at node 77)
Accepted flit rate average= 0.225893
	minimum = 0.126 (at node 83)
	maximum = 0.324 (at node 22)
Injected packet length average = 17.9164
Accepted packet length average = 18.1699
Total in-flight flits = 5813 (0 measured)
latency change    = 0.187966
throughput change = 0.0363257
Class 0:
Packet latency average = 153.972
	minimum = 22
	maximum = 599
Network latency average = 151.417
	minimum = 22
	maximum = 595
Slowest packet = 3979
Flit latency average = 122.074
	minimum = 5
	maximum = 578
Slowest flit = 71457
Fragmentation average = 23.7164
	minimum = 0
	maximum = 198
Injected packet rate average = 0.0134844
	minimum = 0.004 (at node 136)
	maximum = 0.025 (at node 191)
Accepted packet rate average = 0.013224
	minimum = 0.004 (at node 184)
	maximum = 0.022 (at node 24)
Injected flit rate average = 0.242443
	minimum = 0.064 (at node 136)
	maximum = 0.457 (at node 191)
Accepted flit rate average= 0.239828
	minimum = 0.084 (at node 184)
	maximum = 0.408 (at node 16)
Injected packet length average = 17.9795
Accepted packet length average = 18.1359
Total in-flight flits = 6368 (0 measured)
latency change    = 0.197103
throughput change = 0.0581037
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 159.74
	minimum = 22
	maximum = 624
Network latency average = 156.997
	minimum = 22
	maximum = 621
Slowest packet = 7811
Flit latency average = 134.813
	minimum = 5
	maximum = 627
Slowest flit = 133307
Fragmentation average = 25.6775
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0140469
	minimum = 0.004 (at node 150)
	maximum = 0.025 (at node 84)
Accepted packet rate average = 0.0137083
	minimum = 0.004 (at node 154)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.252427
	minimum = 0.072 (at node 150)
	maximum = 0.45 (at node 84)
Accepted flit rate average= 0.245542
	minimum = 0.058 (at node 154)
	maximum = 0.454 (at node 16)
Injected packet length average = 17.9703
Accepted packet length average = 17.9119
Total in-flight flits = 7770 (7770 measured)
latency change    = 0.0361099
throughput change = 0.0232691
Class 0:
Packet latency average = 170.422
	minimum = 22
	maximum = 624
Network latency average = 167.825
	minimum = 22
	maximum = 621
Slowest packet = 7811
Flit latency average = 141.143
	minimum = 5
	maximum = 627
Slowest flit = 133307
Fragmentation average = 24.8182
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0136901
	minimum = 0.0085 (at node 10)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.013737
	minimum = 0.006 (at node 4)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.246563
	minimum = 0.153 (at node 10)
	maximum = 0.392 (at node 126)
Accepted flit rate average= 0.246492
	minimum = 0.1075 (at node 4)
	maximum = 0.39 (at node 129)
Injected packet length average = 18.0103
Accepted packet length average = 17.9437
Total in-flight flits = 6341 (6341 measured)
latency change    = 0.0626788
throughput change = 0.00385619
Class 0:
Packet latency average = 168.289
	minimum = 22
	maximum = 624
Network latency average = 165.688
	minimum = 22
	maximum = 621
Slowest packet = 7811
Flit latency average = 138.012
	minimum = 5
	maximum = 627
Slowest flit = 133307
Fragmentation average = 24.818
	minimum = 0
	maximum = 171
Injected packet rate average = 0.013658
	minimum = 0.00866667 (at node 18)
	maximum = 0.0196667 (at node 116)
Accepted packet rate average = 0.0136163
	minimum = 0.00866667 (at node 86)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.245995
	minimum = 0.156 (at node 18)
	maximum = 0.354 (at node 116)
Accepted flit rate average= 0.244655
	minimum = 0.161 (at node 86)
	maximum = 0.363 (at node 128)
Injected packet length average = 18.0111
Accepted packet length average = 17.9677
Total in-flight flits = 7053 (7053 measured)
latency change    = 0.0126741
throughput change = 0.0075113
Draining remaining packets ...
Time taken is 6498 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 171.356 (1 samples)
	minimum = 22 (1 samples)
	maximum = 624 (1 samples)
Network latency average = 168.731 (1 samples)
	minimum = 22 (1 samples)
	maximum = 621 (1 samples)
Flit latency average = 140.778 (1 samples)
	minimum = 5 (1 samples)
	maximum = 627 (1 samples)
Fragmentation average = 24.8622 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.013658 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0196667 (1 samples)
Accepted packet rate average = 0.0136163 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.245995 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 0.354 (1 samples)
Accepted flit rate average = 0.244655 (1 samples)
	minimum = 0.161 (1 samples)
	maximum = 0.363 (1 samples)
Injected packet size average = 18.0111 (1 samples)
Accepted packet size average = 17.9677 (1 samples)
Hops average = 5.05707 (1 samples)
Total run time 4.01006
