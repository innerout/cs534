BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 370.577
	minimum = 23
	maximum = 979
Network latency average = 285.046
	minimum = 23
	maximum = 904
Slowest packet = 46
Flit latency average = 253.282
	minimum = 6
	maximum = 887
Slowest flit = 5579
Fragmentation average = 54.0998
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0278281
	minimum = 0 (at node 35)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.00980729
	minimum = 0.003 (at node 93)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.495396
	minimum = 0 (at node 35)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.184557
	minimum = 0.063 (at node 174)
	maximum = 0.306 (at node 91)
Injected packet length average = 17.802
Accepted packet length average = 18.8184
Total in-flight flits = 60901 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 679.056
	minimum = 23
	maximum = 1983
Network latency average = 542.863
	minimum = 23
	maximum = 1796
Slowest packet = 46
Flit latency average = 505.688
	minimum = 6
	maximum = 1785
Slowest flit = 17902
Fragmentation average = 61.2609
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0249271
	minimum = 0.004 (at node 43)
	maximum = 0.0395 (at node 9)
Accepted packet rate average = 0.0100208
	minimum = 0.0045 (at node 116)
	maximum = 0.0145 (at node 71)
Injected flit rate average = 0.446445
	minimum = 0.072 (at node 43)
	maximum = 0.703 (at node 9)
Accepted flit rate average= 0.184198
	minimum = 0.081 (at node 116)
	maximum = 0.263 (at node 140)
Injected packet length average = 17.9101
Accepted packet length average = 18.3815
Total in-flight flits = 102320 (0 measured)
latency change    = 0.454276
throughput change = 0.00195103
Class 0:
Packet latency average = 1699.34
	minimum = 27
	maximum = 2938
Network latency average = 1413.26
	minimum = 24
	maximum = 2712
Slowest packet = 3111
Flit latency average = 1388.67
	minimum = 6
	maximum = 2692
Slowest flit = 27261
Fragmentation average = 69.9353
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0145469
	minimum = 0 (at node 129)
	maximum = 0.042 (at node 15)
Accepted packet rate average = 0.00934375
	minimum = 0.003 (at node 0)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.263281
	minimum = 0 (at node 129)
	maximum = 0.757 (at node 15)
Accepted flit rate average= 0.168042
	minimum = 0.055 (at node 0)
	maximum = 0.343 (at node 103)
Injected packet length average = 18.0988
Accepted packet length average = 17.9844
Total in-flight flits = 121212 (0 measured)
latency change    = 0.6004
throughput change = 0.0961443
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1141.6
	minimum = 32
	maximum = 3514
Network latency average = 127.286
	minimum = 23
	maximum = 922
Slowest packet = 12500
Flit latency average = 1991.78
	minimum = 6
	maximum = 3668
Slowest flit = 29813
Fragmentation average = 17.0519
	minimum = 0
	maximum = 128
Injected packet rate average = 0.00957812
	minimum = 0 (at node 28)
	maximum = 0.041 (at node 13)
Accepted packet rate average = 0.00925521
	minimum = 0.002 (at node 23)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.172948
	minimum = 0 (at node 41)
	maximum = 0.738 (at node 170)
Accepted flit rate average= 0.166437
	minimum = 0.036 (at node 23)
	maximum = 0.332 (at node 76)
Injected packet length average = 18.0566
Accepted packet length average = 17.9831
Total in-flight flits = 123042 (32345 measured)
latency change    = 0.488563
throughput change = 0.00963825
Class 0:
Packet latency average = 2132.33
	minimum = 32
	maximum = 4705
Network latency average = 598.848
	minimum = 23
	maximum = 1959
Slowest packet = 12500
Flit latency average = 2277.81
	minimum = 6
	maximum = 4443
Slowest flit = 45413
Fragmentation average = 38.7695
	minimum = 0
	maximum = 128
Injected packet rate average = 0.00936979
	minimum = 0.0005 (at node 104)
	maximum = 0.025 (at node 171)
Accepted packet rate average = 0.00921094
	minimum = 0.004 (at node 23)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.16894
	minimum = 0.009 (at node 104)
	maximum = 0.45 (at node 171)
Accepted flit rate average= 0.165508
	minimum = 0.072 (at node 23)
	maximum = 0.2925 (at node 78)
Injected packet length average = 18.0303
Accepted packet length average = 17.9686
Total in-flight flits = 123123 (60419 measured)
latency change    = 0.464625
throughput change = 0.00561718
Class 0:
Packet latency average = 2894
	minimum = 32
	maximum = 5448
Network latency average = 1181.44
	minimum = 23
	maximum = 2971
Slowest packet = 12500
Flit latency average = 2518.91
	minimum = 6
	maximum = 5206
Slowest flit = 61865
Fragmentation average = 53.472
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00917014
	minimum = 0.00166667 (at node 20)
	maximum = 0.0233333 (at node 171)
Accepted packet rate average = 0.00915104
	minimum = 0.006 (at node 22)
	maximum = 0.0153333 (at node 78)
Injected flit rate average = 0.165054
	minimum = 0.03 (at node 20)
	maximum = 0.42 (at node 171)
Accepted flit rate average= 0.164969
	minimum = 0.104 (at node 161)
	maximum = 0.275667 (at node 78)
Injected packet length average = 17.9991
Accepted packet length average = 18.0273
Total in-flight flits = 121752 (81904 measured)
latency change    = 0.26319
throughput change = 0.00326766
Draining remaining packets ...
Class 0:
Remaining flits: 62640 62641 62642 62643 62644 62645 62646 62647 62648 62649 [...] (92417 flits)
Measured flits: 224208 224209 224210 224211 224212 224213 224214 224215 224216 224217 [...] (69742 flits)
Class 0:
Remaining flits: 66564 66565 66566 66567 66568 66569 66570 66571 66572 66573 [...] (62863 flits)
Measured flits: 224208 224209 224210 224211 224212 224213 224214 224215 224216 224217 [...] (51938 flits)
Class 0:
Remaining flits: 94932 94933 94934 94935 94936 94937 94938 94939 94940 94941 [...] (35085 flits)
Measured flits: 224298 224299 224300 224301 224302 224303 224304 224305 224306 224307 [...] (30722 flits)
Class 0:
Remaining flits: 94932 94933 94934 94935 94936 94937 94938 94939 94940 94941 [...] (9595 flits)
Measured flits: 224694 224695 224696 224697 224698 224699 224700 224701 224702 224703 [...] (8661 flits)
Time taken is 10953 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5536.44 (1 samples)
	minimum = 32 (1 samples)
	maximum = 9997 (1 samples)
Network latency average = 3496.69 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7571 (1 samples)
Flit latency average = 3500.34 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9161 (1 samples)
Fragmentation average = 67.1055 (1 samples)
	minimum = 0 (1 samples)
	maximum = 157 (1 samples)
Injected packet rate average = 0.00917014 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.0233333 (1 samples)
Accepted packet rate average = 0.00915104 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.165054 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.42 (1 samples)
Accepted flit rate average = 0.164969 (1 samples)
	minimum = 0.104 (1 samples)
	maximum = 0.275667 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0273 (1 samples)
Hops average = 5.10002 (1 samples)
Total run time 7.37269
