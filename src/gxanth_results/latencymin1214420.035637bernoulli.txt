BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 297.421
	minimum = 22
	maximum = 813
Network latency average = 284.703
	minimum = 22
	maximum = 791
Slowest packet = 1061
Flit latency average = 260.898
	minimum = 5
	maximum = 774
Slowest flit = 19098
Fragmentation average = 24.04
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0348906
	minimum = 0.02 (at node 21)
	maximum = 0.05 (at node 175)
Accepted packet rate average = 0.0145833
	minimum = 0.008 (at node 28)
	maximum = 0.024 (at node 140)
Injected flit rate average = 0.62274
	minimum = 0.36 (at node 21)
	maximum = 0.888 (at node 175)
Accepted flit rate average= 0.269026
	minimum = 0.144 (at node 174)
	maximum = 0.432 (at node 140)
Injected packet length average = 17.8483
Accepted packet length average = 18.4475
Total in-flight flits = 68983 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 560.916
	minimum = 22
	maximum = 1519
Network latency average = 544.805
	minimum = 22
	maximum = 1455
Slowest packet = 2599
Flit latency average = 521.136
	minimum = 5
	maximum = 1514
Slowest flit = 56988
Fragmentation average = 24.8479
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0331276
	minimum = 0.021 (at node 4)
	maximum = 0.0435 (at node 107)
Accepted packet rate average = 0.015099
	minimum = 0.009 (at node 96)
	maximum = 0.021 (at node 71)
Injected flit rate average = 0.593646
	minimum = 0.378 (at node 4)
	maximum = 0.783 (at node 107)
Accepted flit rate average= 0.274758
	minimum = 0.168 (at node 96)
	maximum = 0.3865 (at node 71)
Injected packet length average = 17.92
Accepted packet length average = 18.1971
Total in-flight flits = 124155 (0 measured)
latency change    = 0.469757
throughput change = 0.0208612
Class 0:
Packet latency average = 1395.68
	minimum = 22
	maximum = 2401
Network latency average = 1358.65
	minimum = 22
	maximum = 2401
Slowest packet = 3835
Flit latency average = 1339.96
	minimum = 5
	maximum = 2384
Slowest flit = 69047
Fragmentation average = 21.476
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0208385
	minimum = 0.003 (at node 56)
	maximum = 0.044 (at node 42)
Accepted packet rate average = 0.0151094
	minimum = 0.004 (at node 184)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.376891
	minimum = 0.054 (at node 56)
	maximum = 0.801 (at node 42)
Accepted flit rate average= 0.271802
	minimum = 0.072 (at node 184)
	maximum = 0.498 (at node 34)
Injected packet length average = 18.0862
Accepted packet length average = 17.989
Total in-flight flits = 145193 (0 measured)
latency change    = 0.598106
throughput change = 0.0108746
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 973.341
	minimum = 22
	maximum = 2411
Network latency average = 106.683
	minimum = 22
	maximum = 813
Slowest packet = 16913
Flit latency average = 1826.63
	minimum = 5
	maximum = 3116
Slowest flit = 105401
Fragmentation average = 5.52033
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0161667
	minimum = 0.003 (at node 4)
	maximum = 0.036 (at node 128)
Accepted packet rate average = 0.0150885
	minimum = 0.007 (at node 155)
	maximum = 0.027 (at node 41)
Injected flit rate average = 0.291807
	minimum = 0.039 (at node 4)
	maximum = 0.638 (at node 128)
Accepted flit rate average= 0.270807
	minimum = 0.125 (at node 155)
	maximum = 0.486 (at node 41)
Injected packet length average = 18.0499
Accepted packet length average = 17.9479
Total in-flight flits = 149484 (54037 measured)
latency change    = 0.433907
throughput change = 0.00367343
Class 0:
Packet latency average = 1842.63
	minimum = 22
	maximum = 3554
Network latency average = 774.628
	minimum = 22
	maximum = 1972
Slowest packet = 16913
Flit latency average = 2052.92
	minimum = 5
	maximum = 3680
Slowest flit = 153539
Fragmentation average = 10.8184
	minimum = 0
	maximum = 62
Injected packet rate average = 0.0153854
	minimum = 0.0035 (at node 121)
	maximum = 0.032 (at node 125)
Accepted packet rate average = 0.0149271
	minimum = 0.0085 (at node 31)
	maximum = 0.0215 (at node 118)
Injected flit rate average = 0.276966
	minimum = 0.063 (at node 121)
	maximum = 0.575 (at node 125)
Accepted flit rate average= 0.268242
	minimum = 0.159 (at node 31)
	maximum = 0.3845 (at node 118)
Injected packet length average = 18.0019
Accepted packet length average = 17.9702
Total in-flight flits = 148568 (98117 measured)
latency change    = 0.471764
throughput change = 0.00956264
Class 0:
Packet latency average = 2576.24
	minimum = 22
	maximum = 4371
Network latency average = 1420.37
	minimum = 22
	maximum = 2947
Slowest packet = 16913
Flit latency average = 2235.17
	minimum = 5
	maximum = 4403
Slowest flit = 186803
Fragmentation average = 14.4061
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0154757
	minimum = 0.00466667 (at node 91)
	maximum = 0.0296667 (at node 125)
Accepted packet rate average = 0.0148385
	minimum = 0.01 (at node 135)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.27862
	minimum = 0.084 (at node 91)
	maximum = 0.531333 (at node 125)
Accepted flit rate average= 0.266601
	minimum = 0.18 (at node 162)
	maximum = 0.374333 (at node 128)
Injected packet length average = 18.0037
Accepted packet length average = 17.9668
Total in-flight flits = 152443 (134361 measured)
latency change    = 0.284761
throughput change = 0.00615712
Class 0:
Packet latency average = 3171.89
	minimum = 22
	maximum = 5117
Network latency average = 1996.95
	minimum = 22
	maximum = 3911
Slowest packet = 16913
Flit latency average = 2373.72
	minimum = 5
	maximum = 5198
Slowest flit = 201311
Fragmentation average = 17.3106
	minimum = 0
	maximum = 178
Injected packet rate average = 0.0152643
	minimum = 0.007 (at node 91)
	maximum = 0.02575 (at node 125)
Accepted packet rate average = 0.0148086
	minimum = 0.01 (at node 31)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.274918
	minimum = 0.126 (at node 91)
	maximum = 0.4635 (at node 125)
Accepted flit rate average= 0.266423
	minimum = 0.18025 (at node 31)
	maximum = 0.3445 (at node 128)
Injected packet length average = 18.0105
Accepted packet length average = 17.9911
Total in-flight flits = 152098 (148708 measured)
latency change    = 0.187792
throughput change = 0.000666298
Class 0:
Packet latency average = 3662.82
	minimum = 22
	maximum = 5846
Network latency average = 2346.07
	minimum = 22
	maximum = 4767
Slowest packet = 16913
Flit latency average = 2473.81
	minimum = 5
	maximum = 5721
Slowest flit = 248291
Fragmentation average = 18.1776
	minimum = 0
	maximum = 218
Injected packet rate average = 0.0151875
	minimum = 0.008 (at node 4)
	maximum = 0.024 (at node 110)
Accepted packet rate average = 0.0148281
	minimum = 0.0106 (at node 149)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.273478
	minimum = 0.1438 (at node 91)
	maximum = 0.4292 (at node 110)
Accepted flit rate average= 0.26686
	minimum = 0.1916 (at node 149)
	maximum = 0.355 (at node 128)
Injected packet length average = 18.0068
Accepted packet length average = 17.9969
Total in-flight flits = 151663 (151229 measured)
latency change    = 0.134029
throughput change = 0.00163846
Class 0:
Packet latency average = 4050.46
	minimum = 22
	maximum = 6547
Network latency average = 2523.32
	minimum = 22
	maximum = 5399
Slowest packet = 16913
Flit latency average = 2545.23
	minimum = 5
	maximum = 6051
Slowest flit = 288233
Fragmentation average = 18.8888
	minimum = 0
	maximum = 218
Injected packet rate average = 0.015092
	minimum = 0.00716667 (at node 164)
	maximum = 0.0241667 (at node 110)
Accepted packet rate average = 0.0148134
	minimum = 0.0118333 (at node 80)
	maximum = 0.0186667 (at node 128)
Injected flit rate average = 0.27171
	minimum = 0.128333 (at node 164)
	maximum = 0.435 (at node 110)
Accepted flit rate average= 0.266584
	minimum = 0.213667 (at node 149)
	maximum = 0.336 (at node 128)
Injected packet length average = 18.0036
Accepted packet length average = 17.9962
Total in-flight flits = 151396 (151378 measured)
latency change    = 0.0957029
throughput change = 0.00103613
Class 0:
Packet latency average = 4389.54
	minimum = 22
	maximum = 7278
Network latency average = 2620.55
	minimum = 22
	maximum = 5951
Slowest packet = 16913
Flit latency average = 2594.46
	minimum = 5
	maximum = 6464
Slowest flit = 290591
Fragmentation average = 19.3343
	minimum = 0
	maximum = 218
Injected packet rate average = 0.0150149
	minimum = 0.00857143 (at node 164)
	maximum = 0.0232857 (at node 110)
Accepted packet rate average = 0.0148162
	minimum = 0.0117143 (at node 149)
	maximum = 0.0185714 (at node 128)
Injected flit rate average = 0.270295
	minimum = 0.154286 (at node 164)
	maximum = 0.419143 (at node 110)
Accepted flit rate average= 0.266647
	minimum = 0.211286 (at node 149)
	maximum = 0.334714 (at node 128)
Injected packet length average = 18.0018
Accepted packet length average = 17.9969
Total in-flight flits = 150438 (150438 measured)
latency change    = 0.0772477
throughput change = 0.000233928
Draining all recorded packets ...
Class 0:
Remaining flits: 375876 375877 375878 375879 375880 375881 375882 375883 375884 375885 [...] (150792 flits)
Measured flits: 375876 375877 375878 375879 375880 375881 375882 375883 375884 375885 [...] (150792 flits)
Class 0:
Remaining flits: 433566 433567 433568 433569 433570 433571 433572 433573 433574 433575 [...] (150443 flits)
Measured flits: 433566 433567 433568 433569 433570 433571 433572 433573 433574 433575 [...] (150443 flits)
Class 0:
Remaining flits: 505836 505837 505838 505839 505840 505841 505842 505843 505844 505845 [...] (152224 flits)
Measured flits: 505836 505837 505838 505839 505840 505841 505842 505843 505844 505845 [...] (152224 flits)
Class 0:
Remaining flits: 541836 541837 541838 541839 541840 541841 541842 541843 541844 541845 [...] (149943 flits)
Measured flits: 541836 541837 541838 541839 541840 541841 541842 541843 541844 541845 [...] (149691 flits)
Class 0:
Remaining flits: 607770 607771 607772 607773 607774 607775 607776 607777 607778 607779 [...] (147961 flits)
Measured flits: 607770 607771 607772 607773 607774 607775 607776 607777 607778 607779 [...] (147511 flits)
Class 0:
Remaining flits: 607787 618840 618841 618842 618843 618844 618845 618846 618847 618848 [...] (147235 flits)
Measured flits: 607787 618840 618841 618842 618843 618844 618845 618846 618847 618848 [...] (145602 flits)
Class 0:
Remaining flits: 656676 656677 656678 656679 656680 656681 656682 656683 656684 656685 [...] (145076 flits)
Measured flits: 656676 656677 656678 656679 656680 656681 656682 656683 656684 656685 [...] (141654 flits)
Class 0:
Remaining flits: 656686 656687 656688 656689 656690 656691 656692 656693 744084 744085 [...] (148029 flits)
Measured flits: 656686 656687 656688 656689 656690 656691 656692 656693 744084 744085 [...] (140595 flits)
Class 0:
Remaining flits: 747522 747523 747524 747525 747526 747527 747528 747529 747530 747531 [...] (147976 flits)
Measured flits: 747522 747523 747524 747525 747526 747527 747528 747529 747530 747531 [...] (133814 flits)
Class 0:
Remaining flits: 784170 784171 784172 784173 784174 784175 784176 784177 784178 784179 [...] (148753 flits)
Measured flits: 784170 784171 784172 784173 784174 784175 784176 784177 784178 784179 [...] (123287 flits)
Class 0:
Remaining flits: 861030 861031 861032 861033 861034 861035 861036 861037 861038 861039 [...] (147429 flits)
Measured flits: 861030 861031 861032 861033 861034 861035 861036 861037 861038 861039 [...] (104879 flits)
Class 0:
Remaining flits: 961743 961744 961745 961746 961747 961748 961749 961750 961751 961752 [...] (143303 flits)
Measured flits: 961743 961744 961745 961746 961747 961748 961749 961750 961751 961752 [...] (79069 flits)
Class 0:
Remaining flits: 990954 990955 990956 990957 990958 990959 990960 990961 990962 990963 [...] (145804 flits)
Measured flits: 990954 990955 990956 990957 990958 990959 990960 990961 990962 990963 [...] (60635 flits)
Class 0:
Remaining flits: 1031526 1031527 1031528 1031529 1031530 1031531 1031532 1031533 1031534 1031535 [...] (144523 flits)
Measured flits: 1031526 1031527 1031528 1031529 1031530 1031531 1031532 1031533 1031534 1031535 [...] (43349 flits)
Class 0:
Remaining flits: 1089000 1089001 1089002 1089003 1089004 1089005 1089006 1089007 1089008 1089009 [...] (142969 flits)
Measured flits: 1089000 1089001 1089002 1089003 1089004 1089005 1089006 1089007 1089008 1089009 [...] (29192 flits)
Class 0:
Remaining flits: 1104084 1104085 1104086 1104087 1104088 1104089 1104090 1104091 1104092 1104093 [...] (139186 flits)
Measured flits: 1104084 1104085 1104086 1104087 1104088 1104089 1104090 1104091 1104092 1104093 [...] (17519 flits)
Class 0:
Remaining flits: 1142172 1142173 1142174 1142175 1142176 1142177 1142178 1142179 1142180 1142181 [...] (141606 flits)
Measured flits: 1142172 1142173 1142174 1142175 1142176 1142177 1142178 1142179 1142180 1142181 [...] (11104 flits)
Class 0:
Remaining flits: 1236186 1236187 1236188 1236189 1236190 1236191 1236192 1236193 1236194 1236195 [...] (140306 flits)
Measured flits: 1265024 1265025 1265026 1265027 1265028 1265029 1265030 1265031 1265032 1265033 [...] (6993 flits)
Class 0:
Remaining flits: 1296252 1296253 1296254 1296255 1296256 1296257 1296258 1296259 1296260 1296261 [...] (138938 flits)
Measured flits: 1307844 1307845 1307846 1307847 1307848 1307849 1307850 1307851 1307852 1307853 [...] (3676 flits)
Class 0:
Remaining flits: 1296252 1296253 1296254 1296255 1296256 1296257 1296258 1296259 1296260 1296261 [...] (143996 flits)
Measured flits: 1331316 1331317 1331318 1331319 1331320 1331321 1331322 1331323 1331324 1331325 [...] (2220 flits)
Class 0:
Remaining flits: 1308780 1308781 1308782 1308783 1308784 1308785 1308786 1308787 1308788 1308789 [...] (142278 flits)
Measured flits: 1492848 1492849 1492850 1492851 1492852 1492853 1492854 1492855 1492856 1492857 [...] (958 flits)
Class 0:
Remaining flits: 1420686 1420687 1420688 1420689 1420690 1420691 1420692 1420693 1420694 1420695 [...] (143288 flits)
Measured flits: 1536660 1536661 1536662 1536663 1536664 1536665 1536666 1536667 1536668 1536669 [...] (345 flits)
Class 0:
Remaining flits: 1469952 1469953 1469954 1469955 1469956 1469957 1469958 1469959 1469960 1469961 [...] (143451 flits)
Measured flits: 1609704 1609705 1609706 1609707 1609708 1609709 1609710 1609711 1609712 1609713 [...] (144 flits)
Class 0:
Remaining flits: 1486926 1486927 1486928 1486929 1486930 1486931 1486932 1486933 1486934 1486935 [...] (145576 flits)
Measured flits: 1650330 1650331 1650332 1650333 1650334 1650335 1650336 1650337 1650338 1650339 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1541862 1541863 1541864 1541865 1541866 1541867 1541868 1541869 1541870 1541871 [...] (95811 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1626228 1626229 1626230 1626231 1626232 1626233 1626234 1626235 1626236 1626237 [...] (47563 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1707642 1707643 1707644 1707645 1707646 1707647 1707648 1707649 1707650 1707651 [...] (6689 flits)
Measured flits: (0 flits)
Time taken is 37826 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8946.43 (1 samples)
	minimum = 22 (1 samples)
	maximum = 24160 (1 samples)
Network latency average = 2857.12 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8238 (1 samples)
Flit latency average = 2777.92 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8757 (1 samples)
Fragmentation average = 20.1918 (1 samples)
	minimum = 0 (1 samples)
	maximum = 262 (1 samples)
Injected packet rate average = 0.0150149 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0232857 (1 samples)
Accepted packet rate average = 0.0148162 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.270295 (1 samples)
	minimum = 0.154286 (1 samples)
	maximum = 0.419143 (1 samples)
Accepted flit rate average = 0.266647 (1 samples)
	minimum = 0.211286 (1 samples)
	maximum = 0.334714 (1 samples)
Injected packet size average = 18.0018 (1 samples)
Accepted packet size average = 17.9969 (1 samples)
Hops average = 5.05604 (1 samples)
Total run time 37.1117
