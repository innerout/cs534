BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 281.21
	minimum = 23
	maximum = 928
Network latency average = 201.737
	minimum = 22
	maximum = 836
Slowest packet = 85
Flit latency average = 168.552
	minimum = 5
	maximum = 840
Slowest flit = 11388
Fragmentation average = 43.359
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0209063
	minimum = 0 (at node 59)
	maximum = 0.045 (at node 37)
Accepted packet rate average = 0.0127396
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.372312
	minimum = 0 (at node 59)
	maximum = 0.81 (at node 37)
Accepted flit rate average= 0.238234
	minimum = 0.084 (at node 174)
	maximum = 0.41 (at node 44)
Injected packet length average = 17.8087
Accepted packet length average = 18.7003
Total in-flight flits = 26673 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 480.233
	minimum = 22
	maximum = 1791
Network latency average = 354.668
	minimum = 22
	maximum = 1580
Slowest packet = 85
Flit latency average = 317.268
	minimum = 5
	maximum = 1563
Slowest flit = 27305
Fragmentation average = 49.6183
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0190599
	minimum = 0.002 (at node 180)
	maximum = 0.038 (at node 185)
Accepted packet rate average = 0.0136172
	minimum = 0.008 (at node 153)
	maximum = 0.0195 (at node 44)
Injected flit rate average = 0.340672
	minimum = 0.036 (at node 180)
	maximum = 0.6805 (at node 185)
Accepted flit rate average= 0.250331
	minimum = 0.1495 (at node 153)
	maximum = 0.351 (at node 44)
Injected packet length average = 17.8738
Accepted packet length average = 18.3834
Total in-flight flits = 35957 (0 measured)
latency change    = 0.414431
throughput change = 0.0483215
Class 0:
Packet latency average = 995.348
	minimum = 25
	maximum = 2682
Network latency average = 682.463
	minimum = 22
	maximum = 2129
Slowest packet = 2477
Flit latency average = 635.225
	minimum = 5
	maximum = 2085
Slowest flit = 48185
Fragmentation average = 58.8506
	minimum = 0
	maximum = 267
Injected packet rate average = 0.0172552
	minimum = 0 (at node 22)
	maximum = 0.04 (at node 6)
Accepted packet rate average = 0.0147135
	minimum = 0.005 (at node 190)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.310698
	minimum = 0 (at node 22)
	maximum = 0.715 (at node 6)
Accepted flit rate average= 0.26576
	minimum = 0.091 (at node 190)
	maximum = 0.461 (at node 16)
Injected packet length average = 18.006
Accepted packet length average = 18.0623
Total in-flight flits = 44871 (0 measured)
latency change    = 0.517522
throughput change = 0.0580586
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1015.43
	minimum = 24
	maximum = 3348
Network latency average = 386.352
	minimum = 22
	maximum = 963
Slowest packet = 10698
Flit latency average = 773.359
	minimum = 5
	maximum = 2484
Slowest flit = 100109
Fragmentation average = 41.8866
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0157969
	minimum = 0.001 (at node 8)
	maximum = 0.036 (at node 71)
Accepted packet rate average = 0.014901
	minimum = 0.006 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.283557
	minimum = 0.018 (at node 8)
	maximum = 0.638 (at node 71)
Accepted flit rate average= 0.269589
	minimum = 0.12 (at node 4)
	maximum = 0.442 (at node 129)
Injected packet length average = 17.9502
Accepted packet length average = 18.0919
Total in-flight flits = 47722 (40764 measured)
latency change    = 0.0197768
throughput change = 0.0141999
Class 0:
Packet latency average = 1445.44
	minimum = 24
	maximum = 4315
Network latency average = 710.294
	minimum = 22
	maximum = 1896
Slowest packet = 10698
Flit latency average = 812.363
	minimum = 5
	maximum = 3248
Slowest flit = 85427
Fragmentation average = 61.0727
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0154219
	minimum = 0.005 (at node 143)
	maximum = 0.0275 (at node 71)
Accepted packet rate average = 0.0150234
	minimum = 0.0095 (at node 4)
	maximum = 0.023 (at node 66)
Injected flit rate average = 0.277396
	minimum = 0.0915 (at node 143)
	maximum = 0.494 (at node 71)
Accepted flit rate average= 0.269984
	minimum = 0.169 (at node 4)
	maximum = 0.4145 (at node 66)
Injected packet length average = 17.9872
Accepted packet length average = 17.9709
Total in-flight flits = 47973 (47657 measured)
latency change    = 0.297496
throughput change = 0.00146613
Class 0:
Packet latency average = 1711.91
	minimum = 24
	maximum = 4953
Network latency average = 813.477
	minimum = 22
	maximum = 2762
Slowest packet = 10698
Flit latency average = 834.123
	minimum = 5
	maximum = 3775
Slowest flit = 116026
Fragmentation average = 66.131
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0155781
	minimum = 0.00633333 (at node 143)
	maximum = 0.026 (at node 71)
Accepted packet rate average = 0.0149774
	minimum = 0.0106667 (at node 2)
	maximum = 0.021 (at node 66)
Injected flit rate average = 0.280021
	minimum = 0.115 (at node 143)
	maximum = 0.462333 (at node 71)
Accepted flit rate average= 0.270418
	minimum = 0.186333 (at node 2)
	maximum = 0.371 (at node 66)
Injected packet length average = 17.9753
Accepted packet length average = 18.0551
Total in-flight flits = 50660 (50647 measured)
latency change    = 0.155652
throughput change = 0.00160502
Class 0:
Packet latency average = 1934.34
	minimum = 24
	maximum = 5928
Network latency average = 873.9
	minimum = 22
	maximum = 3429
Slowest packet = 10698
Flit latency average = 858.385
	minimum = 5
	maximum = 3775
Slowest flit = 116026
Fragmentation average = 69.4194
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0153359
	minimum = 0.00825 (at node 136)
	maximum = 0.02375 (at node 71)
Accepted packet rate average = 0.0149557
	minimum = 0.0105 (at node 2)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.275993
	minimum = 0.15275 (at node 136)
	maximum = 0.427 (at node 71)
Accepted flit rate average= 0.26978
	minimum = 0.18775 (at node 2)
	maximum = 0.36325 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 18.0386
Total in-flight flits = 49972 (49972 measured)
latency change    = 0.114991
throughput change = 0.00236658
Class 0:
Packet latency average = 2137.06
	minimum = 24
	maximum = 6674
Network latency average = 905.075
	minimum = 22
	maximum = 3429
Slowest packet = 10698
Flit latency average = 872.293
	minimum = 5
	maximum = 3775
Slowest flit = 116026
Fragmentation average = 72.0169
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0153021
	minimum = 0.008 (at node 76)
	maximum = 0.0226 (at node 71)
Accepted packet rate average = 0.0149198
	minimum = 0.0112 (at node 171)
	maximum = 0.0202 (at node 95)
Injected flit rate average = 0.275351
	minimum = 0.1444 (at node 76)
	maximum = 0.4062 (at node 71)
Accepted flit rate average= 0.26905
	minimum = 0.2064 (at node 171)
	maximum = 0.362 (at node 95)
Injected packet length average = 17.9943
Accepted packet length average = 18.0331
Total in-flight flits = 51291 (51291 measured)
latency change    = 0.0948594
throughput change = 0.00271306
Class 0:
Packet latency average = 2327.79
	minimum = 24
	maximum = 7425
Network latency average = 925.284
	minimum = 22
	maximum = 3994
Slowest packet = 10698
Flit latency average = 883.499
	minimum = 5
	maximum = 3804
Slowest flit = 288593
Fragmentation average = 73.7078
	minimum = 0
	maximum = 325
Injected packet rate average = 0.0152248
	minimum = 0.00866667 (at node 35)
	maximum = 0.0213333 (at node 71)
Accepted packet rate average = 0.0149366
	minimum = 0.0118333 (at node 171)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.273932
	minimum = 0.157667 (at node 35)
	maximum = 0.384 (at node 71)
Accepted flit rate average= 0.26926
	minimum = 0.214667 (at node 104)
	maximum = 0.3475 (at node 138)
Injected packet length average = 17.9925
Accepted packet length average = 18.0268
Total in-flight flits = 50439 (50439 measured)
latency change    = 0.0819378
throughput change = 0.000781462
Class 0:
Packet latency average = 2512.77
	minimum = 22
	maximum = 8013
Network latency average = 938.376
	minimum = 22
	maximum = 4433
Slowest packet = 10698
Flit latency average = 890.329
	minimum = 5
	maximum = 4397
Slowest flit = 301481
Fragmentation average = 74.8567
	minimum = 0
	maximum = 325
Injected packet rate average = 0.0152388
	minimum = 0.00885714 (at node 88)
	maximum = 0.0217143 (at node 71)
Accepted packet rate average = 0.0149598
	minimum = 0.0121429 (at node 104)
	maximum = 0.0187143 (at node 128)
Injected flit rate average = 0.274097
	minimum = 0.159286 (at node 88)
	maximum = 0.389714 (at node 71)
Accepted flit rate average= 0.269649
	minimum = 0.218 (at node 104)
	maximum = 0.336286 (at node 128)
Injected packet length average = 17.9867
Accepted packet length average = 18.0249
Total in-flight flits = 51049 (51049 measured)
latency change    = 0.073616
throughput change = 0.00144037
Draining all recorded packets ...
Class 0:
Remaining flits: 426551 426552 426553 426554 426555 426556 426557 426558 426559 426560 [...] (51465 flits)
Measured flits: 426551 426552 426553 426554 426555 426556 426557 426558 426559 426560 [...] (48981 flits)
Class 0:
Remaining flits: 453816 453817 453818 453819 453820 453821 453822 453823 453824 453825 [...] (50597 flits)
Measured flits: 453816 453817 453818 453819 453820 453821 453822 453823 453824 453825 [...] (44744 flits)
Class 0:
Remaining flits: 517158 517159 517160 517161 517162 517163 517164 517165 517166 517167 [...] (51533 flits)
Measured flits: 517158 517159 517160 517161 517162 517163 517164 517165 517166 517167 [...] (40990 flits)
Class 0:
Remaining flits: 518022 518023 518024 518025 518026 518027 518028 518029 518030 518031 [...] (50430 flits)
Measured flits: 518022 518023 518024 518025 518026 518027 518028 518029 518030 518031 [...] (33046 flits)
Class 0:
Remaining flits: 622692 622693 622694 622695 622696 622697 622698 622699 622700 622701 [...] (50891 flits)
Measured flits: 622692 622693 622694 622695 622696 622697 622698 622699 622700 622701 [...] (28015 flits)
Class 0:
Remaining flits: 650049 650050 650051 673794 673795 673796 673797 673798 673799 673800 [...] (49803 flits)
Measured flits: 650049 650050 650051 673794 673795 673796 673797 673798 673799 673800 [...] (22455 flits)
Class 0:
Remaining flits: 674334 674335 674336 674337 674338 674339 674340 674341 674342 674343 [...] (49701 flits)
Measured flits: 674334 674335 674336 674337 674338 674339 674340 674341 674342 674343 [...] (18008 flits)
Class 0:
Remaining flits: 737766 737767 737768 737769 737770 737771 737772 737773 737774 737775 [...] (49315 flits)
Measured flits: 737766 737767 737768 737769 737770 737771 737772 737773 737774 737775 [...] (11228 flits)
Class 0:
Remaining flits: 801349 801350 801351 801352 801353 801354 801355 801356 801357 801358 [...] (50157 flits)
Measured flits: 802242 802243 802244 802245 802246 802247 802248 802249 802250 802251 [...] (8105 flits)
Class 0:
Remaining flits: 845712 845713 845714 845715 845716 845717 845718 845719 845720 845721 [...] (49387 flits)
Measured flits: 845712 845713 845714 845715 845716 845717 845718 845719 845720 845721 [...] (5718 flits)
Class 0:
Remaining flits: 896202 896203 896204 896205 896206 896207 896208 896209 896210 896211 [...] (48220 flits)
Measured flits: 920232 920233 920234 920235 920236 920237 920238 920239 920240 920241 [...] (3686 flits)
Class 0:
Remaining flits: 896202 896203 896204 896205 896206 896207 896208 896209 896210 896211 [...] (49116 flits)
Measured flits: 995400 995401 995402 995403 995404 995405 995406 995407 995408 995409 [...] (2336 flits)
Class 0:
Remaining flits: 896202 896203 896204 896205 896206 896207 896208 896209 896210 896211 [...] (48516 flits)
Measured flits: 1016406 1016407 1016408 1016409 1016410 1016411 1016412 1016413 1016414 1016415 [...] (1340 flits)
Class 0:
Remaining flits: 896202 896203 896204 896205 896206 896207 896208 896209 896210 896211 [...] (47863 flits)
Measured flits: 1016406 1016407 1016408 1016409 1016410 1016411 1016412 1016413 1016414 1016415 [...] (1189 flits)
Class 0:
Remaining flits: 1018386 1018387 1018388 1018389 1018390 1018391 1018392 1018393 1018394 1018395 [...] (48403 flits)
Measured flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (535 flits)
Class 0:
Remaining flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (48533 flits)
Measured flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (281 flits)
Class 0:
Remaining flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (48166 flits)
Measured flits: 1125666 1125667 1125668 1125669 1125670 1125671 1125672 1125673 1125674 1125675 [...] (194 flits)
Class 0:
Remaining flits: 1238634 1238635 1238636 1238637 1238638 1238639 1238640 1238641 1238642 1238643 [...] (48856 flits)
Measured flits: 1370664 1370665 1370666 1370667 1370668 1370669 1370670 1370671 1370672 1370673 [...] (72 flits)
Class 0:
Remaining flits: 1260000 1260001 1260002 1260003 1260004 1260005 1260006 1260007 1260008 1260009 [...] (49650 flits)
Measured flits: 1370664 1370665 1370666 1370667 1370668 1370669 1370670 1370671 1370672 1370673 [...] (108 flits)
Class 0:
Remaining flits: 1260000 1260001 1260002 1260003 1260004 1260005 1260006 1260007 1260008 1260009 [...] (48382 flits)
Measured flits: 1544105 1544106 1544107 1544108 1544109 1544110 1544111 1547262 1547263 1547264 [...] (133 flits)
Class 0:
Remaining flits: 1368504 1368505 1368506 1368507 1368508 1368509 1368510 1368511 1368512 1368513 [...] (48652 flits)
Measured flits: 1547262 1547263 1547264 1547265 1547266 1547267 1547268 1547269 1547270 1547271 [...] (232 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1386162 1386163 1386164 1386165 1386166 1386167 1386168 1386169 1386170 1386171 [...] (8192 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1564254 1564255 1564256 1564257 1564258 1564259 1564260 1564261 1564262 1564263 [...] (321 flits)
Measured flits: (0 flits)
Time taken is 33840 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4490.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 22753 (1 samples)
Network latency average = 990.917 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6651 (1 samples)
Flit latency average = 926.229 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8110 (1 samples)
Fragmentation average = 80.7863 (1 samples)
	minimum = 0 (1 samples)
	maximum = 325 (1 samples)
Injected packet rate average = 0.0152388 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0217143 (1 samples)
Accepted packet rate average = 0.0149598 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.274097 (1 samples)
	minimum = 0.159286 (1 samples)
	maximum = 0.389714 (1 samples)
Accepted flit rate average = 0.269649 (1 samples)
	minimum = 0.218 (1 samples)
	maximum = 0.336286 (1 samples)
Injected packet size average = 17.9867 (1 samples)
Accepted packet size average = 18.0249 (1 samples)
Hops average = 5.06728 (1 samples)
Total run time 44.1912
