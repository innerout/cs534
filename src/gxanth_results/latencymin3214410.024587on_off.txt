BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 342.339
	minimum = 27
	maximum = 983
Network latency average = 265.941
	minimum = 23
	maximum = 923
Slowest packet = 85
Flit latency average = 198.373
	minimum = 6
	maximum = 951
Slowest flit = 3314
Fragmentation average = 141.113
	minimum = 0
	maximum = 877
Injected packet rate average = 0.0226979
	minimum = 0 (at node 2)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00951562
	minimum = 0.002 (at node 41)
	maximum = 0.018 (at node 167)
Injected flit rate average = 0.404682
	minimum = 0 (at node 2)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.196563
	minimum = 0.072 (at node 41)
	maximum = 0.344 (at node 44)
Injected packet length average = 17.8291
Accepted packet length average = 20.6568
Total in-flight flits = 40704 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.689
	minimum = 27
	maximum = 1973
Network latency average = 467.203
	minimum = 23
	maximum = 1846
Slowest packet = 99
Flit latency average = 386.879
	minimum = 6
	maximum = 1829
Slowest flit = 10997
Fragmentation average = 181.491
	minimum = 0
	maximum = 1780
Injected packet rate average = 0.0226797
	minimum = 0.003 (at node 2)
	maximum = 0.0535 (at node 37)
Accepted packet rate average = 0.0108281
	minimum = 0.0055 (at node 164)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.40632
	minimum = 0.054 (at node 2)
	maximum = 0.963 (at node 37)
Accepted flit rate average= 0.208349
	minimum = 0.104 (at node 164)
	maximum = 0.327 (at node 131)
Injected packet length average = 17.9156
Accepted packet length average = 19.2415
Total in-flight flits = 76756 (0 measured)
latency change    = 0.39696
throughput change = 0.0565708
Class 0:
Packet latency average = 1132.93
	minimum = 23
	maximum = 2977
Network latency average = 1002.71
	minimum = 23
	maximum = 2844
Slowest packet = 812
Flit latency average = 927.897
	minimum = 6
	maximum = 2827
Slowest flit = 10979
Fragmentation average = 225.027
	minimum = 0
	maximum = 1853
Injected packet rate average = 0.0229896
	minimum = 0 (at node 85)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.012349
	minimum = 0.003 (at node 180)
	maximum = 0.025 (at node 137)
Injected flit rate average = 0.413589
	minimum = 0 (at node 85)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.222917
	minimum = 0.074 (at node 180)
	maximum = 0.413 (at node 137)
Injected packet length average = 17.9903
Accepted packet length average = 18.0515
Total in-flight flits = 113426 (0 measured)
latency change    = 0.498921
throughput change = 0.0653505
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 393.161
	minimum = 31
	maximum = 1978
Network latency average = 204.672
	minimum = 27
	maximum = 989
Slowest packet = 13168
Flit latency average = 1334.68
	minimum = 6
	maximum = 3675
Slowest flit = 19331
Fragmentation average = 64.1693
	minimum = 0
	maximum = 423
Injected packet rate average = 0.0223542
	minimum = 0 (at node 56)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.0121667
	minimum = 0.004 (at node 26)
	maximum = 0.021 (at node 181)
Injected flit rate average = 0.402677
	minimum = 0 (at node 56)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.21888
	minimum = 0.083 (at node 116)
	maximum = 0.364 (at node 181)
Injected packet length average = 18.0135
Accepted packet length average = 17.9902
Total in-flight flits = 148657 (69671 measured)
latency change    = 1.8816
throughput change = 0.0184414
Class 0:
Packet latency average = 742.635
	minimum = 27
	maximum = 3266
Network latency average = 560.129
	minimum = 27
	maximum = 1956
Slowest packet = 13168
Flit latency average = 1550.62
	minimum = 6
	maximum = 4656
Slowest flit = 18215
Fragmentation average = 110.176
	minimum = 0
	maximum = 799
Injected packet rate average = 0.0219375
	minimum = 0 (at node 56)
	maximum = 0.049 (at node 161)
Accepted packet rate average = 0.0120807
	minimum = 0.0075 (at node 116)
	maximum = 0.02 (at node 99)
Injected flit rate average = 0.394742
	minimum = 0 (at node 56)
	maximum = 0.8805 (at node 161)
Accepted flit rate average= 0.217602
	minimum = 0.128 (at node 116)
	maximum = 0.3545 (at node 99)
Injected packet length average = 17.9939
Accepted packet length average = 18.0123
Total in-flight flits = 181553 (128699 measured)
latency change    = 0.470586
throughput change = 0.00587609
Class 0:
Packet latency average = 1203.47
	minimum = 27
	maximum = 4560
Network latency average = 989.022
	minimum = 25
	maximum = 2970
Slowest packet = 13168
Flit latency average = 1757.93
	minimum = 6
	maximum = 5460
Slowest flit = 38618
Fragmentation average = 138.043
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.021342
	minimum = 0.005 (at node 177)
	maximum = 0.0433333 (at node 131)
Accepted packet rate average = 0.0121441
	minimum = 0.00766667 (at node 153)
	maximum = 0.018 (at node 99)
Injected flit rate average = 0.384082
	minimum = 0.09 (at node 177)
	maximum = 0.78 (at node 131)
Accepted flit rate average= 0.217512
	minimum = 0.139333 (at node 153)
	maximum = 0.316333 (at node 99)
Injected packet length average = 17.9965
Accepted packet length average = 17.9109
Total in-flight flits = 209449 (174375 measured)
latency change    = 0.382921
throughput change = 0.000411056
Class 0:
Packet latency average = 1614.08
	minimum = 27
	maximum = 5664
Network latency average = 1374.73
	minimum = 25
	maximum = 3989
Slowest packet = 13168
Flit latency average = 1971.83
	minimum = 6
	maximum = 6228
Slowest flit = 57754
Fragmentation average = 153.954
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.0211549
	minimum = 0.0075 (at node 81)
	maximum = 0.0365 (at node 121)
Accepted packet rate average = 0.0120182
	minimum = 0.0085 (at node 153)
	maximum = 0.0165 (at node 181)
Injected flit rate average = 0.380576
	minimum = 0.135 (at node 81)
	maximum = 0.657 (at node 121)
Accepted flit rate average= 0.215592
	minimum = 0.15275 (at node 139)
	maximum = 0.2915 (at node 181)
Injected packet length average = 17.9899
Accepted packet length average = 17.9388
Total in-flight flits = 240387 (217386 measured)
latency change    = 0.254392
throughput change = 0.00890432
Class 0:
Packet latency average = 1999.69
	minimum = 27
	maximum = 6386
Network latency average = 1725.36
	minimum = 25
	maximum = 4943
Slowest packet = 13168
Flit latency average = 2179.89
	minimum = 6
	maximum = 7261
Slowest flit = 54571
Fragmentation average = 162.399
	minimum = 0
	maximum = 1851
Injected packet rate average = 0.0210677
	minimum = 0.0092 (at node 185)
	maximum = 0.0332 (at node 182)
Accepted packet rate average = 0.0119208
	minimum = 0.0082 (at node 139)
	maximum = 0.0162 (at node 103)
Injected flit rate average = 0.379043
	minimum = 0.169 (at node 185)
	maximum = 0.5952 (at node 182)
Accepted flit rate average= 0.213845
	minimum = 0.1458 (at node 139)
	maximum = 0.2938 (at node 103)
Injected packet length average = 17.9916
Accepted packet length average = 17.9387
Total in-flight flits = 272491 (257532 measured)
latency change    = 0.192838
throughput change = 0.00817255
Class 0:
Packet latency average = 2385.82
	minimum = 27
	maximum = 7341
Network latency average = 2088.33
	minimum = 23
	maximum = 5929
Slowest packet = 13168
Flit latency average = 2384.69
	minimum = 6
	maximum = 8368
Slowest flit = 18629
Fragmentation average = 169.538
	minimum = 0
	maximum = 2588
Injected packet rate average = 0.0205894
	minimum = 0.0095 (at node 81)
	maximum = 0.0315 (at node 53)
Accepted packet rate average = 0.0118012
	minimum = 0.008 (at node 139)
	maximum = 0.016 (at node 151)
Injected flit rate average = 0.370415
	minimum = 0.171 (at node 81)
	maximum = 0.568667 (at node 53)
Accepted flit rate average= 0.211352
	minimum = 0.141833 (at node 139)
	maximum = 0.283167 (at node 151)
Injected packet length average = 17.9906
Accepted packet length average = 17.9093
Total in-flight flits = 297413 (287090 measured)
latency change    = 0.161842
throughput change = 0.0117966
Class 0:
Packet latency average = 2746.67
	minimum = 23
	maximum = 8218
Network latency average = 2421.11
	minimum = 23
	maximum = 6982
Slowest packet = 13168
Flit latency average = 2605.78
	minimum = 6
	maximum = 9165
Slowest flit = 49229
Fragmentation average = 171.98
	minimum = 0
	maximum = 2588
Injected packet rate average = 0.020032
	minimum = 0.0101429 (at node 81)
	maximum = 0.0294286 (at node 71)
Accepted packet rate average = 0.0116935
	minimum = 0.00814286 (at node 190)
	maximum = 0.015 (at node 151)
Injected flit rate average = 0.360452
	minimum = 0.182571 (at node 81)
	maximum = 0.528429 (at node 71)
Accepted flit rate average= 0.209606
	minimum = 0.150143 (at node 190)
	maximum = 0.269 (at node 151)
Injected packet length average = 17.9938
Accepted packet length average = 17.9251
Total in-flight flits = 317139 (310248 measured)
latency change    = 0.131379
throughput change = 0.00832591
Draining all recorded packets ...
Class 0:
Remaining flits: 79020 79021 79022 79023 79024 79025 79026 79027 79028 79029 [...] (332318 flits)
Measured flits: 236556 236557 236558 236559 236560 236561 236562 236563 236564 236565 [...] (306077 flits)
Class 0:
Remaining flits: 79036 79037 80710 80711 95292 95293 95294 95295 95296 95297 [...] (339219 flits)
Measured flits: 236556 236557 236558 236559 236560 236561 236562 236563 236564 236565 [...] (288389 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (345650 flits)
Measured flits: 236568 236569 236570 236571 236572 236573 237222 237223 237224 237225 [...] (269393 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (349217 flits)
Measured flits: 237222 237223 237224 237225 237226 237227 237228 237229 237230 237231 [...] (247662 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (347614 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (224538 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (348005 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (202393 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (347619 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (179454 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (345350 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (158694 flits)
Class 0:
Remaining flits: 96768 96769 96770 96771 96772 96773 96774 96775 96776 96777 [...] (340112 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (138267 flits)
Class 0:
Remaining flits: 96770 96771 96772 96773 96774 96775 96776 96777 96778 96779 [...] (337430 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (118158 flits)
Class 0:
Remaining flits: 96777 96778 96779 96780 96781 96782 96783 96784 96785 143100 [...] (335462 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (101361 flits)
Class 0:
Remaining flits: 96785 143100 143101 143102 143103 143104 143105 143106 143107 143108 [...] (333703 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (85952 flits)
Class 0:
Remaining flits: 207306 207307 207308 207309 207310 207311 207312 207313 207314 207315 [...] (333478 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (74425 flits)
Class 0:
Remaining flits: 211536 211537 211538 211539 211540 211541 211542 211543 211544 211545 [...] (335546 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (62000 flits)
Class 0:
Remaining flits: 211536 211537 211538 211539 211540 211541 211542 211543 211544 211545 [...] (334403 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (52458 flits)
Class 0:
Remaining flits: 233820 233821 233822 233823 233824 233825 233826 233827 233828 233829 [...] (337347 flits)
Measured flits: 238320 238321 238322 238323 238324 238325 238326 238327 238328 238329 [...] (44042 flits)
Class 0:
Remaining flits: 327276 327277 327278 327279 327280 327281 327282 327283 327284 327285 [...] (335549 flits)
Measured flits: 327276 327277 327278 327279 327280 327281 327282 327283 327284 327285 [...] (36425 flits)
Class 0:
Remaining flits: 327276 327277 327278 327279 327280 327281 327282 327283 327284 327285 [...] (336206 flits)
Measured flits: 327276 327277 327278 327279 327280 327281 327282 327283 327284 327285 [...] (29805 flits)
Class 0:
Remaining flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (334515 flits)
Measured flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (24784 flits)
Class 0:
Remaining flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (330778 flits)
Measured flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (20428 flits)
Class 0:
Remaining flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (329098 flits)
Measured flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (16360 flits)
Class 0:
Remaining flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (328905 flits)
Measured flits: 331362 331363 331364 331365 331366 331367 331368 331369 331370 331371 [...] (13318 flits)
Class 0:
Remaining flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (329367 flits)
Measured flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (10659 flits)
Class 0:
Remaining flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (328613 flits)
Measured flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (8385 flits)
Class 0:
Remaining flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (327960 flits)
Measured flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (6399 flits)
Class 0:
Remaining flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (328124 flits)
Measured flits: 505962 505963 505964 505965 505966 505967 505968 505969 505970 505971 [...] (4589 flits)
Class 0:
Remaining flits: 505965 505966 505967 505968 505969 505970 505971 505972 505973 505974 [...] (326776 flits)
Measured flits: 505965 505966 505967 505968 505969 505970 505971 505972 505973 505974 [...] (3505 flits)
Class 0:
Remaining flits: 640746 640747 640748 640749 640750 640751 640752 640753 640754 640755 [...] (325529 flits)
Measured flits: 640746 640747 640748 640749 640750 640751 640752 640753 640754 640755 [...] (2496 flits)
Class 0:
Remaining flits: 640746 640747 640748 640749 640750 640751 640752 640753 640754 640755 [...] (324861 flits)
Measured flits: 640746 640747 640748 640749 640750 640751 640752 640753 640754 640755 [...] (1803 flits)
Class 0:
Remaining flits: 647226 647227 647228 647229 647230 647231 647232 647233 647234 647235 [...] (319830 flits)
Measured flits: 647226 647227 647228 647229 647230 647231 647232 647233 647234 647235 [...] (1274 flits)
Class 0:
Remaining flits: 762228 762229 762230 762231 762232 762233 762234 762235 762236 762237 [...] (316196 flits)
Measured flits: 853138 853139 853140 853141 853142 853143 853144 853145 865764 865765 [...] (912 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (315839 flits)
Measured flits: 865764 865765 865766 865767 865768 865769 865770 865771 865772 865773 [...] (572 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (316341 flits)
Measured flits: 865764 865765 865766 865767 865768 865769 865770 865771 865772 865773 [...] (396 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (318726 flits)
Measured flits: 1122102 1122103 1122104 1122105 1122106 1122107 1122108 1122109 1122110 1122111 [...] (322 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (321222 flits)
Measured flits: 1146582 1146583 1146584 1146585 1146586 1146587 1146588 1146589 1146590 1146591 [...] (145 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (321256 flits)
Measured flits: 1150650 1150651 1150652 1150653 1150654 1150655 1150656 1150657 1150658 1150659 [...] (108 flits)
Class 0:
Remaining flits: 817362 817363 817364 817365 817366 817367 817368 817369 817370 817371 [...] (321032 flits)
Measured flits: 1150653 1150654 1150655 1150656 1150657 1150658 1150659 1150660 1150661 1150662 [...] (69 flits)
Class 0:
Remaining flits: 1092870 1092871 1092872 1092873 1092874 1092875 1092876 1092877 1092878 1092879 [...] (318708 flits)
Measured flits: 1220418 1220419 1220420 1220421 1220422 1220423 1220424 1220425 1220426 1220427 [...] (54 flits)
Class 0:
Remaining flits: 1092873 1092874 1092875 1092876 1092877 1092878 1092879 1092880 1092881 1092882 [...] (315294 flits)
Measured flits: 1220418 1220419 1220420 1220421 1220422 1220423 1220424 1220425 1220426 1220427 [...] (54 flits)
Class 0:
Remaining flits: 1098936 1098937 1098938 1098939 1098940 1098941 1098942 1098943 1098944 1098945 [...] (314439 flits)
Measured flits: 1220418 1220419 1220420 1220421 1220422 1220423 1220424 1220425 1220426 1220427 [...] (36 flits)
Class 0:
Remaining flits: 1098936 1098937 1098938 1098939 1098940 1098941 1098942 1098943 1098944 1098945 [...] (314999 flits)
Measured flits: 1220418 1220419 1220420 1220421 1220422 1220423 1220424 1220425 1220426 1220427 [...] (36 flits)
Class 0:
Remaining flits: 1098937 1098938 1098939 1098940 1098941 1098942 1098943 1098944 1098945 1098946 [...] (316952 flits)
Measured flits: 1221260 1221261 1221262 1221263 (4 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1098937 1098938 1098939 1098940 1098941 1098942 1098943 1098944 1098945 1098946 [...] (281623 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1098952 1098953 1154033 1188391 1188392 1188393 1188394 1188395 1235053 1235054 [...] (247371 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1292580 1292581 1292582 1292583 1292584 1292585 1292586 1292587 1292588 1292589 [...] (212185 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1298614 1298615 1298616 1298617 1298618 1298619 1298620 1298621 1298622 1298623 [...] (177374 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1320354 1320355 1320356 1320357 1320358 1320359 1320360 1320361 1320362 1320363 [...] (144348 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1320354 1320355 1320356 1320357 1320358 1320359 1320360 1320361 1320362 1320363 [...] (112143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1323000 1323001 1323002 1323003 1323004 1323005 1323006 1323007 1323008 1323009 [...] (81664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1323000 1323001 1323002 1323003 1323004 1323005 1323006 1323007 1323008 1323009 [...] (55653 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1435320 1435321 1435322 1435323 1435324 1435325 1435326 1435327 1435328 1435329 [...] (33937 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1459476 1459477 1459478 1459479 1459480 1459481 1459482 1459483 1459484 1459485 [...] (18056 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1459476 1459477 1459478 1459479 1459480 1459481 1459482 1459483 1459484 1459485 [...] (7197 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1712592 1712593 1712594 1712595 1712596 1712597 1712598 1712599 1712600 1712601 [...] (3098 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1727586 1727587 1727588 1727589 1727590 1727591 1727592 1727593 1727594 1727595 [...] (1048 flits)
Measured flits: (0 flits)
Time taken is 65910 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8667.14 (1 samples)
	minimum = 23 (1 samples)
	maximum = 44354 (1 samples)
Network latency average = 6830.72 (1 samples)
	minimum = 23 (1 samples)
	maximum = 31988 (1 samples)
Flit latency average = 7895.98 (1 samples)
	minimum = 6 (1 samples)
	maximum = 35627 (1 samples)
Fragmentation average = 205.896 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3712 (1 samples)
Injected packet rate average = 0.020032 (1 samples)
	minimum = 0.0101429 (1 samples)
	maximum = 0.0294286 (1 samples)
Accepted packet rate average = 0.0116935 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.360452 (1 samples)
	minimum = 0.182571 (1 samples)
	maximum = 0.528429 (1 samples)
Accepted flit rate average = 0.209606 (1 samples)
	minimum = 0.150143 (1 samples)
	maximum = 0.269 (1 samples)
Injected packet size average = 17.9938 (1 samples)
Accepted packet size average = 17.9251 (1 samples)
Hops average = 5.0735 (1 samples)
Total run time 80.5681
