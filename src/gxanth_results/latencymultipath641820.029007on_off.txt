BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 310.272
	minimum = 22
	maximum = 954
Network latency average = 216.928
	minimum = 22
	maximum = 756
Slowest packet = 7
Flit latency average = 181.979
	minimum = 5
	maximum = 739
Slowest flit = 13283
Fragmentation average = 49.3617
	minimum = 0
	maximum = 420
Injected packet rate average = 0.0242656
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0135052
	minimum = 0.006 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.431812
	minimum = 0 (at node 100)
	maximum = 0.995 (at node 27)
Accepted flit rate average= 0.253948
	minimum = 0.108 (at node 116)
	maximum = 0.415 (at node 44)
Injected packet length average = 17.7952
Accepted packet length average = 18.8037
Total in-flight flits = 35140 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 530.537
	minimum = 22
	maximum = 1941
Network latency average = 387.923
	minimum = 22
	maximum = 1396
Slowest packet = 7
Flit latency average = 348.754
	minimum = 5
	maximum = 1453
Slowest flit = 40277
Fragmentation average = 68.6013
	minimum = 0
	maximum = 513
Injected packet rate average = 0.0241693
	minimum = 0.0065 (at node 180)
	maximum = 0.049 (at node 23)
Accepted packet rate average = 0.0142708
	minimum = 0.008 (at node 135)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.432669
	minimum = 0.1095 (at node 180)
	maximum = 0.875 (at node 23)
Accepted flit rate average= 0.264813
	minimum = 0.144 (at node 153)
	maximum = 0.4185 (at node 152)
Injected packet length average = 17.9016
Accepted packet length average = 18.5562
Total in-flight flits = 65604 (0 measured)
latency change    = 0.415173
throughput change = 0.0410275
Class 0:
Packet latency average = 1228.48
	minimum = 29
	maximum = 2833
Network latency average = 967.608
	minimum = 22
	maximum = 2116
Slowest packet = 3808
Flit latency average = 919
	minimum = 5
	maximum = 2127
Slowest flit = 69384
Fragmentation average = 105.096
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0206615
	minimum = 0.001 (at node 175)
	maximum = 0.044 (at node 7)
Accepted packet rate average = 0.0150156
	minimum = 0.005 (at node 96)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.371271
	minimum = 0.018 (at node 175)
	maximum = 0.796 (at node 7)
Accepted flit rate average= 0.271719
	minimum = 0.105 (at node 96)
	maximum = 0.426 (at node 56)
Injected packet length average = 17.9692
Accepted packet length average = 18.0957
Total in-flight flits = 85074 (0 measured)
latency change    = 0.568136
throughput change = 0.0254169
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 823.95
	minimum = 25
	maximum = 2932
Network latency average = 210.994
	minimum = 22
	maximum = 991
Slowest packet = 13325
Flit latency average = 1262.23
	minimum = 5
	maximum = 2598
Slowest flit = 114533
Fragmentation average = 20.9375
	minimum = 0
	maximum = 282
Injected packet rate average = 0.0177708
	minimum = 0 (at node 186)
	maximum = 0.046 (at node 5)
Accepted packet rate average = 0.0148594
	minimum = 0.004 (at node 4)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.320078
	minimum = 0 (at node 186)
	maximum = 0.82 (at node 5)
Accepted flit rate average= 0.267495
	minimum = 0.104 (at node 4)
	maximum = 0.475 (at node 16)
Injected packet length average = 18.0114
Accepted packet length average = 18.0018
Total in-flight flits = 95185 (55355 measured)
latency change    = 0.490965
throughput change = 0.0157908
Class 0:
Packet latency average = 1530.9
	minimum = 25
	maximum = 4317
Network latency average = 795.065
	minimum = 22
	maximum = 1929
Slowest packet = 13325
Flit latency average = 1385.96
	minimum = 5
	maximum = 3441
Slowest flit = 132698
Fragmentation average = 61.6676
	minimum = 0
	maximum = 461
Injected packet rate average = 0.0168776
	minimum = 0.001 (at node 22)
	maximum = 0.0345 (at node 5)
Accepted packet rate average = 0.0149453
	minimum = 0.0085 (at node 113)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.304109
	minimum = 0.0235 (at node 22)
	maximum = 0.6245 (at node 5)
Accepted flit rate average= 0.26937
	minimum = 0.155 (at node 113)
	maximum = 0.4095 (at node 128)
Injected packet length average = 18.0185
Accepted packet length average = 18.0237
Total in-flight flits = 98474 (88981 measured)
latency change    = 0.461786
throughput change = 0.00696069
Class 0:
Packet latency average = 2136.35
	minimum = 25
	maximum = 5546
Network latency average = 1295.2
	minimum = 22
	maximum = 2921
Slowest packet = 13325
Flit latency average = 1501.67
	minimum = 5
	maximum = 4336
Slowest flit = 140122
Fragmentation average = 85.889
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0164965
	minimum = 0.005 (at node 32)
	maximum = 0.0303333 (at node 5)
Accepted packet rate average = 0.0149757
	minimum = 0.01 (at node 79)
	maximum = 0.0233333 (at node 128)
Injected flit rate average = 0.296844
	minimum = 0.0903333 (at node 32)
	maximum = 0.546667 (at node 7)
Accepted flit rate average= 0.26984
	minimum = 0.182333 (at node 64)
	maximum = 0.408667 (at node 128)
Injected packet length average = 17.9943
Accepted packet length average = 18.0185
Total in-flight flits = 100934 (100340 measured)
latency change    = 0.283407
throughput change = 0.00174357
Class 0:
Packet latency average = 2533.44
	minimum = 23
	maximum = 6131
Network latency average = 1544.61
	minimum = 22
	maximum = 3821
Slowest packet = 13325
Flit latency average = 1581.14
	minimum = 5
	maximum = 4760
Slowest flit = 182813
Fragmentation average = 94.0068
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0162552
	minimum = 0.00825 (at node 12)
	maximum = 0.025 (at node 7)
Accepted packet rate average = 0.0149987
	minimum = 0.0105 (at node 64)
	maximum = 0.02175 (at node 128)
Injected flit rate average = 0.292452
	minimum = 0.1485 (at node 12)
	maximum = 0.4505 (at node 7)
Accepted flit rate average= 0.270488
	minimum = 0.1925 (at node 64)
	maximum = 0.3905 (at node 128)
Injected packet length average = 17.9913
Accepted packet length average = 18.0341
Total in-flight flits = 102231 (102194 measured)
latency change    = 0.156738
throughput change = 0.00239568
Class 0:
Packet latency average = 2840.17
	minimum = 23
	maximum = 7066
Network latency average = 1674.4
	minimum = 22
	maximum = 4611
Slowest packet = 13325
Flit latency average = 1642.63
	minimum = 5
	maximum = 4920
Slowest flit = 182825
Fragmentation average = 97.4846
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0159615
	minimum = 0.0094 (at node 0)
	maximum = 0.0226 (at node 5)
Accepted packet rate average = 0.0150094
	minimum = 0.0108 (at node 79)
	maximum = 0.0202 (at node 128)
Injected flit rate average = 0.287343
	minimum = 0.167 (at node 32)
	maximum = 0.4082 (at node 5)
Accepted flit rate average= 0.270396
	minimum = 0.1936 (at node 42)
	maximum = 0.362 (at node 128)
Injected packet length average = 18.0023
Accepted packet length average = 18.0151
Total in-flight flits = 101596 (101578 measured)
latency change    = 0.107998
throughput change = 0.000341898
Class 0:
Packet latency average = 3108.42
	minimum = 23
	maximum = 7612
Network latency average = 1752.63
	minimum = 22
	maximum = 4790
Slowest packet = 13325
Flit latency average = 1689.82
	minimum = 5
	maximum = 5457
Slowest flit = 236681
Fragmentation average = 99.6372
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0158845
	minimum = 0.00916667 (at node 32)
	maximum = 0.0238333 (at node 5)
Accepted packet rate average = 0.0150122
	minimum = 0.0111667 (at node 79)
	maximum = 0.0196667 (at node 128)
Injected flit rate average = 0.285863
	minimum = 0.165167 (at node 32)
	maximum = 0.429333 (at node 5)
Accepted flit rate average= 0.270092
	minimum = 0.202 (at node 79)
	maximum = 0.348333 (at node 128)
Injected packet length average = 17.9963
Accepted packet length average = 17.9916
Total in-flight flits = 103598 (103598 measured)
latency change    = 0.086295
throughput change = 0.00112487
Class 0:
Packet latency average = 3356.37
	minimum = 23
	maximum = 8401
Network latency average = 1802.77
	minimum = 22
	maximum = 4790
Slowest packet = 13325
Flit latency average = 1725.55
	minimum = 5
	maximum = 5457
Slowest flit = 236681
Fragmentation average = 100.853
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0158341
	minimum = 0.00842857 (at node 32)
	maximum = 0.0224286 (at node 5)
Accepted packet rate average = 0.0149814
	minimum = 0.0115714 (at node 79)
	maximum = 0.0191429 (at node 138)
Injected flit rate average = 0.285001
	minimum = 0.15 (at node 32)
	maximum = 0.403857 (at node 5)
Accepted flit rate average= 0.269678
	minimum = 0.208286 (at node 190)
	maximum = 0.345857 (at node 138)
Injected packet length average = 17.9992
Accepted packet length average = 18.0008
Total in-flight flits = 105865 (105865 measured)
latency change    = 0.0738761
throughput change = 0.00153586
Draining all recorded packets ...
Class 0:
Remaining flits: 455469 455470 455471 460906 460907 461106 461107 461108 461109 461110 [...] (104834 flits)
Measured flits: 455469 455470 455471 460906 460907 461106 461107 461108 461109 461110 [...] (102692 flits)
Class 0:
Remaining flits: 463716 463717 463718 463719 463720 463721 463722 463723 463724 463725 [...] (106887 flits)
Measured flits: 463716 463717 463718 463719 463720 463721 463722 463723 463724 463725 [...] (100449 flits)
Class 0:
Remaining flits: 474012 474013 474014 474015 474016 474017 474018 474019 474020 474021 [...] (107941 flits)
Measured flits: 474012 474013 474014 474015 474016 474017 474018 474019 474020 474021 [...] (97683 flits)
Class 0:
Remaining flits: 585594 585595 585596 585597 585598 585599 585600 585601 585602 585603 [...] (106568 flits)
Measured flits: 585594 585595 585596 585597 585598 585599 585600 585601 585602 585603 [...] (90335 flits)
Class 0:
Remaining flits: 585594 585595 585596 585597 585598 585599 585600 585601 585602 585603 [...] (106959 flits)
Measured flits: 585594 585595 585596 585597 585598 585599 585600 585601 585602 585603 [...] (82356 flits)
Class 0:
Remaining flits: 689102 689103 689104 689105 689106 689107 689108 689109 689110 689111 [...] (107842 flits)
Measured flits: 689102 689103 689104 689105 689106 689107 689108 689109 689110 689111 [...] (73234 flits)
Class 0:
Remaining flits: 759618 759619 759620 759621 759622 759623 759624 759625 759626 759627 [...] (108450 flits)
Measured flits: 759618 759619 759620 759621 759622 759623 759624 759625 759626 759627 [...] (65813 flits)
Class 0:
Remaining flits: 794733 794734 794735 794860 794861 800887 800888 800889 800890 800891 [...] (108068 flits)
Measured flits: 794733 794734 794735 794860 794861 800887 800888 800889 800890 800891 [...] (56875 flits)
Class 0:
Remaining flits: 806200 806201 811129 811130 811131 811132 811133 830687 830688 830689 [...] (108738 flits)
Measured flits: 806200 806201 811129 811130 811131 811132 811133 830687 830688 830689 [...] (46975 flits)
Class 0:
Remaining flits: 888156 888157 888158 888159 888160 888161 888162 888163 888164 888165 [...] (106064 flits)
Measured flits: 897226 897227 909137 909138 909139 909140 909141 909142 909143 913275 [...] (34294 flits)
Class 0:
Remaining flits: 927054 927055 927056 927057 927058 927059 927060 927061 927062 927063 [...] (105864 flits)
Measured flits: 927981 927982 927983 927984 927985 927986 927987 927988 927989 929707 [...] (25646 flits)
Class 0:
Remaining flits: 936489 936490 936491 936492 936493 936494 936495 936496 936497 936498 [...] (107226 flits)
Measured flits: 991502 991503 991504 991505 991506 991507 991508 991509 991510 991511 [...] (19594 flits)
Class 0:
Remaining flits: 938268 938269 938270 938271 938272 938273 938274 938275 938276 938277 [...] (106351 flits)
Measured flits: 1011276 1011277 1011278 1011279 1011280 1011281 1011282 1011283 1011284 1011285 [...] (14544 flits)
Class 0:
Remaining flits: 988110 988111 988112 988113 988114 988115 988116 988117 988118 988119 [...] (104569 flits)
Measured flits: 1035792 1035793 1035794 1035795 1035796 1035797 1035798 1035799 1035800 1035801 [...] (10167 flits)
Class 0:
Remaining flits: 988110 988111 988112 988113 988114 988115 988116 988117 988118 988119 [...] (105214 flits)
Measured flits: 1103717 1103718 1103719 1103720 1103721 1103722 1103723 1142496 1142497 1142498 [...] (6176 flits)
Class 0:
Remaining flits: 988110 988111 988112 988113 988114 988115 988116 988117 988118 988119 [...] (105718 flits)
Measured flits: 1161468 1161469 1161470 1161471 1161472 1161473 1161474 1161475 1161476 1161477 [...] (4469 flits)
Class 0:
Remaining flits: 988110 988111 988112 988113 988114 988115 988116 988117 988118 988119 [...] (106202 flits)
Measured flits: 1161471 1161472 1161473 1161474 1161475 1161476 1161477 1161478 1161479 1161480 [...] (3713 flits)
Class 0:
Remaining flits: 1185768 1185769 1185770 1185771 1185772 1185773 1185774 1185775 1185776 1185777 [...] (104147 flits)
Measured flits: 1281312 1281313 1281314 1281315 1281316 1281317 1281318 1281319 1281320 1281321 [...] (2789 flits)
Class 0:
Remaining flits: 1292796 1292797 1292798 1292799 1292800 1292801 1292802 1292803 1292804 1292805 [...] (105471 flits)
Measured flits: 1420848 1420849 1420850 1420851 1420852 1420853 1420854 1420855 1420856 1420857 [...] (1924 flits)
Class 0:
Remaining flits: 1375579 1375580 1375581 1375582 1375583 1375584 1375585 1375586 1375587 1375588 [...] (104198 flits)
Measured flits: 1424880 1424881 1424882 1424883 1424884 1424885 1424886 1424887 1424888 1424889 [...] (1808 flits)
Class 0:
Remaining flits: 1405242 1405243 1405244 1405245 1405246 1405247 1405248 1405249 1405250 1405251 [...] (103197 flits)
Measured flits: 1546380 1546381 1546382 1546383 1546384 1546385 1546386 1546387 1546388 1546389 [...] (1330 flits)
Class 0:
Remaining flits: 1456830 1456831 1456832 1456833 1456834 1456835 1456836 1456837 1456838 1456839 [...] (103518 flits)
Measured flits: 1621800 1621801 1621802 1621803 1621804 1621805 1621806 1621807 1621808 1621809 [...] (949 flits)
Class 0:
Remaining flits: 1464408 1464409 1464410 1464411 1464412 1464413 1464414 1464415 1464416 1464417 [...] (103447 flits)
Measured flits: 1667203 1667204 1667205 1667206 1667207 1667208 1667209 1667210 1667211 1667212 [...] (997 flits)
Class 0:
Remaining flits: 1537782 1537783 1537784 1537785 1537786 1537787 1537788 1537789 1537790 1537791 [...] (100454 flits)
Measured flits: 1721628 1721629 1721630 1721631 1721632 1721633 1721634 1721635 1721636 1721637 [...] (837 flits)
Class 0:
Remaining flits: 1539702 1539703 1539704 1539705 1539706 1539707 1539708 1539709 1539710 1539711 [...] (102056 flits)
Measured flits: 1779946 1779947 1782337 1782338 1782339 1782340 1782341 1792509 1792510 1792511 [...] (720 flits)
Class 0:
Remaining flits: 1539702 1539703 1539704 1539705 1539706 1539707 1539708 1539709 1539710 1539711 [...] (102809 flits)
Measured flits: 1797300 1797301 1797302 1797303 1797304 1797305 1797306 1797307 1797308 1797309 [...] (493 flits)
Class 0:
Remaining flits: 1652184 1652185 1652186 1652187 1652188 1652189 1652190 1652191 1652192 1652193 [...] (101887 flits)
Measured flits: 1881180 1881181 1881182 1881183 1881184 1881185 1881186 1881187 1881188 1881189 [...] (294 flits)
Class 0:
Remaining flits: 1654326 1654327 1654328 1654329 1654330 1654331 1654332 1654333 1654334 1654335 [...] (104289 flits)
Measured flits: 1920855 1920856 1920857 1920858 1920859 1920860 1920861 1920862 1920863 1920864 [...] (182 flits)
Class 0:
Remaining flits: 1745745 1745746 1745747 1752423 1752424 1752425 1756368 1756369 1756370 1756371 [...] (103918 flits)
Measured flits: 2023150 2023151 2023152 2023153 2023154 2023155 2023156 2023157 2023158 2023159 [...] (50 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1785672 1785673 1785674 1785675 1785676 1785677 1785678 1785679 1785680 1785681 [...] (55968 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1876284 1876285 1876286 1876287 1876288 1876289 1876290 1876291 1876292 1876293 [...] (15992 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888808 1888809 1888810 1888811 1900098 1900099 1900100 1900101 1900102 1900103 [...] (2071 flits)
Measured flits: (0 flits)
Time taken is 43551 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6779.07 (1 samples)
	minimum = 23 (1 samples)
	maximum = 30036 (1 samples)
Network latency average = 2009.19 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6842 (1 samples)
Flit latency average = 1971.68 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9631 (1 samples)
Fragmentation average = 118.799 (1 samples)
	minimum = 0 (1 samples)
	maximum = 679 (1 samples)
Injected packet rate average = 0.0158341 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0224286 (1 samples)
Accepted packet rate average = 0.0149814 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.285001 (1 samples)
	minimum = 0.15 (1 samples)
	maximum = 0.403857 (1 samples)
Accepted flit rate average = 0.269678 (1 samples)
	minimum = 0.208286 (1 samples)
	maximum = 0.345857 (1 samples)
Injected packet size average = 17.9992 (1 samples)
Accepted packet size average = 18.0008 (1 samples)
Hops average = 5.05565 (1 samples)
Total run time 86.0654
