BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 162.548
	minimum = 22
	maximum = 661
Network latency average = 151.549
	minimum = 22
	maximum = 604
Slowest packet = 1031
Flit latency average = 124.816
	minimum = 5
	maximum = 620
Slowest flit = 22151
Fragmentation average = 19.6483
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0165938
	minimum = 0.007 (at node 56)
	maximum = 0.027 (at node 74)
Accepted packet rate average = 0.0129271
	minimum = 0.002 (at node 174)
	maximum = 0.022 (at node 76)
Injected flit rate average = 0.295406
	minimum = 0.122 (at node 56)
	maximum = 0.485 (at node 117)
Accepted flit rate average= 0.237411
	minimum = 0.036 (at node 174)
	maximum = 0.407 (at node 76)
Injected packet length average = 17.8023
Accepted packet length average = 18.3654
Total in-flight flits = 12503 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 261.11
	minimum = 22
	maximum = 1164
Network latency average = 211.067
	minimum = 22
	maximum = 1119
Slowest packet = 1360
Flit latency average = 181.545
	minimum = 5
	maximum = 1087
Slowest flit = 27629
Fragmentation average = 20.7817
	minimum = 0
	maximum = 110
Injected packet rate average = 0.0156172
	minimum = 0.009 (at node 87)
	maximum = 0.023 (at node 9)
Accepted packet rate average = 0.0134349
	minimum = 0.008 (at node 153)
	maximum = 0.02 (at node 22)
Injected flit rate average = 0.279427
	minimum = 0.1615 (at node 108)
	maximum = 0.4055 (at node 9)
Accepted flit rate average= 0.244219
	minimum = 0.144 (at node 153)
	maximum = 0.36 (at node 22)
Injected packet length average = 17.8923
Accepted packet length average = 18.1779
Total in-flight flits = 15516 (0 measured)
latency change    = 0.377474
throughput change = 0.0278737
Class 0:
Packet latency average = 551.754
	minimum = 22
	maximum = 2187
Network latency average = 313.868
	minimum = 22
	maximum = 1932
Slowest packet = 2613
Flit latency average = 281.616
	minimum = 5
	maximum = 1915
Slowest flit = 55025
Fragmentation average = 21.6178
	minimum = 0
	maximum = 111
Injected packet rate average = 0.0142344
	minimum = 0.003 (at node 103)
	maximum = 0.027 (at node 26)
Accepted packet rate average = 0.0139688
	minimum = 0.007 (at node 21)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.255719
	minimum = 0.054 (at node 103)
	maximum = 0.477 (at node 26)
Accepted flit rate average= 0.252
	minimum = 0.126 (at node 22)
	maximum = 0.45 (at node 16)
Injected packet length average = 17.9649
Accepted packet length average = 18.0403
Total in-flight flits = 16488 (0 measured)
latency change    = 0.526763
throughput change = 0.030878
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 729.25
	minimum = 28
	maximum = 2457
Network latency average = 262.137
	minimum = 22
	maximum = 893
Slowest packet = 8837
Flit latency average = 303.144
	minimum = 5
	maximum = 1666
Slowest flit = 111364
Fragmentation average = 20.1049
	minimum = 0
	maximum = 95
Injected packet rate average = 0.013875
	minimum = 0.001 (at node 81)
	maximum = 0.025 (at node 41)
Accepted packet rate average = 0.0136615
	minimum = 0.006 (at node 48)
	maximum = 0.024 (at node 123)
Injected flit rate average = 0.250167
	minimum = 0.018 (at node 81)
	maximum = 0.45 (at node 41)
Accepted flit rate average= 0.245922
	minimum = 0.096 (at node 48)
	maximum = 0.411 (at node 123)
Injected packet length average = 18.03
Accepted packet length average = 18.0011
Total in-flight flits = 17457 (17316 measured)
latency change    = 0.243395
throughput change = 0.0247157
Class 0:
Packet latency average = 901.765
	minimum = 22
	maximum = 3004
Network latency average = 317.251
	minimum = 22
	maximum = 1395
Slowest packet = 8837
Flit latency average = 311.436
	minimum = 5
	maximum = 1666
Slowest flit = 111364
Fragmentation average = 21.7877
	minimum = 0
	maximum = 102
Injected packet rate average = 0.013974
	minimum = 0.005 (at node 81)
	maximum = 0.0215 (at node 129)
Accepted packet rate average = 0.0138568
	minimum = 0.0065 (at node 4)
	maximum = 0.021 (at node 123)
Injected flit rate average = 0.251688
	minimum = 0.09 (at node 81)
	maximum = 0.391 (at node 129)
Accepted flit rate average= 0.249232
	minimum = 0.117 (at node 4)
	maximum = 0.3675 (at node 123)
Injected packet length average = 18.0112
Accepted packet length average = 17.9863
Total in-flight flits = 17767 (17767 measured)
latency change    = 0.191308
throughput change = 0.0132804
Class 0:
Packet latency average = 1035.13
	minimum = 22
	maximum = 3074
Network latency average = 331.611
	minimum = 22
	maximum = 1395
Slowest packet = 8837
Flit latency average = 313.813
	minimum = 5
	maximum = 1666
Slowest flit = 111364
Fragmentation average = 22.0713
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0139583
	minimum = 0.00566667 (at node 168)
	maximum = 0.0213333 (at node 123)
Accepted packet rate average = 0.0138698
	minimum = 0.009 (at node 36)
	maximum = 0.0196667 (at node 129)
Injected flit rate average = 0.25133
	minimum = 0.102 (at node 168)
	maximum = 0.384 (at node 123)
Accepted flit rate average= 0.249672
	minimum = 0.162 (at node 36)
	maximum = 0.352 (at node 129)
Injected packet length average = 18.0057
Accepted packet length average = 18.0011
Total in-flight flits = 17613 (17613 measured)
latency change    = 0.128839
throughput change = 0.00176273
Class 0:
Packet latency average = 1152.75
	minimum = 22
	maximum = 3392
Network latency average = 338.087
	minimum = 22
	maximum = 1794
Slowest packet = 8837
Flit latency average = 315.281
	minimum = 5
	maximum = 1777
Slowest flit = 241505
Fragmentation average = 22.3599
	minimum = 0
	maximum = 116
Injected packet rate average = 0.013987
	minimum = 0.00525 (at node 168)
	maximum = 0.0205 (at node 123)
Accepted packet rate average = 0.0138672
	minimum = 0.009 (at node 36)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.251638
	minimum = 0.0945 (at node 168)
	maximum = 0.36725 (at node 151)
Accepted flit rate average= 0.249599
	minimum = 0.162 (at node 36)
	maximum = 0.34075 (at node 129)
Injected packet length average = 17.9909
Accepted packet length average = 17.9992
Total in-flight flits = 18350 (18350 measured)
latency change    = 0.102035
throughput change = 0.000292135
Class 0:
Packet latency average = 1259.32
	minimum = 22
	maximum = 4124
Network latency average = 342.612
	minimum = 22
	maximum = 1794
Slowest packet = 8837
Flit latency average = 317.108
	minimum = 5
	maximum = 1777
Slowest flit = 241505
Fragmentation average = 22.3863
	minimum = 0
	maximum = 122
Injected packet rate average = 0.0139906
	minimum = 0.006 (at node 168)
	maximum = 0.0204 (at node 123)
Accepted packet rate average = 0.0138969
	minimum = 0.0102 (at node 89)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.251916
	minimum = 0.1078 (at node 168)
	maximum = 0.3654 (at node 123)
Accepted flit rate average= 0.250085
	minimum = 0.1836 (at node 89)
	maximum = 0.329 (at node 128)
Injected packet length average = 18.006
Accepted packet length average = 17.9958
Total in-flight flits = 18614 (18614 measured)
latency change    = 0.0846204
throughput change = 0.00194517
Class 0:
Packet latency average = 1376.76
	minimum = 22
	maximum = 4386
Network latency average = 345.698
	minimum = 22
	maximum = 1794
Slowest packet = 8837
Flit latency average = 318.111
	minimum = 5
	maximum = 1777
Slowest flit = 241505
Fragmentation average = 22.3433
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0139653
	minimum = 0.00716667 (at node 168)
	maximum = 0.0193333 (at node 151)
Accepted packet rate average = 0.013888
	minimum = 0.0111667 (at node 42)
	maximum = 0.0181667 (at node 138)
Injected flit rate average = 0.251397
	minimum = 0.128833 (at node 168)
	maximum = 0.350333 (at node 151)
Accepted flit rate average= 0.249909
	minimum = 0.199667 (at node 42)
	maximum = 0.327 (at node 138)
Injected packet length average = 18.0016
Accepted packet length average = 17.9946
Total in-flight flits = 18501 (18501 measured)
latency change    = 0.0853067
throughput change = 0.000706508
Class 0:
Packet latency average = 1493.5
	minimum = 22
	maximum = 4488
Network latency average = 349.445
	minimum = 22
	maximum = 1845
Slowest packet = 8837
Flit latency average = 320.455
	minimum = 5
	maximum = 1828
Slowest flit = 355500
Fragmentation average = 22.5409
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0139881
	minimum = 0.00828571 (at node 117)
	maximum = 0.0184286 (at node 151)
Accepted packet rate average = 0.0139055
	minimum = 0.0105714 (at node 79)
	maximum = 0.0178571 (at node 138)
Injected flit rate average = 0.251802
	minimum = 0.149143 (at node 117)
	maximum = 0.333714 (at node 151)
Accepted flit rate average= 0.250348
	minimum = 0.190857 (at node 79)
	maximum = 0.323429 (at node 138)
Injected packet length average = 18.0012
Accepted packet length average = 18.0035
Total in-flight flits = 18816 (18816 measured)
latency change    = 0.078165
throughput change = 0.001755
Draining all recorded packets ...
Class 0:
Remaining flits: 473508 473509 473510 473511 473512 473513 473514 473515 473516 473517 [...] (18650 flits)
Measured flits: 473508 473509 473510 473511 473512 473513 473514 473515 473516 473517 [...] (17127 flits)
Class 0:
Remaining flits: 532494 532495 532496 532497 532498 532499 532500 532501 532502 532503 [...] (19095 flits)
Measured flits: 532494 532495 532496 532497 532498 532499 532500 532501 532502 532503 [...] (13614 flits)
Class 0:
Remaining flits: 581616 581617 581618 581619 581620 581621 581622 581623 581624 581625 [...] (18380 flits)
Measured flits: 581616 581617 581618 581619 581620 581621 581622 581623 581624 581625 [...] (8504 flits)
Class 0:
Remaining flits: 599580 599581 599582 599583 599584 599585 599586 599587 599588 599589 [...] (18872 flits)
Measured flits: 623772 623773 623774 623775 623776 623777 623778 623779 623780 623781 [...] (3633 flits)
Class 0:
Remaining flits: 680796 680797 680798 680799 680800 680801 680802 680803 680804 680805 [...] (18615 flits)
Measured flits: 697230 697231 697232 697233 697234 697235 697236 697237 697238 697239 [...] (1402 flits)
Class 0:
Remaining flits: 724410 724411 724412 724413 724414 724415 724416 724417 724418 724419 [...] (18436 flits)
Measured flits: 736626 736627 736628 736629 736630 736631 744912 744913 744914 744915 [...] (423 flits)
Class 0:
Remaining flits: 768024 768025 768026 768027 768028 768029 768030 768031 768032 768033 [...] (18593 flits)
Measured flits: 796878 796879 796880 796881 796882 796883 796884 796885 796886 796887 [...] (102 flits)
Draining remaining packets ...
Time taken is 18717 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2017.44 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8093 (1 samples)
Network latency average = 358.855 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2056 (1 samples)
Flit latency average = 325.952 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2039 (1 samples)
Fragmentation average = 22.8285 (1 samples)
	minimum = 0 (1 samples)
	maximum = 133 (1 samples)
Injected packet rate average = 0.0139881 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0184286 (1 samples)
Accepted packet rate average = 0.0139055 (1 samples)
	minimum = 0.0105714 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.251802 (1 samples)
	minimum = 0.149143 (1 samples)
	maximum = 0.333714 (1 samples)
Accepted flit rate average = 0.250348 (1 samples)
	minimum = 0.190857 (1 samples)
	maximum = 0.323429 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 18.0035 (1 samples)
Hops average = 5.07487 (1 samples)
Total run time 17.0628
