BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 199.42
	minimum = 22
	maximum = 764
Network latency average = 194.715
	minimum = 22
	maximum = 706
Slowest packet = 940
Flit latency average = 161.709
	minimum = 5
	maximum = 689
Slowest flit = 16937
Fragmentation average = 40.8567
	minimum = 0
	maximum = 265
Injected packet rate average = 0.020125
	minimum = 0.008 (at node 187)
	maximum = 0.036 (at node 17)
Accepted packet rate average = 0.0133021
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.358672
	minimum = 0.144 (at node 187)
	maximum = 0.64 (at node 17)
Accepted flit rate average= 0.248625
	minimum = 0.095 (at node 174)
	maximum = 0.444 (at node 44)
Injected packet length average = 17.8222
Accepted packet length average = 18.6907
Total in-flight flits = 21834 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 332.196
	minimum = 22
	maximum = 1225
Network latency average = 326.893
	minimum = 22
	maximum = 1197
Slowest packet = 2857
Flit latency average = 289.865
	minimum = 5
	maximum = 1182
Slowest flit = 56154
Fragmentation average = 50.8182
	minimum = 0
	maximum = 386
Injected packet rate average = 0.0199297
	minimum = 0.013 (at node 58)
	maximum = 0.028 (at node 17)
Accepted packet rate average = 0.0141068
	minimum = 0.0085 (at node 10)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.357305
	minimum = 0.234 (at node 58)
	maximum = 0.504 (at node 17)
Accepted flit rate average= 0.260128
	minimum = 0.157 (at node 153)
	maximum = 0.391 (at node 152)
Injected packet length average = 17.9283
Accepted packet length average = 18.4399
Total in-flight flits = 37919 (0 measured)
latency change    = 0.399693
throughput change = 0.0442191
Class 0:
Packet latency average = 680.199
	minimum = 22
	maximum = 1636
Network latency average = 671.553
	minimum = 22
	maximum = 1636
Slowest packet = 5130
Flit latency average = 629.32
	minimum = 5
	maximum = 1619
Slowest flit = 92357
Fragmentation average = 69.8274
	minimum = 0
	maximum = 385
Injected packet rate average = 0.0197031
	minimum = 0.009 (at node 104)
	maximum = 0.031 (at node 4)
Accepted packet rate average = 0.0152083
	minimum = 0.007 (at node 26)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.35351
	minimum = 0.164 (at node 104)
	maximum = 0.545 (at node 4)
Accepted flit rate average= 0.275276
	minimum = 0.129 (at node 150)
	maximum = 0.507 (at node 16)
Injected packet length average = 17.9418
Accepted packet length average = 18.1003
Total in-flight flits = 53232 (0 measured)
latency change    = 0.511619
throughput change = 0.05503
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 337.496
	minimum = 22
	maximum = 1491
Network latency average = 308.563
	minimum = 22
	maximum = 977
Slowest packet = 11472
Flit latency average = 836.377
	minimum = 5
	maximum = 2052
Slowest flit = 102743
Fragmentation average = 27.9361
	minimum = 0
	maximum = 322
Injected packet rate average = 0.0196823
	minimum = 0.004 (at node 60)
	maximum = 0.033 (at node 38)
Accepted packet rate average = 0.0155156
	minimum = 0.007 (at node 4)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.354562
	minimum = 0.064 (at node 60)
	maximum = 0.6 (at node 38)
Accepted flit rate average= 0.278906
	minimum = 0.103 (at node 132)
	maximum = 0.549 (at node 129)
Injected packet length average = 18.0143
Accepted packet length average = 17.9758
Total in-flight flits = 67776 (57065 measured)
latency change    = 1.01543
throughput change = 0.0130159
Class 0:
Packet latency average = 898.142
	minimum = 22
	maximum = 2319
Network latency average = 853.271
	minimum = 22
	maximum = 1980
Slowest packet = 11472
Flit latency average = 947.174
	minimum = 5
	maximum = 2475
Slowest flit = 153539
Fragmentation average = 70.9834
	minimum = 0
	maximum = 769
Injected packet rate average = 0.0190677
	minimum = 0.008 (at node 152)
	maximum = 0.027 (at node 38)
Accepted packet rate average = 0.015362
	minimum = 0.0095 (at node 122)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.34332
	minimum = 0.143 (at node 152)
	maximum = 0.4855 (at node 38)
Accepted flit rate average= 0.277388
	minimum = 0.1775 (at node 122)
	maximum = 0.4335 (at node 129)
Injected packet length average = 18.0053
Accepted packet length average = 18.0568
Total in-flight flits = 78799 (78393 measured)
latency change    = 0.624229
throughput change = 0.0054733
Class 0:
Packet latency average = 1144.03
	minimum = 22
	maximum = 3156
Network latency average = 1081.56
	minimum = 22
	maximum = 2795
Slowest packet = 11472
Flit latency average = 1051.34
	minimum = 5
	maximum = 2946
Slowest flit = 190493
Fragmentation average = 80.7502
	minimum = 0
	maximum = 769
Injected packet rate average = 0.0186701
	minimum = 0.009 (at node 184)
	maximum = 0.026 (at node 66)
Accepted packet rate average = 0.0153142
	minimum = 0.011 (at node 89)
	maximum = 0.0213333 (at node 103)
Injected flit rate average = 0.335943
	minimum = 0.16 (at node 184)
	maximum = 0.468 (at node 66)
Accepted flit rate average= 0.276366
	minimum = 0.19 (at node 89)
	maximum = 0.379667 (at node 103)
Injected packet length average = 17.9936
Accepted packet length average = 18.0464
Total in-flight flits = 88013 (87977 measured)
latency change    = 0.214929
throughput change = 0.00369691
Draining remaining packets ...
Class 0:
Remaining flits: 243351 243352 243353 243354 243355 243356 243357 243358 243359 259686 [...] (40725 flits)
Measured flits: 243351 243352 243353 243354 243355 243356 243357 243358 243359 259686 [...] (40725 flits)
Class 0:
Remaining flits: 292004 292005 292006 292007 292008 292009 292010 292011 292012 292013 [...] (2028 flits)
Measured flits: 292004 292005 292006 292007 292008 292009 292010 292011 292012 292013 [...] (2028 flits)
Time taken is 8438 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1519.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3756 (1 samples)
Network latency average = 1430.61 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3756 (1 samples)
Flit latency average = 1303.37 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3863 (1 samples)
Fragmentation average = 90.7416 (1 samples)
	minimum = 0 (1 samples)
	maximum = 769 (1 samples)
Injected packet rate average = 0.0186701 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0153142 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.335943 (1 samples)
	minimum = 0.16 (1 samples)
	maximum = 0.468 (1 samples)
Accepted flit rate average = 0.276366 (1 samples)
	minimum = 0.19 (1 samples)
	maximum = 0.379667 (1 samples)
Injected packet size average = 17.9936 (1 samples)
Accepted packet size average = 18.0464 (1 samples)
Hops average = 5.1083 (1 samples)
Total run time 10.0291
