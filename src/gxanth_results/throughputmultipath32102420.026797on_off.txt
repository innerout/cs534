BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.429
	minimum = 22
	maximum = 967
Network latency average = 220.817
	minimum = 22
	maximum = 764
Slowest packet = 8
Flit latency average = 190.858
	minimum = 5
	maximum = 751
Slowest flit = 18159
Fragmentation average = 41.0479
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0133802
	minimum = 0.006 (at node 64)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.250219
	minimum = 0.119 (at node 64)
	maximum = 0.396 (at node 44)
Injected packet length average = 17.8353
Accepted packet length average = 18.7007
Total in-flight flits = 35658 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.595
	minimum = 22
	maximum = 1940
Network latency average = 399.08
	minimum = 22
	maximum = 1615
Slowest packet = 8
Flit latency average = 366.501
	minimum = 5
	maximum = 1598
Slowest flit = 31229
Fragmentation average = 52.4594
	minimum = 0
	maximum = 394
Injected packet rate average = 0.024125
	minimum = 0 (at node 30)
	maximum = 0.0545 (at node 91)
Accepted packet rate average = 0.0141927
	minimum = 0.007 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.432654
	minimum = 0 (at node 30)
	maximum = 0.974 (at node 91)
Accepted flit rate average= 0.261682
	minimum = 0.132 (at node 30)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.9338
Accepted packet length average = 18.4378
Total in-flight flits = 66266 (0 measured)
latency change    = 0.390766
throughput change = 0.0438071
Class 0:
Packet latency average = 1058.38
	minimum = 25
	maximum = 2906
Network latency average = 891.707
	minimum = 22
	maximum = 2263
Slowest packet = 4621
Flit latency average = 857.749
	minimum = 5
	maximum = 2322
Slowest flit = 54691
Fragmentation average = 74.8165
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0251198
	minimum = 0 (at node 46)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0153281
	minimum = 0.005 (at node 146)
	maximum = 0.026 (at node 63)
Injected flit rate average = 0.451083
	minimum = 0 (at node 46)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.277792
	minimum = 0.093 (at node 146)
	maximum = 0.494 (at node 183)
Injected packet length average = 17.9573
Accepted packet length average = 18.123
Total in-flight flits = 99744 (0 measured)
latency change    = 0.501509
throughput change = 0.0579909
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 260.663
	minimum = 27
	maximum = 1419
Network latency average = 91.5442
	minimum = 22
	maximum = 972
Slowest packet = 14089
Flit latency average = 1209.92
	minimum = 5
	maximum = 2846
Slowest flit = 95268
Fragmentation average = 8.54884
	minimum = 0
	maximum = 183
Injected packet rate average = 0.0257135
	minimum = 0 (at node 93)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0157188
	minimum = 0.006 (at node 74)
	maximum = 0.031 (at node 59)
Injected flit rate average = 0.463339
	minimum = 0.013 (at node 96)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.283214
	minimum = 0.112 (at node 74)
	maximum = 0.56 (at node 59)
Injected packet length average = 18.0192
Accepted packet length average = 18.0176
Total in-flight flits = 134233 (80855 measured)
latency change    = 3.06036
throughput change = 0.0191441
Class 0:
Packet latency average = 786.994
	minimum = 22
	maximum = 2438
Network latency average = 620.414
	minimum = 22
	maximum = 1990
Slowest packet = 14089
Flit latency average = 1369
	minimum = 5
	maximum = 3552
Slowest flit = 121573
Fragmentation average = 32.2391
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0262682
	minimum = 0.0045 (at node 190)
	maximum = 0.0555 (at node 53)
Accepted packet rate average = 0.0158516
	minimum = 0.009 (at node 4)
	maximum = 0.0235 (at node 59)
Injected flit rate average = 0.472961
	minimum = 0.087 (at node 190)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.285234
	minimum = 0.162 (at node 4)
	maximum = 0.422 (at node 123)
Injected packet length average = 18.0051
Accepted packet length average = 17.9941
Total in-flight flits = 171780 (152084 measured)
latency change    = 0.668787
throughput change = 0.00708482
Class 0:
Packet latency average = 1349.63
	minimum = 22
	maximum = 4197
Network latency average = 1187.01
	minimum = 22
	maximum = 2984
Slowest packet = 14089
Flit latency average = 1545.76
	minimum = 5
	maximum = 4217
Slowest flit = 135377
Fragmentation average = 51.6658
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0263455
	minimum = 0.00566667 (at node 48)
	maximum = 0.0553333 (at node 53)
Accepted packet rate average = 0.0157708
	minimum = 0.0103333 (at node 139)
	maximum = 0.0216667 (at node 157)
Injected flit rate average = 0.474405
	minimum = 0.102 (at node 48)
	maximum = 1 (at node 123)
Accepted flit rate average= 0.284519
	minimum = 0.189667 (at node 139)
	maximum = 0.388 (at node 157)
Injected packet length average = 18.0071
Accepted packet length average = 18.0408
Total in-flight flits = 209011 (203208 measured)
latency change    = 0.416884
throughput change = 0.00251399
Draining remaining packets ...
Class 0:
Remaining flits: 174492 174493 174494 174495 174496 174497 174498 174499 174500 174501 [...] (161474 flits)
Measured flits: 254270 254271 254272 254273 254274 254275 254276 254277 254278 254279 [...] (160348 flits)
Class 0:
Remaining flits: 208278 208279 208280 208281 208282 208283 208284 208285 208286 208287 [...] (114198 flits)
Measured flits: 254596 254597 254598 254599 254600 254601 254602 254603 254604 254605 [...] (114035 flits)
Class 0:
Remaining flits: 268181 274859 275379 275380 275381 275865 275866 275867 280368 280369 [...] (68893 flits)
Measured flits: 268181 274859 275379 275380 275381 275865 275866 275867 280368 280369 [...] (68893 flits)
Class 0:
Remaining flits: 318363 318364 318365 319912 319913 321696 321697 321698 321699 321700 [...] (31771 flits)
Measured flits: 318363 318364 318365 319912 319913 321696 321697 321698 321699 321700 [...] (31771 flits)
Class 0:
Remaining flits: 342690 342691 342692 342693 342694 342695 342696 342697 342698 342699 [...] (12760 flits)
Measured flits: 342690 342691 342692 342693 342694 342695 342696 342697 342698 342699 [...] (12760 flits)
Class 0:
Remaining flits: 375275 375276 375277 375278 375279 375280 375281 377136 377137 377138 [...] (4665 flits)
Measured flits: 375275 375276 375277 375278 375279 375280 375281 377136 377137 377138 [...] (4665 flits)
Class 0:
Remaining flits: 413591 413592 413593 413594 413595 413596 413597 413598 413599 413600 [...] (1323 flits)
Measured flits: 413591 413592 413593 413594 413595 413596 413597 413598 413599 413600 [...] (1323 flits)
Class 0:
Remaining flits: 480204 480205 480206 480207 480208 480209 480210 480211 480212 480213 [...] (324 flits)
Measured flits: 480204 480205 480206 480207 480208 480209 480210 480211 480212 480213 [...] (324 flits)
Time taken is 14329 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3297.66 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9061 (1 samples)
Network latency average = 3097.59 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8588 (1 samples)
Flit latency average = 2740.12 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8571 (1 samples)
Fragmentation average = 94.0368 (1 samples)
	minimum = 0 (1 samples)
	maximum = 478 (1 samples)
Injected packet rate average = 0.0263455 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0553333 (1 samples)
Accepted packet rate average = 0.0157708 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.474405 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.284519 (1 samples)
	minimum = 0.189667 (1 samples)
	maximum = 0.388 (1 samples)
Injected packet size average = 18.0071 (1 samples)
Accepted packet size average = 18.0408 (1 samples)
Hops average = 5.07539 (1 samples)
Total run time 9.46496
