BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 204.88
	minimum = 22
	maximum = 766
Network latency average = 149.565
	minimum = 22
	maximum = 554
Slowest packet = 63
Flit latency average = 120.555
	minimum = 5
	maximum = 537
Slowest flit = 22967
Fragmentation average = 23.7979
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.0114948
	minimum = 0.004 (at node 41)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.210776
	minimum = 0.081 (at node 41)
	maximum = 0.376 (at node 44)
Injected packet length average = 17.8695
Accepted packet length average = 18.3367
Total in-flight flits = 7681 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 236.674
	minimum = 22
	maximum = 1304
Network latency average = 177.519
	minimum = 22
	maximum = 1100
Slowest packet = 63
Flit latency average = 148.955
	minimum = 5
	maximum = 1083
Slowest flit = 41991
Fragmentation average = 25.0114
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.0116094
	minimum = 0.0065 (at node 79)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.212013
	minimum = 0.119 (at node 135)
	maximum = 0.317 (at node 166)
Injected packet length average = 17.9095
Accepted packet length average = 18.2622
Total in-flight flits = 9667 (0 measured)
latency change    = 0.134334
throughput change = 0.00583445
Class 0:
Packet latency average = 320.635
	minimum = 25
	maximum = 1511
Network latency average = 257.164
	minimum = 22
	maximum = 1294
Slowest packet = 2657
Flit latency average = 226.04
	minimum = 5
	maximum = 1277
Slowest flit = 52001
Fragmentation average = 27.9035
	minimum = 0
	maximum = 381
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0126771
	minimum = 0.006 (at node 118)
	maximum = 0.022 (at node 24)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.226849
	minimum = 0.101 (at node 184)
	maximum = 0.406 (at node 34)
Injected packet length average = 18.0569
Accepted packet length average = 17.8944
Total in-flight flits = 11040 (0 measured)
latency change    = 0.261859
throughput change = 0.0654001
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 275.732
	minimum = 22
	maximum = 972
Network latency average = 213.666
	minimum = 22
	maximum = 784
Slowest packet = 7572
Flit latency average = 239.615
	minimum = 5
	maximum = 1278
Slowest flit = 122896
Fragmentation average = 27.1889
	minimum = 0
	maximum = 310
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0127969
	minimum = 0.004 (at node 68)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.23175
	minimum = 0.065 (at node 68)
	maximum = 0.432 (at node 16)
Injected packet length average = 17.981
Accepted packet length average = 18.1099
Total in-flight flits = 12912 (12223 measured)
latency change    = 0.162851
throughput change = 0.021148
Class 0:
Packet latency average = 330.245
	minimum = 22
	maximum = 2165
Network latency average = 263.951
	minimum = 22
	maximum = 1935
Slowest packet = 7619
Flit latency average = 264.201
	minimum = 5
	maximum = 1918
Slowest flit = 138545
Fragmentation average = 29.5722
	minimum = 0
	maximum = 310
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.0129297
	minimum = 0.006 (at node 4)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.233797
	minimum = 0.102 (at node 4)
	maximum = 0.36 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.0822
Total in-flight flits = 15618 (15618 measured)
latency change    = 0.16507
throughput change = 0.00875493
Class 0:
Packet latency average = 369.467
	minimum = 22
	maximum = 2291
Network latency average = 301.591
	minimum = 22
	maximum = 2059
Slowest packet = 7619
Flit latency average = 287.613
	minimum = 5
	maximum = 2042
Slowest flit = 140993
Fragmentation average = 29.7972
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0129566
	minimum = 0.008 (at node 86)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.233019
	minimum = 0.144 (at node 86)
	maximum = 0.338667 (at node 128)
Injected packet length average = 17.9913
Accepted packet length average = 17.9846
Total in-flight flits = 13351 (13351 measured)
latency change    = 0.106156
throughput change = 0.00333783
Draining remaining packets ...
Class 0:
Remaining flits: 257533 257534 257535 257536 257537 257538 257539 257540 257541 257542 [...] (216 flits)
Measured flits: 257533 257534 257535 257536 257537 257538 257539 257540 257541 257542 [...] (216 flits)
Time taken is 7232 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 402.525 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2291 (1 samples)
Network latency average = 334.083 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2059 (1 samples)
Flit latency average = 315.547 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2042 (1 samples)
Fragmentation average = 29.5525 (1 samples)
	minimum = 0 (1 samples)
	maximum = 363 (1 samples)
Injected packet rate average = 0.0131684 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.0129566 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.236917 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.517 (1 samples)
Accepted flit rate average = 0.233019 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.338667 (1 samples)
Injected packet size average = 17.9913 (1 samples)
Accepted packet size average = 17.9846 (1 samples)
Hops average = 5.07673 (1 samples)
Total run time 4.99291
