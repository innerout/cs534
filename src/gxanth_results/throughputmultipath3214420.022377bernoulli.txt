BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 219.762
	minimum = 22
	maximum = 627
Network latency average = 214.413
	minimum = 22
	maximum = 627
Slowest packet = 1460
Flit latency average = 183.063
	minimum = 5
	maximum = 634
Slowest flit = 27119
Fragmentation average = 37.4453
	minimum = 0
	maximum = 319
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.0136615
	minimum = 0.006 (at node 41)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.253161
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.5311
Total in-flight flits = 28739 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 386.233
	minimum = 22
	maximum = 1293
Network latency average = 380.701
	minimum = 22
	maximum = 1249
Slowest packet = 3296
Flit latency average = 349.01
	minimum = 5
	maximum = 1232
Slowest flit = 58031
Fragmentation average = 38.5408
	minimum = 0
	maximum = 319
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143776
	minimum = 0.008 (at node 116)
	maximum = 0.0205 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.262284
	minimum = 0.144 (at node 116)
	maximum = 0.369 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.2425
Total in-flight flits = 53039 (0 measured)
latency change    = 0.431011
throughput change = 0.0347806
Class 0:
Packet latency average = 843.854
	minimum = 22
	maximum = 1552
Network latency average = 837.624
	minimum = 22
	maximum = 1534
Slowest packet = 6144
Flit latency average = 809.068
	minimum = 5
	maximum = 1517
Slowest flit = 110609
Fragmentation average = 38.6173
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0151458
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.273682
	minimum = 0.126 (at node 61)
	maximum = 0.537 (at node 16)
Injected packet length average = 18.0068
Accepted packet length average = 18.0698
Total in-flight flits = 77406 (0 measured)
latency change    = 0.542298
throughput change = 0.0416484
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 40.5593
	minimum = 22
	maximum = 110
Network latency average = 35.2655
	minimum = 22
	maximum = 73
Slowest packet = 13024
Flit latency average = 1102.7
	minimum = 5
	maximum = 1914
Slowest flit = 159173
Fragmentation average = 6.58192
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0153281
	minimum = 0.006 (at node 1)
	maximum = 0.024 (at node 0)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.276609
	minimum = 0.097 (at node 1)
	maximum = 0.449 (at node 0)
Injected packet length average = 17.9469
Accepted packet length average = 18.0459
Total in-flight flits = 103641 (72929 measured)
latency change    = 19.8054
throughput change = 0.010582
Class 0:
Packet latency average = 902.599
	minimum = 22
	maximum = 1980
Network latency average = 897.282
	minimum = 22
	maximum = 1980
Slowest packet = 12856
Flit latency average = 1255.17
	minimum = 5
	maximum = 2320
Slowest flit = 203921
Fragmentation average = 27.782
	minimum = 0
	maximum = 241
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.015375
	minimum = 0.0095 (at node 5)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.27687
	minimum = 0.1635 (at node 86)
	maximum = 0.3835 (at node 128)
Injected packet length average = 17.9983
Accepted packet length average = 18.0078
Total in-flight flits = 126482 (126017 measured)
latency change    = 0.955064
throughput change = 0.000940575
Class 0:
Packet latency average = 1438.13
	minimum = 22
	maximum = 2789
Network latency average = 1432.5
	minimum = 22
	maximum = 2769
Slowest packet = 13334
Flit latency average = 1402.13
	minimum = 5
	maximum = 2788
Slowest flit = 245458
Fragmentation average = 39.6181
	minimum = 0
	maximum = 284
Injected packet rate average = 0.022533
	minimum = 0.0153333 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0154236
	minimum = 0.0106667 (at node 36)
	maximum = 0.0206667 (at node 138)
Injected flit rate average = 0.405578
	minimum = 0.276 (at node 171)
	maximum = 0.54 (at node 139)
Accepted flit rate average= 0.277997
	minimum = 0.182667 (at node 36)
	maximum = 0.376667 (at node 138)
Injected packet length average = 17.9993
Accepted packet length average = 18.0241
Total in-flight flits = 150920 (150920 measured)
latency change    = 0.372379
throughput change = 0.00405306
Draining remaining packets ...
Class 0:
Remaining flits: 269244 269245 269246 269247 269248 269249 269250 269251 269252 269253 [...] (103472 flits)
Measured flits: 269244 269245 269246 269247 269248 269249 269250 269251 269252 269253 [...] (103472 flits)
Class 0:
Remaining flits: 298547 302220 302221 302222 302223 302224 302225 302226 302227 302228 [...] (56750 flits)
Measured flits: 298547 302220 302221 302222 302223 302224 302225 302226 302227 302228 [...] (56750 flits)
Class 0:
Remaining flits: 336490 336491 349866 349867 349868 349869 349870 349871 349872 349873 [...] (12135 flits)
Measured flits: 336490 336491 349866 349867 349868 349869 349870 349871 349872 349873 [...] (12135 flits)
Time taken is 9798 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2273.76 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4932 (1 samples)
Network latency average = 2268.02 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4919 (1 samples)
Flit latency average = 2020.51 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4902 (1 samples)
Fragmentation average = 45.2031 (1 samples)
	minimum = 0 (1 samples)
	maximum = 480 (1 samples)
Injected packet rate average = 0.022533 (1 samples)
	minimum = 0.0153333 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0154236 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.405578 (1 samples)
	minimum = 0.276 (1 samples)
	maximum = 0.54 (1 samples)
Accepted flit rate average = 0.277997 (1 samples)
	minimum = 0.182667 (1 samples)
	maximum = 0.376667 (1 samples)
Injected packet size average = 17.9993 (1 samples)
Accepted packet size average = 18.0241 (1 samples)
Hops average = 5.08051 (1 samples)
Total run time 7.71743
