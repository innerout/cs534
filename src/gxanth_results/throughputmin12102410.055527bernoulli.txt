BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 331.912
	minimum = 23
	maximum = 959
Network latency average = 302.64
	minimum = 23
	maximum = 942
Slowest packet = 380
Flit latency average = 271.036
	minimum = 6
	maximum = 955
Slowest flit = 4631
Fragmentation average = 52.6285
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0115937
	minimum = 0.002 (at node 41)
	maximum = 0.021 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.216922
	minimum = 0.049 (at node 41)
	maximum = 0.378 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 18.7102
Total in-flight flits = 131457 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 627.237
	minimum = 23
	maximum = 1880
Network latency average = 580.07
	minimum = 23
	maximum = 1827
Slowest packet = 1324
Flit latency average = 551.385
	minimum = 6
	maximum = 1810
Slowest flit = 23849
Fragmentation average = 55.1293
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0503932
	minimum = 0.037 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0120833
	minimum = 0.007 (at node 79)
	maximum = 0.0185 (at node 70)
Injected flit rate average = 0.902846
	minimum = 0.6585 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.221698
	minimum = 0.1335 (at node 79)
	maximum = 0.333 (at node 70)
Injected packet length average = 17.916
Accepted packet length average = 18.3474
Total in-flight flits = 263204 (0 measured)
latency change    = 0.470834
throughput change = 0.021543
Class 0:
Packet latency average = 1760.57
	minimum = 34
	maximum = 2832
Network latency average = 1673.97
	minimum = 23
	maximum = 2825
Slowest packet = 1071
Flit latency average = 1657.48
	minimum = 6
	maximum = 2808
Slowest flit = 19295
Fragmentation average = 66.7929
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0438906
	minimum = 0.012 (at node 28)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0102865
	minimum = 0.004 (at node 4)
	maximum = 0.018 (at node 119)
Injected flit rate average = 0.789891
	minimum = 0.211 (at node 116)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.185016
	minimum = 0.079 (at node 4)
	maximum = 0.318 (at node 119)
Injected packet length average = 17.9968
Accepted packet length average = 17.9863
Total in-flight flits = 379421 (0 measured)
latency change    = 0.643731
throughput change = 0.198266
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 661.848
	minimum = 28
	maximum = 2090
Network latency average = 43.3613
	minimum = 23
	maximum = 179
Slowest packet = 27800
Flit latency average = 2412.54
	minimum = 6
	maximum = 3684
Slowest flit = 49991
Fragmentation average = 9.92147
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0443906
	minimum = 0.012 (at node 48)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.010276
	minimum = 0.002 (at node 163)
	maximum = 0.018 (at node 33)
Injected flit rate average = 0.799698
	minimum = 0.207 (at node 180)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.185026
	minimum = 0.034 (at node 163)
	maximum = 0.338 (at node 93)
Injected packet length average = 18.015
Accepted packet length average = 18.0056
Total in-flight flits = 497256 (149869 measured)
latency change    = 1.66009
throughput change = 5.62984e-05
Class 0:
Packet latency average = 844.099
	minimum = 28
	maximum = 2765
Network latency average = 49.1294
	minimum = 23
	maximum = 1806
Slowest packet = 27800
Flit latency average = 2765.48
	minimum = 6
	maximum = 4534
Slowest flit = 75787
Fragmentation average = 10.1827
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0445703
	minimum = 0.0135 (at node 40)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.0102161
	minimum = 0.005 (at node 61)
	maximum = 0.0155 (at node 73)
Injected flit rate average = 0.802445
	minimum = 0.2425 (at node 40)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.183456
	minimum = 0.09 (at node 61)
	maximum = 0.274 (at node 73)
Injected packet length average = 18.004
Accepted packet length average = 17.9574
Total in-flight flits = 616972 (300880 measured)
latency change    = 0.215912
throughput change = 0.00855963
Class 0:
Packet latency average = 991.71
	minimum = 28
	maximum = 3377
Network latency average = 176.791
	minimum = 23
	maximum = 2930
Slowest packet = 27800
Flit latency average = 3155.71
	minimum = 6
	maximum = 5442
Slowest flit = 91733
Fragmentation average = 9.93739
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0432691
	minimum = 0.0136667 (at node 48)
	maximum = 0.0556667 (at node 10)
Accepted packet rate average = 0.0101372
	minimum = 0.006 (at node 4)
	maximum = 0.0153333 (at node 99)
Injected flit rate average = 0.779538
	minimum = 0.248667 (at node 140)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.18176
	minimum = 0.102667 (at node 4)
	maximum = 0.273333 (at node 99)
Injected packet length average = 18.016
Accepted packet length average = 17.9301
Total in-flight flits = 723305 (438198 measured)
latency change    = 0.148845
throughput change = 0.00932718
Draining remaining packets ...
Class 0:
Remaining flits: 86022 86023 86024 86025 86026 86027 86028 86029 86030 86031 [...] (691231 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (436504 flits)
Class 0:
Remaining flits: 112086 112087 112088 112089 112090 112091 112092 112093 112094 112095 [...] (658713 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (434019 flits)
Class 0:
Remaining flits: 124344 124345 124346 124347 124348 124349 124350 124351 124352 124353 [...] (626018 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (430196 flits)
Class 0:
Remaining flits: 131418 131419 131420 131421 131422 131423 131424 131425 131426 131427 [...] (594188 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (426333 flits)
Class 0:
Remaining flits: 141048 141049 141050 141051 141052 141053 141054 141055 141056 141057 [...] (560152 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (419905 flits)
Class 0:
Remaining flits: 180630 180631 180632 180633 180634 180635 180636 180637 180638 180639 [...] (527233 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (412426 flits)
Class 0:
Remaining flits: 200772 200773 200774 200775 200776 200777 200778 200779 200780 200781 [...] (494054 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (402779 flits)
Class 0:
Remaining flits: 217386 217387 217388 217389 217390 217391 217392 217393 217394 217395 [...] (460118 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (390254 flits)
Class 0:
Remaining flits: 227826 227827 227828 227829 227830 227831 227832 227833 227834 227835 [...] (428063 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (376043 flits)
Class 0:
Remaining flits: 251208 251209 251210 251211 251212 251213 251214 251215 251216 251217 [...] (398356 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (361263 flits)
Class 0:
Remaining flits: 289944 289945 289946 289947 289948 289949 289950 289951 289952 289953 [...] (369005 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (343679 flits)
Class 0:
Remaining flits: 323298 323299 323300 323301 323302 323303 323304 323305 323306 323307 [...] (339803 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (322988 flits)
Class 0:
Remaining flits: 323298 323299 323300 323301 323302 323303 323304 323305 323306 323307 [...] (309651 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (299337 flits)
Class 0:
Remaining flits: 352170 352171 352172 352173 352174 352175 352176 352177 352178 352179 [...] (279920 flits)
Measured flits: 500076 500077 500078 500079 500080 500081 500082 500083 500084 500085 [...] (273678 flits)
Class 0:
Remaining flits: 365958 365959 365960 365961 365962 365963 365964 365965 365966 365967 [...] (249723 flits)
Measured flits: 500086 500087 500088 500089 500090 500091 500092 500093 500310 500311 [...] (246637 flits)
Class 0:
Remaining flits: 390168 390169 390170 390171 390172 390173 390174 390175 390176 390177 [...] (220461 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (218726 flits)
Class 0:
Remaining flits: 390744 390745 390746 390747 390748 390749 390750 390751 390752 390753 [...] (191627 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (190736 flits)
Class 0:
Remaining flits: 396288 396289 396290 396291 396292 396293 396294 396295 396296 396297 [...] (162086 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (161578 flits)
Class 0:
Remaining flits: 418212 418213 418214 418215 418216 418217 418218 418219 418220 418221 [...] (133611 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (133257 flits)
Class 0:
Remaining flits: 450720 450721 450722 450723 450724 450725 450726 450727 450728 450729 [...] (104062 flits)
Measured flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (103936 flits)
Class 0:
Remaining flits: 489895 489896 489897 489898 489899 489900 489901 489902 489903 489904 [...] (74137 flits)
Measured flits: 509166 509167 509168 509169 509170 509171 509172 509173 509174 509175 [...] (74108 flits)
Class 0:
Remaining flits: 548007 548008 548009 556146 556147 556148 556149 556150 556151 556152 [...] (44952 flits)
Measured flits: 548007 548008 548009 556146 556147 556148 556149 556150 556151 556152 [...] (44952 flits)
Class 0:
Remaining flits: 641970 641971 641972 641973 641974 641975 641976 641977 641978 641979 [...] (17331 flits)
Measured flits: 641970 641971 641972 641973 641974 641975 641976 641977 641978 641979 [...] (17331 flits)
Class 0:
Remaining flits: 700506 700507 700508 700509 700510 700511 700512 700513 700514 700515 [...] (1964 flits)
Measured flits: 700506 700507 700508 700509 700510 700511 700512 700513 700514 700515 [...] (1964 flits)
Time taken is 30559 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16943.9 (1 samples)
	minimum = 28 (1 samples)
	maximum = 26216 (1 samples)
Network latency average = 16573.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25865 (1 samples)
Flit latency average = 12601.4 (1 samples)
	minimum = 6 (1 samples)
	maximum = 25848 (1 samples)
Fragmentation average = 66.564 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0432691 (1 samples)
	minimum = 0.0136667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0101372 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.779538 (1 samples)
	minimum = 0.248667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.18176 (1 samples)
	minimum = 0.102667 (1 samples)
	maximum = 0.273333 (1 samples)
Injected packet size average = 18.016 (1 samples)
Accepted packet size average = 17.9301 (1 samples)
Hops average = 5.22531 (1 samples)
Total run time 35.0765
