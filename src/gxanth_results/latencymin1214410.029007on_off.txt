BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 333.442
	minimum = 23
	maximum = 984
Network latency average = 259.165
	minimum = 23
	maximum = 899
Slowest packet = 68
Flit latency average = 223.199
	minimum = 6
	maximum = 960
Slowest flit = 4554
Fragmentation average = 55.5909
	minimum = 0
	maximum = 152
Injected packet rate average = 0.023651
	minimum = 0 (at node 16)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0096875
	minimum = 0.003 (at node 142)
	maximum = 0.017 (at node 140)
Injected flit rate average = 0.4215
	minimum = 0 (at node 16)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.182219
	minimum = 0.068 (at node 142)
	maximum = 0.317 (at node 140)
Injected packet length average = 17.8216
Accepted packet length average = 18.8097
Total in-flight flits = 46824 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 605.063
	minimum = 23
	maximum = 1956
Network latency average = 494.041
	minimum = 23
	maximum = 1715
Slowest packet = 68
Flit latency average = 453.426
	minimum = 6
	maximum = 1732
Slowest flit = 20349
Fragmentation average = 62.3161
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0225443
	minimum = 0.002 (at node 123)
	maximum = 0.043 (at node 51)
Accepted packet rate average = 0.00996094
	minimum = 0.005 (at node 96)
	maximum = 0.016 (at node 103)
Injected flit rate average = 0.40344
	minimum = 0.036 (at node 123)
	maximum = 0.774 (at node 51)
Accepted flit rate average= 0.182839
	minimum = 0.09 (at node 96)
	maximum = 0.2895 (at node 103)
Injected packet length average = 17.8955
Accepted packet length average = 18.3556
Total in-flight flits = 85922 (0 measured)
latency change    = 0.448914
throughput change = 0.00338983
Class 0:
Packet latency average = 1441.1
	minimum = 23
	maximum = 2909
Network latency average = 1214.31
	minimum = 23
	maximum = 2591
Slowest packet = 3007
Flit latency average = 1181.31
	minimum = 6
	maximum = 2603
Slowest flit = 29746
Fragmentation average = 68.2449
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0172969
	minimum = 0 (at node 32)
	maximum = 0.054 (at node 41)
Accepted packet rate average = 0.0101875
	minimum = 0.004 (at node 20)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.313406
	minimum = 0 (at node 32)
	maximum = 0.961 (at node 41)
Accepted flit rate average= 0.183625
	minimum = 0.062 (at node 20)
	maximum = 0.357 (at node 16)
Injected packet length average = 18.1192
Accepted packet length average = 18.0245
Total in-flight flits = 111488 (0 measured)
latency change    = 0.580137
throughput change = 0.00428296
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 791.566
	minimum = 29
	maximum = 2584
Network latency average = 210.527
	minimum = 24
	maximum = 908
Slowest packet = 12068
Flit latency average = 1763.47
	minimum = 6
	maximum = 3640
Slowest flit = 28099
Fragmentation average = 35.4264
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0118906
	minimum = 0 (at node 20)
	maximum = 0.046 (at node 63)
Accepted packet rate average = 0.00923437
	minimum = 0.003 (at node 131)
	maximum = 0.019 (at node 159)
Injected flit rate average = 0.213552
	minimum = 0 (at node 65)
	maximum = 0.828 (at node 63)
Accepted flit rate average= 0.166547
	minimum = 0.054 (at node 131)
	maximum = 0.348 (at node 159)
Injected packet length average = 17.9597
Accepted packet length average = 18.0355
Total in-flight flits = 121145 (39146 measured)
latency change    = 0.820564
throughput change = 0.102542
Class 0:
Packet latency average = 1407.68
	minimum = 29
	maximum = 4375
Network latency average = 655.755
	minimum = 24
	maximum = 1950
Slowest packet = 12068
Flit latency average = 2026.32
	minimum = 6
	maximum = 4389
Slowest flit = 34757
Fragmentation average = 46.7018
	minimum = 0
	maximum = 159
Injected packet rate average = 0.011013
	minimum = 0.0005 (at node 95)
	maximum = 0.028 (at node 149)
Accepted packet rate average = 0.00929948
	minimum = 0.005 (at node 108)
	maximum = 0.0165 (at node 182)
Injected flit rate average = 0.197896
	minimum = 0.009 (at node 95)
	maximum = 0.5065 (at node 149)
Accepted flit rate average= 0.167589
	minimum = 0.09 (at node 108)
	maximum = 0.296 (at node 182)
Injected packet length average = 17.9693
Accepted packet length average = 18.0213
Total in-flight flits = 123922 (68621 measured)
latency change    = 0.437682
throughput change = 0.00621562
Class 0:
Packet latency average = 2151.16
	minimum = 29
	maximum = 5189
Network latency average = 1142.75
	minimum = 23
	maximum = 2913
Slowest packet = 12068
Flit latency average = 2250.69
	minimum = 6
	maximum = 5009
Slowest flit = 67931
Fragmentation average = 54.875
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0105642
	minimum = 0.000666667 (at node 5)
	maximum = 0.0246667 (at node 149)
Accepted packet rate average = 0.00926562
	minimum = 0.00466667 (at node 36)
	maximum = 0.0136667 (at node 99)
Injected flit rate average = 0.190097
	minimum = 0.012 (at node 5)
	maximum = 0.442 (at node 149)
Accepted flit rate average= 0.166866
	minimum = 0.0883333 (at node 36)
	maximum = 0.240333 (at node 99)
Injected packet length average = 17.9944
Accepted packet length average = 18.0092
Total in-flight flits = 125857 (91074 measured)
latency change    = 0.345616
throughput change = 0.00432815
Class 0:
Packet latency average = 2814.85
	minimum = 29
	maximum = 6415
Network latency average = 1661.51
	minimum = 23
	maximum = 3965
Slowest packet = 12068
Flit latency average = 2480.54
	minimum = 6
	maximum = 6200
Slowest flit = 58373
Fragmentation average = 59.9861
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0100026
	minimum = 0.00175 (at node 122)
	maximum = 0.024 (at node 185)
Accepted packet rate average = 0.00922266
	minimum = 0.0055 (at node 36)
	maximum = 0.013 (at node 99)
Injected flit rate average = 0.179957
	minimum = 0.0315 (at node 122)
	maximum = 0.4315 (at node 185)
Accepted flit rate average= 0.165956
	minimum = 0.09875 (at node 36)
	maximum = 0.23 (at node 114)
Injected packet length average = 17.991
Accepted packet length average = 17.9944
Total in-flight flits = 123174 (103498 measured)
latency change    = 0.235782
throughput change = 0.00548695
Class 0:
Packet latency average = 3367.67
	minimum = 29
	maximum = 7074
Network latency average = 2061.3
	minimum = 23
	maximum = 4916
Slowest packet = 12068
Flit latency average = 2683.36
	minimum = 6
	maximum = 7070
Slowest flit = 69911
Fragmentation average = 64.3778
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00978229
	minimum = 0.0014 (at node 122)
	maximum = 0.0212 (at node 185)
Accepted packet rate average = 0.00913854
	minimum = 0.006 (at node 36)
	maximum = 0.0128 (at node 99)
Injected flit rate average = 0.175985
	minimum = 0.0252 (at node 122)
	maximum = 0.3816 (at node 185)
Accepted flit rate average= 0.164382
	minimum = 0.109 (at node 36)
	maximum = 0.2276 (at node 99)
Injected packet length average = 17.9902
Accepted packet length average = 17.9878
Total in-flight flits = 123583 (114286 measured)
latency change    = 0.164154
throughput change = 0.00957182
Class 0:
Packet latency average = 3945.01
	minimum = 29
	maximum = 7945
Network latency average = 2428.22
	minimum = 23
	maximum = 5899
Slowest packet = 12068
Flit latency average = 2835.59
	minimum = 6
	maximum = 7616
Slowest flit = 71531
Fragmentation average = 65.6452
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00957552
	minimum = 0.0025 (at node 122)
	maximum = 0.018 (at node 185)
Accepted packet rate average = 0.00913281
	minimum = 0.00633333 (at node 132)
	maximum = 0.012 (at node 99)
Injected flit rate average = 0.172261
	minimum = 0.0438333 (at node 122)
	maximum = 0.324 (at node 185)
Accepted flit rate average= 0.164199
	minimum = 0.113167 (at node 132)
	maximum = 0.213 (at node 165)
Injected packet length average = 17.9898
Accepted packet length average = 17.979
Total in-flight flits = 121717 (117424 measured)
latency change    = 0.146348
throughput change = 0.00111759
Class 0:
Packet latency average = 4460.92
	minimum = 29
	maximum = 9138
Network latency average = 2728.75
	minimum = 23
	maximum = 6834
Slowest packet = 12068
Flit latency average = 2970.42
	minimum = 6
	maximum = 8140
Slowest flit = 132245
Fragmentation average = 66.5467
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00947545
	minimum = 0.00314286 (at node 22)
	maximum = 0.0172857 (at node 62)
Accepted packet rate average = 0.0091064
	minimum = 0.00671429 (at node 35)
	maximum = 0.012 (at node 99)
Injected flit rate average = 0.170513
	minimum = 0.0565714 (at node 22)
	maximum = 0.309571 (at node 62)
Accepted flit rate average= 0.164016
	minimum = 0.121 (at node 36)
	maximum = 0.216 (at node 99)
Injected packet length average = 17.9952
Accepted packet length average = 18.011
Total in-flight flits = 121109 (119254 measured)
latency change    = 0.11565
throughput change = 0.00111672
Draining all recorded packets ...
Class 0:
Remaining flits: 131940 131941 131942 131943 131944 131945 131946 131947 131948 131949 [...] (121871 flits)
Measured flits: 218106 218107 218108 218109 218110 218111 218112 218113 218114 218115 [...] (120670 flits)
Class 0:
Remaining flits: 166086 166087 166088 166089 166090 166091 166092 166093 166094 166095 [...] (122995 flits)
Measured flits: 218106 218107 218108 218109 218110 218111 218112 218113 218114 218115 [...] (122319 flits)
Class 0:
Remaining flits: 191754 191755 191756 191757 191758 191759 191760 191761 191762 191763 [...] (122761 flits)
Measured flits: 229248 229249 229250 229251 229252 229253 229254 229255 229256 229257 [...] (122124 flits)
Class 0:
Remaining flits: 191754 191755 191756 191757 191758 191759 191760 191761 191762 191763 [...] (122730 flits)
Measured flits: 243954 243955 243956 243957 243958 243959 243960 243961 243962 243963 [...] (121236 flits)
Class 0:
Remaining flits: 194526 194527 194528 194529 194530 194531 194532 194533 194534 194535 [...] (121590 flits)
Measured flits: 248022 248023 248024 248025 248026 248027 248028 248029 248030 248031 [...] (118972 flits)
Class 0:
Remaining flits: 210546 210547 210548 210549 210550 210551 210552 210553 210554 210555 [...] (123047 flits)
Measured flits: 249606 249607 249608 249609 249610 249611 249612 249613 249614 249615 [...] (119008 flits)
Class 0:
Remaining flits: 319824 319825 319826 319827 319828 319829 319830 319831 319832 319833 [...] (122029 flits)
Measured flits: 319824 319825 319826 319827 319828 319829 319830 319831 319832 319833 [...] (117154 flits)
Class 0:
Remaining flits: 324144 324145 324146 324147 324148 324149 324150 324151 324152 324153 [...] (122020 flits)
Measured flits: 324144 324145 324146 324147 324148 324149 324150 324151 324152 324153 [...] (115349 flits)
Class 0:
Remaining flits: 324144 324145 324146 324147 324148 324149 324150 324151 324152 324153 [...] (120283 flits)
Measured flits: 324144 324145 324146 324147 324148 324149 324150 324151 324152 324153 [...] (111422 flits)
Class 0:
Remaining flits: 350172 350173 350174 350175 350176 350177 350178 350179 350180 350181 [...] (118417 flits)
Measured flits: 350172 350173 350174 350175 350176 350177 350178 350179 350180 350181 [...] (108356 flits)
Class 0:
Remaining flits: 350179 350180 350181 350182 350183 350184 350185 350186 350187 350188 [...] (118361 flits)
Measured flits: 350179 350180 350181 350182 350183 350184 350185 350186 350187 350188 [...] (105419 flits)
Class 0:
Remaining flits: 444240 444241 444242 444243 444244 444245 444246 444247 444248 444249 [...] (118033 flits)
Measured flits: 444240 444241 444242 444243 444244 444245 444246 444247 444248 444249 [...] (102364 flits)
Class 0:
Remaining flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (117784 flits)
Measured flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (98006 flits)
Class 0:
Remaining flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (119050 flits)
Measured flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (94206 flits)
Class 0:
Remaining flits: 502362 502363 502364 502365 502366 502367 502368 502369 502370 502371 [...] (121259 flits)
Measured flits: 502362 502363 502364 502365 502366 502367 502368 502369 502370 502371 [...] (88307 flits)
Class 0:
Remaining flits: 587340 587341 587342 587343 587344 587345 587346 587347 587348 587349 [...] (120547 flits)
Measured flits: 587340 587341 587342 587343 587344 587345 587346 587347 587348 587349 [...] (82054 flits)
Class 0:
Remaining flits: 617922 617923 617924 617925 617926 617927 617928 617929 617930 617931 [...] (120166 flits)
Measured flits: 617922 617923 617924 617925 617926 617927 617928 617929 617930 617931 [...] (76840 flits)
Class 0:
Remaining flits: 639954 639955 639956 639957 639958 639959 639960 639961 639962 639963 [...] (119989 flits)
Measured flits: 639954 639955 639956 639957 639958 639959 639960 639961 639962 639963 [...] (69840 flits)
Class 0:
Remaining flits: 670356 670357 670358 670359 670360 670361 670362 670363 670364 670365 [...] (119795 flits)
Measured flits: 670356 670357 670358 670359 670360 670361 670362 670363 670364 670365 [...] (63677 flits)
Class 0:
Remaining flits: 711450 711451 711452 711453 711454 711455 711456 711457 711458 711459 [...] (118867 flits)
Measured flits: 711450 711451 711452 711453 711454 711455 711456 711457 711458 711459 [...] (57711 flits)
Class 0:
Remaining flits: 734346 734347 734348 734349 734350 734351 734352 734353 734354 734355 [...] (120390 flits)
Measured flits: 734346 734347 734348 734349 734350 734351 734352 734353 734354 734355 [...] (51337 flits)
Class 0:
Remaining flits: 760968 760969 760970 760971 760972 760973 760974 760975 760976 760977 [...] (121090 flits)
Measured flits: 760968 760969 760970 760971 760972 760973 760974 760975 760976 760977 [...] (46080 flits)
Class 0:
Remaining flits: 762678 762679 762680 762681 762682 762683 762684 762685 762686 762687 [...] (119689 flits)
Measured flits: 762678 762679 762680 762681 762682 762683 762684 762685 762686 762687 [...] (39801 flits)
Class 0:
Remaining flits: 762678 762679 762680 762681 762682 762683 762684 762685 762686 762687 [...] (118471 flits)
Measured flits: 762678 762679 762680 762681 762682 762683 762684 762685 762686 762687 [...] (33447 flits)
Class 0:
Remaining flits: 873216 873217 873218 873219 873220 873221 873222 873223 873224 873225 [...] (119220 flits)
Measured flits: 873216 873217 873218 873219 873220 873221 873222 873223 873224 873225 [...] (28223 flits)
Class 0:
Remaining flits: 873216 873217 873218 873219 873220 873221 873222 873223 873224 873225 [...] (118109 flits)
Measured flits: 873216 873217 873218 873219 873220 873221 873222 873223 873224 873225 [...] (24452 flits)
Class 0:
Remaining flits: 927306 927307 927308 927309 927310 927311 927312 927313 927314 927315 [...] (118004 flits)
Measured flits: 932832 932833 932834 932835 932836 932837 932838 932839 932840 932841 [...] (21103 flits)
Class 0:
Remaining flits: 936468 936469 936470 936471 936472 936473 936474 936475 936476 936477 [...] (117570 flits)
Measured flits: 936468 936469 936470 936471 936472 936473 936474 936475 936476 936477 [...] (17610 flits)
Class 0:
Remaining flits: 936475 936476 936477 936478 936479 936480 936481 936482 936483 936484 [...] (116775 flits)
Measured flits: 936475 936476 936477 936478 936479 936480 936481 936482 936483 936484 [...] (14495 flits)
Class 0:
Remaining flits: 1001070 1001071 1001072 1001073 1001074 1001075 1001076 1001077 1001078 1001079 [...] (117948 flits)
Measured flits: 1001070 1001071 1001072 1001073 1001074 1001075 1001076 1001077 1001078 1001079 [...] (12742 flits)
Class 0:
Remaining flits: 1046790 1046791 1046792 1046793 1046794 1046795 1046796 1046797 1046798 1046799 [...] (118836 flits)
Measured flits: 1046790 1046791 1046792 1046793 1046794 1046795 1046796 1046797 1046798 1046799 [...] (10810 flits)
Class 0:
Remaining flits: 1064034 1064035 1064036 1064037 1064038 1064039 1064040 1064041 1064042 1064043 [...] (118918 flits)
Measured flits: 1064034 1064035 1064036 1064037 1064038 1064039 1064040 1064041 1064042 1064043 [...] (9125 flits)
Class 0:
Remaining flits: 1072404 1072405 1072406 1072407 1072408 1072409 1072410 1072411 1072412 1072413 [...] (119470 flits)
Measured flits: 1072404 1072405 1072406 1072407 1072408 1072409 1072410 1072411 1072412 1072413 [...] (7916 flits)
Class 0:
Remaining flits: 1111662 1111663 1111664 1111665 1111666 1111667 1111668 1111669 1111670 1111671 [...] (119881 flits)
Measured flits: 1111662 1111663 1111664 1111665 1111666 1111667 1111668 1111669 1111670 1111671 [...] (6607 flits)
Class 0:
Remaining flits: 1132092 1132093 1132094 1132095 1132096 1132097 1132098 1132099 1132100 1132101 [...] (122055 flits)
Measured flits: 1132092 1132093 1132094 1132095 1132096 1132097 1132098 1132099 1132100 1132101 [...] (5257 flits)
Class 0:
Remaining flits: 1132092 1132093 1132094 1132095 1132096 1132097 1132098 1132099 1132100 1132101 [...] (120806 flits)
Measured flits: 1132092 1132093 1132094 1132095 1132096 1132097 1132098 1132099 1132100 1132101 [...] (4357 flits)
Class 0:
Remaining flits: 1209942 1209943 1209944 1209945 1209946 1209947 1209948 1209949 1209950 1209951 [...] (119278 flits)
Measured flits: 1209942 1209943 1209944 1209945 1209946 1209947 1209948 1209949 1209950 1209951 [...] (3677 flits)
Class 0:
Remaining flits: 1274094 1274095 1274096 1274097 1274098 1274099 1274100 1274101 1274102 1274103 [...] (118966 flits)
Measured flits: 1274094 1274095 1274096 1274097 1274098 1274099 1274100 1274101 1274102 1274103 [...] (3214 flits)
Class 0:
Remaining flits: 1289124 1289125 1289126 1289127 1289128 1289129 1289130 1289131 1289132 1289133 [...] (118470 flits)
Measured flits: 1289124 1289125 1289126 1289127 1289128 1289129 1289130 1289131 1289132 1289133 [...] (2529 flits)
Class 0:
Remaining flits: 1338156 1338157 1338158 1338159 1338160 1338161 1338162 1338163 1338164 1338165 [...] (120914 flits)
Measured flits: 1351188 1351189 1351190 1351191 1351192 1351193 1351194 1351195 1351196 1351197 [...] (2057 flits)
Class 0:
Remaining flits: 1371114 1371115 1371116 1371117 1371118 1371119 1371120 1371121 1371122 1371123 [...] (124209 flits)
Measured flits: 1424106 1424107 1424108 1424109 1424110 1424111 1424112 1424113 1424114 1424115 [...] (1880 flits)
Class 0:
Remaining flits: 1371114 1371115 1371116 1371117 1371118 1371119 1371120 1371121 1371122 1371123 [...] (123241 flits)
Measured flits: 1448172 1448173 1448174 1448175 1448176 1448177 1448178 1448179 1448180 1448181 [...] (1440 flits)
Class 0:
Remaining flits: 1408824 1408825 1408826 1408827 1408828 1408829 1408830 1408831 1408832 1408833 [...] (120969 flits)
Measured flits: 1472580 1472581 1472582 1472583 1472584 1472585 1472586 1472587 1472588 1472589 [...] (1180 flits)
Class 0:
Remaining flits: 1431720 1431721 1431722 1431723 1431724 1431725 1431726 1431727 1431728 1431729 [...] (119377 flits)
Measured flits: 1555758 1555759 1555760 1555761 1555762 1555763 1555764 1555765 1555766 1555767 [...] (1172 flits)
Class 0:
Remaining flits: 1435230 1435231 1435232 1435233 1435234 1435235 1435236 1435237 1435238 1435239 [...] (120736 flits)
Measured flits: 1625760 1625761 1625762 1625763 1625764 1625765 1625766 1625767 1625768 1625769 [...] (903 flits)
Class 0:
Remaining flits: 1478501 1500678 1500679 1500680 1500681 1500682 1500683 1500684 1500685 1500686 [...] (121591 flits)
Measured flits: 1761786 1761787 1761788 1761789 1761790 1761791 1761792 1761793 1761794 1761795 [...] (553 flits)
Class 0:
Remaining flits: 1551202 1551203 1588878 1588879 1588880 1588881 1588882 1588883 1588884 1588885 [...] (121269 flits)
Measured flits: 1761794 1761795 1761796 1761797 1761798 1761799 1761800 1761801 1761802 1761803 [...] (260 flits)
Class 0:
Remaining flits: 1628820 1628821 1628822 1628823 1628824 1628825 1628826 1628827 1628828 1628829 [...] (119561 flits)
Measured flits: 1784448 1784449 1784450 1784451 1784452 1784453 1784454 1784455 1784456 1784457 [...] (54 flits)
Class 0:
Remaining flits: 1646413 1646414 1646415 1646416 1646417 1646418 1646419 1646420 1646421 1646422 [...] (120543 flits)
Measured flits: 1833408 1833409 1833410 1833411 1833412 1833413 1833414 1833415 1833416 1833417 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1666998 1666999 1667000 1667001 1667002 1667003 1667004 1667005 1667006 1667007 [...] (89992 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1687662 1687663 1687664 1687665 1687666 1687667 1687668 1687669 1687670 1687671 [...] (60498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1731744 1731745 1731746 1731747 1731748 1731749 1731750 1731751 1731752 1731753 [...] (31642 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1768374 1768375 1768376 1768377 1768378 1768379 1768380 1768381 1768382 1768383 [...] (8096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1855494 1855495 1855496 1855497 1855498 1855499 1855500 1855501 1855502 1855503 [...] (132 flits)
Measured flits: (0 flits)
Time taken is 64280 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14178.1 (1 samples)
	minimum = 29 (1 samples)
	maximum = 49167 (1 samples)
Network latency average = 3856.97 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14594 (1 samples)
Flit latency average = 3750.33 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14481 (1 samples)
Fragmentation average = 71.0955 (1 samples)
	minimum = 0 (1 samples)
	maximum = 169 (1 samples)
Injected packet rate average = 0.00947545 (1 samples)
	minimum = 0.00314286 (1 samples)
	maximum = 0.0172857 (1 samples)
Accepted packet rate average = 0.0091064 (1 samples)
	minimum = 0.00671429 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.170513 (1 samples)
	minimum = 0.0565714 (1 samples)
	maximum = 0.309571 (1 samples)
Accepted flit rate average = 0.164016 (1 samples)
	minimum = 0.121 (1 samples)
	maximum = 0.216 (1 samples)
Injected packet size average = 17.9952 (1 samples)
Accepted packet size average = 18.011 (1 samples)
Hops average = 5.05853 (1 samples)
Total run time 48.5015
