BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.236
	minimum = 22
	maximum = 978
Network latency average = 236.915
	minimum = 22
	maximum = 738
Slowest packet = 45
Flit latency average = 214.159
	minimum = 5
	maximum = 790
Slowest flit = 18550
Fragmentation average = 18.9338
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0290938
	minimum = 0 (at node 63)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0140729
	minimum = 0.005 (at node 41)
	maximum = 0.023 (at node 88)
Injected flit rate average = 0.518281
	minimum = 0 (at node 63)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.258583
	minimum = 0.09 (at node 41)
	maximum = 0.414 (at node 88)
Injected packet length average = 17.8142
Accepted packet length average = 18.3745
Total in-flight flits = 50918 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 630.723
	minimum = 22
	maximum = 1910
Network latency average = 464.193
	minimum = 22
	maximum = 1398
Slowest packet = 45
Flit latency average = 441.238
	minimum = 5
	maximum = 1427
Slowest flit = 52707
Fragmentation average = 20.9072
	minimum = 0
	maximum = 103
Injected packet rate average = 0.029401
	minimum = 0.0055 (at node 2)
	maximum = 0.055 (at node 29)
Accepted packet rate average = 0.014625
	minimum = 0.008 (at node 153)
	maximum = 0.022 (at node 22)
Injected flit rate average = 0.527104
	minimum = 0.0925 (at node 2)
	maximum = 0.99 (at node 29)
Accepted flit rate average= 0.265927
	minimum = 0.144 (at node 153)
	maximum = 0.396 (at node 22)
Injected packet length average = 17.9281
Accepted packet length average = 18.183
Total in-flight flits = 101626 (0 measured)
latency change    = 0.43995
throughput change = 0.0276157
Class 0:
Packet latency average = 1469.13
	minimum = 25
	maximum = 2914
Network latency average = 1195.34
	minimum = 22
	maximum = 2054
Slowest packet = 4653
Flit latency average = 1176.84
	minimum = 5
	maximum = 2037
Slowest flit = 83375
Fragmentation average = 20.949
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0220729
	minimum = 0 (at node 21)
	maximum = 0.055 (at node 33)
Accepted packet rate average = 0.0150104
	minimum = 0.006 (at node 190)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.398286
	minimum = 0.015 (at node 21)
	maximum = 0.981 (at node 33)
Accepted flit rate average= 0.269802
	minimum = 0.108 (at node 190)
	maximum = 0.468 (at node 181)
Injected packet length average = 18.0441
Accepted packet length average = 17.9743
Total in-flight flits = 126954 (0 measured)
latency change    = 0.570682
throughput change = 0.0143624
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 849.601
	minimum = 25
	maximum = 2766
Network latency average = 125.182
	minimum = 22
	maximum = 964
Slowest packet = 15721
Flit latency average = 1633.98
	minimum = 5
	maximum = 2866
Slowest flit = 98189
Fragmentation average = 5.9021
	minimum = 0
	maximum = 19
Injected packet rate average = 0.0184792
	minimum = 0 (at node 2)
	maximum = 0.052 (at node 161)
Accepted packet rate average = 0.0149063
	minimum = 0.007 (at node 115)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.332917
	minimum = 0 (at node 2)
	maximum = 0.949 (at node 161)
Accepted flit rate average= 0.267724
	minimum = 0.117 (at node 146)
	maximum = 0.555 (at node 129)
Injected packet length average = 18.0158
Accepted packet length average = 17.9605
Total in-flight flits = 139811 (61599 measured)
latency change    = 0.729196
throughput change = 0.00776219
Class 0:
Packet latency average = 1657.82
	minimum = 25
	maximum = 3964
Network latency average = 862.62
	minimum = 22
	maximum = 1976
Slowest packet = 15721
Flit latency average = 1829.48
	minimum = 5
	maximum = 3623
Slowest flit = 137842
Fragmentation average = 13.0995
	minimum = 0
	maximum = 109
Injected packet rate average = 0.0178828
	minimum = 0.0065 (at node 160)
	maximum = 0.033 (at node 14)
Accepted packet rate average = 0.014875
	minimum = 0.0085 (at node 89)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.32199
	minimum = 0.11 (at node 160)
	maximum = 0.6005 (at node 161)
Accepted flit rate average= 0.267953
	minimum = 0.1565 (at node 89)
	maximum = 0.4585 (at node 129)
Injected packet length average = 18.0055
Accepted packet length average = 18.0137
Total in-flight flits = 148350 (112212 measured)
latency change    = 0.48752
throughput change = 0.000855249
Class 0:
Packet latency average = 2372.85
	minimum = 25
	maximum = 5039
Network latency average = 1473.3
	minimum = 22
	maximum = 2977
Slowest packet = 15721
Flit latency average = 2011.17
	minimum = 5
	maximum = 4249
Slowest flit = 145654
Fragmentation average = 16.6885
	minimum = 0
	maximum = 222
Injected packet rate average = 0.017184
	minimum = 0.00733333 (at node 176)
	maximum = 0.0306667 (at node 139)
Accepted packet rate average = 0.0148958
	minimum = 0.01 (at node 2)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.309536
	minimum = 0.137 (at node 176)
	maximum = 0.552 (at node 139)
Accepted flit rate average= 0.268097
	minimum = 0.173667 (at node 2)
	maximum = 0.395 (at node 129)
Injected packet length average = 18.013
Accepted packet length average = 17.9981
Total in-flight flits = 151450 (141064 measured)
latency change    = 0.301336
throughput change = 0.000537481
Class 0:
Packet latency average = 2958.49
	minimum = 25
	maximum = 5941
Network latency average = 1948.15
	minimum = 22
	maximum = 3886
Slowest packet = 15721
Flit latency average = 2159.02
	minimum = 5
	maximum = 4885
Slowest flit = 206855
Fragmentation average = 17.7996
	minimum = 0
	maximum = 222
Injected packet rate average = 0.0164896
	minimum = 0.007 (at node 176)
	maximum = 0.02875 (at node 170)
Accepted packet rate average = 0.0149023
	minimum = 0.0105 (at node 2)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.296792
	minimum = 0.12975 (at node 176)
	maximum = 0.5175 (at node 170)
Accepted flit rate average= 0.268171
	minimum = 0.18875 (at node 2)
	maximum = 0.3625 (at node 128)
Injected packet length average = 17.9987
Accepted packet length average = 17.9952
Total in-flight flits = 149617 (148460 measured)
latency change    = 0.197952
throughput change = 0.000273523
Class 0:
Packet latency average = 3423.04
	minimum = 25
	maximum = 7015
Network latency average = 2257.12
	minimum = 22
	maximum = 4733
Slowest packet = 15721
Flit latency average = 2278.72
	minimum = 5
	maximum = 5258
Slowest flit = 232955
Fragmentation average = 18.3251
	minimum = 0
	maximum = 222
Injected packet rate average = 0.0163031
	minimum = 0.0084 (at node 160)
	maximum = 0.0266 (at node 170)
Accepted packet rate average = 0.0148979
	minimum = 0.0106 (at node 80)
	maximum = 0.0198 (at node 128)
Injected flit rate average = 0.293475
	minimum = 0.1512 (at node 160)
	maximum = 0.4754 (at node 170)
Accepted flit rate average= 0.268222
	minimum = 0.1942 (at node 80)
	maximum = 0.3564 (at node 128)
Injected packet length average = 18.0012
Accepted packet length average = 18.004
Total in-flight flits = 151899 (151889 measured)
latency change    = 0.135714
throughput change = 0.000191267
Class 0:
Packet latency average = 3803.81
	minimum = 25
	maximum = 7996
Network latency average = 2427.03
	minimum = 22
	maximum = 5357
Slowest packet = 15721
Flit latency average = 2370.12
	minimum = 5
	maximum = 5340
Slowest flit = 318365
Fragmentation average = 18.8673
	minimum = 0
	maximum = 222
Injected packet rate average = 0.0162005
	minimum = 0.00883333 (at node 160)
	maximum = 0.0256667 (at node 170)
Accepted packet rate average = 0.0148785
	minimum = 0.011 (at node 80)
	maximum = 0.0188333 (at node 68)
Injected flit rate average = 0.291606
	minimum = 0.159 (at node 160)
	maximum = 0.462 (at node 170)
Accepted flit rate average= 0.267826
	minimum = 0.1995 (at node 80)
	maximum = 0.343667 (at node 70)
Injected packet length average = 17.9998
Accepted packet length average = 18.0009
Total in-flight flits = 155235 (155235 measured)
latency change    = 0.100101
throughput change = 0.0014799
Class 0:
Packet latency average = 4133.35
	minimum = 25
	maximum = 8777
Network latency average = 2526.84
	minimum = 22
	maximum = 5465
Slowest packet = 15721
Flit latency average = 2438.07
	minimum = 5
	maximum = 5448
Slowest flit = 357587
Fragmentation average = 19.1171
	minimum = 0
	maximum = 222
Injected packet rate average = 0.0160171
	minimum = 0.009 (at node 160)
	maximum = 0.0244286 (at node 170)
Accepted packet rate average = 0.0148996
	minimum = 0.0114286 (at node 80)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.288258
	minimum = 0.162 (at node 160)
	maximum = 0.439714 (at node 170)
Accepted flit rate average= 0.268291
	minimum = 0.205714 (at node 80)
	maximum = 0.341857 (at node 70)
Injected packet length average = 17.9969
Accepted packet length average = 18.0066
Total in-flight flits = 154703 (154703 measured)
latency change    = 0.0797284
throughput change = 0.00173469
Draining all recorded packets ...
Class 0:
Remaining flits: 405735 405736 405737 408204 408205 408206 408207 408208 408209 408210 [...] (153571 flits)
Measured flits: 405735 405736 405737 408204 408205 408206 408207 408208 408209 408210 [...] (153067 flits)
Class 0:
Remaining flits: 465210 465211 465212 465213 465214 465215 465216 465217 465218 465219 [...] (153347 flits)
Measured flits: 465210 465211 465212 465213 465214 465215 465216 465217 465218 465219 [...] (151367 flits)
Class 0:
Remaining flits: 516240 516241 516242 516243 516244 516245 516246 516247 516248 516249 [...] (153367 flits)
Measured flits: 516240 516241 516242 516243 516244 516245 516246 516247 516248 516249 [...] (149099 flits)
Class 0:
Remaining flits: 567288 567289 567290 567291 567292 567293 567294 567295 567296 567297 [...] (150607 flits)
Measured flits: 567288 567289 567290 567291 567292 567293 567294 567295 567296 567297 [...] (142464 flits)
Class 0:
Remaining flits: 576612 576613 576614 576615 576616 576617 576618 576619 576620 576621 [...] (151661 flits)
Measured flits: 576612 576613 576614 576615 576616 576617 576618 576619 576620 576621 [...] (139738 flits)
Class 0:
Remaining flits: 693324 693325 693326 693327 693328 693329 693330 693331 693332 693333 [...] (153724 flits)
Measured flits: 695630 695631 695632 695633 695634 695635 695636 695637 695638 695639 [...] (136573 flits)
Class 0:
Remaining flits: 717516 717517 717518 717519 717520 717521 717522 717523 717524 717525 [...] (151346 flits)
Measured flits: 717516 717517 717518 717519 717520 717521 717522 717523 717524 717525 [...] (127186 flits)
Class 0:
Remaining flits: 760032 760033 760034 760035 760036 760037 760038 760039 760040 760041 [...] (149081 flits)
Measured flits: 760032 760033 760034 760035 760036 760037 760038 760039 760040 760041 [...] (116150 flits)
Class 0:
Remaining flits: 786420 786421 786422 786423 786424 786425 786426 786427 786428 786429 [...] (148423 flits)
Measured flits: 786420 786421 786422 786423 786424 786425 786426 786427 786428 786429 [...] (105972 flits)
Class 0:
Remaining flits: 862187 862188 862189 862190 862191 862192 862193 862194 862195 862196 [...] (148861 flits)
Measured flits: 862187 862188 862189 862190 862191 862192 862193 862194 862195 862196 [...] (99559 flits)
Class 0:
Remaining flits: 884340 884341 884342 884343 884344 884345 884346 884347 884348 884349 [...] (148341 flits)
Measured flits: 927054 927055 927056 927057 927058 927059 927060 927061 927062 927063 [...] (90324 flits)
Class 0:
Remaining flits: 933517 933518 933519 933520 933521 933522 933523 933524 933525 933526 [...] (146879 flits)
Measured flits: 933517 933518 933519 933520 933521 933522 933523 933524 933525 933526 [...] (77976 flits)
Class 0:
Remaining flits: 989077 989078 989079 989080 989081 990090 990091 990092 990093 990094 [...] (145659 flits)
Measured flits: 989077 989078 989079 989080 989081 990090 990091 990092 990093 990094 [...] (65660 flits)
Class 0:
Remaining flits: 1070010 1070011 1070012 1070013 1070014 1070015 1070016 1070017 1070018 1070019 [...] (145854 flits)
Measured flits: 1070010 1070011 1070012 1070013 1070014 1070015 1070016 1070017 1070018 1070019 [...] (54671 flits)
Class 0:
Remaining flits: 1101852 1101853 1101854 1101855 1101856 1101857 1101858 1101859 1101860 1101861 [...] (148861 flits)
Measured flits: 1101852 1101853 1101854 1101855 1101856 1101857 1101858 1101859 1101860 1101861 [...] (46949 flits)
Class 0:
Remaining flits: 1138159 1138160 1138161 1138162 1138163 1138164 1138165 1138166 1138167 1138168 [...] (148367 flits)
Measured flits: 1138159 1138160 1138161 1138162 1138163 1138164 1138165 1138166 1138167 1138168 [...] (37855 flits)
Class 0:
Remaining flits: 1206134 1206135 1206136 1206137 1206138 1206139 1206140 1206141 1206142 1206143 [...] (146654 flits)
Measured flits: 1243566 1243567 1243568 1243569 1243570 1243571 1243572 1243573 1243574 1243575 [...] (29670 flits)
Class 0:
Remaining flits: 1266408 1266409 1266410 1266411 1266412 1266413 1266414 1266415 1266416 1266417 [...] (147048 flits)
Measured flits: 1282014 1282015 1282016 1282017 1282018 1282019 1282020 1282021 1282022 1282023 [...] (23290 flits)
Class 0:
Remaining flits: 1306860 1306861 1306862 1306863 1306864 1306865 1306866 1306867 1306868 1306869 [...] (145729 flits)
Measured flits: 1306860 1306861 1306862 1306863 1306864 1306865 1306866 1306867 1306868 1306869 [...] (18126 flits)
Class 0:
Remaining flits: 1361718 1361719 1361720 1361721 1361722 1361723 1361724 1361725 1361726 1361727 [...] (140563 flits)
Measured flits: 1361718 1361719 1361720 1361721 1361722 1361723 1361724 1361725 1361726 1361727 [...] (13691 flits)
Class 0:
Remaining flits: 1395252 1395253 1395254 1395255 1395256 1395257 1395258 1395259 1395260 1395261 [...] (140448 flits)
Measured flits: 1395252 1395253 1395254 1395255 1395256 1395257 1395258 1395259 1395260 1395261 [...] (12048 flits)
Class 0:
Remaining flits: 1438398 1438399 1438400 1438401 1438402 1438403 1438404 1438405 1438406 1438407 [...] (138974 flits)
Measured flits: 1524402 1524403 1524404 1524405 1524406 1524407 1524408 1524409 1524410 1524411 [...] (9010 flits)
Class 0:
Remaining flits: 1438398 1438399 1438400 1438401 1438402 1438403 1438404 1438405 1438406 1438407 [...] (141616 flits)
Measured flits: 1552482 1552483 1552484 1552485 1552486 1552487 1552488 1552489 1552490 1552491 [...] (6838 flits)
Class 0:
Remaining flits: 1554588 1554589 1554590 1554591 1554592 1554593 1554594 1554595 1554596 1554597 [...] (141628 flits)
Measured flits: 1651824 1651825 1651826 1651827 1651828 1651829 1651830 1651831 1651832 1651833 [...] (5124 flits)
Class 0:
Remaining flits: 1576404 1576405 1576406 1576407 1576408 1576409 1576410 1576411 1576412 1576413 [...] (142833 flits)
Measured flits: 1677276 1677277 1677278 1677279 1677280 1677281 1677282 1677283 1677284 1677285 [...] (3887 flits)
Class 0:
Remaining flits: 1576405 1576406 1576407 1576408 1576409 1576410 1576411 1576412 1576413 1576414 [...] (146156 flits)
Measured flits: 1694430 1694431 1694432 1694433 1694434 1694435 1694436 1694437 1694438 1694439 [...] (2714 flits)
Class 0:
Remaining flits: 1733472 1733473 1733474 1733475 1733476 1733477 1733478 1733479 1733480 1733481 [...] (144385 flits)
Measured flits: 1733472 1733473 1733474 1733475 1733476 1733477 1733478 1733479 1733480 1733481 [...] (2158 flits)
Class 0:
Remaining flits: 1785096 1785097 1785098 1785099 1785100 1785101 1785102 1785103 1785104 1785105 [...] (144037 flits)
Measured flits: 1788966 1788967 1788968 1788969 1788970 1788971 1788972 1788973 1788974 1788975 [...] (1326 flits)
Class 0:
Remaining flits: 1826633 1826634 1826635 1826636 1826637 1826638 1826639 1838088 1838089 1838090 [...] (138769 flits)
Measured flits: 1918800 1918801 1918802 1918803 1918804 1918805 1918806 1918807 1918808 1918809 [...] (558 flits)
Class 0:
Remaining flits: 1879038 1879039 1879040 1879041 1879042 1879043 1879044 1879045 1879046 1879047 [...] (138503 flits)
Measured flits: 1982484 1982485 1982486 1982487 1982488 1982489 1982490 1982491 1982492 1982493 [...] (140 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1923066 1923067 1923068 1923069 1923070 1923071 1923072 1923073 1923074 1923075 [...] (89554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1954422 1954423 1954424 1954425 1954426 1954427 1954428 1954429 1954430 1954431 [...] (41965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2019402 2019403 2019404 2019405 2019406 2019407 2019408 2019409 2019410 2019411 [...] (6398 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2125818 2125819 2125820 2125821 2125822 2125823 2125824 2125825 2125826 2125827 [...] (371 flits)
Measured flits: (0 flits)
Time taken is 45089 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9430.62 (1 samples)
	minimum = 25 (1 samples)
	maximum = 31279 (1 samples)
Network latency average = 2873.55 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7415 (1 samples)
Flit latency average = 2766.18 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8415 (1 samples)
Fragmentation average = 20.2781 (1 samples)
	minimum = 0 (1 samples)
	maximum = 296 (1 samples)
Injected packet rate average = 0.0160171 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0244286 (1 samples)
Accepted packet rate average = 0.0148996 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.288258 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 0.439714 (1 samples)
Accepted flit rate average = 0.268291 (1 samples)
	minimum = 0.205714 (1 samples)
	maximum = 0.341857 (1 samples)
Injected packet size average = 17.9969 (1 samples)
Accepted packet size average = 18.0066 (1 samples)
Hops average = 5.08009 (1 samples)
Total run time 47.1472
