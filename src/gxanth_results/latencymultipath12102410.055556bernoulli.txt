BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.438
	minimum = 23
	maximum = 920
Network latency average = 297.739
	minimum = 23
	maximum = 899
Slowest packet = 178
Flit latency average = 267.624
	minimum = 6
	maximum = 951
Slowest flit = 4635
Fragmentation average = 51.1379
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0123125
	minimum = 0.005 (at node 93)
	maximum = 0.019 (at node 27)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.229901
	minimum = 0.09 (at node 93)
	maximum = 0.342 (at node 27)
Injected packet length average = 17.8419
Accepted packet length average = 18.6722
Total in-flight flits = 129127 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 606.184
	minimum = 23
	maximum = 1835
Network latency average = 561.41
	minimum = 23
	maximum = 1811
Slowest packet = 672
Flit latency average = 534.582
	minimum = 6
	maximum = 1858
Slowest flit = 18088
Fragmentation average = 53.9553
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0503724
	minimum = 0.0415 (at node 159)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0123568
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.902578
	minimum = 0.7425 (at node 159)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.226945
	minimum = 0.1325 (at node 116)
	maximum = 0.356 (at node 22)
Injected packet length average = 17.9181
Accepted packet length average = 18.3661
Total in-flight flits = 261063 (0 measured)
latency change    = 0.461487
throughput change = 0.013024
Class 0:
Packet latency average = 1767.76
	minimum = 32
	maximum = 2886
Network latency average = 1681.48
	minimum = 23
	maximum = 2841
Slowest packet = 815
Flit latency average = 1663.53
	minimum = 6
	maximum = 2824
Slowest flit = 14687
Fragmentation average = 66.4277
	minimum = 0
	maximum = 172
Injected packet rate average = 0.0439688
	minimum = 0.011 (at node 44)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0104115
	minimum = 0.004 (at node 21)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.791667
	minimum = 0.205 (at node 44)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.18587
	minimum = 0.062 (at node 91)
	maximum = 0.325 (at node 114)
Injected packet length average = 18.0052
Accepted packet length average = 17.8524
Total in-flight flits = 377332 (0 measured)
latency change    = 0.657089
throughput change = 0.220991
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 582.363
	minimum = 29
	maximum = 2129
Network latency average = 40.9441
	minimum = 23
	maximum = 162
Slowest packet = 27927
Flit latency average = 2409.55
	minimum = 6
	maximum = 3815
Slowest flit = 25269
Fragmentation average = 9.43575
	minimum = 0
	maximum = 41
Injected packet rate average = 0.044375
	minimum = 0.012 (at node 56)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0102969
	minimum = 0.003 (at node 31)
	maximum = 0.019 (at node 119)
Injected flit rate average = 0.798781
	minimum = 0.216 (at node 72)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.186182
	minimum = 0.049 (at node 49)
	maximum = 0.346 (at node 187)
Injected packet length average = 18.0007
Accepted packet length average = 18.0814
Total in-flight flits = 494909 (150040 measured)
latency change    = 2.03549
throughput change = 0.00167846
Class 0:
Packet latency average = 651.979
	minimum = 29
	maximum = 2629
Network latency average = 45.7307
	minimum = 23
	maximum = 1761
Slowest packet = 27927
Flit latency average = 2762.53
	minimum = 6
	maximum = 4770
Slowest flit = 34403
Fragmentation average = 9.77067
	minimum = 0
	maximum = 52
Injected packet rate average = 0.0443802
	minimum = 0.0135 (at node 0)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0103516
	minimum = 0.005 (at node 7)
	maximum = 0.017 (at node 187)
Injected flit rate average = 0.798664
	minimum = 0.2365 (at node 120)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.185924
	minimum = 0.09 (at node 105)
	maximum = 0.3025 (at node 187)
Injected packet length average = 17.996
Accepted packet length average = 17.961
Total in-flight flits = 612675 (299958 measured)
latency change    = 0.106776
throughput change = 0.00138665
Class 0:
Packet latency average = 893.217
	minimum = 29
	maximum = 3474
Network latency average = 116.6
	minimum = 23
	maximum = 2876
Slowest packet = 27927
Flit latency average = 3149.13
	minimum = 6
	maximum = 5712
Slowest flit = 44897
Fragmentation average = 9.88869
	minimum = 0
	maximum = 52
Injected packet rate average = 0.0434549
	minimum = 0.0136667 (at node 0)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0103038
	minimum = 0.006 (at node 132)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.782733
	minimum = 0.245667 (at node 120)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.185918
	minimum = 0.111333 (at node 132)
	maximum = 0.273 (at node 151)
Injected packet length average = 18.0125
Accepted packet length average = 18.0436
Total in-flight flits = 720783 (440470 measured)
latency change    = 0.270078
throughput change = 3.26831e-05
Class 0:
Packet latency average = 1462.56
	minimum = 29
	maximum = 4292
Network latency average = 524.198
	minimum = 23
	maximum = 3882
Slowest packet = 27927
Flit latency average = 3543.15
	minimum = 6
	maximum = 6384
Slowest flit = 104184
Fragmentation average = 10.2267
	minimum = 0
	maximum = 52
Injected packet rate average = 0.0371888
	minimum = 0.014 (at node 36)
	maximum = 0.05025 (at node 71)
Accepted packet rate average = 0.0102135
	minimum = 0.00625 (at node 36)
	maximum = 0.01525 (at node 157)
Injected flit rate average = 0.670109
	minimum = 0.25075 (at node 60)
	maximum = 0.90475 (at node 71)
Accepted flit rate average= 0.183665
	minimum = 0.1135 (at node 36)
	maximum = 0.2715 (at node 129)
Injected packet length average = 18.0191
Accepted packet length average = 17.9825
Total in-flight flits = 750375 (500136 measured)
latency change    = 0.38928
throughput change = 0.0122671
Class 0:
Packet latency average = 2045.07
	minimum = 29
	maximum = 5997
Network latency average = 1061.94
	minimum = 23
	maximum = 4847
Slowest packet = 27927
Flit latency average = 3947.33
	minimum = 6
	maximum = 7333
Slowest flit = 111734
Fragmentation average = 10.8271
	minimum = 0
	maximum = 96
Injected packet rate average = 0.032724
	minimum = 0.0138 (at node 60)
	maximum = 0.043 (at node 37)
Accepted packet rate average = 0.0101219
	minimum = 0.0072 (at node 36)
	maximum = 0.0144 (at node 157)
Injected flit rate average = 0.589629
	minimum = 0.2488 (at node 60)
	maximum = 0.7752 (at node 37)
Accepted flit rate average= 0.182167
	minimum = 0.1318 (at node 36)
	maximum = 0.2552 (at node 157)
Injected packet length average = 18.0183
Accepted packet length average = 17.9973
Total in-flight flits = 767886 (547136 measured)
latency change    = 0.284833
throughput change = 0.00822707
Class 0:
Packet latency average = 2659.83
	minimum = 29
	maximum = 6869
Network latency average = 1545.01
	minimum = 23
	maximum = 5744
Slowest packet = 27927
Flit latency average = 4323.35
	minimum = 6
	maximum = 8122
Slowest flit = 148582
Fragmentation average = 12.2879
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0297483
	minimum = 0.0138333 (at node 60)
	maximum = 0.0386667 (at node 83)
Accepted packet rate average = 0.0100486
	minimum = 0.00733333 (at node 36)
	maximum = 0.0128333 (at node 56)
Injected flit rate average = 0.536018
	minimum = 0.249333 (at node 60)
	maximum = 0.696 (at node 83)
Accepted flit rate average= 0.181114
	minimum = 0.132 (at node 36)
	maximum = 0.236667 (at node 129)
Injected packet length average = 18.0185
Accepted packet length average = 18.0238
Total in-flight flits = 785585 (593089 measured)
latency change    = 0.231127
throughput change = 0.00581376
Class 0:
Packet latency average = 3288.08
	minimum = 29
	maximum = 8326
Network latency average = 2022.59
	minimum = 23
	maximum = 6908
Slowest packet = 27927
Flit latency average = 4702.06
	minimum = 6
	maximum = 8987
Slowest flit = 173352
Fragmentation average = 14.97
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0276295
	minimum = 0.0137143 (at node 60)
	maximum = 0.035 (at node 83)
Accepted packet rate average = 0.0100186
	minimum = 0.00728571 (at node 126)
	maximum = 0.0137143 (at node 129)
Injected flit rate average = 0.497847
	minimum = 0.248571 (at node 60)
	maximum = 0.63 (at node 83)
Accepted flit rate average= 0.18024
	minimum = 0.131 (at node 126)
	maximum = 0.247857 (at node 129)
Injected packet length average = 18.0187
Accepted packet length average = 17.9906
Total in-flight flits = 803519 (638892 measured)
latency change    = 0.191071
throughput change = 0.00484569
Draining all recorded packets ...
Class 0:
Remaining flits: 174528 174529 174530 174531 174532 174533 174534 174535 174536 174537 [...] (820423 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (682946 flits)
Class 0:
Remaining flits: 175265 193392 193393 193394 193395 193396 193397 193398 193399 193400 [...] (837215 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (725552 flits)
Class 0:
Remaining flits: 202579 202580 202581 202582 202583 202584 202585 202586 202587 202588 [...] (855887 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (766418 flits)
Class 0:
Remaining flits: 225180 225181 225182 225183 225184 225185 225186 225187 225188 225189 [...] (874494 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (805038 flits)
Class 0:
Remaining flits: 245160 245161 245162 245163 245164 245165 245166 245167 245168 245169 [...] (892702 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (840985 flits)
Class 0:
Remaining flits: 273924 273925 273926 273927 273928 273929 273930 273931 273932 273933 [...] (909559 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (873042 flits)
Class 0:
Remaining flits: 293796 293797 293798 293799 293800 293801 293802 293803 293804 293805 [...] (927346 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (902924 flits)
Class 0:
Remaining flits: 293796 293797 293798 293799 293800 293801 293802 293803 293804 293805 [...] (943589 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (927976 flits)
Class 0:
Remaining flits: 321912 321913 321914 321915 321916 321917 321918 321919 321920 321921 [...] (957645 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (947174 flits)
Class 0:
Remaining flits: 321912 321913 321914 321915 321916 321917 321918 321919 321920 321921 [...] (970564 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (962289 flits)
Class 0:
Remaining flits: 373631 373632 373633 373634 373635 373636 373637 373638 373639 373640 [...] (977059 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (968339 flits)
Class 0:
Remaining flits: 386514 386515 386516 386517 386518 386519 386520 386521 386522 386523 [...] (981495 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (967019 flits)
Class 0:
Remaining flits: 413856 413857 413858 413859 413860 413861 413862 413863 413864 413865 [...] (981880 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (957457 flits)
Class 0:
Remaining flits: 430092 430093 430094 430095 430096 430097 430098 430099 430100 430101 [...] (980769 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (942951 flits)
Class 0:
Remaining flits: 465696 465697 465698 465699 465700 465701 465702 465703 465704 465705 [...] (983040 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (926844 flits)
Class 0:
Remaining flits: 465696 465697 465698 465699 465700 465701 465702 465703 465704 465705 [...] (977412 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (906078 flits)
Class 0:
Remaining flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (973527 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (885633 flits)
Class 0:
Remaining flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (971293 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (865736 flits)
Class 0:
Remaining flits: 549828 549829 549830 549831 549832 549833 549834 549835 549836 549837 [...] (967822 flits)
Measured flits: 549828 549829 549830 549831 549832 549833 549834 549835 549836 549837 [...] (844403 flits)
Class 0:
Remaining flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (965727 flits)
Measured flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (819819 flits)
Class 0:
Remaining flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (962781 flits)
Measured flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (795687 flits)
Class 0:
Remaining flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (961058 flits)
Measured flits: 555336 555337 555338 555339 555340 555341 555342 555343 555344 555345 [...] (773642 flits)
Class 0:
Remaining flits: 605232 605233 605234 605235 605236 605237 605238 605239 605240 605241 [...] (956913 flits)
Measured flits: 605232 605233 605234 605235 605236 605237 605238 605239 605240 605241 [...] (748209 flits)
Class 0:
Remaining flits: 605232 605233 605234 605235 605236 605237 605238 605239 605240 605241 [...] (951574 flits)
Measured flits: 605232 605233 605234 605235 605236 605237 605238 605239 605240 605241 [...] (722974 flits)
Class 0:
Remaining flits: 627282 627283 627284 627285 627286 627287 627288 627289 627290 627291 [...] (948714 flits)
Measured flits: 627282 627283 627284 627285 627286 627287 627288 627289 627290 627291 [...] (699270 flits)
Class 0:
Remaining flits: 627282 627283 627284 627285 627286 627287 627288 627289 627290 627291 [...] (945625 flits)
Measured flits: 627282 627283 627284 627285 627286 627287 627288 627289 627290 627291 [...] (674410 flits)
Class 0:
Remaining flits: 679500 679501 679502 679503 679504 679505 679506 679507 679508 679509 [...] (943194 flits)
Measured flits: 679500 679501 679502 679503 679504 679505 679506 679507 679508 679509 [...] (648354 flits)
Class 0:
Remaining flits: 745866 745867 745868 745869 745870 745871 745872 745873 745874 745875 [...] (939635 flits)
Measured flits: 745866 745867 745868 745869 745870 745871 745872 745873 745874 745875 [...] (623069 flits)
Class 0:
Remaining flits: 745866 745867 745868 745869 745870 745871 745872 745873 745874 745875 [...] (935356 flits)
Measured flits: 745866 745867 745868 745869 745870 745871 745872 745873 745874 745875 [...] (597879 flits)
Class 0:
Remaining flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (931210 flits)
Measured flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (571863 flits)
Class 0:
Remaining flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (930210 flits)
Measured flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (548179 flits)
Class 0:
Remaining flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (928208 flits)
Measured flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (522247 flits)
Class 0:
Remaining flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (924039 flits)
Measured flits: 803340 803341 803342 803343 803344 803345 803346 803347 803348 803349 [...] (494995 flits)
Class 0:
Remaining flits: 873036 873037 873038 873039 873040 873041 873042 873043 873044 873045 [...] (922255 flits)
Measured flits: 873036 873037 873038 873039 873040 873041 873042 873043 873044 873045 [...] (468249 flits)
Class 0:
Remaining flits: 873036 873037 873038 873039 873040 873041 873042 873043 873044 873045 [...] (919478 flits)
Measured flits: 873036 873037 873038 873039 873040 873041 873042 873043 873044 873045 [...] (440191 flits)
Class 0:
Remaining flits: 884178 884179 884180 884181 884182 884183 884184 884185 884186 884187 [...] (920118 flits)
Measured flits: 884178 884179 884180 884181 884182 884183 884184 884185 884186 884187 [...] (411712 flits)
Class 0:
Remaining flits: 933048 933049 933050 933051 933052 933053 933054 933055 933056 933057 [...] (917665 flits)
Measured flits: 933048 933049 933050 933051 933052 933053 933054 933055 933056 933057 [...] (383834 flits)
Class 0:
Remaining flits: 965790 965791 965792 965793 965794 965795 965796 965797 965798 965799 [...] (913532 flits)
Measured flits: 965790 965791 965792 965793 965794 965795 965796 965797 965798 965799 [...] (355604 flits)
Class 0:
Remaining flits: 974412 974413 974414 974415 974416 974417 974418 974419 974420 974421 [...] (910004 flits)
Measured flits: 974412 974413 974414 974415 974416 974417 974418 974419 974420 974421 [...] (327557 flits)
Class 0:
Remaining flits: 974412 974413 974414 974415 974416 974417 974418 974419 974420 974421 [...] (906416 flits)
Measured flits: 974412 974413 974414 974415 974416 974417 974418 974419 974420 974421 [...] (299953 flits)
Class 0:
Remaining flits: 1027512 1027513 1027514 1027515 1027516 1027517 1027518 1027519 1027520 1027521 [...] (901792 flits)
Measured flits: 1027512 1027513 1027514 1027515 1027516 1027517 1027518 1027519 1027520 1027521 [...] (272413 flits)
Class 0:
Remaining flits: 1027522 1027523 1027524 1027525 1027526 1027527 1027528 1027529 1049274 1049275 [...] (897742 flits)
Measured flits: 1027522 1027523 1027524 1027525 1027526 1027527 1027528 1027529 1049274 1049275 [...] (244988 flits)
Class 0:
Remaining flits: 1049290 1049291 1108926 1108927 1108928 1108929 1108930 1108931 1108932 1108933 [...] (897645 flits)
Measured flits: 1049290 1049291 1108926 1108927 1108928 1108929 1108930 1108931 1108932 1108933 [...] (219389 flits)
Class 0:
Remaining flits: 1108926 1108927 1108928 1108929 1108930 1108931 1108932 1108933 1108934 1108935 [...] (899534 flits)
Measured flits: 1108926 1108927 1108928 1108929 1108930 1108931 1108932 1108933 1108934 1108935 [...] (195623 flits)
Class 0:
Remaining flits: 1108940 1108941 1108942 1108943 1129212 1129213 1129214 1129215 1129216 1129217 [...] (898014 flits)
Measured flits: 1108940 1108941 1108942 1108943 1129212 1129213 1129214 1129215 1129216 1129217 [...] (172679 flits)
Class 0:
Remaining flits: 1129212 1129213 1129214 1129215 1129216 1129217 1129218 1129219 1129220 1129221 [...] (894769 flits)
Measured flits: 1129212 1129213 1129214 1129215 1129216 1129217 1129218 1129219 1129220 1129221 [...] (151530 flits)
Class 0:
Remaining flits: 1148310 1148311 1148312 1148313 1148314 1148315 1148316 1148317 1148318 1148319 [...] (895404 flits)
Measured flits: 1148310 1148311 1148312 1148313 1148314 1148315 1148316 1148317 1148318 1148319 [...] (130861 flits)
Class 0:
Remaining flits: 1148310 1148311 1148312 1148313 1148314 1148315 1148316 1148317 1148318 1148319 [...] (894756 flits)
Measured flits: 1148310 1148311 1148312 1148313 1148314 1148315 1148316 1148317 1148318 1148319 [...] (112852 flits)
Class 0:
Remaining flits: 1192608 1192609 1192610 1192611 1192612 1192613 1192614 1192615 1192616 1192617 [...] (892517 flits)
Measured flits: 1192608 1192609 1192610 1192611 1192612 1192613 1192614 1192615 1192616 1192617 [...] (95440 flits)
Class 0:
Remaining flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (892071 flits)
Measured flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (80129 flits)
Class 0:
Remaining flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (889971 flits)
Measured flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (66429 flits)
Class 0:
Remaining flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (889182 flits)
Measured flits: 1253988 1253989 1253990 1253991 1253992 1253993 1253994 1253995 1253996 1253997 [...] (54297 flits)
Class 0:
Remaining flits: 1294398 1294399 1294400 1294401 1294402 1294403 1294404 1294405 1294406 1294407 [...] (886666 flits)
Measured flits: 1294398 1294399 1294400 1294401 1294402 1294403 1294404 1294405 1294406 1294407 [...] (44949 flits)
Class 0:
Remaining flits: 1351566 1351567 1351568 1351569 1351570 1351571 1351572 1351573 1351574 1351575 [...] (886089 flits)
Measured flits: 1351566 1351567 1351568 1351569 1351570 1351571 1351572 1351573 1351574 1351575 [...] (35946 flits)
Class 0:
Remaining flits: 1351566 1351567 1351568 1351569 1351570 1351571 1351572 1351573 1351574 1351575 [...] (884093 flits)
Measured flits: 1351566 1351567 1351568 1351569 1351570 1351571 1351572 1351573 1351574 1351575 [...] (28413 flits)
Class 0:
Remaining flits: 1375020 1375021 1375022 1375023 1375024 1375025 1375026 1375027 1375028 1375029 [...] (880475 flits)
Measured flits: 1375020 1375021 1375022 1375023 1375024 1375025 1375026 1375027 1375028 1375029 [...] (22310 flits)
Class 0:
Remaining flits: 1399338 1399339 1399340 1399341 1399342 1399343 1399344 1399345 1399346 1399347 [...] (877929 flits)
Measured flits: 1399338 1399339 1399340 1399341 1399342 1399343 1399344 1399345 1399346 1399347 [...] (17100 flits)
Class 0:
Remaining flits: 1422419 1422420 1422421 1422422 1422423 1422424 1422425 1422426 1422427 1422428 [...] (877727 flits)
Measured flits: 1422419 1422420 1422421 1422422 1422423 1422424 1422425 1422426 1422427 1422428 [...] (13070 flits)
Class 0:
Remaining flits: 1457208 1457209 1457210 1457211 1457212 1457213 1457214 1457215 1457216 1457217 [...] (874921 flits)
Measured flits: 1457208 1457209 1457210 1457211 1457212 1457213 1457214 1457215 1457216 1457217 [...] (10037 flits)
Class 0:
Remaining flits: 1470186 1470187 1470188 1470189 1470190 1470191 1470192 1470193 1470194 1470195 [...] (871473 flits)
Measured flits: 1470186 1470187 1470188 1470189 1470190 1470191 1470192 1470193 1470194 1470195 [...] (7673 flits)
Class 0:
Remaining flits: 1525194 1525195 1525196 1525197 1525198 1525199 1525200 1525201 1525202 1525203 [...] (867856 flits)
Measured flits: 1525194 1525195 1525196 1525197 1525198 1525199 1525200 1525201 1525202 1525203 [...] (5514 flits)
Class 0:
Remaining flits: 1566666 1566667 1566668 1566669 1566670 1566671 1566672 1566673 1566674 1566675 [...] (866669 flits)
Measured flits: 1566666 1566667 1566668 1566669 1566670 1566671 1566672 1566673 1566674 1566675 [...] (3946 flits)
Class 0:
Remaining flits: 1577142 1577143 1577144 1577145 1577146 1577147 1577148 1577149 1577150 1577151 [...] (862189 flits)
Measured flits: 1577142 1577143 1577144 1577145 1577146 1577147 1577148 1577149 1577150 1577151 [...] (2994 flits)
Class 0:
Remaining flits: 1577142 1577143 1577144 1577145 1577146 1577147 1577148 1577149 1577150 1577151 [...] (859901 flits)
Measured flits: 1577142 1577143 1577144 1577145 1577146 1577147 1577148 1577149 1577150 1577151 [...] (2145 flits)
Class 0:
Remaining flits: 1671462 1671463 1671464 1671465 1671466 1671467 1671468 1671469 1671470 1671471 [...] (859650 flits)
Measured flits: 1671462 1671463 1671464 1671465 1671466 1671467 1671468 1671469 1671470 1671471 [...] (1550 flits)
Class 0:
Remaining flits: 1679868 1679869 1679870 1679871 1679872 1679873 1679874 1679875 1679876 1679877 [...] (859310 flits)
Measured flits: 1679868 1679869 1679870 1679871 1679872 1679873 1679874 1679875 1679876 1679877 [...] (1038 flits)
Class 0:
Remaining flits: 1679868 1679869 1679870 1679871 1679872 1679873 1679874 1679875 1679876 1679877 [...] (857029 flits)
Measured flits: 1679868 1679869 1679870 1679871 1679872 1679873 1679874 1679875 1679876 1679877 [...] (684 flits)
Class 0:
Remaining flits: 1743102 1743103 1743104 1743105 1743106 1743107 1743108 1743109 1743110 1743111 [...] (854218 flits)
Measured flits: 1743102 1743103 1743104 1743105 1743106 1743107 1743108 1743109 1743110 1743111 [...] (435 flits)
Class 0:
Remaining flits: 1743102 1743103 1743104 1743105 1743106 1743107 1743108 1743109 1743110 1743111 [...] (853098 flits)
Measured flits: 1743102 1743103 1743104 1743105 1743106 1743107 1743108 1743109 1743110 1743111 [...] (270 flits)
Class 0:
Remaining flits: 1753380 1753381 1753382 1753383 1753384 1753385 1753386 1753387 1753388 1753389 [...] (851726 flits)
Measured flits: 1753380 1753381 1753382 1753383 1753384 1753385 1753386 1753387 1753388 1753389 [...] (144 flits)
Class 0:
Remaining flits: 1755558 1755559 1755560 1755561 1755562 1755563 1755564 1755565 1755566 1755567 [...] (849540 flits)
Measured flits: 1949904 1949905 1949906 1949907 1949908 1949909 1949910 1949911 1949912 1949913 [...] (72 flits)
Class 0:
Remaining flits: 1837782 1837783 1837784 1837785 1837786 1837787 1837788 1837789 1837790 1837791 [...] (848302 flits)
Measured flits: 2550834 2550835 2550836 2550837 2550838 2550839 2550840 2550841 2550842 2550843 [...] (36 flits)
Class 0:
Remaining flits: 1837782 1837783 1837784 1837785 1837786 1837787 1837788 1837789 1837790 1837791 [...] (844482 flits)
Measured flits: 2586572 2586573 2586574 2586575 2586576 2586577 2586578 2586579 2586580 2586581 (10 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1848024 1848025 1848026 1848027 1848028 1848029 1848030 1848031 1848032 1848033 [...] (813490 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888866 1888867 1888868 1888869 1888870 1888871 1888872 1888873 1888874 1888875 [...] (782399 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1899000 1899001 1899002 1899003 1899004 1899005 1899006 1899007 1899008 1899009 [...] (752042 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1961033 1961034 1961035 1961036 1961037 1961038 1961039 1961040 1961041 1961042 [...] (720691 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2026710 2026711 2026712 2026713 2026714 2026715 2026716 2026717 2026718 2026719 [...] (689895 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084994 2084995 2084996 2084997 2084998 2084999 2085000 2085001 2085002 2085003 [...] (658216 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2100078 2100079 2100080 2100081 2100082 2100083 2100084 2100085 2100086 2100087 [...] (627178 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2100078 2100079 2100080 2100081 2100082 2100083 2100084 2100085 2100086 2100087 [...] (596391 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2116908 2116909 2116910 2116911 2116912 2116913 2116914 2116915 2116916 2116917 [...] (564473 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2116908 2116909 2116910 2116911 2116912 2116913 2116914 2116915 2116916 2116917 [...] (532793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2119716 2119717 2119718 2119719 2119720 2119721 2119722 2119723 2119724 2119725 [...] (501749 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2170044 2170045 2170046 2170047 2170048 2170049 2170050 2170051 2170052 2170053 [...] (469794 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2249730 2249731 2249732 2249733 2249734 2249735 2249736 2249737 2249738 2249739 [...] (439366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2249730 2249731 2249732 2249733 2249734 2249735 2249736 2249737 2249738 2249739 [...] (408117 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269422 2269423 2269424 2269425 2269426 2269427 2269428 2269429 2269430 2269431 [...] (378032 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269422 2269423 2269424 2269425 2269426 2269427 2269428 2269429 2269430 2269431 [...] (347599 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269422 2269423 2269424 2269425 2269426 2269427 2269428 2269429 2269430 2269431 [...] (317588 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269422 2269423 2269424 2269425 2269426 2269427 2269428 2269429 2269430 2269431 [...] (288206 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2381562 2381563 2381564 2381565 2381566 2381567 2381568 2381569 2381570 2381571 [...] (259373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2433600 2433601 2433602 2433603 2433604 2433605 2433606 2433607 2433608 2433609 [...] (230481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2448126 2448127 2448128 2448129 2448130 2448131 2448132 2448133 2448134 2448135 [...] (201421 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2465622 2465623 2465624 2465625 2465626 2465627 2465628 2465629 2465630 2465631 [...] (172512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2493684 2493685 2493686 2493687 2493688 2493689 2493690 2493691 2493692 2493693 [...] (144484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2493684 2493685 2493686 2493687 2493688 2493689 2493690 2493691 2493692 2493693 [...] (115767 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2510766 2510767 2510768 2510769 2510770 2510771 2510772 2510773 2510774 2510775 [...] (87700 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2558484 2558485 2558486 2558487 2558488 2558489 2558490 2558491 2558492 2558493 [...] (60816 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2787480 2787481 2787482 2787483 2787484 2787485 2787486 2787487 2787488 2787489 [...] (36648 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2887358 2887359 2887360 2887361 2893590 2893591 2893592 2893593 2893594 2893595 [...] (15720 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2905758 2905759 2905760 2905761 2905762 2905763 2905764 2905765 2905766 2905767 [...] (4796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3216114 3216115 3216116 3216117 3216118 3216119 3216120 3216121 3216122 3216123 [...] (707 flits)
Measured flits: (0 flits)
Time taken is 113456 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 30354.6 (1 samples)
	minimum = 29 (1 samples)
	maximum = 73203 (1 samples)
Network latency average = 23770.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 58311 (1 samples)
Flit latency average = 24260.4 (1 samples)
	minimum = 6 (1 samples)
	maximum = 61702 (1 samples)
Fragmentation average = 72.2149 (1 samples)
	minimum = 0 (1 samples)
	maximum = 189 (1 samples)
Injected packet rate average = 0.0276295 (1 samples)
	minimum = 0.0137143 (1 samples)
	maximum = 0.035 (1 samples)
Accepted packet rate average = 0.0100186 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0137143 (1 samples)
Injected flit rate average = 0.497847 (1 samples)
	minimum = 0.248571 (1 samples)
	maximum = 0.63 (1 samples)
Accepted flit rate average = 0.18024 (1 samples)
	minimum = 0.131 (1 samples)
	maximum = 0.247857 (1 samples)
Injected packet size average = 18.0187 (1 samples)
Accepted packet size average = 17.9906 (1 samples)
Hops average = 5.04463 (1 samples)
Total run time 93.9599
