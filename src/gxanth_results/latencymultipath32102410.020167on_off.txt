BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.49
	minimum = 25
	maximum = 980
Network latency average = 248.752
	minimum = 23
	maximum = 947
Slowest packet = 14
Flit latency average = 182.266
	minimum = 6
	maximum = 973
Slowest flit = 70
Fragmentation average = 134.252
	minimum = 0
	maximum = 803
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.00930729
	minimum = 0.002 (at node 43)
	maximum = 0.018 (at node 103)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.191807
	minimum = 0.072 (at node 43)
	maximum = 0.33 (at node 103)
Injected packet length average = 17.8591
Accepted packet length average = 20.6083
Total in-flight flits = 30511 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 511.404
	minimum = 23
	maximum = 1958
Network latency average = 423.597
	minimum = 23
	maximum = 1730
Slowest packet = 169
Flit latency average = 341.43
	minimum = 6
	maximum = 1915
Slowest flit = 5620
Fragmentation average = 175.783
	minimum = 0
	maximum = 1425
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0105312
	minimum = 0.006 (at node 57)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.203589
	minimum = 0.121 (at node 108)
	maximum = 0.309 (at node 152)
Injected packet length average = 17.9339
Accepted packet length average = 19.3318
Total in-flight flits = 56264 (0 measured)
latency change    = 0.375269
throughput change = 0.0578679
Class 0:
Packet latency average = 989.285
	minimum = 27
	maximum = 2727
Network latency average = 876.14
	minimum = 27
	maximum = 2640
Slowest packet = 798
Flit latency average = 790.683
	minimum = 6
	maximum = 2903
Slowest flit = 5488
Fragmentation average = 222.171
	minimum = 0
	maximum = 1849
Injected packet rate average = 0.0207448
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0125521
	minimum = 0.004 (at node 52)
	maximum = 0.022 (at node 78)
Injected flit rate average = 0.372271
	minimum = 0 (at node 7)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.226198
	minimum = 0.078 (at node 36)
	maximum = 0.409 (at node 133)
Injected packet length average = 17.9453
Accepted packet length average = 18.0207
Total in-flight flits = 84528 (0 measured)
latency change    = 0.483057
throughput change = 0.0999539
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 334.597
	minimum = 27
	maximum = 1132
Network latency average = 248.885
	minimum = 23
	maximum = 987
Slowest packet = 11465
Flit latency average = 1087.93
	minimum = 6
	maximum = 3532
Slowest flit = 29518
Fragmentation average = 87.6314
	minimum = 0
	maximum = 633
Injected packet rate average = 0.020276
	minimum = 0 (at node 77)
	maximum = 0.055 (at node 34)
Accepted packet rate average = 0.0123594
	minimum = 0.006 (at node 20)
	maximum = 0.023 (at node 33)
Injected flit rate average = 0.36649
	minimum = 0 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.220115
	minimum = 0.088 (at node 75)
	maximum = 0.371 (at node 28)
Injected packet length average = 18.075
Accepted packet length average = 17.8095
Total in-flight flits = 112340 (59266 measured)
latency change    = 1.95664
throughput change = 0.0276371
Class 0:
Packet latency average = 667.692
	minimum = 27
	maximum = 2090
Network latency average = 574.267
	minimum = 23
	maximum = 1978
Slowest packet = 11465
Flit latency average = 1267.89
	minimum = 6
	maximum = 4664
Slowest flit = 8462
Fragmentation average = 133.038
	minimum = 0
	maximum = 1104
Injected packet rate average = 0.0198438
	minimum = 0.001 (at node 191)
	maximum = 0.051 (at node 11)
Accepted packet rate average = 0.012263
	minimum = 0.0065 (at node 36)
	maximum = 0.0185 (at node 33)
Injected flit rate average = 0.357513
	minimum = 0.018 (at node 191)
	maximum = 0.918 (at node 11)
Accepted flit rate average= 0.219112
	minimum = 0.1175 (at node 2)
	maximum = 0.3275 (at node 128)
Injected packet length average = 18.0164
Accepted packet length average = 17.8677
Total in-flight flits = 137549 (104590 measured)
latency change    = 0.498874
throughput change = 0.00457576
Class 0:
Packet latency average = 1004.54
	minimum = 27
	maximum = 3447
Network latency average = 907.528
	minimum = 23
	maximum = 2839
Slowest packet = 11465
Flit latency average = 1442.02
	minimum = 6
	maximum = 5864
Slowest flit = 9509
Fragmentation average = 155.214
	minimum = 0
	maximum = 1342
Injected packet rate average = 0.0199097
	minimum = 0.004 (at node 131)
	maximum = 0.042 (at node 55)
Accepted packet rate average = 0.0121719
	minimum = 0.00733333 (at node 36)
	maximum = 0.0173333 (at node 33)
Injected flit rate average = 0.358792
	minimum = 0.072 (at node 131)
	maximum = 0.756 (at node 55)
Accepted flit rate average= 0.218602
	minimum = 0.133 (at node 36)
	maximum = 0.313667 (at node 90)
Injected packet length average = 18.0209
Accepted packet length average = 17.9596
Total in-flight flits = 165037 (144171 measured)
latency change    = 0.335323
throughput change = 0.00233094
Class 0:
Packet latency average = 1295.14
	minimum = 27
	maximum = 4426
Network latency average = 1195.12
	minimum = 23
	maximum = 3965
Slowest packet = 11465
Flit latency average = 1606.29
	minimum = 6
	maximum = 6483
Slowest flit = 21330
Fragmentation average = 166.427
	minimum = 0
	maximum = 2514
Injected packet rate average = 0.0197734
	minimum = 0.0075 (at node 44)
	maximum = 0.04425 (at node 63)
Accepted packet rate average = 0.0121419
	minimum = 0.0075 (at node 2)
	maximum = 0.0165 (at node 51)
Injected flit rate average = 0.356009
	minimum = 0.133 (at node 44)
	maximum = 0.7945 (at node 63)
Accepted flit rate average= 0.217374
	minimum = 0.13775 (at node 2)
	maximum = 0.30075 (at node 66)
Injected packet length average = 18.0044
Accepted packet length average = 17.9027
Total in-flight flits = 190933 (177691 measured)
latency change    = 0.224381
throughput change = 0.00565263
Class 0:
Packet latency average = 1559.57
	minimum = 27
	maximum = 5562
Network latency average = 1455.55
	minimum = 23
	maximum = 4977
Slowest packet = 11465
Flit latency average = 1773.86
	minimum = 6
	maximum = 7653
Slowest flit = 14207
Fragmentation average = 172.533
	minimum = 0
	maximum = 3026
Injected packet rate average = 0.0197396
	minimum = 0.0096 (at node 15)
	maximum = 0.0432 (at node 95)
Accepted packet rate average = 0.0120729
	minimum = 0.0074 (at node 2)
	maximum = 0.0162 (at node 66)
Injected flit rate average = 0.355252
	minimum = 0.1722 (at node 15)
	maximum = 0.7746 (at node 95)
Accepted flit rate average= 0.216832
	minimum = 0.134 (at node 2)
	maximum = 0.2858 (at node 66)
Injected packet length average = 17.9969
Accepted packet length average = 17.9602
Total in-flight flits = 217469 (209283 measured)
latency change    = 0.169551
throughput change = 0.00249689
Class 0:
Packet latency average = 1836.53
	minimum = 27
	maximum = 6525
Network latency average = 1730.65
	minimum = 23
	maximum = 5923
Slowest packet = 11465
Flit latency average = 1949.76
	minimum = 6
	maximum = 8092
Slowest flit = 31895
Fragmentation average = 178.454
	minimum = 0
	maximum = 3026
Injected packet rate average = 0.0199583
	minimum = 0.0105 (at node 56)
	maximum = 0.0436667 (at node 95)
Accepted packet rate average = 0.0120833
	minimum = 0.008 (at node 2)
	maximum = 0.0153333 (at node 66)
Injected flit rate average = 0.359327
	minimum = 0.189 (at node 56)
	maximum = 0.786 (at node 95)
Accepted flit rate average= 0.21646
	minimum = 0.143833 (at node 2)
	maximum = 0.276167 (at node 90)
Injected packet length average = 18.0039
Accepted packet length average = 17.9139
Total in-flight flits = 249022 (243971 measured)
latency change    = 0.15081
throughput change = 0.00171959
Class 0:
Packet latency average = 2080.75
	minimum = 25
	maximum = 7566
Network latency average = 1972.91
	minimum = 23
	maximum = 6843
Slowest packet = 11465
Flit latency average = 2114.22
	minimum = 6
	maximum = 8624
Slowest flit = 92650
Fragmentation average = 178.919
	minimum = 0
	maximum = 3123
Injected packet rate average = 0.0201436
	minimum = 0.009 (at node 118)
	maximum = 0.042 (at node 95)
Accepted packet rate average = 0.0120387
	minimum = 0.00885714 (at node 5)
	maximum = 0.0154286 (at node 90)
Injected flit rate average = 0.362649
	minimum = 0.164286 (at node 118)
	maximum = 0.755571 (at node 95)
Accepted flit rate average= 0.2158
	minimum = 0.159857 (at node 5)
	maximum = 0.276 (at node 90)
Injected packet length average = 18.0032
Accepted packet length average = 17.9255
Total in-flight flits = 281807 (278375 measured)
latency change    = 0.117368
throughput change = 0.0030594
Draining all recorded packets ...
Class 0:
Remaining flits: 78714 78715 78716 78717 78718 78719 78720 78721 78722 78723 [...] (311343 flits)
Measured flits: 206473 206474 206475 206476 206477 206550 206551 206552 206553 206554 [...] (253586 flits)
Class 0:
Remaining flits: 94722 94723 94724 94725 94726 94727 94728 94729 94730 94731 [...] (341084 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (221365 flits)
Class 0:
Remaining flits: 130572 130573 130574 130575 130576 130577 130578 130579 130580 130581 [...] (366639 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (191462 flits)
Class 0:
Remaining flits: 139356 139357 139358 139359 139360 139361 139362 139363 139364 139365 [...] (397624 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (164356 flits)
Class 0:
Remaining flits: 139356 139357 139358 139359 139360 139361 139362 139363 139364 139365 [...] (426215 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (139601 flits)
Class 0:
Remaining flits: 139356 139357 139358 139359 139360 139361 139362 139363 139364 139365 [...] (452512 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (117570 flits)
Class 0:
Remaining flits: 139356 139357 139358 139359 139360 139361 139362 139363 139364 139365 [...] (478314 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (98285 flits)
Class 0:
Remaining flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (509342 flits)
Measured flits: 206838 206839 206840 206841 206842 206843 206844 206845 206846 206847 [...] (81943 flits)
Class 0:
Remaining flits: 219312 219313 219314 219315 219316 219317 219318 219319 219320 219321 [...] (537591 flits)
Measured flits: 219312 219313 219314 219315 219316 219317 219318 219319 219320 219321 [...] (67908 flits)
Class 0:
Remaining flits: 226710 226711 226712 226713 226714 226715 226716 226717 226718 226719 [...] (569159 flits)
Measured flits: 226710 226711 226712 226713 226714 226715 226716 226717 226718 226719 [...] (55351 flits)
Class 0:
Remaining flits: 228658 228659 228660 228661 228662 228663 228664 228665 228666 228667 [...] (598275 flits)
Measured flits: 228658 228659 228660 228661 228662 228663 228664 228665 228666 228667 [...] (45697 flits)
Class 0:
Remaining flits: 250506 250507 250508 250509 250510 250511 250512 250513 250514 250515 [...] (629539 flits)
Measured flits: 250506 250507 250508 250509 250510 250511 250512 250513 250514 250515 [...] (37864 flits)
Class 0:
Remaining flits: 250506 250507 250508 250509 250510 250511 250512 250513 250514 250515 [...] (664519 flits)
Measured flits: 250506 250507 250508 250509 250510 250511 250512 250513 250514 250515 [...] (30674 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (695051 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (24467 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (726812 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (19799 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (759082 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (16193 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (784571 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (13399 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (816854 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (10784 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (849574 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (8480 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (883565 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (7042 flits)
Class 0:
Remaining flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (914919 flits)
Measured flits: 291762 291763 291764 291765 291766 291767 291768 291769 291770 291771 [...] (5557 flits)
Class 0:
Remaining flits: 332415 332416 332417 332418 332419 332420 332421 332422 332423 338637 [...] (944826 flits)
Measured flits: 332415 332416 332417 332418 332419 332420 332421 332422 332423 338637 [...] (4396 flits)
Class 0:
Remaining flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (982649 flits)
Measured flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (3449 flits)
Class 0:
Remaining flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1012573 flits)
Measured flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (2699 flits)
Class 0:
Remaining flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1041056 flits)
Measured flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (2028 flits)
Class 0:
Remaining flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1073139 flits)
Measured flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1685 flits)
Class 0:
Remaining flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1102012 flits)
Measured flits: 442062 442063 442064 442065 442066 442067 442068 442069 442070 442071 [...] (1381 flits)
Class 0:
Remaining flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (1129024 flits)
Measured flits: 443448 443449 443450 443451 443452 443453 443454 443455 443456 443457 [...] (962 flits)
Class 0:
Remaining flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (1158824 flits)
Measured flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (714 flits)
Class 0:
Remaining flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (1185941 flits)
Measured flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (628 flits)
Class 0:
Remaining flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (1217917 flits)
Measured flits: 543024 543025 543026 543027 543028 543029 543030 543031 543032 543033 [...] (413 flits)
Class 0:
Remaining flits: 575496 575497 575498 575499 575500 575501 575502 575503 575504 575505 [...] (1244792 flits)
Measured flits: 575496 575497 575498 575499 575500 575501 575502 575503 575504 575505 [...] (334 flits)
Class 0:
Remaining flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (1275746 flits)
Measured flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (264 flits)
Class 0:
Remaining flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (1301200 flits)
Measured flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (198 flits)
Class 0:
Remaining flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (1327846 flits)
Measured flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (169 flits)
Class 0:
Remaining flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (1357266 flits)
Measured flits: 621900 621901 621902 621903 621904 621905 621906 621907 621908 621909 [...] (126 flits)
Class 0:
Remaining flits: 639540 639541 639542 639543 639544 639545 639546 639547 639548 639549 [...] (1383966 flits)
Measured flits: 639540 639541 639542 639543 639544 639545 639546 639547 639548 639549 [...] (90 flits)
Class 0:
Remaining flits: 639540 639541 639542 639543 639544 639545 639546 639547 639548 639549 [...] (1412038 flits)
Measured flits: 639540 639541 639542 639543 639544 639545 639546 639547 639548 639549 [...] (72 flits)
Class 0:
Remaining flits: 681444 681445 681446 681447 681448 681449 681450 681451 681452 681453 [...] (1436763 flits)
Measured flits: 681444 681445 681446 681447 681448 681449 681450 681451 681452 681453 [...] (18 flits)
Class 0:
Remaining flits: 681444 681445 681446 681447 681448 681449 681450 681451 681452 681453 [...] (1462781 flits)
Measured flits: 681444 681445 681446 681447 681448 681449 681450 681451 681452 681453 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 774972 774973 774974 774975 774976 774977 774978 774979 774980 774981 [...] (1435664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 774972 774973 774974 774975 774976 774977 774978 774979 774980 774981 [...] (1401808 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849546 849547 849548 849549 849550 849551 849552 849553 849554 849555 [...] (1368004 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849546 849547 849548 849549 849550 849551 849552 849553 849554 849555 [...] (1334956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849546 849547 849548 849549 849550 849551 849552 849553 849554 849555 [...] (1301475 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849546 849547 849548 849549 849550 849551 849552 849553 849554 849555 [...] (1268590 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849546 849547 849548 849549 849550 849551 849552 849553 849554 849555 [...] (1235665 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 919260 919261 919262 919263 919264 919265 919266 919267 919268 919269 [...] (1202287 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 919261 919262 919263 919264 919265 919266 919267 919268 919269 919270 [...] (1169351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 919261 919262 919263 919264 919265 919266 919267 919268 919269 919270 [...] (1135494 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1016251 1016252 1016253 1016254 1016255 1016256 1016257 1016258 1016259 1016260 [...] (1101524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1026468 1026469 1026470 1026471 1026472 1026473 1026474 1026475 1026476 1026477 [...] (1067498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1026468 1026469 1026470 1026471 1026472 1026473 1026474 1026475 1026476 1026477 [...] (1033305 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1026468 1026469 1026470 1026471 1026472 1026473 1026474 1026475 1026476 1026477 [...] (999804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1026468 1026469 1026470 1026471 1026472 1026473 1026474 1026475 1026476 1026477 [...] (965822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196694 1196695 1196696 1196697 1196698 1196699 1196700 1196701 1196702 1196703 [...] (932558 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196694 1196695 1196696 1196697 1196698 1196699 1196700 1196701 1196702 1196703 [...] (898987 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196694 1196695 1196696 1196697 1196698 1196699 1196700 1196701 1196702 1196703 [...] (865908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196694 1196695 1196696 1196697 1196698 1196699 1196700 1196701 1196702 1196703 [...] (833031 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196694 1196695 1196696 1196697 1196698 1196699 1196700 1196701 1196702 1196703 [...] (800419 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220742 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 [...] (767451 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220742 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 [...] (733977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220742 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 [...] (700498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220742 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 [...] (667755 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220742 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 [...] (635957 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1220743 1220744 1220745 1220746 1220747 1220748 1220749 1220750 1220751 1220752 [...] (603411 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1225764 1225765 1225766 1225767 1225768 1225769 1225770 1225771 1225772 1225773 [...] (570016 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1267470 1267471 1267472 1267473 1267474 1267475 1267476 1267477 1267478 1267479 [...] (536487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1267472 1267473 1267474 1267475 1267476 1267477 1267478 1267479 1267480 1267481 [...] (502943 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337760 1337761 1337762 1337763 1337764 1337765 1337766 1337767 1337768 1337769 [...] (469314 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337760 1337761 1337762 1337763 1337764 1337765 1337766 1337767 1337768 1337769 [...] (435441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337760 1337761 1337762 1337763 1337764 1337765 1337766 1337767 1337768 1337769 [...] (401789 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337760 1337761 1337762 1337763 1337764 1337765 1337766 1337767 1337768 1337769 [...] (368258 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1531818 1531819 1531820 1531821 1531822 1531823 1531824 1531825 1531826 1531827 [...] (334907 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1558548 1558549 1558550 1558551 1558552 1558553 1558554 1558555 1558556 1558557 [...] (301600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1558548 1558549 1558550 1558551 1558552 1558553 1558554 1558555 1558556 1558557 [...] (268536 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1558555 1558556 1558557 1558558 1558559 1558560 1558561 1558562 1558563 1558564 [...] (236320 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1584288 1584289 1584290 1584291 1584292 1584293 1584294 1584295 1584296 1584297 [...] (204124 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1584288 1584289 1584290 1584291 1584292 1584293 1584294 1584295 1584296 1584297 [...] (172504 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1584288 1584289 1584290 1584291 1584292 1584293 1584294 1584295 1584296 1584297 [...] (142784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1588572 1588573 1588574 1588575 1588576 1588577 1588578 1588579 1588580 1588581 [...] (113784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1604106 1604107 1604108 1604109 1604110 1604111 1604112 1604113 1604114 1604115 [...] (85869 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1606698 1606699 1606700 1606701 1606702 1606703 1606704 1606705 1606706 1606707 [...] (59915 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1606698 1606699 1606700 1606701 1606702 1606703 1606704 1606705 1606706 1606707 [...] (39798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1606698 1606699 1606700 1606701 1606702 1606703 1606704 1606705 1606706 1606707 [...] (22820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1820251 1820252 1820253 1820254 1820255 1820256 1820257 1820258 1820259 1820260 [...] (10385 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2053908 2053909 2053910 2053911 2053912 2053913 2053914 2053915 2053916 2053917 [...] (3767 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2519676 2519677 2519678 2519679 2519680 2519681 2519682 2519683 2519684 2519685 [...] (611 flits)
Measured flits: (0 flits)
Time taken is 98732 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5986.42 (1 samples)
	minimum = 25 (1 samples)
	maximum = 40629 (1 samples)
Network latency average = 5861.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 40512 (1 samples)
Flit latency average = 20884.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 72355 (1 samples)
Fragmentation average = 203.958 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4140 (1 samples)
Injected packet rate average = 0.0201436 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.042 (1 samples)
Accepted packet rate average = 0.0120387 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.362649 (1 samples)
	minimum = 0.164286 (1 samples)
	maximum = 0.755571 (1 samples)
Accepted flit rate average = 0.2158 (1 samples)
	minimum = 0.159857 (1 samples)
	maximum = 0.276 (1 samples)
Injected packet size average = 18.0032 (1 samples)
Accepted packet size average = 17.9255 (1 samples)
Hops average = 5.08792 (1 samples)
Total run time 107.984
