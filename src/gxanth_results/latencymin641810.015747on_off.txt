BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 273.948
	minimum = 23
	maximum = 968
Network latency average = 219.208
	minimum = 23
	maximum = 888
Slowest packet = 90
Flit latency average = 144.754
	minimum = 6
	maximum = 952
Slowest flit = 3430
Fragmentation average = 136.535
	minimum = 0
	maximum = 747
Injected packet rate average = 0.0146458
	minimum = 0 (at node 59)
	maximum = 0.042 (at node 106)
Accepted packet rate average = 0.00874479
	minimum = 0.001 (at node 174)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.260786
	minimum = 0 (at node 59)
	maximum = 0.754 (at node 106)
Accepted flit rate average= 0.180214
	minimum = 0.041 (at node 174)
	maximum = 0.345 (at node 44)
Injected packet length average = 17.8062
Accepted packet length average = 20.6081
Total in-flight flits = 16033 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 404.032
	minimum = 23
	maximum = 1759
Network latency average = 329.793
	minimum = 23
	maximum = 1639
Slowest packet = 257
Flit latency average = 235.818
	minimum = 6
	maximum = 1949
Slowest flit = 3782
Fragmentation average = 183.009
	minimum = 0
	maximum = 1078
Injected packet rate average = 0.0141094
	minimum = 0.0005 (at node 73)
	maximum = 0.032 (at node 135)
Accepted packet rate average = 0.00973437
	minimum = 0.0045 (at node 30)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.252536
	minimum = 0.009 (at node 73)
	maximum = 0.576 (at node 135)
Accepted flit rate average= 0.190307
	minimum = 0.0935 (at node 116)
	maximum = 0.29 (at node 22)
Injected packet length average = 17.8985
Accepted packet length average = 19.55
Total in-flight flits = 24482 (0 measured)
latency change    = 0.321965
throughput change = 0.0530392
Class 0:
Packet latency average = 693.338
	minimum = 24
	maximum = 2749
Network latency average = 556.311
	minimum = 23
	maximum = 2650
Slowest packet = 210
Flit latency average = 429.722
	minimum = 6
	maximum = 2731
Slowest flit = 14359
Fragmentation average = 254.832
	minimum = 0
	maximum = 1997
Injected packet rate average = 0.0135521
	minimum = 0 (at node 0)
	maximum = 0.04 (at node 147)
Accepted packet rate average = 0.0118906
	minimum = 0.005 (at node 61)
	maximum = 0.022 (at node 24)
Injected flit rate average = 0.243526
	minimum = 0 (at node 0)
	maximum = 0.704 (at node 147)
Accepted flit rate average= 0.215349
	minimum = 0.078 (at node 184)
	maximum = 0.36 (at node 89)
Injected packet length average = 17.9696
Accepted packet length average = 18.1108
Total in-flight flits = 29953 (0 measured)
latency change    = 0.417265
throughput change = 0.116284
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 515.369
	minimum = 24
	maximum = 2229
Network latency average = 317.498
	minimum = 24
	maximum = 927
Slowest packet = 8080
Flit latency average = 532.703
	minimum = 6
	maximum = 3628
Slowest flit = 10961
Fragmentation average = 177.141
	minimum = 0
	maximum = 626
Injected packet rate average = 0.0128854
	minimum = 0 (at node 0)
	maximum = 0.036 (at node 134)
Accepted packet rate average = 0.0116771
	minimum = 0.006 (at node 9)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.230411
	minimum = 0 (at node 8)
	maximum = 0.651 (at node 134)
Accepted flit rate average= 0.209677
	minimum = 0.088 (at node 17)
	maximum = 0.414 (at node 16)
Injected packet length average = 17.8816
Accepted packet length average = 17.9563
Total in-flight flits = 34263 (22384 measured)
latency change    = 0.345324
throughput change = 0.0270505
Class 0:
Packet latency average = 714.414
	minimum = 24
	maximum = 3150
Network latency average = 446.819
	minimum = 24
	maximum = 1933
Slowest packet = 8080
Flit latency average = 582.581
	minimum = 6
	maximum = 4281
Slowest flit = 34772
Fragmentation average = 200.8
	minimum = 0
	maximum = 1479
Injected packet rate average = 0.0127214
	minimum = 0.0005 (at node 8)
	maximum = 0.028 (at node 57)
Accepted packet rate average = 0.0116979
	minimum = 0.006 (at node 4)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.227924
	minimum = 0.002 (at node 8)
	maximum = 0.498 (at node 57)
Accepted flit rate average= 0.21174
	minimum = 0.0935 (at node 4)
	maximum = 0.336 (at node 16)
Injected packet length average = 17.9167
Accepted packet length average = 18.1006
Total in-flight flits = 36593 (29800 measured)
latency change    = 0.278614
throughput change = 0.00974074
Class 0:
Packet latency average = 884.572
	minimum = 24
	maximum = 4091
Network latency average = 539.577
	minimum = 24
	maximum = 2974
Slowest packet = 8080
Flit latency average = 621.474
	minimum = 6
	maximum = 5778
Slowest flit = 11570
Fragmentation average = 220.254
	minimum = 0
	maximum = 2251
Injected packet rate average = 0.0124306
	minimum = 0.001 (at node 89)
	maximum = 0.027 (at node 73)
Accepted packet rate average = 0.0117622
	minimum = 0.00733333 (at node 4)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.222958
	minimum = 0.015 (at node 126)
	maximum = 0.488 (at node 73)
Accepted flit rate average= 0.212031
	minimum = 0.125667 (at node 4)
	maximum = 0.298667 (at node 129)
Injected packet length average = 17.9363
Accepted packet length average = 18.0266
Total in-flight flits = 36775 (32390 measured)
latency change    = 0.192361
throughput change = 0.00137558
Class 0:
Packet latency average = 1044.37
	minimum = 24
	maximum = 4782
Network latency average = 612.891
	minimum = 24
	maximum = 3858
Slowest packet = 8080
Flit latency average = 661.483
	minimum = 6
	maximum = 6760
Slowest flit = 12824
Fragmentation average = 231.718
	minimum = 0
	maximum = 2599
Injected packet rate average = 0.0123555
	minimum = 0.0015 (at node 124)
	maximum = 0.02575 (at node 73)
Accepted packet rate average = 0.0117383
	minimum = 0.00725 (at node 89)
	maximum = 0.016 (at node 129)
Injected flit rate average = 0.221654
	minimum = 0.02625 (at node 124)
	maximum = 0.4635 (at node 73)
Accepted flit rate average= 0.211543
	minimum = 0.12925 (at node 89)
	maximum = 0.2835 (at node 129)
Injected packet length average = 17.9397
Accepted packet length average = 18.0216
Total in-flight flits = 38362 (35366 measured)
latency change    = 0.153007
throughput change = 0.00230819
Class 0:
Packet latency average = 1165.18
	minimum = 24
	maximum = 5736
Network latency average = 673.993
	minimum = 24
	maximum = 4719
Slowest packet = 8080
Flit latency average = 698.519
	minimum = 6
	maximum = 7228
Slowest flit = 31139
Fragmentation average = 240.548
	minimum = 0
	maximum = 3663
Injected packet rate average = 0.0121635
	minimum = 0.0028 (at node 190)
	maximum = 0.0262 (at node 73)
Accepted packet rate average = 0.011649
	minimum = 0.0074 (at node 89)
	maximum = 0.0152 (at node 44)
Injected flit rate average = 0.218364
	minimum = 0.0506 (at node 190)
	maximum = 0.4712 (at node 73)
Accepted flit rate average= 0.210382
	minimum = 0.1358 (at node 89)
	maximum = 0.2744 (at node 157)
Injected packet length average = 17.9523
Accepted packet length average = 18.0602
Total in-flight flits = 38298 (36190 measured)
latency change    = 0.103687
throughput change = 0.00551699
Class 0:
Packet latency average = 1284.23
	minimum = 24
	maximum = 6234
Network latency average = 730.006
	minimum = 24
	maximum = 5912
Slowest packet = 8080
Flit latency average = 727.857
	minimum = 6
	maximum = 8090
Slowest flit = 26302
Fragmentation average = 248.84
	minimum = 0
	maximum = 4693
Injected packet rate average = 0.0120503
	minimum = 0.00316667 (at node 148)
	maximum = 0.022 (at node 61)
Accepted packet rate average = 0.0116354
	minimum = 0.00866667 (at node 64)
	maximum = 0.0148333 (at node 157)
Injected flit rate average = 0.216419
	minimum = 0.0598333 (at node 148)
	maximum = 0.397 (at node 73)
Accepted flit rate average= 0.210004
	minimum = 0.154833 (at node 64)
	maximum = 0.270667 (at node 95)
Injected packet length average = 17.9596
Accepted packet length average = 18.0487
Total in-flight flits = 38030 (36532 measured)
latency change    = 0.0926992
throughput change = 0.00179973
Class 0:
Packet latency average = 1414.26
	minimum = 24
	maximum = 7216
Network latency average = 770.3
	minimum = 23
	maximum = 6929
Slowest packet = 8056
Flit latency average = 749.467
	minimum = 6
	maximum = 9301
Slowest flit = 17441
Fragmentation average = 254.85
	minimum = 0
	maximum = 4922
Injected packet rate average = 0.012061
	minimum = 0.00271429 (at node 148)
	maximum = 0.023 (at node 169)
Accepted packet rate average = 0.0116481
	minimum = 0.00885714 (at node 89)
	maximum = 0.0148571 (at node 95)
Injected flit rate average = 0.216795
	minimum = 0.0512857 (at node 148)
	maximum = 0.413571 (at node 169)
Accepted flit rate average= 0.209815
	minimum = 0.159857 (at node 64)
	maximum = 0.267857 (at node 95)
Injected packet length average = 17.9748
Accepted packet length average = 18.0128
Total in-flight flits = 39976 (38931 measured)
latency change    = 0.0919443
throughput change = 0.000903693
Draining all recorded packets ...
Class 0:
Remaining flits: 18396 18397 18398 18399 18400 18401 18402 18403 18404 18405 [...] (41730 flits)
Measured flits: 148320 148321 148322 148323 148324 148325 148326 148327 148328 148329 [...] (31410 flits)
Class 0:
Remaining flits: 18396 18397 18398 18399 18400 18401 18402 18403 18404 18405 [...] (42435 flits)
Measured flits: 148322 148323 148324 148325 148326 148327 148328 148329 148330 148331 [...] (24444 flits)
Class 0:
Remaining flits: 18396 18397 18398 18399 18400 18401 18402 18403 18404 18405 [...] (41642 flits)
Measured flits: 148902 148903 148904 148905 148906 148907 148908 148909 148910 148911 [...] (19531 flits)
Class 0:
Remaining flits: 18405 18406 18407 18408 18409 18410 18411 18412 18413 19905 [...] (38791 flits)
Measured flits: 148902 148903 148904 148905 148906 148907 148908 148909 148910 148911 [...] (15796 flits)
Class 0:
Remaining flits: 19905 19906 19907 44345 44346 44347 44348 44349 44350 44351 [...] (44485 flits)
Measured flits: 148902 148903 148904 148905 148906 148907 148908 148909 148910 148911 [...] (12972 flits)
Class 0:
Remaining flits: 19905 19906 19907 84536 84537 84538 84539 84540 84541 84542 [...] (43619 flits)
Measured flits: 149184 149185 149186 149187 149188 149189 149190 149191 149192 149193 [...] (11519 flits)
Class 0:
Remaining flits: 19905 19906 19907 109889 122886 122887 122888 122889 122890 122891 [...] (45884 flits)
Measured flits: 149184 149185 149186 149187 149188 149189 149190 149191 149192 149193 [...] (8510 flits)
Class 0:
Remaining flits: 19905 19906 19907 122886 122887 122888 122889 122890 122891 122892 [...] (45598 flits)
Measured flits: 149184 149185 149186 149187 149188 149189 149190 149191 149192 149193 [...] (6276 flits)
Class 0:
Remaining flits: 19905 19906 19907 122886 122887 122888 122889 122890 122891 122892 [...] (47260 flits)
Measured flits: 149184 149185 149186 149187 149188 149189 149190 149191 149192 149193 [...] (5171 flits)
Class 0:
Remaining flits: 19905 19906 19907 122886 122887 122888 122889 122890 122891 122892 [...] (47251 flits)
Measured flits: 149184 149185 149186 149187 149188 149189 149190 149191 149192 149193 [...] (4593 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (48040 flits)
Measured flits: 149193 149194 149195 149196 149197 149198 149199 149200 149201 162216 [...] (4269 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (49755 flits)
Measured flits: 149193 149194 149195 149196 149197 149198 149199 149200 149201 162216 [...] (3565 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (50900 flits)
Measured flits: 149196 149197 149198 149199 149200 149201 162216 162217 162218 162219 [...] (3078 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (52120 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (2274 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (54685 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (2018 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (57061 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (1345 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (55036 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (1019 flits)
Class 0:
Remaining flits: 122888 122889 122890 122891 122892 122893 122894 122895 122896 122897 [...] (57974 flits)
Measured flits: 162216 162217 162218 162219 162220 162221 162222 162223 162224 162225 [...] (875 flits)
Class 0:
Remaining flits: 162222 162223 162224 162225 162226 162227 162228 162229 162230 162231 [...] (57038 flits)
Measured flits: 162222 162223 162224 162225 162226 162227 162228 162229 162230 162231 [...] (599 flits)
Class 0:
Remaining flits: 256805 344356 344357 399792 399793 399794 399795 399796 399797 494622 [...] (55343 flits)
Measured flits: 256805 344356 344357 399792 399793 399794 399795 399796 399797 532115 [...] (382 flits)
Class 0:
Remaining flits: 344356 344357 399792 399793 399794 399795 399796 399797 494622 494623 [...] (57202 flits)
Measured flits: 344356 344357 399792 399793 399794 399795 399796 399797 683338 683339 [...] (268 flits)
Class 0:
Remaining flits: 344356 344357 399792 399793 399794 399795 399796 399797 494622 494623 [...] (56674 flits)
Measured flits: 344356 344357 399792 399793 399794 399795 399796 399797 728550 728551 [...] (229 flits)
Class 0:
Remaining flits: 344356 344357 494622 494623 494624 494625 494626 494627 494628 494629 [...] (56698 flits)
Measured flits: 344356 344357 728550 728551 728552 728553 728554 728555 728556 728557 [...] (194 flits)
Class 0:
Remaining flits: 344356 344357 494622 494623 494624 494625 494626 494627 494628 494629 [...] (57633 flits)
Measured flits: 344356 344357 728550 728551 728552 728553 728554 728555 728556 728557 [...] (182 flits)
Class 0:
Remaining flits: 344356 344357 494622 494623 494624 494625 494626 494627 494628 494629 [...] (59511 flits)
Measured flits: 344356 344357 728550 728551 728552 728553 728554 728555 728556 728557 [...] (182 flits)
Class 0:
Remaining flits: 344356 344357 494622 494623 494624 494625 494626 494627 494628 494629 [...] (58146 flits)
Measured flits: 344356 344357 728550 728551 728552 728553 728554 728555 728556 728557 [...] (175 flits)
Class 0:
Remaining flits: 344356 344357 572040 572041 572042 572043 572044 572045 572046 572047 [...] (59075 flits)
Measured flits: 344356 344357 728550 728551 728552 728553 728554 728555 728556 728557 [...] (103 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (57696 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (72 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (60150 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (69 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (59759 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (54 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (62115 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (36 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (64516 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (36 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (63221 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (36 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (63310 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (18 flits)
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (62722 flits)
Measured flits: 728550 728551 728552 728553 728554 728555 728556 728557 728558 728559 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 572040 572041 572042 572043 572044 572045 572046 572047 572048 572049 [...] (36186 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 897174 897175 897176 897177 897178 897179 897180 897181 897182 897183 [...] (22684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 991233 991234 991235 991236 991237 991238 991239 991240 991241 1069806 [...] (12409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1101081 1101082 1101083 1101084 1101085 1101086 1101087 1101088 1101089 1101090 [...] (4624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329696 1329697 1329698 1329699 1329700 1329701 1329702 1329703 1329704 1329705 [...] (1234 flits)
Measured flits: (0 flits)
Time taken is 51154 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3071.7 (1 samples)
	minimum = 24 (1 samples)
	maximum = 35978 (1 samples)
Network latency average = 1181.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 29305 (1 samples)
Flit latency average = 1316.36 (1 samples)
	minimum = 6 (1 samples)
	maximum = 33601 (1 samples)
Fragmentation average = 315.585 (1 samples)
	minimum = 0 (1 samples)
	maximum = 15054 (1 samples)
Injected packet rate average = 0.012061 (1 samples)
	minimum = 0.00271429 (1 samples)
	maximum = 0.023 (1 samples)
Accepted packet rate average = 0.0116481 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0148571 (1 samples)
Injected flit rate average = 0.216795 (1 samples)
	minimum = 0.0512857 (1 samples)
	maximum = 0.413571 (1 samples)
Accepted flit rate average = 0.209815 (1 samples)
	minimum = 0.159857 (1 samples)
	maximum = 0.267857 (1 samples)
Injected packet size average = 17.9748 (1 samples)
Accepted packet size average = 18.0128 (1 samples)
Hops average = 5.05792 (1 samples)
Total run time 68.897
