BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.099
	minimum = 23
	maximum = 955
Network latency average = 345.175
	minimum = 23
	maximum = 951
Slowest packet = 311
Flit latency average = 313.755
	minimum = 6
	maximum = 952
Slowest flit = 5048
Fragmentation average = 57.1117
	minimum = 0
	maximum = 157
Injected packet rate average = 0.0415104
	minimum = 0.021 (at node 16)
	maximum = 0.052 (at node 9)
Accepted packet rate average = 0.010401
	minimum = 0.005 (at node 54)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.740057
	minimum = 0.377 (at node 16)
	maximum = 0.935 (at node 119)
Accepted flit rate average= 0.194734
	minimum = 0.09 (at node 64)
	maximum = 0.348 (at node 44)
Injected packet length average = 17.8282
Accepted packet length average = 18.7226
Total in-flight flits = 106539 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 759.941
	minimum = 23
	maximum = 1900
Network latency average = 691.555
	minimum = 23
	maximum = 1854
Slowest packet = 672
Flit latency average = 656.701
	minimum = 6
	maximum = 1861
Slowest flit = 19281
Fragmentation average = 63.439
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0284948
	minimum = 0.0165 (at node 108)
	maximum = 0.0365 (at node 61)
Accepted packet rate average = 0.0100729
	minimum = 0.006 (at node 96)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.510141
	minimum = 0.2955 (at node 108)
	maximum = 0.653 (at node 61)
Accepted flit rate average= 0.185411
	minimum = 0.108 (at node 96)
	maximum = 0.297 (at node 22)
Injected packet length average = 17.9029
Accepted packet length average = 18.4069
Total in-flight flits = 127036 (0 measured)
latency change    = 0.510358
throughput change = 0.0502823
Class 0:
Packet latency average = 2006.33
	minimum = 405
	maximum = 2833
Network latency average = 1752.72
	minimum = 28
	maximum = 2762
Slowest packet = 1548
Flit latency average = 1722.51
	minimum = 6
	maximum = 2758
Slowest flit = 37874
Fragmentation average = 72.4141
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00995833
	minimum = 0 (at node 3)
	maximum = 0.028 (at node 43)
Accepted packet rate average = 0.00936979
	minimum = 0.003 (at node 4)
	maximum = 0.017 (at node 107)
Injected flit rate average = 0.181583
	minimum = 0 (at node 3)
	maximum = 0.506 (at node 43)
Accepted flit rate average= 0.167036
	minimum = 0.054 (at node 153)
	maximum = 0.309 (at node 107)
Injected packet length average = 18.2343
Accepted packet length average = 17.8271
Total in-flight flits = 130407 (0 measured)
latency change    = 0.621228
throughput change = 0.110006
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2341.41
	minimum = 1485
	maximum = 3237
Network latency average = 163.926
	minimum = 23
	maximum = 887
Slowest packet = 12998
Flit latency average = 2441.63
	minimum = 6
	maximum = 3620
Slowest flit = 56951
Fragmentation average = 23.6852
	minimum = 0
	maximum = 100
Injected packet rate average = 0.00780729
	minimum = 0 (at node 20)
	maximum = 0.039 (at node 27)
Accepted packet rate average = 0.00890104
	minimum = 0.003 (at node 81)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.141245
	minimum = 0 (at node 20)
	maximum = 0.699 (at node 27)
Accepted flit rate average= 0.161036
	minimum = 0.064 (at node 32)
	maximum = 0.312 (at node 56)
Injected packet length average = 18.0914
Accepted packet length average = 18.0919
Total in-flight flits = 126632 (26161 measured)
latency change    = 0.14311
throughput change = 0.0372586
Class 0:
Packet latency average = 3144.43
	minimum = 1485
	maximum = 4216
Network latency average = 570.122
	minimum = 23
	maximum = 1846
Slowest packet = 12998
Flit latency average = 2714.56
	minimum = 6
	maximum = 4608
Slowest flit = 46367
Fragmentation average = 43.558
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00786979
	minimum = 0.0005 (at node 21)
	maximum = 0.0235 (at node 27)
Accepted packet rate average = 0.00885417
	minimum = 0.0045 (at node 36)
	maximum = 0.016 (at node 56)
Injected flit rate average = 0.14163
	minimum = 0.003 (at node 140)
	maximum = 0.423 (at node 27)
Accepted flit rate average= 0.159552
	minimum = 0.081 (at node 36)
	maximum = 0.291 (at node 56)
Injected packet length average = 17.9967
Accepted packet length average = 18.02
Total in-flight flits = 123481 (50943 measured)
latency change    = 0.255378
throughput change = 0.00930339
Class 0:
Packet latency average = 3960.06
	minimum = 1485
	maximum = 5120
Network latency average = 1063.57
	minimum = 23
	maximum = 2870
Slowest packet = 12998
Flit latency average = 2905.79
	minimum = 6
	maximum = 5468
Slowest flit = 80697
Fragmentation average = 55.6083
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00826389
	minimum = 0.000333333 (at node 106)
	maximum = 0.0233333 (at node 20)
Accepted packet rate average = 0.00887326
	minimum = 0.00533333 (at node 4)
	maximum = 0.013 (at node 56)
Injected flit rate average = 0.148637
	minimum = 0.006 (at node 106)
	maximum = 0.415333 (at node 20)
Accepted flit rate average= 0.160097
	minimum = 0.0936667 (at node 4)
	maximum = 0.233667 (at node 51)
Injected packet length average = 17.9863
Accepted packet length average = 18.0427
Total in-flight flits = 123673 (74332 measured)
latency change    = 0.205965
throughput change = 0.00340505
Draining remaining packets ...
Class 0:
Remaining flits: 67381 67382 67383 67384 67385 67386 67387 67388 67389 67390 [...] (92808 flits)
Measured flits: 233694 233695 233696 233697 233698 233699 233700 233701 233702 233703 [...] (62625 flits)
Class 0:
Remaining flits: 90180 90181 90182 90183 90184 90185 90186 90187 90188 90189 [...] (62546 flits)
Measured flits: 233697 233698 233699 233700 233701 233702 233703 233704 233705 233706 [...] (46939 flits)
Class 0:
Remaining flits: 90972 90973 90974 90975 90976 90977 90978 90979 90980 90981 [...] (32862 flits)
Measured flits: 233730 233731 233732 233733 233734 233735 233736 233737 233738 233739 [...] (26473 flits)
Class 0:
Remaining flits: 107028 107029 107030 107031 107032 107033 107034 107035 107036 107037 [...] (7322 flits)
Measured flits: 233730 233731 233732 233733 233734 233735 233736 233737 233738 233739 [...] (6402 flits)
Time taken is 10687 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6507.71 (1 samples)
	minimum = 1485 (1 samples)
	maximum = 9446 (1 samples)
Network latency average = 3399.11 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7224 (1 samples)
Flit latency average = 3794.26 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9541 (1 samples)
Fragmentation average = 64.5386 (1 samples)
	minimum = 0 (1 samples)
	maximum = 197 (1 samples)
Injected packet rate average = 0.00826389 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.0233333 (1 samples)
Accepted packet rate average = 0.00887326 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.148637 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.415333 (1 samples)
Accepted flit rate average = 0.160097 (1 samples)
	minimum = 0.0936667 (1 samples)
	maximum = 0.233667 (1 samples)
Injected packet size average = 17.9863 (1 samples)
Accepted packet size average = 18.0427 (1 samples)
Hops average = 5.05854 (1 samples)
Total run time 7.42
