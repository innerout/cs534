BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.989
	minimum = 22
	maximum = 862
Network latency average = 286.145
	minimum = 22
	maximum = 811
Slowest packet = 907
Flit latency average = 253.848
	minimum = 5
	maximum = 818
Slowest flit = 19257
Fragmentation average = 70.9081
	minimum = 0
	maximum = 396
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 64)
	maximum = 0.024 (at node 88)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.283109
	minimum = 0.141 (at node 64)
	maximum = 0.451 (at node 22)
Injected packet length average = 17.8477
Accepted packet length average = 19.2824
Total in-flight flits = 74469 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 559.446
	minimum = 22
	maximum = 1665
Network latency average = 544.331
	minimum = 22
	maximum = 1637
Slowest packet = 2147
Flit latency average = 502.93
	minimum = 5
	maximum = 1693
Slowest flit = 36806
Fragmentation average = 97.5446
	minimum = 0
	maximum = 406
Injected packet rate average = 0.0376797
	minimum = 0.0285 (at node 81)
	maximum = 0.05 (at node 151)
Accepted packet rate average = 0.0154922
	minimum = 0.0095 (at node 153)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.675784
	minimum = 0.513 (at node 81)
	maximum = 0.897 (at node 151)
Accepted flit rate average= 0.290505
	minimum = 0.1795 (at node 153)
	maximum = 0.4195 (at node 152)
Injected packet length average = 17.935
Accepted packet length average = 18.7517
Total in-flight flits = 148888 (0 measured)
latency change    = 0.465563
throughput change = 0.0254585
Class 0:
Packet latency average = 1295.76
	minimum = 22
	maximum = 2438
Network latency average = 1278.44
	minimum = 22
	maximum = 2364
Slowest packet = 3143
Flit latency average = 1241.47
	minimum = 5
	maximum = 2416
Slowest flit = 71792
Fragmentation average = 141.866
	minimum = 0
	maximum = 543
Injected packet rate average = 0.0375521
	minimum = 0.022 (at node 163)
	maximum = 0.049 (at node 110)
Accepted packet rate average = 0.0162344
	minimum = 0.007 (at node 96)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.675307
	minimum = 0.397 (at node 163)
	maximum = 0.883 (at node 110)
Accepted flit rate average= 0.295578
	minimum = 0.132 (at node 96)
	maximum = 0.5 (at node 16)
Injected packet length average = 17.9832
Accepted packet length average = 18.2069
Total in-flight flits = 221917 (0 measured)
latency change    = 0.568248
throughput change = 0.0171627
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 59.8554
	minimum = 22
	maximum = 357
Network latency average = 39.7789
	minimum = 22
	maximum = 357
Slowest packet = 22381
Flit latency average = 1728.22
	minimum = 5
	maximum = 3136
Slowest flit = 109810
Fragmentation average = 6.13435
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0384792
	minimum = 0.023 (at node 16)
	maximum = 0.053 (at node 104)
Accepted packet rate average = 0.0165104
	minimum = 0.006 (at node 4)
	maximum = 0.03 (at node 123)
Injected flit rate average = 0.692391
	minimum = 0.414 (at node 77)
	maximum = 0.967 (at node 104)
Accepted flit rate average= 0.298005
	minimum = 0.114 (at node 4)
	maximum = 0.57 (at node 123)
Injected packet length average = 17.9939
Accepted packet length average = 18.0495
Total in-flight flits = 297684 (122268 measured)
latency change    = 20.6481
throughput change = 0.00814443
Class 0:
Packet latency average = 60.125
	minimum = 22
	maximum = 357
Network latency average = 41.1883
	minimum = 22
	maximum = 357
Slowest packet = 22381
Flit latency average = 1969.85
	minimum = 5
	maximum = 3961
Slowest flit = 131431
Fragmentation average = 6.37253
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0380859
	minimum = 0.0255 (at node 176)
	maximum = 0.0485 (at node 38)
Accepted packet rate average = 0.0166302
	minimum = 0.0095 (at node 4)
	maximum = 0.0265 (at node 123)
Injected flit rate average = 0.685562
	minimum = 0.4625 (at node 176)
	maximum = 0.8805 (at node 106)
Accepted flit rate average= 0.299974
	minimum = 0.1705 (at node 4)
	maximum = 0.4675 (at node 123)
Injected packet length average = 18.0004
Accepted packet length average = 18.0379
Total in-flight flits = 369977 (241244 measured)
latency change    = 0.00448329
throughput change = 0.00656307
Class 0:
Packet latency average = 58.7822
	minimum = 22
	maximum = 357
Network latency average = 40.7756
	minimum = 22
	maximum = 357
Slowest packet = 22381
Flit latency average = 2219.47
	minimum = 5
	maximum = 4452
Slowest flit = 166337
Fragmentation average = 6.27756
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0379688
	minimum = 0.0276667 (at node 95)
	maximum = 0.0466667 (at node 120)
Accepted packet rate average = 0.0166302
	minimum = 0.0113333 (at node 4)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.683458
	minimum = 0.498 (at node 95)
	maximum = 0.844333 (at node 120)
Accepted flit rate average= 0.299812
	minimum = 0.206333 (at node 4)
	maximum = 0.397333 (at node 123)
Injected packet length average = 18.0005
Accepted packet length average = 18.0282
Total in-flight flits = 442885 (360763 measured)
latency change    = 0.0228432
throughput change = 0.000538531
Class 0:
Packet latency average = 249.83
	minimum = 22
	maximum = 4000
Network latency average = 231.594
	minimum = 22
	maximum = 3967
Slowest packet = 21788
Flit latency average = 2468.77
	minimum = 5
	maximum = 5279
Slowest flit = 219379
Fragmentation average = 15.1188
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0379714
	minimum = 0.0285 (at node 95)
	maximum = 0.0455 (at node 132)
Accepted packet rate average = 0.0166432
	minimum = 0.012 (at node 110)
	maximum = 0.02175 (at node 68)
Injected flit rate average = 0.683424
	minimum = 0.50925 (at node 95)
	maximum = 0.81975 (at node 132)
Accepted flit rate average= 0.29987
	minimum = 0.215 (at node 110)
	maximum = 0.38825 (at node 128)
Injected packet length average = 17.9984
Accepted packet length average = 18.0175
Total in-flight flits = 516533 (477768 measured)
latency change    = 0.764711
throughput change = 0.000191055
Class 0:
Packet latency average = 1219.52
	minimum = 22
	maximum = 5019
Network latency average = 1201.05
	minimum = 22
	maximum = 4986
Slowest packet = 21756
Flit latency average = 2714.97
	minimum = 5
	maximum = 6203
Slowest flit = 233465
Fragmentation average = 54.2192
	minimum = 0
	maximum = 378
Injected packet rate average = 0.0380667
	minimum = 0.0292 (at node 95)
	maximum = 0.0454 (at node 174)
Accepted packet rate average = 0.0167281
	minimum = 0.0132 (at node 149)
	maximum = 0.0208 (at node 70)
Injected flit rate average = 0.685153
	minimum = 0.5228 (at node 95)
	maximum = 0.8146 (at node 174)
Accepted flit rate average= 0.301123
	minimum = 0.239 (at node 149)
	maximum = 0.3716 (at node 123)
Injected packet length average = 17.9988
Accepted packet length average = 18.001
Total in-flight flits = 590631 (578618 measured)
latency change    = 0.79514
throughput change = 0.00416151
Class 0:
Packet latency average = 2253.38
	minimum = 22
	maximum = 6026
Network latency average = 2235.12
	minimum = 22
	maximum = 5949
Slowest packet = 21782
Flit latency average = 2961.24
	minimum = 5
	maximum = 6880
Slowest flit = 254555
Fragmentation average = 89.7508
	minimum = 0
	maximum = 407
Injected packet rate average = 0.0380174
	minimum = 0.0291667 (at node 95)
	maximum = 0.0446667 (at node 172)
Accepted packet rate average = 0.0167361
	minimum = 0.0135 (at node 4)
	maximum = 0.0205 (at node 181)
Injected flit rate average = 0.684288
	minimum = 0.525 (at node 95)
	maximum = 0.801833 (at node 172)
Accepted flit rate average= 0.301365
	minimum = 0.242667 (at node 80)
	maximum = 0.368 (at node 138)
Injected packet length average = 17.9994
Accepted packet length average = 18.0069
Total in-flight flits = 663072 (660844 measured)
latency change    = 0.458807
throughput change = 0.000804786
Class 0:
Packet latency average = 3007.17
	minimum = 22
	maximum = 6965
Network latency average = 2988.63
	minimum = 22
	maximum = 6935
Slowest packet = 22013
Flit latency average = 3208.42
	minimum = 5
	maximum = 7392
Slowest flit = 328625
Fragmentation average = 111.435
	minimum = 0
	maximum = 471
Injected packet rate average = 0.0379301
	minimum = 0.0305714 (at node 95)
	maximum = 0.0441429 (at node 172)
Accepted packet rate average = 0.0167433
	minimum = 0.0128571 (at node 4)
	maximum = 0.021 (at node 70)
Injected flit rate average = 0.682685
	minimum = 0.549714 (at node 95)
	maximum = 0.793571 (at node 172)
Accepted flit rate average= 0.301618
	minimum = 0.230143 (at node 4)
	maximum = 0.379143 (at node 70)
Injected packet length average = 17.9985
Accepted packet length average = 18.0142
Total in-flight flits = 734147 (733878 measured)
latency change    = 0.250664
throughput change = 0.000835854
Draining all recorded packets ...
Class 0:
Remaining flits: 383040 383041 383042 383043 383044 383045 383046 383047 383048 383049 [...] (806936 flits)
Measured flits: 396447 396448 396449 414828 414829 414830 414831 414832 414833 414834 [...] (689171 flits)
Class 0:
Remaining flits: 423792 423793 423794 423795 423796 423797 423798 423799 423800 423801 [...] (877051 flits)
Measured flits: 423792 423793 423794 423795 423796 423797 423798 423799 423800 423801 [...] (642219 flits)
Class 0:
Remaining flits: 465029 475920 475921 475922 475923 475924 475925 475926 475927 475928 [...] (951414 flits)
Measured flits: 465029 475920 475921 475922 475923 475924 475925 475926 475927 475928 [...] (595238 flits)
Class 0:
Remaining flits: 495383 495384 495385 495386 495387 495388 495389 495390 495391 495392 [...] (1025182 flits)
Measured flits: 495383 495384 495385 495386 495387 495388 495389 495390 495391 495392 [...] (547906 flits)
Class 0:
Remaining flits: 562349 562350 562351 562352 562353 562354 562355 566496 566497 566498 [...] (1097014 flits)
Measured flits: 562349 562350 562351 562352 562353 562354 562355 566496 566497 566498 [...] (500879 flits)
Class 0:
Remaining flits: 600572 600573 600574 600575 600576 600577 600578 600579 600580 600581 [...] (1173182 flits)
Measured flits: 600572 600573 600574 600575 600576 600577 600578 600579 600580 600581 [...] (454159 flits)
Class 0:
Remaining flits: 615438 615439 615440 615441 615442 615443 615444 615445 615446 615447 [...] (1245704 flits)
Measured flits: 615438 615439 615440 615441 615442 615443 615444 615445 615446 615447 [...] (407226 flits)
Class 0:
Remaining flits: 720990 720991 720992 720993 720994 720995 720996 720997 720998 720999 [...] (1317855 flits)
Measured flits: 720990 720991 720992 720993 720994 720995 720996 720997 720998 720999 [...] (359824 flits)
Class 0:
Remaining flits: 734379 734380 734381 756450 756451 756452 756453 756454 756455 756456 [...] (1389986 flits)
Measured flits: 734379 734380 734381 756450 756451 756452 756453 756454 756455 756456 [...] (312185 flits)
Class 0:
Remaining flits: 796163 796164 796165 796166 796167 796168 796169 796170 796171 796172 [...] (1458971 flits)
Measured flits: 796163 796164 796165 796166 796167 796168 796169 796170 796171 796172 [...] (264624 flits)
Class 0:
Remaining flits: 834552 834553 834554 834555 834556 834557 834558 834559 834560 834561 [...] (1521476 flits)
Measured flits: 834552 834553 834554 834555 834556 834557 834558 834559 834560 834561 [...] (216978 flits)
Class 0:
Remaining flits: 887729 887730 887731 887732 887733 887734 887735 887736 887737 887738 [...] (1582491 flits)
Measured flits: 887729 887730 887731 887732 887733 887734 887735 887736 887737 887738 [...] (170298 flits)
Class 0:
Remaining flits: 922050 922051 922052 922053 922054 922055 922056 922057 922058 922059 [...] (1638680 flits)
Measured flits: 922050 922051 922052 922053 922054 922055 922056 922057 922058 922059 [...] (125516 flits)
Class 0:
Remaining flits: 936306 936307 936308 936309 936310 936311 936312 936313 936314 936315 [...] (1694835 flits)
Measured flits: 936306 936307 936308 936309 936310 936311 936312 936313 936314 936315 [...] (86121 flits)
Class 0:
Remaining flits: 985530 985531 985532 985533 985534 985535 1017342 1017343 1017344 1017345 [...] (1748692 flits)
Measured flits: 985530 985531 985532 985533 985534 985535 1017342 1017343 1017344 1017345 [...] (54185 flits)
Class 0:
Remaining flits: 1052313 1052314 1052315 1073630 1073631 1073632 1073633 1073634 1073635 1073636 [...] (1805526 flits)
Measured flits: 1052313 1052314 1052315 1073630 1073631 1073632 1073633 1073634 1073635 1073636 [...] (30155 flits)
Class 0:
Remaining flits: 1081106 1081107 1081108 1081109 1081110 1081111 1081112 1081113 1081114 1081115 [...] (1860806 flits)
Measured flits: 1081106 1081107 1081108 1081109 1081110 1081111 1081112 1081113 1081114 1081115 [...] (14140 flits)
Class 0:
Remaining flits: 1155978 1155979 1155980 1155981 1155982 1155983 1155984 1155985 1155986 1155987 [...] (1916978 flits)
Measured flits: 1155978 1155979 1155980 1155981 1155982 1155983 1155984 1155985 1155986 1155987 [...] (5772 flits)
Class 0:
Remaining flits: 1189386 1189387 1189388 1189389 1189390 1189391 1189392 1189393 1189394 1189395 [...] (1971857 flits)
Measured flits: 1189386 1189387 1189388 1189389 1189390 1189391 1189392 1189393 1189394 1189395 [...] (1999 flits)
Class 0:
Remaining flits: 1197090 1197091 1197092 1197093 1197094 1197095 1197096 1197097 1197098 1197099 [...] (2025536 flits)
Measured flits: 1197090 1197091 1197092 1197093 1197094 1197095 1197096 1197097 1197098 1197099 [...] (469 flits)
Class 0:
Remaining flits: 1211886 1211887 1211888 1211889 1211890 1211891 1211892 1211893 1211894 1211895 [...] (2082696 flits)
Measured flits: 1211886 1211887 1211888 1211889 1211890 1211891 1211892 1211893 1211894 1211895 [...] (107 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1358568 1358569 1358570 1358571 1358572 1358573 1358574 1358575 1358576 1358577 [...] (2075545 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1432301 1432302 1432303 1432304 1432305 1432306 1432307 1432308 1432309 1432310 [...] (2025113 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1458450 1458451 1458452 1458453 1458454 1458455 1458456 1458457 1458458 1458459 [...] (1974468 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1520263 1520264 1520265 1520266 1520267 1520268 1520269 1520270 1520271 1520272 [...] (1924355 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1561122 1561123 1561124 1561125 1561126 1561127 1561128 1561129 1561130 1561131 [...] (1873973 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1570356 1570357 1570358 1570359 1570360 1570361 1570362 1570363 1570364 1570365 [...] (1823704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1572606 1572607 1572608 1572609 1572610 1572611 1572612 1572613 1572614 1572615 [...] (1773316 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1572621 1572622 1572623 1608480 1608481 1608482 1608483 1608484 1608485 1608486 [...] (1723836 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1621278 1621279 1621280 1621281 1621282 1621283 1621284 1621285 1621286 1621287 [...] (1674027 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1681096 1681097 1681098 1681099 1681100 1681101 1681102 1681103 1681104 1681105 [...] (1624719 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1697508 1697509 1697510 1697511 1697512 1697513 1697514 1697515 1697516 1697517 [...] (1575802 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765474 1765475 1782601 1782602 1782603 1782604 1782605 1782606 1782607 1782608 [...] (1527107 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1788558 1788559 1788560 1788561 1788562 1788563 1788564 1788565 1788566 1788567 [...] (1478632 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1796578 1796579 1804158 1804159 1804160 1804161 1804162 1804163 1804164 1804165 [...] (1430854 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1806604 1806605 1843056 1843057 1843058 1843059 1843060 1843061 1843062 1843063 [...] (1383038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1894824 1894825 1894826 1894827 1894828 1894829 1894830 1894831 1894832 1894833 [...] (1335241 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1916298 1916299 1916300 1916301 1916302 1916303 1916304 1916305 1916306 1916307 [...] (1287831 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1945006 1945007 1973700 1973701 1973702 1973703 1973704 1973705 1973706 1973707 [...] (1239872 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1996362 1996363 1996364 1996365 1996366 1996367 1996368 1996369 1996370 1996371 [...] (1192096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998828 1998829 1998830 1998831 1998832 1998833 1998834 1998835 1998836 1998837 [...] (1144364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2003670 2003671 2003672 2003673 2003674 2003675 2003676 2003677 2003678 2003679 [...] (1096764 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2105838 2105839 2105840 2105841 2105842 2105843 2105844 2105845 2105846 2105847 [...] (1048854 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2105854 2105855 2140290 2140291 2140292 2140293 2140294 2140295 2140296 2140297 [...] (1000991 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2140290 2140291 2140292 2140293 2140294 2140295 2140296 2140297 2140298 2140299 [...] (953260 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2223414 2223415 2223416 2223417 2223418 2223419 2223420 2223421 2223422 2223423 [...] (905538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2244042 2244043 2244044 2244045 2244046 2244047 2244048 2244049 2244050 2244051 [...] (857822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2310784 2310785 2336796 2336797 2336798 2336799 2336800 2336801 2336802 2336803 [...] (809822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2359206 2359207 2359208 2359209 2359210 2359211 2359212 2359213 2359214 2359215 [...] (761869 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2374126 2374127 2406474 2406475 2406476 2406477 2406478 2406479 2406480 2406481 [...] (714045 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2419810 2419811 2424456 2424457 2424458 2424459 2424460 2424461 2424462 2424463 [...] (666302 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2488698 2488699 2488700 2488701 2488702 2488703 2488704 2488705 2488706 2488707 [...] (618710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2527452 2527453 2527454 2527455 2527456 2527457 2527458 2527459 2527460 2527461 [...] (570829 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2543666 2543667 2543668 2543669 2550744 2550745 2550746 2550747 2550748 2550749 [...] (523234 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2626344 2626345 2626346 2626347 2626348 2626349 2626350 2626351 2626352 2626353 [...] (475566 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2746494 2746495 2746496 2746497 2746498 2746499 2746500 2746501 2746502 2746503 [...] (427887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2749319 2777040 2777041 2777042 2777043 2777044 2777045 2777046 2777047 2777048 [...] (380431 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2777040 2777041 2777042 2777043 2777044 2777045 2777046 2777047 2777048 2777049 [...] (332484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2777040 2777041 2777042 2777043 2777044 2777045 2777046 2777047 2777048 2777049 [...] (284953 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2777040 2777041 2777042 2777043 2777044 2777045 2777046 2777047 2777048 2777049 [...] (237113 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2777040 2777041 2777042 2777043 2777044 2777045 2777046 2777047 2777048 2777049 [...] (189380 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2895804 2895805 2895806 2895807 2895808 2895809 2895810 2895811 2895812 2895813 [...] (141851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2895804 2895805 2895806 2895807 2895808 2895809 2895810 2895811 2895812 2895813 [...] (94734 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3019680 3019681 3019682 3019683 3019684 3019685 3019686 3019687 3019688 3019689 [...] (51593 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3119220 3119221 3119222 3119223 3119224 3119225 3119226 3119227 3119228 3119229 [...] (20543 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3370320 3370321 3370322 3370323 3370324 3370325 3370326 3370327 3370328 3370329 [...] (5571 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3820607 3880044 3880045 3880046 3880047 3880048 3880049 3880050 3880051 3880052 [...] (37 flits)
Measured flits: (0 flits)
Time taken is 77885 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9521.74 (1 samples)
	minimum = 22 (1 samples)
	maximum = 22028 (1 samples)
Network latency average = 9503.56 (1 samples)
	minimum = 22 (1 samples)
	maximum = 22028 (1 samples)
Flit latency average = 21837.6 (1 samples)
	minimum = 5 (1 samples)
	maximum = 52171 (1 samples)
Fragmentation average = 174.982 (1 samples)
	minimum = 0 (1 samples)
	maximum = 548 (1 samples)
Injected packet rate average = 0.0379301 (1 samples)
	minimum = 0.0305714 (1 samples)
	maximum = 0.0441429 (1 samples)
Accepted packet rate average = 0.0167433 (1 samples)
	minimum = 0.0128571 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.682685 (1 samples)
	minimum = 0.549714 (1 samples)
	maximum = 0.793571 (1 samples)
Accepted flit rate average = 0.301618 (1 samples)
	minimum = 0.230143 (1 samples)
	maximum = 0.379143 (1 samples)
Injected packet size average = 17.9985 (1 samples)
Accepted packet size average = 18.0142 (1 samples)
Hops average = 5.06078 (1 samples)
Total run time 93.508
