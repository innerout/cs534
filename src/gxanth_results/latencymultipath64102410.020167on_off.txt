BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 322.353
	minimum = 23
	maximum = 976
Network latency average = 252.194
	minimum = 23
	maximum = 879
Slowest packet = 14
Flit latency average = 177.884
	minimum = 6
	maximum = 980
Slowest flit = 67
Fragmentation average = 146.788
	minimum = 0
	maximum = 749
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.00926042
	minimum = 0.003 (at node 43)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.193062
	minimum = 0.07 (at node 43)
	maximum = 0.337 (at node 103)
Injected packet length average = 17.8591
Accepted packet length average = 20.8481
Total in-flight flits = 30270 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.927
	minimum = 23
	maximum = 1951
Network latency average = 411.072
	minimum = 23
	maximum = 1902
Slowest packet = 247
Flit latency average = 317.917
	minimum = 6
	maximum = 1914
Slowest flit = 5622
Fragmentation average = 198.924
	minimum = 0
	maximum = 1709
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0105781
	minimum = 0.006 (at node 30)
	maximum = 0.0165 (at node 166)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.207984
	minimum = 0.1175 (at node 116)
	maximum = 0.3245 (at node 166)
Injected packet length average = 17.9339
Accepted packet length average = 19.6617
Total in-flight flits = 54576 (0 measured)
latency change    = 0.353907
throughput change = 0.0717452
Class 0:
Packet latency average = 911.842
	minimum = 25
	maximum = 2930
Network latency average = 798.719
	minimum = 25
	maximum = 2879
Slowest packet = 238
Flit latency average = 690.901
	minimum = 6
	maximum = 2902
Slowest flit = 5626
Fragmentation average = 285.163
	minimum = 0
	maximum = 2742
Injected packet rate average = 0.0207448
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0128542
	minimum = 0.002 (at node 153)
	maximum = 0.024 (at node 55)
Injected flit rate average = 0.372271
	minimum = 0 (at node 7)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.237708
	minimum = 0.067 (at node 153)
	maximum = 0.444 (at node 78)
Injected packet length average = 17.9453
Accepted packet length average = 18.4927
Total in-flight flits = 80630 (0 measured)
latency change    = 0.452836
throughput change = 0.125044
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 365.975
	minimum = 28
	maximum = 1226
Network latency average = 277.525
	minimum = 23
	maximum = 979
Slowest packet = 11465
Flit latency average = 987.71
	minimum = 6
	maximum = 3874
Slowest flit = 7612
Fragmentation average = 146.143
	minimum = 0
	maximum = 656
Injected packet rate average = 0.020276
	minimum = 0 (at node 77)
	maximum = 0.055 (at node 34)
Accepted packet rate average = 0.0128438
	minimum = 0.005 (at node 70)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.36649
	minimum = 0 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.23438
	minimum = 0.102 (at node 79)
	maximum = 0.417 (at node 44)
Injected packet length average = 18.075
Accepted packet length average = 18.2486
Total in-flight flits = 105703 (55512 measured)
latency change    = 1.49154
throughput change = 0.0141997
Class 0:
Packet latency average = 669.42
	minimum = 28
	maximum = 2244
Network latency average = 577.471
	minimum = 23
	maximum = 1966
Slowest packet = 11465
Flit latency average = 1137.7
	minimum = 6
	maximum = 4909
Slowest flit = 6185
Fragmentation average = 213.781
	minimum = 0
	maximum = 1280
Injected packet rate average = 0.0198438
	minimum = 0.001 (at node 191)
	maximum = 0.051 (at node 11)
Accepted packet rate average = 0.0129714
	minimum = 0.0065 (at node 75)
	maximum = 0.0195 (at node 44)
Injected flit rate average = 0.357513
	minimum = 0.018 (at node 191)
	maximum = 0.918 (at node 11)
Accepted flit rate average= 0.236544
	minimum = 0.131 (at node 75)
	maximum = 0.3585 (at node 66)
Injected packet length average = 18.0164
Accepted packet length average = 18.2359
Total in-flight flits = 126957 (94388 measured)
latency change    = 0.453295
throughput change = 0.00914866
Class 0:
Packet latency average = 929.157
	minimum = 28
	maximum = 3351
Network latency average = 831.964
	minimum = 23
	maximum = 2960
Slowest packet = 11465
Flit latency average = 1291.49
	minimum = 6
	maximum = 5752
Slowest flit = 14892
Fragmentation average = 250.761
	minimum = 0
	maximum = 1714
Injected packet rate average = 0.0199097
	minimum = 0.004 (at node 131)
	maximum = 0.042 (at node 55)
Accepted packet rate average = 0.0130017
	minimum = 0.00866667 (at node 63)
	maximum = 0.018 (at node 66)
Injected flit rate average = 0.358792
	minimum = 0.072 (at node 131)
	maximum = 0.756 (at node 55)
Accepted flit rate average= 0.236536
	minimum = 0.160667 (at node 162)
	maximum = 0.317667 (at node 66)
Injected packet length average = 18.0209
Accepted packet length average = 18.1927
Total in-flight flits = 150809 (129639 measured)
latency change    = 0.279541
throughput change = 3.30287e-05
Class 0:
Packet latency average = 1171.28
	minimum = 27
	maximum = 4210
Network latency average = 1069.96
	minimum = 23
	maximum = 3843
Slowest packet = 11465
Flit latency average = 1440.33
	minimum = 6
	maximum = 6543
Slowest flit = 25469
Fragmentation average = 274.024
	minimum = 0
	maximum = 2402
Injected packet rate average = 0.0197734
	minimum = 0.0075 (at node 44)
	maximum = 0.04425 (at node 63)
Accepted packet rate average = 0.0129948
	minimum = 0.00975 (at node 4)
	maximum = 0.01825 (at node 51)
Injected flit rate average = 0.356009
	minimum = 0.133 (at node 44)
	maximum = 0.7945 (at node 63)
Accepted flit rate average= 0.2361
	minimum = 0.1705 (at node 144)
	maximum = 0.31425 (at node 95)
Injected packet length average = 18.0044
Accepted packet length average = 18.1688
Total in-flight flits = 172653 (158428 measured)
latency change    = 0.206714
throughput change = 0.00184751
Class 0:
Packet latency average = 1421.47
	minimum = 27
	maximum = 5560
Network latency average = 1316.77
	minimum = 23
	maximum = 4809
Slowest packet = 11465
Flit latency average = 1593.32
	minimum = 6
	maximum = 7528
Slowest flit = 26836
Fragmentation average = 288.923
	minimum = 0
	maximum = 3141
Injected packet rate average = 0.0197396
	minimum = 0.0096 (at node 15)
	maximum = 0.0432 (at node 95)
Accepted packet rate average = 0.013001
	minimum = 0.0098 (at node 71)
	maximum = 0.0172 (at node 51)
Injected flit rate average = 0.355252
	minimum = 0.1722 (at node 15)
	maximum = 0.7746 (at node 95)
Accepted flit rate average= 0.235863
	minimum = 0.181 (at node 173)
	maximum = 0.3036 (at node 99)
Injected packet length average = 17.9969
Accepted packet length average = 18.1418
Total in-flight flits = 195302 (185687 measured)
latency change    = 0.176013
throughput change = 0.00100805
Class 0:
Packet latency average = 1645.95
	minimum = 24
	maximum = 6301
Network latency average = 1538.91
	minimum = 23
	maximum = 5887
Slowest packet = 11465
Flit latency average = 1733.98
	minimum = 6
	maximum = 8368
Slowest flit = 40129
Fragmentation average = 303.472
	minimum = 0
	maximum = 4378
Injected packet rate average = 0.0199583
	minimum = 0.0105 (at node 56)
	maximum = 0.0436667 (at node 95)
Accepted packet rate average = 0.0130148
	minimum = 0.00983333 (at node 63)
	maximum = 0.0165 (at node 51)
Injected flit rate average = 0.359327
	minimum = 0.189 (at node 56)
	maximum = 0.786 (at node 95)
Accepted flit rate average= 0.235378
	minimum = 0.179667 (at node 42)
	maximum = 0.2935 (at node 99)
Injected packet length average = 18.0039
Accepted packet length average = 18.0854
Total in-flight flits = 223331 (216603 measured)
latency change    = 0.13638
throughput change = 0.00206008
Class 0:
Packet latency average = 1853.68
	minimum = 24
	maximum = 7137
Network latency average = 1744.93
	minimum = 23
	maximum = 6957
Slowest packet = 11465
Flit latency average = 1877.78
	minimum = 6
	maximum = 9778
Slowest flit = 5489
Fragmentation average = 312.974
	minimum = 0
	maximum = 4645
Injected packet rate average = 0.0201436
	minimum = 0.009 (at node 118)
	maximum = 0.042 (at node 95)
Accepted packet rate average = 0.0130119
	minimum = 0.01 (at node 42)
	maximum = 0.0164286 (at node 90)
Injected flit rate average = 0.362649
	minimum = 0.164286 (at node 118)
	maximum = 0.755571 (at node 95)
Accepted flit rate average= 0.234716
	minimum = 0.173 (at node 42)
	maximum = 0.290857 (at node 95)
Injected packet length average = 18.0032
Accepted packet length average = 18.0385
Total in-flight flits = 252486 (247565 measured)
latency change    = 0.112065
throughput change = 0.00281971
Draining all recorded packets ...
Class 0:
Remaining flits: 13968 13969 13970 13971 13972 13973 13974 13975 13976 13977 [...] (279071 flits)
Measured flits: 206442 206443 206444 206445 206446 206447 206448 206449 206450 206451 [...] (220547 flits)
Class 0:
Remaining flits: 13978 13979 13980 13981 13982 13983 13984 13985 25938 25939 [...] (306660 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (188599 flits)
Class 0:
Remaining flits: 13978 13979 13980 13981 13982 13983 13984 13985 25938 25939 [...] (329607 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (161530 flits)
Class 0:
Remaining flits: 13978 13979 13980 13981 13982 13983 13984 13985 25938 25939 [...] (358885 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (138066 flits)
Class 0:
Remaining flits: 13978 13979 13980 13981 13982 13983 13984 13985 28494 28495 [...] (386067 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (117513 flits)
Class 0:
Remaining flits: 28494 28495 28496 28497 28498 28499 28500 28501 28502 28503 [...] (412019 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (100254 flits)
Class 0:
Remaining flits: 28494 28495 28496 28497 28498 28499 28500 28501 28502 28503 [...] (437873 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (86260 flits)
Class 0:
Remaining flits: 28494 28495 28496 28497 28498 28499 28500 28501 28502 28503 [...] (468524 flits)
Measured flits: 207414 207415 207416 207417 207418 207419 207420 207421 207422 207423 [...] (73209 flits)
Class 0:
Remaining flits: 28494 28495 28496 28497 28498 28499 28500 28501 28502 28503 [...] (497014 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (62429 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (528986 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (53266 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (557151 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (45244 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (588387 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (39021 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (622085 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (33175 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (652688 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (28318 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (687672 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (24913 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (723415 flits)
Measured flits: 209772 209773 209774 209775 209776 209777 209778 209779 209780 209781 [...] (21735 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (751020 flits)
Measured flits: 229356 229357 229358 229359 229360 229361 229362 229363 229364 229365 [...] (18636 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (785702 flits)
Measured flits: 229356 229357 229358 229359 229360 229361 229362 229363 229364 229365 [...] (16192 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (819554 flits)
Measured flits: 229356 229357 229358 229359 229360 229361 229362 229363 229364 229365 [...] (13835 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (852549 flits)
Measured flits: 229356 229357 229358 229359 229360 229361 229362 229363 229364 229365 [...] (11986 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (882647 flits)
Measured flits: 229356 229357 229358 229359 229360 229361 229362 229363 229364 229365 [...] (10148 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (915269 flits)
Measured flits: 229359 229360 229361 229362 229363 229364 229365 229366 229367 229368 [...] (9086 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (952066 flits)
Measured flits: 242658 242659 242660 242661 242662 242663 242664 242665 242666 242667 [...] (7908 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (986272 flits)
Measured flits: 242658 242659 242660 242661 242662 242663 242664 242665 242666 242667 [...] (7132 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1022228 flits)
Measured flits: 242658 242659 242660 242661 242662 242663 242664 242665 242666 242667 [...] (6314 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1051283 flits)
Measured flits: 242658 242659 242660 242661 242662 242663 242664 242665 242666 242667 [...] (5677 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1087476 flits)
Measured flits: 289503 289504 289505 289506 289507 289508 289509 289510 289511 337320 [...] (4730 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1119559 flits)
Measured flits: 337320 337321 337322 337323 337324 337325 337326 337327 337328 337329 [...] (3814 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1153913 flits)
Measured flits: 337320 337321 337322 337323 337324 337325 337326 337327 337328 337329 [...] (3299 flits)
Class 0:
Remaining flits: 123966 123967 123968 123969 123970 123971 123972 123973 123974 123975 [...] (1192261 flits)
Measured flits: 421393 421394 421395 421396 421397 436644 436645 436646 436647 436648 [...] (2764 flits)
Class 0:
Remaining flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (1224378 flits)
Measured flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (2419 flits)
Class 0:
Remaining flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (1259623 flits)
Measured flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (2138 flits)
Class 0:
Remaining flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (1290701 flits)
Measured flits: 436644 436645 436646 436647 436648 436649 436650 436651 436652 436653 [...] (1914 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1320319 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1643 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1354076 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1421 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1382200 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1234 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1412517 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1133 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1439640 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (896 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1470917 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (785 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1497351 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (607 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1520609 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (531 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1546198 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (399 flits)
Class 0:
Remaining flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (1578766 flits)
Measured flits: 437112 437113 437114 437115 437116 437117 437118 437119 437120 437121 [...] (360 flits)
Class 0:
Remaining flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (1611376 flits)
Measured flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (337 flits)
Class 0:
Remaining flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (1640714 flits)
Measured flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (263 flits)
Class 0:
Remaining flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (1668553 flits)
Measured flits: 437114 437115 437116 437117 437118 437119 437120 437121 437122 437123 [...] (178 flits)
Class 0:
Remaining flits: 437120 437121 437122 437123 437124 437125 437126 437127 437128 437129 [...] (1694323 flits)
Measured flits: 437120 437121 437122 437123 437124 437125 437126 437127 437128 437129 [...] (172 flits)
Class 0:
Remaining flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (1729247 flits)
Measured flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (54 flits)
Class 0:
Remaining flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (1761584 flits)
Measured flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (54 flits)
Class 0:
Remaining flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (1789557 flits)
Measured flits: 577386 577387 577388 577389 577390 577391 577392 577393 577394 577395 [...] (54 flits)
Class 0:
Remaining flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (1822797 flits)
Measured flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (26 flits)
Class 0:
Remaining flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (1854396 flits)
Measured flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (18 flits)
Class 0:
Remaining flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (1879162 flits)
Measured flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (18 flits)
Class 0:
Remaining flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (1906037 flits)
Measured flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (18 flits)
Class 0:
Remaining flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (1940729 flits)
Measured flits: 626796 626797 626798 626799 626800 626801 626802 626803 626804 626805 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1917813 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1886546 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1855828 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1823654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1792570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760842 760843 760844 760845 760846 760847 760848 760849 760850 760851 [...] (1761668 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 835092 835093 835094 835095 835096 835097 835098 835099 835100 835101 [...] (1730806 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 835092 835093 835094 835095 835096 835097 835098 835099 835100 835101 [...] (1700570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 835092 835093 835094 835095 835096 835097 835098 835099 835100 835101 [...] (1670404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 841050 841051 841052 841053 841054 841055 841056 841057 841058 841059 [...] (1638309 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 841050 841051 841052 841053 841054 841055 841056 841057 841058 841059 [...] (1606609 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1574852 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1542450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1510909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1478843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1446718 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1414704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1382027 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 895266 895267 895268 895269 895270 895271 895272 895273 895274 895275 [...] (1350951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 977976 977977 977978 977979 977980 977981 977982 977983 977984 977985 [...] (1319945 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 977976 977977 977978 977979 977980 977981 977982 977983 977984 977985 [...] (1288207 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 977976 977977 977978 977979 977980 977981 977982 977983 977984 977985 [...] (1255794 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 977976 977977 977978 977979 977980 977981 977982 977983 977984 977985 [...] (1223270 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1266426 1266427 1266428 1266429 1266430 1266431 1266432 1266433 1266434 1266435 [...] (1191032 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1266426 1266427 1266428 1266429 1266430 1266431 1266432 1266433 1266434 1266435 [...] (1159807 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1266430 1266431 1266432 1266433 1266434 1266435 1266436 1266437 1266438 1266439 [...] (1128512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (1096641 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (1066466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (1036136 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (1004141 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (971809 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (940117 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (908261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (876707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (844556 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (812489 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (779963 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (748078 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (717678 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (686326 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (657238 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (627443 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (595875 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (564866 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (533378 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (502207 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (470611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329570 1329571 1329572 1329573 1329574 1329575 1329576 1329577 1329578 1329579 [...] (440346 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1400081 1400082 1400083 1400084 1400085 1400086 1400087 1400088 1400089 1400090 [...] (409951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424754 1424755 1424756 1424757 1424758 1424759 1424760 1424761 1424762 1424763 [...] (378572 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424754 1424755 1424756 1424757 1424758 1424759 1424760 1424761 1424762 1424763 [...] (346561 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (314663 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (283303 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (253135 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (223251 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (194353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (166334 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (139499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (114370 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (90127 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (66886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (47179 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (32352 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (20377 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651770 1651771 1651772 1651773 1651774 1651775 1651776 1651777 1651778 1651779 [...] (11750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2331216 2331217 2331218 2331219 2331220 2331221 2331222 2331223 2331224 2331225 [...] (6852 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2331216 2331217 2331218 2331219 2331220 2331221 2331222 2331223 2331224 2331225 [...] (3399 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2905668 2905669 2905670 2905671 2905672 2905673 2905674 2905675 2905676 2905677 [...] (1423 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3033090 3033091 3033092 3033093 3033094 3033095 3033096 3033097 3033098 3033099 [...] (423 flits)
Measured flits: (0 flits)
Time taken is 134708 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5603.44 (1 samples)
	minimum = 24 (1 samples)
	maximum = 56231 (1 samples)
Network latency average = 5478.32 (1 samples)
	minimum = 23 (1 samples)
	maximum = 56180 (1 samples)
Flit latency average = 27940.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 106629 (1 samples)
Fragmentation average = 370.064 (1 samples)
	minimum = 0 (1 samples)
	maximum = 15948 (1 samples)
Injected packet rate average = 0.0201436 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.042 (1 samples)
Accepted packet rate average = 0.0130119 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0164286 (1 samples)
Injected flit rate average = 0.362649 (1 samples)
	minimum = 0.164286 (1 samples)
	maximum = 0.755571 (1 samples)
Accepted flit rate average = 0.234716 (1 samples)
	minimum = 0.173 (1 samples)
	maximum = 0.290857 (1 samples)
Injected packet size average = 18.0032 (1 samples)
Accepted packet size average = 18.0385 (1 samples)
Hops average = 5.08792 (1 samples)
Total run time 333.427
