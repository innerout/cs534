BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 79.3885
	minimum = 22
	maximum = 303
Network latency average = 77.3936
	minimum = 22
	maximum = 295
Slowest packet = 759
Flit latency average = 50.8095
	minimum = 5
	maximum = 278
Slowest flit = 13677
Fragmentation average = 18.4007
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0112448
	minimum = 0.004 (at node 12)
	maximum = 0.021 (at node 162)
Accepted packet rate average = 0.0102292
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 91)
Injected flit rate average = 0.200438
	minimum = 0.072 (at node 12)
	maximum = 0.378 (at node 162)
Accepted flit rate average= 0.187781
	minimum = 0.036 (at node 174)
	maximum = 0.324 (at node 91)
Injected packet length average = 17.8249
Accepted packet length average = 18.3574
Total in-flight flits = 2808 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 87.2633
	minimum = 22
	maximum = 341
Network latency average = 85.2677
	minimum = 22
	maximum = 341
Slowest packet = 2407
Flit latency average = 58.2764
	minimum = 5
	maximum = 324
Slowest flit = 43343
Fragmentation average = 18.9606
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0110964
	minimum = 0.0055 (at node 40)
	maximum = 0.017 (at node 166)
Accepted packet rate average = 0.010651
	minimum = 0.006 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.198698
	minimum = 0.0955 (at node 40)
	maximum = 0.306 (at node 166)
Accepted flit rate average= 0.19351
	minimum = 0.108 (at node 30)
	maximum = 0.306 (at node 22)
Injected packet length average = 17.9066
Accepted packet length average = 18.1682
Total in-flight flits = 2390 (0 measured)
latency change    = 0.0902422
throughput change = 0.0296065
Class 0:
Packet latency average = 87.0868
	minimum = 22
	maximum = 328
Network latency average = 84.9219
	minimum = 22
	maximum = 328
Slowest packet = 5687
Flit latency average = 57.7088
	minimum = 5
	maximum = 311
Slowest flit = 102383
Fragmentation average = 19.4476
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0112969
	minimum = 0.002 (at node 100)
	maximum = 0.022 (at node 144)
Accepted packet rate average = 0.011276
	minimum = 0.005 (at node 121)
	maximum = 0.02 (at node 73)
Injected flit rate average = 0.203667
	minimum = 0.036 (at node 100)
	maximum = 0.386 (at node 144)
Accepted flit rate average= 0.202885
	minimum = 0.077 (at node 184)
	maximum = 0.354 (at node 73)
Injected packet length average = 18.0286
Accepted packet length average = 17.9926
Total in-flight flits = 2478 (0 measured)
latency change    = 0.00202659
throughput change = 0.0462083
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 89.0509
	minimum = 22
	maximum = 314
Network latency average = 86.9862
	minimum = 22
	maximum = 287
Slowest packet = 7301
Flit latency average = 61.2612
	minimum = 5
	maximum = 336
Slowest flit = 142058
Fragmentation average = 19.2834
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0117604
	minimum = 0.002 (at node 104)
	maximum = 0.02 (at node 78)
Accepted packet rate average = 0.0114427
	minimum = 0.003 (at node 163)
	maximum = 0.022 (at node 120)
Injected flit rate average = 0.210964
	minimum = 0.036 (at node 104)
	maximum = 0.36 (at node 117)
Accepted flit rate average= 0.206802
	minimum = 0.064 (at node 163)
	maximum = 0.385 (at node 120)
Injected packet length average = 17.9384
Accepted packet length average = 18.0728
Total in-flight flits = 3416 (3416 measured)
latency change    = 0.022056
throughput change = 0.0189392
Class 0:
Packet latency average = 93.5693
	minimum = 22
	maximum = 447
Network latency average = 91.4724
	minimum = 22
	maximum = 447
Slowest packet = 9036
Flit latency average = 64.392
	minimum = 5
	maximum = 430
Slowest flit = 162648
Fragmentation average = 19.8294
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0114141
	minimum = 0.0065 (at node 18)
	maximum = 0.017 (at node 117)
Accepted packet rate average = 0.0114479
	minimum = 0.0065 (at node 86)
	maximum = 0.019 (at node 182)
Injected flit rate average = 0.20537
	minimum = 0.117 (at node 18)
	maximum = 0.3115 (at node 172)
Accepted flit rate average= 0.205924
	minimum = 0.117 (at node 86)
	maximum = 0.341 (at node 182)
Injected packet length average = 17.9927
Accepted packet length average = 17.9879
Total in-flight flits = 2297 (2297 measured)
latency change    = 0.0482889
throughput change = 0.00426178
Class 0:
Packet latency average = 89.9643
	minimum = 22
	maximum = 447
Network latency average = 87.8462
	minimum = 22
	maximum = 447
Slowest packet = 9036
Flit latency average = 60.9113
	minimum = 5
	maximum = 430
Slowest flit = 162648
Fragmentation average = 19.4813
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0113872
	minimum = 0.007 (at node 104)
	maximum = 0.0163333 (at node 172)
Accepted packet rate average = 0.0113455
	minimum = 0.00566667 (at node 4)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.205071
	minimum = 0.126 (at node 104)
	maximum = 0.297667 (at node 172)
Accepted flit rate average= 0.204398
	minimum = 0.105667 (at node 4)
	maximum = 0.313333 (at node 129)
Injected packet length average = 18.009
Accepted packet length average = 18.0158
Total in-flight flits = 2807 (2807 measured)
latency change    = 0.0400713
throughput change = 0.00747029
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6359 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 90.6886 (1 samples)
	minimum = 22 (1 samples)
	maximum = 447 (1 samples)
Network latency average = 88.5515 (1 samples)
	minimum = 22 (1 samples)
	maximum = 447 (1 samples)
Flit latency average = 61.3413 (1 samples)
	minimum = 5 (1 samples)
	maximum = 430 (1 samples)
Fragmentation average = 19.7313 (1 samples)
	minimum = 0 (1 samples)
	maximum = 160 (1 samples)
Injected packet rate average = 0.0113872 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0163333 (1 samples)
Accepted packet rate average = 0.0113455 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.205071 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.297667 (1 samples)
Accepted flit rate average = 0.204398 (1 samples)
	minimum = 0.105667 (1 samples)
	maximum = 0.313333 (1 samples)
Injected packet size average = 18.009 (1 samples)
Accepted packet size average = 18.0158 (1 samples)
Hops average = 5.06627 (1 samples)
Total run time 4.33325
