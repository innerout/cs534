BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.993
	minimum = 22
	maximum = 975
Network latency average = 242.056
	minimum = 22
	maximum = 756
Slowest packet = 90
Flit latency average = 211.603
	minimum = 5
	maximum = 759
Slowest flit = 21295
Fragmentation average = 48.5033
	minimum = 0
	maximum = 492
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.013599
	minimum = 0.006 (at node 41)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.257078
	minimum = 0.114 (at node 41)
	maximum = 0.456 (at node 76)
Injected packet length average = 17.8246
Accepted packet length average = 18.9043
Total in-flight flits = 51603 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 620.676
	minimum = 22
	maximum = 1963
Network latency average = 456.237
	minimum = 22
	maximum = 1484
Slowest packet = 90
Flit latency average = 422.691
	minimum = 5
	maximum = 1467
Slowest flit = 47051
Fragmentation average = 69.4316
	minimum = 0
	maximum = 837
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0146094
	minimum = 0.0085 (at node 62)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.561727
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 28)
Accepted flit rate average= 0.272453
	minimum = 0.163 (at node 62)
	maximum = 0.39 (at node 152)
Injected packet length average = 17.9111
Accepted packet length average = 18.6492
Total in-flight flits = 112152 (0 measured)
latency change    = 0.429665
throughput change = 0.0564317
Class 0:
Packet latency average = 1349.37
	minimum = 26
	maximum = 2792
Network latency average = 1087.62
	minimum = 22
	maximum = 2278
Slowest packet = 4592
Flit latency average = 1049.24
	minimum = 5
	maximum = 2336
Slowest flit = 60226
Fragmentation average = 123.665
	minimum = 0
	maximum = 855
Injected packet rate average = 0.0354427
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0161042
	minimum = 0.008 (at node 75)
	maximum = 0.027 (at node 182)
Injected flit rate average = 0.638953
	minimum = 0 (at node 14)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.294687
	minimum = 0.17 (at node 180)
	maximum = 0.535 (at node 182)
Injected packet length average = 18.0278
Accepted packet length average = 18.2988
Total in-flight flits = 178062 (0 measured)
latency change    = 0.540024
throughput change = 0.0754507
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 389.019
	minimum = 23
	maximum = 2317
Network latency average = 35.3357
	minimum = 22
	maximum = 75
Slowest packet = 18901
Flit latency average = 1473.98
	minimum = 5
	maximum = 3307
Slowest flit = 64021
Fragmentation average = 6.51304
	minimum = 0
	maximum = 33
Injected packet rate average = 0.03525
	minimum = 0 (at node 42)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0162813
	minimum = 0.006 (at node 88)
	maximum = 0.03 (at node 123)
Injected flit rate average = 0.633786
	minimum = 0 (at node 42)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.297865
	minimum = 0.132 (at node 88)
	maximum = 0.541 (at node 123)
Injected packet length average = 17.9798
Accepted packet length average = 18.2949
Total in-flight flits = 242696 (111363 measured)
latency change    = 2.46864
throughput change = 0.0106662
Class 0:
Packet latency average = 480.187
	minimum = 23
	maximum = 2668
Network latency average = 130.629
	minimum = 22
	maximum = 1978
Slowest packet = 18901
Flit latency average = 1708.29
	minimum = 5
	maximum = 4154
Slowest flit = 79674
Fragmentation average = 9.86583
	minimum = 0
	maximum = 259
Injected packet rate average = 0.0349375
	minimum = 0.002 (at node 86)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0162161
	minimum = 0.0085 (at node 88)
	maximum = 0.0225 (at node 19)
Injected flit rate average = 0.62856
	minimum = 0.036 (at node 86)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.297057
	minimum = 0.1795 (at node 36)
	maximum = 0.427 (at node 103)
Injected packet length average = 17.991
Accepted packet length average = 18.3186
Total in-flight flits = 305480 (219593 measured)
latency change    = 0.189859
throughput change = 0.00271763
Class 0:
Packet latency average = 884.869
	minimum = 23
	maximum = 3685
Network latency average = 552.051
	minimum = 22
	maximum = 2982
Slowest packet = 18901
Flit latency average = 1943.25
	minimum = 5
	maximum = 4973
Slowest flit = 102504
Fragmentation average = 35.0227
	minimum = 0
	maximum = 689
Injected packet rate average = 0.0351771
	minimum = 0.00766667 (at node 60)
	maximum = 0.0556667 (at node 0)
Accepted packet rate average = 0.0162569
	minimum = 0.011 (at node 100)
	maximum = 0.022 (at node 95)
Injected flit rate average = 0.632896
	minimum = 0.138 (at node 60)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.297543
	minimum = 0.202 (at node 100)
	maximum = 0.403 (at node 95)
Injected packet length average = 17.9917
Accepted packet length average = 18.3025
Total in-flight flits = 371393 (323521 measured)
latency change    = 0.457336
throughput change = 0.00163375
Draining remaining packets ...
Class 0:
Remaining flits: 109018 109019 109020 109021 109022 109023 109024 109025 111028 111029 [...] (323564 flits)
Measured flits: 339268 339269 339270 339271 339272 339273 339274 339275 339276 339277 [...] (299121 flits)
Class 0:
Remaining flits: 149084 149085 149086 149087 149088 149089 149090 149091 149092 149093 [...] (276106 flits)
Measured flits: 339318 339319 339320 339321 339322 339323 339324 339325 339326 339327 [...] (264590 flits)
Class 0:
Remaining flits: 215241 215242 215243 221592 221593 221594 221595 221596 221597 226609 [...] (229007 flits)
Measured flits: 339408 339409 339410 339411 339412 339413 339414 339415 339416 339417 [...] (224304 flits)
Class 0:
Remaining flits: 230633 236772 236773 236774 236775 236776 236777 236778 236779 236780 [...] (182396 flits)
Measured flits: 339408 339409 339410 339411 339412 339413 339414 339415 339416 339417 [...] (180874 flits)
Class 0:
Remaining flits: 253598 253599 253600 253601 272173 272174 272175 272176 272177 276235 [...] (135769 flits)
Measured flits: 339420 339421 339422 339423 339424 339425 341058 341059 341060 341061 [...] (135456 flits)
Class 0:
Remaining flits: 326604 326605 326606 326607 326608 326609 329102 329103 329104 329105 [...] (90274 flits)
Measured flits: 344340 344341 344342 344343 344344 344345 344346 344347 344348 344349 [...] (90258 flits)
Class 0:
Remaining flits: 365286 365287 365288 365289 365290 365291 367489 367490 367491 367492 [...] (49549 flits)
Measured flits: 365286 365287 365288 365289 365290 365291 367489 367490 367491 367492 [...] (49549 flits)
Class 0:
Remaining flits: 418879 418880 418881 418882 418883 418884 418885 418886 418887 418888 [...] (20122 flits)
Measured flits: 418879 418880 418881 418882 418883 418884 418885 418886 418887 418888 [...] (20122 flits)
Class 0:
Remaining flits: 471604 471605 471606 471607 471608 471609 471610 471611 471612 471613 [...] (7247 flits)
Measured flits: 471604 471605 471606 471607 471608 471609 471610 471611 471612 471613 [...] (7247 flits)
Class 0:
Remaining flits: 571616 571617 571618 571619 571620 571621 571622 571623 571624 571625 [...] (1293 flits)
Measured flits: 571616 571617 571618 571619 571620 571621 571622 571623 571624 571625 [...] (1293 flits)
Time taken is 16564 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5796.04 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12919 (1 samples)
Network latency average = 5434.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11230 (1 samples)
Flit latency average = 4533.89 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11213 (1 samples)
Fragmentation average = 277.395 (1 samples)
	minimum = 0 (1 samples)
	maximum = 985 (1 samples)
Injected packet rate average = 0.0351771 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0162569 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.632896 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.297543 (1 samples)
	minimum = 0.202 (1 samples)
	maximum = 0.403 (1 samples)
Injected packet size average = 17.9917 (1 samples)
Accepted packet size average = 18.3025 (1 samples)
Hops average = 5.07507 (1 samples)
Total run time 17.0465
