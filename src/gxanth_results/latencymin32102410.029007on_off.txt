BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 357.314
	minimum = 23
	maximum = 989
Network latency average = 268.449
	minimum = 23
	maximum = 927
Slowest packet = 3
Flit latency average = 202.717
	minimum = 6
	maximum = 956
Slowest flit = 3803
Fragmentation average = 142.742
	minimum = 0
	maximum = 806
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0101146
	minimum = 0.004 (at node 178)
	maximum = 0.017 (at node 132)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.207573
	minimum = 0.09 (at node 25)
	maximum = 0.336 (at node 121)
Injected packet length average = 17.8192
Accepted packet length average = 20.5221
Total in-flight flits = 48364 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 593.026
	minimum = 23
	maximum = 1929
Network latency average = 468.941
	minimum = 23
	maximum = 1797
Slowest packet = 170
Flit latency average = 396.484
	minimum = 6
	maximum = 1858
Slowest flit = 8236
Fragmentation average = 177.225
	minimum = 0
	maximum = 1322
Injected packet rate average = 0.0268229
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.011263
	minimum = 0.007 (at node 101)
	maximum = 0.017 (at node 13)
Injected flit rate average = 0.480539
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.21738
	minimum = 0.1295 (at node 135)
	maximum = 0.322 (at node 13)
Injected packet length average = 17.9152
Accepted packet length average = 19.3003
Total in-flight flits = 101926 (0 measured)
latency change    = 0.397474
throughput change = 0.0451158
Class 0:
Packet latency average = 1243.92
	minimum = 25
	maximum = 2897
Network latency average = 1059.22
	minimum = 25
	maximum = 2797
Slowest packet = 3487
Flit latency average = 989.717
	minimum = 6
	maximum = 2780
Slowest flit = 9917
Fragmentation average = 216.248
	minimum = 0
	maximum = 1905
Injected packet rate average = 0.0294115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0130833
	minimum = 0.004 (at node 117)
	maximum = 0.024 (at node 90)
Injected flit rate average = 0.529307
	minimum = 0 (at node 8)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.234984
	minimum = 0.072 (at node 117)
	maximum = 0.411 (at node 155)
Injected packet length average = 17.9966
Accepted packet length average = 17.9606
Total in-flight flits = 158455 (0 measured)
latency change    = 0.523261
throughput change = 0.0749163
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 333.44
	minimum = 25
	maximum = 1432
Network latency average = 122.41
	minimum = 24
	maximum = 928
Slowest packet = 16006
Flit latency average = 1403.53
	minimum = 6
	maximum = 3721
Slowest flit = 21375
Fragmentation average = 37.6698
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0308802
	minimum = 0.001 (at node 58)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0130417
	minimum = 0.005 (at node 100)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.555734
	minimum = 0.018 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.233245
	minimum = 0.098 (at node 145)
	maximum = 0.395 (at node 129)
Injected packet length average = 17.9965
Accepted packet length average = 17.8846
Total in-flight flits = 220394 (96717 measured)
latency change    = 2.73057
throughput change = 0.00745819
Class 0:
Packet latency average = 563.545
	minimum = 25
	maximum = 2246
Network latency average = 364.717
	minimum = 23
	maximum = 1941
Slowest packet = 16006
Flit latency average = 1628.85
	minimum = 6
	maximum = 4482
Slowest flit = 40649
Fragmentation average = 75.621
	minimum = 0
	maximum = 1052
Injected packet rate average = 0.0296953
	minimum = 0.003 (at node 58)
	maximum = 0.0555 (at node 6)
Accepted packet rate average = 0.0130365
	minimum = 0.008 (at node 79)
	maximum = 0.0205 (at node 123)
Injected flit rate average = 0.53482
	minimum = 0.054 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.232888
	minimum = 0.125 (at node 110)
	maximum = 0.3805 (at node 123)
Injected packet length average = 18.0103
Accepted packet length average = 17.8644
Total in-flight flits = 274280 (179700 measured)
latency change    = 0.408316
throughput change = 0.00153194
Class 0:
Packet latency average = 886.934
	minimum = 23
	maximum = 3744
Network latency average = 690.447
	minimum = 23
	maximum = 2977
Slowest packet = 16006
Flit latency average = 1849.37
	minimum = 6
	maximum = 5470
Slowest flit = 42555
Fragmentation average = 95.6495
	minimum = 0
	maximum = 1052
Injected packet rate average = 0.0294514
	minimum = 0.00466667 (at node 58)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0129844
	minimum = 0.008 (at node 79)
	maximum = 0.0216667 (at node 123)
Injected flit rate average = 0.530122
	minimum = 0.084 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.232148
	minimum = 0.138 (at node 155)
	maximum = 0.388333 (at node 123)
Injected packet length average = 17.9999
Accepted packet length average = 17.879
Total in-flight flits = 330090 (258410 measured)
latency change    = 0.364615
throughput change = 0.00318957
Class 0:
Packet latency average = 1257.82
	minimum = 23
	maximum = 4648
Network latency average = 1058.37
	minimum = 23
	maximum = 3964
Slowest packet = 16006
Flit latency average = 2080.23
	minimum = 6
	maximum = 6414
Slowest flit = 44855
Fragmentation average = 112.158
	minimum = 0
	maximum = 1370
Injected packet rate average = 0.0295482
	minimum = 0.00875 (at node 58)
	maximum = 0.05325 (at node 112)
Accepted packet rate average = 0.0129271
	minimum = 0.00825 (at node 79)
	maximum = 0.01925 (at node 123)
Injected flit rate average = 0.532022
	minimum = 0.1575 (at node 58)
	maximum = 0.962 (at node 112)
Accepted flit rate average= 0.231635
	minimum = 0.149 (at node 79)
	maximum = 0.341 (at node 123)
Injected packet length average = 18.0052
Accepted packet length average = 17.9186
Total in-flight flits = 389033 (335683 measured)
latency change    = 0.294865
throughput change = 0.00221103
Class 0:
Packet latency average = 1651.97
	minimum = 23
	maximum = 5721
Network latency average = 1450.94
	minimum = 23
	maximum = 4956
Slowest packet = 16006
Flit latency average = 2336.72
	minimum = 6
	maximum = 7725
Slowest flit = 21496
Fragmentation average = 125.85
	minimum = 0
	maximum = 2332
Injected packet rate average = 0.0293969
	minimum = 0.0124 (at node 58)
	maximum = 0.051 (at node 17)
Accepted packet rate average = 0.0128427
	minimum = 0.0088 (at node 79)
	maximum = 0.0172 (at node 103)
Injected flit rate average = 0.529041
	minimum = 0.2232 (at node 58)
	maximum = 0.9192 (at node 17)
Accepted flit rate average= 0.230656
	minimum = 0.1574 (at node 104)
	maximum = 0.3074 (at node 129)
Injected packet length average = 17.9965
Accepted packet length average = 17.9601
Total in-flight flits = 445003 (406487 measured)
latency change    = 0.238595
throughput change = 0.00424513
Class 0:
Packet latency average = 2040.82
	minimum = 23
	maximum = 6849
Network latency average = 1833.5
	minimum = 23
	maximum = 5907
Slowest packet = 16006
Flit latency average = 2571.53
	minimum = 6
	maximum = 8119
Slowest flit = 74832
Fragmentation average = 133.982
	minimum = 0
	maximum = 2332
Injected packet rate average = 0.0297274
	minimum = 0.0135 (at node 58)
	maximum = 0.0473333 (at node 17)
Accepted packet rate average = 0.0128863
	minimum = 0.009 (at node 104)
	maximum = 0.0173333 (at node 115)
Injected flit rate average = 0.535079
	minimum = 0.243 (at node 58)
	maximum = 0.853667 (at node 17)
Accepted flit rate average= 0.231268
	minimum = 0.155833 (at node 104)
	maximum = 0.308 (at node 115)
Injected packet length average = 17.9995
Accepted packet length average = 17.9469
Total in-flight flits = 508462 (481071 measured)
latency change    = 0.190533
throughput change = 0.00264619
Class 0:
Packet latency average = 2449.25
	minimum = 23
	maximum = 8067
Network latency average = 2239.96
	minimum = 23
	maximum = 6946
Slowest packet = 16006
Flit latency average = 2816.8
	minimum = 6
	maximum = 9424
Slowest flit = 45773
Fragmentation average = 142.12
	minimum = 0
	maximum = 2508
Injected packet rate average = 0.0296756
	minimum = 0.0141429 (at node 113)
	maximum = 0.0485714 (at node 17)
Accepted packet rate average = 0.0128408
	minimum = 0.00957143 (at node 104)
	maximum = 0.0167143 (at node 123)
Injected flit rate average = 0.534177
	minimum = 0.255429 (at node 113)
	maximum = 0.874571 (at node 17)
Accepted flit rate average= 0.230406
	minimum = 0.170143 (at node 104)
	maximum = 0.305714 (at node 123)
Injected packet length average = 18.0006
Accepted packet length average = 17.9433
Total in-flight flits = 566701 (547190 measured)
latency change    = 0.166759
throughput change = 0.00374113
Draining all recorded packets ...
Class 0:
Remaining flits: 63972 63973 63974 63975 63976 63977 63978 63979 63980 63981 [...] (618906 flits)
Measured flits: 287046 287047 287048 287049 287050 287051 287052 287053 287054 287055 [...] (535643 flits)
Class 0:
Remaining flits: 67536 67537 67538 67539 67540 67541 67542 67543 67544 67545 [...] (677882 flits)
Measured flits: 287082 287083 287084 287085 287086 287087 287088 287089 287090 287091 [...] (506158 flits)
Class 0:
Remaining flits: 67536 67537 67538 67539 67540 67541 67542 67543 67544 67545 [...] (729883 flits)
Measured flits: 287082 287083 287084 287085 287086 287087 287088 287089 287090 287091 [...] (474885 flits)
Class 0:
Remaining flits: 67536 67537 67538 67539 67540 67541 67542 67543 67544 67545 [...] (782467 flits)
Measured flits: 287085 287086 287087 287088 287089 287090 287091 287092 287093 287094 [...] (443229 flits)
Class 0:
Remaining flits: 67542 67543 67544 67545 67546 67547 67548 67549 67550 67551 [...] (829353 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (411074 flits)
Class 0:
Remaining flits: 104816 104817 104818 104819 104820 104821 104822 104823 104824 104825 [...] (878362 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (379578 flits)
Class 0:
Remaining flits: 164042 164043 164044 164045 164046 164047 164048 164049 164050 164051 [...] (925908 flits)
Measured flits: 287568 287569 287570 287571 287572 287573 287574 287575 287576 287577 [...] (348792 flits)
Class 0:
Remaining flits: 165528 165529 165530 165531 165532 165533 165534 165535 165536 165537 [...] (973031 flits)
Measured flits: 289695 289696 289697 289698 289699 289700 289701 289702 289703 289704 [...] (318981 flits)
Class 0:
Remaining flits: 165535 165536 165537 165538 165539 165540 165541 165542 165543 165544 [...] (1025020 flits)
Measured flits: 290034 290035 290036 290037 290038 290039 290040 290041 290042 290043 [...] (290210 flits)
Class 0:
Remaining flits: 179100 179101 179102 179103 179104 179105 179106 179107 179108 179109 [...] (1075365 flits)
Measured flits: 290034 290035 290036 290037 290038 290039 290040 290041 290042 290043 [...] (262295 flits)
Class 0:
Remaining flits: 189450 189451 189452 189453 189454 189455 189456 189457 189458 189459 [...] (1121359 flits)
Measured flits: 290430 290431 290432 290433 290434 290435 290436 290437 290438 290439 [...] (235815 flits)
Class 0:
Remaining flits: 189450 189451 189452 189453 189454 189455 189456 189457 189458 189459 [...] (1163866 flits)
Measured flits: 290430 290431 290432 290433 290434 290435 290436 290437 290438 290439 [...] (210060 flits)
Class 0:
Remaining flits: 242118 242119 242120 242121 242122 242123 242124 242125 242126 242127 [...] (1212586 flits)
Measured flits: 290430 290431 290432 290433 290434 290435 290436 290437 290438 290439 [...] (186623 flits)
Class 0:
Remaining flits: 263376 263377 263378 263379 263380 263381 263382 263383 263384 263385 [...] (1265341 flits)
Measured flits: 290430 290431 290432 290433 290434 290435 290436 290437 290438 290439 [...] (165948 flits)
Class 0:
Remaining flits: 263379 263380 263381 263382 263383 263384 263385 263386 263387 263388 [...] (1318173 flits)
Measured flits: 290430 290431 290432 290433 290434 290435 290436 290437 290438 290439 [...] (146209 flits)
Class 0:
Remaining flits: 286074 286075 286076 286077 286078 286079 286080 286081 286082 286083 [...] (1367518 flits)
Measured flits: 290439 290440 290441 290442 290443 290444 290445 290446 290447 316188 [...] (128284 flits)
Class 0:
Remaining flits: 286074 286075 286076 286077 286078 286079 286080 286081 286082 286083 [...] (1423246 flits)
Measured flits: 316188 316189 316190 316191 316192 316193 316194 316195 316196 316197 [...] (111923 flits)
Class 0:
Remaining flits: 286082 286083 286084 286085 286086 286087 286088 286089 286090 286091 [...] (1475719 flits)
Measured flits: 316192 316193 316194 316195 316196 316197 316198 316199 316200 316201 [...] (97137 flits)
Class 0:
Remaining flits: 286091 316192 316193 316194 316195 316196 316197 316198 316199 316200 [...] (1521685 flits)
Measured flits: 316192 316193 316194 316195 316196 316197 316198 316199 316200 316201 [...] (84430 flits)
Class 0:
Remaining flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (1574991 flits)
Measured flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (72913 flits)
Class 0:
Remaining flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (1622298 flits)
Measured flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (62726 flits)
Class 0:
Remaining flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (1672539 flits)
Measured flits: 338022 338023 338024 338025 338026 338027 338028 338029 338030 338031 [...] (53631 flits)
Class 0:
Remaining flits: 396522 396523 396524 396525 396526 396527 396528 396529 396530 396531 [...] (1719946 flits)
Measured flits: 396522 396523 396524 396525 396526 396527 396528 396529 396530 396531 [...] (44956 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1768928 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (38619 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1820221 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (33151 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1863442 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (28646 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1906211 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (23784 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1950184 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (19562 flits)
Class 0:
Remaining flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (1994043 flits)
Measured flits: 430416 430417 430418 430419 430420 430421 430422 430423 430424 430425 [...] (16106 flits)
Class 0:
Remaining flits: 532245 532246 532247 532248 532249 532250 532251 532252 532253 532254 [...] (2036819 flits)
Measured flits: 532245 532246 532247 532248 532249 532250 532251 532252 532253 532254 [...] (13540 flits)
Class 0:
Remaining flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (2077173 flits)
Measured flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (11306 flits)
Class 0:
Remaining flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (2118027 flits)
Measured flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (9115 flits)
Class 0:
Remaining flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (2155051 flits)
Measured flits: 551106 551107 551108 551109 551110 551111 551112 551113 551114 551115 [...] (7214 flits)
Class 0:
Remaining flits: 615492 615493 615494 615495 615496 615497 615498 615499 615500 615501 [...] (2193426 flits)
Measured flits: 615492 615493 615494 615495 615496 615497 615498 615499 615500 615501 [...] (5667 flits)
Class 0:
Remaining flits: 615492 615493 615494 615495 615496 615497 615498 615499 615500 615501 [...] (2232894 flits)
Measured flits: 615492 615493 615494 615495 615496 615497 615498 615499 615500 615501 [...] (4387 flits)
Class 0:
Remaining flits: 718340 718341 718342 718343 718740 718741 718742 718743 718744 718745 [...] (2260492 flits)
Measured flits: 718340 718341 718342 718343 718740 718741 718742 718743 718744 718745 [...] (3631 flits)
Class 0:
Remaining flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (2282896 flits)
Measured flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (2814 flits)
Class 0:
Remaining flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (2309641 flits)
Measured flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (2128 flits)
Class 0:
Remaining flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (2337231 flits)
Measured flits: 751734 751735 751736 751737 751738 751739 751740 751741 751742 751743 [...] (1678 flits)
Class 0:
Remaining flits: 771516 771517 771518 771519 771520 771521 771522 771523 771524 771525 [...] (2355330 flits)
Measured flits: 771516 771517 771518 771519 771520 771521 771522 771523 771524 771525 [...] (1318 flits)
Class 0:
Remaining flits: 819900 819901 819902 819903 819904 819905 819906 819907 819908 819909 [...] (2375021 flits)
Measured flits: 819900 819901 819902 819903 819904 819905 819906 819907 819908 819909 [...] (849 flits)
Class 0:
Remaining flits: 819900 819901 819902 819903 819904 819905 819906 819907 819908 819909 [...] (2390525 flits)
Measured flits: 819900 819901 819902 819903 819904 819905 819906 819907 819908 819909 [...] (682 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2403772 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (503 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2416503 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (427 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2426492 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (316 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2435722 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (263 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2444327 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (234 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2452678 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (168 flits)
Class 0:
Remaining flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (2456630 flits)
Measured flits: 835668 835669 835670 835671 835672 835673 835674 835675 835676 835677 [...] (126 flits)
Class 0:
Remaining flits: 855031 855032 855033 855034 855035 953406 953407 953408 953409 953410 [...] (2460405 flits)
Measured flits: 855031 855032 855033 855034 855035 953406 953407 953408 953409 953410 [...] (77 flits)
Class 0:
Remaining flits: 953406 953407 953408 953409 953410 953411 953412 953413 953414 953415 [...] (2458942 flits)
Measured flits: 953406 953407 953408 953409 953410 953411 953412 953413 953414 953415 [...] (36 flits)
Class 0:
Remaining flits: 953406 953407 953408 953409 953410 953411 953412 953413 953414 953415 [...] (2458824 flits)
Measured flits: 953406 953407 953408 953409 953410 953411 953412 953413 953414 953415 [...] (36 flits)
Class 0:
Remaining flits: 966726 966727 966728 966729 966730 966731 966732 966733 966734 966735 [...] (2457176 flits)
Measured flits: 966726 966727 966728 966729 966730 966731 966732 966733 966734 966735 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1148220 1148221 1148222 1148223 1148224 1148225 1148226 1148227 1148228 1148229 [...] (2418784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1148220 1148221 1148222 1148223 1148224 1148225 1148226 1148227 1148228 1148229 [...] (2384344 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1148220 1148221 1148222 1148223 1148224 1148225 1148226 1148227 1148228 1148229 [...] (2349868 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1148220 1148221 1148222 1148223 1148224 1148225 1148226 1148227 1148228 1148229 [...] (2315745 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2282053 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2247819 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2212277 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2177433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2142309 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2107597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2073007 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2037433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (2001883 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174518 1174519 1174520 1174521 1174522 1174523 1174524 1174525 1174526 1174527 [...] (1967775 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1932562 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1897258 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1862345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1827361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1792402 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1757030 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263780 1263781 1263782 1263783 1263784 1263785 1263786 1263787 1263788 1263789 [...] (1721706 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1373328 1373329 1373330 1373331 1373332 1373333 1373334 1373335 1373336 1373337 [...] (1686909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1373328 1373329 1373330 1373331 1373332 1373333 1373334 1373335 1373336 1373337 [...] (1651550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1616035 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1580688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1545071 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1509462 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1474272 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391490 1391491 1391492 1391493 1391494 1391495 1391496 1391497 1391498 1391499 [...] (1439868 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1593918 1593919 1593920 1593921 1593922 1593923 1593924 1593925 1593926 1593927 [...] (1404754 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1593918 1593919 1593920 1593921 1593922 1593923 1593924 1593925 1593926 1593927 [...] (1369066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1593918 1593919 1593920 1593921 1593922 1593923 1593924 1593925 1593926 1593927 [...] (1334858 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1602720 1602721 1602722 1602723 1602724 1602725 1602726 1602727 1602728 1602729 [...] (1300448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1602720 1602721 1602722 1602723 1602724 1602725 1602726 1602727 1602728 1602729 [...] (1266502 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1696797 1696798 1696799 1696800 1696801 1696802 1696803 1696804 1696805 1805544 [...] (1232098 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805548 1805549 1805550 1805551 1805552 1805553 1805554 1805555 1805556 1805557 [...] (1198437 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1823382 1823383 1823384 1823385 1823386 1823387 1823388 1823389 1823390 1823391 [...] (1163896 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1953540 1953541 1953542 1953543 1953544 1953545 1953546 1953547 1953548 1953549 [...] (1130034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1953540 1953541 1953542 1953543 1953544 1953545 1953546 1953547 1953548 1953549 [...] (1096620 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1980774 1980775 1980776 1980777 1980778 1980779 1980780 1980781 1980782 1980783 [...] (1062428 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1991880 1991881 1991882 1991883 1991884 1991885 1991886 1991887 1991888 1991889 [...] (1028413 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2110914 2110915 2110916 2110917 2110918 2110919 2110920 2110921 2110922 2110923 [...] (993876 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2110914 2110915 2110916 2110917 2110918 2110919 2110920 2110921 2110922 2110923 [...] (959911 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2144844 2144845 2144846 2144847 2144848 2144849 2144850 2144851 2144852 2144853 [...] (926291 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154864 2154865 2154866 2154867 2154868 2154869 2206138 2206139 2206140 2206141 [...] (892496 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211534 2211535 2211536 2211537 2211538 2211539 2211540 2211541 2211542 2211543 [...] (858874 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211534 2211535 2211536 2211537 2211538 2211539 2211540 2211541 2211542 2211543 [...] (825549 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211534 2211535 2211536 2211537 2211538 2211539 2211540 2211541 2211542 2211543 [...] (792062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211534 2211535 2211536 2211537 2211538 2211539 2211540 2211541 2211542 2211543 [...] (757952 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211534 2211535 2211536 2211537 2211538 2211539 2211540 2211541 2211542 2211543 [...] (724537 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211540 2211541 2211542 2211543 2211544 2211545 2211546 2211547 2211548 2211549 [...] (690691 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2321334 2321335 2321336 2321337 2321338 2321339 2321340 2321341 2321342 2321343 [...] (656450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2348154 2348155 2348156 2348157 2348158 2348159 2348160 2348161 2348162 2348163 [...] (622439 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2348154 2348155 2348156 2348157 2348158 2348159 2348160 2348161 2348162 2348163 [...] (588920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2348154 2348155 2348156 2348157 2348158 2348159 2348160 2348161 2348162 2348163 [...] (554894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2348154 2348155 2348156 2348157 2348158 2348159 2348160 2348161 2348162 2348163 [...] (520328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2348154 2348155 2348156 2348157 2348158 2348159 2348160 2348161 2348162 2348163 [...] (485931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2408576 2408577 2408578 2408579 2500956 2500957 2500958 2500959 2500960 2500961 [...] (451768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2408576 2408577 2408578 2408579 2500956 2500957 2500958 2500959 2500960 2500961 [...] (417008 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2521728 2521729 2521730 2521731 2521732 2521733 2521734 2521735 2521736 2521737 [...] (382265 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2521728 2521729 2521730 2521731 2521732 2521733 2521734 2521735 2521736 2521737 [...] (347736 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2521728 2521729 2521730 2521731 2521732 2521733 2521734 2521735 2521736 2521737 [...] (312802 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (278113 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (244002 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (210806 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (178449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (146306 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (116750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (88942 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624436 2624437 2624438 2624439 2624440 2624441 2624442 2624443 2624444 2624445 [...] (63036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624440 2624441 2624442 2624443 2624444 2624445 2624446 2624447 2624448 2624449 [...] (40506 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624452 2624453 2846520 2846521 2846522 2846523 2846524 2846525 2846526 2846527 [...] (22951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2624452 2624453 3141036 3141037 3141038 3141039 3141040 3141041 3141042 3141043 [...] (11755 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3291210 3291211 3291212 3291213 3291214 3291215 3291216 3291217 3291218 3291219 [...] (6249 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3291210 3291211 3291212 3291213 3291214 3291215 3291216 3291217 3291218 3291219 [...] (2337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3912696 3912697 3912698 3912699 3912700 3912701 3912702 3912703 3912704 3912705 [...] (557 flits)
Measured flits: (0 flits)
Time taken is 140483 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11329 (1 samples)
	minimum = 23 (1 samples)
	maximum = 54459 (1 samples)
Network latency average = 11069.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 54298 (1 samples)
Flit latency average = 38300.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 109312 (1 samples)
Fragmentation average = 199.026 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5289 (1 samples)
Injected packet rate average = 0.0296756 (1 samples)
	minimum = 0.0141429 (1 samples)
	maximum = 0.0485714 (1 samples)
Accepted packet rate average = 0.0128408 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0167143 (1 samples)
Injected flit rate average = 0.534177 (1 samples)
	minimum = 0.255429 (1 samples)
	maximum = 0.874571 (1 samples)
Accepted flit rate average = 0.230406 (1 samples)
	minimum = 0.170143 (1 samples)
	maximum = 0.305714 (1 samples)
Injected packet size average = 18.0006 (1 samples)
Accepted packet size average = 17.9433 (1 samples)
Hops average = 5.05488 (1 samples)
Total run time 176.995
