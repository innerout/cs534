BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 305.067
	minimum = 22
	maximum = 843
Network latency average = 290.2
	minimum = 22
	maximum = 772
Slowest packet = 290
Flit latency average = 267.919
	minimum = 5
	maximum = 769
Slowest flit = 27266
Fragmentation average = 24.14
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0383958
	minimum = 0.025 (at node 68)
	maximum = 0.053 (at node 177)
Accepted packet rate average = 0.0148385
	minimum = 0.004 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.68501
	minimum = 0.445 (at node 68)
	maximum = 0.945 (at node 177)
Accepted flit rate average= 0.2735
	minimum = 0.105 (at node 174)
	maximum = 0.435 (at node 44)
Injected packet length average = 17.8407
Accepted packet length average = 18.4317
Total in-flight flits = 80346 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 597.761
	minimum = 22
	maximum = 1536
Network latency average = 577.538
	minimum = 22
	maximum = 1469
Slowest packet = 2431
Flit latency average = 554.751
	minimum = 5
	maximum = 1547
Slowest flit = 58077
Fragmentation average = 25.6005
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0349062
	minimum = 0.021 (at node 128)
	maximum = 0.045 (at node 146)
Accepted packet rate average = 0.0150521
	minimum = 0.008 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.62624
	minimum = 0.378 (at node 128)
	maximum = 0.808 (at node 146)
Accepted flit rate average= 0.27426
	minimum = 0.152 (at node 116)
	maximum = 0.3915 (at node 63)
Injected packet length average = 17.9406
Accepted packet length average = 18.2208
Total in-flight flits = 137306 (0 measured)
latency change    = 0.48965
throughput change = 0.00277261
Class 0:
Packet latency average = 1512.57
	minimum = 22
	maximum = 2296
Network latency average = 1448.56
	minimum = 22
	maximum = 2235
Slowest packet = 3780
Flit latency average = 1430.73
	minimum = 5
	maximum = 2296
Slowest flit = 92420
Fragmentation average = 22.1608
	minimum = 0
	maximum = 203
Injected packet rate average = 0.0178438
	minimum = 0.002 (at node 19)
	maximum = 0.04 (at node 174)
Accepted packet rate average = 0.0151302
	minimum = 0.006 (at node 132)
	maximum = 0.024 (at node 24)
Injected flit rate average = 0.32276
	minimum = 0.036 (at node 19)
	maximum = 0.715 (at node 26)
Accepted flit rate average= 0.270698
	minimum = 0.108 (at node 132)
	maximum = 0.421 (at node 24)
Injected packet length average = 18.0881
Accepted packet length average = 17.8912
Total in-flight flits = 147864 (0 measured)
latency change    = 0.604804
throughput change = 0.0131604
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1331.82
	minimum = 147
	maximum = 2405
Network latency average = 46.6289
	minimum = 22
	maximum = 738
Slowest packet = 16971
Flit latency average = 1965.29
	minimum = 5
	maximum = 3052
Slowest flit = 124032
Fragmentation average = 4.19588
	minimum = 0
	maximum = 26
Injected packet rate average = 0.015776
	minimum = 0.003 (at node 68)
	maximum = 0.04 (at node 28)
Accepted packet rate average = 0.0148281
	minimum = 0.006 (at node 5)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.283432
	minimum = 0.054 (at node 68)
	maximum = 0.725 (at node 28)
Accepted flit rate average= 0.266427
	minimum = 0.121 (at node 35)
	maximum = 0.429 (at node 38)
Injected packet length average = 17.966
Accepted packet length average = 17.9677
Total in-flight flits = 151106 (52650 measured)
latency change    = 0.135711
throughput change = 0.01603
Class 0:
Packet latency average = 2205.6
	minimum = 147
	maximum = 3653
Network latency average = 840.732
	minimum = 22
	maximum = 1954
Slowest packet = 16971
Flit latency average = 2185.11
	minimum = 5
	maximum = 3896
Slowest flit = 140417
Fragmentation average = 11.6888
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0151302
	minimum = 0.0055 (at node 38)
	maximum = 0.0285 (at node 3)
Accepted packet rate average = 0.0148438
	minimum = 0.009 (at node 48)
	maximum = 0.0205 (at node 178)
Injected flit rate average = 0.271924
	minimum = 0.099 (at node 38)
	maximum = 0.515 (at node 100)
Accepted flit rate average= 0.267406
	minimum = 0.17 (at node 48)
	maximum = 0.3715 (at node 178)
Injected packet length average = 17.9723
Accepted packet length average = 18.0147
Total in-flight flits = 149616 (96388 measured)
latency change    = 0.396162
throughput change = 0.00366172
Class 0:
Packet latency average = 2943.78
	minimum = 147
	maximum = 4574
Network latency average = 1403.03
	minimum = 22
	maximum = 2975
Slowest packet = 16971
Flit latency average = 2346.4
	minimum = 5
	maximum = 4647
Slowest flit = 176143
Fragmentation average = 15.0424
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0152899
	minimum = 0.006 (at node 15)
	maximum = 0.0243333 (at node 100)
Accepted packet rate average = 0.0148837
	minimum = 0.01 (at node 135)
	maximum = 0.0206667 (at node 178)
Injected flit rate average = 0.274823
	minimum = 0.112667 (at node 15)
	maximum = 0.439333 (at node 100)
Accepted flit rate average= 0.268059
	minimum = 0.183333 (at node 135)
	maximum = 0.368 (at node 178)
Injected packet length average = 17.9741
Accepted packet length average = 18.0103
Total in-flight flits = 151808 (131963 measured)
latency change    = 0.250759
throughput change = 0.0024352
Class 0:
Packet latency average = 3492.43
	minimum = 147
	maximum = 5474
Network latency average = 1943.43
	minimum = 22
	maximum = 3934
Slowest packet = 16971
Flit latency average = 2469.29
	minimum = 5
	maximum = 5357
Slowest flit = 178235
Fragmentation average = 17.3258
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0153047
	minimum = 0.00625 (at node 104)
	maximum = 0.0255 (at node 107)
Accepted packet rate average = 0.0148581
	minimum = 0.01025 (at node 135)
	maximum = 0.01925 (at node 66)
Injected flit rate average = 0.275206
	minimum = 0.114 (at node 104)
	maximum = 0.456 (at node 107)
Accepted flit rate average= 0.267495
	minimum = 0.18625 (at node 135)
	maximum = 0.3495 (at node 90)
Injected packet length average = 17.9818
Accepted packet length average = 18.0033
Total in-flight flits = 153874 (150892 measured)
latency change    = 0.157098
throughput change = 0.00210933
Class 0:
Packet latency average = 4014.33
	minimum = 147
	maximum = 5969
Network latency average = 2344.87
	minimum = 22
	maximum = 4933
Slowest packet = 16971
Flit latency average = 2556.77
	minimum = 5
	maximum = 5888
Slowest flit = 197298
Fragmentation average = 18.7693
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0149979
	minimum = 0.0078 (at node 161)
	maximum = 0.0236 (at node 107)
Accepted packet rate average = 0.0148656
	minimum = 0.0114 (at node 80)
	maximum = 0.019 (at node 181)
Injected flit rate average = 0.269832
	minimum = 0.1404 (at node 161)
	maximum = 0.4248 (at node 107)
Accepted flit rate average= 0.267548
	minimum = 0.2078 (at node 80)
	maximum = 0.3408 (at node 181)
Injected packet length average = 17.9913
Accepted packet length average = 17.9978
Total in-flight flits = 150254 (150120 measured)
latency change    = 0.130008
throughput change = 0.000198563
Class 0:
Packet latency average = 4434.27
	minimum = 147
	maximum = 6733
Network latency average = 2507.45
	minimum = 22
	maximum = 5602
Slowest packet = 16971
Flit latency average = 2602.5
	minimum = 5
	maximum = 6228
Slowest flit = 282743
Fragmentation average = 19.0402
	minimum = 0
	maximum = 306
Injected packet rate average = 0.0150773
	minimum = 0.00783333 (at node 161)
	maximum = 0.0226667 (at node 107)
Accepted packet rate average = 0.0148698
	minimum = 0.0111667 (at node 80)
	maximum = 0.0191667 (at node 181)
Injected flit rate average = 0.271246
	minimum = 0.141 (at node 161)
	maximum = 0.408 (at node 107)
Accepted flit rate average= 0.26763
	minimum = 0.201 (at node 80)
	maximum = 0.344 (at node 181)
Injected packet length average = 17.9904
Accepted packet length average = 17.9982
Total in-flight flits = 152052 (152052 measured)
latency change    = 0.0947042
throughput change = 0.000307483
Class 0:
Packet latency average = 4794.85
	minimum = 147
	maximum = 7420
Network latency average = 2613.17
	minimum = 22
	maximum = 6253
Slowest packet = 16971
Flit latency average = 2646.59
	minimum = 5
	maximum = 6236
Slowest flit = 321065
Fragmentation average = 19.1827
	minimum = 0
	maximum = 306
Injected packet rate average = 0.0150126
	minimum = 0.00957143 (at node 132)
	maximum = 0.0204286 (at node 107)
Accepted packet rate average = 0.0148363
	minimum = 0.0115714 (at node 80)
	maximum = 0.0188571 (at node 103)
Injected flit rate average = 0.270187
	minimum = 0.172857 (at node 132)
	maximum = 0.367714 (at node 107)
Accepted flit rate average= 0.267025
	minimum = 0.208286 (at node 80)
	maximum = 0.338571 (at node 103)
Injected packet length average = 17.9973
Accepted packet length average = 17.9981
Total in-flight flits = 152204 (152204 measured)
latency change    = 0.0752009
throughput change = 0.00226537
Draining all recorded packets ...
Class 0:
Remaining flits: 415656 415657 415658 415659 415660 415661 415662 415663 415664 415665 [...] (152508 flits)
Measured flits: 415656 415657 415658 415659 415660 415661 415662 415663 415664 415665 [...] (152508 flits)
Class 0:
Remaining flits: 442368 442369 442370 442371 442372 442373 442374 442375 442376 442377 [...] (150819 flits)
Measured flits: 442368 442369 442370 442371 442372 442373 442374 442375 442376 442377 [...] (150819 flits)
Class 0:
Remaining flits: 489942 489943 489944 489945 489946 489947 489948 489949 489950 489951 [...] (150193 flits)
Measured flits: 489942 489943 489944 489945 489946 489947 489948 489949 489950 489951 [...] (150193 flits)
Class 0:
Remaining flits: 566262 566263 566264 566265 566266 566267 566268 566269 566270 566271 [...] (149021 flits)
Measured flits: 566262 566263 566264 566265 566266 566267 566268 566269 566270 566271 [...] (149021 flits)
Class 0:
Remaining flits: 586224 586225 586226 586227 586228 586229 586230 586231 586232 586233 [...] (148689 flits)
Measured flits: 586224 586225 586226 586227 586228 586229 586230 586231 586232 586233 [...] (148689 flits)
Class 0:
Remaining flits: 647118 647119 647120 647121 647122 647123 647124 647125 647126 647127 [...] (148910 flits)
Measured flits: 647118 647119 647120 647121 647122 647123 647124 647125 647126 647127 [...] (148910 flits)
Class 0:
Remaining flits: 691992 691993 691994 691995 691996 691997 691998 691999 692000 692001 [...] (147657 flits)
Measured flits: 691992 691993 691994 691995 691996 691997 691998 691999 692000 692001 [...] (147657 flits)
Class 0:
Remaining flits: 719694 719695 719696 719697 719698 719699 719700 719701 719702 719703 [...] (146628 flits)
Measured flits: 719694 719695 719696 719697 719698 719699 719700 719701 719702 719703 [...] (146556 flits)
Class 0:
Remaining flits: 774594 774595 774596 774597 774598 774599 774600 774601 774602 774603 [...] (147502 flits)
Measured flits: 774594 774595 774596 774597 774598 774599 774600 774601 774602 774603 [...] (147106 flits)
Class 0:
Remaining flits: 819270 819271 819272 819273 819274 819275 819276 819277 819278 819279 [...] (146853 flits)
Measured flits: 819270 819271 819272 819273 819274 819275 819276 819277 819278 819279 [...] (144549 flits)
Class 0:
Remaining flits: 893052 893053 893054 893055 893056 893057 893058 893059 893060 893061 [...] (146029 flits)
Measured flits: 893052 893053 893054 893055 893056 893057 893058 893059 893060 893061 [...] (137497 flits)
Class 0:
Remaining flits: 936432 936433 936434 936435 936436 936437 936438 936439 936440 936441 [...] (145136 flits)
Measured flits: 936432 936433 936434 936435 936436 936437 936438 936439 936440 936441 [...] (128809 flits)
Class 0:
Remaining flits: 944928 944929 944930 944931 944932 944933 944934 944935 944936 944937 [...] (143270 flits)
Measured flits: 944928 944929 944930 944931 944932 944933 944934 944935 944936 944937 [...] (115210 flits)
Class 0:
Remaining flits: 1019970 1019971 1019972 1019973 1019974 1019975 1019976 1019977 1019978 1019979 [...] (144997 flits)
Measured flits: 1019970 1019971 1019972 1019973 1019974 1019975 1019976 1019977 1019978 1019979 [...] (102367 flits)
Class 0:
Remaining flits: 1043298 1043299 1043300 1043301 1043302 1043303 1043304 1043305 1043306 1043307 [...] (142971 flits)
Measured flits: 1043298 1043299 1043300 1043301 1043302 1043303 1043304 1043305 1043306 1043307 [...] (82068 flits)
Class 0:
Remaining flits: 1125288 1125289 1125290 1125291 1125292 1125293 1125294 1125295 1125296 1125297 [...] (143717 flits)
Measured flits: 1125288 1125289 1125290 1125291 1125292 1125293 1125294 1125295 1125296 1125297 [...] (58969 flits)
Class 0:
Remaining flits: 1202778 1202779 1202780 1202781 1202782 1202783 1202784 1202785 1202786 1202787 [...] (141803 flits)
Measured flits: 1202778 1202779 1202780 1202781 1202782 1202783 1202784 1202785 1202786 1202787 [...] (39708 flits)
Class 0:
Remaining flits: 1222758 1222759 1222760 1222761 1222762 1222763 1222764 1222765 1222766 1222767 [...] (141331 flits)
Measured flits: 1222758 1222759 1222760 1222761 1222762 1222763 1222764 1222765 1222766 1222767 [...] (27373 flits)
Class 0:
Remaining flits: 1263960 1263961 1263962 1263963 1263964 1263965 1263966 1263967 1263968 1263969 [...] (142175 flits)
Measured flits: 1311030 1311031 1311032 1311033 1311034 1311035 1311036 1311037 1311038 1311039 [...] (16574 flits)
Class 0:
Remaining flits: 1316466 1316467 1316468 1316469 1316470 1316471 1316472 1316473 1316474 1316475 [...] (138685 flits)
Measured flits: 1337556 1337557 1337558 1337559 1337560 1337561 1365102 1365103 1365104 1365105 [...] (8271 flits)
Class 0:
Remaining flits: 1393182 1393183 1393184 1393185 1393186 1393187 1393188 1393189 1393190 1393191 [...] (139972 flits)
Measured flits: 1393182 1393183 1393184 1393185 1393186 1393187 1393188 1393189 1393190 1393191 [...] (3668 flits)
Class 0:
Remaining flits: 1439442 1439443 1439444 1439445 1439446 1439447 1439448 1439449 1439450 1439451 [...] (138556 flits)
Measured flits: 1490688 1490689 1490690 1490691 1490692 1490693 1490694 1490695 1490696 1490697 [...] (2006 flits)
Class 0:
Remaining flits: 1464210 1464211 1464212 1464213 1464214 1464215 1464216 1464217 1464218 1464219 [...] (138570 flits)
Measured flits: 1490696 1490697 1490698 1490699 1490700 1490701 1490702 1490703 1490704 1490705 [...] (1271 flits)
Class 0:
Remaining flits: 1508650 1508651 1508706 1508707 1508708 1508709 1508710 1508711 1508712 1508713 [...] (139733 flits)
Measured flits: 1508706 1508707 1508708 1508709 1508710 1508711 1508712 1508713 1508714 1508715 [...] (594 flits)
Class 0:
Remaining flits: 1563444 1563445 1563446 1563447 1563448 1563449 1563450 1563451 1563452 1563453 [...] (139505 flits)
Measured flits: 1573146 1573147 1573148 1573149 1573150 1573151 1573152 1573153 1573154 1573155 [...] (307 flits)
Class 0:
Remaining flits: 1575054 1575055 1575056 1575057 1575058 1575059 1575060 1575061 1575062 1575063 [...] (141670 flits)
Measured flits: 1575054 1575055 1575056 1575057 1575058 1575059 1575060 1575061 1575062 1575063 [...] (90 flits)
Class 0:
Remaining flits: 1666584 1666585 1666586 1666587 1666588 1666589 1666590 1666591 1666592 1666593 [...] (141898 flits)
Measured flits: 1831924 1831925 1831926 1831927 1831928 1831929 1831930 1831931 (8 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1731312 1731313 1731314 1731315 1731316 1731317 1731318 1731319 1731320 1731321 [...] (92385 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1731312 1731313 1731314 1731315 1731316 1731317 1731318 1731319 1731320 1731321 [...] (43939 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1822050 1822051 1822052 1822053 1822054 1822055 1822056 1822057 1822058 1822059 [...] (7127 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1986714 1986715 1986716 1986717 1986718 1986719 1986720 1986721 1986722 1986723 [...] (52 flits)
Measured flits: (0 flits)
Time taken is 41088 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10466.3 (1 samples)
	minimum = 147 (1 samples)
	maximum = 27082 (1 samples)
Network latency average = 2851.51 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8811 (1 samples)
Flit latency average = 2781.93 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8868 (1 samples)
Fragmentation average = 20.3678 (1 samples)
	minimum = 0 (1 samples)
	maximum = 306 (1 samples)
Injected packet rate average = 0.0150126 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0204286 (1 samples)
Accepted packet rate average = 0.0148363 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.270187 (1 samples)
	minimum = 0.172857 (1 samples)
	maximum = 0.367714 (1 samples)
Accepted flit rate average = 0.267025 (1 samples)
	minimum = 0.208286 (1 samples)
	maximum = 0.338571 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 17.9981 (1 samples)
Hops average = 5.05079 (1 samples)
Total run time 40.2847
