BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.272
	minimum = 22
	maximum = 841
Network latency average = 313.776
	minimum = 22
	maximum = 762
Slowest packet = 389
Flit latency average = 292.047
	minimum = 5
	maximum = 795
Slowest flit = 29927
Fragmentation average = 25.2797
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0470521
	minimum = 0.029 (at node 100)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.0153229
	minimum = 0.007 (at node 120)
	maximum = 0.025 (at node 48)
Injected flit rate average = 0.837995
	minimum = 0.522 (at node 100)
	maximum = 0.999 (at node 23)
Accepted flit rate average= 0.28288
	minimum = 0.141 (at node 120)
	maximum = 0.46 (at node 166)
Injected packet length average = 17.8099
Accepted packet length average = 18.4613
Total in-flight flits = 108479 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 691.76
	minimum = 22
	maximum = 1676
Network latency average = 644.082
	minimum = 22
	maximum = 1569
Slowest packet = 389
Flit latency average = 619.835
	minimum = 5
	maximum = 1569
Slowest flit = 67855
Fragmentation average = 26.7796
	minimum = 0
	maximum = 164
Injected packet rate average = 0.0366016
	minimum = 0.0185 (at node 100)
	maximum = 0.051 (at node 119)
Accepted packet rate average = 0.0151745
	minimum = 0.008 (at node 153)
	maximum = 0.022 (at node 51)
Injected flit rate average = 0.656596
	minimum = 0.333 (at node 100)
	maximum = 0.91 (at node 119)
Accepted flit rate average= 0.275977
	minimum = 0.1505 (at node 153)
	maximum = 0.404 (at node 154)
Injected packet length average = 17.939
Accepted packet length average = 18.1869
Total in-flight flits = 148905 (0 measured)
latency change    = 0.50377
throughput change = 0.0250153
Class 0:
Packet latency average = 1777.51
	minimum = 25
	maximum = 2576
Network latency average = 1626.32
	minimum = 22
	maximum = 2411
Slowest packet = 4568
Flit latency average = 1608.09
	minimum = 5
	maximum = 2426
Slowest flit = 92995
Fragmentation average = 21.5586
	minimum = 0
	maximum = 204
Injected packet rate average = 0.0147813
	minimum = 0.002 (at node 164)
	maximum = 0.037 (at node 54)
Accepted packet rate average = 0.0146302
	minimum = 0.007 (at node 40)
	maximum = 0.023 (at node 99)
Injected flit rate average = 0.267208
	minimum = 0.023 (at node 164)
	maximum = 0.676 (at node 54)
Accepted flit rate average= 0.262854
	minimum = 0.13 (at node 39)
	maximum = 0.405 (at node 3)
Injected packet length average = 18.0775
Accepted packet length average = 17.9665
Total in-flight flits = 149773 (0 measured)
latency change    = 0.610827
throughput change = 0.0499227
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1939.85
	minimum = 854
	maximum = 2874
Network latency average = 50.043
	minimum = 22
	maximum = 975
Slowest packet = 17086
Flit latency average = 2176.71
	minimum = 5
	maximum = 3274
Slowest flit = 118104
Fragmentation average = 4.78495
	minimum = 0
	maximum = 17
Injected packet rate average = 0.0149635
	minimum = 0.003 (at node 26)
	maximum = 0.037 (at node 86)
Accepted packet rate average = 0.0148021
	minimum = 0.005 (at node 48)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.26951
	minimum = 0.043 (at node 47)
	maximum = 0.65 (at node 86)
Accepted flit rate average= 0.265615
	minimum = 0.097 (at node 48)
	maximum = 0.462 (at node 129)
Injected packet length average = 18.0111
Accepted packet length average = 17.9444
Total in-flight flits = 150543 (50075 measured)
latency change    = 0.0836847
throughput change = 0.0103926
Class 0:
Packet latency average = 2774.62
	minimum = 854
	maximum = 4001
Network latency average = 759.649
	minimum = 22
	maximum = 1974
Slowest packet = 17086
Flit latency average = 2388.22
	minimum = 5
	maximum = 4011
Slowest flit = 158878
Fragmentation average = 9.78378
	minimum = 0
	maximum = 95
Injected packet rate average = 0.0147865
	minimum = 0.005 (at node 152)
	maximum = 0.027 (at node 32)
Accepted packet rate average = 0.0147318
	minimum = 0.006 (at node 36)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.266294
	minimum = 0.098 (at node 152)
	maximum = 0.486 (at node 32)
Accepted flit rate average= 0.264951
	minimum = 0.108 (at node 36)
	maximum = 0.402 (at node 129)
Injected packet length average = 18.0093
Accepted packet length average = 17.985
Total in-flight flits = 150344 (94845 measured)
latency change    = 0.30086
throughput change = 0.00250636
Class 0:
Packet latency average = 3574.63
	minimum = 854
	maximum = 4852
Network latency average = 1369.36
	minimum = 22
	maximum = 2956
Slowest packet = 17086
Flit latency average = 2519.6
	minimum = 5
	maximum = 4752
Slowest flit = 192450
Fragmentation average = 14.2161
	minimum = 0
	maximum = 108
Injected packet rate average = 0.014908
	minimum = 0.006 (at node 99)
	maximum = 0.0233333 (at node 36)
Accepted packet rate average = 0.0147361
	minimum = 0.00866667 (at node 36)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.268286
	minimum = 0.11 (at node 99)
	maximum = 0.42 (at node 183)
Accepted flit rate average= 0.265424
	minimum = 0.156 (at node 36)
	maximum = 0.363333 (at node 129)
Injected packet length average = 17.9962
Accepted packet length average = 18.0118
Total in-flight flits = 151491 (128950 measured)
latency change    = 0.223801
throughput change = 0.0017824
Draining remaining packets ...
Class 0:
Remaining flits: 215460 215461 215462 215463 215464 215465 215466 215467 215468 215469 [...] (101969 flits)
Measured flits: 306252 306253 306254 306255 306256 306257 306258 306259 306260 306261 [...] (97331 flits)
Class 0:
Remaining flits: 281556 281557 281558 281559 281560 281561 281562 281563 281564 281565 [...] (53899 flits)
Measured flits: 306360 306361 306362 306363 306364 306365 306366 306367 306368 306369 [...] (53544 flits)
Class 0:
Remaining flits: 311292 311293 311294 311295 311296 311297 311298 311299 311300 311301 [...] (8538 flits)
Measured flits: 311292 311293 311294 311295 311296 311297 311298 311299 311300 311301 [...] (8538 flits)
Time taken is 9945 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5300.87 (1 samples)
	minimum = 854 (1 samples)
	maximum = 8091 (1 samples)
Network latency average = 2792.53 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6449 (1 samples)
Flit latency average = 2859 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6432 (1 samples)
Fragmentation average = 18.3255 (1 samples)
	minimum = 0 (1 samples)
	maximum = 194 (1 samples)
Injected packet rate average = 0.014908 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0233333 (1 samples)
Accepted packet rate average = 0.0147361 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.268286 (1 samples)
	minimum = 0.11 (1 samples)
	maximum = 0.42 (1 samples)
Accepted flit rate average = 0.265424 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 0.363333 (1 samples)
Injected packet size average = 17.9962 (1 samples)
Accepted packet size average = 18.0118 (1 samples)
Hops average = 5.05169 (1 samples)
Total run time 8.63322
