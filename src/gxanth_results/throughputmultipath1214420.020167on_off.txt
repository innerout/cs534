BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 265.623
	minimum = 22
	maximum = 957
Network latency average = 188.422
	minimum = 22
	maximum = 682
Slowest packet = 3
Flit latency average = 164.898
	minimum = 5
	maximum = 665
Slowest flit = 21307
Fragmentation average = 17.2032
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0195052
	minimum = 0 (at node 92)
	maximum = 0.052 (at node 187)
Accepted packet rate average = 0.0125625
	minimum = 0.003 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.34837
	minimum = 0 (at node 92)
	maximum = 0.935 (at node 187)
Accepted flit rate average= 0.230927
	minimum = 0.067 (at node 174)
	maximum = 0.414 (at node 44)
Injected packet length average = 17.8603
Accepted packet length average = 18.3823
Total in-flight flits = 23072 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 405.779
	minimum = 22
	maximum = 1666
Network latency average = 312.148
	minimum = 22
	maximum = 1253
Slowest packet = 3
Flit latency average = 288.24
	minimum = 5
	maximum = 1267
Slowest flit = 48021
Fragmentation average = 18.0042
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0194219
	minimum = 0.0015 (at node 179)
	maximum = 0.0465 (at node 124)
Accepted packet rate average = 0.0135729
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 44)
Injected flit rate average = 0.348234
	minimum = 0.027 (at node 179)
	maximum = 0.831 (at node 124)
Accepted flit rate average= 0.246852
	minimum = 0.126 (at node 116)
	maximum = 0.351 (at node 44)
Injected packet length average = 17.93
Accepted packet length average = 18.1871
Total in-flight flits = 39471 (0 measured)
latency change    = 0.345399
throughput change = 0.0645103
Class 0:
Packet latency average = 799.401
	minimum = 22
	maximum = 2645
Network latency average = 677.756
	minimum = 22
	maximum = 1964
Slowest packet = 3156
Flit latency average = 656.26
	minimum = 5
	maximum = 1944
Slowest flit = 68615
Fragmentation average = 18.0785
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0201563
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 159)
Accepted packet rate average = 0.0149219
	minimum = 0.005 (at node 117)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.362531
	minimum = 0 (at node 17)
	maximum = 1 (at node 14)
Accepted flit rate average= 0.268484
	minimum = 0.102 (at node 117)
	maximum = 0.535 (at node 16)
Injected packet length average = 17.986
Accepted packet length average = 17.9927
Total in-flight flits = 57690 (0 measured)
latency change    = 0.492396
throughput change = 0.0805738
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 446.124
	minimum = 22
	maximum = 1962
Network latency average = 329.599
	minimum = 22
	maximum = 992
Slowest packet = 11339
Flit latency average = 871.162
	minimum = 5
	maximum = 2491
Slowest flit = 79987
Fragmentation average = 13.0815
	minimum = 0
	maximum = 73
Injected packet rate average = 0.0195417
	minimum = 0 (at node 45)
	maximum = 0.053 (at node 181)
Accepted packet rate average = 0.0149167
	minimum = 0.007 (at node 79)
	maximum = 0.025 (at node 0)
Injected flit rate average = 0.351891
	minimum = 0 (at node 45)
	maximum = 0.937 (at node 181)
Accepted flit rate average= 0.268818
	minimum = 0.129 (at node 174)
	maximum = 0.457 (at node 129)
Injected packet length average = 18.0072
Accepted packet length average = 18.0213
Total in-flight flits = 73775 (55026 measured)
latency change    = 0.79188
throughput change = 0.00124
Class 0:
Packet latency average = 878.974
	minimum = 22
	maximum = 2604
Network latency average = 754.647
	minimum = 22
	maximum = 1947
Slowest packet = 11339
Flit latency average = 999.697
	minimum = 5
	maximum = 2781
Slowest flit = 148115
Fragmentation average = 17.9294
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0189323
	minimum = 0.002 (at node 71)
	maximum = 0.0425 (at node 67)
Accepted packet rate average = 0.0148906
	minimum = 0.0085 (at node 79)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.341154
	minimum = 0.036 (at node 71)
	maximum = 0.765 (at node 67)
Accepted flit rate average= 0.268003
	minimum = 0.16 (at node 79)
	maximum = 0.414 (at node 129)
Injected packet length average = 18.0197
Accepted packet length average = 17.9981
Total in-flight flits = 86015 (81758 measured)
latency change    = 0.492449
throughput change = 0.0030414
Class 0:
Packet latency average = 1150.5
	minimum = 22
	maximum = 4490
Network latency average = 1011.52
	minimum = 22
	maximum = 2932
Slowest packet = 11339
Flit latency average = 1117.16
	minimum = 5
	maximum = 3272
Slowest flit = 183972
Fragmentation average = 18.6361
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0186962
	minimum = 0.00533333 (at node 62)
	maximum = 0.0353333 (at node 111)
Accepted packet rate average = 0.0148837
	minimum = 0.00966667 (at node 79)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.336536
	minimum = 0.096 (at node 62)
	maximum = 0.636 (at node 111)
Accepted flit rate average= 0.267891
	minimum = 0.179 (at node 36)
	maximum = 0.384 (at node 129)
Injected packet length average = 18.0003
Accepted packet length average = 17.999
Total in-flight flits = 97587 (97168 measured)
latency change    = 0.236006
throughput change = 0.000418003
Draining remaining packets ...
Class 0:
Remaining flits: 215670 215671 215672 215673 215674 215675 216679 216680 216681 216682 [...] (49535 flits)
Measured flits: 215670 215671 215672 215673 215674 215675 216679 216680 216681 216682 [...] (49535 flits)
Class 0:
Remaining flits: 244458 244459 244460 244461 244462 244463 244464 244465 244466 244467 [...] (10488 flits)
Measured flits: 244458 244459 244460 244461 244462 244463 244464 244465 244466 244467 [...] (10488 flits)
Time taken is 8935 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1736.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5465 (1 samples)
Network latency average = 1528.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4650 (1 samples)
Flit latency average = 1466.71 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4633 (1 samples)
Fragmentation average = 17.512 (1 samples)
	minimum = 0 (1 samples)
	maximum = 183 (1 samples)
Injected packet rate average = 0.0186962 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0353333 (1 samples)
Accepted packet rate average = 0.0148837 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.336536 (1 samples)
	minimum = 0.096 (1 samples)
	maximum = 0.636 (1 samples)
Accepted flit rate average = 0.267891 (1 samples)
	minimum = 0.179 (1 samples)
	maximum = 0.384 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.999 (1 samples)
Hops average = 5.09918 (1 samples)
Total run time 5.94384
