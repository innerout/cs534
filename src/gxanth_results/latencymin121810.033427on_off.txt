BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 355.723
	minimum = 23
	maximum = 983
Network latency average = 244.992
	minimum = 23
	maximum = 861
Slowest packet = 88
Flit latency average = 201.071
	minimum = 6
	maximum = 844
Slowest flit = 7541
Fragmentation average = 51.6177
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0134896
	minimum = 0 (at node 2)
	maximum = 0.026 (at node 89)
Accepted packet rate average = 0.00904688
	minimum = 0.003 (at node 64)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.23825
	minimum = 0 (at node 2)
	maximum = 0.468 (at node 126)
Accepted flit rate average= 0.168958
	minimum = 0.054 (at node 112)
	maximum = 0.323 (at node 91)
Injected packet length average = 17.6618
Accepted packet length average = 18.6759
Total in-flight flits = 15566 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 648.703
	minimum = 23
	maximum = 1966
Network latency average = 350.149
	minimum = 23
	maximum = 1645
Slowest packet = 88
Flit latency average = 299.729
	minimum = 6
	maximum = 1628
Slowest flit = 6857
Fragmentation average = 54.8271
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0115599
	minimum = 0.0025 (at node 67)
	maximum = 0.0205 (at node 175)
Accepted packet rate average = 0.00904948
	minimum = 0.0045 (at node 174)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.205917
	minimum = 0.044 (at node 67)
	maximum = 0.3615 (at node 175)
Accepted flit rate average= 0.16607
	minimum = 0.081 (at node 174)
	maximum = 0.269 (at node 22)
Injected packet length average = 17.813
Accepted packet length average = 18.3514
Total in-flight flits = 17679 (0 measured)
latency change    = 0.45164
throughput change = 0.0173903
Class 0:
Packet latency average = 1649.52
	minimum = 46
	maximum = 2961
Network latency average = 539.946
	minimum = 23
	maximum = 2052
Slowest packet = 2494
Flit latency average = 480.376
	minimum = 6
	maximum = 2025
Slowest flit = 40446
Fragmentation average = 58.1644
	minimum = 0
	maximum = 123
Injected packet rate average = 0.00895833
	minimum = 0 (at node 124)
	maximum = 0.019 (at node 51)
Accepted packet rate average = 0.00903125
	minimum = 0.003 (at node 10)
	maximum = 0.017 (at node 68)
Injected flit rate average = 0.161937
	minimum = 0 (at node 124)
	maximum = 0.343 (at node 51)
Accepted flit rate average= 0.162333
	minimum = 0.048 (at node 153)
	maximum = 0.315 (at node 68)
Injected packet length average = 18.0767
Accepted packet length average = 17.9746
Total in-flight flits = 17831 (0 measured)
latency change    = 0.606732
throughput change = 0.0230204
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2458.75
	minimum = 46
	maximum = 3942
Network latency average = 320.702
	minimum = 27
	maximum = 903
Slowest packet = 6286
Flit latency average = 473.35
	minimum = 6
	maximum = 2174
Slowest flit = 73960
Fragmentation average = 52.228
	minimum = 0
	maximum = 132
Injected packet rate average = 0.00926042
	minimum = 0.001 (at node 41)
	maximum = 0.024 (at node 167)
Accepted packet rate average = 0.00920833
	minimum = 0.003 (at node 31)
	maximum = 0.017 (at node 34)
Injected flit rate average = 0.167276
	minimum = 0.018 (at node 41)
	maximum = 0.421 (at node 167)
Accepted flit rate average= 0.166792
	minimum = 0.064 (at node 31)
	maximum = 0.323 (at node 34)
Injected packet length average = 18.0636
Accepted packet length average = 18.1131
Total in-flight flits = 17973 (16704 measured)
latency change    = 0.329123
throughput change = 0.02673
Class 0:
Packet latency average = 2877.17
	minimum = 46
	maximum = 4940
Network latency average = 449.144
	minimum = 26
	maximum = 1821
Slowest packet = 6286
Flit latency average = 479.686
	minimum = 6
	maximum = 2178
Slowest flit = 73961
Fragmentation average = 55.7581
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00908333
	minimum = 0.0015 (at node 164)
	maximum = 0.017 (at node 159)
Accepted packet rate average = 0.00914323
	minimum = 0.0045 (at node 190)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.163599
	minimum = 0.027 (at node 164)
	maximum = 0.3085 (at node 167)
Accepted flit rate average= 0.164339
	minimum = 0.081 (at node 190)
	maximum = 0.2945 (at node 16)
Injected packet length average = 18.0109
Accepted packet length average = 17.9738
Total in-flight flits = 17689 (17671 measured)
latency change    = 0.145425
throughput change = 0.0149273
Class 0:
Packet latency average = 3277.8
	minimum = 46
	maximum = 5886
Network latency average = 489.117
	minimum = 25
	maximum = 2745
Slowest packet = 6286
Flit latency average = 480.763
	minimum = 6
	maximum = 2728
Slowest flit = 116981
Fragmentation average = 57.2612
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00907292
	minimum = 0.00333333 (at node 54)
	maximum = 0.0146667 (at node 125)
Accepted packet rate average = 0.00907118
	minimum = 0.00566667 (at node 4)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.163038
	minimum = 0.0633333 (at node 54)
	maximum = 0.264 (at node 125)
Accepted flit rate average= 0.163033
	minimum = 0.101333 (at node 4)
	maximum = 0.276667 (at node 16)
Injected packet length average = 17.9698
Accepted packet length average = 17.9726
Total in-flight flits = 17920 (17920 measured)
latency change    = 0.122226
throughput change = 0.00800792
Class 0:
Packet latency average = 3655.24
	minimum = 46
	maximum = 6714
Network latency average = 506.595
	minimum = 23
	maximum = 2745
Slowest packet = 6286
Flit latency average = 482.081
	minimum = 6
	maximum = 2728
Slowest flit = 116981
Fragmentation average = 57.8179
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00902734
	minimum = 0.00325 (at node 48)
	maximum = 0.0145 (at node 35)
Accepted packet rate average = 0.00903125
	minimum = 0.00525 (at node 4)
	maximum = 0.01325 (at node 182)
Injected flit rate average = 0.162488
	minimum = 0.05925 (at node 48)
	maximum = 0.26375 (at node 35)
Accepted flit rate average= 0.162564
	minimum = 0.094 (at node 4)
	maximum = 0.2385 (at node 182)
Injected packet length average = 17.9996
Accepted packet length average = 18.0001
Total in-flight flits = 17794 (17794 measured)
latency change    = 0.103262
throughput change = 0.00288615
Class 0:
Packet latency average = 4017.4
	minimum = 46
	maximum = 7672
Network latency average = 518.527
	minimum = 23
	maximum = 2917
Slowest packet = 6286
Flit latency average = 485.194
	minimum = 6
	maximum = 2894
Slowest flit = 169901
Fragmentation average = 58.5423
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00895625
	minimum = 0.004 (at node 157)
	maximum = 0.0142 (at node 191)
Accepted packet rate average = 0.00895938
	minimum = 0.0058 (at node 4)
	maximum = 0.0126 (at node 129)
Injected flit rate average = 0.161117
	minimum = 0.072 (at node 157)
	maximum = 0.2526 (at node 191)
Accepted flit rate average= 0.161256
	minimum = 0.1058 (at node 4)
	maximum = 0.2268 (at node 129)
Injected packet length average = 17.9893
Accepted packet length average = 17.9986
Total in-flight flits = 17663 (17663 measured)
latency change    = 0.0901463
throughput change = 0.00810854
Class 0:
Packet latency average = 4385.49
	minimum = 46
	maximum = 8751
Network latency average = 525.312
	minimum = 23
	maximum = 3192
Slowest packet = 6286
Flit latency average = 486.269
	minimum = 6
	maximum = 3095
Slowest flit = 190239
Fragmentation average = 58.5924
	minimum = 0
	maximum = 158
Injected packet rate average = 0.00896094
	minimum = 0.00466667 (at node 157)
	maximum = 0.015 (at node 191)
Accepted packet rate average = 0.00896701
	minimum = 0.006 (at node 36)
	maximum = 0.0121667 (at node 128)
Injected flit rate average = 0.161242
	minimum = 0.082 (at node 157)
	maximum = 0.27 (at node 191)
Accepted flit rate average= 0.1613
	minimum = 0.1055 (at node 36)
	maximum = 0.219 (at node 128)
Injected packet length average = 17.9939
Accepted packet length average = 17.9882
Total in-flight flits = 17899 (17899 measured)
latency change    = 0.0839351
throughput change = 0.000273386
Class 0:
Packet latency average = 4762.33
	minimum = 46
	maximum = 9452
Network latency average = 527.527
	minimum = 23
	maximum = 3192
Slowest packet = 6286
Flit latency average = 485.177
	minimum = 6
	maximum = 3095
Slowest flit = 190239
Fragmentation average = 57.755
	minimum = 0
	maximum = 158
Injected packet rate average = 0.00895313
	minimum = 0.005 (at node 50)
	maximum = 0.0145714 (at node 5)
Accepted packet rate average = 0.00897024
	minimum = 0.00614286 (at node 36)
	maximum = 0.0118571 (at node 128)
Injected flit rate average = 0.161103
	minimum = 0.0884286 (at node 50)
	maximum = 0.262286 (at node 5)
Accepted flit rate average= 0.161347
	minimum = 0.108429 (at node 36)
	maximum = 0.215429 (at node 128)
Injected packet length average = 17.9941
Accepted packet length average = 17.987
Total in-flight flits = 17646 (17646 measured)
latency change    = 0.0791278
throughput change = 0.000292059
Draining all recorded packets ...
Class 0:
Remaining flits: 261360 261361 261362 261363 261364 261365 261366 261367 261368 261369 [...] (18137 flits)
Measured flits: 261360 261361 261362 261363 261364 261365 261366 261367 261368 261369 [...] (18137 flits)
Class 0:
Remaining flits: 326808 326809 326810 326811 326812 326813 326814 326815 326816 326817 [...] (17771 flits)
Measured flits: 326808 326809 326810 326811 326812 326813 326814 326815 326816 326817 [...] (17771 flits)
Class 0:
Remaining flits: 351190 351191 351192 351193 351194 351195 351196 351197 359244 359245 [...] (17904 flits)
Measured flits: 351190 351191 351192 351193 351194 351195 351196 351197 359244 359245 [...] (17904 flits)
Class 0:
Remaining flits: 378846 378847 378848 378849 378850 378851 378852 378853 378854 378855 [...] (17305 flits)
Measured flits: 378846 378847 378848 378849 378850 378851 378852 378853 378854 378855 [...] (17305 flits)
Class 0:
Remaining flits: 382554 382555 382556 382557 382558 382559 382560 382561 382562 382563 [...] (17742 flits)
Measured flits: 382554 382555 382556 382557 382558 382559 382560 382561 382562 382563 [...] (17742 flits)
Class 0:
Remaining flits: 428705 435330 435331 435332 435333 435334 435335 435336 435337 435338 [...] (17513 flits)
Measured flits: 428705 435330 435331 435332 435333 435334 435335 435336 435337 435338 [...] (17513 flits)
Class 0:
Remaining flits: 470412 470413 470414 470415 470416 470417 470418 470419 470420 470421 [...] (17519 flits)
Measured flits: 470412 470413 470414 470415 470416 470417 470418 470419 470420 470421 [...] (17519 flits)
Class 0:
Remaining flits: 506151 506152 506153 506154 506155 506156 506157 506158 506159 509256 [...] (17753 flits)
Measured flits: 506151 506152 506153 506154 506155 506156 506157 506158 506159 509256 [...] (17753 flits)
Class 0:
Remaining flits: 533718 533719 533720 533721 533722 533723 533724 533725 533726 533727 [...] (17662 flits)
Measured flits: 533718 533719 533720 533721 533722 533723 533724 533725 533726 533727 [...] (17572 flits)
Class 0:
Remaining flits: 578034 578035 578036 578037 578038 578039 578040 578041 578042 578043 [...] (17929 flits)
Measured flits: 578034 578035 578036 578037 578038 578039 578040 578041 578042 578043 [...] (17711 flits)
Class 0:
Remaining flits: 611419 611420 611421 611422 611423 612684 612685 612686 612687 612688 [...] (17889 flits)
Measured flits: 611419 611420 611421 611422 611423 612684 612685 612686 612687 612688 [...] (17619 flits)
Class 0:
Remaining flits: 630054 630055 630056 630057 630058 630059 630060 630061 630062 630063 [...] (17623 flits)
Measured flits: 630054 630055 630056 630057 630058 630059 630060 630061 630062 630063 [...] (17065 flits)
Class 0:
Remaining flits: 648627 648628 648629 663066 663067 663068 663069 663070 663071 663072 [...] (17606 flits)
Measured flits: 648627 648628 648629 663066 663067 663068 663069 663070 663071 663072 [...] (17031 flits)
Class 0:
Remaining flits: 675655 675656 675657 675658 675659 675660 675661 675662 675663 675664 [...] (17738 flits)
Measured flits: 675655 675656 675657 675658 675659 675660 675661 675662 675663 675664 [...] (16917 flits)
Class 0:
Remaining flits: 727722 727723 727724 727725 727726 727727 727728 727729 727730 727731 [...] (17779 flits)
Measured flits: 727722 727723 727724 727725 727726 727727 727728 727729 727730 727731 [...] (16766 flits)
Class 0:
Remaining flits: 752994 752995 752996 752997 752998 752999 753000 753001 753002 753003 [...] (18023 flits)
Measured flits: 752994 752995 752996 752997 752998 752999 753000 753001 753002 753003 [...] (16588 flits)
Class 0:
Remaining flits: 788850 788851 788852 788853 788854 788855 788856 788857 788858 788859 [...] (17466 flits)
Measured flits: 788850 788851 788852 788853 788854 788855 788856 788857 788858 788859 [...] (15800 flits)
Class 0:
Remaining flits: 793026 793027 793028 793029 793030 793031 793032 793033 793034 793035 [...] (17728 flits)
Measured flits: 793026 793027 793028 793029 793030 793031 793032 793033 793034 793035 [...] (15402 flits)
Class 0:
Remaining flits: 805428 805429 805430 805431 805432 805433 805434 805435 805436 805437 [...] (17480 flits)
Measured flits: 805428 805429 805430 805431 805432 805433 805434 805435 805436 805437 [...] (14081 flits)
Class 0:
Remaining flits: 877170 877171 877172 877173 877174 877175 878862 878863 878864 878865 [...] (17638 flits)
Measured flits: 877170 877171 877172 877173 877174 877175 878862 878863 878864 878865 [...] (13154 flits)
Class 0:
Remaining flits: 881169 881170 881171 908946 908947 908948 908949 908950 908951 908952 [...] (17715 flits)
Measured flits: 881169 881170 881171 915012 915013 915014 915015 915016 915017 915018 [...] (12409 flits)
Class 0:
Remaining flits: 908959 908960 908961 908962 908963 939510 939511 939512 939513 939514 [...] (17840 flits)
Measured flits: 939510 939511 939512 939513 939514 939515 939516 939517 939518 939519 [...] (12373 flits)
Class 0:
Remaining flits: 968925 968926 968927 968928 968929 968930 968931 968932 968933 968934 [...] (17628 flits)
Measured flits: 968925 968926 968927 968928 968929 968930 968931 968932 968933 968934 [...] (11336 flits)
Class 0:
Remaining flits: 1007442 1007443 1007444 1007445 1007446 1007447 1007448 1007449 1007450 1007451 [...] (17277 flits)
Measured flits: 1007442 1007443 1007444 1007445 1007446 1007447 1007448 1007449 1007450 1007451 [...] (10489 flits)
Class 0:
Remaining flits: 1039122 1039123 1039124 1039125 1039126 1039127 1039128 1039129 1039130 1039131 [...] (17341 flits)
Measured flits: 1039122 1039123 1039124 1039125 1039126 1039127 1039128 1039129 1039130 1039131 [...] (9379 flits)
Class 0:
Remaining flits: 1048770 1048771 1048772 1048773 1048774 1048775 1048776 1048777 1048778 1048779 [...] (17390 flits)
Measured flits: 1059479 1060665 1060666 1060667 1061244 1061245 1061246 1061247 1061248 1061249 [...] (8868 flits)
Class 0:
Remaining flits: 1090224 1090225 1090226 1090227 1090228 1090229 1090230 1090231 1090232 1090233 [...] (17856 flits)
Measured flits: 1090224 1090225 1090226 1090227 1090228 1090229 1090230 1090231 1090232 1090233 [...] (8144 flits)
Class 0:
Remaining flits: 1115550 1115551 1115552 1115553 1115554 1115555 1115556 1115557 1115558 1115559 [...] (17641 flits)
Measured flits: 1115550 1115551 1115552 1115553 1115554 1115555 1115556 1115557 1115558 1115559 [...] (7682 flits)
Class 0:
Remaining flits: 1117391 1117392 1117393 1117394 1117395 1117396 1117397 1117398 1117399 1117400 [...] (17828 flits)
Measured flits: 1117391 1117392 1117393 1117394 1117395 1117396 1117397 1117398 1117399 1117400 [...] (7164 flits)
Class 0:
Remaining flits: 1199844 1199845 1199846 1199847 1199848 1199849 1199850 1199851 1199852 1199853 [...] (17693 flits)
Measured flits: 1202148 1202149 1202150 1202151 1202152 1202153 1202154 1202155 1202156 1202157 [...] (6132 flits)
Class 0:
Remaining flits: 1213560 1213561 1213562 1213563 1213564 1213565 1213566 1213567 1213568 1213569 [...] (17474 flits)
Measured flits: 1220796 1220797 1220798 1220799 1220800 1220801 1220802 1220803 1220804 1220805 [...] (4881 flits)
Class 0:
Remaining flits: 1256130 1256131 1256132 1256133 1256134 1256135 1256136 1256137 1256138 1256139 [...] (17605 flits)
Measured flits: 1256130 1256131 1256132 1256133 1256134 1256135 1256136 1256137 1256138 1256139 [...] (4421 flits)
Class 0:
Remaining flits: 1290096 1290097 1290098 1290099 1290100 1290101 1290102 1290103 1290104 1290105 [...] (17866 flits)
Measured flits: 1303164 1303165 1303166 1303167 1303168 1303169 1303170 1303171 1303172 1303173 [...] (4125 flits)
Class 0:
Remaining flits: 1314216 1314217 1314218 1314219 1314220 1314221 1314222 1314223 1314224 1314225 [...] (17925 flits)
Measured flits: 1326996 1326997 1326998 1326999 1327000 1327001 1327002 1327003 1327004 1327005 [...] (3796 flits)
Class 0:
Remaining flits: 1341383 1341384 1341385 1341386 1341387 1341388 1341389 1341390 1341391 1341392 [...] (17974 flits)
Measured flits: 1348555 1348556 1348557 1348558 1348559 1361196 1361197 1361198 1361199 1361200 [...] (2933 flits)
Class 0:
Remaining flits: 1363410 1363411 1363412 1363413 1363414 1363415 1363416 1363417 1363418 1363419 [...] (17974 flits)
Measured flits: 1371096 1371097 1371098 1371099 1371100 1371101 1371102 1371103 1371104 1371105 [...] (2167 flits)
Class 0:
Remaining flits: 1395324 1395325 1395326 1395327 1395328 1395329 1395330 1395331 1395332 1395333 [...] (17345 flits)
Measured flits: 1403946 1403947 1403948 1403949 1403950 1403951 1403952 1403953 1403954 1403955 [...] (1990 flits)
Class 0:
Remaining flits: 1433574 1433575 1433576 1433577 1433578 1433579 1433580 1433581 1433582 1433583 [...] (17957 flits)
Measured flits: 1436850 1436851 1436852 1436853 1436854 1436855 1436856 1436857 1436858 1436859 [...] (1542 flits)
Class 0:
Remaining flits: 1452744 1452745 1452746 1452747 1452748 1452749 1452750 1452751 1452752 1452753 [...] (17608 flits)
Measured flits: 1463292 1463293 1463294 1463295 1463296 1463297 1463298 1463299 1463300 1463301 [...] (1419 flits)
Class 0:
Remaining flits: 1479816 1479817 1479818 1479819 1479820 1479821 1479822 1479823 1479824 1479825 [...] (17786 flits)
Measured flits: 1526130 1526131 1526132 1526133 1526134 1526135 1526136 1526137 1526138 1526139 [...] (1128 flits)
Class 0:
Remaining flits: 1507896 1507897 1507898 1507899 1507900 1507901 1507902 1507903 1507904 1507905 [...] (17952 flits)
Measured flits: 1550490 1550491 1550492 1550493 1550494 1550495 1550496 1550497 1550498 1550499 [...] (845 flits)
Class 0:
Remaining flits: 1541737 1541738 1541739 1541740 1541741 1541742 1541743 1541744 1541745 1541746 [...] (18062 flits)
Measured flits: 1595250 1595251 1595252 1595253 1595254 1595255 1595256 1595257 1595258 1595259 [...] (478 flits)
Class 0:
Remaining flits: 1581336 1581337 1581338 1581339 1581340 1581341 1581342 1581343 1581344 1581345 [...] (17900 flits)
Measured flits: 1610746 1610747 1627650 1627651 1627652 1627653 1627654 1627655 1627656 1627657 [...] (572 flits)
Class 0:
Remaining flits: 1606737 1606738 1606739 1606740 1606741 1606742 1606743 1606744 1606745 1606746 [...] (17855 flits)
Measured flits: 1669536 1669537 1669538 1669539 1669540 1669541 1669542 1669543 1669544 1669545 [...] (381 flits)
Class 0:
Remaining flits: 1639026 1639027 1639028 1639029 1639030 1639031 1639032 1639033 1639034 1639035 [...] (17378 flits)
Measured flits: 1697364 1697365 1697366 1697367 1697368 1697369 1697370 1697371 1697372 1697373 [...] (270 flits)
Class 0:
Remaining flits: 1676808 1676809 1676810 1676811 1676812 1676813 1676814 1676815 1676816 1676817 [...] (17642 flits)
Measured flits: 1727406 1727407 1727408 1727409 1727410 1727411 1727412 1727413 1727414 1727415 [...] (217 flits)
Class 0:
Remaining flits: 1691388 1691389 1691390 1691391 1691392 1691393 1691394 1691395 1691396 1691397 [...] (17863 flits)
Measured flits: 1735047 1735048 1735049 1735050 1735051 1735052 1735053 1735054 1735055 1757135 [...] (196 flits)
Class 0:
Remaining flits: 1733256 1733257 1733258 1733259 1733260 1733261 1733262 1733263 1733264 1733265 [...] (17822 flits)
Measured flits: 1794096 1794097 1794098 1794099 1794100 1794101 1794102 1794103 1794104 1794105 [...] (108 flits)
Class 0:
Remaining flits: 1783224 1783225 1783226 1783227 1783228 1783229 1783230 1783231 1783232 1783233 [...] (17943 flits)
Measured flits: 1798405 1798406 1798407 1798408 1798409 1798410 1798411 1798412 1798413 1798414 [...] (86 flits)
Class 0:
Remaining flits: 1788279 1788280 1788281 1793322 1793323 1793324 1793325 1793326 1793327 1793328 [...] (17664 flits)
Measured flits: 1837044 1837045 1837046 1837047 1837048 1837049 1837050 1837051 1837052 1837053 [...] (54 flits)
Draining remaining packets ...
Time taken is 61335 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15848.7 (1 samples)
	minimum = 46 (1 samples)
	maximum = 51604 (1 samples)
Network latency average = 550.706 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3537 (1 samples)
Flit latency average = 488.536 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3469 (1 samples)
Fragmentation average = 57.8871 (1 samples)
	minimum = 0 (1 samples)
	maximum = 158 (1 samples)
Injected packet rate average = 0.00895313 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0145714 (1 samples)
Accepted packet rate average = 0.00897024 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.0118571 (1 samples)
Injected flit rate average = 0.161103 (1 samples)
	minimum = 0.0884286 (1 samples)
	maximum = 0.262286 (1 samples)
Accepted flit rate average = 0.161347 (1 samples)
	minimum = 0.108429 (1 samples)
	maximum = 0.215429 (1 samples)
Injected packet size average = 17.9941 (1 samples)
Accepted packet size average = 17.987 (1 samples)
Hops average = 5.06569 (1 samples)
Total run time 41.2818
