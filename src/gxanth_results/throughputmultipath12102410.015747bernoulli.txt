BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.26
	minimum = 23
	maximum = 790
Network latency average = 210.287
	minimum = 23
	maximum = 790
Slowest packet = 555
Flit latency average = 170.183
	minimum = 6
	maximum = 866
Slowest flit = 6948
Fragmentation average = 58.1749
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0158542
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00893229
	minimum = 0.003 (at node 41)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.282297
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.16875
	minimum = 0.063 (at node 174)
	maximum = 0.291 (at node 44)
Injected packet length average = 17.8058
Accepted packet length average = 18.8921
Total in-flight flits = 22392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 372.699
	minimum = 23
	maximum = 1613
Network latency average = 369.519
	minimum = 23
	maximum = 1613
Slowest packet = 948
Flit latency average = 329.295
	minimum = 6
	maximum = 1632
Slowest flit = 19289
Fragmentation average = 64.3029
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 164)
	maximum = 0.0235 (at node 44)
Accepted packet rate average = 0.0093099
	minimum = 0.005 (at node 83)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.280391
	minimum = 0.162 (at node 164)
	maximum = 0.423 (at node 44)
Accepted flit rate average= 0.171948
	minimum = 0.0965 (at node 83)
	maximum = 0.279 (at node 44)
Injected packet length average = 17.9211
Accepted packet length average = 18.4694
Total in-flight flits = 42116 (0 measured)
latency change    = 0.427795
throughput change = 0.0185982
Class 0:
Packet latency average = 864.152
	minimum = 23
	maximum = 2342
Network latency average = 860.89
	minimum = 23
	maximum = 2342
Slowest packet = 1992
Flit latency average = 825.294
	minimum = 6
	maximum = 2327
Slowest flit = 36299
Fragmentation average = 68.4893
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0156458
	minimum = 0.005 (at node 67)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.00969792
	minimum = 0.003 (at node 13)
	maximum = 0.018 (at node 173)
Injected flit rate average = 0.281401
	minimum = 0.101 (at node 67)
	maximum = 0.504 (at node 35)
Accepted flit rate average= 0.173849
	minimum = 0.053 (at node 184)
	maximum = 0.31 (at node 56)
Injected packet length average = 17.9857
Accepted packet length average = 17.9264
Total in-flight flits = 62809 (0 measured)
latency change    = 0.568712
throughput change = 0.010935
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 211.416
	minimum = 23
	maximum = 973
Network latency average = 208.512
	minimum = 23
	maximum = 973
Slowest packet = 9022
Flit latency average = 1158.26
	minimum = 6
	maximum = 3115
Slowest flit = 47622
Fragmentation average = 32.118
	minimum = 0
	maximum = 166
Injected packet rate average = 0.016276
	minimum = 0.008 (at node 154)
	maximum = 0.027 (at node 155)
Accepted packet rate average = 0.00973958
	minimum = 0.002 (at node 132)
	maximum = 0.019 (at node 114)
Injected flit rate average = 0.292349
	minimum = 0.144 (at node 154)
	maximum = 0.486 (at node 155)
Accepted flit rate average= 0.17524
	minimum = 0.035 (at node 132)
	maximum = 0.335 (at node 63)
Injected packet length average = 17.9619
Accepted packet length average = 17.9925
Total in-flight flits = 85413 (49159 measured)
latency change    = 3.08746
throughput change = 0.00793556
Class 0:
Packet latency average = 567.997
	minimum = 23
	maximum = 1942
Network latency average = 564.878
	minimum = 23
	maximum = 1942
Slowest packet = 9037
Flit latency average = 1329.18
	minimum = 6
	maximum = 4003
Slowest flit = 52811
Fragmentation average = 48.9813
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0159089
	minimum = 0.008 (at node 154)
	maximum = 0.023 (at node 150)
Accepted packet rate average = 0.00964062
	minimum = 0.0045 (at node 96)
	maximum = 0.0165 (at node 34)
Injected flit rate average = 0.286482
	minimum = 0.144 (at node 154)
	maximum = 0.414 (at node 150)
Accepted flit rate average= 0.173299
	minimum = 0.0845 (at node 96)
	maximum = 0.292 (at node 63)
Injected packet length average = 18.0077
Accepted packet length average = 17.976
Total in-flight flits = 106224 (87934 measured)
latency change    = 0.627788
throughput change = 0.0111951
Class 0:
Packet latency average = 1002.51
	minimum = 23
	maximum = 2983
Network latency average = 999.185
	minimum = 23
	maximum = 2983
Slowest packet = 9012
Flit latency average = 1510.91
	minimum = 6
	maximum = 4768
Slowest flit = 61631
Fragmentation average = 57.0389
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0158767
	minimum = 0.00966667 (at node 118)
	maximum = 0.0243333 (at node 3)
Accepted packet rate average = 0.00976215
	minimum = 0.005 (at node 36)
	maximum = 0.015 (at node 34)
Injected flit rate average = 0.285889
	minimum = 0.172667 (at node 118)
	maximum = 0.438 (at node 3)
Accepted flit rate average= 0.175915
	minimum = 0.0936667 (at node 36)
	maximum = 0.264333 (at node 34)
Injected packet length average = 18.0068
Accepted packet length average = 18.0201
Total in-flight flits = 126092 (118498 measured)
latency change    = 0.433422
throughput change = 0.0148677
Draining remaining packets ...
Class 0:
Remaining flits: 78372 78373 78374 78375 78376 78377 78378 78379 78380 78381 [...] (96388 flits)
Measured flits: 162270 162271 162272 162273 162274 162275 162276 162277 162278 162279 [...] (93514 flits)
Class 0:
Remaining flits: 96372 96373 96374 96375 96376 96377 96378 96379 96380 96381 [...] (66970 flits)
Measured flits: 162774 162775 162776 162777 162778 162779 162780 162781 162782 162783 [...] (65934 flits)
Class 0:
Remaining flits: 104652 104653 104654 104655 104656 104657 104658 104659 104660 104661 [...] (37416 flits)
Measured flits: 164089 164090 164091 164092 164093 164094 164095 164096 164097 164098 [...] (37098 flits)
Class 0:
Remaining flits: 172944 172945 172946 172947 172948 172949 172950 172951 172952 172953 [...] (9064 flits)
Measured flits: 172944 172945 172946 172947 172948 172949 172950 172951 172952 172953 [...] (9064 flits)
Time taken is 10738 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2844.11 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6919 (1 samples)
Network latency average = 2840.67 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6919 (1 samples)
Flit latency average = 2641.65 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7409 (1 samples)
Fragmentation average = 63.499 (1 samples)
	minimum = 0 (1 samples)
	maximum = 166 (1 samples)
Injected packet rate average = 0.0158767 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.00976215 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.285889 (1 samples)
	minimum = 0.172667 (1 samples)
	maximum = 0.438 (1 samples)
Accepted flit rate average = 0.175915 (1 samples)
	minimum = 0.0936667 (1 samples)
	maximum = 0.264333 (1 samples)
Injected packet size average = 18.0068 (1 samples)
Accepted packet size average = 18.0201 (1 samples)
Hops average = 5.06802 (1 samples)
Total run time 5.98775
