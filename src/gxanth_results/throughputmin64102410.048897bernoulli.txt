BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 334.268
	minimum = 27
	maximum = 931
Network latency average = 311.89
	minimum = 24
	maximum = 931
Slowest packet = 458
Flit latency average = 241.482
	minimum = 6
	maximum = 959
Slowest flit = 3009
Fragmentation average = 189.011
	minimum = 0
	maximum = 795
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.011474
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 122)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.246052
	minimum = 0.104 (at node 174)
	maximum = 0.407 (at node 122)
Injected packet length average = 17.8227
Accepted packet length average = 21.4444
Total in-flight flits = 113282 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 589.593
	minimum = 27
	maximum = 1910
Network latency average = 557.914
	minimum = 23
	maximum = 1887
Slowest packet = 719
Flit latency average = 474.69
	minimum = 6
	maximum = 1909
Slowest flit = 10130
Fragmentation average = 250.35
	minimum = 0
	maximum = 1745
Injected packet rate average = 0.0475937
	minimum = 0.0365 (at node 72)
	maximum = 0.0555 (at node 32)
Accepted packet rate average = 0.0128854
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 165)
Injected flit rate average = 0.852776
	minimum = 0.6485 (at node 72)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.257969
	minimum = 0.141 (at node 116)
	maximum = 0.383 (at node 165)
Injected packet length average = 17.9178
Accepted packet length average = 20.0202
Total in-flight flits = 229908 (0 measured)
latency change    = 0.433053
throughput change = 0.0461942
Class 0:
Packet latency average = 1277.37
	minimum = 27
	maximum = 2871
Network latency average = 1230.87
	minimum = 23
	maximum = 2859
Slowest packet = 546
Flit latency average = 1162.88
	minimum = 6
	maximum = 2871
Slowest flit = 15234
Fragmentation average = 344.31
	minimum = 0
	maximum = 2287
Injected packet rate average = 0.0485
	minimum = 0.034 (at node 2)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0151354
	minimum = 0.007 (at node 4)
	maximum = 0.025 (at node 133)
Injected flit rate average = 0.872458
	minimum = 0.608 (at node 2)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.27462
	minimum = 0.152 (at node 183)
	maximum = 0.431 (at node 159)
Injected packet length average = 17.9888
Accepted packet length average = 18.1442
Total in-flight flits = 344797 (0 measured)
latency change    = 0.53843
throughput change = 0.0606331
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 111.73
	minimum = 27
	maximum = 473
Network latency average = 55.3206
	minimum = 25
	maximum = 372
Slowest packet = 27676
Flit latency average = 1705.05
	minimum = 6
	maximum = 3873
Slowest flit = 15472
Fragmentation average = 17.4461
	minimum = 0
	maximum = 81
Injected packet rate average = 0.0492031
	minimum = 0.031 (at node 153)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.015224
	minimum = 0.005 (at node 64)
	maximum = 0.028 (at node 125)
Injected flit rate average = 0.886292
	minimum = 0.541 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.275813
	minimum = 0.093 (at node 64)
	maximum = 0.469 (at node 125)
Injected packet length average = 18.0129
Accepted packet length average = 18.117
Total in-flight flits = 461887 (156651 measured)
latency change    = 10.4326
throughput change = 0.00432434
Class 0:
Packet latency average = 114.647
	minimum = 25
	maximum = 1714
Network latency average = 55.5089
	minimum = 25
	maximum = 1712
Slowest packet = 28789
Flit latency average = 2010.56
	minimum = 6
	maximum = 4885
Slowest flit = 15123
Fragmentation average = 17.1339
	minimum = 0
	maximum = 407
Injected packet rate average = 0.0489766
	minimum = 0.0365 (at node 153)
	maximum = 0.056 (at node 164)
Accepted packet rate average = 0.0151432
	minimum = 0.007 (at node 35)
	maximum = 0.026 (at node 125)
Injected flit rate average = 0.882107
	minimum = 0.657 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.273359
	minimum = 0.143 (at node 35)
	maximum = 0.423 (at node 125)
Injected packet length average = 18.0108
Accepted packet length average = 18.0516
Total in-flight flits = 578353 (312005 measured)
latency change    = 0.0254442
throughput change = 0.00897399
Class 0:
Packet latency average = 139.481
	minimum = 23
	maximum = 2904
Network latency average = 79.3109
	minimum = 23
	maximum = 2904
Slowest packet = 27911
Flit latency average = 2292.92
	minimum = 6
	maximum = 5716
Slowest flit = 39709
Fragmentation average = 20.1039
	minimum = 0
	maximum = 662
Injected packet rate average = 0.0488854
	minimum = 0.0406667 (at node 152)
	maximum = 0.0556667 (at node 44)
Accepted packet rate average = 0.0151545
	minimum = 0.009 (at node 35)
	maximum = 0.0216667 (at node 125)
Injected flit rate average = 0.88021
	minimum = 0.733333 (at node 152)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.272998
	minimum = 0.169 (at node 35)
	maximum = 0.371333 (at node 125)
Injected packet length average = 18.0056
Accepted packet length average = 18.0143
Total in-flight flits = 694394 (466106 measured)
latency change    = 0.178047
throughput change = 0.00132276
Draining remaining packets ...
Class 0:
Remaining flits: 17732 17733 17734 17735 17736 17737 17738 17739 17740 17741 [...] (655238 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (464349 flits)
Class 0:
Remaining flits: 20484 20485 20486 20487 20488 20489 20490 20491 20492 20493 [...] (616936 flits)
Measured flits: 496596 496597 496598 496599 496600 496601 496602 496603 496604 496605 [...] (460758 flits)
Class 0:
Remaining flits: 20484 20485 20486 20487 20488 20489 20490 20491 20492 20493 [...] (579485 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (454014 flits)
Class 0:
Remaining flits: 20484 20485 20486 20487 20488 20489 20490 20491 20492 20493 [...] (542063 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (442596 flits)
Class 0:
Remaining flits: 40716 40717 40718 40719 40720 40721 40722 40723 40724 40725 [...] (505855 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (425779 flits)
Class 0:
Remaining flits: 40716 40717 40718 40719 40720 40721 40722 40723 40724 40725 [...] (469846 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (405425 flits)
Class 0:
Remaining flits: 40716 40717 40718 40719 40720 40721 40722 40723 40724 40725 [...] (434829 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (381880 flits)
Class 0:
Remaining flits: 51300 51301 51302 51303 51304 51305 51306 51307 51308 51309 [...] (400456 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (356579 flits)
Class 0:
Remaining flits: 51300 51301 51302 51303 51304 51305 51306 51307 51308 51309 [...] (366198 flits)
Measured flits: 496602 496603 496604 496605 496606 496607 496608 496609 496610 496611 [...] (328712 flits)
Class 0:
Remaining flits: 59418 59419 59420 59421 59422 59423 59424 59425 59426 59427 [...] (332220 flits)
Measured flits: 496703 496704 496705 496706 496707 496708 496709 496728 496729 496730 [...] (300418 flits)
Class 0:
Remaining flits: 59418 59419 59420 59421 59422 59423 59424 59425 59426 59427 [...] (298295 flits)
Measured flits: 496728 496729 496730 496731 496732 496733 496734 496735 496736 496737 [...] (271077 flits)
Class 0:
Remaining flits: 59418 59419 59420 59421 59422 59423 59424 59425 59426 59427 [...] (265123 flits)
Measured flits: 496728 496729 496730 496731 496732 496733 496734 496735 496736 496737 [...] (241986 flits)
Class 0:
Remaining flits: 62424 62425 62426 62427 62428 62429 62430 62431 62432 62433 [...] (232461 flits)
Measured flits: 496728 496729 496730 496731 496732 496733 496734 496735 496736 496737 [...] (212946 flits)
Class 0:
Remaining flits: 62424 62425 62426 62427 62428 62429 62430 62431 62432 62433 [...] (199657 flits)
Measured flits: 496764 496765 496766 496767 496768 496769 496770 496771 496772 496773 [...] (183529 flits)
Class 0:
Remaining flits: 62431 62432 62433 62434 62435 62436 62437 62438 62439 62440 [...] (167110 flits)
Measured flits: 496764 496765 496766 496767 496768 496769 496770 496771 496772 496773 [...] (154432 flits)
Class 0:
Remaining flits: 65178 65179 65180 65181 65182 65183 65184 65185 65186 65187 [...] (134695 flits)
Measured flits: 496764 496765 496766 496767 496768 496769 496770 496771 496772 496773 [...] (124947 flits)
Class 0:
Remaining flits: 74268 74269 74270 74271 74272 74273 74274 74275 74276 74277 [...] (102312 flits)
Measured flits: 497808 497809 497810 497811 497812 497813 497814 497815 497816 497817 [...] (95305 flits)
Class 0:
Remaining flits: 74808 74809 74810 74811 74812 74813 74814 74815 74816 74817 [...] (71222 flits)
Measured flits: 497816 497817 497818 497819 497820 497821 497822 497823 497824 497825 [...] (66071 flits)
Class 0:
Remaining flits: 87120 87121 87122 87123 87124 87125 87126 87127 87128 87129 [...] (43200 flits)
Measured flits: 498222 498223 498224 498225 498226 498227 498228 498229 498230 498231 [...] (39897 flits)
Class 0:
Remaining flits: 87120 87121 87122 87123 87124 87125 87126 87127 87128 87129 [...] (21277 flits)
Measured flits: 498330 498331 498332 498333 498334 498335 498336 498337 498338 498339 [...] (19545 flits)
Class 0:
Remaining flits: 87120 87121 87122 87123 87124 87125 87126 87127 87128 87129 [...] (8648 flits)
Measured flits: 500130 500131 500132 500133 500134 500135 500136 500137 500138 500139 [...] (7795 flits)
Class 0:
Remaining flits: 114558 114559 114560 114561 114562 114563 114564 114565 114566 114567 [...] (3404 flits)
Measured flits: 500130 500131 500132 500133 500134 500135 500136 500137 500138 500139 [...] (2957 flits)
Class 0:
Remaining flits: 229415 229416 229417 229418 229419 229420 229421 229422 229423 229424 [...] (2228 flits)
Measured flits: 500134 500135 500136 500137 500138 500139 500140 500141 500142 500143 [...] (2021 flits)
Class 0:
Remaining flits: 401580 401581 401582 401583 401584 401585 401586 401587 401588 401589 [...] (1228 flits)
Measured flits: 503465 503466 503467 503468 503469 503470 503471 503472 503473 503474 [...] (1192 flits)
Class 0:
Remaining flits: 657936 657937 657938 657939 657940 657941 657942 657943 657944 657945 [...] (229 flits)
Measured flits: 657936 657937 657938 657939 657940 657941 657942 657943 657944 657945 [...] (229 flits)
Time taken is 31229 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12782.8 (1 samples)
	minimum = 23 (1 samples)
	maximum = 27448 (1 samples)
Network latency average = 12721.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 27339 (1 samples)
Flit latency average = 10281.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 28403 (1 samples)
Fragmentation average = 253.802 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6955 (1 samples)
Injected packet rate average = 0.0488854 (1 samples)
	minimum = 0.0406667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0151545 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.88021 (1 samples)
	minimum = 0.733333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.272998 (1 samples)
	minimum = 0.169 (1 samples)
	maximum = 0.371333 (1 samples)
Injected packet size average = 18.0056 (1 samples)
Accepted packet size average = 18.0143 (1 samples)
Hops average = 5.07373 (1 samples)
Total run time 41.567
