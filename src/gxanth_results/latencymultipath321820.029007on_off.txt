BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 313.872
	minimum = 22
	maximum = 973
Network latency average = 217.057
	minimum = 22
	maximum = 841
Slowest packet = 21
Flit latency average = 184.361
	minimum = 5
	maximum = 842
Slowest flit = 11733
Fragmentation average = 44.5213
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0223229
	minimum = 0 (at node 122)
	maximum = 0.052 (at node 11)
Accepted packet rate average = 0.0132187
	minimum = 0.006 (at node 150)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.397432
	minimum = 0 (at node 122)
	maximum = 0.933 (at node 11)
Accepted flit rate average= 0.247984
	minimum = 0.108 (at node 150)
	maximum = 0.416 (at node 44)
Injected packet length average = 17.8038
Accepted packet length average = 18.76
Total in-flight flits = 29679 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 532.686
	minimum = 22
	maximum = 1928
Network latency average = 380.781
	minimum = 22
	maximum = 1550
Slowest packet = 21
Flit latency average = 342.3
	minimum = 5
	maximum = 1533
Slowest flit = 20735
Fragmentation average = 52.5321
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0202682
	minimum = 0.003 (at node 2)
	maximum = 0.0415 (at node 171)
Accepted packet rate average = 0.0140026
	minimum = 0.008 (at node 10)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.362677
	minimum = 0.054 (at node 2)
	maximum = 0.747 (at node 171)
Accepted flit rate average= 0.257315
	minimum = 0.144 (at node 10)
	maximum = 0.396 (at node 152)
Injected packet length average = 17.8939
Accepted packet length average = 18.3762
Total in-flight flits = 41699 (0 measured)
latency change    = 0.410775
throughput change = 0.0362619
Class 0:
Packet latency average = 1152.43
	minimum = 28
	maximum = 2874
Network latency average = 757.477
	minimum = 22
	maximum = 2176
Slowest packet = 1888
Flit latency average = 708.613
	minimum = 5
	maximum = 2159
Slowest flit = 60731
Fragmentation average = 64.1424
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0160417
	minimum = 0 (at node 22)
	maximum = 0.036 (at node 0)
Accepted packet rate average = 0.0149219
	minimum = 0.008 (at node 49)
	maximum = 0.025 (at node 166)
Injected flit rate average = 0.287823
	minimum = 0 (at node 22)
	maximum = 0.641 (at node 0)
Accepted flit rate average= 0.270427
	minimum = 0.141 (at node 146)
	maximum = 0.445 (at node 34)
Injected packet length average = 17.9422
Accepted packet length average = 18.1229
Total in-flight flits = 45577 (0 measured)
latency change    = 0.537771
throughput change = 0.0484862
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1221.27
	minimum = 28
	maximum = 3670
Network latency average = 385.518
	minimum = 22
	maximum = 980
Slowest packet = 10909
Flit latency average = 828.684
	minimum = 5
	maximum = 2563
Slowest flit = 79793
Fragmentation average = 36.2132
	minimum = 0
	maximum = 207
Injected packet rate average = 0.0160677
	minimum = 0 (at node 105)
	maximum = 0.032 (at node 103)
Accepted packet rate average = 0.0149635
	minimum = 0.006 (at node 1)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.288729
	minimum = 0 (at node 105)
	maximum = 0.561 (at node 103)
Accepted flit rate average= 0.268771
	minimum = 0.091 (at node 1)
	maximum = 0.458 (at node 129)
Injected packet length average = 17.9695
Accepted packet length average = 17.9617
Total in-flight flits = 49413 (42259 measured)
latency change    = 0.0563718
throughput change = 0.00616231
Class 0:
Packet latency average = 1730.43
	minimum = 25
	maximum = 4722
Network latency average = 728.539
	minimum = 22
	maximum = 1908
Slowest packet = 10909
Flit latency average = 853.594
	minimum = 5
	maximum = 3072
Slowest flit = 86633
Fragmentation average = 61.3136
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0154948
	minimum = 0.0005 (at node 186)
	maximum = 0.0275 (at node 187)
Accepted packet rate average = 0.0148333
	minimum = 0.009 (at node 79)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.278773
	minimum = 0.0145 (at node 186)
	maximum = 0.493 (at node 187)
Accepted flit rate average= 0.26819
	minimum = 0.1655 (at node 4)
	maximum = 0.408 (at node 128)
Injected packet length average = 17.9914
Accepted packet length average = 18.0802
Total in-flight flits = 49710 (49311 measured)
latency change    = 0.294239
throughput change = 0.00216536
Class 0:
Packet latency average = 2077.49
	minimum = 25
	maximum = 5587
Network latency average = 852.449
	minimum = 22
	maximum = 2737
Slowest packet = 10909
Flit latency average = 878.823
	minimum = 5
	maximum = 3072
Slowest flit = 86633
Fragmentation average = 67.378
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0154149
	minimum = 0.00233333 (at node 186)
	maximum = 0.025 (at node 170)
Accepted packet rate average = 0.0148663
	minimum = 0.01 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.2772
	minimum = 0.0456667 (at node 186)
	maximum = 0.455333 (at node 170)
Accepted flit rate average= 0.267753
	minimum = 0.176 (at node 36)
	maximum = 0.376333 (at node 129)
Injected packet length average = 17.9825
Accepted packet length average = 18.0107
Total in-flight flits = 51191 (51173 measured)
latency change    = 0.167054
throughput change = 0.00163072
Class 0:
Packet latency average = 2335.62
	minimum = 25
	maximum = 6309
Network latency average = 902.238
	minimum = 22
	maximum = 3240
Slowest packet = 10909
Flit latency average = 893.525
	minimum = 5
	maximum = 4062
Slowest flit = 188276
Fragmentation average = 70.6882
	minimum = 0
	maximum = 316
Injected packet rate average = 0.015306
	minimum = 0.003 (at node 186)
	maximum = 0.02425 (at node 187)
Accepted packet rate average = 0.0148789
	minimum = 0.01025 (at node 64)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.275404
	minimum = 0.05675 (at node 186)
	maximum = 0.43675 (at node 187)
Accepted flit rate average= 0.268273
	minimum = 0.18575 (at node 64)
	maximum = 0.36025 (at node 128)
Injected packet length average = 17.9932
Accepted packet length average = 18.0305
Total in-flight flits = 51115 (51114 measured)
latency change    = 0.110521
throughput change = 0.00193819
Class 0:
Packet latency average = 2588.41
	minimum = 25
	maximum = 7282
Network latency average = 930.225
	minimum = 22
	maximum = 3690
Slowest packet = 10909
Flit latency average = 903.616
	minimum = 5
	maximum = 4062
Slowest flit = 188276
Fragmentation average = 73.7625
	minimum = 0
	maximum = 326
Injected packet rate average = 0.0152896
	minimum = 0.0082 (at node 186)
	maximum = 0.023 (at node 187)
Accepted packet rate average = 0.0149094
	minimum = 0.0116 (at node 80)
	maximum = 0.0194 (at node 128)
Injected flit rate average = 0.275057
	minimum = 0.1498 (at node 186)
	maximum = 0.4126 (at node 187)
Accepted flit rate average= 0.268752
	minimum = 0.2052 (at node 80)
	maximum = 0.3468 (at node 128)
Injected packet length average = 17.9898
Accepted packet length average = 18.0257
Total in-flight flits = 51779 (51779 measured)
latency change    = 0.0976621
throughput change = 0.00178099
Class 0:
Packet latency average = 2822.04
	minimum = 25
	maximum = 7622
Network latency average = 951.04
	minimum = 22
	maximum = 3690
Slowest packet = 10909
Flit latency average = 913.202
	minimum = 5
	maximum = 4062
Slowest flit = 188276
Fragmentation average = 75.3816
	minimum = 0
	maximum = 333
Injected packet rate average = 0.0152023
	minimum = 0.0095 (at node 32)
	maximum = 0.022 (at node 187)
Accepted packet rate average = 0.0148941
	minimum = 0.0116667 (at node 79)
	maximum = 0.0193333 (at node 138)
Injected flit rate average = 0.27358
	minimum = 0.169167 (at node 32)
	maximum = 0.396333 (at node 187)
Accepted flit rate average= 0.268589
	minimum = 0.214 (at node 171)
	maximum = 0.350333 (at node 138)
Injected packet length average = 17.996
Accepted packet length average = 18.0333
Total in-flight flits = 51558 (51558 measured)
latency change    = 0.0827864
throughput change = 0.000605659
Class 0:
Packet latency average = 3054.25
	minimum = 25
	maximum = 8265
Network latency average = 961.239
	minimum = 22
	maximum = 3831
Slowest packet = 10909
Flit latency average = 916.996
	minimum = 5
	maximum = 4062
Slowest flit = 188276
Fragmentation average = 76.5107
	minimum = 0
	maximum = 333
Injected packet rate average = 0.0151845
	minimum = 0.00985714 (at node 88)
	maximum = 0.0208571 (at node 187)
Accepted packet rate average = 0.0149196
	minimum = 0.0118571 (at node 79)
	maximum = 0.0182857 (at node 103)
Injected flit rate average = 0.273253
	minimum = 0.177714 (at node 88)
	maximum = 0.375571 (at node 187)
Accepted flit rate average= 0.268681
	minimum = 0.214429 (at node 79)
	maximum = 0.331571 (at node 128)
Injected packet length average = 17.9955
Accepted packet length average = 18.0085
Total in-flight flits = 51868 (51868 measured)
latency change    = 0.0760278
throughput change = 0.000340158
Draining all recorded packets ...
Class 0:
Remaining flits: 436716 436717 436718 436719 436720 436721 436722 436723 436724 436725 [...] (50573 flits)
Measured flits: 436716 436717 436718 436719 436720 436721 436722 436723 436724 436725 [...] (49774 flits)
Class 0:
Remaining flits: 436728 436729 436730 436731 436732 436733 475775 496530 496531 496532 [...] (51291 flits)
Measured flits: 436728 436729 436730 436731 436732 436733 475775 496530 496531 496532 [...] (49375 flits)
Class 0:
Remaining flits: 496530 496531 496532 496533 496534 496535 496536 496537 496538 496539 [...] (50744 flits)
Measured flits: 496530 496531 496532 496533 496534 496535 496536 496537 496538 496539 [...] (46672 flits)
Class 0:
Remaining flits: 515772 515773 515774 515775 515776 515777 515778 515779 515780 515781 [...] (50898 flits)
Measured flits: 515772 515773 515774 515775 515776 515777 515778 515779 515780 515781 [...] (44631 flits)
Class 0:
Remaining flits: 589770 589771 589772 589773 589774 589775 589776 589777 589778 589779 [...] (51659 flits)
Measured flits: 589770 589771 589772 589773 589774 589775 589776 589777 589778 589779 [...] (41427 flits)
Class 0:
Remaining flits: 626614 626615 638478 638479 638480 638481 638482 638483 638484 638485 [...] (50715 flits)
Measured flits: 626614 626615 638478 638479 638480 638481 638482 638483 638484 638485 [...] (34975 flits)
Class 0:
Remaining flits: 646758 646759 646760 646761 646762 646763 646764 646765 646766 646767 [...] (50950 flits)
Measured flits: 646758 646759 646760 646761 646762 646763 646764 646765 646766 646767 [...] (30889 flits)
Class 0:
Remaining flits: 646758 646759 646760 646761 646762 646763 646764 646765 646766 646767 [...] (48983 flits)
Measured flits: 646758 646759 646760 646761 646762 646763 646764 646765 646766 646767 [...] (24899 flits)
Class 0:
Remaining flits: 734444 734445 734446 734447 734448 734449 734450 734451 734452 734453 [...] (48966 flits)
Measured flits: 781398 781399 781400 781401 781402 781403 781404 781405 781406 781407 [...] (19107 flits)
Class 0:
Remaining flits: 824405 824406 824407 824408 824409 824410 824411 824412 824413 824414 [...] (48397 flits)
Measured flits: 824405 824406 824407 824408 824409 824410 824411 824412 824413 824414 [...] (15414 flits)
Class 0:
Remaining flits: 825138 825139 825140 825141 825142 825143 825144 825145 825146 825147 [...] (49814 flits)
Measured flits: 825138 825139 825140 825141 825142 825143 825144 825145 825146 825147 [...] (12961 flits)
Class 0:
Remaining flits: 889272 889273 889274 889275 889276 889277 889278 889279 889280 889281 [...] (48558 flits)
Measured flits: 889272 889273 889274 889275 889276 889277 889278 889279 889280 889281 [...] (9282 flits)
Class 0:
Remaining flits: 953838 953839 953840 953841 953842 953843 953844 953845 953846 953847 [...] (47562 flits)
Measured flits: 953838 953839 953840 953841 953842 953843 953844 953845 953846 953847 [...] (6239 flits)
Class 0:
Remaining flits: 994675 994676 994677 994678 994679 1010574 1010575 1010576 1010577 1010578 [...] (48544 flits)
Measured flits: 1010574 1010575 1010576 1010577 1010578 1010579 1010580 1010581 1010582 1010583 [...] (4088 flits)
Class 0:
Remaining flits: 1010574 1010575 1010576 1010577 1010578 1010579 1010580 1010581 1010582 1010583 [...] (48662 flits)
Measured flits: 1010574 1010575 1010576 1010577 1010578 1010579 1010580 1010581 1010582 1010583 [...] (3542 flits)
Class 0:
Remaining flits: 1104084 1104085 1104086 1104087 1104088 1104089 1104090 1104091 1104092 1104093 [...] (48434 flits)
Measured flits: 1151297 1228428 1228429 1228430 1228431 1228432 1228433 1228434 1228435 1228436 [...] (2600 flits)
Class 0:
Remaining flits: 1169082 1169083 1169084 1169085 1169086 1169087 1169088 1169089 1169090 1169091 [...] (49089 flits)
Measured flits: 1270134 1270135 1270136 1270137 1270138 1270139 1270140 1270141 1270142 1270143 [...] (1945 flits)
Class 0:
Remaining flits: 1202634 1202635 1202636 1202637 1202638 1202639 1202640 1202641 1202642 1202643 [...] (50178 flits)
Measured flits: 1310460 1310461 1310462 1310463 1310464 1310465 1310466 1310467 1310468 1310469 [...] (1882 flits)
Class 0:
Remaining flits: 1316138 1316139 1316140 1316141 1321236 1321237 1321238 1321239 1321240 1321241 [...] (50238 flits)
Measured flits: 1342826 1342827 1342828 1342829 1342830 1342831 1342832 1342833 1342834 1342835 [...] (1380 flits)
Class 0:
Remaining flits: 1321236 1321237 1321238 1321239 1321240 1321241 1321242 1321243 1321244 1321245 [...] (49506 flits)
Measured flits: 1429884 1429885 1429886 1429887 1429888 1429889 1429890 1429891 1429892 1429893 [...] (1209 flits)
Class 0:
Remaining flits: 1326780 1326781 1326782 1326783 1326784 1326785 1326786 1326787 1326788 1326789 [...] (49106 flits)
Measured flits: 1459152 1459153 1459154 1459155 1459156 1459157 1459158 1459159 1459160 1459161 [...] (736 flits)
Class 0:
Remaining flits: 1430658 1430659 1430660 1430661 1430662 1430663 1430664 1430665 1430666 1430667 [...] (48885 flits)
Measured flits: 1477710 1477711 1477712 1477713 1477714 1477715 1477716 1477717 1477718 1477719 [...] (511 flits)
Class 0:
Remaining flits: 1466406 1466407 1466408 1466409 1466410 1466411 1466412 1466413 1466414 1466415 [...] (48689 flits)
Measured flits: 1477724 1477725 1477726 1477727 1521864 1521865 1521866 1521867 1521868 1521869 [...] (449 flits)
Class 0:
Remaining flits: 1466406 1466407 1466408 1466409 1466410 1466411 1466412 1466413 1466414 1466415 [...] (48767 flits)
Measured flits: 1584324 1584325 1584326 1584327 1584328 1584329 1584330 1584331 1584332 1584333 [...] (146 flits)
Class 0:
Remaining flits: 1466406 1466407 1466408 1466409 1466410 1466411 1466412 1466413 1466414 1466415 [...] (48756 flits)
Measured flits: 1584324 1584325 1584326 1584327 1584328 1584329 1584330 1584331 1584332 1584333 [...] (19 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1650006 1650007 1650008 1650009 1650010 1650011 1650012 1650013 1650014 1650015 [...] (8449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1729015 1729016 1729017 1729018 1729019 1729020 1729021 1729022 1729023 1729024 [...] (444 flits)
Measured flits: (0 flits)
Time taken is 38231 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6132.57 (1 samples)
	minimum = 25 (1 samples)
	maximum = 26111 (1 samples)
Network latency average = 1010.24 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6950 (1 samples)
Flit latency average = 936.961 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7678 (1 samples)
Fragmentation average = 79.7507 (1 samples)
	minimum = 0 (1 samples)
	maximum = 358 (1 samples)
Injected packet rate average = 0.0151845 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0208571 (1 samples)
Accepted packet rate average = 0.0149196 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.273253 (1 samples)
	minimum = 0.177714 (1 samples)
	maximum = 0.375571 (1 samples)
Accepted flit rate average = 0.268681 (1 samples)
	minimum = 0.214429 (1 samples)
	maximum = 0.331571 (1 samples)
Injected packet size average = 17.9955 (1 samples)
Accepted packet size average = 18.0085 (1 samples)
Hops average = 5.07333 (1 samples)
Total run time 52.7402
