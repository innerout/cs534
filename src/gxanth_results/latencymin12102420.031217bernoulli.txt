BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 274.258
	minimum = 22
	maximum = 740
Network latency average = 265.348
	minimum = 22
	maximum = 713
Slowest packet = 1468
Flit latency average = 242.343
	minimum = 5
	maximum = 696
Slowest flit = 26441
Fragmentation average = 22.4313
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0144792
	minimum = 0.007 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.267156
	minimum = 0.126 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.8483
Accepted packet length average = 18.4511
Total in-flight flits = 55158 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 505.93
	minimum = 22
	maximum = 1420
Network latency average = 495.991
	minimum = 22
	maximum = 1364
Slowest packet = 2974
Flit latency average = 471.842
	minimum = 5
	maximum = 1363
Slowest flit = 66642
Fragmentation average = 23.8733
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.015151
	minimum = 0.008 (at node 153)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.275602
	minimum = 0.1475 (at node 153)
	maximum = 0.3845 (at node 152)
Injected packet length average = 17.9239
Accepted packet length average = 18.1903
Total in-flight flits = 108747 (0 measured)
latency change    = 0.457912
throughput change = 0.0306432
Class 0:
Packet latency average = 1161.88
	minimum = 22
	maximum = 2250
Network latency average = 1150.37
	minimum = 22
	maximum = 2116
Slowest packet = 4810
Flit latency average = 1128.61
	minimum = 5
	maximum = 2099
Slowest flit = 86580
Fragmentation average = 25.0189
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0159427
	minimum = 0.006 (at node 126)
	maximum = 0.027 (at node 159)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.287958
	minimum = 0.114 (at node 126)
	maximum = 0.476 (at node 159)
Injected packet length average = 18.0012
Accepted packet length average = 18.0621
Total in-flight flits = 160379 (0 measured)
latency change    = 0.564561
throughput change = 0.0429117
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 50.5766
	minimum = 22
	maximum = 153
Network latency average = 40.2915
	minimum = 22
	maximum = 131
Slowest packet = 19182
Flit latency average = 1579.7
	minimum = 5
	maximum = 2603
Slowest flit = 149418
Fragmentation average = 4.47872
	minimum = 0
	maximum = 28
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0161302
	minimum = 0.004 (at node 134)
	maximum = 0.029 (at node 103)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.289807
	minimum = 0.072 (at node 134)
	maximum = 0.517 (at node 103)
Injected packet length average = 17.9869
Accepted packet length average = 17.9667
Total in-flight flits = 214716 (101478 measured)
latency change    = 21.9728
throughput change = 0.00637996
Class 0:
Packet latency average = 50.9491
	minimum = 22
	maximum = 187
Network latency average = 40.9761
	minimum = 22
	maximum = 170
Slowest packet = 27510
Flit latency average = 1798.77
	minimum = 5
	maximum = 3263
Slowest flit = 183599
Fragmentation average = 4.74766
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.0160938
	minimum = 0.0095 (at node 86)
	maximum = 0.024 (at node 90)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.28963
	minimum = 0.1675 (at node 86)
	maximum = 0.429 (at node 90)
Injected packet length average = 17.9923
Accepted packet length average = 17.9964
Total in-flight flits = 265809 (199236 measured)
latency change    = 0.00731164
throughput change = 0.000611412
Class 0:
Packet latency average = 204.431
	minimum = 22
	maximum = 3006
Network latency average = 194.385
	minimum = 22
	maximum = 2964
Slowest packet = 17893
Flit latency average = 2017.7
	minimum = 5
	maximum = 3830
Slowest flit = 228041
Fragmentation average = 6.24887
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0313576
	minimum = 0.024 (at node 15)
	maximum = 0.0396667 (at node 67)
Accepted packet rate average = 0.0160799
	minimum = 0.011 (at node 36)
	maximum = 0.0223333 (at node 157)
Injected flit rate average = 0.564382
	minimum = 0.435667 (at node 15)
	maximum = 0.715667 (at node 67)
Accepted flit rate average= 0.289304
	minimum = 0.198333 (at node 36)
	maximum = 0.402667 (at node 157)
Injected packet length average = 17.9982
Accepted packet length average = 17.9917
Total in-flight flits = 318856 (297058 measured)
latency change    = 0.750775
throughput change = 0.00112819
Class 0:
Packet latency average = 1517.75
	minimum = 22
	maximum = 3987
Network latency average = 1507.48
	minimum = 22
	maximum = 3964
Slowest packet = 17936
Flit latency average = 2243.18
	minimum = 5
	maximum = 4380
Slowest flit = 262673
Fragmentation average = 15.416
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0313737
	minimum = 0.024 (at node 15)
	maximum = 0.038 (at node 67)
Accepted packet rate average = 0.0161237
	minimum = 0.012 (at node 161)
	maximum = 0.021 (at node 157)
Injected flit rate average = 0.564616
	minimum = 0.4315 (at node 15)
	maximum = 0.68525 (at node 67)
Accepted flit rate average= 0.290171
	minimum = 0.216 (at node 161)
	maximum = 0.38225 (at node 157)
Injected packet length average = 17.9965
Accepted packet length average = 17.9965
Total in-flight flits = 371238 (369783 measured)
latency change    = 0.865307
throughput change = 0.00298705
Class 0:
Packet latency average = 2368.38
	minimum = 22
	maximum = 4896
Network latency average = 2357.54
	minimum = 22
	maximum = 4854
Slowest packet = 18615
Flit latency average = 2464.07
	minimum = 5
	maximum = 4904
Slowest flit = 306503
Fragmentation average = 20.2202
	minimum = 0
	maximum = 107
Injected packet rate average = 0.031424
	minimum = 0.0256 (at node 15)
	maximum = 0.0378 (at node 64)
Accepted packet rate average = 0.0161927
	minimum = 0.012 (at node 104)
	maximum = 0.021 (at node 103)
Injected flit rate average = 0.565514
	minimum = 0.4608 (at node 97)
	maximum = 0.6776 (at node 64)
Accepted flit rate average= 0.291396
	minimum = 0.216 (at node 104)
	maximum = 0.377 (at node 103)
Injected packet length average = 17.9963
Accepted packet length average = 17.9955
Total in-flight flits = 423645 (423645 measured)
latency change    = 0.359163
throughput change = 0.0042048
Class 0:
Packet latency average = 2832.56
	minimum = 22
	maximum = 5711
Network latency average = 2821.72
	minimum = 22
	maximum = 5676
Slowest packet = 19185
Flit latency average = 2684.9
	minimum = 5
	maximum = 5659
Slowest flit = 345330
Fragmentation average = 22.0565
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0313828
	minimum = 0.0255 (at node 97)
	maximum = 0.0365 (at node 64)
Accepted packet rate average = 0.0162023
	minimum = 0.0121667 (at node 79)
	maximum = 0.0201667 (at node 68)
Injected flit rate average = 0.564931
	minimum = 0.459 (at node 97)
	maximum = 0.6545 (at node 64)
Accepted flit rate average= 0.291625
	minimum = 0.216333 (at node 80)
	maximum = 0.3625 (at node 68)
Injected packet length average = 18.0013
Accepted packet length average = 17.999
Total in-flight flits = 475181 (475181 measured)
latency change    = 0.163872
throughput change = 0.000785827
Class 0:
Packet latency average = 3191.88
	minimum = 22
	maximum = 6262
Network latency average = 3181.14
	minimum = 22
	maximum = 6190
Slowest packet = 22589
Flit latency average = 2911.2
	minimum = 5
	maximum = 6173
Slowest flit = 406619
Fragmentation average = 23.2417
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0313006
	minimum = 0.0252857 (at node 15)
	maximum = 0.036 (at node 64)
Accepted packet rate average = 0.0161749
	minimum = 0.0125714 (at node 80)
	maximum = 0.0202857 (at node 40)
Injected flit rate average = 0.563334
	minimum = 0.455143 (at node 97)
	maximum = 0.648 (at node 64)
Accepted flit rate average= 0.291065
	minimum = 0.224571 (at node 80)
	maximum = 0.364 (at node 40)
Injected packet length average = 17.9976
Accepted packet length average = 17.9949
Total in-flight flits = 526411 (526411 measured)
latency change    = 0.112574
throughput change = 0.00192233
Draining all recorded packets ...
Class 0:
Remaining flits: 457992 457993 457994 457995 457996 457997 457998 457999 458000 458001 [...] (577251 flits)
Measured flits: 457992 457993 457994 457995 457996 457997 457998 457999 458000 458001 [...] (480564 flits)
Class 0:
Remaining flits: 509706 509707 509708 509709 509710 509711 509712 509713 509714 509715 [...] (624705 flits)
Measured flits: 509706 509707 509708 509709 509710 509711 509712 509713 509714 509715 [...] (433305 flits)
Class 0:
Remaining flits: 560160 560161 560162 560163 560164 560165 560166 560167 560168 560169 [...] (673871 flits)
Measured flits: 560160 560161 560162 560163 560164 560165 560166 560167 560168 560169 [...] (386612 flits)
Class 0:
Remaining flits: 607777 607778 607779 607780 607781 607782 607783 607784 607785 607786 [...] (718868 flits)
Measured flits: 607777 607778 607779 607780 607781 607782 607783 607784 607785 607786 [...] (339624 flits)
Class 0:
Remaining flits: 658350 658351 658352 658353 658354 658355 658356 658357 658358 658359 [...] (762689 flits)
Measured flits: 658350 658351 658352 658353 658354 658355 658356 658357 658358 658359 [...] (292255 flits)
Class 0:
Remaining flits: 685404 685405 685406 685407 685408 685409 685410 685411 685412 685413 [...] (805718 flits)
Measured flits: 685404 685405 685406 685407 685408 685409 685410 685411 685412 685413 [...] (245084 flits)
Class 0:
Remaining flits: 723726 723727 723728 723729 723730 723731 723732 723733 723734 723735 [...] (844974 flits)
Measured flits: 723726 723727 723728 723729 723730 723731 723732 723733 723734 723735 [...] (197883 flits)
Class 0:
Remaining flits: 803844 803845 803846 803847 803848 803849 803850 803851 803852 803853 [...] (887920 flits)
Measured flits: 803844 803845 803846 803847 803848 803849 803850 803851 803852 803853 [...] (150229 flits)
Class 0:
Remaining flits: 853685 860255 860580 860581 860582 860583 860584 860585 860586 860587 [...] (931005 flits)
Measured flits: 853685 860255 860580 860581 860582 860583 860584 860585 860586 860587 [...] (102905 flits)
Class 0:
Remaining flits: 899496 899497 899498 899499 899500 899501 899502 899503 899504 899505 [...] (968686 flits)
Measured flits: 899496 899497 899498 899499 899500 899501 899502 899503 899504 899505 [...] (58634 flits)
Class 0:
Remaining flits: 923017 923018 923019 923020 923021 937314 937315 937316 937317 937318 [...] (1001581 flits)
Measured flits: 923017 923018 923019 923020 923021 937314 937315 937316 937317 937318 [...] (26681 flits)
Class 0:
Remaining flits: 957762 957763 957764 957765 957766 957767 957768 957769 957770 957771 [...] (1027572 flits)
Measured flits: 957762 957763 957764 957765 957766 957767 957768 957769 957770 957771 [...] (9806 flits)
Class 0:
Remaining flits: 980226 980227 980228 980229 980230 980231 980232 980233 980234 980235 [...] (1051156 flits)
Measured flits: 980226 980227 980228 980229 980230 980231 980232 980233 980234 980235 [...] (3234 flits)
Class 0:
Remaining flits: 995256 995257 995258 995259 995260 995261 995262 995263 995264 995265 [...] (1062363 flits)
Measured flits: 995256 995257 995258 995259 995260 995261 995262 995263 995264 995265 [...] (801 flits)
Class 0:
Remaining flits: 1048462 1048463 1050714 1050715 1050716 1050717 1050718 1050719 1050720 1050721 [...] (1066782 flits)
Measured flits: 1048462 1048463 1050714 1050715 1050716 1050717 1050718 1050719 1050720 1050721 [...] (236 flits)
Class 0:
Remaining flits: 1052934 1052935 1052936 1052937 1052938 1052939 1052940 1052941 1052942 1052943 [...] (1075662 flits)
Measured flits: 1052934 1052935 1052936 1052937 1052938 1052939 1052940 1052941 1052942 1052943 [...] (12 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1130814 1130815 1130816 1130817 1130818 1130819 1130820 1130821 1130822 1130823 [...] (1026161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1133046 1133047 1133048 1133049 1133050 1133051 1133052 1133053 1133054 1133055 [...] (975580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1186074 1186075 1186076 1186077 1186078 1186079 1186080 1186081 1186082 1186083 [...] (925167 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1208088 1208089 1208090 1208091 1208092 1208093 1208094 1208095 1208096 1208097 [...] (875103 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263672 1263673 1263674 1263675 1263676 1263677 1263678 1263679 1263680 1263681 [...] (824930 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284966 1284967 1284968 1284969 1284970 1284971 1284972 1284973 1284974 1284975 [...] (773961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1325898 1325899 1325900 1325901 1325902 1325903 1325904 1325905 1325906 1325907 [...] (723096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1325898 1325899 1325900 1325901 1325902 1325903 1325904 1325905 1325906 1325907 [...] (673017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1380960 1380961 1380962 1380963 1380964 1380965 1380966 1380967 1380968 1380969 [...] (623465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397034 1397035 1397036 1397037 1397038 1397039 1397040 1397041 1397042 1397043 [...] (574544 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1445292 1445293 1445294 1445295 1445296 1445297 1445298 1445299 1445300 1445301 [...] (526517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1477944 1477945 1477946 1477947 1477948 1477949 1477950 1477951 1477952 1477953 [...] (478540 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1488960 1488961 1488962 1488963 1488964 1488965 1488966 1488967 1488968 1488969 [...] (430554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1545732 1545733 1545734 1545735 1545736 1545737 1545738 1545739 1545740 1545741 [...] (383095 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1583250 1583251 1583252 1583253 1583254 1583255 1583256 1583257 1583258 1583259 [...] (335710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1647558 1647559 1647560 1647561 1647562 1647563 1647564 1647565 1647566 1647567 [...] (288405 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702548 1702549 1702550 1702551 1702552 1702553 1702554 1702555 1702556 1702557 [...] (240419 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1768662 1768663 1768664 1768665 1768666 1768667 1768668 1768669 1768670 1768671 [...] (192777 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1848762 1848763 1848764 1848765 1848766 1848767 1848768 1848769 1848770 1848771 [...] (144642 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1914120 1914121 1914122 1914123 1914124 1914125 1914126 1914127 1914128 1914129 [...] (97194 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1962741 1962742 1962743 1962744 1962745 1962746 1962747 1962748 1962749 1962750 [...] (49866 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2127906 2127907 2127908 2127909 2127910 2127911 2127912 2127913 2127914 2127915 [...] (14373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2192418 2192419 2192420 2192421 2192422 2192423 2192424 2192425 2192426 2192427 [...] (851 flits)
Measured flits: (0 flits)
Time taken is 49471 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6780.91 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16299 (1 samples)
Network latency average = 6770.16 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16283 (1 samples)
Flit latency average = 12140.6 (1 samples)
	minimum = 5 (1 samples)
	maximum = 28092 (1 samples)
Fragmentation average = 28.0921 (1 samples)
	minimum = 0 (1 samples)
	maximum = 647 (1 samples)
Injected packet rate average = 0.0313006 (1 samples)
	minimum = 0.0252857 (1 samples)
	maximum = 0.036 (1 samples)
Accepted packet rate average = 0.0161749 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.0202857 (1 samples)
Injected flit rate average = 0.563334 (1 samples)
	minimum = 0.455143 (1 samples)
	maximum = 0.648 (1 samples)
Accepted flit rate average = 0.291065 (1 samples)
	minimum = 0.224571 (1 samples)
	maximum = 0.364 (1 samples)
Injected packet size average = 17.9976 (1 samples)
Accepted packet size average = 17.9949 (1 samples)
Hops average = 5.07391 (1 samples)
Total run time 43.2425
