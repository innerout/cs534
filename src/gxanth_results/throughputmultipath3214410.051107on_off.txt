BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 374.889
	minimum = 23
	maximum = 981
Network latency average = 273.31
	minimum = 23
	maximum = 959
Slowest packet = 46
Flit latency average = 210.039
	minimum = 6
	maximum = 950
Slowest flit = 3987
Fragmentation average = 142.889
	minimum = 0
	maximum = 885
Injected packet rate average = 0.0297969
	minimum = 0.001 (at node 182)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0104219
	minimum = 0.003 (at node 41)
	maximum = 0.021 (at node 88)
Injected flit rate average = 0.531026
	minimum = 0.018 (at node 182)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.21401
	minimum = 0.085 (at node 172)
	maximum = 0.402 (at node 88)
Injected packet length average = 17.8215
Accepted packet length average = 20.5347
Total in-flight flits = 61888 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 644.675
	minimum = 23
	maximum = 1987
Network latency average = 505.209
	minimum = 23
	maximum = 1838
Slowest packet = 46
Flit latency average = 431.123
	minimum = 6
	maximum = 1971
Slowest flit = 2885
Fragmentation average = 181.075
	minimum = 0
	maximum = 1619
Injected packet rate average = 0.0307995
	minimum = 0.006 (at node 12)
	maximum = 0.056 (at node 118)
Accepted packet rate average = 0.0116615
	minimum = 0.007 (at node 30)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.55144
	minimum = 0.1025 (at node 52)
	maximum = 1 (at node 118)
Accepted flit rate average= 0.224086
	minimum = 0.137 (at node 138)
	maximum = 0.321 (at node 152)
Injected packet length average = 17.9042
Accepted packet length average = 19.2159
Total in-flight flits = 126873 (0 measured)
latency change    = 0.418484
throughput change = 0.0449628
Class 0:
Packet latency average = 1497.67
	minimum = 29
	maximum = 2966
Network latency average = 1272.08
	minimum = 27
	maximum = 2729
Slowest packet = 160
Flit latency average = 1213.52
	minimum = 6
	maximum = 2712
Slowest flit = 10655
Fragmentation average = 224.165
	minimum = 0
	maximum = 2135
Injected packet rate average = 0.029776
	minimum = 0 (at node 34)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0125052
	minimum = 0.004 (at node 20)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.536062
	minimum = 0 (at node 34)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.222214
	minimum = 0.042 (at node 20)
	maximum = 0.403 (at node 16)
Injected packet length average = 18.0031
Accepted packet length average = 17.7697
Total in-flight flits = 187168 (0 measured)
latency change    = 0.569548
throughput change = 0.00842611
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 592.03
	minimum = 23
	maximum = 3151
Network latency average = 173.043
	minimum = 23
	maximum = 940
Slowest packet = 17657
Flit latency average = 1783.65
	minimum = 6
	maximum = 3733
Slowest flit = 22479
Fragmentation average = 43.2974
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0270833
	minimum = 0 (at node 19)
	maximum = 0.056 (at node 53)
Accepted packet rate average = 0.0120313
	minimum = 0.004 (at node 57)
	maximum = 0.022 (at node 14)
Injected flit rate average = 0.487443
	minimum = 0 (at node 19)
	maximum = 1 (at node 42)
Accepted flit rate average= 0.217177
	minimum = 0.061 (at node 118)
	maximum = 0.388 (at node 75)
Injected packet length average = 17.9979
Accepted packet length average = 18.0511
Total in-flight flits = 239052 (89185 measured)
latency change    = 1.52972
throughput change = 0.0231906
Class 0:
Packet latency average = 953.157
	minimum = 23
	maximum = 3557
Network latency average = 480.035
	minimum = 23
	maximum = 1868
Slowest packet = 17657
Flit latency average = 2053.09
	minimum = 6
	maximum = 4737
Slowest flit = 23032
Fragmentation average = 69.1987
	minimum = 0
	maximum = 592
Injected packet rate average = 0.0248229
	minimum = 0 (at node 146)
	maximum = 0.0555 (at node 173)
Accepted packet rate average = 0.0119844
	minimum = 0.005 (at node 113)
	maximum = 0.0195 (at node 181)
Injected flit rate average = 0.446852
	minimum = 0 (at node 146)
	maximum = 1 (at node 173)
Accepted flit rate average= 0.215802
	minimum = 0.0825 (at node 113)
	maximum = 0.346 (at node 181)
Injected packet length average = 18.0016
Accepted packet length average = 18.007
Total in-flight flits = 275876 (159610 measured)
latency change    = 0.378875
throughput change = 0.00637158
Class 0:
Packet latency average = 1612.53
	minimum = 23
	maximum = 5181
Network latency average = 1014.89
	minimum = 23
	maximum = 2950
Slowest packet = 17657
Flit latency average = 2327.47
	minimum = 6
	maximum = 5540
Slowest flit = 42535
Fragmentation average = 105.951
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.0231944
	minimum = 0.007 (at node 146)
	maximum = 0.0446667 (at node 119)
Accepted packet rate average = 0.0119167
	minimum = 0.007 (at node 84)
	maximum = 0.0176667 (at node 181)
Injected flit rate average = 0.417472
	minimum = 0.126 (at node 146)
	maximum = 0.803333 (at node 119)
Accepted flit rate average= 0.214618
	minimum = 0.126667 (at node 84)
	maximum = 0.312667 (at node 181)
Injected packet length average = 17.9988
Accepted packet length average = 18.0099
Total in-flight flits = 304136 (215957 measured)
latency change    = 0.408907
throughput change = 0.00551691
Draining remaining packets ...
Class 0:
Remaining flits: 36845 38067 38068 38069 44028 44029 44030 44031 44032 44033 [...] (265620 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (199767 flits)
Class 0:
Remaining flits: 44031 44032 44033 44034 44035 44036 44037 44038 44039 44040 [...] (228358 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (179671 flits)
Class 0:
Remaining flits: 56844 56845 56846 56847 56848 56849 56850 56851 56852 56853 [...] (192328 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (157005 flits)
Class 0:
Remaining flits: 56844 56845 56846 56847 56848 56849 56850 56851 56852 56853 [...] (157727 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (132742 flits)
Class 0:
Remaining flits: 86940 86941 86942 86943 86944 86945 86946 86947 86948 86949 [...] (123858 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (107499 flits)
Class 0:
Remaining flits: 86940 86941 86942 86943 86944 86945 86946 86947 86948 86949 [...] (90667 flits)
Measured flits: 315990 315991 315992 315993 315994 315995 315996 315997 315998 315999 [...] (80422 flits)
Class 0:
Remaining flits: 86940 86941 86942 86943 86944 86945 86946 86947 86948 86949 [...] (58452 flits)
Measured flits: 316458 316459 316460 316461 316462 316463 316464 316465 316466 316467 [...] (52587 flits)
Class 0:
Remaining flits: 86956 86957 127026 127027 127028 127029 127030 127031 127032 127033 [...] (28965 flits)
Measured flits: 316566 316567 316568 316569 316570 316571 316572 316573 316574 316575 [...] (26267 flits)
Class 0:
Remaining flits: 183546 183547 183548 183549 183550 183551 183552 183553 183554 183555 [...] (7068 flits)
Measured flits: 316571 316572 316573 316574 316575 316576 316577 316578 316579 316580 [...] (6451 flits)
Time taken is 15917 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6663.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14290 (1 samples)
Network latency average = 5951.13 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12698 (1 samples)
Flit latency average = 5260.38 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13958 (1 samples)
Fragmentation average = 171.353 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2502 (1 samples)
Injected packet rate average = 0.0231944 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0446667 (1 samples)
Accepted packet rate average = 0.0119167 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.417472 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.803333 (1 samples)
Accepted flit rate average = 0.214618 (1 samples)
	minimum = 0.126667 (1 samples)
	maximum = 0.312667 (1 samples)
Injected packet size average = 17.9988 (1 samples)
Accepted packet size average = 18.0099 (1 samples)
Hops average = 5.17118 (1 samples)
Total run time 17.3715
