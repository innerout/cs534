BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 220.368
	minimum = 23
	maximum = 915
Network latency average = 217
	minimum = 23
	maximum = 915
Slowest packet = 107
Flit latency average = 141.148
	minimum = 6
	maximum = 898
Slowest flit = 1943
Fragmentation average = 138.362
	minimum = 0
	maximum = 640
Injected packet rate average = 0.0157813
	minimum = 0.003 (at node 110)
	maximum = 0.027 (at node 27)
Accepted packet rate average = 0.00931771
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 65)
Injected flit rate average = 0.281208
	minimum = 0.054 (at node 110)
	maximum = 0.47 (at node 27)
Accepted flit rate average= 0.192208
	minimum = 0.075 (at node 115)
	maximum = 0.338 (at node 44)
Injected packet length average = 17.8191
Accepted packet length average = 20.6283
Total in-flight flits = 17636 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 329.347
	minimum = 23
	maximum = 1847
Network latency average = 323.517
	minimum = 23
	maximum = 1836
Slowest packet = 381
Flit latency average = 227.887
	minimum = 6
	maximum = 1872
Slowest flit = 4137
Fragmentation average = 184.521
	minimum = 0
	maximum = 1791
Injected packet rate average = 0.0153255
	minimum = 0.008 (at node 4)
	maximum = 0.0215 (at node 34)
Accepted packet rate average = 0.0105443
	minimum = 0.005 (at node 62)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.274547
	minimum = 0.139 (at node 4)
	maximum = 0.387 (at node 70)
Accepted flit rate average= 0.20499
	minimum = 0.099 (at node 62)
	maximum = 0.305 (at node 44)
Injected packet length average = 17.9144
Accepted packet length average = 19.4408
Total in-flight flits = 27268 (0 measured)
latency change    = 0.330896
throughput change = 0.0623507
Class 0:
Packet latency average = 561.667
	minimum = 24
	maximum = 2630
Network latency average = 531.452
	minimum = 24
	maximum = 2630
Slowest packet = 946
Flit latency average = 420.972
	minimum = 6
	maximum = 2751
Slowest flit = 13378
Fragmentation average = 250.059
	minimum = 0
	maximum = 1962
Injected packet rate average = 0.0134948
	minimum = 0 (at node 12)
	maximum = 0.025 (at node 50)
Accepted packet rate average = 0.0121042
	minimum = 0.004 (at node 38)
	maximum = 0.02 (at node 102)
Injected flit rate average = 0.241771
	minimum = 0 (at node 12)
	maximum = 0.45 (at node 50)
Accepted flit rate average= 0.219036
	minimum = 0.081 (at node 135)
	maximum = 0.358 (at node 73)
Injected packet length average = 17.9159
Accepted packet length average = 18.096
Total in-flight flits = 31869 (0 measured)
latency change    = 0.413625
throughput change = 0.0641303
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 434.135
	minimum = 25
	maximum = 1919
Network latency average = 301.726
	minimum = 25
	maximum = 970
Slowest packet = 8480
Flit latency average = 555.626
	minimum = 6
	maximum = 3538
Slowest flit = 13391
Fragmentation average = 168.11
	minimum = 0
	maximum = 648
Injected packet rate average = 0.0138021
	minimum = 0 (at node 9)
	maximum = 0.033 (at node 129)
Accepted packet rate average = 0.0119635
	minimum = 0.004 (at node 17)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.247526
	minimum = 0 (at node 9)
	maximum = 0.597 (at node 129)
Accepted flit rate average= 0.217745
	minimum = 0.082 (at node 17)
	maximum = 0.383 (at node 16)
Injected packet length average = 17.934
Accepted packet length average = 18.2007
Total in-flight flits = 37834 (23755 measured)
latency change    = 0.293762
throughput change = 0.00593202
Class 0:
Packet latency average = 611.425
	minimum = 25
	maximum = 3095
Network latency average = 442.547
	minimum = 25
	maximum = 1879
Slowest packet = 8480
Flit latency average = 619.223
	minimum = 6
	maximum = 4322
Slowest flit = 27485
Fragmentation average = 200.345
	minimum = 0
	maximum = 1686
Injected packet rate average = 0.0128984
	minimum = 0 (at node 9)
	maximum = 0.0235 (at node 55)
Accepted packet rate average = 0.0119714
	minimum = 0.006 (at node 4)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.23082
	minimum = 0 (at node 9)
	maximum = 0.418 (at node 55)
Accepted flit rate average= 0.217411
	minimum = 0.0885 (at node 36)
	maximum = 0.3565 (at node 128)
Injected packet length average = 17.8952
Accepted packet length average = 18.161
Total in-flight flits = 37573 (29211 measured)
latency change    = 0.289962
throughput change = 0.00153319
Class 0:
Packet latency average = 779.856
	minimum = 24
	maximum = 4054
Network latency average = 533.73
	minimum = 24
	maximum = 2755
Slowest packet = 8480
Flit latency average = 658.507
	minimum = 6
	maximum = 5715
Slowest flit = 14877
Fragmentation average = 216.854
	minimum = 0
	maximum = 2307
Injected packet rate average = 0.0124913
	minimum = 0 (at node 76)
	maximum = 0.022 (at node 189)
Accepted packet rate average = 0.0118663
	minimum = 0.007 (at node 36)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.22424
	minimum = 0.001 (at node 76)
	maximum = 0.397 (at node 189)
Accepted flit rate average= 0.214441
	minimum = 0.117333 (at node 36)
	maximum = 0.341333 (at node 128)
Injected packet length average = 17.9516
Accepted packet length average = 18.0714
Total in-flight flits = 37969 (32375 measured)
latency change    = 0.215977
throughput change = 0.0138522
Draining remaining packets ...
Class 0:
Remaining flits: 4138 4139 30528 30529 30530 30531 30532 30533 30534 30535 [...] (6987 flits)
Measured flits: 152946 152947 152948 152949 152950 152951 152952 152953 152954 152955 [...] (5633 flits)
Time taken is 7988 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1123.87 (1 samples)
	minimum = 24 (1 samples)
	maximum = 5531 (1 samples)
Network latency average = 823.633 (1 samples)
	minimum = 24 (1 samples)
	maximum = 4424 (1 samples)
Flit latency average = 963.729 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7115 (1 samples)
Fragmentation average = 226.454 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2924 (1 samples)
Injected packet rate average = 0.0124913 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0118663 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.22424 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.397 (1 samples)
Accepted flit rate average = 0.214441 (1 samples)
	minimum = 0.117333 (1 samples)
	maximum = 0.341333 (1 samples)
Injected packet size average = 17.9516 (1 samples)
Accepted packet size average = 18.0714 (1 samples)
Hops average = 5.13651 (1 samples)
Total run time 8.80184
