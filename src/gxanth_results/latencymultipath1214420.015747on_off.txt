BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.08
	minimum = 22
	maximum = 873
Network latency average = 150.635
	minimum = 22
	maximum = 680
Slowest packet = 70
Flit latency average = 127.143
	minimum = 5
	maximum = 663
Slowest flit = 16937
Fragmentation average = 14.6005
	minimum = 0
	maximum = 76
Injected packet rate average = 0.0157917
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117865
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282193
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.216839
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8697
Accepted packet length average = 18.3973
Total in-flight flits = 12943 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 295.003
	minimum = 22
	maximum = 1501
Network latency average = 223.7
	minimum = 22
	maximum = 1058
Slowest packet = 70
Flit latency average = 199.243
	minimum = 5
	maximum = 1041
Slowest flit = 48474
Fragmentation average = 15.4663
	minimum = 0
	maximum = 85
Injected packet rate average = 0.0150938
	minimum = 0.0015 (at node 55)
	maximum = 0.0395 (at node 43)
Accepted packet rate average = 0.0124974
	minimum = 0.0065 (at node 116)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.270372
	minimum = 0.027 (at node 55)
	maximum = 0.711 (at node 43)
Accepted flit rate average= 0.227057
	minimum = 0.1195 (at node 116)
	maximum = 0.342 (at node 152)
Injected packet length average = 17.9129
Accepted packet length average = 18.1684
Total in-flight flits = 17138 (0 measured)
latency change    = 0.277705
throughput change = 0.0450052
Class 0:
Packet latency average = 471.819
	minimum = 25
	maximum = 1777
Network latency average = 390.086
	minimum = 22
	maximum = 1613
Slowest packet = 3273
Flit latency average = 367.552
	minimum = 5
	maximum = 1596
Slowest flit = 68345
Fragmentation average = 16.4674
	minimum = 0
	maximum = 79
Injected packet rate average = 0.016375
	minimum = 0 (at node 51)
	maximum = 0.044 (at node 177)
Accepted packet rate average = 0.0137292
	minimum = 0.004 (at node 184)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.294896
	minimum = 0 (at node 51)
	maximum = 0.797 (at node 177)
Accepted flit rate average= 0.247214
	minimum = 0.072 (at node 184)
	maximum = 0.433 (at node 16)
Injected packet length average = 18.0089
Accepted packet length average = 18.0064
Total in-flight flits = 26265 (0 measured)
latency change    = 0.374753
throughput change = 0.0815338
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 355.346
	minimum = 25
	maximum = 1116
Network latency average = 291.901
	minimum = 22
	maximum = 990
Slowest packet = 8995
Flit latency average = 478.305
	minimum = 5
	maximum = 1804
Slowest flit = 76768
Fragmentation average = 14.762
	minimum = 0
	maximum = 81
Injected packet rate average = 0.0150885
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 121)
Accepted packet rate average = 0.0141667
	minimum = 0.006 (at node 48)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.272141
	minimum = 0 (at node 10)
	maximum = 1 (at node 121)
Accepted flit rate average= 0.255047
	minimum = 0.094 (at node 48)
	maximum = 0.464 (at node 129)
Injected packet length average = 18.0362
Accepted packet length average = 18.0033
Total in-flight flits = 29478 (27858 measured)
latency change    = 0.327772
throughput change = 0.0307133
Class 0:
Packet latency average = 529.587
	minimum = 22
	maximum = 2109
Network latency average = 455.449
	minimum = 22
	maximum = 1988
Slowest packet = 8973
Flit latency average = 511.018
	minimum = 5
	maximum = 2358
Slowest flit = 125081
Fragmentation average = 16.3533
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0149714
	minimum = 0.0005 (at node 67)
	maximum = 0.036 (at node 57)
Accepted packet rate average = 0.0142552
	minimum = 0.008 (at node 132)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.269763
	minimum = 0.009 (at node 67)
	maximum = 0.642 (at node 57)
Accepted flit rate average= 0.256643
	minimum = 0.144 (at node 132)
	maximum = 0.441 (at node 129)
Injected packet length average = 18.0186
Accepted packet length average = 18.0035
Total in-flight flits = 31214 (31178 measured)
latency change    = 0.329012
throughput change = 0.00622013
Class 0:
Packet latency average = 616.159
	minimum = 22
	maximum = 2846
Network latency average = 536.978
	minimum = 22
	maximum = 2560
Slowest packet = 8973
Flit latency average = 551.188
	minimum = 5
	maximum = 2956
Slowest flit = 137446
Fragmentation average = 16.3688
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0146858
	minimum = 0.003 (at node 133)
	maximum = 0.0306667 (at node 121)
Accepted packet rate average = 0.0142552
	minimum = 0.009 (at node 132)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.264517
	minimum = 0.051 (at node 133)
	maximum = 0.553667 (at node 121)
Accepted flit rate average= 0.256472
	minimum = 0.162 (at node 132)
	maximum = 0.373 (at node 128)
Injected packet length average = 18.0118
Accepted packet length average = 17.9915
Total in-flight flits = 30835 (30835 measured)
latency change    = 0.140503
throughput change = 0.000666766
Class 0:
Packet latency average = 650.131
	minimum = 22
	maximum = 3284
Network latency average = 570.139
	minimum = 22
	maximum = 3226
Slowest packet = 8973
Flit latency average = 569.702
	minimum = 5
	maximum = 3209
Slowest flit = 201104
Fragmentation average = 16.2391
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0149245
	minimum = 0.00375 (at node 60)
	maximum = 0.03225 (at node 73)
Accepted packet rate average = 0.0142513
	minimum = 0.01 (at node 89)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.26866
	minimum = 0.0675 (at node 60)
	maximum = 0.58425 (at node 73)
Accepted flit rate average= 0.256525
	minimum = 0.18 (at node 89)
	maximum = 0.36 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 18.0001
Total in-flight flits = 35624 (35624 measured)
latency change    = 0.0522543
throughput change = 0.000204726
Class 0:
Packet latency average = 676.308
	minimum = 22
	maximum = 3729
Network latency average = 594.317
	minimum = 22
	maximum = 3600
Slowest packet = 11794
Flit latency average = 586.544
	minimum = 5
	maximum = 3583
Slowest flit = 212303
Fragmentation average = 16.5298
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0150365
	minimum = 0.0036 (at node 60)
	maximum = 0.0286 (at node 73)
Accepted packet rate average = 0.0142875
	minimum = 0.011 (at node 162)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.270689
	minimum = 0.0648 (at node 60)
	maximum = 0.5158 (at node 73)
Accepted flit rate average= 0.257233
	minimum = 0.198 (at node 162)
	maximum = 0.333 (at node 128)
Injected packet length average = 18.0021
Accepted packet length average = 18.0041
Total in-flight flits = 39241 (39241 measured)
latency change    = 0.0387061
throughput change = 0.00275467
Class 0:
Packet latency average = 705.634
	minimum = 22
	maximum = 3729
Network latency average = 622.05
	minimum = 22
	maximum = 3600
Slowest packet = 11794
Flit latency average = 608.33
	minimum = 5
	maximum = 3583
Slowest flit = 212303
Fragmentation average = 16.794
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0150295
	minimum = 0.00533333 (at node 60)
	maximum = 0.0278333 (at node 73)
Accepted packet rate average = 0.0143125
	minimum = 0.011 (at node 86)
	maximum = 0.0181667 (at node 128)
Injected flit rate average = 0.270523
	minimum = 0.096 (at node 60)
	maximum = 0.5035 (at node 73)
Accepted flit rate average= 0.257653
	minimum = 0.199 (at node 86)
	maximum = 0.326 (at node 128)
Injected packet length average = 17.9994
Accepted packet length average = 18.0019
Total in-flight flits = 41227 (41227 measured)
latency change    = 0.0415592
throughput change = 0.00162794
Class 0:
Packet latency average = 735.394
	minimum = 22
	maximum = 3895
Network latency average = 649.215
	minimum = 22
	maximum = 3600
Slowest packet = 11794
Flit latency average = 632.016
	minimum = 5
	maximum = 3583
Slowest flit = 212303
Fragmentation average = 16.7797
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0150692
	minimum = 0.00542857 (at node 60)
	maximum = 0.0272857 (at node 23)
Accepted packet rate average = 0.0143095
	minimum = 0.0114286 (at node 80)
	maximum = 0.0177143 (at node 157)
Injected flit rate average = 0.271246
	minimum = 0.0977143 (at node 60)
	maximum = 0.491143 (at node 23)
Accepted flit rate average= 0.257648
	minimum = 0.205714 (at node 80)
	maximum = 0.318857 (at node 157)
Injected packet length average = 18
Accepted packet length average = 18.0054
Total in-flight flits = 44702 (44702 measured)
latency change    = 0.0404676
throughput change = 1.82897e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 405342 405343 405344 405345 405346 405347 405348 405349 405350 405351 [...] (49356 flits)
Measured flits: 405342 405343 405344 405345 405346 405347 405348 405349 405350 405351 [...] (14266 flits)
Class 0:
Remaining flits: 432648 432649 432650 432651 432652 432653 432654 432655 432656 432657 [...] (56596 flits)
Measured flits: 432648 432649 432650 432651 432652 432653 432654 432655 432656 432657 [...] (3853 flits)
Class 0:
Remaining flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (53836 flits)
Measured flits: 465498 465499 465500 465501 465502 465503 465504 465505 465506 465507 [...] (1284 flits)
Class 0:
Remaining flits: 520560 520561 520562 520563 520564 520565 520566 520567 520568 520569 [...] (58910 flits)
Measured flits: 520560 520561 520562 520563 520564 520565 520566 520567 520568 520569 [...] (338 flits)
Class 0:
Remaining flits: 594522 594523 594524 594525 594526 594527 594528 594529 594530 594531 [...] (62105 flits)
Measured flits: 594522 594523 594524 594525 594526 594527 594528 594529 594530 594531 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 624553 624554 624555 624556 624557 624558 624559 624560 624561 624562 [...] (21982 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 715428 715429 715430 715431 715432 715433 715434 715435 715436 715437 [...] (2599 flits)
Measured flits: (0 flits)
Time taken is 18206 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 859.631 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5906 (1 samples)
Network latency average = 753.824 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4625 (1 samples)
Flit latency average = 868.039 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4629 (1 samples)
Fragmentation average = 17.0107 (1 samples)
	minimum = 0 (1 samples)
	maximum = 125 (1 samples)
Injected packet rate average = 0.0150692 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.0272857 (1 samples)
Accepted packet rate average = 0.0143095 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0177143 (1 samples)
Injected flit rate average = 0.271246 (1 samples)
	minimum = 0.0977143 (1 samples)
	maximum = 0.491143 (1 samples)
Accepted flit rate average = 0.257648 (1 samples)
	minimum = 0.205714 (1 samples)
	maximum = 0.318857 (1 samples)
Injected packet size average = 18 (1 samples)
Accepted packet size average = 18.0054 (1 samples)
Hops average = 5.07445 (1 samples)
Total run time 11.084
