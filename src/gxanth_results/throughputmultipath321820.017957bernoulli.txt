BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 170.651
	minimum = 22
	maximum = 615
Network latency average = 167.044
	minimum = 22
	maximum = 615
Slowest packet = 675
Flit latency average = 135.909
	minimum = 5
	maximum = 610
Slowest flit = 21399
Fragmentation average = 34.594
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0179479
	minimum = 0.008 (at node 50)
	maximum = 0.032 (at node 9)
Accepted packet rate average = 0.0129948
	minimum = 0.005 (at node 93)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319365
	minimum = 0.144 (at node 50)
	maximum = 0.576 (at node 9)
Accepted flit rate average= 0.24263
	minimum = 0.092 (at node 174)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.794
Accepted packet length average = 18.6713
Total in-flight flits = 15461 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 264.776
	minimum = 22
	maximum = 1129
Network latency average = 260.665
	minimum = 22
	maximum = 1110
Slowest packet = 1279
Flit latency average = 225.864
	minimum = 5
	maximum = 1093
Slowest flit = 23039
Fragmentation average = 40.9945
	minimum = 0
	maximum = 257
Injected packet rate average = 0.017651
	minimum = 0.011 (at node 24)
	maximum = 0.026 (at node 83)
Accepted packet rate average = 0.0138073
	minimum = 0.008 (at node 116)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.316568
	minimum = 0.198 (at node 24)
	maximum = 0.4665 (at node 83)
Accepted flit rate average= 0.253508
	minimum = 0.144 (at node 116)
	maximum = 0.358 (at node 152)
Injected packet length average = 17.9348
Accepted packet length average = 18.3604
Total in-flight flits = 24729 (0 measured)
latency change    = 0.355491
throughput change = 0.0429084
Class 0:
Packet latency average = 500.426
	minimum = 22
	maximum = 1772
Network latency average = 487.953
	minimum = 22
	maximum = 1582
Slowest packet = 4222
Flit latency average = 447.283
	minimum = 5
	maximum = 1524
Slowest flit = 50885
Fragmentation average = 58.8532
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0169531
	minimum = 0.007 (at node 80)
	maximum = 0.03 (at node 35)
Accepted packet rate average = 0.0148698
	minimum = 0.007 (at node 180)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.303729
	minimum = 0.141 (at node 80)
	maximum = 0.537 (at node 35)
Accepted flit rate average= 0.269943
	minimum = 0.126 (at node 180)
	maximum = 0.473 (at node 34)
Injected packet length average = 17.9158
Accepted packet length average = 18.1538
Total in-flight flits = 31616 (0 measured)
latency change    = 0.470897
throughput change = 0.0608829
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 459.597
	minimum = 22
	maximum = 1952
Network latency average = 402.958
	minimum = 22
	maximum = 983
Slowest packet = 10055
Flit latency average = 570.654
	minimum = 5
	maximum = 2173
Slowest flit = 101933
Fragmentation average = 49.6632
	minimum = 0
	maximum = 236
Injected packet rate average = 0.016724
	minimum = 0.002 (at node 144)
	maximum = 0.027 (at node 133)
Accepted packet rate average = 0.0150729
	minimum = 0.005 (at node 4)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.300417
	minimum = 0.042 (at node 144)
	maximum = 0.486 (at node 165)
Accepted flit rate average= 0.271036
	minimum = 0.106 (at node 4)
	maximum = 0.528 (at node 19)
Injected packet length average = 17.9633
Accepted packet length average = 17.9817
Total in-flight flits = 37573 (35396 measured)
latency change    = 0.0888363
throughput change = 0.00403543
Class 0:
Packet latency average = 689.673
	minimum = 22
	maximum = 2582
Network latency average = 595.549
	minimum = 22
	maximum = 1909
Slowest packet = 10055
Flit latency average = 617.127
	minimum = 5
	maximum = 2774
Slowest flit = 134893
Fragmentation average = 63.1857
	minimum = 0
	maximum = 284
Injected packet rate average = 0.0165234
	minimum = 0.009 (at node 140)
	maximum = 0.0255 (at node 190)
Accepted packet rate average = 0.0150495
	minimum = 0.009 (at node 36)
	maximum = 0.0235 (at node 165)
Injected flit rate average = 0.297216
	minimum = 0.1585 (at node 144)
	maximum = 0.459 (at node 190)
Accepted flit rate average= 0.271516
	minimum = 0.1745 (at node 5)
	maximum = 0.414 (at node 165)
Injected packet length average = 17.9875
Accepted packet length average = 18.0415
Total in-flight flits = 41690 (41656 measured)
latency change    = 0.333602
throughput change = 0.00176478
Class 0:
Packet latency average = 823.386
	minimum = 22
	maximum = 3267
Network latency average = 688.39
	minimum = 22
	maximum = 2361
Slowest packet = 10055
Flit latency average = 662.652
	minimum = 5
	maximum = 2947
Slowest flit = 134909
Fragmentation average = 68.6092
	minimum = 0
	maximum = 312
Injected packet rate average = 0.0162257
	minimum = 0.00966667 (at node 144)
	maximum = 0.0236667 (at node 190)
Accepted packet rate average = 0.015026
	minimum = 0.009 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.291811
	minimum = 0.173 (at node 184)
	maximum = 0.422 (at node 190)
Accepted flit rate average= 0.270509
	minimum = 0.175667 (at node 36)
	maximum = 0.352333 (at node 157)
Injected packet length average = 17.9845
Accepted packet length average = 18.0027
Total in-flight flits = 44301 (44301 measured)
latency change    = 0.162395
throughput change = 0.00372241
Draining remaining packets ...
Class 0:
Remaining flits: 243936 243937 243938 243939 243940 243941 243942 243943 243944 243945 [...] (2764 flits)
Measured flits: 243936 243937 243938 243939 243940 243941 243942 243943 243944 243945 [...] (2764 flits)
Time taken is 7374 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 955.855 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3591 (1 samples)
Network latency average = 785.554 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3163 (1 samples)
Flit latency average = 733.24 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3146 (1 samples)
Fragmentation average = 70.289 (1 samples)
	minimum = 0 (1 samples)
	maximum = 339 (1 samples)
Injected packet rate average = 0.0162257 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0236667 (1 samples)
Accepted packet rate average = 0.015026 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.291811 (1 samples)
	minimum = 0.173 (1 samples)
	maximum = 0.422 (1 samples)
Accepted flit rate average = 0.270509 (1 samples)
	minimum = 0.175667 (1 samples)
	maximum = 0.352333 (1 samples)
Injected packet size average = 17.9845 (1 samples)
Accepted packet size average = 18.0027 (1 samples)
Hops average = 5.10608 (1 samples)
Total run time 7.34748
