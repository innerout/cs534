BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.853
	minimum = 22
	maximum = 977
Network latency average = 233.127
	minimum = 22
	maximum = 739
Slowest packet = 82
Flit latency average = 210.542
	minimum = 5
	maximum = 722
Slowest flit = 20411
Fragmentation average = 18.8369
	minimum = 0
	maximum = 90
Injected packet rate average = 0.0275729
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 25)
Accepted packet rate average = 0.0138281
	minimum = 0.005 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.490807
	minimum = 0 (at node 5)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.254547
	minimum = 0.098 (at node 41)
	maximum = 0.435 (at node 44)
Injected packet length average = 17.8003
Accepted packet length average = 18.4079
Total in-flight flits = 46437 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.859
	minimum = 22
	maximum = 1903
Network latency average = 444.36
	minimum = 22
	maximum = 1465
Slowest packet = 82
Flit latency average = 421.803
	minimum = 5
	maximum = 1451
Slowest flit = 49572
Fragmentation average = 20.598
	minimum = 0
	maximum = 105
Injected packet rate average = 0.027875
	minimum = 0 (at node 5)
	maximum = 0.0555 (at node 18)
Accepted packet rate average = 0.0144792
	minimum = 0.008 (at node 116)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.499927
	minimum = 0 (at node 5)
	maximum = 0.995 (at node 18)
Accepted flit rate average= 0.263305
	minimum = 0.1485 (at node 116)
	maximum = 0.3705 (at node 152)
Injected packet length average = 17.9346
Accepted packet length average = 18.1851
Total in-flight flits = 92103 (0 measured)
latency change    = 0.430574
throughput change = 0.0332611
Class 0:
Packet latency average = 1346.13
	minimum = 25
	maximum = 2510
Network latency average = 1109.98
	minimum = 22
	maximum = 2223
Slowest packet = 3593
Flit latency average = 1088.95
	minimum = 5
	maximum = 2226
Slowest flit = 70653
Fragmentation average = 20.2458
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0231094
	minimum = 0 (at node 91)
	maximum = 0.055 (at node 6)
Accepted packet rate average = 0.0152135
	minimum = 0.007 (at node 91)
	maximum = 0.027 (at node 3)
Injected flit rate average = 0.415776
	minimum = 0 (at node 91)
	maximum = 0.981 (at node 162)
Accepted flit rate average= 0.273641
	minimum = 0.109 (at node 91)
	maximum = 0.483 (at node 3)
Injected packet length average = 17.9917
Accepted packet length average = 17.9866
Total in-flight flits = 120078 (0 measured)
latency change    = 0.55141
throughput change = 0.0377719
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 616.252
	minimum = 26
	maximum = 3037
Network latency average = 69.2848
	minimum = 22
	maximum = 670
Slowest packet = 15231
Flit latency average = 1527.93
	minimum = 5
	maximum = 2911
Slowest flit = 104598
Fragmentation average = 4.83444
	minimum = 0
	maximum = 29
Injected packet rate average = 0.018724
	minimum = 0.002 (at node 179)
	maximum = 0.048 (at node 157)
Accepted packet rate average = 0.0147344
	minimum = 0.005 (at node 77)
	maximum = 0.028 (at node 0)
Injected flit rate average = 0.338068
	minimum = 0.036 (at node 179)
	maximum = 0.855 (at node 157)
Accepted flit rate average= 0.265302
	minimum = 0.085 (at node 77)
	maximum = 0.518 (at node 0)
Injected packet length average = 18.0554
Accepted packet length average = 18.0057
Total in-flight flits = 134372 (62440 measured)
latency change    = 1.18438
throughput change = 0.0314304
Class 0:
Packet latency average = 1511.82
	minimum = 22
	maximum = 4443
Network latency average = 891.013
	minimum = 22
	maximum = 1966
Slowest packet = 15231
Flit latency average = 1721.06
	minimum = 5
	maximum = 3879
Slowest flit = 107748
Fragmentation average = 12.3364
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0183021
	minimum = 0.005 (at node 113)
	maximum = 0.0375 (at node 55)
Accepted packet rate average = 0.0147995
	minimum = 0.008 (at node 86)
	maximum = 0.0225 (at node 128)
Injected flit rate average = 0.329617
	minimum = 0.09 (at node 113)
	maximum = 0.676 (at node 55)
Accepted flit rate average= 0.266258
	minimum = 0.144 (at node 86)
	maximum = 0.402 (at node 128)
Injected packet length average = 18.0098
Accepted packet length average = 17.991
Total in-flight flits = 144897 (112775 measured)
latency change    = 0.592377
throughput change = 0.00358949
Class 0:
Packet latency average = 2164.02
	minimum = 22
	maximum = 5030
Network latency average = 1472.4
	minimum = 22
	maximum = 2942
Slowest packet = 15231
Flit latency average = 1895.05
	minimum = 5
	maximum = 4326
Slowest flit = 138113
Fragmentation average = 16.191
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0175174
	minimum = 0.008 (at node 107)
	maximum = 0.0326667 (at node 9)
Accepted packet rate average = 0.0148368
	minimum = 0.01 (at node 64)
	maximum = 0.0203333 (at node 6)
Injected flit rate average = 0.315648
	minimum = 0.140667 (at node 107)
	maximum = 0.584667 (at node 9)
Accepted flit rate average= 0.267019
	minimum = 0.177667 (at node 89)
	maximum = 0.364 (at node 128)
Injected packet length average = 18.0191
Accepted packet length average = 17.9971
Total in-flight flits = 148723 (137559 measured)
latency change    = 0.301383
throughput change = 0.00285105
Class 0:
Packet latency average = 2678.92
	minimum = 22
	maximum = 5879
Network latency average = 1848.58
	minimum = 22
	maximum = 3934
Slowest packet = 15231
Flit latency average = 2044.49
	minimum = 5
	maximum = 5135
Slowest flit = 170459
Fragmentation average = 17.7681
	minimum = 0
	maximum = 146
Injected packet rate average = 0.016875
	minimum = 0.0085 (at node 138)
	maximum = 0.02925 (at node 9)
Accepted packet rate average = 0.0149258
	minimum = 0.01025 (at node 64)
	maximum = 0.01975 (at node 165)
Injected flit rate average = 0.30387
	minimum = 0.1545 (at node 138)
	maximum = 0.5225 (at node 9)
Accepted flit rate average= 0.268517
	minimum = 0.185 (at node 86)
	maximum = 0.3555 (at node 165)
Injected packet length average = 18.0071
Accepted packet length average = 17.9901
Total in-flight flits = 148073 (145976 measured)
latency change    = 0.192205
throughput change = 0.00557816
Class 0:
Packet latency average = 3137.06
	minimum = 22
	maximum = 6758
Network latency average = 2134.91
	minimum = 22
	maximum = 4815
Slowest packet = 15231
Flit latency average = 2173.71
	minimum = 5
	maximum = 5620
Slowest flit = 196109
Fragmentation average = 18.4962
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0165
	minimum = 0.009 (at node 111)
	maximum = 0.0264 (at node 9)
Accepted packet rate average = 0.0148615
	minimum = 0.0104 (at node 64)
	maximum = 0.0188 (at node 138)
Injected flit rate average = 0.297154
	minimum = 0.162 (at node 111)
	maximum = 0.475 (at node 139)
Accepted flit rate average= 0.267547
	minimum = 0.1872 (at node 64)
	maximum = 0.3416 (at node 138)
Injected packet length average = 18.0093
Accepted packet length average = 18.0027
Total in-flight flits = 149325 (149105 measured)
latency change    = 0.146041
throughput change = 0.00362573
Class 0:
Packet latency average = 3529.08
	minimum = 22
	maximum = 7643
Network latency average = 2325.71
	minimum = 22
	maximum = 5646
Slowest packet = 15231
Flit latency average = 2281.02
	minimum = 5
	maximum = 6027
Slowest flit = 197567
Fragmentation average = 18.7119
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0162326
	minimum = 0.00783333 (at node 138)
	maximum = 0.0253333 (at node 46)
Accepted packet rate average = 0.0148941
	minimum = 0.0115 (at node 171)
	maximum = 0.0185 (at node 128)
Injected flit rate average = 0.292339
	minimum = 0.142 (at node 138)
	maximum = 0.456167 (at node 46)
Accepted flit rate average= 0.267987
	minimum = 0.208667 (at node 171)
	maximum = 0.333333 (at node 128)
Injected packet length average = 18.0093
Accepted packet length average = 17.9928
Total in-flight flits = 148857 (148857 measured)
latency change    = 0.111084
throughput change = 0.00164226
Class 0:
Packet latency average = 3866.63
	minimum = 22
	maximum = 8851
Network latency average = 2430.2
	minimum = 22
	maximum = 6097
Slowest packet = 15231
Flit latency average = 2352.22
	minimum = 5
	maximum = 6073
Slowest flit = 290789
Fragmentation average = 19.0427
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0160595
	minimum = 0.009 (at node 8)
	maximum = 0.0241429 (at node 55)
Accepted packet rate average = 0.0148966
	minimum = 0.0114286 (at node 171)
	maximum = 0.0187143 (at node 138)
Injected flit rate average = 0.289122
	minimum = 0.161571 (at node 8)
	maximum = 0.435429 (at node 55)
Accepted flit rate average= 0.268165
	minimum = 0.206143 (at node 171)
	maximum = 0.336857 (at node 138)
Injected packet length average = 18.0032
Accepted packet length average = 18.0018
Total in-flight flits = 149004 (149004 measured)
latency change    = 0.0872985
throughput change = 0.000664514
Draining all recorded packets ...
Class 0:
Remaining flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (151698 flits)
Measured flits: 351882 351883 351884 351885 351886 351887 351888 351889 351890 351891 [...] (150402 flits)
Class 0:
Remaining flits: 418374 418375 418376 418377 418378 418379 418380 418381 418382 418383 [...] (150868 flits)
Measured flits: 418374 418375 418376 418377 418378 418379 418380 418381 418382 418383 [...] (147466 flits)
Class 0:
Remaining flits: 502848 502849 502850 502851 502852 502853 502854 502855 502856 502857 [...] (149305 flits)
Measured flits: 502848 502849 502850 502851 502852 502853 502854 502855 502856 502857 [...] (143494 flits)
Class 0:
Remaining flits: 537786 537787 537788 537789 537790 537791 537792 537793 537794 537795 [...] (149091 flits)
Measured flits: 537786 537787 537788 537789 537790 537791 537792 537793 537794 537795 [...] (137846 flits)
Class 0:
Remaining flits: 541890 541891 541892 541893 541894 541895 541896 541897 541898 541899 [...] (146730 flits)
Measured flits: 541890 541891 541892 541893 541894 541895 541896 541897 541898 541899 [...] (130801 flits)
Class 0:
Remaining flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (147025 flits)
Measured flits: 548172 548173 548174 548175 548176 548177 548178 548179 548180 548181 [...] (123668 flits)
Class 0:
Remaining flits: 680202 680203 680204 680205 680206 680207 680208 680209 680210 680211 [...] (145795 flits)
Measured flits: 680202 680203 680204 680205 680206 680207 680208 680209 680210 680211 [...] (115032 flits)
Class 0:
Remaining flits: 731952 731953 731954 731955 731956 731957 731958 731959 731960 731961 [...] (144400 flits)
Measured flits: 731952 731953 731954 731955 731956 731957 731958 731959 731960 731961 [...] (102397 flits)
Class 0:
Remaining flits: 767430 767431 767432 767433 767434 767435 767436 767437 767438 767439 [...] (145080 flits)
Measured flits: 767430 767431 767432 767433 767434 767435 767436 767437 767438 767439 [...] (92849 flits)
Class 0:
Remaining flits: 789498 789499 789500 789501 789502 789503 789504 789505 789506 789507 [...] (146161 flits)
Measured flits: 789498 789499 789500 789501 789502 789503 789504 789505 789506 789507 [...] (82955 flits)
Class 0:
Remaining flits: 871056 871057 871058 871059 871060 871061 871062 871063 871064 871065 [...] (147248 flits)
Measured flits: 941886 941887 941888 941889 941890 941891 941892 941893 941894 941895 [...] (73286 flits)
Class 0:
Remaining flits: 954936 954937 954938 954939 954940 954941 954942 954943 954944 954945 [...] (144083 flits)
Measured flits: 954936 954937 954938 954939 954940 954941 954942 954943 954944 954945 [...] (61151 flits)
Class 0:
Remaining flits: 1038744 1038745 1038746 1038747 1038748 1038749 1038750 1038751 1038752 1038753 [...] (143143 flits)
Measured flits: 1038744 1038745 1038746 1038747 1038748 1038749 1038750 1038751 1038752 1038753 [...] (55250 flits)
Class 0:
Remaining flits: 1055916 1055917 1055918 1055919 1055920 1055921 1055922 1055923 1055924 1055925 [...] (145616 flits)
Measured flits: 1055916 1055917 1055918 1055919 1055920 1055921 1055922 1055923 1055924 1055925 [...] (47386 flits)
Class 0:
Remaining flits: 1066662 1066663 1066664 1066665 1066666 1066667 1066668 1066669 1066670 1066671 [...] (148260 flits)
Measured flits: 1077426 1077427 1077428 1077429 1077430 1077431 1077432 1077433 1077434 1077435 [...] (37286 flits)
Class 0:
Remaining flits: 1104716 1104717 1104718 1104719 1104720 1104721 1104722 1104723 1104724 1104725 [...] (144972 flits)
Measured flits: 1104716 1104717 1104718 1104719 1104720 1104721 1104722 1104723 1104724 1104725 [...] (28222 flits)
Class 0:
Remaining flits: 1112292 1112293 1112294 1112295 1112296 1112297 1112298 1112299 1112300 1112301 [...] (144358 flits)
Measured flits: 1115055 1115056 1115057 1115058 1115059 1115060 1115061 1115062 1115063 1153404 [...] (20010 flits)
Class 0:
Remaining flits: 1210446 1210447 1210448 1210449 1210450 1210451 1210452 1210453 1210454 1210455 [...] (145059 flits)
Measured flits: 1210446 1210447 1210448 1210449 1210450 1210451 1210452 1210453 1210454 1210455 [...] (13845 flits)
Class 0:
Remaining flits: 1213506 1213507 1213508 1213509 1213510 1213511 1213512 1213513 1213514 1213515 [...] (142948 flits)
Measured flits: 1367550 1367551 1367552 1367553 1367554 1367555 1367556 1367557 1367558 1367559 [...] (9725 flits)
Class 0:
Remaining flits: 1277676 1277677 1277678 1277679 1277680 1277681 1277682 1277683 1277684 1277685 [...] (145020 flits)
Measured flits: 1410336 1410337 1410338 1410339 1410340 1410341 1410342 1410343 1410344 1410345 [...] (6985 flits)
Class 0:
Remaining flits: 1362186 1362187 1362188 1362189 1362190 1362191 1362192 1362193 1362194 1362195 [...] (140669 flits)
Measured flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (4396 flits)
Class 0:
Remaining flits: 1433142 1433143 1433144 1433145 1433146 1433147 1433148 1433149 1433150 1433151 [...] (140629 flits)
Measured flits: 1540566 1540567 1540568 1540569 1540570 1540571 1540572 1540573 1540574 1540575 [...] (2462 flits)
Class 0:
Remaining flits: 1458972 1458973 1458974 1458975 1458976 1458977 1458978 1458979 1458980 1458981 [...] (141682 flits)
Measured flits: 1548054 1548055 1548056 1548057 1548058 1548059 1548060 1548061 1548062 1548063 [...] (1226 flits)
Class 0:
Remaining flits: 1532412 1532413 1532414 1532415 1532416 1532417 1532418 1532419 1532420 1532421 [...] (138687 flits)
Measured flits: 1587096 1587097 1587098 1587099 1587100 1587101 1587102 1587103 1587104 1587105 [...] (630 flits)
Class 0:
Remaining flits: 1541192 1541193 1541194 1541195 1569258 1569259 1569260 1569261 1569262 1569263 [...] (141082 flits)
Measured flits: 1700964 1700965 1700966 1700967 1700968 1700969 1700970 1700971 1700972 1700973 [...] (90 flits)
Class 0:
Remaining flits: 1569258 1569259 1569260 1569261 1569262 1569263 1569264 1569265 1569266 1569267 [...] (142677 flits)
Measured flits: 1710396 1710397 1710398 1710399 1710400 1710401 1710402 1710403 1710404 1710405 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1602972 1602973 1602974 1602975 1602976 1602977 1602978 1602979 1602980 1602981 [...] (95141 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1746846 1746847 1746848 1746849 1746850 1746851 1746852 1746853 1746854 1746855 [...] (47949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1769076 1769077 1769078 1769079 1769080 1769081 1769082 1769083 1769084 1769085 [...] (8260 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1818666 1818667 1818668 1818669 1818670 1818671 1818672 1818673 1818674 1818675 [...] (111 flits)
Measured flits: (0 flits)
Time taken is 40521 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8580.81 (1 samples)
	minimum = 22 (1 samples)
	maximum = 26532 (1 samples)
Network latency average = 2798.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8144 (1 samples)
Flit latency average = 2722.71 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9572 (1 samples)
Fragmentation average = 20.4009 (1 samples)
	minimum = 0 (1 samples)
	maximum = 299 (1 samples)
Injected packet rate average = 0.0160595 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0241429 (1 samples)
Accepted packet rate average = 0.0148966 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.289122 (1 samples)
	minimum = 0.161571 (1 samples)
	maximum = 0.435429 (1 samples)
Accepted flit rate average = 0.268165 (1 samples)
	minimum = 0.206143 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 18.0032 (1 samples)
Accepted packet size average = 18.0018 (1 samples)
Hops average = 5.05907 (1 samples)
Total run time 39.1593
