BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 318.502
	minimum = 25
	maximum = 943
Network latency average = 302.734
	minimum = 25
	maximum = 943
Slowest packet = 252
Flit latency average = 247.266
	minimum = 6
	maximum = 967
Slowest flit = 2527
Fragmentation average = 149.654
	minimum = 0
	maximum = 790
Injected packet rate average = 0.040875
	minimum = 0.024 (at node 30)
	maximum = 0.055 (at node 51)
Accepted packet rate average = 0.0114687
	minimum = 0.004 (at node 108)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.729615
	minimum = 0.428 (at node 30)
	maximum = 0.98 (at node 51)
Accepted flit rate average= 0.234719
	minimum = 0.086 (at node 108)
	maximum = 0.387 (at node 104)
Injected packet length average = 17.8499
Accepted packet length average = 20.4659
Total in-flight flits = 96198 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 632.418
	minimum = 25
	maximum = 1911
Network latency average = 609.835
	minimum = 25
	maximum = 1911
Slowest packet = 557
Flit latency average = 541.225
	minimum = 6
	maximum = 1894
Slowest flit = 10043
Fragmentation average = 183.036
	minimum = 0
	maximum = 1732
Injected packet rate average = 0.0388724
	minimum = 0.025 (at node 96)
	maximum = 0.0505 (at node 177)
Accepted packet rate average = 0.0123203
	minimum = 0.0055 (at node 108)
	maximum = 0.0185 (at node 106)
Injected flit rate average = 0.69637
	minimum = 0.444 (at node 96)
	maximum = 0.903 (at node 177)
Accepted flit rate average= 0.234557
	minimum = 0.119 (at node 108)
	maximum = 0.359 (at node 177)
Injected packet length average = 17.9142
Accepted packet length average = 19.0383
Total in-flight flits = 178634 (0 measured)
latency change    = 0.496374
throughput change = 0.000688354
Class 0:
Packet latency average = 1649.78
	minimum = 31
	maximum = 2801
Network latency average = 1604.27
	minimum = 28
	maximum = 2780
Slowest packet = 808
Flit latency average = 1559.14
	minimum = 6
	maximum = 2919
Slowest flit = 7821
Fragmentation average = 230.489
	minimum = 3
	maximum = 1914
Injected packet rate average = 0.034875
	minimum = 0.011 (at node 0)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0119792
	minimum = 0.005 (at node 64)
	maximum = 0.025 (at node 92)
Injected flit rate average = 0.627995
	minimum = 0.201 (at node 0)
	maximum = 1 (at node 137)
Accepted flit rate average= 0.218229
	minimum = 0.077 (at node 66)
	maximum = 0.44 (at node 92)
Injected packet length average = 18.007
Accepted packet length average = 18.2174
Total in-flight flits = 257262 (0 measured)
latency change    = 0.616666
throughput change = 0.074821
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 860.846
	minimum = 31
	maximum = 2250
Network latency average = 50.5846
	minimum = 29
	maximum = 730
Slowest packet = 21705
Flit latency average = 2196.76
	minimum = 6
	maximum = 3817
Slowest flit = 14923
Fragmentation average = 13.1154
	minimum = 2
	maximum = 304
Injected packet rate average = 0.0262865
	minimum = 0.01 (at node 184)
	maximum = 0.048 (at node 169)
Accepted packet rate average = 0.0120677
	minimum = 0.005 (at node 36)
	maximum = 0.021 (at node 75)
Injected flit rate average = 0.472354
	minimum = 0.182 (at node 184)
	maximum = 0.859 (at node 169)
Accepted flit rate average= 0.214573
	minimum = 0.079 (at node 92)
	maximum = 0.396 (at node 137)
Injected packet length average = 17.9695
Accepted packet length average = 17.7808
Total in-flight flits = 306964 (88540 measured)
latency change    = 0.916466
throughput change = 0.0170397
Class 0:
Packet latency average = 1305.33
	minimum = 30
	maximum = 3262
Network latency average = 239.552
	minimum = 29
	maximum = 1933
Slowest packet = 21705
Flit latency average = 2513.8
	minimum = 6
	maximum = 4801
Slowest flit = 24772
Fragmentation average = 25.5878
	minimum = 2
	maximum = 304
Injected packet rate average = 0.0214193
	minimum = 0.006 (at node 184)
	maximum = 0.0365 (at node 109)
Accepted packet rate average = 0.0118932
	minimum = 0.006 (at node 36)
	maximum = 0.02 (at node 0)
Injected flit rate average = 0.385177
	minimum = 0.107 (at node 184)
	maximum = 0.661 (at node 109)
Accepted flit rate average= 0.212458
	minimum = 0.105 (at node 5)
	maximum = 0.3685 (at node 0)
Injected packet length average = 17.9827
Accepted packet length average = 17.8638
Total in-flight flits = 323818 (142933 measured)
latency change    = 0.340514
throughput change = 0.00995293
Class 0:
Packet latency average = 2153.27
	minimum = 30
	maximum = 4389
Network latency average = 895.116
	minimum = 29
	maximum = 2986
Slowest packet = 21705
Flit latency average = 2811.82
	minimum = 6
	maximum = 5585
Slowest flit = 54191
Fragmentation average = 61.5973
	minimum = 1
	maximum = 645
Injected packet rate average = 0.0192083
	minimum = 0.00833333 (at node 51)
	maximum = 0.0293333 (at node 109)
Accepted packet rate average = 0.011783
	minimum = 0.007 (at node 36)
	maximum = 0.0176667 (at node 0)
Injected flit rate average = 0.345408
	minimum = 0.147667 (at node 51)
	maximum = 0.527 (at node 109)
Accepted flit rate average= 0.21091
	minimum = 0.121 (at node 67)
	maximum = 0.316 (at node 0)
Injected packet length average = 17.9822
Accepted packet length average = 17.8995
Total in-flight flits = 335272 (188353 measured)
latency change    = 0.393792
throughput change = 0.00734253
Draining remaining packets ...
Class 0:
Remaining flits: 29052 29053 29054 29055 29056 29057 29058 29059 29060 29061 [...] (297333 flits)
Measured flits: 389232 389233 389234 389235 389236 389237 389238 389239 389240 389241 [...] (180850 flits)
Class 0:
Remaining flits: 29052 29053 29054 29055 29056 29057 29058 29059 29060 29061 [...] (259570 flits)
Measured flits: 389232 389233 389234 389235 389236 389237 389238 389239 389240 389241 [...] (169592 flits)
Class 0:
Remaining flits: 29052 29053 29054 29055 29056 29057 29058 29059 29060 29061 [...] (222954 flits)
Measured flits: 389232 389233 389234 389235 389236 389237 389238 389239 389240 389241 [...] (155781 flits)
Class 0:
Remaining flits: 49140 49141 49142 49143 49144 49145 49146 49147 49148 49149 [...] (187503 flits)
Measured flits: 389250 389251 389252 389253 389254 389255 389256 389257 389258 389259 [...] (137890 flits)
Class 0:
Remaining flits: 70434 70435 70436 70437 70438 70439 70440 70441 70442 70443 [...] (153158 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (117746 flits)
Class 0:
Remaining flits: 70434 70435 70436 70437 70438 70439 70440 70441 70442 70443 [...] (119116 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (95181 flits)
Class 0:
Remaining flits: 70441 70442 70443 70444 70445 70446 70447 70448 70449 70450 [...] (85310 flits)
Measured flits: 389502 389503 389504 389505 389506 389507 389508 389509 389510 389511 [...] (69130 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (53282 flits)
Measured flits: 389502 389503 389504 389505 389506 389507 389508 389509 389510 389511 [...] (43783 flits)
Class 0:
Remaining flits: 135270 135271 135272 135273 135274 135275 135276 135277 135278 135279 [...] (24758 flits)
Measured flits: 389592 389593 389594 389595 389596 389597 389598 389599 389600 389601 [...] (20352 flits)
Class 0:
Remaining flits: 164790 164791 164792 164793 164794 164795 164796 164797 164798 164799 [...] (5376 flits)
Measured flits: 390286 390287 390288 390289 390290 390291 390292 390293 391158 391159 [...] (4326 flits)
Class 0:
Remaining flits: 269658 269659 269660 269661 269662 269663 269664 269665 269666 269667 [...] (350 flits)
Measured flits: 398898 398899 398900 398901 398902 398903 398904 398905 398906 398907 [...] (268 flits)
Time taken is 17284 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8014.31 (1 samples)
	minimum = 30 (1 samples)
	maximum = 14906 (1 samples)
Network latency average = 7199.05 (1 samples)
	minimum = 29 (1 samples)
	maximum = 14186 (1 samples)
Flit latency average = 6101.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15365 (1 samples)
Fragmentation average = 163.341 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2975 (1 samples)
Injected packet rate average = 0.0192083 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.0293333 (1 samples)
Accepted packet rate average = 0.011783 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.345408 (1 samples)
	minimum = 0.147667 (1 samples)
	maximum = 0.527 (1 samples)
Accepted flit rate average = 0.21091 (1 samples)
	minimum = 0.121 (1 samples)
	maximum = 0.316 (1 samples)
Injected packet size average = 17.9822 (1 samples)
Accepted packet size average = 17.8995 (1 samples)
Hops average = 5.12659 (1 samples)
Total run time 19.36
