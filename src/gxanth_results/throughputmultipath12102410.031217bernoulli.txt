BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 291.494
	minimum = 23
	maximum = 954
Network latency average = 283.425
	minimum = 23
	maximum = 937
Slowest packet = 316
Flit latency average = 251.412
	minimum = 6
	maximum = 920
Slowest flit = 5695
Fragmentation average = 54.4047
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0104115
	minimum = 0.004 (at node 127)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.196755
	minimum = 0.072 (at node 127)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.8483
Accepted packet length average = 18.8979
Total in-flight flits = 68675 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.968
	minimum = 23
	maximum = 1781
Network latency average = 544.512
	minimum = 23
	maximum = 1767
Slowest packet = 1170
Flit latency average = 512.632
	minimum = 6
	maximum = 1844
Slowest flit = 15014
Fragmentation average = 58.0309
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0108594
	minimum = 0.005 (at node 174)
	maximum = 0.0165 (at node 167)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.19974
	minimum = 0.09 (at node 174)
	maximum = 0.297 (at node 167)
Injected packet length average = 17.9239
Accepted packet length average = 18.3933
Total in-flight flits = 137878 (0 measured)
latency change    = 0.473807
throughput change = 0.0149413
Class 0:
Packet latency average = 1327.03
	minimum = 23
	maximum = 2839
Network latency average = 1316.54
	minimum = 23
	maximum = 2810
Slowest packet = 987
Flit latency average = 1291.8
	minimum = 6
	maximum = 2793
Slowest flit = 17783
Fragmentation average = 62.8442
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0307917
	minimum = 0.019 (at node 173)
	maximum = 0.047 (at node 120)
Accepted packet rate average = 0.0109635
	minimum = 0.003 (at node 96)
	maximum = 0.023 (at node 51)
Injected flit rate average = 0.553875
	minimum = 0.342 (at node 173)
	maximum = 0.84 (at node 120)
Accepted flit rate average= 0.196828
	minimum = 0.058 (at node 96)
	maximum = 0.384 (at node 156)
Injected packet length average = 17.9878
Accepted packet length average = 17.953
Total in-flight flits = 206503 (0 measured)
latency change    = 0.582551
throughput change = 0.0147919
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 88.0098
	minimum = 23
	maximum = 584
Network latency average = 69.7101
	minimum = 23
	maximum = 584
Slowest packet = 19594
Flit latency average = 1858.94
	minimum = 6
	maximum = 3525
Slowest flit = 47716
Fragmentation average = 9.7371
	minimum = 0
	maximum = 35
Injected packet rate average = 0.030276
	minimum = 0.013 (at node 44)
	maximum = 0.046 (at node 81)
Accepted packet rate average = 0.0108698
	minimum = 0.003 (at node 43)
	maximum = 0.018 (at node 80)
Injected flit rate average = 0.544979
	minimum = 0.239 (at node 44)
	maximum = 0.827 (at node 117)
Accepted flit rate average= 0.196339
	minimum = 0.054 (at node 43)
	maximum = 0.317 (at node 80)
Injected packet length average = 18.0003
Accepted packet length average = 18.0628
Total in-flight flits = 273476 (97181 measured)
latency change    = 14.0782
throughput change = 0.00249357
Class 0:
Packet latency average = 162.963
	minimum = 23
	maximum = 1195
Network latency average = 119.914
	minimum = 23
	maximum = 1195
Slowest packet = 20983
Flit latency average = 2211.45
	minimum = 6
	maximum = 4413
Slowest flit = 60351
Fragmentation average = 9.70572
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0289062
	minimum = 0.0135 (at node 44)
	maximum = 0.0405 (at node 154)
Accepted packet rate average = 0.0105807
	minimum = 0.0045 (at node 121)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.520328
	minimum = 0.243 (at node 176)
	maximum = 0.729 (at node 154)
Accepted flit rate average= 0.190372
	minimum = 0.0825 (at node 121)
	maximum = 0.3035 (at node 44)
Injected packet length average = 18.0005
Accepted packet length average = 17.9924
Total in-flight flits = 333200 (186548 measured)
latency change    = 0.459941
throughput change = 0.0313393
Class 0:
Packet latency average = 319.662
	minimum = 23
	maximum = 2979
Network latency average = 234.525
	minimum = 23
	maximum = 2958
Slowest packet = 18052
Flit latency average = 2540.9
	minimum = 6
	maximum = 5356
Slowest flit = 43793
Fragmentation average = 10.532
	minimum = 0
	maximum = 127
Injected packet rate average = 0.0282344
	minimum = 0.0136667 (at node 100)
	maximum = 0.04 (at node 154)
Accepted packet rate average = 0.0103802
	minimum = 0.006 (at node 64)
	maximum = 0.0153333 (at node 27)
Injected flit rate average = 0.507927
	minimum = 0.249 (at node 100)
	maximum = 0.718667 (at node 154)
Accepted flit rate average= 0.187207
	minimum = 0.103667 (at node 64)
	maximum = 0.274667 (at node 27)
Injected packet length average = 17.9897
Accepted packet length average = 18.035
Total in-flight flits = 391424 (273995 measured)
latency change    = 0.490201
throughput change = 0.0169107
Draining remaining packets ...
Class 0:
Remaining flits: 85158 85159 85160 85161 85162 85163 85164 85165 85166 85167 [...] (358964 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (270076 flits)
Class 0:
Remaining flits: 107424 107425 107426 107427 107428 107429 107430 107431 107432 107433 [...] (327822 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (264025 flits)
Class 0:
Remaining flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (296695 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (254168 flits)
Class 0:
Remaining flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (267380 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (241507 flits)
Class 0:
Remaining flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (238201 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (223385 flits)
Class 0:
Remaining flits: 146070 146071 146072 146073 146074 146075 146076 146077 146078 146079 [...] (208864 flits)
Measured flits: 321264 321265 321266 321267 321268 321269 321270 321271 321272 321273 [...] (201186 flits)
Class 0:
Remaining flits: 189396 189397 189398 189399 189400 189401 189402 189403 189404 189405 [...] (179523 flits)
Measured flits: 321264 321265 321266 321267 321268 321269 321270 321271 321272 321273 [...] (175708 flits)
Class 0:
Remaining flits: 213840 213841 213842 213843 213844 213845 213846 213847 213848 213849 [...] (149923 flits)
Measured flits: 321264 321265 321266 321267 321268 321269 321270 321271 321272 321273 [...] (148152 flits)
Class 0:
Remaining flits: 213840 213841 213842 213843 213844 213845 213846 213847 213848 213849 [...] (120438 flits)
Measured flits: 323478 323479 323480 323481 323482 323483 323484 323485 323486 323487 [...] (119441 flits)
Class 0:
Remaining flits: 272484 272485 272486 272487 272488 272489 272490 272491 272492 272493 [...] (90385 flits)
Measured flits: 323478 323479 323480 323481 323482 323483 323484 323485 323486 323487 [...] (90038 flits)
Class 0:
Remaining flits: 305856 305857 305858 305859 305860 305861 305862 305863 305864 305865 [...] (61841 flits)
Measured flits: 325228 325229 325230 325231 325232 325233 325234 325235 325236 325237 [...] (61769 flits)
Class 0:
Remaining flits: 305863 305864 305865 305866 305867 305868 305869 305870 305871 305872 [...] (33111 flits)
Measured flits: 333990 333991 333992 333993 333994 333995 333996 333997 333998 333999 [...] (33082 flits)
Class 0:
Remaining flits: 440514 440515 440516 440517 440518 440519 440520 440521 440522 440523 [...] (8752 flits)
Measured flits: 440514 440515 440516 440517 440518 440519 440520 440521 440522 440523 [...] (8752 flits)
Time taken is 19992 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9162.78 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15487 (1 samples)
Network latency average = 9090.48 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15292 (1 samples)
Flit latency average = 7322.53 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15292 (1 samples)
Fragmentation average = 66.0387 (1 samples)
	minimum = 0 (1 samples)
	maximum = 169 (1 samples)
Injected packet rate average = 0.0282344 (1 samples)
	minimum = 0.0136667 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0103802 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.507927 (1 samples)
	minimum = 0.249 (1 samples)
	maximum = 0.718667 (1 samples)
Accepted flit rate average = 0.187207 (1 samples)
	minimum = 0.103667 (1 samples)
	maximum = 0.274667 (1 samples)
Injected packet size average = 17.9897 (1 samples)
Accepted packet size average = 18.035 (1 samples)
Hops average = 5.15181 (1 samples)
Total run time 12.3823
