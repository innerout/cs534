BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.046
	minimum = 24
	maximum = 880
Network latency average = 163.819
	minimum = 22
	maximum = 727
Slowest packet = 21
Flit latency average = 136.43
	minimum = 5
	maximum = 747
Slowest flit = 14990
Fragmentation average = 28.6859
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0122552
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.227964
	minimum = 0.108 (at node 41)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6014
Total in-flight flits = 18241 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 357.96
	minimum = 22
	maximum = 1821
Network latency average = 276.63
	minimum = 22
	maximum = 1439
Slowest packet = 21
Flit latency average = 245.59
	minimum = 5
	maximum = 1422
Slowest flit = 26189
Fragmentation average = 35.5711
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0132187
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241724
	minimum = 0.153 (at node 116)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.2864
Total in-flight flits = 26770 (0 measured)
latency change    = 0.360136
throughput change = 0.0569262
Class 0:
Packet latency average = 628.204
	minimum = 22
	maximum = 2515
Network latency average = 526.7
	minimum = 22
	maximum = 2067
Slowest packet = 2278
Flit latency average = 492.106
	minimum = 5
	maximum = 2050
Slowest flit = 52991
Fragmentation average = 42.4846
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145208
	minimum = 0.004 (at node 118)
	maximum = 0.025 (at node 57)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.26126
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9921
Total in-flight flits = 38978 (0 measured)
latency change    = 0.430185
throughput change = 0.0747777
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 414.67
	minimum = 25
	maximum = 1431
Network latency average = 316.29
	minimum = 22
	maximum = 974
Slowest packet = 10126
Flit latency average = 642.327
	minimum = 5
	maximum = 2529
Slowest flit = 72107
Fragmentation average = 26.1359
	minimum = 0
	maximum = 213
Injected packet rate average = 0.0184219
	minimum = 0 (at node 20)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148229
	minimum = 0.006 (at node 43)
	maximum = 0.027 (at node 103)
Injected flit rate average = 0.331411
	minimum = 0 (at node 20)
	maximum = 0.989 (at node 23)
Accepted flit rate average= 0.268182
	minimum = 0.108 (at node 43)
	maximum = 0.514 (at node 103)
Injected packet length average = 17.9901
Accepted packet length average = 18.0924
Total in-flight flits = 51171 (43263 measured)
latency change    = 0.51495
throughput change = 0.0258103
Class 0:
Packet latency average = 699.263
	minimum = 25
	maximum = 2161
Network latency average = 593.587
	minimum = 22
	maximum = 1966
Slowest packet = 10126
Flit latency average = 730.812
	minimum = 5
	maximum = 2762
Slowest flit = 81017
Fragmentation average = 39.3935
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0183385
	minimum = 0.003 (at node 136)
	maximum = 0.042 (at node 111)
Accepted packet rate average = 0.0148229
	minimum = 0.0085 (at node 43)
	maximum = 0.023 (at node 178)
Injected flit rate average = 0.330224
	minimum = 0.0505 (at node 184)
	maximum = 0.763 (at node 111)
Accepted flit rate average= 0.268224
	minimum = 0.153 (at node 43)
	maximum = 0.4135 (at node 178)
Injected packet length average = 18.0071
Accepted packet length average = 18.0952
Total in-flight flits = 62736 (61675 measured)
latency change    = 0.40699
throughput change = 0.000155343
Class 0:
Packet latency average = 888.837
	minimum = 22
	maximum = 3399
Network latency average = 782.826
	minimum = 22
	maximum = 2828
Slowest packet = 10126
Flit latency average = 828.303
	minimum = 5
	maximum = 3213
Slowest flit = 162017
Fragmentation average = 44.8901
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0177674
	minimum = 0.00266667 (at node 136)
	maximum = 0.0376667 (at node 34)
Accepted packet rate average = 0.0147882
	minimum = 0.01 (at node 36)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.320111
	minimum = 0.048 (at node 136)
	maximum = 0.678 (at node 34)
Accepted flit rate average= 0.267257
	minimum = 0.18 (at node 36)
	maximum = 0.384 (at node 129)
Injected packet length average = 18.0168
Accepted packet length average = 18.0723
Total in-flight flits = 69250 (69160 measured)
latency change    = 0.213283
throughput change = 0.00361829
Draining remaining packets ...
Class 0:
Remaining flits: 193158 193159 193160 193161 193162 193163 193164 193165 193166 193167 [...] (25158 flits)
Measured flits: 193158 193159 193160 193161 193162 193163 193164 193165 193166 193167 [...] (25158 flits)
Class 0:
Remaining flits: 219778 219779 222030 222031 222032 222033 222034 222035 222036 222037 [...] (4794 flits)
Measured flits: 219778 219779 222030 222031 222032 222033 222034 222035 222036 222037 [...] (4794 flits)
Class 0:
Remaining flits: 304596 304597 304598 304599 304600 304601 304602 304603 304604 304605 [...] (660 flits)
Measured flits: 304596 304597 304598 304599 304600 304601 304602 304603 304604 304605 [...] (660 flits)
Time taken is 9444 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1251.8 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5224 (1 samples)
Network latency average = 1142.83 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4915 (1 samples)
Flit latency average = 1094.34 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4898 (1 samples)
Fragmentation average = 45.3483 (1 samples)
	minimum = 0 (1 samples)
	maximum = 439 (1 samples)
Injected packet rate average = 0.0177674 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.0376667 (1 samples)
Accepted packet rate average = 0.0147882 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.320111 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.678 (1 samples)
Accepted flit rate average = 0.267257 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.384 (1 samples)
Injected packet size average = 18.0168 (1 samples)
Accepted packet size average = 18.0723 (1 samples)
Hops average = 5.07465 (1 samples)
Total run time 5.73373
