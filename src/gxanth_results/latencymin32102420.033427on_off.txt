BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.886
	minimum = 24
	maximum = 955
Network latency average = 233.455
	minimum = 22
	maximum = 778
Slowest packet = 57
Flit latency average = 203.711
	minimum = 5
	maximum = 788
Slowest flit = 17104
Fragmentation average = 43.6166
	minimum = 0
	maximum = 413
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.013776
	minimum = 0.007 (at node 23)
	maximum = 0.024 (at node 76)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.259057
	minimum = 0.126 (at node 28)
	maximum = 0.432 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.8049
Total in-flight flits = 50701 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 605.038
	minimum = 22
	maximum = 1931
Network latency average = 445.545
	minimum = 22
	maximum = 1568
Slowest packet = 57
Flit latency average = 414.462
	minimum = 5
	maximum = 1663
Slowest flit = 28486
Fragmentation average = 62.7326
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.014776
	minimum = 0.0075 (at node 83)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.274857
	minimum = 0.1415 (at node 83)
	maximum = 0.404 (at node 152)
Injected packet length average = 17.9205
Accepted packet length average = 18.6015
Total in-flight flits = 105199 (0 measured)
latency change    = 0.431629
throughput change = 0.0574826
Class 0:
Packet latency average = 1334.23
	minimum = 22
	maximum = 2769
Network latency average = 1086.18
	minimum = 22
	maximum = 2387
Slowest packet = 3341
Flit latency average = 1052.38
	minimum = 5
	maximum = 2370
Slowest flit = 43055
Fragmentation average = 101.687
	minimum = 0
	maximum = 417
Injected packet rate average = 0.031875
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.0161979
	minimum = 0.005 (at node 61)
	maximum = 0.029 (at node 63)
Injected flit rate average = 0.573807
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.292807
	minimum = 0.12 (at node 61)
	maximum = 0.545 (at node 63)
Injected packet length average = 18.0018
Accepted packet length average = 18.0768
Total in-flight flits = 159140 (0 measured)
latency change    = 0.546526
throughput change = 0.0613049
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 351.022
	minimum = 26
	maximum = 1467
Network latency average = 35.1673
	minimum = 22
	maximum = 130
Slowest packet = 17887
Flit latency average = 1507.45
	minimum = 5
	maximum = 3047
Slowest flit = 93040
Fragmentation average = 6.12948
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0326719
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0162708
	minimum = 0.005 (at node 134)
	maximum = 0.029 (at node 151)
Injected flit rate average = 0.587896
	minimum = 0 (at node 5)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292385
	minimum = 0.12 (at node 134)
	maximum = 0.487 (at node 120)
Injected packet length average = 17.9939
Accepted packet length average = 17.9699
Total in-flight flits = 215916 (103795 measured)
latency change    = 2.80098
throughput change = 0.00144287
Class 0:
Packet latency average = 392.936
	minimum = 25
	maximum = 2562
Network latency average = 87.3481
	minimum = 22
	maximum = 1992
Slowest packet = 17887
Flit latency average = 1720.7
	minimum = 5
	maximum = 3670
Slowest flit = 136113
Fragmentation average = 8.68558
	minimum = 0
	maximum = 213
Injected packet rate average = 0.032362
	minimum = 0.004 (at node 123)
	maximum = 0.0555 (at node 0)
Accepted packet rate average = 0.0162422
	minimum = 0.0075 (at node 134)
	maximum = 0.0235 (at node 39)
Injected flit rate average = 0.582534
	minimum = 0.0715 (at node 123)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292906
	minimum = 0.1575 (at node 134)
	maximum = 0.4175 (at node 129)
Injected packet length average = 18.0006
Accepted packet length average = 18.0337
Total in-flight flits = 270350 (204632 measured)
latency change    = 0.106668
throughput change = 0.00177816
Class 0:
Packet latency average = 971.181
	minimum = 22
	maximum = 3556
Network latency average = 699.412
	minimum = 22
	maximum = 2956
Slowest packet = 17887
Flit latency average = 1935.93
	minimum = 5
	maximum = 4559
Slowest flit = 146267
Fragmentation average = 30.6225
	minimum = 0
	maximum = 312
Injected packet rate average = 0.0321302
	minimum = 0.00866667 (at node 95)
	maximum = 0.0556667 (at node 25)
Accepted packet rate average = 0.0162448
	minimum = 0.0116667 (at node 36)
	maximum = 0.0223333 (at node 165)
Injected flit rate average = 0.578356
	minimum = 0.156 (at node 95)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292922
	minimum = 0.202 (at node 86)
	maximum = 0.393667 (at node 165)
Injected packet length average = 18.0004
Accepted packet length average = 18.0317
Total in-flight flits = 323543 (293101 measured)
latency change    = 0.595404
throughput change = 5.33419e-05
Class 0:
Packet latency average = 1774.74
	minimum = 22
	maximum = 4824
Network latency average = 1519.08
	minimum = 22
	maximum = 3945
Slowest packet = 17887
Flit latency average = 2159.68
	minimum = 5
	maximum = 5287
Slowest flit = 180344
Fragmentation average = 60.5322
	minimum = 0
	maximum = 444
Injected packet rate average = 0.0320638
	minimum = 0.00975 (at node 165)
	maximum = 0.05575 (at node 25)
Accepted packet rate average = 0.0161875
	minimum = 0.01225 (at node 64)
	maximum = 0.022 (at node 165)
Injected flit rate average = 0.577078
	minimum = 0.177 (at node 165)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292423
	minimum = 0.219 (at node 64)
	maximum = 0.3855 (at node 165)
Injected packet length average = 17.9978
Accepted packet length average = 18.0648
Total in-flight flits = 377809 (364846 measured)
latency change    = 0.452776
throughput change = 0.0017054
Class 0:
Packet latency average = 2364.91
	minimum = 22
	maximum = 6252
Network latency average = 2107.02
	minimum = 22
	maximum = 4946
Slowest packet = 17887
Flit latency average = 2380.92
	minimum = 5
	maximum = 5848
Slowest flit = 225162
Fragmentation average = 81.6457
	minimum = 0
	maximum = 444
Injected packet rate average = 0.0319188
	minimum = 0.0092 (at node 164)
	maximum = 0.0556 (at node 25)
Accepted packet rate average = 0.0162198
	minimum = 0.0126 (at node 31)
	maximum = 0.021 (at node 11)
Injected flit rate average = 0.574432
	minimum = 0.1656 (at node 164)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292598
	minimum = 0.229 (at node 31)
	maximum = 0.3742 (at node 11)
Injected packet length average = 17.9967
Accepted packet length average = 18.0396
Total in-flight flits = 429802 (424869 measured)
latency change    = 0.249552
throughput change = 0.0005972
Class 0:
Packet latency average = 2849.16
	minimum = 22
	maximum = 7167
Network latency average = 2582.54
	minimum = 22
	maximum = 5909
Slowest packet = 17887
Flit latency average = 2617.38
	minimum = 5
	maximum = 6672
Slowest flit = 217727
Fragmentation average = 94.6406
	minimum = 0
	maximum = 444
Injected packet rate average = 0.0318793
	minimum = 0.0128333 (at node 180)
	maximum = 0.0556667 (at node 25)
Accepted packet rate average = 0.0161667
	minimum = 0.0126667 (at node 31)
	maximum = 0.0205 (at node 118)
Injected flit rate average = 0.573848
	minimum = 0.231 (at node 180)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.291976
	minimum = 0.231167 (at node 31)
	maximum = 0.372 (at node 118)
Injected packet length average = 18.0006
Accepted packet length average = 18.0604
Total in-flight flits = 483834 (482221 measured)
latency change    = 0.169961
throughput change = 0.00213108
Class 0:
Packet latency average = 3242.81
	minimum = 22
	maximum = 8010
Network latency average = 2969.87
	minimum = 22
	maximum = 6936
Slowest packet = 17887
Flit latency average = 2844.82
	minimum = 5
	maximum = 7305
Slowest flit = 264635
Fragmentation average = 104.033
	minimum = 0
	maximum = 444
Injected packet rate average = 0.0323906
	minimum = 0.0138571 (at node 180)
	maximum = 0.0555714 (at node 0)
Accepted packet rate average = 0.0162165
	minimum = 0.0125714 (at node 67)
	maximum = 0.0204286 (at node 118)
Injected flit rate average = 0.582882
	minimum = 0.249429 (at node 180)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.29238
	minimum = 0.226429 (at node 67)
	maximum = 0.370857 (at node 118)
Injected packet length average = 17.9954
Accepted packet length average = 18.0298
Total in-flight flits = 549775 (549338 measured)
latency change    = 0.121392
throughput change = 0.00138352
Draining all recorded packets ...
Class 0:
Remaining flits: 313420 313421 313422 313423 313424 313425 313426 313427 313428 313429 [...] (611681 flits)
Measured flits: 325850 325851 325852 325853 325945 325946 325947 325948 325949 325950 [...] (535611 flits)
Class 0:
Remaining flits: 342883 342884 342885 342886 342887 342888 342889 342890 342891 342892 [...] (669301 flits)
Measured flits: 342883 342884 342885 342886 342887 342888 342889 342890 342891 342892 [...] (489467 flits)
Class 0:
Remaining flits: 375888 375889 375890 375891 375892 375893 379854 379855 379856 379857 [...] (725164 flits)
Measured flits: 375888 375889 375890 375891 375892 375893 379854 379855 379856 379857 [...] (442536 flits)
Class 0:
Remaining flits: 411865 411866 411867 411868 411869 411870 411871 411872 411873 411874 [...] (783670 flits)
Measured flits: 411865 411866 411867 411868 411869 411870 411871 411872 411873 411874 [...] (395214 flits)
Class 0:
Remaining flits: 425648 425649 425650 425651 425652 425653 425654 425655 425656 425657 [...] (842503 flits)
Measured flits: 425648 425649 425650 425651 425652 425653 425654 425655 425656 425657 [...] (348124 flits)
Class 0:
Remaining flits: 494154 494155 494156 494157 494158 494159 494160 494161 494162 494163 [...] (904020 flits)
Measured flits: 494154 494155 494156 494157 494158 494159 494160 494161 494162 494163 [...] (300605 flits)
Class 0:
Remaining flits: 524106 524107 524108 524109 524110 524111 524112 524113 524114 524115 [...] (964387 flits)
Measured flits: 524106 524107 524108 524109 524110 524111 524112 524113 524114 524115 [...] (253309 flits)
Class 0:
Remaining flits: 612440 612441 612442 612443 612444 612445 612446 612447 612448 612449 [...] (1022158 flits)
Measured flits: 612440 612441 612442 612443 612444 612445 612446 612447 612448 612449 [...] (206753 flits)
Class 0:
Remaining flits: 659412 659413 659414 659415 659416 659417 659418 659419 659420 659421 [...] (1080689 flits)
Measured flits: 659412 659413 659414 659415 659416 659417 659418 659419 659420 659421 [...] (161544 flits)
Class 0:
Remaining flits: 674982 674983 674984 674985 674986 674987 674988 674989 674990 674991 [...] (1138087 flits)
Measured flits: 674982 674983 674984 674985 674986 674987 674988 674989 674990 674991 [...] (120349 flits)
Class 0:
Remaining flits: 711612 711613 711614 711615 711616 711617 711618 711619 711620 711621 [...] (1197346 flits)
Measured flits: 711612 711613 711614 711615 711616 711617 711618 711619 711620 711621 [...] (86313 flits)
Class 0:
Remaining flits: 734349 734350 734351 734352 734353 734354 734355 734356 734357 734358 [...] (1253078 flits)
Measured flits: 734349 734350 734351 734352 734353 734354 734355 734356 734357 734358 [...] (60686 flits)
Class 0:
Remaining flits: 776567 776568 776569 776570 776571 776572 776573 786186 786187 786188 [...] (1307609 flits)
Measured flits: 776567 776568 776569 776570 776571 776572 776573 786186 786187 786188 [...] (42360 flits)
Class 0:
Remaining flits: 801713 801714 801715 801716 801717 801718 801719 803628 803629 803630 [...] (1360217 flits)
Measured flits: 801713 801714 801715 801716 801717 801718 801719 803628 803629 803630 [...] (29505 flits)
Class 0:
Remaining flits: 812034 812035 812036 812037 812038 812039 812040 812041 812042 812043 [...] (1411623 flits)
Measured flits: 812034 812035 812036 812037 812038 812039 812040 812041 812042 812043 [...] (20080 flits)
Class 0:
Remaining flits: 812041 812042 812043 812044 812045 812046 812047 812048 812049 812050 [...] (1469580 flits)
Measured flits: 812041 812042 812043 812044 812045 812046 812047 812048 812049 812050 [...] (13464 flits)
Class 0:
Remaining flits: 839152 839153 839154 839155 839156 839157 839158 839159 852192 852193 [...] (1527805 flits)
Measured flits: 839152 839153 839154 839155 839156 839157 839158 839159 852192 852193 [...] (8939 flits)
Class 0:
Remaining flits: 887148 887149 887150 887151 887152 887153 887154 887155 887156 887157 [...] (1577349 flits)
Measured flits: 887148 887149 887150 887151 887152 887153 887154 887155 887156 887157 [...] (5576 flits)
Class 0:
Remaining flits: 920142 920143 920144 920145 920146 920147 920148 920149 920150 920151 [...] (1624748 flits)
Measured flits: 920142 920143 920144 920145 920146 920147 920148 920149 920150 920151 [...] (3523 flits)
Class 0:
Remaining flits: 942840 942841 942842 942843 942844 942845 942846 942847 942848 942849 [...] (1676864 flits)
Measured flits: 942840 942841 942842 942843 942844 942845 942846 942847 942848 942849 [...] (2154 flits)
Class 0:
Remaining flits: 977314 977315 977316 977317 977318 977319 977320 977321 977322 977323 [...] (1727384 flits)
Measured flits: 977314 977315 977316 977317 977318 977319 977320 977321 977322 977323 [...] (1190 flits)
Class 0:
Remaining flits: 1015920 1015921 1015922 1015923 1015924 1015925 1015926 1015927 1015928 1015929 [...] (1773878 flits)
Measured flits: 1015920 1015921 1015922 1015923 1015924 1015925 1015926 1015927 1015928 1015929 [...] (593 flits)
Class 0:
Remaining flits: 1025154 1025155 1025156 1025157 1025158 1025159 1025160 1025161 1025162 1025163 [...] (1817335 flits)
Measured flits: 1025154 1025155 1025156 1025157 1025158 1025159 1025160 1025161 1025162 1025163 [...] (395 flits)
Class 0:
Remaining flits: 1098668 1098669 1098670 1098671 1098672 1098673 1098674 1098675 1098676 1098677 [...] (1860595 flits)
Measured flits: 1098668 1098669 1098670 1098671 1098672 1098673 1098674 1098675 1098676 1098677 [...] (165 flits)
Class 0:
Remaining flits: 1170946 1170947 1170948 1170949 1170950 1170951 1170952 1170953 1192889 1192890 [...] (1902426 flits)
Measured flits: 1192889 1192890 1192891 1192892 1192893 1192894 1192895 1201644 1201645 1201646 [...] (97 flits)
Class 0:
Remaining flits: 1214442 1214443 1214444 1214445 1214446 1214447 1214448 1214449 1214450 1214451 [...] (1948001 flits)
Measured flits: 1220955 1220956 1220957 1229256 1229257 1229258 1229259 1229260 1229261 1229262 [...] (42 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1227096 1227097 1227098 1227099 1227100 1227101 1227102 1227103 1227104 1227105 [...] (1907480 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1268859 1268860 1268861 1268862 1268863 1268864 1268865 1268866 1268867 1268868 [...] (1858477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274800 1274801 1274802 1274803 1274804 1274805 1274806 1274807 1274808 1274809 [...] (1809711 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1334448 1334449 1334450 1334451 1334452 1334453 1334454 1334455 1334456 1334457 [...] (1760820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1393740 1393741 1393742 1393743 1393744 1393745 1393746 1393747 1393748 1393749 [...] (1711433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1399176 1399177 1399178 1399179 1399180 1399181 1399182 1399183 1399184 1399185 [...] (1662520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1418218 1418219 1438020 1438021 1438022 1438023 1438024 1438025 1438026 1438027 [...] (1613796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1459674 1459675 1459676 1459677 1459678 1459679 1459680 1459681 1459682 1459683 [...] (1565272 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1463670 1463671 1463672 1463673 1463674 1463675 1463676 1463677 1463678 1463679 [...] (1516642 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1463670 1463671 1463672 1463673 1463674 1463675 1463676 1463677 1463678 1463679 [...] (1468330 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1463670 1463671 1463672 1463673 1463674 1463675 1463676 1463677 1463678 1463679 [...] (1420145 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1503735 1503736 1503737 1518462 1518463 1518464 1518465 1518466 1518467 1518468 [...] (1371933 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1557540 1557541 1557542 1557543 1557544 1557545 1557546 1557547 1557548 1557549 [...] (1323949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1650438 1650439 1650440 1650441 1650442 1650443 1650444 1650445 1650446 1650447 [...] (1275578 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1650438 1650439 1650440 1650441 1650442 1650443 1650444 1650445 1650446 1650447 [...] (1227764 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1650438 1650439 1650440 1650441 1650442 1650443 1650444 1650445 1650446 1650447 [...] (1179967 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1650438 1650439 1650440 1650441 1650442 1650443 1650444 1650445 1650446 1650447 [...] (1132165 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839132 1839133 1839134 1839135 1839136 1839137 1839138 1839139 1839140 1839141 [...] (1084476 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1962486 1962487 1962488 1962489 1962490 1962491 1962492 1962493 1962494 1962495 [...] (1037035 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2055078 2055079 2055080 2055081 2055082 2055083 2055084 2055085 2055086 2055087 [...] (989343 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2087870 2087871 2087872 2087873 2100186 2100187 2100188 2100189 2100190 2100191 [...] (941491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2100186 2100187 2100188 2100189 2100190 2100191 2100192 2100193 2100194 2100195 [...] (893886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2100186 2100187 2100188 2100189 2100190 2100191 2100192 2100193 2100194 2100195 [...] (846177 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2100200 2100201 2100202 2100203 2102270 2102271 2102272 2102273 2120238 2120239 [...] (798302 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2120238 2120239 2120240 2120241 2120242 2120243 2120244 2120245 2120246 2120247 [...] (750755 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2187446 2187447 2187448 2187449 2199618 2199619 2199620 2199621 2199622 2199623 [...] (703734 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2199618 2199619 2199620 2199621 2199622 2199623 2199624 2199625 2199626 2199627 [...] (655933 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2262186 2262187 2262188 2262189 2262190 2262191 2262192 2262193 2262194 2262195 [...] (608314 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2270682 2270683 2270684 2270685 2270686 2270687 2270688 2270689 2270690 2270691 [...] (560920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2270682 2270683 2270684 2270685 2270686 2270687 2270688 2270689 2270690 2270691 [...] (512482 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2320099 2320100 2320101 2320102 2320103 2320104 2320105 2320106 2320107 2320108 [...] (464774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2355966 2355967 2355968 2355969 2355970 2355971 2355972 2355973 2355974 2355975 [...] (417130 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2355966 2355967 2355968 2355969 2355970 2355971 2355972 2355973 2355974 2355975 [...] (369504 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2379451 2379452 2379453 2379454 2379455 2389320 2389321 2389322 2389323 2389324 [...] (321964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2406924 2406925 2406926 2406927 2406928 2406929 2406930 2406931 2406932 2406933 [...] (274104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2438996 2438997 2438998 2438999 2445408 2445409 2445410 2445411 2445412 2445413 [...] (227466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2447568 2447569 2447570 2447571 2447572 2447573 2447574 2447575 2447576 2447577 [...] (182896 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2528856 2528857 2528858 2528859 2528860 2528861 2528862 2528863 2528864 2528865 [...] (139490 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2869056 2869057 2869058 2869059 2869060 2869061 2869062 2869063 2869064 2869065 [...] (103543 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2917836 2917837 2917838 2917839 2917840 2917841 2917842 2917843 2917844 2917845 [...] (75218 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2975778 2975779 2975780 2975781 2975782 2975783 2975784 2975785 2975786 2975787 [...] (52776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3037770 3037771 3037772 3037773 3037774 3037775 3037776 3037777 3037778 3037779 [...] (33891 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3264642 3264643 3264644 3264645 3264646 3264647 3264648 3264649 3264650 3264651 [...] (20595 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3286584 3286585 3286586 3286587 3286588 3286589 3286590 3286591 3286592 3286593 [...] (11285 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3333060 3333061 3333062 3333063 3333064 3333065 3333066 3333067 3333068 3333069 [...] (4376 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3467628 3467629 3467630 3467631 3467632 3467633 3467634 3467635 3467636 3467637 [...] (917 flits)
Measured flits: (0 flits)
Time taken is 83241 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7658.93 (1 samples)
	minimum = 22 (1 samples)
	maximum = 26969 (1 samples)
Network latency average = 7342.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 25192 (1 samples)
Flit latency average = 19996 (1 samples)
	minimum = 5 (1 samples)
	maximum = 52627 (1 samples)
Fragmentation average = 148.772 (1 samples)
	minimum = 0 (1 samples)
	maximum = 561 (1 samples)
Injected packet rate average = 0.0323906 (1 samples)
	minimum = 0.0138571 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.0162165 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.0204286 (1 samples)
Injected flit rate average = 0.582882 (1 samples)
	minimum = 0.249429 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.29238 (1 samples)
	minimum = 0.226429 (1 samples)
	maximum = 0.370857 (1 samples)
Injected packet size average = 17.9954 (1 samples)
Accepted packet size average = 18.0298 (1 samples)
Hops average = 5.06158 (1 samples)
Total run time 90.715
