BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.429
	minimum = 22
	maximum = 967
Network latency average = 220.817
	minimum = 22
	maximum = 764
Slowest packet = 8
Flit latency average = 190.858
	minimum = 5
	maximum = 751
Slowest flit = 18159
Fragmentation average = 41.0479
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0133802
	minimum = 0.006 (at node 64)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.250219
	minimum = 0.119 (at node 64)
	maximum = 0.396 (at node 44)
Injected packet length average = 17.8353
Accepted packet length average = 18.7007
Total in-flight flits = 35658 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.595
	minimum = 22
	maximum = 1940
Network latency average = 399.08
	minimum = 22
	maximum = 1615
Slowest packet = 8
Flit latency average = 366.501
	minimum = 5
	maximum = 1598
Slowest flit = 31229
Fragmentation average = 52.4594
	minimum = 0
	maximum = 394
Injected packet rate average = 0.024125
	minimum = 0 (at node 30)
	maximum = 0.0545 (at node 91)
Accepted packet rate average = 0.0141927
	minimum = 0.007 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.432654
	minimum = 0 (at node 30)
	maximum = 0.974 (at node 91)
Accepted flit rate average= 0.261682
	minimum = 0.132 (at node 30)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.9338
Accepted packet length average = 18.4378
Total in-flight flits = 66266 (0 measured)
latency change    = 0.390766
throughput change = 0.0438071
Class 0:
Packet latency average = 1058.38
	minimum = 25
	maximum = 2906
Network latency average = 891.707
	minimum = 22
	maximum = 2263
Slowest packet = 4621
Flit latency average = 857.749
	minimum = 5
	maximum = 2322
Slowest flit = 54691
Fragmentation average = 74.8165
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0251198
	minimum = 0 (at node 46)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0153281
	minimum = 0.005 (at node 146)
	maximum = 0.026 (at node 63)
Injected flit rate average = 0.451083
	minimum = 0 (at node 46)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.277792
	minimum = 0.093 (at node 146)
	maximum = 0.494 (at node 183)
Injected packet length average = 17.9573
Accepted packet length average = 18.123
Total in-flight flits = 99744 (0 measured)
latency change    = 0.501509
throughput change = 0.0579909
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 260.663
	minimum = 27
	maximum = 1419
Network latency average = 91.5442
	minimum = 22
	maximum = 972
Slowest packet = 14089
Flit latency average = 1209.92
	minimum = 5
	maximum = 2846
Slowest flit = 95268
Fragmentation average = 8.54884
	minimum = 0
	maximum = 183
Injected packet rate average = 0.0257135
	minimum = 0 (at node 93)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0157188
	minimum = 0.006 (at node 74)
	maximum = 0.031 (at node 59)
Injected flit rate average = 0.463339
	minimum = 0.013 (at node 96)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.283214
	minimum = 0.112 (at node 74)
	maximum = 0.56 (at node 59)
Injected packet length average = 18.0192
Accepted packet length average = 18.0176
Total in-flight flits = 134233 (80855 measured)
latency change    = 3.06036
throughput change = 0.0191441
Class 0:
Packet latency average = 786.994
	minimum = 22
	maximum = 2438
Network latency average = 620.414
	minimum = 22
	maximum = 1990
Slowest packet = 14089
Flit latency average = 1369
	minimum = 5
	maximum = 3552
Slowest flit = 121573
Fragmentation average = 32.2391
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0262682
	minimum = 0.0045 (at node 190)
	maximum = 0.0555 (at node 53)
Accepted packet rate average = 0.0158516
	minimum = 0.009 (at node 4)
	maximum = 0.0235 (at node 59)
Injected flit rate average = 0.472961
	minimum = 0.087 (at node 190)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.285234
	minimum = 0.162 (at node 4)
	maximum = 0.422 (at node 123)
Injected packet length average = 18.0051
Accepted packet length average = 17.9941
Total in-flight flits = 171780 (152084 measured)
latency change    = 0.668787
throughput change = 0.00708482
Class 0:
Packet latency average = 1349.63
	minimum = 22
	maximum = 4197
Network latency average = 1187.01
	minimum = 22
	maximum = 2984
Slowest packet = 14089
Flit latency average = 1545.76
	minimum = 5
	maximum = 4217
Slowest flit = 135377
Fragmentation average = 51.6658
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0263455
	minimum = 0.00566667 (at node 48)
	maximum = 0.0553333 (at node 53)
Accepted packet rate average = 0.0157708
	minimum = 0.0103333 (at node 139)
	maximum = 0.0216667 (at node 157)
Injected flit rate average = 0.474405
	minimum = 0.102 (at node 48)
	maximum = 1 (at node 123)
Accepted flit rate average= 0.284519
	minimum = 0.189667 (at node 139)
	maximum = 0.388 (at node 157)
Injected packet length average = 18.0071
Accepted packet length average = 18.0408
Total in-flight flits = 209011 (203208 measured)
latency change    = 0.416884
throughput change = 0.00251399
Class 0:
Packet latency average = 1778.56
	minimum = 22
	maximum = 4934
Network latency average = 1605.79
	minimum = 22
	maximum = 3950
Slowest packet = 14089
Flit latency average = 1729.83
	minimum = 5
	maximum = 4911
Slowest flit = 155771
Fragmentation average = 63.6855
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0263372
	minimum = 0.007 (at node 46)
	maximum = 0.05275 (at node 123)
Accepted packet rate average = 0.0157786
	minimum = 0.0105 (at node 64)
	maximum = 0.0205 (at node 123)
Injected flit rate average = 0.474254
	minimum = 0.126 (at node 46)
	maximum = 0.9535 (at node 123)
Accepted flit rate average= 0.284333
	minimum = 0.19375 (at node 64)
	maximum = 0.36675 (at node 123)
Injected packet length average = 18.007
Accepted packet length average = 18.0201
Total in-flight flits = 245462 (244335 measured)
latency change    = 0.241165
throughput change = 0.000653331
Class 0:
Packet latency average = 2107.69
	minimum = 22
	maximum = 5304
Network latency average = 1926.74
	minimum = 22
	maximum = 4916
Slowest packet = 14089
Flit latency average = 1912.01
	minimum = 5
	maximum = 5484
Slowest flit = 193571
Fragmentation average = 74.0181
	minimum = 0
	maximum = 478
Injected packet rate average = 0.0265667
	minimum = 0.0088 (at node 46)
	maximum = 0.0518 (at node 60)
Accepted packet rate average = 0.0157604
	minimum = 0.012 (at node 80)
	maximum = 0.0204 (at node 123)
Injected flit rate average = 0.478122
	minimum = 0.1584 (at node 46)
	maximum = 0.9318 (at node 60)
Accepted flit rate average= 0.284463
	minimum = 0.2172 (at node 80)
	maximum = 0.3674 (at node 123)
Injected packet length average = 17.9971
Accepted packet length average = 18.0492
Total in-flight flits = 285732 (285568 measured)
latency change    = 0.156156
throughput change = 0.000454073
Class 0:
Packet latency average = 2384.75
	minimum = 22
	maximum = 6408
Network latency average = 2199.13
	minimum = 22
	maximum = 5761
Slowest packet = 14089
Flit latency average = 2097.66
	minimum = 5
	maximum = 6325
Slowest flit = 223235
Fragmentation average = 81.3385
	minimum = 0
	maximum = 501
Injected packet rate average = 0.0267813
	minimum = 0.0085 (at node 46)
	maximum = 0.0453333 (at node 60)
Accepted packet rate average = 0.0157856
	minimum = 0.0118333 (at node 80)
	maximum = 0.0201667 (at node 103)
Injected flit rate average = 0.482087
	minimum = 0.153 (at node 46)
	maximum = 0.816 (at node 170)
Accepted flit rate average= 0.284946
	minimum = 0.213 (at node 80)
	maximum = 0.361167 (at node 103)
Injected packet length average = 18.0009
Accepted packet length average = 18.051
Total in-flight flits = 326822 (326822 measured)
latency change    = 0.116182
throughput change = 0.00169745
Class 0:
Packet latency average = 2634.3
	minimum = 22
	maximum = 6723
Network latency average = 2445.25
	minimum = 22
	maximum = 6335
Slowest packet = 14089
Flit latency average = 2283.01
	minimum = 5
	maximum = 6325
Slowest flit = 223235
Fragmentation average = 85.4301
	minimum = 0
	maximum = 501
Injected packet rate average = 0.0268036
	minimum = 0.01 (at node 46)
	maximum = 0.0461429 (at node 162)
Accepted packet rate average = 0.0158214
	minimum = 0.012 (at node 80)
	maximum = 0.0191429 (at node 50)
Injected flit rate average = 0.482461
	minimum = 0.18 (at node 46)
	maximum = 0.830857 (at node 162)
Accepted flit rate average= 0.284998
	minimum = 0.216857 (at node 80)
	maximum = 0.346714 (at node 138)
Injected packet length average = 17.9999
Accepted packet length average = 18.0134
Total in-flight flits = 365139 (365139 measured)
latency change    = 0.0947302
throughput change = 0.000181009
Draining all recorded packets ...
Class 0:
Remaining flits: 321711 321712 321713 336237 336238 336239 342684 342685 342686 342687 [...] (406638 flits)
Measured flits: 321711 321712 321713 336237 336238 336239 342684 342685 342686 342687 [...] (337701 flits)
Class 0:
Remaining flits: 360685 360686 360687 360688 360689 360690 360691 360692 360693 360694 [...] (452196 flits)
Measured flits: 360685 360686 360687 360688 360689 360690 360691 360692 360693 360694 [...] (290612 flits)
Class 0:
Remaining flits: 387900 387901 387902 387903 387904 387905 387906 387907 387908 387909 [...] (487990 flits)
Measured flits: 387900 387901 387902 387903 387904 387905 387906 387907 387908 387909 [...] (243220 flits)
Class 0:
Remaining flits: 421560 421561 421562 421563 421564 421565 421566 421567 421568 421569 [...] (524141 flits)
Measured flits: 421560 421561 421562 421563 421564 421565 421566 421567 421568 421569 [...] (196182 flits)
Class 0:
Remaining flits: 439704 439705 439706 439707 439708 439709 439710 439711 439712 439713 [...] (560614 flits)
Measured flits: 439704 439705 439706 439707 439708 439709 439710 439711 439712 439713 [...] (149244 flits)
Class 0:
Remaining flits: 449851 449852 449853 449854 449855 481824 481825 481826 481827 481828 [...] (595299 flits)
Measured flits: 449851 449852 449853 449854 449855 481824 481825 481826 481827 481828 [...] (104147 flits)
Class 0:
Remaining flits: 488413 488414 488415 488416 488417 488418 488419 488420 488421 488422 [...] (627081 flits)
Measured flits: 488413 488414 488415 488416 488417 488418 488419 488420 488421 488422 [...] (66492 flits)
Class 0:
Remaining flits: 534721 534722 534723 534724 534725 536400 536401 536402 536403 536404 [...] (667839 flits)
Measured flits: 534721 534722 534723 534724 534725 536400 536401 536402 536403 536404 [...] (39732 flits)
Class 0:
Remaining flits: 554819 554820 554821 554822 554823 554824 554825 554826 554827 554828 [...] (704185 flits)
Measured flits: 554819 554820 554821 554822 554823 554824 554825 554826 554827 554828 [...] (22218 flits)
Class 0:
Remaining flits: 665550 665551 665552 665553 665554 665555 665556 665557 665558 665559 [...] (735732 flits)
Measured flits: 665550 665551 665552 665553 665554 665555 665556 665557 665558 665559 [...] (12117 flits)
Class 0:
Remaining flits: 672318 672319 672320 672321 672322 672323 672324 672325 672326 672327 [...] (768814 flits)
Measured flits: 672318 672319 672320 672321 672322 672323 672324 672325 672326 672327 [...] (7141 flits)
Class 0:
Remaining flits: 760356 760357 760358 760359 760360 760361 760362 760363 760364 760365 [...] (805090 flits)
Measured flits: 760356 760357 760358 760359 760360 760361 760362 760363 760364 760365 [...] (3950 flits)
Class 0:
Remaining flits: 793690 793691 799668 799669 799670 799671 799672 799673 799674 799675 [...] (839887 flits)
Measured flits: 793690 793691 799668 799669 799670 799671 799672 799673 799674 799675 [...] (1902 flits)
Class 0:
Remaining flits: 844208 844209 844210 844211 844212 844213 844214 844215 844216 844217 [...] (881156 flits)
Measured flits: 844208 844209 844210 844211 844212 844213 844214 844215 844216 844217 [...] (683 flits)
Class 0:
Remaining flits: 857266 857267 858983 858984 858985 858986 858987 858988 858989 858990 [...] (924051 flits)
Measured flits: 857266 857267 858983 858984 858985 858986 858987 858988 858989 858990 [...] (252 flits)
Class 0:
Remaining flits: 889200 889201 889202 889203 889204 889205 889206 889207 889208 889209 [...] (962163 flits)
Measured flits: 889200 889201 889202 889203 889204 889205 889206 889207 889208 889209 [...] (86 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 975384 975385 975386 975387 975388 975389 975390 975391 975392 975393 [...] (939609 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 992114 992115 992116 992117 992118 992119 992120 992121 992122 992123 [...] (892184 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1038978 1038979 1038980 1038981 1038982 1038983 1038984 1038985 1038986 1038987 [...] (844521 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1096799 1096800 1096801 1096802 1096803 1096804 1096805 1096806 1096807 1096808 [...] (796862 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1148705 1210716 1210717 1210718 1210719 1210720 1210721 1210722 1210723 1210724 [...] (749126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1222577 1239879 1239880 1239881 1239882 1239883 1239884 1239885 1239886 1239887 [...] (701507 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1246626 1246627 1246628 1246629 1246630 1246631 1246632 1246633 1246634 1246635 [...] (653774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1266714 1266715 1266716 1266717 1266718 1266719 1266720 1266721 1266722 1266723 [...] (606335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1307412 1307413 1307414 1307415 1307416 1307417 1307418 1307419 1307420 1307421 [...] (558773 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1336338 1336339 1336340 1336341 1336342 1336343 1336344 1336345 1336346 1336347 [...] (511450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1347732 1347733 1347734 1347735 1347736 1347737 1347738 1347739 1347740 1347741 [...] (464023 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1372066 1372067 1387386 1387387 1387388 1387389 1387390 1387391 1387392 1387393 [...] (416341 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1409814 1409815 1409816 1409817 1409818 1409819 1409820 1409821 1409822 1409823 [...] (368585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1409830 1409831 1441293 1441294 1441295 1444558 1444559 1444560 1444561 1444562 [...] (321084 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1460700 1460701 1460702 1460703 1460704 1460705 1460706 1460707 1460708 1460709 [...] (273709 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1557468 1557469 1557470 1557471 1557472 1557473 1557474 1557475 1557476 1557477 [...] (227195 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1588230 1588231 1588232 1588233 1588234 1588235 1588236 1588237 1588238 1588239 [...] (181354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1597104 1597105 1597106 1597107 1597108 1597109 1597110 1597111 1597112 1597113 [...] (136648 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1597120 1597121 1679310 1679311 1679312 1679313 1679314 1679315 1679316 1679317 [...] (99716 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1679310 1679311 1679312 1679313 1679314 1679315 1679316 1679317 1679318 1679319 [...] (70611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1679325 1679326 1679327 1770210 1770211 1770212 1770213 1770214 1770215 1770216 [...] (49600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1802149 1802150 1802151 1802152 1802153 1802154 1802155 1802156 1802157 1802158 [...] (34101 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1821942 1821943 1821944 1821945 1821946 1821947 1821948 1821949 1821950 1821951 [...] (23403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1855710 1855711 1855712 1855713 1855714 1855715 1855716 1855717 1855718 1855719 [...] (16630 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1855710 1855711 1855712 1855713 1855714 1855715 1855716 1855717 1855718 1855719 [...] (11707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1902706 1902707 1905732 1905733 1905734 1905735 1905736 1905737 1905738 1905739 [...] (7710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2079520 2079521 2086488 2086489 2086490 2086491 2086492 2086493 2086494 2086495 [...] (4137 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2225428 2225429 2241972 2241973 2241974 2241975 2241976 2241977 2241978 2241979 [...] (1888 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2310003 2310004 2310005 2310006 2310007 2310008 2310009 2310010 2310011 2311686 [...] (566 flits)
Measured flits: (0 flits)
Time taken is 56395 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4957.23 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16871 (1 samples)
Network latency average = 4745.86 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16666 (1 samples)
Flit latency average = 10245 (1 samples)
	minimum = 5 (1 samples)
	maximum = 32306 (1 samples)
Fragmentation average = 109.002 (1 samples)
	minimum = 0 (1 samples)
	maximum = 650 (1 samples)
Injected packet rate average = 0.0268036 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0461429 (1 samples)
Accepted packet rate average = 0.0158214 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.482461 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.830857 (1 samples)
Accepted flit rate average = 0.284998 (1 samples)
	minimum = 0.216857 (1 samples)
	maximum = 0.346714 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 18.0134 (1 samples)
Hops average = 5.06903 (1 samples)
Total run time 47.7066
