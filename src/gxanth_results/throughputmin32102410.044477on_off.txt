BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 376.948
	minimum = 23
	maximum = 984
Network latency average = 271.732
	minimum = 23
	maximum = 935
Slowest packet = 46
Flit latency average = 209.552
	minimum = 6
	maximum = 950
Slowest flit = 3987
Fragmentation average = 141.569
	minimum = 0
	maximum = 858
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0104896
	minimum = 0.004 (at node 25)
	maximum = 0.019 (at node 88)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.214849
	minimum = 0.073 (at node 25)
	maximum = 0.38 (at node 88)
Injected packet length average = 17.8285
Accepted packet length average = 20.4821
Total in-flight flits = 61583 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 649.812
	minimum = 23
	maximum = 1938
Network latency average = 500.126
	minimum = 23
	maximum = 1794
Slowest packet = 131
Flit latency average = 427.753
	minimum = 6
	maximum = 1878
Slowest flit = 9648
Fragmentation average = 176.798
	minimum = 0
	maximum = 1744
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.011862
	minimum = 0.0065 (at node 174)
	maximum = 0.019 (at node 177)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.227005
	minimum = 0.129 (at node 174)
	maximum = 0.356 (at node 177)
Injected packet length average = 17.9125
Accepted packet length average = 19.1372
Total in-flight flits = 129604 (0 measured)
latency change    = 0.419912
throughput change = 0.0535505
Class 0:
Packet latency average = 1363.09
	minimum = 27
	maximum = 2924
Network latency average = 1119.27
	minimum = 23
	maximum = 2675
Slowest packet = 774
Flit latency average = 1062.74
	minimum = 6
	maximum = 2890
Slowest flit = 8851
Fragmentation average = 203.547
	minimum = 0
	maximum = 1709
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0136042
	minimum = 0.006 (at node 71)
	maximum = 0.024 (at node 51)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.247021
	minimum = 0.091 (at node 71)
	maximum = 0.437 (at node 68)
Injected packet length average = 17.9975
Accepted packet length average = 18.1577
Total in-flight flits = 204342 (0 measured)
latency change    = 0.523281
throughput change = 0.0810281
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 479.159
	minimum = 27
	maximum = 2080
Network latency average = 85.2496
	minimum = 23
	maximum = 906
Slowest packet = 18852
Flit latency average = 1490.23
	minimum = 6
	maximum = 3725
Slowest flit = 24324
Fragmentation average = 25.4652
	minimum = 0
	maximum = 368
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0139896
	minimum = 0.004 (at node 130)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.250708
	minimum = 0.072 (at node 130)
	maximum = 0.44 (at node 16)
Injected packet length average = 18.0134
Accepted packet length average = 17.9211
Total in-flight flits = 285590 (117734 measured)
latency change    = 1.84476
throughput change = 0.0147083
Class 0:
Packet latency average = 595.403
	minimum = 27
	maximum = 2283
Network latency average = 227.29
	minimum = 23
	maximum = 1994
Slowest packet = 18852
Flit latency average = 1737.37
	minimum = 6
	maximum = 4638
Slowest flit = 31823
Fragmentation average = 47.3279
	minimum = 0
	maximum = 1212
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0138203
	minimum = 0.0085 (at node 130)
	maximum = 0.0205 (at node 16)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.247763
	minimum = 0.1445 (at node 130)
	maximum = 0.4045 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 17.9275
Total in-flight flits = 363991 (228543 measured)
latency change    = 0.195236
throughput change = 0.0118876
Class 0:
Packet latency average = 838.566
	minimum = 27
	maximum = 3516
Network latency average = 481.304
	minimum = 23
	maximum = 2927
Slowest packet = 18852
Flit latency average = 1984.93
	minimum = 6
	maximum = 5618
Slowest flit = 34584
Fragmentation average = 67.7701
	minimum = 0
	maximum = 1212
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0137257
	minimum = 0.009 (at node 175)
	maximum = 0.0193333 (at node 103)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.246149
	minimum = 0.157 (at node 79)
	maximum = 0.343 (at node 103)
Injected packet length average = 18.0004
Accepted packet length average = 17.9335
Total in-flight flits = 443656 (336413 measured)
latency change    = 0.289975
throughput change = 0.00655584
Draining remaining packets ...
Class 0:
Remaining flits: 30024 30025 30026 30027 30028 30029 30030 30031 30032 30033 [...] (407773 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (323495 flits)
Class 0:
Remaining flits: 30024 30025 30026 30027 30028 30029 30030 30031 30032 30033 [...] (372190 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (307143 flits)
Class 0:
Remaining flits: 30024 30025 30026 30027 30028 30029 30030 30031 30032 30033 [...] (336685 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (288158 flits)
Class 0:
Remaining flits: 30024 30025 30026 30027 30028 30029 30030 30031 30032 30033 [...] (301915 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (265097 flits)
Class 0:
Remaining flits: 30024 30025 30026 30027 30028 30029 30030 30031 30032 30033 [...] (267013 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (239546 flits)
Class 0:
Remaining flits: 84302 84303 84304 84305 84306 84307 84308 84309 84310 84311 [...] (232129 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (212060 flits)
Class 0:
Remaining flits: 127836 127837 127838 127839 127840 127841 127842 127843 127844 127845 [...] (197979 flits)
Measured flits: 339066 339067 339068 339069 339070 339071 339072 339073 339074 339075 [...] (183063 flits)
Class 0:
Remaining flits: 127836 127837 127838 127839 127840 127841 127842 127843 127844 127845 [...] (164883 flits)
Measured flits: 339066 339067 339068 339069 339070 339071 339072 339073 339074 339075 [...] (154290 flits)
Class 0:
Remaining flits: 127836 127837 127838 127839 127840 127841 127842 127843 127844 127845 [...] (132060 flits)
Measured flits: 339081 339082 339083 339282 339283 339284 339285 339286 339287 339288 [...] (124735 flits)
Class 0:
Remaining flits: 127836 127837 127838 127839 127840 127841 127842 127843 127844 127845 [...] (99561 flits)
Measured flits: 339282 339283 339284 339285 339286 339287 339288 339289 339290 339291 [...] (94461 flits)
Class 0:
Remaining flits: 127848 127849 127850 127851 127852 127853 164826 164827 164828 164829 [...] (68362 flits)
Measured flits: 339282 339283 339284 339285 339286 339287 339288 339289 339290 339291 [...] (65244 flits)
Class 0:
Remaining flits: 174132 174133 174134 174135 174136 174137 174138 174139 174140 174141 [...] (41599 flits)
Measured flits: 339282 339283 339284 339285 339286 339287 339288 339289 339290 339291 [...] (39766 flits)
Class 0:
Remaining flits: 188018 188019 188020 188021 188022 188023 188024 188025 188026 188027 [...] (20929 flits)
Measured flits: 340519 340520 340521 340522 340523 340668 340669 340670 340671 340672 [...] (20026 flits)
Class 0:
Remaining flits: 192143 192144 192145 192146 192147 192148 192149 199962 199963 199964 [...] (6654 flits)
Measured flits: 340668 340669 340670 340671 340672 340673 340674 340675 340676 340677 [...] (6350 flits)
Class 0:
Remaining flits: 269334 269335 269336 269337 269338 269339 269340 269341 269342 269343 [...] (1725 flits)
Measured flits: 344682 344683 344684 344685 344686 344687 344688 344689 344690 344691 [...] (1620 flits)
Class 0:
Remaining flits: 413658 413659 413660 413661 413662 413663 413664 413665 413666 413667 [...] (564 flits)
Measured flits: 413658 413659 413660 413661 413662 413663 413664 413665 413666 413667 [...] (564 flits)
Time taken is 22572 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8417.32 (1 samples)
	minimum = 27 (1 samples)
	maximum = 19253 (1 samples)
Network latency average = 7994.79 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18591 (1 samples)
Flit latency average = 6904.09 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18842 (1 samples)
Fragmentation average = 182.302 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4644 (1 samples)
Injected packet rate average = 0.0367569 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0137257 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.661641 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.246149 (1 samples)
	minimum = 0.157 (1 samples)
	maximum = 0.343 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 17.9335 (1 samples)
Hops average = 5.07855 (1 samples)
Total run time 20.0038
