BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 325.489
	minimum = 22
	maximum = 951
Network latency average = 222.939
	minimum = 22
	maximum = 815
Slowest packet = 74
Flit latency average = 188.898
	minimum = 5
	maximum = 803
Slowest flit = 14631
Fragmentation average = 55.4176
	minimum = 0
	maximum = 438
Injected packet rate average = 0.0245625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 46)
Accepted packet rate average = 0.0133958
	minimum = 0.006 (at node 135)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.437453
	minimum = 0 (at node 5)
	maximum = 1 (at node 46)
Accepted flit rate average= 0.255385
	minimum = 0.108 (at node 135)
	maximum = 0.426 (at node 48)
Injected packet length average = 17.8098
Accepted packet length average = 19.0645
Total in-flight flits = 35908 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 551.36
	minimum = 22
	maximum = 1934
Network latency average = 405.299
	minimum = 22
	maximum = 1570
Slowest packet = 74
Flit latency average = 364.302
	minimum = 5
	maximum = 1553
Slowest flit = 34037
Fragmentation average = 76.0439
	minimum = 0
	maximum = 707
Injected packet rate average = 0.0230443
	minimum = 0.001 (at node 66)
	maximum = 0.046 (at node 46)
Accepted packet rate average = 0.0141302
	minimum = 0.008 (at node 164)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.41249
	minimum = 0.018 (at node 66)
	maximum = 0.827 (at node 46)
Accepted flit rate average= 0.263227
	minimum = 0.1495 (at node 164)
	maximum = 0.3945 (at node 152)
Injected packet length average = 17.8999
Accepted packet length average = 18.6286
Total in-flight flits = 58473 (0 measured)
latency change    = 0.409662
throughput change = 0.0297886
Class 0:
Packet latency average = 1136.8
	minimum = 22
	maximum = 2744
Network latency average = 897.452
	minimum = 22
	maximum = 2191
Slowest packet = 4179
Flit latency average = 841.755
	minimum = 5
	maximum = 2174
Slowest flit = 53315
Fragmentation average = 102.148
	minimum = 0
	maximum = 646
Injected packet rate average = 0.0201615
	minimum = 0 (at node 19)
	maximum = 0.043 (at node 77)
Accepted packet rate average = 0.0153854
	minimum = 0.004 (at node 138)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.362812
	minimum = 0 (at node 19)
	maximum = 0.78 (at node 77)
Accepted flit rate average= 0.275724
	minimum = 0.072 (at node 138)
	maximum = 0.491 (at node 182)
Injected packet length average = 17.9954
Accepted packet length average = 17.9211
Total in-flight flits = 75248 (0 measured)
latency change    = 0.514988
throughput change = 0.0453258
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 691.185
	minimum = 23
	maximum = 2671
Network latency average = 199.772
	minimum = 22
	maximum = 971
Slowest packet = 12762
Flit latency average = 1142.71
	minimum = 5
	maximum = 2839
Slowest flit = 97966
Fragmentation average = 21.1432
	minimum = 0
	maximum = 312
Injected packet rate average = 0.0195625
	minimum = 0.003 (at node 73)
	maximum = 0.042 (at node 37)
Accepted packet rate average = 0.015224
	minimum = 0.006 (at node 149)
	maximum = 0.033 (at node 129)
Injected flit rate average = 0.351375
	minimum = 0.037 (at node 73)
	maximum = 0.75 (at node 37)
Accepted flit rate average= 0.274938
	minimum = 0.119 (at node 149)
	maximum = 0.569 (at node 129)
Injected packet length average = 17.9617
Accepted packet length average = 18.0595
Total in-flight flits = 90284 (59611 measured)
latency change    = 0.644706
throughput change = 0.0028605
Class 0:
Packet latency average = 1362.61
	minimum = 23
	maximum = 3844
Network latency average = 820.542
	minimum = 22
	maximum = 1980
Slowest packet = 12762
Flit latency average = 1276.17
	minimum = 5
	maximum = 3263
Slowest flit = 139451
Fragmentation average = 70.6328
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0180703
	minimum = 0.0055 (at node 53)
	maximum = 0.033 (at node 187)
Accepted packet rate average = 0.015026
	minimum = 0.0095 (at node 2)
	maximum = 0.025 (at node 0)
Injected flit rate average = 0.325219
	minimum = 0.099 (at node 53)
	maximum = 0.589 (at node 187)
Accepted flit rate average= 0.271615
	minimum = 0.172 (at node 26)
	maximum = 0.4485 (at node 0)
Injected packet length average = 17.9974
Accepted packet length average = 18.0763
Total in-flight flits = 96066 (90147 measured)
latency change    = 0.492749
throughput change = 0.0122339
Class 0:
Packet latency average = 1901.15
	minimum = 23
	maximum = 4900
Network latency average = 1245.09
	minimum = 22
	maximum = 2954
Slowest packet = 12762
Flit latency average = 1388.6
	minimum = 5
	maximum = 3801
Slowest flit = 170225
Fragmentation average = 87.9333
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0173872
	minimum = 0.00533333 (at node 168)
	maximum = 0.0303333 (at node 2)
Accepted packet rate average = 0.0150781
	minimum = 0.00966667 (at node 36)
	maximum = 0.021 (at node 0)
Injected flit rate average = 0.312748
	minimum = 0.101667 (at node 168)
	maximum = 0.546667 (at node 2)
Accepted flit rate average= 0.27172
	minimum = 0.180333 (at node 36)
	maximum = 0.380667 (at node 128)
Injected packet length average = 17.9873
Accepted packet length average = 18.0208
Total in-flight flits = 99313 (98871 measured)
latency change    = 0.28327
throughput change = 0.000389749
Class 0:
Packet latency average = 2259.38
	minimum = 23
	maximum = 6060
Network latency average = 1469.27
	minimum = 22
	maximum = 3530
Slowest packet = 12762
Flit latency average = 1481.28
	minimum = 5
	maximum = 4438
Slowest flit = 180647
Fragmentation average = 94.2724
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0169023
	minimum = 0.008 (at node 168)
	maximum = 0.02675 (at node 2)
Accepted packet rate average = 0.015026
	minimum = 0.011 (at node 2)
	maximum = 0.02075 (at node 128)
Injected flit rate average = 0.304124
	minimum = 0.14475 (at node 168)
	maximum = 0.479 (at node 2)
Accepted flit rate average= 0.271146
	minimum = 0.1955 (at node 79)
	maximum = 0.366 (at node 128)
Injected packet length average = 17.993
Accepted packet length average = 18.0451
Total in-flight flits = 101026 (101008 measured)
latency change    = 0.158551
throughput change = 0.00211935
Class 0:
Packet latency average = 2547.67
	minimum = 23
	maximum = 6981
Network latency average = 1603.3
	minimum = 22
	maximum = 4591
Slowest packet = 12762
Flit latency average = 1555.5
	minimum = 5
	maximum = 4478
Slowest flit = 249972
Fragmentation average = 98.1337
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0165844
	minimum = 0.0076 (at node 176)
	maximum = 0.0236 (at node 55)
Accepted packet rate average = 0.0150406
	minimum = 0.011 (at node 171)
	maximum = 0.0194 (at node 128)
Injected flit rate average = 0.298297
	minimum = 0.1356 (at node 176)
	maximum = 0.4224 (at node 55)
Accepted flit rate average= 0.271058
	minimum = 0.2014 (at node 102)
	maximum = 0.3496 (at node 128)
Injected packet length average = 17.9866
Accepted packet length average = 18.0217
Total in-flight flits = 101952 (101952 measured)
latency change    = 0.113159
throughput change = 0.000322809
Class 0:
Packet latency average = 2815.44
	minimum = 23
	maximum = 7400
Network latency average = 1683.66
	minimum = 22
	maximum = 4608
Slowest packet = 12762
Flit latency average = 1609.25
	minimum = 5
	maximum = 4591
Slowest flit = 311507
Fragmentation average = 99.9175
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0164089
	minimum = 0.00866667 (at node 176)
	maximum = 0.0223333 (at node 177)
Accepted packet rate average = 0.0150495
	minimum = 0.0116667 (at node 102)
	maximum = 0.0198333 (at node 138)
Injected flit rate average = 0.295174
	minimum = 0.157 (at node 176)
	maximum = 0.399333 (at node 177)
Accepted flit rate average= 0.271055
	minimum = 0.209167 (at node 79)
	maximum = 0.357333 (at node 138)
Injected packet length average = 17.9887
Accepted packet length average = 18.0109
Total in-flight flits = 103661 (103661 measured)
latency change    = 0.095107
throughput change = 1.34505e-05
Class 0:
Packet latency average = 3057.02
	minimum = 23
	maximum = 8395
Network latency average = 1741.21
	minimum = 22
	maximum = 4608
Slowest packet = 12762
Flit latency average = 1653.64
	minimum = 5
	maximum = 4591
Slowest flit = 311507
Fragmentation average = 101.613
	minimum = 0
	maximum = 696
Injected packet rate average = 0.0162165
	minimum = 0.00814286 (at node 168)
	maximum = 0.0224286 (at node 2)
Accepted packet rate average = 0.0150342
	minimum = 0.0112857 (at node 79)
	maximum = 0.0191429 (at node 181)
Injected flit rate average = 0.291773
	minimum = 0.149 (at node 168)
	maximum = 0.402286 (at node 2)
Accepted flit rate average= 0.270801
	minimum = 0.201857 (at node 79)
	maximum = 0.344571 (at node 181)
Injected packet length average = 17.9923
Accepted packet length average = 18.0123
Total in-flight flits = 104088 (104088 measured)
latency change    = 0.0790245
throughput change = 0.0009383
Draining all recorded packets ...
Class 0:
Remaining flits: 427416 427417 427418 427419 427420 427421 427422 427423 427424 427425 [...] (107171 flits)
Measured flits: 427416 427417 427418 427419 427420 427421 427422 427423 427424 427425 [...] (103064 flits)
Class 0:
Remaining flits: 460383 460384 460385 484279 484280 484281 484282 484283 484284 484285 [...] (106794 flits)
Measured flits: 460383 460384 460385 484279 484280 484281 484282 484283 484284 484285 [...] (96222 flits)
Class 0:
Remaining flits: 537698 537699 537700 537701 537702 537703 537704 537705 537706 537707 [...] (105245 flits)
Measured flits: 537698 537699 537700 537701 537702 537703 537704 537705 537706 537707 [...] (86580 flits)
Class 0:
Remaining flits: 559864 559865 559866 559867 559868 559869 559870 559871 567357 567358 [...] (106632 flits)
Measured flits: 559864 559865 559866 559867 559868 559869 559870 559871 567357 567358 [...] (78016 flits)
Class 0:
Remaining flits: 611100 611101 611102 611103 611104 611105 611106 611107 611108 611109 [...] (105947 flits)
Measured flits: 611100 611101 611102 611103 611104 611105 611106 611107 611108 611109 [...] (68308 flits)
Class 0:
Remaining flits: 684518 684519 684520 684521 688932 688933 688934 688935 688936 688937 [...] (106724 flits)
Measured flits: 684518 684519 684520 684521 688932 688933 688934 688935 688936 688937 [...] (57394 flits)
Class 0:
Remaining flits: 696060 696061 696062 696063 696064 696065 696066 696067 696068 696069 [...] (105961 flits)
Measured flits: 696060 696061 696062 696063 696064 696065 696066 696067 696068 696069 [...] (46533 flits)
Class 0:
Remaining flits: 733753 733754 733755 733756 733757 733758 733759 733760 733761 733762 [...] (104304 flits)
Measured flits: 733753 733754 733755 733756 733757 733758 733759 733760 733761 733762 [...] (37574 flits)
Class 0:
Remaining flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (105743 flits)
Measured flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (28767 flits)
Class 0:
Remaining flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (105297 flits)
Measured flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (23030 flits)
Class 0:
Remaining flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (103459 flits)
Measured flits: 753930 753931 753932 753933 753934 753935 753936 753937 753938 753939 [...] (16963 flits)
Class 0:
Remaining flits: 884484 884485 884486 884487 884488 884489 884490 884491 884492 884493 [...] (105858 flits)
Measured flits: 939815 949383 949384 949385 949386 949387 949388 949389 949390 949391 [...] (12550 flits)
Class 0:
Remaining flits: 949846 949847 949848 949849 949850 949851 949852 949853 949854 949855 [...] (103598 flits)
Measured flits: 1029211 1029212 1029213 1029214 1029215 1029216 1029217 1029218 1029219 1029220 [...] (7729 flits)
Class 0:
Remaining flits: 1013832 1013833 1013834 1013835 1013836 1013837 1013838 1013839 1013840 1013841 [...] (103864 flits)
Measured flits: 1068292 1068293 1068294 1068295 1068296 1068297 1068298 1068299 1094022 1094023 [...] (5212 flits)
Class 0:
Remaining flits: 1013832 1013833 1013834 1013835 1013836 1013837 1013838 1013839 1013840 1013841 [...] (105480 flits)
Measured flits: 1096596 1096597 1096598 1096599 1096600 1096601 1096602 1096603 1096604 1096605 [...] (4308 flits)
Class 0:
Remaining flits: 1013832 1013833 1013834 1013835 1013836 1013837 1013838 1013839 1013840 1013841 [...] (104363 flits)
Measured flits: 1197630 1197631 1197632 1197633 1197634 1197635 1197636 1197637 1197638 1197639 [...] (3876 flits)
Class 0:
Remaining flits: 1131570 1131571 1131572 1131573 1131574 1131575 1131576 1131577 1131578 1131579 [...] (104743 flits)
Measured flits: 1202526 1202527 1202528 1202529 1202530 1202531 1202532 1202533 1202534 1202535 [...] (3141 flits)
Class 0:
Remaining flits: 1159863 1159864 1159865 1175436 1175437 1175438 1175439 1175440 1175441 1175442 [...] (103388 flits)
Measured flits: 1249686 1249687 1249688 1249689 1249690 1249691 1249692 1249693 1249694 1249695 [...] (2845 flits)
Class 0:
Remaining flits: 1272389 1272390 1272391 1272392 1272393 1272394 1272395 1272396 1272397 1272398 [...] (103048 flits)
Measured flits: 1290852 1290853 1290854 1290855 1290856 1290857 1290858 1290859 1290860 1290861 [...] (2248 flits)
Class 0:
Remaining flits: 1306854 1306855 1306856 1306857 1306858 1306859 1306860 1306861 1306862 1306863 [...] (104295 flits)
Measured flits: 1506690 1506691 1506692 1506693 1506694 1506695 1506696 1506697 1506698 1506699 [...] (1978 flits)
Class 0:
Remaining flits: 1359467 1402157 1402158 1402159 1402160 1402161 1402162 1402163 1408122 1408123 [...] (104585 flits)
Measured flits: 1506690 1506691 1506692 1506693 1506694 1506695 1506696 1506697 1506698 1506699 [...] (1521 flits)
Class 0:
Remaining flits: 1446780 1446781 1446782 1446783 1446784 1446785 1451844 1451845 1451846 1451847 [...] (103403 flits)
Measured flits: 1533150 1533151 1533152 1533153 1533154 1533155 1533156 1533157 1533158 1533159 [...] (1094 flits)
Class 0:
Remaining flits: 1467333 1467334 1467335 1467336 1467337 1467338 1467339 1467340 1467341 1550097 [...] (104175 flits)
Measured flits: 1616463 1616464 1616465 1616466 1616467 1616468 1616469 1616470 1616471 1694466 [...] (681 flits)
Class 0:
Remaining flits: 1558566 1558567 1558568 1558569 1558570 1558571 1558572 1558573 1558574 1558575 [...] (105116 flits)
Measured flits: 1694466 1694467 1694468 1694469 1694470 1694471 1694472 1694473 1694474 1694475 [...] (328 flits)
Class 0:
Remaining flits: 1599300 1599301 1599302 1599303 1599304 1599305 1599306 1599307 1599308 1599309 [...] (103071 flits)
Measured flits: 1754856 1754857 1754858 1754859 1754860 1754861 1754862 1754863 1754864 1754865 [...] (69 flits)
Class 0:
Remaining flits: 1662624 1662625 1662626 1662627 1662628 1662629 1662630 1662631 1662632 1662633 [...] (102652 flits)
Measured flits: 1754856 1754857 1754858 1754859 1754860 1754861 1754862 1754863 1754864 1754865 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1743000 1743001 1743002 1743003 1743004 1743005 1743006 1743007 1743008 1743009 [...] (54096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1744184 1744185 1744186 1744187 1744188 1744189 1744190 1744191 1744192 1744193 [...] (13735 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1798128 1798129 1798130 1798131 1798132 1798133 1798134 1798135 1798136 1798137 [...] (1727 flits)
Measured flits: (0 flits)
Time taken is 40173 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6016.93 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26668 (1 samples)
Network latency average = 1955.68 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9471 (1 samples)
Flit latency average = 1937.39 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9454 (1 samples)
Fragmentation average = 116.45 (1 samples)
	minimum = 0 (1 samples)
	maximum = 696 (1 samples)
Injected packet rate average = 0.0162165 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0224286 (1 samples)
Accepted packet rate average = 0.0150342 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.291773 (1 samples)
	minimum = 0.149 (1 samples)
	maximum = 0.402286 (1 samples)
Accepted flit rate average = 0.270801 (1 samples)
	minimum = 0.201857 (1 samples)
	maximum = 0.344571 (1 samples)
Injected packet size average = 17.9923 (1 samples)
Accepted packet size average = 18.0123 (1 samples)
Hops average = 5.05503 (1 samples)
Total run time 77.4129
