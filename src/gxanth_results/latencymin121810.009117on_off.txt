BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 189.585
	minimum = 23
	maximum = 888
Network latency average = 140.768
	minimum = 23
	maximum = 763
Slowest packet = 262
Flit latency average = 103.686
	minimum = 6
	maximum = 724
Slowest flit = 8261
Fragmentation average = 38.1232
	minimum = 0
	maximum = 98
Injected packet rate average = 0.00890104
	minimum = 0 (at node 70)
	maximum = 0.028 (at node 84)
Accepted packet rate average = 0.00739583
	minimum = 0.001 (at node 41)
	maximum = 0.014 (at node 48)
Injected flit rate average = 0.158786
	minimum = 0 (at node 70)
	maximum = 0.504 (at node 84)
Accepted flit rate average= 0.13725
	minimum = 0.018 (at node 41)
	maximum = 0.258 (at node 165)
Injected packet length average = 17.8391
Accepted packet length average = 18.5577
Total in-flight flits = 4608 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 248.552
	minimum = 23
	maximum = 1477
Network latency average = 173.366
	minimum = 23
	maximum = 1222
Slowest packet = 262
Flit latency average = 133.203
	minimum = 6
	maximum = 1192
Slowest flit = 19349
Fragmentation average = 41.791
	minimum = 0
	maximum = 116
Injected packet rate average = 0.00871094
	minimum = 0 (at node 183)
	maximum = 0.0215 (at node 136)
Accepted packet rate average = 0.00777344
	minimum = 0.003 (at node 174)
	maximum = 0.013 (at node 44)
Injected flit rate average = 0.156034
	minimum = 0 (at node 183)
	maximum = 0.387 (at node 136)
Accepted flit rate average= 0.142349
	minimum = 0.0575 (at node 174)
	maximum = 0.234 (at node 44)
Injected packet length average = 17.9124
Accepted packet length average = 18.3122
Total in-flight flits = 5782 (0 measured)
latency change    = 0.237243
throughput change = 0.0358201
Class 0:
Packet latency average = 359.321
	minimum = 23
	maximum = 2000
Network latency average = 245.315
	minimum = 23
	maximum = 1166
Slowest packet = 3063
Flit latency average = 198.958
	minimum = 6
	maximum = 1149
Slowest flit = 42011
Fragmentation average = 49.3812
	minimum = 0
	maximum = 123
Injected packet rate average = 0.00904167
	minimum = 0 (at node 5)
	maximum = 0.022 (at node 41)
Accepted packet rate average = 0.00830729
	minimum = 0.002 (at node 153)
	maximum = 0.017 (at node 179)
Injected flit rate average = 0.161552
	minimum = 0 (at node 5)
	maximum = 0.396 (at node 41)
Accepted flit rate average= 0.149688
	minimum = 0.036 (at node 153)
	maximum = 0.292 (at node 179)
Injected packet length average = 17.8675
Accepted packet length average = 18.0188
Total in-flight flits = 8488 (0 measured)
latency change    = 0.308272
throughput change = 0.0490257
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 417.145
	minimum = 24
	maximum = 2224
Network latency average = 236.366
	minimum = 23
	maximum = 968
Slowest packet = 5156
Flit latency average = 257.204
	minimum = 6
	maximum = 1488
Slowest flit = 55583
Fragmentation average = 50.4807
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00871354
	minimum = 0 (at node 4)
	maximum = 0.026 (at node 78)
Accepted packet rate average = 0.00850521
	minimum = 0.001 (at node 184)
	maximum = 0.015 (at node 24)
Injected flit rate average = 0.156594
	minimum = 0 (at node 4)
	maximum = 0.468 (at node 78)
Accepted flit rate average= 0.152802
	minimum = 0.018 (at node 184)
	maximum = 0.27 (at node 119)
Injected packet length average = 17.9713
Accepted packet length average = 17.9657
Total in-flight flits = 9300 (9226 measured)
latency change    = 0.138618
throughput change = 0.0203831
Class 0:
Packet latency average = 509.095
	minimum = 23
	maximum = 2333
Network latency average = 287.257
	minimum = 23
	maximum = 1304
Slowest packet = 5156
Flit latency average = 264.906
	minimum = 6
	maximum = 1633
Slowest flit = 86597
Fragmentation average = 52.3126
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00867708
	minimum = 0.001 (at node 77)
	maximum = 0.022 (at node 98)
Accepted packet rate average = 0.0086224
	minimum = 0.003 (at node 184)
	maximum = 0.0145 (at node 56)
Injected flit rate average = 0.156367
	minimum = 0.018 (at node 77)
	maximum = 0.394 (at node 98)
Accepted flit rate average= 0.155174
	minimum = 0.0585 (at node 184)
	maximum = 0.261 (at node 56)
Injected packet length average = 18.0207
Accepted packet length average = 17.9967
Total in-flight flits = 8877 (8877 measured)
latency change    = 0.180615
throughput change = 0.0152886
Class 0:
Packet latency average = 557.477
	minimum = 23
	maximum = 3031
Network latency average = 301.215
	minimum = 23
	maximum = 1977
Slowest packet = 5156
Flit latency average = 267.737
	minimum = 6
	maximum = 1941
Slowest flit = 105839
Fragmentation average = 52.6897
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00863368
	minimum = 0.000666667 (at node 77)
	maximum = 0.0186667 (at node 98)
Accepted packet rate average = 0.00862153
	minimum = 0.00433333 (at node 190)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.155568
	minimum = 0.012 (at node 77)
	maximum = 0.336 (at node 98)
Accepted flit rate average= 0.155339
	minimum = 0.078 (at node 190)
	maximum = 0.268 (at node 16)
Injected packet length average = 18.0187
Accepted packet length average = 18.0175
Total in-flight flits = 8653 (8653 measured)
latency change    = 0.0867867
throughput change = 0.00105616
Class 0:
Packet latency average = 585.917
	minimum = 23
	maximum = 3174
Network latency average = 306.631
	minimum = 23
	maximum = 1977
Slowest packet = 5156
Flit latency average = 267.386
	minimum = 6
	maximum = 1941
Slowest flit = 105839
Fragmentation average = 52.9464
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00869792
	minimum = 0.0015 (at node 77)
	maximum = 0.01575 (at node 98)
Accepted packet rate average = 0.00867448
	minimum = 0.00525 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.156551
	minimum = 0.027 (at node 77)
	maximum = 0.2825 (at node 98)
Accepted flit rate average= 0.156022
	minimum = 0.09125 (at node 4)
	maximum = 0.248 (at node 16)
Injected packet length average = 17.9987
Accepted packet length average = 17.9863
Total in-flight flits = 9011 (9011 measured)
latency change    = 0.0485394
throughput change = 0.00438139
Class 0:
Packet latency average = 596.149
	minimum = 23
	maximum = 3934
Network latency average = 309.278
	minimum = 23
	maximum = 1977
Slowest packet = 5156
Flit latency average = 267.835
	minimum = 6
	maximum = 1941
Slowest flit = 105839
Fragmentation average = 52.097
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00873646
	minimum = 0.0018 (at node 77)
	maximum = 0.0152 (at node 66)
Accepted packet rate average = 0.0087
	minimum = 0.0052 (at node 4)
	maximum = 0.0128 (at node 16)
Injected flit rate average = 0.157274
	minimum = 0.0324 (at node 77)
	maximum = 0.2736 (at node 66)
Accepted flit rate average= 0.156682
	minimum = 0.0934 (at node 4)
	maximum = 0.2272 (at node 16)
Injected packet length average = 18.002
Accepted packet length average = 18.0095
Total in-flight flits = 9093 (9093 measured)
latency change    = 0.0171633
throughput change = 0.00421334
Class 0:
Packet latency average = 596.199
	minimum = 23
	maximum = 4077
Network latency average = 306.889
	minimum = 23
	maximum = 1977
Slowest packet = 5156
Flit latency average = 264.567
	minimum = 6
	maximum = 1941
Slowest flit = 105839
Fragmentation average = 51.5836
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00872049
	minimum = 0.00266667 (at node 45)
	maximum = 0.0146667 (at node 168)
Accepted packet rate average = 0.00869618
	minimum = 0.00583333 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.157093
	minimum = 0.048 (at node 45)
	maximum = 0.263 (at node 168)
Accepted flit rate average= 0.156537
	minimum = 0.106 (at node 36)
	maximum = 0.216 (at node 128)
Injected packet length average = 18.0142
Accepted packet length average = 18.0007
Total in-flight flits = 9309 (9309 measured)
latency change    = 8.52431e-05
throughput change = 0.000926075
Draining all recorded packets ...
Class 0:
Remaining flits: 227682 227683 227684 227685 227686 227687 227688 227689 227690 227691 [...] (10055 flits)
Measured flits: 227682 227683 227684 227685 227686 227687 227688 227689 227690 227691 [...] (3322 flits)
Class 0:
Remaining flits: 282222 282223 282224 282225 282226 282227 282228 282229 282230 282231 [...] (10967 flits)
Measured flits: 282222 282223 282224 282225 282226 282227 282228 282229 282230 282231 [...] (1247 flits)
Class 0:
Remaining flits: 312174 312175 312176 312177 312178 312179 312180 312181 312182 312183 [...] (11337 flits)
Measured flits: 321336 321337 321338 321339 321340 321341 321342 321343 321344 321345 [...] (404 flits)
Class 0:
Remaining flits: 337404 337405 337406 337407 337408 337409 337842 337843 337844 337845 [...] (10845 flits)
Measured flits: 358062 358063 358064 358065 358066 358067 358068 358069 358070 358071 [...] (12 flits)
Draining remaining packets ...
Time taken is 13659 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 716.99 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4320 (1 samples)
Network latency average = 327.538 (1 samples)
	minimum = 23 (1 samples)
	maximum = 2638 (1 samples)
Flit latency average = 287.24 (1 samples)
	minimum = 6 (1 samples)
	maximum = 2573 (1 samples)
Fragmentation average = 51.9801 (1 samples)
	minimum = 0 (1 samples)
	maximum = 148 (1 samples)
Injected packet rate average = 0.00872049 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.0146667 (1 samples)
Accepted packet rate average = 0.00869618 (1 samples)
	minimum = 0.00583333 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.157093 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.263 (1 samples)
Accepted flit rate average = 0.156537 (1 samples)
	minimum = 0.106 (1 samples)
	maximum = 0.216 (1 samples)
Injected packet size average = 18.0142 (1 samples)
Accepted packet size average = 18.0007 (1 samples)
Hops average = 5.05515 (1 samples)
Total run time 7.03399
