BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 307.414
	minimum = 22
	maximum = 872
Network latency average = 288.725
	minimum = 22
	maximum = 849
Slowest packet = 1021
Flit latency average = 251.998
	minimum = 5
	maximum = 873
Slowest flit = 13277
Fragmentation average = 64.3367
	minimum = 0
	maximum = 336
Injected packet rate average = 0.0292917
	minimum = 0.016 (at node 160)
	maximum = 0.039 (at node 61)
Accepted packet rate average = 0.0143542
	minimum = 0.006 (at node 9)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.521068
	minimum = 0.288 (at node 160)
	maximum = 0.7 (at node 121)
Accepted flit rate average= 0.271052
	minimum = 0.126 (at node 150)
	maximum = 0.448 (at node 44)
Injected packet length average = 17.7889
Accepted packet length average = 18.8832
Total in-flight flits = 50036 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 608.804
	minimum = 22
	maximum = 1709
Network latency average = 532.896
	minimum = 22
	maximum = 1675
Slowest packet = 1313
Flit latency average = 488.278
	minimum = 5
	maximum = 1658
Slowest flit = 37115
Fragmentation average = 67.2044
	minimum = 0
	maximum = 336
Injected packet rate average = 0.0222708
	minimum = 0.0095 (at node 160)
	maximum = 0.0295 (at node 22)
Accepted packet rate average = 0.0145729
	minimum = 0.0075 (at node 153)
	maximum = 0.021 (at node 166)
Injected flit rate average = 0.398411
	minimum = 0.171 (at node 160)
	maximum = 0.524 (at node 22)
Accepted flit rate average= 0.267737
	minimum = 0.1495 (at node 153)
	maximum = 0.378 (at node 166)
Injected packet length average = 17.8894
Accepted packet length average = 18.3722
Total in-flight flits = 52241 (0 measured)
latency change    = 0.495052
throughput change = 0.0123819
Class 0:
Packet latency average = 1503.87
	minimum = 418
	maximum = 2477
Network latency average = 993.75
	minimum = 22
	maximum = 2365
Slowest packet = 3021
Flit latency average = 936.496
	minimum = 5
	maximum = 2348
Slowest flit = 69587
Fragmentation average = 65.949
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0149375
	minimum = 0.001 (at node 74)
	maximum = 0.03 (at node 183)
Accepted packet rate average = 0.0148073
	minimum = 0.006 (at node 21)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.267906
	minimum = 0.025 (at node 74)
	maximum = 0.545 (at node 183)
Accepted flit rate average= 0.267609
	minimum = 0.107 (at node 134)
	maximum = 0.468 (at node 16)
Injected packet length average = 17.9351
Accepted packet length average = 18.0728
Total in-flight flits = 52340 (0 measured)
latency change    = 0.595175
throughput change = 0.00047683
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2038.83
	minimum = 804
	maximum = 3128
Network latency average = 367.099
	minimum = 22
	maximum = 949
Slowest packet = 11489
Flit latency average = 966.784
	minimum = 5
	maximum = 2906
Slowest flit = 91655
Fragmentation average = 37.3511
	minimum = 0
	maximum = 278
Injected packet rate average = 0.014974
	minimum = 0 (at node 126)
	maximum = 0.025 (at node 99)
Accepted packet rate average = 0.0148958
	minimum = 0.004 (at node 36)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.26938
	minimum = 0 (at node 126)
	maximum = 0.452 (at node 99)
Accepted flit rate average= 0.267661
	minimum = 0.07 (at node 36)
	maximum = 0.547 (at node 129)
Injected packet length average = 17.9899
Accepted packet length average = 17.9689
Total in-flight flits = 52771 (41362 measured)
latency change    = 0.262387
throughput change = 0.000194587
Class 0:
Packet latency average = 2573.09
	minimum = 804
	maximum = 4328
Network latency average = 762.708
	minimum = 22
	maximum = 1893
Slowest packet = 11489
Flit latency average = 973.754
	minimum = 5
	maximum = 3438
Slowest flit = 100835
Fragmentation average = 60.1676
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148724
	minimum = 0.0065 (at node 71)
	maximum = 0.0225 (at node 67)
Accepted packet rate average = 0.0148333
	minimum = 0.0085 (at node 4)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.267529
	minimum = 0.124 (at node 71)
	maximum = 0.401 (at node 67)
Accepted flit rate average= 0.267852
	minimum = 0.1505 (at node 4)
	maximum = 0.4245 (at node 129)
Injected packet length average = 17.9883
Accepted packet length average = 18.0574
Total in-flight flits = 52157 (51387 measured)
latency change    = 0.207632
throughput change = 0.000709737
Class 0:
Packet latency average = 2952.06
	minimum = 804
	maximum = 4929
Network latency average = 898.317
	minimum = 22
	maximum = 2829
Slowest packet = 11489
Flit latency average = 976.458
	minimum = 5
	maximum = 3438
Slowest flit = 100835
Fragmentation average = 68.5088
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148524
	minimum = 0.00866667 (at node 111)
	maximum = 0.022 (at node 29)
Accepted packet rate average = 0.0148212
	minimum = 0.009 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.267153
	minimum = 0.157667 (at node 111)
	maximum = 0.395 (at node 29)
Accepted flit rate average= 0.2675
	minimum = 0.156667 (at node 36)
	maximum = 0.372 (at node 129)
Injected packet length average = 17.9871
Accepted packet length average = 18.0485
Total in-flight flits = 52016 (51998 measured)
latency change    = 0.128375
throughput change = 0.00131425
Class 0:
Packet latency average = 3278.13
	minimum = 804
	maximum = 5547
Network latency average = 939.912
	minimum = 22
	maximum = 3190
Slowest packet = 11489
Flit latency average = 970.601
	minimum = 5
	maximum = 3989
Slowest flit = 190421
Fragmentation average = 70.8949
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148672
	minimum = 0.008 (at node 48)
	maximum = 0.0205 (at node 99)
Accepted packet rate average = 0.0148424
	minimum = 0.0105 (at node 36)
	maximum = 0.019 (at node 66)
Injected flit rate average = 0.267367
	minimum = 0.14325 (at node 48)
	maximum = 0.36675 (at node 99)
Accepted flit rate average= 0.267728
	minimum = 0.19575 (at node 36)
	maximum = 0.345 (at node 118)
Injected packet length average = 17.9837
Accepted packet length average = 18.038
Total in-flight flits = 51925 (51925 measured)
latency change    = 0.0994701
throughput change = 0.000851105
Class 0:
Packet latency average = 3601.53
	minimum = 804
	maximum = 6306
Network latency average = 963.185
	minimum = 22
	maximum = 4113
Slowest packet = 11489
Flit latency average = 967.607
	minimum = 5
	maximum = 4069
Slowest flit = 244871
Fragmentation average = 73.4408
	minimum = 0
	maximum = 338
Injected packet rate average = 0.0148865
	minimum = 0.0082 (at node 64)
	maximum = 0.0208 (at node 99)
Accepted packet rate average = 0.0148979
	minimum = 0.011 (at node 171)
	maximum = 0.0196 (at node 118)
Injected flit rate average = 0.268075
	minimum = 0.1476 (at node 64)
	maximum = 0.3742 (at node 99)
Accepted flit rate average= 0.268609
	minimum = 0.198 (at node 42)
	maximum = 0.3496 (at node 118)
Injected packet length average = 18.008
Accepted packet length average = 18.03
Total in-flight flits = 51803 (51803 measured)
latency change    = 0.0897944
throughput change = 0.00328176
Class 0:
Packet latency average = 3909.18
	minimum = 804
	maximum = 7022
Network latency average = 979.812
	minimum = 22
	maximum = 4113
Slowest packet = 11489
Flit latency average = 968.741
	minimum = 5
	maximum = 4069
Slowest flit = 244871
Fragmentation average = 74.6939
	minimum = 0
	maximum = 338
Injected packet rate average = 0.0148967
	minimum = 0.00883333 (at node 64)
	maximum = 0.0201667 (at node 99)
Accepted packet rate average = 0.0148845
	minimum = 0.0116667 (at node 171)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.268146
	minimum = 0.157833 (at node 64)
	maximum = 0.362667 (at node 99)
Accepted flit rate average= 0.268363
	minimum = 0.21 (at node 171)
	maximum = 0.340167 (at node 118)
Injected packet length average = 18.0003
Accepted packet length average = 18.0296
Total in-flight flits = 51814 (51814 measured)
latency change    = 0.0787001
throughput change = 0.000918636
Class 0:
Packet latency average = 4224.64
	minimum = 804
	maximum = 8125
Network latency average = 988.016
	minimum = 22
	maximum = 4187
Slowest packet = 11489
Flit latency average = 966.217
	minimum = 5
	maximum = 4170
Slowest flit = 342251
Fragmentation average = 76.4785
	minimum = 0
	maximum = 338
Injected packet rate average = 0.0148884
	minimum = 0.00885714 (at node 64)
	maximum = 0.0201429 (at node 99)
Accepted packet rate average = 0.0149189
	minimum = 0.0118571 (at node 63)
	maximum = 0.0185714 (at node 70)
Injected flit rate average = 0.268003
	minimum = 0.157286 (at node 64)
	maximum = 0.361714 (at node 99)
Accepted flit rate average= 0.268703
	minimum = 0.209714 (at node 63)
	maximum = 0.339 (at node 70)
Injected packet length average = 18.0008
Accepted packet length average = 18.0109
Total in-flight flits = 51185 (51185 measured)
latency change    = 0.074671
throughput change = 0.00126637
Draining all recorded packets ...
Class 0:
Remaining flits: 423936 423937 423938 423939 423940 423941 423942 423943 423944 423945 [...] (51684 flits)
Measured flits: 423936 423937 423938 423939 423940 423941 423942 423943 423944 423945 [...] (51684 flits)
Class 0:
Remaining flits: 467028 467029 467030 467031 467032 467033 467034 467035 467036 467037 [...] (51737 flits)
Measured flits: 467028 467029 467030 467031 467032 467033 467034 467035 467036 467037 [...] (51737 flits)
Class 0:
Remaining flits: 500256 500257 500258 500259 500260 500261 500262 500263 500264 500265 [...] (51138 flits)
Measured flits: 500256 500257 500258 500259 500260 500261 500262 500263 500264 500265 [...] (51138 flits)
Class 0:
Remaining flits: 500256 500257 500258 500259 500260 500261 500262 500263 500264 500265 [...] (50766 flits)
Measured flits: 500256 500257 500258 500259 500260 500261 500262 500263 500264 500265 [...] (50766 flits)
Class 0:
Remaining flits: 564138 564139 564140 564141 564142 564143 564144 564145 564146 564147 [...] (50516 flits)
Measured flits: 564138 564139 564140 564141 564142 564143 564144 564145 564146 564147 [...] (50516 flits)
Class 0:
Remaining flits: 585972 585973 585974 585975 585976 585977 585978 585979 585980 585981 [...] (49850 flits)
Measured flits: 585972 585973 585974 585975 585976 585977 585978 585979 585980 585981 [...] (49850 flits)
Class 0:
Remaining flits: 585972 585973 585974 585975 585976 585977 585978 585979 585980 585981 [...] (49776 flits)
Measured flits: 585972 585973 585974 585975 585976 585977 585978 585979 585980 585981 [...] (49776 flits)
Class 0:
Remaining flits: 720126 720127 720128 720129 720130 720131 720132 720133 720134 720135 [...] (49519 flits)
Measured flits: 720126 720127 720128 720129 720130 720131 720132 720133 720134 720135 [...] (49519 flits)
Class 0:
Remaining flits: 799812 799813 799814 799815 799816 799817 799818 799819 799820 799821 [...] (48597 flits)
Measured flits: 799812 799813 799814 799815 799816 799817 799818 799819 799820 799821 [...] (48453 flits)
Class 0:
Remaining flits: 844650 844651 844652 844653 844654 844655 844656 844657 844658 844659 [...] (49014 flits)
Measured flits: 844650 844651 844652 844653 844654 844655 844656 844657 844658 844659 [...] (48132 flits)
Class 0:
Remaining flits: 844650 844651 844652 844653 844654 844655 844656 844657 844658 844659 [...] (48087 flits)
Measured flits: 844650 844651 844652 844653 844654 844655 844656 844657 844658 844659 [...] (45711 flits)
Class 0:
Remaining flits: 888408 888409 888410 888411 888412 888413 888414 888415 888416 888417 [...] (48561 flits)
Measured flits: 888408 888409 888410 888411 888412 888413 888414 888415 888416 888417 [...] (42891 flits)
Class 0:
Remaining flits: 937152 937153 937154 937155 937156 937157 937158 937159 937160 937161 [...] (48709 flits)
Measured flits: 937152 937153 937154 937155 937156 937157 937158 937159 937160 937161 [...] (36175 flits)
Class 0:
Remaining flits: 937944 937945 937946 937947 937948 937949 937950 937951 937952 937953 [...] (47779 flits)
Measured flits: 937944 937945 937946 937947 937948 937949 937950 937951 937952 937953 [...] (27227 flits)
Class 0:
Remaining flits: 1012608 1012609 1012610 1012611 1012612 1012613 1012614 1012615 1012616 1012617 [...] (48763 flits)
Measured flits: 1012608 1012609 1012610 1012611 1012612 1012613 1012614 1012615 1012616 1012617 [...] (18817 flits)
Class 0:
Remaining flits: 1012608 1012609 1012610 1012611 1012612 1012613 1012614 1012615 1012616 1012617 [...] (48363 flits)
Measured flits: 1012608 1012609 1012610 1012611 1012612 1012613 1012614 1012615 1012616 1012617 [...] (13352 flits)
Class 0:
Remaining flits: 1099959 1099960 1099961 1154268 1154269 1154270 1154271 1154272 1154273 1154274 [...] (47964 flits)
Measured flits: 1099959 1099960 1099961 1154268 1154269 1154270 1154271 1154272 1154273 1154274 [...] (8580 flits)
Class 0:
Remaining flits: 1154268 1154269 1154270 1154271 1154272 1154273 1154274 1154275 1154276 1154277 [...] (48396 flits)
Measured flits: 1154268 1154269 1154270 1154271 1154272 1154273 1154274 1154275 1154276 1154277 [...] (5597 flits)
Class 0:
Remaining flits: 1154278 1154279 1154280 1154281 1154282 1154283 1154284 1154285 1301526 1301527 [...] (48375 flits)
Measured flits: 1154278 1154279 1154280 1154281 1154282 1154283 1154284 1154285 1301526 1301527 [...] (4151 flits)
Class 0:
Remaining flits: 1325664 1325665 1325666 1325667 1325668 1325669 1325670 1325671 1325672 1325673 [...] (49640 flits)
Measured flits: 1334862 1334863 1334864 1334865 1334866 1334867 1334868 1334869 1334870 1334871 [...] (2913 flits)
Class 0:
Remaining flits: 1386720 1386721 1386722 1386723 1386724 1386725 1386726 1386727 1386728 1386729 [...] (48205 flits)
Measured flits: 1450890 1450891 1450892 1450893 1450894 1450895 1450896 1450897 1450898 1450899 [...] (2547 flits)
Class 0:
Remaining flits: 1386720 1386721 1386722 1386723 1386724 1386725 1386726 1386727 1386728 1386729 [...] (48467 flits)
Measured flits: 1514448 1514449 1514450 1514451 1514452 1514453 1514454 1514455 1514456 1514457 [...] (1459 flits)
Class 0:
Remaining flits: 1449873 1449874 1449875 1449876 1449877 1449878 1449879 1449880 1449881 1452240 [...] (48205 flits)
Measured flits: 1530720 1530721 1530722 1530723 1530724 1530725 1530726 1530727 1530728 1530729 [...] (922 flits)
Class 0:
Remaining flits: 1514934 1514935 1514936 1514937 1514938 1514939 1514940 1514941 1514942 1514943 [...] (50103 flits)
Measured flits: 1546884 1546885 1546886 1546887 1546888 1546889 1546890 1546891 1546892 1546893 [...] (543 flits)
Class 0:
Remaining flits: 1526850 1526851 1526852 1526853 1526854 1526855 1526856 1526857 1526858 1526859 [...] (49045 flits)
Measured flits: 1765458 1765459 1765460 1765461 1765462 1765463 1765464 1765465 1765466 1765467 [...] (190 flits)
Class 0:
Remaining flits: 1526850 1526851 1526852 1526853 1526854 1526855 1526856 1526857 1526858 1526859 [...] (48842 flits)
Measured flits: 1780758 1780759 1780760 1780761 1780762 1780763 1780764 1780765 1780766 1780767 [...] (186 flits)
Class 0:
Remaining flits: 1648380 1648381 1648382 1648383 1648384 1648385 1698131 1698132 1698133 1698134 [...] (48786 flits)
Measured flits: 1798758 1798759 1798760 1798761 1798762 1798763 1798764 1798765 1798766 1798767 [...] (144 flits)
Class 0:
Remaining flits: 1753082 1753083 1753084 1753085 1753086 1753087 1753088 1753089 1753090 1753091 [...] (48753 flits)
Measured flits: 1798775 1898154 1898155 1898156 1898157 1898158 1898159 1898160 1898161 1898162 [...] (91 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1810656 1810657 1810658 1810659 1810660 1810661 1810662 1810663 1810664 1810665 [...] (7921 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1916244 1916245 1916246 1916247 1916248 1916249 1916250 1916251 1916252 1916253 [...] (198 flits)
Measured flits: (0 flits)
Time taken is 41107 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9229.37 (1 samples)
	minimum = 804 (1 samples)
	maximum = 29147 (1 samples)
Network latency average = 1000.08 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8265 (1 samples)
Flit latency average = 936.511 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8219 (1 samples)
Fragmentation average = 78.6255 (1 samples)
	minimum = 0 (1 samples)
	maximum = 362 (1 samples)
Injected packet rate average = 0.0148884 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0201429 (1 samples)
Accepted packet rate average = 0.0149189 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.268003 (1 samples)
	minimum = 0.157286 (1 samples)
	maximum = 0.361714 (1 samples)
Accepted flit rate average = 0.268703 (1 samples)
	minimum = 0.209714 (1 samples)
	maximum = 0.339 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 18.0109 (1 samples)
Hops average = 5.06222 (1 samples)
Total run time 55.96
