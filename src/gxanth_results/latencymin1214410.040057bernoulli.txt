BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 336.014
	minimum = 23
	maximum = 914
Network latency average = 321.08
	minimum = 23
	maximum = 890
Slowest packet = 472
Flit latency average = 290.433
	minimum = 6
	maximum = 930
Slowest flit = 7241
Fragmentation average = 54.9438
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0351042
	minimum = 0.017 (at node 125)
	maximum = 0.052 (at node 169)
Accepted packet rate average = 0.0101979
	minimum = 0.003 (at node 96)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.625271
	minimum = 0.306 (at node 125)
	maximum = 0.93 (at node 169)
Accepted flit rate average= 0.191198
	minimum = 0.054 (at node 96)
	maximum = 0.335 (at node 140)
Injected packet length average = 17.8119
Accepted packet length average = 18.7487
Total in-flight flits = 84736 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 680.363
	minimum = 23
	maximum = 1873
Network latency average = 641.17
	minimum = 23
	maximum = 1867
Slowest packet = 430
Flit latency average = 604.967
	minimum = 6
	maximum = 1850
Slowest flit = 7757
Fragmentation average = 61.4895
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0283958
	minimum = 0.0135 (at node 76)
	maximum = 0.0385 (at node 102)
Accepted packet rate average = 0.0100286
	minimum = 0.005 (at node 93)
	maximum = 0.015 (at node 152)
Injected flit rate average = 0.508375
	minimum = 0.243 (at node 76)
	maximum = 0.692 (at node 102)
Accepted flit rate average= 0.184365
	minimum = 0.099 (at node 135)
	maximum = 0.273 (at node 152)
Injected packet length average = 17.9032
Accepted packet length average = 18.3838
Total in-flight flits = 126718 (0 measured)
latency change    = 0.506126
throughput change = 0.0370642
Class 0:
Packet latency average = 1814.54
	minimum = 43
	maximum = 2753
Network latency average = 1660.19
	minimum = 27
	maximum = 2734
Slowest packet = 1326
Flit latency average = 1634.29
	minimum = 6
	maximum = 2814
Slowest flit = 22520
Fragmentation average = 70.8119
	minimum = 0
	maximum = 169
Injected packet rate average = 0.00985938
	minimum = 0 (at node 35)
	maximum = 0.028 (at node 98)
Accepted packet rate average = 0.00941667
	minimum = 0.003 (at node 20)
	maximum = 0.018 (at node 177)
Injected flit rate average = 0.180328
	minimum = 0 (at node 35)
	maximum = 0.51 (at node 98)
Accepted flit rate average= 0.168807
	minimum = 0.057 (at node 65)
	maximum = 0.307 (at node 177)
Injected packet length average = 18.29
Accepted packet length average = 17.9264
Total in-flight flits = 129497 (0 measured)
latency change    = 0.625049
throughput change = 0.0921601
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1970.09
	minimum = 752
	maximum = 2883
Network latency average = 151.211
	minimum = 31
	maximum = 848
Slowest packet = 12971
Flit latency average = 2300.03
	minimum = 6
	maximum = 3585
Slowest flit = 52311
Fragmentation average = 21.3158
	minimum = 0
	maximum = 92
Injected packet rate average = 0.007875
	minimum = 0 (at node 3)
	maximum = 0.04 (at node 150)
Accepted packet rate average = 0.00893229
	minimum = 0.002 (at node 4)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.141859
	minimum = 0 (at node 3)
	maximum = 0.71 (at node 150)
Accepted flit rate average= 0.161255
	minimum = 0.036 (at node 4)
	maximum = 0.315 (at node 78)
Injected packet length average = 18.0139
Accepted packet length average = 18.0531
Total in-flight flits = 125698 (26068 measured)
latency change    = 0.0789568
throughput change = 0.0468331
Class 0:
Packet latency average = 2665.43
	minimum = 752
	maximum = 3848
Network latency average = 558.303
	minimum = 23
	maximum = 1883
Slowest packet = 12971
Flit latency average = 2612.35
	minimum = 6
	maximum = 4584
Slowest flit = 45431
Fragmentation average = 42.3487
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0082474
	minimum = 0 (at node 72)
	maximum = 0.0295 (at node 167)
Accepted packet rate average = 0.00904948
	minimum = 0.0035 (at node 4)
	maximum = 0.0165 (at node 151)
Injected flit rate average = 0.148003
	minimum = 0 (at node 72)
	maximum = 0.531 (at node 167)
Accepted flit rate average= 0.163117
	minimum = 0.0645 (at node 4)
	maximum = 0.291 (at node 151)
Injected packet length average = 17.9454
Accepted packet length average = 18.025
Total in-flight flits = 123686 (53127 measured)
latency change    = 0.260875
throughput change = 0.011415
Class 0:
Packet latency average = 3422.83
	minimum = 752
	maximum = 4975
Network latency average = 1103.26
	minimum = 23
	maximum = 2946
Slowest packet = 12971
Flit latency average = 2834.05
	minimum = 6
	maximum = 5523
Slowest flit = 40661
Fragmentation average = 55.5017
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00836285
	minimum = 0.001 (at node 78)
	maximum = 0.0256667 (at node 167)
Accepted packet rate average = 0.00905556
	minimum = 0.005 (at node 36)
	maximum = 0.0143333 (at node 16)
Injected flit rate average = 0.150297
	minimum = 0.018 (at node 78)
	maximum = 0.457333 (at node 167)
Accepted flit rate average= 0.162885
	minimum = 0.087 (at node 36)
	maximum = 0.257667 (at node 16)
Injected packet length average = 17.972
Accepted packet length average = 17.9873
Total in-flight flits = 122129 (75780 measured)
latency change    = 0.221278
throughput change = 0.00142291
Class 0:
Packet latency average = 4056.99
	minimum = 752
	maximum = 6014
Network latency average = 1570.58
	minimum = 23
	maximum = 3932
Slowest packet = 12971
Flit latency average = 3017.83
	minimum = 6
	maximum = 6338
Slowest flit = 79878
Fragmentation average = 59.3344
	minimum = 0
	maximum = 155
Injected packet rate average = 0.008375
	minimum = 0.001 (at node 107)
	maximum = 0.02225 (at node 167)
Accepted packet rate average = 0.00901823
	minimum = 0.0045 (at node 36)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.150745
	minimum = 0.018 (at node 107)
	maximum = 0.4005 (at node 167)
Accepted flit rate average= 0.162361
	minimum = 0.07825 (at node 36)
	maximum = 0.24 (at node 16)
Injected packet length average = 17.9994
Accepted packet length average = 18.0036
Total in-flight flits = 120562 (93225 measured)
latency change    = 0.156314
throughput change = 0.00323194
Class 0:
Packet latency average = 4698.32
	minimum = 752
	maximum = 6891
Network latency average = 2011.53
	minimum = 23
	maximum = 4823
Slowest packet = 12971
Flit latency average = 3163.05
	minimum = 6
	maximum = 6981
Slowest flit = 116297
Fragmentation average = 63.2687
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00857917
	minimum = 0.0008 (at node 107)
	maximum = 0.0218 (at node 167)
Accepted packet rate average = 0.00905104
	minimum = 0.0052 (at node 36)
	maximum = 0.0126 (at node 16)
Injected flit rate average = 0.154253
	minimum = 0.0144 (at node 107)
	maximum = 0.3904 (at node 167)
Accepted flit rate average= 0.163035
	minimum = 0.0914 (at node 36)
	maximum = 0.2266 (at node 16)
Injected packet length average = 17.98
Accepted packet length average = 18.0129
Total in-flight flits = 120997 (106891 measured)
latency change    = 0.136502
throughput change = 0.00413861
Class 0:
Packet latency average = 5270.9
	minimum = 752
	maximum = 7949
Network latency average = 2360.04
	minimum = 23
	maximum = 5825
Slowest packet = 12971
Flit latency average = 3255.05
	minimum = 6
	maximum = 8045
Slowest flit = 82169
Fragmentation average = 65.1752
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00863542
	minimum = 0.00216667 (at node 107)
	maximum = 0.0226667 (at node 167)
Accepted packet rate average = 0.00905035
	minimum = 0.00566667 (at node 36)
	maximum = 0.0115 (at node 16)
Injected flit rate average = 0.155398
	minimum = 0.039 (at node 107)
	maximum = 0.408 (at node 167)
Accepted flit rate average= 0.163024
	minimum = 0.100167 (at node 36)
	maximum = 0.206333 (at node 16)
Injected packet length average = 17.9955
Accepted packet length average = 18.013
Total in-flight flits = 120721 (114100 measured)
latency change    = 0.108631
throughput change = 6.81562e-05
Class 0:
Packet latency average = 5830.48
	minimum = 752
	maximum = 8917
Network latency average = 2631.42
	minimum = 23
	maximum = 6781
Slowest packet = 12971
Flit latency average = 3320.58
	minimum = 6
	maximum = 8798
Slowest flit = 102581
Fragmentation average = 67.0393
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00872247
	minimum = 0.002 (at node 107)
	maximum = 0.022 (at node 167)
Accepted packet rate average = 0.00906473
	minimum = 0.00571429 (at node 36)
	maximum = 0.0112857 (at node 78)
Injected flit rate average = 0.156953
	minimum = 0.036 (at node 107)
	maximum = 0.395429 (at node 167)
Accepted flit rate average= 0.163202
	minimum = 0.101286 (at node 36)
	maximum = 0.204143 (at node 128)
Injected packet length average = 17.9941
Accepted packet length average = 18.0041
Total in-flight flits = 121023 (118487 measured)
latency change    = 0.0959749
throughput change = 0.00109113
Draining all recorded packets ...
Class 0:
Remaining flits: 164070 164071 164072 164073 164074 164075 164076 164077 164078 164079 [...] (121575 flits)
Measured flits: 233244 233245 233246 233247 233248 233249 233250 233251 233252 233253 [...] (120559 flits)
Class 0:
Remaining flits: 175968 175969 175970 175971 175972 175973 175974 175975 175976 175977 [...] (120955 flits)
Measured flits: 233244 233245 233246 233247 233248 233249 233250 233251 233252 233253 [...] (120349 flits)
Class 0:
Remaining flits: 177408 177409 177410 177411 177412 177413 177414 177415 177416 177417 [...] (121589 flits)
Measured flits: 233244 233245 233246 233247 233248 233249 233250 233251 233252 233253 [...] (121225 flits)
Class 0:
Remaining flits: 181098 181099 181100 181101 181102 181103 181104 181105 181106 181107 [...] (121388 flits)
Measured flits: 241632 241633 241634 241635 241636 241637 241638 241639 241640 241641 [...] (121208 flits)
Class 0:
Remaining flits: 181098 181099 181100 181101 181102 181103 181104 181105 181106 181107 [...] (124401 flits)
Measured flits: 262134 262135 262136 262137 262138 262139 262140 262141 262142 262143 [...] (124367 flits)
Class 0:
Remaining flits: 298836 298837 298838 298839 298840 298841 298842 298843 298844 298845 [...] (123393 flits)
Measured flits: 298836 298837 298838 298839 298840 298841 298842 298843 298844 298845 [...] (123393 flits)
Class 0:
Remaining flits: 317592 317593 317594 317595 317596 317597 317598 317599 317600 317601 [...] (122168 flits)
Measured flits: 317592 317593 317594 317595 317596 317597 317598 317599 317600 317601 [...] (122168 flits)
Class 0:
Remaining flits: 346374 346375 346376 346377 346378 346379 346380 346381 346382 346383 [...] (122760 flits)
Measured flits: 346374 346375 346376 346377 346378 346379 346380 346381 346382 346383 [...] (122760 flits)
Class 0:
Remaining flits: 346374 346375 346376 346377 346378 346379 346380 346381 346382 346383 [...] (123156 flits)
Measured flits: 346374 346375 346376 346377 346378 346379 346380 346381 346382 346383 [...] (123156 flits)
Class 0:
Remaining flits: 417909 417910 417911 417912 417913 417914 417915 417916 417917 417918 [...] (122610 flits)
Measured flits: 417909 417910 417911 417912 417913 417914 417915 417916 417917 417918 [...] (122610 flits)
Class 0:
Remaining flits: 440856 440857 440858 440859 440860 440861 440862 440863 440864 440865 [...] (121828 flits)
Measured flits: 440856 440857 440858 440859 440860 440861 440862 440863 440864 440865 [...] (121828 flits)
Class 0:
Remaining flits: 449514 449515 449516 449517 449518 449519 449520 449521 449522 449523 [...] (119571 flits)
Measured flits: 449514 449515 449516 449517 449518 449519 449520 449521 449522 449523 [...] (119571 flits)
Class 0:
Remaining flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (118302 flits)
Measured flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (118230 flits)
Class 0:
Remaining flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (120422 flits)
Measured flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (120152 flits)
Class 0:
Remaining flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (120208 flits)
Measured flits: 535986 535987 535988 535989 535990 535991 535992 535993 535994 535995 [...] (119794 flits)
Class 0:
Remaining flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (122204 flits)
Measured flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (121736 flits)
Class 0:
Remaining flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (121203 flits)
Measured flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (120735 flits)
Class 0:
Remaining flits: 608076 608077 608078 608079 608080 608081 608082 608083 608084 608085 [...] (120624 flits)
Measured flits: 608076 608077 608078 608079 608080 608081 608082 608083 608084 608085 [...] (120084 flits)
Class 0:
Remaining flits: 608076 608077 608078 608079 608080 608081 608082 608083 608084 608085 [...] (118015 flits)
Measured flits: 608076 608077 608078 608079 608080 608081 608082 608083 608084 608085 [...] (117296 flits)
Class 0:
Remaining flits: 608084 608085 608086 608087 608088 608089 608090 608091 608092 608093 [...] (116714 flits)
Measured flits: 608084 608085 608086 608087 608088 608089 608090 608091 608092 608093 [...] (115753 flits)
Class 0:
Remaining flits: 728010 728011 728012 728013 728014 728015 728016 728017 728018 728019 [...] (115752 flits)
Measured flits: 728010 728011 728012 728013 728014 728015 728016 728017 728018 728019 [...] (114368 flits)
Class 0:
Remaining flits: 800928 800929 800930 800931 800932 800933 800934 800935 800936 800937 [...] (117057 flits)
Measured flits: 800928 800929 800930 800931 800932 800933 800934 800935 800936 800937 [...] (115083 flits)
Class 0:
Remaining flits: 850680 850681 850682 850683 850684 850685 850686 850687 850688 850689 [...] (118571 flits)
Measured flits: 850680 850681 850682 850683 850684 850685 850686 850687 850688 850689 [...] (116187 flits)
Class 0:
Remaining flits: 850693 850694 850695 850696 850697 864936 864937 864938 864939 864940 [...] (118527 flits)
Measured flits: 850693 850694 850695 850696 850697 864936 864937 864938 864939 864940 [...] (114850 flits)
Class 0:
Remaining flits: 871974 871975 871976 871977 871978 871979 871980 871981 871982 871983 [...] (120363 flits)
Measured flits: 871974 871975 871976 871977 871978 871979 871980 871981 871982 871983 [...] (113065 flits)
Class 0:
Remaining flits: 889776 889777 889778 889779 889780 889781 889782 889783 889784 889785 [...] (119991 flits)
Measured flits: 889776 889777 889778 889779 889780 889781 889782 889783 889784 889785 [...] (107797 flits)
Class 0:
Remaining flits: 890964 890965 890966 890967 890968 890969 890970 890971 890972 890973 [...] (118389 flits)
Measured flits: 890964 890965 890966 890967 890968 890969 890970 890971 890972 890973 [...] (101468 flits)
Class 0:
Remaining flits: 957708 957709 957710 957711 957712 957713 957714 957715 957716 957717 [...] (118194 flits)
Measured flits: 957708 957709 957710 957711 957712 957713 957714 957715 957716 957717 [...] (96331 flits)
Class 0:
Remaining flits: 965785 965786 965787 965788 965789 983664 983665 983666 983667 983668 [...] (117309 flits)
Measured flits: 965785 965786 965787 965788 965789 983664 983665 983666 983667 983668 [...] (89868 flits)
Class 0:
Remaining flits: 1003374 1003375 1003376 1003377 1003378 1003379 1003380 1003381 1003382 1003383 [...] (119502 flits)
Measured flits: 1003374 1003375 1003376 1003377 1003378 1003379 1003380 1003381 1003382 1003383 [...] (84741 flits)
Class 0:
Remaining flits: 1081091 1081092 1081093 1081094 1081095 1081096 1081097 1085616 1085617 1085618 [...] (120876 flits)
Measured flits: 1081091 1081092 1081093 1081094 1081095 1081096 1081097 1085616 1085617 1085618 [...] (77965 flits)
Class 0:
Remaining flits: 1087308 1087309 1087310 1087311 1087312 1087313 1087314 1087315 1087316 1087317 [...] (119367 flits)
Measured flits: 1087308 1087309 1087310 1087311 1087312 1087313 1087314 1087315 1087316 1087317 [...] (67842 flits)
Class 0:
Remaining flits: 1105686 1105687 1105688 1105689 1105690 1105691 1105692 1105693 1105694 1105695 [...] (119512 flits)
Measured flits: 1105686 1105687 1105688 1105689 1105690 1105691 1105692 1105693 1105694 1105695 [...] (57701 flits)
Class 0:
Remaining flits: 1112076 1112077 1112078 1112079 1112080 1112081 1112082 1112083 1112084 1112085 [...] (118949 flits)
Measured flits: 1112076 1112077 1112078 1112079 1112080 1112081 1112082 1112083 1112084 1112085 [...] (47019 flits)
Class 0:
Remaining flits: 1198980 1198981 1198982 1198983 1198984 1198985 1198986 1198987 1198988 1198989 [...] (119937 flits)
Measured flits: 1198980 1198981 1198982 1198983 1198984 1198985 1198986 1198987 1198988 1198989 [...] (36362 flits)
Class 0:
Remaining flits: 1198980 1198981 1198982 1198983 1198984 1198985 1198986 1198987 1198988 1198989 [...] (119445 flits)
Measured flits: 1198980 1198981 1198982 1198983 1198984 1198985 1198986 1198987 1198988 1198989 [...] (29744 flits)
Class 0:
Remaining flits: 1251270 1251271 1251272 1251273 1251274 1251275 1251276 1251277 1251278 1251279 [...] (121798 flits)
Measured flits: 1251270 1251271 1251272 1251273 1251274 1251275 1251276 1251277 1251278 1251279 [...] (23214 flits)
Class 0:
Remaining flits: 1293570 1293571 1293572 1293573 1293574 1293575 1293576 1293577 1293578 1293579 [...] (123877 flits)
Measured flits: 1322046 1322047 1322048 1322049 1322050 1322051 1322052 1322053 1322054 1322055 [...] (17632 flits)
Class 0:
Remaining flits: 1294380 1294381 1294382 1294383 1294384 1294385 1294386 1294387 1294388 1294389 [...] (121809 flits)
Measured flits: 1326618 1326619 1326620 1326621 1326622 1326623 1326624 1326625 1326626 1326627 [...] (12866 flits)
Class 0:
Remaining flits: 1340604 1340605 1340606 1340607 1340608 1340609 1340610 1340611 1340612 1340613 [...] (119542 flits)
Measured flits: 1387692 1387693 1387694 1387695 1387696 1387697 1387698 1387699 1387700 1387701 [...] (8407 flits)
Class 0:
Remaining flits: 1342746 1342747 1342748 1342749 1342750 1342751 1342752 1342753 1342754 1342755 [...] (118229 flits)
Measured flits: 1397466 1397467 1397468 1397469 1397470 1397471 1397472 1397473 1397474 1397475 [...] (6246 flits)
Class 0:
Remaining flits: 1342746 1342747 1342748 1342749 1342750 1342751 1342752 1342753 1342754 1342755 [...] (120425 flits)
Measured flits: 1426608 1426609 1426610 1426611 1426612 1426613 1426614 1426615 1426616 1426617 [...] (4869 flits)
Class 0:
Remaining flits: 1378818 1378819 1378820 1378821 1378822 1378823 1378824 1378825 1378826 1378827 [...] (120253 flits)
Measured flits: 1435734 1435735 1435736 1435737 1435738 1435739 1435740 1435741 1435742 1435743 [...] (3056 flits)
Class 0:
Remaining flits: 1469385 1469386 1469387 1469388 1469389 1469390 1469391 1469392 1469393 1476846 [...] (117744 flits)
Measured flits: 1495818 1495819 1495820 1495821 1495822 1495823 1495824 1495825 1495826 1495827 [...] (1681 flits)
Class 0:
Remaining flits: 1496862 1496863 1496864 1496865 1496866 1496867 1496868 1496869 1496870 1496871 [...] (116922 flits)
Measured flits: 1496862 1496863 1496864 1496865 1496866 1496867 1496868 1496869 1496870 1496871 [...] (1152 flits)
Class 0:
Remaining flits: 1498170 1498171 1498172 1498173 1498174 1498175 1515420 1515421 1515422 1515423 [...] (120305 flits)
Measured flits: 1498170 1498171 1498172 1498173 1498174 1498175 1515420 1515421 1515422 1515423 [...] (695 flits)
Class 0:
Remaining flits: 1534464 1534465 1534466 1534467 1534468 1534469 1534470 1534471 1534472 1534473 [...] (122591 flits)
Measured flits: 1661472 1661473 1661474 1661475 1661476 1661477 1661478 1661479 1661480 1661481 [...] (504 flits)
Class 0:
Remaining flits: 1534464 1534465 1534466 1534467 1534468 1534469 1534470 1534471 1534472 1534473 [...] (120633 flits)
Measured flits: 1661472 1661473 1661474 1661475 1661476 1661477 1661478 1661479 1661480 1661481 [...] (504 flits)
Class 0:
Remaining flits: 1564704 1564705 1564706 1564707 1564708 1564709 1564710 1564711 1564712 1564713 [...] (117497 flits)
Measured flits: 1661472 1661473 1661474 1661475 1661476 1661477 1661478 1661479 1661480 1661481 [...] (450 flits)
Class 0:
Remaining flits: 1581786 1581787 1581788 1581789 1581790 1581791 1581792 1581793 1581794 1581795 [...] (118810 flits)
Measured flits: 1707084 1707085 1707086 1707087 1707088 1707089 1707090 1707091 1707092 1707093 [...] (397 flits)
Class 0:
Remaining flits: 1608012 1608013 1608014 1608015 1608016 1608017 1608018 1608019 1608020 1608021 [...] (120185 flits)
Measured flits: 1707084 1707085 1707086 1707087 1707088 1707089 1707090 1707091 1707092 1707093 [...] (180 flits)
Class 0:
Remaining flits: 1639998 1639999 1640000 1640001 1640002 1640003 1640004 1640005 1640006 1640007 [...] (121473 flits)
Measured flits: 1710058 1710059 1710060 1710061 1710062 1710063 1710064 1710065 1710066 1710067 [...] (104 flits)
Class 0:
Remaining flits: 1639998 1639999 1640000 1640001 1640002 1640003 1640004 1640005 1640006 1640007 [...] (121624 flits)
Measured flits: 1826540 1826541 1826542 1826543 1826544 1826545 1826546 1826547 1826548 1826549 [...] (28 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1641222 1641223 1641224 1641225 1641226 1641227 1641228 1641229 1641230 1641231 [...] (90786 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1646586 1646587 1646588 1646589 1646590 1646591 1646592 1646593 1646594 1646595 [...] (61586 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1841184 1841185 1841186 1841187 1841188 1841189 1841190 1841191 1841192 1841193 [...] (33253 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1889622 1889623 1889624 1889625 1889626 1889627 1889628 1889629 1889630 1889631 [...] (7460 flits)
Measured flits: (0 flits)
Time taken is 68082 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 20288.7 (1 samples)
	minimum = 752 (1 samples)
	maximum = 53462 (1 samples)
Network latency average = 3837.65 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14737 (1 samples)
Flit latency average = 3791.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15900 (1 samples)
Fragmentation average = 71.4402 (1 samples)
	minimum = 0 (1 samples)
	maximum = 176 (1 samples)
Injected packet rate average = 0.00872247 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.00906473 (1 samples)
	minimum = 0.00571429 (1 samples)
	maximum = 0.0112857 (1 samples)
Injected flit rate average = 0.156953 (1 samples)
	minimum = 0.036 (1 samples)
	maximum = 0.395429 (1 samples)
Accepted flit rate average = 0.163202 (1 samples)
	minimum = 0.101286 (1 samples)
	maximum = 0.204143 (1 samples)
Injected packet size average = 17.9941 (1 samples)
Accepted packet size average = 18.0041 (1 samples)
Hops average = 5.05397 (1 samples)
Total run time 52.0242
