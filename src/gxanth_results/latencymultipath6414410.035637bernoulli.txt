BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 317.699
	minimum = 23
	maximum = 961
Network latency average = 306.265
	minimum = 23
	maximum = 961
Slowest packet = 167
Flit latency average = 233.577
	minimum = 6
	maximum = 967
Slowest flit = 2162
Fragmentation average = 181.945
	minimum = 0
	maximum = 872
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.0106354
	minimum = 0.003 (at node 115)
	maximum = 0.019 (at node 76)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.227432
	minimum = 0.075 (at node 93)
	maximum = 0.377 (at node 152)
Injected packet length average = 17.8324
Accepted packet length average = 21.3844
Total in-flight flits = 78103 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.028
	minimum = 23
	maximum = 1929
Network latency average = 553.578
	minimum = 23
	maximum = 1929
Slowest packet = 321
Flit latency average = 461.555
	minimum = 6
	maximum = 1937
Slowest flit = 5373
Fragmentation average = 247.824
	minimum = 0
	maximum = 1494
Injected packet rate average = 0.0354115
	minimum = 0.0255 (at node 2)
	maximum = 0.0445 (at node 44)
Accepted packet rate average = 0.0121068
	minimum = 0.007 (at node 93)
	maximum = 0.02 (at node 136)
Injected flit rate average = 0.635091
	minimum = 0.4535 (at node 2)
	maximum = 0.7985 (at node 44)
Accepted flit rate average= 0.24124
	minimum = 0.1405 (at node 153)
	maximum = 0.383 (at node 136)
Injected packet length average = 17.9346
Accepted packet length average = 19.926
Total in-flight flits = 152128 (0 measured)
latency change    = 0.439712
throughput change = 0.0572348
Class 0:
Packet latency average = 1229.22
	minimum = 26
	maximum = 2874
Network latency average = 1211.11
	minimum = 24
	maximum = 2857
Slowest packet = 569
Flit latency average = 1128.99
	minimum = 6
	maximum = 2926
Slowest flit = 6149
Fragmentation average = 345.068
	minimum = 1
	maximum = 2123
Injected packet rate average = 0.0322344
	minimum = 0.011 (at node 44)
	maximum = 0.049 (at node 58)
Accepted packet rate average = 0.0136979
	minimum = 0.005 (at node 161)
	maximum = 0.026 (at node 172)
Injected flit rate average = 0.579656
	minimum = 0.196 (at node 148)
	maximum = 0.872 (at node 58)
Accepted flit rate average= 0.253745
	minimum = 0.101 (at node 161)
	maximum = 0.473 (at node 172)
Injected packet length average = 17.9825
Accepted packet length average = 18.5243
Total in-flight flits = 214811 (0 measured)
latency change    = 0.538709
throughput change = 0.0492826
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 187.456
	minimum = 25
	maximum = 1456
Network latency average = 88.4769
	minimum = 25
	maximum = 917
Slowest packet = 19863
Flit latency average = 1697.23
	minimum = 6
	maximum = 3863
Slowest flit = 13630
Fragmentation average = 24.7407
	minimum = 1
	maximum = 523
Injected packet rate average = 0.0306458
	minimum = 0.005 (at node 160)
	maximum = 0.055 (at node 137)
Accepted packet rate average = 0.0137865
	minimum = 0.003 (at node 6)
	maximum = 0.023 (at node 0)
Injected flit rate average = 0.551146
	minimum = 0.093 (at node 160)
	maximum = 1 (at node 137)
Accepted flit rate average= 0.249385
	minimum = 0.085 (at node 149)
	maximum = 0.395 (at node 157)
Injected packet length average = 17.9844
Accepted packet length average = 18.0892
Total in-flight flits = 272859 (97886 measured)
latency change    = 5.55737
throughput change = 0.0174805
Class 0:
Packet latency average = 398.658
	minimum = 25
	maximum = 2286
Network latency average = 237.91
	minimum = 25
	maximum = 1897
Slowest packet = 19863
Flit latency average = 2042.77
	minimum = 6
	maximum = 4849
Slowest flit = 13967
Fragmentation average = 53.6257
	minimum = 1
	maximum = 901
Injected packet rate average = 0.030388
	minimum = 0.009 (at node 36)
	maximum = 0.0495 (at node 138)
Accepted packet rate average = 0.013526
	minimum = 0.007 (at node 100)
	maximum = 0.0185 (at node 102)
Injected flit rate average = 0.546667
	minimum = 0.16 (at node 36)
	maximum = 0.89 (at node 138)
Accepted flit rate average= 0.243313
	minimum = 0.143 (at node 6)
	maximum = 0.3315 (at node 181)
Injected packet length average = 17.9895
Accepted packet length average = 17.9884
Total in-flight flits = 331421 (194150 measured)
latency change    = 0.529782
throughput change = 0.0249593
Class 0:
Packet latency average = 806.775
	minimum = 25
	maximum = 3531
Network latency average = 586.151
	minimum = 25
	maximum = 2969
Slowest packet = 19863
Flit latency average = 2380.66
	minimum = 6
	maximum = 5809
Slowest flit = 21134
Fragmentation average = 97.4387
	minimum = 1
	maximum = 1257
Injected packet rate average = 0.0301285
	minimum = 0.00966667 (at node 36)
	maximum = 0.0456667 (at node 163)
Accepted packet rate average = 0.0133802
	minimum = 0.00866667 (at node 100)
	maximum = 0.0186667 (at node 103)
Injected flit rate average = 0.542257
	minimum = 0.174333 (at node 36)
	maximum = 0.823 (at node 163)
Accepted flit rate average= 0.240378
	minimum = 0.163333 (at node 100)
	maximum = 0.340333 (at node 103)
Injected packet length average = 17.9982
Accepted packet length average = 17.9652
Total in-flight flits = 388725 (286632 measured)
latency change    = 0.505863
throughput change = 0.0122059
Class 0:
Packet latency average = 1373.07
	minimum = 25
	maximum = 4612
Network latency average = 1085.43
	minimum = 25
	maximum = 3965
Slowest packet = 19863
Flit latency average = 2699.54
	minimum = 6
	maximum = 6628
Slowest flit = 29979
Fragmentation average = 145.124
	minimum = 1
	maximum = 1399
Injected packet rate average = 0.0300859
	minimum = 0.01025 (at node 152)
	maximum = 0.04375 (at node 138)
Accepted packet rate average = 0.013194
	minimum = 0.008 (at node 52)
	maximum = 0.0185 (at node 103)
Injected flit rate average = 0.541384
	minimum = 0.1825 (at node 152)
	maximum = 0.7865 (at node 138)
Accepted flit rate average= 0.237379
	minimum = 0.158 (at node 52)
	maximum = 0.3275 (at node 103)
Injected packet length average = 17.9946
Accepted packet length average = 17.9914
Total in-flight flits = 448430 (376589 measured)
latency change    = 0.412429
throughput change = 0.0126362
Class 0:
Packet latency average = 2064.68
	minimum = 25
	maximum = 5585
Network latency average = 1694.28
	minimum = 25
	maximum = 4986
Slowest packet = 19863
Flit latency average = 2994.62
	minimum = 6
	maximum = 7602
Slowest flit = 43919
Fragmentation average = 193.916
	minimum = 1
	maximum = 2518
Injected packet rate average = 0.0296177
	minimum = 0.0082 (at node 152)
	maximum = 0.0424 (at node 163)
Accepted packet rate average = 0.013076
	minimum = 0.009 (at node 100)
	maximum = 0.0174 (at node 157)
Injected flit rate average = 0.532827
	minimum = 0.146 (at node 152)
	maximum = 0.7606 (at node 163)
Accepted flit rate average= 0.234981
	minimum = 0.1726 (at node 100)
	maximum = 0.31 (at node 157)
Injected packet length average = 17.9902
Accepted packet length average = 17.9704
Total in-flight flits = 501077 (452938 measured)
latency change    = 0.334972
throughput change = 0.0102036
Class 0:
Packet latency average = 2899.25
	minimum = 25
	maximum = 6569
Network latency average = 2451.6
	minimum = 25
	maximum = 5928
Slowest packet = 19863
Flit latency average = 3266.53
	minimum = 6
	maximum = 8559
Slowest flit = 49542
Fragmentation average = 235.171
	minimum = 1
	maximum = 2909
Injected packet rate average = 0.0284123
	minimum = 0.0095 (at node 152)
	maximum = 0.0391667 (at node 65)
Accepted packet rate average = 0.0129939
	minimum = 0.00983333 (at node 52)
	maximum = 0.017 (at node 75)
Injected flit rate average = 0.511263
	minimum = 0.169 (at node 152)
	maximum = 0.704 (at node 65)
Accepted flit rate average= 0.233124
	minimum = 0.1705 (at node 126)
	maximum = 0.301 (at node 75)
Injected packet length average = 17.9944
Accepted packet length average = 17.941
Total in-flight flits = 535572 (501614 measured)
latency change    = 0.287859
throughput change = 0.00796622
Class 0:
Packet latency average = 3624.37
	minimum = 25
	maximum = 7500
Network latency average = 3111.09
	minimum = 25
	maximum = 6955
Slowest packet = 19863
Flit latency average = 3527.73
	minimum = 6
	maximum = 9639
Slowest flit = 20681
Fragmentation average = 265.685
	minimum = 1
	maximum = 2977
Injected packet rate average = 0.0268891
	minimum = 0.0102857 (at node 152)
	maximum = 0.0352857 (at node 63)
Accepted packet rate average = 0.0128705
	minimum = 0.00957143 (at node 6)
	maximum = 0.0164286 (at node 157)
Injected flit rate average = 0.483794
	minimum = 0.184857 (at node 152)
	maximum = 0.633857 (at node 63)
Accepted flit rate average= 0.230906
	minimum = 0.175429 (at node 6)
	maximum = 0.294429 (at node 157)
Injected packet length average = 17.9922
Accepted packet length average = 17.9407
Total in-flight flits = 555083 (529282 measured)
latency change    = 0.200067
throughput change = 0.00960512
Draining all recorded packets ...
Class 0:
Remaining flits: 16848 16849 16850 16851 16852 16853 16854 16855 16856 16857 [...] (567798 flits)
Measured flits: 356184 356185 356186 356187 356188 356189 356190 356191 356192 356193 [...] (541223 flits)
Class 0:
Remaining flits: 16848 16849 16850 16851 16852 16853 16854 16855 16856 16857 [...] (577949 flits)
Measured flits: 356377 356378 356379 356380 356381 356400 356401 356402 356403 356404 [...] (540918 flits)
Class 0:
Remaining flits: 16920 16921 16922 16923 16924 16925 16926 16927 16928 16929 [...] (591706 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (532645 flits)
Class 0:
Remaining flits: 16922 16923 16924 16925 16926 16927 16928 16929 16930 16931 [...] (608486 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (515970 flits)
Class 0:
Remaining flits: 16927 16928 16929 16930 16931 16932 16933 16934 16935 16936 [...] (622832 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (493838 flits)
Class 0:
Remaining flits: 16927 16928 16929 16930 16931 16932 16933 16934 16935 16936 [...] (639161 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (470300 flits)
Class 0:
Remaining flits: 16927 16928 16929 16930 16931 16932 16933 16934 16935 16936 [...] (653625 flits)
Measured flits: 356508 356509 356510 356511 356512 356513 356514 356515 356516 356517 [...] (446500 flits)
Class 0:
Remaining flits: 61740 61741 61742 61743 61744 61745 61746 61747 61748 61749 [...] (669977 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (423614 flits)
Class 0:
Remaining flits: 61740 61741 61742 61743 61744 61745 61746 61747 61748 61749 [...] (689374 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (400839 flits)
Class 0:
Remaining flits: 61740 61741 61742 61743 61744 61745 61746 61747 61748 61749 [...] (701733 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (376610 flits)
Class 0:
Remaining flits: 83466 83467 83468 83469 83470 83471 83472 83473 83474 83475 [...] (720673 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (352838 flits)
Class 0:
Remaining flits: 90810 90811 90812 90813 90814 90815 90816 90817 90818 90819 [...] (729573 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (327764 flits)
Class 0:
Remaining flits: 93978 93979 93980 93981 93982 93983 93984 93985 93986 93987 [...] (735190 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (303901 flits)
Class 0:
Remaining flits: 114048 114049 114050 114051 114052 114053 114054 114055 114056 114057 [...] (742632 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (279675 flits)
Class 0:
Remaining flits: 137592 137593 137594 137595 137596 137597 137598 137599 137600 137601 [...] (737586 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (256790 flits)
Class 0:
Remaining flits: 137592 137593 137594 137595 137596 137597 137598 137599 137600 137601 [...] (732982 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (233674 flits)
Class 0:
Remaining flits: 137592 137593 137594 137595 137596 137597 137598 137599 137600 137601 [...] (729629 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (212341 flits)
Class 0:
Remaining flits: 137592 137593 137594 137595 137596 137597 137598 137599 137600 137601 [...] (724674 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (192343 flits)
Class 0:
Remaining flits: 203922 203923 203924 203925 203926 203927 203928 203929 203930 203931 [...] (723011 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (173032 flits)
Class 0:
Remaining flits: 214650 214651 214652 214653 214654 214655 214656 214657 214658 214659 [...] (725685 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (156401 flits)
Class 0:
Remaining flits: 214650 214651 214652 214653 214654 214655 214656 214657 214658 214659 [...] (728372 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (139611 flits)
Class 0:
Remaining flits: 214650 214651 214652 214653 214654 214655 214656 214657 214658 214659 [...] (730324 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (124936 flits)
Class 0:
Remaining flits: 228060 228061 228062 228063 228064 228065 228066 228067 228068 228069 [...] (728734 flits)
Measured flits: 362700 362701 362702 362703 362704 362705 362706 362707 362708 362709 [...] (111229 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (725329 flits)
Measured flits: 362700 362701 362702 362703 362704 362705 362706 362707 362708 362709 [...] (98832 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (723920 flits)
Measured flits: 362703 362704 362705 362706 362707 362708 362709 362710 362711 362712 [...] (88082 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (717785 flits)
Measured flits: 362703 362704 362705 362706 362707 362708 362709 362710 362711 362712 [...] (77912 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (723261 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (69245 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (726533 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (60981 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (724292 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (54107 flits)
Class 0:
Remaining flits: 291690 291691 291692 291693 291694 291695 291696 291697 291698 291699 [...] (720295 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (47002 flits)
Class 0:
Remaining flits: 291692 291693 291694 291695 291696 291697 291698 291699 291700 291701 [...] (724591 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (41164 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (729649 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (36094 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (726643 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (32015 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (727787 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (27252 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (724042 flits)
Measured flits: 363744 363745 363746 363747 363748 363749 363750 363751 363752 363753 [...] (23763 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (722980 flits)
Measured flits: 363759 363760 363761 375984 375985 375986 375987 375988 375989 375990 [...] (20491 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (724298 flits)
Measured flits: 363759 363760 363761 375984 375985 375986 375987 375988 375989 375990 [...] (18112 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (719113 flits)
Measured flits: 363759 363760 363761 375984 375985 375986 375987 375988 375989 375990 [...] (15549 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (714697 flits)
Measured flits: 375984 375985 375986 375987 375988 375989 375990 375991 375992 375993 [...] (13498 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (710078 flits)
Measured flits: 375984 375985 375986 375987 375988 375989 375990 375991 375992 375993 [...] (11537 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (713698 flits)
Measured flits: 387429 387430 387431 470556 470557 470558 470559 470560 470561 470562 [...] (9928 flits)
Class 0:
Remaining flits: 300672 300673 300674 300675 300676 300677 300678 300679 300680 300681 [...] (713971 flits)
Measured flits: 470556 470557 470558 470559 470560 470561 470562 470563 470564 470565 [...] (8755 flits)
Class 0:
Remaining flits: 470556 470557 470558 470559 470560 470561 470562 470563 470564 470565 [...] (714119 flits)
Measured flits: 470556 470557 470558 470559 470560 470561 470562 470563 470564 470565 [...] (7449 flits)
Class 0:
Remaining flits: 470556 470557 470558 470559 470560 470561 470562 470563 470564 470565 [...] (714453 flits)
Measured flits: 470556 470557 470558 470559 470560 470561 470562 470563 470564 470565 [...] (6375 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (713127 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (5499 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (710979 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (4946 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (711510 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (4020 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (713574 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (3483 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (712897 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (2979 flits)
Class 0:
Remaining flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (710781 flits)
Measured flits: 505188 505189 505190 505191 505192 505193 505194 505195 505196 505197 [...] (2617 flits)
Class 0:
Remaining flits: 505189 505190 505191 505192 505193 505194 505195 505196 505197 505198 [...] (709025 flits)
Measured flits: 505189 505190 505191 505192 505193 505194 505195 505196 505197 505198 [...] (2349 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (713153 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (1978 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (713568 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (1744 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (708448 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (1506 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (709788 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (1188 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (711090 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (1029 flits)
Class 0:
Remaining flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (708776 flits)
Measured flits: 571428 571429 571430 571431 571432 571433 571434 571435 571436 571437 [...] (831 flits)
Class 0:
Remaining flits: 798048 798049 798050 798051 798052 798053 798054 798055 798056 798057 [...] (709059 flits)
Measured flits: 798048 798049 798050 798051 798052 798053 798054 798055 798056 798057 [...] (659 flits)
Class 0:
Remaining flits: 867810 867811 867812 867813 867814 867815 917784 917785 917786 917787 [...] (709053 flits)
Measured flits: 867810 867811 867812 867813 867814 867815 917784 917785 917786 917787 [...] (518 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (710579 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (392 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (711977 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (306 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (706787 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (288 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (702431 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (218 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (699676 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (210 flits)
Class 0:
Remaining flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (697866 flits)
Measured flits: 917784 917785 917786 917787 917788 917789 917790 917791 917792 917793 [...] (163 flits)
Class 0:
Remaining flits: 918846 918847 918848 918849 918850 918851 918852 918853 918854 918855 [...] (698031 flits)
Measured flits: 918846 918847 918848 918849 918850 918851 918852 918853 918854 918855 [...] (91 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (699121 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (54 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (695773 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (54 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (697028 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (54 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (696589 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (40 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (697287 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (36 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (694835 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (36 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (691151 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (36 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (688959 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (36 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (689413 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (18 flits)
Class 0:
Remaining flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (689146 flits)
Measured flits: 1021500 1021501 1021502 1021503 1021504 1021505 1021506 1021507 1021508 1021509 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1333350 1333351 1333352 1333353 1333354 1333355 1333356 1333357 1333358 1333359 [...] (656173 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1333350 1333351 1333352 1333353 1333354 1333355 1333356 1333357 1333358 1333359 [...] (622092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1333350 1333351 1333352 1333353 1333354 1333355 1333356 1333357 1333358 1333359 [...] (588164 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1333350 1333351 1333352 1333353 1333354 1333355 1333356 1333357 1333358 1333359 [...] (554213 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (519946 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (485984 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (452489 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (419656 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (386337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473156 1473157 1473158 1473159 1473160 1473161 1473162 1473163 1473164 1473165 [...] (353322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1566667 1566668 1566669 1566670 1566671 1566672 1566673 1566674 1566675 1566676 [...] (320713 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (287728 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (255813 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (223647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (192472 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (163063 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (134232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (107166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (82949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659312 1659313 1659314 1659315 1659316 1659317 1659318 1659319 1659320 1659321 [...] (60246 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1723968 1723969 1723970 1723971 1723972 1723973 1723974 1723975 1723976 1723977 [...] (39748 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1723968 1723969 1723970 1723971 1723972 1723973 1723974 1723975 1723976 1723977 [...] (24079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845666 1845667 1845668 1845669 1845670 1845671 1845672 1845673 1845674 1845675 [...] (12853 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845683 2301840 2301841 2301842 2301843 2301844 2301845 2301846 2301847 2301848 [...] (5887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2424204 2424205 2424206 2424207 2424208 2424209 2424210 2424211 2424212 2424213 [...] (761 flits)
Measured flits: (0 flits)
Time taken is 111903 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14476.2 (1 samples)
	minimum = 25 (1 samples)
	maximum = 77629 (1 samples)
Network latency average = 12511.5 (1 samples)
	minimum = 25 (1 samples)
	maximum = 76008 (1 samples)
Flit latency average = 17241.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 84679 (1 samples)
Fragmentation average = 265.846 (1 samples)
	minimum = 0 (1 samples)
	maximum = 10167 (1 samples)
Injected packet rate average = 0.0268891 (1 samples)
	minimum = 0.0102857 (1 samples)
	maximum = 0.0352857 (1 samples)
Accepted packet rate average = 0.0128705 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0164286 (1 samples)
Injected flit rate average = 0.483794 (1 samples)
	minimum = 0.184857 (1 samples)
	maximum = 0.633857 (1 samples)
Accepted flit rate average = 0.230906 (1 samples)
	minimum = 0.175429 (1 samples)
	maximum = 0.294429 (1 samples)
Injected packet size average = 17.9922 (1 samples)
Accepted packet size average = 17.9407 (1 samples)
Hops average = 5.06002 (1 samples)
Total run time 238.283
