BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 360.877
	minimum = 23
	maximum = 991
Network latency average = 262.578
	minimum = 23
	maximum = 938
Slowest packet = 46
Flit latency average = 230.171
	minimum = 6
	maximum = 930
Slowest flit = 6255
Fragmentation average = 52.8594
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.010224
	minimum = 0.004 (at node 24)
	maximum = 0.018 (at node 132)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.193661
	minimum = 0.084 (at node 24)
	maximum = 0.332 (at node 102)
Injected packet length average = 17.8285
Accepted packet length average = 18.9419
Total in-flight flits = 65651 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 630.627
	minimum = 23
	maximum = 1932
Network latency average = 491.291
	minimum = 23
	maximum = 1786
Slowest packet = 46
Flit latency average = 457.776
	minimum = 6
	maximum = 1815
Slowest flit = 15813
Fragmentation average = 57.2752
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0315208
	minimum = 0.005 (at node 160)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0108724
	minimum = 0.0055 (at node 116)
	maximum = 0.017 (at node 63)
Injected flit rate average = 0.564422
	minimum = 0.088 (at node 160)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.199893
	minimum = 0.099 (at node 116)
	maximum = 0.314 (at node 63)
Injected packet length average = 17.9063
Accepted packet length average = 18.3854
Total in-flight flits = 141113 (0 measured)
latency change    = 0.427748
throughput change = 0.0311755
Class 0:
Packet latency average = 1467.08
	minimum = 30
	maximum = 2910
Network latency average = 1254.54
	minimum = 24
	maximum = 2591
Slowest packet = 2759
Flit latency average = 1229.77
	minimum = 6
	maximum = 2796
Slowest flit = 18435
Fragmentation average = 64.234
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0324844
	minimum = 0 (at node 33)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0108177
	minimum = 0.002 (at node 108)
	maximum = 0.021 (at node 75)
Injected flit rate average = 0.584688
	minimum = 0 (at node 33)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.194724
	minimum = 0.044 (at node 108)
	maximum = 0.398 (at node 75)
Injected packet length average = 17.999
Accepted packet length average = 18.0005
Total in-flight flits = 215992 (0 measured)
latency change    = 0.570149
throughput change = 0.0265467
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 520.205
	minimum = 28
	maximum = 2291
Network latency average = 105.135
	minimum = 28
	maximum = 975
Slowest packet = 18356
Flit latency average = 1825.43
	minimum = 6
	maximum = 3527
Slowest flit = 44030
Fragmentation average = 13.7637
	minimum = 0
	maximum = 117
Injected packet rate average = 0.0326406
	minimum = 0 (at node 158)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0107396
	minimum = 0.005 (at node 35)
	maximum = 0.022 (at node 142)
Injected flit rate average = 0.587135
	minimum = 0 (at node 158)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.192797
	minimum = 0.075 (at node 35)
	maximum = 0.401 (at node 142)
Injected packet length average = 17.9879
Accepted packet length average = 17.952
Total in-flight flits = 291799 (106501 measured)
latency change    = 1.8202
throughput change = 0.00999541
Class 0:
Packet latency average = 676.048
	minimum = 23
	maximum = 3353
Network latency average = 222.553
	minimum = 23
	maximum = 1878
Slowest packet = 18356
Flit latency average = 2123.65
	minimum = 6
	maximum = 4314
Slowest flit = 59327
Fragmentation average = 16.0803
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0329557
	minimum = 0.0045 (at node 165)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0105573
	minimum = 0.0055 (at node 35)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.593195
	minimum = 0.0835 (at node 165)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.18974
	minimum = 0.0945 (at node 35)
	maximum = 0.3365 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 17.9724
Total in-flight flits = 370940 (214210 measured)
latency change    = 0.230521
throughput change = 0.0161131
Class 0:
Packet latency average = 928.488
	minimum = 23
	maximum = 3738
Network latency average = 473.439
	minimum = 23
	maximum = 2857
Slowest packet = 18356
Flit latency average = 2459.53
	minimum = 6
	maximum = 5349
Slowest flit = 63217
Fragmentation average = 19.8751
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0322465
	minimum = 0.006 (at node 189)
	maximum = 0.0556667 (at node 11)
Accepted packet rate average = 0.0104549
	minimum = 0.006 (at node 189)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.58034
	minimum = 0.108 (at node 189)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.188
	minimum = 0.112 (at node 189)
	maximum = 0.308333 (at node 129)
Injected packet length average = 17.997
Accepted packet length average = 17.9821
Total in-flight flits = 442036 (313125 measured)
latency change    = 0.271882
throughput change = 0.0092531
Draining remaining packets ...
Class 0:
Remaining flits: 74394 74395 74396 74397 74398 74399 74400 74401 74402 74403 [...] (410801 flits)
Measured flits: 330138 330139 330140 330141 330142 330143 330144 330145 330146 330147 [...] (306735 flits)
Class 0:
Remaining flits: 80971 80972 80973 80974 80975 80976 80977 80978 80979 80980 [...] (379823 flits)
Measured flits: 330138 330139 330140 330141 330142 330143 330144 330145 330146 330147 [...] (298442 flits)
Class 0:
Remaining flits: 109758 109759 109760 109761 109762 109763 111906 111907 111908 111909 [...] (348475 flits)
Measured flits: 330138 330139 330140 330141 330142 330143 330144 330145 330146 330147 [...] (287065 flits)
Class 0:
Remaining flits: 118224 118225 118226 118227 118228 118229 118230 118231 118232 118233 [...] (317540 flits)
Measured flits: 330142 330143 330144 330145 330146 330147 330148 330149 330150 330151 [...] (272914 flits)
Class 0:
Remaining flits: 126288 126289 126290 126291 126292 126293 126294 126295 126296 126297 [...] (286351 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (255278 flits)
Class 0:
Remaining flits: 126288 126289 126290 126291 126292 126293 126294 126295 126296 126297 [...] (255861 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (234218 flits)
Class 0:
Remaining flits: 148525 148526 148527 148528 148529 148530 148531 148532 148533 148534 [...] (225875 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (211689 flits)
Class 0:
Remaining flits: 161888 161889 161890 161891 178146 178147 178148 178149 178150 178151 [...] (196652 flits)
Measured flits: 330189 330190 330191 330192 330193 330194 330195 330196 330197 330198 [...] (187802 flits)
Class 0:
Remaining flits: 184734 184735 184736 184737 184738 184739 184740 184741 184742 184743 [...] (167521 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (162150 flits)
Class 0:
Remaining flits: 187272 187273 187274 187275 187276 187277 187278 187279 187280 187281 [...] (138235 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (135129 flits)
Class 0:
Remaining flits: 187285 187286 187287 187288 187289 217530 217531 217532 217533 217534 [...] (109737 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (108128 flits)
Class 0:
Remaining flits: 255492 255493 255494 255495 255496 255497 255498 255499 255500 255501 [...] (82703 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (82004 flits)
Class 0:
Remaining flits: 264433 264434 264435 264436 264437 288774 288775 288776 288777 288778 [...] (56485 flits)
Measured flits: 331182 331183 331184 331185 331186 331187 331188 331189 331190 331191 [...] (56290 flits)
Class 0:
Remaining flits: 306039 306040 306041 306042 306043 306044 306045 306046 306047 306048 [...] (32778 flits)
Measured flits: 331182 331183 331184 331185 331186 331187 331188 331189 331190 331191 [...] (32730 flits)
Class 0:
Remaining flits: 347652 347653 347654 347655 347656 347657 347658 347659 347660 347661 [...] (13423 flits)
Measured flits: 347652 347653 347654 347655 347656 347657 347658 347659 347660 347661 [...] (13423 flits)
Class 0:
Remaining flits: 419983 419984 419985 419986 419987 419988 419989 419990 419991 419992 [...] (1185 flits)
Measured flits: 419983 419984 419985 419986 419987 419988 419989 419990 419991 419992 [...] (1185 flits)
Time taken is 22375 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10382.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 19263 (1 samples)
Network latency average = 9857.18 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18308 (1 samples)
Flit latency average = 8105.14 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18291 (1 samples)
Fragmentation average = 71.2012 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.0322465 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0104549 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.58034 (1 samples)
	minimum = 0.108 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.188 (1 samples)
	minimum = 0.112 (1 samples)
	maximum = 0.308333 (1 samples)
Injected packet size average = 17.997 (1 samples)
Accepted packet size average = 17.9821 (1 samples)
Hops average = 5.16593 (1 samples)
Total run time 22.2393
