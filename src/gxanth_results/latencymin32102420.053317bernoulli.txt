BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 329.365
	minimum = 22
	maximum = 903
Network latency average = 302.749
	minimum = 22
	maximum = 851
Slowest packet = 236
Flit latency average = 268.975
	minimum = 5
	maximum = 868
Slowest flit = 18221
Fragmentation average = 86.2543
	minimum = 0
	maximum = 392
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0151354
	minimum = 0.008 (at node 25)
	maximum = 0.026 (at node 22)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.296146
	minimum = 0.16 (at node 30)
	maximum = 0.491 (at node 44)
Injected packet length average = 17.8388
Accepted packet length average = 19.5664
Total in-flight flits = 112124 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.847
	minimum = 22
	maximum = 1765
Network latency average = 574.94
	minimum = 22
	maximum = 1701
Slowest packet = 1493
Flit latency average = 533.58
	minimum = 5
	maximum = 1749
Slowest flit = 37629
Fragmentation average = 115.052
	minimum = 0
	maximum = 506
Injected packet rate average = 0.0506302
	minimum = 0.04 (at node 59)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0163203
	minimum = 0.008 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.907271
	minimum = 0.7125 (at node 59)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.308081
	minimum = 0.1625 (at node 30)
	maximum = 0.4295 (at node 152)
Injected packet length average = 17.9196
Accepted packet length average = 18.8771
Total in-flight flits = 231653 (0 measured)
latency change    = 0.464315
throughput change = 0.0387395
Class 0:
Packet latency average = 1419.62
	minimum = 25
	maximum = 2725
Network latency average = 1347.81
	minimum = 22
	maximum = 2635
Slowest packet = 2596
Flit latency average = 1316.68
	minimum = 5
	maximum = 2618
Slowest flit = 46745
Fragmentation average = 148.249
	minimum = 0
	maximum = 436
Injected packet rate average = 0.052125
	minimum = 0.041 (at node 43)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0179063
	minimum = 0.006 (at node 190)
	maximum = 0.033 (at node 73)
Injected flit rate average = 0.938458
	minimum = 0.732 (at node 43)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.322443
	minimum = 0.112 (at node 190)
	maximum = 0.581 (at node 73)
Injected packet length average = 18.004
Accepted packet length average = 18.0073
Total in-flight flits = 349888 (0 measured)
latency change    = 0.566893
throughput change = 0.0445412
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 172.581
	minimum = 25
	maximum = 735
Network latency average = 42.2105
	minimum = 22
	maximum = 269
Slowest packet = 29475
Flit latency average = 1843.75
	minimum = 5
	maximum = 3355
Slowest flit = 90467
Fragmentation average = 7.09569
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0529427
	minimum = 0.034 (at node 163)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0182344
	minimum = 0.008 (at node 138)
	maximum = 0.031 (at node 129)
Injected flit rate average = 0.952562
	minimum = 0.607 (at node 163)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.325771
	minimum = 0.159 (at node 79)
	maximum = 0.523 (at node 129)
Injected packet length average = 17.9923
Accepted packet length average = 17.8658
Total in-flight flits = 470310 (167690 measured)
latency change    = 7.2258
throughput change = 0.0102162
Class 0:
Packet latency average = 178.665
	minimum = 24
	maximum = 741
Network latency average = 41.3542
	minimum = 22
	maximum = 299
Slowest packet = 29475
Flit latency average = 2118.45
	minimum = 5
	maximum = 3993
Slowest flit = 166800
Fragmentation average = 6.89464
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0530859
	minimum = 0.044 (at node 18)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0180599
	minimum = 0.0105 (at node 36)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.955115
	minimum = 0.7975 (at node 18)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.324656
	minimum = 0.1945 (at node 36)
	maximum = 0.4575 (at node 129)
Injected packet length average = 17.9919
Accepted packet length average = 17.9766
Total in-flight flits = 592150 (336398 measured)
latency change    = 0.03405
throughput change = 0.00343312
Class 0:
Packet latency average = 180.059
	minimum = 24
	maximum = 868
Network latency average = 41.709
	minimum = 22
	maximum = 390
Slowest packet = 29475
Flit latency average = 2387.27
	minimum = 5
	maximum = 4830
Slowest flit = 191123
Fragmentation average = 6.7949
	minimum = 0
	maximum = 53
Injected packet rate average = 0.0529236
	minimum = 0.0436667 (at node 124)
	maximum = 0.0556667 (at node 4)
Accepted packet rate average = 0.0180226
	minimum = 0.0123333 (at node 36)
	maximum = 0.0243333 (at node 129)
Injected flit rate average = 0.952755
	minimum = 0.785667 (at node 124)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.324417
	minimum = 0.216667 (at node 36)
	maximum = 0.426333 (at node 40)
Injected packet length average = 18.0025
Accepted packet length average = 18.0006
Total in-flight flits = 711736 (502701 measured)
latency change    = 0.0077416
throughput change = 0.000738505
Class 0:
Packet latency average = 183.912
	minimum = 24
	maximum = 914
Network latency average = 41.7104
	minimum = 22
	maximum = 390
Slowest packet = 29475
Flit latency average = 2663.31
	minimum = 5
	maximum = 5533
Slowest flit = 246923
Fragmentation average = 6.87533
	minimum = 0
	maximum = 53
Injected packet rate average = 0.0529583
	minimum = 0.04275 (at node 157)
	maximum = 0.05575 (at node 7)
Accepted packet rate average = 0.018013
	minimum = 0.0125 (at node 79)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.953161
	minimum = 0.772 (at node 157)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.323947
	minimum = 0.21975 (at node 79)
	maximum = 0.423 (at node 129)
Injected packet length average = 17.9983
Accepted packet length average = 17.984
Total in-flight flits = 833193 (671143 measured)
latency change    = 0.0209499
throughput change = 0.00145102
Draining all recorded packets ...
Class 0:
Remaining flits: 286128 286129 286130 286131 286132 286133 286134 286135 286136 286137 [...] (954025 flits)
Measured flits: 530100 530101 530102 530103 530104 530105 530106 530107 530108 530109 [...] (699479 flits)
Class 0:
Remaining flits: 320198 320199 320200 320201 330209 334872 334873 334874 334875 334876 [...] (1073390 flits)
Measured flits: 530100 530101 530102 530103 530104 530105 530106 530107 530108 530109 [...] (697240 flits)
Class 0:
Remaining flits: 387180 387181 387182 387183 387184 387185 387186 387187 387188 387189 [...] (1185668 flits)
Measured flits: 530100 530101 530102 530103 530104 530105 530106 530107 530108 530109 [...] (687758 flits)
Class 0:
Remaining flits: 428721 428722 428723 430002 430003 430004 430005 430006 430007 430008 [...] (1290467 flits)
Measured flits: 530229 530230 530231 530232 530233 530234 530235 530236 530237 530238 [...] (663401 flits)
Class 0:
Remaining flits: 450054 450055 450056 450057 450058 450059 450060 450061 450062 450063 [...] (1394020 flits)
Measured flits: 530676 530677 530678 530679 530680 530681 530682 530683 530684 530685 [...] (623473 flits)
Class 0:
Remaining flits: 501933 501934 501935 501936 501937 501938 501939 501940 501941 501942 [...] (1498486 flits)
Measured flits: 532522 532523 532524 532525 532526 532527 532528 532529 533880 533881 [...] (577542 flits)
Class 0:
Remaining flits: 547578 547579 547580 547581 547582 547583 547584 547585 547586 547587 [...] (1603574 flits)
Measured flits: 547578 547579 547580 547581 547582 547583 547584 547585 547586 547587 [...] (530367 flits)
Class 0:
Remaining flits: 580363 580364 580365 580366 580367 580368 580369 580370 580371 580372 [...] (1708887 flits)
Measured flits: 580363 580364 580365 580366 580367 580368 580369 580370 580371 580372 [...] (483284 flits)
Class 0:
Remaining flits: 599339 599340 599341 599342 599343 599344 599345 631177 631178 631179 [...] (1813498 flits)
Measured flits: 599339 599340 599341 599342 599343 599344 599345 631177 631178 631179 [...] (435781 flits)
Class 0:
Remaining flits: 682722 682723 682724 682725 682726 682727 682728 682729 682730 682731 [...] (1919363 flits)
Measured flits: 682722 682723 682724 682725 682726 682727 682728 682729 682730 682731 [...] (388585 flits)
Class 0:
Remaining flits: 710621 713766 713767 713768 713769 713770 713771 715716 715717 715718 [...] (2023804 flits)
Measured flits: 710621 713766 713767 713768 713769 713770 713771 715716 715717 715718 [...] (341309 flits)
Class 0:
Remaining flits: 747098 747099 747100 747101 747102 747103 747104 747105 747106 747107 [...] (2130199 flits)
Measured flits: 747098 747099 747100 747101 747102 747103 747104 747105 747106 747107 [...] (294416 flits)
Class 0:
Remaining flits: 793152 793153 793154 793155 793156 793157 793158 793159 793160 793161 [...] (2232251 flits)
Measured flits: 793152 793153 793154 793155 793156 793157 793158 793159 793160 793161 [...] (246910 flits)
Class 0:
Remaining flits: 849294 849295 849296 849297 849298 849299 849300 849301 849302 849303 [...] (2331514 flits)
Measured flits: 849294 849295 849296 849297 849298 849299 849300 849301 849302 849303 [...] (199603 flits)
Class 0:
Remaining flits: 859266 859267 859268 859269 859270 859271 859272 859273 859274 859275 [...] (2426708 flits)
Measured flits: 859266 859267 859268 859269 859270 859271 859272 859273 859274 859275 [...] (154064 flits)
Class 0:
Remaining flits: 905004 905005 905006 905007 905008 905009 905010 905011 905012 905013 [...] (2524929 flits)
Measured flits: 905004 905005 905006 905007 905008 905009 905010 905011 905012 905013 [...] (111720 flits)
Class 0:
Remaining flits: 961002 961003 961004 961005 961006 961007 961008 961009 961010 961011 [...] (2616765 flits)
Measured flits: 961002 961003 961004 961005 961006 961007 961008 961009 961010 961011 [...] (74393 flits)
Class 0:
Remaining flits: 977443 977444 977445 977446 977447 977448 977449 977450 977451 977452 [...] (2688234 flits)
Measured flits: 977443 977444 977445 977446 977447 977448 977449 977450 977451 977452 [...] (44872 flits)
Class 0:
Remaining flits: 1026252 1026253 1026254 1026255 1026256 1026257 1026258 1026259 1026260 1026261 [...] (2725812 flits)
Measured flits: 1026252 1026253 1026254 1026255 1026256 1026257 1026258 1026259 1026260 1026261 [...] (24604 flits)
Class 0:
Remaining flits: 1085040 1085041 1085042 1085043 1085044 1085045 1085046 1085047 1085048 1085049 [...] (2733033 flits)
Measured flits: 1085040 1085041 1085042 1085043 1085044 1085045 1085046 1085047 1085048 1085049 [...] (11643 flits)
Class 0:
Remaining flits: 1107882 1107883 1107884 1107885 1107886 1107887 1107888 1107889 1107890 1107891 [...] (2728504 flits)
Measured flits: 1107882 1107883 1107884 1107885 1107886 1107887 1107888 1107889 1107890 1107891 [...] (5289 flits)
Class 0:
Remaining flits: 1156644 1156645 1156646 1156647 1156648 1156649 1156650 1156651 1156652 1156653 [...] (2723242 flits)
Measured flits: 1156644 1156645 1156646 1156647 1156648 1156649 1156650 1156651 1156652 1156653 [...] (1901 flits)
Class 0:
Remaining flits: 1202950 1202951 1202952 1202953 1202954 1202955 1202956 1202957 1236024 1236025 [...] (2717746 flits)
Measured flits: 1202950 1202951 1202952 1202953 1202954 1202955 1202956 1202957 1236024 1236025 [...] (955 flits)
Class 0:
Remaining flits: 1239156 1239157 1239158 1239159 1239160 1239161 1239162 1239163 1239164 1239165 [...] (2713413 flits)
Measured flits: 1239156 1239157 1239158 1239159 1239160 1239161 1239162 1239163 1239164 1239165 [...] (467 flits)
Class 0:
Remaining flits: 1295442 1295443 1295444 1295445 1295446 1295447 1295448 1295449 1295450 1295451 [...] (2702577 flits)
Measured flits: 1350090 1350091 1350092 1350093 1350094 1350095 1350096 1350097 1350098 1350099 [...] (170 flits)
Class 0:
Remaining flits: 1301968 1301969 1301970 1301971 1301972 1301973 1301974 1301975 1351836 1351837 [...] (2699814 flits)
Measured flits: 1415754 1415755 1415756 1415757 1415758 1415759 1415760 1415761 1415762 1415763 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1400544 1400545 1400546 1400547 1400548 1400549 1400550 1400551 1400552 1400553 [...] (2649600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1488906 1488907 1488908 1488909 1488910 1488911 1488912 1488913 1488914 1488915 [...] (2598906 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1527782 1527783 1527784 1527785 1540906 1540907 1550796 1550797 1550798 1550799 [...] (2548653 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1620248 1620249 1620250 1620251 1625724 1625725 1625726 1625727 1625728 1625729 [...] (2497771 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1632237 1632238 1632239 1645164 1645165 1645166 1645167 1645168 1645169 1645170 [...] (2447505 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1667574 1667575 1667576 1667577 1667578 1667579 1667580 1667581 1667582 1667583 [...] (2396959 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1719844 1719845 1729044 1729045 1729046 1729047 1729048 1729049 1729050 1729051 [...] (2346892 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1729278 1729279 1729280 1729281 1729282 1729283 1729284 1729285 1729286 1729287 [...] (2296508 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1761174 1761175 1761176 1761177 1761178 1761179 1761180 1761181 1761182 1761183 [...] (2245592 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1853244 1853245 1853246 1853247 1853248 1853249 1853250 1853251 1853252 1853253 [...] (2194996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1853248 1853249 1853250 1853251 1853252 1853253 1853254 1853255 1853256 1853257 [...] (2144158 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1924686 1924687 1924688 1924689 1924690 1924691 1924692 1924693 1924694 1924695 [...] (2093630 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1933709 1933710 1933711 1933712 1933713 1933714 1933715 1933716 1933717 1933718 [...] (2043068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1940796 1940797 1940798 1940799 1940800 1940801 1940802 1940803 1940804 1940805 [...] (1992410 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1993806 1993807 1993808 1993809 1993810 1993811 1993812 1993813 1993814 1993815 [...] (1941866 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2026641 2026642 2026643 2026644 2026645 2026646 2026647 2026648 2026649 2026650 [...] (1891034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2061918 2061919 2061920 2061921 2061922 2061923 2061924 2061925 2061926 2061927 [...] (1840328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2061934 2061935 2123206 2123207 2124036 2124037 2124038 2124039 2124040 2124041 [...] (1789822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2124036 2124037 2124038 2124039 2124040 2124041 2124042 2124043 2124044 2124045 [...] (1739164 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2124036 2124037 2124038 2124039 2124040 2124041 2124042 2124043 2124044 2124045 [...] (1688784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2124052 2124053 2140200 2140201 2140202 2140203 2140204 2140205 2140206 2140207 [...] (1639254 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2140200 2140201 2140202 2140203 2140204 2140205 2140206 2140207 2140208 2140209 [...] (1589549 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2140200 2140201 2140202 2140203 2140204 2140205 2140206 2140207 2140208 2140209 [...] (1539942 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2180556 2180557 2180558 2180559 2180560 2180561 2180562 2180563 2180564 2180565 [...] (1491604 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2180556 2180557 2180558 2180559 2180560 2180561 2180562 2180563 2180564 2180565 [...] (1443345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2185092 2185093 2185094 2185095 2185096 2185097 2185098 2185099 2185100 2185101 [...] (1395532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2234250 2234251 2234252 2234253 2234254 2234255 2234256 2234257 2234258 2234259 [...] (1347503 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2325154 2325155 2325156 2325157 2325158 2325159 2325160 2325161 2325162 2325163 [...] (1299809 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2383344 2383345 2383346 2383347 2383348 2383349 2383350 2383351 2383352 2383353 [...] (1252024 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2421468 2421469 2421470 2421471 2421472 2421473 2421474 2421475 2421476 2421477 [...] (1203909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2430468 2430469 2430470 2430471 2430472 2430473 2430474 2430475 2430476 2430477 [...] (1156034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2430468 2430469 2430470 2430471 2430472 2430473 2430474 2430475 2430476 2430477 [...] (1108562 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2430468 2430469 2430470 2430471 2430472 2430473 2430474 2430475 2430476 2430477 [...] (1061199 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2430468 2430469 2430470 2430471 2430472 2430473 2430474 2430475 2430476 2430477 [...] (1013055 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499696 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 [...] (965792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499696 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 [...] (917776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499696 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 [...] (870042 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499696 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 [...] (822415 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499696 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 [...] (775131 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499697 2499698 2499699 2499700 2499701 2499702 2499703 2499704 2499705 2499706 [...] (727555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2596032 2596033 2596034 2596035 2596036 2596037 2596038 2596039 2596040 2596041 [...] (679727 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2596032 2596033 2596034 2596035 2596036 2596037 2596038 2596039 2596040 2596041 [...] (632106 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2596048 2596049 2792120 2792121 2792122 2792123 2876940 2876941 2876942 2876943 [...] (584248 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2901816 2901817 2901818 2901819 2901820 2901821 2901822 2901823 2901824 2901825 [...] (536722 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2982536 2982537 2982538 2982539 2982540 2982541 2982542 2982543 2982544 2982545 [...] (488453 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3006394 3006395 3081276 3081277 3081278 3081279 3081280 3081281 3081282 3081283 [...] (440929 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3172734 3172735 3172736 3172737 3172738 3172739 3172740 3172741 3172742 3172743 [...] (393025 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3212710 3212711 3278304 3278305 3278306 3278307 3278308 3278309 3278310 3278311 [...] (344985 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3374730 3374731 3374732 3374733 3374734 3374735 3374736 3374737 3374738 3374739 [...] (296966 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3394002 3394003 3394004 3394005 3394006 3394007 3421170 3421171 3421172 3421173 [...] (249143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3465684 3465685 3465686 3465687 3465688 3465689 3465690 3465691 3465692 3465693 [...] (201741 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3465684 3465685 3465686 3465687 3465688 3465689 3465690 3465691 3465692 3465693 [...] (154298 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3552498 3552499 3552500 3552501 3552502 3552503 3552504 3552505 3552506 3552507 [...] (106894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3596184 3596185 3596186 3596187 3596188 3596189 3596190 3596191 3596192 3596193 [...] (61230 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3660804 3660805 3660806 3660807 3660808 3660809 3660810 3660811 3660812 3660813 [...] (25497 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3817964 3817965 3817966 3817967 3817968 3817969 3817970 3817971 3817972 3817973 [...] (6275 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3846668 3846669 3846670 3846671 3908088 3908089 3908090 3908091 3908092 3908093 [...] (1015 flits)
Measured flits: (0 flits)
Time taken is 91462 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12015.4 (1 samples)
	minimum = 24 (1 samples)
	maximum = 26571 (1 samples)
Network latency average = 11868.1 (1 samples)
	minimum = 22 (1 samples)
	maximum = 25596 (1 samples)
Flit latency average = 29835.8 (1 samples)
	minimum = 5 (1 samples)
	maximum = 68040 (1 samples)
Fragmentation average = 175.057 (1 samples)
	minimum = 0 (1 samples)
	maximum = 512 (1 samples)
Injected packet rate average = 0.0529583 (1 samples)
	minimum = 0.04275 (1 samples)
	maximum = 0.05575 (1 samples)
Accepted packet rate average = 0.018013 (1 samples)
	minimum = 0.0125 (1 samples)
	maximum = 0.024 (1 samples)
Injected flit rate average = 0.953161 (1 samples)
	minimum = 0.772 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.323947 (1 samples)
	minimum = 0.21975 (1 samples)
	maximum = 0.423 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 17.984 (1 samples)
Hops average = 5.06303 (1 samples)
Total run time 121.787
