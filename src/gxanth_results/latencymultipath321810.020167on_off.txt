BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 308.698
	minimum = 23
	maximum = 963
Network latency average = 242.321
	minimum = 23
	maximum = 914
Slowest packet = 3
Flit latency average = 168.837
	minimum = 6
	maximum = 936
Slowest flit = 3072
Fragmentation average = 133.23
	minimum = 0
	maximum = 760
Injected packet rate average = 0.0160417
	minimum = 0 (at node 65)
	maximum = 0.036 (at node 129)
Accepted packet rate average = 0.00927083
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.284156
	minimum = 0 (at node 65)
	maximum = 0.648 (at node 129)
Accepted flit rate average= 0.187547
	minimum = 0.048 (at node 174)
	maximum = 0.324 (at node 48)
Injected packet length average = 17.7136
Accepted packet length average = 20.2298
Total in-flight flits = 19431 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 493.34
	minimum = 23
	maximum = 1981
Network latency average = 372.695
	minimum = 23
	maximum = 1930
Slowest packet = 267
Flit latency average = 283.046
	minimum = 6
	maximum = 1913
Slowest flit = 4823
Fragmentation average = 166.781
	minimum = 0
	maximum = 1877
Injected packet rate average = 0.0143646
	minimum = 0.002 (at node 67)
	maximum = 0.029 (at node 118)
Accepted packet rate average = 0.0100547
	minimum = 0.006 (at node 83)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.256146
	minimum = 0.036 (at node 67)
	maximum = 0.519 (at node 118)
Accepted flit rate average= 0.192404
	minimum = 0.115 (at node 116)
	maximum = 0.28 (at node 44)
Injected packet length average = 17.8318
Accepted packet length average = 19.1357
Total in-flight flits = 25495 (0 measured)
latency change    = 0.37427
throughput change = 0.0252426
Class 0:
Packet latency average = 1054.38
	minimum = 30
	maximum = 2788
Network latency average = 681.301
	minimum = 25
	maximum = 2659
Slowest packet = 417
Flit latency average = 567.649
	minimum = 6
	maximum = 2779
Slowest flit = 12424
Fragmentation average = 207.629
	minimum = 1
	maximum = 2202
Injected packet rate average = 0.0127656
	minimum = 0 (at node 3)
	maximum = 0.031 (at node 111)
Accepted packet rate average = 0.0109792
	minimum = 0.005 (at node 38)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.228474
	minimum = 0 (at node 7)
	maximum = 0.544 (at node 111)
Accepted flit rate average= 0.196792
	minimum = 0.092 (at node 178)
	maximum = 0.379 (at node 56)
Injected packet length average = 17.8976
Accepted packet length average = 17.9241
Total in-flight flits = 32081 (0 measured)
latency change    = 0.532102
throughput change = 0.0222978
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1206.87
	minimum = 28
	maximum = 3467
Network latency average = 345.458
	minimum = 25
	maximum = 977
Slowest packet = 7988
Flit latency average = 708.98
	minimum = 6
	maximum = 3773
Slowest flit = 13534
Fragmentation average = 133.127
	minimum = 0
	maximum = 596
Injected packet rate average = 0.0111979
	minimum = 0 (at node 29)
	maximum = 0.03 (at node 171)
Accepted packet rate average = 0.0107656
	minimum = 0.003 (at node 4)
	maximum = 0.019 (at node 3)
Injected flit rate average = 0.201495
	minimum = 0 (at node 29)
	maximum = 0.542 (at node 171)
Accepted flit rate average= 0.195245
	minimum = 0.034 (at node 4)
	maximum = 0.376 (at node 159)
Injected packet length average = 17.994
Accepted packet length average = 18.1359
Total in-flight flits = 33276 (23971 measured)
latency change    = 0.126359
throughput change = 0.00792275
Class 0:
Packet latency average = 1560.61
	minimum = 28
	maximum = 4442
Network latency average = 550.479
	minimum = 25
	maximum = 1980
Slowest packet = 7988
Flit latency average = 746.186
	minimum = 6
	maximum = 4234
Slowest flit = 41111
Fragmentation average = 158.917
	minimum = 0
	maximum = 1588
Injected packet rate average = 0.0113255
	minimum = 0 (at node 41)
	maximum = 0.0225 (at node 99)
Accepted packet rate average = 0.0108307
	minimum = 0.004 (at node 4)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.203703
	minimum = 0.002 (at node 41)
	maximum = 0.4045 (at node 183)
Accepted flit rate average= 0.194622
	minimum = 0.068 (at node 4)
	maximum = 0.311 (at node 16)
Injected packet length average = 17.9862
Accepted packet length average = 17.9695
Total in-flight flits = 35754 (32878 measured)
latency change    = 0.226666
throughput change = 0.00319797
Class 0:
Packet latency average = 1899.07
	minimum = 28
	maximum = 5375
Network latency average = 690.626
	minimum = 25
	maximum = 2814
Slowest packet = 7988
Flit latency average = 787.176
	minimum = 6
	maximum = 4810
Slowest flit = 44549
Fragmentation average = 171.134
	minimum = 0
	maximum = 1924
Injected packet rate average = 0.011283
	minimum = 0.001 (at node 97)
	maximum = 0.0226667 (at node 43)
Accepted packet rate average = 0.0108385
	minimum = 0.00433333 (at node 4)
	maximum = 0.0173333 (at node 16)
Injected flit rate average = 0.203005
	minimum = 0.016 (at node 97)
	maximum = 0.403 (at node 43)
Accepted flit rate average= 0.195146
	minimum = 0.0826667 (at node 4)
	maximum = 0.302 (at node 16)
Injected packet length average = 17.9922
Accepted packet length average = 18.0048
Total in-flight flits = 36695 (35846 measured)
latency change    = 0.178221
throughput change = 0.00268229
Class 0:
Packet latency average = 2170.15
	minimum = 28
	maximum = 6374
Network latency average = 781.936
	minimum = 25
	maximum = 3954
Slowest packet = 7988
Flit latency average = 811.486
	minimum = 6
	maximum = 5323
Slowest flit = 67222
Fragmentation average = 182.371
	minimum = 0
	maximum = 3067
Injected packet rate average = 0.0110977
	minimum = 0.00075 (at node 97)
	maximum = 0.021 (at node 183)
Accepted packet rate average = 0.0107839
	minimum = 0.006 (at node 4)
	maximum = 0.0155 (at node 128)
Injected flit rate average = 0.199609
	minimum = 0.012 (at node 97)
	maximum = 0.3795 (at node 183)
Accepted flit rate average= 0.193971
	minimum = 0.11525 (at node 4)
	maximum = 0.278 (at node 128)
Injected packet length average = 17.9866
Accepted packet length average = 17.9872
Total in-flight flits = 36759 (36440 measured)
latency change    = 0.124915
throughput change = 0.00605491
Class 0:
Packet latency average = 2410.09
	minimum = 28
	maximum = 6963
Network latency average = 850.101
	minimum = 25
	maximum = 4451
Slowest packet = 7988
Flit latency average = 835.18
	minimum = 6
	maximum = 7476
Slowest flit = 29568
Fragmentation average = 186.809
	minimum = 0
	maximum = 3067
Injected packet rate average = 0.0109323
	minimum = 0.0026 (at node 116)
	maximum = 0.0202 (at node 183)
Accepted packet rate average = 0.0107187
	minimum = 0.0066 (at node 36)
	maximum = 0.0148 (at node 177)
Injected flit rate average = 0.19668
	minimum = 0.0482 (at node 116)
	maximum = 0.3624 (at node 183)
Accepted flit rate average= 0.193147
	minimum = 0.123 (at node 36)
	maximum = 0.2674 (at node 177)
Injected packet length average = 17.9908
Accepted packet length average = 18.0195
Total in-flight flits = 35642 (35439 measured)
latency change    = 0.0995572
throughput change = 0.00426866
Class 0:
Packet latency average = 2669.27
	minimum = 28
	maximum = 7838
Network latency average = 884.28
	minimum = 25
	maximum = 5805
Slowest packet = 7988
Flit latency average = 846.544
	minimum = 6
	maximum = 7482
Slowest flit = 29573
Fragmentation average = 188.791
	minimum = 0
	maximum = 3067
Injected packet rate average = 0.0109002
	minimum = 0.00416667 (at node 61)
	maximum = 0.0191667 (at node 183)
Accepted packet rate average = 0.0107092
	minimum = 0.00733333 (at node 36)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.196126
	minimum = 0.077 (at node 61)
	maximum = 0.344333 (at node 183)
Accepted flit rate average= 0.19272
	minimum = 0.138167 (at node 36)
	maximum = 0.255167 (at node 128)
Injected packet length average = 17.9929
Accepted packet length average = 17.9958
Total in-flight flits = 36183 (36113 measured)
latency change    = 0.0970972
throughput change = 0.00221247
Class 0:
Packet latency average = 2903.51
	minimum = 28
	maximum = 9069
Network latency average = 915.635
	minimum = 23
	maximum = 5934
Slowest packet = 7988
Flit latency average = 859.504
	minimum = 6
	maximum = 7482
Slowest flit = 29573
Fragmentation average = 192.031
	minimum = 0
	maximum = 3899
Injected packet rate average = 0.0109479
	minimum = 0.00471429 (at node 88)
	maximum = 0.0184286 (at node 19)
Accepted packet rate average = 0.0107232
	minimum = 0.00814286 (at node 89)
	maximum = 0.0135714 (at node 128)
Injected flit rate average = 0.196964
	minimum = 0.0854286 (at node 88)
	maximum = 0.333714 (at node 19)
Accepted flit rate average= 0.193068
	minimum = 0.149571 (at node 64)
	maximum = 0.248286 (at node 128)
Injected packet length average = 17.991
Accepted packet length average = 18.0047
Total in-flight flits = 37611 (37593 measured)
latency change    = 0.0806732
throughput change = 0.00180229
Draining all recorded packets ...
Class 0:
Remaining flits: 151361 169326 169327 169328 169329 169330 169331 169332 169333 169334 [...] (34628 flits)
Measured flits: 151361 169326 169327 169328 169329 169330 169331 169332 169333 169334 [...] (34229 flits)
Class 0:
Remaining flits: 247698 247699 247700 247701 247702 247703 247704 247705 247706 247707 [...] (34660 flits)
Measured flits: 247698 247699 247700 247701 247702 247703 247704 247705 247706 247707 [...] (32470 flits)
Class 0:
Remaining flits: 247702 247703 247704 247705 247706 247707 247708 247709 247710 247711 [...] (35828 flits)
Measured flits: 247702 247703 247704 247705 247706 247707 247708 247709 247710 247711 [...] (31629 flits)
Class 0:
Remaining flits: 275005 275006 275007 275008 275009 275010 275011 275012 275013 275014 [...] (35293 flits)
Measured flits: 275005 275006 275007 275008 275009 275010 275011 275012 275013 275014 [...] (28817 flits)
Class 0:
Remaining flits: 335990 335991 335992 335993 335994 335995 335996 335997 335998 335999 [...] (36251 flits)
Measured flits: 335990 335991 335992 335993 335994 335995 335996 335997 335998 335999 [...] (26629 flits)
Class 0:
Remaining flits: 343852 343853 371052 371053 371054 371055 371056 371057 371058 371059 [...] (35753 flits)
Measured flits: 343852 343853 371052 371053 371054 371055 371056 371057 371058 371059 [...] (22988 flits)
Class 0:
Remaining flits: 390940 390941 418235 418236 418237 418238 418239 418240 418241 418242 [...] (34405 flits)
Measured flits: 390940 390941 418235 418236 418237 418238 418239 418240 418241 418242 [...] (18528 flits)
Class 0:
Remaining flits: 428202 428203 428204 428205 428206 428207 428208 428209 428210 428211 [...] (36049 flits)
Measured flits: 428202 428203 428204 428205 428206 428207 428208 428209 428210 428211 [...] (15467 flits)
Class 0:
Remaining flits: 471593 471594 471595 471596 471597 471598 471599 475326 475327 475328 [...] (38440 flits)
Measured flits: 471593 471594 471595 471596 471597 471598 471599 475326 475327 475328 [...] (14336 flits)
Class 0:
Remaining flits: 491300 491301 491302 491303 491304 491305 491306 491307 491308 491309 [...] (37826 flits)
Measured flits: 491300 491301 491302 491303 491304 491305 491306 491307 491308 491309 [...] (11227 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (36497 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (8673 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (37788 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (8000 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (36954 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (6853 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (36507 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (5560 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (35682 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (3826 flits)
Class 0:
Remaining flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (35829 flits)
Measured flits: 556290 556291 556292 556293 556294 556295 556296 556297 556298 556299 [...] (3126 flits)
Class 0:
Remaining flits: 723708 723709 723710 723711 723712 723713 723714 723715 723716 723717 [...] (35645 flits)
Measured flits: 774954 774955 774956 774957 774958 774959 774960 774961 774962 774963 [...] (2185 flits)
Class 0:
Remaining flits: 723708 723709 723710 723711 723712 723713 723714 723715 723716 723717 [...] (36255 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (1109 flits)
Class 0:
Remaining flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (36207 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (889 flits)
Class 0:
Remaining flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (36666 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (884 flits)
Class 0:
Remaining flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (35036 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (757 flits)
Class 0:
Remaining flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (36218 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (625 flits)
Class 0:
Remaining flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (37875 flits)
Measured flits: 786690 786691 786692 786693 786694 786695 786696 786697 786698 786699 [...] (516 flits)
Class 0:
Remaining flits: 965790 965791 965792 965793 965794 965795 965796 965797 965798 965799 [...] (37436 flits)
Measured flits: 1049904 1049905 1049906 1049907 1049908 1049909 1049910 1049911 1049912 1049913 [...] (491 flits)
Class 0:
Remaining flits: 1049907 1049908 1049909 1049910 1049911 1049912 1049913 1049914 1049915 1049916 [...] (37896 flits)
Measured flits: 1049907 1049908 1049909 1049910 1049911 1049912 1049913 1049914 1049915 1049916 [...] (429 flits)
Class 0:
Remaining flits: 1068804 1068805 1068806 1068807 1068808 1068809 1068810 1068811 1068812 1068813 [...] (37116 flits)
Measured flits: 1166274 1166275 1166276 1166277 1166278 1166279 1166280 1166281 1166282 1166283 [...] (462 flits)
Class 0:
Remaining flits: 1068804 1068805 1068806 1068807 1068808 1068809 1068810 1068811 1068812 1068813 [...] (35584 flits)
Measured flits: 1166274 1166275 1166276 1166277 1166278 1166279 1166280 1166281 1166282 1166283 [...] (360 flits)
Class 0:
Remaining flits: 1080990 1080991 1080992 1080993 1080994 1080995 1080996 1080997 1080998 1080999 [...] (34376 flits)
Measured flits: 1166274 1166275 1166276 1166277 1166278 1166279 1166280 1166281 1166282 1166283 [...] (393 flits)
Class 0:
Remaining flits: 1080991 1080992 1080993 1080994 1080995 1080996 1080997 1080998 1080999 1081000 [...] (35138 flits)
Measured flits: 1166274 1166275 1166276 1166277 1166278 1166279 1166280 1166281 1166282 1166283 [...] (425 flits)
Class 0:
Remaining flits: 1112429 1112430 1112431 1112432 1112433 1112434 1112435 1161452 1161453 1161454 [...] (34572 flits)
Measured flits: 1209942 1209943 1209944 1209945 1209946 1209947 1209948 1209949 1209950 1209951 [...] (483 flits)
Class 0:
Remaining flits: 1112429 1112430 1112431 1112432 1112433 1112434 1112435 1201212 1201213 1201214 [...] (35628 flits)
Measured flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (406 flits)
Class 0:
Remaining flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (35738 flits)
Measured flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (304 flits)
Class 0:
Remaining flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (35432 flits)
Measured flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (296 flits)
Class 0:
Remaining flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (37277 flits)
Measured flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (293 flits)
Class 0:
Remaining flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (35704 flits)
Measured flits: 1284030 1284031 1284032 1284033 1284034 1284035 1284036 1284037 1284038 1284039 [...] (337 flits)
Class 0:
Remaining flits: 1342926 1342927 1342928 1342929 1342930 1342931 1342932 1342933 1342934 1342935 [...] (36278 flits)
Measured flits: 1474182 1474183 1474184 1474185 1474186 1474187 1474188 1474189 1474190 1474191 [...] (344 flits)
Class 0:
Remaining flits: 1342926 1342927 1342928 1342929 1342930 1342931 1342932 1342933 1342934 1342935 [...] (35672 flits)
Measured flits: 1535454 1535455 1535456 1535457 1535458 1535459 1535460 1535461 1535462 1535463 [...] (239 flits)
Class 0:
Remaining flits: 1488005 1488923 1524798 1524799 1524800 1524801 1524802 1524803 1524804 1524805 [...] (36829 flits)
Measured flits: 1548324 1548325 1548326 1548327 1548328 1548329 1548330 1548331 1548332 1548333 [...] (121 flits)
Class 0:
Remaining flits: 1548324 1548325 1548326 1548327 1548328 1548329 1548330 1548331 1548332 1548333 [...] (37002 flits)
Measured flits: 1548324 1548325 1548326 1548327 1548328 1548329 1548330 1548331 1548332 1548333 [...] (72 flits)
Class 0:
Remaining flits: 1548324 1548325 1548326 1548327 1548328 1548329 1548330 1548331 1548332 1548333 [...] (36207 flits)
Measured flits: 1548324 1548325 1548326 1548327 1548328 1548329 1548330 1548331 1548332 1548333 [...] (52 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1580184 1580185 1580186 1580187 1580188 1580189 1580190 1580191 1580192 1580193 [...] (8245 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1754856 1754857 1754858 1754859 1754860 1754861 1754862 1754863 1754864 1754865 [...] (924 flits)
Measured flits: (0 flits)
Time taken is 53432 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6388.48 (1 samples)
	minimum = 28 (1 samples)
	maximum = 41182 (1 samples)
Network latency average = 1054.09 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13687 (1 samples)
Flit latency average = 942.735 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13670 (1 samples)
Fragmentation average = 207.897 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5039 (1 samples)
Injected packet rate average = 0.0109479 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0184286 (1 samples)
Accepted packet rate average = 0.0107232 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0135714 (1 samples)
Injected flit rate average = 0.196964 (1 samples)
	minimum = 0.0854286 (1 samples)
	maximum = 0.333714 (1 samples)
Accepted flit rate average = 0.193068 (1 samples)
	minimum = 0.149571 (1 samples)
	maximum = 0.248286 (1 samples)
Injected packet size average = 17.991 (1 samples)
Accepted packet size average = 18.0047 (1 samples)
Hops average = 5.05625 (1 samples)
Total run time 58.3463
