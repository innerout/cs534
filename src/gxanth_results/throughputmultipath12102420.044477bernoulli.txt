BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 311.255
	minimum = 22
	maximum = 902
Network latency average = 293.776
	minimum = 22
	maximum = 839
Slowest packet = 1124
Flit latency average = 270.403
	minimum = 5
	maximum = 822
Slowest flit = 20249
Fragmentation average = 24.0955
	minimum = 0
	maximum = 97
Injected packet rate average = 0.042901
	minimum = 0.03 (at node 51)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0154844
	minimum = 0.006 (at node 115)
	maximum = 0.024 (at node 49)
Injected flit rate average = 0.76549
	minimum = 0.531 (at node 145)
	maximum = 0.992 (at node 55)
Accepted flit rate average= 0.284495
	minimum = 0.108 (at node 115)
	maximum = 0.438 (at node 49)
Injected packet length average = 17.8431
Accepted packet length average = 18.373
Total in-flight flits = 93643 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 573.984
	minimum = 22
	maximum = 1698
Network latency average = 551.259
	minimum = 22
	maximum = 1567
Slowest packet = 2167
Flit latency average = 528.799
	minimum = 5
	maximum = 1550
Slowest flit = 57293
Fragmentation average = 25.0331
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0438047
	minimum = 0.029 (at node 51)
	maximum = 0.055 (at node 74)
Accepted packet rate average = 0.016125
	minimum = 0.0095 (at node 153)
	maximum = 0.022 (at node 46)
Injected flit rate average = 0.784995
	minimum = 0.5145 (at node 51)
	maximum = 0.984 (at node 74)
Accepted flit rate average= 0.293641
	minimum = 0.175 (at node 153)
	maximum = 0.404 (at node 78)
Injected packet length average = 17.9203
Accepted packet length average = 18.2103
Total in-flight flits = 190020 (0 measured)
latency change    = 0.457729
throughput change = 0.0311463
Class 0:
Packet latency average = 1351.23
	minimum = 24
	maximum = 2362
Network latency average = 1320.22
	minimum = 22
	maximum = 2298
Slowest packet = 4262
Flit latency average = 1299.92
	minimum = 5
	maximum = 2341
Slowest flit = 93937
Fragmentation average = 25.0492
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0442031
	minimum = 0.031 (at node 51)
	maximum = 0.056 (at node 95)
Accepted packet rate average = 0.0172396
	minimum = 0.007 (at node 21)
	maximum = 0.036 (at node 16)
Injected flit rate average = 0.796068
	minimum = 0.573 (at node 51)
	maximum = 1 (at node 55)
Accepted flit rate average= 0.310323
	minimum = 0.126 (at node 26)
	maximum = 0.656 (at node 16)
Injected packet length average = 18.0093
Accepted packet length average = 18.0006
Total in-flight flits = 283204 (0 measured)
latency change    = 0.575215
throughput change = 0.0537578
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 84.8991
	minimum = 22
	maximum = 568
Network latency average = 48.0512
	minimum = 22
	maximum = 437
Slowest packet = 25336
Flit latency average = 1858.21
	minimum = 5
	maximum = 3027
Slowest flit = 142134
Fragmentation average = 5.42236
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0442135
	minimum = 0.027 (at node 168)
	maximum = 0.056 (at node 35)
Accepted packet rate average = 0.0169635
	minimum = 0.008 (at node 71)
	maximum = 0.027 (at node 123)
Injected flit rate average = 0.796005
	minimum = 0.486 (at node 168)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.306375
	minimum = 0.155 (at node 71)
	maximum = 0.506 (at node 123)
Injected packet length average = 18.0037
Accepted packet length average = 18.0608
Total in-flight flits = 377200 (141148 measured)
latency change    = 14.9158
throughput change = 0.0128859
Class 0:
Packet latency average = 116.641
	minimum = 22
	maximum = 909
Network latency average = 66.1427
	minimum = 22
	maximum = 806
Slowest packet = 25336
Flit latency average = 2152.82
	minimum = 5
	maximum = 3800
Slowest flit = 174869
Fragmentation average = 5.42741
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0433906
	minimum = 0.029 (at node 112)
	maximum = 0.0555 (at node 11)
Accepted packet rate average = 0.0168359
	minimum = 0.008 (at node 4)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.780654
	minimum = 0.519 (at node 112)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.302867
	minimum = 0.144 (at node 4)
	maximum = 0.4195 (at node 129)
Injected packet length average = 17.9913
Accepted packet length average = 17.9893
Total in-flight flits = 466819 (277631 measured)
latency change    = 0.272132
throughput change = 0.011582
Class 0:
Packet latency average = 180.348
	minimum = 22
	maximum = 1472
Network latency average = 116.068
	minimum = 22
	maximum = 1252
Slowest packet = 25336
Flit latency average = 2454.69
	minimum = 5
	maximum = 4462
Slowest flit = 223663
Fragmentation average = 5.59196
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0427361
	minimum = 0.0293333 (at node 112)
	maximum = 0.0553333 (at node 83)
Accepted packet rate average = 0.0166892
	minimum = 0.01 (at node 4)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.769028
	minimum = 0.527333 (at node 112)
	maximum = 1 (at node 83)
Accepted flit rate average= 0.299998
	minimum = 0.182333 (at node 4)
	maximum = 0.393667 (at node 129)
Injected packet length average = 17.9948
Accepted packet length average = 17.9756
Total in-flight flits = 553511 (411224 measured)
latency change    = 0.353247
throughput change = 0.00956313
Draining remaining packets ...
Class 0:
Remaining flits: 271872 271873 271874 271875 271876 271877 271878 271879 271880 271881 [...] (502573 flits)
Measured flits: 455544 455545 455546 455547 455548 455549 455550 455551 455552 455553 [...] (407238 flits)
Class 0:
Remaining flits: 307278 307279 307280 307281 307282 307283 307284 307285 307286 307287 [...] (454595 flits)
Measured flits: 455562 455563 455564 455565 455566 455567 455568 455569 455570 455571 [...] (404584 flits)
Class 0:
Remaining flits: 347953 347954 347955 347956 347957 356508 356509 356510 356511 356512 [...] (407232 flits)
Measured flits: 455580 455581 455582 455583 455584 455585 455586 455587 455588 455589 [...] (391184 flits)
Class 0:
Remaining flits: 387144 387145 387146 387147 387148 387149 387150 387151 387152 387153 [...] (360234 flits)
Measured flits: 455616 455617 455618 455619 455620 455621 455622 455623 455624 455625 [...] (358065 flits)
Class 0:
Remaining flits: 420308 420309 420310 420311 420312 420313 420314 420315 420316 420317 [...] (313543 flits)
Measured flits: 455922 455923 455924 455925 455926 455927 455928 455929 455930 455931 [...] (313407 flits)
Class 0:
Remaining flits: 469674 469675 469676 469677 469678 469679 469680 469681 469682 469683 [...] (266541 flits)
Measured flits: 469674 469675 469676 469677 469678 469679 469680 469681 469682 469683 [...] (266541 flits)
Class 0:
Remaining flits: 516636 516637 516638 516639 516640 516641 516642 516643 516644 516645 [...] (219643 flits)
Measured flits: 516636 516637 516638 516639 516640 516641 516642 516643 516644 516645 [...] (219643 flits)
Class 0:
Remaining flits: 590094 590095 590096 590097 590098 590099 590100 590101 590102 590103 [...] (172198 flits)
Measured flits: 590094 590095 590096 590097 590098 590099 590100 590101 590102 590103 [...] (172198 flits)
Class 0:
Remaining flits: 633402 633403 633404 633405 633406 633407 633408 633409 633410 633411 [...] (124664 flits)
Measured flits: 633402 633403 633404 633405 633406 633407 633408 633409 633410 633411 [...] (124664 flits)
Class 0:
Remaining flits: 656766 656767 656768 656769 656770 656771 656772 656773 656774 656775 [...] (77894 flits)
Measured flits: 656766 656767 656768 656769 656770 656771 656772 656773 656774 656775 [...] (77894 flits)
Class 0:
Remaining flits: 692082 692083 692084 692085 692086 692087 692088 692089 692090 692091 [...] (31335 flits)
Measured flits: 692082 692083 692084 692085 692086 692087 692088 692089 692090 692091 [...] (31335 flits)
Class 0:
Remaining flits: 755640 755641 755642 755643 755644 755645 755646 755647 755648 755649 [...] (3286 flits)
Measured flits: 755640 755641 755642 755643 755644 755645 755646 755647 755648 755649 [...] (3286 flits)
Time taken is 18932 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8213.84 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13635 (1 samples)
Network latency average = 8148.46 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13592 (1 samples)
Flit latency average = 6539.29 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13575 (1 samples)
Fragmentation average = 26.9838 (1 samples)
	minimum = 0 (1 samples)
	maximum = 297 (1 samples)
Injected packet rate average = 0.0427361 (1 samples)
	minimum = 0.0293333 (1 samples)
	maximum = 0.0553333 (1 samples)
Accepted packet rate average = 0.0166892 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.769028 (1 samples)
	minimum = 0.527333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.299998 (1 samples)
	minimum = 0.182333 (1 samples)
	maximum = 0.393667 (1 samples)
Injected packet size average = 17.9948 (1 samples)
Accepted packet size average = 17.9756 (1 samples)
Hops average = 5.10448 (1 samples)
Total run time 13.9486
