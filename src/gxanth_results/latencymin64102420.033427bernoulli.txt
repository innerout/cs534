BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 282.59
	minimum = 22
	maximum = 774
Network latency average = 273.14
	minimum = 22
	maximum = 753
Slowest packet = 1282
Flit latency average = 243.11
	minimum = 5
	maximum = 766
Slowest flit = 24220
Fragmentation average = 55.8435
	minimum = 0
	maximum = 515
Injected packet rate average = 0.0329635
	minimum = 0.017 (at node 97)
	maximum = 0.048 (at node 118)
Accepted packet rate average = 0.0142813
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.587724
	minimum = 0.306 (at node 97)
	maximum = 0.862 (at node 118)
Accepted flit rate average= 0.272724
	minimum = 0.104 (at node 174)
	maximum = 0.443 (at node 44)
Injected packet length average = 17.8295
Accepted packet length average = 19.0966
Total in-flight flits = 61559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 525.84
	minimum = 22
	maximum = 1684
Network latency average = 514.378
	minimum = 22
	maximum = 1600
Slowest packet = 2543
Flit latency average = 477.55
	minimum = 5
	maximum = 1583
Slowest flit = 39599
Fragmentation average = 83.5268
	minimum = 0
	maximum = 785
Injected packet rate average = 0.03325
	minimum = 0.0205 (at node 62)
	maximum = 0.043 (at node 117)
Accepted packet rate average = 0.0151354
	minimum = 0.0085 (at node 116)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.596021
	minimum = 0.369 (at node 62)
	maximum = 0.771 (at node 118)
Accepted flit rate average= 0.283862
	minimum = 0.1555 (at node 153)
	maximum = 0.415 (at node 152)
Injected packet length average = 17.9254
Accepted packet length average = 18.7548
Total in-flight flits = 120821 (0 measured)
latency change    = 0.462593
throughput change = 0.0392375
Class 0:
Packet latency average = 1216.45
	minimum = 23
	maximum = 2291
Network latency average = 1202.87
	minimum = 22
	maximum = 2242
Slowest packet = 4140
Flit latency average = 1159.07
	minimum = 5
	maximum = 2368
Slowest flit = 67173
Fragmentation average = 149.914
	minimum = 0
	maximum = 792
Injected packet rate average = 0.0330729
	minimum = 0.016 (at node 111)
	maximum = 0.051 (at node 17)
Accepted packet rate average = 0.0160208
	minimum = 0.006 (at node 180)
	maximum = 0.027 (at node 159)
Injected flit rate average = 0.59449
	minimum = 0.283 (at node 111)
	maximum = 0.923 (at node 17)
Accepted flit rate average= 0.29187
	minimum = 0.121 (at node 180)
	maximum = 0.518 (at node 159)
Injected packet length average = 17.9751
Accepted packet length average = 18.2181
Total in-flight flits = 179082 (0 measured)
latency change    = 0.567725
throughput change = 0.0274362
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 48.9548
	minimum = 24
	maximum = 172
Network latency average = 35.472
	minimum = 22
	maximum = 103
Slowest packet = 19137
Flit latency average = 1607.72
	minimum = 5
	maximum = 3202
Slowest flit = 87682
Fragmentation average = 6.7179
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0338698
	minimum = 0.019 (at node 87)
	maximum = 0.05 (at node 98)
Accepted packet rate average = 0.01625
	minimum = 0.005 (at node 43)
	maximum = 0.032 (at node 90)
Injected flit rate average = 0.609469
	minimum = 0.35 (at node 87)
	maximum = 0.904 (at node 121)
Accepted flit rate average= 0.29674
	minimum = 0.111 (at node 43)
	maximum = 0.57 (at node 90)
Injected packet length average = 17.9945
Accepted packet length average = 18.2609
Total in-flight flits = 239162 (107025 measured)
latency change    = 23.8484
throughput change = 0.016411
Class 0:
Packet latency average = 48.652
	minimum = 22
	maximum = 203
Network latency average = 35.3519
	minimum = 22
	maximum = 103
Slowest packet = 19137
Flit latency average = 1855.56
	minimum = 5
	maximum = 4103
Slowest flit = 99989
Fragmentation average = 6.52637
	minimum = 0
	maximum = 37
Injected packet rate average = 0.033474
	minimum = 0.024 (at node 87)
	maximum = 0.0445 (at node 114)
Accepted packet rate average = 0.0160495
	minimum = 0.009 (at node 35)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.602742
	minimum = 0.4305 (at node 87)
	maximum = 0.801 (at node 114)
Accepted flit rate average= 0.29312
	minimum = 0.172 (at node 35)
	maximum = 0.4485 (at node 90)
Injected packet length average = 18.0063
Accepted packet length average = 18.2635
Total in-flight flits = 297896 (212542 measured)
latency change    = 0.00622434
throughput change = 0.0123492
Class 0:
Packet latency average = 57.277
	minimum = 22
	maximum = 2980
Network latency average = 44.3047
	minimum = 22
	maximum = 2980
Slowest packet = 19173
Flit latency average = 2080.65
	minimum = 5
	maximum = 4847
Slowest flit = 111995
Fragmentation average = 6.89905
	minimum = 0
	maximum = 169
Injected packet rate average = 0.033474
	minimum = 0.0226667 (at node 87)
	maximum = 0.0423333 (at node 114)
Accepted packet rate average = 0.0159931
	minimum = 0.0113333 (at node 35)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.602689
	minimum = 0.410667 (at node 87)
	maximum = 0.765333 (at node 114)
Accepted flit rate average= 0.293377
	minimum = 0.218 (at node 144)
	maximum = 0.415 (at node 128)
Injected packet length average = 18.0047
Accepted packet length average = 18.344
Total in-flight flits = 357155 (318250 measured)
latency change    = 0.150584
throughput change = 0.000875817
Class 0:
Packet latency average = 997.967
	minimum = 22
	maximum = 3995
Network latency average = 985.913
	minimum = 22
	maximum = 3972
Slowest packet = 19195
Flit latency average = 2306.12
	minimum = 5
	maximum = 5612
Slowest flit = 156499
Fragmentation average = 66.4543
	minimum = 0
	maximum = 738
Injected packet rate average = 0.033487
	minimum = 0.023 (at node 87)
	maximum = 0.04125 (at node 35)
Accepted packet rate average = 0.0160391
	minimum = 0.01125 (at node 104)
	maximum = 0.02125 (at node 128)
Injected flit rate average = 0.602991
	minimum = 0.41175 (at node 87)
	maximum = 0.7445 (at node 114)
Accepted flit rate average= 0.293109
	minimum = 0.2145 (at node 104)
	maximum = 0.386 (at node 128)
Injected packet length average = 18.0067
Accepted packet length average = 18.2747
Total in-flight flits = 416898 (405226 measured)
latency change    = 0.942606
throughput change = 0.000912155
Class 0:
Packet latency average = 2058.59
	minimum = 22
	maximum = 4934
Network latency average = 2046.34
	minimum = 22
	maximum = 4920
Slowest packet = 19448
Flit latency average = 2538.6
	minimum = 5
	maximum = 6251
Slowest flit = 178793
Fragmentation average = 149.286
	minimum = 0
	maximum = 817
Injected packet rate average = 0.0335615
	minimum = 0.026 (at node 18)
	maximum = 0.0418 (at node 35)
Accepted packet rate average = 0.0160771
	minimum = 0.012 (at node 52)
	maximum = 0.0204 (at node 51)
Injected flit rate average = 0.60412
	minimum = 0.4672 (at node 18)
	maximum = 0.754 (at node 35)
Accepted flit rate average= 0.293674
	minimum = 0.2182 (at node 52)
	maximum = 0.3746 (at node 68)
Injected packet length average = 18.0004
Accepted packet length average = 18.2666
Total in-flight flits = 477097 (473518 measured)
latency change    = 0.515219
throughput change = 0.00192248
Class 0:
Packet latency average = 2719.27
	minimum = 22
	maximum = 5927
Network latency average = 2706.5
	minimum = 22
	maximum = 5927
Slowest packet = 19501
Flit latency average = 2774.77
	minimum = 5
	maximum = 6789
Slowest flit = 249721
Fragmentation average = 200.832
	minimum = 0
	maximum = 876
Injected packet rate average = 0.0335356
	minimum = 0.0256667 (at node 18)
	maximum = 0.0398333 (at node 97)
Accepted packet rate average = 0.0161476
	minimum = 0.0121667 (at node 52)
	maximum = 0.0216667 (at node 138)
Injected flit rate average = 0.603856
	minimum = 0.463667 (at node 18)
	maximum = 0.717 (at node 97)
Accepted flit rate average= 0.29432
	minimum = 0.2245 (at node 52)
	maximum = 0.396167 (at node 138)
Injected packet length average = 18.0064
Accepted packet length average = 18.2269
Total in-flight flits = 535419 (534524 measured)
latency change    = 0.242961
throughput change = 0.00219609
Class 0:
Packet latency average = 3196.89
	minimum = 22
	maximum = 6948
Network latency average = 3183.8
	minimum = 22
	maximum = 6883
Slowest packet = 19209
Flit latency average = 3014.95
	minimum = 5
	maximum = 7414
Slowest flit = 277595
Fragmentation average = 229.698
	minimum = 0
	maximum = 895
Injected packet rate average = 0.0334628
	minimum = 0.0265714 (at node 18)
	maximum = 0.0387143 (at node 24)
Accepted packet rate average = 0.0161659
	minimum = 0.0122857 (at node 80)
	maximum = 0.0211429 (at node 138)
Injected flit rate average = 0.602362
	minimum = 0.478 (at node 18)
	maximum = 0.697286 (at node 130)
Accepted flit rate average= 0.29444
	minimum = 0.230714 (at node 80)
	maximum = 0.382571 (at node 138)
Injected packet length average = 18.001
Accepted packet length average = 18.2136
Total in-flight flits = 592887 (592733 measured)
latency change    = 0.1494
throughput change = 0.000405583
Draining all recorded packets ...
Class 0:
Remaining flits: 317604 317605 317606 317607 317608 317609 336146 336147 336148 336149 [...] (652748 flits)
Measured flits: 352158 352159 352160 352161 352162 352163 352164 352165 352166 352167 [...] (547093 flits)
Class 0:
Remaining flits: 381389 381390 381391 381392 381393 381394 381395 381396 381397 381398 [...] (708745 flits)
Measured flits: 381389 381390 381391 381392 381393 381394 381395 381396 381397 381398 [...] (500019 flits)
Class 0:
Remaining flits: 471682 471683 471684 471685 471686 471687 471688 471689 477450 477451 [...] (768634 flits)
Measured flits: 471682 471683 471684 471685 471686 471687 471688 471689 477450 477451 [...] (453362 flits)
Class 0:
Remaining flits: 493520 493521 493522 493523 501557 501558 501559 501560 501561 501562 [...] (828611 flits)
Measured flits: 493520 493521 493522 493523 501557 501558 501559 501560 501561 501562 [...] (406301 flits)
Class 0:
Remaining flits: 526477 526478 526479 526480 526481 560934 560935 560936 560937 560938 [...] (886958 flits)
Measured flits: 526477 526478 526479 526480 526481 560934 560935 560936 560937 560938 [...] (358736 flits)
Class 0:
Remaining flits: 587249 593331 593332 593333 596464 596465 598197 598198 598199 598200 [...] (948654 flits)
Measured flits: 587249 593331 593332 593333 596464 596465 598197 598198 598199 598200 [...] (311570 flits)
Class 0:
Remaining flits: 634017 634018 634019 634020 634021 634022 634023 634024 634025 634026 [...] (1006352 flits)
Measured flits: 634017 634018 634019 634020 634021 634022 634023 634024 634025 634026 [...] (264391 flits)
Class 0:
Remaining flits: 644817 644818 644819 644820 644821 644822 644823 644824 644825 644826 [...] (1063848 flits)
Measured flits: 644817 644818 644819 644820 644821 644822 644823 644824 644825 644826 [...] (216932 flits)
Class 0:
Remaining flits: 692873 704157 704158 704159 708624 708625 708626 708627 708628 708629 [...] (1122678 flits)
Measured flits: 692873 704157 704158 704159 708624 708625 708626 708627 708628 708629 [...] (169571 flits)
Class 0:
Remaining flits: 708624 708625 708626 708627 708628 708629 708630 708631 708632 708633 [...] (1182077 flits)
Measured flits: 708624 708625 708626 708627 708628 708629 708630 708631 708632 708633 [...] (122400 flits)
Class 0:
Remaining flits: 764216 764217 764218 764219 764220 764221 764222 764223 764224 764225 [...] (1239246 flits)
Measured flits: 764216 764217 764218 764219 764220 764221 764222 764223 764224 764225 [...] (79426 flits)
Class 0:
Remaining flits: 828414 828415 828416 828417 828418 828419 828420 828421 828422 828423 [...] (1296370 flits)
Measured flits: 828414 828415 828416 828417 828418 828419 828420 828421 828422 828423 [...] (47919 flits)
Class 0:
Remaining flits: 861705 861706 861707 861708 861709 861710 861711 861712 861713 870225 [...] (1355934 flits)
Measured flits: 861705 861706 861707 861708 861709 861710 861711 861712 861713 870225 [...] (27486 flits)
Class 0:
Remaining flits: 885599 893731 893732 893733 893734 893735 900000 900001 900002 900003 [...] (1412705 flits)
Measured flits: 885599 893731 893732 893733 893734 893735 900000 900001 900002 900003 [...] (15086 flits)
Class 0:
Remaining flits: 903836 903837 903838 903839 903840 903841 903842 903843 903844 903845 [...] (1471850 flits)
Measured flits: 903836 903837 903838 903839 903840 903841 903842 903843 903844 903845 [...] (7426 flits)
Class 0:
Remaining flits: 928506 928507 928508 928509 928510 928511 960013 960014 960015 960016 [...] (1531528 flits)
Measured flits: 928506 928507 928508 928509 928510 928511 960013 960014 960015 960016 [...] (3367 flits)
Class 0:
Remaining flits: 980946 980947 980948 980949 980950 980951 980952 980953 980954 980955 [...] (1590400 flits)
Measured flits: 980946 980947 980948 980949 980950 980951 980952 980953 980954 980955 [...] (1480 flits)
Class 0:
Remaining flits: 1024797 1024798 1024799 1024800 1024801 1024802 1024803 1024804 1024805 1024806 [...] (1649196 flits)
Measured flits: 1024797 1024798 1024799 1024800 1024801 1024802 1024803 1024804 1024805 1024806 [...] (641 flits)
Class 0:
Remaining flits: 1089072 1089073 1089074 1089075 1089076 1089077 1089078 1089079 1089080 1089081 [...] (1705328 flits)
Measured flits: 1089072 1089073 1089074 1089075 1089076 1089077 1089078 1089079 1089080 1089081 [...] (239 flits)
Class 0:
Remaining flits: 1089076 1089077 1089078 1089079 1089080 1089081 1089082 1089083 1089084 1089085 [...] (1764429 flits)
Measured flits: 1089076 1089077 1089078 1089079 1089080 1089081 1089082 1089083 1089084 1089085 [...] (113 flits)
Class 0:
Remaining flits: 1121207 1121208 1121209 1121210 1121211 1121212 1121213 1121214 1121215 1121216 [...] (1821359 flits)
Measured flits: 1121207 1121208 1121209 1121210 1121211 1121212 1121213 1121214 1121215 1121216 [...] (40 flits)
Class 0:
Remaining flits: 1152558 1152559 1152560 1152561 1152562 1152563 1152564 1152565 1152566 1152567 [...] (1879411 flits)
Measured flits: 1152558 1152559 1152560 1152561 1152562 1152563 1152564 1152565 1152566 1152567 [...] (18 flits)
Class 0:
Remaining flits: 1152569 1152570 1152571 1152572 1152573 1152574 1152575 1214892 1214893 1214894 [...] (1938422 flits)
Measured flits: 1152569 1152570 1152571 1152572 1152573 1152574 1152575 (7 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1257712 1257713 1307178 1307179 1307180 1307181 1307182 1307183 1307184 1307185 [...] (1902978 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1307178 1307179 1307180 1307181 1307182 1307183 1307184 1307185 1307186 1307187 [...] (1855487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1326780 1326781 1326782 1326783 1326784 1326785 1326786 1326787 1326788 1326789 [...] (1808012 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1352028 1352029 1352030 1352031 1352032 1352033 1355680 1355681 1355682 1355683 [...] (1760947 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1440126 1440127 1440128 1440129 1440130 1440131 1440132 1440133 1440134 1440135 [...] (1713332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473210 1473211 1473212 1473213 1473214 1473215 1473216 1473217 1473218 1473219 [...] (1665738 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1519161 1519162 1519163 1535639 1535640 1535641 1535642 1535643 1535644 1535645 [...] (1618277 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1544919 1544920 1544921 1560924 1560925 1560926 1560927 1560928 1560929 1560930 [...] (1570491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1560938 1560939 1560940 1560941 1582362 1582363 1582364 1582365 1582366 1582367 [...] (1522925 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1591488 1591489 1591490 1591491 1591492 1591493 1591494 1591495 1591496 1591497 [...] (1475522 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1677340 1677341 1677342 1677343 1677344 1677345 1677346 1677347 1702746 1702747 [...] (1427955 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702752 1702753 1702754 1702755 1702756 1702757 1702758 1702759 1702760 1702761 [...] (1380112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1758168 1758169 1758170 1758171 1758172 1758173 1758174 1758175 1758176 1758177 [...] (1332378 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1782281 1782282 1782283 1782284 1782285 1782286 1782287 1820016 1820017 1820018 [...] (1284830 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1842084 1842085 1842086 1842087 1842088 1842089 1842090 1842091 1842092 1842093 [...] (1237269 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888903 1888904 1888905 1888906 1888907 1888908 1888909 1888910 1888911 1888912 [...] (1189459 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1921496 1921497 1921498 1921499 1971053 1972278 1972279 1972280 1972281 1972282 [...] (1141770 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1991610 1991611 1991612 1991613 1991614 1991615 1991616 1991617 1991618 1991619 [...] (1094300 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2013066 2013067 2013068 2013069 2013070 2013071 2013072 2013073 2013074 2013075 [...] (1046902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2036091 2036092 2036093 2036094 2036095 2036096 2036097 2036098 2036099 2036100 [...] (999299 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2082622 2082623 2082624 2082625 2082626 2082627 2082628 2082629 2082630 2082631 [...] (951570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2147014 2147015 2147016 2147017 2147018 2147019 2147020 2147021 2160450 2160451 [...] (903863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2178491 2178492 2178493 2178494 2178495 2178496 2178497 2178498 2178499 2178500 [...] (855982 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2209932 2209933 2209934 2209935 2209936 2209937 2209938 2209939 2209940 2209941 [...] (808092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269158 2269159 2269160 2269161 2269162 2269163 2269164 2269165 2269166 2269167 [...] (760345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2290344 2290345 2290346 2290347 2290348 2290349 2290350 2290351 2290352 2290353 [...] (712288 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2312532 2312533 2312534 2312535 2312536 2312537 2312538 2312539 2312540 2312541 [...] (664354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2345449 2345450 2345451 2345452 2345453 2385594 2385595 2385596 2385597 2385598 [...] (616411 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2399796 2399797 2399798 2399799 2399800 2399801 2399802 2399803 2399804 2399805 [...] (568520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2483478 2483479 2483480 2483481 2483482 2483483 2483484 2483485 2483486 2483487 [...] (520705 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2483491 2483492 2483493 2483494 2483495 2499858 2499859 2499860 2499861 2499862 [...] (473166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2499858 2499859 2499860 2499861 2499862 2499863 2499864 2499865 2499866 2499867 [...] (425282 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2621520 2621521 2621522 2621523 2621524 2621525 2621526 2621527 2621528 2621529 [...] (377521 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2621526 2621527 2621528 2621529 2621530 2621531 2621532 2621533 2621534 2621535 [...] (329895 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2686356 2686357 2686358 2686359 2686360 2686361 2686362 2686363 2686364 2686365 [...] (282139 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2686367 2686368 2686369 2686370 2686371 2686372 2686373 2696940 2696941 2696942 [...] (234593 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2778336 2778337 2778338 2778339 2778340 2778341 2778342 2778343 2778344 2778345 [...] (186941 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2798715 2798716 2798717 2798718 2798719 2798720 2798721 2798722 2798723 2798724 [...] (139486 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2853846 2853847 2853848 2853849 2853850 2853851 2853852 2853853 2853854 2853855 [...] (93025 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2964852 2964853 2964854 2964855 2964856 2964857 2964858 2964859 2964860 2964861 [...] (49763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018898 3018899 3018900 3018901 3018902 3018903 3018904 3018905 3048750 3048751 [...] (17529 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3243762 3243763 3243764 3243765 3243766 3243767 3243768 3243769 3243770 3243771 [...] (3088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3389714 3389715 3389716 3389717 3389718 3389719 3389720 3389721 3389722 3389723 [...] (405 flits)
Measured flits: (0 flits)
Time taken is 76640 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7831.82 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23215 (1 samples)
Network latency average = 7818.81 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23215 (1 samples)
Flit latency average = 19701.8 (1 samples)
	minimum = 5 (1 samples)
	maximum = 48483 (1 samples)
Fragmentation average = 381.422 (1 samples)
	minimum = 0 (1 samples)
	maximum = 970 (1 samples)
Injected packet rate average = 0.0334628 (1 samples)
	minimum = 0.0265714 (1 samples)
	maximum = 0.0387143 (1 samples)
Accepted packet rate average = 0.0161659 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0211429 (1 samples)
Injected flit rate average = 0.602362 (1 samples)
	minimum = 0.478 (1 samples)
	maximum = 0.697286 (1 samples)
Accepted flit rate average = 0.29444 (1 samples)
	minimum = 0.230714 (1 samples)
	maximum = 0.382571 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 18.2136 (1 samples)
Hops average = 5.07185 (1 samples)
Total run time 106.678
