BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 188.876
	minimum = 23
	maximum = 931
Network latency average = 186.608
	minimum = 23
	maximum = 909
Slowest packet = 191
Flit latency average = 117.504
	minimum = 6
	maximum = 892
Slowest flit = 3455
Fragmentation average = 115.813
	minimum = 0
	maximum = 754
Injected packet rate average = 0.0136094
	minimum = 0.006 (at node 87)
	maximum = 0.022 (at node 123)
Accepted packet rate average = 0.00910417
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.242297
	minimum = 0.1 (at node 87)
	maximum = 0.396 (at node 123)
Accepted flit rate average= 0.182781
	minimum = 0.059 (at node 174)
	maximum = 0.326 (at node 132)
Injected packet length average = 17.8037
Accepted packet length average = 20.0767
Total in-flight flits = 11940 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 275.32
	minimum = 23
	maximum = 1758
Network latency average = 272.844
	minimum = 23
	maximum = 1758
Slowest packet = 582
Flit latency average = 187.02
	minimum = 6
	maximum = 1741
Slowest flit = 10493
Fragmentation average = 155.819
	minimum = 0
	maximum = 1310
Injected packet rate average = 0.0133906
	minimum = 0.0075 (at node 161)
	maximum = 0.019 (at node 77)
Accepted packet rate average = 0.0100573
	minimum = 0.0055 (at node 116)
	maximum = 0.0165 (at node 166)
Injected flit rate average = 0.239911
	minimum = 0.135 (at node 161)
	maximum = 0.342 (at node 77)
Accepted flit rate average= 0.19401
	minimum = 0.1065 (at node 116)
	maximum = 0.3095 (at node 166)
Injected packet length average = 17.9164
Accepted packet length average = 19.2905
Total in-flight flits = 18056 (0 measured)
latency change    = 0.313976
throughput change = 0.0578792
Class 0:
Packet latency average = 442.022
	minimum = 26
	maximum = 2536
Network latency average = 439.218
	minimum = 23
	maximum = 2536
Slowest packet = 1030
Flit latency average = 329.714
	minimum = 6
	maximum = 2519
Slowest flit = 18557
Fragmentation average = 218.279
	minimum = 0
	maximum = 2307
Injected packet rate average = 0.0134844
	minimum = 0.004 (at node 136)
	maximum = 0.025 (at node 191)
Accepted packet rate average = 0.0116667
	minimum = 0.002 (at node 184)
	maximum = 0.019 (at node 56)
Injected flit rate average = 0.242443
	minimum = 0.064 (at node 136)
	maximum = 0.457 (at node 191)
Accepted flit rate average= 0.214328
	minimum = 0.043 (at node 184)
	maximum = 0.335 (at node 159)
Injected packet length average = 17.9795
Accepted packet length average = 18.371
Total in-flight flits = 23507 (0 measured)
latency change    = 0.377135
throughput change = 0.0947972
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 294.391
	minimum = 27
	maximum = 965
Network latency average = 291.651
	minimum = 27
	maximum = 965
Slowest packet = 7756
Flit latency average = 402.613
	minimum = 6
	maximum = 3299
Slowest flit = 22391
Fragmentation average = 163.571
	minimum = 0
	maximum = 836
Injected packet rate average = 0.0140469
	minimum = 0.004 (at node 150)
	maximum = 0.025 (at node 84)
Accepted packet rate average = 0.0121667
	minimum = 0.005 (at node 1)
	maximum = 0.022 (at node 120)
Injected flit rate average = 0.252427
	minimum = 0.072 (at node 150)
	maximum = 0.45 (at node 84)
Accepted flit rate average= 0.22162
	minimum = 0.083 (at node 17)
	maximum = 0.377 (at node 129)
Injected packet length average = 17.9703
Accepted packet length average = 18.2153
Total in-flight flits = 29502 (20904 measured)
latency change    = 0.501481
throughput change = 0.0329017
Class 0:
Packet latency average = 382.775
	minimum = 23
	maximum = 1966
Network latency average = 380.114
	minimum = 23
	maximum = 1953
Slowest packet = 7751
Flit latency average = 455.424
	minimum = 6
	maximum = 4290
Slowest flit = 21946
Fragmentation average = 185.429
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0136901
	minimum = 0.0085 (at node 10)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.0122969
	minimum = 0.0055 (at node 4)
	maximum = 0.0195 (at node 123)
Injected flit rate average = 0.246563
	minimum = 0.153 (at node 10)
	maximum = 0.392 (at node 126)
Accepted flit rate average= 0.223063
	minimum = 0.111 (at node 4)
	maximum = 0.346 (at node 129)
Injected packet length average = 18.0103
Accepted packet length average = 18.1398
Total in-flight flits = 32477 (27617 measured)
latency change    = 0.230904
throughput change = 0.00646773
Class 0:
Packet latency average = 458.183
	minimum = 23
	maximum = 2757
Network latency average = 455.605
	minimum = 23
	maximum = 2757
Slowest packet = 7794
Flit latency average = 489.947
	minimum = 6
	maximum = 5695
Slowest flit = 11286
Fragmentation average = 204.508
	minimum = 0
	maximum = 2217
Injected packet rate average = 0.013658
	minimum = 0.00866667 (at node 18)
	maximum = 0.0196667 (at node 116)
Accepted packet rate average = 0.0122865
	minimum = 0.008 (at node 17)
	maximum = 0.0173333 (at node 123)
Injected flit rate average = 0.245995
	minimum = 0.156 (at node 18)
	maximum = 0.354 (at node 116)
Accepted flit rate average= 0.222828
	minimum = 0.145333 (at node 17)
	maximum = 0.301667 (at node 19)
Injected packet length average = 18.0111
Accepted packet length average = 18.1361
Total in-flight flits = 36764 (33476 measured)
latency change    = 0.164581
throughput change = 0.00105182
Class 0:
Packet latency average = 523.169
	minimum = 23
	maximum = 3898
Network latency average = 520.591
	minimum = 23
	maximum = 3898
Slowest packet = 7843
Flit latency average = 522.004
	minimum = 6
	maximum = 6258
Slowest flit = 19104
Fragmentation average = 221.096
	minimum = 0
	maximum = 2802
Injected packet rate average = 0.0136523
	minimum = 0.0085 (at node 172)
	maximum = 0.01775 (at node 126)
Accepted packet rate average = 0.0123372
	minimum = 0.008 (at node 113)
	maximum = 0.0165 (at node 129)
Injected flit rate average = 0.245932
	minimum = 0.15675 (at node 172)
	maximum = 0.3195 (at node 126)
Accepted flit rate average= 0.223531
	minimum = 0.15725 (at node 36)
	maximum = 0.29775 (at node 129)
Injected packet length average = 18.0139
Accepted packet length average = 18.1184
Total in-flight flits = 40565 (37975 measured)
latency change    = 0.124216
throughput change = 0.00314553
Class 0:
Packet latency average = 582.704
	minimum = 23
	maximum = 4716
Network latency average = 580.085
	minimum = 23
	maximum = 4716
Slowest packet = 8473
Flit latency average = 563.913
	minimum = 6
	maximum = 7253
Slowest flit = 18808
Fragmentation average = 230.682
	minimum = 0
	maximum = 3500
Injected packet rate average = 0.013574
	minimum = 0.0106 (at node 163)
	maximum = 0.0178 (at node 116)
Accepted packet rate average = 0.0123719
	minimum = 0.0092 (at node 1)
	maximum = 0.0162 (at node 129)
Injected flit rate average = 0.244522
	minimum = 0.191 (at node 163)
	maximum = 0.3178 (at node 116)
Accepted flit rate average= 0.224127
	minimum = 0.1608 (at node 162)
	maximum = 0.2936 (at node 129)
Injected packet length average = 18.014
Accepted packet length average = 18.1159
Total in-flight flits = 42903 (40960 measured)
latency change    = 0.102171
throughput change = 0.00265846
Class 0:
Packet latency average = 631.385
	minimum = 23
	maximum = 5754
Network latency average = 628.785
	minimum = 23
	maximum = 5754
Slowest packet = 7912
Flit latency average = 599.38
	minimum = 6
	maximum = 8416
Slowest flit = 13109
Fragmentation average = 237.93
	minimum = 0
	maximum = 4078
Injected packet rate average = 0.013599
	minimum = 0.0103333 (at node 163)
	maximum = 0.0173333 (at node 165)
Accepted packet rate average = 0.0124097
	minimum = 0.00933333 (at node 17)
	maximum = 0.0155 (at node 103)
Injected flit rate average = 0.244839
	minimum = 0.186167 (at node 163)
	maximum = 0.311 (at node 165)
Accepted flit rate average= 0.224625
	minimum = 0.172833 (at node 17)
	maximum = 0.2775 (at node 103)
Injected packet length average = 18.0043
Accepted packet length average = 18.1007
Total in-flight flits = 46727 (45248 measured)
latency change    = 0.077101
throughput change = 0.00221666
Class 0:
Packet latency average = 676.605
	minimum = 23
	maximum = 6129
Network latency average = 673.935
	minimum = 23
	maximum = 6129
Slowest packet = 8663
Flit latency average = 632.785
	minimum = 6
	maximum = 8808
Slowest flit = 13113
Fragmentation average = 245.257
	minimum = 0
	maximum = 4740
Injected packet rate average = 0.0135863
	minimum = 0.0104286 (at node 163)
	maximum = 0.0168571 (at node 165)
Accepted packet rate average = 0.0124182
	minimum = 0.00971429 (at node 139)
	maximum = 0.0154286 (at node 128)
Injected flit rate average = 0.244568
	minimum = 0.187 (at node 163)
	maximum = 0.303429 (at node 165)
Accepted flit rate average= 0.224647
	minimum = 0.172857 (at node 149)
	maximum = 0.276857 (at node 128)
Injected packet length average = 18.001
Accepted packet length average = 18.0902
Total in-flight flits = 50262 (49114 measured)
latency change    = 0.0668338
throughput change = 9.60503e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 16164 16165 16166 16167 16168 16169 16170 16171 16172 16173 [...] (54752 flits)
Measured flits: 140264 140265 140266 140267 140268 140269 140270 140271 140272 140273 [...] (26726 flits)
Class 0:
Remaining flits: 16164 16165 16166 16167 16168 16169 16170 16171 16172 16173 [...] (57007 flits)
Measured flits: 140562 140563 140564 140565 140566 140567 140568 140569 140570 140571 [...] (17578 flits)
Class 0:
Remaining flits: 16164 16165 16166 16167 16168 16169 16170 16171 16172 16173 [...] (60103 flits)
Measured flits: 140562 140563 140564 140565 140566 140567 140568 140569 140570 140571 [...] (12773 flits)
Class 0:
Remaining flits: 16164 16165 16166 16167 16168 16169 16170 16171 16172 16173 [...] (64061 flits)
Measured flits: 140562 140563 140564 140565 140566 140567 140568 140569 140570 140571 [...] (9267 flits)
Class 0:
Remaining flits: 16164 16165 16166 16167 16168 16169 16170 16171 16172 16173 [...] (67443 flits)
Measured flits: 140562 140563 140564 140565 140566 140567 140568 140569 140570 140571 [...] (7232 flits)
Class 0:
Remaining flits: 16175 16176 16177 16178 16179 16180 16181 33462 33463 33464 [...] (73777 flits)
Measured flits: 140562 140563 140564 140565 140566 140567 140568 140569 140570 140571 [...] (5710 flits)
Class 0:
Remaining flits: 33469 33470 33471 33472 33473 33474 33475 33476 33477 33478 [...] (76804 flits)
Measured flits: 145566 145567 145568 145569 145570 145571 145572 145573 145574 145575 [...] (4514 flits)
Class 0:
Remaining flits: 37764 37765 37766 37767 37768 37769 37770 37771 37772 37773 [...] (81445 flits)
Measured flits: 145566 145567 145568 145569 145570 145571 145572 145573 145574 145575 [...] (3601 flits)
Class 0:
Remaining flits: 37767 37768 37769 37770 37771 37772 37773 37774 37775 37776 [...] (86045 flits)
Measured flits: 145566 145567 145568 145569 145570 145571 145572 145573 145574 145575 [...] (2934 flits)
Class 0:
Remaining flits: 37767 37768 37769 37770 37771 37772 37773 37774 37775 37776 [...] (90990 flits)
Measured flits: 145576 145577 145578 145579 145580 145581 145582 145583 151812 151813 [...] (2489 flits)
Class 0:
Remaining flits: 37767 37768 37769 37770 37771 37772 37773 37774 37775 37776 [...] (96020 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (2174 flits)
Class 0:
Remaining flits: 42534 42535 42536 42537 42538 42539 42540 42541 42542 42543 [...] (101626 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (1825 flits)
Class 0:
Remaining flits: 42534 42535 42536 42537 42538 42539 42540 42541 42542 42543 [...] (107436 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (1522 flits)
Class 0:
Remaining flits: 42534 42535 42536 42537 42538 42539 42540 42541 42542 42543 [...] (112645 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (1338 flits)
Class 0:
Remaining flits: 42534 42535 42536 42537 42538 42539 42540 42541 42542 42543 [...] (117489 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (1164 flits)
Class 0:
Remaining flits: 42534 42535 42536 42537 42538 42539 42540 42541 42542 42543 [...] (123916 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (1070 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (130473 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (937 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (136018 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (779 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (141990 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (685 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (148766 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (539 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (154310 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (490 flits)
Class 0:
Remaining flits: 42537 42538 42539 42540 42541 42542 42543 42544 42545 42546 [...] (159566 flits)
Measured flits: 151812 151813 151814 151815 151816 151817 151818 151819 151820 151821 [...] (462 flits)
Class 0:
Remaining flits: 42550 42551 66807 66808 66809 66810 66811 66812 66813 66814 [...] (164378 flits)
Measured flits: 151826 151827 151828 151829 175536 175537 175538 175539 175540 175541 [...] (363 flits)
Class 0:
Remaining flits: 42550 42551 66807 66808 66809 66810 66811 66812 66813 66814 [...] (170463 flits)
Measured flits: 151827 151828 151829 175536 175537 175538 175539 175540 175541 175542 [...] (339 flits)
Class 0:
Remaining flits: 42550 42551 66807 66808 66809 66810 66811 66812 66813 66814 [...] (176069 flits)
Measured flits: 151827 151828 151829 175536 175537 175538 175539 175540 175541 175542 [...] (328 flits)
Class 0:
Remaining flits: 66807 66808 66809 66810 66811 66812 66813 66814 66815 69552 [...] (182161 flits)
Measured flits: 175536 175537 175538 175539 175540 175541 175542 175543 175544 175545 [...] (298 flits)
Class 0:
Remaining flits: 66807 66808 66809 66810 66811 66812 66813 66814 66815 69552 [...] (188901 flits)
Measured flits: 175536 175537 175538 175539 175540 175541 175542 175543 175544 175545 [...] (296 flits)
Class 0:
Remaining flits: 66807 66808 66809 66810 66811 66812 66813 66814 66815 69552 [...] (195203 flits)
Measured flits: 175536 175537 175538 175539 175540 175541 175542 175543 175544 175545 [...] (245 flits)
Class 0:
Remaining flits: 66807 66808 66809 66810 66811 66812 66813 66814 66815 69552 [...] (203703 flits)
Measured flits: 182304 182305 182306 182307 182308 182309 182310 182311 182312 182313 [...] (192 flits)
Class 0:
Remaining flits: 66807 66808 66809 66810 66811 66812 66813 66814 66815 69552 [...] (211230 flits)
Measured flits: 182304 182305 182306 182307 182308 182309 182310 182311 182312 182313 [...] (180 flits)
Class 0:
Remaining flits: 69552 69553 69554 69555 69556 69557 69558 69559 69560 69561 [...] (218184 flits)
Measured flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (92 flits)
Class 0:
Remaining flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (226265 flits)
Measured flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (65 flits)
Class 0:
Remaining flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (232725 flits)
Measured flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (54 flits)
Class 0:
Remaining flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (239556 flits)
Measured flits: 194382 194383 194384 194385 194386 194387 194388 194389 194390 194391 [...] (54 flits)
Class 0:
Remaining flits: 194389 194390 194391 194392 194393 194394 194395 194396 194397 194398 [...] (246397 flits)
Measured flits: 194389 194390 194391 194392 194393 194394 194395 194396 194397 194398 [...] (11 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 507312 507313 507314 507315 507316 507317 507318 507319 507320 507321 [...] (211579 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 507312 507313 507314 507315 507316 507317 507318 507319 507320 507321 [...] (176000 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 507312 507313 507314 507315 507316 507317 507318 507319 507320 507321 [...] (140675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 507312 507313 507314 507315 507316 507317 507318 507319 507320 507321 [...] (106286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 862479 862480 862481 862482 862483 862484 862485 862486 862487 946188 [...] (75335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 988157 988158 988159 988160 988161 988162 988163 1240596 1240597 1240598 [...] (48151 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1240596 1240597 1240598 1240599 1240600 1240601 1240602 1240603 1240604 1240605 [...] (27194 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1240596 1240597 1240598 1240599 1240600 1240601 1240602 1240603 1240604 1240605 [...] (15050 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1277880 1277881 1277882 1277883 1277884 1277885 1277886 1277887 1277888 1277889 [...] (7230 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1506879 1506880 1506881 1506882 1506883 1506884 1506885 1506886 1506887 1519218 [...] (2269 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1953196 1953197 1984716 1984717 1984718 1984719 1984720 1984721 1984722 1984723 [...] (56 flits)
Measured flits: (0 flits)
Time taken is 56176 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1286.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 40979 (1 samples)
Network latency average = 1283.53 (1 samples)
	minimum = 23 (1 samples)
	maximum = 40966 (1 samples)
Flit latency average = 3017.63 (1 samples)
	minimum = 6 (1 samples)
	maximum = 40949 (1 samples)
Fragmentation average = 345.457 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18095 (1 samples)
Injected packet rate average = 0.0135863 (1 samples)
	minimum = 0.0104286 (1 samples)
	maximum = 0.0168571 (1 samples)
Accepted packet rate average = 0.0124182 (1 samples)
	minimum = 0.00971429 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.244568 (1 samples)
	minimum = 0.187 (1 samples)
	maximum = 0.303429 (1 samples)
Accepted flit rate average = 0.224647 (1 samples)
	minimum = 0.172857 (1 samples)
	maximum = 0.276857 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 18.0902 (1 samples)
Hops average = 5.05853 (1 samples)
Total run time 66.1139
