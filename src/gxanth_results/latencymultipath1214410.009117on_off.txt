BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 189.391
	minimum = 23
	maximum = 892
Network latency average = 156.169
	minimum = 23
	maximum = 779
Slowest packet = 367
Flit latency average = 118.815
	minimum = 6
	maximum = 790
Slowest flit = 8704
Fragmentation average = 43.2087
	minimum = 0
	maximum = 124
Injected packet rate average = 0.00982292
	minimum = 0 (at node 17)
	maximum = 0.032 (at node 13)
Accepted packet rate average = 0.0075625
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 73)
Injected flit rate average = 0.175427
	minimum = 0 (at node 17)
	maximum = 0.576 (at node 13)
Accepted flit rate average= 0.142036
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 73)
Injected packet length average = 17.859
Accepted packet length average = 18.7817
Total in-flight flits = 6677 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 240.916
	minimum = 23
	maximum = 1261
Network latency average = 206.031
	minimum = 23
	maximum = 1145
Slowest packet = 1297
Flit latency average = 162.794
	minimum = 6
	maximum = 1128
Slowest flit = 28799
Fragmentation average = 51.9054
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00913542
	minimum = 0.0005 (at node 34)
	maximum = 0.022 (at node 88)
Accepted packet rate average = 0.007875
	minimum = 0.004 (at node 174)
	maximum = 0.013 (at node 44)
Injected flit rate average = 0.163831
	minimum = 0.009 (at node 34)
	maximum = 0.3905 (at node 88)
Accepted flit rate average= 0.14475
	minimum = 0.078 (at node 174)
	maximum = 0.234 (at node 44)
Injected packet length average = 17.9336
Accepted packet length average = 18.381
Total in-flight flits = 7560 (0 measured)
latency change    = 0.213869
throughput change = 0.0187464
Class 0:
Packet latency average = 352.771
	minimum = 27
	maximum = 1956
Network latency average = 310.302
	minimum = 27
	maximum = 1794
Slowest packet = 1696
Flit latency average = 262.026
	minimum = 6
	maximum = 1777
Slowest flit = 30545
Fragmentation average = 65.4979
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0100208
	minimum = 0 (at node 14)
	maximum = 0.04 (at node 15)
Accepted packet rate average = 0.00878646
	minimum = 0.001 (at node 153)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.180245
	minimum = 0 (at node 14)
	maximum = 0.725 (at node 15)
Accepted flit rate average= 0.158474
	minimum = 0.018 (at node 153)
	maximum = 0.323 (at node 78)
Injected packet length average = 17.987
Accepted packet length average = 18.0362
Total in-flight flits = 11765 (0 measured)
latency change    = 0.317077
throughput change = 0.0866007
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 285.629
	minimum = 26
	maximum = 1428
Network latency average = 244.659
	minimum = 25
	maximum = 910
Slowest packet = 5437
Flit latency average = 336.501
	minimum = 6
	maximum = 1952
Slowest flit = 57671
Fragmentation average = 59.358
	minimum = 0
	maximum = 143
Injected packet rate average = 0.00898958
	minimum = 0 (at node 67)
	maximum = 0.04 (at node 187)
Accepted packet rate average = 0.00883854
	minimum = 0.002 (at node 184)
	maximum = 0.015 (at node 24)
Injected flit rate average = 0.161443
	minimum = 0 (at node 67)
	maximum = 0.72 (at node 187)
Accepted flit rate average= 0.159151
	minimum = 0.036 (at node 184)
	maximum = 0.281 (at node 56)
Injected packet length average = 17.9589
Accepted packet length average = 18.0065
Total in-flight flits = 12276 (11233 measured)
latency change    = 0.235066
throughput change = 0.00425434
Class 0:
Packet latency average = 382.701
	minimum = 26
	maximum = 1947
Network latency average = 341.905
	minimum = 25
	maximum = 1787
Slowest packet = 5437
Flit latency average = 367.153
	minimum = 6
	maximum = 2886
Slowest flit = 58283
Fragmentation average = 60.7991
	minimum = 0
	maximum = 146
Injected packet rate average = 0.00898958
	minimum = 0 (at node 67)
	maximum = 0.0225 (at node 17)
Accepted packet rate average = 0.0088151
	minimum = 0.0045 (at node 118)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.16181
	minimum = 0 (at node 67)
	maximum = 0.408 (at node 17)
Accepted flit rate average= 0.158815
	minimum = 0.0795 (at node 163)
	maximum = 0.301 (at node 16)
Injected packet length average = 17.9997
Accepted packet length average = 18.0162
Total in-flight flits = 12916 (12868 measured)
latency change    = 0.253648
throughput change = 0.00211527
Class 0:
Packet latency average = 418.514
	minimum = 26
	maximum = 2703
Network latency average = 380.395
	minimum = 25
	maximum = 2687
Slowest packet = 5543
Flit latency average = 377.866
	minimum = 6
	maximum = 2886
Slowest flit = 58283
Fragmentation average = 61.6637
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00886458
	minimum = 0.000333333 (at node 67)
	maximum = 0.0223333 (at node 1)
Accepted packet rate average = 0.0087934
	minimum = 0.005 (at node 190)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.159609
	minimum = 0.006 (at node 67)
	maximum = 0.403 (at node 1)
Accepted flit rate average= 0.158066
	minimum = 0.09 (at node 190)
	maximum = 0.285667 (at node 16)
Injected packet length average = 18.0053
Accepted packet length average = 17.9755
Total in-flight flits = 12627 (12627 measured)
latency change    = 0.085573
throughput change = 0.00473936
Class 0:
Packet latency average = 441.142
	minimum = 26
	maximum = 3892
Network latency average = 403.186
	minimum = 23
	maximum = 3375
Slowest packet = 5687
Flit latency average = 385.109
	minimum = 6
	maximum = 3350
Slowest flit = 110735
Fragmentation average = 62.6646
	minimum = 0
	maximum = 157
Injected packet rate average = 0.00874089
	minimum = 0.0025 (at node 118)
	maximum = 0.01875 (at node 1)
Accepted packet rate average = 0.00876042
	minimum = 0.00475 (at node 4)
	maximum = 0.01375 (at node 16)
Injected flit rate average = 0.157311
	minimum = 0.045 (at node 118)
	maximum = 0.34075 (at node 1)
Accepted flit rate average= 0.157566
	minimum = 0.0875 (at node 4)
	maximum = 0.24625 (at node 16)
Injected packet length average = 17.9972
Accepted packet length average = 17.9862
Total in-flight flits = 11606 (11606 measured)
latency change    = 0.0512937
throughput change = 0.00317051
Class 0:
Packet latency average = 445.691
	minimum = 23
	maximum = 3892
Network latency average = 407.538
	minimum = 23
	maximum = 3780
Slowest packet = 5687
Flit latency average = 382.413
	minimum = 6
	maximum = 3763
Slowest flit = 135467
Fragmentation average = 62.7828
	minimum = 0
	maximum = 157
Injected packet rate average = 0.00886979
	minimum = 0.0028 (at node 67)
	maximum = 0.0172 (at node 1)
Accepted packet rate average = 0.00870313
	minimum = 0.006 (at node 4)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.159608
	minimum = 0.0504 (at node 67)
	maximum = 0.3122 (at node 1)
Accepted flit rate average= 0.156917
	minimum = 0.1082 (at node 132)
	maximum = 0.2326 (at node 16)
Injected packet length average = 17.9946
Accepted packet length average = 18.0299
Total in-flight flits = 14395 (14395 measured)
latency change    = 0.0102075
throughput change = 0.00414067
Class 0:
Packet latency average = 456.372
	minimum = 23
	maximum = 4307
Network latency average = 417.659
	minimum = 23
	maximum = 4249
Slowest packet = 7566
Flit latency average = 388.004
	minimum = 6
	maximum = 4232
Slowest flit = 136205
Fragmentation average = 62.9357
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00890278
	minimum = 0.00316667 (at node 37)
	maximum = 0.0178333 (at node 7)
Accepted packet rate average = 0.00878819
	minimum = 0.00616667 (at node 36)
	maximum = 0.012 (at node 129)
Injected flit rate average = 0.160194
	minimum = 0.0548333 (at node 37)
	maximum = 0.321 (at node 7)
Accepted flit rate average= 0.158112
	minimum = 0.1095 (at node 36)
	maximum = 0.215667 (at node 129)
Injected packet length average = 17.9938
Accepted packet length average = 17.9914
Total in-flight flits = 14228 (14228 measured)
latency change    = 0.0234036
throughput change = 0.00755991
Class 0:
Packet latency average = 473.707
	minimum = 23
	maximum = 4542
Network latency average = 434.641
	minimum = 23
	maximum = 4450
Slowest packet = 9372
Flit latency average = 400.84
	minimum = 6
	maximum = 4433
Slowest flit = 173825
Fragmentation average = 63.7464
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00896354
	minimum = 0.00342857 (at node 155)
	maximum = 0.0164286 (at node 7)
Accepted packet rate average = 0.00877381
	minimum = 0.00628571 (at node 36)
	maximum = 0.0115714 (at node 129)
Injected flit rate average = 0.161374
	minimum = 0.0617143 (at node 155)
	maximum = 0.295714 (at node 7)
Accepted flit rate average= 0.157978
	minimum = 0.111857 (at node 36)
	maximum = 0.206429 (at node 129)
Injected packet length average = 18.0033
Accepted packet length average = 18.0057
Total in-flight flits = 16306 (16306 measured)
latency change    = 0.036595
throughput change = 0.00084541
Draining all recorded packets ...
Class 0:
Remaining flits: 251082 251083 251084 251085 251086 251087 251088 251089 251090 251091 [...] (17389 flits)
Measured flits: 251082 251083 251084 251085 251086 251087 251088 251089 251090 251091 [...] (3311 flits)
Class 0:
Remaining flits: 251082 251083 251084 251085 251086 251087 251088 251089 251090 251091 [...] (19120 flits)
Measured flits: 251082 251083 251084 251085 251086 251087 251088 251089 251090 251091 [...] (1045 flits)
Class 0:
Remaining flits: 270648 270649 270650 270651 270652 270653 270654 270655 270656 270657 [...] (20055 flits)
Measured flits: 270648 270649 270650 270651 270652 270653 270654 270655 270656 270657 [...] (321 flits)
Class 0:
Remaining flits: 316992 316993 316994 316995 316996 316997 320562 320563 320564 320565 [...] (22027 flits)
Measured flits: 316992 316993 316994 316995 316996 316997 335772 335773 335774 335775 [...] (60 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 358092 358093 358094 358095 358096 358097 358098 358099 358100 358101 [...] (2616 flits)
Measured flits: (0 flits)
Time taken is 16054 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 536.344 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6127 (1 samples)
Network latency average = 489.191 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4856 (1 samples)
Flit latency average = 509.591 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4842 (1 samples)
Fragmentation average = 64.5743 (1 samples)
	minimum = 0 (1 samples)
	maximum = 170 (1 samples)
Injected packet rate average = 0.00896354 (1 samples)
	minimum = 0.00342857 (1 samples)
	maximum = 0.0164286 (1 samples)
Accepted packet rate average = 0.00877381 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0115714 (1 samples)
Injected flit rate average = 0.161374 (1 samples)
	minimum = 0.0617143 (1 samples)
	maximum = 0.295714 (1 samples)
Accepted flit rate average = 0.157978 (1 samples)
	minimum = 0.111857 (1 samples)
	maximum = 0.206429 (1 samples)
Injected packet size average = 18.0033 (1 samples)
Accepted packet size average = 18.0057 (1 samples)
Hops average = 5.07009 (1 samples)
Total run time 7.46137
