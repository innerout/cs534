BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 228.166
	minimum = 24
	maximum = 889
Network latency average = 162.996
	minimum = 22
	maximum = 717
Slowest packet = 21
Flit latency average = 139.956
	minimum = 5
	maximum = 728
Slowest flit = 16573
Fragmentation average = 15.6822
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0123229
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22688
	minimum = 0.108 (at node 41)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.4112
Total in-flight flits = 18449 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 354.558
	minimum = 22
	maximum = 1839
Network latency average = 273.277
	minimum = 22
	maximum = 1287
Slowest packet = 21
Flit latency average = 249.266
	minimum = 5
	maximum = 1270
Slowest flit = 38663
Fragmentation average = 17.0856
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0132604
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241073
	minimum = 0.148 (at node 83)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.1799
Total in-flight flits = 27020 (0 measured)
latency change    = 0.356478
throughput change = 0.0588731
Class 0:
Packet latency average = 622.755
	minimum = 22
	maximum = 2135
Network latency average = 521.076
	minimum = 22
	maximum = 1828
Slowest packet = 3034
Flit latency average = 496.442
	minimum = 5
	maximum = 1902
Slowest flit = 67297
Fragmentation average = 17.6736
	minimum = 0
	maximum = 83
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145208
	minimum = 0.004 (at node 118)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261328
	minimum = 0.072 (at node 118)
	maximum = 0.433 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9968
Total in-flight flits = 39215 (0 measured)
latency change    = 0.430662
throughput change = 0.0775087
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.18
	minimum = 25
	maximum = 1442
Network latency average = 314.33
	minimum = 22
	maximum = 988
Slowest packet = 10139
Flit latency average = 644.804
	minimum = 5
	maximum = 2293
Slowest flit = 81000
Fragmentation average = 14.258
	minimum = 0
	maximum = 83
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148542
	minimum = 0.007 (at node 57)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267365
	minimum = 0.126 (at node 141)
	maximum = 0.468 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 17.9993
Total in-flight flits = 49819 (41687 measured)
latency change    = 0.525685
throughput change = 0.0225776
Class 0:
Packet latency average = 670.787
	minimum = 22
	maximum = 2173
Network latency average = 573.148
	minimum = 22
	maximum = 1937
Slowest packet = 10139
Flit latency average = 725.444
	minimum = 5
	maximum = 2403
Slowest flit = 152729
Fragmentation average = 16.631
	minimum = 0
	maximum = 84
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0148359
	minimum = 0.0095 (at node 5)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.266992
	minimum = 0.171 (at node 5)
	maximum = 0.413 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 17.9963
Total in-flight flits = 56336 (55285 measured)
latency change    = 0.39149
throughput change = 0.00139478
Class 0:
Packet latency average = 834.625
	minimum = 22
	maximum = 3447
Network latency average = 737.197
	minimum = 22
	maximum = 2906
Slowest packet = 10139
Flit latency average = 801.277
	minimum = 5
	maximum = 3036
Slowest flit = 178928
Fragmentation average = 16.9403
	minimum = 0
	maximum = 95
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0148594
	minimum = 0.0103333 (at node 135)
	maximum = 0.021 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267509
	minimum = 0.188667 (at node 135)
	maximum = 0.382667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.0027
Total in-flight flits = 66264 (66255 measured)
latency change    = 0.196302
throughput change = 0.00193075
Class 0:
Packet latency average = 957.731
	minimum = 22
	maximum = 3792
Network latency average = 858.514
	minimum = 22
	maximum = 3510
Slowest packet = 10139
Flit latency average = 877.197
	minimum = 5
	maximum = 3577
Slowest flit = 207261
Fragmentation average = 17.2523
	minimum = 0
	maximum = 95
Injected packet rate average = 0.017625
	minimum = 0.0025 (at node 120)
	maximum = 0.038 (at node 17)
Accepted packet rate average = 0.0148242
	minimum = 0.01125 (at node 64)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.317267
	minimum = 0.045 (at node 120)
	maximum = 0.683 (at node 17)
Accepted flit rate average= 0.266974
	minimum = 0.20025 (at node 64)
	maximum = 0.34825 (at node 128)
Injected packet length average = 18.001
Accepted packet length average = 18.0093
Total in-flight flits = 77827 (77827 measured)
latency change    = 0.128539
throughput change = 0.0020029
Class 0:
Packet latency average = 1069.31
	minimum = 22
	maximum = 3984
Network latency average = 969.354
	minimum = 22
	maximum = 3923
Slowest packet = 13523
Flit latency average = 962.105
	minimum = 5
	maximum = 3906
Slowest flit = 243427
Fragmentation average = 17.5684
	minimum = 0
	maximum = 95
Injected packet rate average = 0.0175719
	minimum = 0.0032 (at node 89)
	maximum = 0.0388 (at node 23)
Accepted packet rate average = 0.0148406
	minimum = 0.0112 (at node 86)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.31629
	minimum = 0.0576 (at node 89)
	maximum = 0.6994 (at node 23)
Accepted flit rate average= 0.26723
	minimum = 0.2016 (at node 86)
	maximum = 0.3428 (at node 118)
Injected packet length average = 17.9998
Accepted packet length average = 18.0067
Total in-flight flits = 86316 (86316 measured)
latency change    = 0.104345
throughput change = 0.000958911
Class 0:
Packet latency average = 1170.86
	minimum = 22
	maximum = 4221
Network latency average = 1069.24
	minimum = 22
	maximum = 4111
Slowest packet = 13700
Flit latency average = 1045.95
	minimum = 5
	maximum = 4094
Slowest flit = 246617
Fragmentation average = 17.8181
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0177665
	minimum = 0.00516667 (at node 89)
	maximum = 0.0361667 (at node 23)
Accepted packet rate average = 0.0148785
	minimum = 0.0116667 (at node 80)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.319868
	minimum = 0.093 (at node 89)
	maximum = 0.651 (at node 23)
Accepted flit rate average= 0.267886
	minimum = 0.210833 (at node 80)
	maximum = 0.3425 (at node 118)
Injected packet length average = 18.004
Accepted packet length average = 18.005
Total in-flight flits = 99016 (99016 measured)
latency change    = 0.0867325
throughput change = 0.00244909
Class 0:
Packet latency average = 1252.31
	minimum = 22
	maximum = 4221
Network latency average = 1152.04
	minimum = 22
	maximum = 4111
Slowest packet = 13700
Flit latency average = 1119.21
	minimum = 5
	maximum = 4094
Slowest flit = 246617
Fragmentation average = 17.8452
	minimum = 0
	maximum = 100
Injected packet rate average = 0.017744
	minimum = 0.00557143 (at node 89)
	maximum = 0.0318571 (at node 23)
Accepted packet rate average = 0.0149122
	minimum = 0.012 (at node 64)
	maximum = 0.0187143 (at node 118)
Injected flit rate average = 0.319408
	minimum = 0.100286 (at node 89)
	maximum = 0.575429 (at node 23)
Accepted flit rate average= 0.268457
	minimum = 0.216429 (at node 64)
	maximum = 0.336857 (at node 118)
Injected packet length average = 18.0009
Accepted packet length average = 18.0025
Total in-flight flits = 107673 (107673 measured)
latency change    = 0.0650375
throughput change = 0.00212533
Draining all recorded packets ...
Class 0:
Remaining flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (120687 flits)
Measured flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (68497 flits)
Class 0:
Remaining flits: 475665 475666 475667 476982 476983 476984 476985 476986 476987 476988 [...] (129934 flits)
Measured flits: 475665 475666 475667 476982 476983 476984 476985 476986 476987 476988 [...] (31935 flits)
Class 0:
Remaining flits: 503557 503558 503559 503560 503561 503562 503563 503564 503565 503566 [...] (139002 flits)
Measured flits: 503557 503558 503559 503560 503561 503562 503563 503564 503565 503566 [...] (10630 flits)
Class 0:
Remaining flits: 546768 546769 546770 546771 546772 546773 546774 546775 546776 546777 [...] (150001 flits)
Measured flits: 546768 546769 546770 546771 546772 546773 546774 546775 546776 546777 [...] (2160 flits)
Class 0:
Remaining flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (161338 flits)
Measured flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (144 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 668592 668593 668594 668595 668596 668597 668598 668599 668600 668601 [...] (116762 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 725292 725293 725294 725295 725296 725297 725298 725299 725300 725301 [...] (71867 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 767801 767802 767803 767804 767805 767806 767807 777348 777349 777350 [...] (34876 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 798606 798607 798608 798609 798610 798611 798612 798613 798614 798615 [...] (13541 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 874179 874180 874181 874182 874183 874184 874185 874186 874187 874557 [...] (4252 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 920958 920959 920960 920961 920962 920963 920964 920965 920966 920967 [...] (704 flits)
Measured flits: (0 flits)
Time taken is 21586 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1608.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5658 (1 samples)
Network latency average = 1506.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5393 (1 samples)
Flit latency average = 1944.29 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6464 (1 samples)
Fragmentation average = 18.5836 (1 samples)
	minimum = 0 (1 samples)
	maximum = 110 (1 samples)
Injected packet rate average = 0.017744 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0318571 (1 samples)
Accepted packet rate average = 0.0149122 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.319408 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.575429 (1 samples)
Accepted flit rate average = 0.268457 (1 samples)
	minimum = 0.216429 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.0025 (1 samples)
Hops average = 5.0647 (1 samples)
Total run time 12.1828
