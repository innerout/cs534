BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.569
	minimum = 24
	maximum = 884
Network latency average = 164.333
	minimum = 22
	maximum = 747
Slowest packet = 64
Flit latency average = 136.812
	minimum = 5
	maximum = 745
Slowest flit = 14990
Fragmentation average = 28.8525
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.01225
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22788
	minimum = 0.108 (at node 41)
	maximum = 0.442 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6025
Total in-flight flits = 18257 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 355.905
	minimum = 22
	maximum = 1755
Network latency average = 275.021
	minimum = 22
	maximum = 1322
Slowest packet = 64
Flit latency average = 245.416
	minimum = 5
	maximum = 1413
Slowest flit = 33114
Fragmentation average = 35.0354
	minimum = 0
	maximum = 548
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0131849
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241599
	minimum = 0.153 (at node 83)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.3239
Total in-flight flits = 26818 (0 measured)
latency change    = 0.354971
throughput change = 0.0567832
Class 0:
Packet latency average = 629.938
	minimum = 22
	maximum = 2428
Network latency average = 527.649
	minimum = 22
	maximum = 2021
Slowest packet = 2901
Flit latency average = 492.646
	minimum = 5
	maximum = 2066
Slowest flit = 56778
Fragmentation average = 43.7914
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145313
	minimum = 0.004 (at node 118)
	maximum = 0.025 (at node 57)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261432
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.991
Total in-flight flits = 38993 (0 measured)
latency change    = 0.435017
throughput change = 0.0758641
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.575
	minimum = 25
	maximum = 1427
Network latency average = 315.059
	minimum = 22
	maximum = 986
Slowest packet = 10126
Flit latency average = 637.743
	minimum = 5
	maximum = 2511
Slowest flit = 62801
Fragmentation average = 29.4123
	minimum = 0
	maximum = 318
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148125
	minimum = 0.007 (at node 57)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267599
	minimum = 0.114 (at node 117)
	maximum = 0.483 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 18.0658
Total in-flight flits = 49552 (41388 measured)
latency change    = 0.541794
throughput change = 0.0230444
Class 0:
Packet latency average = 672.476
	minimum = 22
	maximum = 2170
Network latency average = 575.343
	minimum = 22
	maximum = 1970
Slowest packet = 10126
Flit latency average = 719.631
	minimum = 5
	maximum = 2511
Slowest flit = 62801
Fragmentation average = 40.5752
	minimum = 0
	maximum = 695
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0148177
	minimum = 0.009 (at node 110)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.267266
	minimum = 0.171 (at node 36)
	maximum = 0.422 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 18.0369
Total in-flight flits = 56009 (54890 measured)
latency change    = 0.392432
throughput change = 0.0012472
Class 0:
Packet latency average = 836.815
	minimum = 22
	maximum = 3423
Network latency average = 739.454
	minimum = 22
	maximum = 2946
Slowest packet = 10126
Flit latency average = 794.032
	minimum = 5
	maximum = 3296
Slowest flit = 162016
Fragmentation average = 44.6468
	minimum = 0
	maximum = 695
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.014849
	minimum = 0.0103333 (at node 135)
	maximum = 0.0213333 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267741
	minimum = 0.190667 (at node 144)
	maximum = 0.381667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.031
Total in-flight flits = 65908 (65791 measured)
latency change    = 0.196386
throughput change = 0.00177669
Class 0:
Packet latency average = 960.94
	minimum = 22
	maximum = 3986
Network latency average = 862.061
	minimum = 22
	maximum = 3644
Slowest packet = 10126
Flit latency average = 870.557
	minimum = 5
	maximum = 4122
Slowest flit = 166031
Fragmentation average = 49.727
	minimum = 0
	maximum = 695
Injected packet rate average = 0.017625
	minimum = 0.0025 (at node 120)
	maximum = 0.038 (at node 17)
Accepted packet rate average = 0.0148411
	minimum = 0.011 (at node 144)
	maximum = 0.0195 (at node 138)
Injected flit rate average = 0.317267
	minimum = 0.045 (at node 120)
	maximum = 0.683 (at node 17)
Accepted flit rate average= 0.267465
	minimum = 0.20275 (at node 144)
	maximum = 0.35375 (at node 118)
Injected packet length average = 18.001
Accepted packet length average = 18.0218
Total in-flight flits = 77228 (77228 measured)
latency change    = 0.12917
throughput change = 0.00103369
Class 0:
Packet latency average = 1075.62
	minimum = 22
	maximum = 4188
Network latency average = 975.758
	minimum = 22
	maximum = 4090
Slowest packet = 10126
Flit latency average = 955.247
	minimum = 5
	maximum = 4122
Slowest flit = 166031
Fragmentation average = 50.8308
	minimum = 0
	maximum = 695
Injected packet rate average = 0.0175719
	minimum = 0.0032 (at node 89)
	maximum = 0.0388 (at node 23)
Accepted packet rate average = 0.0148573
	minimum = 0.0114 (at node 64)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.31629
	minimum = 0.0576 (at node 89)
	maximum = 0.6994 (at node 23)
Accepted flit rate average= 0.267464
	minimum = 0.2036 (at node 86)
	maximum = 0.345 (at node 118)
Injected packet length average = 17.9998
Accepted packet length average = 18.0022
Total in-flight flits = 85870 (85870 measured)
latency change    = 0.106615
throughput change = 4.86826e-06
Class 0:
Packet latency average = 1173.47
	minimum = 22
	maximum = 4803
Network latency average = 1072
	minimum = 22
	maximum = 4693
Slowest packet = 13700
Flit latency average = 1037.01
	minimum = 5
	maximum = 4676
Slowest flit = 246616
Fragmentation average = 51.7686
	minimum = 0
	maximum = 756
Injected packet rate average = 0.0177665
	minimum = 0.00516667 (at node 89)
	maximum = 0.0361667 (at node 23)
Accepted packet rate average = 0.0148646
	minimum = 0.0118333 (at node 80)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.319868
	minimum = 0.093 (at node 89)
	maximum = 0.651 (at node 23)
Accepted flit rate average= 0.268102
	minimum = 0.212667 (at node 80)
	maximum = 0.344 (at node 118)
Injected packet length average = 18.004
Accepted packet length average = 18.0363
Total in-flight flits = 98546 (98546 measured)
latency change    = 0.0833893
throughput change = 0.00237977
Class 0:
Packet latency average = 1258.48
	minimum = 22
	maximum = 5022
Network latency average = 1158.27
	minimum = 22
	maximum = 4963
Slowest packet = 13995
Flit latency average = 1111.64
	minimum = 5
	maximum = 4946
Slowest flit = 251927
Fragmentation average = 53.3555
	minimum = 0
	maximum = 756
Injected packet rate average = 0.0177403
	minimum = 0.00557143 (at node 89)
	maximum = 0.032 (at node 23)
Accepted packet rate average = 0.0149048
	minimum = 0.012 (at node 64)
	maximum = 0.0187143 (at node 118)
Injected flit rate average = 0.319364
	minimum = 0.100286 (at node 89)
	maximum = 0.576714 (at node 23)
Accepted flit rate average= 0.268678
	minimum = 0.217571 (at node 64)
	maximum = 0.336857 (at node 118)
Injected packet length average = 18.0021
Accepted packet length average = 18.0263
Total in-flight flits = 107064 (107064 measured)
latency change    = 0.0675467
throughput change = 0.00214482
Draining all recorded packets ...
Class 0:
Remaining flits: 385650 385651 385652 385653 385654 385655 385656 385657 385658 385659 [...] (119309 flits)
Measured flits: 385650 385651 385652 385653 385654 385655 385656 385657 385658 385659 [...] (67460 flits)
Class 0:
Remaining flits: 419233 419234 419235 419236 419237 420372 420373 420374 420375 420376 [...] (132610 flits)
Measured flits: 419233 419234 419235 419236 419237 420372 420373 420374 420375 420376 [...] (31487 flits)
Class 0:
Remaining flits: 463708 463709 463710 463711 463712 463713 463714 463715 466866 466867 [...] (139487 flits)
Measured flits: 463708 463709 463710 463711 463712 463713 463714 463715 466866 466867 [...] (10669 flits)
Class 0:
Remaining flits: 490932 490933 490934 490935 490936 490937 490938 490939 490940 490941 [...] (154171 flits)
Measured flits: 490932 490933 490934 490935 490936 490937 490938 490939 490940 490941 [...] (2357 flits)
Class 0:
Remaining flits: 509490 509491 509492 509493 509494 509495 509496 509497 509498 509499 [...] (165474 flits)
Measured flits: 509490 509491 509492 509493 509494 509495 509496 509497 509498 509499 [...] (432 flits)
Class 0:
Remaining flits: 566550 566551 566552 566553 566554 566555 566556 566557 566558 566559 [...] (174993 flits)
Measured flits: 566550 566551 566552 566553 566554 566555 566556 566557 566558 566559 [...] (139 flits)
Class 0:
Remaining flits: 581526 581527 581528 581529 581530 581531 581532 581533 581534 581535 [...] (180325 flits)
Measured flits: 581526 581527 581528 581529 581530 581531 581532 581533 581534 581535 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 729810 729811 729812 729813 729814 729815 729816 729817 729818 729819 [...] (142382 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 729810 729811 729812 729813 729814 729815 729816 729817 729818 729819 [...] (96467 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 762354 762355 762356 762357 762358 762359 762360 762361 762362 762363 [...] (55726 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 793602 793603 793604 793605 793606 793607 793608 793609 793610 793611 [...] (26434 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866646 866647 866648 866649 866650 866651 866652 866653 866654 866655 [...] (8389 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 866646 866647 866648 866649 866650 866651 866652 866653 866654 866655 [...] (1555 flits)
Measured flits: (0 flits)
Time taken is 24875 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1624.57 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8505 (1 samples)
Network latency average = 1522.82 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8426 (1 samples)
Flit latency average = 2210.08 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10079 (1 samples)
Fragmentation average = 62.0435 (1 samples)
	minimum = 0 (1 samples)
	maximum = 793 (1 samples)
Injected packet rate average = 0.0177403 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.032 (1 samples)
Accepted packet rate average = 0.0149048 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.319364 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.576714 (1 samples)
Accepted flit rate average = 0.268678 (1 samples)
	minimum = 0.217571 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 18.0021 (1 samples)
Accepted packet size average = 18.0263 (1 samples)
Hops average = 5.06552 (1 samples)
Total run time 21.7164
