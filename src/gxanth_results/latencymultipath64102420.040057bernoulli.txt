BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 306.083
	minimum = 22
	maximum = 860
Network latency average = 291.488
	minimum = 22
	maximum = 822
Slowest packet = 613
Flit latency average = 259.424
	minimum = 5
	maximum = 833
Slowest flit = 16953
Fragmentation average = 72.8259
	minimum = 0
	maximum = 637
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0143906
	minimum = 0.006 (at node 112)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.280885
	minimum = 0.113 (at node 174)
	maximum = 0.481 (at node 44)
Injected packet length average = 17.8489
Accepted packet length average = 19.5186
Total in-flight flits = 81358 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 569.471
	minimum = 22
	maximum = 1573
Network latency average = 551.679
	minimum = 22
	maximum = 1573
Slowest packet = 2914
Flit latency average = 508.038
	minimum = 5
	maximum = 1661
Slowest flit = 41448
Fragmentation average = 119.725
	minimum = 0
	maximum = 892
Injected packet rate average = 0.0397656
	minimum = 0.0285 (at node 88)
	maximum = 0.0485 (at node 100)
Accepted packet rate average = 0.0154089
	minimum = 0.0085 (at node 153)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.712951
	minimum = 0.5055 (at node 88)
	maximum = 0.8645 (at node 100)
Accepted flit rate average= 0.293086
	minimum = 0.1665 (at node 153)
	maximum = 0.4345 (at node 152)
Injected packet length average = 17.9288
Accepted packet length average = 19.0206
Total in-flight flits = 162315 (0 measured)
latency change    = 0.462513
throughput change = 0.0416278
Class 0:
Packet latency average = 1325.21
	minimum = 24
	maximum = 2440
Network latency average = 1304.44
	minimum = 22
	maximum = 2392
Slowest packet = 3292
Flit latency average = 1250.77
	minimum = 5
	maximum = 2505
Slowest flit = 61630
Fragmentation average = 213.491
	minimum = 0
	maximum = 892
Injected packet rate average = 0.0396615
	minimum = 0.026 (at node 91)
	maximum = 0.054 (at node 36)
Accepted packet rate average = 0.0164063
	minimum = 0.007 (at node 134)
	maximum = 0.026 (at node 56)
Injected flit rate average = 0.713333
	minimum = 0.465 (at node 91)
	maximum = 0.969 (at node 139)
Accepted flit rate average= 0.30187
	minimum = 0.129 (at node 17)
	maximum = 0.457 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.3997
Total in-flight flits = 241426 (0 measured)
latency change    = 0.570279
throughput change = 0.0290982
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 57.9984
	minimum = 23
	maximum = 252
Network latency average = 37.6181
	minimum = 22
	maximum = 234
Slowest packet = 23336
Flit latency average = 1730.13
	minimum = 5
	maximum = 3365
Slowest flit = 78496
Fragmentation average = 6.74168
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0406562
	minimum = 0.027 (at node 70)
	maximum = 0.056 (at node 66)
Accepted packet rate average = 0.0166927
	minimum = 0.006 (at node 64)
	maximum = 0.027 (at node 27)
Injected flit rate average = 0.732083
	minimum = 0.478 (at node 70)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.306495
	minimum = 0.152 (at node 64)
	maximum = 0.536 (at node 181)
Injected packet length average = 18.0067
Accepted packet length average = 18.361
Total in-flight flits = 323087 (129039 measured)
latency change    = 21.8491
throughput change = 0.01509
Class 0:
Packet latency average = 59.2924
	minimum = 23
	maximum = 469
Network latency average = 37.6214
	minimum = 22
	maximum = 415
Slowest packet = 28482
Flit latency average = 1994.89
	minimum = 5
	maximum = 4108
Slowest flit = 118590
Fragmentation average = 6.38911
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0401901
	minimum = 0.0295 (at node 92)
	maximum = 0.051 (at node 117)
Accepted packet rate average = 0.0165339
	minimum = 0.009 (at node 64)
	maximum = 0.0245 (at node 83)
Injected flit rate average = 0.723805
	minimum = 0.532 (at node 92)
	maximum = 0.923 (at node 117)
Accepted flit rate average= 0.303862
	minimum = 0.1765 (at node 64)
	maximum = 0.437 (at node 83)
Injected packet length average = 18.0095
Accepted packet length average = 18.3782
Total in-flight flits = 402537 (255519 measured)
latency change    = 0.0218245
throughput change = 0.0086645
Class 0:
Packet latency average = 60.4755
	minimum = 23
	maximum = 621
Network latency average = 38.771
	minimum = 22
	maximum = 584
Slowest packet = 39884
Flit latency average = 2246.44
	minimum = 5
	maximum = 5066
Slowest flit = 123362
Fragmentation average = 6.19084
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0401163
	minimum = 0.0286667 (at node 92)
	maximum = 0.0493333 (at node 117)
Accepted packet rate average = 0.0164444
	minimum = 0.01 (at node 64)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.722292
	minimum = 0.514667 (at node 92)
	maximum = 0.886667 (at node 117)
Accepted flit rate average= 0.302714
	minimum = 0.190333 (at node 64)
	maximum = 0.421 (at node 128)
Injected packet length average = 18.0049
Accepted packet length average = 18.4083
Total in-flight flits = 482989 (382741 measured)
latency change    = 0.019562
throughput change = 0.00379381
Class 0:
Packet latency average = 305.715
	minimum = 23
	maximum = 4000
Network latency average = 283.843
	minimum = 22
	maximum = 3937
Slowest packet = 23021
Flit latency average = 2500.22
	minimum = 5
	maximum = 5943
Slowest flit = 141183
Fragmentation average = 29.8068
	minimum = 0
	maximum = 623
Injected packet rate average = 0.0401615
	minimum = 0.032 (at node 92)
	maximum = 0.0495 (at node 104)
Accepted packet rate average = 0.0165104
	minimum = 0.011 (at node 64)
	maximum = 0.02375 (at node 128)
Injected flit rate average = 0.722917
	minimum = 0.57475 (at node 92)
	maximum = 0.891 (at node 104)
Accepted flit rate average= 0.302835
	minimum = 0.20875 (at node 171)
	maximum = 0.4285 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 18.342
Total in-flight flits = 564041 (505141 measured)
latency change    = 0.802183
throughput change = 0.000399868
Class 0:
Packet latency average = 1132.38
	minimum = 23
	maximum = 4980
Network latency average = 1110.97
	minimum = 22
	maximum = 4953
Slowest packet = 23156
Flit latency average = 2748.59
	minimum = 5
	maximum = 6726
Slowest flit = 167975
Fragmentation average = 109.287
	minimum = 0
	maximum = 773
Injected packet rate average = 0.0402271
	minimum = 0.0334 (at node 80)
	maximum = 0.0476 (at node 104)
Accepted packet rate average = 0.0166187
	minimum = 0.0124 (at node 64)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.724119
	minimum = 0.6012 (at node 80)
	maximum = 0.8568 (at node 104)
Accepted flit rate average= 0.303846
	minimum = 0.226 (at node 171)
	maximum = 0.4176 (at node 128)
Injected packet length average = 18.0008
Accepted packet length average = 18.2833
Total in-flight flits = 644858 (613449 measured)
latency change    = 0.730024
throughput change = 0.003328
Class 0:
Packet latency average = 1966.96
	minimum = 23
	maximum = 5983
Network latency average = 1945.81
	minimum = 22
	maximum = 5972
Slowest packet = 22997
Flit latency average = 3006.03
	minimum = 5
	maximum = 7692
Slowest flit = 176094
Fragmentation average = 177.906
	minimum = 0
	maximum = 841
Injected packet rate average = 0.0402257
	minimum = 0.0346667 (at node 92)
	maximum = 0.0475 (at node 12)
Accepted packet rate average = 0.0166832
	minimum = 0.0125 (at node 80)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.724063
	minimum = 0.622833 (at node 92)
	maximum = 0.855333 (at node 12)
Accepted flit rate average= 0.304371
	minimum = 0.223 (at node 80)
	maximum = 0.385667 (at node 128)
Injected packet length average = 18
Accepted packet length average = 18.2442
Total in-flight flits = 724911 (711113 measured)
latency change    = 0.424302
throughput change = 0.0017243
Class 0:
Packet latency average = 2715.96
	minimum = 23
	maximum = 6973
Network latency average = 2694.66
	minimum = 22
	maximum = 6951
Slowest packet = 22969
Flit latency average = 3272.35
	minimum = 5
	maximum = 8291
Slowest flit = 176111
Fragmentation average = 227.78
	minimum = 0
	maximum = 841
Injected packet rate average = 0.0401332
	minimum = 0.0347143 (at node 113)
	maximum = 0.0471429 (at node 12)
Accepted packet rate average = 0.016718
	minimum = 0.0128571 (at node 80)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.722361
	minimum = 0.624286 (at node 113)
	maximum = 0.849143 (at node 12)
Accepted flit rate average= 0.30449
	minimum = 0.231143 (at node 80)
	maximum = 0.375857 (at node 128)
Injected packet length average = 17.9991
Accepted packet length average = 18.2133
Total in-flight flits = 803094 (798472 measured)
latency change    = 0.275776
throughput change = 0.000390567
Draining all recorded packets ...
Class 0:
Remaining flits: 291174 291175 291176 291177 291178 291179 291180 291181 291182 291183 [...] (882768 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (757522 flits)
Class 0:
Remaining flits: 341452 341453 341454 341455 341456 341457 341458 341459 359385 359386 [...] (958412 flits)
Measured flits: 412236 412237 412238 412239 412240 412241 412242 412243 412244 412245 [...] (711201 flits)
Class 0:
Remaining flits: 379007 381456 381457 381458 381459 381460 381461 381462 381463 381464 [...] (1039151 flits)
Measured flits: 445338 445339 445340 445341 445342 445343 445344 445345 445346 445347 [...] (663694 flits)
Class 0:
Remaining flits: 454479 454480 454481 457015 457016 457017 457018 457019 462096 462097 [...] (1120901 flits)
Measured flits: 454479 454480 454481 457015 457016 457017 457018 457019 462096 462097 [...] (616513 flits)
Class 0:
Remaining flits: 503730 503731 503732 503733 503734 503735 503736 503737 503738 503739 [...] (1199712 flits)
Measured flits: 503730 503731 503732 503733 503734 503735 503736 503737 503738 503739 [...] (569108 flits)
Class 0:
Remaining flits: 545220 545221 545222 545223 545224 545225 545226 545227 545228 545229 [...] (1282610 flits)
Measured flits: 545220 545221 545222 545223 545224 545225 545226 545227 545228 545229 [...] (521809 flits)
Class 0:
Remaining flits: 570060 570061 570062 570063 570064 570065 570066 570067 570068 570069 [...] (1360776 flits)
Measured flits: 570060 570061 570062 570063 570064 570065 570066 570067 570068 570069 [...] (474431 flits)
Class 0:
Remaining flits: 595127 595128 595129 595130 595131 595132 595133 637630 637631 642923 [...] (1440012 flits)
Measured flits: 595127 595128 595129 595130 595131 595132 595133 637630 637631 642923 [...] (426912 flits)
Class 0:
Remaining flits: 688996 688997 688998 688999 689000 689001 689002 689003 700560 700561 [...] (1520670 flits)
Measured flits: 688996 688997 688998 688999 689000 689001 689002 689003 700560 700561 [...] (379659 flits)
Class 0:
Remaining flits: 701994 701995 701996 701997 701998 701999 715983 715984 715985 726891 [...] (1599995 flits)
Measured flits: 701994 701995 701996 701997 701998 701999 715983 715984 715985 726891 [...] (332045 flits)
Class 0:
Remaining flits: 728700 728701 728702 728703 728704 728705 728706 728707 728708 728709 [...] (1677166 flits)
Measured flits: 728700 728701 728702 728703 728704 728705 728706 728707 728708 728709 [...] (284793 flits)
Class 0:
Remaining flits: 784654 784655 792298 792299 792300 792301 792302 792303 792304 792305 [...] (1756118 flits)
Measured flits: 784654 784655 792298 792299 792300 792301 792302 792303 792304 792305 [...] (238372 flits)
Class 0:
Remaining flits: 825534 825535 825536 825537 825538 825539 825540 825541 825542 825543 [...] (1835106 flits)
Measured flits: 825534 825535 825536 825537 825538 825539 825540 825541 825542 825543 [...] (194241 flits)
Class 0:
Remaining flits: 871000 871001 877626 877627 877628 877629 877630 877631 877632 877633 [...] (1912087 flits)
Measured flits: 871000 871001 877626 877627 877628 877629 877630 877631 877632 877633 [...] (152582 flits)
Class 0:
Remaining flits: 877632 877633 877634 877635 877636 877637 877638 877639 877640 877641 [...] (1988857 flits)
Measured flits: 877632 877633 877634 877635 877636 877637 877638 877639 877640 877641 [...] (115646 flits)
Class 0:
Remaining flits: 944252 944253 944254 944255 944256 944257 944258 944259 944260 944261 [...] (2066509 flits)
Measured flits: 944252 944253 944254 944255 944256 944257 944258 944259 944260 944261 [...] (83875 flits)
Class 0:
Remaining flits: 968226 968227 968228 968229 968230 968231 968232 968233 968234 968235 [...] (2141361 flits)
Measured flits: 968226 968227 968228 968229 968230 968231 968232 968233 968234 968235 [...] (58310 flits)
Class 0:
Remaining flits: 1020467 1020468 1020469 1020470 1020471 1020472 1020473 1041637 1041638 1041639 [...] (2212568 flits)
Measured flits: 1020467 1020468 1020469 1020470 1020471 1020472 1020473 1041637 1041638 1041639 [...] (39342 flits)
Class 0:
Remaining flits: 1058418 1058419 1058420 1058421 1058422 1058423 1058424 1058425 1058426 1058427 [...] (2281888 flits)
Measured flits: 1058418 1058419 1058420 1058421 1058422 1058423 1058424 1058425 1058426 1058427 [...] (25010 flits)
Class 0:
Remaining flits: 1058418 1058419 1058420 1058421 1058422 1058423 1058424 1058425 1058426 1058427 [...] (2350189 flits)
Measured flits: 1058418 1058419 1058420 1058421 1058422 1058423 1058424 1058425 1058426 1058427 [...] (15107 flits)
Class 0:
Remaining flits: 1058427 1058428 1058429 1058430 1058431 1058432 1058433 1058434 1058435 1102446 [...] (2417192 flits)
Measured flits: 1058427 1058428 1058429 1058430 1058431 1058432 1058433 1058434 1058435 1102446 [...] (8394 flits)
Class 0:
Remaining flits: 1175526 1175527 1175528 1175529 1175530 1175531 1175532 1175533 1175534 1175535 [...] (2485339 flits)
Measured flits: 1175526 1175527 1175528 1175529 1175530 1175531 1175532 1175533 1175534 1175535 [...] (4318 flits)
Class 0:
Remaining flits: 1182809 1182810 1182811 1182812 1182813 1182814 1182815 1202958 1202959 1202960 [...] (2552454 flits)
Measured flits: 1182809 1182810 1182811 1182812 1182813 1182814 1182815 1202958 1202959 1202960 [...] (1959 flits)
Class 0:
Remaining flits: 1218410 1218411 1218412 1218413 1218414 1218415 1218416 1218417 1218418 1218419 [...] (2620278 flits)
Measured flits: 1218410 1218411 1218412 1218413 1218414 1218415 1218416 1218417 1218418 1218419 [...] (961 flits)
Class 0:
Remaining flits: 1259766 1259767 1259768 1259769 1259770 1259771 1259772 1259773 1259774 1259775 [...] (2688752 flits)
Measured flits: 1259766 1259767 1259768 1259769 1259770 1259771 1259772 1259773 1259774 1259775 [...] (457 flits)
Class 0:
Remaining flits: 1271975 1271976 1271977 1271978 1271979 1271980 1271981 1271982 1271983 1271984 [...] (2755892 flits)
Measured flits: 1271975 1271976 1271977 1271978 1271979 1271980 1271981 1271982 1271983 1271984 [...] (250 flits)
Class 0:
Remaining flits: 1308878 1308879 1308880 1308881 1308882 1308883 1308884 1308885 1308886 1308887 [...] (2823817 flits)
Measured flits: 1308878 1308879 1308880 1308881 1308882 1308883 1308884 1308885 1308886 1308887 [...] (116 flits)
Class 0:
Remaining flits: 1321434 1321435 1321436 1321437 1321438 1321439 1321440 1321441 1321442 1321443 [...] (2893902 flits)
Measured flits: 1321434 1321435 1321436 1321437 1321438 1321439 1321440 1321441 1321442 1321443 [...] (54 flits)
Class 0:
Remaining flits: 1321434 1321435 1321436 1321437 1321438 1321439 1321440 1321441 1321442 1321443 [...] (2964310 flits)
Measured flits: 1321434 1321435 1321436 1321437 1321438 1321439 1321440 1321441 1321442 1321443 [...] (54 flits)
Class 0:
Remaining flits: 1321448 1321449 1321450 1321451 1340820 1340821 1340822 1340823 1340824 1340825 [...] (3033208 flits)
Measured flits: 1321448 1321449 1321450 1321451 1340820 1340821 1340822 1340823 1340824 1340825 [...] (24 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1385784 1385785 1385786 1385787 1385788 1385789 1385790 1385791 1385792 1385793 [...] (3039985 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1468416 1468417 1468418 1468419 1468420 1468421 1505844 1505845 1505846 1505847 [...] (2992425 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505844 1505845 1505846 1505847 1505848 1505849 1505850 1505851 1505852 1505853 [...] (2944930 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505844 1505845 1505846 1505847 1505848 1505849 1505850 1505851 1505852 1505853 [...] (2897410 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1616670 1616671 1616672 1616673 1616674 1616675 1616676 1616677 1616678 1616679 [...] (2850135 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1681884 1681885 1681886 1681887 1681888 1681889 1681890 1681891 1681892 1681893 [...] (2802701 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1681895 1681896 1681897 1681898 1681899 1681900 1681901 1735537 1735538 1735539 [...] (2755018 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1814801 1814802 1814803 1814804 1814805 1814806 1814807 1814808 1814809 1814810 [...] (2707560 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1835638 1835639 1875015 1875016 1875017 1875018 1875019 1875020 1875021 1875022 [...] (2659904 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1889730 1889731 1889732 1889733 1889734 1889735 1889736 1889737 1889738 1889739 [...] (2612161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1889730 1889731 1889732 1889733 1889734 1889735 1889736 1889737 1889738 1889739 [...] (2564332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1952946 1952947 1952948 1952949 1952950 1952951 1952952 1952953 1952954 1952955 [...] (2516619 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998216 1998217 1998218 1998219 1998220 1998221 1998222 1998223 1998224 1998225 [...] (2468803 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998223 1998224 1998225 1998226 1998227 1998228 1998229 1998230 1998231 1998232 [...] (2421202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2049390 2049391 2049392 2049393 2049394 2049395 2049396 2049397 2049398 2049399 [...] (2373307 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2061566 2061567 2061568 2061569 2061570 2061571 2061572 2061573 2061574 2061575 [...] (2325353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2136887 2137536 2137537 2137538 2137539 2137540 2137541 2137542 2137543 2137544 [...] (2277595 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2197044 2197045 2197046 2197047 2197048 2197049 2197050 2197051 2197052 2197053 [...] (2229910 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2197052 2197053 2197054 2197055 2197056 2197057 2197058 2197059 2197060 2197061 [...] (2182249 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2265146 2265147 2265148 2265149 2265150 2265151 2265152 2265153 2265154 2265155 [...] (2134362 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2330622 2330623 2330624 2330625 2330626 2330627 2330628 2330629 2330630 2330631 [...] (2086989 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2330622 2330623 2330624 2330625 2330626 2330627 2330628 2330629 2330630 2330631 [...] (2039425 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2401902 2401903 2401904 2401905 2401906 2401907 2401908 2401909 2401910 2401911 [...] (1991832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2449080 2449081 2449082 2449083 2449084 2449085 2449086 2449087 2449088 2449089 [...] (1944038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2449093 2449094 2449095 2449096 2449097 2488788 2488789 2488790 2488791 2488792 [...] (1896439 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2528856 2528857 2528858 2528859 2528860 2528861 2528862 2528863 2528864 2528865 [...] (1848551 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2528863 2528864 2528865 2528866 2528867 2528868 2528869 2528870 2528871 2528872 [...] (1800634 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2592180 2592181 2592182 2592183 2592184 2592185 2592186 2592187 2592188 2592189 [...] (1752705 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2612268 2612269 2612270 2612271 2612272 2612273 2612274 2612275 2612276 2612277 [...] (1704602 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2662433 2678202 2678203 2678204 2678205 2678206 2678207 2678208 2678209 2678210 [...] (1657068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2721001 2721002 2721003 2721004 2721005 2762238 2762239 2762240 2762241 2762242 [...] (1609367 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2830572 2830573 2830574 2830575 2830576 2830577 2830578 2830579 2830580 2830581 [...] (1561999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2902734 2902735 2902736 2902737 2902738 2902739 2902740 2902741 2902742 2902743 [...] (1514388 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2902734 2902735 2902736 2902737 2902738 2902739 2902740 2902741 2902742 2902743 [...] (1466919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2925338 2925339 2925340 2925341 2959848 2959849 2959850 2959851 2959852 2959853 [...] (1419066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3088080 3088081 3088082 3088083 3088084 3088085 3088086 3088087 3088088 3088089 [...] (1371021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3088096 3088097 3101290 3101291 3124673 3137508 3137509 3137510 3137511 3137512 [...] (1323047 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3149297 3179916 3179917 3179918 3179919 3179920 3179921 3179922 3179923 3179924 [...] (1275132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3185190 3185191 3185192 3185193 3185194 3185195 3185196 3185197 3185198 3185199 [...] (1227199 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3245866 3245867 3252564 3252565 3252566 3252567 3252568 3252569 3252570 3252571 [...] (1179153 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3322782 3322783 3322784 3322785 3322786 3322787 3322788 3322789 3322790 3322791 [...] (1131295 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3322797 3322798 3322799 3353565 3353566 3353567 3353568 3353569 3353570 3353571 [...] (1083222 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3353578 3353579 3402064 3402065 3402066 3402067 3402068 3402069 3402070 3402071 [...] (1035253 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3468114 3468115 3468116 3468117 3468118 3468119 3468120 3468121 3468122 3468123 [...] (987382 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3468114 3468115 3468116 3468117 3468118 3468119 3468120 3468121 3468122 3468123 [...] (939615 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3512366 3512367 3512368 3512369 3512370 3512371 3512372 3512373 3512374 3512375 [...] (891667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3512374 3512375 3560624 3560625 3560626 3560627 3560628 3560629 3560630 3560631 [...] (843646 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3601656 3601657 3601658 3601659 3601660 3601661 3601662 3601663 3601664 3601665 [...] (795824 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3626037 3626038 3626039 3626040 3626041 3626042 3626043 3626044 3626045 3644928 [...] (747964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3677580 3677581 3677582 3677583 3677584 3677585 3677586 3677587 3677588 3677589 [...] (700299 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3697308 3697309 3697310 3697311 3697312 3697313 3697314 3697315 3697316 3697317 [...] (652736 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3709330 3709331 3743262 3743263 3743264 3743265 3743266 3743267 3743268 3743269 [...] (604870 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3743276 3743277 3743278 3743279 3779064 3779065 3779066 3779067 3779068 3779069 [...] (557209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3743278 3743279 3779066 3779067 3779068 3779069 3779070 3779071 3779072 3779073 [...] (509481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3804786 3804787 3804788 3804789 3804790 3804791 3804792 3804793 3804794 3804795 [...] (461688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3804786 3804787 3804788 3804789 3804790 3804791 3804792 3804793 3804794 3804795 [...] (414115 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3804802 3804803 3933756 3933757 3933758 3933759 3933760 3933761 3933762 3933763 [...] (366472 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3978286 3978287 4057324 4057325 4075686 4075687 4075688 4075689 4075690 4075691 [...] (318712 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4177834 4177835 4215784 4215785 4215786 4215787 4215788 4215789 4215790 4215791 [...] (270664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4215796 4215797 4222458 4222459 4222460 4222461 4222462 4222463 4222464 4222465 [...] (222828 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4274890 4274891 4289850 4289851 4289852 4289853 4289854 4289855 4289856 4289857 [...] (175267 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4274890 4274891 4433382 4433383 4433384 4433385 4433386 4433387 4433388 4433389 [...] (127524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4453884 4453885 4453886 4453887 4453888 4453889 4453890 4453891 4453892 4453893 [...] (81022 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4453900 4453901 4475932 4475933 4520196 4520197 4520198 4520199 4520200 4520201 [...] (41220 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4683672 4683673 4683674 4683675 4683676 4683677 4683678 4683679 4683680 4683681 [...] (14018 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4921452 4921453 4921454 4921455 4921456 4921457 4921458 4921459 4921460 4921461 [...] (1850 flits)
Measured flits: (0 flits)
Time taken is 107852 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10598.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 31098 (1 samples)
Network latency average = 10575.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 31079 (1 samples)
Flit latency average = 31124.4 (1 samples)
	minimum = 5 (1 samples)
	maximum = 72034 (1 samples)
Fragmentation average = 430.884 (1 samples)
	minimum = 0 (1 samples)
	maximum = 978 (1 samples)
Injected packet rate average = 0.0401332 (1 samples)
	minimum = 0.0347143 (1 samples)
	maximum = 0.0471429 (1 samples)
Accepted packet rate average = 0.016718 (1 samples)
	minimum = 0.0128571 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.722361 (1 samples)
	minimum = 0.624286 (1 samples)
	maximum = 0.849143 (1 samples)
Accepted flit rate average = 0.30449 (1 samples)
	minimum = 0.231143 (1 samples)
	maximum = 0.375857 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.2133 (1 samples)
Hops average = 5.07047 (1 samples)
Total run time 164.883
