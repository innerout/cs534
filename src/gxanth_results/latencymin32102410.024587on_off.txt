BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 340.997
	minimum = 27
	maximum = 974
Network latency average = 263.304
	minimum = 23
	maximum = 941
Slowest packet = 85
Flit latency average = 196.258
	minimum = 6
	maximum = 951
Slowest flit = 3313
Fragmentation average = 139.837
	minimum = 0
	maximum = 874
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00966146
	minimum = 0.002 (at node 41)
	maximum = 0.019 (at node 167)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.199422
	minimum = 0.075 (at node 41)
	maximum = 0.354 (at node 44)
Injected packet length average = 17.8416
Accepted packet length average = 20.641
Total in-flight flits = 41379 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 561.993
	minimum = 27
	maximum = 1943
Network latency average = 458.763
	minimum = 23
	maximum = 1809
Slowest packet = 99
Flit latency average = 383.339
	minimum = 6
	maximum = 1867
Slowest flit = 9106
Fragmentation average = 177.175
	minimum = 0
	maximum = 1534
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0109271
	minimum = 0.005 (at node 41)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.210664
	minimum = 0.117 (at node 174)
	maximum = 0.3585 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 19.2791
Total in-flight flits = 81429 (0 measured)
latency change    = 0.393237
throughput change = 0.0533655
Class 0:
Packet latency average = 1160.61
	minimum = 29
	maximum = 2943
Network latency average = 1023.49
	minimum = 23
	maximum = 2810
Slowest packet = 505
Flit latency average = 943.786
	minimum = 6
	maximum = 2793
Slowest flit = 10979
Fragmentation average = 219.67
	minimum = 0
	maximum = 2009
Injected packet rate average = 0.0248021
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0127135
	minimum = 0.004 (at node 172)
	maximum = 0.025 (at node 56)
Injected flit rate average = 0.4465
	minimum = 0 (at node 0)
	maximum = 1 (at node 60)
Accepted flit rate average= 0.227547
	minimum = 0.101 (at node 52)
	maximum = 0.443 (at node 56)
Injected packet length average = 18.0025
Accepted packet length average = 17.898
Total in-flight flits = 123456 (0 measured)
latency change    = 0.51578
throughput change = 0.0741949
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 295.201
	minimum = 28
	maximum = 1163
Network latency average = 141.327
	minimum = 23
	maximum = 867
Slowest packet = 13795
Flit latency average = 1340.42
	minimum = 6
	maximum = 3620
Slowest flit = 19331
Fragmentation average = 46.158
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0253698
	minimum = 0 (at node 40)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0125885
	minimum = 0.006 (at node 176)
	maximum = 0.022 (at node 107)
Injected flit rate average = 0.456693
	minimum = 0 (at node 40)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.227807
	minimum = 0.117 (at node 85)
	maximum = 0.375 (at node 152)
Injected packet length average = 18.0014
Accepted packet length average = 18.0964
Total in-flight flits = 167395 (78427 measured)
latency change    = 2.9316
throughput change = 0.00114314
Class 0:
Packet latency average = 592.221
	minimum = 27
	maximum = 2237
Network latency average = 458.39
	minimum = 23
	maximum = 1970
Slowest packet = 13795
Flit latency average = 1554.46
	minimum = 6
	maximum = 4558
Slowest flit = 18666
Fragmentation average = 92.1088
	minimum = 0
	maximum = 1054
Injected packet rate average = 0.0246979
	minimum = 0.002 (at node 86)
	maximum = 0.0545 (at node 117)
Accepted packet rate average = 0.0125339
	minimum = 0.007 (at node 134)
	maximum = 0.0205 (at node 95)
Injected flit rate average = 0.444807
	minimum = 0.0405 (at node 86)
	maximum = 0.9805 (at node 117)
Accepted flit rate average= 0.22681
	minimum = 0.1215 (at node 134)
	maximum = 0.355 (at node 95)
Injected packet length average = 18.0099
Accepted packet length average = 18.0958
Total in-flight flits = 207073 (145448 measured)
latency change    = 0.501535
throughput change = 0.0043975
Class 0:
Packet latency average = 959.886
	minimum = 23
	maximum = 3620
Network latency average = 831.425
	minimum = 23
	maximum = 2964
Slowest packet = 13795
Flit latency average = 1743.48
	minimum = 6
	maximum = 5370
Slowest flit = 18683
Fragmentation average = 122.82
	minimum = 0
	maximum = 1415
Injected packet rate average = 0.0242535
	minimum = 0.00566667 (at node 171)
	maximum = 0.052 (at node 88)
Accepted packet rate average = 0.0125035
	minimum = 0.007 (at node 134)
	maximum = 0.0193333 (at node 95)
Injected flit rate average = 0.436701
	minimum = 0.102 (at node 171)
	maximum = 0.935 (at node 88)
Accepted flit rate average= 0.224781
	minimum = 0.129667 (at node 134)
	maximum = 0.340333 (at node 95)
Injected packet length average = 18.0057
Accepted packet length average = 17.9775
Total in-flight flits = 245442 (202604 measured)
latency change    = 0.38303
throughput change = 0.00902498
Class 0:
Packet latency average = 1348.47
	minimum = 23
	maximum = 4720
Network latency average = 1217.64
	minimum = 23
	maximum = 3977
Slowest packet = 13795
Flit latency average = 1945.24
	minimum = 6
	maximum = 6719
Slowest flit = 20017
Fragmentation average = 141.261
	minimum = 0
	maximum = 1651
Injected packet rate average = 0.0240182
	minimum = 0.009 (at node 46)
	maximum = 0.04525 (at node 152)
Accepted packet rate average = 0.0124922
	minimum = 0.00775 (at node 134)
	maximum = 0.01825 (at node 95)
Injected flit rate average = 0.432365
	minimum = 0.162 (at node 46)
	maximum = 0.81175 (at node 152)
Accepted flit rate average= 0.224003
	minimum = 0.13925 (at node 134)
	maximum = 0.3235 (at node 95)
Injected packet length average = 18.0015
Accepted packet length average = 17.9314
Total in-flight flits = 283450 (254073 measured)
latency change    = 0.288168
throughput change = 0.00347606
Class 0:
Packet latency average = 1716.22
	minimum = 23
	maximum = 5676
Network latency average = 1577.4
	minimum = 23
	maximum = 4974
Slowest packet = 13795
Flit latency average = 2153.59
	minimum = 6
	maximum = 7469
Slowest flit = 36953
Fragmentation average = 150.764
	minimum = 0
	maximum = 1741
Injected packet rate average = 0.0241313
	minimum = 0.0078 (at node 99)
	maximum = 0.044 (at node 13)
Accepted packet rate average = 0.0124406
	minimum = 0.0084 (at node 31)
	maximum = 0.0166 (at node 19)
Injected flit rate average = 0.434378
	minimum = 0.1404 (at node 99)
	maximum = 0.7902 (at node 13)
Accepted flit rate average= 0.222967
	minimum = 0.1534 (at node 31)
	maximum = 0.299 (at node 166)
Injected packet length average = 18.0006
Accepted packet length average = 17.9225
Total in-flight flits = 326396 (306491 measured)
latency change    = 0.214277
throughput change = 0.00464615
Class 0:
Packet latency average = 2062.35
	minimum = 23
	maximum = 6522
Network latency average = 1917.21
	minimum = 23
	maximum = 5980
Slowest packet = 13795
Flit latency average = 2359.09
	minimum = 6
	maximum = 8124
Slowest flit = 52433
Fragmentation average = 157.59
	minimum = 0
	maximum = 2186
Injected packet rate average = 0.0242769
	minimum = 0.00883333 (at node 99)
	maximum = 0.0458333 (at node 47)
Accepted packet rate average = 0.0123715
	minimum = 0.00816667 (at node 31)
	maximum = 0.0165 (at node 166)
Injected flit rate average = 0.43702
	minimum = 0.159 (at node 99)
	maximum = 0.824167 (at node 47)
Accepted flit rate average= 0.222359
	minimum = 0.149833 (at node 31)
	maximum = 0.294667 (at node 166)
Injected packet length average = 18.0015
Accepted packet length average = 17.9734
Total in-flight flits = 370705 (357142 measured)
latency change    = 0.167832
throughput change = 0.00273504
Class 0:
Packet latency average = 2401.58
	minimum = 23
	maximum = 7613
Network latency average = 2251.59
	minimum = 23
	maximum = 6916
Slowest packet = 13795
Flit latency average = 2560.05
	minimum = 6
	maximum = 9160
Slowest flit = 58976
Fragmentation average = 162.196
	minimum = 0
	maximum = 2693
Injected packet rate average = 0.0246682
	minimum = 0.0102857 (at node 37)
	maximum = 0.0462857 (at node 47)
Accepted packet rate average = 0.0123519
	minimum = 0.00871429 (at node 189)
	maximum = 0.016 (at node 123)
Injected flit rate average = 0.444052
	minimum = 0.185571 (at node 37)
	maximum = 0.833143 (at node 47)
Accepted flit rate average= 0.222089
	minimum = 0.156714 (at node 189)
	maximum = 0.291143 (at node 123)
Injected packet length average = 18.001
Accepted packet length average = 17.9801
Total in-flight flits = 421740 (411928 measured)
latency change    = 0.141253
throughput change = 0.00121222
Draining all recorded packets ...
Class 0:
Remaining flits: 67320 67321 67322 67323 67324 67325 67326 67327 67328 67329 [...] (465327 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (396423 flits)
Class 0:
Remaining flits: 97524 97525 97526 97527 97528 97529 97530 97531 97532 97533 [...] (505667 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (365680 flits)
Class 0:
Remaining flits: 111204 111205 111206 111207 111208 111209 111210 111211 111212 111213 [...] (545876 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (333962 flits)
Class 0:
Remaining flits: 111204 111205 111206 111207 111208 111209 111210 111211 111212 111213 [...] (587411 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (302197 flits)
Class 0:
Remaining flits: 111204 111205 111206 111207 111208 111209 111210 111211 111212 111213 [...] (637143 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (271568 flits)
Class 0:
Remaining flits: 128250 128251 128252 128253 128254 128255 128256 128257 128258 128259 [...] (682114 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (242233 flits)
Class 0:
Remaining flits: 128266 128267 131094 131095 131096 131097 131098 131099 131100 131101 [...] (721394 flits)
Measured flits: 248274 248275 248276 248277 248278 248279 248280 248281 248282 248283 [...] (214444 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (757760 flits)
Measured flits: 248278 248279 248280 248281 248282 248283 248284 248285 248286 248287 [...] (188321 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (797546 flits)
Measured flits: 250164 250165 250166 250167 250168 250169 250170 250171 250172 250173 [...] (164412 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (840082 flits)
Measured flits: 251766 251767 251768 251769 251770 251771 251772 251773 251774 251775 [...] (142710 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (878884 flits)
Measured flits: 251766 251767 251768 251769 251770 251771 251772 251773 251774 251775 [...] (122640 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (915099 flits)
Measured flits: 268632 268633 268634 268635 268636 268637 268638 268639 268640 268641 [...] (105645 flits)
Class 0:
Remaining flits: 131094 131095 131096 131097 131098 131099 131100 131101 131102 131103 [...] (958932 flits)
Measured flits: 268632 268633 268634 268635 268636 268637 268638 268639 268640 268641 [...] (90030 flits)
Class 0:
Remaining flits: 157897 157898 157899 157900 157901 157902 157903 157904 157905 157906 [...] (997863 flits)
Measured flits: 268632 268633 268634 268635 268636 268637 268638 268639 268640 268641 [...] (76749 flits)
Class 0:
Remaining flits: 157913 219852 219853 219854 219855 219856 219857 219858 219859 219860 [...] (1040636 flits)
Measured flits: 290556 290557 290558 290559 290560 290561 290562 290563 290564 290565 [...] (65177 flits)
Class 0:
Remaining flits: 219852 219853 219854 219855 219856 219857 219858 219859 219860 219861 [...] (1084662 flits)
Measured flits: 290556 290557 290558 290559 290560 290561 290562 290563 290564 290565 [...] (55826 flits)
Class 0:
Remaining flits: 290556 290557 290558 290559 290560 290561 290562 290563 290564 290565 [...] (1124531 flits)
Measured flits: 290556 290557 290558 290559 290560 290561 290562 290563 290564 290565 [...] (47429 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1163209 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (40017 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1207199 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (33516 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1245853 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (27609 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1285692 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (22824 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1325199 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (18542 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (1365661 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (15525 flits)
Class 0:
Remaining flits: 300243 300244 300245 300246 300247 300248 300249 300250 300251 300252 [...] (1408953 flits)
Measured flits: 300243 300244 300245 300246 300247 300248 300249 300250 300251 300252 [...] (12715 flits)
Class 0:
Remaining flits: 336240 336241 336242 336243 336244 336245 336246 336247 336248 336249 [...] (1449079 flits)
Measured flits: 336240 336241 336242 336243 336244 336245 336246 336247 336248 336249 [...] (10054 flits)
Class 0:
Remaining flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (1487808 flits)
Measured flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (8181 flits)
Class 0:
Remaining flits: 369972 369973 369974 369975 369976 369977 369978 369979 369980 369981 [...] (1531618 flits)
Measured flits: 369972 369973 369974 369975 369976 369977 369978 369979 369980 369981 [...] (6832 flits)
Class 0:
Remaining flits: 381470 381471 381472 381473 546534 546535 546536 546537 546538 546539 [...] (1572539 flits)
Measured flits: 381470 381471 381472 381473 546534 546535 546536 546537 546538 546539 [...] (5574 flits)
Class 0:
Remaining flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1611192 flits)
Measured flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (4357 flits)
Class 0:
Remaining flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1650624 flits)
Measured flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (3478 flits)
Class 0:
Remaining flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1688318 flits)
Measured flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (2819 flits)
Class 0:
Remaining flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1730301 flits)
Measured flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (2276 flits)
Class 0:
Remaining flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1768668 flits)
Measured flits: 547992 547993 547994 547995 547996 547997 547998 547999 548000 548001 [...] (1730 flits)
Class 0:
Remaining flits: 569358 569359 569360 569361 569362 569363 569364 569365 569366 569367 [...] (1806975 flits)
Measured flits: 569358 569359 569360 569361 569362 569363 569364 569365 569366 569367 [...] (1472 flits)
Class 0:
Remaining flits: 569358 569359 569360 569361 569362 569363 569364 569365 569366 569367 [...] (1846717 flits)
Measured flits: 569358 569359 569360 569361 569362 569363 569364 569365 569366 569367 [...] (1105 flits)
Class 0:
Remaining flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (1885624 flits)
Measured flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (882 flits)
Class 0:
Remaining flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (1921405 flits)
Measured flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (706 flits)
Class 0:
Remaining flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (1959129 flits)
Measured flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (630 flits)
Class 0:
Remaining flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (1988189 flits)
Measured flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (545 flits)
Class 0:
Remaining flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (2023339 flits)
Measured flits: 649224 649225 649226 649227 649228 649229 649230 649231 649232 649233 [...] (460 flits)
Class 0:
Remaining flits: 649228 649229 649230 649231 649232 649233 649234 649235 649236 649237 [...] (2054601 flits)
Measured flits: 649228 649229 649230 649231 649232 649233 649234 649235 649236 649237 [...] (410 flits)
Class 0:
Remaining flits: 655308 655309 655310 655311 655312 655313 655314 655315 655316 655317 [...] (2085549 flits)
Measured flits: 655308 655309 655310 655311 655312 655313 655314 655315 655316 655317 [...] (288 flits)
Class 0:
Remaining flits: 702198 702199 702200 702201 702202 702203 702204 702205 702206 702207 [...] (2113232 flits)
Measured flits: 702198 702199 702200 702201 702202 702203 702204 702205 702206 702207 [...] (162 flits)
Class 0:
Remaining flits: 772398 772399 772400 772401 772402 772403 772404 772405 772406 772407 [...] (2143280 flits)
Measured flits: 772398 772399 772400 772401 772402 772403 772404 772405 772406 772407 [...] (108 flits)
Class 0:
Remaining flits: 772398 772399 772400 772401 772402 772403 772404 772405 772406 772407 [...] (2170971 flits)
Measured flits: 772398 772399 772400 772401 772402 772403 772404 772405 772406 772407 [...] (95 flits)
Class 0:
Remaining flits: 806832 806833 806834 806835 806836 806837 806838 806839 806840 806841 [...] (2197127 flits)
Measured flits: 806832 806833 806834 806835 806836 806837 806838 806839 806840 806841 [...] (72 flits)
Class 0:
Remaining flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (2220640 flits)
Measured flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (36 flits)
Class 0:
Remaining flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (2247780 flits)
Measured flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (36 flits)
Class 0:
Remaining flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (2272364 flits)
Measured flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (36 flits)
Class 0:
Remaining flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2296544 flits)
Measured flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (18 flits)
Class 0:
Remaining flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2315432 flits)
Measured flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (18 flits)
Class 0:
Remaining flits: 902496 902497 902498 902499 902500 902501 923994 923995 923996 923997 [...] (2332054 flits)
Measured flits: 902496 902497 902498 902499 902500 902501 (6 flits)
Class 0:
Remaining flits: 902496 902497 902498 902499 902500 902501 923994 923995 923996 923997 [...] (2346861 flits)
Measured flits: 902496 902497 902498 902499 902500 902501 (6 flits)
Class 0:
Remaining flits: 902496 902497 902498 902499 902500 902501 923994 923995 923996 923997 [...] (2362611 flits)
Measured flits: 902496 902497 902498 902499 902500 902501 (6 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 923994 923995 923996 923997 923998 923999 924000 924001 924002 924003 [...] (2330098 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923994 923995 923996 923997 923998 923999 924000 924001 924002 924003 [...] (2294766 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923994 923995 923996 923997 923998 923999 924000 924001 924002 924003 [...] (2259846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923994 923995 923996 923997 923998 923999 924000 924001 924002 924003 [...] (2224998 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923994 923995 923996 923997 923998 923999 924000 924001 924002 924003 [...] (2189959 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032498 1032499 1032500 1032501 1032502 1032503 1032504 1032505 1032506 1032507 [...] (2155476 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032498 1032499 1032500 1032501 1032502 1032503 1032504 1032505 1032506 1032507 [...] (2120538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1036125 1036126 1036127 1036128 1036129 1036130 1036131 1036132 1036133 1037628 [...] (2085571 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1154556 1154557 1154558 1154559 1154560 1154561 1154562 1154563 1154564 1154565 [...] (2050931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1263945 1263946 1263947 1263948 1263949 1263950 1263951 1263952 1263953 1263954 [...] (2016833 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310346 1310347 1310348 1310349 1310350 1310351 1310352 1310353 1310354 1310355 [...] (1981014 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310346 1310347 1310348 1310349 1310350 1310351 1310352 1310353 1310354 1310355 [...] (1945206 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310346 1310347 1310348 1310349 1310350 1310351 1310352 1310353 1310354 1310355 [...] (1910662 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310346 1310347 1310348 1310349 1310350 1310351 1310352 1310353 1310354 1310355 [...] (1875633 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310346 1310347 1310348 1310349 1310350 1310351 1310352 1310353 1310354 1310355 [...] (1840606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310358 1310359 1310360 1310361 1310362 1310363 1449774 1449775 1449776 1449777 [...] (1805749 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1456398 1456399 1456400 1456401 1456402 1456403 1456404 1456405 1456406 1456407 [...] (1771015 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1456398 1456399 1456400 1456401 1456402 1456403 1456404 1456405 1456406 1456407 [...] (1737087 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487088 1487089 1487090 1487091 1487092 1487093 1487094 1487095 1487096 1487097 [...] (1702766 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487088 1487089 1487090 1487091 1487092 1487093 1487094 1487095 1487096 1487097 [...] (1668409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1487088 1487089 1487090 1487091 1487092 1487093 1487094 1487095 1487096 1487097 [...] (1633577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1541736 1541737 1541738 1541739 1541740 1541741 1541742 1541743 1541744 1541745 [...] (1598644 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1543132 1543133 1543134 1543135 1543136 1543137 1543138 1543139 1586988 1586989 [...] (1563597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1610208 1610209 1610210 1610211 1610212 1610213 1610214 1610215 1610216 1610217 [...] (1528847 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1610208 1610209 1610210 1610211 1610212 1610213 1610214 1610215 1610216 1610217 [...] (1493793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1610208 1610209 1610210 1610211 1610212 1610213 1610214 1610215 1610216 1610217 [...] (1458727 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1423940 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1389416 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1355716 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1321474 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1287103 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1252821 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1218416 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1184804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1150379 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1116703 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1666782 1666783 1666784 1666785 1666786 1666787 1666788 1666789 1666790 1666791 [...] (1082707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2001942 2001943 2001944 2001945 2001946 2001947 2001948 2001949 2001950 2001951 [...] (1049327 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2001942 2001943 2001944 2001945 2001946 2001947 2001948 2001949 2001950 2001951 [...] (1015666 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2064006 2064007 2064008 2064009 2064010 2064011 2064012 2064013 2064014 2064015 [...] (981927 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2064006 2064007 2064008 2064009 2064010 2064011 2064012 2064013 2064014 2064015 [...] (948396 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2064006 2064007 2064008 2064009 2064010 2064011 2064012 2064013 2064014 2064015 [...] (915177 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2161494 2161495 2161496 2161497 2161498 2161499 2161500 2161501 2161502 2161503 [...] (881658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2212254 2212255 2212256 2212257 2212258 2212259 2212260 2212261 2212262 2212263 [...] (848058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2226078 2226079 2226080 2226081 2226082 2226083 2226084 2226085 2226086 2226087 [...] (814381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2226078 2226079 2226080 2226081 2226082 2226083 2226084 2226085 2226086 2226087 [...] (780802 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2226078 2226079 2226080 2226081 2226082 2226083 2226084 2226085 2226086 2226087 [...] (747267 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2226078 2226079 2226080 2226081 2226082 2226083 2226084 2226085 2226086 2226087 [...] (713686 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2226082 2226083 2226084 2226085 2226086 2226087 2226088 2226089 2226090 2226091 [...] (679798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2398446 2398447 2398448 2398449 2398450 2398451 2398452 2398453 2398454 2398455 [...] (646491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2398460 2398461 2398462 2398463 2413656 2413657 2413658 2413659 2413660 2413661 [...] (612875 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2413656 2413657 2413658 2413659 2413660 2413661 2413662 2413663 2413664 2413665 [...] (578972 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2413656 2413657 2413658 2413659 2413660 2413661 2413662 2413663 2413664 2413665 [...] (545613 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2413656 2413657 2413658 2413659 2413660 2413661 2413662 2413663 2413664 2413665 [...] (511572 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2413669 2413670 2413671 2413672 2413673 2525976 2525977 2525978 2525979 2525980 [...] (477634 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2601756 2601757 2601758 2601759 2601760 2601761 2601762 2601763 2601764 2601765 [...] (443075 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2601756 2601757 2601758 2601759 2601760 2601761 2601762 2601763 2601764 2601765 [...] (408850 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2606706 2606707 2606708 2606709 2606710 2606711 2606712 2606713 2606714 2606715 [...] (374173 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2606709 2606710 2606711 2606712 2606713 2606714 2606715 2606716 2606717 2606718 [...] (339439 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (304945 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (271273 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (237617 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (204072 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (170735 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660004 2660005 2660006 2660007 2660008 2660009 2660010 2660011 2660012 2660013 [...] (139632 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2660007 2660008 2660009 2660010 2660011 2660012 2660013 2660014 2660015 2660016 [...] (110227 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2677158 2677159 2677160 2677161 2677162 2677163 2677164 2677165 2677166 2677167 [...] (84120 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2677158 2677159 2677160 2677161 2677162 2677163 2677164 2677165 2677166 2677167 [...] (60070 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2677158 2677159 2677160 2677161 2677162 2677163 2677164 2677165 2677166 2677167 [...] (40171 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2898054 2898055 2898056 2898057 2898058 2898059 2898060 2898061 2898062 2898063 [...] (25790 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3093174 3093175 3093176 3093177 3093178 3093179 3093180 3093181 3093182 3093183 [...] (14521 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3093174 3093175 3093176 3093177 3093178 3093179 3093180 3093181 3093182 3093183 [...] (6290 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3093188 3093189 3093190 3093191 3261168 3261169 3261170 3261171 3261172 3261173 [...] (1677 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3421439 3924234 3924235 3924236 3924237 3924238 3924239 3924240 3924241 3924242 [...] (193 flits)
Measured flits: (0 flits)
Time taken is 138407 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8695.55 (1 samples)
	minimum = 23 (1 samples)
	maximum = 54248 (1 samples)
Network latency average = 8508.46 (1 samples)
	minimum = 23 (1 samples)
	maximum = 53531 (1 samples)
Flit latency average = 34499.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 100918 (1 samples)
Fragmentation average = 207.067 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4479 (1 samples)
Injected packet rate average = 0.0246682 (1 samples)
	minimum = 0.0102857 (1 samples)
	maximum = 0.0462857 (1 samples)
Accepted packet rate average = 0.0123519 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.444052 (1 samples)
	minimum = 0.185571 (1 samples)
	maximum = 0.833143 (1 samples)
Accepted flit rate average = 0.222089 (1 samples)
	minimum = 0.156714 (1 samples)
	maximum = 0.291143 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 17.9801 (1 samples)
Hops average = 5.0607 (1 samples)
Total run time 170.805
