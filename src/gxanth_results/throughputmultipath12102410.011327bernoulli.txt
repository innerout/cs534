BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 153.322
	minimum = 23
	maximum = 643
Network latency average = 151.355
	minimum = 23
	maximum = 643
Slowest packet = 564
Flit latency average = 112.071
	minimum = 6
	maximum = 688
Slowest flit = 11392
Fragmentation average = 50.1465
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0112448
	minimum = 0.004 (at node 12)
	maximum = 0.021 (at node 162)
Accepted packet rate average = 0.00825
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.200438
	minimum = 0.072 (at node 12)
	maximum = 0.378 (at node 162)
Accepted flit rate average= 0.15662
	minimum = 0.036 (at node 41)
	maximum = 0.302 (at node 91)
Injected packet length average = 17.8249
Accepted packet length average = 18.9842
Total in-flight flits = 8791 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 235.149
	minimum = 23
	maximum = 1486
Network latency average = 233.152
	minimum = 23
	maximum = 1486
Slowest packet = 1042
Flit latency average = 188.916
	minimum = 6
	maximum = 1469
Slowest flit = 18773
Fragmentation average = 57.722
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0110964
	minimum = 0.0055 (at node 40)
	maximum = 0.017 (at node 166)
Accepted packet rate average = 0.00882292
	minimum = 0.004 (at node 174)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.198698
	minimum = 0.0955 (at node 40)
	maximum = 0.306 (at node 166)
Accepted flit rate average= 0.162755
	minimum = 0.086 (at node 174)
	maximum = 0.2755 (at node 22)
Injected packet length average = 17.9066
Accepted packet length average = 18.4469
Total in-flight flits = 14200 (0 measured)
latency change    = 0.347979
throughput change = 0.0376972
Class 0:
Packet latency average = 441.13
	minimum = 23
	maximum = 1836
Network latency average = 439.088
	minimum = 23
	maximum = 1836
Slowest packet = 1154
Flit latency average = 393.352
	minimum = 6
	maximum = 2329
Slowest flit = 25562
Fragmentation average = 67.5982
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0112969
	minimum = 0.002 (at node 100)
	maximum = 0.022 (at node 144)
Accepted packet rate average = 0.00948958
	minimum = 0.004 (at node 121)
	maximum = 0.016 (at node 63)
Injected flit rate average = 0.203667
	minimum = 0.036 (at node 100)
	maximum = 0.386 (at node 144)
Accepted flit rate average= 0.170354
	minimum = 0.06 (at node 121)
	maximum = 0.293 (at node 113)
Injected packet length average = 18.0286
Accepted packet length average = 17.9517
Total in-flight flits = 20534 (0 measured)
latency change    = 0.46694
throughput change = 0.0446068
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 295.192
	minimum = 23
	maximum = 994
Network latency average = 293.173
	minimum = 23
	maximum = 987
Slowest packet = 6453
Flit latency average = 546.396
	minimum = 6
	maximum = 2524
Slowest flit = 54935
Fragmentation average = 60.1311
	minimum = 0
	maximum = 133
Injected packet rate average = 0.0117604
	minimum = 0.002 (at node 104)
	maximum = 0.02 (at node 78)
Accepted packet rate average = 0.00951562
	minimum = 0.003 (at node 153)
	maximum = 0.017 (at node 137)
Injected flit rate average = 0.210964
	minimum = 0.036 (at node 104)
	maximum = 0.36 (at node 117)
Accepted flit rate average= 0.171427
	minimum = 0.054 (at node 153)
	maximum = 0.326 (at node 137)
Injected packet length average = 17.9384
Accepted packet length average = 18.0153
Total in-flight flits = 28264 (24439 measured)
latency change    = 0.494384
throughput change = 0.00625873
Class 0:
Packet latency average = 518.868
	minimum = 23
	maximum = 1940
Network latency average = 516.768
	minimum = 23
	maximum = 1940
Slowest packet = 6542
Flit latency average = 625.538
	minimum = 6
	maximum = 2910
Slowest flit = 63575
Fragmentation average = 67.0508
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0114141
	minimum = 0.0065 (at node 18)
	maximum = 0.017 (at node 117)
Accepted packet rate average = 0.00942708
	minimum = 0.005 (at node 117)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.20537
	minimum = 0.117 (at node 18)
	maximum = 0.3115 (at node 172)
Accepted flit rate average= 0.169383
	minimum = 0.093 (at node 117)
	maximum = 0.267 (at node 16)
Injected packet length average = 17.9927
Accepted packet length average = 17.9677
Total in-flight flits = 34385 (33722 measured)
latency change    = 0.431085
throughput change = 0.0120689
Class 0:
Packet latency average = 674.504
	minimum = 23
	maximum = 2917
Network latency average = 672.417
	minimum = 23
	maximum = 2917
Slowest packet = 6604
Flit latency average = 708.601
	minimum = 6
	maximum = 3294
Slowest flit = 91043
Fragmentation average = 67.0378
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0113872
	minimum = 0.007 (at node 104)
	maximum = 0.0163333 (at node 172)
Accepted packet rate average = 0.0095
	minimum = 0.005 (at node 36)
	maximum = 0.0146667 (at node 34)
Injected flit rate average = 0.205071
	minimum = 0.126 (at node 104)
	maximum = 0.297667 (at node 172)
Accepted flit rate average= 0.17091
	minimum = 0.0943333 (at node 36)
	maximum = 0.262 (at node 34)
Injected packet length average = 18.009
Accepted packet length average = 17.9905
Total in-flight flits = 40152 (40080 measured)
latency change    = 0.230741
throughput change = 0.00893401
Draining remaining packets ...
Class 0:
Remaining flits: 126569 126570 126571 126572 126573 126574 126575 141700 141701 141702 [...] (11721 flits)
Measured flits: 126569 126570 126571 126572 126573 126574 126575 141700 141701 141702 [...] (11721 flits)
Time taken is 7877 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 968.683 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3764 (1 samples)
Network latency average = 966.555 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3764 (1 samples)
Flit latency average = 932.691 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3875 (1 samples)
Fragmentation average = 63.3487 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.0113872 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0163333 (1 samples)
Accepted packet rate average = 0.0095 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0146667 (1 samples)
Injected flit rate average = 0.205071 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.297667 (1 samples)
Accepted flit rate average = 0.17091 (1 samples)
	minimum = 0.0943333 (1 samples)
	maximum = 0.262 (1 samples)
Injected packet size average = 18.009 (1 samples)
Accepted packet size average = 17.9905 (1 samples)
Hops average = 5.06678 (1 samples)
Total run time 3.98214
