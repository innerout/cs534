BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.134
	minimum = 22
	maximum = 886
Network latency average = 312.921
	minimum = 22
	maximum = 812
Slowest packet = 87
Flit latency average = 290.116
	minimum = 5
	maximum = 795
Slowest flit = 23884
Fragmentation average = 25.5024
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0471615
	minimum = 0.032 (at node 92)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.0154479
	minimum = 0.006 (at node 108)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.841599
	minimum = 0.576 (at node 92)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.28438
	minimum = 0.108 (at node 108)
	maximum = 0.466 (at node 44)
Injected packet length average = 17.8451
Accepted packet length average = 18.409
Total in-flight flits = 108695 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 690.303
	minimum = 22
	maximum = 1689
Network latency average = 641.303
	minimum = 22
	maximum = 1640
Slowest packet = 87
Flit latency average = 617.182
	minimum = 5
	maximum = 1623
Slowest flit = 55349
Fragmentation average = 26.5279
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0363203
	minimum = 0.0205 (at node 64)
	maximum = 0.0515 (at node 183)
Accepted packet rate average = 0.0152135
	minimum = 0.0085 (at node 153)
	maximum = 0.0215 (at node 56)
Injected flit rate average = 0.652229
	minimum = 0.369 (at node 64)
	maximum = 0.9185 (at node 183)
Accepted flit rate average= 0.276625
	minimum = 0.153 (at node 153)
	maximum = 0.3935 (at node 152)
Injected packet length average = 17.9577
Accepted packet length average = 18.1828
Total in-flight flits = 147072 (0 measured)
latency change    = 0.502923
throughput change = 0.0280351
Class 0:
Packet latency average = 1779.72
	minimum = 397
	maximum = 2578
Network latency average = 1634.37
	minimum = 22
	maximum = 2452
Slowest packet = 4254
Flit latency average = 1615.03
	minimum = 5
	maximum = 2487
Slowest flit = 82913
Fragmentation average = 21.2309
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0150729
	minimum = 0.001 (at node 156)
	maximum = 0.036 (at node 141)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.271526
	minimum = 0.029 (at node 156)
	maximum = 0.662 (at node 141)
Accepted flit rate average= 0.263917
	minimum = 0.109 (at node 167)
	maximum = 0.452 (at node 137)
Injected packet length average = 18.0142
Accepted packet length average = 17.9752
Total in-flight flits = 148384 (0 measured)
latency change    = 0.612128
throughput change = 0.0481528
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2003.63
	minimum = 701
	maximum = 2804
Network latency average = 55.2336
	minimum = 22
	maximum = 797
Slowest packet = 16981
Flit latency average = 2158.21
	minimum = 5
	maximum = 3199
Slowest flit = 128068
Fragmentation average = 4.42056
	minimum = 0
	maximum = 23
Injected packet rate average = 0.0154219
	minimum = 0.003 (at node 63)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0148125
	minimum = 0.007 (at node 127)
	maximum = 0.025 (at node 81)
Injected flit rate average = 0.277458
	minimum = 0.054 (at node 63)
	maximum = 0.712 (at node 158)
Accepted flit rate average= 0.266542
	minimum = 0.126 (at node 127)
	maximum = 0.447 (at node 81)
Injected packet length average = 17.9912
Accepted packet length average = 17.9944
Total in-flight flits = 150596 (51411 measured)
latency change    = 0.11175
throughput change = 0.00984837
Class 0:
Packet latency average = 2762.49
	minimum = 701
	maximum = 3993
Network latency average = 789.77
	minimum = 22
	maximum = 1939
Slowest packet = 16981
Flit latency average = 2360.05
	minimum = 5
	maximum = 4025
Slowest flit = 151989
Fragmentation average = 10.9799
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0153073
	minimum = 0.005 (at node 175)
	maximum = 0.031 (at node 158)
Accepted packet rate average = 0.0148229
	minimum = 0.008 (at node 116)
	maximum = 0.024 (at node 0)
Injected flit rate average = 0.275198
	minimum = 0.084 (at node 175)
	maximum = 0.563 (at node 158)
Accepted flit rate average= 0.266708
	minimum = 0.148 (at node 116)
	maximum = 0.4315 (at node 0)
Injected packet length average = 17.9782
Accepted packet length average = 17.993
Total in-flight flits = 151754 (97454 measured)
latency change    = 0.274703
throughput change = 0.000624902
Class 0:
Packet latency average = 3495.54
	minimum = 701
	maximum = 4866
Network latency average = 1391.3
	minimum = 22
	maximum = 2954
Slowest packet = 16981
Flit latency average = 2514.71
	minimum = 5
	maximum = 5006
Slowest flit = 162378
Fragmentation average = 15.2912
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0151944
	minimum = 0.00733333 (at node 32)
	maximum = 0.026 (at node 186)
Accepted packet rate average = 0.0147986
	minimum = 0.009 (at node 36)
	maximum = 0.021 (at node 0)
Injected flit rate average = 0.273257
	minimum = 0.129 (at node 127)
	maximum = 0.463333 (at node 186)
Accepted flit rate average= 0.266344
	minimum = 0.162 (at node 36)
	maximum = 0.381333 (at node 0)
Injected packet length average = 17.984
Accepted packet length average = 17.9979
Total in-flight flits = 152470 (131461 measured)
latency change    = 0.20971
throughput change = 0.00136885
Draining remaining packets ...
Class 0:
Remaining flits: 179427 179428 179429 179430 179431 179432 179433 179434 179435 179436 [...] (102341 flits)
Measured flits: 305586 305587 305588 305589 305590 305591 305592 305593 305594 305595 [...] (98902 flits)
Class 0:
Remaining flits: 255852 255853 255854 255855 255856 255857 255858 255859 255860 255861 [...] (53867 flits)
Measured flits: 307062 307063 307064 307065 307066 307067 307068 307069 307070 307071 [...] (53737 flits)
Class 0:
Remaining flits: 322596 322597 322598 322599 322600 322601 322602 322603 322604 322605 [...] (8335 flits)
Measured flits: 322596 322597 322598 322599 322600 322601 322602 322603 322604 322605 [...] (8335 flits)
Time taken is 9876 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5271.2 (1 samples)
	minimum = 701 (1 samples)
	maximum = 7868 (1 samples)
Network latency average = 2778.42 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5781 (1 samples)
Flit latency average = 2838.32 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6144 (1 samples)
Fragmentation average = 18.1854 (1 samples)
	minimum = 0 (1 samples)
	maximum = 158 (1 samples)
Injected packet rate average = 0.0151944 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0147986 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.273257 (1 samples)
	minimum = 0.129 (1 samples)
	maximum = 0.463333 (1 samples)
Accepted flit rate average = 0.266344 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 0.381333 (1 samples)
Injected packet size average = 17.984 (1 samples)
Accepted packet size average = 17.9979 (1 samples)
Hops average = 5.06766 (1 samples)
Total run time 14.9962
