BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.569
	minimum = 24
	maximum = 884
Network latency average = 164.333
	minimum = 22
	maximum = 747
Slowest packet = 64
Flit latency average = 136.812
	minimum = 5
	maximum = 745
Slowest flit = 14990
Fragmentation average = 28.8525
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.01225
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22788
	minimum = 0.108 (at node 41)
	maximum = 0.442 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6025
Total in-flight flits = 18257 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 355.905
	minimum = 22
	maximum = 1755
Network latency average = 275.021
	minimum = 22
	maximum = 1322
Slowest packet = 64
Flit latency average = 245.416
	minimum = 5
	maximum = 1413
Slowest flit = 33114
Fragmentation average = 35.0354
	minimum = 0
	maximum = 548
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0131849
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241599
	minimum = 0.153 (at node 83)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.3239
Total in-flight flits = 26818 (0 measured)
latency change    = 0.354971
throughput change = 0.0567832
Class 0:
Packet latency average = 629.938
	minimum = 22
	maximum = 2428
Network latency average = 527.649
	minimum = 22
	maximum = 2021
Slowest packet = 2901
Flit latency average = 492.646
	minimum = 5
	maximum = 2066
Slowest flit = 56778
Fragmentation average = 43.7914
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145313
	minimum = 0.004 (at node 118)
	maximum = 0.025 (at node 57)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261432
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.991
Total in-flight flits = 38993 (0 measured)
latency change    = 0.435017
throughput change = 0.0758641
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.575
	minimum = 25
	maximum = 1427
Network latency average = 315.059
	minimum = 22
	maximum = 986
Slowest packet = 10126
Flit latency average = 637.743
	minimum = 5
	maximum = 2511
Slowest flit = 62801
Fragmentation average = 29.4123
	minimum = 0
	maximum = 318
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148125
	minimum = 0.007 (at node 57)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267599
	minimum = 0.114 (at node 117)
	maximum = 0.483 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 18.0658
Total in-flight flits = 49552 (41388 measured)
latency change    = 0.541794
throughput change = 0.0230444
Class 0:
Packet latency average = 672.476
	minimum = 22
	maximum = 2170
Network latency average = 575.343
	minimum = 22
	maximum = 1970
Slowest packet = 10126
Flit latency average = 719.631
	minimum = 5
	maximum = 2511
Slowest flit = 62801
Fragmentation average = 40.5752
	minimum = 0
	maximum = 695
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0148177
	minimum = 0.009 (at node 110)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.267266
	minimum = 0.171 (at node 36)
	maximum = 0.422 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 18.0369
Total in-flight flits = 56009 (54890 measured)
latency change    = 0.392432
throughput change = 0.0012472
Class 0:
Packet latency average = 836.815
	minimum = 22
	maximum = 3423
Network latency average = 739.454
	minimum = 22
	maximum = 2946
Slowest packet = 10126
Flit latency average = 794.032
	minimum = 5
	maximum = 3296
Slowest flit = 162016
Fragmentation average = 44.6468
	minimum = 0
	maximum = 695
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.014849
	minimum = 0.0103333 (at node 135)
	maximum = 0.0213333 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267741
	minimum = 0.190667 (at node 144)
	maximum = 0.381667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.031
Total in-flight flits = 65908 (65791 measured)
latency change    = 0.196386
throughput change = 0.00177669
Draining remaining packets ...
Class 0:
Remaining flits: 203290 203291 205218 205219 205220 205221 205222 205223 205224 205225 [...] (24123 flits)
Measured flits: 203290 203291 205218 205219 205220 205221 205222 205223 205224 205225 [...] (24123 flits)
Class 0:
Remaining flits: 244404 244405 244406 244407 244408 244409 244410 244411 244412 244413 [...] (4470 flits)
Measured flits: 244404 244405 244406 244407 244408 244409 244410 244411 244412 244413 [...] (4470 flits)
Class 0:
Remaining flits: 331344 331345 331346 331347 331348 331349 331350 331351 331352 331353 [...] (417 flits)
Measured flits: 331344 331345 331346 331347 331348 331349 331350 331351 331352 331353 [...] (417 flits)
Time taken is 9240 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1197.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4529 (1 samples)
Network latency average = 1093.44 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4436 (1 samples)
Flit latency average = 1052.46 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4419 (1 samples)
Fragmentation average = 49.5903 (1 samples)
	minimum = 0 (1 samples)
	maximum = 796 (1 samples)
Injected packet rate average = 0.0174705 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.014849 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.314714 (1 samples)
	minimum = 0.049 (1 samples)
	maximum = 0.725333 (1 samples)
Accepted flit rate average = 0.267741 (1 samples)
	minimum = 0.190667 (1 samples)
	maximum = 0.381667 (1 samples)
Injected packet size average = 18.014 (1 samples)
Accepted packet size average = 18.031 (1 samples)
Hops average = 5.07761 (1 samples)
Total run time 7.07613
