BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 196.602
	minimum = 22
	maximum = 609
Network latency average = 191.97
	minimum = 22
	maximum = 589
Slowest packet = 1533
Flit latency average = 167.759
	minimum = 5
	maximum = 572
Slowest flit = 27611
Fragmentation average = 18.3594
	minimum = 0
	maximum = 79
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.0134635
	minimum = 0.005 (at node 115)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.24687
	minimum = 0.09 (at node 115)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.8234
Accepted packet length average = 18.3362
Total in-flight flits = 22207 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 333.068
	minimum = 22
	maximum = 1107
Network latency average = 328.216
	minimum = 22
	maximum = 1107
Slowest packet = 3424
Flit latency average = 303.37
	minimum = 5
	maximum = 1090
Slowest flit = 61647
Fragmentation average = 19.2569
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0141719
	minimum = 0.0075 (at node 153)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.257341
	minimum = 0.135 (at node 153)
	maximum = 0.3735 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 18.1586
Total in-flight flits = 39385 (0 measured)
latency change    = 0.409726
throughput change = 0.0406906
Class 0:
Packet latency average = 697.891
	minimum = 22
	maximum = 1482
Network latency average = 693.169
	minimum = 22
	maximum = 1476
Slowest packet = 4635
Flit latency average = 670.021
	minimum = 5
	maximum = 1459
Slowest flit = 83447
Fragmentation average = 18.0806
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.015
	minimum = 0.006 (at node 126)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.270995
	minimum = 0.108 (at node 126)
	maximum = 0.481 (at node 16)
Injected packet length average = 17.9811
Accepted packet length average = 18.0663
Total in-flight flits = 57032 (0 measured)
latency change    = 0.522751
throughput change = 0.0503834
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 195.572
	minimum = 22
	maximum = 1010
Network latency average = 190.393
	minimum = 22
	maximum = 981
Slowest packet = 11551
Flit latency average = 892.585
	minimum = 5
	maximum = 1731
Slowest flit = 152074
Fragmentation average = 7.46096
	minimum = 0
	maximum = 72
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0154271
	minimum = 0.007 (at node 48)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.277073
	minimum = 0.126 (at node 48)
	maximum = 0.477 (at node 129)
Injected packet length average = 17.9618
Accepted packet length average = 17.9602
Total in-flight flits = 75456 (64284 measured)
latency change    = 2.56847
throughput change = 0.0219369
Class 0:
Packet latency average = 960.01
	minimum = 22
	maximum = 1960
Network latency average = 955.069
	minimum = 22
	maximum = 1950
Slowest packet = 11595
Flit latency average = 1012.57
	minimum = 5
	maximum = 1996
Slowest flit = 204552
Fragmentation average = 16.5928
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.015375
	minimum = 0.009 (at node 113)
	maximum = 0.023 (at node 66)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.276216
	minimum = 0.17 (at node 113)
	maximum = 0.4085 (at node 66)
Injected packet length average = 18.0096
Accepted packet length average = 17.9653
Total in-flight flits = 91077 (91077 measured)
latency change    = 0.796282
throughput change = 0.00310181
Class 0:
Packet latency average = 1192.28
	minimum = 22
	maximum = 2320
Network latency average = 1187.24
	minimum = 22
	maximum = 2305
Slowest packet = 14137
Flit latency average = 1134.21
	minimum = 5
	maximum = 2288
Slowest flit = 254483
Fragmentation average = 17.7559
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0153663
	minimum = 0.01 (at node 135)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.276564
	minimum = 0.178667 (at node 135)
	maximum = 0.382333 (at node 128)
Injected packet length average = 18.0047
Accepted packet length average = 17.9981
Total in-flight flits = 108511 (108511 measured)
latency change    = 0.194811
throughput change = 0.00125862
Class 0:
Packet latency average = 1345.37
	minimum = 22
	maximum = 2711
Network latency average = 1340.39
	minimum = 22
	maximum = 2701
Slowest packet = 16233
Flit latency average = 1252.44
	minimum = 5
	maximum = 2684
Slowest flit = 292211
Fragmentation average = 18.1752
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0203372
	minimum = 0.01475 (at node 5)
	maximum = 0.026 (at node 156)
Accepted packet rate average = 0.0153268
	minimum = 0.011 (at node 135)
	maximum = 0.02075 (at node 128)
Injected flit rate average = 0.366148
	minimum = 0.2655 (at node 5)
	maximum = 0.468 (at node 156)
Accepted flit rate average= 0.275792
	minimum = 0.197 (at node 135)
	maximum = 0.37275 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 17.9941
Total in-flight flits = 126366 (126366 measured)
latency change    = 0.113791
throughput change = 0.00280128
Class 0:
Packet latency average = 1478.09
	minimum = 22
	maximum = 3072
Network latency average = 1473.16
	minimum = 22
	maximum = 3061
Slowest packet = 19087
Flit latency average = 1369.39
	minimum = 5
	maximum = 3044
Slowest flit = 343583
Fragmentation average = 18.4429
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0203438
	minimum = 0.0154 (at node 139)
	maximum = 0.0248 (at node 113)
Accepted packet rate average = 0.0153573
	minimum = 0.0118 (at node 42)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.366196
	minimum = 0.2772 (at node 139)
	maximum = 0.4464 (at node 113)
Accepted flit rate average= 0.276358
	minimum = 0.2082 (at node 42)
	maximum = 0.3622 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 17.9953
Total in-flight flits = 143268 (143268 measured)
latency change    = 0.0897894
throughput change = 0.00205048
Class 0:
Packet latency average = 1608.73
	minimum = 22
	maximum = 3368
Network latency average = 1603.77
	minimum = 22
	maximum = 3364
Slowest packet = 21300
Flit latency average = 1489.51
	minimum = 5
	maximum = 3347
Slowest flit = 389554
Fragmentation average = 18.5931
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0203151
	minimum = 0.016 (at node 155)
	maximum = 0.0245 (at node 96)
Accepted packet rate average = 0.0153281
	minimum = 0.0121667 (at node 171)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.365752
	minimum = 0.289333 (at node 155)
	maximum = 0.441 (at node 96)
Accepted flit rate average= 0.275887
	minimum = 0.2185 (at node 42)
	maximum = 0.3505 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 17.9988
Total in-flight flits = 160464 (160464 measured)
latency change    = 0.0812083
throughput change = 0.00170787
Class 0:
Packet latency average = 1735.71
	minimum = 22
	maximum = 3766
Network latency average = 1730.72
	minimum = 22
	maximum = 3766
Slowest packet = 23579
Flit latency average = 1609.93
	minimum = 5
	maximum = 3751
Slowest flit = 435528
Fragmentation average = 18.6329
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0202984
	minimum = 0.0158571 (at node 155)
	maximum = 0.0248571 (at node 165)
Accepted packet rate average = 0.0153341
	minimum = 0.0118571 (at node 42)
	maximum = 0.0192857 (at node 70)
Injected flit rate average = 0.365336
	minimum = 0.287286 (at node 155)
	maximum = 0.445429 (at node 165)
Accepted flit rate average= 0.276038
	minimum = 0.210429 (at node 42)
	maximum = 0.347429 (at node 70)
Injected packet length average = 17.9983
Accepted packet length average = 18.0016
Total in-flight flits = 177095 (177095 measured)
latency change    = 0.0731599
throughput change = 0.000546279
Draining all recorded packets ...
Class 0:
Remaining flits: 467383 467384 467385 467386 467387 477810 477811 477812 477813 477814 [...] (194026 flits)
Measured flits: 467383 467384 467385 467386 467387 477810 477811 477812 477813 477814 [...] (130329 flits)
Class 0:
Remaining flits: 521921 521922 521923 521924 521925 521926 521927 535932 535933 535934 [...] (208805 flits)
Measured flits: 521921 521922 521923 521924 521925 521926 521927 535932 535933 535934 [...] (83077 flits)
Class 0:
Remaining flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (225376 flits)
Measured flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (36387 flits)
Class 0:
Remaining flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (241927 flits)
Measured flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (4873 flits)
Class 0:
Remaining flits: 677114 677115 677116 677117 677118 677119 677120 677121 677122 677123 [...] (257667 flits)
Measured flits: 677114 677115 677116 677117 677118 677119 677120 677121 677122 677123 [...] (61 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 731722 731723 731724 731725 731726 731727 731728 731729 731730 731731 [...] (214895 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 780588 780589 780590 780591 780592 780593 780594 780595 780596 780597 [...] (167403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825539 825540 825541 825542 825543 825544 825545 825546 825547 825548 [...] (119706 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 893497 893498 893499 893500 893501 894186 894187 894188 894189 894190 [...] (72189 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951112 951113 951114 951115 951116 951117 951118 951119 957762 957763 [...] (26974 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 994140 994141 994142 994143 994144 994145 994146 994147 994148 994149 [...] (2813 flits)
Measured flits: (0 flits)
Time taken is 22140 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2313.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5329 (1 samples)
Network latency average = 2308.42 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5329 (1 samples)
Flit latency average = 2986.41 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7217 (1 samples)
Fragmentation average = 19.4313 (1 samples)
	minimum = 0 (1 samples)
	maximum = 102 (1 samples)
Injected packet rate average = 0.0202984 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0248571 (1 samples)
Accepted packet rate average = 0.0153341 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0192857 (1 samples)
Injected flit rate average = 0.365336 (1 samples)
	minimum = 0.287286 (1 samples)
	maximum = 0.445429 (1 samples)
Accepted flit rate average = 0.276038 (1 samples)
	minimum = 0.210429 (1 samples)
	maximum = 0.347429 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 18.0016 (1 samples)
Hops average = 5.07713 (1 samples)
Total run time 14.451
