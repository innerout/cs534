BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 277.553
	minimum = 26
	maximum = 914
Network latency average = 271.431
	minimum = 23
	maximum = 909
Slowest packet = 228
Flit latency average = 237.319
	minimum = 6
	maximum = 917
Slowest flit = 5858
Fragmentation average = 56.8067
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0240885
	minimum = 0.012 (at node 164)
	maximum = 0.04 (at node 30)
Accepted packet rate average = 0.00980729
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 91)
Injected flit rate average = 0.430167
	minimum = 0.213 (at node 164)
	maximum = 0.72 (at node 30)
Accepted flit rate average= 0.184901
	minimum = 0.054 (at node 41)
	maximum = 0.324 (at node 91)
Injected packet length average = 17.8577
Accepted packet length average = 18.8534
Total in-flight flits = 47839 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 533.721
	minimum = 26
	maximum = 1782
Network latency average = 523.403
	minimum = 23
	maximum = 1782
Slowest packet = 755
Flit latency average = 488.268
	minimum = 6
	maximum = 1813
Slowest flit = 13642
Fragmentation average = 61.394
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0232031
	minimum = 0.0135 (at node 36)
	maximum = 0.034 (at node 30)
Accepted packet rate average = 0.00991406
	minimum = 0.0045 (at node 30)
	maximum = 0.015 (at node 152)
Injected flit rate average = 0.415471
	minimum = 0.243 (at node 36)
	maximum = 0.6075 (at node 30)
Accepted flit rate average= 0.182542
	minimum = 0.081 (at node 30)
	maximum = 0.2755 (at node 152)
Injected packet length average = 17.9058
Accepted packet length average = 18.4124
Total in-flight flits = 90608 (0 measured)
latency change    = 0.479967
throughput change = 0.0129251
Class 0:
Packet latency average = 1371.97
	minimum = 23
	maximum = 2496
Network latency average = 1331.17
	minimum = 23
	maximum = 2496
Slowest packet = 2317
Flit latency average = 1301.52
	minimum = 6
	maximum = 2492
Slowest flit = 41522
Fragmentation average = 66.1726
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0180833
	minimum = 0 (at node 140)
	maximum = 0.032 (at node 25)
Accepted packet rate average = 0.00977604
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.32675
	minimum = 0 (at node 140)
	maximum = 0.576 (at node 25)
Accepted flit rate average= 0.175562
	minimum = 0.054 (at node 40)
	maximum = 0.331 (at node 152)
Injected packet length average = 18.0691
Accepted packet length average = 17.9584
Total in-flight flits = 120512 (0 measured)
latency change    = 0.610983
throughput change = 0.0397532
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 836.493
	minimum = 23
	maximum = 2563
Network latency average = 180.88
	minimum = 23
	maximum = 884
Slowest packet = 12469
Flit latency average = 1890.13
	minimum = 6
	maximum = 3371
Slowest flit = 50867
Fragmentation average = 24.3067
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0111719
	minimum = 0 (at node 28)
	maximum = 0.03 (at node 85)
Accepted packet rate average = 0.00920833
	minimum = 0.003 (at node 65)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.200958
	minimum = 0 (at node 28)
	maximum = 0.529 (at node 85)
Accepted flit rate average= 0.165089
	minimum = 0.043 (at node 96)
	maximum = 0.329 (at node 16)
Injected packet length average = 17.9879
Accepted packet length average = 17.9282
Total in-flight flits = 127983 (37775 measured)
latency change    = 0.640149
throughput change = 0.0634445
Class 0:
Packet latency average = 1555.01
	minimum = 23
	maximum = 3190
Network latency average = 681.918
	minimum = 23
	maximum = 1952
Slowest packet = 12469
Flit latency average = 2164.93
	minimum = 6
	maximum = 4076
Slowest flit = 70254
Fragmentation average = 41.1765
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00976823
	minimum = 0.0005 (at node 64)
	maximum = 0.0225 (at node 61)
Accepted packet rate average = 0.00920573
	minimum = 0.0045 (at node 32)
	maximum = 0.016 (at node 182)
Injected flit rate average = 0.176435
	minimum = 0.009 (at node 64)
	maximum = 0.4 (at node 61)
Accepted flit rate average= 0.165628
	minimum = 0.0755 (at node 96)
	maximum = 0.272 (at node 182)
Injected packet length average = 18.0621
Accepted packet length average = 17.9918
Total in-flight flits = 125545 (63869 measured)
latency change    = 0.462066
throughput change = 0.00325467
Class 0:
Packet latency average = 2266.65
	minimum = 23
	maximum = 4266
Network latency average = 1200.97
	minimum = 23
	maximum = 2940
Slowest packet = 12469
Flit latency average = 2395.12
	minimum = 6
	maximum = 4882
Slowest flit = 82133
Fragmentation average = 53.745
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00927257
	minimum = 0.00166667 (at node 53)
	maximum = 0.0193333 (at node 27)
Accepted packet rate average = 0.00918229
	minimum = 0.00533333 (at node 96)
	maximum = 0.0146667 (at node 182)
Injected flit rate average = 0.166986
	minimum = 0.0263333 (at node 66)
	maximum = 0.348 (at node 27)
Accepted flit rate average= 0.165365
	minimum = 0.098 (at node 96)
	maximum = 0.256333 (at node 99)
Injected packet length average = 18.0086
Accepted packet length average = 18.0091
Total in-flight flits = 122174 (83818 measured)
latency change    = 0.313962
throughput change = 0.00159055
Class 0:
Packet latency average = 2858.67
	minimum = 23
	maximum = 5440
Network latency average = 1768.3
	minimum = 23
	maximum = 3951
Slowest packet = 12469
Flit latency average = 2607.13
	minimum = 6
	maximum = 5785
Slowest flit = 99799
Fragmentation average = 60.9779
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00914062
	minimum = 0.002 (at node 43)
	maximum = 0.018 (at node 58)
Accepted packet rate average = 0.00908464
	minimum = 0.00625 (at node 36)
	maximum = 0.01325 (at node 182)
Injected flit rate average = 0.16441
	minimum = 0.036 (at node 43)
	maximum = 0.3225 (at node 58)
Accepted flit rate average= 0.163382
	minimum = 0.1125 (at node 36)
	maximum = 0.23725 (at node 99)
Injected packet length average = 17.9868
Accepted packet length average = 17.9844
Total in-flight flits = 122079 (100172 measured)
latency change    = 0.207095
throughput change = 0.0121377
Draining remaining packets ...
Class 0:
Remaining flits: 102280 102281 102282 102283 102284 102285 102286 102287 102288 102289 [...] (92558 flits)
Measured flits: 224334 224335 224336 224337 224338 224339 224340 224341 224342 224343 [...] (80799 flits)
Class 0:
Remaining flits: 128088 128089 128090 128091 128092 128093 128094 128095 128096 128097 [...] (62778 flits)
Measured flits: 224352 224353 224354 224355 224356 224357 224358 224359 224360 224361 [...] (57429 flits)
Class 0:
Remaining flits: 130053 130054 130055 130056 130057 130058 130059 130060 130061 130062 [...] (34343 flits)
Measured flits: 224352 224353 224354 224355 224356 224357 224358 224359 224360 224361 [...] (32456 flits)
Class 0:
Remaining flits: 163508 163509 163510 163511 168444 168445 168446 168447 168448 168449 [...] (9176 flits)
Measured flits: 230454 230455 230456 230457 230458 230459 230460 230461 230462 230463 [...] (8936 flits)
Class 0:
Remaining flits: 293850 293851 293852 293853 293854 293855 293856 293857 293858 293859 [...] (36 flits)
Measured flits: 293850 293851 293852 293853 293854 293855 293856 293857 293858 293859 [...] (36 flits)
Time taken is 12040 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5041.36 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9038 (1 samples)
Network latency average = 3637.78 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8406 (1 samples)
Flit latency average = 3486.18 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9162 (1 samples)
Fragmentation average = 68.4044 (1 samples)
	minimum = 0 (1 samples)
	maximum = 166 (1 samples)
Injected packet rate average = 0.00914062 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.018 (1 samples)
Accepted packet rate average = 0.00908464 (1 samples)
	minimum = 0.00625 (1 samples)
	maximum = 0.01325 (1 samples)
Injected flit rate average = 0.16441 (1 samples)
	minimum = 0.036 (1 samples)
	maximum = 0.3225 (1 samples)
Accepted flit rate average = 0.163382 (1 samples)
	minimum = 0.1125 (1 samples)
	maximum = 0.23725 (1 samples)
Injected packet size average = 17.9868 (1 samples)
Accepted packet size average = 17.9844 (1 samples)
Hops average = 5.10145 (1 samples)
Total run time 8.13633
