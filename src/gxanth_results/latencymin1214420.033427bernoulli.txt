BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 278.397
	minimum = 22
	maximum = 741
Network latency average = 268.773
	minimum = 22
	maximum = 721
Slowest packet = 1258
Flit latency average = 246.188
	minimum = 5
	maximum = 735
Slowest flit = 27834
Fragmentation average = 22.7866
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0328958
	minimum = 0.02 (at node 62)
	maximum = 0.048 (at node 157)
Accepted packet rate average = 0.0147917
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.586443
	minimum = 0.355 (at node 62)
	maximum = 0.852 (at node 157)
Accepted flit rate average= 0.27313
	minimum = 0.134 (at node 174)
	maximum = 0.436 (at node 152)
Injected packet length average = 17.8273
Accepted packet length average = 18.4651
Total in-flight flits = 61301 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 529.314
	minimum = 22
	maximum = 1595
Network latency average = 516.583
	minimum = 22
	maximum = 1567
Slowest packet = 2447
Flit latency average = 492.311
	minimum = 5
	maximum = 1557
Slowest flit = 48409
Fragmentation average = 23.9515
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0317422
	minimum = 0.0205 (at node 160)
	maximum = 0.0415 (at node 151)
Accepted packet rate average = 0.0152396
	minimum = 0.009 (at node 83)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.569065
	minimum = 0.369 (at node 160)
	maximum = 0.747 (at node 151)
Accepted flit rate average= 0.277146
	minimum = 0.162 (at node 116)
	maximum = 0.3975 (at node 152)
Injected packet length average = 17.9277
Accepted packet length average = 18.1859
Total in-flight flits = 113554 (0 measured)
latency change    = 0.474041
throughput change = 0.0144892
Class 0:
Packet latency average = 1316.58
	minimum = 22
	maximum = 2317
Network latency average = 1288.37
	minimum = 22
	maximum = 2219
Slowest packet = 4090
Flit latency average = 1270.71
	minimum = 5
	maximum = 2202
Slowest flit = 84761
Fragmentation average = 21.4473
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0231719
	minimum = 0.004 (at node 98)
	maximum = 0.041 (at node 33)
Accepted packet rate average = 0.0149167
	minimum = 0.005 (at node 22)
	maximum = 0.025 (at node 151)
Injected flit rate average = 0.418896
	minimum = 0.073 (at node 98)
	maximum = 0.739 (at node 33)
Accepted flit rate average= 0.267932
	minimum = 0.118 (at node 22)
	maximum = 0.441 (at node 151)
Injected packet length average = 18.0778
Accepted packet length average = 17.9619
Total in-flight flits = 143507 (0 measured)
latency change    = 0.597962
throughput change = 0.0343876
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 822.72
	minimum = 25
	maximum = 1774
Network latency average = 116.976
	minimum = 22
	maximum = 975
Slowest packet = 16827
Flit latency average = 1745.32
	minimum = 5
	maximum = 2867
Slowest flit = 107657
Fragmentation average = 6.112
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0167031
	minimum = 0.002 (at node 36)
	maximum = 0.042 (at node 119)
Accepted packet rate average = 0.0150833
	minimum = 0.006 (at node 79)
	maximum = 0.024 (at node 99)
Injected flit rate average = 0.300073
	minimum = 0.036 (at node 36)
	maximum = 0.743 (at node 119)
Accepted flit rate average= 0.270479
	minimum = 0.108 (at node 79)
	maximum = 0.44 (at node 99)
Injected packet length average = 17.9651
Accepted packet length average = 17.9323
Total in-flight flits = 149409 (55549 measured)
latency change    = 0.600275
throughput change = 0.00941616
Class 0:
Packet latency average = 1559.52
	minimum = 25
	maximum = 3212
Network latency average = 673.482
	minimum = 22
	maximum = 1949
Slowest packet = 16827
Flit latency average = 1958.78
	minimum = 5
	maximum = 3606
Slowest flit = 157212
Fragmentation average = 10.5965
	minimum = 0
	maximum = 85
Injected packet rate average = 0.0157526
	minimum = 0.006 (at node 61)
	maximum = 0.0305 (at node 113)
Accepted packet rate average = 0.0149349
	minimum = 0.009 (at node 79)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.283445
	minimum = 0.1045 (at node 184)
	maximum = 0.546 (at node 113)
Accepted flit rate average= 0.268911
	minimum = 0.1645 (at node 79)
	maximum = 0.405 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0056
Total in-flight flits = 149379 (100594 measured)
latency change    = 0.472452
throughput change = 0.00582983
Class 0:
Packet latency average = 2352.05
	minimum = 25
	maximum = 3934
Network latency average = 1410.69
	minimum = 22
	maximum = 2969
Slowest packet = 16827
Flit latency average = 2136.69
	minimum = 5
	maximum = 4423
Slowest flit = 176490
Fragmentation average = 14.7485
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0157031
	minimum = 0.00733333 (at node 111)
	maximum = 0.0296667 (at node 113)
Accepted packet rate average = 0.0149826
	minimum = 0.01 (at node 79)
	maximum = 0.0203333 (at node 165)
Injected flit rate average = 0.282675
	minimum = 0.132 (at node 111)
	maximum = 0.529 (at node 113)
Accepted flit rate average= 0.269486
	minimum = 0.18 (at node 79)
	maximum = 0.369 (at node 165)
Injected packet length average = 18.0012
Accepted packet length average = 17.9866
Total in-flight flits = 151543 (133542 measured)
latency change    = 0.336954
throughput change = 0.0021324
Class 0:
Packet latency average = 2914.55
	minimum = 25
	maximum = 4695
Network latency average = 1918.37
	minimum = 22
	maximum = 3965
Slowest packet = 16827
Flit latency average = 2281.38
	minimum = 5
	maximum = 5048
Slowest flit = 214049
Fragmentation average = 16.9052
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0155924
	minimum = 0.008 (at node 163)
	maximum = 0.0295 (at node 113)
Accepted packet rate average = 0.0149661
	minimum = 0.011 (at node 86)
	maximum = 0.021 (at node 138)
Injected flit rate average = 0.280553
	minimum = 0.14825 (at node 163)
	maximum = 0.531 (at node 113)
Accepted flit rate average= 0.269447
	minimum = 0.197 (at node 86)
	maximum = 0.376 (at node 138)
Injected packet length average = 17.9929
Accepted packet length average = 18.0037
Total in-flight flits = 152446 (148962 measured)
latency change    = 0.192998
throughput change = 0.000146584
Class 0:
Packet latency average = 3382.65
	minimum = 25
	maximum = 5731
Network latency average = 2299.4
	minimum = 22
	maximum = 4934
Slowest packet = 16827
Flit latency average = 2407.96
	minimum = 5
	maximum = 5820
Slowest flit = 220157
Fragmentation average = 18.1025
	minimum = 0
	maximum = 155
Injected packet rate average = 0.01545
	minimum = 0.009 (at node 111)
	maximum = 0.0278 (at node 113)
Accepted packet rate average = 0.01495
	minimum = 0.0106 (at node 79)
	maximum = 0.0196 (at node 138)
Injected flit rate average = 0.278065
	minimum = 0.1608 (at node 111)
	maximum = 0.5004 (at node 113)
Accepted flit rate average= 0.269067
	minimum = 0.1936 (at node 79)
	maximum = 0.3512 (at node 138)
Injected packet length average = 17.9977
Accepted packet length average = 17.9978
Total in-flight flits = 152521 (152383 measured)
latency change    = 0.138383
throughput change = 0.0014121
Class 0:
Packet latency average = 3763.97
	minimum = 25
	maximum = 6423
Network latency average = 2488.72
	minimum = 22
	maximum = 5542
Slowest packet = 16827
Flit latency average = 2487.23
	minimum = 5
	maximum = 5952
Slowest flit = 225359
Fragmentation average = 18.6373
	minimum = 0
	maximum = 201
Injected packet rate average = 0.0153212
	minimum = 0.00833333 (at node 111)
	maximum = 0.0243333 (at node 113)
Accepted packet rate average = 0.0149288
	minimum = 0.0108333 (at node 79)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.275741
	minimum = 0.1495 (at node 111)
	maximum = 0.438 (at node 113)
Accepted flit rate average= 0.268692
	minimum = 0.195 (at node 79)
	maximum = 0.340667 (at node 138)
Injected packet length average = 17.9974
Accepted packet length average = 17.9982
Total in-flight flits = 152016 (152016 measured)
latency change    = 0.101307
throughput change = 0.001395
Class 0:
Packet latency average = 4102.87
	minimum = 25
	maximum = 7314
Network latency average = 2579.73
	minimum = 22
	maximum = 6448
Slowest packet = 16827
Flit latency average = 2537.47
	minimum = 5
	maximum = 6431
Slowest flit = 327069
Fragmentation average = 18.7688
	minimum = 0
	maximum = 201
Injected packet rate average = 0.0152173
	minimum = 0.00971429 (at node 47)
	maximum = 0.0225714 (at node 113)
Accepted packet rate average = 0.014933
	minimum = 0.0105714 (at node 79)
	maximum = 0.0181429 (at node 70)
Injected flit rate average = 0.273929
	minimum = 0.174857 (at node 47)
	maximum = 0.406143 (at node 113)
Accepted flit rate average= 0.268796
	minimum = 0.195 (at node 79)
	maximum = 0.327286 (at node 70)
Injected packet length average = 18.0012
Accepted packet length average = 18.0001
Total in-flight flits = 150777 (150777 measured)
latency change    = 0.0826014
throughput change = 0.000387992
Draining all recorded packets ...
Class 0:
Remaining flits: 413478 413479 413480 413481 413482 413483 413484 413485 413486 413487 [...] (150015 flits)
Measured flits: 413478 413479 413480 413481 413482 413483 413484 413485 413486 413487 [...] (150015 flits)
Class 0:
Remaining flits: 441144 441145 441146 441147 441148 441149 441150 441151 441152 441153 [...] (149248 flits)
Measured flits: 441144 441145 441146 441147 441148 441149 441150 441151 441152 441153 [...] (149248 flits)
Class 0:
Remaining flits: 506232 506233 506234 506235 506236 506237 506238 506239 506240 506241 [...] (149248 flits)
Measured flits: 506232 506233 506234 506235 506236 506237 506238 506239 506240 506241 [...] (149248 flits)
Class 0:
Remaining flits: 525564 525565 525566 525567 525568 525569 525570 525571 525572 525573 [...] (146826 flits)
Measured flits: 525564 525565 525566 525567 525568 525569 525570 525571 525572 525573 [...] (146808 flits)
Class 0:
Remaining flits: 596628 596629 596630 596631 596632 596633 596634 596635 596636 596637 [...] (145268 flits)
Measured flits: 596628 596629 596630 596631 596632 596633 596634 596635 596636 596637 [...] (144044 flits)
Class 0:
Remaining flits: 637596 637597 637598 637599 637600 637601 637602 637603 637604 637605 [...] (145175 flits)
Measured flits: 637596 637597 637598 637599 637600 637601 637602 637603 637604 637605 [...] (142043 flits)
Class 0:
Remaining flits: 669600 669601 669602 669603 669604 669605 669606 669607 669608 669609 [...] (144008 flits)
Measured flits: 669600 669601 669602 669603 669604 669605 669606 669607 669608 669609 [...] (135332 flits)
Class 0:
Remaining flits: 726102 726103 726104 726105 726106 726107 726108 726109 726110 726111 [...] (143053 flits)
Measured flits: 726102 726103 726104 726105 726106 726107 726108 726109 726110 726111 [...] (125702 flits)
Class 0:
Remaining flits: 768798 768799 768800 768801 768802 768803 768804 768805 768806 768807 [...] (142745 flits)
Measured flits: 768798 768799 768800 768801 768802 768803 768804 768805 768806 768807 [...] (111782 flits)
Class 0:
Remaining flits: 789930 789931 789932 789933 789934 789935 789936 789937 789938 789939 [...] (143710 flits)
Measured flits: 789930 789931 789932 789933 789934 789935 789936 789937 789938 789939 [...] (91542 flits)
Class 0:
Remaining flits: 865404 865405 865406 865407 865408 865409 865410 865411 865412 865413 [...] (145718 flits)
Measured flits: 865404 865405 865406 865407 865408 865409 865410 865411 865412 865413 [...] (73136 flits)
Class 0:
Remaining flits: 892278 892279 892280 892281 892282 892283 892284 892285 892286 892287 [...] (145098 flits)
Measured flits: 892278 892279 892280 892281 892282 892283 892284 892285 892286 892287 [...] (52721 flits)
Class 0:
Remaining flits: 962772 962773 962774 962775 962776 962777 962778 962779 962780 962781 [...] (146309 flits)
Measured flits: 962772 962773 962774 962775 962776 962777 962778 962779 962780 962781 [...] (36435 flits)
Class 0:
Remaining flits: 1050048 1050049 1050050 1050051 1050052 1050053 1050054 1050055 1050056 1050057 [...] (146039 flits)
Measured flits: 1050048 1050049 1050050 1050051 1050052 1050053 1050054 1050055 1050056 1050057 [...] (21857 flits)
Class 0:
Remaining flits: 1081728 1081729 1081730 1081731 1081732 1081733 1081734 1081735 1081736 1081737 [...] (147260 flits)
Measured flits: 1102050 1102051 1102052 1102053 1102054 1102055 1102056 1102057 1102058 1102059 [...] (11299 flits)
Class 0:
Remaining flits: 1106208 1106209 1106210 1106211 1106212 1106213 1106214 1106215 1106216 1106217 [...] (149772 flits)
Measured flits: 1141452 1141453 1141454 1141455 1141456 1141457 1141458 1141459 1141460 1141461 [...] (5363 flits)
Class 0:
Remaining flits: 1218927 1218928 1218929 1218930 1218931 1218932 1218933 1218934 1218935 1218936 [...] (146655 flits)
Measured flits: 1240686 1240687 1240688 1240689 1240690 1240691 1240692 1240693 1240694 1240695 [...] (1689 flits)
Class 0:
Remaining flits: 1271448 1271449 1271450 1271451 1271452 1271453 1271454 1271455 1271456 1271457 [...] (145681 flits)
Measured flits: 1332936 1332937 1332938 1332939 1332940 1332941 1332942 1332943 1332944 1332945 [...] (502 flits)
Class 0:
Remaining flits: 1313253 1313254 1313255 1313256 1313257 1313258 1313259 1313260 1313261 1314270 [...] (147004 flits)
Measured flits: 1436001 1436002 1436003 1437840 1437841 1437842 1437843 1437844 1437845 1437846 [...] (93 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1380420 1380421 1380422 1380423 1380424 1380425 1380426 1380427 1380428 1380429 [...] (94818 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1380420 1380421 1380422 1380423 1380424 1380425 1380426 1380427 1380428 1380429 [...] (46387 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391976 1391977 1391978 1391979 1391980 1391981 1391982 1391983 1391984 1391985 [...] (6641 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1596853 1596854 1596855 1596856 1596857 1596858 1596859 1596860 1596861 1596862 [...] (196 flits)
Measured flits: (0 flits)
Time taken is 33750 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7969.71 (1 samples)
	minimum = 25 (1 samples)
	maximum = 19625 (1 samples)
Network latency average = 2836.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8407 (1 samples)
Flit latency average = 2778.08 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8441 (1 samples)
Fragmentation average = 19.8369 (1 samples)
	minimum = 0 (1 samples)
	maximum = 222 (1 samples)
Injected packet rate average = 0.0152173 (1 samples)
	minimum = 0.00971429 (1 samples)
	maximum = 0.0225714 (1 samples)
Accepted packet rate average = 0.014933 (1 samples)
	minimum = 0.0105714 (1 samples)
	maximum = 0.0181429 (1 samples)
Injected flit rate average = 0.273929 (1 samples)
	minimum = 0.174857 (1 samples)
	maximum = 0.406143 (1 samples)
Accepted flit rate average = 0.268796 (1 samples)
	minimum = 0.195 (1 samples)
	maximum = 0.327286 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 18.0001 (1 samples)
Hops average = 5.05035 (1 samples)
Total run time 32.4424
