BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.847
	minimum = 22
	maximum = 981
Network latency average = 236.899
	minimum = 22
	maximum = 820
Slowest packet = 46
Flit latency average = 205.553
	minimum = 5
	maximum = 803
Slowest flit = 8027
Fragmentation average = 47.9638
	minimum = 0
	maximum = 614
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139375
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26337
	minimum = 0.118 (at node 150)
	maximum = 0.432 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 18.8965
Total in-flight flits = 52267 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.551
	minimum = 22
	maximum = 1947
Network latency average = 447.509
	minimum = 22
	maximum = 1713
Slowest packet = 46
Flit latency average = 415.643
	minimum = 5
	maximum = 1706
Slowest flit = 25331
Fragmentation average = 73.3836
	minimum = 0
	maximum = 755
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.014737
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.276185
	minimum = 0.1655 (at node 164)
	maximum = 0.3965 (at node 152)
Injected packet length average = 17.9125
Accepted packet length average = 18.7409
Total in-flight flits = 110719 (0 measured)
latency change    = 0.427473
throughput change = 0.0464005
Class 0:
Packet latency average = 1350.49
	minimum = 25
	maximum = 2851
Network latency average = 1079.87
	minimum = 22
	maximum = 2376
Slowest packet = 2239
Flit latency average = 1040.17
	minimum = 5
	maximum = 2508
Slowest flit = 43803
Fragmentation average = 137.575
	minimum = 0
	maximum = 919
Injected packet rate average = 0.0345885
	minimum = 0 (at node 29)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0162083
	minimum = 0.007 (at node 190)
	maximum = 0.034 (at node 16)
Injected flit rate average = 0.623354
	minimum = 0 (at node 29)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.29574
	minimum = 0.126 (at node 113)
	maximum = 0.591 (at node 16)
Injected packet length average = 18.022
Accepted packet length average = 18.2461
Total in-flight flits = 173475 (0 measured)
latency change    = 0.544944
throughput change = 0.0661213
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 422.054
	minimum = 25
	maximum = 2039
Network latency average = 36.4162
	minimum = 22
	maximum = 107
Slowest packet = 18691
Flit latency average = 1462.5
	minimum = 5
	maximum = 3267
Slowest flit = 68912
Fragmentation average = 6.53299
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0359115
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0164844
	minimum = 0.007 (at node 5)
	maximum = 0.027 (at node 136)
Injected flit rate average = 0.645875
	minimum = 0 (at node 100)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.301057
	minimum = 0.121 (at node 5)
	maximum = 0.467 (at node 128)
Injected packet length average = 17.9852
Accepted packet length average = 18.2632
Total in-flight flits = 239782 (113314 measured)
latency change    = 2.19981
throughput change = 0.0176634
Class 0:
Packet latency average = 507.284
	minimum = 25
	maximum = 3058
Network latency average = 118.825
	minimum = 22
	maximum = 1981
Slowest packet = 18691
Flit latency average = 1694.51
	minimum = 5
	maximum = 4246
Slowest flit = 72839
Fragmentation average = 11.085
	minimum = 0
	maximum = 372
Injected packet rate average = 0.0352786
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0162656
	minimum = 0.009 (at node 5)
	maximum = 0.0245 (at node 128)
Injected flit rate average = 0.634745
	minimum = 0 (at node 100)
	maximum = 1 (at node 10)
Accepted flit rate average= 0.298245
	minimum = 0.1925 (at node 5)
	maximum = 0.4445 (at node 128)
Injected packet length average = 17.9923
Accepted packet length average = 18.3359
Total in-flight flits = 302795 (221303 measured)
latency change    = 0.168013
throughput change = 0.00943017
Class 0:
Packet latency average = 907.169
	minimum = 23
	maximum = 3884
Network latency average = 538.797
	minimum = 22
	maximum = 2963
Slowest packet = 18691
Flit latency average = 1933.76
	minimum = 5
	maximum = 5027
Slowest flit = 97682
Fragmentation average = 40.304
	minimum = 0
	maximum = 625
Injected packet rate average = 0.0347708
	minimum = 0.00566667 (at node 100)
	maximum = 0.0556667 (at node 31)
Accepted packet rate average = 0.016224
	minimum = 0.011 (at node 2)
	maximum = 0.0216667 (at node 136)
Injected flit rate average = 0.625705
	minimum = 0.102 (at node 100)
	maximum = 1 (at node 31)
Accepted flit rate average= 0.297352
	minimum = 0.203 (at node 171)
	maximum = 0.401 (at node 138)
Injected packet length average = 17.9951
Accepted packet length average = 18.328
Total in-flight flits = 362704 (318923 measured)
latency change    = 0.440805
throughput change = 0.00300102
Class 0:
Packet latency average = 1619.38
	minimum = 23
	maximum = 5570
Network latency average = 1281.03
	minimum = 22
	maximum = 3951
Slowest packet = 18691
Flit latency average = 2174.7
	minimum = 5
	maximum = 5714
Slowest flit = 133486
Fragmentation average = 94.6198
	minimum = 0
	maximum = 761
Injected packet rate average = 0.0344154
	minimum = 0.00775 (at node 100)
	maximum = 0.05575 (at node 39)
Accepted packet rate average = 0.0162513
	minimum = 0.01075 (at node 5)
	maximum = 0.0215 (at node 70)
Injected flit rate average = 0.619281
	minimum = 0.1395 (at node 100)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.297133
	minimum = 0.19975 (at node 5)
	maximum = 0.41425 (at node 138)
Injected packet length average = 17.9943
Accepted packet length average = 18.2836
Total in-flight flits = 421053 (399322 measured)
latency change    = 0.439806
throughput change = 0.000739124
Class 0:
Packet latency average = 2319.97
	minimum = 23
	maximum = 7073
Network latency average = 1981.25
	minimum = 22
	maximum = 4965
Slowest packet = 18691
Flit latency average = 2428.83
	minimum = 5
	maximum = 6533
Slowest flit = 145565
Fragmentation average = 147.769
	minimum = 0
	maximum = 940
Injected packet rate average = 0.0338177
	minimum = 0.016 (at node 147)
	maximum = 0.0556 (at node 39)
Accepted packet rate average = 0.0162437
	minimum = 0.0122 (at node 5)
	maximum = 0.0216 (at node 87)
Injected flit rate average = 0.608667
	minimum = 0.285 (at node 147)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.295939
	minimum = 0.2204 (at node 171)
	maximum = 0.386 (at node 138)
Injected packet length average = 17.9985
Accepted packet length average = 18.2186
Total in-flight flits = 473870 (463031 measured)
latency change    = 0.301982
throughput change = 0.00403554
Class 0:
Packet latency average = 2884.33
	minimum = 23
	maximum = 7490
Network latency average = 2531.08
	minimum = 22
	maximum = 5964
Slowest packet = 18691
Flit latency average = 2696.72
	minimum = 5
	maximum = 7325
Slowest flit = 178019
Fragmentation average = 182.487
	minimum = 0
	maximum = 1001
Injected packet rate average = 0.0335017
	minimum = 0.0148333 (at node 147)
	maximum = 0.0545 (at node 143)
Accepted packet rate average = 0.016158
	minimum = 0.0121667 (at node 63)
	maximum = 0.0215 (at node 138)
Injected flit rate average = 0.602871
	minimum = 0.267 (at node 147)
	maximum = 0.9795 (at node 143)
Accepted flit rate average= 0.294089
	minimum = 0.2235 (at node 63)
	maximum = 0.387333 (at node 138)
Injected packet length average = 17.9952
Accepted packet length average = 18.2009
Total in-flight flits = 529484 (525238 measured)
latency change    = 0.195664
throughput change = 0.00628765
Class 0:
Packet latency average = 3371.26
	minimum = 23
	maximum = 8507
Network latency average = 3001.16
	minimum = 22
	maximum = 6900
Slowest packet = 18691
Flit latency average = 2956.49
	minimum = 5
	maximum = 7770
Slowest flit = 218609
Fragmentation average = 207.117
	minimum = 0
	maximum = 1135
Injected packet rate average = 0.033218
	minimum = 0.0161429 (at node 154)
	maximum = 0.0521429 (at node 62)
Accepted packet rate average = 0.0161042
	minimum = 0.0122857 (at node 63)
	maximum = 0.0207143 (at node 138)
Injected flit rate average = 0.597745
	minimum = 0.290857 (at node 154)
	maximum = 0.937714 (at node 62)
Accepted flit rate average= 0.292594
	minimum = 0.228143 (at node 171)
	maximum = 0.374857 (at node 138)
Injected packet length average = 17.9946
Accepted packet length average = 18.1689
Total in-flight flits = 583982 (582691 measured)
latency change    = 0.144434
throughput change = 0.00510917
Draining all recorded packets ...
Class 0:
Remaining flits: 304574 304575 304576 304577 310426 310427 315479 315480 315481 315482 [...] (635266 flits)
Measured flits: 336723 336724 336725 337270 337271 337272 337273 337274 337275 337276 [...] (577902 flits)
Class 0:
Remaining flits: 318957 318958 318959 339592 339593 339594 339595 339596 339597 339598 [...] (677692 flits)
Measured flits: 339592 339593 339594 339595 339596 339597 339598 339599 339600 339601 [...] (542021 flits)
Class 0:
Remaining flits: 378535 378536 378537 378538 378539 380628 380629 380630 380631 380632 [...] (712677 flits)
Measured flits: 378535 378536 378537 378538 378539 380628 380629 380630 380631 380632 [...] (504662 flits)
Class 0:
Remaining flits: 390527 402588 402589 402590 402591 402592 402593 402594 402595 402596 [...] (742568 flits)
Measured flits: 390527 402588 402589 402590 402591 402592 402593 402594 402595 402596 [...] (464119 flits)
Class 0:
Remaining flits: 402599 402600 402601 402602 402603 402604 402605 434664 434665 434666 [...] (763310 flits)
Measured flits: 402599 402600 402601 402602 402603 402604 402605 434664 434665 434666 [...] (422667 flits)
Class 0:
Remaining flits: 444482 444483 444484 444485 444486 444487 444488 444489 444490 444491 [...] (783263 flits)
Measured flits: 444482 444483 444484 444485 444486 444487 444488 444489 444490 444491 [...] (381657 flits)
Class 0:
Remaining flits: 499824 499825 499826 499827 499828 499829 499830 499831 499832 499833 [...] (794576 flits)
Measured flits: 499824 499825 499826 499827 499828 499829 499830 499831 499832 499833 [...] (338021 flits)
Class 0:
Remaining flits: 503692 503693 507888 507889 507890 507891 507892 507893 507894 507895 [...] (805324 flits)
Measured flits: 503692 503693 507888 507889 507890 507891 507892 507893 507894 507895 [...] (297301 flits)
Class 0:
Remaining flits: 531098 531099 531100 531101 531102 531103 531104 531105 531106 531107 [...] (812865 flits)
Measured flits: 531098 531099 531100 531101 531102 531103 531104 531105 531106 531107 [...] (256917 flits)
Class 0:
Remaining flits: 570128 570129 570130 570131 606096 606097 606098 606099 606100 606101 [...] (814493 flits)
Measured flits: 570128 570129 570130 570131 606096 606097 606098 606099 606100 606101 [...] (220551 flits)
Class 0:
Remaining flits: 621000 621001 621002 621003 621004 621005 621006 621007 621008 621009 [...] (820583 flits)
Measured flits: 621000 621001 621002 621003 621004 621005 621006 621007 621008 621009 [...] (186766 flits)
Class 0:
Remaining flits: 621000 621001 621002 621003 621004 621005 621006 621007 621008 621009 [...] (830461 flits)
Measured flits: 621000 621001 621002 621003 621004 621005 621006 621007 621008 621009 [...] (155397 flits)
Class 0:
Remaining flits: 659628 659629 659630 659631 659632 659633 659634 659635 659636 659637 [...] (836765 flits)
Measured flits: 659628 659629 659630 659631 659632 659633 659634 659635 659636 659637 [...] (128130 flits)
Class 0:
Remaining flits: 659641 659642 659643 659644 659645 692514 692515 692516 692517 692518 [...] (839888 flits)
Measured flits: 659641 659642 659643 659644 659645 692514 692515 692516 692517 692518 [...] (104354 flits)
Class 0:
Remaining flits: 692525 692526 692527 692528 692529 692530 692531 727578 727579 727580 [...] (838116 flits)
Measured flits: 692525 692526 692527 692528 692529 692530 692531 727578 727579 727580 [...] (84843 flits)
Class 0:
Remaining flits: 728878 728879 728880 728881 728882 728883 728884 728885 728886 728887 [...] (838430 flits)
Measured flits: 728878 728879 728880 728881 728882 728883 728884 728885 728886 728887 [...] (67864 flits)
Class 0:
Remaining flits: 728890 728891 746550 746551 746552 746553 746554 746555 746556 746557 [...] (840535 flits)
Measured flits: 728890 728891 746550 746551 746552 746553 746554 746555 746556 746557 [...] (53473 flits)
Class 0:
Remaining flits: 746550 746551 746552 746553 746554 746555 746556 746557 746558 746559 [...] (843805 flits)
Measured flits: 746550 746551 746552 746553 746554 746555 746556 746557 746558 746559 [...] (42291 flits)
Class 0:
Remaining flits: 755262 755263 755264 755265 755266 755267 755268 755269 755270 755271 [...] (844842 flits)
Measured flits: 755262 755263 755264 755265 755266 755267 755268 755269 755270 755271 [...] (33480 flits)
Class 0:
Remaining flits: 755262 755263 755264 755265 755266 755267 755268 755269 755270 755271 [...] (849181 flits)
Measured flits: 755262 755263 755264 755265 755266 755267 755268 755269 755270 755271 [...] (26283 flits)
Class 0:
Remaining flits: 819792 819793 819794 819795 819796 819797 819798 819799 819800 819801 [...] (848772 flits)
Measured flits: 819792 819793 819794 819795 819796 819797 819798 819799 819800 819801 [...] (20657 flits)
Class 0:
Remaining flits: 822798 822799 822800 822801 822802 822803 822804 822805 822806 822807 [...] (848091 flits)
Measured flits: 822798 822799 822800 822801 822802 822803 822804 822805 822806 822807 [...] (16050 flits)
Class 0:
Remaining flits: 822809 822810 822811 822812 822813 822814 822815 854982 854983 854984 [...] (846670 flits)
Measured flits: 822809 822810 822811 822812 822813 822814 822815 854982 854983 854984 [...] (11854 flits)
Class 0:
Remaining flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (848996 flits)
Measured flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (8573 flits)
Class 0:
Remaining flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (852316 flits)
Measured flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (6427 flits)
Class 0:
Remaining flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (857382 flits)
Measured flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (4868 flits)
Class 0:
Remaining flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (862001 flits)
Measured flits: 921546 921547 921548 921549 921550 921551 921552 921553 921554 921555 [...] (3494 flits)
Class 0:
Remaining flits: 921557 921558 921559 921560 921561 921562 921563 953010 953011 953012 [...] (864426 flits)
Measured flits: 921557 921558 921559 921560 921561 921562 921563 953010 953011 953012 [...] (2169 flits)
Class 0:
Remaining flits: 953010 953011 953012 953013 953014 953015 953016 953017 953018 953019 [...] (860280 flits)
Measured flits: 953010 953011 953012 953013 953014 953015 953016 953017 953018 953019 [...] (1474 flits)
Class 0:
Remaining flits: 953015 953016 953017 953018 953019 953020 953021 953022 953023 953024 [...] (857689 flits)
Measured flits: 953015 953016 953017 953018 953019 953020 953021 953022 953023 953024 [...] (1070 flits)
Class 0:
Remaining flits: 1328382 1328383 1328384 1328385 1328386 1328387 1328388 1328389 1328390 1328391 [...] (858535 flits)
Measured flits: 1485666 1485667 1485668 1485669 1485670 1485671 1485672 1485673 1485674 1485675 [...] (708 flits)
Class 0:
Remaining flits: 1357884 1357885 1357886 1357887 1357888 1357889 1357890 1357891 1357892 1357893 [...] (860764 flits)
Measured flits: 2364876 2364877 2364878 2364879 2364880 2364881 2364882 2364883 2364884 2364885 [...] (322 flits)
Class 0:
Remaining flits: 1383732 1383733 1383734 1383735 1383736 1383737 1383738 1383739 1383740 1383741 [...] (862062 flits)
Measured flits: 2402287 2402288 2402289 2402290 2402291 2402292 2402293 2402294 2402295 2402296 [...] (170 flits)
Class 0:
Remaining flits: 1383732 1383733 1383734 1383735 1383736 1383737 1383738 1383739 1383740 1383741 [...] (861304 flits)
Measured flits: 2457347 2457348 2457349 2457350 2457351 2457352 2457353 2457354 2457355 2457356 [...] (67 flits)
Class 0:
Remaining flits: 1417968 1417969 1417970 1417971 1417972 1417973 1417974 1417975 1417976 1417977 [...] (860770 flits)
Measured flits: 2471241 2471242 2471243 2471244 2471245 2471246 2471247 2471248 2471249 2471250 [...] (33 flits)
Class 0:
Remaining flits: 1676916 1676917 1676918 1676919 1676920 1676921 1676922 1676923 1676924 1676925 [...] (859348 flits)
Measured flits: 2521800 2521801 2521802 2521803 2521804 2521805 2521806 2521807 2521808 2521809 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1714824 1714825 1714826 1714827 1714828 1714829 1714830 1714831 1714832 1714833 [...] (806163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1714824 1714825 1714826 1714827 1714828 1714829 1714830 1714831 1714832 1714833 [...] (755334 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1714824 1714825 1714826 1714827 1714828 1714829 1714830 1714831 1714832 1714833 [...] (704580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1714824 1714825 1714826 1714827 1714828 1714829 1714830 1714831 1714832 1714833 [...] (654072 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (604017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (554148 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (505179 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (456675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2123568 2123569 2123570 2123571 2123572 2123573 2123574 2123575 2123576 2123577 [...] (409102 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2146590 2146591 2146592 2146593 2146594 2146595 2146596 2146597 2146598 2146599 [...] (361465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2146590 2146591 2146592 2146593 2146594 2146595 2146596 2146597 2146598 2146599 [...] (313663 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2146590 2146591 2146592 2146593 2146594 2146595 2146596 2146597 2146598 2146599 [...] (266289 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2146590 2146591 2146592 2146593 2146594 2146595 2146596 2146597 2146598 2146599 [...] (218535 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2338056 2338057 2338058 2338059 2338060 2338061 2338062 2338063 2338064 2338065 [...] (170917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2427732 2427733 2427734 2427735 2427736 2427737 2427738 2427739 2427740 2427741 [...] (123104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2427732 2427733 2427734 2427735 2427736 2427737 2427738 2427739 2427740 2427741 [...] (75262 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2559492 2559493 2559494 2559495 2559496 2559497 2559498 2559499 2559500 2559501 [...] (29966 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2684349 2684350 2684351 2684352 2684353 2684354 2684355 2684356 2684357 2707902 [...] (3805 flits)
Measured flits: (0 flits)
Time taken is 65106 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9433.84 (1 samples)
	minimum = 23 (1 samples)
	maximum = 36648 (1 samples)
Network latency average = 8336.44 (1 samples)
	minimum = 22 (1 samples)
	maximum = 31781 (1 samples)
Flit latency average = 12731.9 (1 samples)
	minimum = 5 (1 samples)
	maximum = 36776 (1 samples)
Fragmentation average = 241.032 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1957 (1 samples)
Injected packet rate average = 0.033218 (1 samples)
	minimum = 0.0161429 (1 samples)
	maximum = 0.0521429 (1 samples)
Accepted packet rate average = 0.0161042 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0207143 (1 samples)
Injected flit rate average = 0.597745 (1 samples)
	minimum = 0.290857 (1 samples)
	maximum = 0.937714 (1 samples)
Accepted flit rate average = 0.292594 (1 samples)
	minimum = 0.228143 (1 samples)
	maximum = 0.374857 (1 samples)
Injected packet size average = 17.9946 (1 samples)
Accepted packet size average = 18.1689 (1 samples)
Hops average = 5.05676 (1 samples)
Total run time 127.841
