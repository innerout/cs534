BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 256.25
	minimum = 25
	maximum = 968
Network latency average = 176.327
	minimum = 22
	maximum = 777
Slowest packet = 72
Flit latency average = 146.866
	minimum = 5
	maximum = 787
Slowest flit = 14140
Fragmentation average = 36.5886
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0188229
	minimum = 0 (at node 138)
	maximum = 0.051 (at node 105)
Accepted packet rate average = 0.0125729
	minimum = 0.004 (at node 174)
	maximum = 0.021 (at node 132)
Injected flit rate average = 0.335615
	minimum = 0 (at node 138)
	maximum = 0.912 (at node 105)
Accepted flit rate average= 0.23524
	minimum = 0.09 (at node 115)
	maximum = 0.39 (at node 132)
Injected packet length average = 17.8301
Accepted packet length average = 18.71
Total in-flight flits = 19994 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 415.042
	minimum = 22
	maximum = 1723
Network latency average = 301.007
	minimum = 22
	maximum = 1395
Slowest packet = 72
Flit latency average = 265.219
	minimum = 5
	maximum = 1458
Slowest flit = 33949
Fragmentation average = 43.5642
	minimum = 0
	maximum = 291
Injected packet rate average = 0.0178307
	minimum = 0.003 (at node 58)
	maximum = 0.0435 (at node 181)
Accepted packet rate average = 0.0135052
	minimum = 0.0075 (at node 116)
	maximum = 0.019 (at node 78)
Injected flit rate average = 0.319315
	minimum = 0.0485 (at node 58)
	maximum = 0.7815 (at node 181)
Accepted flit rate average= 0.247612
	minimum = 0.142 (at node 116)
	maximum = 0.352 (at node 152)
Injected packet length average = 17.9081
Accepted packet length average = 18.3346
Total in-flight flits = 28433 (0 measured)
latency change    = 0.382594
throughput change = 0.0499669
Class 0:
Packet latency average = 784.128
	minimum = 22
	maximum = 2359
Network latency average = 556.113
	minimum = 22
	maximum = 1876
Slowest packet = 3971
Flit latency average = 513.564
	minimum = 5
	maximum = 1859
Slowest flit = 43197
Fragmentation average = 54.106
	minimum = 0
	maximum = 375
Injected packet rate average = 0.0173542
	minimum = 0 (at node 48)
	maximum = 0.049 (at node 51)
Accepted packet rate average = 0.0145938
	minimum = 0.007 (at node 7)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.311307
	minimum = 0.004 (at node 48)
	maximum = 0.88 (at node 51)
Accepted flit rate average= 0.264766
	minimum = 0.134 (at node 185)
	maximum = 0.554 (at node 16)
Injected packet length average = 17.9385
Accepted packet length average = 18.1424
Total in-flight flits = 37664 (0 measured)
latency change    = 0.470696
throughput change = 0.064788
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 746.685
	minimum = 26
	maximum = 3067
Network latency average = 407.642
	minimum = 22
	maximum = 971
Slowest packet = 10229
Flit latency average = 669.762
	minimum = 5
	maximum = 2236
Slowest flit = 85643
Fragmentation average = 42.9125
	minimum = 0
	maximum = 204
Injected packet rate average = 0.0161042
	minimum = 0 (at node 39)
	maximum = 0.041 (at node 151)
Accepted packet rate average = 0.014974
	minimum = 0.005 (at node 36)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.289604
	minimum = 0 (at node 39)
	maximum = 0.738 (at node 151)
Accepted flit rate average= 0.269083
	minimum = 0.075 (at node 48)
	maximum = 0.451 (at node 90)
Injected packet length average = 17.9832
Accepted packet length average = 17.9701
Total in-flight flits = 41836 (37879 measured)
latency change    = 0.0501464
throughput change = 0.016046
Class 0:
Packet latency average = 1114.03
	minimum = 26
	maximum = 3466
Network latency average = 649.134
	minimum = 22
	maximum = 1870
Slowest packet = 10229
Flit latency average = 706.693
	minimum = 5
	maximum = 2537
Slowest flit = 104507
Fragmentation average = 57.1135
	minimum = 0
	maximum = 266
Injected packet rate average = 0.0159193
	minimum = 0.003 (at node 38)
	maximum = 0.0285 (at node 151)
Accepted packet rate average = 0.0149844
	minimum = 0.0085 (at node 5)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.286367
	minimum = 0.054 (at node 38)
	maximum = 0.5095 (at node 151)
Accepted flit rate average= 0.269669
	minimum = 0.1545 (at node 36)
	maximum = 0.416 (at node 129)
Injected packet length average = 17.9887
Accepted packet length average = 17.9967
Total in-flight flits = 44361 (44226 measured)
latency change    = 0.329742
throughput change = 0.0021728
Class 0:
Packet latency average = 1316.65
	minimum = 23
	maximum = 4053
Network latency average = 752.3
	minimum = 22
	maximum = 2683
Slowest packet = 10229
Flit latency average = 746.796
	minimum = 5
	maximum = 2921
Slowest flit = 158615
Fragmentation average = 62.7913
	minimum = 0
	maximum = 292
Injected packet rate average = 0.0155399
	minimum = 0.00366667 (at node 86)
	maximum = 0.0253333 (at node 15)
Accepted packet rate average = 0.014934
	minimum = 0.008 (at node 36)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.27984
	minimum = 0.066 (at node 86)
	maximum = 0.453333 (at node 15)
Accepted flit rate average= 0.268766
	minimum = 0.140333 (at node 36)
	maximum = 0.368667 (at node 129)
Injected packet length average = 18.0078
Accepted packet length average = 17.9969
Total in-flight flits = 44315 (44315 measured)
latency change    = 0.153895
throughput change = 0.00336221
Draining remaining packets ...
Class 0:
Remaining flits: 270305 271746 271747 271748 271749 271750 271751 271752 271753 271754 [...] (3131 flits)
Measured flits: 270305 271746 271747 271748 271749 271750 271751 271752 271753 271754 [...] (3131 flits)
Time taken is 7579 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1514.86 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4969 (1 samples)
Network latency average = 846.368 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2977 (1 samples)
Flit latency average = 807.457 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2921 (1 samples)
Fragmentation average = 64.7514 (1 samples)
	minimum = 0 (1 samples)
	maximum = 300 (1 samples)
Injected packet rate average = 0.0155399 (1 samples)
	minimum = 0.00366667 (1 samples)
	maximum = 0.0253333 (1 samples)
Accepted packet rate average = 0.014934 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.27984 (1 samples)
	minimum = 0.066 (1 samples)
	maximum = 0.453333 (1 samples)
Accepted flit rate average = 0.268766 (1 samples)
	minimum = 0.140333 (1 samples)
	maximum = 0.368667 (1 samples)
Injected packet size average = 18.0078 (1 samples)
Accepted packet size average = 17.9969 (1 samples)
Hops average = 5.10691 (1 samples)
Total run time 9.68623
