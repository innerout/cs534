BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 238.991
	minimum = 23
	maximum = 905
Network latency average = 234.922
	minimum = 23
	maximum = 905
Slowest packet = 76
Flit latency average = 156.752
	minimum = 6
	maximum = 931
Slowest flit = 2950
Fragmentation average = 151.345
	minimum = 0
	maximum = 847
Injected packet rate average = 0.0176823
	minimum = 0.006 (at node 15)
	maximum = 0.027 (at node 51)
Accepted packet rate average = 0.00922917
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.315115
	minimum = 0.108 (at node 15)
	maximum = 0.486 (at node 51)
Accepted flit rate average= 0.195156
	minimum = 0.063 (at node 174)
	maximum = 0.391 (at node 44)
Injected packet length average = 17.8209
Accepted packet length average = 21.1456
Total in-flight flits = 23658 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 377.101
	minimum = 23
	maximum = 1810
Network latency average = 367.455
	minimum = 23
	maximum = 1798
Slowest packet = 363
Flit latency average = 266.641
	minimum = 6
	maximum = 1880
Slowest flit = 5946
Fragmentation average = 206.991
	minimum = 0
	maximum = 1622
Injected packet rate average = 0.0168802
	minimum = 0.006 (at node 40)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.010599
	minimum = 0.0045 (at node 178)
	maximum = 0.0155 (at node 67)
Injected flit rate average = 0.301789
	minimum = 0.1045 (at node 40)
	maximum = 0.459 (at node 135)
Accepted flit rate average= 0.207773
	minimum = 0.098 (at node 178)
	maximum = 0.3125 (at node 177)
Injected packet length average = 17.8783
Accepted packet length average = 19.6032
Total in-flight flits = 36981 (0 measured)
latency change    = 0.366242
throughput change = 0.0607257
Class 0:
Packet latency average = 712.531
	minimum = 25
	maximum = 2776
Network latency average = 658.666
	minimum = 25
	maximum = 2776
Slowest packet = 760
Flit latency average = 545.974
	minimum = 6
	maximum = 2807
Slowest flit = 10102
Fragmentation average = 268.559
	minimum = 1
	maximum = 2131
Injected packet rate average = 0.0139115
	minimum = 0 (at node 52)
	maximum = 0.03 (at node 131)
Accepted packet rate average = 0.0122917
	minimum = 0.004 (at node 60)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.248625
	minimum = 0 (at node 60)
	maximum = 0.542 (at node 131)
Accepted flit rate average= 0.220589
	minimum = 0.099 (at node 113)
	maximum = 0.403 (at node 16)
Injected packet length average = 17.872
Accepted packet length average = 17.9462
Total in-flight flits = 42724 (0 measured)
latency change    = 0.470758
throughput change = 0.0580951
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 565.07
	minimum = 27
	maximum = 2240
Network latency average = 308.673
	minimum = 27
	maximum = 953
Slowest packet = 9194
Flit latency average = 714.732
	minimum = 6
	maximum = 3756
Slowest flit = 14817
Fragmentation average = 157.491
	minimum = 2
	maximum = 815
Injected packet rate average = 0.0123073
	minimum = 0 (at node 4)
	maximum = 0.038 (at node 169)
Accepted packet rate average = 0.0121198
	minimum = 0.005 (at node 1)
	maximum = 0.023 (at node 81)
Injected flit rate average = 0.221599
	minimum = 0 (at node 4)
	maximum = 0.685 (at node 169)
Accepted flit rate average= 0.221005
	minimum = 0.095 (at node 1)
	maximum = 0.429 (at node 99)
Injected packet length average = 18.0055
Accepted packet length average = 18.2351
Total in-flight flits = 42933 (22798 measured)
latency change    = 0.26096
throughput change = 0.00188533
Class 0:
Packet latency average = 804.307
	minimum = 26
	maximum = 3367
Network latency average = 458.085
	minimum = 26
	maximum = 1952
Slowest packet = 9194
Flit latency average = 773.556
	minimum = 6
	maximum = 4851
Slowest flit = 7553
Fragmentation average = 191.413
	minimum = 1
	maximum = 1487
Injected packet rate average = 0.0126693
	minimum = 0 (at node 4)
	maximum = 0.0275 (at node 169)
Accepted packet rate average = 0.0120208
	minimum = 0.006 (at node 4)
	maximum = 0.02 (at node 107)
Injected flit rate average = 0.227992
	minimum = 0 (at node 49)
	maximum = 0.4955 (at node 169)
Accepted flit rate average= 0.217221
	minimum = 0.1185 (at node 4)
	maximum = 0.3455 (at node 99)
Injected packet length average = 17.9957
Accepted packet length average = 18.0704
Total in-flight flits = 47007 (35329 measured)
latency change    = 0.297444
throughput change = 0.0174193
Class 0:
Packet latency average = 1045.26
	minimum = 25
	maximum = 4204
Network latency average = 590.643
	minimum = 24
	maximum = 2918
Slowest packet = 9194
Flit latency average = 831.487
	minimum = 6
	maximum = 5727
Slowest flit = 13085
Fragmentation average = 211.443
	minimum = 1
	maximum = 1917
Injected packet rate average = 0.0126076
	minimum = 0 (at node 4)
	maximum = 0.0256667 (at node 169)
Accepted packet rate average = 0.0119375
	minimum = 0.008 (at node 135)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.226575
	minimum = 0 (at node 49)
	maximum = 0.460667 (at node 169)
Accepted flit rate average= 0.215231
	minimum = 0.137333 (at node 135)
	maximum = 0.325667 (at node 129)
Injected packet length average = 17.9712
Accepted packet length average = 18.0298
Total in-flight flits = 49593 (42229 measured)
latency change    = 0.230517
throughput change = 0.00924798
Draining remaining packets ...
Class 0:
Remaining flits: 10512 10513 10514 10515 10516 10517 10518 10519 10520 10521 [...] (16026 flits)
Measured flits: 166050 166051 166052 166053 166054 166055 166056 166057 166058 166059 [...] (12600 flits)
Class 0:
Remaining flits: 78588 78589 78590 78591 78592 78593 78594 78595 78596 78597 [...] (357 flits)
Measured flits: 170706 170707 170708 170709 170710 170711 181096 181097 184150 184151 [...] (336 flits)
Time taken is 8140 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1511.98 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5797 (1 samples)
Network latency average = 990.319 (1 samples)
	minimum = 24 (1 samples)
	maximum = 4886 (1 samples)
Flit latency average = 1191.51 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7295 (1 samples)
Fragmentation average = 230.885 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3311 (1 samples)
Injected packet rate average = 0.0126076 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0256667 (1 samples)
Accepted packet rate average = 0.0119375 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.226575 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.460667 (1 samples)
Accepted flit rate average = 0.215231 (1 samples)
	minimum = 0.137333 (1 samples)
	maximum = 0.325667 (1 samples)
Injected packet size average = 17.9712 (1 samples)
Accepted packet size average = 18.0298 (1 samples)
Hops average = 5.13303 (1 samples)
Total run time 9.89041
