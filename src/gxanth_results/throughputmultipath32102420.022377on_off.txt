BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 274.84
	minimum = 22
	maximum = 937
Network latency average = 196.988
	minimum = 22
	maximum = 784
Slowest packet = 69
Flit latency average = 166.613
	minimum = 5
	maximum = 767
Slowest flit = 12131
Fragmentation average = 36.1574
	minimum = 0
	maximum = 292
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.012776
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 48)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.238
	minimum = 0.076 (at node 174)
	maximum = 0.396 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6286
Total in-flight flits = 27078 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 444.344
	minimum = 22
	maximum = 1723
Network latency average = 339.585
	minimum = 22
	maximum = 1411
Slowest packet = 69
Flit latency average = 307.647
	minimum = 5
	maximum = 1486
Slowest flit = 33853
Fragmentation average = 47.9911
	minimum = 0
	maximum = 350
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0137578
	minimum = 0.0075 (at node 153)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253198
	minimum = 0.14 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 18.4039
Total in-flight flits = 53180 (0 measured)
latency change    = 0.381471
throughput change = 0.0600239
Class 0:
Packet latency average = 958.881
	minimum = 22
	maximum = 2570
Network latency average = 798.677
	minimum = 22
	maximum = 2021
Slowest packet = 3247
Flit latency average = 760.114
	minimum = 5
	maximum = 2071
Slowest flit = 64867
Fragmentation average = 67.985
	minimum = 0
	maximum = 404
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0152604
	minimum = 0.007 (at node 102)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.274276
	minimum = 0.11 (at node 102)
	maximum = 0.51 (at node 16)
Injected packet length average = 18
Accepted packet length average = 17.973
Total in-flight flits = 78423 (0 measured)
latency change    = 0.536602
throughput change = 0.07685
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 355.852
	minimum = 22
	maximum = 1325
Network latency average = 225.394
	minimum = 22
	maximum = 983
Slowest packet = 12702
Flit latency average = 1065.4
	minimum = 5
	maximum = 2693
Slowest flit = 94372
Fragmentation average = 18.8284
	minimum = 0
	maximum = 213
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0152656
	minimum = 0.008 (at node 22)
	maximum = 0.029 (at node 129)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.275651
	minimum = 0.137 (at node 89)
	maximum = 0.496 (at node 16)
Injected packet length average = 17.9946
Accepted packet length average = 18.057
Total in-flight flits = 101764 (67334 measured)
latency change    = 1.69461
throughput change = 0.00498819
Class 0:
Packet latency average = 837.356
	minimum = 22
	maximum = 2227
Network latency average = 717.472
	minimum = 22
	maximum = 1991
Slowest packet = 12702
Flit latency average = 1209.18
	minimum = 5
	maximum = 3248
Slowest flit = 128105
Fragmentation average = 34.7422
	minimum = 0
	maximum = 332
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0153047
	minimum = 0.0085 (at node 22)
	maximum = 0.0255 (at node 129)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.275742
	minimum = 0.1565 (at node 22)
	maximum = 0.4285 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0168
Total in-flight flits = 125448 (114947 measured)
latency change    = 0.575029
throughput change = 0.000330547
Class 0:
Packet latency average = 1253.63
	minimum = 22
	maximum = 3188
Network latency average = 1132.49
	minimum = 22
	maximum = 2975
Slowest packet = 12702
Flit latency average = 1365.13
	minimum = 5
	maximum = 3966
Slowest flit = 143585
Fragmentation average = 48.8801
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0152587
	minimum = 0.0103333 (at node 84)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.27522
	minimum = 0.192 (at node 84)
	maximum = 0.392667 (at node 138)
Injected packet length average = 18.0055
Accepted packet length average = 18.037
Total in-flight flits = 146948 (145629 measured)
latency change    = 0.332056
throughput change = 0.00189558
Draining remaining packets ...
Class 0:
Remaining flits: 200615 200616 200617 200618 200619 200620 200621 200622 200623 200624 [...] (99739 flits)
Measured flits: 230040 230041 230042 230043 230044 230045 230046 230047 230048 230049 [...] (99692 flits)
Class 0:
Remaining flits: 241740 241741 241742 241743 241744 241745 241746 241747 241748 241749 [...] (54176 flits)
Measured flits: 241740 241741 241742 241743 241744 241745 241746 241747 241748 241749 [...] (54176 flits)
Class 0:
Remaining flits: 303214 303215 303216 303217 303218 303219 303220 303221 303222 303223 [...] (17977 flits)
Measured flits: 303214 303215 303216 303217 303218 303219 303220 303221 303222 303223 [...] (17977 flits)
Class 0:
Remaining flits: 368468 368469 368470 368471 368472 368473 368474 368475 368476 368477 [...] (2595 flits)
Measured flits: 368468 368469 368470 368471 368472 368473 368474 368475 368476 368477 [...] (2595 flits)
Class 0:
Remaining flits: 453584 453585 453586 453587 453588 453589 453590 453591 453592 453593 [...] (16 flits)
Measured flits: 453584 453585 453586 453587 453588 453589 453590 453591 453592 453593 [...] (16 flits)
Time taken is 11018 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2402.97 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5777 (1 samples)
Network latency average = 2259.62 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5284 (1 samples)
Flit latency average = 2068.77 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5267 (1 samples)
Fragmentation average = 67.5984 (1 samples)
	minimum = 0 (1 samples)
	maximum = 554 (1 samples)
Injected packet rate average = 0.0218993 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0152587 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.394309 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.926 (1 samples)
Accepted flit rate average = 0.27522 (1 samples)
	minimum = 0.192 (1 samples)
	maximum = 0.392667 (1 samples)
Injected packet size average = 18.0055 (1 samples)
Accepted packet size average = 18.037 (1 samples)
Hops average = 5.08776 (1 samples)
Total run time 7.75583
