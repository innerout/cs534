BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.006907
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 84.648
	minimum = 22
	maximum = 323
Network latency average = 58.6399
	minimum = 22
	maximum = 252
Slowest packet = 7
Flit latency average = 36.009
	minimum = 5
	maximum = 235
Slowest flit = 14470
Fragmentation average = 10.6399
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00764583
	minimum = 0 (at node 3)
	maximum = 0.029 (at node 97)
Accepted packet rate average = 0.00714583
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 165)
Injected flit rate average = 0.136563
	minimum = 0 (at node 3)
	maximum = 0.522 (at node 97)
Accepted flit rate average= 0.130552
	minimum = 0.018 (at node 41)
	maximum = 0.27 (at node 165)
Injected packet length average = 17.861
Accepted packet length average = 18.2697
Total in-flight flits = 1358 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 88.2262
	minimum = 22
	maximum = 400
Network latency average = 61.4015
	minimum = 22
	maximum = 325
Slowest packet = 7
Flit latency average = 38.7414
	minimum = 5
	maximum = 308
Slowest flit = 23435
Fragmentation average = 10.4705
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0072474
	minimum = 0.0005 (at node 9)
	maximum = 0.019 (at node 161)
Accepted packet rate average = 0.00705729
	minimum = 0.0035 (at node 25)
	maximum = 0.0125 (at node 44)
Injected flit rate average = 0.129961
	minimum = 0.009 (at node 9)
	maximum = 0.342 (at node 161)
Accepted flit rate average= 0.127826
	minimum = 0.063 (at node 25)
	maximum = 0.231 (at node 44)
Injected packet length average = 17.9321
Accepted packet length average = 18.1125
Total in-flight flits = 1009 (0 measured)
latency change    = 0.0405576
throughput change = 0.0213303
Class 0:
Packet latency average = 90.7952
	minimum = 22
	maximum = 399
Network latency average = 63.833
	minimum = 22
	maximum = 281
Slowest packet = 2682
Flit latency average = 40.9387
	minimum = 5
	maximum = 264
Slowest flit = 54408
Fragmentation average = 11.2676
	minimum = 0
	maximum = 105
Injected packet rate average = 0.00692188
	minimum = 0 (at node 9)
	maximum = 0.033 (at node 20)
Accepted packet rate average = 0.00689063
	minimum = 0.001 (at node 23)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.124344
	minimum = 0 (at node 9)
	maximum = 0.586 (at node 20)
Accepted flit rate average= 0.124036
	minimum = 0.018 (at node 23)
	maximum = 0.259 (at node 22)
Injected packet length average = 17.9639
Accepted packet length average = 18.0008
Total in-flight flits = 1116 (0 measured)
latency change    = 0.0282941
throughput change = 0.030548
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 95.3502
	minimum = 22
	maximum = 494
Network latency average = 64.8478
	minimum = 22
	maximum = 248
Slowest packet = 4121
Flit latency average = 41.9252
	minimum = 5
	maximum = 231
Slowest flit = 85967
Fragmentation average = 11.5229
	minimum = 0
	maximum = 94
Injected packet rate average = 0.00702083
	minimum = 0 (at node 47)
	maximum = 0.031 (at node 79)
Accepted packet rate average = 0.00701563
	minimum = 0.001 (at node 121)
	maximum = 0.014 (at node 6)
Injected flit rate average = 0.126255
	minimum = 0 (at node 47)
	maximum = 0.57 (at node 79)
Accepted flit rate average= 0.126521
	minimum = 0.018 (at node 121)
	maximum = 0.242 (at node 6)
Injected packet length average = 17.9829
Accepted packet length average = 18.0341
Total in-flight flits = 1088 (1088 measured)
latency change    = 0.0477712
throughput change = 0.0196361
Class 0:
Packet latency average = 95.4074
	minimum = 22
	maximum = 582
Network latency average = 67.2641
	minimum = 22
	maximum = 438
Slowest packet = 4121
Flit latency average = 44.0363
	minimum = 5
	maximum = 421
Slowest flit = 105818
Fragmentation average = 12.1369
	minimum = 0
	maximum = 164
Injected packet rate average = 0.00723177
	minimum = 0.0005 (at node 143)
	maximum = 0.0265 (at node 79)
Accepted packet rate average = 0.0071276
	minimum = 0.003 (at node 184)
	maximum = 0.0125 (at node 56)
Injected flit rate average = 0.130234
	minimum = 0.009 (at node 143)
	maximum = 0.4765 (at node 79)
Accepted flit rate average= 0.128505
	minimum = 0.0475 (at node 184)
	maximum = 0.225 (at node 56)
Injected packet length average = 18.0086
Accepted packet length average = 18.0292
Total in-flight flits = 1756 (1756 measured)
latency change    = 0.000600493
throughput change = 0.015442
Class 0:
Packet latency average = 93.767
	minimum = 22
	maximum = 582
Network latency average = 66.0735
	minimum = 22
	maximum = 438
Slowest packet = 4121
Flit latency average = 42.9847
	minimum = 5
	maximum = 421
Slowest flit = 105818
Fragmentation average = 11.6011
	minimum = 0
	maximum = 164
Injected packet rate average = 0.00697917
	minimum = 0.000666667 (at node 143)
	maximum = 0.0213333 (at node 79)
Accepted packet rate average = 0.00701389
	minimum = 0.00366667 (at node 153)
	maximum = 0.0123333 (at node 34)
Injected flit rate average = 0.125767
	minimum = 0.012 (at node 143)
	maximum = 0.388 (at node 79)
Accepted flit rate average= 0.126033
	minimum = 0.066 (at node 153)
	maximum = 0.222 (at node 34)
Injected packet length average = 18.0204
Accepted packet length average = 17.9691
Total in-flight flits = 881 (881 measured)
latency change    = 0.0174952
throughput change = 0.0196157
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6486 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 94.2088 (1 samples)
	minimum = 22 (1 samples)
	maximum = 582 (1 samples)
Network latency average = 66.3017 (1 samples)
	minimum = 22 (1 samples)
	maximum = 438 (1 samples)
Flit latency average = 43.7616 (1 samples)
	minimum = 5 (1 samples)
	maximum = 421 (1 samples)
Fragmentation average = 11.5845 (1 samples)
	minimum = 0 (1 samples)
	maximum = 164 (1 samples)
Injected packet rate average = 0.00697917 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.0213333 (1 samples)
Accepted packet rate average = 0.00701389 (1 samples)
	minimum = 0.00366667 (1 samples)
	maximum = 0.0123333 (1 samples)
Injected flit rate average = 0.125767 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.388 (1 samples)
Accepted flit rate average = 0.126033 (1 samples)
	minimum = 0.066 (1 samples)
	maximum = 0.222 (1 samples)
Injected packet size average = 18.0204 (1 samples)
Accepted packet size average = 17.9691 (1 samples)
Hops average = 5.07035 (1 samples)
Total run time 2.58834
