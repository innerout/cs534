BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 410.052
	minimum = 23
	maximum = 916
Network latency average = 354.621
	minimum = 23
	maximum = 885
Slowest packet = 278
Flit latency average = 284.521
	minimum = 6
	maximum = 950
Slowest flit = 5227
Fragmentation average = 168.909
	minimum = 0
	maximum = 755
Injected packet rate average = 0.0224479
	minimum = 0.007 (at node 100)
	maximum = 0.036 (at node 155)
Accepted packet rate average = 0.0101094
	minimum = 0.004 (at node 93)
	maximum = 0.018 (at node 48)
Injected flit rate average = 0.39688
	minimum = 0.117 (at node 100)
	maximum = 0.641 (at node 155)
Accepted flit rate average= 0.208927
	minimum = 0.081 (at node 174)
	maximum = 0.344 (at node 152)
Injected packet length average = 17.68
Accepted packet length average = 20.6667
Total in-flight flits = 37754 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 802.547
	minimum = 23
	maximum = 1956
Network latency average = 595.778
	minimum = 23
	maximum = 1928
Slowest packet = 272
Flit latency average = 493.536
	minimum = 6
	maximum = 1928
Slowest flit = 6850
Fragmentation average = 196.819
	minimum = 0
	maximum = 1727
Injected packet rate average = 0.0164089
	minimum = 0.0045 (at node 48)
	maximum = 0.025 (at node 50)
Accepted packet rate average = 0.0105859
	minimum = 0.006 (at node 96)
	maximum = 0.0175 (at node 22)
Injected flit rate average = 0.291721
	minimum = 0.0805 (at node 48)
	maximum = 0.4465 (at node 50)
Accepted flit rate average= 0.202521
	minimum = 0.1175 (at node 153)
	maximum = 0.326 (at node 166)
Injected packet length average = 17.7783
Accepted packet length average = 19.1311
Total in-flight flits = 35974 (0 measured)
latency change    = 0.489063
throughput change = 0.0316325
Class 0:
Packet latency average = 1965.43
	minimum = 954
	maximum = 2930
Network latency average = 990.84
	minimum = 27
	maximum = 2903
Slowest packet = 696
Flit latency average = 846.31
	minimum = 6
	maximum = 2906
Slowest flit = 10418
Fragmentation average = 220.257
	minimum = 0
	maximum = 2458
Injected packet rate average = 0.0110469
	minimum = 0 (at node 2)
	maximum = 0.032 (at node 37)
Accepted packet rate average = 0.0109479
	minimum = 0.002 (at node 184)
	maximum = 0.022 (at node 63)
Injected flit rate average = 0.19974
	minimum = 0 (at node 41)
	maximum = 0.579 (at node 37)
Accepted flit rate average= 0.195078
	minimum = 0.033 (at node 184)
	maximum = 0.411 (at node 63)
Injected packet length average = 18.0811
Accepted packet length average = 17.8187
Total in-flight flits = 36769 (0 measured)
latency change    = 0.591669
throughput change = 0.0381524
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2789.89
	minimum = 1788
	maximum = 3553
Network latency average = 366.878
	minimum = 24
	maximum = 955
Slowest packet = 8461
Flit latency average = 898.951
	minimum = 6
	maximum = 3884
Slowest flit = 14550
Fragmentation average = 127.931
	minimum = 0
	maximum = 595
Injected packet rate average = 0.011
	minimum = 0 (at node 17)
	maximum = 0.027 (at node 79)
Accepted packet rate average = 0.0108542
	minimum = 0.002 (at node 48)
	maximum = 0.023 (at node 159)
Injected flit rate average = 0.198266
	minimum = 0 (at node 22)
	maximum = 0.5 (at node 79)
Accepted flit rate average= 0.198453
	minimum = 0.054 (at node 154)
	maximum = 0.359 (at node 159)
Injected packet length average = 18.0241
Accepted packet length average = 18.2836
Total in-flight flits = 36718 (25428 measured)
latency change    = 0.295516
throughput change = 0.0170065
Class 0:
Packet latency average = 3330.72
	minimum = 1788
	maximum = 4566
Network latency average = 618.788
	minimum = 24
	maximum = 1941
Slowest packet = 8461
Flit latency average = 915.406
	minimum = 6
	maximum = 4769
Slowest flit = 31933
Fragmentation average = 155.126
	minimum = 0
	maximum = 1209
Injected packet rate average = 0.0108125
	minimum = 0.0015 (at node 6)
	maximum = 0.022 (at node 27)
Accepted packet rate average = 0.0108672
	minimum = 0.006 (at node 138)
	maximum = 0.0175 (at node 19)
Injected flit rate average = 0.194685
	minimum = 0.0335 (at node 6)
	maximum = 0.394 (at node 27)
Accepted flit rate average= 0.195904
	minimum = 0.1115 (at node 17)
	maximum = 0.299 (at node 78)
Injected packet length average = 18.0055
Accepted packet length average = 18.0271
Total in-flight flits = 36386 (33076 measured)
latency change    = 0.162376
throughput change = 0.0130139
Class 0:
Packet latency average = 3801.86
	minimum = 1788
	maximum = 5382
Network latency average = 769.222
	minimum = 24
	maximum = 2845
Slowest packet = 8461
Flit latency average = 931.21
	minimum = 6
	maximum = 5242
Slowest flit = 42713
Fragmentation average = 168.561
	minimum = 0
	maximum = 2023
Injected packet rate average = 0.0108646
	minimum = 0.002 (at node 137)
	maximum = 0.0206667 (at node 66)
Accepted packet rate average = 0.0107812
	minimum = 0.007 (at node 79)
	maximum = 0.0153333 (at node 78)
Injected flit rate average = 0.19555
	minimum = 0.038 (at node 137)
	maximum = 0.372333 (at node 66)
Accepted flit rate average= 0.194401
	minimum = 0.123 (at node 4)
	maximum = 0.294667 (at node 178)
Injected packet length average = 17.9989
Accepted packet length average = 18.0314
Total in-flight flits = 37474 (36456 measured)
latency change    = 0.123923
throughput change = 0.0077294
Draining remaining packets ...
Class 0:
Remaining flits: 79512 79513 79514 79515 79516 79517 79518 79519 79520 79521 [...] (6718 flits)
Measured flits: 153198 153199 153200 153201 153202 153203 153204 153205 153206 153207 [...] (6486 flits)
Time taken is 7735 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4347.84 (1 samples)
	minimum = 1788 (1 samples)
	maximum = 7001 (1 samples)
Network latency average = 1005.43 (1 samples)
	minimum = 24 (1 samples)
	maximum = 4457 (1 samples)
Flit latency average = 1047.65 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5998 (1 samples)
Fragmentation average = 172.463 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2485 (1 samples)
Injected packet rate average = 0.0108646 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.0206667 (1 samples)
Accepted packet rate average = 0.0107812 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.19555 (1 samples)
	minimum = 0.038 (1 samples)
	maximum = 0.372333 (1 samples)
Accepted flit rate average = 0.194401 (1 samples)
	minimum = 0.123 (1 samples)
	maximum = 0.294667 (1 samples)
Injected packet size average = 17.9989 (1 samples)
Accepted packet size average = 18.0314 (1 samples)
Hops average = 5.13435 (1 samples)
Total run time 9.96011
