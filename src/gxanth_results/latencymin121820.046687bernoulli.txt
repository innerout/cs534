BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.932
	minimum = 25
	maximum = 905
Network latency average = 255.735
	minimum = 22
	maximum = 816
Slowest packet = 770
Flit latency average = 226.017
	minimum = 5
	maximum = 809
Slowest flit = 22420
Fragmentation average = 22.7886
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0186406
	minimum = 0.01 (at node 124)
	maximum = 0.028 (at node 70)
Accepted packet rate average = 0.013651
	minimum = 0.007 (at node 30)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.332104
	minimum = 0.178 (at node 148)
	maximum = 0.504 (at node 70)
Accepted flit rate average= 0.250536
	minimum = 0.126 (at node 30)
	maximum = 0.41 (at node 44)
Injected packet length average = 17.8161
Accepted packet length average = 18.3529
Total in-flight flits = 18335 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 692.564
	minimum = 25
	maximum = 1705
Network latency average = 311.063
	minimum = 22
	maximum = 1312
Slowest packet = 770
Flit latency average = 277.612
	minimum = 5
	maximum = 1283
Slowest flit = 29465
Fragmentation average = 22.8153
	minimum = 0
	maximum = 122
Injected packet rate average = 0.0161797
	minimum = 0.007 (at node 92)
	maximum = 0.0255 (at node 75)
Accepted packet rate average = 0.0137214
	minimum = 0.0075 (at node 153)
	maximum = 0.0195 (at node 98)
Injected flit rate average = 0.289409
	minimum = 0.126 (at node 92)
	maximum = 0.4535 (at node 75)
Accepted flit rate average= 0.249383
	minimum = 0.135 (at node 153)
	maximum = 0.351 (at node 98)
Injected packet length average = 17.8872
Accepted packet length average = 18.1748
Total in-flight flits = 18069 (0 measured)
latency change    = 0.488953
throughput change = 0.004626
Class 0:
Packet latency average = 1741.79
	minimum = 956
	maximum = 2647
Network latency average = 368.775
	minimum = 22
	maximum = 1808
Slowest packet = 4203
Flit latency average = 332.697
	minimum = 5
	maximum = 1791
Slowest flit = 66725
Fragmentation average = 22.5257
	minimum = 0
	maximum = 134
Injected packet rate average = 0.01375
	minimum = 0.002 (at node 2)
	maximum = 0.028 (at node 39)
Accepted packet rate average = 0.0136615
	minimum = 0.004 (at node 40)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.247875
	minimum = 0.036 (at node 2)
	maximum = 0.489 (at node 39)
Accepted flit rate average= 0.246417
	minimum = 0.073 (at node 40)
	maximum = 0.481 (at node 16)
Injected packet length average = 18.0273
Accepted packet length average = 18.0374
Total in-flight flits = 18295 (0 measured)
latency change    = 0.602383
throughput change = 0.0120371
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2526.48
	minimum = 1461
	maximum = 3404
Network latency average = 281.154
	minimum = 22
	maximum = 957
Slowest packet = 9015
Flit latency average = 329.828
	minimum = 5
	maximum = 1915
Slowest flit = 92428
Fragmentation average = 20.9771
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0141146
	minimum = 0.002 (at node 107)
	maximum = 0.028 (at node 122)
Accepted packet rate average = 0.0139896
	minimum = 0.006 (at node 85)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.25312
	minimum = 0.036 (at node 107)
	maximum = 0.499 (at node 122)
Accepted flit rate average= 0.251932
	minimum = 0.118 (at node 85)
	maximum = 0.465 (at node 123)
Injected packet length average = 17.9332
Accepted packet length average = 18.0086
Total in-flight flits = 18434 (18326 measured)
latency change    = 0.310587
throughput change = 0.0218933
Class 0:
Packet latency average = 2899.3
	minimum = 1461
	maximum = 4407
Network latency average = 329.618
	minimum = 22
	maximum = 1543
Slowest packet = 9015
Flit latency average = 327.215
	minimum = 5
	maximum = 1915
Slowest flit = 92428
Fragmentation average = 22.6983
	minimum = 0
	maximum = 126
Injected packet rate average = 0.014112
	minimum = 0.006 (at node 171)
	maximum = 0.022 (at node 5)
Accepted packet rate average = 0.013974
	minimum = 0.0085 (at node 4)
	maximum = 0.0205 (at node 38)
Injected flit rate average = 0.253495
	minimum = 0.1055 (at node 171)
	maximum = 0.3995 (at node 5)
Accepted flit rate average= 0.251659
	minimum = 0.153 (at node 86)
	maximum = 0.369 (at node 123)
Injected packet length average = 17.9631
Accepted packet length average = 18.0091
Total in-flight flits = 19020 (19020 measured)
latency change    = 0.128591
throughput change = 0.00108654
Class 0:
Packet latency average = 3260.01
	minimum = 1461
	maximum = 5080
Network latency average = 343.742
	minimum = 22
	maximum = 1950
Slowest packet = 9015
Flit latency average = 327.727
	minimum = 5
	maximum = 1915
Slowest flit = 92428
Fragmentation average = 23.2744
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140781
	minimum = 0.00766667 (at node 104)
	maximum = 0.0213333 (at node 146)
Accepted packet rate average = 0.0140174
	minimum = 0.00933333 (at node 36)
	maximum = 0.0193333 (at node 128)
Injected flit rate average = 0.253118
	minimum = 0.139 (at node 104)
	maximum = 0.383333 (at node 146)
Accepted flit rate average= 0.252484
	minimum = 0.168 (at node 36)
	maximum = 0.350333 (at node 128)
Injected packet length average = 17.9795
Accepted packet length average = 18.0123
Total in-flight flits = 18718 (18718 measured)
latency change    = 0.110645
throughput change = 0.00326959
Class 0:
Packet latency average = 3619.32
	minimum = 1461
	maximum = 5843
Network latency average = 349.673
	minimum = 22
	maximum = 1950
Slowest packet = 9015
Flit latency average = 327.305
	minimum = 5
	maximum = 1915
Slowest flit = 92428
Fragmentation average = 23.2258
	minimum = 0
	maximum = 126
Injected packet rate average = 0.014056
	minimum = 0.008 (at node 93)
	maximum = 0.02025 (at node 115)
Accepted packet rate average = 0.0140495
	minimum = 0.01 (at node 104)
	maximum = 0.01975 (at node 128)
Injected flit rate average = 0.252855
	minimum = 0.14425 (at node 93)
	maximum = 0.36375 (at node 115)
Accepted flit rate average= 0.253046
	minimum = 0.17875 (at node 104)
	maximum = 0.3555 (at node 128)
Injected packet length average = 17.9892
Accepted packet length average = 18.011
Total in-flight flits = 18284 (18284 measured)
latency change    = 0.0992762
throughput change = 0.00221777
Class 0:
Packet latency average = 3964.73
	minimum = 1461
	maximum = 6460
Network latency average = 351.996
	minimum = 22
	maximum = 1950
Slowest packet = 9015
Flit latency average = 327.04
	minimum = 5
	maximum = 1915
Slowest flit = 92428
Fragmentation average = 23.1337
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0139542
	minimum = 0.0074 (at node 93)
	maximum = 0.0198 (at node 115)
Accepted packet rate average = 0.0139813
	minimum = 0.0104 (at node 104)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.25117
	minimum = 0.1302 (at node 93)
	maximum = 0.355 (at node 10)
Accepted flit rate average= 0.251482
	minimum = 0.1862 (at node 104)
	maximum = 0.337 (at node 128)
Injected packet length average = 17.9996
Accepted packet length average = 17.9871
Total in-flight flits = 18054 (18054 measured)
latency change    = 0.0871222
throughput change = 0.00621627
Class 0:
Packet latency average = 4302.17
	minimum = 1461
	maximum = 7227
Network latency average = 354.445
	minimum = 22
	maximum = 1981
Slowest packet = 9015
Flit latency average = 327.678
	minimum = 5
	maximum = 1964
Slowest flit = 341693
Fragmentation average = 22.7109
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0138403
	minimum = 0.00716667 (at node 93)
	maximum = 0.0188333 (at node 115)
Accepted packet rate average = 0.0138568
	minimum = 0.011 (at node 171)
	maximum = 0.0176667 (at node 128)
Injected flit rate average = 0.249012
	minimum = 0.129167 (at node 93)
	maximum = 0.34 (at node 115)
Accepted flit rate average= 0.249329
	minimum = 0.194667 (at node 171)
	maximum = 0.32 (at node 128)
Injected packet length average = 17.9918
Accepted packet length average = 17.9933
Total in-flight flits = 18024 (18024 measured)
latency change    = 0.0784341
throughput change = 0.00863637
Class 0:
Packet latency average = 4655.01
	minimum = 1461
	maximum = 8049
Network latency average = 356.345
	minimum = 22
	maximum = 1981
Slowest packet = 9015
Flit latency average = 328.477
	minimum = 5
	maximum = 1964
Slowest flit = 341693
Fragmentation average = 22.4532
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0138177
	minimum = 0.00814286 (at node 93)
	maximum = 0.0187143 (at node 25)
Accepted packet rate average = 0.0138229
	minimum = 0.0111429 (at node 79)
	maximum = 0.0178571 (at node 138)
Injected flit rate average = 0.248577
	minimum = 0.145143 (at node 151)
	maximum = 0.335286 (at node 25)
Accepted flit rate average= 0.248708
	minimum = 0.198571 (at node 79)
	maximum = 0.321429 (at node 138)
Injected packet length average = 17.9897
Accepted packet length average = 17.9924
Total in-flight flits = 18076 (18076 measured)
latency change    = 0.0757981
throughput change = 0.00249853
Draining all recorded packets ...
Class 0:
Remaining flits: 486783 486784 486785 486786 486787 486788 486789 486790 486791 488642 [...] (17851 flits)
Measured flits: 486783 486784 486785 486786 486787 486788 486789 486790 486791 488642 [...] (17851 flits)
Class 0:
Remaining flits: 530676 530677 530678 530679 530680 530681 530682 530683 530684 530685 [...] (18177 flits)
Measured flits: 530676 530677 530678 530679 530680 530681 530682 530683 530684 530685 [...] (18177 flits)
Class 0:
Remaining flits: 544914 544915 544916 544917 544918 544919 544920 544921 544922 544923 [...] (18158 flits)
Measured flits: 544914 544915 544916 544917 544918 544919 544920 544921 544922 544923 [...] (18158 flits)
Class 0:
Remaining flits: 614048 614049 614050 614051 621531 621532 621533 621534 621535 621536 [...] (18831 flits)
Measured flits: 614048 614049 614050 614051 621531 621532 621533 621534 621535 621536 [...] (18831 flits)
Class 0:
Remaining flits: 659592 659593 659594 659595 659596 659597 659598 659599 659600 659601 [...] (18477 flits)
Measured flits: 659592 659593 659594 659595 659596 659597 659598 659599 659600 659601 [...] (18477 flits)
Class 0:
Remaining flits: 698670 698671 698672 698673 698674 698675 698676 698677 698678 698679 [...] (18376 flits)
Measured flits: 698670 698671 698672 698673 698674 698675 698676 698677 698678 698679 [...] (18376 flits)
Class 0:
Remaining flits: 765396 765397 765398 765399 765400 765401 765402 765403 765404 765405 [...] (18132 flits)
Measured flits: 765396 765397 765398 765399 765400 765401 765402 765403 765404 765405 [...] (18132 flits)
Class 0:
Remaining flits: 793386 793387 793388 793389 793390 793391 793392 793393 793394 793395 [...] (18629 flits)
Measured flits: 793386 793387 793388 793389 793390 793391 793392 793393 793394 793395 [...] (18629 flits)
Class 0:
Remaining flits: 853776 853777 853778 853779 853780 853781 853782 853783 853784 853785 [...] (18345 flits)
Measured flits: 853776 853777 853778 853779 853780 853781 853782 853783 853784 853785 [...] (18345 flits)
Class 0:
Remaining flits: 901098 901099 901100 901101 901102 901103 901104 901105 901106 901107 [...] (18341 flits)
Measured flits: 901098 901099 901100 901101 901102 901103 901104 901105 901106 901107 [...] (18341 flits)
Class 0:
Remaining flits: 967464 967465 967466 967467 967468 967469 967470 967471 967472 967473 [...] (18813 flits)
Measured flits: 967464 967465 967466 967467 967468 967469 967470 967471 967472 967473 [...] (18813 flits)
Class 0:
Remaining flits: 1019232 1019233 1019234 1019235 1019236 1019237 1019238 1019239 1019240 1019241 [...] (17943 flits)
Measured flits: 1019232 1019233 1019234 1019235 1019236 1019237 1019238 1019239 1019240 1019241 [...] (17943 flits)
Class 0:
Remaining flits: 1043820 1043821 1043822 1043823 1043824 1043825 1043826 1043827 1043828 1043829 [...] (18498 flits)
Measured flits: 1043820 1043821 1043822 1043823 1043824 1043825 1043826 1043827 1043828 1043829 [...] (18498 flits)
Class 0:
Remaining flits: 1112886 1112887 1112888 1112889 1112890 1112891 1112892 1112893 1112894 1112895 [...] (18316 flits)
Measured flits: 1112886 1112887 1112888 1112889 1112890 1112891 1112892 1112893 1112894 1112895 [...] (18316 flits)
Class 0:
Remaining flits: 1139966 1139967 1139968 1139969 1139970 1139971 1139972 1139973 1139974 1139975 [...] (18291 flits)
Measured flits: 1139966 1139967 1139968 1139969 1139970 1139971 1139972 1139973 1139974 1139975 [...] (18291 flits)
Class 0:
Remaining flits: 1208088 1208089 1208090 1208091 1208092 1208093 1208094 1208095 1208096 1208097 [...] (18388 flits)
Measured flits: 1208088 1208089 1208090 1208091 1208092 1208093 1208094 1208095 1208096 1208097 [...] (18388 flits)
Class 0:
Remaining flits: 1232514 1232515 1232516 1232517 1232518 1232519 1232520 1232521 1232522 1232523 [...] (18619 flits)
Measured flits: 1232514 1232515 1232516 1232517 1232518 1232519 1232520 1232521 1232522 1232523 [...] (18487 flits)
Class 0:
Remaining flits: 1284264 1284265 1284266 1284267 1284268 1284269 1284270 1284271 1284272 1284273 [...] (18402 flits)
Measured flits: 1284264 1284265 1284266 1284267 1284268 1284269 1284270 1284271 1284272 1284273 [...] (18189 flits)
Class 0:
Remaining flits: 1355112 1355113 1355114 1355115 1355116 1355117 1355118 1355119 1355120 1355121 [...] (18651 flits)
Measured flits: 1355112 1355113 1355114 1355115 1355116 1355117 1355118 1355119 1355120 1355121 [...] (17986 flits)
Class 0:
Remaining flits: 1374696 1374697 1374698 1374699 1374700 1374701 1374702 1374703 1374704 1374705 [...] (18062 flits)
Measured flits: 1374696 1374697 1374698 1374699 1374700 1374701 1374702 1374703 1374704 1374705 [...] (16662 flits)
Class 0:
Remaining flits: 1376586 1376587 1376588 1376589 1376590 1376591 1376592 1376593 1376594 1376595 [...] (18383 flits)
Measured flits: 1376586 1376587 1376588 1376589 1376590 1376591 1376592 1376593 1376594 1376595 [...] (15473 flits)
Class 0:
Remaining flits: 1483434 1483435 1483436 1483437 1483438 1483439 1483440 1483441 1483442 1483443 [...] (18375 flits)
Measured flits: 1483434 1483435 1483436 1483437 1483438 1483439 1483440 1483441 1483442 1483443 [...] (12917 flits)
Class 0:
Remaining flits: 1548846 1548847 1548848 1548849 1548850 1548851 1548852 1548853 1548854 1548855 [...] (18405 flits)
Measured flits: 1549610 1549611 1549612 1549613 1549614 1549615 1549616 1549617 1549618 1549619 [...] (10445 flits)
Class 0:
Remaining flits: 1586682 1586683 1586684 1586685 1586686 1586687 1586688 1586689 1586690 1586691 [...] (18238 flits)
Measured flits: 1599318 1599319 1599320 1599321 1599322 1599323 1599324 1599325 1599326 1599327 [...] (7657 flits)
Class 0:
Remaining flits: 1611882 1611883 1611884 1611885 1611886 1611887 1611888 1611889 1611890 1611891 [...] (18575 flits)
Measured flits: 1611882 1611883 1611884 1611885 1611886 1611887 1611888 1611889 1611890 1611891 [...] (5375 flits)
Class 0:
Remaining flits: 1660500 1660501 1660502 1660503 1660504 1660505 1660506 1660507 1660508 1660509 [...] (18565 flits)
Measured flits: 1674534 1674535 1674536 1674537 1674538 1674539 1684080 1684081 1684082 1684083 [...] (2899 flits)
Class 0:
Remaining flits: 1663117 1663118 1663119 1663120 1663121 1663122 1663123 1663124 1663125 1663126 [...] (18734 flits)
Measured flits: 1740968 1740969 1740970 1740971 1740972 1740973 1740974 1740975 1740976 1740977 [...] (1704 flits)
Class 0:
Remaining flits: 1747188 1747189 1747190 1747191 1747192 1747193 1747194 1747195 1747196 1747197 [...] (18101 flits)
Measured flits: 1795698 1795699 1795700 1795701 1795702 1795703 1795704 1795705 1795706 1795707 [...] (686 flits)
Class 0:
Remaining flits: 1779549 1779550 1779551 1792098 1792099 1792100 1792101 1792102 1792103 1792104 [...] (18521 flits)
Measured flits: 1843830 1843831 1843832 1843833 1843834 1843835 1843836 1843837 1843838 1843839 [...] (270 flits)
Class 0:
Remaining flits: 1812962 1812963 1812964 1812965 1812966 1812967 1812968 1812969 1812970 1812971 [...] (18386 flits)
Measured flits: 1903824 1903825 1903826 1903827 1903828 1903829 1903830 1903831 1903832 1903833 [...] (180 flits)
Class 0:
Remaining flits: 1905800 1905801 1905802 1905803 1912122 1912123 1912124 1912125 1912126 1912127 [...] (18338 flits)
Measured flits: 1970455 1970456 1970457 1970458 1970459 1978053 1978054 1978055 1979190 1979191 [...] (69 flits)
Draining remaining packets ...
Time taken is 42184 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13171 (1 samples)
	minimum = 1461 (1 samples)
	maximum = 31557 (1 samples)
Network latency average = 364.206 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2169 (1 samples)
Flit latency average = 329.445 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2831 (1 samples)
Fragmentation average = 22.773 (1 samples)
	minimum = 0 (1 samples)
	maximum = 143 (1 samples)
Injected packet rate average = 0.0138177 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0187143 (1 samples)
Accepted packet rate average = 0.0138229 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.248577 (1 samples)
	minimum = 0.145143 (1 samples)
	maximum = 0.335286 (1 samples)
Accepted flit rate average = 0.248708 (1 samples)
	minimum = 0.198571 (1 samples)
	maximum = 0.321429 (1 samples)
Injected packet size average = 17.9897 (1 samples)
Accepted packet size average = 17.9924 (1 samples)
Hops average = 5.06511 (1 samples)
Total run time 37.4002
