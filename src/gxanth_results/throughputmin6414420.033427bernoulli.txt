BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 282.593
	minimum = 22
	maximum = 774
Network latency average = 273.143
	minimum = 22
	maximum = 753
Slowest packet = 1282
Flit latency average = 243.115
	minimum = 5
	maximum = 766
Slowest flit = 24220
Fragmentation average = 55.8417
	minimum = 0
	maximum = 515
Injected packet rate average = 0.0329635
	minimum = 0.017 (at node 97)
	maximum = 0.048 (at node 118)
Accepted packet rate average = 0.0142813
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.587724
	minimum = 0.306 (at node 97)
	maximum = 0.862 (at node 118)
Accepted flit rate average= 0.272724
	minimum = 0.104 (at node 174)
	maximum = 0.443 (at node 44)
Injected packet length average = 17.8295
Accepted packet length average = 19.0966
Total in-flight flits = 61559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 526.286
	minimum = 22
	maximum = 1690
Network latency average = 514.768
	minimum = 22
	maximum = 1606
Slowest packet = 2543
Flit latency average = 477.432
	minimum = 5
	maximum = 1589
Slowest flit = 39599
Fragmentation average = 83.8557
	minimum = 0
	maximum = 784
Injected packet rate average = 0.03325
	minimum = 0.0205 (at node 62)
	maximum = 0.043 (at node 117)
Accepted packet rate average = 0.015138
	minimum = 0.0085 (at node 116)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.596021
	minimum = 0.369 (at node 62)
	maximum = 0.771 (at node 118)
Accepted flit rate average= 0.283828
	minimum = 0.1555 (at node 153)
	maximum = 0.4145 (at node 152)
Injected packet length average = 17.9254
Accepted packet length average = 18.7494
Total in-flight flits = 120834 (0 measured)
latency change    = 0.463043
throughput change = 0.0391229
Class 0:
Packet latency average = 1217.06
	minimum = 23
	maximum = 2308
Network latency average = 1203.5
	minimum = 22
	maximum = 2196
Slowest packet = 3613
Flit latency average = 1159.99
	minimum = 5
	maximum = 2395
Slowest flit = 67161
Fragmentation average = 150.281
	minimum = 0
	maximum = 808
Injected packet rate average = 0.0330729
	minimum = 0.016 (at node 111)
	maximum = 0.051 (at node 17)
Accepted packet rate average = 0.0159896
	minimum = 0.006 (at node 180)
	maximum = 0.026 (at node 159)
Injected flit rate average = 0.59449
	minimum = 0.283 (at node 111)
	maximum = 0.923 (at node 17)
Accepted flit rate average= 0.291766
	minimum = 0.121 (at node 180)
	maximum = 0.5 (at node 159)
Injected packet length average = 17.9751
Accepted packet length average = 18.2472
Total in-flight flits = 179115 (0 measured)
latency change    = 0.567576
throughput change = 0.0272051
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 49.1339
	minimum = 24
	maximum = 169
Network latency average = 35.4202
	minimum = 22
	maximum = 70
Slowest packet = 19137
Flit latency average = 1609.46
	minimum = 5
	maximum = 3209
Slowest flit = 87682
Fragmentation average = 6.67706
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0338646
	minimum = 0.02 (at node 44)
	maximum = 0.05 (at node 146)
Accepted packet rate average = 0.0162292
	minimum = 0.007 (at node 134)
	maximum = 0.03 (at node 90)
Injected flit rate average = 0.609292
	minimum = 0.365 (at node 111)
	maximum = 0.896 (at node 146)
Accepted flit rate average= 0.29625
	minimum = 0.13 (at node 53)
	maximum = 0.524 (at node 90)
Injected packet length average = 17.992
Accepted packet length average = 18.2542
Total in-flight flits = 239271 (107069 measured)
latency change    = 23.7703
throughput change = 0.0151371
Class 0:
Packet latency average = 48.3933
	minimum = 22
	maximum = 268
Network latency average = 35.309
	minimum = 22
	maximum = 116
Slowest packet = 19137
Flit latency average = 1846.98
	minimum = 5
	maximum = 4049
Slowest flit = 106281
Fragmentation average = 6.45037
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0334427
	minimum = 0.023 (at node 74)
	maximum = 0.043 (at node 169)
Accepted packet rate average = 0.016151
	minimum = 0.01 (at node 80)
	maximum = 0.023 (at node 19)
Injected flit rate average = 0.602279
	minimum = 0.414 (at node 74)
	maximum = 0.7775 (at node 169)
Accepted flit rate average= 0.294323
	minimum = 0.1915 (at node 135)
	maximum = 0.4185 (at node 0)
Injected packet length average = 18.0093
Accepted packet length average = 18.2232
Total in-flight flits = 297251 (211805 measured)
latency change    = 0.0153056
throughput change = 0.00654751
Class 0:
Packet latency average = 89.1243
	minimum = 22
	maximum = 2969
Network latency average = 75.4466
	minimum = 22
	maximum = 2969
Slowest packet = 19229
Flit latency average = 2067.01
	minimum = 5
	maximum = 4819
Slowest flit = 132010
Fragmentation average = 7.52867
	minimum = 0
	maximum = 279
Injected packet rate average = 0.0333802
	minimum = 0.0253333 (at node 20)
	maximum = 0.043 (at node 89)
Accepted packet rate average = 0.016158
	minimum = 0.0103333 (at node 80)
	maximum = 0.0213333 (at node 0)
Injected flit rate average = 0.601309
	minimum = 0.461333 (at node 20)
	maximum = 0.776667 (at node 89)
Accepted flit rate average= 0.294997
	minimum = 0.194333 (at node 80)
	maximum = 0.388667 (at node 0)
Injected packet length average = 18.0139
Accepted packet length average = 18.257
Total in-flight flits = 355283 (315942 measured)
latency change    = 0.457014
throughput change = 0.00228345
Draining remaining packets ...
Class 0:
Remaining flits: 174345 174346 174347 184239 184240 184241 184242 184243 184244 184245 [...] (308407 flits)
Measured flits: 344214 344215 344216 344217 344218 344219 344220 344221 344222 344223 [...] (296178 flits)
Class 0:
Remaining flits: 213173 224528 224529 224530 224531 237685 237686 237687 237688 237689 [...] (261500 flits)
Measured flits: 344324 344325 344326 344327 344328 344329 344330 344331 344332 344333 [...] (257899 flits)
Class 0:
Remaining flits: 249726 249727 249728 249729 249730 249731 251740 251741 251742 251743 [...] (213916 flits)
Measured flits: 344340 344341 344342 344343 344344 344345 344346 344347 344348 344349 [...] (212925 flits)
Class 0:
Remaining flits: 297985 297986 297987 297988 297989 299682 299683 299684 299685 299686 [...] (166781 flits)
Measured flits: 344340 344341 344342 344343 344344 344345 344346 344347 344348 344349 [...] (166561 flits)
Class 0:
Remaining flits: 301860 301861 301862 301863 301864 301865 301866 301867 301868 301869 [...] (119532 flits)
Measured flits: 357264 357265 357266 357267 357268 357269 357270 357271 357272 357273 [...] (119507 flits)
Class 0:
Remaining flits: 357264 357265 357266 357267 357268 357269 357270 357271 357272 357273 [...] (72209 flits)
Measured flits: 357264 357265 357266 357267 357268 357269 357270 357271 357272 357273 [...] (72209 flits)
Class 0:
Remaining flits: 357269 357270 357271 357272 357273 357274 357275 357276 357277 357278 [...] (26425 flits)
Measured flits: 357269 357270 357271 357272 357273 357274 357275 357276 357277 357278 [...] (26425 flits)
Class 0:
Remaining flits: 479472 479473 479474 479475 479476 479477 479478 479479 479480 479481 [...] (1689 flits)
Measured flits: 479472 479473 479474 479475 479476 479477 479478 479479 479480 479481 [...] (1689 flits)
Time taken is 14386 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5316.2 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9983 (1 samples)
Network latency average = 5302.02 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9965 (1 samples)
Flit latency average = 4373.6 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9948 (1 samples)
Fragmentation average = 248.651 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1448 (1 samples)
Injected packet rate average = 0.0333802 (1 samples)
	minimum = 0.0253333 (1 samples)
	maximum = 0.043 (1 samples)
Accepted packet rate average = 0.016158 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.601309 (1 samples)
	minimum = 0.461333 (1 samples)
	maximum = 0.776667 (1 samples)
Accepted flit rate average = 0.294997 (1 samples)
	minimum = 0.194333 (1 samples)
	maximum = 0.388667 (1 samples)
Injected packet size average = 18.0139 (1 samples)
Accepted packet size average = 18.257 (1 samples)
Hops average = 5.06819 (1 samples)
Total run time 15.6661
