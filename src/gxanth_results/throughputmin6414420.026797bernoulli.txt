BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 256.65
	minimum = 22
	maximum = 806
Network latency average = 250.065
	minimum = 22
	maximum = 760
Slowest packet = 1143
Flit latency average = 218.791
	minimum = 5
	maximum = 743
Slowest flit = 20591
Fragmentation average = 44.0753
	minimum = 0
	maximum = 474
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0139115
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.260104
	minimum = 0.126 (at node 174)
	maximum = 0.443 (at node 131)
Injected packet length average = 17.8601
Accepted packet length average = 18.6971
Total in-flight flits = 42670 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.877
	minimum = 22
	maximum = 1353
Network latency average = 457.415
	minimum = 22
	maximum = 1340
Slowest packet = 1918
Flit latency average = 425.167
	minimum = 5
	maximum = 1323
Slowest flit = 59453
Fragmentation average = 52.6204
	minimum = 0
	maximum = 652
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0145781
	minimum = 0.0085 (at node 30)
	maximum = 0.0205 (at node 14)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.268758
	minimum = 0.162 (at node 83)
	maximum = 0.38 (at node 14)
Injected packet length average = 17.9276
Accepted packet length average = 18.4357
Total in-flight flits = 81225 (0 measured)
latency change    = 0.447918
throughput change = 0.0321987
Class 0:
Packet latency average = 1042.48
	minimum = 22
	maximum = 2051
Network latency average = 1034.58
	minimum = 22
	maximum = 2029
Slowest packet = 4514
Flit latency average = 1003.78
	minimum = 5
	maximum = 2012
Slowest flit = 81269
Fragmentation average = 72.017
	minimum = 0
	maximum = 755
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0156094
	minimum = 0.006 (at node 154)
	maximum = 0.031 (at node 16)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.282208
	minimum = 0.132 (at node 146)
	maximum = 0.553 (at node 16)
Injected packet length average = 17.9863
Accepted packet length average = 18.0794
Total in-flight flits = 119327 (0 measured)
latency change    = 0.554067
throughput change = 0.0476617
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 42.8067
	minimum = 22
	maximum = 114
Network latency average = 34.4177
	minimum = 22
	maximum = 60
Slowest packet = 15419
Flit latency average = 1390.17
	minimum = 5
	maximum = 2526
Slowest flit = 115595
Fragmentation average = 6.08115
	minimum = 0
	maximum = 26
Injected packet rate average = 0.0271719
	minimum = 0.013 (at node 159)
	maximum = 0.046 (at node 26)
Accepted packet rate average = 0.0159896
	minimum = 0.007 (at node 4)
	maximum = 0.029 (at node 90)
Injected flit rate average = 0.488312
	minimum = 0.25 (at node 159)
	maximum = 0.822 (at node 26)
Accepted flit rate average= 0.287682
	minimum = 0.129 (at node 4)
	maximum = 0.553 (at node 123)
Injected packet length average = 17.9712
Accepted packet length average = 17.9919
Total in-flight flits = 157998 (86240 measured)
latency change    = 23.3532
throughput change = 0.0190278
Class 0:
Packet latency average = 127.307
	minimum = 22
	maximum = 1980
Network latency average = 119.19
	minimum = 22
	maximum = 1980
Slowest packet = 15377
Flit latency average = 1576.33
	minimum = 5
	maximum = 2993
Slowest flit = 184002
Fragmentation average = 8.85861
	minimum = 0
	maximum = 219
Injected packet rate average = 0.02675
	minimum = 0.0185 (at node 28)
	maximum = 0.038 (at node 87)
Accepted packet rate average = 0.0158568
	minimum = 0.008 (at node 79)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.481448
	minimum = 0.333 (at node 28)
	maximum = 0.692 (at node 87)
Accepted flit rate average= 0.284875
	minimum = 0.1525 (at node 79)
	maximum = 0.424 (at node 129)
Injected packet length average = 17.9981
Accepted packet length average = 17.9655
Total in-flight flits = 194831 (168839 measured)
latency change    = 0.663752
throughput change = 0.00985447
Class 0:
Packet latency average = 1233.77
	minimum = 22
	maximum = 2968
Network latency average = 1225.89
	minimum = 22
	maximum = 2965
Slowest packet = 15476
Flit latency average = 1765.51
	minimum = 5
	maximum = 3826
Slowest flit = 198730
Fragmentation average = 39.6082
	minimum = 0
	maximum = 572
Injected packet rate average = 0.0267986
	minimum = 0.0193333 (at node 28)
	maximum = 0.034 (at node 26)
Accepted packet rate average = 0.015901
	minimum = 0.011 (at node 36)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.48247
	minimum = 0.348 (at node 28)
	maximum = 0.611667 (at node 26)
Accepted flit rate average= 0.285979
	minimum = 0.2 (at node 36)
	maximum = 0.384667 (at node 129)
Injected packet length average = 18.0036
Accepted packet length average = 17.9849
Total in-flight flits = 232451 (229984 measured)
latency change    = 0.896815
throughput change = 0.003861
Draining remaining packets ...
Class 0:
Remaining flits: 217087 217088 217089 217090 217091 217092 217093 217094 217095 217096 [...] (184961 flits)
Measured flits: 278334 278335 278336 278337 278338 278339 278340 278341 278342 278343 [...] (184667 flits)
Class 0:
Remaining flits: 244133 249570 249571 249572 249573 249574 249575 249576 249577 249578 [...] (137648 flits)
Measured flits: 282418 282419 285102 285103 285104 285105 285106 285107 285108 285109 [...] (137593 flits)
Class 0:
Remaining flits: 249579 249580 249581 249582 249583 249584 249585 249586 249587 294264 [...] (90700 flits)
Measured flits: 294264 294265 294266 294267 294268 294269 294270 294271 294272 294273 [...] (90691 flits)
Class 0:
Remaining flits: 301410 301411 301412 301413 301414 301415 301416 301417 301418 301419 [...] (43541 flits)
Measured flits: 301410 301411 301412 301413 301414 301415 301416 301417 301418 301419 [...] (43541 flits)
Class 0:
Remaining flits: 363042 363043 363044 363045 363046 363047 363048 363049 363050 363051 [...] (5187 flits)
Measured flits: 363042 363043 363044 363045 363046 363047 363048 363049 363050 363051 [...] (5187 flits)
Time taken is 11780 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3464.91 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7556 (1 samples)
Network latency average = 3456.83 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7556 (1 samples)
Flit latency average = 2982.2 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7539 (1 samples)
Fragmentation average = 80.9439 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1213 (1 samples)
Injected packet rate average = 0.0267986 (1 samples)
	minimum = 0.0193333 (1 samples)
	maximum = 0.034 (1 samples)
Accepted packet rate average = 0.015901 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.48247 (1 samples)
	minimum = 0.348 (1 samples)
	maximum = 0.611667 (1 samples)
Accepted flit rate average = 0.285979 (1 samples)
	minimum = 0.2 (1 samples)
	maximum = 0.384667 (1 samples)
Injected packet size average = 18.0036 (1 samples)
Accepted packet size average = 17.9849 (1 samples)
Hops average = 5.0837 (1 samples)
Total run time 11.1338
