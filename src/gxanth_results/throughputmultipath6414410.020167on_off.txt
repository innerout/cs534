BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.715
	minimum = 23
	maximum = 973
Network latency average = 251.708
	minimum = 23
	maximum = 889
Slowest packet = 14
Flit latency average = 177.495
	minimum = 6
	maximum = 980
Slowest flit = 67
Fragmentation average = 147.128
	minimum = 0
	maximum = 819
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.00925
	minimum = 0.003 (at node 43)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.192974
	minimum = 0.07 (at node 43)
	maximum = 0.337 (at node 103)
Injected packet length average = 17.8591
Accepted packet length average = 20.862
Total in-flight flits = 30287 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 500.544
	minimum = 23
	maximum = 1895
Network latency average = 412.474
	minimum = 23
	maximum = 1861
Slowest packet = 164
Flit latency average = 317.728
	minimum = 6
	maximum = 1922
Slowest flit = 4462
Fragmentation average = 199.042
	minimum = 0
	maximum = 1429
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0105964
	minimum = 0.0055 (at node 30)
	maximum = 0.0165 (at node 103)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.208365
	minimum = 0.12 (at node 135)
	maximum = 0.3085 (at node 103)
Injected packet length average = 17.9339
Accepted packet length average = 19.6638
Total in-flight flits = 54430 (0 measured)
latency change    = 0.35727
throughput change = 0.0738639
Class 0:
Packet latency average = 922.979
	minimum = 24
	maximum = 2887
Network latency average = 808.367
	minimum = 23
	maximum = 2814
Slowest packet = 504
Flit latency average = 702.218
	minimum = 6
	maximum = 2814
Slowest flit = 12018
Fragmentation average = 297.077
	minimum = 0
	maximum = 2339
Injected packet rate average = 0.0198594
	minimum = 0 (at node 101)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0124271
	minimum = 0.002 (at node 146)
	maximum = 0.026 (at node 34)
Injected flit rate average = 0.356953
	minimum = 0 (at node 101)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.231396
	minimum = 0.093 (at node 146)
	maximum = 0.483 (at node 78)
Injected packet length average = 17.974
Accepted packet length average = 18.6203
Total in-flight flits = 78636 (0 measured)
latency change    = 0.457686
throughput change = 0.0995318
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 371.978
	minimum = 26
	maximum = 1342
Network latency average = 262.907
	minimum = 25
	maximum = 975
Slowest packet = 11282
Flit latency average = 997.326
	minimum = 6
	maximum = 3886
Slowest flit = 7125
Fragmentation average = 130.69
	minimum = 0
	maximum = 721
Injected packet rate average = 0.0217969
	minimum = 0 (at node 45)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0131354
	minimum = 0.002 (at node 132)
	maximum = 0.028 (at node 51)
Injected flit rate average = 0.392135
	minimum = 0 (at node 45)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.239891
	minimum = 0.057 (at node 132)
	maximum = 0.512 (at node 51)
Injected packet length average = 17.9904
Accepted packet length average = 18.2629
Total in-flight flits = 107907 (60656 measured)
latency change    = 1.48127
throughput change = 0.0354111
Class 0:
Packet latency average = 683.094
	minimum = 26
	maximum = 2474
Network latency average = 571.978
	minimum = 25
	maximum = 1915
Slowest packet = 11282
Flit latency average = 1149.92
	minimum = 6
	maximum = 4675
Slowest flit = 11357
Fragmentation average = 212.794
	minimum = 0
	maximum = 1425
Injected packet rate average = 0.0209297
	minimum = 0.003 (at node 144)
	maximum = 0.0505 (at node 30)
Accepted packet rate average = 0.0131745
	minimum = 0.0045 (at node 132)
	maximum = 0.0205 (at node 51)
Injected flit rate average = 0.3765
	minimum = 0.054 (at node 144)
	maximum = 0.909 (at node 30)
Accepted flit rate average= 0.238638
	minimum = 0.1075 (at node 132)
	maximum = 0.372 (at node 51)
Injected packet length average = 17.9888
Accepted packet length average = 18.1137
Total in-flight flits = 131665 (102054 measured)
latency change    = 0.45545
throughput change = 0.00524897
Class 0:
Packet latency average = 965.179
	minimum = 26
	maximum = 3213
Network latency average = 851.548
	minimum = 25
	maximum = 2983
Slowest packet = 11282
Flit latency average = 1301.17
	minimum = 6
	maximum = 5703
Slowest flit = 14219
Fragmentation average = 251.033
	minimum = 0
	maximum = 2587
Injected packet rate average = 0.0202847
	minimum = 0.002 (at node 144)
	maximum = 0.045 (at node 5)
Accepted packet rate average = 0.0130017
	minimum = 0.00833333 (at node 135)
	maximum = 0.018 (at node 107)
Injected flit rate average = 0.365069
	minimum = 0.036 (at node 144)
	maximum = 0.808333 (at node 5)
Accepted flit rate average= 0.235602
	minimum = 0.155667 (at node 135)
	maximum = 0.330667 (at node 157)
Injected packet length average = 17.9973
Accepted packet length average = 18.1208
Total in-flight flits = 153241 (133945 measured)
latency change    = 0.292262
throughput change = 0.0128844
Draining remaining packets ...
Class 0:
Remaining flits: 5489 13968 13969 13970 13971 13972 13973 13974 13975 13976 [...] (114452 flits)
Measured flits: 203112 203113 203114 203115 203116 203117 203118 203119 203120 203121 [...] (101708 flits)
Class 0:
Remaining flits: 13977 13978 13979 13980 13981 13982 13983 13984 13985 19476 [...] (78418 flits)
Measured flits: 203112 203113 203114 203115 203116 203117 203118 203119 203120 203121 [...] (70051 flits)
Class 0:
Remaining flits: 19476 19477 19478 19479 19480 19481 19482 19483 19484 19485 [...] (45634 flits)
Measured flits: 203112 203113 203114 203115 203116 203117 203118 203119 203120 203121 [...] (40524 flits)
Class 0:
Remaining flits: 37657 37658 37659 37660 37661 37662 37663 37664 37665 37666 [...] (20473 flits)
Measured flits: 203112 203113 203114 203115 203116 203117 203118 203119 203120 203121 [...] (17507 flits)
Class 0:
Remaining flits: 63054 63055 63056 63057 63058 63059 63060 63061 63062 63063 [...] (3922 flits)
Measured flits: 204786 204787 204788 204789 204790 204791 204792 204793 204794 204795 [...] (3267 flits)
Time taken is 11975 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2823.44 (1 samples)
	minimum = 26 (1 samples)
	maximum = 8866 (1 samples)
Network latency average = 2680.66 (1 samples)
	minimum = 25 (1 samples)
	maximum = 8650 (1 samples)
Flit latency average = 2621 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10329 (1 samples)
Fragmentation average = 302.507 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6432 (1 samples)
Injected packet rate average = 0.0202847 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.045 (1 samples)
Accepted packet rate average = 0.0130017 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.365069 (1 samples)
	minimum = 0.036 (1 samples)
	maximum = 0.808333 (1 samples)
Accepted flit rate average = 0.235602 (1 samples)
	minimum = 0.155667 (1 samples)
	maximum = 0.330667 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 18.1208 (1 samples)
Hops average = 5.08944 (1 samples)
Total run time 14.2143
