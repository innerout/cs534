BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 289.726
	minimum = 27
	maximum = 929
Network latency average = 236.923
	minimum = 27
	maximum = 883
Slowest packet = 124
Flit latency average = 160.603
	minimum = 6
	maximum = 923
Slowest flit = 4133
Fragmentation average = 150.212
	minimum = 0
	maximum = 688
Injected packet rate average = 0.015099
	minimum = 0 (at node 43)
	maximum = 0.039 (at node 165)
Accepted packet rate average = 0.00861979
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.269474
	minimum = 0 (at node 43)
	maximum = 0.687 (at node 165)
Accepted flit rate average= 0.180917
	minimum = 0.067 (at node 174)
	maximum = 0.341 (at node 48)
Injected packet length average = 17.8472
Accepted packet length average = 20.9885
Total in-flight flits = 17482 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 428.127
	minimum = 27
	maximum = 1831
Network latency average = 358.355
	minimum = 26
	maximum = 1709
Slowest packet = 541
Flit latency average = 254.316
	minimum = 6
	maximum = 1692
Slowest flit = 16577
Fragmentation average = 200.732
	minimum = 0
	maximum = 1174
Injected packet rate average = 0.0135937
	minimum = 0.001 (at node 188)
	maximum = 0.03 (at node 124)
Accepted packet rate average = 0.00976562
	minimum = 0.0055 (at node 30)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.243456
	minimum = 0.018 (at node 188)
	maximum = 0.538 (at node 124)
Accepted flit rate average= 0.189167
	minimum = 0.108 (at node 81)
	maximum = 0.304 (at node 98)
Injected packet length average = 17.9094
Accepted packet length average = 19.3707
Total in-flight flits = 21320 (0 measured)
latency change    = 0.32327
throughput change = 0.0436123
Class 0:
Packet latency average = 669.335
	minimum = 26
	maximum = 2791
Network latency average = 547.871
	minimum = 23
	maximum = 2592
Slowest packet = 675
Flit latency average = 423.139
	minimum = 6
	maximum = 2630
Slowest flit = 19904
Fragmentation average = 247.612
	minimum = 0
	maximum = 2162
Injected packet rate average = 0.0122344
	minimum = 0 (at node 41)
	maximum = 0.037 (at node 137)
Accepted packet rate average = 0.0109948
	minimum = 0.004 (at node 38)
	maximum = 0.021 (at node 119)
Injected flit rate average = 0.219484
	minimum = 0 (at node 41)
	maximum = 0.666 (at node 137)
Accepted flit rate average= 0.200266
	minimum = 0.082 (at node 38)
	maximum = 0.355 (at node 119)
Injected packet length average = 17.94
Accepted packet length average = 18.2146
Total in-flight flits = 25241 (0 measured)
latency change    = 0.36037
throughput change = 0.0554212
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 485.879
	minimum = 29
	maximum = 2220
Network latency average = 289.503
	minimum = 27
	maximum = 908
Slowest packet = 7578
Flit latency average = 471.882
	minimum = 6
	maximum = 3557
Slowest flit = 6334
Fragmentation average = 165.09
	minimum = 0
	maximum = 607
Injected packet rate average = 0.0117604
	minimum = 0 (at node 3)
	maximum = 0.042 (at node 13)
Accepted packet rate average = 0.0112396
	minimum = 0.003 (at node 47)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.210807
	minimum = 0 (at node 3)
	maximum = 0.754 (at node 109)
Accepted flit rate average= 0.203766
	minimum = 0.054 (at node 47)
	maximum = 0.484 (at node 16)
Injected packet length average = 17.9252
Accepted packet length average = 18.1293
Total in-flight flits = 26762 (16764 measured)
latency change    = 0.377575
throughput change = 0.0171766
Class 0:
Packet latency average = 628.957
	minimum = 27
	maximum = 3076
Network latency average = 386.257
	minimum = 27
	maximum = 1968
Slowest packet = 7578
Flit latency average = 519.128
	minimum = 6
	maximum = 4688
Slowest flit = 8551
Fragmentation average = 191.564
	minimum = 0
	maximum = 1058
Injected packet rate average = 0.0117604
	minimum = 0 (at node 3)
	maximum = 0.027 (at node 13)
Accepted packet rate average = 0.0112266
	minimum = 0.0055 (at node 71)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.211422
	minimum = 0 (at node 64)
	maximum = 0.4785 (at node 13)
Accepted flit rate average= 0.202547
	minimum = 0.1055 (at node 71)
	maximum = 0.3485 (at node 16)
Injected packet length average = 17.9774
Accepted packet length average = 18.0418
Total in-flight flits = 28769 (23209 measured)
latency change    = 0.227485
throughput change = 0.00601713
Class 0:
Packet latency average = 768.693
	minimum = 27
	maximum = 3941
Network latency average = 466.922
	minimum = 26
	maximum = 2731
Slowest packet = 7578
Flit latency average = 551.779
	minimum = 6
	maximum = 5688
Slowest flit = 13337
Fragmentation average = 212.096
	minimum = 0
	maximum = 2519
Injected packet rate average = 0.0116302
	minimum = 0 (at node 64)
	maximum = 0.0253333 (at node 54)
Accepted packet rate average = 0.0111788
	minimum = 0.00633333 (at node 180)
	maximum = 0.0183333 (at node 129)
Injected flit rate average = 0.209101
	minimum = 0 (at node 64)
	maximum = 0.459 (at node 54)
Accepted flit rate average= 0.202172
	minimum = 0.130667 (at node 180)
	maximum = 0.308333 (at node 129)
Injected packet length average = 17.9791
Accepted packet length average = 18.0853
Total in-flight flits = 29390 (25839 measured)
latency change    = 0.181783
throughput change = 0.00185486
Class 0:
Packet latency average = 875.296
	minimum = 23
	maximum = 5409
Network latency average = 532.189
	minimum = 23
	maximum = 3915
Slowest packet = 7578
Flit latency average = 582.459
	minimum = 6
	maximum = 6814
Slowest flit = 8561
Fragmentation average = 223.396
	minimum = 0
	maximum = 2521
Injected packet rate average = 0.0114596
	minimum = 0 (at node 64)
	maximum = 0.024 (at node 54)
Accepted packet rate average = 0.0112161
	minimum = 0.0075 (at node 36)
	maximum = 0.0165 (at node 129)
Injected flit rate average = 0.206072
	minimum = 0 (at node 64)
	maximum = 0.43575 (at node 54)
Accepted flit rate average= 0.202398
	minimum = 0.13325 (at node 36)
	maximum = 0.2865 (at node 129)
Injected packet length average = 17.9824
Accepted packet length average = 18.0453
Total in-flight flits = 28271 (25890 measured)
latency change    = 0.121791
throughput change = 0.00111939
Class 0:
Packet latency average = 955.52
	minimum = 23
	maximum = 6081
Network latency average = 571.579
	minimum = 23
	maximum = 4296
Slowest packet = 7578
Flit latency average = 595.676
	minimum = 6
	maximum = 7548
Slowest flit = 23643
Fragmentation average = 233.599
	minimum = 0
	maximum = 3622
Injected packet rate average = 0.011549
	minimum = 0.0002 (at node 64)
	maximum = 0.0218 (at node 138)
Accepted packet rate average = 0.0112417
	minimum = 0.0072 (at node 36)
	maximum = 0.0156 (at node 129)
Injected flit rate average = 0.207629
	minimum = 0.0018 (at node 64)
	maximum = 0.3922 (at node 138)
Accepted flit rate average= 0.203205
	minimum = 0.14 (at node 36)
	maximum = 0.2708 (at node 128)
Injected packet length average = 17.9782
Accepted packet length average = 18.0761
Total in-flight flits = 29748 (28119 measured)
latency change    = 0.0839589
throughput change = 0.00397023
Class 0:
Packet latency average = 1054.29
	minimum = 23
	maximum = 6924
Network latency average = 612.051
	minimum = 23
	maximum = 5349
Slowest packet = 7578
Flit latency average = 607.466
	minimum = 6
	maximum = 8339
Slowest flit = 24353
Fragmentation average = 241.555
	minimum = 0
	maximum = 3622
Injected packet rate average = 0.0114757
	minimum = 0.000166667 (at node 64)
	maximum = 0.0205 (at node 46)
Accepted packet rate average = 0.011263
	minimum = 0.00816667 (at node 79)
	maximum = 0.015 (at node 129)
Injected flit rate average = 0.206346
	minimum = 0.0015 (at node 64)
	maximum = 0.366167 (at node 46)
Accepted flit rate average= 0.202988
	minimum = 0.140667 (at node 162)
	maximum = 0.261333 (at node 129)
Injected packet length average = 17.9812
Accepted packet length average = 18.0225
Total in-flight flits = 29449 (28213 measured)
latency change    = 0.0936816
throughput change = 0.00107081
Class 0:
Packet latency average = 1146.44
	minimum = 23
	maximum = 7844
Network latency average = 637.417
	minimum = 23
	maximum = 6484
Slowest packet = 7578
Flit latency average = 616.553
	minimum = 6
	maximum = 9014
Slowest flit = 35639
Fragmentation average = 246.808
	minimum = 0
	maximum = 5787
Injected packet rate average = 0.0115699
	minimum = 0.00142857 (at node 1)
	maximum = 0.0197143 (at node 46)
Accepted packet rate average = 0.0112574
	minimum = 0.008 (at node 162)
	maximum = 0.014 (at node 103)
Injected flit rate average = 0.208015
	minimum = 0.0234286 (at node 1)
	maximum = 0.354 (at node 138)
Accepted flit rate average= 0.203313
	minimum = 0.142286 (at node 162)
	maximum = 0.251714 (at node 103)
Injected packet length average = 17.9789
Accepted packet length average = 18.0603
Total in-flight flits = 31871 (30973 measured)
latency change    = 0.0803818
throughput change = 0.00159682
Draining all recorded packets ...
Class 0:
Remaining flits: 8779 8780 8781 8782 8783 28370 28371 28372 28373 28374 [...] (34077 flits)
Measured flits: 138883 138884 138885 138886 138887 139035 139036 139037 139038 139039 [...] (20961 flits)
Class 0:
Remaining flits: 8779 8780 8781 8782 8783 28380 28381 28382 28383 28384 [...] (33049 flits)
Measured flits: 146171 146172 146173 146174 146175 146176 146177 146232 146233 146234 [...] (14750 flits)
Class 0:
Remaining flits: 28600 28601 31662 31663 31664 31665 31666 31667 31668 31669 [...] (34782 flits)
Measured flits: 146232 146233 146234 146235 146236 146237 146238 146239 146240 146241 [...] (11451 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (33586 flits)
Measured flits: 146232 146233 146234 146235 146236 146237 146238 146239 146240 146241 [...] (8746 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (37423 flits)
Measured flits: 146232 146233 146234 146235 146236 146237 146238 146239 146240 146241 [...] (6848 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (39636 flits)
Measured flits: 154998 154999 155000 155001 155002 155003 155004 155005 155006 155007 [...] (5116 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (38117 flits)
Measured flits: 154998 154999 155000 155001 155002 155003 155004 155005 155006 155007 [...] (3566 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (35777 flits)
Measured flits: 154998 154999 155000 155001 155002 155003 155004 155005 155006 155007 [...] (2869 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (36435 flits)
Measured flits: 155574 155575 155576 155577 155578 155579 155580 155581 155582 155583 [...] (2188 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (36538 flits)
Measured flits: 186789 186790 186791 186792 186793 186794 186795 186796 186797 186798 [...] (1782 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (37157 flits)
Measured flits: 186790 186791 186792 186793 186794 186795 186796 186797 186798 186799 [...] (1544 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (37859 flits)
Measured flits: 188838 188839 188840 188841 188842 188843 188844 188845 188846 188847 [...] (1351 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (38956 flits)
Measured flits: 188838 188839 188840 188841 188842 188843 188844 188845 188846 188847 [...] (970 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (41941 flits)
Measured flits: 188838 188839 188840 188841 188842 188843 188844 188845 188846 188847 [...] (746 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (45107 flits)
Measured flits: 188838 188839 188840 188841 188842 188843 188844 188845 188846 188847 [...] (546 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (44566 flits)
Measured flits: 188850 188851 188852 188853 188854 188855 228816 228817 228818 228819 [...] (319 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (45907 flits)
Measured flits: 188850 188851 188852 188853 188854 188855 228816 228817 228818 228819 [...] (211 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (48085 flits)
Measured flits: 188850 188851 188852 188853 188854 188855 228816 228817 228818 228819 [...] (142 flits)
Class 0:
Remaining flits: 31662 31663 31664 31665 31666 31667 31668 31669 31670 31671 [...] (49256 flits)
Measured flits: 228816 228817 228818 228819 228820 228821 228822 228823 228824 228825 [...] (105 flits)
Class 0:
Remaining flits: 31664 31665 31666 31667 31668 31669 31670 31671 31672 31673 [...] (51507 flits)
Measured flits: 228821 228822 228823 228824 228825 228826 228827 228828 228829 228830 [...] (89 flits)
Class 0:
Remaining flits: 31674 31675 31676 31677 31678 31679 85086 85087 85088 85089 [...] (51488 flits)
Measured flits: 539172 539173 539174 539175 539176 539177 539178 539179 539180 539181 [...] (75 flits)
Class 0:
Remaining flits: 31674 31675 31676 31677 31678 31679 85086 85087 85088 85089 [...] (51739 flits)
Measured flits: 539172 539173 539174 539175 539176 539177 539178 539179 539180 539181 [...] (59 flits)
Class 0:
Remaining flits: 31674 31675 31676 31677 31678 31679 85086 85087 85088 85089 [...] (48153 flits)
Measured flits: 631743 631744 631745 661266 661267 661268 661269 661270 661271 661272 [...] (39 flits)
Class 0:
Remaining flits: 31674 31675 31676 31677 31678 31679 85086 85087 85088 85089 [...] (49623 flits)
Measured flits: 661266 661267 661268 661269 661270 661271 661272 661273 661274 661275 [...] (36 flits)
Class 0:
Remaining flits: 31674 31675 31676 31677 31678 31679 85086 85087 85088 85089 [...] (48573 flits)
Measured flits: 661274 661275 661276 661277 661278 661279 661280 661281 661282 661283 (10 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 755964 755965 755966 755967 755968 755969 755970 755971 755972 755973 [...] (22624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 755968 755969 755970 755971 755972 755973 755974 755975 755976 755977 [...] (11395 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 868572 868573 868574 868575 868576 868577 868578 868579 868580 868581 [...] (5117 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 890136 890137 890138 890139 890140 890141 890142 890143 890144 890145 [...] (1763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 954648 954649 954650 954651 954652 954653 954654 954655 954656 954657 [...] (73 flits)
Measured flits: (0 flits)
Time taken is 40409 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2093.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26386 (1 samples)
Network latency average = 952.534 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24869 (1 samples)
Flit latency average = 989.371 (1 samples)
	minimum = 6 (1 samples)
	maximum = 35374 (1 samples)
Fragmentation average = 302.864 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14675 (1 samples)
Injected packet rate average = 0.0115699 (1 samples)
	minimum = 0.00142857 (1 samples)
	maximum = 0.0197143 (1 samples)
Accepted packet rate average = 0.0112574 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.208015 (1 samples)
	minimum = 0.0234286 (1 samples)
	maximum = 0.354 (1 samples)
Accepted flit rate average = 0.203313 (1 samples)
	minimum = 0.142286 (1 samples)
	maximum = 0.251714 (1 samples)
Injected packet size average = 17.9789 (1 samples)
Accepted packet size average = 18.0603 (1 samples)
Hops average = 5.07954 (1 samples)
Total run time 50.5746
