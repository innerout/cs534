BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 264.814
	minimum = 23
	maximum = 934
Network latency average = 213.009
	minimum = 23
	maximum = 868
Slowest packet = 64
Flit latency average = 175.488
	minimum = 6
	maximum = 851
Slowest flit = 5237
Fragmentation average = 52.6225
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.00903646
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.170365
	minimum = 0.036 (at node 174)
	maximum = 0.316 (at node 152)
Injected packet length average = 17.8661
Accepted packet length average = 18.853
Total in-flight flits = 21884 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 429.915
	minimum = 23
	maximum = 1860
Network latency average = 368.307
	minimum = 23
	maximum = 1533
Slowest packet = 64
Flit latency average = 326.748
	minimum = 6
	maximum = 1516
Slowest flit = 21185
Fragmentation average = 60.013
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.00940885
	minimum = 0.005 (at node 108)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.172831
	minimum = 0.09 (at node 153)
	maximum = 0.265 (at node 22)
Injected packet length average = 17.9137
Accepted packet length average = 18.3689
Total in-flight flits = 41273 (0 measured)
latency change    = 0.384031
throughput change = 0.0142691
Class 0:
Packet latency average = 886.736
	minimum = 27
	maximum = 2750
Network latency average = 810.12
	minimum = 27
	maximum = 2517
Slowest packet = 1441
Flit latency average = 770.932
	minimum = 6
	maximum = 2500
Slowest flit = 24263
Fragmentation average = 70.5192
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.00975
	minimum = 0.002 (at node 151)
	maximum = 0.019 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.176943
	minimum = 0.036 (at node 184)
	maximum = 0.345 (at node 56)
Injected packet length average = 18.0167
Accepted packet length average = 18.148
Total in-flight flits = 63280 (0 measured)
latency change    = 0.515171
throughput change = 0.023239
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 290.968
	minimum = 23
	maximum = 1047
Network latency average = 220.867
	minimum = 23
	maximum = 976
Slowest packet = 9099
Flit latency average = 1114.15
	minimum = 6
	maximum = 3142
Slowest flit = 31049
Fragmentation average = 37.0461
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0153438
	minimum = 0 (at node 35)
	maximum = 0.049 (at node 138)
Accepted packet rate average = 0.0101198
	minimum = 0.003 (at node 36)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.276078
	minimum = 0 (at node 35)
	maximum = 0.885 (at node 138)
Accepted flit rate average= 0.182854
	minimum = 0.053 (at node 36)
	maximum = 0.323 (at node 97)
Injected packet length average = 17.9929
Accepted packet length average = 18.069
Total in-flight flits = 81200 (45068 measured)
latency change    = 2.04753
throughput change = 0.0323288
Class 0:
Packet latency average = 631.934
	minimum = 23
	maximum = 2195
Network latency average = 561.103
	minimum = 23
	maximum = 1975
Slowest packet = 9099
Flit latency average = 1265.91
	minimum = 6
	maximum = 3957
Slowest flit = 35675
Fragmentation average = 53.3728
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0151641
	minimum = 0 (at node 80)
	maximum = 0.0405 (at node 39)
Accepted packet rate average = 0.00995573
	minimum = 0.0035 (at node 36)
	maximum = 0.0155 (at node 145)
Injected flit rate average = 0.273049
	minimum = 0 (at node 80)
	maximum = 0.7205 (at node 39)
Accepted flit rate average= 0.179068
	minimum = 0.0625 (at node 36)
	maximum = 0.277 (at node 145)
Injected packet length average = 18.0064
Accepted packet length average = 17.9864
Total in-flight flits = 99332 (80049 measured)
latency change    = 0.539559
throughput change = 0.0211454
Class 0:
Packet latency average = 998.921
	minimum = 23
	maximum = 3086
Network latency average = 926.171
	minimum = 23
	maximum = 2983
Slowest packet = 9099
Flit latency average = 1439
	minimum = 6
	maximum = 4948
Slowest flit = 49535
Fragmentation average = 61.6495
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0149444
	minimum = 0.000666667 (at node 68)
	maximum = 0.032 (at node 41)
Accepted packet rate average = 0.00980729
	minimum = 0.00366667 (at node 36)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.269177
	minimum = 0.012 (at node 68)
	maximum = 0.576 (at node 41)
Accepted flit rate average= 0.176179
	minimum = 0.0683333 (at node 36)
	maximum = 0.254667 (at node 151)
Injected packet length average = 18.0118
Accepted packet length average = 17.9641
Total in-flight flits = 116745 (107069 measured)
latency change    = 0.367383
throughput change = 0.0163975
Draining remaining packets ...
Class 0:
Remaining flits: 88092 88093 88094 88095 88096 88097 88098 88099 88100 88101 [...] (86874 flits)
Measured flits: 164232 164233 164234 164235 164236 164237 164238 164239 164240 164241 [...] (82667 flits)
Class 0:
Remaining flits: 96876 96877 96878 96879 96880 96881 96882 96883 96884 96885 [...] (57498 flits)
Measured flits: 164232 164233 164234 164235 164236 164237 164238 164239 164240 164241 [...] (56083 flits)
Class 0:
Remaining flits: 111953 111954 111955 111956 111957 111958 111959 115884 115885 115886 [...] (30022 flits)
Measured flits: 164232 164233 164234 164235 164236 164237 164238 164239 164240 164241 [...] (29657 flits)
Class 0:
Remaining flits: 117900 117901 117902 117903 117904 117905 117906 117907 117908 117909 [...] (9544 flits)
Measured flits: 169308 169309 169310 169311 169312 169313 169314 169315 169316 169317 [...] (9454 flits)
Class 0:
Remaining flits: 207590 207591 207592 207593 209873 209874 209875 209876 209877 209878 [...] (568 flits)
Measured flits: 207590 207591 207592 207593 209873 209874 209875 209876 209877 209878 [...] (568 flits)
Time taken is 11308 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2788.45 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7765 (1 samples)
Network latency average = 2702.06 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7561 (1 samples)
Flit latency average = 2553.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8317 (1 samples)
Fragmentation average = 67.9263 (1 samples)
	minimum = 0 (1 samples)
	maximum = 163 (1 samples)
Injected packet rate average = 0.0149444 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.032 (1 samples)
Accepted packet rate average = 0.00980729 (1 samples)
	minimum = 0.00366667 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.269177 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.576 (1 samples)
Accepted flit rate average = 0.176179 (1 samples)
	minimum = 0.0683333 (1 samples)
	maximum = 0.254667 (1 samples)
Injected packet size average = 18.0118 (1 samples)
Accepted packet size average = 17.9641 (1 samples)
Hops average = 5.09085 (1 samples)
Total run time 9.55066
