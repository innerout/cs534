BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 191.846
	minimum = 23
	maximum = 916
Network latency average = 189.32
	minimum = 23
	maximum = 916
Slowest packet = 103
Flit latency average = 120.828
	minimum = 6
	maximum = 899
Slowest flit = 1871
Fragmentation average = 116.295
	minimum = 0
	maximum = 851
Injected packet rate average = 0.0134635
	minimum = 0.006 (at node 94)
	maximum = 0.024 (at node 33)
Accepted packet rate average = 0.00892188
	minimum = 0.003 (at node 30)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.239865
	minimum = 0.108 (at node 94)
	maximum = 0.432 (at node 33)
Accepted flit rate average= 0.179792
	minimum = 0.066 (at node 174)
	maximum = 0.32 (at node 91)
Injected packet length average = 17.8159
Accepted packet length average = 20.1518
Total in-flight flits = 12028 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 291.451
	minimum = 23
	maximum = 1675
Network latency average = 284.502
	minimum = 23
	maximum = 1675
Slowest packet = 419
Flit latency average = 199.933
	minimum = 6
	maximum = 1658
Slowest flit = 7559
Fragmentation average = 148.914
	minimum = 0
	maximum = 1233
Injected packet rate average = 0.0129167
	minimum = 0.0065 (at node 20)
	maximum = 0.02 (at node 22)
Accepted packet rate average = 0.00980469
	minimum = 0.0055 (at node 79)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.23082
	minimum = 0.1115 (at node 20)
	maximum = 0.36 (at node 33)
Accepted flit rate average= 0.187622
	minimum = 0.099 (at node 79)
	maximum = 0.292 (at node 22)
Injected packet length average = 17.87
Accepted packet length average = 19.136
Total in-flight flits = 17251 (0 measured)
latency change    = 0.341755
throughput change = 0.0417366
Class 0:
Packet latency average = 567.862
	minimum = 23
	maximum = 2739
Network latency average = 513.429
	minimum = 23
	maximum = 2739
Slowest packet = 416
Flit latency average = 405.42
	minimum = 6
	maximum = 2722
Slowest flit = 7505
Fragmentation average = 208.833
	minimum = 0
	maximum = 2604
Injected packet rate average = 0.0119167
	minimum = 0 (at node 119)
	maximum = 0.027 (at node 83)
Accepted packet rate average = 0.0108177
	minimum = 0.003 (at node 9)
	maximum = 0.019 (at node 24)
Injected flit rate average = 0.213531
	minimum = 0 (at node 145)
	maximum = 0.488 (at node 83)
Accepted flit rate average= 0.196547
	minimum = 0.079 (at node 184)
	maximum = 0.342 (at node 152)
Injected packet length average = 17.9187
Accepted packet length average = 18.169
Total in-flight flits = 20698 (0 measured)
latency change    = 0.486758
throughput change = 0.0454064
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 491.775
	minimum = 23
	maximum = 2426
Network latency average = 323.902
	minimum = 23
	maximum = 974
Slowest packet = 7281
Flit latency average = 486.277
	minimum = 6
	maximum = 3364
Slowest flit = 17477
Fragmentation average = 143.862
	minimum = 0
	maximum = 622
Injected packet rate average = 0.0124323
	minimum = 0 (at node 6)
	maximum = 0.031 (at node 35)
Accepted packet rate average = 0.0111458
	minimum = 0.001 (at node 91)
	maximum = 0.021 (at node 120)
Injected flit rate average = 0.223182
	minimum = 0.001 (at node 6)
	maximum = 0.55 (at node 35)
Accepted flit rate average= 0.199729
	minimum = 0.057 (at node 91)
	maximum = 0.383 (at node 120)
Injected packet length average = 17.9518
Accepted packet length average = 17.9196
Total in-flight flits = 25424 (21605 measured)
latency change    = 0.154718
throughput change = 0.015933
Class 0:
Packet latency average = 681.319
	minimum = 23
	maximum = 3301
Network latency average = 490.243
	minimum = 23
	maximum = 1958
Slowest packet = 7281
Flit latency average = 537.879
	minimum = 6
	maximum = 3738
Slowest flit = 51137
Fragmentation average = 167.553
	minimum = 0
	maximum = 1189
Injected packet rate average = 0.0121068
	minimum = 0 (at node 6)
	maximum = 0.024 (at node 185)
Accepted packet rate average = 0.0110156
	minimum = 0.0045 (at node 180)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.218104
	minimum = 0.0015 (at node 99)
	maximum = 0.436 (at node 185)
Accepted flit rate average= 0.197576
	minimum = 0.1 (at node 180)
	maximum = 0.3155 (at node 78)
Injected packet length average = 18.0151
Accepted packet length average = 17.9359
Total in-flight flits = 28727 (27892 measured)
latency change    = 0.278201
throughput change = 0.0109004
Class 0:
Packet latency average = 838.903
	minimum = 23
	maximum = 4036
Network latency average = 595.188
	minimum = 23
	maximum = 2910
Slowest packet = 7281
Flit latency average = 580.978
	minimum = 6
	maximum = 3854
Slowest flit = 94404
Fragmentation average = 181.827
	minimum = 0
	maximum = 2472
Injected packet rate average = 0.0117708
	minimum = 0 (at node 99)
	maximum = 0.0196667 (at node 185)
Accepted packet rate average = 0.010941
	minimum = 0.006 (at node 4)
	maximum = 0.0163333 (at node 165)
Injected flit rate average = 0.211764
	minimum = 0.001 (at node 99)
	maximum = 0.354333 (at node 185)
Accepted flit rate average= 0.196217
	minimum = 0.116667 (at node 4)
	maximum = 0.283667 (at node 16)
Injected packet length average = 17.9906
Accepted packet length average = 17.9341
Total in-flight flits = 29951 (29774 measured)
latency change    = 0.187845
throughput change = 0.00692349
Class 0:
Packet latency average = 969.106
	minimum = 23
	maximum = 4265
Network latency average = 670.042
	minimum = 23
	maximum = 3793
Slowest packet = 7281
Flit latency average = 617.719
	minimum = 6
	maximum = 5209
Slowest flit = 61865
Fragmentation average = 188.69
	minimum = 0
	maximum = 2472
Injected packet rate average = 0.0116042
	minimum = 0.0025 (at node 176)
	maximum = 0.01825 (at node 145)
Accepted packet rate average = 0.0108581
	minimum = 0.00775 (at node 4)
	maximum = 0.0155 (at node 165)
Injected flit rate average = 0.20828
	minimum = 0.04475 (at node 176)
	maximum = 0.329 (at node 145)
Accepted flit rate average= 0.195249
	minimum = 0.14 (at node 89)
	maximum = 0.27775 (at node 128)
Injected packet length average = 17.9487
Accepted packet length average = 17.9819
Total in-flight flits = 31307 (31227 measured)
latency change    = 0.134354
throughput change = 0.0049594
Class 0:
Packet latency average = 1083.55
	minimum = 23
	maximum = 5351
Network latency average = 722.289
	minimum = 23
	maximum = 4686
Slowest packet = 7281
Flit latency average = 647.456
	minimum = 6
	maximum = 5891
Slowest flit = 92892
Fragmentation average = 193.444
	minimum = 0
	maximum = 2893
Injected packet rate average = 0.0114312
	minimum = 0.002 (at node 176)
	maximum = 0.0172 (at node 185)
Accepted packet rate average = 0.0108594
	minimum = 0.0076 (at node 31)
	maximum = 0.0156 (at node 128)
Injected flit rate average = 0.205515
	minimum = 0.0358 (at node 176)
	maximum = 0.3112 (at node 185)
Accepted flit rate average= 0.195454
	minimum = 0.137 (at node 31)
	maximum = 0.2844 (at node 128)
Injected packet length average = 17.9783
Accepted packet length average = 17.9987
Total in-flight flits = 30864 (30838 measured)
latency change    = 0.105616
throughput change = 0.00105124
Class 0:
Packet latency average = 1191.03
	minimum = 23
	maximum = 7090
Network latency average = 761.154
	minimum = 23
	maximum = 5580
Slowest packet = 7281
Flit latency average = 671.285
	minimum = 6
	maximum = 5944
Slowest flit = 92897
Fragmentation average = 197.042
	minimum = 0
	maximum = 3942
Injected packet rate average = 0.0114115
	minimum = 0.003 (at node 176)
	maximum = 0.0166667 (at node 35)
Accepted packet rate average = 0.0108828
	minimum = 0.00766667 (at node 89)
	maximum = 0.0155 (at node 128)
Injected flit rate average = 0.205268
	minimum = 0.0538333 (at node 176)
	maximum = 0.3005 (at node 35)
Accepted flit rate average= 0.195426
	minimum = 0.139 (at node 89)
	maximum = 0.275 (at node 128)
Injected packet length average = 17.9879
Accepted packet length average = 17.9573
Total in-flight flits = 32609 (32609 measured)
latency change    = 0.0902445
throughput change = 0.000143028
Class 0:
Packet latency average = 1298.87
	minimum = 23
	maximum = 7090
Network latency average = 785.265
	minimum = 23
	maximum = 5595
Slowest packet = 7281
Flit latency average = 685.312
	minimum = 6
	maximum = 5944
Slowest flit = 92897
Fragmentation average = 198.267
	minimum = 0
	maximum = 3942
Injected packet rate average = 0.011343
	minimum = 0.00257143 (at node 176)
	maximum = 0.0168571 (at node 145)
Accepted packet rate average = 0.0108713
	minimum = 0.00814286 (at node 89)
	maximum = 0.0154286 (at node 128)
Injected flit rate average = 0.203937
	minimum = 0.0471429 (at node 176)
	maximum = 0.304143 (at node 145)
Accepted flit rate average= 0.19518
	minimum = 0.146 (at node 89)
	maximum = 0.274714 (at node 128)
Injected packet length average = 17.9791
Accepted packet length average = 17.9537
Total in-flight flits = 33038 (33038 measured)
latency change    = 0.0830267
throughput change = 0.00126117
Draining all recorded packets ...
Class 0:
Remaining flits: 183011 183012 183013 183014 183015 183016 183017 183018 183019 183020 [...] (35054 flits)
Measured flits: 183011 183012 183013 183014 183015 183016 183017 183018 183019 183020 [...] (25060 flits)
Class 0:
Remaining flits: 253998 253999 254000 254001 254002 254003 254004 254005 254006 254007 [...] (35422 flits)
Measured flits: 253998 253999 254000 254001 254002 254003 254004 254005 254006 254007 [...] (17286 flits)
Class 0:
Remaining flits: 317918 317919 317920 317921 317922 317923 317924 317925 317926 317927 [...] (35635 flits)
Measured flits: 317918 317919 317920 317921 317922 317923 317924 317925 317926 317927 [...] (10779 flits)
Class 0:
Remaining flits: 323369 338995 338996 338997 338998 338999 339000 339001 339002 339003 [...] (33595 flits)
Measured flits: 323369 338995 338996 338997 338998 338999 339000 339001 339002 339003 [...] (5569 flits)
Class 0:
Remaining flits: 365184 365185 365186 365187 365188 365189 365190 365191 365192 365193 [...] (34926 flits)
Measured flits: 365184 365185 365186 365187 365188 365189 365190 365191 365192 365193 [...] (3449 flits)
Class 0:
Remaining flits: 378974 378975 378976 378977 378978 378979 378980 378981 378982 378983 [...] (34428 flits)
Measured flits: 378974 378975 378976 378977 378978 378979 378980 378981 378982 378983 [...] (1983 flits)
Class 0:
Remaining flits: 410274 410275 410276 410277 410278 410279 410280 410281 410282 410283 [...] (32528 flits)
Measured flits: 410274 410275 410276 410277 410278 410279 410280 410281 410282 410283 [...] (877 flits)
Class 0:
Remaining flits: 410274 410275 410276 410277 410278 410279 410280 410281 410282 410283 [...] (35725 flits)
Measured flits: 410274 410275 410276 410277 410278 410279 410280 410281 410282 410283 [...] (729 flits)
Class 0:
Remaining flits: 440374 440375 440376 440377 440378 440379 440380 440381 440382 440383 [...] (35087 flits)
Measured flits: 527610 527611 527612 527613 527614 527615 606366 606367 606368 606369 [...] (349 flits)
Class 0:
Remaining flits: 440375 440376 440377 440378 440379 440380 440381 440382 440383 440384 [...] (35962 flits)
Measured flits: 527615 606366 606367 606368 606369 606370 606371 606372 606373 606374 [...] (169 flits)
Class 0:
Remaining flits: 440376 440377 440378 440379 440380 440381 440382 440383 440384 440385 [...] (36681 flits)
Measured flits: 649965 649966 649967 649968 649969 649970 649971 649972 649973 649974 [...] (51 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 528012 528013 528014 528015 528016 528017 528018 528019 528020 528021 [...] (7103 flits)
Measured flits: (0 flits)
Time taken is 23274 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1975.53 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12106 (1 samples)
Network latency average = 929.705 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8259 (1 samples)
Flit latency average = 847.121 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10835 (1 samples)
Fragmentation average = 208.322 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3942 (1 samples)
Injected packet rate average = 0.011343 (1 samples)
	minimum = 0.00257143 (1 samples)
	maximum = 0.0168571 (1 samples)
Accepted packet rate average = 0.0108713 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0154286 (1 samples)
Injected flit rate average = 0.203937 (1 samples)
	minimum = 0.0471429 (1 samples)
	maximum = 0.304143 (1 samples)
Accepted flit rate average = 0.19518 (1 samples)
	minimum = 0.146 (1 samples)
	maximum = 0.274714 (1 samples)
Injected packet size average = 17.9791 (1 samples)
Accepted packet size average = 17.9537 (1 samples)
Hops average = 5.07186 (1 samples)
Total run time 23.9267
