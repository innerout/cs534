BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 332.017
	minimum = 23
	maximum = 967
Network latency average = 306.493
	minimum = 23
	maximum = 962
Slowest packet = 113
Flit latency average = 252.688
	minimum = 6
	maximum = 971
Slowest flit = 1616
Fragmentation average = 152.36
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0119792
	minimum = 0.006 (at node 61)
	maximum = 0.022 (at node 70)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.24488
	minimum = 0.114 (at node 93)
	maximum = 0.422 (at node 70)
Injected packet length average = 17.8433
Accepted packet length average = 20.4422
Total in-flight flits = 116819 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 623.313
	minimum = 23
	maximum = 1956
Network latency average = 586.707
	minimum = 23
	maximum = 1927
Slowest packet = 463
Flit latency average = 525.217
	minimum = 6
	maximum = 1910
Slowest flit = 8351
Fragmentation average = 177.356
	minimum = 0
	maximum = 1775
Injected packet rate average = 0.0490573
	minimum = 0.039 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0134896
	minimum = 0.0085 (at node 8)
	maximum = 0.02 (at node 136)
Injected flit rate average = 0.878964
	minimum = 0.698 (at node 43)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.257253
	minimum = 0.1625 (at node 153)
	maximum = 0.3705 (at node 177)
Injected packet length average = 17.9171
Accepted packet length average = 19.0705
Total in-flight flits = 240299 (0 measured)
latency change    = 0.467335
throughput change = 0.0480943
Class 0:
Packet latency average = 1392.56
	minimum = 29
	maximum = 2786
Network latency average = 1336.39
	minimum = 23
	maximum = 2786
Slowest packet = 854
Flit latency average = 1300.75
	minimum = 6
	maximum = 2926
Slowest flit = 6999
Fragmentation average = 194.887
	minimum = 0
	maximum = 1891
Injected packet rate average = 0.050625
	minimum = 0.036 (at node 119)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0148802
	minimum = 0.006 (at node 83)
	maximum = 0.025 (at node 75)
Injected flit rate average = 0.91126
	minimum = 0.651 (at node 119)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.267641
	minimum = 0.107 (at node 153)
	maximum = 0.454 (at node 151)
Injected packet length average = 18.0002
Accepted packet length average = 17.9863
Total in-flight flits = 363872 (0 measured)
latency change    = 0.552398
throughput change = 0.0388133
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 135.283
	minimum = 25
	maximum = 575
Network latency average = 52.1864
	minimum = 23
	maximum = 324
Slowest packet = 28608
Flit latency average = 1865.6
	minimum = 6
	maximum = 3899
Slowest flit = 11115
Fragmentation average = 16.1524
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0511719
	minimum = 0.037 (at node 10)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0146458
	minimum = 0.007 (at node 100)
	maximum = 0.024 (at node 187)
Injected flit rate average = 0.921703
	minimum = 0.68 (at node 10)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.26351
	minimum = 0.126 (at node 110)
	maximum = 0.389 (at node 187)
Injected packet length average = 18.0119
Accepted packet length average = 17.9922
Total in-flight flits = 490128 (163307 measured)
latency change    = 9.29369
throughput change = 0.0156738
Class 0:
Packet latency average = 143.87
	minimum = 25
	maximum = 1085
Network latency average = 55.991
	minimum = 23
	maximum = 933
Slowest packet = 31317
Flit latency average = 2126.44
	minimum = 6
	maximum = 4803
Slowest flit = 25821
Fragmentation average = 16.3976
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0512031
	minimum = 0.0395 (at node 43)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0147578
	minimum = 0.009 (at node 4)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.921833
	minimum = 0.71 (at node 43)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.264859
	minimum = 0.161 (at node 4)
	maximum = 0.3955 (at node 44)
Injected packet length average = 18.0035
Accepted packet length average = 17.9471
Total in-flight flits = 616082 (325730 measured)
latency change    = 0.0596849
throughput change = 0.00509311
Class 0:
Packet latency average = 143.341
	minimum = 23
	maximum = 1196
Network latency average = 57.1754
	minimum = 23
	maximum = 1034
Slowest packet = 31317
Flit latency average = 2390.84
	minimum = 6
	maximum = 5681
Slowest flit = 46917
Fragmentation average = 16.6526
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0510087
	minimum = 0.042 (at node 182)
	maximum = 0.0556667 (at node 15)
Accepted packet rate average = 0.0148038
	minimum = 0.00966667 (at node 20)
	maximum = 0.0193333 (at node 8)
Injected flit rate average = 0.918224
	minimum = 0.756667 (at node 182)
	maximum = 1 (at node 15)
Accepted flit rate average= 0.265339
	minimum = 0.168333 (at node 36)
	maximum = 0.347 (at node 90)
Injected packet length average = 18.0013
Accepted packet length average = 17.9237
Total in-flight flits = 739895 (485980 measured)
latency change    = 0.00368603
throughput change = 0.00180587
Draining remaining packets ...
Class 0:
Remaining flits: 43884 43885 43886 43887 43888 43889 43890 43891 43892 43893 [...] (703915 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (485424 flits)
Class 0:
Remaining flits: 60012 60013 60014 60015 60016 60017 60018 60019 60020 60021 [...] (668840 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (485091 flits)
Class 0:
Remaining flits: 67212 67213 67214 67215 67216 67217 67218 67219 67220 67221 [...] (634761 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (484246 flits)
Class 0:
Remaining flits: 67212 67213 67214 67215 67216 67217 67218 67219 67220 67221 [...] (600471 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (480443 flits)
Class 0:
Remaining flits: 75420 75421 75422 75423 75424 75425 75426 75427 75428 75429 [...] (566202 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (472320 flits)
Class 0:
Remaining flits: 75420 75421 75422 75423 75424 75425 75426 75427 75428 75429 [...] (531824 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (458819 flits)
Class 0:
Remaining flits: 85032 85033 85034 85035 85036 85037 85038 85039 85040 85041 [...] (497065 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (441268 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (462939 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (420429 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (428726 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (395542 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (394568 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (369345 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (360732 flits)
Measured flits: 514108 514109 514110 514111 514112 514113 514114 514115 514134 514135 [...] (341248 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (326721 flits)
Measured flits: 514134 514135 514136 514137 514138 514139 514140 514141 514142 514143 [...] (311973 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (293081 flits)
Measured flits: 514147 514148 514149 514150 514151 514170 514171 514172 514173 514174 [...] (282176 flits)
Class 0:
Remaining flits: 138150 138151 138152 138153 138154 138155 138156 138157 138158 138159 [...] (259406 flits)
Measured flits: 514170 514171 514172 514173 514174 514175 514176 514177 514178 514179 [...] (251336 flits)
Class 0:
Remaining flits: 138158 138159 138160 138161 138162 138163 138164 138165 138166 138167 [...] (226237 flits)
Measured flits: 514170 514171 514172 514173 514174 514175 514176 514177 514178 514179 [...] (220662 flits)
Class 0:
Remaining flits: 153234 153235 153236 153237 153238 153239 153240 153241 153242 153243 [...] (192489 flits)
Measured flits: 514170 514171 514172 514173 514174 514175 514176 514177 514178 514179 [...] (188969 flits)
Class 0:
Remaining flits: 153234 153235 153236 153237 153238 153239 153240 153241 153242 153243 [...] (158415 flits)
Measured flits: 514170 514171 514172 514173 514174 514175 514176 514177 514178 514179 [...] (156085 flits)
Class 0:
Remaining flits: 153234 153235 153236 153237 153238 153239 153240 153241 153242 153243 [...] (125803 flits)
Measured flits: 515178 515179 515180 515181 515182 515183 515184 515185 515186 515187 [...] (124271 flits)
Class 0:
Remaining flits: 245213 267291 267292 267293 267294 267295 267296 267297 267298 267299 [...] (92834 flits)
Measured flits: 516204 516205 516206 516207 516208 516209 516210 516211 516212 516213 [...] (91878 flits)
Class 0:
Remaining flits: 368334 368335 368336 368337 368338 368339 368340 368341 368342 368343 [...] (61111 flits)
Measured flits: 516204 516205 516206 516207 516208 516209 516210 516211 516212 516213 [...] (60839 flits)
Class 0:
Remaining flits: 451962 451963 451964 451965 451966 451967 451968 451969 451970 451971 [...] (30969 flits)
Measured flits: 516204 516205 516206 516207 516208 516209 516210 516211 516212 516213 [...] (30871 flits)
Class 0:
Remaining flits: 543645 543646 543647 543648 543649 543650 543651 543652 543653 557412 [...] (8993 flits)
Measured flits: 543645 543646 543647 543648 543649 543650 543651 543652 543653 557412 [...] (8993 flits)
Class 0:
Remaining flits: 632724 632725 632726 632727 632728 632729 632730 632731 632732 632733 [...] (1208 flits)
Measured flits: 632724 632725 632726 632727 632728 632729 632730 632731 632732 632733 [...] (1208 flits)
Time taken is 29525 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14436.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25820 (1 samples)
Network latency average = 14347 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25401 (1 samples)
Flit latency average = 11270.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 25384 (1 samples)
Fragmentation average = 167.456 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3287 (1 samples)
Injected packet rate average = 0.0510087 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0148038 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.918224 (1 samples)
	minimum = 0.756667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.265339 (1 samples)
	minimum = 0.168333 (1 samples)
	maximum = 0.347 (1 samples)
Injected packet size average = 18.0013 (1 samples)
Accepted packet size average = 17.9237 (1 samples)
Hops average = 5.07297 (1 samples)
Total run time 29.9643
