BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 217.636
	minimum = 23
	maximum = 863
Network latency average = 148.16
	minimum = 22
	maximum = 703
Slowest packet = 21
Flit latency average = 118.34
	minimum = 5
	maximum = 686
Slowest flit = 11015
Fragmentation average = 30.6533
	minimum = 0
	maximum = 242
Injected packet rate average = 0.0165417
	minimum = 0 (at node 66)
	maximum = 0.055 (at node 61)
Accepted packet rate average = 0.0121979
	minimum = 0.005 (at node 174)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.295
	minimum = 0 (at node 66)
	maximum = 0.98 (at node 61)
Accepted flit rate average= 0.226776
	minimum = 0.1 (at node 174)
	maximum = 0.385 (at node 70)
Injected packet length average = 17.8338
Accepted packet length average = 18.5914
Total in-flight flits = 13663 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 323.537
	minimum = 22
	maximum = 1646
Network latency average = 232.005
	minimum = 22
	maximum = 1339
Slowest packet = 21
Flit latency average = 198.098
	minimum = 5
	maximum = 1306
Slowest flit = 35194
Fragmentation average = 37.1206
	minimum = 0
	maximum = 343
Injected packet rate average = 0.0161458
	minimum = 0.0005 (at node 76)
	maximum = 0.0435 (at node 10)
Accepted packet rate average = 0.0129948
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.289245
	minimum = 0.009 (at node 76)
	maximum = 0.7775 (at node 10)
Accepted flit rate average= 0.238005
	minimum = 0.126 (at node 116)
	maximum = 0.356 (at node 166)
Injected packet length average = 17.9145
Accepted packet length average = 18.3154
Total in-flight flits = 20350 (0 measured)
latency change    = 0.327324
throughput change = 0.0471803
Class 0:
Packet latency average = 569.436
	minimum = 25
	maximum = 2274
Network latency average = 416.797
	minimum = 22
	maximum = 1875
Slowest packet = 3991
Flit latency average = 379.882
	minimum = 5
	maximum = 1858
Slowest flit = 34091
Fragmentation average = 44.8341
	minimum = 0
	maximum = 272
Injected packet rate average = 0.016099
	minimum = 0 (at node 102)
	maximum = 0.041 (at node 75)
Accepted packet rate average = 0.0140677
	minimum = 0.005 (at node 91)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.28912
	minimum = 0 (at node 102)
	maximum = 0.74 (at node 75)
Accepted flit rate average= 0.253792
	minimum = 0.09 (at node 91)
	maximum = 0.451 (at node 34)
Injected packet length average = 17.9589
Accepted packet length average = 18.0407
Total in-flight flits = 27368 (0 measured)
latency change    = 0.431828
throughput change = 0.0622024
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 502.516
	minimum = 22
	maximum = 3107
Network latency average = 317.074
	minimum = 22
	maximum = 938
Slowest packet = 9379
Flit latency average = 503.653
	minimum = 5
	maximum = 2186
Slowest flit = 94668
Fragmentation average = 39.7063
	minimum = 0
	maximum = 233
Injected packet rate average = 0.0158385
	minimum = 0 (at node 165)
	maximum = 0.045 (at node 131)
Accepted packet rate average = 0.0145365
	minimum = 0.005 (at node 36)
	maximum = 0.024 (at node 78)
Injected flit rate average = 0.284766
	minimum = 0 (at node 165)
	maximum = 0.812 (at node 131)
Accepted flit rate average= 0.263625
	minimum = 0.09 (at node 36)
	maximum = 0.451 (at node 129)
Injected packet length average = 17.9793
Accepted packet length average = 18.1354
Total in-flight flits = 31580 (29618 measured)
latency change    = 0.133169
throughput change = 0.0373005
Class 0:
Packet latency average = 747.995
	minimum = 22
	maximum = 3693
Network latency average = 493.341
	minimum = 22
	maximum = 1907
Slowest packet = 9379
Flit latency average = 534.748
	minimum = 5
	maximum = 2576
Slowest flit = 133541
Fragmentation average = 51.072
	minimum = 0
	maximum = 254
Injected packet rate average = 0.0156406
	minimum = 0.0015 (at node 75)
	maximum = 0.031 (at node 39)
Accepted packet rate average = 0.014638
	minimum = 0.009 (at node 36)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.281607
	minimum = 0.027 (at node 75)
	maximum = 0.558 (at node 39)
Accepted flit rate average= 0.264422
	minimum = 0.1715 (at node 36)
	maximum = 0.4275 (at node 129)
Injected packet length average = 18.0048
Accepted packet length average = 18.064
Total in-flight flits = 34190 (34082 measured)
latency change    = 0.328182
throughput change = 0.00301365
Class 0:
Packet latency average = 882.582
	minimum = 22
	maximum = 4111
Network latency average = 573.842
	minimum = 22
	maximum = 2554
Slowest packet = 9379
Flit latency average = 570.716
	minimum = 5
	maximum = 2694
Slowest flit = 132371
Fragmentation average = 54.0053
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0152882
	minimum = 0.001 (at node 75)
	maximum = 0.0283333 (at node 158)
Accepted packet rate average = 0.014651
	minimum = 0.00933333 (at node 36)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.274924
	minimum = 0.018 (at node 75)
	maximum = 0.51 (at node 158)
Accepted flit rate average= 0.264111
	minimum = 0.168 (at node 36)
	maximum = 0.376 (at node 129)
Injected packet length average = 17.9827
Accepted packet length average = 18.0268
Total in-flight flits = 33910 (33910 measured)
latency change    = 0.152493
throughput change = 0.00117664
Class 0:
Packet latency average = 984.441
	minimum = 22
	maximum = 4746
Network latency average = 606.994
	minimum = 22
	maximum = 2723
Slowest packet = 9379
Flit latency average = 586.986
	minimum = 5
	maximum = 2701
Slowest flit = 230957
Fragmentation average = 55.0937
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0153932
	minimum = 0.00075 (at node 75)
	maximum = 0.02725 (at node 126)
Accepted packet rate average = 0.014694
	minimum = 0.00975 (at node 36)
	maximum = 0.01975 (at node 128)
Injected flit rate average = 0.27691
	minimum = 0.0135 (at node 75)
	maximum = 0.48975 (at node 126)
Accepted flit rate average= 0.26474
	minimum = 0.17825 (at node 36)
	maximum = 0.3575 (at node 128)
Injected packet length average = 17.9891
Accepted packet length average = 18.0168
Total in-flight flits = 37096 (37096 measured)
latency change    = 0.103469
throughput change = 0.00237393
Class 0:
Packet latency average = 1078.96
	minimum = 22
	maximum = 5621
Network latency average = 634.923
	minimum = 22
	maximum = 3195
Slowest packet = 9379
Flit latency average = 605.039
	minimum = 5
	maximum = 3147
Slowest flit = 245952
Fragmentation average = 56.5337
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0153417
	minimum = 0.0026 (at node 75)
	maximum = 0.0266 (at node 126)
Accepted packet rate average = 0.0146687
	minimum = 0.0106 (at node 36)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.275903
	minimum = 0.0468 (at node 75)
	maximum = 0.4772 (at node 126)
Accepted flit rate average= 0.264622
	minimum = 0.1918 (at node 64)
	maximum = 0.342 (at node 157)
Injected packet length average = 17.9839
Accepted packet length average = 18.0398
Total in-flight flits = 38813 (38813 measured)
latency change    = 0.0876032
throughput change = 0.000444817
Class 0:
Packet latency average = 1169.92
	minimum = 22
	maximum = 5863
Network latency average = 661.033
	minimum = 22
	maximum = 3760
Slowest packet = 9379
Flit latency average = 624.152
	minimum = 5
	maximum = 4780
Slowest flit = 234955
Fragmentation average = 58.1812
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0152882
	minimum = 0.00533333 (at node 177)
	maximum = 0.026 (at node 126)
Accepted packet rate average = 0.0146936
	minimum = 0.0113333 (at node 79)
	maximum = 0.0186667 (at node 129)
Injected flit rate average = 0.274979
	minimum = 0.0983333 (at node 177)
	maximum = 0.465167 (at node 126)
Accepted flit rate average= 0.264748
	minimum = 0.204667 (at node 79)
	maximum = 0.339833 (at node 129)
Injected packet length average = 17.9864
Accepted packet length average = 18.018
Total in-flight flits = 39682 (39682 measured)
latency change    = 0.0777474
throughput change = 0.000477393
Class 0:
Packet latency average = 1258.93
	minimum = 22
	maximum = 6487
Network latency average = 684.921
	minimum = 22
	maximum = 4915
Slowest packet = 9379
Flit latency average = 643.311
	minimum = 5
	maximum = 4898
Slowest flit = 234971
Fragmentation average = 59.4088
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0152411
	minimum = 0.00457143 (at node 177)
	maximum = 0.0244286 (at node 126)
Accepted packet rate average = 0.0146815
	minimum = 0.0114286 (at node 79)
	maximum = 0.0187143 (at node 128)
Injected flit rate average = 0.274193
	minimum = 0.0842857 (at node 177)
	maximum = 0.439714 (at node 126)
Accepted flit rate average= 0.264615
	minimum = 0.207857 (at node 79)
	maximum = 0.334714 (at node 128)
Injected packet length average = 17.9904
Accepted packet length average = 18.0236
Total in-flight flits = 40744 (40744 measured)
latency change    = 0.0707
throughput change = 0.00050519
Draining all recorded packets ...
Class 0:
Remaining flits: 419940 419941 419942 419943 419944 419945 419946 419947 419948 419949 [...] (44151 flits)
Measured flits: 419940 419941 419942 419943 419944 419945 419946 419947 419948 419949 [...] (28272 flits)
Class 0:
Remaining flits: 496149 496150 496151 498924 498925 498926 498927 498928 498929 498930 [...] (43929 flits)
Measured flits: 496149 496150 496151 498924 498925 498926 498927 498928 498929 498930 [...] (17992 flits)
Class 0:
Remaining flits: 516222 516223 516224 516225 516226 516227 516228 516229 516230 516231 [...] (44246 flits)
Measured flits: 516222 516223 516224 516225 516226 516227 516228 516229 516230 516231 [...] (11722 flits)
Class 0:
Remaining flits: 526788 526789 526790 526791 526792 526793 526794 526795 526796 526797 [...] (44230 flits)
Measured flits: 526788 526789 526790 526791 526792 526793 526794 526795 526796 526797 [...] (7294 flits)
Class 0:
Remaining flits: 554148 554149 554150 554151 554152 554153 554154 554155 554156 554157 [...] (41675 flits)
Measured flits: 554148 554149 554150 554151 554152 554153 554154 554155 554156 554157 [...] (3846 flits)
Class 0:
Remaining flits: 617958 617959 617960 617961 617962 617963 617964 617965 617966 617967 [...] (42683 flits)
Measured flits: 658142 658143 658144 658145 658146 658147 658148 658149 658150 658151 [...] (2232 flits)
Class 0:
Remaining flits: 700596 700597 700598 700599 700600 700601 700602 700603 700604 700605 [...] (43545 flits)
Measured flits: 700596 700597 700598 700599 700600 700601 700602 700603 700604 700605 [...] (1319 flits)
Class 0:
Remaining flits: 712224 712225 712226 712227 712228 712229 712230 712231 712232 712233 [...] (44161 flits)
Measured flits: 808938 808939 808940 808941 808942 808943 808944 808945 808946 808947 [...] (638 flits)
Class 0:
Remaining flits: 749848 749849 749850 749851 749852 749853 749854 749855 749856 749857 [...] (44554 flits)
Measured flits: 880687 880688 880689 880690 880691 880692 880693 880694 880695 880696 [...] (586 flits)
Class 0:
Remaining flits: 809550 809551 809552 809553 809554 809555 809556 809557 809558 809559 [...] (44035 flits)
Measured flits: 912351 912352 912353 912354 912355 912356 912357 912358 912359 912360 [...] (238 flits)
Class 0:
Remaining flits: 809564 809565 809566 809567 819270 819271 819272 819273 819274 819275 [...] (43985 flits)
Measured flits: 982062 982063 982064 982065 982066 982067 982068 982069 982070 982071 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 917748 917749 917750 917751 917752 917753 917754 917755 917756 917757 [...] (6755 flits)
Measured flits: (0 flits)
Time taken is 23245 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1869.26 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12448 (1 samples)
Network latency average = 769.684 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5060 (1 samples)
Flit latency average = 763.705 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5745 (1 samples)
Fragmentation average = 64.2168 (1 samples)
	minimum = 0 (1 samples)
	maximum = 367 (1 samples)
Injected packet rate average = 0.0152411 (1 samples)
	minimum = 0.00457143 (1 samples)
	maximum = 0.0244286 (1 samples)
Accepted packet rate average = 0.0146815 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.274193 (1 samples)
	minimum = 0.0842857 (1 samples)
	maximum = 0.439714 (1 samples)
Accepted flit rate average = 0.264615 (1 samples)
	minimum = 0.207857 (1 samples)
	maximum = 0.334714 (1 samples)
Injected packet size average = 17.9904 (1 samples)
Accepted packet size average = 18.0236 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 26.1589
