BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.161
	minimum = 23
	maximum = 964
Network latency average = 204.648
	minimum = 22
	maximum = 729
Slowest packet = 85
Flit latency average = 176.589
	minimum = 5
	maximum = 770
Slowest flit = 15870
Fragmentation average = 38.2824
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.0129479
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.243104
	minimum = 0.095 (at node 174)
	maximum = 0.404 (at node 44)
Injected packet length average = 17.8416
Accepted packet length average = 18.7755
Total in-flight flits = 32992 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 503.083
	minimum = 23
	maximum = 1901
Network latency average = 382.149
	minimum = 22
	maximum = 1434
Slowest packet = 85
Flit latency average = 349.978
	minimum = 5
	maximum = 1417
Slowest flit = 37295
Fragmentation average = 49.6044
	minimum = 0
	maximum = 368
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0140495
	minimum = 0.0085 (at node 81)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.258258
	minimum = 0.1585 (at node 164)
	maximum = 0.405 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 18.382
Total in-flight flits = 63153 (0 measured)
latency change    = 0.411309
throughput change = 0.0586764
Class 0:
Packet latency average = 1038.71
	minimum = 25
	maximum = 2440
Network latency average = 884.234
	minimum = 22
	maximum = 2022
Slowest packet = 2921
Flit latency average = 844.616
	minimum = 5
	maximum = 2120
Slowest flit = 67327
Fragmentation average = 71.3735
	minimum = 0
	maximum = 390
Injected packet rate average = 0.0248021
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0153802
	minimum = 0.005 (at node 163)
	maximum = 0.028 (at node 3)
Injected flit rate average = 0.4465
	minimum = 0 (at node 0)
	maximum = 1 (at node 60)
Accepted flit rate average= 0.277729
	minimum = 0.095 (at node 163)
	maximum = 0.499 (at node 3)
Injected packet length average = 18.0025
Accepted packet length average = 18.0576
Total in-flight flits = 95545 (0 measured)
latency change    = 0.515667
throughput change = 0.0701091
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 321.629
	minimum = 26
	maximum = 1168
Network latency average = 169.517
	minimum = 22
	maximum = 987
Slowest packet = 13795
Flit latency average = 1167.73
	minimum = 5
	maximum = 2625
Slowest flit = 110131
Fragmentation average = 16.4175
	minimum = 0
	maximum = 209
Injected packet rate average = 0.0253698
	minimum = 0 (at node 40)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0155104
	minimum = 0.006 (at node 189)
	maximum = 0.028 (at node 182)
Injected flit rate average = 0.456693
	minimum = 0 (at node 40)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.281417
	minimum = 0.136 (at node 189)
	maximum = 0.469 (at node 182)
Injected packet length average = 18.0014
Accepted packet length average = 18.1437
Total in-flight flits = 129191 (78035 measured)
latency change    = 2.22954
throughput change = 0.0131033
Class 0:
Packet latency average = 739.812
	minimum = 25
	maximum = 2384
Network latency average = 599.466
	minimum = 22
	maximum = 1964
Slowest packet = 13795
Flit latency average = 1352.23
	minimum = 5
	maximum = 3107
Slowest flit = 151751
Fragmentation average = 35.182
	minimum = 0
	maximum = 345
Injected packet rate average = 0.0246979
	minimum = 0.002 (at node 86)
	maximum = 0.0545 (at node 117)
Accepted packet rate average = 0.0154844
	minimum = 0.0085 (at node 36)
	maximum = 0.024 (at node 95)
Injected flit rate average = 0.444807
	minimum = 0.0405 (at node 86)
	maximum = 0.9805 (at node 117)
Accepted flit rate average= 0.279945
	minimum = 0.1605 (at node 36)
	maximum = 0.421 (at node 95)
Injected packet length average = 18.0099
Accepted packet length average = 18.0792
Total in-flight flits = 158758 (140484 measured)
latency change    = 0.565255
throughput change = 0.00525586
Class 0:
Packet latency average = 1272.91
	minimum = 22
	maximum = 3791
Network latency average = 1128.97
	minimum = 22
	maximum = 2989
Slowest packet = 13795
Flit latency average = 1519.14
	minimum = 5
	maximum = 3760
Slowest flit = 179117
Fragmentation average = 48.9704
	minimum = 0
	maximum = 386
Injected packet rate average = 0.0242535
	minimum = 0.00566667 (at node 171)
	maximum = 0.052 (at node 88)
Accepted packet rate average = 0.0154583
	minimum = 0.01 (at node 36)
	maximum = 0.0213333 (at node 95)
Injected flit rate average = 0.436701
	minimum = 0.102 (at node 171)
	maximum = 0.935 (at node 88)
Accepted flit rate average= 0.279243
	minimum = 0.184333 (at node 36)
	maximum = 0.384 (at node 128)
Injected packet length average = 18.0057
Accepted packet length average = 18.0642
Total in-flight flits = 186161 (182903 measured)
latency change    = 0.418804
throughput change = 0.00251486
Class 0:
Packet latency average = 1730.96
	minimum = 22
	maximum = 4555
Network latency average = 1578.53
	minimum = 22
	maximum = 3913
Slowest packet = 13795
Flit latency average = 1695.31
	minimum = 5
	maximum = 4484
Slowest flit = 204173
Fragmentation average = 63.5388
	minimum = 0
	maximum = 386
Injected packet rate average = 0.0240182
	minimum = 0.009 (at node 46)
	maximum = 0.04525 (at node 152)
Accepted packet rate average = 0.0154701
	minimum = 0.01025 (at node 36)
	maximum = 0.0205 (at node 95)
Injected flit rate average = 0.432365
	minimum = 0.162 (at node 46)
	maximum = 0.81175 (at node 152)
Accepted flit rate average= 0.279306
	minimum = 0.19025 (at node 36)
	maximum = 0.36825 (at node 118)
Injected packet length average = 18.0015
Accepted packet length average = 18.0546
Total in-flight flits = 213066 (212721 measured)
latency change    = 0.264619
throughput change = 0.000225323
Class 0:
Packet latency average = 2060.35
	minimum = 22
	maximum = 5602
Network latency average = 1903.47
	minimum = 22
	maximum = 4871
Slowest packet = 13795
Flit latency average = 1873.59
	minimum = 5
	maximum = 5165
Slowest flit = 232476
Fragmentation average = 71.2632
	minimum = 0
	maximum = 433
Injected packet rate average = 0.0241313
	minimum = 0.0078 (at node 99)
	maximum = 0.044 (at node 13)
Accepted packet rate average = 0.0155125
	minimum = 0.012 (at node 104)
	maximum = 0.0198 (at node 138)
Injected flit rate average = 0.434378
	minimum = 0.1404 (at node 99)
	maximum = 0.7902 (at node 13)
Accepted flit rate average= 0.279877
	minimum = 0.2196 (at node 149)
	maximum = 0.3592 (at node 138)
Injected packet length average = 18.0006
Accepted packet length average = 18.042
Total in-flight flits = 243851 (243836 measured)
latency change    = 0.159872
throughput change = 0.00204052
Class 0:
Packet latency average = 2321.54
	minimum = 22
	maximum = 5985
Network latency average = 2162.23
	minimum = 22
	maximum = 5721
Slowest packet = 13795
Flit latency average = 2050.93
	minimum = 5
	maximum = 5802
Slowest flit = 264011
Fragmentation average = 74.9567
	minimum = 0
	maximum = 450
Injected packet rate average = 0.0242769
	minimum = 0.00883333 (at node 99)
	maximum = 0.0458333 (at node 47)
Accepted packet rate average = 0.0155712
	minimum = 0.012 (at node 80)
	maximum = 0.0201667 (at node 138)
Injected flit rate average = 0.43702
	minimum = 0.159 (at node 99)
	maximum = 0.824167 (at node 47)
Accepted flit rate average= 0.280615
	minimum = 0.215667 (at node 80)
	maximum = 0.364667 (at node 138)
Injected packet length average = 18.0015
Accepted packet length average = 18.0215
Total in-flight flits = 275682 (275682 measured)
latency change    = 0.112509
throughput change = 0.00263125
Class 0:
Packet latency average = 2533.15
	minimum = 22
	maximum = 7073
Network latency average = 2370.41
	minimum = 22
	maximum = 6395
Slowest packet = 13795
Flit latency average = 2215.69
	minimum = 5
	maximum = 6429
Slowest flit = 294766
Fragmentation average = 75.2691
	minimum = 0
	maximum = 450
Injected packet rate average = 0.0246682
	minimum = 0.0102857 (at node 37)
	maximum = 0.0462857 (at node 47)
Accepted packet rate average = 0.0156057
	minimum = 0.012 (at node 67)
	maximum = 0.02 (at node 138)
Injected flit rate average = 0.444052
	minimum = 0.185571 (at node 37)
	maximum = 0.833143 (at node 47)
Accepted flit rate average= 0.281319
	minimum = 0.217857 (at node 67)
	maximum = 0.362429 (at node 138)
Injected packet length average = 18.001
Accepted packet length average = 18.0267
Total in-flight flits = 314224 (314224 measured)
latency change    = 0.0835363
throughput change = 0.00250159
Draining all recorded packets ...
Class 0:
Remaining flits: 320462 320463 320464 320465 320466 320467 320468 320469 320470 320471 [...] (345903 flits)
Measured flits: 320462 320463 320464 320465 320466 320467 320468 320469 320470 320471 [...] (282470 flits)
Class 0:
Remaining flits: 366453 366454 366455 366456 366457 366458 366459 366460 366461 367953 [...] (376059 flits)
Measured flits: 366453 366454 366455 366456 366457 366458 366459 366460 366461 367953 [...] (235421 flits)
Class 0:
Remaining flits: 409919 409920 409921 409922 409923 409924 409925 409926 409927 409928 [...] (403651 flits)
Measured flits: 409919 409920 409921 409922 409923 409924 409925 409926 409927 409928 [...] (189473 flits)
Class 0:
Remaining flits: 464976 464977 464978 464979 464980 464981 464982 464983 464984 464985 [...] (436007 flits)
Measured flits: 464976 464977 464978 464979 464980 464981 464982 464983 464984 464985 [...] (144656 flits)
Class 0:
Remaining flits: 498456 498457 498458 498459 498460 498461 498462 498463 498464 498465 [...] (464430 flits)
Measured flits: 498456 498457 498458 498459 498460 498461 498462 498463 498464 498465 [...] (103242 flits)
Class 0:
Remaining flits: 528176 528177 528178 528179 528180 528181 528182 528183 528184 528185 [...] (492639 flits)
Measured flits: 528176 528177 528178 528179 528180 528181 528182 528183 528184 528185 [...] (65553 flits)
Class 0:
Remaining flits: 575388 575389 575390 575391 575392 575393 575394 575395 575396 575397 [...] (517406 flits)
Measured flits: 575388 575389 575390 575391 575392 575393 575394 575395 575396 575397 [...] (37741 flits)
Class 0:
Remaining flits: 601218 601219 601220 601221 601222 601223 601224 601225 601226 601227 [...] (546075 flits)
Measured flits: 601218 601219 601220 601221 601222 601223 601224 601225 601226 601227 [...] (19585 flits)
Class 0:
Remaining flits: 621443 621444 621445 621446 621447 621448 621449 627750 627751 627752 [...] (574886 flits)
Measured flits: 621443 621444 621445 621446 621447 621448 621449 627750 627751 627752 [...] (8936 flits)
Class 0:
Remaining flits: 690901 690902 690903 690904 690905 690906 690907 690908 690909 690910 [...] (597387 flits)
Measured flits: 690901 690902 690903 690904 690905 690906 690907 690908 690909 690910 [...] (3458 flits)
Class 0:
Remaining flits: 753192 753193 753194 753195 753196 753197 753198 753199 753200 753201 [...] (628085 flits)
Measured flits: 753192 753193 753194 753195 753196 753197 753198 753199 753200 753201 [...] (1109 flits)
Class 0:
Remaining flits: 777481 777482 777483 777484 777485 777486 777487 777488 777489 777490 [...] (660513 flits)
Measured flits: 777481 777482 777483 777484 777485 777486 777487 777488 777489 777490 [...] (292 flits)
Class 0:
Remaining flits: 808353 808354 808355 808356 808357 808358 808359 808360 808361 814820 [...] (692549 flits)
Measured flits: 808353 808354 808355 808356 808357 808358 808359 808360 808361 814820 [...] (35 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 879246 879247 879248 879249 879250 879251 879252 879253 879254 879255 [...] (653420 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 893478 893479 893480 893481 893482 893483 907578 907579 907580 907581 [...] (606056 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 930265 930266 930267 930268 930269 930270 930271 930272 930273 930274 [...] (558820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 950098 950099 950100 950101 950102 950103 950104 950105 950106 950107 [...] (511435 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 964782 964783 964784 964785 964786 964787 964788 964789 964790 964791 [...] (463978 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1020348 1020349 1020350 1020351 1020352 1020353 1020354 1020355 1020356 1020357 [...] (416252 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1058659 1058660 1058661 1058662 1058663 1058664 1058665 1058666 1058667 1058668 [...] (368357 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1118946 1118947 1118948 1118949 1118950 1118951 1121559 1121560 1121561 1125396 [...] (320614 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1164901 1164902 1164903 1164904 1164905 1170347 1170348 1170349 1170350 1170351 [...] (273319 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1190178 1190179 1190180 1190181 1190182 1190183 1190184 1190185 1190186 1190187 [...] (226239 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1197232 1197233 1209469 1209470 1209471 1209472 1209473 1215420 1215421 1215422 [...] (179990 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1247045 1247046 1247047 1247048 1247049 1247050 1247051 1247052 1247053 1247054 [...] (137725 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1358411 1358412 1358413 1358414 1358415 1358416 1358417 1358418 1358419 1358420 [...] (100471 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1431252 1431253 1431254 1431255 1431256 1431257 1431258 1431259 1431260 1431261 [...] (71067 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1466467 1466468 1466469 1466470 1466471 1466472 1466473 1466474 1466475 1466476 [...] (46597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492128 1492129 1492130 1492131 1492132 1492133 1492134 1492135 1492136 1492137 [...] (26689 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505826 1505827 1505828 1505829 1505830 1505831 1505832 1505833 1505834 1505835 [...] (15834 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1537704 1537705 1537706 1537707 1537708 1537709 1537710 1537711 1537712 1537713 [...] (9841 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1537720 1537721 1543608 1543609 1543610 1543611 1543612 1543613 1543614 1543615 [...] (6839 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1588050 1588051 1588052 1588053 1588054 1588055 1588056 1588057 1588058 1588059 [...] (3949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1732105 1732106 1732107 1732108 1732109 1732110 1732111 1732112 1732113 1732114 [...] (1956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1785255 1785256 1785257 1800846 1800847 1800848 1800849 1800850 1800851 1800852 [...] (677 flits)
Measured flits: (0 flits)
Time taken is 46135 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4382.37 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13706 (1 samples)
Network latency average = 4199.07 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13525 (1 samples)
Flit latency average = 7558.65 (1 samples)
	minimum = 5 (1 samples)
	maximum = 24944 (1 samples)
Fragmentation average = 93.3366 (1 samples)
	minimum = 0 (1 samples)
	maximum = 522 (1 samples)
Injected packet rate average = 0.0246682 (1 samples)
	minimum = 0.0102857 (1 samples)
	maximum = 0.0462857 (1 samples)
Accepted packet rate average = 0.0156057 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.444052 (1 samples)
	minimum = 0.185571 (1 samples)
	maximum = 0.833143 (1 samples)
Accepted flit rate average = 0.281319 (1 samples)
	minimum = 0.217857 (1 samples)
	maximum = 0.362429 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 18.0267 (1 samples)
Hops average = 5.05978 (1 samples)
Total run time 34.8655
