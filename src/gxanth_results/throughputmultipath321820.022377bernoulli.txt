BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.058
	minimum = 22
	maximum = 739
Network latency average = 210.392
	minimum = 22
	maximum = 739
Slowest packet = 1081
Flit latency average = 178.152
	minimum = 5
	maximum = 722
Slowest flit = 19475
Fragmentation average = 41.0084
	minimum = 0
	maximum = 199
Injected packet rate average = 0.0220677
	minimum = 0.012 (at node 120)
	maximum = 0.034 (at node 29)
Accepted packet rate average = 0.013625
	minimum = 0.004 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.393286
	minimum = 0.209 (at node 120)
	maximum = 0.612 (at node 29)
Accepted flit rate average= 0.254682
	minimum = 0.072 (at node 174)
	maximum = 0.45 (at node 44)
Injected packet length average = 17.8218
Accepted packet length average = 18.6923
Total in-flight flits = 27529 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 376.22
	minimum = 22
	maximum = 1384
Network latency average = 362.97
	minimum = 22
	maximum = 1384
Slowest packet = 2622
Flit latency average = 325.988
	minimum = 5
	maximum = 1451
Slowest flit = 42018
Fragmentation average = 48.7344
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0207917
	minimum = 0.011 (at node 72)
	maximum = 0.027 (at node 22)
Accepted packet rate average = 0.0143932
	minimum = 0.0085 (at node 153)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.372336
	minimum = 0.195 (at node 120)
	maximum = 0.4855 (at node 22)
Accepted flit rate average= 0.26407
	minimum = 0.158 (at node 153)
	maximum = 0.3805 (at node 22)
Injected packet length average = 17.9079
Accepted packet length average = 18.3468
Total in-flight flits = 42867 (0 measured)
latency change    = 0.425713
throughput change = 0.0355512
Class 0:
Packet latency average = 830.333
	minimum = 22
	maximum = 1971
Network latency average = 741.328
	minimum = 22
	maximum = 1971
Slowest packet = 4166
Flit latency average = 697.343
	minimum = 5
	maximum = 1936
Slowest flit = 74993
Fragmentation average = 60.9862
	minimum = 0
	maximum = 318
Injected packet rate average = 0.0168542
	minimum = 0 (at node 71)
	maximum = 0.032 (at node 77)
Accepted packet rate average = 0.0151146
	minimum = 0.006 (at node 40)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.302063
	minimum = 0 (at node 71)
	maximum = 0.576 (at node 77)
Accepted flit rate average= 0.27288
	minimum = 0.127 (at node 20)
	maximum = 0.481 (at node 16)
Injected packet length average = 17.9221
Accepted packet length average = 18.0541
Total in-flight flits = 49010 (0 measured)
latency change    = 0.546904
throughput change = 0.0322848
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 836.263
	minimum = 25
	maximum = 2289
Network latency average = 366.722
	minimum = 22
	maximum = 954
Slowest packet = 11279
Flit latency average = 847.504
	minimum = 5
	maximum = 2652
Slowest flit = 100423
Fragmentation average = 34.6817
	minimum = 0
	maximum = 202
Injected packet rate average = 0.0154115
	minimum = 0.001 (at node 132)
	maximum = 0.026 (at node 174)
Accepted packet rate average = 0.0149948
	minimum = 0.006 (at node 4)
	maximum = 0.028 (at node 129)
Injected flit rate average = 0.277177
	minimum = 0.001 (at node 132)
	maximum = 0.467 (at node 174)
Accepted flit rate average= 0.27176
	minimum = 0.114 (at node 74)
	maximum = 0.5 (at node 129)
Injected packet length average = 17.9851
Accepted packet length average = 18.1237
Total in-flight flits = 50238 (41333 measured)
latency change    = 0.00709121
throughput change = 0.00412051
Class 0:
Packet latency average = 1272.36
	minimum = 25
	maximum = 3128
Network latency average = 745.441
	minimum = 22
	maximum = 1916
Slowest packet = 11279
Flit latency average = 885.759
	minimum = 5
	maximum = 3061
Slowest flit = 118061
Fragmentation average = 58.8197
	minimum = 0
	maximum = 277
Injected packet rate average = 0.0155417
	minimum = 0.0045 (at node 96)
	maximum = 0.023 (at node 133)
Accepted packet rate average = 0.0150104
	minimum = 0.0105 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.279799
	minimum = 0.081 (at node 96)
	maximum = 0.4135 (at node 141)
Accepted flit rate average= 0.270685
	minimum = 0.184 (at node 42)
	maximum = 0.4315 (at node 129)
Injected packet length average = 18.0032
Accepted packet length average = 18.0331
Total in-flight flits = 52617 (52197 measured)
latency change    = 0.342744
throughput change = 0.00397333
Class 0:
Packet latency average = 1521.83
	minimum = 25
	maximum = 3718
Network latency average = 863.459
	minimum = 22
	maximum = 2771
Slowest packet = 11279
Flit latency average = 903.571
	minimum = 5
	maximum = 3654
Slowest flit = 162791
Fragmentation average = 64.9986
	minimum = 0
	maximum = 296
Injected packet rate average = 0.0153038
	minimum = 0.00533333 (at node 132)
	maximum = 0.0223333 (at node 34)
Accepted packet rate average = 0.0149757
	minimum = 0.01 (at node 135)
	maximum = 0.0216667 (at node 129)
Injected flit rate average = 0.275175
	minimum = 0.0943333 (at node 132)
	maximum = 0.403667 (at node 34)
Accepted flit rate average= 0.270571
	minimum = 0.181 (at node 135)
	maximum = 0.386667 (at node 129)
Injected packet length average = 17.9808
Accepted packet length average = 18.0674
Total in-flight flits = 51795 (51741 measured)
latency change    = 0.163929
throughput change = 0.000420279
Draining remaining packets ...
Class 0:
Remaining flits: 219564 219565 219566 219567 219568 219569 219570 219571 219572 219573 [...] (6935 flits)
Measured flits: 219564 219565 219566 219567 219568 219569 219570 219571 219572 219573 [...] (6935 flits)
Time taken is 7779 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1780.03 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4638 (1 samples)
Network latency average = 996.557 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4048 (1 samples)
Flit latency average = 973.405 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3939 (1 samples)
Fragmentation average = 69.2265 (1 samples)
	minimum = 0 (1 samples)
	maximum = 308 (1 samples)
Injected packet rate average = 0.0153038 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0149757 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.275175 (1 samples)
	minimum = 0.0943333 (1 samples)
	maximum = 0.403667 (1 samples)
Accepted flit rate average = 0.270571 (1 samples)
	minimum = 0.181 (1 samples)
	maximum = 0.386667 (1 samples)
Injected packet size average = 17.9808 (1 samples)
Accepted packet size average = 18.0674 (1 samples)
Hops average = 5.08896 (1 samples)
Total run time 8.65854
