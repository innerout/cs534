BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 323.8
	minimum = 22
	maximum = 969
Network latency average = 218.701
	minimum = 22
	maximum = 753
Slowest packet = 7
Flit latency average = 190.91
	minimum = 5
	maximum = 742
Slowest flit = 20147
Fragmentation average = 38.5746
	minimum = 0
	maximum = 469
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135052
	minimum = 0.006 (at node 25)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.253755
	minimum = 0.119 (at node 25)
	maximum = 0.424 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 18.7894
Total in-flight flits = 39497 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 549.337
	minimum = 22
	maximum = 1909
Network latency average = 405.838
	minimum = 22
	maximum = 1505
Slowest packet = 7
Flit latency average = 374.931
	minimum = 5
	maximum = 1488
Slowest flit = 39635
Fragmentation average = 56.5117
	minimum = 0
	maximum = 611
Injected packet rate average = 0.0268229
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.0144427
	minimum = 0.0085 (at node 83)
	maximum = 0.02 (at node 167)
Injected flit rate average = 0.480539
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.267914
	minimum = 0.153 (at node 153)
	maximum = 0.3635 (at node 167)
Injected packet length average = 17.9152
Accepted packet length average = 18.5501
Total in-flight flits = 82521 (0 measured)
latency change    = 0.410563
throughput change = 0.0528485
Class 0:
Packet latency average = 1146.09
	minimum = 22
	maximum = 2719
Network latency average = 945.599
	minimum = 22
	maximum = 2284
Slowest packet = 3638
Flit latency average = 904.502
	minimum = 5
	maximum = 2267
Slowest flit = 56807
Fragmentation average = 104.101
	minimum = 0
	maximum = 747
Injected packet rate average = 0.0294115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0159063
	minimum = 0.007 (at node 117)
	maximum = 0.026 (at node 99)
Injected flit rate average = 0.529307
	minimum = 0 (at node 8)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.289151
	minimum = 0.141 (at node 117)
	maximum = 0.471 (at node 181)
Injected packet length average = 17.9966
Accepted packet length average = 18.1785
Total in-flight flits = 128650 (0 measured)
latency change    = 0.520688
throughput change = 0.073446
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 294.546
	minimum = 23
	maximum = 1422
Network latency average = 74.1727
	minimum = 22
	maximum = 839
Slowest packet = 15951
Flit latency average = 1319.14
	minimum = 5
	maximum = 3148
Slowest flit = 69266
Fragmentation average = 7.88755
	minimum = 0
	maximum = 69
Injected packet rate average = 0.0308802
	minimum = 0.001 (at node 58)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0160833
	minimum = 0.005 (at node 86)
	maximum = 0.031 (at node 62)
Injected flit rate average = 0.555734
	minimum = 0.018 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.290073
	minimum = 0.109 (at node 86)
	maximum = 0.55 (at node 62)
Injected packet length average = 17.9965
Accepted packet length average = 18.0356
Total in-flight flits = 179678 (97637 measured)
latency change    = 2.89105
throughput change = 0.00317808
Class 0:
Packet latency average = 599.069
	minimum = 23
	maximum = 2583
Network latency average = 380.157
	minimum = 22
	maximum = 1978
Slowest packet = 15951
Flit latency average = 1518.9
	minimum = 5
	maximum = 4150
Slowest flit = 72954
Fragmentation average = 22.1011
	minimum = 0
	maximum = 534
Injected packet rate average = 0.0296953
	minimum = 0.003 (at node 58)
	maximum = 0.0555 (at node 6)
Accepted packet rate average = 0.0159401
	minimum = 0.01 (at node 35)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.53482
	minimum = 0.054 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.289326
	minimum = 0.1765 (at node 35)
	maximum = 0.4365 (at node 128)
Injected packet length average = 18.0103
Accepted packet length average = 18.1508
Total in-flight flits = 222803 (180554 measured)
latency change    = 0.508327
throughput change = 0.00258324
Class 0:
Packet latency average = 1184.91
	minimum = 23
	maximum = 3918
Network latency average = 972.484
	minimum = 22
	maximum = 2983
Slowest packet = 15951
Flit latency average = 1726.49
	minimum = 5
	maximum = 4899
Slowest flit = 94260
Fragmentation average = 63.4556
	minimum = 0
	maximum = 672
Injected packet rate average = 0.0294514
	minimum = 0.00466667 (at node 58)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0158507
	minimum = 0.011 (at node 4)
	maximum = 0.0226667 (at node 123)
Injected flit rate average = 0.530122
	minimum = 0.084 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.289559
	minimum = 0.195 (at node 135)
	maximum = 0.407667 (at node 123)
Injected packet length average = 17.9999
Accepted packet length average = 18.2679
Total in-flight flits = 267216 (251015 measured)
latency change    = 0.494417
throughput change = 0.000806423
Draining remaining packets ...
Class 0:
Remaining flits: 113850 113851 113852 113853 113854 113855 113856 113857 113858 113859 [...] (220067 flits)
Measured flits: 287082 287083 287084 287085 287086 287087 287088 287089 287090 287091 [...] (213263 flits)
Class 0:
Remaining flits: 136951 136952 136953 136954 136955 136956 136957 136958 136959 136960 [...] (172723 flits)
Measured flits: 287082 287083 287084 287085 287086 287087 287088 287089 287090 287091 [...] (169183 flits)
Class 0:
Remaining flits: 140591 140592 140593 140594 140595 140596 140597 142308 142309 142310 [...] (125869 flits)
Measured flits: 287094 287095 287096 287097 287098 287099 287280 287281 287282 287283 [...] (124029 flits)
Class 0:
Remaining flits: 200672 200673 200674 200675 200676 200677 200678 200679 200680 200681 [...] (79359 flits)
Measured flits: 287280 287281 287282 287283 287284 287285 287286 287287 287288 287289 [...] (78717 flits)
Class 0:
Remaining flits: 204356 204357 204358 204359 204360 204361 204362 204363 204364 204365 [...] (38795 flits)
Measured flits: 289800 289801 289802 289803 289804 289805 289806 289807 289808 289809 [...] (38604 flits)
Class 0:
Remaining flits: 259463 259464 259465 259466 259467 259468 259469 272801 272802 272803 [...] (15490 flits)
Measured flits: 289800 289801 289802 289803 289804 289805 289806 289807 289808 289809 [...] (15422 flits)
Class 0:
Remaining flits: 280249 280250 280251 280252 280253 280254 280255 280256 280257 280258 [...] (7048 flits)
Measured flits: 289807 289808 289809 289810 289811 289812 289813 289814 289815 289816 [...] (7037 flits)
Class 0:
Remaining flits: 312519 312520 312521 312522 312523 312524 312525 312526 312527 312528 [...] (4226 flits)
Measured flits: 312519 312520 312521 312522 312523 312524 312525 312526 312527 312528 [...] (4226 flits)
Class 0:
Remaining flits: 321821 337032 337033 337034 337035 337036 337037 337038 337039 337040 [...] (2473 flits)
Measured flits: 321821 337032 337033 337034 337035 337036 337037 337038 337039 337040 [...] (2473 flits)
Class 0:
Remaining flits: 348449 348450 348451 348452 348453 348454 348455 348456 348457 348458 [...] (1472 flits)
Measured flits: 348449 348450 348451 348452 348453 348454 348455 348456 348457 348458 [...] (1472 flits)
Class 0:
Remaining flits: 422204 422205 422206 422207 429516 429517 429518 429519 429520 429521 [...] (473 flits)
Measured flits: 422204 422205 422206 422207 429516 429517 429518 429519 429520 429521 [...] (473 flits)
Time taken is 17481 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4232.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13771 (1 samples)
Network latency average = 3982.94 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13014 (1 samples)
Flit latency average = 3432.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12997 (1 samples)
Fragmentation average = 196.079 (1 samples)
	minimum = 0 (1 samples)
	maximum = 936 (1 samples)
Injected packet rate average = 0.0294514 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0158507 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.530122 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.289559 (1 samples)
	minimum = 0.195 (1 samples)
	maximum = 0.407667 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 18.2679 (1 samples)
Hops average = 5.0514 (1 samples)
Total run time 13.4994
