BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 286.53
	minimum = 22
	maximum = 823
Network latency average = 274.053
	minimum = 22
	maximum = 818
Slowest packet = 758
Flit latency average = 240.175
	minimum = 5
	maximum = 814
Slowest flit = 18203
Fragmentation average = 56.2302
	minimum = 0
	maximum = 305
Injected packet rate average = 0.0285938
	minimum = 0.013 (at node 52)
	maximum = 0.04 (at node 53)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 115)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.508839
	minimum = 0.227 (at node 52)
	maximum = 0.719 (at node 66)
Accepted flit rate average= 0.268604
	minimum = 0.111 (at node 115)
	maximum = 0.443 (at node 152)
Injected packet length average = 17.7954
Accepted packet length average = 18.877
Total in-flight flits = 47716 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 555.103
	minimum = 22
	maximum = 1754
Network latency average = 503.074
	minimum = 22
	maximum = 1754
Slowest packet = 1082
Flit latency average = 461.173
	minimum = 5
	maximum = 1737
Slowest flit = 19493
Fragmentation average = 60.4769
	minimum = 0
	maximum = 305
Injected packet rate average = 0.0223464
	minimum = 0.0125 (at node 32)
	maximum = 0.0305 (at node 122)
Accepted packet rate average = 0.0146354
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.399458
	minimum = 0.222 (at node 32)
	maximum = 0.548 (at node 122)
Accepted flit rate average= 0.268328
	minimum = 0.1495 (at node 153)
	maximum = 0.388 (at node 152)
Injected packet length average = 17.8758
Accepted packet length average = 18.3342
Total in-flight flits = 52518 (0 measured)
latency change    = 0.483826
throughput change = 0.00102875
Class 0:
Packet latency average = 1370.16
	minimum = 115
	maximum = 2482
Network latency average = 987.104
	minimum = 22
	maximum = 2441
Slowest packet = 3267
Flit latency average = 932.889
	minimum = 5
	maximum = 2424
Slowest flit = 58823
Fragmentation average = 61.5478
	minimum = 0
	maximum = 311
Injected packet rate average = 0.014875
	minimum = 0.004 (at node 168)
	maximum = 0.027 (at node 55)
Accepted packet rate average = 0.0145938
	minimum = 0.006 (at node 147)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.266698
	minimum = 0.076 (at node 168)
	maximum = 0.488 (at node 55)
Accepted flit rate average= 0.264359
	minimum = 0.108 (at node 190)
	maximum = 0.467 (at node 16)
Injected packet length average = 17.9293
Accepted packet length average = 18.1146
Total in-flight flits = 52917 (0 measured)
latency change    = 0.594864
throughput change = 0.0150127
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1801.27
	minimum = 625
	maximum = 2914
Network latency average = 356.383
	minimum = 22
	maximum = 974
Slowest packet = 11531
Flit latency average = 976.841
	minimum = 5
	maximum = 3038
Slowest flit = 87717
Fragmentation average = 30.6512
	minimum = 0
	maximum = 186
Injected packet rate average = 0.0147031
	minimum = 0.002 (at node 17)
	maximum = 0.03 (at node 186)
Accepted packet rate average = 0.0149792
	minimum = 0.005 (at node 135)
	maximum = 0.026 (at node 90)
Injected flit rate average = 0.265172
	minimum = 0.047 (at node 17)
	maximum = 0.544 (at node 186)
Accepted flit rate average= 0.27001
	minimum = 0.104 (at node 135)
	maximum = 0.454 (at node 90)
Injected packet length average = 18.0351
Accepted packet length average = 18.0257
Total in-flight flits = 51979 (41190 measured)
latency change    = 0.239337
throughput change = 0.020929
Class 0:
Packet latency average = 2319.55
	minimum = 625
	maximum = 3511
Network latency average = 756.777
	minimum = 22
	maximum = 1958
Slowest packet = 11531
Flit latency average = 969.136
	minimum = 5
	maximum = 3584
Slowest flit = 111797
Fragmentation average = 56.3193
	minimum = 0
	maximum = 289
Injected packet rate average = 0.014888
	minimum = 0.005 (at node 0)
	maximum = 0.0235 (at node 154)
Accepted packet rate average = 0.0149714
	minimum = 0.006 (at node 135)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.268164
	minimum = 0.093 (at node 0)
	maximum = 0.421 (at node 186)
Accepted flit rate average= 0.269667
	minimum = 0.1225 (at node 135)
	maximum = 0.3855 (at node 129)
Injected packet length average = 18.0121
Accepted packet length average = 18.0122
Total in-flight flits = 52235 (51677 measured)
latency change    = 0.223439
throughput change = 0.00127472
Class 0:
Packet latency average = 2674.22
	minimum = 625
	maximum = 4424
Network latency average = 888.145
	minimum = 22
	maximum = 2757
Slowest packet = 11531
Flit latency average = 968.86
	minimum = 5
	maximum = 3984
Slowest flit = 152261
Fragmentation average = 62.7348
	minimum = 0
	maximum = 289
Injected packet rate average = 0.0149236
	minimum = 0.00566667 (at node 0)
	maximum = 0.0243333 (at node 2)
Accepted packet rate average = 0.0149253
	minimum = 0.009 (at node 135)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.269066
	minimum = 0.104 (at node 0)
	maximum = 0.442 (at node 2)
Accepted flit rate average= 0.26905
	minimum = 0.169 (at node 135)
	maximum = 0.37 (at node 129)
Injected packet length average = 18.0295
Accepted packet length average = 18.0264
Total in-flight flits = 52960 (52904 measured)
latency change    = 0.132626
throughput change = 0.00229072
Class 0:
Packet latency average = 2982.88
	minimum = 625
	maximum = 5473
Network latency average = 939.649
	minimum = 22
	maximum = 3429
Slowest packet = 11531
Flit latency average = 969.708
	minimum = 5
	maximum = 4065
Slowest flit = 159983
Fragmentation average = 66.9742
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0149297
	minimum = 0.0065 (at node 0)
	maximum = 0.0235 (at node 2)
Accepted packet rate average = 0.0149284
	minimum = 0.0105 (at node 64)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.268846
	minimum = 0.11725 (at node 0)
	maximum = 0.423 (at node 2)
Accepted flit rate average= 0.269163
	minimum = 0.189 (at node 64)
	maximum = 0.35175 (at node 128)
Injected packet length average = 18.0075
Accepted packet length average = 18.0303
Total in-flight flits = 52642 (52642 measured)
latency change    = 0.103476
throughput change = 0.00041764
Class 0:
Packet latency average = 3271.93
	minimum = 625
	maximum = 6511
Network latency average = 964.747
	minimum = 22
	maximum = 4121
Slowest packet = 11531
Flit latency average = 970.045
	minimum = 5
	maximum = 4210
Slowest flit = 245792
Fragmentation average = 69.4327
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0149365
	minimum = 0.0082 (at node 0)
	maximum = 0.022 (at node 2)
Accepted packet rate average = 0.0149427
	minimum = 0.011 (at node 64)
	maximum = 0.0192 (at node 128)
Injected flit rate average = 0.268894
	minimum = 0.1478 (at node 0)
	maximum = 0.3978 (at node 2)
Accepted flit rate average= 0.269398
	minimum = 0.2018 (at node 64)
	maximum = 0.3444 (at node 128)
Injected packet length average = 18.0025
Accepted packet length average = 18.0287
Total in-flight flits = 52343 (52343 measured)
latency change    = 0.0883436
throughput change = 0.000872896
Class 0:
Packet latency average = 3556.54
	minimum = 625
	maximum = 6722
Network latency average = 980.418
	minimum = 22
	maximum = 4404
Slowest packet = 11531
Flit latency average = 969.736
	minimum = 5
	maximum = 4424
Slowest flit = 286671
Fragmentation average = 71.2685
	minimum = 0
	maximum = 308
Injected packet rate average = 0.014875
	minimum = 0.00883333 (at node 24)
	maximum = 0.0208333 (at node 2)
Accepted packet rate average = 0.0149358
	minimum = 0.0116667 (at node 42)
	maximum = 0.0188333 (at node 128)
Injected flit rate average = 0.267818
	minimum = 0.160667 (at node 24)
	maximum = 0.3765 (at node 2)
Accepted flit rate average= 0.269153
	minimum = 0.209667 (at node 42)
	maximum = 0.336667 (at node 128)
Injected packet length average = 18.0046
Accepted packet length average = 18.0207
Total in-flight flits = 51139 (51139 measured)
latency change    = 0.0800245
throughput change = 0.00091078
Class 0:
Packet latency average = 3834.87
	minimum = 625
	maximum = 7648
Network latency average = 989.675
	minimum = 22
	maximum = 4596
Slowest packet = 11531
Flit latency average = 968.632
	minimum = 5
	maximum = 4579
Slowest flit = 286685
Fragmentation average = 73.6332
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0148891
	minimum = 0.00828571 (at node 24)
	maximum = 0.0208571 (at node 2)
Accepted packet rate average = 0.0149226
	minimum = 0.012 (at node 63)
	maximum = 0.0187143 (at node 181)
Injected flit rate average = 0.268117
	minimum = 0.149 (at node 24)
	maximum = 0.374714 (at node 2)
Accepted flit rate average= 0.269094
	minimum = 0.213857 (at node 63)
	maximum = 0.336429 (at node 181)
Injected packet length average = 18.0075
Accepted packet length average = 18.0326
Total in-flight flits = 51669 (51669 measured)
latency change    = 0.0725768
throughput change = 0.000219358
Draining all recorded packets ...
Class 0:
Remaining flits: 401598 401599 401600 401601 401602 401603 401604 401605 401606 401607 [...] (51475 flits)
Measured flits: 401598 401599 401600 401601 401602 401603 401604 401605 401606 401607 [...] (51475 flits)
Class 0:
Remaining flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (51590 flits)
Measured flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (51590 flits)
Class 0:
Remaining flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (51792 flits)
Measured flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (51792 flits)
Class 0:
Remaining flits: 580212 580213 580214 580215 580216 580217 580218 580219 580220 580221 [...] (51560 flits)
Measured flits: 580212 580213 580214 580215 580216 580217 580218 580219 580220 580221 [...] (51560 flits)
Class 0:
Remaining flits: 607254 607255 607256 607257 607258 607259 607260 607261 607262 607263 [...] (51874 flits)
Measured flits: 607254 607255 607256 607257 607258 607259 607260 607261 607262 607263 [...] (51874 flits)
Class 0:
Remaining flits: 619956 619957 619958 619959 619960 619961 619962 619963 619964 619965 [...] (51258 flits)
Measured flits: 619956 619957 619958 619959 619960 619961 619962 619963 619964 619965 [...] (51258 flits)
Class 0:
Remaining flits: 677628 677629 677630 677631 677632 677633 677634 677635 677636 677637 [...] (50396 flits)
Measured flits: 677628 677629 677630 677631 677632 677633 677634 677635 677636 677637 [...] (50090 flits)
Class 0:
Remaining flits: 760644 760645 760646 760647 760648 760649 760650 760651 760652 760653 [...] (49993 flits)
Measured flits: 760644 760645 760646 760647 760648 760649 760650 760651 760652 760653 [...] (49079 flits)
Class 0:
Remaining flits: 760644 760645 760646 760647 760648 760649 760650 760651 760652 760653 [...] (50156 flits)
Measured flits: 760644 760645 760646 760647 760648 760649 760650 760651 760652 760653 [...] (45467 flits)
Class 0:
Remaining flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (50172 flits)
Measured flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (38472 flits)
Class 0:
Remaining flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (50163 flits)
Measured flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (29083 flits)
Class 0:
Remaining flits: 852531 852532 852533 941760 941761 941762 941763 941764 941765 941766 [...] (48585 flits)
Measured flits: 852531 852532 852533 941760 941761 941762 941763 941764 941765 941766 [...] (18723 flits)
Class 0:
Remaining flits: 963702 963703 963704 963705 963706 963707 963708 963709 963710 963711 [...] (48513 flits)
Measured flits: 963702 963703 963704 963705 963706 963707 963708 963709 963710 963711 [...] (11195 flits)
Class 0:
Remaining flits: 963702 963703 963704 963705 963706 963707 963708 963709 963710 963711 [...] (48303 flits)
Measured flits: 963702 963703 963704 963705 963706 963707 963708 963709 963710 963711 [...] (6920 flits)
Class 0:
Remaining flits: 1012032 1012033 1012034 1012035 1012036 1012037 1012038 1012039 1012040 1012041 [...] (48613 flits)
Measured flits: 1012032 1012033 1012034 1012035 1012036 1012037 1012038 1012039 1012040 1012041 [...] (5108 flits)
Class 0:
Remaining flits: 1012032 1012033 1012034 1012035 1012036 1012037 1012038 1012039 1012040 1012041 [...] (49117 flits)
Measured flits: 1012032 1012033 1012034 1012035 1012036 1012037 1012038 1012039 1012040 1012041 [...] (3197 flits)
Class 0:
Remaining flits: 1152288 1152289 1152290 1152291 1152292 1152293 1152294 1152295 1152296 1152297 [...] (49556 flits)
Measured flits: 1306062 1306063 1306064 1306065 1306066 1306067 1306068 1306069 1306070 1306071 [...] (1630 flits)
Class 0:
Remaining flits: 1235682 1235683 1235684 1235685 1235686 1235687 1235688 1235689 1235690 1235691 [...] (49509 flits)
Measured flits: 1344906 1344907 1344908 1344909 1344910 1344911 1344912 1344913 1344914 1344915 [...] (973 flits)
Class 0:
Remaining flits: 1274994 1274995 1274996 1274997 1274998 1274999 1275000 1275001 1275002 1275003 [...] (49330 flits)
Measured flits: 1344922 1344923 1403118 1403119 1403120 1403121 1403122 1403123 1403124 1403125 [...] (305 flits)
Class 0:
Remaining flits: 1276031 1276032 1276033 1276034 1276035 1276036 1276037 1328391 1328392 1328393 [...] (49789 flits)
Measured flits: 1434917 1434918 1434919 1434920 1434921 1434922 1434923 1465146 1465147 1465148 [...] (301 flits)
Class 0:
Remaining flits: 1396818 1396819 1396820 1396821 1396822 1396823 1396824 1396825 1396826 1396827 [...] (49240 flits)
Measured flits: 1528830 1528831 1528832 1528833 1528834 1528835 1528836 1528837 1528838 1528839 [...] (270 flits)
Class 0:
Remaining flits: 1468224 1468225 1468226 1468227 1468228 1468229 1468230 1468231 1468232 1468233 [...] (47951 flits)
Measured flits: 1528830 1528831 1528832 1528833 1528834 1528835 1528836 1528837 1528838 1528839 [...] (143 flits)
Class 0:
Remaining flits: 1493388 1493389 1493390 1493391 1493392 1493393 1493394 1493395 1493396 1493397 [...] (48120 flits)
Measured flits: 1569600 1569601 1569602 1569603 1569604 1569605 1569606 1569607 1569608 1569609 [...] (108 flits)
Class 0:
Remaining flits: 1527984 1527985 1527986 1527987 1527988 1527989 1527990 1527991 1527992 1527993 [...] (47384 flits)
Measured flits: 1632492 1632493 1632494 1632495 1632496 1632497 1632498 1632499 1632500 1632501 [...] (93 flits)
Class 0:
Remaining flits: 1565460 1565461 1565462 1565463 1565464 1565465 1565466 1565467 1565468 1565469 [...] (48934 flits)
Measured flits: 1637286 1637287 1637288 1637289 1637290 1637291 1637292 1637293 1637294 1637295 [...] (241 flits)
Class 0:
Remaining flits: 1642798 1642799 1642800 1642801 1642802 1642803 1642804 1642805 1698970 1698971 [...] (49197 flits)
Measured flits: 1854738 1854739 1854740 1854741 1854742 1854743 1854744 1854745 1854746 1854747 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1702314 1702315 1702316 1702317 1702318 1702319 1702320 1702321 1702322 1702323 [...] (8023 flits)
Measured flits: (0 flits)
Time taken is 38168 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7541.83 (1 samples)
	minimum = 625 (1 samples)
	maximum = 26273 (1 samples)
Network latency average = 1011.17 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7895 (1 samples)
Flit latency average = 943.59 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7859 (1 samples)
Fragmentation average = 80.626 (1 samples)
	minimum = 0 (1 samples)
	maximum = 349 (1 samples)
Injected packet rate average = 0.0148891 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0208571 (1 samples)
Accepted packet rate average = 0.0149226 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.268117 (1 samples)
	minimum = 0.149 (1 samples)
	maximum = 0.374714 (1 samples)
Accepted flit rate average = 0.269094 (1 samples)
	minimum = 0.213857 (1 samples)
	maximum = 0.336429 (1 samples)
Injected packet size average = 18.0075 (1 samples)
Accepted packet size average = 18.0326 (1 samples)
Hops average = 5.05983 (1 samples)
Total run time 54.2805
