BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 154.572
	minimum = 25
	maximum = 586
Network latency average = 107.337
	minimum = 22
	maximum = 474
Slowest packet = 45
Flit latency average = 84.5726
	minimum = 5
	maximum = 457
Slowest flit = 19601
Fragmentation average = 12.2953
	minimum = 0
	maximum = 65
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.010125
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.18563
	minimum = 0.036 (at node 174)
	maximum = 0.324 (at node 140)
Injected packet length average = 17.8411
Accepted packet length average = 18.3338
Total in-flight flits = 5489 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 186.932
	minimum = 25
	maximum = 1130
Network latency average = 135.671
	minimum = 22
	maximum = 693
Slowest packet = 45
Flit latency average = 112.212
	minimum = 5
	maximum = 676
Slowest flit = 52631
Fragmentation average = 12.5314
	minimum = 0
	maximum = 76
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.0105703
	minimum = 0.0055 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.191896
	minimum = 0.099 (at node 30)
	maximum = 0.306 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 18.1542
Total in-flight flits = 5908 (0 measured)
latency change    = 0.173116
throughput change = 0.0326512
Class 0:
Packet latency average = 196.889
	minimum = 22
	maximum = 904
Network latency average = 152.581
	minimum = 22
	maximum = 772
Slowest packet = 3187
Flit latency average = 128.097
	minimum = 5
	maximum = 755
Slowest flit = 75021
Fragmentation average = 12.8005
	minimum = 0
	maximum = 71
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0113854
	minimum = 0.004 (at node 28)
	maximum = 0.02 (at node 97)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.204755
	minimum = 0.065 (at node 184)
	maximum = 0.354 (at node 16)
Injected packet length average = 17.9936
Accepted packet length average = 17.984
Total in-flight flits = 5691 (0 measured)
latency change    = 0.0505705
throughput change = 0.0628037
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 203.857
	minimum = 22
	maximum = 871
Network latency average = 153.697
	minimum = 22
	maximum = 793
Slowest packet = 6604
Flit latency average = 145.044
	minimum = 5
	maximum = 813
Slowest flit = 116855
Fragmentation average = 13.2623
	minimum = 0
	maximum = 68
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.011526
	minimum = 0.005 (at node 150)
	maximum = 0.025 (at node 120)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.206755
	minimum = 0.09 (at node 163)
	maximum = 0.44 (at node 120)
Injected packet length average = 18.0247
Accepted packet length average = 17.9381
Total in-flight flits = 6746 (6746 measured)
latency change    = 0.0341782
throughput change = 0.00967328
Class 0:
Packet latency average = 229.363
	minimum = 22
	maximum = 1150
Network latency average = 176.869
	minimum = 22
	maximum = 943
Slowest packet = 6604
Flit latency average = 159.004
	minimum = 5
	maximum = 926
Slowest flit = 160703
Fragmentation average = 13.0784
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.011276
	minimum = 0.0065 (at node 4)
	maximum = 0.019 (at node 182)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.202914
	minimum = 0.117 (at node 4)
	maximum = 0.343 (at node 182)
Injected packet length average = 17.9755
Accepted packet length average = 17.9952
Total in-flight flits = 7890 (7890 measured)
latency change    = 0.111206
throughput change = 0.0189299
Class 0:
Packet latency average = 241.839
	minimum = 22
	maximum = 1150
Network latency average = 188.812
	minimum = 22
	maximum = 1097
Slowest packet = 6604
Flit latency average = 168.059
	minimum = 5
	maximum = 1080
Slowest flit = 166860
Fragmentation average = 12.9476
	minimum = 0
	maximum = 93
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0112378
	minimum = 0.00533333 (at node 4)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.202109
	minimum = 0.0976667 (at node 4)
	maximum = 0.298667 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 17.9847
Total in-flight flits = 6186 (6186 measured)
latency change    = 0.051589
throughput change = 0.00398145
Draining remaining packets ...
Class 0:
Remaining flits: 226672 226673 227988 227989 227990 227991 227992 227993 227994 227995 [...] (292 flits)
Measured flits: 226672 226673 227988 227989 227990 227991 227992 227993 227994 227995 [...] (292 flits)
Time taken is 7337 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 256.669 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1576 (1 samples)
Network latency average = 201.775 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1323 (1 samples)
Flit latency average = 180.42 (1 samples)
	minimum = 5 (1 samples)
	maximum = 1306 (1 samples)
Fragmentation average = 12.9166 (1 samples)
	minimum = 0 (1 samples)
	maximum = 93 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0112378 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.202109 (1 samples)
	minimum = 0.0976667 (1 samples)
	maximum = 0.298667 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 17.9847 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 3.09046
