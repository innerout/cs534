BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.13
	minimum = 25
	maximum = 981
Network latency average = 252.532
	minimum = 25
	maximum = 947
Slowest packet = 140
Flit latency average = 187.92
	minimum = 6
	maximum = 979
Slowest flit = 1258
Fragmentation average = 135.299
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00939583
	minimum = 0.004 (at node 108)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.375682
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.193828
	minimum = 0.087 (at node 108)
	maximum = 0.339 (at node 44)
Injected packet length average = 17.841
Accepted packet length average = 20.6292
Total in-flight flits = 35559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 525.376
	minimum = 25
	maximum = 1923
Network latency average = 433.009
	minimum = 23
	maximum = 1758
Slowest packet = 289
Flit latency average = 352.223
	minimum = 6
	maximum = 1927
Slowest flit = 5236
Fragmentation average = 175.001
	minimum = 0
	maximum = 1603
Injected packet rate average = 0.0211615
	minimum = 0.002 (at node 130)
	maximum = 0.0465 (at node 46)
Accepted packet rate average = 0.0108437
	minimum = 0.004 (at node 83)
	maximum = 0.0175 (at node 44)
Injected flit rate average = 0.379271
	minimum = 0.036 (at node 130)
	maximum = 0.837 (at node 46)
Accepted flit rate average= 0.207922
	minimum = 0.0965 (at node 83)
	maximum = 0.3195 (at node 44)
Injected packet length average = 17.9227
Accepted packet length average = 19.1744
Total in-flight flits = 66444 (0 measured)
latency change    = 0.392568
throughput change = 0.0677839
Class 0:
Packet latency average = 1073.48
	minimum = 24
	maximum = 2883
Network latency average = 943.972
	minimum = 23
	maximum = 2766
Slowest packet = 426
Flit latency average = 860.508
	minimum = 6
	maximum = 2839
Slowest flit = 7894
Fragmentation average = 227.851
	minimum = 0
	maximum = 2162
Injected packet rate average = 0.022151
	minimum = 0 (at node 99)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0123802
	minimum = 0.004 (at node 184)
	maximum = 0.025 (at node 63)
Injected flit rate average = 0.397464
	minimum = 0 (at node 99)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.226464
	minimum = 0.095 (at node 138)
	maximum = 0.454 (at node 63)
Injected packet length average = 17.9433
Accepted packet length average = 18.2924
Total in-flight flits = 99517 (0 measured)
latency change    = 0.510586
throughput change = 0.0818748
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 370.404
	minimum = 31
	maximum = 1765
Network latency average = 231.731
	minimum = 23
	maximum = 933
Slowest packet = 12407
Flit latency average = 1242.04
	minimum = 6
	maximum = 3590
Slowest flit = 21041
Fragmentation average = 78.377
	minimum = 0
	maximum = 652
Injected packet rate average = 0.0211198
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 113)
Accepted packet rate average = 0.0123802
	minimum = 0.005 (at node 79)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.381552
	minimum = 0 (at node 17)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.221255
	minimum = 0.079 (at node 135)
	maximum = 0.364 (at node 95)
Injected packet length average = 18.0661
Accepted packet length average = 17.8717
Total in-flight flits = 130026 (63808 measured)
latency change    = 1.89813
throughput change = 0.0235399
Class 0:
Packet latency average = 756.924
	minimum = 27
	maximum = 2924
Network latency average = 611.399
	minimum = 23
	maximum = 1982
Slowest packet = 12407
Flit latency average = 1451.43
	minimum = 6
	maximum = 4708
Slowest flit = 18914
Fragmentation average = 131.638
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0201172
	minimum = 0.002 (at node 17)
	maximum = 0.0515 (at node 135)
Accepted packet rate average = 0.0121432
	minimum = 0.006 (at node 79)
	maximum = 0.0185 (at node 128)
Injected flit rate average = 0.362742
	minimum = 0.034 (at node 17)
	maximum = 0.931 (at node 135)
Accepted flit rate average= 0.218117
	minimum = 0.109 (at node 80)
	maximum = 0.3275 (at node 128)
Injected packet length average = 18.0315
Accepted packet length average = 17.962
Total in-flight flits = 154810 (112309 measured)
latency change    = 0.510646
throughput change = 0.0143869
Class 0:
Packet latency average = 1152.11
	minimum = 26
	maximum = 4069
Network latency average = 984.872
	minimum = 23
	maximum = 2951
Slowest packet = 12407
Flit latency average = 1632.82
	minimum = 6
	maximum = 5631
Slowest flit = 24412
Fragmentation average = 149.756
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0200417
	minimum = 0.00333333 (at node 151)
	maximum = 0.0423333 (at node 10)
Accepted packet rate average = 0.0120365
	minimum = 0.00766667 (at node 79)
	maximum = 0.0173333 (at node 128)
Injected flit rate average = 0.361019
	minimum = 0.0596667 (at node 151)
	maximum = 0.762 (at node 10)
Accepted flit rate average= 0.215988
	minimum = 0.139333 (at node 80)
	maximum = 0.323 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 17.9445
Total in-flight flits = 182936 (155307 measured)
latency change    = 0.34301
throughput change = 0.00985861
Draining remaining packets ...
Class 0:
Remaining flits: 56592 56593 56594 56595 56596 56597 56598 56599 56600 56601 [...] (147002 flits)
Measured flits: 222840 222841 222842 222843 222844 222845 222846 222847 222848 222849 [...] (128236 flits)
Class 0:
Remaining flits: 56610 56611 56612 56613 56614 56615 56616 56617 56618 56619 [...] (111961 flits)
Measured flits: 222840 222841 222842 222843 222844 222845 222846 222847 222848 222849 [...] (99676 flits)
Class 0:
Remaining flits: 56610 56611 56612 56613 56614 56615 56616 56617 56618 56619 [...] (78474 flits)
Measured flits: 223056 223057 223058 223059 223060 223061 223062 223063 223064 223065 [...] (70944 flits)
Class 0:
Remaining flits: 62012 62013 62014 62015 62016 62017 62018 62019 62020 62021 [...] (46956 flits)
Measured flits: 223056 223057 223058 223059 223060 223061 223062 223063 223064 223065 [...] (42663 flits)
Class 0:
Remaining flits: 77274 77275 77276 77277 77278 77279 77280 77281 77282 77283 [...] (21082 flits)
Measured flits: 223056 223057 223058 223059 223060 223061 223062 223063 223064 223065 [...] (18996 flits)
Class 0:
Remaining flits: 92286 92287 92288 92289 92290 92291 92292 92293 92294 92295 [...] (7041 flits)
Measured flits: 225306 225307 225308 225309 225310 225311 225312 225313 225314 225315 [...] (6111 flits)
Class 0:
Remaining flits: 104112 104113 104114 104115 104116 104117 104118 104119 104120 104121 [...] (630 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (475 flits)
Time taken is 13424 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3707.66 (1 samples)
	minimum = 26 (1 samples)
	maximum = 11145 (1 samples)
Network latency average = 3468.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9801 (1 samples)
Flit latency average = 3289.65 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11710 (1 samples)
Fragmentation average = 179.277 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2762 (1 samples)
Injected packet rate average = 0.0200417 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.0423333 (1 samples)
Accepted packet rate average = 0.0120365 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.361019 (1 samples)
	minimum = 0.0596667 (1 samples)
	maximum = 0.762 (1 samples)
Accepted flit rate average = 0.215988 (1 samples)
	minimum = 0.139333 (1 samples)
	maximum = 0.323 (1 samples)
Injected packet size average = 18.0134 (1 samples)
Accepted packet size average = 17.9445 (1 samples)
Hops average = 5.10965 (1 samples)
Total run time 12.112
