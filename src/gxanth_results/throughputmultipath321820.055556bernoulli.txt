BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 379.367
	minimum = 22
	maximum = 928
Network latency average = 342.239
	minimum = 22
	maximum = 838
Slowest packet = 220
Flit latency average = 295.298
	minimum = 5
	maximum = 841
Slowest flit = 22920
Fragmentation average = 95.3867
	minimum = 0
	maximum = 346
Injected packet rate average = 0.0291406
	minimum = 0.016 (at node 108)
	maximum = 0.041 (at node 174)
Accepted packet rate average = 0.0139688
	minimum = 0.003 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.51787
	minimum = 0.286 (at node 108)
	maximum = 0.727 (at node 174)
Accepted flit rate average= 0.26801
	minimum = 0.084 (at node 174)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.7714
Accepted packet length average = 19.1864
Total in-flight flits = 49810 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 745.615
	minimum = 22
	maximum = 1737
Network latency average = 601.349
	minimum = 22
	maximum = 1737
Slowest packet = 2222
Flit latency average = 542.611
	minimum = 5
	maximum = 1772
Slowest flit = 32827
Fragmentation average = 91.7867
	minimum = 0
	maximum = 361
Injected packet rate average = 0.0220833
	minimum = 0.0125 (at node 92)
	maximum = 0.0315 (at node 191)
Accepted packet rate average = 0.0143828
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.394674
	minimum = 0.2235 (at node 92)
	maximum = 0.56 (at node 191)
Accepted flit rate average= 0.26488
	minimum = 0.144 (at node 153)
	maximum = 0.3985 (at node 152)
Injected packet length average = 17.8721
Accepted packet length average = 18.4164
Total in-flight flits = 51736 (0 measured)
latency change    = 0.491203
throughput change = 0.0118174
Class 0:
Packet latency average = 1815.74
	minimum = 918
	maximum = 2718
Network latency average = 989.029
	minimum = 22
	maximum = 2631
Slowest packet = 3074
Flit latency average = 923.934
	minimum = 5
	maximum = 2599
Slowest flit = 55349
Fragmentation average = 71.8064
	minimum = 0
	maximum = 327
Injected packet rate average = 0.0151771
	minimum = 0.006 (at node 16)
	maximum = 0.03 (at node 2)
Accepted packet rate average = 0.0150365
	minimum = 0.006 (at node 113)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.272844
	minimum = 0.104 (at node 122)
	maximum = 0.541 (at node 2)
Accepted flit rate average= 0.27062
	minimum = 0.11 (at node 36)
	maximum = 0.465 (at node 120)
Injected packet length average = 17.9774
Accepted packet length average = 17.9976
Total in-flight flits = 52427 (0 measured)
latency change    = 0.589361
throughput change = 0.021209
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2573.89
	minimum = 1520
	maximum = 3400
Network latency average = 363.391
	minimum = 22
	maximum = 970
Slowest packet = 11506
Flit latency average = 961.672
	minimum = 5
	maximum = 2971
Slowest flit = 59236
Fragmentation average = 33.1907
	minimum = 0
	maximum = 200
Injected packet rate average = 0.0146979
	minimum = 0.001 (at node 109)
	maximum = 0.03 (at node 122)
Accepted packet rate average = 0.0149688
	minimum = 0.005 (at node 1)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.265521
	minimum = 0.012 (at node 109)
	maximum = 0.533 (at node 122)
Accepted flit rate average= 0.267937
	minimum = 0.095 (at node 1)
	maximum = 0.432 (at node 99)
Injected packet length average = 18.0652
Accepted packet length average = 17.8998
Total in-flight flits = 51797 (40835 measured)
latency change    = 0.294553
throughput change = 0.0100109
Class 0:
Packet latency average = 3147.21
	minimum = 1520
	maximum = 4203
Network latency average = 759.719
	minimum = 22
	maximum = 1952
Slowest packet = 11506
Flit latency average = 968.207
	minimum = 5
	maximum = 3732
Slowest flit = 69029
Fragmentation average = 58.2796
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0148906
	minimum = 0.006 (at node 123)
	maximum = 0.0235 (at node 42)
Accepted packet rate average = 0.0147734
	minimum = 0.01 (at node 2)
	maximum = 0.021 (at node 66)
Injected flit rate average = 0.267799
	minimum = 0.1105 (at node 123)
	maximum = 0.4275 (at node 42)
Accepted flit rate average= 0.266172
	minimum = 0.1715 (at node 5)
	maximum = 0.3675 (at node 178)
Injected packet length average = 17.9844
Accepted packet length average = 18.0169
Total in-flight flits = 52889 (52260 measured)
latency change    = 0.182169
throughput change = 0.0066334
Class 0:
Packet latency average = 3593.76
	minimum = 1520
	maximum = 5059
Network latency average = 892.354
	minimum = 22
	maximum = 2871
Slowest packet = 11506
Flit latency average = 968.669
	minimum = 5
	maximum = 3732
Slowest flit = 69029
Fragmentation average = 66.81
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0148733
	minimum = 0.009 (at node 4)
	maximum = 0.0236667 (at node 122)
Accepted packet rate average = 0.0148438
	minimum = 0.011 (at node 36)
	maximum = 0.0193333 (at node 165)
Injected flit rate average = 0.267906
	minimum = 0.164 (at node 40)
	maximum = 0.424333 (at node 122)
Accepted flit rate average= 0.267347
	minimum = 0.196667 (at node 64)
	maximum = 0.343333 (at node 165)
Injected packet length average = 18.0126
Accepted packet length average = 18.0108
Total in-flight flits = 52641 (52633 measured)
latency change    = 0.124257
throughput change = 0.00439633
Draining remaining packets ...
Class 0:
Remaining flits: 237744 237745 237746 237747 237748 237749 237750 237751 237752 237753 [...] (7336 flits)
Measured flits: 237744 237745 237746 237747 237748 237749 237750 237751 237752 237753 [...] (7336 flits)
Time taken is 7837 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4042.84 (1 samples)
	minimum = 1520 (1 samples)
	maximum = 6472 (1 samples)
Network latency average = 1024.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3647 (1 samples)
Flit latency average = 1026.66 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3732 (1 samples)
Fragmentation average = 70.124 (1 samples)
	minimum = 0 (1 samples)
	maximum = 301 (1 samples)
Injected packet rate average = 0.0148733 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0236667 (1 samples)
Accepted packet rate average = 0.0148438 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.267906 (1 samples)
	minimum = 0.164 (1 samples)
	maximum = 0.424333 (1 samples)
Accepted flit rate average = 0.267347 (1 samples)
	minimum = 0.196667 (1 samples)
	maximum = 0.343333 (1 samples)
Injected packet size average = 18.0126 (1 samples)
Accepted packet size average = 18.0108 (1 samples)
Hops average = 5.10237 (1 samples)
Total run time 9.48276
