BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 363.136
	minimum = 22
	maximum = 941
Network latency average = 327.14
	minimum = 22
	maximum = 922
Slowest packet = 760
Flit latency average = 282.562
	minimum = 5
	maximum = 929
Slowest flit = 7263
Fragmentation average = 84.6786
	minimum = 0
	maximum = 383
Injected packet rate average = 0.0292448
	minimum = 0.014 (at node 104)
	maximum = 0.042 (at node 70)
Accepted packet rate average = 0.0142604
	minimum = 0.006 (at node 62)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.520026
	minimum = 0.252 (at node 104)
	maximum = 0.739 (at node 70)
Accepted flit rate average= 0.270453
	minimum = 0.108 (at node 174)
	maximum = 0.424 (at node 44)
Injected packet length average = 17.7818
Accepted packet length average = 18.9653
Total in-flight flits = 49935 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 708.406
	minimum = 22
	maximum = 1811
Network latency average = 578.282
	minimum = 22
	maximum = 1758
Slowest packet = 1940
Flit latency average = 525.232
	minimum = 5
	maximum = 1741
Slowest flit = 34937
Fragmentation average = 81.6434
	minimum = 0
	maximum = 383
Injected packet rate average = 0.022362
	minimum = 0.0115 (at node 104)
	maximum = 0.0305 (at node 103)
Accepted packet rate average = 0.0145677
	minimum = 0.0085 (at node 153)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.399758
	minimum = 0.207 (at node 104)
	maximum = 0.5485 (at node 142)
Accepted flit rate average= 0.268411
	minimum = 0.153 (at node 153)
	maximum = 0.3915 (at node 63)
Injected packet length average = 17.8767
Accepted packet length average = 18.4251
Total in-flight flits = 52468 (0 measured)
latency change    = 0.487391
throughput change = 0.00760648
Class 0:
Packet latency average = 1758.47
	minimum = 788
	maximum = 2623
Network latency average = 1007.53
	minimum = 22
	maximum = 2469
Slowest packet = 2367
Flit latency average = 943.825
	minimum = 5
	maximum = 2549
Slowest flit = 62302
Fragmentation average = 71.1542
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0150104
	minimum = 0 (at node 63)
	maximum = 0.034 (at node 65)
Accepted packet rate average = 0.0149271
	minimum = 0.008 (at node 21)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.270052
	minimum = 0 (at node 63)
	maximum = 0.602 (at node 65)
Accepted flit rate average= 0.267682
	minimum = 0.15 (at node 190)
	maximum = 0.479 (at node 16)
Injected packet length average = 17.991
Accepted packet length average = 17.9327
Total in-flight flits = 53021 (0 measured)
latency change    = 0.597146
throughput change = 0.002724
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2458.29
	minimum = 1347
	maximum = 3368
Network latency average = 354.983
	minimum = 22
	maximum = 974
Slowest packet = 11561
Flit latency average = 969.852
	minimum = 5
	maximum = 2697
Slowest flit = 91313
Fragmentation average = 32.3512
	minimum = 0
	maximum = 209
Injected packet rate average = 0.0146563
	minimum = 0.002 (at node 10)
	maximum = 0.026 (at node 166)
Accepted packet rate average = 0.0149167
	minimum = 0.005 (at node 139)
	maximum = 0.025 (at node 182)
Injected flit rate average = 0.264276
	minimum = 0.05 (at node 10)
	maximum = 0.479 (at node 166)
Accepted flit rate average= 0.269375
	minimum = 0.09 (at node 139)
	maximum = 0.45 (at node 182)
Injected packet length average = 18.0316
Accepted packet length average = 18.0587
Total in-flight flits = 51899 (41063 measured)
latency change    = 0.284679
throughput change = 0.00628384
Class 0:
Packet latency average = 3044.78
	minimum = 1347
	maximum = 4202
Network latency average = 753.067
	minimum = 22
	maximum = 1923
Slowest packet = 11561
Flit latency average = 960.602
	minimum = 5
	maximum = 3163
Slowest flit = 128105
Fragmentation average = 55.8319
	minimum = 0
	maximum = 274
Injected packet rate average = 0.0147786
	minimum = 0.006 (at node 112)
	maximum = 0.022 (at node 150)
Accepted packet rate average = 0.0148542
	minimum = 0.0075 (at node 139)
	maximum = 0.0215 (at node 103)
Injected flit rate average = 0.266151
	minimum = 0.1145 (at node 112)
	maximum = 0.4 (at node 150)
Accepted flit rate average= 0.268297
	minimum = 0.1505 (at node 139)
	maximum = 0.372 (at node 90)
Injected packet length average = 18.0092
Accepted packet length average = 18.0621
Total in-flight flits = 52055 (51446 measured)
latency change    = 0.192622
throughput change = 0.0040184
Class 0:
Packet latency average = 3470.39
	minimum = 1347
	maximum = 5158
Network latency average = 892.721
	minimum = 22
	maximum = 2920
Slowest packet = 11561
Flit latency average = 965.427
	minimum = 5
	maximum = 3165
Slowest flit = 145313
Fragmentation average = 65.6151
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0148524
	minimum = 0.008 (at node 136)
	maximum = 0.0223333 (at node 15)
Accepted packet rate average = 0.0149028
	minimum = 0.0106667 (at node 64)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.267495
	minimum = 0.144 (at node 136)
	maximum = 0.405333 (at node 15)
Accepted flit rate average= 0.268823
	minimum = 0.181333 (at node 64)
	maximum = 0.377 (at node 128)
Injected packet length average = 18.0102
Accepted packet length average = 18.0384
Total in-flight flits = 52115 (52093 measured)
latency change    = 0.122638
throughput change = 0.00195683
Draining remaining packets ...
Class 0:
Remaining flits: 237690 237691 237692 237693 237694 237695 237696 237697 237698 237699 [...] (7032 flits)
Measured flits: 237690 237691 237692 237693 237694 237695 237696 237697 237698 237699 [...] (7032 flits)
Time taken is 7858 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3900.27 (1 samples)
	minimum = 1347 (1 samples)
	maximum = 6594 (1 samples)
Network latency average = 1016.77 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3551 (1 samples)
Flit latency average = 1017.94 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3533 (1 samples)
Fragmentation average = 69.1831 (1 samples)
	minimum = 0 (1 samples)
	maximum = 320 (1 samples)
Injected packet rate average = 0.0148524 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0149028 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.267495 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.405333 (1 samples)
Accepted flit rate average = 0.268823 (1 samples)
	minimum = 0.181333 (1 samples)
	maximum = 0.377 (1 samples)
Injected packet size average = 18.0102 (1 samples)
Accepted packet size average = 18.0384 (1 samples)
Hops average = 5.1098 (1 samples)
Total run time 9.12754
