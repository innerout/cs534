BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 278.677
	minimum = 22
	maximum = 752
Network latency average = 269.845
	minimum = 22
	maximum = 728
Slowest packet = 811
Flit latency average = 237.255
	minimum = 5
	maximum = 758
Slowest flit = 22602
Fragmentation average = 54.1276
	minimum = 0
	maximum = 350
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.01425
	minimum = 0.006 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.269766
	minimum = 0.12 (at node 174)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8483
Accepted packet length average = 18.9309
Total in-flight flits = 54657 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 511.337
	minimum = 22
	maximum = 1547
Network latency average = 501.458
	minimum = 22
	maximum = 1479
Slowest packet = 2924
Flit latency average = 464.669
	minimum = 5
	maximum = 1533
Slowest flit = 46212
Fragmentation average = 71.2927
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0149453
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.27738
	minimum = 0.15 (at node 153)
	maximum = 0.379 (at node 152)
Injected packet length average = 17.9239
Accepted packet length average = 18.5597
Total in-flight flits = 108064 (0 measured)
latency change    = 0.455003
throughput change = 0.0274518
Class 0:
Packet latency average = 1169.03
	minimum = 23
	maximum = 2342
Network latency average = 1157.64
	minimum = 22
	maximum = 2289
Slowest packet = 3942
Flit latency average = 1121.99
	minimum = 5
	maximum = 2272
Slowest flit = 70973
Fragmentation average = 97.6344
	minimum = 0
	maximum = 492
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0160104
	minimum = 0.005 (at node 126)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.289062
	minimum = 0.108 (at node 96)
	maximum = 0.452 (at node 16)
Injected packet length average = 18.0012
Accepted packet length average = 18.0547
Total in-flight flits = 159484 (0 measured)
latency change    = 0.562596
throughput change = 0.0404144
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 54.341
	minimum = 22
	maximum = 547
Network latency average = 37.4304
	minimum = 22
	maximum = 210
Slowest packet = 18416
Flit latency average = 1565.15
	minimum = 5
	maximum = 2785
Slowest flit = 129130
Fragmentation average = 6.08524
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0307448
	minimum = 0.004 (at node 44)
	maximum = 0.047 (at node 115)
Accepted packet rate average = 0.0160938
	minimum = 0.005 (at node 134)
	maximum = 0.03 (at node 103)
Injected flit rate average = 0.553453
	minimum = 0.084 (at node 44)
	maximum = 0.852 (at node 115)
Accepted flit rate average= 0.290714
	minimum = 0.061 (at node 134)
	maximum = 0.506 (at node 103)
Injected packet length average = 18.0015
Accepted packet length average = 18.0638
Total in-flight flits = 210083 (97686 measured)
latency change    = 20.5128
throughput change = 0.00567927
Class 0:
Packet latency average = 72.223
	minimum = 22
	maximum = 1299
Network latency average = 43.847
	minimum = 22
	maximum = 699
Slowest packet = 18416
Flit latency average = 1800.25
	minimum = 5
	maximum = 3470
Slowest flit = 160333
Fragmentation average = 6.56721
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0295
	minimum = 0.01 (at node 48)
	maximum = 0.041 (at node 165)
Accepted packet rate average = 0.0160104
	minimum = 0.01 (at node 86)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.531172
	minimum = 0.1805 (at node 48)
	maximum = 0.7375 (at node 165)
Accepted flit rate average= 0.287693
	minimum = 0.1675 (at node 86)
	maximum = 0.452 (at node 129)
Injected packet length average = 18.0058
Accepted packet length average = 17.9691
Total in-flight flits = 253238 (187690 measured)
latency change    = 0.247594
throughput change = 0.0105002
Class 0:
Packet latency average = 577.057
	minimum = 22
	maximum = 2990
Network latency average = 525.773
	minimum = 22
	maximum = 2973
Slowest packet = 17971
Flit latency average = 2044.02
	minimum = 5
	maximum = 4230
Slowest flit = 188164
Fragmentation average = 14.4284
	minimum = 0
	maximum = 293
Injected packet rate average = 0.0288646
	minimum = 0.009 (at node 128)
	maximum = 0.0396667 (at node 107)
Accepted packet rate average = 0.0158698
	minimum = 0.0103333 (at node 86)
	maximum = 0.0216667 (at node 129)
Injected flit rate average = 0.519648
	minimum = 0.164 (at node 128)
	maximum = 0.711333 (at node 107)
Accepted flit rate average= 0.284486
	minimum = 0.179 (at node 86)
	maximum = 0.385333 (at node 129)
Injected packet length average = 18.0029
Accepted packet length average = 17.9263
Total in-flight flits = 295230 (271420 measured)
latency change    = 0.874842
throughput change = 0.0112715
Class 0:
Packet latency average = 1801.14
	minimum = 22
	maximum = 3987
Network latency average = 1749.13
	minimum = 22
	maximum = 3964
Slowest packet = 18028
Flit latency average = 2281.71
	minimum = 5
	maximum = 4967
Slowest flit = 192347
Fragmentation average = 39.1693
	minimum = 0
	maximum = 549
Injected packet rate average = 0.0283529
	minimum = 0.0075 (at node 128)
	maximum = 0.0365 (at node 107)
Accepted packet rate average = 0.0157357
	minimum = 0.0115 (at node 86)
	maximum = 0.02025 (at node 157)
Injected flit rate average = 0.51043
	minimum = 0.1365 (at node 128)
	maximum = 0.6575 (at node 174)
Accepted flit rate average= 0.282251
	minimum = 0.205 (at node 86)
	maximum = 0.37025 (at node 157)
Injected packet length average = 18.0028
Accepted packet length average = 17.937
Total in-flight flits = 335187 (328220 measured)
latency change    = 0.679616
throughput change = 0.0079178
Class 0:
Packet latency average = 2534.06
	minimum = 22
	maximum = 4982
Network latency average = 2481.39
	minimum = 22
	maximum = 4969
Slowest packet = 18024
Flit latency average = 2526.88
	minimum = 5
	maximum = 5683
Slowest flit = 196037
Fragmentation average = 52.2689
	minimum = 0
	maximum = 632
Injected packet rate average = 0.0278146
	minimum = 0.0096 (at node 48)
	maximum = 0.0364 (at node 141)
Accepted packet rate average = 0.0156229
	minimum = 0.0118 (at node 190)
	maximum = 0.0208 (at node 103)
Injected flit rate average = 0.500667
	minimum = 0.1728 (at node 48)
	maximum = 0.6556 (at node 141)
Accepted flit rate average= 0.280014
	minimum = 0.2118 (at node 190)
	maximum = 0.3774 (at node 103)
Injected packet length average = 18.0001
Accepted packet length average = 17.9233
Total in-flight flits = 371973 (370228 measured)
latency change    = 0.289227
throughput change = 0.00799161
Class 0:
Packet latency average = 3036.1
	minimum = 22
	maximum = 5895
Network latency average = 2966.57
	minimum = 22
	maximum = 5860
Slowest packet = 18167
Flit latency average = 2769.96
	minimum = 5
	maximum = 6681
Slowest flit = 245987
Fragmentation average = 56.2464
	minimum = 0
	maximum = 723
Injected packet rate average = 0.0266398
	minimum = 0.0101667 (at node 48)
	maximum = 0.0341667 (at node 70)
Accepted packet rate average = 0.015526
	minimum = 0.0118333 (at node 190)
	maximum = 0.0206667 (at node 103)
Injected flit rate average = 0.479542
	minimum = 0.183167 (at node 48)
	maximum = 0.6135 (at node 70)
Accepted flit rate average= 0.278488
	minimum = 0.214333 (at node 190)
	maximum = 0.368167 (at node 103)
Injected packet length average = 18.001
Accepted packet length average = 17.9368
Total in-flight flits = 392436 (392046 measured)
latency change    = 0.165358
throughput change = 0.0054785
Class 0:
Packet latency average = 3437.13
	minimum = 22
	maximum = 6879
Network latency average = 3345.42
	minimum = 22
	maximum = 6841
Slowest packet = 18720
Flit latency average = 3010.98
	minimum = 5
	maximum = 7497
Slowest flit = 264347
Fragmentation average = 57.8645
	minimum = 0
	maximum = 723
Injected packet rate average = 0.0253237
	minimum = 0.00914286 (at node 48)
	maximum = 0.0327143 (at node 13)
Accepted packet rate average = 0.0154725
	minimum = 0.012 (at node 190)
	maximum = 0.0201429 (at node 157)
Injected flit rate average = 0.455714
	minimum = 0.162286 (at node 48)
	maximum = 0.590857 (at node 13)
Accepted flit rate average= 0.277643
	minimum = 0.212857 (at node 190)
	maximum = 0.361 (at node 157)
Injected packet length average = 17.9956
Accepted packet length average = 17.9443
Total in-flight flits = 400078 (399996 measured)
latency change    = 0.116675
throughput change = 0.00304344
Draining all recorded packets ...
Class 0:
Remaining flits: 357642 357643 357644 357645 357646 357647 357648 357649 357650 357651 [...] (407402 flits)
Measured flits: 357642 357643 357644 357645 357646 357647 357648 357649 357650 357651 [...] (393002 flits)
Class 0:
Remaining flits: 382518 382519 382520 382521 382522 382523 382524 382525 382526 382527 [...] (408095 flits)
Measured flits: 382518 382519 382520 382521 382522 382523 382524 382525 382526 382527 [...] (370925 flits)
Class 0:
Remaining flits: 387054 387055 387056 387057 387058 387059 387060 387061 387062 387063 [...] (407491 flits)
Measured flits: 387054 387055 387056 387057 387058 387059 387060 387061 387062 387063 [...] (340513 flits)
Class 0:
Remaining flits: 421740 421741 421742 421743 421744 421745 421746 421747 421748 421749 [...] (409756 flits)
Measured flits: 421740 421741 421742 421743 421744 421745 421746 421747 421748 421749 [...] (304027 flits)
Class 0:
Remaining flits: 433314 433315 433316 433317 433318 433319 433320 433321 433322 433323 [...] (410093 flits)
Measured flits: 433314 433315 433316 433317 433318 433319 433320 433321 433322 433323 [...] (264241 flits)
Class 0:
Remaining flits: 458856 458857 458858 458859 458860 458861 458862 458863 458864 458865 [...] (411648 flits)
Measured flits: 458856 458857 458858 458859 458860 458861 458862 458863 458864 458865 [...] (224963 flits)
Class 0:
Remaining flits: 533106 533107 533108 533109 533110 533111 533112 533113 533114 533115 [...] (412759 flits)
Measured flits: 533106 533107 533108 533109 533110 533111 533112 533113 533114 533115 [...] (183763 flits)
Class 0:
Remaining flits: 535878 535879 535880 535881 535882 535883 535884 535885 535886 535887 [...] (415021 flits)
Measured flits: 535878 535879 535880 535881 535882 535883 535884 535885 535886 535887 [...] (143674 flits)
Class 0:
Remaining flits: 535878 535879 535880 535881 535882 535883 535884 535885 535886 535887 [...] (416020 flits)
Measured flits: 535878 535879 535880 535881 535882 535883 535884 535885 535886 535887 [...] (105436 flits)
Class 0:
Remaining flits: 602802 602803 602804 602805 602806 602807 602808 602809 602810 602811 [...] (420261 flits)
Measured flits: 602802 602803 602804 602805 602806 602807 602808 602809 602810 602811 [...] (71557 flits)
Class 0:
Remaining flits: 669870 669871 669872 669873 669874 669875 669876 669877 669878 669879 [...] (419345 flits)
Measured flits: 669870 669871 669872 669873 669874 669875 669876 669877 669878 669879 [...] (44535 flits)
Class 0:
Remaining flits: 716706 716707 716708 716709 716710 716711 716712 716713 716714 716715 [...] (418627 flits)
Measured flits: 716706 716707 716708 716709 716710 716711 716712 716713 716714 716715 [...] (25252 flits)
Class 0:
Remaining flits: 742788 742789 742790 742791 742792 742793 742794 742795 742796 742797 [...] (418295 flits)
Measured flits: 742788 742789 742790 742791 742792 742793 742794 742795 742796 742797 [...] (13669 flits)
Class 0:
Remaining flits: 854316 854317 854318 854319 854320 854321 854322 854323 854324 854325 [...] (420724 flits)
Measured flits: 854316 854317 854318 854319 854320 854321 854322 854323 854324 854325 [...] (6902 flits)
Class 0:
Remaining flits: 879804 879805 879806 879807 879808 879809 879810 879811 879812 879813 [...] (420243 flits)
Measured flits: 879804 879805 879806 879807 879808 879809 879810 879811 879812 879813 [...] (3456 flits)
Class 0:
Remaining flits: 879804 879805 879806 879807 879808 879809 879810 879811 879812 879813 [...] (420635 flits)
Measured flits: 879804 879805 879806 879807 879808 879809 879810 879811 879812 879813 [...] (1313 flits)
Class 0:
Remaining flits: 893610 893611 893612 893613 893614 893615 893616 893617 893618 893619 [...] (422551 flits)
Measured flits: 893610 893611 893612 893613 893614 893615 893616 893617 893618 893619 [...] (489 flits)
Class 0:
Remaining flits: 988506 988507 988508 988509 988510 988511 988512 988513 988514 988515 [...] (422609 flits)
Measured flits: 988506 988507 988508 988509 988510 988511 988512 988513 988514 988515 [...] (147 flits)
Class 0:
Remaining flits: 1065816 1065817 1065818 1065819 1065820 1065821 1065822 1065823 1065824 1065825 [...] (422844 flits)
Measured flits: 1299384 1299385 1299386 1299387 1299388 1299389 1299390 1299391 1299392 1299393 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1093824 1093825 1093826 1093827 1093828 1093829 1093830 1093831 1093832 1093833 [...] (370840 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1213002 1213003 1213004 1213005 1213006 1213007 1213008 1213009 1213010 1213011 [...] (321015 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1266336 1266337 1266338 1266339 1266340 1266341 1266342 1266343 1266344 1266345 [...] (270450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1307430 1307431 1307432 1307433 1307434 1307435 1307436 1307437 1307438 1307439 [...] (220999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1390637 1390638 1390639 1390640 1390641 1390642 1390643 1400922 1400923 1400924 [...] (173089 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1446750 1446751 1446752 1446753 1446754 1446755 1446756 1446757 1446758 1446759 [...] (125454 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1516932 1516933 1516934 1516935 1516936 1516937 1516938 1516939 1516940 1516941 [...] (77970 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1516932 1516933 1516934 1516935 1516936 1516937 1516938 1516939 1516940 1516941 [...] (31153 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1635642 1635643 1635644 1635645 1635646 1635647 1635648 1635649 1635650 1635651 [...] (4627 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1764089 1781199 1781200 1781201 1781202 1781203 1781204 1781205 1781206 1781207 [...] (1038 flits)
Measured flits: (0 flits)
Time taken is 40197 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6958.23 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19773 (1 samples)
Network latency average = 6037.2 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18083 (1 samples)
Flit latency average = 6713.16 (1 samples)
	minimum = 5 (1 samples)
	maximum = 18490 (1 samples)
Fragmentation average = 53.6779 (1 samples)
	minimum = 0 (1 samples)
	maximum = 780 (1 samples)
Injected packet rate average = 0.0253237 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.0327143 (1 samples)
Accepted packet rate average = 0.0154725 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0201429 (1 samples)
Injected flit rate average = 0.455714 (1 samples)
	minimum = 0.162286 (1 samples)
	maximum = 0.590857 (1 samples)
Accepted flit rate average = 0.277643 (1 samples)
	minimum = 0.212857 (1 samples)
	maximum = 0.361 (1 samples)
Injected packet size average = 17.9956 (1 samples)
Accepted packet size average = 17.9443 (1 samples)
Hops average = 5.07063 (1 samples)
Total run time 53.4178
