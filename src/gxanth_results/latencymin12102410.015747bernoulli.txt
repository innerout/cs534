BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.26
	minimum = 23
	maximum = 790
Network latency average = 210.287
	minimum = 23
	maximum = 790
Slowest packet = 555
Flit latency average = 170.183
	minimum = 6
	maximum = 866
Slowest flit = 6948
Fragmentation average = 58.1749
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0158542
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00893229
	minimum = 0.003 (at node 41)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.282297
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.16875
	minimum = 0.063 (at node 174)
	maximum = 0.291 (at node 44)
Injected packet length average = 17.8058
Accepted packet length average = 18.8921
Total in-flight flits = 22392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 372.699
	minimum = 23
	maximum = 1613
Network latency average = 369.519
	minimum = 23
	maximum = 1613
Slowest packet = 948
Flit latency average = 329.295
	minimum = 6
	maximum = 1632
Slowest flit = 19289
Fragmentation average = 64.3029
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 164)
	maximum = 0.0235 (at node 44)
Accepted packet rate average = 0.0093099
	minimum = 0.005 (at node 83)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.280391
	minimum = 0.162 (at node 164)
	maximum = 0.423 (at node 44)
Accepted flit rate average= 0.171948
	minimum = 0.0965 (at node 83)
	maximum = 0.279 (at node 44)
Injected packet length average = 17.9211
Accepted packet length average = 18.4694
Total in-flight flits = 42116 (0 measured)
latency change    = 0.427795
throughput change = 0.0185982
Class 0:
Packet latency average = 864.152
	minimum = 23
	maximum = 2342
Network latency average = 860.89
	minimum = 23
	maximum = 2342
Slowest packet = 1992
Flit latency average = 825.294
	minimum = 6
	maximum = 2327
Slowest flit = 36299
Fragmentation average = 68.4893
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0156458
	minimum = 0.005 (at node 67)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.00969792
	minimum = 0.003 (at node 13)
	maximum = 0.018 (at node 173)
Injected flit rate average = 0.281401
	minimum = 0.101 (at node 67)
	maximum = 0.504 (at node 35)
Accepted flit rate average= 0.173849
	minimum = 0.053 (at node 184)
	maximum = 0.31 (at node 56)
Injected packet length average = 17.9857
Accepted packet length average = 17.9264
Total in-flight flits = 62809 (0 measured)
latency change    = 0.568712
throughput change = 0.010935
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 211.416
	minimum = 23
	maximum = 973
Network latency average = 208.512
	minimum = 23
	maximum = 973
Slowest packet = 9022
Flit latency average = 1158.26
	minimum = 6
	maximum = 3115
Slowest flit = 47622
Fragmentation average = 32.118
	minimum = 0
	maximum = 166
Injected packet rate average = 0.016276
	minimum = 0.008 (at node 154)
	maximum = 0.027 (at node 155)
Accepted packet rate average = 0.00973958
	minimum = 0.002 (at node 132)
	maximum = 0.019 (at node 114)
Injected flit rate average = 0.292349
	minimum = 0.144 (at node 154)
	maximum = 0.486 (at node 155)
Accepted flit rate average= 0.17524
	minimum = 0.035 (at node 132)
	maximum = 0.335 (at node 63)
Injected packet length average = 17.9619
Accepted packet length average = 17.9925
Total in-flight flits = 85413 (49159 measured)
latency change    = 3.08746
throughput change = 0.00793556
Class 0:
Packet latency average = 567.997
	minimum = 23
	maximum = 1942
Network latency average = 564.878
	minimum = 23
	maximum = 1942
Slowest packet = 9037
Flit latency average = 1329.18
	minimum = 6
	maximum = 4003
Slowest flit = 52811
Fragmentation average = 48.9813
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0159089
	minimum = 0.008 (at node 154)
	maximum = 0.023 (at node 150)
Accepted packet rate average = 0.00964062
	minimum = 0.0045 (at node 96)
	maximum = 0.0165 (at node 34)
Injected flit rate average = 0.286482
	minimum = 0.144 (at node 154)
	maximum = 0.414 (at node 150)
Accepted flit rate average= 0.173299
	minimum = 0.0845 (at node 96)
	maximum = 0.292 (at node 63)
Injected packet length average = 18.0077
Accepted packet length average = 17.976
Total in-flight flits = 106224 (87934 measured)
latency change    = 0.627788
throughput change = 0.0111951
Class 0:
Packet latency average = 1002.51
	minimum = 23
	maximum = 2983
Network latency average = 999.185
	minimum = 23
	maximum = 2983
Slowest packet = 9012
Flit latency average = 1510.91
	minimum = 6
	maximum = 4768
Slowest flit = 61631
Fragmentation average = 57.0389
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0158767
	minimum = 0.00966667 (at node 118)
	maximum = 0.0243333 (at node 3)
Accepted packet rate average = 0.00976215
	minimum = 0.005 (at node 36)
	maximum = 0.015 (at node 34)
Injected flit rate average = 0.285889
	minimum = 0.172667 (at node 118)
	maximum = 0.438 (at node 3)
Accepted flit rate average= 0.175915
	minimum = 0.0936667 (at node 36)
	maximum = 0.264333 (at node 34)
Injected packet length average = 18.0068
Accepted packet length average = 18.0201
Total in-flight flits = 126092 (118498 measured)
latency change    = 0.433422
throughput change = 0.0148677
Class 0:
Packet latency average = 1407.36
	minimum = 23
	maximum = 3926
Network latency average = 1403.98
	minimum = 23
	maximum = 3926
Slowest packet = 9192
Flit latency average = 1697.93
	minimum = 6
	maximum = 5216
Slowest flit = 85211
Fragmentation average = 60.989
	minimum = 0
	maximum = 166
Injected packet rate average = 0.015875
	minimum = 0.00975 (at node 154)
	maximum = 0.022 (at node 3)
Accepted packet rate average = 0.00975911
	minimum = 0.006 (at node 86)
	maximum = 0.01425 (at node 128)
Injected flit rate average = 0.285848
	minimum = 0.1755 (at node 154)
	maximum = 0.3945 (at node 3)
Accepted flit rate average= 0.175553
	minimum = 0.104 (at node 86)
	maximum = 0.2555 (at node 128)
Injected packet length average = 18.0062
Accepted packet length average = 17.9887
Total in-flight flits = 147440 (144521 measured)
latency change    = 0.287671
throughput change = 0.00205946
Class 0:
Packet latency average = 1725.59
	minimum = 23
	maximum = 4888
Network latency average = 1722.2
	minimum = 23
	maximum = 4888
Slowest packet = 9150
Flit latency average = 1870.86
	minimum = 6
	maximum = 6369
Slowest flit = 82979
Fragmentation average = 62.4899
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0158021
	minimum = 0.0116 (at node 138)
	maximum = 0.02 (at node 3)
Accepted packet rate average = 0.00977604
	minimum = 0.0064 (at node 36)
	maximum = 0.0138 (at node 128)
Injected flit rate average = 0.284525
	minimum = 0.2084 (at node 138)
	maximum = 0.36 (at node 3)
Accepted flit rate average= 0.17616
	minimum = 0.1152 (at node 36)
	maximum = 0.2476 (at node 128)
Injected packet length average = 18.0055
Accepted packet length average = 18.0196
Total in-flight flits = 166755 (165670 measured)
latency change    = 0.184414
throughput change = 0.0034459
Class 0:
Packet latency average = 2007.07
	minimum = 23
	maximum = 5735
Network latency average = 2003.65
	minimum = 23
	maximum = 5735
Slowest packet = 9392
Flit latency average = 2044.68
	minimum = 6
	maximum = 6891
Slowest flit = 108377
Fragmentation average = 63.7222
	minimum = 0
	maximum = 166
Injected packet rate average = 0.015822
	minimum = 0.0118333 (at node 154)
	maximum = 0.0195 (at node 185)
Accepted packet rate average = 0.00977604
	minimum = 0.0065 (at node 36)
	maximum = 0.0135 (at node 128)
Injected flit rate average = 0.284834
	minimum = 0.210667 (at node 154)
	maximum = 0.352833 (at node 185)
Accepted flit rate average= 0.176047
	minimum = 0.117 (at node 36)
	maximum = 0.243667 (at node 128)
Injected packet length average = 18.0024
Accepted packet length average = 18.008
Total in-flight flits = 188089 (187552 measured)
latency change    = 0.140246
throughput change = 0.000644951
Class 0:
Packet latency average = 2252.26
	minimum = 23
	maximum = 6734
Network latency average = 2248.83
	minimum = 23
	maximum = 6729
Slowest packet = 9801
Flit latency average = 2218.7
	minimum = 6
	maximum = 8024
Slowest flit = 104669
Fragmentation average = 64.7415
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0158222
	minimum = 0.0117143 (at node 154)
	maximum = 0.0197143 (at node 140)
Accepted packet rate average = 0.00977679
	minimum = 0.00714286 (at node 36)
	maximum = 0.013 (at node 128)
Injected flit rate average = 0.284795
	minimum = 0.210857 (at node 154)
	maximum = 0.355714 (at node 140)
Accepted flit rate average= 0.175875
	minimum = 0.128571 (at node 36)
	maximum = 0.235857 (at node 128)
Injected packet length average = 17.9998
Accepted packet length average = 17.989
Total in-flight flits = 209203 (209026 measured)
latency change    = 0.108864
throughput change = 0.000977257
Draining all recorded packets ...
Class 0:
Remaining flits: 125766 125767 125768 125769 125770 125771 125772 125773 125774 125775 [...] (230164 flits)
Measured flits: 164088 164089 164090 164091 164092 164093 164094 164095 164096 164097 [...] (179493 flits)
Class 0:
Remaining flits: 130644 130645 130646 130647 130648 130649 130650 130651 130652 130653 [...] (247490 flits)
Measured flits: 173988 173989 173990 173991 173992 173993 173994 173995 173996 173997 [...] (150268 flits)
Class 0:
Remaining flits: 189384 189385 189386 189387 189388 189389 189390 189391 189392 189393 [...] (267874 flits)
Measured flits: 189384 189385 189386 189387 189388 189389 189390 189391 189392 189393 [...] (122600 flits)
Class 0:
Remaining flits: 204192 204193 204194 204195 204196 204197 204198 204199 204200 204201 [...] (288404 flits)
Measured flits: 204192 204193 204194 204195 204196 204197 204198 204199 204200 204201 [...] (97189 flits)
Class 0:
Remaining flits: 220482 220483 220484 220485 220486 220487 220488 220489 220490 220491 [...] (307086 flits)
Measured flits: 220482 220483 220484 220485 220486 220487 220488 220489 220490 220491 [...] (72940 flits)
Class 0:
Remaining flits: 220482 220483 220484 220485 220486 220487 220488 220489 220490 220491 [...] (329343 flits)
Measured flits: 220482 220483 220484 220485 220486 220487 220488 220489 220490 220491 [...] (53043 flits)
Class 0:
Remaining flits: 226080 226081 226082 226083 226084 226085 226086 226087 226088 226089 [...] (349240 flits)
Measured flits: 226080 226081 226082 226083 226084 226085 226086 226087 226088 226089 [...] (36634 flits)
Class 0:
Remaining flits: 244263 244264 244265 244266 244267 244268 244269 244270 244271 244272 [...] (369407 flits)
Measured flits: 244263 244264 244265 244266 244267 244268 244269 244270 244271 244272 [...] (23239 flits)
Class 0:
Remaining flits: 295002 295003 295004 295005 295006 295007 295008 295009 295010 295011 [...] (390621 flits)
Measured flits: 295002 295003 295004 295005 295006 295007 295008 295009 295010 295011 [...] (13813 flits)
Class 0:
Remaining flits: 296946 296947 296948 296949 296950 296951 296952 296953 296954 296955 [...] (410899 flits)
Measured flits: 296946 296947 296948 296949 296950 296951 296952 296953 296954 296955 [...] (7781 flits)
Class 0:
Remaining flits: 322920 322921 322922 322923 322924 322925 322926 322927 322928 322929 [...] (430945 flits)
Measured flits: 322920 322921 322922 322923 322924 322925 322926 322927 322928 322929 [...] (4421 flits)
Class 0:
Remaining flits: 345692 345693 345694 345695 345696 345697 345698 345699 345700 345701 [...] (451712 flits)
Measured flits: 345692 345693 345694 345695 345696 345697 345698 345699 345700 345701 [...] (2236 flits)
Class 0:
Remaining flits: 368424 368425 368426 368427 368428 368429 368430 368431 368432 368433 [...] (470817 flits)
Measured flits: 368424 368425 368426 368427 368428 368429 368430 368431 368432 368433 [...] (861 flits)
Class 0:
Remaining flits: 433674 433675 433676 433677 433678 433679 433680 433681 433682 433683 [...] (490883 flits)
Measured flits: 433674 433675 433676 433677 433678 433679 433680 433681 433682 433683 [...] (450 flits)
Class 0:
Remaining flits: 450684 450685 450686 450687 450688 450689 450690 450691 450692 450693 [...] (508866 flits)
Measured flits: 450684 450685 450686 450687 450688 450689 450690 450691 450692 450693 [...] (301 flits)
Class 0:
Remaining flits: 455454 455455 455456 455457 455458 455459 455460 455461 455462 455463 [...] (526709 flits)
Measured flits: 455454 455455 455456 455457 455458 455459 455460 455461 455462 455463 [...] (165 flits)
Class 0:
Remaining flits: 513306 513307 513308 513309 513310 513311 513312 513313 513314 513315 [...] (543669 flits)
Measured flits: 513306 513307 513308 513309 513310 513311 513312 513313 513314 513315 [...] (54 flits)
Class 0:
Remaining flits: 540810 540811 540812 540813 540814 540815 540816 540817 540818 540819 [...] (557463 flits)
Measured flits: 540810 540811 540812 540813 540814 540815 540816 540817 540818 540819 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 591948 591949 591950 591951 591952 591953 591954 591955 591956 591957 [...] (530200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 603918 603919 603920 603921 603922 603923 603924 603925 603926 603927 [...] (499898 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 614322 614323 614324 614325 614326 614327 614328 614329 614330 614331 [...] (470491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 626994 626995 626996 626997 626998 626999 627000 627001 627002 627003 [...] (440993 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 637974 637975 637976 637977 637978 637979 637980 637981 637982 637983 [...] (410991 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 667548 667549 667550 667551 667552 667553 667554 667555 667556 667557 [...] (381395 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 678006 678007 678008 678009 678010 678011 678012 678013 678014 678015 [...] (351894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 696240 696241 696242 696243 696244 696245 696246 696247 696248 696249 [...] (322191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 701496 701497 701498 701499 701500 701501 701502 701503 701504 701505 [...] (293537 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 710262 710263 710264 710265 710266 710267 710268 710269 710270 710271 [...] (263614 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 746153 751050 751051 751052 751053 751054 751055 751056 751057 751058 [...] (234128 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 760446 760447 760448 760449 760450 760451 760452 760453 760454 760455 [...] (204893 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 761562 761563 761564 761565 761566 761567 761568 761569 761570 761571 [...] (176036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 761562 761563 761564 761565 761566 761567 761568 761569 761570 761571 [...] (146898 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 798390 798391 798392 798393 798394 798395 798396 798397 798398 798399 [...] (117086 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 828720 828721 828722 828723 828724 828725 828726 828727 828728 828729 [...] (88242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 908676 908677 908678 908679 908680 908681 908682 908683 908684 908685 [...] (59863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 928908 928909 928910 928911 928912 928913 928914 928915 928916 928917 [...] (32332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1157778 1157779 1157780 1157781 1157782 1157783 1157784 1157785 1157786 1157787 [...] (11955 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1238346 1238347 1238348 1238349 1238350 1238351 1238352 1238353 1238354 1238355 [...] (1907 flits)
Measured flits: (0 flits)
Time taken is 49044 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4560.94 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18351 (1 samples)
Network latency average = 4557.57 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18341 (1 samples)
Flit latency average = 9512.15 (1 samples)
	minimum = 6 (1 samples)
	maximum = 29464 (1 samples)
Fragmentation average = 72.0408 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0158222 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0197143 (1 samples)
Accepted packet rate average = 0.00977679 (1 samples)
	minimum = 0.00714286 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.284795 (1 samples)
	minimum = 0.210857 (1 samples)
	maximum = 0.355714 (1 samples)
Accepted flit rate average = 0.175875 (1 samples)
	minimum = 0.128571 (1 samples)
	maximum = 0.235857 (1 samples)
Injected packet size average = 17.9998 (1 samples)
Accepted packet size average = 17.989 (1 samples)
Hops average = 5.07096 (1 samples)
Total run time 30.7172
