BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 268.179
	minimum = 23
	maximum = 933
Network latency average = 212.345
	minimum = 23
	maximum = 901
Slowest packet = 183
Flit latency average = 144.197
	minimum = 6
	maximum = 884
Slowest flit = 3311
Fragmentation average = 120.733
	minimum = 0
	maximum = 731
Injected packet rate average = 0.0145104
	minimum = 0 (at node 21)
	maximum = 0.04 (at node 186)
Accepted packet rate average = 0.00889583
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.25762
	minimum = 0 (at node 21)
	maximum = 0.713 (at node 186)
Accepted flit rate average= 0.180505
	minimum = 0.057 (at node 174)
	maximum = 0.324 (at node 48)
Injected packet length average = 17.7541
Accepted packet length average = 20.291
Total in-flight flits = 15509 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 433.335
	minimum = 23
	maximum = 1859
Network latency average = 337.114
	minimum = 23
	maximum = 1690
Slowest packet = 619
Flit latency average = 249.265
	minimum = 6
	maximum = 1834
Slowest flit = 9315
Fragmentation average = 157.989
	minimum = 0
	maximum = 1486
Injected packet rate average = 0.0133047
	minimum = 0.001 (at node 59)
	maximum = 0.038 (at node 186)
Accepted packet rate average = 0.00978906
	minimum = 0.005 (at node 30)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.237289
	minimum = 0.018 (at node 59)
	maximum = 0.6815 (at node 186)
Accepted flit rate average= 0.186586
	minimum = 0.0985 (at node 164)
	maximum = 0.2815 (at node 152)
Injected packet length average = 17.835
Accepted packet length average = 19.0607
Total in-flight flits = 20367 (0 measured)
latency change    = 0.381128
throughput change = 0.0325894
Class 0:
Packet latency average = 835.07
	minimum = 29
	maximum = 2915
Network latency average = 578.437
	minimum = 25
	maximum = 2760
Slowest packet = 597
Flit latency average = 466.066
	minimum = 6
	maximum = 2743
Slowest flit = 10763
Fragmentation average = 199.083
	minimum = 1
	maximum = 2387
Injected packet rate average = 0.0118542
	minimum = 0 (at node 1)
	maximum = 0.031 (at node 6)
Accepted packet rate average = 0.0106406
	minimum = 0.003 (at node 118)
	maximum = 0.02 (at node 119)
Injected flit rate average = 0.212833
	minimum = 0 (at node 48)
	maximum = 0.554 (at node 74)
Accepted flit rate average= 0.192
	minimum = 0.054 (at node 184)
	maximum = 0.341 (at node 119)
Injected packet length average = 17.9543
Accepted packet length average = 18.0441
Total in-flight flits = 24525 (0 measured)
latency change    = 0.481079
throughput change = 0.0281982
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 822.414
	minimum = 31
	maximum = 3074
Network latency average = 333.568
	minimum = 25
	maximum = 918
Slowest packet = 7394
Flit latency average = 583.761
	minimum = 6
	maximum = 3149
Slowest flit = 33363
Fragmentation average = 138.256
	minimum = 0
	maximum = 630
Injected packet rate average = 0.0113333
	minimum = 0 (at node 12)
	maximum = 0.033 (at node 98)
Accepted packet rate average = 0.0106562
	minimum = 0.002 (at node 134)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.203646
	minimum = 0 (at node 30)
	maximum = 0.588 (at node 98)
Accepted flit rate average= 0.192964
	minimum = 0.048 (at node 85)
	maximum = 0.38 (at node 120)
Injected packet length average = 17.9688
Accepted packet length average = 18.108
Total in-flight flits = 26698 (21264 measured)
latency change    = 0.0153892
throughput change = 0.00499339
Class 0:
Packet latency average = 1119.89
	minimum = 30
	maximum = 3885
Network latency average = 502.584
	minimum = 23
	maximum = 1981
Slowest packet = 7394
Flit latency average = 608.004
	minimum = 6
	maximum = 4073
Slowest flit = 14903
Fragmentation average = 162.505
	minimum = 0
	maximum = 1596
Injected packet rate average = 0.0113542
	minimum = 0 (at node 12)
	maximum = 0.025 (at node 1)
Accepted packet rate average = 0.0107839
	minimum = 0.0065 (at node 85)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.203839
	minimum = 0.0025 (at node 12)
	maximum = 0.444 (at node 1)
Accepted flit rate average= 0.195005
	minimum = 0.1055 (at node 4)
	maximum = 0.3335 (at node 16)
Injected packet length average = 17.9528
Accepted packet length average = 18.0831
Total in-flight flits = 28321 (26886 measured)
latency change    = 0.265629
throughput change = 0.0104698
Class 0:
Packet latency average = 1354
	minimum = 25
	maximum = 4755
Network latency average = 607.614
	minimum = 23
	maximum = 2730
Slowest packet = 7394
Flit latency average = 630.91
	minimum = 6
	maximum = 4073
Slowest flit = 14903
Fragmentation average = 178.637
	minimum = 0
	maximum = 2099
Injected packet rate average = 0.0112309
	minimum = 0 (at node 12)
	maximum = 0.0213333 (at node 33)
Accepted packet rate average = 0.0107934
	minimum = 0.00633333 (at node 4)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.201899
	minimum = 0.00466667 (at node 12)
	maximum = 0.384 (at node 33)
Accepted flit rate average= 0.194896
	minimum = 0.109 (at node 4)
	maximum = 0.283333 (at node 78)
Injected packet length average = 17.9771
Accepted packet length average = 18.0569
Total in-flight flits = 28833 (28213 measured)
latency change    = 0.172902
throughput change = 0.000561197
Draining remaining packets ...
Class 0:
Remaining flits: 169881 169882 169883 188568 188569 188570 188571 188572 188573 188574 [...] (738 flits)
Measured flits: 169881 169882 169883 188568 188569 188570 188571 188572 188573 188574 [...] (738 flits)
Time taken is 7299 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1576.52 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5695 (1 samples)
Network latency average = 765.547 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3773 (1 samples)
Flit latency average = 729.527 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5636 (1 samples)
Fragmentation average = 187.625 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2704 (1 samples)
Injected packet rate average = 0.0112309 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0213333 (1 samples)
Accepted packet rate average = 0.0107934 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.0156667 (1 samples)
Injected flit rate average = 0.201899 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.384 (1 samples)
Accepted flit rate average = 0.194896 (1 samples)
	minimum = 0.109 (1 samples)
	maximum = 0.283333 (1 samples)
Injected packet size average = 17.9771 (1 samples)
Accepted packet size average = 18.0569 (1 samples)
Hops average = 5.08462 (1 samples)
Total run time 6.47011
