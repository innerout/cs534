BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 225.806
	minimum = 22
	maximum = 936
Network latency average = 142.956
	minimum = 22
	maximum = 621
Slowest packet = 21
Flit latency average = 117.751
	minimum = 5
	maximum = 631
Slowest flit = 20824
Fragmentation average = 16.6171
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0153281
	minimum = 0 (at node 9)
	maximum = 0.038 (at node 161)
Accepted packet rate average = 0.0123229
	minimum = 0.004 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.273474
	minimum = 0 (at node 9)
	maximum = 0.684 (at node 161)
Accepted flit rate average= 0.226948
	minimum = 0.072 (at node 174)
	maximum = 0.417 (at node 44)
Injected packet length average = 17.8413
Accepted packet length average = 18.4167
Total in-flight flits = 10228 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 353.539
	minimum = 22
	maximum = 1671
Network latency average = 192.365
	minimum = 22
	maximum = 1175
Slowest packet = 21
Flit latency average = 164.578
	minimum = 5
	maximum = 1267
Slowest flit = 38939
Fragmentation average = 18.2478
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0147031
	minimum = 0 (at node 72)
	maximum = 0.0305 (at node 161)
Accepted packet rate average = 0.012862
	minimum = 0.007 (at node 153)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.262932
	minimum = 0 (at node 72)
	maximum = 0.545 (at node 161)
Accepted flit rate average= 0.233875
	minimum = 0.126 (at node 153)
	maximum = 0.3275 (at node 98)
Injected packet length average = 17.8827
Accepted packet length average = 18.1834
Total in-flight flits = 12702 (0 measured)
latency change    = 0.361298
throughput change = 0.0296187
Class 0:
Packet latency average = 724.991
	minimum = 22
	maximum = 2269
Network latency average = 269.359
	minimum = 22
	maximum = 1328
Slowest packet = 4623
Flit latency average = 236.582
	minimum = 5
	maximum = 1311
Slowest flit = 38951
Fragmentation average = 21.1516
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0142969
	minimum = 0.002 (at node 40)
	maximum = 0.034 (at node 123)
Accepted packet rate average = 0.0138073
	minimum = 0.006 (at node 117)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.257443
	minimum = 0.033 (at node 126)
	maximum = 0.612 (at node 123)
Accepted flit rate average= 0.249375
	minimum = 0.11 (at node 118)
	maximum = 0.45 (at node 16)
Injected packet length average = 18.0069
Accepted packet length average = 18.0611
Total in-flight flits = 14448 (0 measured)
latency change    = 0.512353
throughput change = 0.0621554
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 925.059
	minimum = 25
	maximum = 3023
Network latency average = 258.736
	minimum = 22
	maximum = 848
Slowest packet = 8453
Flit latency average = 273.117
	minimum = 5
	maximum = 1210
Slowest flit = 93005
Fragmentation average = 20.4377
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0139635
	minimum = 0 (at node 140)
	maximum = 0.029 (at node 60)
Accepted packet rate average = 0.0136875
	minimum = 0.005 (at node 71)
	maximum = 0.023 (at node 19)
Injected flit rate average = 0.251245
	minimum = 0 (at node 140)
	maximum = 0.51 (at node 60)
Accepted flit rate average= 0.245781
	minimum = 0.076 (at node 71)
	maximum = 0.429 (at node 78)
Injected packet length average = 17.9929
Accepted packet length average = 17.9566
Total in-flight flits = 15840 (15840 measured)
latency change    = 0.216276
throughput change = 0.0146217
Class 0:
Packet latency average = 1084.35
	minimum = 25
	maximum = 3508
Network latency average = 297.398
	minimum = 22
	maximum = 1413
Slowest packet = 8453
Flit latency average = 281.083
	minimum = 5
	maximum = 1396
Slowest flit = 172673
Fragmentation average = 21.7491
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0139818
	minimum = 0.0025 (at node 133)
	maximum = 0.023 (at node 42)
Accepted packet rate average = 0.0138177
	minimum = 0.007 (at node 36)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.251727
	minimum = 0.053 (at node 133)
	maximum = 0.414 (at node 42)
Accepted flit rate average= 0.248513
	minimum = 0.132 (at node 36)
	maximum = 0.4115 (at node 129)
Injected packet length average = 18.0039
Accepted packet length average = 17.9851
Total in-flight flits = 16237 (16237 measured)
latency change    = 0.146903
throughput change = 0.0109925
Class 0:
Packet latency average = 1218.48
	minimum = 22
	maximum = 4191
Network latency average = 308.508
	minimum = 22
	maximum = 1413
Slowest packet = 8453
Flit latency average = 285.526
	minimum = 5
	maximum = 1396
Slowest flit = 172673
Fragmentation average = 21.2976
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0139184
	minimum = 0.003 (at node 133)
	maximum = 0.0236667 (at node 118)
Accepted packet rate average = 0.0137951
	minimum = 0.009 (at node 36)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.250495
	minimum = 0.0593333 (at node 133)
	maximum = 0.431667 (at node 118)
Accepted flit rate average= 0.248115
	minimum = 0.162 (at node 36)
	maximum = 0.362333 (at node 128)
Injected packet length average = 17.9974
Accepted packet length average = 17.9857
Total in-flight flits = 16218 (16218 measured)
latency change    = 0.11008
throughput change = 0.00160586
Class 0:
Packet latency average = 1344.1
	minimum = 22
	maximum = 4781
Network latency average = 317.385
	minimum = 22
	maximum = 1452
Slowest packet = 8453
Flit latency average = 290.483
	minimum = 5
	maximum = 1435
Slowest flit = 229304
Fragmentation average = 21.5163
	minimum = 0
	maximum = 120
Injected packet rate average = 0.0139167
	minimum = 0.004 (at node 172)
	maximum = 0.02125 (at node 42)
Accepted packet rate average = 0.0137943
	minimum = 0.00925 (at node 36)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.25053
	minimum = 0.072 (at node 172)
	maximum = 0.38075 (at node 42)
Accepted flit rate average= 0.248142
	minimum = 0.17075 (at node 36)
	maximum = 0.34825 (at node 128)
Injected packet length average = 18.0022
Accepted packet length average = 17.9888
Total in-flight flits = 16871 (16871 measured)
latency change    = 0.0934586
throughput change = 0.000110194
Class 0:
Packet latency average = 1448.95
	minimum = 22
	maximum = 5237
Network latency average = 319.86
	minimum = 22
	maximum = 1452
Slowest packet = 8453
Flit latency average = 291.428
	minimum = 5
	maximum = 1435
Slowest flit = 229304
Fragmentation average = 21.3665
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0139969
	minimum = 0.0054 (at node 133)
	maximum = 0.0212 (at node 42)
Accepted packet rate average = 0.0138573
	minimum = 0.0102 (at node 64)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.251829
	minimum = 0.0992 (at node 133)
	maximum = 0.3788 (at node 42)
Accepted flit rate average= 0.249261
	minimum = 0.1836 (at node 64)
	maximum = 0.3348 (at node 128)
Injected packet length average = 17.9918
Accepted packet length average = 17.9877
Total in-flight flits = 17509 (17509 measured)
latency change    = 0.0723623
throughput change = 0.00449139
Class 0:
Packet latency average = 1549.63
	minimum = 22
	maximum = 6031
Network latency average = 323.513
	minimum = 22
	maximum = 1452
Slowest packet = 8453
Flit latency average = 293.759
	minimum = 5
	maximum = 1435
Slowest flit = 229304
Fragmentation average = 21.7093
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140564
	minimum = 0.00683333 (at node 81)
	maximum = 0.0208333 (at node 42)
Accepted packet rate average = 0.0139193
	minimum = 0.0111667 (at node 104)
	maximum = 0.0186667 (at node 128)
Injected flit rate average = 0.252916
	minimum = 0.123 (at node 81)
	maximum = 0.373333 (at node 42)
Accepted flit rate average= 0.250646
	minimum = 0.201167 (at node 104)
	maximum = 0.334167 (at node 128)
Injected packet length average = 17.9929
Accepted packet length average = 18.0071
Total in-flight flits = 17700 (17700 measured)
latency change    = 0.0649683
throughput change = 0.00552323
Class 0:
Packet latency average = 1653.45
	minimum = 22
	maximum = 6244
Network latency average = 326.085
	minimum = 22
	maximum = 1452
Slowest packet = 8453
Flit latency average = 295.223
	minimum = 5
	maximum = 1435
Slowest flit = 229304
Fragmentation average = 21.9456
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140685
	minimum = 0.00785714 (at node 75)
	maximum = 0.0198571 (at node 42)
Accepted packet rate average = 0.0139688
	minimum = 0.0111429 (at node 79)
	maximum = 0.0178571 (at node 138)
Injected flit rate average = 0.253115
	minimum = 0.141429 (at node 75)
	maximum = 0.356 (at node 42)
Accepted flit rate average= 0.251376
	minimum = 0.198429 (at node 79)
	maximum = 0.321714 (at node 138)
Injected packet length average = 17.9916
Accepted packet length average = 17.9956
Total in-flight flits = 17321 (17321 measured)
latency change    = 0.0627884
throughput change = 0.00290366
Draining all recorded packets ...
Class 0:
Remaining flits: 486612 486613 486614 486615 486616 486617 486618 486619 486620 486621 [...] (17740 flits)
Measured flits: 486612 486613 486614 486615 486616 486617 486618 486619 486620 486621 [...] (13766 flits)
Class 0:
Remaining flits: 500544 500545 500546 500547 500548 500549 500550 500551 500552 500553 [...] (17484 flits)
Measured flits: 533905 533906 533907 533908 533909 533910 533911 533912 533913 533914 [...] (10108 flits)
Class 0:
Remaining flits: 575262 575263 575264 575265 575266 575267 575268 575269 575270 575271 [...] (17953 flits)
Measured flits: 575262 575263 575264 575265 575266 575267 575268 575269 575270 575271 [...] (7334 flits)
Class 0:
Remaining flits: 616869 616870 616871 616872 616873 616874 616875 616876 616877 619632 [...] (17395 flits)
Measured flits: 630198 630199 630200 630201 630202 630203 630204 630205 630206 630207 [...] (5003 flits)
Class 0:
Remaining flits: 661773 661774 661775 661776 661777 661778 661779 661780 661781 661782 [...] (17511 flits)
Measured flits: 661773 661774 661775 661776 661777 661778 661779 661780 661781 661782 [...] (3477 flits)
Class 0:
Remaining flits: 704862 704863 704864 704865 704866 704867 704868 704869 704870 704871 [...] (17642 flits)
Measured flits: 704862 704863 704864 704865 704866 704867 704868 704869 704870 704871 [...] (1997 flits)
Class 0:
Remaining flits: 752850 752851 752852 752853 752854 752855 752856 752857 752858 752859 [...] (17730 flits)
Measured flits: 774450 774451 774452 774453 774454 774455 774456 774457 774458 774459 [...] (1062 flits)
Class 0:
Remaining flits: 821840 821841 821842 821843 822744 822745 822746 822747 822748 822749 [...] (18412 flits)
Measured flits: 827010 827011 827012 827013 827014 827015 827016 827017 827018 827019 [...] (463 flits)
Class 0:
Remaining flits: 853866 853867 853868 853869 853870 853871 853872 853873 853874 853875 [...] (18130 flits)
Measured flits: 868482 868483 868484 868485 868486 868487 868488 868489 868490 868491 [...] (252 flits)
Class 0:
Remaining flits: 886899 886900 886901 886902 886903 886904 886905 886906 886907 886908 [...] (17915 flits)
Measured flits: 964476 964477 964478 964479 964480 964481 964482 964483 964484 964485 [...] (72 flits)
Class 0:
Remaining flits: 954900 954901 954902 954903 954904 954905 954906 954907 954908 954909 [...] (17916 flits)
Measured flits: 1003028 1003029 1003030 1003031 1020726 1020727 1020728 1020729 1020730 1020731 [...] (22 flits)
Draining remaining packets ...
Time taken is 21772 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2403.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11204 (1 samples)
Network latency average = 336.907 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1859 (1 samples)
Flit latency average = 309.281 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2133 (1 samples)
Fragmentation average = 22.4948 (1 samples)
	minimum = 0 (1 samples)
	maximum = 127 (1 samples)
Injected packet rate average = 0.0140685 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.0139688 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.253115 (1 samples)
	minimum = 0.141429 (1 samples)
	maximum = 0.356 (1 samples)
Accepted flit rate average = 0.251376 (1 samples)
	minimum = 0.198429 (1 samples)
	maximum = 0.321714 (1 samples)
Injected packet size average = 17.9916 (1 samples)
Accepted packet size average = 17.9956 (1 samples)
Hops average = 5.06756 (1 samples)
Total run time 18.0761
