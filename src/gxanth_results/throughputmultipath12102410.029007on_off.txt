BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 337.081
	minimum = 23
	maximum = 973
Network latency average = 255.484
	minimum = 23
	maximum = 888
Slowest packet = 68
Flit latency average = 220.994
	minimum = 6
	maximum = 873
Slowest flit = 9428
Fragmentation average = 55.3297
	minimum = 0
	maximum = 146
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0100156
	minimum = 0.004 (at node 79)
	maximum = 0.017 (at node 2)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.188781
	minimum = 0.087 (at node 79)
	maximum = 0.321 (at node 131)
Injected packet length average = 17.8192
Accepted packet length average = 18.8487
Total in-flight flits = 51972 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 583.133
	minimum = 23
	maximum = 1945
Network latency average = 467.117
	minimum = 23
	maximum = 1753
Slowest packet = 68
Flit latency average = 429.741
	minimum = 6
	maximum = 1781
Slowest flit = 17749
Fragmentation average = 60.003
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0271224
	minimum = 0.0005 (at node 183)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0105625
	minimum = 0.0055 (at node 135)
	maximum = 0.017 (at node 131)
Injected flit rate average = 0.486044
	minimum = 0.009 (at node 183)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.194138
	minimum = 0.1065 (at node 135)
	maximum = 0.3105 (at node 131)
Injected packet length average = 17.9204
Accepted packet length average = 18.3799
Total in-flight flits = 112921 (0 measured)
latency change    = 0.421949
throughput change = 0.0275926
Class 0:
Packet latency average = 1335.07
	minimum = 27
	maximum = 2924
Network latency average = 1163.58
	minimum = 23
	maximum = 2589
Slowest packet = 4317
Flit latency average = 1134.42
	minimum = 6
	maximum = 2575
Slowest flit = 33435
Fragmentation average = 63.9719
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0289688
	minimum = 0 (at node 184)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0107396
	minimum = 0.002 (at node 62)
	maximum = 0.019 (at node 119)
Injected flit rate average = 0.521047
	minimum = 0 (at node 184)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.193854
	minimum = 0.04 (at node 62)
	maximum = 0.32 (at node 119)
Injected packet length average = 17.9865
Accepted packet length average = 18.0504
Total in-flight flits = 175817 (0 measured)
latency change    = 0.563218
throughput change = 0.00146427
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 337.03
	minimum = 34
	maximum = 2376
Network latency average = 102.401
	minimum = 25
	maximum = 926
Slowest packet = 16102
Flit latency average = 1656.54
	minimum = 6
	maximum = 3633
Slowest flit = 29037
Fragmentation average = 15.545
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0279583
	minimum = 0 (at node 16)
	maximum = 0.056 (at node 22)
Accepted packet rate average = 0.0106146
	minimum = 0.003 (at node 49)
	maximum = 0.018 (at node 66)
Injected flit rate average = 0.50337
	minimum = 0 (at node 16)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.190917
	minimum = 0.055 (at node 131)
	maximum = 0.339 (at node 68)
Injected packet length average = 18.0043
Accepted packet length average = 17.9863
Total in-flight flits = 235785 (89870 measured)
latency change    = 2.96127
throughput change = 0.0153863
Class 0:
Packet latency average = 491.132
	minimum = 23
	maximum = 2535
Network latency average = 247.902
	minimum = 23
	maximum = 1926
Slowest packet = 16102
Flit latency average = 1960.1
	minimum = 6
	maximum = 4483
Slowest flit = 38123
Fragmentation average = 20.9125
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0268516
	minimum = 0.003 (at node 97)
	maximum = 0.0555 (at node 134)
Accepted packet rate average = 0.0104922
	minimum = 0.005 (at node 49)
	maximum = 0.016 (at node 66)
Injected flit rate average = 0.48332
	minimum = 0.054 (at node 97)
	maximum = 1 (at node 134)
Accepted flit rate average= 0.189083
	minimum = 0.0895 (at node 117)
	maximum = 0.3005 (at node 66)
Injected packet length average = 17.9997
Accepted packet length average = 18.0213
Total in-flight flits = 288825 (171230 measured)
latency change    = 0.313769
throughput change = 0.0096959
Class 0:
Packet latency average = 797.14
	minimum = 23
	maximum = 3304
Network latency average = 545.87
	minimum = 23
	maximum = 2961
Slowest packet = 16102
Flit latency average = 2233.01
	minimum = 6
	maximum = 5186
Slowest flit = 68317
Fragmentation average = 27.2592
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0266215
	minimum = 0.00466667 (at node 16)
	maximum = 0.0556667 (at node 139)
Accepted packet rate average = 0.0104826
	minimum = 0.005 (at node 49)
	maximum = 0.0153333 (at node 66)
Injected flit rate average = 0.479212
	minimum = 0.084 (at node 16)
	maximum = 1 (at node 139)
Accepted flit rate average= 0.188273
	minimum = 0.0983333 (at node 49)
	maximum = 0.273333 (at node 168)
Injected packet length average = 18.0009
Accepted packet length average = 17.9604
Total in-flight flits = 343384 (251103 measured)
latency change    = 0.383882
throughput change = 0.00430633
Draining remaining packets ...
Class 0:
Remaining flits: 53819 56916 56917 56918 56919 56920 56921 56922 56923 56924 [...] (311999 flits)
Measured flits: 287586 287587 287588 287589 287590 287591 287592 287593 287594 287595 [...] (242003 flits)
Class 0:
Remaining flits: 65595 65596 65597 65598 65599 65600 65601 65602 65603 65604 [...] (281104 flits)
Measured flits: 287586 287587 287588 287589 287590 287591 287592 287593 287594 287595 [...] (230131 flits)
Class 0:
Remaining flits: 72936 72937 72938 72939 72940 72941 72942 72943 72944 72945 [...] (251238 flits)
Measured flits: 287586 287587 287588 287589 287590 287591 287592 287593 287594 287595 [...] (214992 flits)
Class 0:
Remaining flits: 104562 104563 104564 104565 104566 104567 104568 104569 104570 104571 [...] (221240 flits)
Measured flits: 287586 287587 287588 287589 287590 287591 287592 287593 287594 287595 [...] (196065 flits)
Class 0:
Remaining flits: 108108 108109 108110 108111 108112 108113 108114 108115 108116 108117 [...] (191181 flits)
Measured flits: 287712 287713 287714 287715 287716 287717 287718 287719 287720 287721 [...] (174636 flits)
Class 0:
Remaining flits: 130608 130609 130610 130611 130612 130613 130614 130615 130616 130617 [...] (161804 flits)
Measured flits: 287712 287713 287714 287715 287716 287717 287718 287719 287720 287721 [...] (151129 flits)
Class 0:
Remaining flits: 142164 142165 142166 142167 142168 142169 142170 142171 142172 142173 [...] (133117 flits)
Measured flits: 287712 287713 287714 287715 287716 287717 287718 287719 287720 287721 [...] (126430 flits)
Class 0:
Remaining flits: 152010 152011 152012 152013 152014 152015 152016 152017 152018 152019 [...] (104425 flits)
Measured flits: 287712 287713 287714 287715 287716 287717 287718 287719 287720 287721 [...] (100363 flits)
Class 0:
Remaining flits: 163791 163792 163793 163794 163795 163796 163797 163798 163799 168696 [...] (78299 flits)
Measured flits: 287748 287749 287750 287751 287752 287753 287754 287755 287756 287757 [...] (75960 flits)
Class 0:
Remaining flits: 198594 198595 198596 198597 198598 198599 198600 198601 198602 198603 [...] (53642 flits)
Measured flits: 287748 287749 287750 287751 287752 287753 287754 287755 287756 287757 [...] (52392 flits)
Class 0:
Remaining flits: 209034 209035 209036 209037 209038 209039 209040 209041 209042 209043 [...] (31403 flits)
Measured flits: 287928 287929 287930 287931 287932 287933 287934 287935 287936 287937 [...] (30834 flits)
Class 0:
Remaining flits: 209034 209035 209036 209037 209038 209039 209040 209041 209042 209043 [...] (15436 flits)
Measured flits: 298206 298207 298208 298209 298210 298211 298212 298213 298214 298215 [...] (15198 flits)
Class 0:
Remaining flits: 254808 254809 254810 254811 254812 254813 254814 254815 254816 254817 [...] (3477 flits)
Measured flits: 298206 298207 298208 298209 298210 298211 298212 298213 298214 298215 [...] (3441 flits)
Time taken is 19842 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8051.91 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16327 (1 samples)
Network latency average = 7750.93 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16279 (1 samples)
Flit latency average = 6561.12 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16632 (1 samples)
Fragmentation average = 70.822 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.0266215 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0104826 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.479212 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.188273 (1 samples)
	minimum = 0.0983333 (1 samples)
	maximum = 0.273333 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 17.9604 (1 samples)
Hops average = 5.13486 (1 samples)
Total run time 11.2798
