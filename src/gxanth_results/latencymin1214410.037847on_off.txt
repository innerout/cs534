BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 370.577
	minimum = 23
	maximum = 979
Network latency average = 285.046
	minimum = 23
	maximum = 904
Slowest packet = 46
Flit latency average = 253.282
	minimum = 6
	maximum = 887
Slowest flit = 5579
Fragmentation average = 54.0998
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0278281
	minimum = 0 (at node 35)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.00980729
	minimum = 0.003 (at node 93)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.495396
	minimum = 0 (at node 35)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.184557
	minimum = 0.063 (at node 174)
	maximum = 0.306 (at node 91)
Injected packet length average = 17.802
Accepted packet length average = 18.8184
Total in-flight flits = 60901 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 679.056
	minimum = 23
	maximum = 1983
Network latency average = 542.863
	minimum = 23
	maximum = 1796
Slowest packet = 46
Flit latency average = 505.688
	minimum = 6
	maximum = 1785
Slowest flit = 17902
Fragmentation average = 61.2609
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0249271
	minimum = 0.004 (at node 43)
	maximum = 0.0395 (at node 9)
Accepted packet rate average = 0.0100208
	minimum = 0.0045 (at node 116)
	maximum = 0.0145 (at node 71)
Injected flit rate average = 0.446445
	minimum = 0.072 (at node 43)
	maximum = 0.703 (at node 9)
Accepted flit rate average= 0.184198
	minimum = 0.081 (at node 116)
	maximum = 0.263 (at node 140)
Injected packet length average = 17.9101
Accepted packet length average = 18.3815
Total in-flight flits = 102320 (0 measured)
latency change    = 0.454276
throughput change = 0.00195103
Class 0:
Packet latency average = 1699.34
	minimum = 27
	maximum = 2938
Network latency average = 1413.26
	minimum = 24
	maximum = 2712
Slowest packet = 3111
Flit latency average = 1388.67
	minimum = 6
	maximum = 2692
Slowest flit = 27261
Fragmentation average = 69.9353
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0145469
	minimum = 0 (at node 129)
	maximum = 0.042 (at node 15)
Accepted packet rate average = 0.00934375
	minimum = 0.003 (at node 0)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.263281
	minimum = 0 (at node 129)
	maximum = 0.757 (at node 15)
Accepted flit rate average= 0.168042
	minimum = 0.055 (at node 0)
	maximum = 0.343 (at node 103)
Injected packet length average = 18.0988
Accepted packet length average = 17.9844
Total in-flight flits = 121212 (0 measured)
latency change    = 0.6004
throughput change = 0.0961443
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1141.6
	minimum = 32
	maximum = 3514
Network latency average = 127.286
	minimum = 23
	maximum = 922
Slowest packet = 12500
Flit latency average = 1991.78
	minimum = 6
	maximum = 3668
Slowest flit = 29813
Fragmentation average = 17.0519
	minimum = 0
	maximum = 128
Injected packet rate average = 0.00957812
	minimum = 0 (at node 28)
	maximum = 0.041 (at node 13)
Accepted packet rate average = 0.00925521
	minimum = 0.002 (at node 23)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.172948
	minimum = 0 (at node 41)
	maximum = 0.738 (at node 170)
Accepted flit rate average= 0.166437
	minimum = 0.036 (at node 23)
	maximum = 0.332 (at node 76)
Injected packet length average = 18.0566
Accepted packet length average = 17.9831
Total in-flight flits = 123042 (32345 measured)
latency change    = 0.488563
throughput change = 0.00963825
Class 0:
Packet latency average = 2132.33
	minimum = 32
	maximum = 4705
Network latency average = 598.848
	minimum = 23
	maximum = 1959
Slowest packet = 12500
Flit latency average = 2277.81
	minimum = 6
	maximum = 4443
Slowest flit = 45413
Fragmentation average = 38.7695
	minimum = 0
	maximum = 128
Injected packet rate average = 0.00936979
	minimum = 0.0005 (at node 104)
	maximum = 0.025 (at node 171)
Accepted packet rate average = 0.00921094
	minimum = 0.004 (at node 23)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.16894
	minimum = 0.009 (at node 104)
	maximum = 0.45 (at node 171)
Accepted flit rate average= 0.165508
	minimum = 0.072 (at node 23)
	maximum = 0.2925 (at node 78)
Injected packet length average = 18.0303
Accepted packet length average = 17.9686
Total in-flight flits = 123123 (60419 measured)
latency change    = 0.464625
throughput change = 0.00561718
Class 0:
Packet latency average = 2894
	minimum = 32
	maximum = 5448
Network latency average = 1181.44
	minimum = 23
	maximum = 2971
Slowest packet = 12500
Flit latency average = 2518.91
	minimum = 6
	maximum = 5206
Slowest flit = 61865
Fragmentation average = 53.472
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00917014
	minimum = 0.00166667 (at node 20)
	maximum = 0.0233333 (at node 171)
Accepted packet rate average = 0.00915104
	minimum = 0.006 (at node 22)
	maximum = 0.0153333 (at node 78)
Injected flit rate average = 0.165054
	minimum = 0.03 (at node 20)
	maximum = 0.42 (at node 171)
Accepted flit rate average= 0.164969
	minimum = 0.104 (at node 161)
	maximum = 0.275667 (at node 78)
Injected packet length average = 17.9991
Accepted packet length average = 18.0273
Total in-flight flits = 121752 (81904 measured)
latency change    = 0.26319
throughput change = 0.00326766
Class 0:
Packet latency average = 3522.59
	minimum = 32
	maximum = 6676
Network latency average = 1632.45
	minimum = 23
	maximum = 3955
Slowest packet = 12500
Flit latency average = 2722.85
	minimum = 6
	maximum = 6266
Slowest flit = 64673
Fragmentation average = 59.985
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00916276
	minimum = 0.002 (at node 41)
	maximum = 0.02175 (at node 123)
Accepted packet rate average = 0.00909375
	minimum = 0.00525 (at node 36)
	maximum = 0.013 (at node 0)
Injected flit rate average = 0.164785
	minimum = 0.036 (at node 41)
	maximum = 0.3915 (at node 123)
Accepted flit rate average= 0.163512
	minimum = 0.0925 (at node 36)
	maximum = 0.2345 (at node 0)
Injected packet length average = 17.9842
Accepted packet length average = 17.9807
Total in-flight flits = 122769 (100069 measured)
latency change    = 0.178445
throughput change = 0.00891087
Class 0:
Packet latency average = 4189.97
	minimum = 32
	maximum = 7615
Network latency average = 2110.91
	minimum = 23
	maximum = 4899
Slowest packet = 12500
Flit latency average = 2895.78
	minimum = 6
	maximum = 6923
Slowest flit = 84653
Fragmentation average = 62.6845
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00928646
	minimum = 0.0028 (at node 95)
	maximum = 0.0184 (at node 123)
Accepted packet rate average = 0.00909896
	minimum = 0.0048 (at node 36)
	maximum = 0.0128 (at node 0)
Injected flit rate average = 0.167048
	minimum = 0.0512 (at node 95)
	maximum = 0.3312 (at node 123)
Accepted flit rate average= 0.163855
	minimum = 0.0848 (at node 36)
	maximum = 0.2332 (at node 0)
Injected packet length average = 17.9883
Accepted packet length average = 18.0081
Total in-flight flits = 124867 (113432 measured)
latency change    = 0.159279
throughput change = 0.0020963
Class 0:
Packet latency average = 4825.15
	minimum = 32
	maximum = 8419
Network latency average = 2469.92
	minimum = 23
	maximum = 5959
Slowest packet = 12500
Flit latency average = 3027.49
	minimum = 6
	maximum = 7912
Slowest flit = 77543
Fragmentation average = 65.765
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00916406
	minimum = 0.00283333 (at node 81)
	maximum = 0.0156667 (at node 171)
Accepted packet rate average = 0.00907205
	minimum = 0.00583333 (at node 36)
	maximum = 0.0128333 (at node 0)
Injected flit rate average = 0.164994
	minimum = 0.051 (at node 81)
	maximum = 0.282 (at node 171)
Accepted flit rate average= 0.163356
	minimum = 0.106 (at node 36)
	maximum = 0.230833 (at node 0)
Injected packet length average = 18.0045
Accepted packet length average = 18.0065
Total in-flight flits = 123862 (118578 measured)
latency change    = 0.13164
throughput change = 0.00305655
Class 0:
Packet latency average = 5403.72
	minimum = 32
	maximum = 9446
Network latency average = 2777.13
	minimum = 23
	maximum = 6759
Slowest packet = 12500
Flit latency average = 3146.57
	minimum = 6
	maximum = 8544
Slowest flit = 94949
Fragmentation average = 67.1079
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00909226
	minimum = 0.00328571 (at node 41)
	maximum = 0.0148571 (at node 27)
Accepted packet rate average = 0.00908333
	minimum = 0.00671429 (at node 36)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.163589
	minimum = 0.0591429 (at node 41)
	maximum = 0.266 (at node 27)
Accepted flit rate average= 0.163439
	minimum = 0.119714 (at node 36)
	maximum = 0.219286 (at node 128)
Injected packet length average = 17.9921
Accepted packet length average = 17.9933
Total in-flight flits = 122032 (119979 measured)
latency change    = 0.107069
throughput change = 0.000508357
Draining all recorded packets ...
Class 0:
Remaining flits: 160704 160705 160706 160707 160708 160709 160710 160711 160712 160713 [...] (121679 flits)
Measured flits: 225090 225091 225092 225093 225094 225095 225096 225097 225098 225099 [...] (120869 flits)
Class 0:
Remaining flits: 160704 160705 160706 160707 160708 160709 160710 160711 160712 160713 [...] (122234 flits)
Measured flits: 225504 225505 225506 225507 225508 225509 225510 225511 225512 225513 [...] (121879 flits)
Class 0:
Remaining flits: 161928 161929 161930 161931 161932 161933 161934 161935 161936 161937 [...] (122325 flits)
Measured flits: 225504 225505 225506 225507 225508 225509 225510 225511 225512 225513 [...] (122134 flits)
Class 0:
Remaining flits: 165744 165745 165746 165747 165748 165749 165750 165751 165752 165753 [...] (122570 flits)
Measured flits: 238338 238339 238340 238341 238342 238343 238344 238345 238346 238347 [...] (122498 flits)
Class 0:
Remaining flits: 174204 174205 174206 174207 174208 174209 174210 174211 174212 174213 [...] (121774 flits)
Measured flits: 268308 268309 268310 268311 268312 268313 268314 268315 268316 268317 [...] (121756 flits)
Class 0:
Remaining flits: 174204 174205 174206 174207 174208 174209 174210 174211 174212 174213 [...] (119998 flits)
Measured flits: 316566 316567 316568 316569 316570 316571 316572 316573 316574 316575 [...] (119962 flits)
Class 0:
Remaining flits: 318294 318295 318296 318297 318298 318299 318300 318301 318302 318303 [...] (119680 flits)
Measured flits: 318294 318295 318296 318297 318298 318299 318300 318301 318302 318303 [...] (119410 flits)
Class 0:
Remaining flits: 376808 376809 376810 376811 377730 377731 377732 377733 377734 377735 [...] (121511 flits)
Measured flits: 376808 376809 376810 376811 377730 377731 377732 377733 377734 377735 [...] (120935 flits)
Class 0:
Remaining flits: 397494 397495 397496 397497 397498 397499 397500 397501 397502 397503 [...] (122143 flits)
Measured flits: 397494 397495 397496 397497 397498 397499 397500 397501 397502 397503 [...] (121171 flits)
Class 0:
Remaining flits: 397501 397502 397503 397504 397505 397506 397507 397508 397509 397510 [...] (120771 flits)
Measured flits: 397501 397502 397503 397504 397505 397506 397507 397508 397509 397510 [...] (118941 flits)
Class 0:
Remaining flits: 465156 465157 465158 465159 465160 465161 465162 465163 465164 465165 [...] (120098 flits)
Measured flits: 465156 465157 465158 465159 465160 465161 465162 465163 465164 465165 [...] (117594 flits)
Class 0:
Remaining flits: 465156 465157 465158 465159 465160 465161 465162 465163 465164 465165 [...] (121083 flits)
Measured flits: 465156 465157 465158 465159 465160 465161 465162 465163 465164 465165 [...] (117667 flits)
Class 0:
Remaining flits: 484056 484057 484058 484059 484060 484061 484062 484063 484064 484065 [...] (121798 flits)
Measured flits: 484056 484057 484058 484059 484060 484061 484062 484063 484064 484065 [...] (117198 flits)
Class 0:
Remaining flits: 495360 495361 495362 495363 495364 495365 495366 495367 495368 495369 [...] (121352 flits)
Measured flits: 495360 495361 495362 495363 495364 495365 495366 495367 495368 495369 [...] (115040 flits)
Class 0:
Remaining flits: 512208 512209 512210 512211 512212 512213 512214 512215 512216 512217 [...] (121533 flits)
Measured flits: 512208 512209 512210 512211 512212 512213 512214 512215 512216 512217 [...] (113590 flits)
Class 0:
Remaining flits: 567702 567703 567704 567705 567706 567707 567708 567709 567710 567711 [...] (119844 flits)
Measured flits: 567702 567703 567704 567705 567706 567707 567708 567709 567710 567711 [...] (109710 flits)
Class 0:
Remaining flits: 590130 590131 590132 590133 590134 590135 590136 590137 590138 590139 [...] (119932 flits)
Measured flits: 590130 590131 590132 590133 590134 590135 590136 590137 590138 590139 [...] (106282 flits)
Class 0:
Remaining flits: 647946 647947 647948 647949 647950 647951 647952 647953 647954 647955 [...] (119428 flits)
Measured flits: 647946 647947 647948 647949 647950 647951 647952 647953 647954 647955 [...] (102267 flits)
Class 0:
Remaining flits: 651906 651907 651908 651909 651910 651911 651912 651913 651914 651915 [...] (118301 flits)
Measured flits: 651906 651907 651908 651909 651910 651911 651912 651913 651914 651915 [...] (96750 flits)
Class 0:
Remaining flits: 662886 662887 662888 662889 662890 662891 662892 662893 662894 662895 [...] (117580 flits)
Measured flits: 662886 662887 662888 662889 662890 662891 662892 662893 662894 662895 [...] (91389 flits)
Class 0:
Remaining flits: 685764 685765 685766 685767 685768 685769 685770 685771 685772 685773 [...] (120404 flits)
Measured flits: 731916 731917 731918 731919 731920 731921 731922 731923 731924 731925 [...] (89220 flits)
Class 0:
Remaining flits: 751464 751465 751466 751467 751468 751469 751470 751471 751472 751473 [...] (119833 flits)
Measured flits: 751464 751465 751466 751467 751468 751469 751470 751471 751472 751473 [...] (83675 flits)
Class 0:
Remaining flits: 751464 751465 751466 751467 751468 751469 751470 751471 751472 751473 [...] (119288 flits)
Measured flits: 751464 751465 751466 751467 751468 751469 751470 751471 751472 751473 [...] (79085 flits)
Class 0:
Remaining flits: 799866 799867 799868 799869 799870 799871 799872 799873 799874 799875 [...] (121600 flits)
Measured flits: 799866 799867 799868 799869 799870 799871 799872 799873 799874 799875 [...] (76746 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (122129 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (73455 flits)
Class 0:
Remaining flits: 901350 901351 901352 901353 901354 901355 901356 901357 901358 901359 [...] (121529 flits)
Measured flits: 909288 909289 909290 909291 909292 909293 909294 909295 909296 909297 [...] (69830 flits)
Class 0:
Remaining flits: 920304 920305 920306 920307 920308 920309 920310 920311 920312 920313 [...] (121138 flits)
Measured flits: 939096 939097 939098 939099 939100 939101 939102 939103 939104 939105 [...] (66012 flits)
Class 0:
Remaining flits: 922518 922519 922520 922521 922522 922523 922524 922525 922526 922527 [...] (119679 flits)
Measured flits: 939726 939727 939728 939729 939730 939731 939732 939733 939734 939735 [...] (60502 flits)
Class 0:
Remaining flits: 971406 971407 971408 971409 971410 971411 971412 971413 971414 971415 [...] (120530 flits)
Measured flits: 971406 971407 971408 971409 971410 971411 971412 971413 971414 971415 [...] (57644 flits)
Class 0:
Remaining flits: 1016046 1016047 1016048 1016049 1016050 1016051 1016052 1016053 1016054 1016055 [...] (121344 flits)
Measured flits: 1016046 1016047 1016048 1016049 1016050 1016051 1016052 1016053 1016054 1016055 [...] (53492 flits)
Class 0:
Remaining flits: 1029744 1029745 1029746 1029747 1029748 1029749 1029750 1029751 1029752 1029753 [...] (121620 flits)
Measured flits: 1029744 1029745 1029746 1029747 1029748 1029749 1029750 1029751 1029752 1029753 [...] (48611 flits)
Class 0:
Remaining flits: 1084770 1084771 1084772 1084773 1084774 1084775 1084776 1084777 1084778 1084779 [...] (122238 flits)
Measured flits: 1084770 1084771 1084772 1084773 1084774 1084775 1084776 1084777 1084778 1084779 [...] (42835 flits)
Class 0:
Remaining flits: 1085544 1085545 1085546 1085547 1085548 1085549 1085550 1085551 1085552 1085553 [...] (120190 flits)
Measured flits: 1085544 1085545 1085546 1085547 1085548 1085549 1085550 1085551 1085552 1085553 [...] (38121 flits)
Class 0:
Remaining flits: 1086947 1120284 1120285 1120286 1120287 1120288 1120289 1120290 1120291 1120292 [...] (118668 flits)
Measured flits: 1086947 1120284 1120285 1120286 1120287 1120288 1120289 1120290 1120291 1120292 [...] (34466 flits)
Class 0:
Remaining flits: 1120284 1120285 1120286 1120287 1120288 1120289 1120290 1120291 1120292 1120293 [...] (117856 flits)
Measured flits: 1120284 1120285 1120286 1120287 1120288 1120289 1120290 1120291 1120292 1120293 [...] (31185 flits)
Class 0:
Remaining flits: 1186812 1186813 1186814 1186815 1186816 1186817 1186818 1186819 1186820 1186821 [...] (117631 flits)
Measured flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (29573 flits)
Class 0:
Remaining flits: 1235754 1235755 1235756 1235757 1235758 1235759 1235760 1235761 1235762 1235763 [...] (118398 flits)
Measured flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (27305 flits)
Class 0:
Remaining flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (118493 flits)
Measured flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (24378 flits)
Class 0:
Remaining flits: 1311066 1311067 1311068 1311069 1311070 1311071 1311072 1311073 1311074 1311075 [...] (120727 flits)
Measured flits: 1313892 1313893 1313894 1313895 1313896 1313897 1313898 1313899 1313900 1313901 [...] (22402 flits)
Class 0:
Remaining flits: 1313892 1313893 1313894 1313895 1313896 1313897 1313898 1313899 1313900 1313901 [...] (122931 flits)
Measured flits: 1313892 1313893 1313894 1313895 1313896 1313897 1313898 1313899 1313900 1313901 [...] (20731 flits)
Class 0:
Remaining flits: 1371414 1371415 1371416 1371417 1371418 1371419 1374336 1374337 1374338 1374339 [...] (122113 flits)
Measured flits: 1386990 1386991 1386992 1386993 1386994 1386995 1386996 1386997 1386998 1386999 [...] (18749 flits)
Class 0:
Remaining flits: 1376334 1376335 1376336 1376337 1376338 1376339 1376340 1376341 1376342 1376343 [...] (120555 flits)
Measured flits: 1386990 1386991 1386992 1386993 1386994 1386995 1386996 1386997 1386998 1386999 [...] (15981 flits)
Class 0:
Remaining flits: 1376334 1376335 1376336 1376337 1376338 1376339 1376340 1376341 1376342 1376343 [...] (121000 flits)
Measured flits: 1386992 1386993 1386994 1386995 1386996 1386997 1386998 1386999 1387000 1387001 [...] (14055 flits)
Class 0:
Remaining flits: 1450566 1450567 1450568 1450569 1450570 1450571 1450572 1450573 1450574 1450575 [...] (120141 flits)
Measured flits: 1459566 1459567 1459568 1459569 1459570 1459571 1459572 1459573 1459574 1459575 [...] (12847 flits)
Class 0:
Remaining flits: 1459566 1459567 1459568 1459569 1459570 1459571 1459572 1459573 1459574 1459575 [...] (121144 flits)
Measured flits: 1459566 1459567 1459568 1459569 1459570 1459571 1459572 1459573 1459574 1459575 [...] (11347 flits)
Class 0:
Remaining flits: 1510916 1510917 1510918 1510919 1526734 1526735 1526736 1526737 1526738 1526739 [...] (121085 flits)
Measured flits: 1544598 1544599 1544600 1544601 1544602 1544603 1544604 1544605 1544606 1544607 [...] (10364 flits)
Class 0:
Remaining flits: 1571387 1571388 1571389 1571390 1571391 1571392 1571393 1571394 1571395 1571396 [...] (122912 flits)
Measured flits: 1588194 1588195 1588196 1588197 1588198 1588199 1588200 1588201 1588202 1588203 [...] (8450 flits)
Class 0:
Remaining flits: 1596564 1596565 1596566 1596567 1596568 1596569 1596570 1596571 1596572 1596573 [...] (121357 flits)
Measured flits: 1611108 1611109 1611110 1611111 1611112 1611113 1611114 1611115 1611116 1611117 [...] (7024 flits)
Class 0:
Remaining flits: 1610568 1610569 1610570 1610571 1610572 1610573 1610574 1610575 1610576 1610577 [...] (122181 flits)
Measured flits: 1644804 1644805 1644806 1644807 1644808 1644809 1644810 1644811 1644812 1644813 [...] (5476 flits)
Class 0:
Remaining flits: 1657296 1657297 1657298 1657299 1657300 1657301 1657302 1657303 1657304 1657305 [...] (121582 flits)
Measured flits: 1703106 1703107 1703108 1703109 1703110 1703111 1703112 1703113 1703114 1703115 [...] (4832 flits)
Class 0:
Remaining flits: 1661976 1661977 1661978 1661979 1661980 1661981 1661982 1661983 1661984 1661985 [...] (121042 flits)
Measured flits: 1703106 1703107 1703108 1703109 1703110 1703111 1703112 1703113 1703114 1703115 [...] (4045 flits)
Class 0:
Remaining flits: 1662894 1662895 1662896 1662897 1662898 1662899 1662900 1662901 1662902 1662903 [...] (118936 flits)
Measured flits: 1703106 1703107 1703108 1703109 1703110 1703111 1703112 1703113 1703114 1703115 [...] (3192 flits)
Class 0:
Remaining flits: 1729440 1729441 1729442 1729443 1729444 1729445 1729446 1729447 1729448 1729449 [...] (118555 flits)
Measured flits: 1820772 1820773 1820774 1820775 1820776 1820777 1820778 1820779 1820780 1820781 [...] (3027 flits)
Class 0:
Remaining flits: 1786914 1786915 1786916 1786917 1786918 1786919 1786920 1786921 1786922 1786923 [...] (118558 flits)
Measured flits: 1820772 1820773 1820774 1820775 1820776 1820777 1820778 1820779 1820780 1820781 [...] (2503 flits)
Class 0:
Remaining flits: 1801134 1801135 1801136 1801137 1801138 1801139 1801140 1801141 1801142 1801143 [...] (118319 flits)
Measured flits: 1972188 1972189 1972190 1972191 1972192 1972193 1972194 1972195 1972196 1972197 [...] (2283 flits)
Class 0:
Remaining flits: 1823004 1823005 1823006 1823007 1823008 1823009 1823010 1823011 1823012 1823013 [...] (118458 flits)
Measured flits: 1995138 1995139 1995140 1995141 1995142 1995143 1995144 1995145 1995146 1995147 [...] (2010 flits)
Class 0:
Remaining flits: 1874142 1874143 1874144 1874145 1874146 1874147 1874148 1874149 1874150 1874151 [...] (118655 flits)
Measured flits: 1995642 1995643 1995644 1995645 1995646 1995647 1995648 1995649 1995650 1995651 [...] (1896 flits)
Class 0:
Remaining flits: 1891440 1891441 1891442 1891443 1891444 1891445 1891446 1891447 1891448 1891449 [...] (119348 flits)
Measured flits: 2068884 2068885 2068886 2068887 2068888 2068889 2068890 2068891 2068892 2068893 [...] (1684 flits)
Class 0:
Remaining flits: 1891447 1891448 1891449 1891450 1891451 1891452 1891453 1891454 1891455 1891456 [...] (120095 flits)
Measured flits: 2098002 2098003 2098004 2098005 2098006 2098007 2101320 2101321 2101322 2101323 [...] (1452 flits)
Class 0:
Remaining flits: 1897344 1897345 1897346 1897347 1897348 1897349 1897350 1897351 1897352 1897353 [...] (120327 flits)
Measured flits: 2101320 2101321 2101322 2101323 2101324 2101325 2101326 2101327 2101328 2101329 [...] (871 flits)
Class 0:
Remaining flits: 1978524 1978525 1978526 1978527 1978528 1978529 1978530 1978531 1978532 1978533 [...] (120781 flits)
Measured flits: 2177298 2177299 2177300 2177301 2177302 2177303 2177304 2177305 2177306 2177307 [...] (599 flits)
Class 0:
Remaining flits: 1985022 1985023 1985024 1985025 1985026 1985027 1985028 1985029 1985030 1985031 [...] (119964 flits)
Measured flits: 2193750 2193751 2193752 2193753 2193754 2193755 2193756 2193757 2193758 2193759 [...] (450 flits)
Class 0:
Remaining flits: 2063898 2063899 2063900 2063901 2063902 2063903 2063904 2063905 2063906 2063907 [...] (117754 flits)
Measured flits: 2201544 2201545 2201546 2201547 2201548 2201549 2201550 2201551 2201552 2201553 [...] (396 flits)
Class 0:
Remaining flits: 2089674 2089675 2089676 2089677 2089678 2089679 2089680 2089681 2089682 2089683 [...] (117910 flits)
Measured flits: 2201550 2201551 2201552 2201553 2201554 2201555 2201556 2201557 2201558 2201559 [...] (390 flits)
Class 0:
Remaining flits: 2102679 2102680 2102681 2102682 2102683 2102684 2102685 2102686 2102687 2120076 [...] (118541 flits)
Measured flits: 2236788 2236789 2236790 2236791 2236792 2236793 2236794 2236795 2236796 2236797 [...] (90 flits)
Class 0:
Remaining flits: 2138645 2138646 2138647 2138648 2138649 2138650 2138651 2167218 2167219 2167220 [...] (116376 flits)
Measured flits: 2237670 2237671 2237672 2237673 2237674 2237675 2237676 2237677 2237678 2237679 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2181906 2181907 2181908 2181909 2181910 2181911 2181912 2181913 2181914 2181915 [...] (86213 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2206854 2206855 2206856 2206857 2206858 2206859 2206860 2206861 2206862 2206863 [...] (56951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2215008 2215009 2215010 2215011 2215012 2215013 2215014 2215015 2215016 2215017 [...] (29082 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2254387 2254388 2254389 2254390 2254391 2269262 2269263 2269264 2269265 2269266 [...] (5955 flits)
Measured flits: (0 flits)
Time taken is 81418 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 19821.8 (1 samples)
	minimum = 32 (1 samples)
	maximum = 66870 (1 samples)
Network latency average = 3838.07 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13828 (1 samples)
Flit latency average = 3797.41 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14032 (1 samples)
Fragmentation average = 71.5786 (1 samples)
	minimum = 0 (1 samples)
	maximum = 185 (1 samples)
Injected packet rate average = 0.00909226 (1 samples)
	minimum = 0.00328571 (1 samples)
	maximum = 0.0148571 (1 samples)
Accepted packet rate average = 0.00908333 (1 samples)
	minimum = 0.00671429 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.163589 (1 samples)
	minimum = 0.0591429 (1 samples)
	maximum = 0.266 (1 samples)
Accepted flit rate average = 0.163439 (1 samples)
	minimum = 0.119714 (1 samples)
	maximum = 0.219286 (1 samples)
Injected packet size average = 17.9921 (1 samples)
Accepted packet size average = 17.9933 (1 samples)
Hops average = 5.05582 (1 samples)
Total run time 61.9833
