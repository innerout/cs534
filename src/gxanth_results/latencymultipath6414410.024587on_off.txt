BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.018
	minimum = 25
	maximum = 984
Network latency average = 262.03
	minimum = 25
	maximum = 968
Slowest packet = 144
Flit latency average = 189.781
	minimum = 6
	maximum = 951
Slowest flit = 2609
Fragmentation average = 152.68
	minimum = 0
	maximum = 765
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00945833
	minimum = 0.004 (at node 41)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.199948
	minimum = 0.095 (at node 3)
	maximum = 0.35 (at node 70)
Injected packet length average = 17.8416
Accepted packet length average = 21.1399
Total in-flight flits = 41278 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.291
	minimum = 25
	maximum = 1919
Network latency average = 450.232
	minimum = 24
	maximum = 1799
Slowest packet = 167
Flit latency average = 356.61
	minimum = 6
	maximum = 1925
Slowest flit = 5092
Fragmentation average = 217.861
	minimum = 0
	maximum = 1468
Injected packet rate average = 0.0231823
	minimum = 0.002 (at node 118)
	maximum = 0.056 (at node 133)
Accepted packet rate average = 0.0108385
	minimum = 0.006 (at node 153)
	maximum = 0.0165 (at node 98)
Injected flit rate average = 0.415701
	minimum = 0.036 (at node 118)
	maximum = 1 (at node 133)
Accepted flit rate average= 0.214633
	minimum = 0.116 (at node 164)
	maximum = 0.3155 (at node 78)
Injected packet length average = 17.9318
Accepted packet length average = 19.8027
Total in-flight flits = 77817 (0 measured)
latency change    = 0.389077
throughput change = 0.0684187
Class 0:
Packet latency average = 1102.15
	minimum = 29
	maximum = 2894
Network latency average = 959.213
	minimum = 23
	maximum = 2757
Slowest packet = 282
Flit latency average = 860.32
	minimum = 6
	maximum = 2956
Slowest flit = 3227
Fragmentation average = 323.964
	minimum = 0
	maximum = 2198
Injected packet rate average = 0.0240729
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 25)
Accepted packet rate average = 0.012849
	minimum = 0.005 (at node 147)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.432667
	minimum = 0 (at node 15)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.237667
	minimum = 0.082 (at node 147)
	maximum = 0.404 (at node 99)
Injected packet length average = 17.9732
Accepted packet length average = 18.497
Total in-flight flits = 115381 (0 measured)
latency change    = 0.497992
throughput change = 0.0969166
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 375.058
	minimum = 28
	maximum = 2015
Network latency average = 215.872
	minimum = 27
	maximum = 984
Slowest packet = 13563
Flit latency average = 1200.26
	minimum = 6
	maximum = 3797
Slowest flit = 10997
Fragmentation average = 106.352
	minimum = 0
	maximum = 629
Injected packet rate average = 0.0246563
	minimum = 0 (at node 68)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0133698
	minimum = 0.003 (at node 175)
	maximum = 0.023 (at node 37)
Injected flit rate average = 0.443568
	minimum = 0 (at node 68)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.242891
	minimum = 0.098 (at node 175)
	maximum = 0.411 (at node 37)
Injected packet length average = 17.9901
Accepted packet length average = 18.1671
Total in-flight flits = 153958 (73294 measured)
latency change    = 1.93862
throughput change = 0.0215075
Class 0:
Packet latency average = 714.818
	minimum = 28
	maximum = 2785
Network latency average = 548.864
	minimum = 27
	maximum = 1961
Slowest packet = 13563
Flit latency average = 1405.75
	minimum = 6
	maximum = 4709
Slowest flit = 19901
Fragmentation average = 192.814
	minimum = 0
	maximum = 1564
Injected packet rate average = 0.0238932
	minimum = 0.002 (at node 72)
	maximum = 0.0555 (at node 19)
Accepted packet rate average = 0.0132448
	minimum = 0.0075 (at node 149)
	maximum = 0.02 (at node 123)
Injected flit rate average = 0.430354
	minimum = 0.036 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.24106
	minimum = 0.1485 (at node 98)
	maximum = 0.351 (at node 151)
Injected packet length average = 18.0116
Accepted packet length average = 18.2004
Total in-flight flits = 187964 (131299 measured)
latency change    = 0.475309
throughput change = 0.0075945
Class 0:
Packet latency average = 1059.09
	minimum = 28
	maximum = 3552
Network latency average = 888.558
	minimum = 27
	maximum = 2992
Slowest packet = 13563
Flit latency average = 1617.3
	minimum = 6
	maximum = 5792
Slowest flit = 15069
Fragmentation average = 237.288
	minimum = 0
	maximum = 1704
Injected packet rate average = 0.023401
	minimum = 0.00666667 (at node 185)
	maximum = 0.0556667 (at node 65)
Accepted packet rate average = 0.0131823
	minimum = 0.008 (at node 135)
	maximum = 0.0193333 (at node 123)
Injected flit rate average = 0.421082
	minimum = 0.119333 (at node 185)
	maximum = 1 (at node 65)
Accepted flit rate average= 0.238299
	minimum = 0.144 (at node 135)
	maximum = 0.326667 (at node 87)
Injected packet length average = 17.9941
Accepted packet length average = 18.0772
Total in-flight flits = 220743 (181591 measured)
latency change    = 0.325062
throughput change = 0.0115875
Class 0:
Packet latency average = 1414.69
	minimum = 28
	maximum = 4548
Network latency average = 1232.45
	minimum = 25
	maximum = 3990
Slowest packet = 13563
Flit latency average = 1835.31
	minimum = 6
	maximum = 6725
Slowest flit = 18215
Fragmentation average = 265.712
	minimum = 0
	maximum = 3051
Injected packet rate average = 0.0228477
	minimum = 0.00675 (at node 17)
	maximum = 0.045 (at node 65)
Accepted packet rate average = 0.0130807
	minimum = 0.008 (at node 135)
	maximum = 0.018 (at node 90)
Injected flit rate average = 0.411145
	minimum = 0.1215 (at node 17)
	maximum = 0.80975 (at node 65)
Accepted flit rate average= 0.236342
	minimum = 0.14325 (at node 135)
	maximum = 0.3195 (at node 93)
Injected packet length average = 17.995
Accepted packet length average = 18.068
Total in-flight flits = 249716 (223384 measured)
latency change    = 0.251363
throughput change = 0.00827682
Class 0:
Packet latency average = 1789.2
	minimum = 28
	maximum = 5863
Network latency average = 1591.49
	minimum = 25
	maximum = 4945
Slowest packet = 13563
Flit latency average = 2057.9
	minimum = 6
	maximum = 7433
Slowest flit = 30717
Fragmentation average = 287.677
	minimum = 0
	maximum = 4019
Injected packet rate average = 0.0223125
	minimum = 0.0066 (at node 72)
	maximum = 0.0408 (at node 177)
Accepted packet rate average = 0.013001
	minimum = 0.0076 (at node 135)
	maximum = 0.0174 (at node 90)
Injected flit rate average = 0.401525
	minimum = 0.1188 (at node 72)
	maximum = 0.7344 (at node 177)
Accepted flit rate average= 0.234718
	minimum = 0.1428 (at node 135)
	maximum = 0.3056 (at node 90)
Injected packet length average = 17.9955
Accepted packet length average = 18.0538
Total in-flight flits = 275612 (257955 measured)
latency change    = 0.209317
throughput change = 0.0069221
Class 0:
Packet latency average = 2152.53
	minimum = 28
	maximum = 6755
Network latency average = 1933.33
	minimum = 25
	maximum = 5853
Slowest packet = 13563
Flit latency average = 2261.01
	minimum = 6
	maximum = 8418
Slowest flit = 41820
Fragmentation average = 301.818
	minimum = 0
	maximum = 4019
Injected packet rate average = 0.0223568
	minimum = 0.00933333 (at node 72)
	maximum = 0.0381667 (at node 177)
Accepted packet rate average = 0.0129688
	minimum = 0.0085 (at node 135)
	maximum = 0.0165 (at node 68)
Injected flit rate average = 0.402111
	minimum = 0.1665 (at node 72)
	maximum = 0.688833 (at node 177)
Accepted flit rate average= 0.233942
	minimum = 0.157667 (at node 135)
	maximum = 0.294667 (at node 68)
Injected packet length average = 17.9861
Accepted packet length average = 18.0389
Total in-flight flits = 309488 (297030 measured)
latency change    = 0.168792
throughput change = 0.0033165
Class 0:
Packet latency average = 2482.96
	minimum = 27
	maximum = 8223
Network latency average = 2240.52
	minimum = 25
	maximum = 6934
Slowest packet = 13563
Flit latency average = 2453.86
	minimum = 6
	maximum = 9171
Slowest flit = 52973
Fragmentation average = 313.402
	minimum = 0
	maximum = 5343
Injected packet rate average = 0.0224219
	minimum = 0.0121429 (at node 82)
	maximum = 0.037 (at node 99)
Accepted packet rate average = 0.0129226
	minimum = 0.00914286 (at node 135)
	maximum = 0.0164286 (at node 68)
Injected flit rate average = 0.403446
	minimum = 0.218571 (at node 82)
	maximum = 0.666 (at node 99)
Accepted flit rate average= 0.232866
	minimum = 0.170143 (at node 135)
	maximum = 0.297571 (at node 68)
Injected packet length average = 17.9934
Accepted packet length average = 18.02
Total in-flight flits = 344839 (335802 measured)
latency change    = 0.133082
throughput change = 0.00461969
Draining all recorded packets ...
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (378817 flits)
Measured flits: 243807 243808 243809 243874 243875 243876 243877 243878 243879 243880 [...] (320980 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (408904 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (295051 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (438082 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (268329 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (461713 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (241861 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (485633 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (216596 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (506057 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (193972 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (533190 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (173125 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (556783 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (154610 flits)
Class 0:
Remaining flits: 34740 34741 34742 34743 34744 34745 34746 34747 34748 34749 [...] (578492 flits)
Measured flits: 244026 244027 244028 244029 244030 244031 244032 244033 244034 244035 [...] (137095 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (600425 flits)
Measured flits: 247518 247519 247520 247521 247522 247523 247524 247525 247526 247527 [...] (121499 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (623560 flits)
Measured flits: 247518 247519 247520 247521 247522 247523 247524 247525 247526 247527 [...] (107680 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (645259 flits)
Measured flits: 247528 247529 247530 247531 247532 247533 247534 247535 249030 249031 [...] (94813 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (660875 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (84024 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (668845 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (74208 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (674320 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (65002 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (679267 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (56235 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (687623 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (49018 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (697013 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (42099 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (707535 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (36392 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (711667 flits)
Measured flits: 249030 249031 249032 249033 249034 249035 249036 249037 249038 249039 [...] (31347 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (712881 flits)
Measured flits: 251082 251083 251084 251085 251086 251087 251088 251089 251090 251091 [...] (27080 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (713993 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (23630 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (713888 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (20603 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (716857 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (18115 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (718017 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (16013 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (724661 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (14220 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (727544 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (12933 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (721869 flits)
Measured flits: 252648 252649 252650 252651 252652 252653 252654 252655 252656 252657 [...] (11167 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (726906 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (9607 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (725846 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (8340 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (722657 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (7164 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (719902 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (6029 flits)
Class 0:
Remaining flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (722521 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (5034 flits)
Class 0:
Remaining flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (727551 flits)
Measured flits: 322992 322993 322994 322995 322996 322997 322998 322999 323000 323001 [...] (4161 flits)
Class 0:
Remaining flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (726712 flits)
Measured flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (3342 flits)
Class 0:
Remaining flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (721970 flits)
Measured flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (3023 flits)
Class 0:
Remaining flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (715829 flits)
Measured flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (2722 flits)
Class 0:
Remaining flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (709183 flits)
Measured flits: 384606 384607 384608 384609 384610 384611 384612 384613 384614 384615 [...] (2342 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (707547 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (2113 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (707750 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (1739 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (708042 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (1507 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (707723 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (1340 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (702542 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (1156 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (704191 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (882 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (705810 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (846 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (704995 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (787 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (702579 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (732 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (696275 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (589 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (694951 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (517 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (697467 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (473 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (700607 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (461 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (701121 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (412 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (703255 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (378 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (705791 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (306 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (706437 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (253 flits)
Class 0:
Remaining flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (700090 flits)
Measured flits: 452214 452215 452216 452217 452218 452219 452220 452221 452222 452223 [...] (253 flits)
Class 0:
Remaining flits: 688824 688825 688826 688827 688828 688829 688830 688831 688832 688833 [...] (697705 flits)
Measured flits: 688824 688825 688826 688827 688828 688829 688830 688831 688832 688833 [...] (180 flits)
Class 0:
Remaining flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (700256 flits)
Measured flits: 712476 712477 712478 712479 712480 712481 712482 712483 712484 712485 [...] (126 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (703049 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (93 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (702738 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (90 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (703089 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (72 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (700457 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (72 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (699164 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (36 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (697878 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (36 flits)
Class 0:
Remaining flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (697435 flits)
Measured flits: 761256 761257 761258 761259 761260 761261 761262 761263 761264 761265 [...] (36 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (696338 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (699592 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (696427 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (698011 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (694279 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (688711 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Class 0:
Remaining flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (687687 flits)
Measured flits: 773298 773299 773300 773301 773302 773303 773304 773305 773306 773307 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 873540 873541 873542 873543 873544 873545 873546 873547 873548 873549 [...] (654347 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 873540 873541 873542 873543 873544 873545 873546 873547 873548 873549 [...] (621495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 873540 873541 873542 873543 873544 873545 873546 873547 873548 873549 [...] (588072 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 873540 873541 873542 873543 873544 873545 873546 873547 873548 873549 [...] (555337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (523956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (491653 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (460394 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (430033 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (399708 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (370177 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (340666 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (310762 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (279994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (251213 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (223088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (193759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010412 1010413 1010414 1010415 1010416 1010417 1010418 1010419 1010420 1010421 [...] (167082 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010420 1010421 1010422 1010423 1010424 1010425 1010426 1010427 1010428 1010429 [...] (140953 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (114624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (90147 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (65933 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (45242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (28970 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (17710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1612170 1612171 1612172 1612173 1612174 1612175 1612176 1612177 1612178 1612179 [...] (9218 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2139534 2139535 2139536 2139537 2139538 2139539 2139540 2139541 2139542 2139543 [...] (4504 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2379081 2379082 2379083 2379084 2379085 2379086 2379087 2379088 2379089 2379090 [...] (2124 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2477808 2477809 2477810 2477811 2477812 2477813 2477814 2477815 2477816 2477817 [...] (366 flits)
Measured flits: (0 flits)
Time taken is 111357 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8263.61 (1 samples)
	minimum = 27 (1 samples)
	maximum = 73428 (1 samples)
Network latency average = 7570.41 (1 samples)
	minimum = 25 (1 samples)
	maximum = 73152 (1 samples)
Flit latency average = 16361.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 88352 (1 samples)
Fragmentation average = 344.151 (1 samples)
	minimum = 0 (1 samples)
	maximum = 10481 (1 samples)
Injected packet rate average = 0.0224219 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.037 (1 samples)
Accepted packet rate average = 0.0129226 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.0164286 (1 samples)
Injected flit rate average = 0.403446 (1 samples)
	minimum = 0.218571 (1 samples)
	maximum = 0.666 (1 samples)
Accepted flit rate average = 0.232866 (1 samples)
	minimum = 0.170143 (1 samples)
	maximum = 0.297571 (1 samples)
Injected packet size average = 17.9934 (1 samples)
Accepted packet size average = 18.02 (1 samples)
Hops average = 5.06162 (1 samples)
Total run time 223.105
