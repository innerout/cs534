BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 193.445
	minimum = 22
	maximum = 615
Network latency average = 188.698
	minimum = 22
	maximum = 615
Slowest packet = 1315
Flit latency average = 156.962
	minimum = 5
	maximum = 657
Slowest flit = 23352
Fragmentation average = 38.0101
	minimum = 0
	maximum = 218
Injected packet rate average = 0.0199948
	minimum = 0.012 (at node 59)
	maximum = 0.029 (at node 75)
Accepted packet rate average = 0.0134219
	minimum = 0.005 (at node 115)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.357115
	minimum = 0.216 (at node 59)
	maximum = 0.522 (at node 75)
Accepted flit rate average= 0.251271
	minimum = 0.09 (at node 115)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8604
Accepted packet length average = 18.721
Total in-flight flits = 20858 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 323.956
	minimum = 22
	maximum = 1225
Network latency average = 316.378
	minimum = 22
	maximum = 1202
Slowest packet = 2948
Flit latency average = 280.125
	minimum = 5
	maximum = 1185
Slowest flit = 53513
Fragmentation average = 46.3538
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0194896
	minimum = 0.0125 (at node 1)
	maximum = 0.0255 (at node 70)
Accepted packet rate average = 0.0141823
	minimum = 0.008 (at node 83)
	maximum = 0.02 (at node 98)
Injected flit rate average = 0.349081
	minimum = 0.221 (at node 1)
	maximum = 0.459 (at node 78)
Accepted flit rate average= 0.260531
	minimum = 0.144 (at node 153)
	maximum = 0.368 (at node 152)
Injected packet length average = 17.9111
Accepted packet length average = 18.3702
Total in-flight flits = 34938 (0 measured)
latency change    = 0.402867
throughput change = 0.0355444
Class 0:
Packet latency average = 675.308
	minimum = 22
	maximum = 2108
Network latency average = 631.835
	minimum = 22
	maximum = 2108
Slowest packet = 3107
Flit latency average = 590.349
	minimum = 5
	maximum = 2091
Slowest flit = 55943
Fragmentation average = 60.5478
	minimum = 0
	maximum = 287
Injected packet rate average = 0.0176302
	minimum = 0.007 (at node 58)
	maximum = 0.028 (at node 2)
Accepted packet rate average = 0.0148802
	minimum = 0.005 (at node 61)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.316286
	minimum = 0.134 (at node 58)
	maximum = 0.504 (at node 2)
Accepted flit rate average= 0.270115
	minimum = 0.106 (at node 61)
	maximum = 0.444 (at node 34)
Injected packet length average = 17.94
Accepted packet length average = 18.1526
Total in-flight flits = 44222 (0 measured)
latency change    = 0.520284
throughput change = 0.0354788
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 647.688
	minimum = 22
	maximum = 2030
Network latency average = 424.038
	minimum = 22
	maximum = 959
Slowest packet = 10908
Flit latency average = 759.074
	minimum = 5
	maximum = 2573
Slowest flit = 82781
Fragmentation average = 42.561
	minimum = 0
	maximum = 199
Injected packet rate average = 0.0161771
	minimum = 0.005 (at node 86)
	maximum = 0.026 (at node 50)
Accepted packet rate average = 0.0147656
	minimum = 0.007 (at node 1)
	maximum = 0.03 (at node 151)
Injected flit rate average = 0.291
	minimum = 0.095 (at node 86)
	maximum = 0.468 (at node 50)
Accepted flit rate average= 0.265813
	minimum = 0.11 (at node 53)
	maximum = 0.54 (at node 151)
Injected packet length average = 17.9884
Accepted packet length average = 18.0021
Total in-flight flits = 49310 (42386 measured)
latency change    = 0.0426454
throughput change = 0.0161847
Class 0:
Packet latency average = 1010.02
	minimum = 22
	maximum = 2531
Network latency average = 728.974
	minimum = 22
	maximum = 1964
Slowest packet = 10908
Flit latency average = 809.485
	minimum = 5
	maximum = 2661
Slowest flit = 153070
Fragmentation average = 59.1757
	minimum = 0
	maximum = 267
Injected packet rate average = 0.0158307
	minimum = 0.007 (at node 163)
	maximum = 0.024 (at node 35)
Accepted packet rate average = 0.0149245
	minimum = 0.008 (at node 36)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.284633
	minimum = 0.1305 (at node 163)
	maximum = 0.4275 (at node 35)
Accepted flit rate average= 0.268672
	minimum = 0.149 (at node 36)
	maximum = 0.3885 (at node 129)
Injected packet length average = 17.9798
Accepted packet length average = 18.0021
Total in-flight flits = 50834 (50333 measured)
latency change    = 0.358741
throughput change = 0.0106426
Class 0:
Packet latency average = 1210.75
	minimum = 22
	maximum = 3249
Network latency average = 853.957
	minimum = 22
	maximum = 2804
Slowest packet = 10908
Flit latency average = 855.924
	minimum = 5
	maximum = 3604
Slowest flit = 106739
Fragmentation average = 65.2556
	minimum = 0
	maximum = 267
Injected packet rate average = 0.0154462
	minimum = 0.00766667 (at node 74)
	maximum = 0.0216667 (at node 35)
Accepted packet rate average = 0.014809
	minimum = 0.00966667 (at node 36)
	maximum = 0.0196667 (at node 151)
Injected flit rate average = 0.277715
	minimum = 0.137 (at node 74)
	maximum = 0.387667 (at node 35)
Accepted flit rate average= 0.26676
	minimum = 0.173333 (at node 36)
	maximum = 0.354 (at node 151)
Injected packet length average = 17.9795
Accepted packet length average = 18.0134
Total in-flight flits = 51056 (51020 measured)
latency change    = 0.165783
throughput change = 0.00716545
Draining remaining packets ...
Class 0:
Remaining flits: 167868 167869 167870 167871 167872 167873 167874 167875 167876 167877 [...] (6423 flits)
Measured flits: 249840 249841 249842 249843 249844 249845 249846 249847 249848 249849 [...] (6405 flits)
Time taken is 7669 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1429.75 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4256 (1 samples)
Network latency average = 981.029 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3711 (1 samples)
Flit latency average = 934.634 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4607 (1 samples)
Fragmentation average = 69.0442 (1 samples)
	minimum = 0 (1 samples)
	maximum = 302 (1 samples)
Injected packet rate average = 0.0154462 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0216667 (1 samples)
Accepted packet rate average = 0.014809 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0196667 (1 samples)
Injected flit rate average = 0.277715 (1 samples)
	minimum = 0.137 (1 samples)
	maximum = 0.387667 (1 samples)
Accepted flit rate average = 0.26676 (1 samples)
	minimum = 0.173333 (1 samples)
	maximum = 0.354 (1 samples)
Injected packet size average = 17.9795 (1 samples)
Accepted packet size average = 18.0134 (1 samples)
Hops average = 5.10891 (1 samples)
Total run time 8.31422
