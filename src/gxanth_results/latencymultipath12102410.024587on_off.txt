BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.852
	minimum = 23
	maximum = 974
Network latency average = 250.692
	minimum = 23
	maximum = 835
Slowest packet = 85
Flit latency average = 213.731
	minimum = 6
	maximum = 855
Slowest flit = 10854
Fragmentation average = 53.954
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00961979
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.181568
	minimum = 0.067 (at node 174)
	maximum = 0.316 (at node 152)
Injected packet length average = 17.8416
Accepted packet length average = 18.8744
Total in-flight flits = 44807 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 555.514
	minimum = 23
	maximum = 1962
Network latency average = 457.037
	minimum = 23
	maximum = 1729
Slowest packet = 85
Flit latency average = 419.812
	minimum = 6
	maximum = 1815
Slowest flit = 13430
Fragmentation average = 59.5055
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0101172
	minimum = 0.004 (at node 174)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.186086
	minimum = 0.0835 (at node 174)
	maximum = 0.3305 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 18.3931
Total in-flight flits = 90867 (0 measured)
latency change    = 0.415222
throughput change = 0.0242803
Class 0:
Packet latency average = 1222.42
	minimum = 23
	maximum = 2945
Network latency average = 1083.55
	minimum = 23
	maximum = 2680
Slowest packet = 886
Flit latency average = 1055.39
	minimum = 6
	maximum = 2663
Slowest flit = 17153
Fragmentation average = 63.2132
	minimum = 0
	maximum = 174
Injected packet rate average = 0.0254635
	minimum = 0 (at node 165)
	maximum = 0.056 (at node 51)
Accepted packet rate average = 0.010724
	minimum = 0.004 (at node 32)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.458318
	minimum = 0 (at node 165)
	maximum = 1 (at node 51)
Accepted flit rate average= 0.193453
	minimum = 0.073 (at node 32)
	maximum = 0.34 (at node 144)
Injected packet length average = 17.999
Accepted packet length average = 18.0393
Total in-flight flits = 141726 (0 measured)
latency change    = 0.545561
throughput change = 0.0380825
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 265.495
	minimum = 23
	maximum = 1302
Network latency average = 109.215
	minimum = 23
	maximum = 916
Slowest packet = 13922
Flit latency average = 1569.69
	minimum = 6
	maximum = 3723
Slowest flit = 19966
Fragmentation average = 16.6957
	minimum = 0
	maximum = 127
Injected packet rate average = 0.023151
	minimum = 0 (at node 103)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0106406
	minimum = 0.004 (at node 2)
	maximum = 0.02 (at node 81)
Injected flit rate average = 0.417427
	minimum = 0 (at node 103)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.19024
	minimum = 0.072 (at node 117)
	maximum = 0.362 (at node 81)
Injected packet length average = 18.0306
Accepted packet length average = 17.8786
Total in-flight flits = 185210 (73260 measured)
latency change    = 3.6043
throughput change = 0.0168921
Class 0:
Packet latency average = 462.504
	minimum = 23
	maximum = 2364
Network latency average = 323.78
	minimum = 23
	maximum = 1915
Slowest packet = 13922
Flit latency average = 1828.6
	minimum = 6
	maximum = 4453
Slowest flit = 39891
Fragmentation average = 27.0667
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0227812
	minimum = 0.0035 (at node 1)
	maximum = 0.049 (at node 22)
Accepted packet rate average = 0.0103229
	minimum = 0.0045 (at node 144)
	maximum = 0.016 (at node 81)
Injected flit rate average = 0.410362
	minimum = 0.063 (at node 1)
	maximum = 0.887 (at node 22)
Accepted flit rate average= 0.186135
	minimum = 0.0875 (at node 144)
	maximum = 0.2815 (at node 81)
Injected packet length average = 18.0131
Accepted packet length average = 18.0313
Total in-flight flits = 227714 (142006 measured)
latency change    = 0.425962
throughput change = 0.0220494
Class 0:
Packet latency average = 849.24
	minimum = 23
	maximum = 3248
Network latency average = 707.159
	minimum = 23
	maximum = 2954
Slowest packet = 13922
Flit latency average = 2084.04
	minimum = 6
	maximum = 5167
Slowest flit = 64368
Fragmentation average = 37.3515
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0227587
	minimum = 0.003 (at node 101)
	maximum = 0.0503333 (at node 91)
Accepted packet rate average = 0.0103073
	minimum = 0.006 (at node 45)
	maximum = 0.015 (at node 81)
Injected flit rate average = 0.409865
	minimum = 0.054 (at node 101)
	maximum = 0.910333 (at node 91)
Accepted flit rate average= 0.185257
	minimum = 0.106333 (at node 84)
	maximum = 0.269333 (at node 81)
Injected packet length average = 18.0092
Accepted packet length average = 17.9734
Total in-flight flits = 270998 (207438 measured)
latency change    = 0.455391
throughput change = 0.00474191
Class 0:
Packet latency average = 1351.28
	minimum = 23
	maximum = 4314
Network latency average = 1207
	minimum = 23
	maximum = 3976
Slowest packet = 13922
Flit latency average = 2357.34
	minimum = 6
	maximum = 5896
Slowest flit = 78173
Fragmentation average = 45.0125
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0225924
	minimum = 0.0045 (at node 101)
	maximum = 0.04525 (at node 91)
Accepted packet rate average = 0.01025
	minimum = 0.00575 (at node 84)
	maximum = 0.01425 (at node 6)
Injected flit rate average = 0.406543
	minimum = 0.081 (at node 101)
	maximum = 0.81525 (at node 91)
Accepted flit rate average= 0.184462
	minimum = 0.104 (at node 84)
	maximum = 0.2545 (at node 6)
Injected packet length average = 17.9946
Accepted packet length average = 17.9963
Total in-flight flits = 312395 (266851 measured)
latency change    = 0.371528
throughput change = 0.00430823
Class 0:
Packet latency average = 1826.02
	minimum = 23
	maximum = 5433
Network latency average = 1681.87
	minimum = 23
	maximum = 4953
Slowest packet = 13922
Flit latency average = 2623.21
	minimum = 6
	maximum = 6882
Slowest flit = 87829
Fragmentation average = 49.3446
	minimum = 0
	maximum = 154
Injected packet rate average = 0.022376
	minimum = 0.0044 (at node 101)
	maximum = 0.0418 (at node 53)
Accepted packet rate average = 0.0102104
	minimum = 0.0064 (at node 84)
	maximum = 0.0138 (at node 129)
Injected flit rate average = 0.402771
	minimum = 0.0784 (at node 101)
	maximum = 0.7524 (at node 53)
Accepted flit rate average= 0.183666
	minimum = 0.114 (at node 84)
	maximum = 0.244 (at node 159)
Injected packet length average = 18.0001
Accepted packet length average = 17.9881
Total in-flight flits = 352101 (320540 measured)
latency change    = 0.259987
throughput change = 0.00433731
Class 0:
Packet latency average = 2317.25
	minimum = 23
	maximum = 6491
Network latency average = 2168.18
	minimum = 23
	maximum = 5928
Slowest packet = 13922
Flit latency average = 2894.45
	minimum = 6
	maximum = 8199
Slowest flit = 54071
Fragmentation average = 52.9686
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0225434
	minimum = 0.00733333 (at node 101)
	maximum = 0.0411667 (at node 90)
Accepted packet rate average = 0.0101675
	minimum = 0.00766667 (at node 139)
	maximum = 0.0135 (at node 129)
Injected flit rate average = 0.405698
	minimum = 0.130167 (at node 101)
	maximum = 0.7425 (at node 90)
Accepted flit rate average= 0.182784
	minimum = 0.138 (at node 139)
	maximum = 0.243 (at node 159)
Injected packet length average = 17.9963
Accepted packet length average = 17.9772
Total in-flight flits = 398637 (377631 measured)
latency change    = 0.211988
throughput change = 0.00482412
Class 0:
Packet latency average = 2791.18
	minimum = 23
	maximum = 7420
Network latency average = 2635.64
	minimum = 23
	maximum = 6908
Slowest packet = 13922
Flit latency average = 3154.97
	minimum = 6
	maximum = 8478
Slowest flit = 107477
Fragmentation average = 56.0984
	minimum = 0
	maximum = 154
Injected packet rate average = 0.022753
	minimum = 0.00842857 (at node 101)
	maximum = 0.0394286 (at node 105)
Accepted packet rate average = 0.0101696
	minimum = 0.00757143 (at node 139)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.409452
	minimum = 0.151714 (at node 101)
	maximum = 0.708571 (at node 105)
Accepted flit rate average= 0.182953
	minimum = 0.136286 (at node 139)
	maximum = 0.232 (at node 151)
Injected packet length average = 17.9956
Accepted packet length average = 17.9901
Total in-flight flits = 446277 (432562 measured)
latency change    = 0.169797
throughput change = 0.000925214
Draining all recorded packets ...
Class 0:
Remaining flits: 121590 121591 121592 121593 121594 121595 121596 121597 121598 121599 [...] (490327 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (425288 flits)
Class 0:
Remaining flits: 121600 121601 121602 121603 121604 121605 121606 121607 122940 122941 [...] (532693 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (404297 flits)
Class 0:
Remaining flits: 123408 123409 123410 123411 123412 123413 123414 123415 123416 123417 [...] (581642 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (380431 flits)
Class 0:
Remaining flits: 124776 124777 124778 124779 124780 124781 124782 124783 124784 124785 [...] (626864 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (355560 flits)
Class 0:
Remaining flits: 147868 147869 152532 152533 152534 152535 152536 152537 152538 152539 [...] (663710 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (329123 flits)
Class 0:
Remaining flits: 152532 152533 152534 152535 152536 152537 152538 152539 152540 152541 [...] (703394 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (302117 flits)
Class 0:
Remaining flits: 219024 219025 219026 219027 219028 219029 219030 219031 219032 219033 [...] (745823 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (274988 flits)
Class 0:
Remaining flits: 219024 219025 219026 219027 219028 219029 219030 219031 219032 219033 [...] (777158 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (247988 flits)
Class 0:
Remaining flits: 233514 233515 233516 233517 233518 233519 233520 233521 233522 233523 [...] (809320 flits)
Measured flits: 269406 269407 269408 269409 269410 269411 269412 269413 269414 269415 [...] (221432 flits)
Class 0:
Remaining flits: 273762 273763 273764 273765 273766 273767 273768 273769 273770 273771 [...] (840908 flits)
Measured flits: 273762 273763 273764 273765 273766 273767 273768 273769 273770 273771 [...] (196081 flits)
Class 0:
Remaining flits: 296118 296119 296120 296121 296122 296123 296124 296125 296126 296127 [...] (869589 flits)
Measured flits: 296118 296119 296120 296121 296122 296123 296124 296125 296126 296127 [...] (171771 flits)
Class 0:
Remaining flits: 302598 302599 302600 302601 302602 302603 302604 302605 302606 302607 [...] (891666 flits)
Measured flits: 302598 302599 302600 302601 302602 302603 302604 302605 302606 302607 [...] (149793 flits)
Class 0:
Remaining flits: 314406 314407 314408 314409 314410 314411 314412 314413 314414 314415 [...] (907927 flits)
Measured flits: 314406 314407 314408 314409 314410 314411 314412 314413 314414 314415 [...] (129051 flits)
Class 0:
Remaining flits: 332550 332551 332552 332553 332554 332555 332556 332557 332558 332559 [...] (919233 flits)
Measured flits: 332550 332551 332552 332553 332554 332555 332556 332557 332558 332559 [...] (110301 flits)
Class 0:
Remaining flits: 350536 350537 350538 350539 350540 350541 350542 350543 350544 350545 [...] (923881 flits)
Measured flits: 350536 350537 350538 350539 350540 350541 350542 350543 350544 350545 [...] (93617 flits)
Class 0:
Remaining flits: 362808 362809 362810 362811 362812 362813 362814 362815 362816 362817 [...] (929914 flits)
Measured flits: 362808 362809 362810 362811 362812 362813 362814 362815 362816 362817 [...] (79108 flits)
Class 0:
Remaining flits: 378720 378721 378722 378723 378724 378725 378726 378727 378728 378729 [...] (938994 flits)
Measured flits: 378720 378721 378722 378723 378724 378725 378726 378727 378728 378729 [...] (66111 flits)
Class 0:
Remaining flits: 389682 389683 389684 389685 389686 389687 389688 389689 389690 389691 [...] (944151 flits)
Measured flits: 389682 389683 389684 389685 389686 389687 389688 389689 389690 389691 [...] (54478 flits)
Class 0:
Remaining flits: 405378 405379 405380 405381 405382 405383 405384 405385 405386 405387 [...] (947071 flits)
Measured flits: 405378 405379 405380 405381 405382 405383 405384 405385 405386 405387 [...] (44535 flits)
Class 0:
Remaining flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (948024 flits)
Measured flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (36584 flits)
Class 0:
Remaining flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (947809 flits)
Measured flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (29244 flits)
Class 0:
Remaining flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (947589 flits)
Measured flits: 480582 480583 480584 480585 480586 480587 480588 480589 480590 480591 [...] (22790 flits)
Class 0:
Remaining flits: 509886 509887 509888 509889 509890 509891 509892 509893 509894 509895 [...] (944351 flits)
Measured flits: 509886 509887 509888 509889 509890 509891 509892 509893 509894 509895 [...] (17580 flits)
Class 0:
Remaining flits: 509886 509887 509888 509889 509890 509891 509892 509893 509894 509895 [...] (940896 flits)
Measured flits: 509886 509887 509888 509889 509890 509891 509892 509893 509894 509895 [...] (13916 flits)
Class 0:
Remaining flits: 524970 524971 524972 524973 524974 524975 524976 524977 524978 524979 [...] (939573 flits)
Measured flits: 524970 524971 524972 524973 524974 524975 524976 524977 524978 524979 [...] (11020 flits)
Class 0:
Remaining flits: 545796 545797 545798 545799 545800 545801 545802 545803 545804 545805 [...] (939254 flits)
Measured flits: 545796 545797 545798 545799 545800 545801 545802 545803 545804 545805 [...] (8828 flits)
Class 0:
Remaining flits: 589914 589915 589916 589917 589918 589919 589920 589921 589922 589923 [...] (936462 flits)
Measured flits: 589914 589915 589916 589917 589918 589919 589920 589921 589922 589923 [...] (6785 flits)
Class 0:
Remaining flits: 621720 621721 621722 621723 621724 621725 621726 621727 621728 621729 [...] (934624 flits)
Measured flits: 621720 621721 621722 621723 621724 621725 621726 621727 621728 621729 [...] (5291 flits)
Class 0:
Remaining flits: 621720 621721 621722 621723 621724 621725 621726 621727 621728 621729 [...] (933214 flits)
Measured flits: 621720 621721 621722 621723 621724 621725 621726 621727 621728 621729 [...] (3997 flits)
Class 0:
Remaining flits: 666954 666955 666956 666957 666958 666959 666960 666961 666962 666963 [...] (931101 flits)
Measured flits: 666954 666955 666956 666957 666958 666959 666960 666961 666962 666963 [...] (2884 flits)
Class 0:
Remaining flits: 670842 670843 670844 670845 670846 670847 670848 670849 670850 670851 [...] (931373 flits)
Measured flits: 670842 670843 670844 670845 670846 670847 670848 670849 670850 670851 [...] (2144 flits)
Class 0:
Remaining flits: 685080 685081 685082 685083 685084 685085 685086 685087 685088 685089 [...] (928645 flits)
Measured flits: 685080 685081 685082 685083 685084 685085 685086 685087 685088 685089 [...] (1482 flits)
Class 0:
Remaining flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (927089 flits)
Measured flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (1134 flits)
Class 0:
Remaining flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (926358 flits)
Measured flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (787 flits)
Class 0:
Remaining flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (921078 flits)
Measured flits: 687852 687853 687854 687855 687856 687857 687858 687859 687860 687861 [...] (575 flits)
Class 0:
Remaining flits: 696582 696583 696584 696585 696586 696587 696588 696589 696590 696591 [...] (917030 flits)
Measured flits: 696582 696583 696584 696585 696586 696587 696588 696589 696590 696591 [...] (386 flits)
Class 0:
Remaining flits: 709578 709579 709580 709581 709582 709583 709584 709585 709586 709587 [...] (914656 flits)
Measured flits: 709578 709579 709580 709581 709582 709583 709584 709585 709586 709587 [...] (241 flits)
Class 0:
Remaining flits: 819594 819595 819596 819597 819598 819599 819600 819601 819602 819603 [...] (916443 flits)
Measured flits: 1298916 1298917 1298918 1298919 1298920 1298921 1298922 1298923 1298924 1298925 [...] (126 flits)
Class 0:
Remaining flits: 827244 827245 827246 827247 827248 827249 827250 827251 827252 827253 [...] (915320 flits)
Measured flits: 1316070 1316071 1316072 1316073 1316074 1316075 1316076 1316077 1316078 1316079 [...] (108 flits)
Class 0:
Remaining flits: 827256 827257 827258 827259 827260 827261 874854 874855 874856 874857 [...] (913989 flits)
Measured flits: 1355022 1355023 1355024 1355025 1355026 1355027 1355028 1355029 1355030 1355031 [...] (90 flits)
Class 0:
Remaining flits: 915354 915355 915356 915357 915358 915359 915360 915361 915362 915363 [...] (911360 flits)
Measured flits: 1397466 1397467 1397468 1397469 1397470 1397471 1397472 1397473 1397474 1397475 [...] (54 flits)
Class 0:
Remaining flits: 915354 915355 915356 915357 915358 915359 915360 915361 915362 915363 [...] (910021 flits)
Measured flits: 1580364 1580365 1580366 1580367 1580368 1580369 1580370 1580371 1580372 1580373 [...] (36 flits)
Class 0:
Remaining flits: 919692 919693 919694 919695 919696 919697 919698 919699 919700 919701 [...] (907897 flits)
Measured flits: 1581534 1581535 1581536 1581537 1581538 1581539 1581540 1581541 1581542 1581543 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 927540 927541 927542 927543 927544 927545 927546 927547 927548 927549 [...] (875141 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933948 933949 933950 933951 933952 933953 933954 933955 933956 933957 [...] (844031 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933948 933949 933950 933951 933952 933953 933954 933955 933956 933957 [...] (812477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 933948 933949 933950 933951 933952 933953 933954 933955 933956 933957 [...] (781589 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 963468 963469 963470 963471 963472 963473 963474 963475 963476 963477 [...] (749937 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 963468 963469 963470 963471 963472 963473 963474 963475 963476 963477 [...] (719477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 971550 971551 971552 971553 971554 971555 971556 971557 971558 971559 [...] (688250 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 971550 971551 971552 971553 971554 971555 971556 971557 971558 971559 [...] (656692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1067778 1067779 1067780 1067781 1067782 1067783 1067784 1067785 1067786 1067787 [...] (625694 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1067778 1067779 1067780 1067781 1067782 1067783 1067784 1067785 1067786 1067787 [...] (595344 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1084446 1084447 1084448 1084449 1084450 1084451 1084452 1084453 1084454 1084455 [...] (565126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1084446 1084447 1084448 1084449 1084450 1084451 1084452 1084453 1084454 1084455 [...] (534687 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091556 1091557 1091558 1091559 1091560 1091561 1091562 1091563 1091564 1091565 [...] (503956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1194570 1194571 1194572 1194573 1194574 1194575 1194576 1194577 1194578 1194579 [...] (472887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1194577 1194578 1194579 1194580 1194581 1194582 1194583 1194584 1194585 1194586 [...] (441149 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270638 1270639 1270640 1270641 1270642 1270643 1270644 1270645 1270646 1270647 [...] (410018 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1286622 1286623 1286624 1286625 1286626 1286627 1286628 1286629 1286630 1286631 [...] (378948 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1302894 1302895 1302896 1302897 1302898 1302899 1302900 1302901 1302902 1302903 [...] (349032 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1370484 1370485 1370486 1370487 1370488 1370489 1370490 1370491 1370492 1370493 [...] (318818 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1370484 1370485 1370486 1370487 1370488 1370489 1370490 1370491 1370492 1370493 [...] (289455 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1560258 1560259 1560260 1560261 1560262 1560263 1560264 1560265 1560266 1560267 [...] (259810 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1585635 1585636 1585637 1624338 1624339 1624340 1624341 1624342 1624343 1624344 [...] (230163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1624338 1624339 1624340 1624341 1624342 1624343 1624344 1624345 1624346 1624347 [...] (201364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1639332 1639333 1639334 1639335 1639336 1639337 1639338 1639339 1639340 1639341 [...] (173210 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1708560 1708561 1708562 1708563 1708564 1708565 1708566 1708567 1708568 1708569 [...] (144884 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1730412 1730413 1730414 1730415 1730416 1730417 1730418 1730419 1730420 1730421 [...] (116182 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1758960 1758961 1758962 1758963 1758964 1758965 1758966 1758967 1758968 1758969 [...] (88055 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1841166 1841167 1841168 1841169 1841170 1841171 1841172 1841173 1841174 1841175 [...] (61520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1841166 1841167 1841168 1841169 1841170 1841171 1841172 1841173 1841174 1841175 [...] (38628 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1952046 1952047 1952048 1952049 1952050 1952051 1952052 1952053 1952054 1952055 [...] (18877 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1965294 1965295 1965296 1965297 1965298 1965299 1965300 1965301 1965302 1965303 [...] (5594 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2174382 2174383 2174384 2174385 2174386 2174387 2174388 2174389 2174390 2174391 [...] (734 flits)
Measured flits: (0 flits)
Time taken is 86604 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10655.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 43981 (1 samples)
Network latency average = 10242.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 38831 (1 samples)
Flit latency average = 20680.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 56969 (1 samples)
Fragmentation average = 72.0309 (1 samples)
	minimum = 0 (1 samples)
	maximum = 189 (1 samples)
Injected packet rate average = 0.022753 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0394286 (1 samples)
Accepted packet rate average = 0.0101696 (1 samples)
	minimum = 0.00757143 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.409452 (1 samples)
	minimum = 0.151714 (1 samples)
	maximum = 0.708571 (1 samples)
Accepted flit rate average = 0.182953 (1 samples)
	minimum = 0.136286 (1 samples)
	maximum = 0.232 (1 samples)
Injected packet size average = 17.9956 (1 samples)
Accepted packet size average = 17.9901 (1 samples)
Hops average = 5.08133 (1 samples)
Total run time 66.4168
