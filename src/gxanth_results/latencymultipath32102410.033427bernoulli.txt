BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 306.022
	minimum = 23
	maximum = 972
Network latency average = 296.566
	minimum = 23
	maximum = 972
Slowest packet = 135
Flit latency average = 236.695
	minimum = 6
	maximum = 955
Slowest flit = 2447
Fragmentation average = 153.324
	minimum = 0
	maximum = 932
Injected packet rate average = 0.0329635
	minimum = 0.017 (at node 97)
	maximum = 0.048 (at node 118)
Accepted packet rate average = 0.0107812
	minimum = 0.004 (at node 127)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.587724
	minimum = 0.306 (at node 97)
	maximum = 0.862 (at node 118)
Accepted flit rate average= 0.222526
	minimum = 0.1 (at node 127)
	maximum = 0.354 (at node 34)
Injected packet length average = 17.8295
Accepted packet length average = 20.6401
Total in-flight flits = 71197 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 565.818
	minimum = 23
	maximum = 1818
Network latency average = 554.7
	minimum = 23
	maximum = 1818
Slowest packet = 935
Flit latency average = 483.418
	minimum = 6
	maximum = 1886
Slowest flit = 10966
Fragmentation average = 186.302
	minimum = 0
	maximum = 1465
Injected packet rate average = 0.03325
	minimum = 0.0205 (at node 62)
	maximum = 0.043 (at node 117)
Accepted packet rate average = 0.0121068
	minimum = 0.005 (at node 153)
	maximum = 0.017 (at node 63)
Injected flit rate average = 0.596021
	minimum = 0.369 (at node 62)
	maximum = 0.771 (at node 118)
Accepted flit rate average= 0.232698
	minimum = 0.097 (at node 153)
	maximum = 0.3415 (at node 63)
Injected packet length average = 17.9254
Accepted packet length average = 19.2205
Total in-flight flits = 140468 (0 measured)
latency change    = 0.459152
throughput change = 0.0437128
Class 0:
Packet latency average = 1295.54
	minimum = 23
	maximum = 2742
Network latency average = 1282.34
	minimum = 23
	maximum = 2733
Slowest packet = 1325
Flit latency average = 1231.91
	minimum = 6
	maximum = 2853
Slowest flit = 13317
Fragmentation average = 211.106
	minimum = 0
	maximum = 2139
Injected packet rate average = 0.0330729
	minimum = 0.016 (at node 111)
	maximum = 0.051 (at node 17)
Accepted packet rate average = 0.0134115
	minimum = 0.007 (at node 0)
	maximum = 0.023 (at node 136)
Injected flit rate average = 0.59449
	minimum = 0.283 (at node 111)
	maximum = 0.923 (at node 17)
Accepted flit rate average= 0.239677
	minimum = 0.115 (at node 188)
	maximum = 0.41 (at node 186)
Injected packet length average = 17.9751
Accepted packet length average = 17.8711
Total in-flight flits = 208750 (0 measured)
latency change    = 0.563256
throughput change = 0.029119
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 65.4917
	minimum = 23
	maximum = 453
Network latency average = 52.1039
	minimum = 23
	maximum = 451
Slowest packet = 20493
Flit latency average = 1709.84
	minimum = 6
	maximum = 3758
Slowest flit = 24225
Fragmentation average = 13.9796
	minimum = 0
	maximum = 48
Injected packet rate average = 0.0338698
	minimum = 0.019 (at node 87)
	maximum = 0.05 (at node 98)
Accepted packet rate average = 0.0134375
	minimum = 0.007 (at node 88)
	maximum = 0.024 (at node 75)
Injected flit rate average = 0.609469
	minimum = 0.35 (at node 87)
	maximum = 0.904 (at node 121)
Accepted flit rate average= 0.239193
	minimum = 0.106 (at node 88)
	maximum = 0.398 (at node 75)
Injected packet length average = 17.9945
Accepted packet length average = 17.8004
Total in-flight flits = 279879 (107181 measured)
latency change    = 18.7817
throughput change = 0.00202504
Class 0:
Packet latency average = 70.6013
	minimum = 23
	maximum = 1860
Network latency average = 57.2815
	minimum = 23
	maximum = 1860
Slowest packet = 19229
Flit latency average = 1980.89
	minimum = 6
	maximum = 4646
Slowest flit = 38651
Fragmentation average = 14.7819
	minimum = 0
	maximum = 183
Injected packet rate average = 0.033474
	minimum = 0.024 (at node 87)
	maximum = 0.0445 (at node 114)
Accepted packet rate average = 0.0131094
	minimum = 0.0065 (at node 48)
	maximum = 0.0205 (at node 34)
Injected flit rate average = 0.602742
	minimum = 0.4305 (at node 87)
	maximum = 0.801 (at node 114)
Accepted flit rate average= 0.234747
	minimum = 0.131 (at node 48)
	maximum = 0.3915 (at node 90)
Injected packet length average = 18.0063
Accepted packet length average = 17.9068
Total in-flight flits = 349979 (212463 measured)
latency change    = 0.0723739
throughput change = 0.0189366
Class 0:
Packet latency average = 205.559
	minimum = 23
	maximum = 2937
Network latency average = 192.767
	minimum = 23
	maximum = 2930
Slowest packet = 19241
Flit latency average = 2232.41
	minimum = 6
	maximum = 5541
Slowest flit = 39475
Fragmentation average = 25.0618
	minimum = 0
	maximum = 380
Injected packet rate average = 0.033474
	minimum = 0.0226667 (at node 87)
	maximum = 0.0423333 (at node 114)
Accepted packet rate average = 0.0130972
	minimum = 0.00833333 (at node 48)
	maximum = 0.0193333 (at node 90)
Injected flit rate average = 0.602689
	minimum = 0.410667 (at node 87)
	maximum = 0.765333 (at node 114)
Accepted flit rate average= 0.234347
	minimum = 0.149 (at node 48)
	maximum = 0.348333 (at node 90)
Injected packet length average = 18.0047
Accepted packet length average = 17.8929
Total in-flight flits = 420824 (316526 measured)
latency change    = 0.65654
throughput change = 0.00170761
Class 0:
Packet latency average = 537.538
	minimum = 23
	maximum = 3986
Network latency average = 524.807
	minimum = 23
	maximum = 3983
Slowest packet = 19160
Flit latency average = 2491.85
	minimum = 6
	maximum = 6317
Slowest flit = 72769
Fragmentation average = 42.5837
	minimum = 0
	maximum = 1262
Injected packet rate average = 0.033487
	minimum = 0.023 (at node 87)
	maximum = 0.04125 (at node 35)
Accepted packet rate average = 0.0130586
	minimum = 0.0085 (at node 141)
	maximum = 0.01925 (at node 130)
Injected flit rate average = 0.602991
	minimum = 0.41175 (at node 87)
	maximum = 0.7445 (at node 114)
Accepted flit rate average= 0.235012
	minimum = 0.15225 (at node 141)
	maximum = 0.34125 (at node 130)
Injected packet length average = 18.0067
Accepted packet length average = 17.9967
Total in-flight flits = 491185 (416709 measured)
latency change    = 0.617591
throughput change = 0.0028275
Class 0:
Packet latency average = 1139.45
	minimum = 23
	maximum = 5014
Network latency average = 1126.76
	minimum = 23
	maximum = 4990
Slowest packet = 19169
Flit latency average = 2744.55
	minimum = 6
	maximum = 7320
Slowest flit = 76057
Fragmentation average = 68.0219
	minimum = 0
	maximum = 1856
Injected packet rate average = 0.0335615
	minimum = 0.026 (at node 18)
	maximum = 0.0418 (at node 35)
Accepted packet rate average = 0.0130969
	minimum = 0.0086 (at node 54)
	maximum = 0.019 (at node 130)
Injected flit rate average = 0.60412
	minimum = 0.4672 (at node 18)
	maximum = 0.754 (at node 35)
Accepted flit rate average= 0.234832
	minimum = 0.1528 (at node 54)
	maximum = 0.3358 (at node 130)
Injected packet length average = 18.0004
Accepted packet length average = 17.9304
Total in-flight flits = 563253 (510830 measured)
latency change    = 0.528248
throughput change = 0.000764065
Class 0:
Packet latency average = 1755.82
	minimum = 23
	maximum = 5981
Network latency average = 1743.27
	minimum = 23
	maximum = 5976
Slowest packet = 19120
Flit latency average = 2994.81
	minimum = 6
	maximum = 8384
Slowest flit = 58049
Fragmentation average = 86.7693
	minimum = 0
	maximum = 1856
Injected packet rate average = 0.0335356
	minimum = 0.0256667 (at node 18)
	maximum = 0.0398333 (at node 97)
Accepted packet rate average = 0.0130469
	minimum = 0.00883333 (at node 54)
	maximum = 0.0175 (at node 130)
Injected flit rate average = 0.603856
	minimum = 0.463667 (at node 18)
	maximum = 0.717 (at node 97)
Accepted flit rate average= 0.234337
	minimum = 0.163833 (at node 54)
	maximum = 0.312333 (at node 130)
Injected packet length average = 18.0064
Accepted packet length average = 17.9611
Total in-flight flits = 634188 (597637 measured)
latency change    = 0.351045
throughput change = 0.00211442
Class 0:
Packet latency average = 2337.06
	minimum = 23
	maximum = 6947
Network latency average = 2324.51
	minimum = 23
	maximum = 6947
Slowest packet = 19125
Flit latency average = 3249.83
	minimum = 6
	maximum = 8977
Slowest flit = 115188
Fragmentation average = 102.871
	minimum = 0
	maximum = 2689
Injected packet rate average = 0.0334472
	minimum = 0.0258571 (at node 18)
	maximum = 0.0387143 (at node 35)
Accepted packet rate average = 0.0129926
	minimum = 0.00885714 (at node 80)
	maximum = 0.0161429 (at node 118)
Injected flit rate average = 0.602126
	minimum = 0.466143 (at node 18)
	maximum = 0.696857 (at node 97)
Accepted flit rate average= 0.233246
	minimum = 0.158429 (at node 80)
	maximum = 0.290714 (at node 138)
Injected packet length average = 18.0023
Accepted packet length average = 17.9522
Total in-flight flits = 704422 (679182 measured)
latency change    = 0.248704
throughput change = 0.00467863
Draining all recorded packets ...
Class 0:
Remaining flits: 82495 82496 82497 82498 82499 82500 82501 82502 82503 82504 [...] (774785 flits)
Measured flits: 344124 344125 344126 344127 344128 344129 344130 344131 344132 344133 [...] (653803 flits)
Class 0:
Remaining flits: 106286 106287 106288 106289 135270 135271 135272 135273 135274 135275 [...] (836591 flits)
Measured flits: 344124 344125 344126 344127 344128 344129 344130 344131 344132 344133 [...] (624602 flits)
Class 0:
Remaining flits: 135270 135271 135272 135273 135274 135275 135276 135277 135278 135279 [...] (898958 flits)
Measured flits: 344124 344125 344126 344127 344128 344129 344130 344131 344132 344133 [...] (594821 flits)
Class 0:
Remaining flits: 182286 182287 182288 182289 182290 182291 182292 182293 182294 182295 [...] (958462 flits)
Measured flits: 344124 344125 344126 344127 344128 344129 344130 344131 344132 344133 [...] (563440 flits)
Class 0:
Remaining flits: 182286 182287 182288 182289 182290 182291 182292 182293 182294 182295 [...] (1020042 flits)
Measured flits: 344135 344136 344137 344138 344139 344140 344141 344574 344575 344576 [...] (531259 flits)
Class 0:
Remaining flits: 182286 182287 182288 182289 182290 182291 182292 182293 182294 182295 [...] (1080737 flits)
Measured flits: 344574 344575 344576 344577 344578 344579 344580 344581 344582 344583 [...] (498809 flits)
Class 0:
Remaining flits: 219996 219997 219998 219999 220000 220001 220002 220003 220004 220005 [...] (1141462 flits)
Measured flits: 344574 344575 344576 344577 344578 344579 344580 344581 344582 344583 [...] (466184 flits)
Class 0:
Remaining flits: 231480 231481 231482 231483 231484 231485 231486 231487 231488 231489 [...] (1202106 flits)
Measured flits: 344574 344575 344576 344577 344578 344579 344580 344581 344582 344583 [...] (433587 flits)
Class 0:
Remaining flits: 231480 231481 231482 231483 231484 231485 231486 231487 231488 231489 [...] (1262054 flits)
Measured flits: 344574 344575 344576 344577 344578 344579 344580 344581 344582 344583 [...] (400695 flits)
Class 0:
Remaining flits: 231480 231481 231482 231483 231484 231485 231486 231487 231488 231489 [...] (1322636 flits)
Measured flits: 345600 345601 345602 345603 345604 345605 345606 345607 345608 345609 [...] (367543 flits)
Class 0:
Remaining flits: 261450 261451 261452 261453 261454 261455 261456 261457 261458 261459 [...] (1385198 flits)
Measured flits: 345600 345601 345602 345603 345604 345605 345606 345607 345608 345609 [...] (334803 flits)
Class 0:
Remaining flits: 261450 261451 261452 261453 261454 261455 261456 261457 261458 261459 [...] (1444028 flits)
Measured flits: 345600 345601 345602 345603 345604 345605 345606 345607 345608 345609 [...] (302779 flits)
Class 0:
Remaining flits: 303588 303589 303590 303591 303592 303593 303594 303595 303596 303597 [...] (1503419 flits)
Measured flits: 347436 347437 347438 347439 347440 347441 347442 347443 347444 347445 [...] (271302 flits)
Class 0:
Remaining flits: 303588 303589 303590 303591 303592 303593 303594 303595 303596 303597 [...] (1564286 flits)
Measured flits: 347850 347851 347852 347853 347854 347855 347856 347857 347858 347859 [...] (242165 flits)
Class 0:
Remaining flits: 303588 303589 303590 303591 303592 303593 303594 303595 303596 303597 [...] (1624076 flits)
Measured flits: 358794 358795 358796 358797 358798 358799 358800 358801 358802 358803 [...] (214063 flits)
Class 0:
Remaining flits: 303588 303589 303590 303591 303592 303593 303594 303595 303596 303597 [...] (1685516 flits)
Measured flits: 358794 358795 358796 358797 358798 358799 358800 358801 358802 358803 [...] (188480 flits)
Class 0:
Remaining flits: 303588 303589 303590 303591 303592 303593 303594 303595 303596 303597 [...] (1746314 flits)
Measured flits: 358794 358795 358796 358797 358798 358799 358800 358801 358802 358803 [...] (165359 flits)
Class 0:
Remaining flits: 303597 303598 303599 303600 303601 303602 303603 303604 303605 315882 [...] (1807954 flits)
Measured flits: 358794 358795 358796 358797 358798 358799 358800 358801 358802 358803 [...] (145125 flits)
Class 0:
Remaining flits: 358808 358809 358810 358811 373932 373933 373934 373935 373936 373937 [...] (1868928 flits)
Measured flits: 358808 358809 358810 358811 373932 373933 373934 373935 373936 373937 [...] (125378 flits)
Class 0:
Remaining flits: 463732 463733 468738 468739 468740 468741 468742 468743 468744 468745 [...] (1928100 flits)
Measured flits: 463732 463733 468738 468739 468740 468741 468742 468743 468744 468745 [...] (107930 flits)
Class 0:
Remaining flits: 468738 468739 468740 468741 468742 468743 468744 468745 468746 468747 [...] (1987684 flits)
Measured flits: 468738 468739 468740 468741 468742 468743 468744 468745 468746 468747 [...] (92108 flits)
Class 0:
Remaining flits: 468738 468739 468740 468741 468742 468743 468744 468745 468746 468747 [...] (2047554 flits)
Measured flits: 468738 468739 468740 468741 468742 468743 468744 468745 468746 468747 [...] (78699 flits)
Class 0:
Remaining flits: 514476 514477 514478 514479 514480 514481 514482 514483 514484 514485 [...] (2106934 flits)
Measured flits: 514476 514477 514478 514479 514480 514481 514482 514483 514484 514485 [...] (67305 flits)
Class 0:
Remaining flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (2159071 flits)
Measured flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (57219 flits)
Class 0:
Remaining flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (2206418 flits)
Measured flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (48079 flits)
Class 0:
Remaining flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (2241425 flits)
Measured flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (41222 flits)
Class 0:
Remaining flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (2269004 flits)
Measured flits: 565128 565129 565130 565131 565132 565133 565134 565135 565136 565137 [...] (34794 flits)
Class 0:
Remaining flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (2289319 flits)
Measured flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (29272 flits)
Class 0:
Remaining flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (2305273 flits)
Measured flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (24557 flits)
Class 0:
Remaining flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (2317375 flits)
Measured flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (19890 flits)
Class 0:
Remaining flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (2330793 flits)
Measured flits: 627750 627751 627752 627753 627754 627755 627756 627757 627758 627759 [...] (16414 flits)
Class 0:
Remaining flits: 673020 673021 673022 673023 673024 673025 673026 673027 673028 673029 [...] (2341734 flits)
Measured flits: 673020 673021 673022 673023 673024 673025 673026 673027 673028 673029 [...] (13233 flits)
Class 0:
Remaining flits: 673020 673021 673022 673023 673024 673025 673026 673027 673028 673029 [...] (2356729 flits)
Measured flits: 673020 673021 673022 673023 673024 673025 673026 673027 673028 673029 [...] (10722 flits)
Class 0:
Remaining flits: 718621 718622 718623 718624 718625 718626 718627 718628 718629 718630 [...] (2365323 flits)
Measured flits: 718621 718622 718623 718624 718625 718626 718627 718628 718629 718630 [...] (8807 flits)
Class 0:
Remaining flits: 781704 781705 781706 781707 781708 781709 781710 781711 781712 781713 [...] (2376861 flits)
Measured flits: 781704 781705 781706 781707 781708 781709 781710 781711 781712 781713 [...] (7138 flits)
Class 0:
Remaining flits: 781704 781705 781706 781707 781708 781709 781710 781711 781712 781713 [...] (2387776 flits)
Measured flits: 781704 781705 781706 781707 781708 781709 781710 781711 781712 781713 [...] (5566 flits)
Class 0:
Remaining flits: 857232 857233 857234 857235 857236 857237 857238 857239 857240 857241 [...] (2400878 flits)
Measured flits: 857232 857233 857234 857235 857236 857237 857238 857239 857240 857241 [...] (4346 flits)
Class 0:
Remaining flits: 857232 857233 857234 857235 857236 857237 857238 857239 857240 857241 [...] (2410499 flits)
Measured flits: 857232 857233 857234 857235 857236 857237 857238 857239 857240 857241 [...] (3489 flits)
Class 0:
Remaining flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2421112 flits)
Measured flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2944 flits)
Class 0:
Remaining flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2433346 flits)
Measured flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2309 flits)
Class 0:
Remaining flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (2441976 flits)
Measured flits: 902484 902485 902486 902487 902488 902489 902490 902491 902492 902493 [...] (1866 flits)
Class 0:
Remaining flits: 902486 902487 902488 902489 902490 902491 902492 902493 902494 902495 [...] (2449197 flits)
Measured flits: 902486 902487 902488 902489 902490 902491 902492 902493 902494 902495 [...] (1461 flits)
Class 0:
Remaining flits: 919656 919657 919658 919659 919660 919661 919662 919663 919664 919665 [...] (2451636 flits)
Measured flits: 919656 919657 919658 919659 919660 919661 919662 919663 919664 919665 [...] (1126 flits)
Class 0:
Remaining flits: 919669 919670 919671 919672 919673 961254 961255 961256 961257 961258 [...] (2458645 flits)
Measured flits: 919669 919670 919671 919672 919673 961254 961255 961256 961257 961258 [...] (889 flits)
Class 0:
Remaining flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (2464252 flits)
Measured flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (617 flits)
Class 0:
Remaining flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (2470531 flits)
Measured flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (485 flits)
Class 0:
Remaining flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (2473361 flits)
Measured flits: 974268 974269 974270 974271 974272 974273 974274 974275 974276 974277 [...] (382 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2477478 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (216 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2483526 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (196 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2484943 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (177 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2482524 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (142 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2478228 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (127 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2476702 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (121 flits)
Class 0:
Remaining flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (2468637 flits)
Measured flits: 1049598 1049599 1049600 1049601 1049602 1049603 1049604 1049605 1049606 1049607 [...] (92 flits)
Class 0:
Remaining flits: 1141394 1141395 1141396 1141397 1180584 1180585 1180586 1180587 1180588 1180589 [...] (2461247 flits)
Measured flits: 1141394 1141395 1141396 1141397 (4 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1180584 1180585 1180586 1180587 1180588 1180589 1180590 1180591 1180592 1180593 [...] (2426207 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1180584 1180585 1180586 1180587 1180588 1180589 1180590 1180591 1180592 1180593 [...] (2391771 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1243944 1243945 1243946 1243947 1243948 1243949 1243950 1243951 1243952 1243953 [...] (2357795 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1243944 1243945 1243946 1243947 1243948 1243949 1243950 1243951 1243952 1243953 [...] (2323423 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1302984 1302985 1302986 1302987 1302988 1302989 1302990 1302991 1302992 1302993 [...] (2288563 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1313442 1313443 1313444 1313445 1313446 1313447 1313448 1313449 1313450 1313451 [...] (2253901 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1313442 1313443 1313444 1313445 1313446 1313447 1313448 1313449 1313450 1313451 [...] (2219105 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1322694 1322695 1322696 1322697 1322698 1322699 1322700 1322701 1322702 1322703 [...] (2184393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1322694 1322695 1322696 1322697 1322698 1322699 1322700 1322701 1322702 1322703 [...] (2148861 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (2113240 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (2077630 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (2042054 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (2006262 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453338 1453339 1453340 1453341 1453342 1453343 1453344 1453345 1453346 1453347 [...] (1971111 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1473732 1473733 1473734 1473735 1473736 1473737 1473738 1473739 1473740 1473741 [...] (1936044 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1478214 1478215 1478216 1478217 1478218 1478219 1478220 1478221 1478222 1478223 [...] (1901454 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1526850 1526851 1526852 1526853 1526854 1526855 1526856 1526857 1526858 1526859 [...] (1867322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1526858 1526859 1526860 1526861 1526862 1526863 1526864 1526865 1526866 1526867 [...] (1832160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1543860 1543861 1543862 1543863 1543864 1543865 1543866 1543867 1543868 1543869 [...] (1797459 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1543860 1543861 1543862 1543863 1543864 1543865 1543866 1543867 1543868 1543869 [...] (1762129 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1727464 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1692245 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1655310 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1619308 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1583218 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1609272 1609273 1609274 1609275 1609276 1609277 1609278 1609279 1609280 1609281 [...] (1547576 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1644660 1644661 1644662 1644663 1644664 1644665 1644666 1644667 1644668 1644669 [...] (1512379 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1758294 1758295 1758296 1758297 1758298 1758299 1758300 1758301 1758302 1758303 [...] (1476920 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836432 1836433 1836434 1836435 1836436 1836437 1836438 1836439 1836440 1836441 [...] (1441019 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836432 1836433 1836434 1836435 1836436 1836437 1836438 1836439 1836440 1836441 [...] (1405684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836437 1836438 1836439 1836440 1836441 1836442 1836443 1836444 1836445 1836446 [...] (1370487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888038 1888039 1888040 1888041 1888042 1888043 1888044 1888045 1888046 1888047 [...] (1336008 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888038 1888039 1888040 1888041 1888042 1888043 1888044 1888045 1888046 1888047 [...] (1301755 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998432 1998433 1998434 1998435 1998436 1998437 1998438 1998439 1998440 1998441 [...] (1267271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998433 1998434 1998435 1998436 1998437 1998438 1998439 1998440 1998441 1998442 [...] (1232019 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2039268 2039269 2039270 2039271 2039272 2039273 2191050 2191051 2191052 2191053 [...] (1197567 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (1163741 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (1129619 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (1095290 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (1061219 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (1027455 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (993888 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2219112 2219113 2219114 2219115 2219116 2219117 2219118 2219119 2219120 2219121 [...] (960689 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2254842 2254843 2254844 2254845 2254846 2254847 2254848 2254849 2254850 2254851 [...] (926964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2373516 2373517 2373518 2373519 2373520 2373521 2373522 2373523 2373524 2373525 [...] (892538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2373516 2373517 2373518 2373519 2373520 2373521 2373522 2373523 2373524 2373525 [...] (859044 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2373516 2373517 2373518 2373519 2373520 2373521 2373522 2373523 2373524 2373525 [...] (825898 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2508408 2508409 2508410 2508411 2508412 2508413 2508414 2508415 2508416 2508417 [...] (792523 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2508408 2508409 2508410 2508411 2508412 2508413 2508414 2508415 2508416 2508417 [...] (758506 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2526678 2526679 2526680 2526681 2526682 2526683 2526684 2526685 2526686 2526687 [...] (724754 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2526693 2526694 2526695 2612898 2612899 2612900 2612901 2612902 2612903 2612904 [...] (690584 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2612898 2612899 2612900 2612901 2612902 2612903 2612904 2612905 2612906 2612907 [...] (656733 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2612898 2612899 2612900 2612901 2612902 2612903 2612904 2612905 2612906 2612907 [...] (622499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2632482 2632483 2632484 2632485 2632486 2632487 2632488 2632489 2632490 2632491 [...] (588471 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2632482 2632483 2632484 2632485 2632486 2632487 2632488 2632489 2632490 2632491 [...] (554273 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638836 2638837 2638838 2638839 2638840 2638841 2638842 2638843 2638844 2638845 [...] (520657 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638836 2638837 2638838 2638839 2638840 2638841 2638842 2638843 2638844 2638845 [...] (486475 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638836 2638837 2638838 2638839 2638840 2638841 2638842 2638843 2638844 2638845 [...] (452176 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638836 2638837 2638838 2638839 2638840 2638841 2638842 2638843 2638844 2638845 [...] (417456 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638836 2638837 2638838 2638839 2638840 2638841 2638842 2638843 2638844 2638845 [...] (382580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (347701 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (313168 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (278606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (245441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (212410 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2757060 2757061 2757062 2757063 2757064 2757065 2757066 2757067 2757068 2757069 [...] (180576 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2857482 2857483 2857484 2857485 2857486 2857487 2857488 2857489 2857490 2857491 [...] (149729 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2857482 2857483 2857484 2857485 2857486 2857487 2857488 2857489 2857490 2857491 [...] (120607 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2960550 2960551 2960552 2960553 2960554 2960555 2960556 2960557 2960558 2960559 [...] (93675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2960550 2960551 2960552 2960553 2960554 2960555 2960556 2960557 2960558 2960559 [...] (68832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018762 3018763 3018764 3018765 3018766 3018767 3018768 3018769 3018770 3018771 [...] (49191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3152952 3152953 3152954 3152955 3152956 3152957 3152958 3152959 3152960 3152961 [...] (31763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3152964 3152965 3152966 3152967 3152968 3152969 3235212 3235213 3235214 3235215 [...] (18321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3235212 3235213 3235214 3235215 3235216 3235217 3235218 3235219 3235220 3235221 [...] (12215 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3235212 3235213 3235214 3235215 3235216 3235217 3235218 3235219 3235220 3235221 [...] (7757 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3505248 3505249 3505250 3505251 3505252 3505253 3505254 3505255 3505256 3505257 [...] (4259 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3819726 3819727 3819728 3819729 3819730 3819731 3819732 3819733 3819734 3819735 [...] (1409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4575150 4575151 4575152 4575153 4575154 4575155 4575156 4575157 4575158 4575159 [...] (70 flits)
Measured flits: (0 flits)
Time taken is 143104 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13246.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 55267 (1 samples)
Network latency average = 13233.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 55258 (1 samples)
Flit latency average = 41033.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 109681 (1 samples)
Fragmentation average = 195.039 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5625 (1 samples)
Injected packet rate average = 0.0334472 (1 samples)
	minimum = 0.0258571 (1 samples)
	maximum = 0.0387143 (1 samples)
Accepted packet rate average = 0.0129926 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0161429 (1 samples)
Injected flit rate average = 0.602126 (1 samples)
	minimum = 0.466143 (1 samples)
	maximum = 0.696857 (1 samples)
Accepted flit rate average = 0.233246 (1 samples)
	minimum = 0.158429 (1 samples)
	maximum = 0.290714 (1 samples)
Injected packet size average = 18.0023 (1 samples)
Accepted packet size average = 17.9522 (1 samples)
Hops average = 5.07107 (1 samples)
Total run time 184.29
