BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 300.081
	minimum = 22
	maximum = 850
Network latency average = 285.699
	minimum = 22
	maximum = 793
Slowest packet = 666
Flit latency average = 249.841
	minimum = 5
	maximum = 839
Slowest flit = 15817
Fragmentation average = 84.5238
	minimum = 0
	maximum = 639
Injected packet rate average = 0.0351354
	minimum = 0.024 (at node 4)
	maximum = 0.049 (at node 7)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 96)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.627089
	minimum = 0.424 (at node 120)
	maximum = 0.87 (at node 7)
Accepted flit rate average= 0.278573
	minimum = 0.114 (at node 115)
	maximum = 0.473 (at node 76)
Injected packet length average = 17.8478
Accepted packet length average = 19.5776
Total in-flight flits = 68140 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 588.729
	minimum = 22
	maximum = 1655
Network latency average = 559.417
	minimum = 22
	maximum = 1625
Slowest packet = 2437
Flit latency average = 509.97
	minimum = 5
	maximum = 1658
Slowest flit = 41893
Fragmentation average = 112.969
	minimum = 0
	maximum = 823
Injected packet rate average = 0.0293073
	minimum = 0.018 (at node 4)
	maximum = 0.0395 (at node 7)
Accepted packet rate average = 0.0147552
	minimum = 0.0095 (at node 96)
	maximum = 0.021 (at node 14)
Injected flit rate average = 0.523966
	minimum = 0.3185 (at node 4)
	maximum = 0.703 (at node 7)
Accepted flit rate average= 0.277018
	minimum = 0.1795 (at node 153)
	maximum = 0.393 (at node 44)
Injected packet length average = 17.8784
Accepted packet length average = 18.7743
Total in-flight flits = 96467 (0 measured)
latency change    = 0.49029
throughput change = 0.00561222
Class 0:
Packet latency average = 1497.83
	minimum = 35
	maximum = 2339
Network latency average = 1364.59
	minimum = 22
	maximum = 2273
Slowest packet = 4377
Flit latency average = 1310.43
	minimum = 5
	maximum = 2307
Slowest flit = 85042
Fragmentation average = 143.121
	minimum = 0
	maximum = 804
Injected packet rate average = 0.0159635
	minimum = 0.004 (at node 136)
	maximum = 0.033 (at node 145)
Accepted packet rate average = 0.0149844
	minimum = 0.005 (at node 184)
	maximum = 0.026 (at node 182)
Injected flit rate average = 0.288125
	minimum = 0.072 (at node 136)
	maximum = 0.599 (at node 145)
Accepted flit rate average= 0.268641
	minimum = 0.101 (at node 184)
	maximum = 0.471 (at node 182)
Injected packet length average = 18.0489
Accepted packet length average = 17.9281
Total in-flight flits = 100382 (0 measured)
latency change    = 0.606946
throughput change = 0.0311852
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1330.51
	minimum = 259
	maximum = 2620
Network latency average = 88.1707
	minimum = 22
	maximum = 987
Slowest packet = 14375
Flit latency average = 1663.28
	minimum = 5
	maximum = 3071
Slowest flit = 93761
Fragmentation average = 9.26016
	minimum = 0
	maximum = 180
Injected packet rate average = 0.0156563
	minimum = 0.001 (at node 51)
	maximum = 0.031 (at node 135)
Accepted packet rate average = 0.0149063
	minimum = 0.005 (at node 1)
	maximum = 0.027 (at node 128)
Injected flit rate average = 0.281688
	minimum = 0.003 (at node 51)
	maximum = 0.562 (at node 135)
Accepted flit rate average= 0.269187
	minimum = 0.114 (at node 1)
	maximum = 0.512 (at node 128)
Injected packet length average = 17.992
Accepted packet length average = 18.0587
Total in-flight flits = 102680 (49429 measured)
latency change    = 0.125759
throughput change = 0.00203158
Class 0:
Packet latency average = 2154.43
	minimum = 259
	maximum = 3794
Network latency average = 670.013
	minimum = 22
	maximum = 1988
Slowest packet = 14375
Flit latency average = 1749.19
	minimum = 5
	maximum = 3711
Slowest flit = 148693
Fragmentation average = 53.2966
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0154583
	minimum = 0.0055 (at node 48)
	maximum = 0.025 (at node 35)
Accepted packet rate average = 0.0150495
	minimum = 0.0095 (at node 146)
	maximum = 0.022 (at node 181)
Injected flit rate average = 0.278242
	minimum = 0.0995 (at node 48)
	maximum = 0.4535 (at node 35)
Accepted flit rate average= 0.269714
	minimum = 0.16 (at node 146)
	maximum = 0.399 (at node 128)
Injected packet length average = 17.9995
Accepted packet length average = 17.9218
Total in-flight flits = 103822 (88640 measured)
latency change    = 0.382433
throughput change = 0.00195037
Class 0:
Packet latency average = 2901.29
	minimum = 259
	maximum = 4489
Network latency average = 1332.25
	minimum = 22
	maximum = 2960
Slowest packet = 14375
Flit latency average = 1800.23
	minimum = 5
	maximum = 4248
Slowest flit = 153467
Fragmentation average = 91.2238
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0153229
	minimum = 0.00533333 (at node 48)
	maximum = 0.023 (at node 143)
Accepted packet rate average = 0.0150156
	minimum = 0.0103333 (at node 139)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.275938
	minimum = 0.0966667 (at node 48)
	maximum = 0.409 (at node 143)
Accepted flit rate average= 0.270092
	minimum = 0.187 (at node 146)
	maximum = 0.378 (at node 128)
Injected packet length average = 18.0082
Accepted packet length average = 17.9874
Total in-flight flits = 103785 (102383 measured)
latency change    = 0.257421
throughput change = 0.00140127
Class 0:
Packet latency average = 3372.41
	minimum = 259
	maximum = 5455
Network latency average = 1652
	minimum = 22
	maximum = 3908
Slowest packet = 14375
Flit latency average = 1834.96
	minimum = 5
	maximum = 4280
Slowest flit = 242223
Fragmentation average = 103.344
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0152682
	minimum = 0.00775 (at node 48)
	maximum = 0.02175 (at node 35)
Accepted packet rate average = 0.0150234
	minimum = 0.01075 (at node 190)
	maximum = 0.02 (at node 118)
Injected flit rate average = 0.274879
	minimum = 0.14125 (at node 48)
	maximum = 0.39 (at node 35)
Accepted flit rate average= 0.269984
	minimum = 0.20275 (at node 190)
	maximum = 0.362 (at node 118)
Injected packet length average = 18.0033
Accepted packet length average = 17.9709
Total in-flight flits = 104084 (104009 measured)
latency change    = 0.1397
throughput change = 0.000398686
Class 0:
Packet latency average = 3745.6
	minimum = 259
	maximum = 6184
Network latency average = 1787.92
	minimum = 22
	maximum = 4572
Slowest packet = 14375
Flit latency average = 1862.17
	minimum = 5
	maximum = 4872
Slowest flit = 239939
Fragmentation average = 106.567
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0152521
	minimum = 0.0066 (at node 68)
	maximum = 0.0202 (at node 149)
Accepted packet rate average = 0.0150187
	minimum = 0.0114 (at node 31)
	maximum = 0.0198 (at node 128)
Injected flit rate average = 0.2746
	minimum = 0.1196 (at node 68)
	maximum = 0.3634 (at node 149)
Accepted flit rate average= 0.269672
	minimum = 0.2034 (at node 42)
	maximum = 0.356 (at node 128)
Injected packet length average = 18.0041
Accepted packet length average = 17.9557
Total in-flight flits = 105125 (105125 measured)
latency change    = 0.099633
throughput change = 0.00115882
Class 0:
Packet latency average = 4076.36
	minimum = 259
	maximum = 7023
Network latency average = 1855.25
	minimum = 22
	maximum = 4787
Slowest packet = 14375
Flit latency average = 1879.49
	minimum = 5
	maximum = 4872
Slowest flit = 239939
Fragmentation average = 108.704
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0151155
	minimum = 0.00816667 (at node 48)
	maximum = 0.0193333 (at node 41)
Accepted packet rate average = 0.0149939
	minimum = 0.0116667 (at node 42)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.272137
	minimum = 0.1485 (at node 48)
	maximum = 0.347 (at node 98)
Accepted flit rate average= 0.269638
	minimum = 0.213 (at node 126)
	maximum = 0.344 (at node 138)
Injected packet length average = 18.0039
Accepted packet length average = 17.9832
Total in-flight flits = 103229 (103229 measured)
latency change    = 0.0811405
throughput change = 0.000125554
Class 0:
Packet latency average = 4398.15
	minimum = 259
	maximum = 7663
Network latency average = 1894.39
	minimum = 22
	maximum = 4975
Slowest packet = 14375
Flit latency average = 1890.35
	minimum = 5
	maximum = 4958
Slowest flit = 351863
Fragmentation average = 109.737
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0151734
	minimum = 0.00857143 (at node 48)
	maximum = 0.0205714 (at node 143)
Accepted packet rate average = 0.0149851
	minimum = 0.012 (at node 42)
	maximum = 0.0188571 (at node 181)
Injected flit rate average = 0.273208
	minimum = 0.153571 (at node 48)
	maximum = 0.37 (at node 143)
Accepted flit rate average= 0.269424
	minimum = 0.213286 (at node 42)
	maximum = 0.342857 (at node 181)
Injected packet length average = 18.0057
Accepted packet length average = 17.9794
Total in-flight flits = 105458 (105458 measured)
latency change    = 0.0731655
throughput change = 0.000793966
Draining all recorded packets ...
Class 0:
Remaining flits: 394506 394507 394508 394509 394510 394511 394512 394513 394514 394515 [...] (107268 flits)
Measured flits: 394506 394507 394508 394509 394510 394511 394512 394513 394514 394515 [...] (107268 flits)
Class 0:
Remaining flits: 394506 394507 394508 394509 394510 394511 394512 394513 394514 394515 [...] (106068 flits)
Measured flits: 394506 394507 394508 394509 394510 394511 394512 394513 394514 394515 [...] (106068 flits)
Class 0:
Remaining flits: 476478 476479 476480 476481 476482 476483 476484 476485 476486 476487 [...] (105865 flits)
Measured flits: 476478 476479 476480 476481 476482 476483 476484 476485 476486 476487 [...] (105865 flits)
Class 0:
Remaining flits: 556398 556399 556400 556401 556402 556403 556404 556405 556406 556407 [...] (106251 flits)
Measured flits: 556398 556399 556400 556401 556402 556403 556404 556405 556406 556407 [...] (106251 flits)
Class 0:
Remaining flits: 556411 556412 556413 556414 556415 593276 593277 593278 593279 596764 [...] (106731 flits)
Measured flits: 556411 556412 556413 556414 556415 593276 593277 593278 593279 596764 [...] (106731 flits)
Class 0:
Remaining flits: 647440 647441 650862 650863 650864 650865 650866 650867 650868 650869 [...] (107128 flits)
Measured flits: 647440 647441 650862 650863 650864 650865 650866 650867 650868 650869 [...] (107128 flits)
Class 0:
Remaining flits: 690246 690247 690248 690249 690250 690251 690252 690253 690254 690255 [...] (105780 flits)
Measured flits: 690246 690247 690248 690249 690250 690251 690252 690253 690254 690255 [...] (105780 flits)
Class 0:
Remaining flits: 746998 746999 776412 776413 776414 776415 776416 776417 776418 776419 [...] (106602 flits)
Measured flits: 746998 746999 776412 776413 776414 776415 776416 776417 776418 776419 [...] (106602 flits)
Class 0:
Remaining flits: 821841 821842 821843 868012 868013 870282 870283 870284 870285 870286 [...] (105855 flits)
Measured flits: 821841 821842 821843 868012 868013 870282 870283 870284 870285 870286 [...] (105783 flits)
Class 0:
Remaining flits: 885294 885295 885296 885297 885298 885299 885300 885301 885302 885303 [...] (106124 flits)
Measured flits: 885294 885295 885296 885297 885298 885299 885300 885301 885302 885303 [...] (103352 flits)
Class 0:
Remaining flits: 897894 897895 897896 897897 897898 897899 897900 897901 897902 897903 [...] (105528 flits)
Measured flits: 897894 897895 897896 897897 897898 897899 897900 897901 897902 897903 [...] (93907 flits)
Class 0:
Remaining flits: 941418 941419 941420 941421 941422 941423 941424 941425 941426 941427 [...] (105362 flits)
Measured flits: 941418 941419 941420 941421 941422 941423 941424 941425 941426 941427 [...] (76977 flits)
Class 0:
Remaining flits: 941425 941426 941427 941428 941429 941430 941431 941432 941433 941434 [...] (104380 flits)
Measured flits: 941425 941426 941427 941428 941429 941430 941431 941432 941433 941434 [...] (53215 flits)
Class 0:
Remaining flits: 1070190 1070191 1070192 1070193 1070194 1070195 1070196 1070197 1070198 1070199 [...] (103898 flits)
Measured flits: 1070190 1070191 1070192 1070193 1070194 1070195 1070196 1070197 1070198 1070199 [...] (34695 flits)
Class 0:
Remaining flits: 1070203 1070204 1070205 1070206 1070207 1078068 1078069 1078070 1078071 1078072 [...] (104435 flits)
Measured flits: 1070203 1070204 1070205 1070206 1070207 1078068 1078069 1078070 1078071 1078072 [...] (23340 flits)
Class 0:
Remaining flits: 1105343 1184989 1184990 1184991 1184992 1184993 1186434 1186435 1186436 1186437 [...] (103762 flits)
Measured flits: 1105343 1186434 1186435 1186436 1186437 1186438 1186439 1186440 1186441 1186442 [...] (16454 flits)
Class 0:
Remaining flits: 1187262 1187263 1187264 1187265 1187266 1187267 1187268 1187269 1187270 1187271 [...] (102932 flits)
Measured flits: 1187262 1187263 1187264 1187265 1187266 1187267 1187268 1187269 1187270 1187271 [...] (13978 flits)
Class 0:
Remaining flits: 1241795 1241796 1241797 1241798 1241799 1241800 1241801 1244502 1244503 1244504 [...] (103022 flits)
Measured flits: 1241795 1241796 1241797 1241798 1241799 1241800 1241801 1244502 1244503 1244504 [...] (12441 flits)
Class 0:
Remaining flits: 1299339 1299340 1299341 1299342 1299343 1299344 1299345 1299346 1299347 1301400 [...] (104423 flits)
Measured flits: 1373202 1373203 1373204 1373205 1373206 1373207 1373208 1373209 1373210 1373211 [...] (10855 flits)
Class 0:
Remaining flits: 1357320 1357321 1357322 1357323 1357324 1357325 1399842 1399843 1399844 1399845 [...] (102894 flits)
Measured flits: 1416508 1416509 1436515 1436516 1436517 1436518 1436519 1436520 1436521 1436522 [...] (9317 flits)
Class 0:
Remaining flits: 1422324 1422325 1422326 1422327 1422328 1422329 1422330 1422331 1422332 1422333 [...] (103390 flits)
Measured flits: 1492308 1492309 1492310 1492311 1492312 1492313 1492314 1492315 1492316 1492317 [...] (7267 flits)
Class 0:
Remaining flits: 1445094 1445095 1445096 1445097 1445098 1445099 1445100 1445101 1445102 1445103 [...] (101387 flits)
Measured flits: 1569276 1569277 1569278 1569279 1569280 1569281 1569282 1569283 1569284 1569285 [...] (5264 flits)
Class 0:
Remaining flits: 1455068 1455069 1455070 1455071 1455072 1455073 1455074 1455075 1455076 1455077 [...] (104054 flits)
Measured flits: 1599371 1652292 1652293 1652294 1652295 1652296 1652297 1652298 1652299 1652300 [...] (2901 flits)
Class 0:
Remaining flits: 1535756 1535757 1535758 1535759 1543230 1543231 1543232 1543233 1543234 1543235 [...] (103922 flits)
Measured flits: 1652292 1652293 1652294 1652295 1652296 1652297 1652298 1652299 1652300 1652301 [...] (1653 flits)
Class 0:
Remaining flits: 1583856 1583857 1583858 1583859 1583860 1583861 1583862 1583863 1583864 1583865 [...] (104475 flits)
Measured flits: 1676322 1676323 1676324 1676325 1676326 1676327 1676328 1676329 1676330 1676331 [...] (1099 flits)
Class 0:
Remaining flits: 1605183 1605184 1605185 1657404 1657405 1657406 1657407 1657408 1657409 1657410 [...] (103072 flits)
Measured flits: 1763388 1763389 1763390 1763391 1763392 1763393 1763394 1763395 1763396 1763397 [...] (833 flits)
Class 0:
Remaining flits: 1675656 1675657 1675658 1675659 1675660 1675661 1675662 1675663 1675664 1675665 [...] (102180 flits)
Measured flits: 1846908 1846909 1846910 1846911 1846912 1846913 1846914 1846915 1846916 1846917 [...] (394 flits)
Class 0:
Remaining flits: 1675656 1675657 1675658 1675659 1675660 1675661 1675662 1675663 1675664 1675665 [...] (101190 flits)
Measured flits: 1846908 1846909 1846910 1846911 1846912 1846913 1846914 1846915 1846916 1846917 [...] (162 flits)
Class 0:
Remaining flits: 1759302 1759303 1759304 1759305 1759306 1759307 1759308 1759309 1759310 1759311 [...] (101963 flits)
Measured flits: 1846908 1846909 1846910 1846911 1846912 1846913 1846914 1846915 1846916 1846917 [...] (54 flits)
Class 0:
Remaining flits: 1767942 1767943 1767944 1767945 1767946 1767947 1767948 1767949 1767950 1767951 [...] (101152 flits)
Measured flits: 1846908 1846909 1846910 1846911 1846912 1846913 1846914 1846915 1846916 1846917 [...] (36 flits)
Class 0:
Remaining flits: 1821096 1821097 1821098 1821099 1821100 1821101 1821102 1821103 1821104 1821105 [...] (103079 flits)
Measured flits: 1846918 1846919 1846920 1846921 1846922 1846923 1846924 1846925 1939500 1939501 [...] (26 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1834794 1834795 1834796 1834797 1834798 1834799 1834800 1834801 1834802 1834803 [...] (54534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1834794 1834795 1834796 1834797 1834798 1834799 1834800 1834801 1834802 1834803 [...] (14200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1834794 1834795 1834796 1834797 1834798 1834799 1834800 1834801 1834802 1834803 [...] (1375 flits)
Measured flits: (0 flits)
Time taken is 45389 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9513.11 (1 samples)
	minimum = 259 (1 samples)
	maximum = 32144 (1 samples)
Network latency average = 2049.66 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7686 (1 samples)
Flit latency average = 1982.29 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11861 (1 samples)
Fragmentation average = 129.219 (1 samples)
	minimum = 0 (1 samples)
	maximum = 806 (1 samples)
Injected packet rate average = 0.0151734 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0205714 (1 samples)
Accepted packet rate average = 0.0149851 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.273208 (1 samples)
	minimum = 0.153571 (1 samples)
	maximum = 0.37 (1 samples)
Accepted flit rate average = 0.269424 (1 samples)
	minimum = 0.213286 (1 samples)
	maximum = 0.342857 (1 samples)
Injected packet size average = 18.0057 (1 samples)
Accepted packet size average = 17.9794 (1 samples)
Hops average = 5.05663 (1 samples)
Total run time 92.8524
