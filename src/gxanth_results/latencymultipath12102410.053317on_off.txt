BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 360.877
	minimum = 23
	maximum = 991
Network latency average = 262.578
	minimum = 23
	maximum = 938
Slowest packet = 46
Flit latency average = 230.171
	minimum = 6
	maximum = 930
Slowest flit = 6255
Fragmentation average = 52.8594
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.010224
	minimum = 0.004 (at node 24)
	maximum = 0.018 (at node 132)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.193661
	minimum = 0.084 (at node 24)
	maximum = 0.332 (at node 102)
Injected packet length average = 17.8285
Accepted packet length average = 18.9419
Total in-flight flits = 65651 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 630.627
	minimum = 23
	maximum = 1932
Network latency average = 491.291
	minimum = 23
	maximum = 1786
Slowest packet = 46
Flit latency average = 457.776
	minimum = 6
	maximum = 1815
Slowest flit = 15813
Fragmentation average = 57.2752
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0315208
	minimum = 0.005 (at node 160)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0108724
	minimum = 0.0055 (at node 116)
	maximum = 0.017 (at node 63)
Injected flit rate average = 0.564422
	minimum = 0.088 (at node 160)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.199893
	minimum = 0.099 (at node 116)
	maximum = 0.314 (at node 63)
Injected packet length average = 17.9063
Accepted packet length average = 18.3854
Total in-flight flits = 141113 (0 measured)
latency change    = 0.427748
throughput change = 0.0311755
Class 0:
Packet latency average = 1467.08
	minimum = 30
	maximum = 2910
Network latency average = 1254.54
	minimum = 24
	maximum = 2591
Slowest packet = 2759
Flit latency average = 1229.77
	minimum = 6
	maximum = 2796
Slowest flit = 18435
Fragmentation average = 64.234
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0324844
	minimum = 0 (at node 33)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0108177
	minimum = 0.002 (at node 108)
	maximum = 0.021 (at node 75)
Injected flit rate average = 0.584688
	minimum = 0 (at node 33)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.194724
	minimum = 0.044 (at node 108)
	maximum = 0.398 (at node 75)
Injected packet length average = 17.999
Accepted packet length average = 18.0005
Total in-flight flits = 215992 (0 measured)
latency change    = 0.570149
throughput change = 0.0265467
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 520.205
	minimum = 28
	maximum = 2291
Network latency average = 105.135
	minimum = 28
	maximum = 975
Slowest packet = 18356
Flit latency average = 1825.43
	minimum = 6
	maximum = 3527
Slowest flit = 44030
Fragmentation average = 13.7637
	minimum = 0
	maximum = 117
Injected packet rate average = 0.0326406
	minimum = 0 (at node 158)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0107396
	minimum = 0.005 (at node 35)
	maximum = 0.022 (at node 142)
Injected flit rate average = 0.587135
	minimum = 0 (at node 158)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.192797
	minimum = 0.075 (at node 35)
	maximum = 0.401 (at node 142)
Injected packet length average = 17.9879
Accepted packet length average = 17.952
Total in-flight flits = 291799 (106501 measured)
latency change    = 1.8202
throughput change = 0.00999541
Class 0:
Packet latency average = 676.048
	minimum = 23
	maximum = 3353
Network latency average = 222.553
	minimum = 23
	maximum = 1878
Slowest packet = 18356
Flit latency average = 2123.65
	minimum = 6
	maximum = 4314
Slowest flit = 59327
Fragmentation average = 16.0803
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0329557
	minimum = 0.0045 (at node 165)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0105573
	minimum = 0.0055 (at node 35)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.593195
	minimum = 0.0835 (at node 165)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.18974
	minimum = 0.0945 (at node 35)
	maximum = 0.3365 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 17.9724
Total in-flight flits = 370940 (214210 measured)
latency change    = 0.230521
throughput change = 0.0161131
Class 0:
Packet latency average = 928.488
	minimum = 23
	maximum = 3738
Network latency average = 473.439
	minimum = 23
	maximum = 2857
Slowest packet = 18356
Flit latency average = 2459.53
	minimum = 6
	maximum = 5349
Slowest flit = 63217
Fragmentation average = 19.8751
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0322465
	minimum = 0.006 (at node 189)
	maximum = 0.0556667 (at node 11)
Accepted packet rate average = 0.0104549
	minimum = 0.006 (at node 189)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.58034
	minimum = 0.108 (at node 189)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.188
	minimum = 0.112 (at node 189)
	maximum = 0.308333 (at node 129)
Injected packet length average = 17.997
Accepted packet length average = 17.9821
Total in-flight flits = 442036 (313125 measured)
latency change    = 0.271882
throughput change = 0.0092531
Class 0:
Packet latency average = 1345.07
	minimum = 23
	maximum = 4555
Network latency average = 879.476
	minimum = 23
	maximum = 3951
Slowest packet = 18356
Flit latency average = 2782.53
	minimum = 6
	maximum = 6064
Slowest flit = 93274
Fragmentation average = 27.2127
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0319167
	minimum = 0.0105 (at node 189)
	maximum = 0.05575 (at node 18)
Accepted packet rate average = 0.0103164
	minimum = 0.0065 (at node 132)
	maximum = 0.01675 (at node 129)
Injected flit rate average = 0.574654
	minimum = 0.189 (at node 189)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.185552
	minimum = 0.116 (at node 132)
	maximum = 0.301 (at node 129)
Injected packet length average = 18.0048
Accepted packet length average = 17.9861
Total in-flight flits = 514740 (410528 measured)
latency change    = 0.309711
throughput change = 0.0131926
Class 0:
Packet latency average = 1829.41
	minimum = 23
	maximum = 5758
Network latency average = 1344.72
	minimum = 23
	maximum = 4977
Slowest packet = 18356
Flit latency average = 3109.55
	minimum = 6
	maximum = 7150
Slowest flit = 80981
Fragmentation average = 32.8512
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0313729
	minimum = 0.013 (at node 122)
	maximum = 0.0556 (at node 11)
Accepted packet rate average = 0.0102406
	minimum = 0.0068 (at node 162)
	maximum = 0.0152 (at node 129)
Injected flit rate average = 0.564777
	minimum = 0.2358 (at node 122)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.184259
	minimum = 0.1272 (at node 132)
	maximum = 0.273 (at node 129)
Injected packet length average = 18.0021
Accepted packet length average = 17.993
Total in-flight flits = 581227 (499644 measured)
latency change    = 0.264751
throughput change = 0.0070157
Class 0:
Packet latency average = 2446.87
	minimum = 23
	maximum = 7274
Network latency average = 1953.95
	minimum = 23
	maximum = 5962
Slowest packet = 18356
Flit latency average = 3456.06
	minimum = 6
	maximum = 7923
Slowest flit = 109757
Fragmentation average = 39.0288
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0302526
	minimum = 0.014 (at node 12)
	maximum = 0.0498333 (at node 35)
Accepted packet rate average = 0.0101832
	minimum = 0.007 (at node 190)
	maximum = 0.0138333 (at node 129)
Injected flit rate average = 0.544689
	minimum = 0.252667 (at node 32)
	maximum = 0.896 (at node 35)
Accepted flit rate average= 0.183413
	minimum = 0.129 (at node 190)
	maximum = 0.252667 (at node 129)
Injected packet length average = 18.0047
Accepted packet length average = 18.0114
Total in-flight flits = 632036 (570433 measured)
latency change    = 0.252347
throughput change = 0.00461352
Class 0:
Packet latency average = 3065.39
	minimum = 23
	maximum = 8311
Network latency average = 2545.33
	minimum = 23
	maximum = 6871
Slowest packet = 18356
Flit latency average = 3778.75
	minimum = 6
	maximum = 8788
Slowest flit = 121445
Fragmentation average = 43.7494
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0296228
	minimum = 0.014 (at node 32)
	maximum = 0.0468571 (at node 114)
Accepted packet rate average = 0.0101384
	minimum = 0.00771429 (at node 52)
	maximum = 0.0135714 (at node 129)
Injected flit rate average = 0.533205
	minimum = 0.250857 (at node 32)
	maximum = 0.843429 (at node 114)
Accepted flit rate average= 0.182426
	minimum = 0.139571 (at node 132)
	maximum = 0.242714 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 17.9935
Total in-flight flits = 687446 (642209 measured)
latency change    = 0.201776
throughput change = 0.00541371
Draining all recorded packets ...
Class 0:
Remaining flits: 126288 126289 126290 126291 126292 126293 126294 126295 126296 126297 [...] (735044 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (668210 flits)
Class 0:
Remaining flits: 143126 143127 143128 143129 143130 143131 143132 143133 143134 143135 [...] (776815 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (669738 flits)
Class 0:
Remaining flits: 151315 151316 151317 151318 151319 151320 151321 151322 151323 151324 [...] (814232 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (666395 flits)
Class 0:
Remaining flits: 184734 184735 184736 184737 184738 184739 184740 184741 184742 184743 [...] (848464 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (659758 flits)
Class 0:
Remaining flits: 184734 184735 184736 184737 184738 184739 184740 184741 184742 184743 [...] (877756 flits)
Measured flits: 330174 330175 330176 330177 330178 330179 330180 330181 330182 330183 [...] (650341 flits)
Class 0:
Remaining flits: 187272 187273 187274 187275 187276 187277 187278 187279 187280 187281 [...] (902801 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (636897 flits)
Class 0:
Remaining flits: 207016 207017 213264 213265 213266 213267 213268 213269 213270 213271 [...] (924153 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (620695 flits)
Class 0:
Remaining flits: 217530 217531 217532 217533 217534 217535 217536 217537 217538 217539 [...] (942604 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (601285 flits)
Class 0:
Remaining flits: 217530 217531 217532 217533 217534 217535 217536 217537 217538 217539 [...] (955972 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (580801 flits)
Class 0:
Remaining flits: 264420 264421 264422 264423 264424 264425 264426 264427 264428 264429 [...] (967407 flits)
Measured flits: 332154 332155 332156 332157 332158 332159 332160 332161 332162 332163 [...] (558741 flits)
Class 0:
Remaining flits: 264420 264421 264422 264423 264424 264425 264426 264427 264428 264429 [...] (973743 flits)
Measured flits: 333000 333001 333002 333003 333004 333005 333006 333007 333008 333009 [...] (535048 flits)
Class 0:
Remaining flits: 300456 300457 300458 300459 300460 300461 300462 300463 300464 300465 [...] (976498 flits)
Measured flits: 333000 333001 333002 333003 333004 333005 333006 333007 333008 333009 [...] (509112 flits)
Class 0:
Remaining flits: 300456 300457 300458 300459 300460 300461 300462 300463 300464 300465 [...] (977544 flits)
Measured flits: 333000 333001 333002 333003 333004 333005 333006 333007 333008 333009 [...] (483232 flits)
Class 0:
Remaining flits: 322963 322964 322965 322966 322967 322968 322969 322970 322971 322972 [...] (976927 flits)
Measured flits: 345132 345133 345134 345135 345136 345137 345138 345139 345140 345141 [...] (457786 flits)
Class 0:
Remaining flits: 345142 345143 345144 345145 345146 345147 345148 345149 353186 353187 [...] (971307 flits)
Measured flits: 345142 345143 345144 345145 345146 345147 345148 345149 353186 353187 [...] (431696 flits)
Class 0:
Remaining flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (971058 flits)
Measured flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (406002 flits)
Class 0:
Remaining flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (970703 flits)
Measured flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (380536 flits)
Class 0:
Remaining flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (965351 flits)
Measured flits: 367848 367849 367850 367851 367852 367853 367854 367855 367856 367857 [...] (355797 flits)
Class 0:
Remaining flits: 390240 390241 390242 390243 390244 390245 390246 390247 390248 390249 [...] (962886 flits)
Measured flits: 390240 390241 390242 390243 390244 390245 390246 390247 390248 390249 [...] (331376 flits)
Class 0:
Remaining flits: 422406 422407 422408 422409 422410 422411 422412 422413 422414 422415 [...] (959788 flits)
Measured flits: 422406 422407 422408 422409 422410 422411 422412 422413 422414 422415 [...] (307641 flits)
Class 0:
Remaining flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (957091 flits)
Measured flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (285059 flits)
Class 0:
Remaining flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (950682 flits)
Measured flits: 442602 442603 442604 442605 442606 442607 442608 442609 442610 442611 [...] (262373 flits)
Class 0:
Remaining flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (946885 flits)
Measured flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (240031 flits)
Class 0:
Remaining flits: 527040 527041 527042 527043 527044 527045 527046 527047 527048 527049 [...] (943794 flits)
Measured flits: 527040 527041 527042 527043 527044 527045 527046 527047 527048 527049 [...] (221194 flits)
Class 0:
Remaining flits: 527040 527041 527042 527043 527044 527045 527046 527047 527048 527049 [...] (941107 flits)
Measured flits: 527040 527041 527042 527043 527044 527045 527046 527047 527048 527049 [...] (202118 flits)
Class 0:
Remaining flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (938010 flits)
Measured flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (184300 flits)
Class 0:
Remaining flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (934662 flits)
Measured flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (166606 flits)
Class 0:
Remaining flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (933795 flits)
Measured flits: 555660 555661 555662 555663 555664 555665 555666 555667 555668 555669 [...] (150550 flits)
Class 0:
Remaining flits: 584568 584569 584570 584571 584572 584573 584574 584575 584576 584577 [...] (930494 flits)
Measured flits: 584568 584569 584570 584571 584572 584573 584574 584575 584576 584577 [...] (135144 flits)
Class 0:
Remaining flits: 584568 584569 584570 584571 584572 584573 584574 584575 584576 584577 [...] (930613 flits)
Measured flits: 584568 584569 584570 584571 584572 584573 584574 584575 584576 584577 [...] (120663 flits)
Class 0:
Remaining flits: 644095 644096 644097 644098 644099 644100 644101 644102 644103 644104 [...] (928580 flits)
Measured flits: 644095 644096 644097 644098 644099 644100 644101 644102 644103 644104 [...] (107634 flits)
Class 0:
Remaining flits: 669150 669151 669152 669153 669154 669155 669156 669157 669158 669159 [...] (924500 flits)
Measured flits: 669150 669151 669152 669153 669154 669155 669156 669157 669158 669159 [...] (94976 flits)
Class 0:
Remaining flits: 707372 707373 707374 707375 707376 707377 707378 707379 707380 707381 [...] (922258 flits)
Measured flits: 707372 707373 707374 707375 707376 707377 707378 707379 707380 707381 [...] (84358 flits)
Class 0:
Remaining flits: 724878 724879 724880 724881 724882 724883 724884 724885 724886 724887 [...] (920633 flits)
Measured flits: 724878 724879 724880 724881 724882 724883 724884 724885 724886 724887 [...] (74653 flits)
Class 0:
Remaining flits: 746064 746065 746066 746067 746068 746069 746070 746071 746072 746073 [...] (922984 flits)
Measured flits: 746064 746065 746066 746067 746068 746069 746070 746071 746072 746073 [...] (64374 flits)
Class 0:
Remaining flits: 746064 746065 746066 746067 746068 746069 746070 746071 746072 746073 [...] (921756 flits)
Measured flits: 746064 746065 746066 746067 746068 746069 746070 746071 746072 746073 [...] (55814 flits)
Class 0:
Remaining flits: 751428 751429 751430 751431 751432 751433 751434 751435 751436 751437 [...] (918538 flits)
Measured flits: 751428 751429 751430 751431 751432 751433 751434 751435 751436 751437 [...] (48805 flits)
Class 0:
Remaining flits: 751442 751443 751444 751445 857160 857161 857162 857163 857164 857165 [...] (917509 flits)
Measured flits: 751442 751443 751444 751445 857160 857161 857162 857163 857164 857165 [...] (41818 flits)
Class 0:
Remaining flits: 857160 857161 857162 857163 857164 857165 857166 857167 857168 857169 [...] (919764 flits)
Measured flits: 857160 857161 857162 857163 857164 857165 857166 857167 857168 857169 [...] (35240 flits)
Class 0:
Remaining flits: 881424 881425 881426 881427 881428 881429 881430 881431 881432 881433 [...] (920824 flits)
Measured flits: 881424 881425 881426 881427 881428 881429 881430 881431 881432 881433 [...] (29933 flits)
Class 0:
Remaining flits: 881424 881425 881426 881427 881428 881429 881430 881431 881432 881433 [...] (918725 flits)
Measured flits: 881424 881425 881426 881427 881428 881429 881430 881431 881432 881433 [...] (24949 flits)
Class 0:
Remaining flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (916126 flits)
Measured flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (20503 flits)
Class 0:
Remaining flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (914342 flits)
Measured flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (16818 flits)
Class 0:
Remaining flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (912719 flits)
Measured flits: 906300 906301 906302 906303 906304 906305 906306 906307 906308 906309 [...] (14101 flits)
Class 0:
Remaining flits: 925056 925057 925058 925059 925060 925061 925062 925063 925064 925065 [...] (911197 flits)
Measured flits: 925056 925057 925058 925059 925060 925061 925062 925063 925064 925065 [...] (11749 flits)
Class 0:
Remaining flits: 1004204 1004205 1004206 1004207 1004208 1004209 1004210 1004211 1004212 1004213 [...] (907267 flits)
Measured flits: 1004204 1004205 1004206 1004207 1004208 1004209 1004210 1004211 1004212 1004213 [...] (9952 flits)
Class 0:
Remaining flits: 1011834 1011835 1011836 1011837 1011838 1011839 1011840 1011841 1011842 1011843 [...] (905136 flits)
Measured flits: 1011834 1011835 1011836 1011837 1011838 1011839 1011840 1011841 1011842 1011843 [...] (8011 flits)
Class 0:
Remaining flits: 1067195 1067196 1067197 1067198 1067199 1067200 1067201 1072656 1072657 1072658 [...] (901655 flits)
Measured flits: 1067195 1067196 1067197 1067198 1067199 1067200 1067201 1072656 1072657 1072658 [...] (6446 flits)
Class 0:
Remaining flits: 1115334 1115335 1115336 1115337 1115338 1115339 1115340 1115341 1115342 1115343 [...] (901531 flits)
Measured flits: 1115334 1115335 1115336 1115337 1115338 1115339 1115340 1115341 1115342 1115343 [...] (5066 flits)
Class 0:
Remaining flits: 1115334 1115335 1115336 1115337 1115338 1115339 1115340 1115341 1115342 1115343 [...] (901876 flits)
Measured flits: 1115334 1115335 1115336 1115337 1115338 1115339 1115340 1115341 1115342 1115343 [...] (3882 flits)
Class 0:
Remaining flits: 1203714 1203715 1203716 1203717 1203718 1203719 1203720 1203721 1203722 1203723 [...] (901115 flits)
Measured flits: 1203714 1203715 1203716 1203717 1203718 1203719 1203720 1203721 1203722 1203723 [...] (2976 flits)
Class 0:
Remaining flits: 1258593 1258594 1258595 1273122 1273123 1273124 1273125 1273126 1273127 1273128 [...] (900441 flits)
Measured flits: 1432512 1432513 1432514 1432515 1432516 1432517 1432518 1432519 1432520 1432521 [...] (2302 flits)
Class 0:
Remaining flits: 1273122 1273123 1273124 1273125 1273126 1273127 1273128 1273129 1273130 1273131 [...] (895901 flits)
Measured flits: 1466766 1466767 1466768 1466769 1466770 1466771 1466772 1466773 1466774 1466775 [...] (1701 flits)
Class 0:
Remaining flits: 1273122 1273123 1273124 1273125 1273126 1273127 1273128 1273129 1273130 1273131 [...] (890563 flits)
Measured flits: 1471896 1471897 1471898 1471899 1471900 1471901 1471902 1471903 1471904 1471905 [...] (1236 flits)
Class 0:
Remaining flits: 1273122 1273123 1273124 1273125 1273126 1273127 1273128 1273129 1273130 1273131 [...] (889551 flits)
Measured flits: 1490094 1490095 1490096 1490097 1490098 1490099 1490100 1490101 1490102 1490103 [...] (802 flits)
Class 0:
Remaining flits: 1273122 1273123 1273124 1273125 1273126 1273127 1273128 1273129 1273130 1273131 [...] (888376 flits)
Measured flits: 1490094 1490095 1490096 1490097 1490098 1490099 1490100 1490101 1490102 1490103 [...] (604 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (885902 flits)
Measured flits: 1537812 1537813 1537814 1537815 1537816 1537817 1537818 1537819 1537820 1537821 [...] (406 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (883337 flits)
Measured flits: 1537812 1537813 1537814 1537815 1537816 1537817 1537818 1537819 1537820 1537821 [...] (306 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (881558 flits)
Measured flits: 1581408 1581409 1581410 1581411 1581412 1581413 1581414 1581415 1581416 1581417 [...] (180 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (880323 flits)
Measured flits: 1581422 1581423 1581424 1581425 1675422 1675423 1675424 1675425 1675426 1675427 [...] (94 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (879651 flits)
Measured flits: 1988658 1988659 1988660 1988661 1988662 1988663 1988664 1988665 1988666 1988667 [...] (18 flits)
Class 0:
Remaining flits: 1389222 1389223 1389224 1389225 1389226 1389227 1389228 1389229 1389230 1389231 [...] (877557 flits)
Measured flits: 1988658 1988659 1988660 1988661 1988662 1988663 1988664 1988665 1988666 1988667 [...] (18 flits)
Class 0:
Remaining flits: 1439244 1439245 1439246 1439247 1439248 1439249 1439250 1439251 1439252 1439253 [...] (875927 flits)
Measured flits: 1988658 1988659 1988660 1988661 1988662 1988663 1988664 1988665 1988666 1988667 [...] (18 flits)
Class 0:
Remaining flits: 1536066 1536067 1536068 1536069 1536070 1536071 1536072 1536073 1536074 1536075 [...] (873131 flits)
Measured flits: 1988658 1988659 1988660 1988661 1988662 1988663 1988664 1988665 1988666 1988667 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1569467 1569468 1569469 1569470 1569471 1569472 1569473 1641258 1641259 1641260 [...] (843702 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1641258 1641259 1641260 1641261 1641262 1641263 1641264 1641265 1641266 1641267 [...] (812162 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1728648 1728649 1728650 1728651 1728652 1728653 1728654 1728655 1728656 1728657 [...] (781554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1755702 1755703 1755704 1755705 1755706 1755707 1755708 1755709 1755710 1755711 [...] (749884 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1757826 1757827 1757828 1757829 1757830 1757831 1757832 1757833 1757834 1757835 [...] (718623 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1812348 1812349 1812350 1812351 1812352 1812353 1812354 1812355 1812356 1812357 [...] (687611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1832058 1832059 1832060 1832061 1832062 1832063 1832064 1832065 1832066 1832067 [...] (656604 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1832058 1832059 1832060 1832061 1832062 1832063 1832064 1832065 1832066 1832067 [...] (625269 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1865196 1865197 1865198 1865199 1865200 1865201 1865202 1865203 1865204 1865205 [...] (593833 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1933272 1933273 1933274 1933275 1933276 1933277 1933278 1933279 1933280 1933281 [...] (563160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1933289 1934658 1934659 1934660 1934661 1934662 1934663 1934664 1934665 1934666 [...] (532437 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1934659 1934660 1934661 1934662 1934663 1934664 1934665 1934666 1934667 1934668 [...] (501012 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1969038 1969039 1969040 1969041 1969042 1969043 1969044 1969045 1969046 1969047 [...] (469897 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1992042 1992043 1992044 1992045 1992046 1992047 1992048 1992049 1992050 1992051 [...] (438859 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1992042 1992043 1992044 1992045 1992046 1992047 1992048 1992049 1992050 1992051 [...] (408347 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1992042 1992043 1992044 1992045 1992046 1992047 1992048 1992049 1992050 1992051 [...] (377279 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1992042 1992043 1992044 1992045 1992046 1992047 1992048 1992049 1992050 1992051 [...] (346878 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2047446 2047447 2047448 2047449 2047450 2047451 2047452 2047453 2047454 2047455 [...] (317337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2130948 2130949 2130950 2130951 2130952 2130953 2130954 2130955 2130956 2130957 [...] (287476 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2130948 2130949 2130950 2130951 2130952 2130953 2130954 2130955 2130956 2130957 [...] (257611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230596 2230597 2230598 2230599 2230600 2230601 2230602 2230603 2230604 2230605 [...] (228510 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230596 2230597 2230598 2230599 2230600 2230601 2230602 2230603 2230604 2230605 [...] (200348 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230598 2230599 2230600 2230601 2230602 2230603 2230604 2230605 2230606 2230607 [...] (172055 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2355080 2355081 2355082 2355083 2367216 2367217 2367218 2367219 2367220 2367221 [...] (144252 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2367216 2367217 2367218 2367219 2367220 2367221 2367222 2367223 2367224 2367225 [...] (117243 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2416392 2416393 2416394 2416395 2416396 2416397 2416398 2416399 2416400 2416401 [...] (90678 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2416392 2416393 2416394 2416395 2416396 2416397 2416398 2416399 2416400 2416401 [...] (64373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2481408 2481409 2481410 2481411 2481412 2481413 2481414 2481415 2481416 2481417 [...] (39163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2533392 2533393 2533394 2533395 2533396 2533397 2533398 2533399 2533400 2533401 [...] (20806 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2714282 2714283 2714284 2714285 2714286 2714287 2714288 2714289 2714290 2714291 [...] (7867 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2884770 2884771 2884772 2884773 2884774 2884775 2884776 2884777 2884778 2884779 [...] (1472 flits)
Measured flits: (0 flits)
Time taken is 106472 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 19290.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 64797 (1 samples)
Network latency average = 16723.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 51901 (1 samples)
Flit latency average = 23354 (1 samples)
	minimum = 6 (1 samples)
	maximum = 61784 (1 samples)
Fragmentation average = 72.037 (1 samples)
	minimum = 0 (1 samples)
	maximum = 180 (1 samples)
Injected packet rate average = 0.0296228 (1 samples)
	minimum = 0.014 (1 samples)
	maximum = 0.0468571 (1 samples)
Accepted packet rate average = 0.0101384 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0135714 (1 samples)
Injected flit rate average = 0.533205 (1 samples)
	minimum = 0.250857 (1 samples)
	maximum = 0.843429 (1 samples)
Accepted flit rate average = 0.182426 (1 samples)
	minimum = 0.139571 (1 samples)
	maximum = 0.242714 (1 samples)
Injected packet size average = 17.9998 (1 samples)
Accepted packet size average = 17.9935 (1 samples)
Hops average = 5.04984 (1 samples)
Total run time 86.3252
