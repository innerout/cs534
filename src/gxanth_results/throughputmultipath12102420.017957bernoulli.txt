BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 169.43
	minimum = 22
	maximum = 505
Network latency average = 165.675
	minimum = 22
	maximum = 502
Slowest packet = 1296
Flit latency average = 141.64
	minimum = 5
	maximum = 543
Slowest flit = 27617
Fragmentation average = 17.1659
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.0130312
	minimum = 0.004 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.239318
	minimum = 0.072 (at node 41)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 18.3649
Total in-flight flits = 16115 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 269.249
	minimum = 22
	maximum = 928
Network latency average = 265.37
	minimum = 22
	maximum = 928
Slowest packet = 3647
Flit latency average = 240.94
	minimum = 5
	maximum = 914
Slowest flit = 67198
Fragmentation average = 17.8418
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.013776
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.250633
	minimum = 0.135 (at node 153)
	maximum = 0.36 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 18.1934
Total in-flight flits = 26463 (0 measured)
latency change    = 0.37073
throughput change = 0.0451461
Class 0:
Packet latency average = 524.212
	minimum = 25
	maximum = 1173
Network latency average = 520.152
	minimum = 22
	maximum = 1173
Slowest packet = 5933
Flit latency average = 496.554
	minimum = 5
	maximum = 1159
Slowest flit = 112694
Fragmentation average = 18.0618
	minimum = 0
	maximum = 86
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0147604
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 159)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.265844
	minimum = 0.108 (at node 163)
	maximum = 0.48 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 18.0106
Total in-flight flits = 37215 (0 measured)
latency change    = 0.486373
throughput change = 0.0572176
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 458.325
	minimum = 22
	maximum = 948
Network latency average = 454.432
	minimum = 22
	maximum = 948
Slowest packet = 10298
Flit latency average = 653.05
	minimum = 5
	maximum = 1376
Slowest flit = 152081
Fragmentation average = 14.3608
	minimum = 0
	maximum = 72
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.015125
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.272016
	minimum = 0.139 (at node 86)
	maximum = 0.495 (at node 90)
Injected packet length average = 17.9626
Accepted packet length average = 17.9845
Total in-flight flits = 48978 (46861 measured)
latency change    = 0.143757
throughput change = 0.0226894
Class 0:
Packet latency average = 736.949
	minimum = 22
	maximum = 1922
Network latency average = 732.737
	minimum = 22
	maximum = 1905
Slowest packet = 10539
Flit latency average = 736.404
	minimum = 5
	maximum = 1888
Slowest flit = 189719
Fragmentation average = 17.0613
	minimum = 0
	maximum = 89
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.015099
	minimum = 0.0105 (at node 2)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.271443
	minimum = 0.177 (at node 2)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 17.9776
Total in-flight flits = 58369 (58369 measured)
latency change    = 0.378078
throughput change = 0.00211064
Class 0:
Packet latency average = 864.57
	minimum = 22
	maximum = 2092
Network latency average = 860.323
	minimum = 22
	maximum = 2089
Slowest packet = 13109
Flit latency average = 824.71
	minimum = 5
	maximum = 2075
Slowest flit = 242860
Fragmentation average = 17.7475
	minimum = 0
	maximum = 90
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0150764
	minimum = 0.0106667 (at node 2)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.271566
	minimum = 0.187333 (at node 2)
	maximum = 0.378 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.0127
Total in-flight flits = 68857 (68857 measured)
latency change    = 0.147612
throughput change = 0.0004539
Draining remaining packets ...
Class 0:
Remaining flits: 301646 301647 301648 301649 301650 301651 301652 301653 301654 301655 [...] (22351 flits)
Measured flits: 301646 301647 301648 301649 301650 301651 301652 301653 301654 301655 [...] (22351 flits)
Class 0:
Remaining flits: 366492 366493 366494 366495 366496 366497 369972 369973 369974 369975 [...] (42 flits)
Measured flits: 366492 366493 366494 366495 366496 366497 369972 369973 369974 369975 [...] (42 flits)
Time taken is 8053 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1066.58 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2513 (1 samples)
Network latency average = 1062.28 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2513 (1 samples)
Flit latency average = 994.196 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2496 (1 samples)
Fragmentation average = 18.0317 (1 samples)
	minimum = 0 (1 samples)
	maximum = 110 (1 samples)
Injected packet rate average = 0.0181389 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0150764 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.326543 (1 samples)
	minimum = 0.186 (1 samples)
	maximum = 0.463 (1 samples)
Accepted flit rate average = 0.271566 (1 samples)
	minimum = 0.187333 (1 samples)
	maximum = 0.378 (1 samples)
Injected packet size average = 18.0024 (1 samples)
Accepted packet size average = 18.0127 (1 samples)
Hops average = 5.07111 (1 samples)
Total run time 4.84888
