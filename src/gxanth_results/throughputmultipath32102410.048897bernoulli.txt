BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 330.764
	minimum = 27
	maximum = 929
Network latency average = 308.611
	minimum = 23
	maximum = 925
Slowest packet = 191
Flit latency average = 255.042
	minimum = 6
	maximum = 963
Slowest flit = 2535
Fragmentation average = 149.581
	minimum = 0
	maximum = 880
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0118333
	minimum = 0.002 (at node 174)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.24212
	minimum = 0.066 (at node 174)
	maximum = 0.406 (at node 44)
Injected packet length average = 17.8227
Accepted packet length average = 20.4608
Total in-flight flits = 114037 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 620.299
	minimum = 27
	maximum = 1918
Network latency average = 589.739
	minimum = 23
	maximum = 1894
Slowest packet = 803
Flit latency average = 527.58
	minimum = 6
	maximum = 1904
Slowest flit = 9820
Fragmentation average = 177.03
	minimum = 0
	maximum = 1729
Injected packet rate average = 0.0475937
	minimum = 0.0365 (at node 72)
	maximum = 0.0555 (at node 32)
Accepted packet rate average = 0.0131589
	minimum = 0.008 (at node 174)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.852776
	minimum = 0.6485 (at node 72)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.250937
	minimum = 0.1525 (at node 174)
	maximum = 0.362 (at node 140)
Injected packet length average = 17.9178
Accepted packet length average = 19.0699
Total in-flight flits = 232608 (0 measured)
latency change    = 0.466767
throughput change = 0.0351391
Class 0:
Packet latency average = 1404.46
	minimum = 25
	maximum = 2783
Network latency average = 1360.73
	minimum = 23
	maximum = 2770
Slowest packet = 244
Flit latency average = 1321.35
	minimum = 6
	maximum = 2883
Slowest flit = 13328
Fragmentation average = 195.922
	minimum = 0
	maximum = 1893
Injected packet rate average = 0.0485
	minimum = 0.034 (at node 2)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0145
	minimum = 0.006 (at node 17)
	maximum = 0.023 (at node 136)
Injected flit rate average = 0.872458
	minimum = 0.608 (at node 2)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.261063
	minimum = 0.097 (at node 17)
	maximum = 0.428 (at node 159)
Injected packet length average = 17.9888
Accepted packet length average = 18.0043
Total in-flight flits = 350100 (0 measured)
latency change    = 0.558336
throughput change = 0.0387838
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 111.43
	minimum = 25
	maximum = 529
Network latency average = 55.0658
	minimum = 25
	maximum = 480
Slowest packet = 28196
Flit latency average = 1855.05
	minimum = 6
	maximum = 3788
Slowest flit = 15583
Fragmentation average = 15.4863
	minimum = 0
	maximum = 70
Injected packet rate average = 0.0492031
	minimum = 0.031 (at node 153)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0144896
	minimum = 0.006 (at node 153)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.886292
	minimum = 0.541 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.260073
	minimum = 0.115 (at node 64)
	maximum = 0.464 (at node 78)
Injected packet length average = 18.0129
Accepted packet length average = 17.949
Total in-flight flits = 470212 (156667 measured)
latency change    = 11.6039
throughput change = 0.00380502
Class 0:
Packet latency average = 114.567
	minimum = 25
	maximum = 549
Network latency average = 55.0975
	minimum = 23
	maximum = 480
Slowest packet = 28196
Flit latency average = 2140.19
	minimum = 6
	maximum = 4678
Slowest flit = 45328
Fragmentation average = 15.7672
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0489766
	minimum = 0.0365 (at node 153)
	maximum = 0.056 (at node 164)
Accepted packet rate average = 0.0143958
	minimum = 0.008 (at node 35)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.882107
	minimum = 0.657 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.257372
	minimum = 0.159 (at node 35)
	maximum = 0.38 (at node 168)
Injected packet length average = 18.0108
Accepted packet length average = 17.8783
Total in-flight flits = 589795 (312121 measured)
latency change    = 0.0273828
throughput change = 0.0104927
Class 0:
Packet latency average = 113.795
	minimum = 23
	maximum = 549
Network latency average = 54.2942
	minimum = 23
	maximum = 480
Slowest packet = 28196
Flit latency average = 2401.08
	minimum = 6
	maximum = 5599
Slowest flit = 58804
Fragmentation average = 15.9682
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0487847
	minimum = 0.038 (at node 153)
	maximum = 0.0556667 (at node 44)
Accepted packet rate average = 0.0143281
	minimum = 0.00966667 (at node 23)
	maximum = 0.0196667 (at node 19)
Injected flit rate average = 0.878443
	minimum = 0.682333 (at node 153)
	maximum = 1 (at node 26)
Accepted flit rate average= 0.257196
	minimum = 0.17 (at node 23)
	maximum = 0.355333 (at node 19)
Injected packet length average = 18.0065
Accepted packet length average = 17.9504
Total in-flight flits = 707755 (465390 measured)
latency change    = 0.00679021
throughput change = 0.00068514
Draining remaining packets ...
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (671593 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (464799 flits)
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (636053 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (464257 flits)
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (601197 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (462357 flits)
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (566941 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (456900 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (532383 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (447204 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (498446 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (432689 flits)
Class 0:
Remaining flits: 110934 110935 110936 110937 110938 110939 110940 110941 110942 110943 [...] (465039 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (413626 flits)
Class 0:
Remaining flits: 127764 127765 127766 127767 127768 127769 127770 127771 127772 127773 [...] (431406 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (391832 flits)
Class 0:
Remaining flits: 127764 127765 127766 127767 127768 127769 127770 127771 127772 127773 [...] (398445 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (367768 flits)
Class 0:
Remaining flits: 174654 174655 174656 174657 174658 174659 174660 174661 174662 174663 [...] (364381 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (341227 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (330256 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (312394 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (296057 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (282137 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (262109 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (251186 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (228707 flits)
Measured flits: 496601 496854 496855 496856 496857 496858 496859 496860 496861 496862 [...] (220637 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (195710 flits)
Measured flits: 496854 496855 496856 496857 496858 496859 496860 496861 496862 496863 [...] (189670 flits)
Class 0:
Remaining flits: 180018 180019 180020 180021 180022 180023 180024 180025 180026 180027 [...] (162433 flits)
Measured flits: 496854 496855 496856 496857 496858 496859 496860 496861 496862 496863 [...] (158272 flits)
Class 0:
Remaining flits: 229410 229411 229412 229413 229414 229415 229416 229417 229418 229419 [...] (128858 flits)
Measured flits: 496854 496855 496856 496857 496858 496859 496860 496861 496862 496863 [...] (126076 flits)
Class 0:
Remaining flits: 229410 229411 229412 229413 229414 229415 229416 229417 229418 229419 [...] (95905 flits)
Measured flits: 498298 498299 498300 498301 498302 498303 498304 498305 498306 498307 [...] (94266 flits)
Class 0:
Remaining flits: 229410 229411 229412 229413 229414 229415 229416 229417 229418 229419 [...] (64679 flits)
Measured flits: 498600 498601 498602 498603 498604 498605 498606 498607 498608 498609 [...] (63784 flits)
Class 0:
Remaining flits: 318361 318362 318363 318364 318365 346086 346087 346088 346089 346090 [...] (35431 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (35055 flits)
Class 0:
Remaining flits: 346089 346090 346091 346092 346093 346094 346095 346096 346097 346098 [...] (13764 flits)
Measured flits: 513882 513883 513884 513885 513886 513887 513888 513889 513890 513891 [...] (13657 flits)
Class 0:
Remaining flits: 488574 488575 488576 488577 488578 488579 488580 488581 488582 488583 [...] (3508 flits)
Measured flits: 582462 582463 582464 582465 582466 582467 582468 582469 582470 582471 [...] (3490 flits)
Class 0:
Remaining flits: 889625 889626 889627 889628 889629 889630 889631 961865 977004 977005 [...] (26 flits)
Measured flits: 889625 889626 889627 889628 889629 889630 889631 961865 977004 977005 [...] (26 flits)
Time taken is 29029 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13849.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24925 (1 samples)
Network latency average = 13788.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24857 (1 samples)
Flit latency average = 10887.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 25161 (1 samples)
Fragmentation average = 164.021 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4420 (1 samples)
Injected packet rate average = 0.0487847 (1 samples)
	minimum = 0.038 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0143281 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0196667 (1 samples)
Injected flit rate average = 0.878443 (1 samples)
	minimum = 0.682333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.257196 (1 samples)
	minimum = 0.17 (1 samples)
	maximum = 0.355333 (1 samples)
Injected packet size average = 18.0065 (1 samples)
Accepted packet size average = 17.9504 (1 samples)
Hops average = 5.07491 (1 samples)
Total run time 29.7518
