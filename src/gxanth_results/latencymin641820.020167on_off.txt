BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 263.35
	minimum = 22
	maximum = 877
Network latency average = 183.949
	minimum = 22
	maximum = 724
Slowest packet = 72
Flit latency average = 153.829
	minimum = 5
	maximum = 813
Slowest flit = 11968
Fragmentation average = 41.8356
	minimum = 0
	maximum = 480
Injected packet rate average = 0.0198594
	minimum = 0 (at node 10)
	maximum = 0.053 (at node 162)
Accepted packet rate average = 0.0125156
	minimum = 0.004 (at node 135)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.354474
	minimum = 0 (at node 10)
	maximum = 0.937 (at node 162)
Accepted flit rate average= 0.235328
	minimum = 0.085 (at node 135)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.8492
Accepted packet length average = 18.8027
Total in-flight flits = 23487 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 429.262
	minimum = 22
	maximum = 1773
Network latency average = 329.817
	minimum = 22
	maximum = 1580
Slowest packet = 72
Flit latency average = 291.642
	minimum = 5
	maximum = 1563
Slowest flit = 27629
Fragmentation average = 57.5097
	minimum = 0
	maximum = 492
Injected packet rate average = 0.0192839
	minimum = 0.0015 (at node 26)
	maximum = 0.038 (at node 51)
Accepted packet rate average = 0.0133411
	minimum = 0.0075 (at node 116)
	maximum = 0.0195 (at node 166)
Injected flit rate average = 0.34588
	minimum = 0.027 (at node 26)
	maximum = 0.684 (at node 51)
Accepted flit rate average= 0.247573
	minimum = 0.135 (at node 116)
	maximum = 0.351 (at node 166)
Injected packet length average = 17.9363
Accepted packet length average = 18.5571
Total in-flight flits = 38312 (0 measured)
latency change    = 0.386505
throughput change = 0.0494593
Class 0:
Packet latency average = 830.421
	minimum = 25
	maximum = 2506
Network latency average = 688.877
	minimum = 22
	maximum = 1932
Slowest packet = 3167
Flit latency average = 638.453
	minimum = 5
	maximum = 1915
Slowest flit = 68003
Fragmentation average = 88.207
	minimum = 0
	maximum = 541
Injected packet rate average = 0.018125
	minimum = 0 (at node 62)
	maximum = 0.045 (at node 189)
Accepted packet rate average = 0.0151719
	minimum = 0.006 (at node 163)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.325177
	minimum = 0 (at node 62)
	maximum = 0.802 (at node 189)
Accepted flit rate average= 0.272854
	minimum = 0.116 (at node 180)
	maximum = 0.483 (at node 16)
Injected packet length average = 17.9408
Accepted packet length average = 17.9842
Total in-flight flits = 48600 (0 measured)
latency change    = 0.483079
throughput change = 0.0926548
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 511.622
	minimum = 23
	maximum = 2201
Network latency average = 342.809
	minimum = 22
	maximum = 973
Slowest packet = 10929
Flit latency average = 793.349
	minimum = 4
	maximum = 2343
Slowest flit = 109547
Fragmentation average = 42.3827
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0176771
	minimum = 0 (at node 105)
	maximum = 0.049 (at node 26)
Accepted packet rate average = 0.0151198
	minimum = 0.005 (at node 43)
	maximum = 0.025 (at node 78)
Injected flit rate average = 0.31851
	minimum = 0 (at node 105)
	maximum = 0.871 (at node 26)
Accepted flit rate average= 0.271854
	minimum = 0.123 (at node 43)
	maximum = 0.465 (at node 101)
Injected packet length average = 18.0183
Accepted packet length average = 17.98
Total in-flight flits = 57604 (45143 measured)
latency change    = 0.623113
throughput change = 0.00367844
Class 0:
Packet latency average = 894.76
	minimum = 23
	maximum = 3185
Network latency average = 692.937
	minimum = 22
	maximum = 1956
Slowest packet = 10929
Flit latency average = 889.809
	minimum = 4
	maximum = 2871
Slowest flit = 140691
Fragmentation average = 69.5333
	minimum = 0
	maximum = 536
Injected packet rate average = 0.0175156
	minimum = 0.002 (at node 79)
	maximum = 0.036 (at node 26)
Accepted packet rate average = 0.0149635
	minimum = 0.007 (at node 4)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.315164
	minimum = 0.036 (at node 79)
	maximum = 0.6465 (at node 26)
Accepted flit rate average= 0.268943
	minimum = 0.126 (at node 4)
	maximum = 0.403 (at node 129)
Injected packet length average = 17.9933
Accepted packet length average = 17.9732
Total in-flight flits = 66412 (65385 measured)
latency change    = 0.428202
throughput change = 0.0108256
Class 0:
Packet latency average = 1170.12
	minimum = 22
	maximum = 4130
Network latency average = 912.15
	minimum = 22
	maximum = 2917
Slowest packet = 10929
Flit latency average = 966.014
	minimum = 4
	maximum = 2930
Slowest flit = 167705
Fragmentation average = 80.0312
	minimum = 0
	maximum = 676
Injected packet rate average = 0.017224
	minimum = 0.00233333 (at node 79)
	maximum = 0.033 (at node 154)
Accepted packet rate average = 0.0149253
	minimum = 0.00933333 (at node 84)
	maximum = 0.0203333 (at node 129)
Injected flit rate average = 0.309868
	minimum = 0.042 (at node 79)
	maximum = 0.594333 (at node 154)
Accepted flit rate average= 0.269231
	minimum = 0.168 (at node 84)
	maximum = 0.374 (at node 129)
Injected packet length average = 17.9905
Accepted packet length average = 18.0385
Total in-flight flits = 72263 (72245 measured)
latency change    = 0.235326
throughput change = 0.00107044
Class 0:
Packet latency average = 1344.4
	minimum = 22
	maximum = 4790
Network latency average = 1046.22
	minimum = 22
	maximum = 3573
Slowest packet = 10929
Flit latency average = 1036.53
	minimum = 4
	maximum = 3534
Slowest flit = 203235
Fragmentation average = 85.5106
	minimum = 0
	maximum = 676
Injected packet rate average = 0.016931
	minimum = 0.0055 (at node 79)
	maximum = 0.031 (at node 154)
Accepted packet rate average = 0.0149063
	minimum = 0.0105 (at node 89)
	maximum = 0.02025 (at node 95)
Injected flit rate average = 0.304668
	minimum = 0.099 (at node 79)
	maximum = 0.5565 (at node 154)
Accepted flit rate average= 0.268818
	minimum = 0.19475 (at node 89)
	maximum = 0.3655 (at node 95)
Injected packet length average = 17.9947
Accepted packet length average = 18.0339
Total in-flight flits = 76616 (76616 measured)
latency change    = 0.129635
throughput change = 0.00153708
Class 0:
Packet latency average = 1473.96
	minimum = 22
	maximum = 5551
Network latency average = 1140.31
	minimum = 22
	maximum = 4016
Slowest packet = 10929
Flit latency average = 1100.06
	minimum = 4
	maximum = 3999
Slowest flit = 235277
Fragmentation average = 87.9691
	minimum = 0
	maximum = 676
Injected packet rate average = 0.0169417
	minimum = 0.0066 (at node 79)
	maximum = 0.0294 (at node 151)
Accepted packet rate average = 0.014951
	minimum = 0.0114 (at node 64)
	maximum = 0.0194 (at node 128)
Injected flit rate average = 0.304815
	minimum = 0.1188 (at node 79)
	maximum = 0.5282 (at node 151)
Accepted flit rate average= 0.269326
	minimum = 0.205 (at node 126)
	maximum = 0.3482 (at node 128)
Injected packet length average = 17.992
Accepted packet length average = 18.0139
Total in-flight flits = 83051 (83051 measured)
latency change    = 0.0879001
throughput change = 0.00188743
Class 0:
Packet latency average = 1602.06
	minimum = 22
	maximum = 5551
Network latency average = 1221.26
	minimum = 22
	maximum = 4507
Slowest packet = 10929
Flit latency average = 1162.56
	minimum = 4
	maximum = 4474
Slowest flit = 255579
Fragmentation average = 91.0052
	minimum = 0
	maximum = 676
Injected packet rate average = 0.0166997
	minimum = 0.007 (at node 79)
	maximum = 0.0303333 (at node 151)
Accepted packet rate average = 0.0149306
	minimum = 0.0118333 (at node 80)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.300488
	minimum = 0.126 (at node 79)
	maximum = 0.545167 (at node 151)
Accepted flit rate average= 0.269098
	minimum = 0.211667 (at node 102)
	maximum = 0.337167 (at node 70)
Injected packet length average = 17.9937
Accepted packet length average = 18.0233
Total in-flight flits = 85297 (85297 measured)
latency change    = 0.079958
throughput change = 0.000847094
Class 0:
Packet latency average = 1728.85
	minimum = 22
	maximum = 6139
Network latency average = 1288.25
	minimum = 22
	maximum = 4507
Slowest packet = 10929
Flit latency average = 1217.29
	minimum = 4
	maximum = 4474
Slowest flit = 255579
Fragmentation average = 93.1895
	minimum = 0
	maximum = 676
Injected packet rate average = 0.0166391
	minimum = 0.00714286 (at node 79)
	maximum = 0.0284286 (at node 151)
Accepted packet rate average = 0.0149531
	minimum = 0.0118571 (at node 80)
	maximum = 0.0191429 (at node 70)
Injected flit rate average = 0.299371
	minimum = 0.128571 (at node 79)
	maximum = 0.510857 (at node 151)
Accepted flit rate average= 0.269464
	minimum = 0.215571 (at node 80)
	maximum = 0.343429 (at node 70)
Injected packet length average = 17.992
Accepted packet length average = 18.0206
Total in-flight flits = 89461 (89461 measured)
latency change    = 0.0733375
throughput change = 0.00135622
Draining all recorded packets ...
Class 0:
Remaining flits: 402102 402103 402104 402105 402106 402107 402108 402109 402110 402111 [...] (93919 flits)
Measured flits: 402102 402103 402104 402105 402106 402107 402108 402109 402110 402111 [...] (73316 flits)
Class 0:
Remaining flits: 402102 402103 402104 402105 402106 402107 402108 402109 402110 402111 [...] (97405 flits)
Measured flits: 402102 402103 402104 402105 402106 402107 402108 402109 402110 402111 [...] (53405 flits)
Class 0:
Remaining flits: 457686 457687 457688 457689 457690 457691 457692 457693 457694 457695 [...] (98391 flits)
Measured flits: 457686 457687 457688 457689 457690 457691 457692 457693 457694 457695 [...] (37900 flits)
Class 0:
Remaining flits: 524214 524215 524216 524217 524218 524219 524220 524221 524222 524223 [...] (98877 flits)
Measured flits: 524214 524215 524216 524217 524218 524219 524220 524221 524222 524223 [...] (25999 flits)
Class 0:
Remaining flits: 572670 572671 572672 572673 572674 572675 572676 572677 572678 572679 [...] (99948 flits)
Measured flits: 572670 572671 572672 572673 572674 572675 572676 572677 572678 572679 [...] (15153 flits)
Class 0:
Remaining flits: 610111 610112 610113 610114 610115 610116 610117 610118 610119 610120 [...] (103208 flits)
Measured flits: 610111 610112 610113 610114 610115 610116 610117 610118 610119 610120 [...] (8368 flits)
Class 0:
Remaining flits: 680598 680599 680600 680601 680602 680603 680604 680605 680606 680607 [...] (103048 flits)
Measured flits: 761097 761098 761099 761100 761101 761102 761103 761104 761105 761106 [...] (5021 flits)
Class 0:
Remaining flits: 680598 680599 680600 680601 680602 680603 680604 680605 680606 680607 [...] (102919 flits)
Measured flits: 793579 793580 793581 793582 793583 829404 829405 829406 829407 829408 [...] (2979 flits)
Class 0:
Remaining flits: 825442 825443 832603 832604 832605 832606 832607 846594 846595 846596 [...] (103711 flits)
Measured flits: 882449 916112 916113 916114 916115 916116 916117 916118 916119 916120 [...] (1610 flits)
Class 0:
Remaining flits: 852300 852301 852302 852303 852304 852305 852306 852307 852308 852309 [...] (102343 flits)
Measured flits: 1002600 1002601 1002602 1002603 1002604 1002605 1002606 1002607 1002608 1002609 [...] (1349 flits)
Class 0:
Remaining flits: 944270 944271 944272 944273 944274 944275 944276 944277 944278 944279 [...] (102336 flits)
Measured flits: 1063329 1063330 1063331 1065294 1065295 1065296 1065297 1065298 1065299 1065300 [...] (766 flits)
Class 0:
Remaining flits: 953838 953839 953840 953841 953842 953843 953844 953845 953846 953847 [...] (103154 flits)
Measured flits: 1088802 1088803 1088804 1088805 1088806 1088807 1088808 1088809 1088810 1088811 [...] (668 flits)
Class 0:
Remaining flits: 991326 991327 991328 991329 991330 991331 1016421 1016422 1016423 1019253 [...] (104004 flits)
Measured flits: 1188274 1188275 1188276 1188277 1188278 1188279 1188280 1188281 1188282 1188283 [...] (601 flits)
Class 0:
Remaining flits: 1078254 1078255 1078256 1078257 1078258 1078259 1078260 1078261 1078262 1078263 [...] (104648 flits)
Measured flits: 1219483 1219484 1219485 1219486 1219487 1219488 1219489 1219490 1219491 1219492 [...] (215 flits)
Class 0:
Remaining flits: 1085994 1085995 1085996 1085997 1085998 1085999 1086000 1086001 1086002 1086003 [...] (103817 flits)
Measured flits: 1314054 1314055 1314056 1314057 1314058 1314059 1314060 1314061 1314062 1314063 [...] (198 flits)
Class 0:
Remaining flits: 1161018 1161019 1161020 1161021 1161022 1161023 1161024 1161025 1161026 1161027 [...] (101510 flits)
Measured flits: 1333354 1333355 1333356 1333357 1333358 1333359 1333360 1333361 1333362 1333363 [...] (126 flits)
Class 0:
Remaining flits: 1161035 1215648 1215649 1215650 1215651 1215652 1215653 1215654 1215655 1215656 [...] (101325 flits)
Measured flits: 1397769 1397770 1397771 (3 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1274256 1274257 1274258 1274259 1274260 1274261 1274262 1274263 1274264 1274265 [...] (53535 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1308467 1308468 1308469 1308470 1308471 1308472 1308473 1315422 1315423 1315424 [...] (11915 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1420222 1420223 1420224 1420225 1420226 1420227 1420228 1420229 1420230 1420231 [...] (231 flits)
Measured flits: (0 flits)
Time taken is 30215 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2720.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17071 (1 samples)
Network latency average = 1551.49 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6281 (1 samples)
Flit latency average = 1732.5 (1 samples)
	minimum = 4 (1 samples)
	maximum = 6851 (1 samples)
Fragmentation average = 103.591 (1 samples)
	minimum = 0 (1 samples)
	maximum = 699 (1 samples)
Injected packet rate average = 0.0166391 (1 samples)
	minimum = 0.00714286 (1 samples)
	maximum = 0.0284286 (1 samples)
Accepted packet rate average = 0.0149531 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.299371 (1 samples)
	minimum = 0.128571 (1 samples)
	maximum = 0.510857 (1 samples)
Accepted flit rate average = 0.269464 (1 samples)
	minimum = 0.215571 (1 samples)
	maximum = 0.343429 (1 samples)
Injected packet size average = 17.992 (1 samples)
Accepted packet size average = 18.0206 (1 samples)
Hops average = 5.06808 (1 samples)
Total run time 51.7685
