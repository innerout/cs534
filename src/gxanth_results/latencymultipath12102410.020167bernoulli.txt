BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 246.763
	minimum = 23
	maximum = 828
Network latency average = 242.767
	minimum = 23
	maximum = 814
Slowest packet = 427
Flit latency average = 207.043
	minimum = 6
	maximum = 881
Slowest flit = 7456
Fragmentation average = 57.0356
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00950521
	minimum = 0.001 (at node 174)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.179031
	minimum = 0.018 (at node 174)
	maximum = 0.306 (at node 70)
Injected packet length average = 17.8234
Accepted packet length average = 18.8351
Total in-flight flits = 35232 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 460.26
	minimum = 23
	maximum = 1752
Network latency average = 455.509
	minimum = 23
	maximum = 1727
Slowest packet = 570
Flit latency average = 415.973
	minimum = 6
	maximum = 1763
Slowest flit = 14937
Fragmentation average = 63.4551
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.00989844
	minimum = 0.0055 (at node 174)
	maximum = 0.0155 (at node 98)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.18201
	minimum = 0.107 (at node 174)
	maximum = 0.2805 (at node 103)
Injected packet length average = 17.9289
Accepted packet length average = 18.3878
Total in-flight flits = 68312 (0 measured)
latency change    = 0.463862
throughput change = 0.0163681
Class 0:
Packet latency average = 1092.63
	minimum = 23
	maximum = 2460
Network latency average = 1087.79
	minimum = 23
	maximum = 2460
Slowest packet = 1616
Flit latency average = 1054.5
	minimum = 6
	maximum = 2443
Slowest flit = 29102
Fragmentation average = 66.9286
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0100677
	minimum = 0.003 (at node 4)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.181115
	minimum = 0.043 (at node 4)
	maximum = 0.358 (at node 152)
Injected packet length average = 17.9811
Accepted packet length average = 17.9897
Total in-flight flits = 103216 (0 measured)
latency change    = 0.578759
throughput change = 0.00494622
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 84.4494
	minimum = 23
	maximum = 924
Network latency average = 79.443
	minimum = 23
	maximum = 924
Slowest packet = 11700
Flit latency average = 1473.6
	minimum = 6
	maximum = 3137
Slowest flit = 56231
Fragmentation average = 11.9715
	minimum = 0
	maximum = 134
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0101823
	minimum = 0.003 (at node 91)
	maximum = 0.018 (at node 47)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.183542
	minimum = 0.042 (at node 180)
	maximum = 0.324 (at node 47)
Injected packet length average = 17.9618
Accepted packet length average = 18.0256
Total in-flight flits = 139598 (65750 measured)
latency change    = 11.9382
throughput change = 0.0132236
Class 0:
Packet latency average = 350.494
	minimum = 23
	maximum = 1947
Network latency average = 345.771
	minimum = 23
	maximum = 1945
Slowest packet = 11655
Flit latency average = 1695.96
	minimum = 6
	maximum = 4020
Slowest flit = 65771
Fragmentation average = 25.8488
	minimum = 0
	maximum = 136
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0103411
	minimum = 0.005 (at node 146)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.186245
	minimum = 0.0825 (at node 146)
	maximum = 0.2825 (at node 81)
Injected packet length average = 18.0096
Accepted packet length average = 18.0101
Total in-flight flits = 171810 (124652 measured)
latency change    = 0.759056
throughput change = 0.0145138
Class 0:
Packet latency average = 790.311
	minimum = 23
	maximum = 2980
Network latency average = 785.741
	minimum = 23
	maximum = 2978
Slowest packet = 11610
Flit latency average = 1916
	minimum = 6
	maximum = 5000
Slowest flit = 68908
Fragmentation average = 37.9908
	minimum = 0
	maximum = 136
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0102743
	minimum = 0.006 (at node 96)
	maximum = 0.015 (at node 29)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.184719
	minimum = 0.110333 (at node 96)
	maximum = 0.271667 (at node 29)
Injected packet length average = 18.0047
Accepted packet length average = 17.9787
Total in-flight flits = 207598 (180858 measured)
latency change    = 0.556511
throughput change = 0.00826143
Class 0:
Packet latency average = 1346.53
	minimum = 23
	maximum = 4000
Network latency average = 1341.66
	minimum = 23
	maximum = 3965
Slowest packet = 11551
Flit latency average = 2122.55
	minimum = 6
	maximum = 5885
Slowest flit = 72827
Fragmentation average = 47.6391
	minimum = 0
	maximum = 145
Injected packet rate average = 0.020332
	minimum = 0.0145 (at node 147)
	maximum = 0.02625 (at node 27)
Accepted packet rate average = 0.0102005
	minimum = 0.007 (at node 163)
	maximum = 0.01425 (at node 6)
Injected flit rate average = 0.366017
	minimum = 0.261 (at node 147)
	maximum = 0.4745 (at node 27)
Accepted flit rate average= 0.183642
	minimum = 0.127 (at node 180)
	maximum = 0.255 (at node 6)
Injected packet length average = 18.002
Accepted packet length average = 18.0032
Total in-flight flits = 243249 (228911 measured)
latency change    = 0.413074
throughput change = 0.00586371
Class 0:
Packet latency average = 1866.96
	minimum = 23
	maximum = 4955
Network latency average = 1862.04
	minimum = 23
	maximum = 4951
Slowest packet = 11614
Flit latency average = 2357.35
	minimum = 6
	maximum = 6460
Slowest flit = 91313
Fragmentation average = 53.1673
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0203271
	minimum = 0.0148 (at node 110)
	maximum = 0.0246 (at node 7)
Accepted packet rate average = 0.0102042
	minimum = 0.0074 (at node 79)
	maximum = 0.013 (at node 6)
Injected flit rate average = 0.365944
	minimum = 0.2692 (at node 110)
	maximum = 0.4438 (at node 7)
Accepted flit rate average= 0.183951
	minimum = 0.1346 (at node 79)
	maximum = 0.236 (at node 129)
Injected packet length average = 18.0028
Accepted packet length average = 18.0271
Total in-flight flits = 277875 (271363 measured)
latency change    = 0.278759
throughput change = 0.00168042
Class 0:
Packet latency average = 2340.84
	minimum = 23
	maximum = 5877
Network latency average = 2335.81
	minimum = 23
	maximum = 5870
Slowest packet = 11704
Flit latency average = 2586.76
	minimum = 6
	maximum = 7108
Slowest flit = 128501
Fragmentation average = 56.4824
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0202691
	minimum = 0.014 (at node 110)
	maximum = 0.025 (at node 7)
Accepted packet rate average = 0.0101988
	minimum = 0.00783333 (at node 17)
	maximum = 0.0128333 (at node 66)
Injected flit rate average = 0.364766
	minimum = 0.254333 (at node 110)
	maximum = 0.451167 (at node 7)
Accepted flit rate average= 0.183463
	minimum = 0.141 (at node 17)
	maximum = 0.233833 (at node 66)
Injected packet length average = 17.9962
Accepted packet length average = 17.9887
Total in-flight flits = 312167 (309499 measured)
latency change    = 0.202444
throughput change = 0.00266195
Class 0:
Packet latency average = 2765.15
	minimum = 23
	maximum = 6935
Network latency average = 2759.48
	minimum = 23
	maximum = 6933
Slowest packet = 11740
Flit latency average = 2820.9
	minimum = 6
	maximum = 8038
Slowest flit = 123965
Fragmentation average = 58.0755
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0202054
	minimum = 0.0151429 (at node 110)
	maximum = 0.0245714 (at node 7)
Accepted packet rate average = 0.0102143
	minimum = 0.00771429 (at node 135)
	maximum = 0.0128571 (at node 99)
Injected flit rate average = 0.363603
	minimum = 0.272571 (at node 110)
	maximum = 0.443286 (at node 7)
Accepted flit rate average= 0.183931
	minimum = 0.138 (at node 135)
	maximum = 0.233286 (at node 157)
Injected packet length average = 17.9954
Accepted packet length average = 18.0072
Total in-flight flits = 344857 (343647 measured)
latency change    = 0.153447
throughput change = 0.00254514
Draining all recorded packets ...
Class 0:
Remaining flits: 137088 137089 137090 137091 137092 137093 137094 137095 137096 137097 [...] (378919 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (316396 flits)
Class 0:
Remaining flits: 180432 180433 180434 180435 180436 180437 180438 180439 180440 180441 [...] (409120 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (287242 flits)
Class 0:
Remaining flits: 209530 209531 209532 209533 209534 209535 209536 209537 213840 213841 [...] (442335 flits)
Measured flits: 209530 209531 209532 209533 209534 209535 209536 209537 213840 213841 [...] (257559 flits)
Class 0:
Remaining flits: 214218 214219 214220 214221 214222 214223 214224 214225 214226 214227 [...] (473665 flits)
Measured flits: 214218 214219 214220 214221 214222 214223 214224 214225 214226 214227 [...] (228440 flits)
Class 0:
Remaining flits: 222643 222644 222645 222646 222647 222648 222649 222650 222651 222652 [...] (505818 flits)
Measured flits: 222643 222644 222645 222646 222647 222648 222649 222650 222651 222652 [...] (199372 flits)
Class 0:
Remaining flits: 230346 230347 230348 230349 230350 230351 230352 230353 230354 230355 [...] (538943 flits)
Measured flits: 230346 230347 230348 230349 230350 230351 230352 230353 230354 230355 [...] (170529 flits)
Class 0:
Remaining flits: 231624 231625 231626 231627 231628 231629 231630 231631 231632 231633 [...] (569169 flits)
Measured flits: 231624 231625 231626 231627 231628 231629 231630 231631 231632 231633 [...] (143204 flits)
Class 0:
Remaining flits: 277776 277777 277778 277779 277780 277781 277782 277783 277784 277785 [...] (599579 flits)
Measured flits: 277776 277777 277778 277779 277780 277781 277782 277783 277784 277785 [...] (117084 flits)
Class 0:
Remaining flits: 290896 290897 298566 298567 298568 298569 298570 298571 298572 298573 [...] (631308 flits)
Measured flits: 290896 290897 298566 298567 298568 298569 298570 298571 298572 298573 [...] (92412 flits)
Class 0:
Remaining flits: 308916 308917 308918 308919 308920 308921 308922 308923 308924 308925 [...] (661930 flits)
Measured flits: 308916 308917 308918 308919 308920 308921 308922 308923 308924 308925 [...] (71077 flits)
Class 0:
Remaining flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (689863 flits)
Measured flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (52367 flits)
Class 0:
Remaining flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (719779 flits)
Measured flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (37640 flits)
Class 0:
Remaining flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (748000 flits)
Measured flits: 332298 332299 332300 332301 332302 332303 332304 332305 332306 332307 [...] (25515 flits)
Class 0:
Remaining flits: 356220 356221 356222 356223 356224 356225 356226 356227 356228 356229 [...] (775068 flits)
Measured flits: 356220 356221 356222 356223 356224 356225 356226 356227 356228 356229 [...] (16533 flits)
Class 0:
Remaining flits: 405162 405163 405164 405165 405166 405167 405168 405169 405170 405171 [...] (802369 flits)
Measured flits: 405162 405163 405164 405165 405166 405167 405168 405169 405170 405171 [...] (10658 flits)
Class 0:
Remaining flits: 433278 433279 433280 433281 433282 433283 433284 433285 433286 433287 [...] (828191 flits)
Measured flits: 433278 433279 433280 433281 433282 433283 433284 433285 433286 433287 [...] (6701 flits)
Class 0:
Remaining flits: 433278 433279 433280 433281 433282 433283 433284 433285 433286 433287 [...] (851443 flits)
Measured flits: 433278 433279 433280 433281 433282 433283 433284 433285 433286 433287 [...] (4228 flits)
Class 0:
Remaining flits: 482472 482473 482474 482475 482476 482477 482478 482479 482480 482481 [...] (876733 flits)
Measured flits: 482472 482473 482474 482475 482476 482477 482478 482479 482480 482481 [...] (2489 flits)
Class 0:
Remaining flits: 554958 554959 554960 554961 554962 554963 554964 554965 554966 554967 [...] (895268 flits)
Measured flits: 554958 554959 554960 554961 554962 554963 554964 554965 554966 554967 [...] (1541 flits)
Class 0:
Remaining flits: 554958 554959 554960 554961 554962 554963 554964 554965 554966 554967 [...] (911422 flits)
Measured flits: 554958 554959 554960 554961 554962 554963 554964 554965 554966 554967 [...] (927 flits)
Class 0:
Remaining flits: 606492 606493 606494 606495 606496 606497 606498 606499 606500 606501 [...] (923630 flits)
Measured flits: 606492 606493 606494 606495 606496 606497 606498 606499 606500 606501 [...] (578 flits)
Class 0:
Remaining flits: 606492 606493 606494 606495 606496 606497 606498 606499 606500 606501 [...] (934033 flits)
Measured flits: 606492 606493 606494 606495 606496 606497 606498 606499 606500 606501 [...] (234 flits)
Class 0:
Remaining flits: 670608 670609 670610 670611 670612 670613 670614 670615 670616 670617 [...] (938971 flits)
Measured flits: 670608 670609 670610 670611 670612 670613 670614 670615 670616 670617 [...] (145 flits)
Class 0:
Remaining flits: 681842 681843 681844 681845 681846 681847 681848 681849 681850 681851 [...] (942927 flits)
Measured flits: 681842 681843 681844 681845 681846 681847 681848 681849 681850 681851 [...] (34 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 752256 752257 752258 752259 752260 752261 752262 752263 752264 752265 [...] (915726 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 752256 752257 752258 752259 752260 752261 752262 752263 752264 752265 [...] (885036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 806850 806851 806852 806853 806854 806855 806856 806857 806858 806859 [...] (852993 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 852678 852679 852680 852681 852682 852683 852684 852685 852686 852687 [...] (822791 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860400 860401 860402 860403 860404 860405 860406 860407 860408 860409 [...] (791630 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 882882 882883 882884 882885 882886 882887 882888 882889 882890 882891 [...] (760580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 882882 882883 882884 882885 882886 882887 882888 882889 882890 882891 [...] (728497 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944892 944893 944894 944895 944896 944897 944898 944899 944900 944901 [...] (697329 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 958032 958033 958034 958035 958036 958037 958038 958039 958040 958041 [...] (665381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 958045 958046 958047 958048 958049 958428 958429 958430 958431 958432 [...] (634409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 958428 958429 958430 958431 958432 958433 958434 958435 958436 958437 [...] (603581 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1011564 1011565 1011566 1011567 1011568 1011569 1011570 1011571 1011572 1011573 [...] (572008 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1011564 1011565 1011566 1011567 1011568 1011569 1011570 1011571 1011572 1011573 [...] (541742 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1011564 1011565 1011566 1011567 1011568 1011569 1011570 1011571 1011572 1011573 [...] (511411 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1011564 1011565 1011566 1011567 1011568 1011569 1011570 1011571 1011572 1011573 [...] (480988 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1026108 1026109 1026110 1026111 1026112 1026113 1026114 1026115 1026116 1026117 [...] (451865 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1072008 1072009 1072010 1072011 1072012 1072013 1072014 1072015 1072016 1072017 [...] (421139 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1072008 1072009 1072010 1072011 1072012 1072013 1072014 1072015 1072016 1072017 [...] (391051 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1101204 1101205 1101206 1101207 1101208 1101209 1101210 1101211 1101212 1101213 [...] (361312 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1118448 1118449 1118450 1118451 1118452 1118453 1118454 1118455 1118456 1118457 [...] (331730 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1118448 1118449 1118450 1118451 1118452 1118453 1118454 1118455 1118456 1118457 [...] (302478 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1156608 1156609 1156610 1156611 1156612 1156613 1156614 1156615 1156616 1156617 [...] (272607 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1191564 1191565 1191566 1191567 1191568 1191569 1191570 1191571 1191572 1191573 [...] (243943 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1227798 1227799 1227800 1227801 1227802 1227803 1227804 1227805 1227806 1227807 [...] (214965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1250514 1250515 1250516 1250517 1250518 1250519 1250520 1250521 1250522 1250523 [...] (185750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1279134 1279135 1279136 1279137 1279138 1279139 1279140 1279141 1279142 1279143 [...] (156242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1320210 1320211 1320212 1320213 1320214 1320215 1320216 1320217 1320218 1320219 [...] (126742 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1376334 1376335 1376336 1376337 1376338 1376339 1376340 1376341 1376342 1376343 [...] (98461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1471476 1471477 1471478 1471479 1471480 1471481 1486206 1486207 1486208 1486209 [...] (69769 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1572768 1572769 1572770 1572771 1572772 1572773 1572774 1572775 1572776 1572777 [...] (42381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1639080 1639081 1639082 1639083 1639084 1639085 1639086 1639087 1639088 1639089 [...] (17861 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1715058 1715059 1715060 1715061 1715062 1715063 1715064 1715065 1715066 1715067 [...] (4468 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1887534 1887535 1887536 1887537 1887538 1887539 1887540 1887541 1887542 1887543 [...] (555 flits)
Measured flits: (0 flits)
Time taken is 68509 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7318.18 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25046 (1 samples)
Network latency average = 7306.47 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25021 (1 samples)
Flit latency average = 16760.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 43262 (1 samples)
Fragmentation average = 71.2291 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.0202054 (1 samples)
	minimum = 0.0151429 (1 samples)
	maximum = 0.0245714 (1 samples)
Accepted packet rate average = 0.0102143 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0128571 (1 samples)
Injected flit rate average = 0.363603 (1 samples)
	minimum = 0.272571 (1 samples)
	maximum = 0.443286 (1 samples)
Accepted flit rate average = 0.183931 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 0.233286 (1 samples)
Injected packet size average = 17.9954 (1 samples)
Accepted packet size average = 18.0072 (1 samples)
Hops average = 5.07874 (1 samples)
Total run time 49.4807
