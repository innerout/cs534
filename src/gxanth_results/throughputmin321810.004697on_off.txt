BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 88.6349
	minimum = 23
	maximum = 369
Network latency average = 72.503
	minimum = 23
	maximum = 278
Slowest packet = 66
Flit latency average = 40.8371
	minimum = 6
	maximum = 261
Slowest flit = 13733
Fragmentation average = 28.3793
	minimum = 0
	maximum = 190
Injected packet rate average = 0.00542187
	minimum = 0 (at node 10)
	maximum = 0.023 (at node 121)
Accepted packet rate average = 0.00513542
	minimum = 0.001 (at node 41)
	maximum = 0.011 (at node 48)
Injected flit rate average = 0.0967865
	minimum = 0 (at node 10)
	maximum = 0.414 (at node 121)
Accepted flit rate average= 0.0940521
	minimum = 0.018 (at node 41)
	maximum = 0.198 (at node 48)
Injected packet length average = 17.8511
Accepted packet length average = 18.3144
Total in-flight flits = 680 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 91.4339
	minimum = 23
	maximum = 429
Network latency average = 72.5104
	minimum = 23
	maximum = 290
Slowest packet = 66
Flit latency average = 40.8092
	minimum = 6
	maximum = 273
Slowest flit = 27269
Fragmentation average = 28.5826
	minimum = 0
	maximum = 190
Injected packet rate average = 0.00510156
	minimum = 0 (at node 62)
	maximum = 0.0175 (at node 14)
Accepted packet rate average = 0.00490365
	minimum = 0.001 (at node 174)
	maximum = 0.009 (at node 91)
Injected flit rate average = 0.091224
	minimum = 0 (at node 62)
	maximum = 0.315 (at node 14)
Accepted flit rate average= 0.0891094
	minimum = 0.018 (at node 174)
	maximum = 0.162 (at node 91)
Injected packet length average = 17.8816
Accepted packet length average = 18.1721
Total in-flight flits = 1044 (0 measured)
latency change    = 0.0306122
throughput change = 0.0554679
Class 0:
Packet latency average = 90.501
	minimum = 23
	maximum = 308
Network latency average = 72.8817
	minimum = 23
	maximum = 277
Slowest packet = 1819
Flit latency average = 40.9078
	minimum = 6
	maximum = 260
Slowest flit = 36845
Fragmentation average = 29.7697
	minimum = 0
	maximum = 175
Injected packet rate average = 0.00498438
	minimum = 0 (at node 38)
	maximum = 0.016 (at node 95)
Accepted packet rate average = 0.00502083
	minimum = 0 (at node 141)
	maximum = 0.014 (at node 173)
Injected flit rate average = 0.0903333
	minimum = 0 (at node 38)
	maximum = 0.3 (at node 95)
Accepted flit rate average= 0.0910365
	minimum = 0 (at node 141)
	maximum = 0.252 (at node 173)
Injected packet length average = 18.1233
Accepted packet length average = 18.1317
Total in-flight flits = 791 (0 measured)
latency change    = 0.0103076
throughput change = 0.0211683
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 100.598
	minimum = 23
	maximum = 411
Network latency average = 81.9718
	minimum = 23
	maximum = 345
Slowest packet = 3204
Flit latency average = 47.1735
	minimum = 6
	maximum = 328
Slowest flit = 59615
Fragmentation average = 36.0781
	minimum = 0
	maximum = 243
Injected packet rate average = 0.00513542
	minimum = 0 (at node 1)
	maximum = 0.024 (at node 154)
Accepted packet rate average = 0.00516146
	minimum = 0 (at node 18)
	maximum = 0.011 (at node 22)
Injected flit rate average = 0.0921979
	minimum = 0 (at node 1)
	maximum = 0.429 (at node 154)
Accepted flit rate average= 0.0923646
	minimum = 0.014 (at node 18)
	maximum = 0.205 (at node 152)
Injected packet length average = 17.9533
Accepted packet length average = 17.8951
Total in-flight flits = 805 (805 measured)
latency change    = 0.100366
throughput change = 0.0143792
Class 0:
Packet latency average = 92.9652
	minimum = 23
	maximum = 411
Network latency average = 76.1139
	minimum = 23
	maximum = 345
Slowest packet = 3204
Flit latency average = 43.4026
	minimum = 6
	maximum = 328
Slowest flit = 59615
Fragmentation average = 31.6409
	minimum = 0
	maximum = 243
Injected packet rate average = 0.004875
	minimum = 0 (at node 114)
	maximum = 0.014 (at node 154)
Accepted packet rate average = 0.00482031
	minimum = 0.001 (at node 153)
	maximum = 0.009 (at node 174)
Injected flit rate average = 0.0877344
	minimum = 0 (at node 114)
	maximum = 0.252 (at node 154)
Accepted flit rate average= 0.0869922
	minimum = 0.018 (at node 153)
	maximum = 0.1535 (at node 174)
Injected packet length average = 17.9968
Accepted packet length average = 18.047
Total in-flight flits = 1082 (1082 measured)
latency change    = 0.0820996
throughput change = 0.0617572
Class 0:
Packet latency average = 92.5409
	minimum = 23
	maximum = 411
Network latency average = 74.7777
	minimum = 23
	maximum = 345
Slowest packet = 3204
Flit latency average = 42.3862
	minimum = 6
	maximum = 328
Slowest flit = 59615
Fragmentation average = 30.826
	minimum = 0
	maximum = 243
Injected packet rate average = 0.00482292
	minimum = 0 (at node 120)
	maximum = 0.0113333 (at node 103)
Accepted packet rate average = 0.00482812
	minimum = 0.00133333 (at node 153)
	maximum = 0.00766667 (at node 113)
Injected flit rate average = 0.0867569
	minimum = 0 (at node 120)
	maximum = 0.204 (at node 103)
Accepted flit rate average= 0.0867743
	minimum = 0.024 (at node 153)
	maximum = 0.138 (at node 113)
Injected packet length average = 17.9885
Accepted packet length average = 17.9727
Total in-flight flits = 813 (813 measured)
latency change    = 0.00458477
throughput change = 0.0025109
Class 0:
Packet latency average = 93.8761
	minimum = 23
	maximum = 508
Network latency average = 75.9905
	minimum = 23
	maximum = 345
Slowest packet = 3204
Flit latency average = 43.0697
	minimum = 6
	maximum = 328
Slowest flit = 59615
Fragmentation average = 31.6421
	minimum = 0
	maximum = 243
Injected packet rate average = 0.00490885
	minimum = 0.00175 (at node 29)
	maximum = 0.011 (at node 164)
Accepted packet rate average = 0.00489193
	minimum = 0.0025 (at node 153)
	maximum = 0.00825 (at node 56)
Injected flit rate average = 0.088349
	minimum = 0.0315 (at node 29)
	maximum = 0.198 (at node 164)
Accepted flit rate average= 0.0881497
	minimum = 0.045 (at node 153)
	maximum = 0.149 (at node 56)
Injected packet length average = 17.9979
Accepted packet length average = 18.0194
Total in-flight flits = 952 (952 measured)
latency change    = 0.0142225
throughput change = 0.0156034
Class 0:
Packet latency average = 92.7495
	minimum = 23
	maximum = 508
Network latency average = 74.5682
	minimum = 23
	maximum = 345
Slowest packet = 3204
Flit latency average = 42.1336
	minimum = 6
	maximum = 328
Slowest flit = 59615
Fragmentation average = 30.6064
	minimum = 0
	maximum = 243
Injected packet rate average = 0.00486042
	minimum = 0.0014 (at node 128)
	maximum = 0.0108 (at node 172)
Accepted packet rate average = 0.00484063
	minimum = 0.0024 (at node 153)
	maximum = 0.0076 (at node 152)
Injected flit rate average = 0.0874052
	minimum = 0.0252 (at node 128)
	maximum = 0.1944 (at node 172)
Accepted flit rate average= 0.0872385
	minimum = 0.0432 (at node 153)
	maximum = 0.1402 (at node 152)
Injected packet length average = 17.9831
Accepted packet length average = 18.0222
Total in-flight flits = 1030 (1030 measured)
latency change    = 0.012147
throughput change = 0.0104449
Draining remaining packets ...
Time taken is 8090 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 93.0298 (1 samples)
	minimum = 23 (1 samples)
	maximum = 508 (1 samples)
Network latency average = 74.7441 (1 samples)
	minimum = 23 (1 samples)
	maximum = 345 (1 samples)
Flit latency average = 42.2841 (1 samples)
	minimum = 6 (1 samples)
	maximum = 328 (1 samples)
Fragmentation average = 30.6832 (1 samples)
	minimum = 0 (1 samples)
	maximum = 243 (1 samples)
Injected packet rate average = 0.00486042 (1 samples)
	minimum = 0.0014 (1 samples)
	maximum = 0.0108 (1 samples)
Accepted packet rate average = 0.00484063 (1 samples)
	minimum = 0.0024 (1 samples)
	maximum = 0.0076 (1 samples)
Injected flit rate average = 0.0874052 (1 samples)
	minimum = 0.0252 (1 samples)
	maximum = 0.1944 (1 samples)
Accepted flit rate average = 0.0872385 (1 samples)
	minimum = 0.0432 (1 samples)
	maximum = 0.1402 (1 samples)
Injected packet size average = 17.9831 (1 samples)
Accepted packet size average = 18.0222 (1 samples)
Hops average = 5.0793 (1 samples)
Total run time 3.16674
