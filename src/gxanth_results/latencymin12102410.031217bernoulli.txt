BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 291.494
	minimum = 23
	maximum = 954
Network latency average = 283.425
	minimum = 23
	maximum = 937
Slowest packet = 316
Flit latency average = 251.412
	minimum = 6
	maximum = 920
Slowest flit = 5695
Fragmentation average = 54.4047
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0104115
	minimum = 0.004 (at node 127)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.196755
	minimum = 0.072 (at node 127)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.8483
Accepted packet length average = 18.8979
Total in-flight flits = 68675 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.968
	minimum = 23
	maximum = 1781
Network latency average = 544.512
	minimum = 23
	maximum = 1767
Slowest packet = 1170
Flit latency average = 512.632
	minimum = 6
	maximum = 1844
Slowest flit = 15014
Fragmentation average = 58.0309
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0108594
	minimum = 0.005 (at node 174)
	maximum = 0.0165 (at node 167)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.19974
	minimum = 0.09 (at node 174)
	maximum = 0.297 (at node 167)
Injected packet length average = 17.9239
Accepted packet length average = 18.3933
Total in-flight flits = 137878 (0 measured)
latency change    = 0.473807
throughput change = 0.0149413
Class 0:
Packet latency average = 1327.03
	minimum = 23
	maximum = 2839
Network latency average = 1316.54
	minimum = 23
	maximum = 2810
Slowest packet = 987
Flit latency average = 1291.8
	minimum = 6
	maximum = 2793
Slowest flit = 17783
Fragmentation average = 62.8442
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0307917
	minimum = 0.019 (at node 173)
	maximum = 0.047 (at node 120)
Accepted packet rate average = 0.0109635
	minimum = 0.003 (at node 96)
	maximum = 0.023 (at node 51)
Injected flit rate average = 0.553875
	minimum = 0.342 (at node 173)
	maximum = 0.84 (at node 120)
Accepted flit rate average= 0.196828
	minimum = 0.058 (at node 96)
	maximum = 0.384 (at node 156)
Injected packet length average = 17.9878
Accepted packet length average = 17.953
Total in-flight flits = 206503 (0 measured)
latency change    = 0.582551
throughput change = 0.0147919
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 88.0098
	minimum = 23
	maximum = 584
Network latency average = 69.7101
	minimum = 23
	maximum = 584
Slowest packet = 19594
Flit latency average = 1858.94
	minimum = 6
	maximum = 3525
Slowest flit = 47716
Fragmentation average = 9.7371
	minimum = 0
	maximum = 35
Injected packet rate average = 0.030276
	minimum = 0.013 (at node 44)
	maximum = 0.046 (at node 81)
Accepted packet rate average = 0.0108698
	minimum = 0.003 (at node 43)
	maximum = 0.018 (at node 80)
Injected flit rate average = 0.544979
	minimum = 0.239 (at node 44)
	maximum = 0.827 (at node 117)
Accepted flit rate average= 0.196339
	minimum = 0.054 (at node 43)
	maximum = 0.317 (at node 80)
Injected packet length average = 18.0003
Accepted packet length average = 18.0628
Total in-flight flits = 273476 (97181 measured)
latency change    = 14.0782
throughput change = 0.00249357
Class 0:
Packet latency average = 162.963
	minimum = 23
	maximum = 1195
Network latency average = 119.914
	minimum = 23
	maximum = 1195
Slowest packet = 20983
Flit latency average = 2211.45
	minimum = 6
	maximum = 4413
Slowest flit = 60351
Fragmentation average = 9.70572
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0289062
	minimum = 0.0135 (at node 44)
	maximum = 0.0405 (at node 154)
Accepted packet rate average = 0.0105807
	minimum = 0.0045 (at node 121)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.520328
	minimum = 0.243 (at node 176)
	maximum = 0.729 (at node 154)
Accepted flit rate average= 0.190372
	minimum = 0.0825 (at node 121)
	maximum = 0.3035 (at node 44)
Injected packet length average = 18.0005
Accepted packet length average = 17.9924
Total in-flight flits = 333200 (186548 measured)
latency change    = 0.459941
throughput change = 0.0313393
Class 0:
Packet latency average = 319.662
	minimum = 23
	maximum = 2979
Network latency average = 234.525
	minimum = 23
	maximum = 2958
Slowest packet = 18052
Flit latency average = 2540.9
	minimum = 6
	maximum = 5356
Slowest flit = 43793
Fragmentation average = 10.532
	minimum = 0
	maximum = 127
Injected packet rate average = 0.0282344
	minimum = 0.0136667 (at node 100)
	maximum = 0.04 (at node 154)
Accepted packet rate average = 0.0103802
	minimum = 0.006 (at node 64)
	maximum = 0.0153333 (at node 27)
Injected flit rate average = 0.507927
	minimum = 0.249 (at node 100)
	maximum = 0.718667 (at node 154)
Accepted flit rate average= 0.187207
	minimum = 0.103667 (at node 64)
	maximum = 0.274667 (at node 27)
Injected packet length average = 17.9897
Accepted packet length average = 18.035
Total in-flight flits = 391424 (273995 measured)
latency change    = 0.490201
throughput change = 0.0169107
Class 0:
Packet latency average = 596.655
	minimum = 23
	maximum = 3859
Network latency average = 461.137
	minimum = 23
	maximum = 3834
Slowest packet = 18443
Flit latency average = 2864.61
	minimum = 6
	maximum = 6219
Slowest flit = 45809
Fragmentation average = 13.7556
	minimum = 0
	maximum = 127
Injected packet rate average = 0.0279388
	minimum = 0.014 (at node 100)
	maximum = 0.0395 (at node 59)
Accepted packet rate average = 0.0103398
	minimum = 0.006 (at node 36)
	maximum = 0.015 (at node 66)
Injected flit rate average = 0.502701
	minimum = 0.2515 (at node 100)
	maximum = 0.709 (at node 59)
Accepted flit rate average= 0.186025
	minimum = 0.10475 (at node 64)
	maximum = 0.27075 (at node 66)
Injected packet length average = 17.9929
Accepted packet length average = 17.9911
Total in-flight flits = 449934 (361046 measured)
latency change    = 0.464244
throughput change = 0.00635323
Class 0:
Packet latency average = 1148.15
	minimum = 23
	maximum = 4973
Network latency average = 986.761
	minimum = 23
	maximum = 4973
Slowest packet = 17980
Flit latency average = 3196.25
	minimum = 6
	maximum = 6878
Slowest flit = 105443
Fragmentation average = 21.1911
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0279229
	minimum = 0.0138 (at node 100)
	maximum = 0.0382 (at node 59)
Accepted packet rate average = 0.010225
	minimum = 0.0056 (at node 36)
	maximum = 0.014 (at node 66)
Injected flit rate average = 0.50247
	minimum = 0.2508 (at node 100)
	maximum = 0.6884 (at node 59)
Accepted flit rate average= 0.184067
	minimum = 0.1002 (at node 36)
	maximum = 0.2554 (at node 129)
Injected packet length average = 17.9949
Accepted packet length average = 18.0016
Total in-flight flits = 512307 (448523 measured)
latency change    = 0.480333
throughput change = 0.0106378
Class 0:
Packet latency average = 1934.61
	minimum = 23
	maximum = 6183
Network latency average = 1757.93
	minimum = 23
	maximum = 5972
Slowest packet = 17836
Flit latency average = 3526.79
	minimum = 6
	maximum = 7648
Slowest flit = 107621
Fragmentation average = 30.401
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0277639
	minimum = 0.014 (at node 100)
	maximum = 0.038 (at node 153)
Accepted packet rate average = 0.0102101
	minimum = 0.00583333 (at node 36)
	maximum = 0.0131667 (at node 66)
Injected flit rate average = 0.499717
	minimum = 0.254 (at node 100)
	maximum = 0.682333 (at node 153)
Accepted flit rate average= 0.18376
	minimum = 0.102833 (at node 64)
	maximum = 0.239333 (at node 66)
Injected packet length average = 17.9988
Accepted packet length average = 17.998
Total in-flight flits = 570577 (528138 measured)
latency change    = 0.406521
throughput change = 0.00166657
Class 0:
Packet latency average = 2734.31
	minimum = 23
	maximum = 6980
Network latency average = 2542.35
	minimum = 23
	maximum = 6940
Slowest packet = 18018
Flit latency average = 3840.38
	minimum = 6
	maximum = 8450
Slowest flit = 164848
Fragmentation average = 37.0251
	minimum = 0
	maximum = 167
Injected packet rate average = 0.0277031
	minimum = 0.0141429 (at node 100)
	maximum = 0.0367143 (at node 59)
Accepted packet rate average = 0.0102046
	minimum = 0.00657143 (at node 64)
	maximum = 0.0128571 (at node 66)
Injected flit rate average = 0.498604
	minimum = 0.254714 (at node 100)
	maximum = 0.660286 (at node 59)
Accepted flit rate average= 0.183731
	minimum = 0.117143 (at node 64)
	maximum = 0.234429 (at node 103)
Injected packet length average = 17.9981
Accepted packet length average = 18.0047
Total in-flight flits = 629781 (604131 measured)
latency change    = 0.292469
throughput change = 0.000161987
Draining all recorded packets ...
Class 0:
Remaining flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (689101 flits)
Measured flits: 321012 321013 321014 321015 321016 321017 321018 321019 321020 321021 [...] (595585 flits)
Class 0:
Remaining flits: 146085 146086 146087 185508 185509 185510 185511 185512 185513 185514 [...] (749133 flits)
Measured flits: 321024 321025 321026 321027 321028 321029 321264 321265 321266 321267 [...] (582308 flits)
Class 0:
Remaining flits: 189396 189397 189398 189399 189400 189401 189402 189403 189404 189405 [...] (802649 flits)
Measured flits: 321264 321265 321266 321267 321268 321269 321270 321271 321272 321273 [...] (566389 flits)
Class 0:
Remaining flits: 189396 189397 189398 189399 189400 189401 189402 189403 189404 189405 [...] (850931 flits)
Measured flits: 321264 321265 321266 321267 321268 321269 321270 321271 321272 321273 [...] (549517 flits)
Class 0:
Remaining flits: 213840 213841 213842 213843 213844 213845 213846 213847 213848 213849 [...] (884331 flits)
Measured flits: 321444 321445 321446 321447 321448 321449 321450 321451 321452 321453 [...] (531173 flits)
Class 0:
Remaining flits: 267642 267643 267644 267645 267646 267647 267648 267649 267650 267651 [...] (908886 flits)
Measured flits: 321624 321625 321626 321627 321628 321629 321630 321631 321632 321633 [...] (510246 flits)
Class 0:
Remaining flits: 272499 272500 272501 285030 285031 285032 285033 285034 285035 285036 [...] (926750 flits)
Measured flits: 323802 323803 323804 323805 323806 323807 323808 323809 323810 323811 [...] (487222 flits)
Class 0:
Remaining flits: 303804 303805 303806 303807 303808 303809 303810 303811 303812 303813 [...] (939918 flits)
Measured flits: 325224 325225 325226 325227 325228 325229 325230 325231 325232 325233 [...] (460369 flits)
Class 0:
Remaining flits: 305856 305857 305858 305859 305860 305861 305862 305863 305864 305865 [...] (949905 flits)
Measured flits: 325224 325225 325226 325227 325228 325229 325230 325231 325232 325233 [...] (431742 flits)
Class 0:
Remaining flits: 305856 305857 305858 305859 305860 305861 305862 305863 305864 305865 [...] (959014 flits)
Measured flits: 333990 333991 333992 333993 333994 333995 333996 333997 333998 333999 [...] (402951 flits)
Class 0:
Remaining flits: 366048 366049 366050 366051 366052 366053 366054 366055 366056 366057 [...] (966517 flits)
Measured flits: 366048 366049 366050 366051 366052 366053 366054 366055 366056 366057 [...] (373970 flits)
Class 0:
Remaining flits: 401004 401005 401006 401007 401008 401009 401010 401011 401012 401013 [...] (968335 flits)
Measured flits: 401004 401005 401006 401007 401008 401009 401010 401011 401012 401013 [...] (344700 flits)
Class 0:
Remaining flits: 437958 437959 437960 437961 437962 437963 437964 437965 437966 437967 [...] (969418 flits)
Measured flits: 437958 437959 437960 437961 437962 437963 437964 437965 437966 437967 [...] (315412 flits)
Class 0:
Remaining flits: 442728 442729 442730 442731 442732 442733 442734 442735 442736 442737 [...] (970161 flits)
Measured flits: 442728 442729 442730 442731 442732 442733 442734 442735 442736 442737 [...] (286359 flits)
Class 0:
Remaining flits: 442735 442736 442737 442738 442739 442740 442741 442742 442743 442744 [...] (970121 flits)
Measured flits: 442735 442736 442737 442738 442739 442740 442741 442742 442743 442744 [...] (257741 flits)
Class 0:
Remaining flits: 491922 491923 491924 491925 491926 491927 491928 491929 491930 491931 [...] (968605 flits)
Measured flits: 491922 491923 491924 491925 491926 491927 491928 491929 491930 491931 [...] (229328 flits)
Class 0:
Remaining flits: 523260 523261 523262 523263 523264 523265 523266 523267 523268 523269 [...] (963962 flits)
Measured flits: 523260 523261 523262 523263 523264 523265 523266 523267 523268 523269 [...] (202076 flits)
Class 0:
Remaining flits: 523260 523261 523262 523263 523264 523265 523266 523267 523268 523269 [...] (960833 flits)
Measured flits: 523260 523261 523262 523263 523264 523265 523266 523267 523268 523269 [...] (175487 flits)
Class 0:
Remaining flits: 541098 541099 541100 541101 541102 541103 541104 541105 541106 541107 [...] (956473 flits)
Measured flits: 541098 541099 541100 541101 541102 541103 541104 541105 541106 541107 [...] (149333 flits)
Class 0:
Remaining flits: 569772 569773 569774 569775 569776 569777 569778 569779 569780 569781 [...] (953848 flits)
Measured flits: 569772 569773 569774 569775 569776 569777 569778 569779 569780 569781 [...] (125248 flits)
Class 0:
Remaining flits: 569772 569773 569774 569775 569776 569777 569778 569779 569780 569781 [...] (951783 flits)
Measured flits: 569772 569773 569774 569775 569776 569777 569778 569779 569780 569781 [...] (102608 flits)
Class 0:
Remaining flits: 627372 627373 627374 627375 627376 627377 627378 627379 627380 627381 [...] (947019 flits)
Measured flits: 627372 627373 627374 627375 627376 627377 627378 627379 627380 627381 [...] (82387 flits)
Class 0:
Remaining flits: 634158 634159 634160 634161 634162 634163 634164 634165 634166 634167 [...] (946153 flits)
Measured flits: 634158 634159 634160 634161 634162 634163 634164 634165 634166 634167 [...] (64461 flits)
Class 0:
Remaining flits: 668706 668707 668708 668709 668710 668711 668712 668713 668714 668715 [...] (944151 flits)
Measured flits: 668706 668707 668708 668709 668710 668711 668712 668713 668714 668715 [...] (50160 flits)
Class 0:
Remaining flits: 704898 704899 704900 704901 704902 704903 704904 704905 704906 704907 [...] (940456 flits)
Measured flits: 704898 704899 704900 704901 704902 704903 704904 704905 704906 704907 [...] (37421 flits)
Class 0:
Remaining flits: 715176 715177 715178 715179 715180 715181 715182 715183 715184 715185 [...] (936651 flits)
Measured flits: 715176 715177 715178 715179 715180 715181 715182 715183 715184 715185 [...] (27790 flits)
Class 0:
Remaining flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (931885 flits)
Measured flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (20457 flits)
Class 0:
Remaining flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (929522 flits)
Measured flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (14608 flits)
Class 0:
Remaining flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (930057 flits)
Measured flits: 756918 756919 756920 756921 756922 756923 756924 756925 756926 756927 [...] (10408 flits)
Class 0:
Remaining flits: 773568 773569 773570 773571 773572 773573 773574 773575 773576 773577 [...] (924202 flits)
Measured flits: 773568 773569 773570 773571 773572 773573 773574 773575 773576 773577 [...] (7535 flits)
Class 0:
Remaining flits: 811170 811171 811172 811173 811174 811175 811176 811177 811178 811179 [...] (919640 flits)
Measured flits: 811170 811171 811172 811173 811174 811175 811176 811177 811178 811179 [...] (5450 flits)
Class 0:
Remaining flits: 815202 815203 815204 815205 815206 815207 815208 815209 815210 815211 [...] (914251 flits)
Measured flits: 815202 815203 815204 815205 815206 815207 815208 815209 815210 815211 [...] (3673 flits)
Class 0:
Remaining flits: 815202 815203 815204 815205 815206 815207 815208 815209 815210 815211 [...] (910123 flits)
Measured flits: 815202 815203 815204 815205 815206 815207 815208 815209 815210 815211 [...] (2705 flits)
Class 0:
Remaining flits: 815436 815437 815438 815439 815440 815441 815442 815443 815444 815445 [...] (910545 flits)
Measured flits: 815436 815437 815438 815439 815440 815441 815442 815443 815444 815445 [...] (1815 flits)
Class 0:
Remaining flits: 849942 849943 849944 849945 849946 849947 849948 849949 849950 849951 [...] (912088 flits)
Measured flits: 849942 849943 849944 849945 849946 849947 849948 849949 849950 849951 [...] (1240 flits)
Class 0:
Remaining flits: 869580 869581 869582 869583 869584 869585 869586 869587 869588 869589 [...] (908936 flits)
Measured flits: 869580 869581 869582 869583 869584 869585 869586 869587 869588 869589 [...] (810 flits)
Class 0:
Remaining flits: 889416 889417 889418 889419 889420 889421 889422 889423 889424 889425 [...] (905032 flits)
Measured flits: 889416 889417 889418 889419 889420 889421 889422 889423 889424 889425 [...] (558 flits)
Class 0:
Remaining flits: 891018 891019 891020 891021 891022 891023 891024 891025 891026 891027 [...] (903417 flits)
Measured flits: 891018 891019 891020 891021 891022 891023 891024 891025 891026 891027 [...] (414 flits)
Class 0:
Remaining flits: 928602 928603 928604 928605 928606 928607 928608 928609 928610 928611 [...] (905772 flits)
Measured flits: 928602 928603 928604 928605 928606 928607 928608 928609 928610 928611 [...] (252 flits)
Class 0:
Remaining flits: 953640 953641 953642 953643 953644 953645 953646 953647 953648 953649 [...] (904093 flits)
Measured flits: 953640 953641 953642 953643 953644 953645 953646 953647 953648 953649 [...] (180 flits)
Class 0:
Remaining flits: 971964 971965 971966 971967 971968 971969 971970 971971 971972 971973 [...] (900876 flits)
Measured flits: 971964 971965 971966 971967 971968 971969 971970 971971 971972 971973 [...] (144 flits)
Class 0:
Remaining flits: 1037286 1037287 1037288 1037289 1037290 1037291 1037292 1037293 1037294 1037295 [...] (900232 flits)
Measured flits: 1517634 1517635 1517636 1517637 1517638 1517639 1517640 1517641 1517642 1517643 [...] (72 flits)
Class 0:
Remaining flits: 1055664 1055665 1055666 1055667 1055668 1055669 1055670 1055671 1055672 1055673 [...] (899642 flits)
Measured flits: 1543266 1543267 1543268 1543269 1543270 1543271 1543272 1543273 1543274 1543275 [...] (54 flits)
Class 0:
Remaining flits: 1055664 1055665 1055666 1055667 1055668 1055669 1055670 1055671 1055672 1055673 [...] (899442 flits)
Measured flits: 1549134 1549135 1549136 1549137 1549138 1549139 1549140 1549141 1549142 1549143 [...] (36 flits)
Class 0:
Remaining flits: 1055664 1055665 1055666 1055667 1055668 1055669 1055670 1055671 1055672 1055673 [...] (897322 flits)
Measured flits: 1554894 1554895 1554896 1554897 1554898 1554899 1554900 1554901 1554902 1554903 [...] (18 flits)
Class 0:
Remaining flits: 1055664 1055665 1055666 1055667 1055668 1055669 1055670 1055671 1055672 1055673 [...] (892747 flits)
Measured flits: 1554894 1554895 1554896 1554897 1554898 1554899 1554900 1554901 1554902 1554903 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1145862 1145863 1145864 1145865 1145866 1145867 1145868 1145869 1145870 1145871 [...] (860315 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1168344 1168345 1168346 1168347 1168348 1168349 1168350 1168351 1168352 1168353 [...] (830128 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1168344 1168345 1168346 1168347 1168348 1168349 1168350 1168351 1168352 1168353 [...] (799354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1173366 1173367 1173368 1173369 1173370 1173371 1173372 1173373 1173374 1173375 [...] (768753 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1173378 1173379 1173380 1173381 1173382 1173383 1194732 1194733 1194734 1194735 [...] (736517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1194732 1194733 1194734 1194735 1194736 1194737 1194738 1194739 1194740 1194741 [...] (704829 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1217574 1217575 1217576 1217577 1217578 1217579 1217580 1217581 1217582 1217583 [...] (673692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1241262 1241263 1241264 1241265 1241266 1241267 1241268 1241269 1241270 1241271 [...] (642559 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1245780 1245781 1245782 1245783 1245784 1245785 1245786 1245787 1245788 1245789 [...] (610958 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1269792 1269793 1269794 1269795 1269796 1269797 1269798 1269799 1269800 1269801 [...] (579885 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379358 1379359 1379360 1379361 1379362 1379363 1379364 1379365 1379366 1379367 [...] (548095 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1475280 1475281 1475282 1475283 1475284 1475285 1475286 1475287 1475288 1475289 [...] (515911 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492200 1492201 1492202 1492203 1492204 1492205 1492206 1492207 1492208 1492209 [...] (484697 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492200 1492201 1492202 1492203 1492204 1492205 1492206 1492207 1492208 1492209 [...] (455406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1596150 1596151 1596152 1596153 1596154 1596155 1596156 1596157 1596158 1596159 [...] (424248 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1596150 1596151 1596152 1596153 1596154 1596155 1596156 1596157 1596158 1596159 [...] (394367 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1596150 1596151 1596152 1596153 1596154 1596155 1596156 1596157 1596158 1596159 [...] (364260 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1605024 1605025 1605026 1605027 1605028 1605029 1605030 1605031 1605032 1605033 [...] (333629 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1605035 1605036 1605037 1605038 1605039 1605040 1605041 1627002 1627003 1627004 [...] (303761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1627002 1627003 1627004 1627005 1627006 1627007 1627008 1627009 1627010 1627011 [...] (273484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1675314 1675315 1675316 1675317 1675318 1675319 1675320 1675321 1675322 1675323 [...] (244204 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1688130 1688131 1688132 1688133 1688134 1688135 1688136 1688137 1688138 1688139 [...] (215502 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1688130 1688131 1688132 1688133 1688134 1688135 1688136 1688137 1688138 1688139 [...] (186209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1703142 1703143 1703144 1703145 1703146 1703147 1703148 1703149 1703150 1703151 [...] (156919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1817028 1817029 1817030 1817031 1817032 1817033 1817034 1817035 1817036 1817037 [...] (129041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1819242 1819243 1819244 1819245 1819246 1819247 1819248 1819249 1819250 1819251 [...] (101623 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866798 1866799 1866800 1866801 1866802 1866803 1866804 1866805 1866806 1866807 [...] (75766 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1981818 1981819 1981820 1981821 1981822 1981823 1981824 1981825 1981826 1981827 [...] (51109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2123442 2123443 2123444 2123445 2123446 2123447 2123448 2123449 2123450 2123451 [...] (27014 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2213604 2213605 2213606 2213607 2213608 2213609 2213610 2213611 2213612 2213613 [...] (9812 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2452086 2452087 2452088 2452089 2452090 2452091 2452092 2452093 2452094 2452095 [...] (853 flits)
Measured flits: (0 flits)
Time taken is 87716 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14677 (1 samples)
	minimum = 23 (1 samples)
	maximum = 46323 (1 samples)
Network latency average = 13892.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 42092 (1 samples)
Flit latency average = 22171.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 59448 (1 samples)
Fragmentation average = 72.439 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0277031 (1 samples)
	minimum = 0.0141429 (1 samples)
	maximum = 0.0367143 (1 samples)
Accepted packet rate average = 0.0102046 (1 samples)
	minimum = 0.00657143 (1 samples)
	maximum = 0.0128571 (1 samples)
Injected flit rate average = 0.498604 (1 samples)
	minimum = 0.254714 (1 samples)
	maximum = 0.660286 (1 samples)
Accepted flit rate average = 0.183731 (1 samples)
	minimum = 0.117143 (1 samples)
	maximum = 0.234429 (1 samples)
Injected packet size average = 17.9981 (1 samples)
Accepted packet size average = 18.0047 (1 samples)
Hops average = 5.06739 (1 samples)
Total run time 68.6051
