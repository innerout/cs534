BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 299.154
	minimum = 22
	maximum = 806
Network latency average = 287.544
	minimum = 22
	maximum = 789
Slowest packet = 984
Flit latency average = 253.85
	minimum = 5
	maximum = 781
Slowest flit = 22893
Fragmentation average = 66.9274
	minimum = 0
	maximum = 388
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.014276
	minimum = 0.005 (at node 115)
	maximum = 0.023 (at node 103)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.27363
	minimum = 0.093 (at node 115)
	maximum = 0.424 (at node 76)
Injected packet length average = 17.8324
Accepted packet length average = 19.1671
Total in-flight flits = 69233 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 549.886
	minimum = 22
	maximum = 1715
Network latency average = 536.122
	minimum = 22
	maximum = 1618
Slowest packet = 2129
Flit latency average = 496.798
	minimum = 5
	maximum = 1633
Slowest flit = 40582
Fragmentation average = 91.3219
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0354844
	minimum = 0.0215 (at node 120)
	maximum = 0.0485 (at node 160)
Accepted packet rate average = 0.0152839
	minimum = 0.0085 (at node 116)
	maximum = 0.0205 (at node 67)
Injected flit rate average = 0.636125
	minimum = 0.387 (at node 120)
	maximum = 0.8695 (at node 160)
Accepted flit rate average= 0.286148
	minimum = 0.1655 (at node 116)
	maximum = 0.3795 (at node 14)
Injected packet length average = 17.9269
Accepted packet length average = 18.7223
Total in-flight flits = 135387 (0 measured)
latency change    = 0.455971
throughput change = 0.0437473
Class 0:
Packet latency average = 1278.04
	minimum = 22
	maximum = 2515
Network latency average = 1263.17
	minimum = 22
	maximum = 2476
Slowest packet = 3377
Flit latency average = 1221.21
	minimum = 5
	maximum = 2459
Slowest flit = 60803
Fragmentation average = 136.887
	minimum = 0
	maximum = 425
Injected packet rate average = 0.0353229
	minimum = 0.02 (at node 116)
	maximum = 0.051 (at node 86)
Accepted packet rate average = 0.0161563
	minimum = 0.008 (at node 150)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.635583
	minimum = 0.36 (at node 190)
	maximum = 0.927 (at node 86)
Accepted flit rate average= 0.293255
	minimum = 0.15 (at node 150)
	maximum = 0.529 (at node 159)
Injected packet length average = 17.9935
Accepted packet length average = 18.1512
Total in-flight flits = 201158 (0 measured)
latency change    = 0.569743
throughput change = 0.0242341
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 51.9555
	minimum = 22
	maximum = 210
Network latency average = 37.5184
	minimum = 22
	maximum = 200
Slowest packet = 20847
Flit latency average = 1703.5
	minimum = 5
	maximum = 3049
Slowest flit = 102671
Fragmentation average = 5.96518
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0362344
	minimum = 0.019 (at node 168)
	maximum = 0.052 (at node 30)
Accepted packet rate average = 0.0161875
	minimum = 0.004 (at node 4)
	maximum = 0.03 (at node 3)
Injected flit rate average = 0.652
	minimum = 0.342 (at node 168)
	maximum = 0.936 (at node 30)
Accepted flit rate average= 0.292365
	minimum = 0.064 (at node 4)
	maximum = 0.543 (at node 129)
Injected packet length average = 17.994
Accepted packet length average = 18.0611
Total in-flight flits = 270250 (115789 measured)
latency change    = 23.5988
throughput change = 0.00304628
Class 0:
Packet latency average = 53.941
	minimum = 22
	maximum = 313
Network latency average = 39.8592
	minimum = 22
	maximum = 280
Slowest packet = 26219
Flit latency average = 1923.7
	minimum = 5
	maximum = 3705
Slowest flit = 132767
Fragmentation average = 6.08627
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0358438
	minimum = 0.0265 (at node 168)
	maximum = 0.0495 (at node 187)
Accepted packet rate average = 0.0164505
	minimum = 0.0105 (at node 4)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.645065
	minimum = 0.477 (at node 168)
	maximum = 0.8905 (at node 187)
Accepted flit rate average= 0.297107
	minimum = 0.1765 (at node 139)
	maximum = 0.4695 (at node 129)
Injected packet length average = 17.9966
Accepted packet length average = 18.0606
Total in-flight flits = 334821 (227156 measured)
latency change    = 0.0368089
throughput change = 0.0159612
Class 0:
Packet latency average = 53.7245
	minimum = 22
	maximum = 313
Network latency average = 39.4758
	minimum = 22
	maximum = 280
Slowest packet = 26219
Flit latency average = 2161.81
	minimum = 5
	maximum = 4516
Slowest flit = 179092
Fragmentation average = 6.01926
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0357135
	minimum = 0.0283333 (at node 28)
	maximum = 0.0463333 (at node 187)
Accepted packet rate average = 0.0165104
	minimum = 0.011 (at node 171)
	maximum = 0.0253333 (at node 129)
Injected flit rate average = 0.642979
	minimum = 0.510667 (at node 28)
	maximum = 0.833 (at node 187)
Accepted flit rate average= 0.297479
	minimum = 0.194333 (at node 171)
	maximum = 0.456333 (at node 129)
Injected packet length average = 18.0038
Accepted packet length average = 18.0177
Total in-flight flits = 400088 (339324 measured)
latency change    = 0.00403096
throughput change = 0.00125184
Class 0:
Packet latency average = 556.067
	minimum = 22
	maximum = 4003
Network latency average = 541.831
	minimum = 22
	maximum = 3968
Slowest packet = 20422
Flit latency average = 2399.29
	minimum = 5
	maximum = 5118
Slowest flit = 228395
Fragmentation average = 28.9189
	minimum = 0
	maximum = 347
Injected packet rate average = 0.03575
	minimum = 0.02825 (at node 44)
	maximum = 0.045 (at node 187)
Accepted packet rate average = 0.0165677
	minimum = 0.01125 (at node 171)
	maximum = 0.02425 (at node 129)
Injected flit rate average = 0.643565
	minimum = 0.5105 (at node 44)
	maximum = 0.81 (at node 187)
Accepted flit rate average= 0.298628
	minimum = 0.202 (at node 171)
	maximum = 0.43375 (at node 129)
Injected packet length average = 18.0018
Accepted packet length average = 18.0247
Total in-flight flits = 466020 (443786 measured)
latency change    = 0.903385
throughput change = 0.00384572
Class 0:
Packet latency average = 1684.3
	minimum = 22
	maximum = 5001
Network latency average = 1669.94
	minimum = 22
	maximum = 4987
Slowest packet = 20408
Flit latency average = 2638.2
	minimum = 5
	maximum = 5810
Slowest flit = 267052
Fragmentation average = 74.9507
	minimum = 0
	maximum = 404
Injected packet rate average = 0.035826
	minimum = 0.0292 (at node 44)
	maximum = 0.0452 (at node 187)
Accepted packet rate average = 0.0165531
	minimum = 0.0116 (at node 171)
	maximum = 0.0226 (at node 129)
Injected flit rate average = 0.64493
	minimum = 0.5256 (at node 44)
	maximum = 0.8128 (at node 187)
Accepted flit rate average= 0.298456
	minimum = 0.2128 (at node 171)
	maximum = 0.3984 (at node 129)
Injected packet length average = 18.0017
Accepted packet length average = 18.0302
Total in-flight flits = 533714 (528588 measured)
latency change    = 0.669852
throughput change = 0.000574135
Class 0:
Packet latency average = 2550.83
	minimum = 22
	maximum = 6023
Network latency average = 2536.11
	minimum = 22
	maximum = 5986
Slowest packet = 20423
Flit latency average = 2873.83
	minimum = 5
	maximum = 6532
Slowest flit = 297539
Fragmentation average = 104.292
	minimum = 0
	maximum = 414
Injected packet rate average = 0.0358264
	minimum = 0.03 (at node 109)
	maximum = 0.0431667 (at node 187)
Accepted packet rate average = 0.0165938
	minimum = 0.0126667 (at node 63)
	maximum = 0.0218333 (at node 129)
Injected flit rate average = 0.644852
	minimum = 0.538833 (at node 109)
	maximum = 0.775 (at node 187)
Accepted flit rate average= 0.299113
	minimum = 0.229833 (at node 67)
	maximum = 0.389 (at node 129)
Injected packet length average = 17.9993
Accepted packet length average = 18.0256
Total in-flight flits = 599476 (598768 measured)
latency change    = 0.339706
throughput change = 0.00219515
Class 0:
Packet latency average = 3158.59
	minimum = 22
	maximum = 6908
Network latency average = 3143.98
	minimum = 22
	maximum = 6876
Slowest packet = 20502
Flit latency average = 3125.48
	minimum = 5
	maximum = 7059
Slowest flit = 359280
Fragmentation average = 119.749
	minimum = 0
	maximum = 505
Injected packet rate average = 0.0357225
	minimum = 0.0301429 (at node 109)
	maximum = 0.0408571 (at node 157)
Accepted packet rate average = 0.0165521
	minimum = 0.0127143 (at node 190)
	maximum = 0.0217143 (at node 129)
Injected flit rate average = 0.642952
	minimum = 0.542714 (at node 109)
	maximum = 0.735286 (at node 157)
Accepted flit rate average= 0.298404
	minimum = 0.225714 (at node 190)
	maximum = 0.383143 (at node 129)
Injected packet length average = 17.9985
Accepted packet length average = 18.0282
Total in-flight flits = 664301 (664266 measured)
latency change    = 0.192415
throughput change = 0.0023754
Draining all recorded packets ...
Class 0:
Remaining flits: 383274 383275 383276 383277 383278 383279 383280 383281 383282 383283 [...] (730624 flits)
Measured flits: 383274 383275 383276 383277 383278 383279 383280 383281 383282 383283 [...] (618666 flits)
Class 0:
Remaining flits: 472002 472003 472004 472005 472006 472007 472008 472009 472010 472011 [...] (793165 flits)
Measured flits: 472002 472003 472004 472005 472006 472007 472008 472009 472010 472011 [...] (571531 flits)
Class 0:
Remaining flits: 500769 500770 500771 500772 500773 500774 500775 500776 500777 505584 [...] (859475 flits)
Measured flits: 500769 500770 500771 500772 500773 500774 500775 500776 500777 505584 [...] (524436 flits)
Class 0:
Remaining flits: 530455 530456 530457 530458 530459 530460 530461 530462 530463 530464 [...] (928111 flits)
Measured flits: 530455 530456 530457 530458 530459 530460 530461 530462 530463 530464 [...] (477190 flits)
Class 0:
Remaining flits: 553401 553402 553403 553404 553405 553406 553407 553408 553409 582075 [...] (994496 flits)
Measured flits: 553401 553402 553403 553404 553405 553406 553407 553408 553409 582075 [...] (429753 flits)
Class 0:
Remaining flits: 611550 611551 611552 611553 611554 611555 611556 611557 611558 611559 [...] (1062649 flits)
Measured flits: 611550 611551 611552 611553 611554 611555 611556 611557 611558 611559 [...] (382412 flits)
Class 0:
Remaining flits: 663642 663643 663644 663645 663646 663647 663648 663649 663650 663651 [...] (1126915 flits)
Measured flits: 663642 663643 663644 663645 663646 663647 663648 663649 663650 663651 [...] (334846 flits)
Class 0:
Remaining flits: 694361 694362 694363 694364 694365 694366 694367 697881 697882 697883 [...] (1191497 flits)
Measured flits: 694361 694362 694363 694364 694365 694366 694367 697881 697882 697883 [...] (287369 flits)
Class 0:
Remaining flits: 757634 757635 757636 757637 760392 760393 760394 760395 760396 760397 [...] (1257448 flits)
Measured flits: 757634 757635 757636 757637 760392 760393 760394 760395 760396 760397 [...] (239748 flits)
Class 0:
Remaining flits: 760403 760404 760405 760406 760407 760408 760409 795438 795439 795440 [...] (1323477 flits)
Measured flits: 760403 760404 760405 760406 760407 760408 760409 795438 795439 795440 [...] (192126 flits)
Class 0:
Remaining flits: 810360 810361 810362 810363 810364 810365 810366 810367 810368 810369 [...] (1386571 flits)
Measured flits: 810360 810361 810362 810363 810364 810365 810366 810367 810368 810369 [...] (145373 flits)
Class 0:
Remaining flits: 826201 826202 826203 826204 826205 826206 826207 826208 826209 826210 [...] (1450167 flits)
Measured flits: 826201 826202 826203 826204 826205 826206 826207 826208 826209 826210 [...] (102165 flits)
Class 0:
Remaining flits: 924187 924188 924189 924190 924191 944907 944908 944909 951732 951733 [...] (1510154 flits)
Measured flits: 924187 924188 924189 924190 924191 944907 944908 944909 951732 951733 [...] (65385 flits)
Class 0:
Remaining flits: 951737 951738 951739 951740 951741 951742 951743 951744 951745 951746 [...] (1564417 flits)
Measured flits: 951737 951738 951739 951740 951741 951742 951743 951744 951745 951746 [...] (37724 flits)
Class 0:
Remaining flits: 975924 975925 975926 975927 975928 975929 975930 975931 975932 975933 [...] (1618320 flits)
Measured flits: 975924 975925 975926 975927 975928 975929 975930 975931 975932 975933 [...] (18526 flits)
Class 0:
Remaining flits: 1039049 1064769 1064770 1064771 1073322 1073323 1073324 1073325 1073326 1073327 [...] (1670771 flits)
Measured flits: 1039049 1064769 1064770 1064771 1073322 1073323 1073324 1073325 1073326 1073327 [...] (7338 flits)
Class 0:
Remaining flits: 1097918 1097919 1097920 1097921 1097922 1097923 1097924 1097925 1097926 1097927 [...] (1722928 flits)
Measured flits: 1097918 1097919 1097920 1097921 1097922 1097923 1097924 1097925 1097926 1097927 [...] (2263 flits)
Class 0:
Remaining flits: 1143754 1143755 1154592 1154593 1154594 1154595 1154596 1154597 1154598 1154599 [...] (1772456 flits)
Measured flits: 1143754 1143755 1154592 1154593 1154594 1154595 1154596 1154597 1154598 1154599 [...] (513 flits)
Class 0:
Remaining flits: 1184958 1184959 1184960 1184961 1184962 1184963 1184964 1184965 1184966 1184967 [...] (1821940 flits)
Measured flits: 1184958 1184959 1184960 1184961 1184962 1184963 1184964 1184965 1184966 1184967 [...] (103 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1270939 1270940 1270941 1270942 1270943 1298592 1298593 1298594 1298595 1298596 [...] (1810797 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1323978 1323979 1323980 1323981 1323982 1323983 1323984 1323985 1323986 1323987 [...] (1761466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1370592 1370593 1370594 1370595 1370596 1370597 1370598 1370599 1370600 1370601 [...] (1712184 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1427369 1427370 1427371 1427372 1427373 1427374 1427375 1427376 1427377 1427378 [...] (1663503 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1475694 1475695 1475696 1475697 1475698 1475699 1475700 1475701 1475702 1475703 [...] (1615211 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1552086 1552087 1552088 1552089 1552090 1552091 1552092 1552093 1552094 1552095 [...] (1567136 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565980 1565981 1570320 1570321 1570322 1570323 1570324 1570325 1570326 1570327 [...] (1519495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1610369 1619172 1619173 1619174 1619175 1619176 1619177 1619178 1619179 1619180 [...] (1471406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651788 1651789 1651790 1651791 1651792 1651793 1651794 1651795 1651796 1651797 [...] (1423499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1656207 1656208 1656209 1656210 1656211 1656212 1656213 1656214 1656215 1675602 [...] (1376185 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1656214 1656215 1675602 1675603 1675604 1675605 1675606 1675607 1675608 1675609 [...] (1328700 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1675602 1675603 1675604 1675605 1675606 1675607 1675608 1675609 1675610 1675611 [...] (1280859 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1699524 1699525 1699526 1699527 1699528 1699529 1699530 1699531 1699532 1699533 [...] (1233284 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765184 1765185 1765186 1765187 1779714 1779715 1779716 1779717 1779718 1779719 [...] (1185383 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1779714 1779715 1779716 1779717 1779718 1779719 1779720 1779721 1779722 1779723 [...] (1137679 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1793142 1793143 1793144 1793145 1793146 1793147 1793148 1793149 1793150 1793151 [...] (1089996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1815624 1815625 1815626 1815627 1815628 1815629 1815630 1815631 1815632 1815633 [...] (1042242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1815624 1815625 1815626 1815627 1815628 1815629 1815630 1815631 1815632 1815633 [...] (994524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1837764 1837765 1837766 1837767 1837768 1837769 1837770 1837771 1837772 1837773 [...] (946688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1919862 1919863 1919864 1919865 1919866 1919867 1919868 1919869 1919870 1919871 [...] (899038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1944114 1944115 1944116 1944117 1944118 1944119 1944120 1944121 1944122 1944123 [...] (851480 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2024964 2024965 2024966 2024967 2024968 2024969 2024970 2024971 2024972 2024973 [...] (803812 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2049102 2049103 2049104 2049105 2049106 2049107 2049108 2049109 2049110 2049111 [...] (756197 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2142792 2142793 2142794 2142795 2142796 2142797 2142798 2142799 2142800 2142801 [...] (708442 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2151432 2151433 2151434 2151435 2151436 2151437 2151438 2151439 2151440 2151441 [...] (660540 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2195619 2195620 2195621 2239107 2239108 2239109 2248524 2248525 2248526 2248527 [...] (612874 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2264130 2264131 2264132 2264133 2264134 2264135 2264136 2264137 2264138 2264139 [...] (565122 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2303116 2303117 2305350 2305351 2305352 2305353 2305354 2305355 2305356 2305357 [...] (517538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305358 2305359 2305360 2305361 2305362 2305363 2305364 2305365 2305366 2305367 [...] (469803 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2307528 2307529 2307530 2307531 2307532 2307533 2307534 2307535 2307536 2307537 [...] (422013 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2351304 2351305 2351306 2351307 2351308 2351309 2351310 2351311 2351312 2351313 [...] (374722 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2402352 2402353 2402354 2402355 2402356 2402357 2402358 2402359 2402360 2402361 [...] (327284 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2412882 2412883 2412884 2412885 2412886 2412887 2412888 2412889 2412890 2412891 [...] (279575 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2549750 2549751 2549752 2549753 2555856 2555857 2555858 2555859 2555860 2555861 [...] (231919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555870 2555871 2555872 2555873 2564568 2564569 2564570 2564571 2564572 2564573 [...] (184298 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2566548 2566549 2566550 2566551 2566552 2566553 2566554 2566555 2566556 2566557 [...] (136441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2566562 2566563 2566564 2566565 2584004 2584005 2584006 2584007 2645766 2645767 [...] (88830 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2656987 2656988 2656989 2656990 2656991 2656992 2656993 2656994 2656995 2656996 [...] (44394 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2976424 2976425 3009006 3009007 3009008 3009009 3009010 3009011 3009012 3009013 [...] (14523 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3023532 3023533 3023534 3023535 3023536 3023537 3023538 3023539 3023540 3023541 [...] (1929 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3355254 3355255 3355256 3355257 3355258 3355259 3355260 3355261 3355262 3355263 [...] (126 flits)
Measured flits: (0 flits)
Time taken is 70978 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8602.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19902 (1 samples)
Network latency average = 8587.06 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19891 (1 samples)
Flit latency average = 19187.7 (1 samples)
	minimum = 5 (1 samples)
	maximum = 46430 (1 samples)
Fragmentation average = 173.701 (1 samples)
	minimum = 0 (1 samples)
	maximum = 510 (1 samples)
Injected packet rate average = 0.0357225 (1 samples)
	minimum = 0.0301429 (1 samples)
	maximum = 0.0408571 (1 samples)
Accepted packet rate average = 0.0165521 (1 samples)
	minimum = 0.0127143 (1 samples)
	maximum = 0.0217143 (1 samples)
Injected flit rate average = 0.642952 (1 samples)
	minimum = 0.542714 (1 samples)
	maximum = 0.735286 (1 samples)
Accepted flit rate average = 0.298404 (1 samples)
	minimum = 0.225714 (1 samples)
	maximum = 0.383143 (1 samples)
Injected packet size average = 17.9985 (1 samples)
Accepted packet size average = 18.0282 (1 samples)
Hops average = 5.06929 (1 samples)
Total run time 80.0525
