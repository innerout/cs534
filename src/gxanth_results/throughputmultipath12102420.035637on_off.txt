BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 352.225
	minimum = 22
	maximum = 980
Network latency average = 239.446
	minimum = 22
	maximum = 736
Slowest packet = 45
Flit latency average = 217.07
	minimum = 5
	maximum = 792
Slowest flit = 18547
Fragmentation average = 19.3577
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.013849
	minimum = 0.005 (at node 41)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.254948
	minimum = 0.103 (at node 41)
	maximum = 0.45 (at node 76)
Injected packet length average = 17.8246
Accepted packet length average = 18.4092
Total in-flight flits = 52012 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 618.199
	minimum = 22
	maximum = 1952
Network latency average = 453.18
	minimum = 22
	maximum = 1430
Slowest packet = 45
Flit latency average = 430.48
	minimum = 5
	maximum = 1413
Slowest flit = 50453
Fragmentation average = 20.7359
	minimum = 0
	maximum = 101
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0149089
	minimum = 0.0085 (at node 62)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.561727
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 28)
Accepted flit rate average= 0.27131
	minimum = 0.153 (at node 62)
	maximum = 0.389 (at node 152)
Injected packet length average = 17.9111
Accepted packet length average = 18.1979
Total in-flight flits = 112591 (0 measured)
latency change    = 0.430241
throughput change = 0.0603073
Class 0:
Packet latency average = 1344.1
	minimum = 25
	maximum = 2766
Network latency average = 1080.67
	minimum = 22
	maximum = 2123
Slowest packet = 4068
Flit latency average = 1061.67
	minimum = 5
	maximum = 2107
Slowest flit = 88129
Fragmentation average = 22.1241
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0355208
	minimum = 0 (at node 147)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0162865
	minimum = 0.008 (at node 49)
	maximum = 0.028 (at node 182)
Injected flit rate average = 0.640312
	minimum = 0 (at node 147)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.293078
	minimum = 0.156 (at node 49)
	maximum = 0.499 (at node 182)
Injected packet length average = 18.0264
Accepted packet length average = 17.9952
Total in-flight flits = 179080 (0 measured)
latency change    = 0.540064
throughput change = 0.0742745
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 395.439
	minimum = 25
	maximum = 1706
Network latency average = 42.6079
	minimum = 22
	maximum = 171
Slowest packet = 18884
Flit latency average = 1498.88
	minimum = 5
	maximum = 2906
Slowest flit = 111029
Fragmentation average = 5.42446
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0350365
	minimum = 0.001 (at node 88)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0164427
	minimum = 0.007 (at node 113)
	maximum = 0.029 (at node 151)
Injected flit rate average = 0.62949
	minimum = 0.018 (at node 88)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.296047
	minimum = 0.125 (at node 43)
	maximum = 0.525 (at node 151)
Injected packet length average = 17.9667
Accepted packet length average = 18.0048
Total in-flight flits = 243325 (110992 measured)
latency change    = 2.399
throughput change = 0.010028
Class 0:
Packet latency average = 473.77
	minimum = 25
	maximum = 2399
Network latency average = 130.394
	minimum = 22
	maximum = 1968
Slowest packet = 18884
Flit latency average = 1734.23
	minimum = 5
	maximum = 3578
Slowest flit = 145493
Fragmentation average = 6.00426
	minimum = 0
	maximum = 78
Injected packet rate average = 0.0343255
	minimum = 0.003 (at node 88)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0164349
	minimum = 0.009 (at node 183)
	maximum = 0.026 (at node 138)
Injected flit rate average = 0.617635
	minimum = 0.054 (at node 88)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.295995
	minimum = 0.1565 (at node 183)
	maximum = 0.468 (at node 138)
Injected packet length average = 17.9935
Accepted packet length average = 18.0101
Total in-flight flits = 302676 (215951 measured)
latency change    = 0.165336
throughput change = 0.00017596
Class 0:
Packet latency average = 936.11
	minimum = 25
	maximum = 3557
Network latency average = 615.972
	minimum = 22
	maximum = 2991
Slowest packet = 18884
Flit latency average = 2000.25
	minimum = 5
	maximum = 4227
Slowest flit = 183437
Fragmentation average = 10.6804
	minimum = 0
	maximum = 90
Injected packet rate average = 0.0337448
	minimum = 0.00566667 (at node 41)
	maximum = 0.0556667 (at node 9)
Accepted packet rate average = 0.016276
	minimum = 0.0116667 (at node 54)
	maximum = 0.023 (at node 138)
Injected flit rate average = 0.607384
	minimum = 0.105667 (at node 41)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.293141
	minimum = 0.21 (at node 54)
	maximum = 0.419 (at node 138)
Injected packet length average = 17.9993
Accepted packet length average = 18.0106
Total in-flight flits = 360097 (311908 measured)
latency change    = 0.493895
throughput change = 0.00973651
Draining remaining packets ...
Class 0:
Remaining flits: 202518 202519 202520 202521 202522 202523 202524 202525 202526 202527 [...] (312414 flits)
Measured flits: 339534 339535 339536 339537 339538 339539 339540 339541 339542 339543 [...] (291466 flits)
Class 0:
Remaining flits: 241560 241561 241562 241563 241564 241565 241566 241567 241568 241569 [...] (265221 flits)
Measured flits: 339588 339589 339590 339591 339592 339593 339594 339595 339596 339597 [...] (257802 flits)
Class 0:
Remaining flits: 275040 275041 275042 275043 275044 275045 275046 275047 275048 275049 [...] (218161 flits)
Measured flits: 339696 339697 339698 339699 339700 339701 339702 339703 339704 339705 [...] (216675 flits)
Class 0:
Remaining flits: 325098 325099 325100 325101 325102 325103 325104 325105 325106 325107 [...] (171400 flits)
Measured flits: 341604 341605 341606 341607 341608 341609 341610 341611 341612 341613 [...] (171266 flits)
Class 0:
Remaining flits: 359046 359047 359048 359049 359050 359051 359052 359053 359054 359055 [...] (124372 flits)
Measured flits: 359046 359047 359048 359049 359050 359051 359052 359053 359054 359055 [...] (124372 flits)
Class 0:
Remaining flits: 386676 386677 386678 386679 386680 386681 386682 386683 386684 386685 [...] (79201 flits)
Measured flits: 386676 386677 386678 386679 386680 386681 386682 386683 386684 386685 [...] (79201 flits)
Class 0:
Remaining flits: 418014 418015 418016 418017 418018 418019 418020 418021 418022 418023 [...] (41559 flits)
Measured flits: 418014 418015 418016 418017 418018 418019 418020 418021 418022 418023 [...] (41559 flits)
Class 0:
Remaining flits: 460674 460675 460676 460677 460678 460679 460680 460681 460682 460683 [...] (14173 flits)
Measured flits: 460674 460675 460676 460677 460678 460679 460680 460681 460682 460683 [...] (14173 flits)
Class 0:
Remaining flits: 537354 537355 537356 537357 537358 537359 537360 537361 537362 537363 [...] (3937 flits)
Measured flits: 537354 537355 537356 537357 537358 537359 537360 537361 537362 537363 [...] (3937 flits)
Class 0:
Remaining flits: 585828 585829 585830 585831 585832 585833 585834 585835 585836 585837 [...] (942 flits)
Measured flits: 585828 585829 585830 585831 585832 585833 585834 585835 585836 585837 [...] (942 flits)
Time taken is 16859 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5668.79 (1 samples)
	minimum = 25 (1 samples)
	maximum = 12025 (1 samples)
Network latency average = 5285.65 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11044 (1 samples)
Flit latency average = 4471.78 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11027 (1 samples)
Fragmentation average = 23.7764 (1 samples)
	minimum = 0 (1 samples)
	maximum = 106 (1 samples)
Injected packet rate average = 0.0337448 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.016276 (1 samples)
	minimum = 0.0116667 (1 samples)
	maximum = 0.023 (1 samples)
Injected flit rate average = 0.607384 (1 samples)
	minimum = 0.105667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.293141 (1 samples)
	minimum = 0.21 (1 samples)
	maximum = 0.419 (1 samples)
Injected packet size average = 17.9993 (1 samples)
Accepted packet size average = 18.0106 (1 samples)
Hops average = 5.09353 (1 samples)
Total run time 10.0909
