BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 333.788
	minimum = 25
	maximum = 962
Network latency average = 227.541
	minimum = 22
	maximum = 816
Slowest packet = 40
Flit latency average = 196.586
	minimum = 5
	maximum = 799
Slowest flit = 11123
Fragmentation average = 53.2059
	minimum = 0
	maximum = 557
Injected packet rate average = 0.0266042
	minimum = 0 (at node 43)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0134323
	minimum = 0.006 (at node 41)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.473937
	minimum = 0 (at node 43)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.257349
	minimum = 0.121 (at node 41)
	maximum = 0.431 (at node 76)
Injected packet length average = 17.8144
Accepted packet length average = 19.159
Total in-flight flits = 42623 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 600.101
	minimum = 22
	maximum = 1936
Network latency average = 436.718
	minimum = 22
	maximum = 1579
Slowest packet = 40
Flit latency average = 393.13
	minimum = 5
	maximum = 1562
Slowest flit = 35099
Fragmentation average = 77.4433
	minimum = 0
	maximum = 670
Injected packet rate average = 0.0251563
	minimum = 0.001 (at node 24)
	maximum = 0.048 (at node 131)
Accepted packet rate average = 0.0143802
	minimum = 0.0085 (at node 83)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.450299
	minimum = 0.018 (at node 24)
	maximum = 0.8575 (at node 131)
Accepted flit rate average= 0.266552
	minimum = 0.153 (at node 83)
	maximum = 0.3935 (at node 177)
Injected packet length average = 17.9001
Accepted packet length average = 18.536
Total in-flight flits = 71668 (0 measured)
latency change    = 0.44378
throughput change = 0.0345266
Class 0:
Packet latency average = 1343.85
	minimum = 26
	maximum = 2815
Network latency average = 1040.58
	minimum = 22
	maximum = 2222
Slowest packet = 6942
Flit latency average = 998.855
	minimum = 5
	maximum = 2205
Slowest flit = 66905
Fragmentation average = 103.785
	minimum = 0
	maximum = 763
Injected packet rate average = 0.0194115
	minimum = 0 (at node 127)
	maximum = 0.051 (at node 106)
Accepted packet rate average = 0.0149167
	minimum = 0.006 (at node 134)
	maximum = 0.028 (at node 3)
Injected flit rate average = 0.348901
	minimum = 0 (at node 127)
	maximum = 0.916 (at node 106)
Accepted flit rate average= 0.270813
	minimum = 0.098 (at node 134)
	maximum = 0.494 (at node 3)
Injected packet length average = 17.974
Accepted packet length average = 18.155
Total in-flight flits = 86956 (0 measured)
latency change    = 0.553446
throughput change = 0.015732
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 896.282
	minimum = 25
	maximum = 3177
Network latency average = 119.06
	minimum = 22
	maximum = 952
Slowest packet = 13453
Flit latency average = 1320.52
	minimum = 5
	maximum = 3008
Slowest flit = 76067
Fragmentation average = 10.755
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0174792
	minimum = 0.001 (at node 127)
	maximum = 0.041 (at node 191)
Accepted packet rate average = 0.0151458
	minimum = 0.007 (at node 36)
	maximum = 0.028 (at node 123)
Injected flit rate average = 0.314995
	minimum = 0.018 (at node 127)
	maximum = 0.734 (at node 191)
Accepted flit rate average= 0.272422
	minimum = 0.098 (at node 160)
	maximum = 0.495 (at node 123)
Injected packet length average = 18.0212
Accepted packet length average = 17.9866
Total in-flight flits = 95329 (55053 measured)
latency change    = 0.499359
throughput change = 0.00590766
Class 0:
Packet latency average = 1615.01
	minimum = 25
	maximum = 4789
Network latency average = 792.97
	minimum = 22
	maximum = 1979
Slowest packet = 13453
Flit latency average = 1441.65
	minimum = 5
	maximum = 3941
Slowest flit = 93412
Fragmentation average = 56.4641
	minimum = 0
	maximum = 530
Injected packet rate average = 0.0173151
	minimum = 0.005 (at node 48)
	maximum = 0.033 (at node 191)
Accepted packet rate average = 0.0150469
	minimum = 0.0085 (at node 5)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.311685
	minimum = 0.0985 (at node 48)
	maximum = 0.5945 (at node 191)
Accepted flit rate average= 0.271039
	minimum = 0.16 (at node 5)
	maximum = 0.4125 (at node 44)
Injected packet length average = 18.0008
Accepted packet length average = 18.013
Total in-flight flits = 102865 (93554 measured)
latency change    = 0.44503
throughput change = 0.00510189
Class 0:
Packet latency average = 2246.55
	minimum = 25
	maximum = 5474
Network latency average = 1308.8
	minimum = 22
	maximum = 2937
Slowest packet = 13453
Flit latency average = 1542.95
	minimum = 5
	maximum = 4668
Slowest flit = 103409
Fragmentation average = 82.9038
	minimum = 0
	maximum = 585
Injected packet rate average = 0.0165295
	minimum = 0.004 (at node 48)
	maximum = 0.0276667 (at node 191)
Accepted packet rate average = 0.0150573
	minimum = 0.01 (at node 89)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.297656
	minimum = 0.0776667 (at node 48)
	maximum = 0.496 (at node 191)
Accepted flit rate average= 0.271095
	minimum = 0.173333 (at node 89)
	maximum = 0.385667 (at node 128)
Injected packet length average = 18.0076
Accepted packet length average = 18.0043
Total in-flight flits = 102741 (102214 measured)
latency change    = 0.281117
throughput change = 0.000208132
Draining remaining packets ...
Class 0:
Remaining flits: 188312 188313 188314 188315 200772 200773 200774 200775 200776 200777 [...] (55151 flits)
Measured flits: 247752 247753 247754 247755 247756 247757 247758 247759 247760 247761 [...] (55114 flits)
Class 0:
Remaining flits: 300064 300065 300066 300067 300068 300069 300070 300071 300072 300073 [...] (9796 flits)
Measured flits: 300064 300065 300066 300067 300068 300069 300070 300071 300072 300073 [...] (9796 flits)
Time taken is 8749 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3165.74 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7290 (1 samples)
Network latency average = 1896.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4453 (1 samples)
Flit latency average = 1802.9 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4898 (1 samples)
Fragmentation average = 96.5064 (1 samples)
	minimum = 0 (1 samples)
	maximum = 585 (1 samples)
Injected packet rate average = 0.0165295 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0150573 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.297656 (1 samples)
	minimum = 0.0776667 (1 samples)
	maximum = 0.496 (1 samples)
Accepted flit rate average = 0.271095 (1 samples)
	minimum = 0.173333 (1 samples)
	maximum = 0.385667 (1 samples)
Injected packet size average = 18.0076 (1 samples)
Accepted packet size average = 18.0043 (1 samples)
Hops average = 5.13599 (1 samples)
Total run time 12.8111
