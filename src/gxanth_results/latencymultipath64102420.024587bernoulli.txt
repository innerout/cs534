BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 239.564
	minimum = 22
	maximum = 727
Network latency average = 233.687
	minimum = 22
	maximum = 706
Slowest packet = 1239
Flit latency average = 203.844
	minimum = 5
	maximum = 689
Slowest flit = 22319
Fragmentation average = 42.7141
	minimum = 0
	maximum = 441
Injected packet rate average = 0.0246823
	minimum = 0.014 (at node 179)
	maximum = 0.039 (at node 160)
Accepted packet rate average = 0.0137708
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 51)
Injected flit rate average = 0.440411
	minimum = 0.237 (at node 179)
	maximum = 0.695 (at node 160)
Accepted flit rate average= 0.258276
	minimum = 0.089 (at node 174)
	maximum = 0.408 (at node 76)
Injected packet length average = 17.8432
Accepted packet length average = 18.7553
Total in-flight flits = 35713 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 431.145
	minimum = 22
	maximum = 1341
Network latency average = 424.764
	minimum = 22
	maximum = 1284
Slowest packet = 3253
Flit latency average = 391.904
	minimum = 5
	maximum = 1311
Slowest flit = 58701
Fragmentation average = 47.2768
	minimum = 0
	maximum = 663
Injected packet rate average = 0.0244792
	minimum = 0.016 (at node 17)
	maximum = 0.034 (at node 160)
Accepted packet rate average = 0.0144583
	minimum = 0.0075 (at node 153)
	maximum = 0.0215 (at node 154)
Injected flit rate average = 0.438792
	minimum = 0.288 (at node 17)
	maximum = 0.606 (at node 160)
Accepted flit rate average= 0.265352
	minimum = 0.139 (at node 153)
	maximum = 0.387 (at node 154)
Injected packet length average = 17.9251
Accepted packet length average = 18.3528
Total in-flight flits = 67305 (0 measured)
latency change    = 0.444355
throughput change = 0.0266647
Class 0:
Packet latency average = 958.612
	minimum = 22
	maximum = 1845
Network latency average = 951.564
	minimum = 22
	maximum = 1830
Slowest packet = 5518
Flit latency average = 920.876
	minimum = 5
	maximum = 1813
Slowest flit = 99341
Fragmentation average = 50.6312
	minimum = 0
	maximum = 446
Injected packet rate average = 0.024599
	minimum = 0.014 (at node 8)
	maximum = 0.037 (at node 48)
Accepted packet rate average = 0.0154792
	minimum = 0.006 (at node 149)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.442599
	minimum = 0.252 (at node 8)
	maximum = 0.649 (at node 48)
Accepted flit rate average= 0.276865
	minimum = 0.108 (at node 149)
	maximum = 0.504 (at node 34)
Injected packet length average = 17.9926
Accepted packet length average = 17.8863
Total in-flight flits = 99161 (0 measured)
latency change    = 0.55024
throughput change = 0.0415836
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 42.2112
	minimum = 22
	maximum = 105
Network latency average = 35.0763
	minimum = 22
	maximum = 84
Slowest packet = 14152
Flit latency average = 1255.06
	minimum = 5
	maximum = 2229
Slowest flit = 149496
Fragmentation average = 6.1883
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0249896
	minimum = 0.011 (at node 14)
	maximum = 0.041 (at node 91)
Accepted packet rate average = 0.0155469
	minimum = 0.007 (at node 17)
	maximum = 0.03 (at node 129)
Injected flit rate average = 0.449177
	minimum = 0.183 (at node 14)
	maximum = 0.741 (at node 91)
Accepted flit rate average= 0.280958
	minimum = 0.126 (at node 17)
	maximum = 0.52 (at node 129)
Injected packet length average = 17.9746
Accepted packet length average = 18.0717
Total in-flight flits = 131581 (79163 measured)
latency change    = 21.7099
throughput change = 0.0145707
Class 0:
Packet latency average = 373.094
	minimum = 22
	maximum = 2007
Network latency average = 366.274
	minimum = 22
	maximum = 1989
Slowest packet = 14170
Flit latency average = 1434.85
	minimum = 5
	maximum = 2669
Slowest flit = 196671
Fragmentation average = 13.7006
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0245599
	minimum = 0.0175 (at node 14)
	maximum = 0.035 (at node 91)
Accepted packet rate average = 0.0155469
	minimum = 0.009 (at node 35)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.442172
	minimum = 0.3075 (at node 14)
	maximum = 0.6255 (at node 91)
Accepted flit rate average= 0.279966
	minimum = 0.167 (at node 35)
	maximum = 0.426 (at node 129)
Injected packet length average = 18.0038
Accepted packet length average = 18.0079
Total in-flight flits = 161412 (151363 measured)
latency change    = 0.886862
throughput change = 0.00354396
Class 0:
Packet latency average = 1441.36
	minimum = 22
	maximum = 2990
Network latency average = 1434.6
	minimum = 22
	maximum = 2983
Slowest packet = 14223
Flit latency average = 1611.85
	minimum = 5
	maximum = 3210
Slowest flit = 235024
Fragmentation average = 37.0581
	minimum = 0
	maximum = 395
Injected packet rate average = 0.0246302
	minimum = 0.0183333 (at node 191)
	maximum = 0.0313333 (at node 123)
Accepted packet rate average = 0.0155434
	minimum = 0.0103333 (at node 36)
	maximum = 0.0226667 (at node 129)
Injected flit rate average = 0.443411
	minimum = 0.324333 (at node 191)
	maximum = 0.566667 (at node 123)
Accepted flit rate average= 0.280682
	minimum = 0.178333 (at node 36)
	maximum = 0.409 (at node 129)
Injected packet length average = 18.0027
Accepted packet length average = 18.058
Total in-flight flits = 192854 (192663 measured)
latency change    = 0.741151
throughput change = 0.00255145
Class 0:
Packet latency average = 1874.07
	minimum = 22
	maximum = 3536
Network latency average = 1867.11
	minimum = 22
	maximum = 3519
Slowest packet = 16212
Flit latency average = 1792.25
	minimum = 5
	maximum = 3502
Slowest flit = 291833
Fragmentation average = 43.5299
	minimum = 0
	maximum = 492
Injected packet rate average = 0.0246263
	minimum = 0.0195 (at node 7)
	maximum = 0.02975 (at node 94)
Accepted packet rate average = 0.015569
	minimum = 0.01075 (at node 36)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.443243
	minimum = 0.34825 (at node 7)
	maximum = 0.53525 (at node 94)
Accepted flit rate average= 0.280732
	minimum = 0.189 (at node 36)
	maximum = 0.3875 (at node 129)
Injected packet length average = 17.9988
Accepted packet length average = 18.0314
Total in-flight flits = 223993 (223993 measured)
latency change    = 0.230897
throughput change = 0.000176251
Class 0:
Packet latency average = 2141.83
	minimum = 22
	maximum = 4084
Network latency average = 2134.89
	minimum = 22
	maximum = 4056
Slowest packet = 18328
Flit latency average = 1966.85
	minimum = 5
	maximum = 4039
Slowest flit = 329921
Fragmentation average = 47.4924
	minimum = 0
	maximum = 492
Injected packet rate average = 0.024674
	minimum = 0.019 (at node 128)
	maximum = 0.0304 (at node 94)
Accepted packet rate average = 0.0156271
	minimum = 0.012 (at node 36)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.444141
	minimum = 0.3426 (at node 128)
	maximum = 0.5454 (at node 94)
Accepted flit rate average= 0.281348
	minimum = 0.2088 (at node 36)
	maximum = 0.3554 (at node 138)
Injected packet length average = 18.0004
Accepted packet length average = 18.0039
Total in-flight flits = 255433 (255433 measured)
latency change    = 0.125011
throughput change = 0.00218998
Class 0:
Packet latency average = 2349.92
	minimum = 22
	maximum = 4476
Network latency average = 2343.23
	minimum = 22
	maximum = 4476
Slowest packet = 21210
Flit latency average = 2134.36
	minimum = 5
	maximum = 4459
Slowest flit = 381797
Fragmentation average = 47.4414
	minimum = 0
	maximum = 492
Injected packet rate average = 0.024651
	minimum = 0.0198333 (at node 29)
	maximum = 0.0296667 (at node 94)
Accepted packet rate average = 0.0156519
	minimum = 0.0123333 (at node 126)
	maximum = 0.0203333 (at node 138)
Injected flit rate average = 0.443805
	minimum = 0.357 (at node 29)
	maximum = 0.534 (at node 94)
Accepted flit rate average= 0.281887
	minimum = 0.222 (at node 36)
	maximum = 0.368667 (at node 138)
Injected packet length average = 18.0035
Accepted packet length average = 18.0098
Total in-flight flits = 285591 (285591 measured)
latency change    = 0.0885544
throughput change = 0.00191295
Class 0:
Packet latency average = 2545.78
	minimum = 22
	maximum = 4968
Network latency average = 2539.04
	minimum = 22
	maximum = 4953
Slowest packet = 23609
Flit latency average = 2304.69
	minimum = 5
	maximum = 4936
Slowest flit = 424979
Fragmentation average = 49.7514
	minimum = 0
	maximum = 627
Injected packet rate average = 0.0246265
	minimum = 0.0205714 (at node 7)
	maximum = 0.03 (at node 94)
Accepted packet rate average = 0.0156369
	minimum = 0.0124286 (at node 80)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.443225
	minimum = 0.370143 (at node 29)
	maximum = 0.538571 (at node 94)
Accepted flit rate average= 0.281918
	minimum = 0.220571 (at node 80)
	maximum = 0.345857 (at node 138)
Injected packet length average = 17.9979
Accepted packet length average = 18.029
Total in-flight flits = 316027 (316027 measured)
latency change    = 0.0769359
throughput change = 0.000109968
Draining all recorded packets ...
Class 0:
Remaining flits: 486409 486410 486411 486412 486413 491735 491736 491737 491738 491739 [...] (347341 flits)
Measured flits: 486409 486410 486411 486412 486413 491735 491736 491737 491738 491739 [...] (269645 flits)
Class 0:
Remaining flits: 534074 534075 534076 534077 535764 535765 535766 535767 535768 535769 [...] (375884 flits)
Measured flits: 534074 534075 534076 534077 535764 535765 535766 535767 535768 535769 [...] (222410 flits)
Class 0:
Remaining flits: 590940 590941 590942 590943 590944 590945 590946 590947 590948 590949 [...] (406287 flits)
Measured flits: 590940 590941 590942 590943 590944 590945 590946 590947 590948 590949 [...] (175297 flits)
Class 0:
Remaining flits: 613584 613585 613586 613587 613588 613589 613590 613591 613592 613593 [...] (436692 flits)
Measured flits: 613584 613585 613586 613587 613588 613589 613590 613591 613592 613593 [...] (127960 flits)
Class 0:
Remaining flits: 680202 680203 680204 680205 680206 680207 680208 680209 680210 680211 [...] (466562 flits)
Measured flits: 680202 680203 680204 680205 680206 680207 680208 680209 680210 680211 [...] (80477 flits)
Class 0:
Remaining flits: 734958 734959 734960 734961 734962 734963 734964 734965 734966 734967 [...] (499462 flits)
Measured flits: 734958 734959 734960 734961 734962 734963 734964 734965 734966 734967 [...] (34953 flits)
Class 0:
Remaining flits: 794982 794983 794984 794985 794986 794987 796387 796388 796389 796390 [...] (528743 flits)
Measured flits: 794982 794983 794984 794985 794986 794987 796387 796388 796389 796390 [...] (5278 flits)
Class 0:
Remaining flits: 836222 836223 836224 836225 839876 839877 839878 839879 841273 841274 [...] (559727 flits)
Measured flits: 836222 836223 836224 836225 839876 839877 839878 839879 841273 841274 [...] (120 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 897402 897403 897404 897405 897406 897407 898583 898584 898585 898586 [...] (515842 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 937944 937945 937946 937947 937948 937949 937950 937951 937952 937953 [...] (468257 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 972193 972194 972195 972196 972197 974934 974935 974936 974937 974938 [...] (420630 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1027944 1027945 1027946 1027947 1027948 1027949 1027950 1027951 1027952 1027953 [...] (373438 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1058242 1058243 1058244 1058245 1058246 1058247 1058248 1058249 1058250 1058251 [...] (325908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1104984 1104985 1104986 1104987 1104988 1104989 1104990 1104991 1104992 1104993 [...] (278647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1153818 1153819 1153820 1153821 1153822 1153823 1153824 1153825 1153826 1153827 [...] (230999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1213692 1213693 1213694 1213695 1213696 1213697 1213698 1213699 1213700 1213701 [...] (183835 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1243916 1243917 1243918 1243919 1243920 1243921 1243922 1243923 1243924 1243925 [...] (136524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1316484 1316485 1316486 1316487 1316488 1316489 1316490 1316491 1316492 1316493 [...] (89316 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1360368 1360369 1360370 1360371 1360372 1360373 1360374 1360375 1360376 1360377 [...] (42925 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424391 1424392 1424393 1428090 1428091 1428092 1428093 1428094 1428095 1428096 [...] (9914 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494289 1494290 1494291 1494292 1494293 1494294 1494295 1494296 1494297 1494298 [...] (300 flits)
Measured flits: (0 flits)
Time taken is 31356 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4100.64 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8169 (1 samples)
Network latency average = 4093.73 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8166 (1 samples)
Flit latency average = 6093.33 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13802 (1 samples)
Fragmentation average = 59.8758 (1 samples)
	minimum = 0 (1 samples)
	maximum = 657 (1 samples)
Injected packet rate average = 0.0246265 (1 samples)
	minimum = 0.0205714 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0156369 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.443225 (1 samples)
	minimum = 0.370143 (1 samples)
	maximum = 0.538571 (1 samples)
Accepted flit rate average = 0.281918 (1 samples)
	minimum = 0.220571 (1 samples)
	maximum = 0.345857 (1 samples)
Injected packet size average = 17.9979 (1 samples)
Accepted packet size average = 18.029 (1 samples)
Hops average = 5.06218 (1 samples)
Total run time 31.6482
