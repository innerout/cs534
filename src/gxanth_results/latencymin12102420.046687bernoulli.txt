BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.691
	minimum = 22
	maximum = 845
Network latency average = 302.013
	minimum = 22
	maximum = 823
Slowest packet = 270
Flit latency average = 278.911
	minimum = 5
	maximum = 806
Slowest flit = 23399
Fragmentation average = 25.5091
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0152344
	minimum = 0.006 (at node 64)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.280906
	minimum = 0.108 (at node 64)
	maximum = 0.457 (at node 44)
Injected packet length average = 17.8602
Accepted packet length average = 18.439
Total in-flight flits = 100434 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 592.247
	minimum = 22
	maximum = 1682
Network latency average = 566.718
	minimum = 22
	maximum = 1543
Slowest packet = 270
Flit latency average = 543.523
	minimum = 5
	maximum = 1538
Slowest flit = 66380
Fragmentation average = 25.7087
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0457656
	minimum = 0.0315 (at node 10)
	maximum = 0.055 (at node 42)
Accepted packet rate average = 0.0161458
	minimum = 0.0085 (at node 83)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.820128
	minimum = 0.567 (at node 10)
	maximum = 0.9855 (at node 135)
Accepted flit rate average= 0.294156
	minimum = 0.155 (at node 83)
	maximum = 0.432 (at node 152)
Injected packet length average = 17.9202
Accepted packet length average = 18.2187
Total in-flight flits = 203376 (0 measured)
latency change    = 0.456829
throughput change = 0.0450441
Class 0:
Packet latency average = 1401.75
	minimum = 22
	maximum = 2331
Network latency average = 1364.64
	minimum = 22
	maximum = 2255
Slowest packet = 4309
Flit latency average = 1349.66
	minimum = 5
	maximum = 2279
Slowest flit = 110088
Fragmentation average = 25.35
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0460208
	minimum = 0.031 (at node 100)
	maximum = 0.056 (at node 71)
Accepted packet rate average = 0.0171875
	minimum = 0.008 (at node 4)
	maximum = 0.03 (at node 120)
Injected flit rate average = 0.82812
	minimum = 0.561 (at node 100)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.310276
	minimum = 0.144 (at node 4)
	maximum = 0.53 (at node 120)
Injected packet length average = 17.9945
Accepted packet length average = 18.0524
Total in-flight flits = 302851 (0 measured)
latency change    = 0.577495
throughput change = 0.0519531
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 111.756
	minimum = 22
	maximum = 834
Network latency average = 58.2548
	minimum = 22
	maximum = 590
Slowest packet = 26435
Flit latency average = 1907.3
	minimum = 5
	maximum = 3057
Slowest flit = 143826
Fragmentation average = 5.59194
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0457812
	minimum = 0.026 (at node 132)
	maximum = 0.056 (at node 63)
Accepted packet rate average = 0.0170677
	minimum = 0.008 (at node 36)
	maximum = 0.035 (at node 83)
Injected flit rate average = 0.824411
	minimum = 0.479 (at node 132)
	maximum = 1 (at node 63)
Accepted flit rate average= 0.306661
	minimum = 0.127 (at node 36)
	maximum = 0.635 (at node 83)
Injected packet length average = 18.0076
Accepted packet length average = 17.9673
Total in-flight flits = 402228 (146960 measured)
latency change    = 11.5429
throughput change = 0.0117869
Class 0:
Packet latency average = 175.872
	minimum = 22
	maximum = 1450
Network latency average = 104.62
	minimum = 22
	maximum = 1115
Slowest packet = 26435
Flit latency average = 2209.5
	minimum = 5
	maximum = 3742
Slowest flit = 195754
Fragmentation average = 5.50671
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0447474
	minimum = 0.0265 (at node 72)
	maximum = 0.0555 (at node 183)
Accepted packet rate average = 0.0167734
	minimum = 0.01 (at node 36)
	maximum = 0.0255 (at node 83)
Injected flit rate average = 0.805867
	minimum = 0.48 (at node 72)
	maximum = 1 (at node 191)
Accepted flit rate average= 0.301417
	minimum = 0.1725 (at node 36)
	maximum = 0.458 (at node 83)
Injected packet length average = 18.0093
Accepted packet length average = 17.9699
Total in-flight flits = 496401 (287682 measured)
latency change    = 0.364557
throughput change = 0.0174005
Class 0:
Packet latency average = 282.84
	minimum = 22
	maximum = 1560
Network latency average = 178.046
	minimum = 22
	maximum = 1397
Slowest packet = 26435
Flit latency average = 2519.75
	minimum = 5
	maximum = 4487
Slowest flit = 231461
Fragmentation average = 5.51977
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0439375
	minimum = 0.0276667 (at node 72)
	maximum = 0.0546667 (at node 118)
Accepted packet rate average = 0.0166059
	minimum = 0.0106667 (at node 36)
	maximum = 0.0226667 (at node 181)
Injected flit rate average = 0.790986
	minimum = 0.501 (at node 72)
	maximum = 0.984333 (at node 118)
Accepted flit rate average= 0.298441
	minimum = 0.192 (at node 36)
	maximum = 0.407 (at node 181)
Injected packet length average = 18.0025
Accepted packet length average = 17.972
Total in-flight flits = 586493 (424444 measured)
latency change    = 0.378194
throughput change = 0.0099708
Class 0:
Packet latency average = 404.139
	minimum = 22
	maximum = 3883
Network latency average = 262.178
	minimum = 22
	maximum = 3856
Slowest packet = 26840
Flit latency average = 2822.69
	minimum = 5
	maximum = 5235
Slowest flit = 277795
Fragmentation average = 5.54111
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0436393
	minimum = 0.02825 (at node 40)
	maximum = 0.055 (at node 118)
Accepted packet rate average = 0.0165339
	minimum = 0.0125 (at node 144)
	maximum = 0.02125 (at node 62)
Injected flit rate average = 0.785543
	minimum = 0.5085 (at node 40)
	maximum = 0.98825 (at node 118)
Accepted flit rate average= 0.297121
	minimum = 0.227 (at node 2)
	maximum = 0.384 (at node 129)
Injected packet length average = 18.0008
Accepted packet length average = 17.9705
Total in-flight flits = 677950 (562439 measured)
latency change    = 0.300141
throughput change = 0.00444222
Class 0:
Packet latency average = 554.369
	minimum = 22
	maximum = 4904
Network latency average = 381.613
	minimum = 22
	maximum = 4853
Slowest packet = 26988
Flit latency average = 3136.65
	minimum = 5
	maximum = 5962
Slowest flit = 320337
Fragmentation average = 5.71634
	minimum = 0
	maximum = 54
Injected packet rate average = 0.0435167
	minimum = 0.0286 (at node 40)
	maximum = 0.0534 (at node 45)
Accepted packet rate average = 0.0164677
	minimum = 0.0132 (at node 80)
	maximum = 0.0214 (at node 181)
Injected flit rate average = 0.783267
	minimum = 0.517 (at node 40)
	maximum = 0.9612 (at node 45)
Accepted flit rate average= 0.29614
	minimum = 0.2374 (at node 80)
	maximum = 0.3868 (at node 181)
Injected packet length average = 17.9992
Accepted packet length average = 17.983
Total in-flight flits = 770525 (701839 measured)
latency change    = 0.270993
throughput change = 0.00331435
Class 0:
Packet latency average = 1143.78
	minimum = 22
	maximum = 6153
Network latency average = 949.732
	minimum = 22
	maximum = 5989
Slowest packet = 26753
Flit latency average = 3449.71
	minimum = 5
	maximum = 6773
Slowest flit = 338669
Fragmentation average = 8.20569
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0432188
	minimum = 0.0271667 (at node 8)
	maximum = 0.0538333 (at node 45)
Accepted packet rate average = 0.0164097
	minimum = 0.0135 (at node 64)
	maximum = 0.0216667 (at node 181)
Injected flit rate average = 0.778101
	minimum = 0.488667 (at node 8)
	maximum = 0.967667 (at node 45)
Accepted flit rate average= 0.295095
	minimum = 0.240667 (at node 64)
	maximum = 0.388 (at node 181)
Injected packet length average = 18.0038
Accepted packet length average = 17.9829
Total in-flight flits = 859122 (831223 measured)
latency change    = 0.515317
throughput change = 0.00354112
Class 0:
Packet latency average = 2496.14
	minimum = 22
	maximum = 7246
Network latency average = 2318.81
	minimum = 22
	maximum = 6987
Slowest packet = 26753
Flit latency average = 3768.37
	minimum = 5
	maximum = 7518
Slowest flit = 392062
Fragmentation average = 13.7686
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0428266
	minimum = 0.0254286 (at node 44)
	maximum = 0.052 (at node 45)
Accepted packet rate average = 0.0162879
	minimum = 0.0132857 (at node 80)
	maximum = 0.0204286 (at node 181)
Injected flit rate average = 0.771068
	minimum = 0.457143 (at node 44)
	maximum = 0.936 (at node 45)
Accepted flit rate average= 0.293052
	minimum = 0.239143 (at node 80)
	maximum = 0.366 (at node 181)
Injected packet length average = 18.0044
Accepted packet length average = 17.992
Total in-flight flits = 945051 (939596 measured)
latency change    = 0.541782
throughput change = 0.00696987
Draining all recorded packets ...
Class 0:
Remaining flits: 429474 429475 429476 429477 429478 429479 436104 436105 436106 436107 [...] (1017538 flits)
Measured flits: 475380 475381 475382 475383 475384 475385 475386 475387 475388 475389 [...] (912821 flits)
Class 0:
Remaining flits: 469438 469439 478098 478099 478100 478101 478102 478103 478104 478105 [...] (1058783 flits)
Measured flits: 478098 478099 478100 478101 478102 478103 478104 478105 478106 478107 [...] (874646 flits)
Class 0:
Remaining flits: 531411 531412 531413 534168 534169 534170 534171 534172 534173 534174 [...] (1074801 flits)
Measured flits: 531411 531412 531413 534168 534169 534170 534171 534172 534173 534174 [...] (837178 flits)
Class 0:
Remaining flits: 579582 579583 579584 579585 579586 579587 579588 579589 579590 579591 [...] (1081407 flits)
Measured flits: 579582 579583 579584 579585 579586 579587 579588 579589 579590 579591 [...] (801403 flits)
Class 0:
Remaining flits: 608921 613188 613189 613190 613191 613192 613193 613194 613195 613196 [...] (1081420 flits)
Measured flits: 608921 613188 613189 613190 613191 613192 613193 613194 613195 613196 [...] (763619 flits)
Class 0:
Remaining flits: 645048 645049 645050 645051 645052 645053 645054 645055 645056 645057 [...] (1084313 flits)
Measured flits: 645048 645049 645050 645051 645052 645053 645054 645055 645056 645057 [...] (725393 flits)
Class 0:
Remaining flits: 699300 699301 699302 699303 699304 699305 699306 699307 699308 699309 [...] (1083786 flits)
Measured flits: 699300 699301 699302 699303 699304 699305 699306 699307 699308 699309 [...] (683462 flits)
Class 0:
Remaining flits: 734022 734023 734024 734025 734026 734027 734028 734029 734030 734031 [...] (1088678 flits)
Measured flits: 734022 734023 734024 734025 734026 734027 734028 734029 734030 734031 [...] (639814 flits)
Class 0:
Remaining flits: 774594 774595 774596 774597 774598 774599 774600 774601 774602 774603 [...] (1083046 flits)
Measured flits: 774594 774595 774596 774597 774598 774599 774600 774601 774602 774603 [...] (594009 flits)
Class 0:
Remaining flits: 790326 790327 790328 790329 790330 790331 790332 790333 790334 790335 [...] (1083635 flits)
Measured flits: 790326 790327 790328 790329 790330 790331 790332 790333 790334 790335 [...] (548691 flits)
Class 0:
Remaining flits: 805662 805663 805664 805665 805666 805667 805668 805669 805670 805671 [...] (1079416 flits)
Measured flits: 805662 805663 805664 805665 805666 805667 805668 805669 805670 805671 [...] (502587 flits)
Class 0:
Remaining flits: 852264 852265 852266 852267 852268 852269 852270 852271 852272 852273 [...] (1074398 flits)
Measured flits: 852264 852265 852266 852267 852268 852269 852270 852271 852272 852273 [...] (455197 flits)
Class 0:
Remaining flits: 876821 876822 876823 876824 876825 876826 876827 876828 876829 876830 [...] (1074074 flits)
Measured flits: 876821 876822 876823 876824 876825 876826 876827 876828 876829 876830 [...] (407552 flits)
Class 0:
Remaining flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (1071076 flits)
Measured flits: 900936 900937 900938 900939 900940 900941 900942 900943 900944 900945 [...] (360386 flits)
Class 0:
Remaining flits: 960084 960085 960086 960087 960088 960089 960090 960091 960092 960093 [...] (1069498 flits)
Measured flits: 960084 960085 960086 960087 960088 960089 960090 960091 960092 960093 [...] (312988 flits)
Class 0:
Remaining flits: 1022508 1022509 1022510 1022511 1022512 1022513 1022514 1022515 1022516 1022517 [...] (1069534 flits)
Measured flits: 1022508 1022509 1022510 1022511 1022512 1022513 1022514 1022515 1022516 1022517 [...] (266735 flits)
Class 0:
Remaining flits: 1044162 1044163 1044164 1044165 1044166 1044167 1044168 1044169 1044170 1044171 [...] (1066747 flits)
Measured flits: 1044162 1044163 1044164 1044165 1044166 1044167 1044168 1044169 1044170 1044171 [...] (220831 flits)
Class 0:
Remaining flits: 1106424 1106425 1106426 1106427 1106428 1106429 1106430 1106431 1106432 1106433 [...] (1064425 flits)
Measured flits: 1106424 1106425 1106426 1106427 1106428 1106429 1106430 1106431 1106432 1106433 [...] (174642 flits)
Class 0:
Remaining flits: 1121544 1121545 1121546 1121547 1121548 1121549 1121550 1121551 1121552 1121553 [...] (1068225 flits)
Measured flits: 1121544 1121545 1121546 1121547 1121548 1121549 1121550 1121551 1121552 1121553 [...] (130780 flits)
Class 0:
Remaining flits: 1163394 1163395 1163396 1163397 1163398 1163399 1163400 1163401 1163402 1163403 [...] (1064583 flits)
Measured flits: 1163394 1163395 1163396 1163397 1163398 1163399 1163400 1163401 1163402 1163403 [...] (91125 flits)
Class 0:
Remaining flits: 1177218 1177219 1177220 1177221 1177222 1177223 1177224 1177225 1177226 1177227 [...] (1063596 flits)
Measured flits: 1177218 1177219 1177220 1177221 1177222 1177223 1177224 1177225 1177226 1177227 [...] (59130 flits)
Class 0:
Remaining flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (1065142 flits)
Measured flits: 1219518 1219519 1219520 1219521 1219522 1219523 1219524 1219525 1219526 1219527 [...] (35402 flits)
Class 0:
Remaining flits: 1250622 1250623 1250624 1250625 1250626 1250627 1250628 1250629 1250630 1250631 [...] (1062533 flits)
Measured flits: 1250622 1250623 1250624 1250625 1250626 1250627 1250628 1250629 1250630 1250631 [...] (19557 flits)
Class 0:
Remaining flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (1059718 flits)
Measured flits: 1258866 1258867 1258868 1258869 1258870 1258871 1258872 1258873 1258874 1258875 [...] (9384 flits)
Class 0:
Remaining flits: 1290276 1290277 1290278 1290279 1290280 1290281 1290282 1290283 1290284 1290285 [...] (1062184 flits)
Measured flits: 1290276 1290277 1290278 1290279 1290280 1290281 1290282 1290283 1290284 1290285 [...] (4312 flits)
Class 0:
Remaining flits: 1337094 1337095 1337096 1337097 1337098 1337099 1337100 1337101 1337102 1337103 [...] (1061661 flits)
Measured flits: 1337094 1337095 1337096 1337097 1337098 1337099 1337100 1337101 1337102 1337103 [...] (1897 flits)
Class 0:
Remaining flits: 1388268 1388269 1388270 1388271 1388272 1388273 1388274 1388275 1388276 1388277 [...] (1066871 flits)
Measured flits: 1388268 1388269 1388270 1388271 1388272 1388273 1388274 1388275 1388276 1388277 [...] (615 flits)
Class 0:
Remaining flits: 1406232 1406233 1406234 1406235 1406236 1406237 1406238 1406239 1406240 1406241 [...] (1068061 flits)
Measured flits: 1406232 1406233 1406234 1406235 1406236 1406237 1406238 1406239 1406240 1406241 [...] (312 flits)
Class 0:
Remaining flits: 1426824 1426825 1426826 1426827 1426828 1426829 1426830 1426831 1426832 1426833 [...] (1065202 flits)
Measured flits: 1426824 1426825 1426826 1426827 1426828 1426829 1426830 1426831 1426832 1426833 [...] (108 flits)
Class 0:
Remaining flits: 1480878 1480879 1480880 1480881 1480882 1480883 1480884 1480885 1480886 1480887 [...] (1065706 flits)
Measured flits: 1480878 1480879 1480880 1480881 1480882 1480883 1480884 1480885 1480886 1480887 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1572912 1572913 1572914 1572915 1572916 1572917 1572918 1572919 1572920 1572921 [...] (1015364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1572919 1572920 1572921 1572922 1572923 1572924 1572925 1572926 1572927 1572928 [...] (964889 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1735758 1735759 1735760 1735761 1735762 1735763 1735764 1735765 1735766 1735767 [...] (914946 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1747260 1747261 1747262 1747263 1747264 1747265 1747266 1747267 1747268 1747269 [...] (864403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1814290 1814291 1816596 1816597 1816598 1816599 1816600 1816601 1816602 1816603 [...] (813776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1816596 1816597 1816598 1816599 1816600 1816601 1816602 1816603 1816604 1816605 [...] (762975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1816596 1816597 1816598 1816599 1816600 1816601 1816602 1816603 1816604 1816605 [...] (712434 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2034288 2034289 2034290 2034291 2034292 2034293 2034294 2034295 2034296 2034297 [...] (662259 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2068542 2068543 2068544 2068545 2068546 2068547 2068548 2068549 2068550 2068551 [...] (612649 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2078316 2078317 2078318 2078319 2078320 2078321 2078322 2078323 2078324 2078325 [...] (563365 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2130588 2130589 2130590 2130591 2130592 2130593 2130594 2130595 2130596 2130597 [...] (515366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2130588 2130589 2130590 2130591 2130592 2130593 2130594 2130595 2130596 2130597 [...] (467429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2243250 2243251 2243252 2243253 2243254 2243255 2243256 2243257 2243258 2243259 [...] (419791 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2283095 2283096 2283097 2283098 2283099 2283100 2283101 2286072 2286073 2286074 [...] (372264 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2349522 2349523 2349524 2349525 2349526 2349527 2349528 2349529 2349530 2349531 [...] (324162 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2396250 2396251 2396252 2396253 2396254 2396255 2396256 2396257 2396258 2396259 [...] (276579 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2474298 2474299 2474300 2474301 2474302 2474303 2474304 2474305 2474306 2474307 [...] (228624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2474298 2474299 2474300 2474301 2474302 2474303 2474304 2474305 2474306 2474307 [...] (180907 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2593728 2593729 2593730 2593731 2593732 2593733 2593734 2593735 2593736 2593737 [...] (133042 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2725182 2725183 2725184 2725185 2725186 2725187 2725188 2725189 2725190 2725191 [...] (85133 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2749652 2749653 2749654 2749655 2749656 2749657 2749658 2749659 2749660 2749661 [...] (39034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2876508 2876509 2876510 2876511 2876512 2876513 2876514 2876515 2876516 2876517 [...] (6910 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3006945 3006946 3006947 3006948 3006949 3006950 3006951 3006952 3006953 3037572 [...] (322 flits)
Measured flits: (0 flits)
Time taken is 63548 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13216.3 (1 samples)
	minimum = 22 (1 samples)
	maximum = 30439 (1 samples)
Network latency average = 12579.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 30393 (1 samples)
Flit latency average = 16019.7 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34385 (1 samples)
Fragmentation average = 28.2516 (1 samples)
	minimum = 0 (1 samples)
	maximum = 827 (1 samples)
Injected packet rate average = 0.0428266 (1 samples)
	minimum = 0.0254286 (1 samples)
	maximum = 0.052 (1 samples)
Accepted packet rate average = 0.0162879 (1 samples)
	minimum = 0.0132857 (1 samples)
	maximum = 0.0204286 (1 samples)
Injected flit rate average = 0.771068 (1 samples)
	minimum = 0.457143 (1 samples)
	maximum = 0.936 (1 samples)
Accepted flit rate average = 0.293052 (1 samples)
	minimum = 0.239143 (1 samples)
	maximum = 0.366 (1 samples)
Injected packet size average = 18.0044 (1 samples)
Accepted packet size average = 17.992 (1 samples)
Hops average = 5.06926 (1 samples)
Total run time 62.2284
