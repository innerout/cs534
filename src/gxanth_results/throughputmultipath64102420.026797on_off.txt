BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 320.97
	minimum = 22
	maximum = 963
Network latency average = 220.839
	minimum = 22
	maximum = 817
Slowest packet = 8
Flit latency average = 191.36
	minimum = 5
	maximum = 800
Slowest flit = 11141
Fragmentation average = 40.4912
	minimum = 0
	maximum = 551
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0133906
	minimum = 0.006 (at node 64)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.250292
	minimum = 0.122 (at node 64)
	maximum = 0.399 (at node 152)
Injected packet length average = 17.8353
Accepted packet length average = 18.6916
Total in-flight flits = 35644 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 529.112
	minimum = 22
	maximum = 1890
Network latency average = 400.959
	minimum = 22
	maximum = 1399
Slowest packet = 8
Flit latency average = 366.318
	minimum = 5
	maximum = 1547
Slowest flit = 35432
Fragmentation average = 54.9443
	minimum = 0
	maximum = 742
Injected packet rate average = 0.024125
	minimum = 0 (at node 30)
	maximum = 0.0545 (at node 91)
Accepted packet rate average = 0.0142083
	minimum = 0.007 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.432654
	minimum = 0 (at node 30)
	maximum = 0.974 (at node 91)
Accepted flit rate average= 0.261534
	minimum = 0.1285 (at node 30)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.9338
Accepted packet length average = 18.4071
Total in-flight flits = 66323 (0 measured)
latency change    = 0.393379
throughput change = 0.0429856
Class 0:
Packet latency average = 1060.56
	minimum = 23
	maximum = 2794
Network latency average = 892.904
	minimum = 22
	maximum = 2296
Slowest packet = 2545
Flit latency average = 858.238
	minimum = 5
	maximum = 2434
Slowest flit = 44345
Fragmentation average = 83.4112
	minimum = 0
	maximum = 656
Injected packet rate average = 0.0251198
	minimum = 0 (at node 46)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0152135
	minimum = 0.005 (at node 146)
	maximum = 0.027 (at node 63)
Injected flit rate average = 0.451083
	minimum = 0 (at node 46)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.278281
	minimum = 0.103 (at node 146)
	maximum = 0.498 (at node 106)
Injected packet length average = 17.9573
Accepted packet length average = 18.2917
Total in-flight flits = 99707 (0 measured)
latency change    = 0.5011
throughput change = 0.0601815
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 258.615
	minimum = 25
	maximum = 1061
Network latency average = 90.4548
	minimum = 22
	maximum = 960
Slowest packet = 14113
Flit latency average = 1204.85
	minimum = 5
	maximum = 2924
Slowest flit = 89342
Fragmentation average = 8.3109
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0257135
	minimum = 0 (at node 93)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0157292
	minimum = 0.006 (at node 23)
	maximum = 0.03 (at node 59)
Injected flit rate average = 0.463339
	minimum = 0.013 (at node 96)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.283167
	minimum = 0.117 (at node 185)
	maximum = 0.531 (at node 59)
Injected packet length average = 18.0192
Accepted packet length average = 18.0026
Total in-flight flits = 134205 (80893 measured)
latency change    = 3.10091
throughput change = 0.0172528
Class 0:
Packet latency average = 782.372
	minimum = 22
	maximum = 2441
Network latency average = 616.173
	minimum = 22
	maximum = 1994
Slowest packet = 14113
Flit latency average = 1365.31
	minimum = 5
	maximum = 3830
Slowest flit = 95273
Fragmentation average = 34.1172
	minimum = 0
	maximum = 534
Injected packet rate average = 0.0262682
	minimum = 0.0045 (at node 190)
	maximum = 0.0555 (at node 53)
Accepted packet rate average = 0.0156875
	minimum = 0.009 (at node 4)
	maximum = 0.023 (at node 59)
Injected flit rate average = 0.472961
	minimum = 0.087 (at node 190)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.284497
	minimum = 0.1625 (at node 4)
	maximum = 0.413 (at node 178)
Injected packet length average = 18.0051
Accepted packet length average = 18.1353
Total in-flight flits = 172026 (152087 measured)
latency change    = 0.669448
throughput change = 0.00467747
Class 0:
Packet latency average = 1350.01
	minimum = 22
	maximum = 3450
Network latency average = 1188.22
	minimum = 22
	maximum = 2951
Slowest packet = 14113
Flit latency average = 1544.36
	minimum = 5
	maximum = 4528
Slowest flit = 116783
Fragmentation average = 58.1135
	minimum = 0
	maximum = 750
Injected packet rate average = 0.0263455
	minimum = 0.00566667 (at node 48)
	maximum = 0.0553333 (at node 53)
Accepted packet rate average = 0.0157344
	minimum = 0.01 (at node 64)
	maximum = 0.0223333 (at node 157)
Injected flit rate average = 0.474405
	minimum = 0.102 (at node 48)
	maximum = 1 (at node 123)
Accepted flit rate average= 0.284516
	minimum = 0.183333 (at node 64)
	maximum = 0.400667 (at node 157)
Injected packet length average = 18.0071
Accepted packet length average = 18.0824
Total in-flight flits = 208976 (203018 measured)
latency change    = 0.420468
throughput change = 6.40709e-05
Draining remaining packets ...
Class 0:
Remaining flits: 139381 139382 139383 139384 139385 139386 139387 139388 139389 139390 [...] (161562 flits)
Measured flits: 253670 253671 253672 253673 254178 254179 254180 254181 254182 254183 [...] (160234 flits)
Class 0:
Remaining flits: 181334 181335 181336 181337 181338 181339 181340 181341 181342 181343 [...] (114086 flits)
Measured flits: 254268 254269 254270 254271 254272 254273 254274 254275 254276 254277 [...] (113688 flits)
Class 0:
Remaining flits: 193562 193563 193564 193565 193566 193567 193568 193569 193570 193571 [...] (68921 flits)
Measured flits: 254268 254269 254270 254271 254272 254273 254274 254275 254276 254277 [...] (68871 flits)
Class 0:
Remaining flits: 286524 286525 286526 286527 286528 286529 286530 286531 286532 286533 [...] (31867 flits)
Measured flits: 286524 286525 286526 286527 286528 286529 286530 286531 286532 286533 [...] (31867 flits)
Class 0:
Remaining flits: 288176 288177 288178 288179 305622 305623 305624 305625 305626 305627 [...] (12790 flits)
Measured flits: 288176 288177 288178 288179 305622 305623 305624 305625 305626 305627 [...] (12790 flits)
Class 0:
Remaining flits: 315208 315209 315210 315211 315212 315213 315214 315215 331399 331400 [...] (4670 flits)
Measured flits: 315208 315209 315210 315211 315212 315213 315214 315215 331399 331400 [...] (4670 flits)
Class 0:
Remaining flits: 364297 364298 364299 364300 364301 375634 375635 375636 375637 375638 [...] (1325 flits)
Measured flits: 364297 364298 364299 364300 364301 375634 375635 375636 375637 375638 [...] (1325 flits)
Class 0:
Remaining flits: 457482 457483 457484 457485 457486 457487 462245 462246 462247 462248 [...] (323 flits)
Measured flits: 457482 457483 457484 457485 457486 457487 462245 462246 462247 462248 [...] (323 flits)
Time taken is 14330 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3315.22 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9873 (1 samples)
Network latency average = 3115.15 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9042 (1 samples)
Flit latency average = 2740.29 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9025 (1 samples)
Fragmentation average = 137.863 (1 samples)
	minimum = 0 (1 samples)
	maximum = 800 (1 samples)
Injected packet rate average = 0.0263455 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0553333 (1 samples)
Accepted packet rate average = 0.0157344 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0223333 (1 samples)
Injected flit rate average = 0.474405 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.284516 (1 samples)
	minimum = 0.183333 (1 samples)
	maximum = 0.400667 (1 samples)
Injected packet size average = 18.0071 (1 samples)
Accepted packet size average = 18.0824 (1 samples)
Hops average = 5.07539 (1 samples)
Total run time 11.4248
