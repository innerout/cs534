BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 304.03
	minimum = 23
	maximum = 897
Network latency average = 292.168
	minimum = 23
	maximum = 864
Slowest packet = 671
Flit latency average = 263.825
	minimum = 6
	maximum = 894
Slowest flit = 10914
Fragmentation average = 50.5781
	minimum = 0
	maximum = 146
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0111979
	minimum = 0.005 (at node 96)
	maximum = 0.019 (at node 98)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.210589
	minimum = 0.09 (at node 135)
	maximum = 0.35 (at node 124)
Injected packet length average = 17.8477
Accepted packet length average = 18.806
Total in-flight flits = 88393 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 575.191
	minimum = 23
	maximum = 1855
Network latency average = 560.816
	minimum = 23
	maximum = 1829
Slowest packet = 783
Flit latency average = 528.661
	minimum = 6
	maximum = 1818
Slowest flit = 20867
Fragmentation average = 54.7293
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0376797
	minimum = 0.0285 (at node 81)
	maximum = 0.05 (at node 151)
Accepted packet rate average = 0.0115339
	minimum = 0.0065 (at node 9)
	maximum = 0.0175 (at node 98)
Injected flit rate average = 0.675784
	minimum = 0.513 (at node 81)
	maximum = 0.897 (at node 151)
Accepted flit rate average= 0.211672
	minimum = 0.119 (at node 9)
	maximum = 0.319 (at node 98)
Injected packet length average = 17.935
Accepted packet length average = 18.3522
Total in-flight flits = 179160 (0 measured)
latency change    = 0.471428
throughput change = 0.00511798
Class 0:
Packet latency average = 1429.8
	minimum = 23
	maximum = 2691
Network latency average = 1412
	minimum = 23
	maximum = 2691
Slowest packet = 2027
Flit latency average = 1392.27
	minimum = 6
	maximum = 2674
Slowest flit = 36503
Fragmentation average = 64.0231
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0360156
	minimum = 0.018 (at node 184)
	maximum = 0.052 (at node 147)
Accepted packet rate average = 0.0110625
	minimum = 0.003 (at node 153)
	maximum = 0.018 (at node 43)
Injected flit rate average = 0.646583
	minimum = 0.319 (at node 184)
	maximum = 0.944 (at node 147)
Accepted flit rate average= 0.198974
	minimum = 0.061 (at node 2)
	maximum = 0.35 (at node 43)
Injected packet length average = 17.9529
Accepted packet length average = 17.9863
Total in-flight flits = 265427 (0 measured)
latency change    = 0.597711
throughput change = 0.063817
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 243.397
	minimum = 23
	maximum = 1101
Network latency average = 140.521
	minimum = 23
	maximum = 901
Slowest packet = 21542
Flit latency average = 2128.78
	minimum = 6
	maximum = 3609
Slowest flit = 45269
Fragmentation average = 9.62547
	minimum = 0
	maximum = 66
Injected packet rate average = 0.0330833
	minimum = 0.012 (at node 36)
	maximum = 0.056 (at node 61)
Accepted packet rate average = 0.010375
	minimum = 0.003 (at node 96)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.595609
	minimum = 0.211 (at node 36)
	maximum = 1 (at node 61)
Accepted flit rate average= 0.186542
	minimum = 0.054 (at node 96)
	maximum = 0.331 (at node 128)
Injected packet length average = 18.0033
Accepted packet length average = 17.9799
Total in-flight flits = 343965 (109518 measured)
latency change    = 4.87434
throughput change = 0.0666462
Class 0:
Packet latency average = 517.064
	minimum = 23
	maximum = 1886
Network latency average = 342.096
	minimum = 23
	maximum = 1876
Slowest packet = 22160
Flit latency average = 2499.91
	minimum = 6
	maximum = 4532
Slowest flit = 57895
Fragmentation average = 10.0182
	minimum = 0
	maximum = 66
Injected packet rate average = 0.0326979
	minimum = 0.0135 (at node 20)
	maximum = 0.05 (at node 125)
Accepted packet rate average = 0.0102552
	minimum = 0.005 (at node 4)
	maximum = 0.0175 (at node 14)
Injected flit rate average = 0.588682
	minimum = 0.249 (at node 20)
	maximum = 0.9015 (at node 125)
Accepted flit rate average= 0.184315
	minimum = 0.091 (at node 4)
	maximum = 0.316 (at node 14)
Injected packet length average = 18.0037
Accepted packet length average = 17.9728
Total in-flight flits = 420694 (216069 measured)
latency change    = 0.529271
throughput change = 0.0120802
Class 0:
Packet latency average = 715.054
	minimum = 23
	maximum = 2570
Network latency average = 494.911
	minimum = 23
	maximum = 2527
Slowest packet = 22657
Flit latency average = 2854.04
	minimum = 6
	maximum = 5345
Slowest flit = 83397
Fragmentation average = 9.91941
	minimum = 0
	maximum = 66
Injected packet rate average = 0.0323177
	minimum = 0.0136667 (at node 68)
	maximum = 0.0466667 (at node 87)
Accepted packet rate average = 0.010125
	minimum = 0.006 (at node 31)
	maximum = 0.0146667 (at node 137)
Injected flit rate average = 0.581858
	minimum = 0.246667 (at node 68)
	maximum = 0.842667 (at node 87)
Accepted flit rate average= 0.182078
	minimum = 0.108 (at node 31)
	maximum = 0.270667 (at node 137)
Injected packet length average = 18.0043
Accepted packet length average = 17.983
Total in-flight flits = 495674 (320262 measured)
latency change    = 0.276888
throughput change = 0.0122858
Class 0:
Packet latency average = 970.28
	minimum = 23
	maximum = 3847
Network latency average = 686.768
	minimum = 23
	maximum = 3847
Slowest packet = 21512
Flit latency average = 3191.35
	minimum = 6
	maximum = 6187
Slowest flit = 77543
Fragmentation average = 10.241
	minimum = 0
	maximum = 80
Injected packet rate average = 0.0322773
	minimum = 0.01425 (at node 44)
	maximum = 0.046 (at node 125)
Accepted packet rate average = 0.0101745
	minimum = 0.00625 (at node 31)
	maximum = 0.014 (at node 33)
Injected flit rate average = 0.581155
	minimum = 0.254 (at node 68)
	maximum = 0.82675 (at node 125)
Accepted flit rate average= 0.183331
	minimum = 0.11425 (at node 4)
	maximum = 0.25175 (at node 33)
Injected packet length average = 18.005
Accepted packet length average = 18.0187
Total in-flight flits = 570831 (425217 measured)
latency change    = 0.263044
throughput change = 0.00683248
Draining remaining packets ...
Class 0:
Remaining flits: 112032 112033 112034 112035 112036 112037 112038 112039 112040 112041 [...] (538065 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (421834 flits)
Class 0:
Remaining flits: 130014 130015 130016 130017 130018 130019 130020 130021 130022 130023 [...] (506514 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (417187 flits)
Class 0:
Remaining flits: 156168 156169 156170 156171 156172 156173 156174 156175 156176 156177 [...] (473863 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (409426 flits)
Class 0:
Remaining flits: 170838 170839 170840 170841 170842 170843 170844 170845 170846 170847 [...] (442309 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (398031 flits)
Class 0:
Remaining flits: 185004 185005 185006 185007 185008 185009 185010 185011 185012 185013 [...] (409286 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (381377 flits)
Class 0:
Remaining flits: 185004 185005 185006 185007 185008 185009 185010 185011 185012 185013 [...] (376447 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (360484 flits)
Class 0:
Remaining flits: 208566 208567 208568 208569 208570 208571 208572 208573 208574 208575 [...] (346246 flits)
Measured flits: 384912 384913 384914 384915 384916 384917 384918 384919 384920 384921 [...] (336975 flits)
Class 0:
Remaining flits: 218268 218269 218270 218271 218272 218273 218274 218275 218276 218277 [...] (316821 flits)
Measured flits: 385056 385057 385058 385059 385060 385061 385062 385063 385064 385065 [...] (311822 flits)
Class 0:
Remaining flits: 263513 263514 263515 263516 263517 263518 263519 269946 269947 269948 [...] (287818 flits)
Measured flits: 385470 385471 385472 385473 385474 385475 385476 385477 385478 385479 [...] (284944 flits)
Class 0:
Remaining flits: 293652 293653 293654 293655 293656 293657 293658 293659 293660 293661 [...] (258483 flits)
Measured flits: 386370 386371 386372 386373 386374 386375 386376 386377 386378 386379 [...] (256984 flits)
Class 0:
Remaining flits: 300744 300745 300746 300747 300748 300749 300750 300751 300752 300753 [...] (228360 flits)
Measured flits: 386820 386821 386822 386823 386824 386825 386826 386827 386828 386829 [...] (227568 flits)
Class 0:
Remaining flits: 339628 339629 339630 339631 339632 339633 339634 339635 339636 339637 [...] (198544 flits)
Measured flits: 388836 388837 388838 388839 388840 388841 388842 388843 388844 388845 [...] (198228 flits)
Class 0:
Remaining flits: 359602 359603 367740 367741 367742 367743 367744 367745 367746 367747 [...] (169111 flits)
Measured flits: 389862 389863 389864 389865 389866 389867 389868 389869 389870 389871 [...] (168998 flits)
Class 0:
Remaining flits: 381114 381115 381116 381117 381118 381119 381120 381121 381122 381123 [...] (139719 flits)
Measured flits: 393912 393913 393914 393915 393916 393917 393918 393919 393920 393921 [...] (139701 flits)
Class 0:
Remaining flits: 407376 407377 407378 407379 407380 407381 407382 407383 407384 407385 [...] (110220 flits)
Measured flits: 407376 407377 407378 407379 407380 407381 407382 407383 407384 407385 [...] (110220 flits)
Class 0:
Remaining flits: 428256 428257 428258 428259 428260 428261 428262 428263 428264 428265 [...] (80166 flits)
Measured flits: 428256 428257 428258 428259 428260 428261 428262 428263 428264 428265 [...] (80166 flits)
Class 0:
Remaining flits: 473958 473959 473960 473961 473962 473963 473964 473965 473966 473967 [...] (51178 flits)
Measured flits: 473958 473959 473960 473961 473962 473963 473964 473965 473966 473967 [...] (51178 flits)
Class 0:
Remaining flits: 473958 473959 473960 473961 473962 473963 473964 473965 473966 473967 [...] (24301 flits)
Measured flits: 473958 473959 473960 473961 473962 473963 473964 473965 473966 473967 [...] (24301 flits)
Class 0:
Remaining flits: 535294 535295 535296 535297 535298 535299 535300 535301 551609 560628 [...] (4142 flits)
Measured flits: 535294 535295 535296 535297 535298 535299 535300 535301 551609 560628 [...] (4142 flits)
Time taken is 26836 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12891.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21708 (1 samples)
Network latency average = 12717.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21708 (1 samples)
Flit latency average = 10146.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 21691 (1 samples)
Fragmentation average = 66.6797 (1 samples)
	minimum = 0 (1 samples)
	maximum = 185 (1 samples)
Injected packet rate average = 0.0322773 (1 samples)
	minimum = 0.01425 (1 samples)
	maximum = 0.046 (1 samples)
Accepted packet rate average = 0.0101745 (1 samples)
	minimum = 0.00625 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.581155 (1 samples)
	minimum = 0.254 (1 samples)
	maximum = 0.82675 (1 samples)
Accepted flit rate average = 0.183331 (1 samples)
	minimum = 0.11425 (1 samples)
	maximum = 0.25175 (1 samples)
Injected packet size average = 18.005 (1 samples)
Accepted packet size average = 18.0187 (1 samples)
Hops average = 5.18444 (1 samples)
Total run time 27.1545
