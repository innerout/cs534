BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.034
	minimum = 22
	maximum = 889
Network latency average = 301.164
	minimum = 22
	maximum = 862
Slowest packet = 911
Flit latency average = 263.736
	minimum = 5
	maximum = 846
Slowest flit = 17473
Fragmentation average = 104.516
	minimum = 0
	maximum = 598
Injected packet rate average = 0.0380312
	minimum = 0.022 (at node 4)
	maximum = 0.05 (at node 181)
Accepted packet rate average = 0.0139063
	minimum = 0.006 (at node 30)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.678682
	minimum = 0.394 (at node 4)
	maximum = 0.899 (at node 181)
Accepted flit rate average= 0.2805
	minimum = 0.128 (at node 30)
	maximum = 0.447 (at node 167)
Injected packet length average = 17.8454
Accepted packet length average = 20.1708
Total in-flight flits = 77760 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 655.279
	minimum = 22
	maximum = 1752
Network latency average = 608.007
	minimum = 22
	maximum = 1688
Slowest packet = 2198
Flit latency average = 545.475
	minimum = 5
	maximum = 1703
Slowest flit = 39218
Fragmentation average = 153.331
	minimum = 0
	maximum = 843
Injected packet rate average = 0.0291927
	minimum = 0.018 (at node 88)
	maximum = 0.0395 (at node 181)
Accepted packet rate average = 0.0146302
	minimum = 0.008 (at node 116)
	maximum = 0.023 (at node 14)
Injected flit rate average = 0.52237
	minimum = 0.3235 (at node 184)
	maximum = 0.7055 (at node 181)
Accepted flit rate average= 0.277164
	minimum = 0.176 (at node 116)
	maximum = 0.422 (at node 14)
Injected packet length average = 17.8938
Accepted packet length average = 18.9446
Total in-flight flits = 95691 (0 measured)
latency change    = 0.510081
throughput change = 0.012036
Class 0:
Packet latency average = 1650.67
	minimum = 200
	maximum = 2700
Network latency average = 1449.4
	minimum = 23
	maximum = 2562
Slowest packet = 3055
Flit latency average = 1384.23
	minimum = 5
	maximum = 2545
Slowest flit = 55007
Fragmentation average = 177.328
	minimum = 0
	maximum = 771
Injected packet rate average = 0.0153594
	minimum = 0.002 (at node 156)
	maximum = 0.035 (at node 33)
Accepted packet rate average = 0.0149635
	minimum = 0.006 (at node 60)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.276839
	minimum = 0.037 (at node 156)
	maximum = 0.636 (at node 33)
Accepted flit rate average= 0.267609
	minimum = 0.107 (at node 180)
	maximum = 0.425 (at node 97)
Injected packet length average = 18.0241
Accepted packet length average = 17.8841
Total in-flight flits = 97536 (0 measured)
latency change    = 0.603023
throughput change = 0.0357039
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1699.42
	minimum = 1024
	maximum = 2903
Network latency average = 79.8912
	minimum = 22
	maximum = 969
Slowest packet = 14215
Flit latency average = 1696.01
	minimum = 5
	maximum = 3071
Slowest flit = 118123
Fragmentation average = 7.94142
	minimum = 0
	maximum = 48
Injected packet rate average = 0.0153958
	minimum = 0.001 (at node 62)
	maximum = 0.037 (at node 74)
Accepted packet rate average = 0.0149375
	minimum = 0.006 (at node 36)
	maximum = 0.027 (at node 103)
Injected flit rate average = 0.276656
	minimum = 0.022 (at node 62)
	maximum = 0.667 (at node 74)
Accepted flit rate average= 0.267521
	minimum = 0.083 (at node 36)
	maximum = 0.459 (at node 103)
Injected packet length average = 17.9696
Accepted packet length average = 17.9093
Total in-flight flits = 99272 (48716 measured)
latency change    = 0.0286835
throughput change = 0.000330971
Class 0:
Packet latency average = 2555.58
	minimum = 1024
	maximum = 4107
Network latency average = 724.141
	minimum = 22
	maximum = 1957
Slowest packet = 14215
Flit latency average = 1762.54
	minimum = 5
	maximum = 3794
Slowest flit = 149072
Fragmentation average = 64.8083
	minimum = 0
	maximum = 656
Injected packet rate average = 0.0154974
	minimum = 0.001 (at node 62)
	maximum = 0.0245 (at node 30)
Accepted packet rate average = 0.014974
	minimum = 0.009 (at node 4)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.278802
	minimum = 0.0175 (at node 62)
	maximum = 0.444 (at node 30)
Accepted flit rate average= 0.268279
	minimum = 0.17 (at node 36)
	maximum = 0.377 (at node 129)
Injected packet length average = 17.9903
Accepted packet length average = 17.9163
Total in-flight flits = 101545 (87835 measured)
latency change    = 0.335016
throughput change = 0.00282472
Class 0:
Packet latency average = 3268.18
	minimum = 1024
	maximum = 4836
Network latency average = 1378.2
	minimum = 22
	maximum = 2927
Slowest packet = 14215
Flit latency average = 1802.93
	minimum = 5
	maximum = 4215
Slowest flit = 158975
Fragmentation average = 105.635
	minimum = 0
	maximum = 736
Injected packet rate average = 0.0152951
	minimum = 0.00733333 (at node 68)
	maximum = 0.022 (at node 74)
Accepted packet rate average = 0.0149757
	minimum = 0.00966667 (at node 36)
	maximum = 0.02 (at node 165)
Injected flit rate average = 0.275526
	minimum = 0.134333 (at node 24)
	maximum = 0.4 (at node 74)
Accepted flit rate average= 0.267866
	minimum = 0.173333 (at node 36)
	maximum = 0.368333 (at node 118)
Injected packet length average = 18.014
Accepted packet length average = 17.8867
Total in-flight flits = 101951 (100877 measured)
latency change    = 0.218043
throughput change = 0.0015393
Draining remaining packets ...
Class 0:
Remaining flits: 234270 234271 234272 234273 234274 234275 234276 234277 234278 234279 [...] (54587 flits)
Measured flits: 259234 259235 262620 262621 262622 262623 262624 262625 262626 262627 [...] (54526 flits)
Class 0:
Remaining flits: 293143 293144 293145 293146 293147 297306 297307 297308 297309 297310 [...] (8825 flits)
Measured flits: 293143 293144 293145 293146 293147 297306 297307 297308 297309 297310 [...] (8825 flits)
Time taken is 8804 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4235.65 (1 samples)
	minimum = 1024 (1 samples)
	maximum = 6744 (1 samples)
Network latency average = 1979.74 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4616 (1 samples)
Flit latency average = 1966.21 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4877 (1 samples)
Fragmentation average = 123.21 (1 samples)
	minimum = 0 (1 samples)
	maximum = 736 (1 samples)
Injected packet rate average = 0.0152951 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0149757 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.275526 (1 samples)
	minimum = 0.134333 (1 samples)
	maximum = 0.4 (1 samples)
Accepted flit rate average = 0.267866 (1 samples)
	minimum = 0.173333 (1 samples)
	maximum = 0.368333 (1 samples)
Injected packet size average = 18.014 (1 samples)
Accepted packet size average = 17.8867 (1 samples)
Hops average = 5.13009 (1 samples)
Total run time 13.7996
