BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 313.197
	minimum = 23
	maximum = 933
Network latency average = 300.582
	minimum = 23
	maximum = 933
Slowest packet = 355
Flit latency average = 245.58
	minimum = 6
	maximum = 935
Slowest flit = 4800
Fragmentation average = 150.632
	minimum = 0
	maximum = 733
Injected packet rate average = 0.0372188
	minimum = 0.024 (at node 86)
	maximum = 0.052 (at node 151)
Accepted packet rate average = 0.011151
	minimum = 0.004 (at node 81)
	maximum = 0.018 (at node 48)
Injected flit rate average = 0.664563
	minimum = 0.432 (at node 86)
	maximum = 0.926 (at node 151)
Accepted flit rate average= 0.22975
	minimum = 0.09 (at node 135)
	maximum = 0.37 (at node 48)
Injected packet length average = 17.8556
Accepted packet length average = 20.6035
Total in-flight flits = 84516 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 609.038
	minimum = 23
	maximum = 1873
Network latency average = 592.045
	minimum = 23
	maximum = 1867
Slowest packet = 723
Flit latency average = 521.656
	minimum = 6
	maximum = 1900
Slowest flit = 7654
Fragmentation average = 190.012
	minimum = 0
	maximum = 1441
Injected packet rate average = 0.035862
	minimum = 0.026 (at node 76)
	maximum = 0.0465 (at node 35)
Accepted packet rate average = 0.0121536
	minimum = 0.007 (at node 40)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.642693
	minimum = 0.4635 (at node 76)
	maximum = 0.837 (at node 35)
Accepted flit rate average= 0.232437
	minimum = 0.143 (at node 40)
	maximum = 0.324 (at node 73)
Injected packet length average = 17.9213
Accepted packet length average = 19.1249
Total in-flight flits = 158658 (0 measured)
latency change    = 0.485752
throughput change = 0.0115622
Class 0:
Packet latency average = 1542.44
	minimum = 28
	maximum = 2844
Network latency average = 1506.85
	minimum = 28
	maximum = 2802
Slowest packet = 902
Flit latency average = 1463.69
	minimum = 6
	maximum = 2854
Slowest flit = 15807
Fragmentation average = 221.881
	minimum = 1
	maximum = 1773
Injected packet rate average = 0.0317969
	minimum = 0.008 (at node 144)
	maximum = 0.053 (at node 85)
Accepted packet rate average = 0.0122135
	minimum = 0.005 (at node 176)
	maximum = 0.022 (at node 1)
Injected flit rate average = 0.571677
	minimum = 0.151 (at node 144)
	maximum = 0.954 (at node 85)
Accepted flit rate average= 0.21962
	minimum = 0.075 (at node 176)
	maximum = 0.406 (at node 152)
Injected packet length average = 17.979
Accepted packet length average = 17.9817
Total in-flight flits = 226399 (0 measured)
latency change    = 0.605146
throughput change = 0.0583632
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 746.817
	minimum = 31
	maximum = 2000
Network latency average = 86.1069
	minimum = 29
	maximum = 853
Slowest packet = 19894
Flit latency average = 2115.38
	minimum = 6
	maximum = 3865
Slowest flit = 14689
Fragmentation average = 14.6489
	minimum = 4
	maximum = 250
Injected packet rate average = 0.0299896
	minimum = 0 (at node 116)
	maximum = 0.05 (at node 17)
Accepted packet rate average = 0.0119323
	minimum = 0.005 (at node 122)
	maximum = 0.021 (at node 114)
Injected flit rate average = 0.540073
	minimum = 0 (at node 116)
	maximum = 0.889 (at node 17)
Accepted flit rate average= 0.214078
	minimum = 0.083 (at node 122)
	maximum = 0.366 (at node 114)
Injected packet length average = 18.0087
Accepted packet length average = 17.9411
Total in-flight flits = 288922 (101236 measured)
latency change    = 1.06535
throughput change = 0.0258862
Class 0:
Packet latency average = 1108.89
	minimum = 28
	maximum = 3282
Network latency average = 422
	minimum = 28
	maximum = 1918
Slowest packet = 19894
Flit latency average = 2417.45
	minimum = 6
	maximum = 4671
Slowest flit = 40308
Fragmentation average = 25.1903
	minimum = 3
	maximum = 263
Injected packet rate average = 0.0253516
	minimum = 0 (at node 116)
	maximum = 0.0405 (at node 23)
Accepted packet rate average = 0.0117708
	minimum = 0.0065 (at node 4)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.455906
	minimum = 0.0025 (at node 116)
	maximum = 0.7325 (at node 23)
Accepted flit rate average= 0.211141
	minimum = 0.1135 (at node 122)
	maximum = 0.3145 (at node 99)
Injected packet length average = 17.9834
Accepted packet length average = 17.9376
Total in-flight flits = 320605 (169507 measured)
latency change    = 0.326517
throughput change = 0.0139125
Class 0:
Packet latency average = 1875.92
	minimum = 28
	maximum = 4248
Network latency average = 1014.12
	minimum = 24
	maximum = 2856
Slowest packet = 19894
Flit latency average = 2705.51
	minimum = 6
	maximum = 5559
Slowest flit = 48725
Fragmentation average = 55.7844
	minimum = 1
	maximum = 851
Injected packet rate average = 0.0221198
	minimum = 0 (at node 116)
	maximum = 0.0326667 (at node 33)
Accepted packet rate average = 0.0118108
	minimum = 0.007 (at node 122)
	maximum = 0.0176667 (at node 99)
Injected flit rate average = 0.397937
	minimum = 0.00166667 (at node 116)
	maximum = 0.592667 (at node 33)
Accepted flit rate average= 0.211944
	minimum = 0.120667 (at node 122)
	maximum = 0.317333 (at node 99)
Injected packet length average = 17.9901
Accepted packet length average = 17.945
Total in-flight flits = 333837 (216443 measured)
latency change    = 0.408885
throughput change = 0.0037926
Class 0:
Packet latency average = 2588.88
	minimum = 28
	maximum = 5346
Network latency average = 1608.03
	minimum = 24
	maximum = 3909
Slowest packet = 19894
Flit latency average = 2991.76
	minimum = 6
	maximum = 6691
Slowest flit = 37496
Fragmentation average = 90.4314
	minimum = 1
	maximum = 1617
Injected packet rate average = 0.0203255
	minimum = 0.00275 (at node 116)
	maximum = 0.03275 (at node 155)
Accepted packet rate average = 0.0117878
	minimum = 0.0075 (at node 4)
	maximum = 0.01675 (at node 99)
Injected flit rate average = 0.365759
	minimum = 0.0515 (at node 116)
	maximum = 0.5895 (at node 155)
Accepted flit rate average= 0.210815
	minimum = 0.14075 (at node 4)
	maximum = 0.3 (at node 99)
Injected packet length average = 17.9951
Accepted packet length average = 17.8842
Total in-flight flits = 345761 (257520 measured)
latency change    = 0.275393
throughput change = 0.00535702
Class 0:
Packet latency average = 3240.93
	minimum = 28
	maximum = 6096
Network latency average = 2211.7
	minimum = 24
	maximum = 4957
Slowest packet = 19894
Flit latency average = 3271.26
	minimum = 6
	maximum = 7449
Slowest flit = 47591
Fragmentation average = 113.773
	minimum = 0
	maximum = 1617
Injected packet rate average = 0.0190937
	minimum = 0.0076 (at node 116)
	maximum = 0.0274 (at node 33)
Accepted packet rate average = 0.0116906
	minimum = 0.0086 (at node 2)
	maximum = 0.0154 (at node 99)
Injected flit rate average = 0.343761
	minimum = 0.139 (at node 116)
	maximum = 0.4948 (at node 33)
Accepted flit rate average= 0.209894
	minimum = 0.151 (at node 2)
	maximum = 0.2752 (at node 81)
Injected packet length average = 18.0039
Accepted packet length average = 17.954
Total in-flight flits = 355471 (290740 measured)
latency change    = 0.201192
throughput change = 0.00438962
Class 0:
Packet latency average = 3854.97
	minimum = 28
	maximum = 7185
Network latency average = 2788.73
	minimum = 24
	maximum = 5959
Slowest packet = 19894
Flit latency average = 3550.81
	minimum = 6
	maximum = 8241
Slowest flit = 74771
Fragmentation average = 130.381
	minimum = 0
	maximum = 1617
Injected packet rate average = 0.0180347
	minimum = 0.01 (at node 32)
	maximum = 0.0286667 (at node 78)
Accepted packet rate average = 0.0116467
	minimum = 0.0085 (at node 36)
	maximum = 0.0151667 (at node 16)
Injected flit rate average = 0.324753
	minimum = 0.180167 (at node 32)
	maximum = 0.514333 (at node 78)
Accepted flit rate average= 0.209026
	minimum = 0.154167 (at node 36)
	maximum = 0.280833 (at node 123)
Injected packet length average = 18.0071
Accepted packet length average = 17.9472
Total in-flight flits = 360523 (314719 measured)
latency change    = 0.159284
throughput change = 0.0041512
Class 0:
Packet latency average = 4438
	minimum = 28
	maximum = 8315
Network latency average = 3329.64
	minimum = 24
	maximum = 6924
Slowest packet = 19894
Flit latency average = 3814.06
	minimum = 6
	maximum = 9173
Slowest flit = 105196
Fragmentation average = 141.493
	minimum = 0
	maximum = 1896
Injected packet rate average = 0.0171912
	minimum = 0.00985714 (at node 159)
	maximum = 0.0255714 (at node 78)
Accepted packet rate average = 0.011567
	minimum = 0.00842857 (at node 52)
	maximum = 0.0151429 (at node 123)
Injected flit rate average = 0.3094
	minimum = 0.177429 (at node 159)
	maximum = 0.459857 (at node 78)
Accepted flit rate average= 0.207129
	minimum = 0.152 (at node 92)
	maximum = 0.273857 (at node 128)
Injected packet length average = 17.9976
Accepted packet length average = 17.9069
Total in-flight flits = 364952 (332651 measured)
latency change    = 0.131373
throughput change = 0.00916011
Draining all recorded packets ...
Class 0:
Remaining flits: 58679 76050 76051 76052 76053 76054 76055 76056 76057 76058 [...] (362810 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (341419 flits)
Class 0:
Remaining flits: 58679 76050 76051 76052 76053 76054 76055 76056 76057 76058 [...] (362619 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (348641 flits)
Class 0:
Remaining flits: 76058 76059 76060 76061 76062 76063 76064 76065 76066 76067 [...] (358953 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (350075 flits)
Class 0:
Remaining flits: 136359 136360 136361 136362 136363 136364 136365 136366 136367 143717 [...] (356112 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (350270 flits)
Class 0:
Remaining flits: 179460 179461 179462 179463 179464 179465 179466 179467 179468 179469 [...] (354776 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (351010 flits)
Class 0:
Remaining flits: 179464 179465 179466 179467 179468 179469 179470 179471 179472 179473 [...] (352047 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (349294 flits)
Class 0:
Remaining flits: 179464 179465 179466 179467 179468 179469 179470 179471 179472 179473 [...] (355873 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (353240 flits)
Class 0:
Remaining flits: 179474 179475 179476 179477 227988 227989 227990 227991 227992 227993 [...] (353004 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (348848 flits)
Class 0:
Remaining flits: 268385 268386 268387 268388 268389 268390 268391 268392 268393 268394 [...] (349126 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (343536 flits)
Class 0:
Remaining flits: 268385 268386 268387 268388 268389 268390 268391 268392 268393 268394 [...] (342653 flits)
Measured flits: 357876 357877 357878 357879 357880 357881 357882 357883 357884 357885 [...] (333129 flits)
Class 0:
Remaining flits: 268385 268386 268387 268388 268389 268390 268391 268392 268393 268394 [...] (342496 flits)
Measured flits: 360065 360066 360067 360068 360069 360070 360071 371844 371845 371846 [...] (326656 flits)
Class 0:
Remaining flits: 293742 293743 293744 293745 293746 293747 293748 293749 293750 293751 [...] (339707 flits)
Measured flits: 371844 371845 371846 371847 371848 371849 371850 371851 371852 371853 [...] (314757 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (337178 flits)
Measured flits: 392724 392725 392726 392727 392728 392729 392730 392731 392732 392733 [...] (301075 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (334015 flits)
Measured flits: 392724 392725 392726 392727 392728 392729 392730 392731 392732 392733 [...] (283370 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (334803 flits)
Measured flits: 392724 392725 392726 392727 392728 392729 392730 392731 392732 392733 [...] (264397 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (333576 flits)
Measured flits: 392724 392725 392726 392727 392728 392729 392730 392731 392732 392733 [...] (242630 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (334019 flits)
Measured flits: 402858 402859 402860 402861 402862 402863 402864 402865 402866 402867 [...] (220453 flits)
Class 0:
Remaining flits: 327744 327745 327746 327747 327748 327749 327750 327751 327752 327753 [...] (333566 flits)
Measured flits: 402858 402859 402860 402861 402862 402863 402864 402865 402866 402867 [...] (199031 flits)
Class 0:
Remaining flits: 327746 327747 327748 327749 327750 327751 327752 327753 327754 327755 [...] (333715 flits)
Measured flits: 402858 402859 402860 402861 402862 402863 402864 402865 402866 402867 [...] (177204 flits)
Class 0:
Remaining flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (334519 flits)
Measured flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (155532 flits)
Class 0:
Remaining flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (332215 flits)
Measured flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (133322 flits)
Class 0:
Remaining flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (329684 flits)
Measured flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (113720 flits)
Class 0:
Remaining flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (331145 flits)
Measured flits: 478692 478693 478694 478695 478696 478697 478698 478699 478700 478701 [...] (94680 flits)
Class 0:
Remaining flits: 504774 504775 504776 504777 504778 504779 504780 504781 504782 504783 [...] (332060 flits)
Measured flits: 504774 504775 504776 504777 504778 504779 504780 504781 504782 504783 [...] (78901 flits)
Class 0:
Remaining flits: 504774 504775 504776 504777 504778 504779 504780 504781 504782 504783 [...] (333303 flits)
Measured flits: 504774 504775 504776 504777 504778 504779 504780 504781 504782 504783 [...] (65038 flits)
Class 0:
Remaining flits: 504780 504781 504782 504783 504784 504785 504786 504787 504788 504789 [...] (329510 flits)
Measured flits: 504780 504781 504782 504783 504784 504785 504786 504787 504788 504789 [...] (53044 flits)
Class 0:
Remaining flits: 616176 616177 616178 616179 616180 616181 616182 616183 616184 616185 [...] (330815 flits)
Measured flits: 616176 616177 616178 616179 616180 616181 616182 616183 616184 616185 [...] (42915 flits)
Class 0:
Remaining flits: 708187 708188 708189 708190 708191 725796 725797 725798 725799 725800 [...] (331378 flits)
Measured flits: 708187 708188 708189 708190 708191 725796 725797 725798 725799 725800 [...] (33491 flits)
Class 0:
Remaining flits: 708189 708190 708191 725796 725797 725798 725799 725800 725801 725802 [...] (328555 flits)
Measured flits: 708189 708190 708191 725796 725797 725798 725799 725800 725801 725802 [...] (26967 flits)
Class 0:
Remaining flits: 725813 726336 726337 726338 726339 726340 726341 726342 726343 726344 [...] (327158 flits)
Measured flits: 725813 726336 726337 726338 726339 726340 726341 726342 726343 726344 [...] (21976 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (327593 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (17205 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (326041 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (14101 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (324760 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (11361 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (321747 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (8719 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (321245 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (6233 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (320023 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (4551 flits)
Class 0:
Remaining flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (318407 flits)
Measured flits: 752832 752833 752834 752835 752836 752837 752838 752839 752840 752841 [...] (3444 flits)
Class 0:
Remaining flits: 1015200 1015201 1015202 1015203 1015204 1015205 1015206 1015207 1015208 1015209 [...] (320100 flits)
Measured flits: 1015200 1015201 1015202 1015203 1015204 1015205 1015206 1015207 1015208 1015209 [...] (2483 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (320829 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (1820 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (322975 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (1297 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (323847 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (930 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (321057 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (706 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (318908 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (600 flits)
Class 0:
Remaining flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (312849 flits)
Measured flits: 1056276 1056277 1056278 1056279 1056280 1056281 1056282 1056283 1056284 1056285 [...] (472 flits)
Class 0:
Remaining flits: 1056280 1056281 1056282 1056283 1056284 1056285 1056286 1056287 1056288 1056289 [...] (312572 flits)
Measured flits: 1056280 1056281 1056282 1056283 1056284 1056285 1056286 1056287 1056288 1056289 [...] (341 flits)
Class 0:
Remaining flits: 1236114 1236115 1236116 1236117 1236118 1236119 1236120 1236121 1236122 1236123 [...] (307763 flits)
Measured flits: 1236114 1236115 1236116 1236117 1236118 1236119 1236120 1236121 1236122 1236123 [...] (184 flits)
Class 0:
Remaining flits: 1237356 1237357 1237358 1237359 1237360 1237361 1237362 1237363 1237364 1237365 [...] (308986 flits)
Measured flits: 1237356 1237357 1237358 1237359 1237360 1237361 1237362 1237363 1237364 1237365 [...] (127 flits)
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (308703 flits)
Measured flits: 1958076 1958077 1958078 1958079 1958080 1958081 1958082 1958083 1958084 1958085 [...] (18 flits)
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (308923 flits)
Measured flits: 1958076 1958077 1958078 1958079 1958080 1958081 1958082 1958083 1958084 1958085 [...] (18 flits)
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (308926 flits)
Measured flits: 1958076 1958077 1958078 1958079 1958080 1958081 1958082 1958083 1958084 1958085 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (274593 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (239821 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329912 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 [...] (206156 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1329913 1329914 1329915 1329916 1329917 1329918 1329919 1329920 1329921 1329922 [...] (173223 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492377 1492378 1492379 1560006 1560007 1560008 1560009 1560010 1560011 1560012 [...] (141121 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492377 1492378 1492379 1560006 1560007 1560008 1560009 1560010 1560011 1560012 [...] (109839 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1601334 1601335 1601336 1601337 1601338 1601339 1601340 1601341 1601342 1601343 [...] (81495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1601334 1601335 1601336 1601337 1601338 1601339 1601340 1601341 1601342 1601343 [...] (56552 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692882 1692883 1692884 1692885 1692886 1692887 1692888 1692889 1692890 1692891 [...] (35919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692882 1692883 1692884 1692885 1692886 1692887 1692888 1692889 1692890 1692891 [...] (19324 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1867770 1867771 1867772 1867773 1867774 1867775 1867776 1867777 1867778 1867779 [...] (9041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1929402 1929403 1929404 1929405 1929406 1929407 1929408 1929409 1929410 1929411 [...] (4032 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1929403 1929404 1929405 1929406 1929407 1929408 1929409 1929410 1929411 1929412 [...] (1534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2373318 2373319 2373320 2373321 2373322 2373323 2373324 2373325 2373326 2373327 [...] (171 flits)
Measured flits: (0 flits)
Time taken is 75115 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15679.3 (1 samples)
	minimum = 28 (1 samples)
	maximum = 51514 (1 samples)
Network latency average = 8896.95 (1 samples)
	minimum = 23 (1 samples)
	maximum = 38545 (1 samples)
Flit latency average = 8451.46 (1 samples)
	minimum = 6 (1 samples)
	maximum = 39287 (1 samples)
Fragmentation average = 206.613 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6009 (1 samples)
Injected packet rate average = 0.0171912 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0255714 (1 samples)
Accepted packet rate average = 0.011567 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0151429 (1 samples)
Injected flit rate average = 0.3094 (1 samples)
	minimum = 0.177429 (1 samples)
	maximum = 0.459857 (1 samples)
Accepted flit rate average = 0.207129 (1 samples)
	minimum = 0.152 (1 samples)
	maximum = 0.273857 (1 samples)
Injected packet size average = 17.9976 (1 samples)
Accepted packet size average = 17.9069 (1 samples)
Hops average = 5.04718 (1 samples)
Total run time 96.7828
