BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 297.421
	minimum = 22
	maximum = 813
Network latency average = 284.703
	minimum = 22
	maximum = 791
Slowest packet = 1061
Flit latency average = 260.898
	minimum = 5
	maximum = 774
Slowest flit = 19098
Fragmentation average = 24.04
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0348906
	minimum = 0.02 (at node 21)
	maximum = 0.05 (at node 175)
Accepted packet rate average = 0.0145833
	minimum = 0.008 (at node 28)
	maximum = 0.024 (at node 140)
Injected flit rate average = 0.62274
	minimum = 0.36 (at node 21)
	maximum = 0.888 (at node 175)
Accepted flit rate average= 0.269026
	minimum = 0.144 (at node 174)
	maximum = 0.432 (at node 140)
Injected packet length average = 17.8483
Accepted packet length average = 18.4475
Total in-flight flits = 68983 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 560.916
	minimum = 22
	maximum = 1519
Network latency average = 544.805
	minimum = 22
	maximum = 1455
Slowest packet = 2599
Flit latency average = 521.136
	minimum = 5
	maximum = 1514
Slowest flit = 56988
Fragmentation average = 24.8479
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0331276
	minimum = 0.021 (at node 4)
	maximum = 0.0435 (at node 107)
Accepted packet rate average = 0.015099
	minimum = 0.009 (at node 96)
	maximum = 0.021 (at node 71)
Injected flit rate average = 0.593646
	minimum = 0.378 (at node 4)
	maximum = 0.783 (at node 107)
Accepted flit rate average= 0.274758
	minimum = 0.168 (at node 96)
	maximum = 0.3865 (at node 71)
Injected packet length average = 17.92
Accepted packet length average = 18.1971
Total in-flight flits = 124155 (0 measured)
latency change    = 0.469757
throughput change = 0.0208612
Class 0:
Packet latency average = 1395.68
	minimum = 22
	maximum = 2401
Network latency average = 1358.65
	minimum = 22
	maximum = 2401
Slowest packet = 3835
Flit latency average = 1339.96
	minimum = 5
	maximum = 2384
Slowest flit = 69047
Fragmentation average = 21.476
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0208385
	minimum = 0.003 (at node 56)
	maximum = 0.044 (at node 42)
Accepted packet rate average = 0.0151094
	minimum = 0.004 (at node 184)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.376891
	minimum = 0.054 (at node 56)
	maximum = 0.801 (at node 42)
Accepted flit rate average= 0.271802
	minimum = 0.072 (at node 184)
	maximum = 0.498 (at node 34)
Injected packet length average = 18.0862
Accepted packet length average = 17.989
Total in-flight flits = 145193 (0 measured)
latency change    = 0.598106
throughput change = 0.0108746
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 973.341
	minimum = 22
	maximum = 2411
Network latency average = 106.683
	minimum = 22
	maximum = 813
Slowest packet = 16913
Flit latency average = 1826.63
	minimum = 5
	maximum = 3116
Slowest flit = 105401
Fragmentation average = 5.52033
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0161667
	minimum = 0.003 (at node 4)
	maximum = 0.036 (at node 128)
Accepted packet rate average = 0.0150885
	minimum = 0.007 (at node 155)
	maximum = 0.027 (at node 41)
Injected flit rate average = 0.291807
	minimum = 0.039 (at node 4)
	maximum = 0.638 (at node 128)
Accepted flit rate average= 0.270807
	minimum = 0.125 (at node 155)
	maximum = 0.486 (at node 41)
Injected packet length average = 18.0499
Accepted packet length average = 17.9479
Total in-flight flits = 149484 (54037 measured)
latency change    = 0.433907
throughput change = 0.00367343
Class 0:
Packet latency average = 1842.63
	minimum = 22
	maximum = 3554
Network latency average = 774.628
	minimum = 22
	maximum = 1972
Slowest packet = 16913
Flit latency average = 2052.92
	minimum = 5
	maximum = 3680
Slowest flit = 153539
Fragmentation average = 10.8184
	minimum = 0
	maximum = 62
Injected packet rate average = 0.0153854
	minimum = 0.0035 (at node 121)
	maximum = 0.032 (at node 125)
Accepted packet rate average = 0.0149271
	minimum = 0.0085 (at node 31)
	maximum = 0.0215 (at node 118)
Injected flit rate average = 0.276966
	minimum = 0.063 (at node 121)
	maximum = 0.575 (at node 125)
Accepted flit rate average= 0.268242
	minimum = 0.159 (at node 31)
	maximum = 0.3845 (at node 118)
Injected packet length average = 18.0019
Accepted packet length average = 17.9702
Total in-flight flits = 148568 (98117 measured)
latency change    = 0.471764
throughput change = 0.00956264
Class 0:
Packet latency average = 2576.24
	minimum = 22
	maximum = 4371
Network latency average = 1420.37
	minimum = 22
	maximum = 2947
Slowest packet = 16913
Flit latency average = 2235.17
	minimum = 5
	maximum = 4403
Slowest flit = 186803
Fragmentation average = 14.4061
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0154757
	minimum = 0.00466667 (at node 91)
	maximum = 0.0296667 (at node 125)
Accepted packet rate average = 0.0148385
	minimum = 0.01 (at node 135)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.27862
	minimum = 0.084 (at node 91)
	maximum = 0.531333 (at node 125)
Accepted flit rate average= 0.266601
	minimum = 0.18 (at node 162)
	maximum = 0.374333 (at node 128)
Injected packet length average = 18.0037
Accepted packet length average = 17.9668
Total in-flight flits = 152443 (134361 measured)
latency change    = 0.284761
throughput change = 0.00615712
Draining remaining packets ...
Class 0:
Remaining flits: 199170 199171 199172 199173 199174 199175 199176 199177 199178 199179 [...] (102974 flits)
Measured flits: 302904 302905 302906 302907 302908 302909 302910 302911 302912 302913 [...] (99584 flits)
Class 0:
Remaining flits: 258498 258499 258500 258501 258502 258503 258504 258505 258506 258507 [...] (54169 flits)
Measured flits: 310068 310069 310070 310071 310072 310073 310074 310075 310076 310077 [...] (53801 flits)
Class 0:
Remaining flits: 290581 290582 290583 290584 290585 290586 290587 290588 290589 290590 [...] (8358 flits)
Measured flits: 328672 328673 328674 328675 328676 328677 328678 328679 350064 350065 [...] (8347 flits)
Time taken is 9899 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4122.73 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6969 (1 samples)
Network latency average = 2784.07 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5534 (1 samples)
Flit latency average = 2680.33 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6201 (1 samples)
Fragmentation average = 18.4588 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0154757 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0296667 (1 samples)
Accepted packet rate average = 0.0148385 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.27862 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.531333 (1 samples)
Accepted flit rate average = 0.266601 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.374333 (1 samples)
Injected packet size average = 18.0037 (1 samples)
Accepted packet size average = 17.9668 (1 samples)
Hops average = 5.0966 (1 samples)
Total run time 11.7444
