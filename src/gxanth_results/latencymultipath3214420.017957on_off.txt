BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.046
	minimum = 24
	maximum = 880
Network latency average = 163.819
	minimum = 22
	maximum = 727
Slowest packet = 21
Flit latency average = 136.43
	minimum = 5
	maximum = 747
Slowest flit = 14990
Fragmentation average = 28.6859
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0122552
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.227964
	minimum = 0.108 (at node 41)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6014
Total in-flight flits = 18241 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 357.96
	minimum = 22
	maximum = 1821
Network latency average = 276.63
	minimum = 22
	maximum = 1439
Slowest packet = 21
Flit latency average = 245.59
	minimum = 5
	maximum = 1422
Slowest flit = 26189
Fragmentation average = 35.5711
	minimum = 0
	maximum = 335
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0132187
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241724
	minimum = 0.153 (at node 116)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.2864
Total in-flight flits = 26770 (0 measured)
latency change    = 0.360136
throughput change = 0.0569262
Class 0:
Packet latency average = 628.204
	minimum = 22
	maximum = 2515
Network latency average = 526.7
	minimum = 22
	maximum = 2067
Slowest packet = 2278
Flit latency average = 492.106
	minimum = 5
	maximum = 2050
Slowest flit = 52991
Fragmentation average = 42.4846
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145208
	minimum = 0.004 (at node 118)
	maximum = 0.025 (at node 57)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.26126
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9921
Total in-flight flits = 38978 (0 measured)
latency change    = 0.430185
throughput change = 0.0747777
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 414.67
	minimum = 25
	maximum = 1431
Network latency average = 316.29
	minimum = 22
	maximum = 974
Slowest packet = 10126
Flit latency average = 642.327
	minimum = 5
	maximum = 2529
Slowest flit = 72107
Fragmentation average = 26.1359
	minimum = 0
	maximum = 213
Injected packet rate average = 0.0184219
	minimum = 0 (at node 20)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148229
	minimum = 0.006 (at node 43)
	maximum = 0.027 (at node 103)
Injected flit rate average = 0.331411
	minimum = 0 (at node 20)
	maximum = 0.989 (at node 23)
Accepted flit rate average= 0.268182
	minimum = 0.108 (at node 43)
	maximum = 0.514 (at node 103)
Injected packet length average = 17.9901
Accepted packet length average = 18.0924
Total in-flight flits = 51171 (43263 measured)
latency change    = 0.51495
throughput change = 0.0258103
Class 0:
Packet latency average = 699.263
	minimum = 25
	maximum = 2161
Network latency average = 593.587
	minimum = 22
	maximum = 1966
Slowest packet = 10126
Flit latency average = 730.812
	minimum = 5
	maximum = 2762
Slowest flit = 81017
Fragmentation average = 39.3935
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0183385
	minimum = 0.003 (at node 136)
	maximum = 0.042 (at node 111)
Accepted packet rate average = 0.0148229
	minimum = 0.0085 (at node 43)
	maximum = 0.023 (at node 178)
Injected flit rate average = 0.330224
	minimum = 0.0505 (at node 184)
	maximum = 0.763 (at node 111)
Accepted flit rate average= 0.268224
	minimum = 0.153 (at node 43)
	maximum = 0.4135 (at node 178)
Injected packet length average = 18.0071
Accepted packet length average = 18.0952
Total in-flight flits = 62736 (61675 measured)
latency change    = 0.40699
throughput change = 0.000155343
Class 0:
Packet latency average = 888.837
	minimum = 22
	maximum = 3399
Network latency average = 782.826
	minimum = 22
	maximum = 2828
Slowest packet = 10126
Flit latency average = 828.303
	minimum = 5
	maximum = 3213
Slowest flit = 162017
Fragmentation average = 44.8901
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0177674
	minimum = 0.00266667 (at node 136)
	maximum = 0.0376667 (at node 34)
Accepted packet rate average = 0.0147882
	minimum = 0.01 (at node 36)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.320111
	minimum = 0.048 (at node 136)
	maximum = 0.678 (at node 34)
Accepted flit rate average= 0.267257
	minimum = 0.18 (at node 36)
	maximum = 0.384 (at node 129)
Injected packet length average = 18.0168
Accepted packet length average = 18.0723
Total in-flight flits = 69250 (69160 measured)
latency change    = 0.213283
throughput change = 0.00361829
Class 0:
Packet latency average = 1045.25
	minimum = 22
	maximum = 4223
Network latency average = 939.095
	minimum = 22
	maximum = 3881
Slowest packet = 10274
Flit latency average = 931.023
	minimum = 5
	maximum = 3864
Slowest flit = 188999
Fragmentation average = 46.6551
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0174323
	minimum = 0.00425 (at node 105)
	maximum = 0.03325 (at node 22)
Accepted packet rate average = 0.0148125
	minimum = 0.01075 (at node 144)
	maximum = 0.01975 (at node 129)
Injected flit rate average = 0.314033
	minimum = 0.0765 (at node 105)
	maximum = 0.59825 (at node 22)
Accepted flit rate average= 0.266408
	minimum = 0.193 (at node 144)
	maximum = 0.35325 (at node 129)
Injected packet length average = 18.0144
Accepted packet length average = 17.9853
Total in-flight flits = 75379 (75379 measured)
latency change    = 0.149644
throughput change = 0.00318832
Class 0:
Packet latency average = 1138.85
	minimum = 22
	maximum = 4739
Network latency average = 1035.09
	minimum = 22
	maximum = 4073
Slowest packet = 10274
Flit latency average = 1006.46
	minimum = 5
	maximum = 4056
Slowest flit = 193174
Fragmentation average = 46.3957
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0174573
	minimum = 0.0058 (at node 75)
	maximum = 0.034 (at node 111)
Accepted packet rate average = 0.0148573
	minimum = 0.011 (at node 57)
	maximum = 0.0198 (at node 128)
Injected flit rate average = 0.314215
	minimum = 0.1044 (at node 105)
	maximum = 0.6154 (at node 111)
Accepted flit rate average= 0.267572
	minimum = 0.1988 (at node 57)
	maximum = 0.3562 (at node 128)
Injected packet length average = 17.999
Accepted packet length average = 18.0095
Total in-flight flits = 83789 (83789 measured)
latency change    = 0.0821837
throughput change = 0.00435144
Class 0:
Packet latency average = 1212.37
	minimum = 22
	maximum = 4999
Network latency average = 1111.74
	minimum = 22
	maximum = 4715
Slowest packet = 10274
Flit latency average = 1071.8
	minimum = 5
	maximum = 5329
Slowest flit = 222035
Fragmentation average = 45.8649
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0176319
	minimum = 0.00666667 (at node 75)
	maximum = 0.0295 (at node 74)
Accepted packet rate average = 0.0149392
	minimum = 0.0116667 (at node 79)
	maximum = 0.0193333 (at node 128)
Injected flit rate average = 0.317412
	minimum = 0.120333 (at node 75)
	maximum = 0.533333 (at node 74)
Accepted flit rate average= 0.268968
	minimum = 0.2085 (at node 80)
	maximum = 0.348 (at node 128)
Injected packet length average = 18.0021
Accepted packet length average = 18.0041
Total in-flight flits = 94743 (94743 measured)
latency change    = 0.0606408
throughput change = 0.00519024
Class 0:
Packet latency average = 1285.2
	minimum = 22
	maximum = 5720
Network latency average = 1183.49
	minimum = 22
	maximum = 5411
Slowest packet = 12335
Flit latency average = 1135.86
	minimum = 5
	maximum = 5394
Slowest flit = 222047
Fragmentation average = 46.4662
	minimum = 0
	maximum = 465
Injected packet rate average = 0.0178118
	minimum = 0.00785714 (at node 175)
	maximum = 0.0287143 (at node 149)
Accepted packet rate average = 0.0149717
	minimum = 0.0118571 (at node 80)
	maximum = 0.0185714 (at node 128)
Injected flit rate average = 0.320653
	minimum = 0.142286 (at node 175)
	maximum = 0.516857 (at node 149)
Accepted flit rate average= 0.269589
	minimum = 0.212571 (at node 171)
	maximum = 0.333571 (at node 128)
Injected packet length average = 18.0023
Accepted packet length average = 18.0066
Total in-flight flits = 107552 (107552 measured)
latency change    = 0.0566721
throughput change = 0.002305
Draining all recorded packets ...
Class 0:
Remaining flits: 314010 314011 314012 314013 314014 314015 314016 314017 314018 314019 [...] (119596 flits)
Measured flits: 314010 314011 314012 314013 314014 314015 314016 314017 314018 314019 [...] (67158 flits)
Class 0:
Remaining flits: 322411 322412 322413 322414 322415 330104 330105 330106 330107 330108 [...] (131887 flits)
Measured flits: 322411 322412 322413 322414 322415 330104 330105 330106 330107 330108 [...] (29304 flits)
Class 0:
Remaining flits: 435888 435889 435890 435891 435892 435893 435894 435895 435896 435897 [...] (139768 flits)
Measured flits: 435888 435889 435890 435891 435892 435893 435894 435895 435896 435897 [...] (7896 flits)
Class 0:
Remaining flits: 531201 531202 531203 531204 531205 531206 531207 531208 531209 531210 [...] (152037 flits)
Measured flits: 531201 531202 531203 531204 531205 531206 531207 531208 531209 531210 [...] (2371 flits)
Class 0:
Remaining flits: 537822 537823 537824 537825 537826 537827 537828 537829 537830 537831 [...] (163862 flits)
Measured flits: 537822 537823 537824 537825 537826 537827 537828 537829 537830 537831 [...] (674 flits)
Class 0:
Remaining flits: 589356 589357 589358 589359 589360 589361 589362 589363 589364 589365 [...] (173741 flits)
Measured flits: 589356 589357 589358 589359 589360 589361 589362 589363 589364 589365 [...] (125 flits)
Class 0:
Remaining flits: 589356 589357 589358 589359 589360 589361 589362 589363 589364 589365 [...] (181016 flits)
Measured flits: 589356 589357 589358 589359 589360 589361 589362 589363 589364 589365 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 630787 630788 630789 630790 630791 654786 654787 654788 654789 654790 [...] (135799 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 689616 689617 689618 689619 689620 689621 689622 689623 689624 689625 [...] (89021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 693126 693127 693128 693129 693130 693131 693132 693133 693134 693135 [...] (46139 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 753966 753967 753968 753969 753970 753971 753972 753973 753974 753975 [...] (17205 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 837486 837487 837488 837489 837490 837491 837492 837493 837494 837495 [...] (3894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 976644 976645 976646 976647 976648 976649 976650 976651 976652 976653 [...] (108 flits)
Measured flits: (0 flits)
Time taken is 23380 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1618.37 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7601 (1 samples)
Network latency average = 1509.1 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7601 (1 samples)
Flit latency average = 2129.31 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9670 (1 samples)
Fragmentation average = 48.2636 (1 samples)
	minimum = 0 (1 samples)
	maximum = 619 (1 samples)
Injected packet rate average = 0.0178118 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.0287143 (1 samples)
Accepted packet rate average = 0.0149717 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.320653 (1 samples)
	minimum = 0.142286 (1 samples)
	maximum = 0.516857 (1 samples)
Accepted flit rate average = 0.269589 (1 samples)
	minimum = 0.212571 (1 samples)
	maximum = 0.333571 (1 samples)
Injected packet size average = 18.0023 (1 samples)
Accepted packet size average = 18.0066 (1 samples)
Hops average = 5.07801 (1 samples)
Total run time 18.8142
