BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.301
	minimum = 23
	maximum = 993
Network latency average = 268.661
	minimum = 23
	maximum = 940
Slowest packet = 169
Flit latency average = 201.724
	minimum = 6
	maximum = 961
Slowest flit = 2860
Fragmentation average = 140.914
	minimum = 0
	maximum = 789
Injected packet rate average = 0.0243177
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.00994271
	minimum = 0.004 (at node 59)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.433458
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.203448
	minimum = 0.09 (at node 93)
	maximum = 0.342 (at node 152)
Injected packet length average = 17.8248
Accepted packet length average = 20.462
Total in-flight flits = 44980 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 582.876
	minimum = 23
	maximum = 1951
Network latency average = 472.508
	minimum = 23
	maximum = 1822
Slowest packet = 169
Flit latency average = 396.78
	minimum = 6
	maximum = 1949
Slowest flit = 3308
Fragmentation average = 177.964
	minimum = 0
	maximum = 1389
Injected packet rate average = 0.0243776
	minimum = 0.0025 (at node 141)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0111641
	minimum = 0.006 (at node 164)
	maximum = 0.0175 (at node 44)
Injected flit rate average = 0.436568
	minimum = 0.045 (at node 141)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.214917
	minimum = 0.113 (at node 174)
	maximum = 0.334 (at node 44)
Injected packet length average = 17.9086
Accepted packet length average = 19.2508
Total in-flight flits = 85970 (0 measured)
latency change    = 0.393866
throughput change = 0.0533637
Class 0:
Packet latency average = 1224.05
	minimum = 30
	maximum = 2967
Network latency average = 1066.78
	minimum = 25
	maximum = 2811
Slowest packet = 429
Flit latency average = 991.514
	minimum = 6
	maximum = 2809
Slowest flit = 11482
Fragmentation average = 229.224
	minimum = 0
	maximum = 2230
Injected packet rate average = 0.0244635
	minimum = 0 (at node 56)
	maximum = 0.056 (at node 86)
Accepted packet rate average = 0.0125313
	minimum = 0.005 (at node 3)
	maximum = 0.021 (at node 110)
Injected flit rate average = 0.440313
	minimum = 0 (at node 56)
	maximum = 1 (at node 27)
Accepted flit rate average= 0.223057
	minimum = 0.101 (at node 100)
	maximum = 0.375 (at node 27)
Injected packet length average = 17.9987
Accepted packet length average = 17.8001
Total in-flight flits = 127689 (0 measured)
latency change    = 0.523816
throughput change = 0.0364957
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 373.936
	minimum = 27
	maximum = 1917
Network latency average = 195.514
	minimum = 27
	maximum = 916
Slowest packet = 14146
Flit latency average = 1398.33
	minimum = 6
	maximum = 3766
Slowest flit = 16919
Fragmentation average = 59.3995
	minimum = 0
	maximum = 391
Injected packet rate average = 0.0253281
	minimum = 0 (at node 34)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0122604
	minimum = 0.003 (at node 112)
	maximum = 0.02 (at node 11)
Injected flit rate average = 0.45626
	minimum = 0 (at node 34)
	maximum = 1 (at node 18)
Accepted flit rate average= 0.221792
	minimum = 0.069 (at node 74)
	maximum = 0.348 (at node 59)
Injected packet length average = 18.014
Accepted packet length average = 18.0901
Total in-flight flits = 172639 (79757 measured)
latency change    = 2.27343
throughput change = 0.00570637
Class 0:
Packet latency average = 751.81
	minimum = 23
	maximum = 2767
Network latency average = 556.477
	minimum = 23
	maximum = 1947
Slowest packet = 14146
Flit latency average = 1638.12
	minimum = 6
	maximum = 4735
Slowest flit = 20946
Fragmentation average = 101.842
	minimum = 0
	maximum = 1261
Injected packet rate average = 0.023737
	minimum = 0 (at node 37)
	maximum = 0.054 (at node 53)
Accepted packet rate average = 0.0121328
	minimum = 0.006 (at node 39)
	maximum = 0.0185 (at node 151)
Injected flit rate average = 0.427607
	minimum = 0 (at node 37)
	maximum = 0.964 (at node 53)
Accepted flit rate average= 0.218487
	minimum = 0.1185 (at node 2)
	maximum = 0.3385 (at node 151)
Injected packet length average = 18.0144
Accepted packet length average = 18.0079
Total in-flight flits = 207914 (142622 measured)
latency change    = 0.502618
throughput change = 0.0151253
Class 0:
Packet latency average = 1248.2
	minimum = 23
	maximum = 4427
Network latency average = 1021.88
	minimum = 23
	maximum = 2956
Slowest packet = 14146
Flit latency average = 1892.7
	minimum = 6
	maximum = 5547
Slowest flit = 22643
Fragmentation average = 130.029
	minimum = 0
	maximum = 1605
Injected packet rate average = 0.0226545
	minimum = 0.00533333 (at node 148)
	maximum = 0.0443333 (at node 53)
Accepted packet rate average = 0.0119896
	minimum = 0.00733333 (at node 109)
	maximum = 0.0176667 (at node 151)
Injected flit rate average = 0.407573
	minimum = 0.096 (at node 148)
	maximum = 0.798 (at node 53)
Accepted flit rate average= 0.215818
	minimum = 0.137667 (at node 65)
	maximum = 0.318 (at node 151)
Injected packet length average = 17.9908
Accepted packet length average = 18.0004
Total in-flight flits = 238278 (193077 measured)
latency change    = 0.397684
throughput change = 0.0123682
Class 0:
Packet latency average = 1709.02
	minimum = 23
	maximum = 4972
Network latency average = 1450.32
	minimum = 23
	maximum = 3948
Slowest packet = 14146
Flit latency average = 2119.42
	minimum = 6
	maximum = 6661
Slowest flit = 26829
Fragmentation average = 145.613
	minimum = 0
	maximum = 2240
Injected packet rate average = 0.0218216
	minimum = 0.00675 (at node 19)
	maximum = 0.04 (at node 167)
Accepted packet rate average = 0.0119518
	minimum = 0.00775 (at node 36)
	maximum = 0.01675 (at node 151)
Injected flit rate average = 0.392504
	minimum = 0.12025 (at node 19)
	maximum = 0.7165 (at node 167)
Accepted flit rate average= 0.21415
	minimum = 0.13825 (at node 36)
	maximum = 0.303 (at node 151)
Injected packet length average = 17.9869
Accepted packet length average = 17.9177
Total in-flight flits = 265010 (233587 measured)
latency change    = 0.269643
throughput change = 0.0077888
Class 0:
Packet latency average = 2144.88
	minimum = 23
	maximum = 6034
Network latency average = 1849.35
	minimum = 23
	maximum = 4939
Slowest packet = 14146
Flit latency average = 2356.95
	minimum = 6
	maximum = 7329
Slowest flit = 54075
Fragmentation average = 154.859
	minimum = 0
	maximum = 2240
Injected packet rate average = 0.0213906
	minimum = 0.0076 (at node 7)
	maximum = 0.0354 (at node 167)
Accepted packet rate average = 0.0118125
	minimum = 0.0078 (at node 36)
	maximum = 0.0166 (at node 136)
Injected flit rate average = 0.384905
	minimum = 0.1368 (at node 7)
	maximum = 0.6366 (at node 167)
Accepted flit rate average= 0.211955
	minimum = 0.1374 (at node 36)
	maximum = 0.3022 (at node 136)
Injected packet length average = 17.9941
Accepted packet length average = 17.9433
Total in-flight flits = 294256 (273015 measured)
latency change    = 0.203206
throughput change = 0.0103538
Class 0:
Packet latency average = 2557.64
	minimum = 23
	maximum = 7002
Network latency average = 2229.88
	minimum = 23
	maximum = 5958
Slowest packet = 14146
Flit latency average = 2590.15
	minimum = 6
	maximum = 8648
Slowest flit = 28321
Fragmentation average = 160.769
	minimum = 0
	maximum = 2240
Injected packet rate average = 0.0205104
	minimum = 0.00966667 (at node 96)
	maximum = 0.0316667 (at node 161)
Accepted packet rate average = 0.0117005
	minimum = 0.0075 (at node 36)
	maximum = 0.0156667 (at node 25)
Injected flit rate average = 0.369163
	minimum = 0.174 (at node 96)
	maximum = 0.569667 (at node 161)
Accepted flit rate average= 0.20984
	minimum = 0.137167 (at node 36)
	maximum = 0.2795 (at node 25)
Injected packet length average = 17.9988
Accepted packet length average = 17.9343
Total in-flight flits = 311833 (297320 measured)
latency change    = 0.161386
throughput change = 0.0100788
Class 0:
Packet latency average = 2960.61
	minimum = 23
	maximum = 7779
Network latency average = 2602.21
	minimum = 23
	maximum = 6940
Slowest packet = 14146
Flit latency average = 2834.15
	minimum = 6
	maximum = 8813
Slowest flit = 99181
Fragmentation average = 165.421
	minimum = 0
	maximum = 3248
Injected packet rate average = 0.0198296
	minimum = 0.00942857 (at node 84)
	maximum = 0.0321429 (at node 167)
Accepted packet rate average = 0.0115878
	minimum = 0.008 (at node 36)
	maximum = 0.0148571 (at node 136)
Injected flit rate average = 0.356973
	minimum = 0.170286 (at node 84)
	maximum = 0.576429 (at node 167)
Accepted flit rate average= 0.207969
	minimum = 0.141857 (at node 36)
	maximum = 0.270143 (at node 136)
Injected packet length average = 18.002
Accepted packet length average = 17.9473
Total in-flight flits = 328832 (319235 measured)
latency change    = 0.136109
throughput change = 0.00899547
Draining all recorded packets ...
Class 0:
Remaining flits: 58374 58375 58376 58377 58378 58379 58380 58381 58382 58383 [...] (336447 flits)
Measured flits: 253098 253099 253100 253101 253102 253103 253104 253105 253106 253107 [...] (313563 flits)
Class 0:
Remaining flits: 58374 58375 58376 58377 58378 58379 58380 58381 58382 58383 [...] (341889 flits)
Measured flits: 253188 253189 253190 253191 253192 253193 253194 253195 253196 253197 [...] (301169 flits)
Class 0:
Remaining flits: 58389 58390 58391 116874 116875 116876 116877 116878 116879 116880 [...] (346866 flits)
Measured flits: 253188 253189 253190 253191 253192 253193 253194 253195 253196 253197 [...] (285517 flits)
Class 0:
Remaining flits: 151722 151723 151724 151725 151726 151727 151728 151729 151730 151731 [...] (349883 flits)
Measured flits: 253189 253190 253191 253192 253193 253194 253195 253196 253197 253198 [...] (267893 flits)
Class 0:
Remaining flits: 151722 151723 151724 151725 151726 151727 151728 151729 151730 151731 [...] (350915 flits)
Measured flits: 253890 253891 253892 253893 253894 253895 253896 253897 253898 253899 [...] (251030 flits)
Class 0:
Remaining flits: 151722 151723 151724 151725 151726 151727 151728 151729 151730 151731 [...] (347740 flits)
Measured flits: 253890 253891 253892 253893 253894 253895 253896 253897 253898 253899 [...] (231829 flits)
Class 0:
Remaining flits: 173286 173287 173288 173289 173290 173291 173292 173293 173294 173295 [...] (347709 flits)
Measured flits: 253890 253891 253892 253893 253894 253895 253896 253897 253898 253899 [...] (211810 flits)
Class 0:
Remaining flits: 173296 173297 173298 173299 173300 173301 173302 173303 177444 177445 [...] (345525 flits)
Measured flits: 253890 253891 253892 253893 253894 253895 253896 253897 253898 253899 [...] (191876 flits)
Class 0:
Remaining flits: 191700 191701 191702 191703 191704 191705 191706 191707 191708 191709 [...] (345439 flits)
Measured flits: 253890 253891 253892 253893 253894 253895 253896 253897 253898 253899 [...] (174150 flits)
Class 0:
Remaining flits: 191700 191701 191702 191703 191704 191705 191706 191707 191708 191709 [...] (342682 flits)
Measured flits: 288270 288271 288272 288273 288274 288275 288276 288277 288278 288279 [...] (155504 flits)
Class 0:
Remaining flits: 191700 191701 191702 191703 191704 191705 191706 191707 191708 191709 [...] (342654 flits)
Measured flits: 288270 288271 288272 288273 288274 288275 288276 288277 288278 288279 [...] (136976 flits)
Class 0:
Remaining flits: 191700 191701 191702 191703 191704 191705 191706 191707 191708 191709 [...] (339881 flits)
Measured flits: 288270 288271 288272 288273 288274 288275 288276 288277 288278 288279 [...] (118022 flits)
Class 0:
Remaining flits: 191700 191701 191702 191703 191704 191705 191706 191707 191708 191709 [...] (337654 flits)
Measured flits: 288284 288285 288286 288287 303390 303391 303392 303393 303394 303395 [...] (101207 flits)
Class 0:
Remaining flits: 191703 191704 191705 191706 191707 191708 191709 191710 191711 191712 [...] (337062 flits)
Measured flits: 303390 303391 303392 303393 303394 303395 303396 303397 303398 303399 [...] (86088 flits)
Class 0:
Remaining flits: 191703 191704 191705 191706 191707 191708 191709 191710 191711 191712 [...] (334388 flits)
Measured flits: 304218 304219 304220 304221 304222 304223 304224 304225 304226 304227 [...] (73220 flits)
Class 0:
Remaining flits: 191713 191714 191715 191716 191717 361620 361621 361622 361623 361624 [...] (335071 flits)
Measured flits: 361620 361621 361622 361623 361624 361625 361626 361627 361628 361629 [...] (59713 flits)
Class 0:
Remaining flits: 374976 374977 374978 374979 374980 374981 374982 374983 374984 374985 [...] (332809 flits)
Measured flits: 374976 374977 374978 374979 374980 374981 374982 374983 374984 374985 [...] (49998 flits)
Class 0:
Remaining flits: 381960 381961 381962 381963 381964 381965 381966 381967 381968 381969 [...] (333904 flits)
Measured flits: 381960 381961 381962 381963 381964 381965 381966 381967 381968 381969 [...] (40629 flits)
Class 0:
Remaining flits: 397332 397333 397334 397335 397336 397337 397338 397339 397340 397341 [...] (333722 flits)
Measured flits: 397332 397333 397334 397335 397336 397337 397338 397339 397340 397341 [...] (33357 flits)
Class 0:
Remaining flits: 397332 397333 397334 397335 397336 397337 397338 397339 397340 397341 [...] (329464 flits)
Measured flits: 397332 397333 397334 397335 397336 397337 397338 397339 397340 397341 [...] (26705 flits)
Class 0:
Remaining flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (328635 flits)
Measured flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (21640 flits)
Class 0:
Remaining flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (324478 flits)
Measured flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (17005 flits)
Class 0:
Remaining flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (324140 flits)
Measured flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (13164 flits)
Class 0:
Remaining flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (320884 flits)
Measured flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (10150 flits)
Class 0:
Remaining flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (318247 flits)
Measured flits: 480510 480511 480512 480513 480514 480515 480516 480517 480518 480519 [...] (7698 flits)
Class 0:
Remaining flits: 480522 480523 480524 480525 480526 480527 598608 598609 598610 598611 [...] (319610 flits)
Measured flits: 480522 480523 480524 480525 480526 480527 598608 598609 598610 598611 [...] (5927 flits)
Class 0:
Remaining flits: 598608 598609 598610 598611 598612 598613 598614 598615 598616 598617 [...] (318534 flits)
Measured flits: 598608 598609 598610 598611 598612 598613 598614 598615 598616 598617 [...] (4694 flits)
Class 0:
Remaining flits: 638316 638317 638318 638319 638320 638321 638322 638323 638324 638325 [...] (316637 flits)
Measured flits: 638316 638317 638318 638319 638320 638321 638322 638323 638324 638325 [...] (3646 flits)
Class 0:
Remaining flits: 638316 638317 638318 638319 638320 638321 638322 638323 638324 638325 [...] (318649 flits)
Measured flits: 638316 638317 638318 638319 638320 638321 638322 638323 638324 638325 [...] (2797 flits)
Class 0:
Remaining flits: 638333 741366 741367 741368 741369 741370 741371 741372 741373 741374 [...] (317945 flits)
Measured flits: 638333 802008 802009 802010 802011 802012 802013 802014 802015 802016 [...] (2171 flits)
Class 0:
Remaining flits: 741366 741367 741368 741369 741370 741371 741372 741373 741374 741375 [...] (316459 flits)
Measured flits: 802008 802009 802010 802011 802012 802013 802014 802015 802016 802017 [...] (1713 flits)
Class 0:
Remaining flits: 802368 802369 802370 802371 802372 802373 802374 802375 802376 802377 [...] (313943 flits)
Measured flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (1374 flits)
Class 0:
Remaining flits: 802368 802369 802370 802371 802372 802373 802374 802375 802376 802377 [...] (311973 flits)
Measured flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (859 flits)
Class 0:
Remaining flits: 802368 802369 802370 802371 802372 802373 802374 802375 802376 802377 [...] (314790 flits)
Measured flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (445 flits)
Class 0:
Remaining flits: 802368 802369 802370 802371 802372 802373 802374 802375 802376 802377 [...] (311436 flits)
Measured flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (360 flits)
Class 0:
Remaining flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (310607 flits)
Measured flits: 859536 859537 859538 859539 859540 859541 859542 859543 859544 859545 [...] (291 flits)
Class 0:
Remaining flits: 887040 887041 887042 887043 887044 887045 887046 887047 887048 887049 [...] (313183 flits)
Measured flits: 933552 933553 933554 933555 933556 933557 933558 933559 933560 933561 [...] (195 flits)
Class 0:
Remaining flits: 887040 887041 887042 887043 887044 887045 887046 887047 887048 887049 [...] (311874 flits)
Measured flits: 933555 933556 933557 933558 933559 933560 933561 933562 933563 933564 [...] (160 flits)
Class 0:
Remaining flits: 887040 887041 887042 887043 887044 887045 887046 887047 887048 887049 [...] (310802 flits)
Measured flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (74 flits)
Class 0:
Remaining flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (311625 flits)
Measured flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (30 flits)
Class 0:
Remaining flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (312914 flits)
Measured flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (18 flits)
Class 0:
Remaining flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (313091 flits)
Measured flits: 967374 967375 967376 967377 967378 967379 967380 967381 967382 967383 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1196604 1196605 1196606 1196607 1196608 1196609 1196610 1196611 1196612 1196613 [...] (276287 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196604 1196605 1196606 1196607 1196608 1196609 1196610 1196611 1196612 1196613 [...] (242102 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196604 1196605 1196606 1196607 1196608 1196609 1196610 1196611 1196612 1196613 [...] (208434 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (175949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (142756 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (112873 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (82968 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (56512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249794 1249795 1249796 1249797 1249798 1249799 1249800 1249801 1249802 1249803 [...] (34626 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1416330 1416331 1416332 1416333 1416334 1416335 1416336 1416337 1416338 1416339 [...] (17361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1634814 1634815 1634816 1634817 1634818 1634819 1634820 1634821 1634822 1634823 [...] (5634 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1634814 1634815 1634816 1634817 1634818 1634819 1634820 1634821 1634822 1634823 [...] (1027 flits)
Measured flits: (0 flits)
Time taken is 65812 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9674.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 42881 (1 samples)
Network latency average = 7281.98 (1 samples)
	minimum = 23 (1 samples)
	maximum = 36970 (1 samples)
Flit latency average = 8023.92 (1 samples)
	minimum = 6 (1 samples)
	maximum = 38713 (1 samples)
Fragmentation average = 200.306 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7081 (1 samples)
Injected packet rate average = 0.0198296 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0321429 (1 samples)
Accepted packet rate average = 0.0115878 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0148571 (1 samples)
Injected flit rate average = 0.356973 (1 samples)
	minimum = 0.170286 (1 samples)
	maximum = 0.576429 (1 samples)
Accepted flit rate average = 0.207969 (1 samples)
	minimum = 0.141857 (1 samples)
	maximum = 0.270143 (1 samples)
Injected packet size average = 18.002 (1 samples)
Accepted packet size average = 17.9473 (1 samples)
Hops average = 5.04224 (1 samples)
Total run time 83.9114
