BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 321.018
	minimum = 23
	maximum = 977
Network latency average = 306.478
	minimum = 23
	maximum = 951
Slowest packet = 263
Flit latency average = 251.177
	minimum = 6
	maximum = 958
Slowest flit = 2103
Fragmentation average = 151.795
	minimum = 0
	maximum = 866
Injected packet rate average = 0.0389948
	minimum = 0.025 (at node 70)
	maximum = 0.052 (at node 177)
Accepted packet rate average = 0.0111771
	minimum = 0.004 (at node 174)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.696193
	minimum = 0.45 (at node 70)
	maximum = 0.93 (at node 177)
Accepted flit rate average= 0.230479
	minimum = 0.087 (at node 174)
	maximum = 0.367 (at node 154)
Injected packet length average = 17.8535
Accepted packet length average = 20.6207
Total in-flight flits = 90550 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 623.307
	minimum = 23
	maximum = 1890
Network latency average = 603.564
	minimum = 23
	maximum = 1859
Slowest packet = 883
Flit latency average = 534.96
	minimum = 6
	maximum = 1920
Slowest flit = 7137
Fragmentation average = 187.543
	minimum = 0
	maximum = 1749
Injected packet rate average = 0.0372969
	minimum = 0.026 (at node 159)
	maximum = 0.05 (at node 18)
Accepted packet rate average = 0.0122005
	minimum = 0.007 (at node 28)
	maximum = 0.017 (at node 2)
Injected flit rate average = 0.66787
	minimum = 0.466 (at node 159)
	maximum = 0.898 (at node 18)
Accepted flit rate average= 0.233865
	minimum = 0.144 (at node 135)
	maximum = 0.3155 (at node 2)
Injected packet length average = 17.9069
Accepted packet length average = 19.1684
Total in-flight flits = 168046 (0 measured)
latency change    = 0.484977
throughput change = 0.014476
Class 0:
Packet latency average = 1583.43
	minimum = 29
	maximum = 2824
Network latency average = 1542.51
	minimum = 28
	maximum = 2802
Slowest packet = 857
Flit latency average = 1500.43
	minimum = 6
	maximum = 2829
Slowest flit = 20164
Fragmentation average = 228.018
	minimum = 0
	maximum = 1889
Injected packet rate average = 0.0330521
	minimum = 0.01 (at node 180)
	maximum = 0.054 (at node 145)
Accepted packet rate average = 0.0122135
	minimum = 0.004 (at node 31)
	maximum = 0.022 (at node 34)
Injected flit rate average = 0.595448
	minimum = 0.177 (at node 180)
	maximum = 0.975 (at node 145)
Accepted flit rate average= 0.218109
	minimum = 0.083 (at node 31)
	maximum = 0.37 (at node 43)
Injected packet length average = 18.0154
Accepted packet length average = 17.858
Total in-flight flits = 240361 (0 measured)
latency change    = 0.606356
throughput change = 0.0722354
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 822.371
	minimum = 29
	maximum = 1999
Network latency average = 63.3179
	minimum = 28
	maximum = 878
Slowest packet = 20699
Flit latency average = 2126.58
	minimum = 6
	maximum = 3771
Slowest flit = 26403
Fragmentation average = 12.4768
	minimum = 2
	maximum = 47
Injected packet rate average = 0.0288385
	minimum = 0 (at node 88)
	maximum = 0.048 (at node 165)
Accepted packet rate average = 0.0121823
	minimum = 0.004 (at node 17)
	maximum = 0.021 (at node 3)
Injected flit rate average = 0.518573
	minimum = 0 (at node 88)
	maximum = 0.864 (at node 165)
Accepted flit rate average= 0.218578
	minimum = 0.07 (at node 17)
	maximum = 0.357 (at node 30)
Injected packet length average = 17.9819
Accepted packet length average = 17.9423
Total in-flight flits = 298114 (96964 measured)
latency change    = 0.925441
throughput change = 0.00214454
Class 0:
Packet latency average = 1199.9
	minimum = 29
	maximum = 3177
Network latency average = 314.643
	minimum = 28
	maximum = 1900
Slowest packet = 20699
Flit latency average = 2442.96
	minimum = 6
	maximum = 4703
Slowest flit = 38379
Fragmentation average = 21.7685
	minimum = 2
	maximum = 258
Injected packet rate average = 0.0235911
	minimum = 0.0045 (at node 0)
	maximum = 0.038 (at node 51)
Accepted packet rate average = 0.0119193
	minimum = 0.0055 (at node 17)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.424479
	minimum = 0.084 (at node 0)
	maximum = 0.6795 (at node 51)
Accepted flit rate average= 0.213701
	minimum = 0.1 (at node 17)
	maximum = 0.3195 (at node 16)
Injected packet length average = 17.9932
Accepted packet length average = 17.929
Total in-flight flits = 321560 (157492 measured)
latency change    = 0.314634
throughput change = 0.0228245
Class 0:
Packet latency average = 2029.62
	minimum = 29
	maximum = 4286
Network latency average = 976.98
	minimum = 28
	maximum = 2846
Slowest packet = 20699
Flit latency average = 2735.95
	minimum = 6
	maximum = 5739
Slowest flit = 23565
Fragmentation average = 61.115
	minimum = 2
	maximum = 805
Injected packet rate average = 0.0207569
	minimum = 0.00833333 (at node 0)
	maximum = 0.03 (at node 85)
Accepted packet rate average = 0.0117465
	minimum = 0.00633333 (at node 17)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.37338
	minimum = 0.147333 (at node 0)
	maximum = 0.54 (at node 143)
Accepted flit rate average= 0.210705
	minimum = 0.115 (at node 17)
	maximum = 0.299667 (at node 16)
Injected packet length average = 17.9882
Accepted packet length average = 17.9376
Total in-flight flits = 334419 (203056 measured)
latency change    = 0.408807
throughput change = 0.0142173
Draining remaining packets ...
Class 0:
Remaining flits: 40356 40357 40358 40359 40360 40361 40362 40363 40364 40365 [...] (295717 flits)
Measured flits: 372042 372043 372044 372045 372046 372047 372048 372049 372050 372051 [...] (194181 flits)
Class 0:
Remaining flits: 49428 49429 49430 49431 49432 49433 49434 49435 49436 49437 [...] (257544 flits)
Measured flits: 372042 372043 372044 372045 372046 372047 372048 372049 372050 372051 [...] (180872 flits)
Class 0:
Remaining flits: 49435 49436 49437 49438 49439 49440 49441 49442 49443 49444 [...] (220195 flits)
Measured flits: 372042 372043 372044 372045 372046 372047 372048 372049 372050 372051 [...] (164068 flits)
Class 0:
Remaining flits: 53244 53245 53246 53247 53248 53249 53250 53251 53252 53253 [...] (184753 flits)
Measured flits: 372042 372043 372044 372045 372046 372047 372048 372049 372050 372051 [...] (144809 flits)
Class 0:
Remaining flits: 53257 53258 53259 53260 53261 67896 67897 67898 67899 67900 [...] (150318 flits)
Measured flits: 372114 372115 372116 372117 372118 372119 372120 372121 372122 372123 [...] (122939 flits)
Class 0:
Remaining flits: 100906 100907 112662 112663 112664 112665 112666 112667 112668 112669 [...] (116869 flits)
Measured flits: 372120 372121 372122 372123 372124 372125 372126 372127 372128 372129 [...] (98677 flits)
Class 0:
Remaining flits: 112662 112663 112664 112665 112666 112667 112668 112669 112670 112671 [...] (83704 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (72138 flits)
Class 0:
Remaining flits: 146538 146539 146540 146541 146542 146543 146544 146545 146546 146547 [...] (51108 flits)
Measured flits: 372150 372151 372152 372153 372154 372155 372156 372157 372158 372159 [...] (44691 flits)
Class 0:
Remaining flits: 146538 146539 146540 146541 146542 146543 146544 146545 146546 146547 [...] (22437 flits)
Measured flits: 373302 373303 373304 373305 373306 373307 373308 373309 373310 373311 [...] (20047 flits)
Class 0:
Remaining flits: 162991 162992 162993 162994 162995 162996 162997 162998 162999 163000 [...] (5082 flits)
Measured flits: 375948 375949 375950 375951 375952 375953 375954 375955 375956 375957 [...] (4691 flits)
Time taken is 16927 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7755.34 (1 samples)
	minimum = 29 (1 samples)
	maximum = 14775 (1 samples)
Network latency average = 7091.29 (1 samples)
	minimum = 28 (1 samples)
	maximum = 13656 (1 samples)
Flit latency average = 5976.21 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15325 (1 samples)
Fragmentation average = 162.794 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2864 (1 samples)
Injected packet rate average = 0.0207569 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0117465 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.37338 (1 samples)
	minimum = 0.147333 (1 samples)
	maximum = 0.54 (1 samples)
Accepted flit rate average = 0.210705 (1 samples)
	minimum = 0.115 (1 samples)
	maximum = 0.299667 (1 samples)
Injected packet size average = 17.9882 (1 samples)
Accepted packet size average = 17.9376 (1 samples)
Hops average = 5.13486 (1 samples)
Total run time 18.4724
