BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 270.542
	minimum = 22
	maximum = 781
Network latency average = 262.436
	minimum = 22
	maximum = 762
Slowest packet = 1150
Flit latency average = 238.653
	minimum = 5
	maximum = 745
Slowest flit = 20484
Fragmentation average = 21.8069
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0141042
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.260042
	minimum = 0.105 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.4372
Total in-flight flits = 49720 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 493.446
	minimum = 22
	maximum = 1483
Network latency average = 484.16
	minimum = 22
	maximum = 1403
Slowest packet = 2856
Flit latency average = 460.059
	minimum = 5
	maximum = 1386
Slowest flit = 59022
Fragmentation average = 22.5122
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0282891
	minimum = 0.0175 (at node 134)
	maximum = 0.0375 (at node 66)
Accepted packet rate average = 0.0149323
	minimum = 0.0075 (at node 153)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.507083
	minimum = 0.315 (at node 134)
	maximum = 0.671 (at node 66)
Accepted flit rate average= 0.27151
	minimum = 0.135 (at node 153)
	maximum = 0.4155 (at node 152)
Injected packet length average = 17.9251
Accepted packet length average = 18.1828
Total in-flight flits = 91562 (0 measured)
latency change    = 0.451729
throughput change = 0.0422406
Class 0:
Packet latency average = 1180.7
	minimum = 22
	maximum = 2227
Network latency average = 1166.23
	minimum = 22
	maximum = 2201
Slowest packet = 4091
Flit latency average = 1146.29
	minimum = 5
	maximum = 2232
Slowest flit = 75625
Fragmentation average = 21.2953
	minimum = 0
	maximum = 186
Injected packet rate average = 0.024526
	minimum = 0.003 (at node 64)
	maximum = 0.039 (at node 83)
Accepted packet rate average = 0.0149583
	minimum = 0.007 (at node 180)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.44174
	minimum = 0.064 (at node 64)
	maximum = 0.706 (at node 83)
Accepted flit rate average= 0.268776
	minimum = 0.137 (at node 180)
	maximum = 0.464 (at node 182)
Injected packet length average = 18.011
Accepted packet length average = 17.9683
Total in-flight flits = 125385 (0 measured)
latency change    = 0.582074
throughput change = 0.0101734
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 454.218
	minimum = 22
	maximum = 1703
Network latency average = 126.234
	minimum = 22
	maximum = 742
Slowest packet = 15643
Flit latency average = 1551.02
	minimum = 5
	maximum = 2789
Slowest flit = 117126
Fragmentation average = 6.10106
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0202292
	minimum = 0.003 (at node 8)
	maximum = 0.035 (at node 49)
Accepted packet rate average = 0.0151302
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 83)
Injected flit rate average = 0.365073
	minimum = 0.054 (at node 8)
	maximum = 0.628 (at node 37)
Accepted flit rate average= 0.272427
	minimum = 0.126 (at node 86)
	maximum = 0.5 (at node 83)
Injected packet length average = 18.0469
Accepted packet length average = 18.0055
Total in-flight flits = 143837 (67295 measured)
latency change    = 1.59942
throughput change = 0.0134019
Class 0:
Packet latency average = 1205.95
	minimum = 22
	maximum = 3091
Network latency average = 774.477
	minimum = 22
	maximum = 1971
Slowest packet = 15643
Flit latency average = 1747.15
	minimum = 5
	maximum = 3415
Slowest flit = 155447
Fragmentation average = 12.386
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0185182
	minimum = 0.0065 (at node 68)
	maximum = 0.0355 (at node 22)
Accepted packet rate average = 0.0150417
	minimum = 0.009 (at node 4)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.333852
	minimum = 0.117 (at node 68)
	maximum = 0.644 (at node 22)
Accepted flit rate average= 0.270755
	minimum = 0.1605 (at node 4)
	maximum = 0.394 (at node 123)
Injected packet length average = 18.0283
Accepted packet length average = 18.0003
Total in-flight flits = 150565 (117058 measured)
latency change    = 0.623353
throughput change = 0.00617486
Class 0:
Packet latency average = 1978.79
	minimum = 22
	maximum = 3881
Network latency average = 1526.59
	minimum = 22
	maximum = 2969
Slowest packet = 15643
Flit latency average = 1936.45
	minimum = 5
	maximum = 3986
Slowest flit = 193643
Fragmentation average = 16.2805
	minimum = 0
	maximum = 166
Injected packet rate average = 0.017441
	minimum = 0.00733333 (at node 68)
	maximum = 0.032 (at node 22)
Accepted packet rate average = 0.0149375
	minimum = 0.0103333 (at node 158)
	maximum = 0.021 (at node 118)
Injected flit rate average = 0.314116
	minimum = 0.132 (at node 68)
	maximum = 0.573667 (at node 22)
Accepted flit rate average= 0.268991
	minimum = 0.189333 (at node 88)
	maximum = 0.381667 (at node 118)
Injected packet length average = 18.0103
Accepted packet length average = 18.0078
Total in-flight flits = 152372 (142925 measured)
latency change    = 0.390561
throughput change = 0.00655742
Class 0:
Packet latency average = 2494.49
	minimum = 22
	maximum = 4553
Network latency average = 1997.89
	minimum = 22
	maximum = 3948
Slowest packet = 15643
Flit latency average = 2097.41
	minimum = 5
	maximum = 4765
Slowest flit = 213659
Fragmentation average = 17.3586
	minimum = 0
	maximum = 257
Injected packet rate average = 0.0167174
	minimum = 0.007 (at node 26)
	maximum = 0.031 (at node 22)
Accepted packet rate average = 0.0149362
	minimum = 0.011 (at node 171)
	maximum = 0.02 (at node 118)
Injected flit rate average = 0.300943
	minimum = 0.12825 (at node 26)
	maximum = 0.5605 (at node 22)
Accepted flit rate average= 0.268777
	minimum = 0.19875 (at node 171)
	maximum = 0.35875 (at node 118)
Injected packet length average = 18.0017
Accepted packet length average = 17.995
Total in-flight flits = 151272 (149739 measured)
latency change    = 0.206735
throughput change = 0.000796108
Class 0:
Packet latency average = 2876.6
	minimum = 22
	maximum = 5211
Network latency average = 2284.16
	minimum = 22
	maximum = 4967
Slowest packet = 15643
Flit latency average = 2232.67
	minimum = 5
	maximum = 5382
Slowest flit = 239111
Fragmentation average = 17.8279
	minimum = 0
	maximum = 257
Injected packet rate average = 0.0163427
	minimum = 0.0088 (at node 177)
	maximum = 0.028 (at node 22)
Accepted packet rate average = 0.0149021
	minimum = 0.0114 (at node 149)
	maximum = 0.0188 (at node 90)
Injected flit rate average = 0.294335
	minimum = 0.1608 (at node 177)
	maximum = 0.506 (at node 22)
Accepted flit rate average= 0.268272
	minimum = 0.208 (at node 149)
	maximum = 0.3372 (at node 90)
Injected packet length average = 18.0102
Accepted packet length average = 18.0023
Total in-flight flits = 151542 (151434 measured)
latency change    = 0.132834
throughput change = 0.00188417
Class 0:
Packet latency average = 3203.7
	minimum = 22
	maximum = 6043
Network latency average = 2447.86
	minimum = 22
	maximum = 5528
Slowest packet = 15643
Flit latency average = 2334.64
	minimum = 5
	maximum = 5511
Slowest flit = 299789
Fragmentation average = 18.3252
	minimum = 0
	maximum = 257
Injected packet rate average = 0.0161727
	minimum = 0.00883333 (at node 45)
	maximum = 0.0273333 (at node 37)
Accepted packet rate average = 0.0148932
	minimum = 0.0118333 (at node 42)
	maximum = 0.0191667 (at node 157)
Injected flit rate average = 0.291268
	minimum = 0.160667 (at node 45)
	maximum = 0.494 (at node 37)
Accepted flit rate average= 0.268195
	minimum = 0.209667 (at node 126)
	maximum = 0.343833 (at node 157)
Injected packet length average = 18.0098
Accepted packet length average = 18.0079
Total in-flight flits = 153006 (153006 measured)
latency change    = 0.102101
throughput change = 0.000285473
Class 0:
Packet latency average = 3504.74
	minimum = 22
	maximum = 6560
Network latency average = 2570.67
	minimum = 22
	maximum = 6325
Slowest packet = 15643
Flit latency average = 2425.47
	minimum = 5
	maximum = 6308
Slowest flit = 310458
Fragmentation average = 18.5499
	minimum = 0
	maximum = 257
Injected packet rate average = 0.0159092
	minimum = 0.00814286 (at node 45)
	maximum = 0.0261429 (at node 37)
Accepted packet rate average = 0.0148638
	minimum = 0.0121429 (at node 42)
	maximum = 0.0188571 (at node 157)
Injected flit rate average = 0.286546
	minimum = 0.148 (at node 45)
	maximum = 0.471286 (at node 37)
Accepted flit rate average= 0.267604
	minimum = 0.218286 (at node 112)
	maximum = 0.337571 (at node 157)
Injected packet length average = 18.0113
Accepted packet length average = 18.0037
Total in-flight flits = 151969 (151969 measured)
latency change    = 0.0858961
throughput change = 0.00220903
Draining all recorded packets ...
Class 0:
Remaining flits: 406710 406711 406712 406713 406714 406715 406716 406717 406718 406719 [...] (150758 flits)
Measured flits: 406710 406711 406712 406713 406714 406715 406716 406717 406718 406719 [...] (150758 flits)
Class 0:
Remaining flits: 483354 483355 483356 483357 483358 483359 483360 483361 483362 483363 [...] (150618 flits)
Measured flits: 483354 483355 483356 483357 483358 483359 483360 483361 483362 483363 [...] (150528 flits)
Class 0:
Remaining flits: 529941 529942 529943 529944 529945 529946 529947 529948 529949 529950 [...] (150139 flits)
Measured flits: 529941 529942 529943 529944 529945 529946 529947 529948 529949 529950 [...] (148681 flits)
Class 0:
Remaining flits: 546120 546121 546122 546123 546124 546125 546126 546127 546128 546129 [...] (149652 flits)
Measured flits: 546120 546121 546122 546123 546124 546125 546126 546127 546128 546129 [...] (143802 flits)
Class 0:
Remaining flits: 551646 551647 551648 551649 551650 551651 551652 551653 551654 551655 [...] (147479 flits)
Measured flits: 551646 551647 551648 551649 551650 551651 551652 551653 551654 551655 [...] (134181 flits)
Class 0:
Remaining flits: 642474 642475 642476 642477 642478 642479 642480 642481 642482 642483 [...] (146610 flits)
Measured flits: 642474 642475 642476 642477 642478 642479 642480 642481 642482 642483 [...] (118820 flits)
Class 0:
Remaining flits: 692802 692803 692804 692805 692806 692807 692808 692809 692810 692811 [...] (144616 flits)
Measured flits: 692802 692803 692804 692805 692806 692807 692808 692809 692810 692811 [...] (97137 flits)
Class 0:
Remaining flits: 706104 706105 706106 706107 706108 706109 706110 706111 706112 706113 [...] (145935 flits)
Measured flits: 706104 706105 706106 706107 706108 706109 706110 706111 706112 706113 [...] (76659 flits)
Class 0:
Remaining flits: 794452 794453 794454 794455 794456 794457 794458 794459 794460 794461 [...] (145393 flits)
Measured flits: 794452 794453 794454 794455 794456 794457 794458 794459 794460 794461 [...] (53897 flits)
Class 0:
Remaining flits: 837828 837829 837830 837831 837832 837833 837834 837835 837836 837837 [...] (143222 flits)
Measured flits: 837828 837829 837830 837831 837832 837833 837834 837835 837836 837837 [...] (30855 flits)
Class 0:
Remaining flits: 871484 871485 871486 871487 873540 873541 873542 873543 873544 873545 [...] (145440 flits)
Measured flits: 871484 871485 871486 871487 875754 875755 875756 875757 875758 875759 [...] (16005 flits)
Class 0:
Remaining flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (147213 flits)
Measured flits: 937116 937117 937118 937119 937120 937121 937122 937123 937124 937125 [...] (7211 flits)
Class 0:
Remaining flits: 982152 982153 982154 982155 982156 982157 982158 982159 982160 982161 [...] (146211 flits)
Measured flits: 1033668 1033669 1033670 1033671 1033672 1033673 1033674 1033675 1033676 1033677 [...] (3253 flits)
Class 0:
Remaining flits: 1057410 1057411 1057412 1057413 1057414 1057415 1057416 1057417 1057418 1057419 [...] (146235 flits)
Measured flits: 1057410 1057411 1057412 1057413 1057414 1057415 1057416 1057417 1057418 1057419 [...] (1216 flits)
Class 0:
Remaining flits: 1076148 1076149 1076150 1076151 1076152 1076153 1076154 1076155 1076156 1076157 [...] (146608 flits)
Measured flits: 1288365 1288366 1288367 1311111 1311112 1311113 1311114 1311115 1311116 1311117 [...] (228 flits)
Class 0:
Remaining flits: 1151460 1151461 1151462 1151463 1151464 1151465 1151466 1151467 1151468 1151469 [...] (146495 flits)
Measured flits: 1371438 1371439 1371440 1371441 1371442 1371443 1371444 1371445 1371446 1371447 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1227168 1227169 1227170 1227171 1227172 1227173 1227174 1227175 1227176 1227177 [...] (95799 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1268874 1268875 1268876 1268877 1268878 1268879 1268880 1268881 1268882 1268883 [...] (47453 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1287792 1287793 1287794 1287795 1287796 1287797 1287798 1287799 1287800 1287801 [...] (8943 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310562 1310563 1310564 1310565 1310566 1310567 1310568 1310569 1310570 1310571 [...] (52 flits)
Measured flits: (0 flits)
Time taken is 30915 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6228.85 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16886 (1 samples)
Network latency average = 2835.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7683 (1 samples)
Flit latency average = 2748.31 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8603 (1 samples)
Fragmentation average = 19.8618 (1 samples)
	minimum = 0 (1 samples)
	maximum = 271 (1 samples)
Injected packet rate average = 0.0159092 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0261429 (1 samples)
Accepted packet rate average = 0.0148638 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.286546 (1 samples)
	minimum = 0.148 (1 samples)
	maximum = 0.471286 (1 samples)
Accepted flit rate average = 0.267604 (1 samples)
	minimum = 0.218286 (1 samples)
	maximum = 0.337571 (1 samples)
Injected packet size average = 18.0113 (1 samples)
Accepted packet size average = 18.0037 (1 samples)
Hops average = 5.0519 (1 samples)
Total run time 31.5613
