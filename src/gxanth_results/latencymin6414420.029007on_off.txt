BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 323.812
	minimum = 22
	maximum = 969
Network latency average = 218.713
	minimum = 22
	maximum = 753
Slowest packet = 7
Flit latency average = 190.953
	minimum = 5
	maximum = 742
Slowest flit = 20147
Fragmentation average = 38.5766
	minimum = 0
	maximum = 469
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135052
	minimum = 0.006 (at node 25)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.253708
	minimum = 0.119 (at node 25)
	maximum = 0.424 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 18.786
Total in-flight flits = 39506 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 549.923
	minimum = 22
	maximum = 1909
Network latency average = 406.177
	minimum = 22
	maximum = 1505
Slowest packet = 7
Flit latency average = 374.384
	minimum = 5
	maximum = 1488
Slowest flit = 39635
Fragmentation average = 56.799
	minimum = 0
	maximum = 611
Injected packet rate average = 0.0268125
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.0144583
	minimum = 0.0085 (at node 83)
	maximum = 0.0195 (at node 88)
Injected flit rate average = 0.480391
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.268224
	minimum = 0.153 (at node 153)
	maximum = 0.362 (at node 166)
Injected packet length average = 17.9167
Accepted packet length average = 18.5515
Total in-flight flits = 82330 (0 measured)
latency change    = 0.411168
throughput change = 0.0541176
Class 0:
Packet latency average = 1145.34
	minimum = 23
	maximum = 2693
Network latency average = 946.873
	minimum = 22
	maximum = 2149
Slowest packet = 4193
Flit latency average = 909.49
	minimum = 5
	maximum = 2208
Slowest flit = 66425
Fragmentation average = 104.519
	minimum = 0
	maximum = 751
Injected packet rate average = 0.0301198
	minimum = 0 (at node 48)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0158073
	minimum = 0.006 (at node 184)
	maximum = 0.029 (at node 168)
Injected flit rate average = 0.541453
	minimum = 0 (at node 48)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287932
	minimum = 0.105 (at node 184)
	maximum = 0.537 (at node 168)
Injected packet length average = 17.9767
Accepted packet length average = 18.2152
Total in-flight flits = 131141 (0 measured)
latency change    = 0.519862
throughput change = 0.0684478
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 293.295
	minimum = 22
	maximum = 1415
Network latency average = 61.1586
	minimum = 22
	maximum = 738
Slowest packet = 16088
Flit latency average = 1315.56
	minimum = 5
	maximum = 3175
Slowest flit = 67406
Fragmentation average = 8.62249
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0299219
	minimum = 0.001 (at node 148)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0159323
	minimum = 0.007 (at node 4)
	maximum = 0.027 (at node 165)
Injected flit rate average = 0.539427
	minimum = 0.017 (at node 148)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.289286
	minimum = 0.125 (at node 4)
	maximum = 0.489 (at node 165)
Injected packet length average = 18.0279
Accepted packet length average = 18.1572
Total in-flight flits = 179008 (94345 measured)
latency change    = 2.90509
throughput change = 0.00468106
Class 0:
Packet latency average = 609.33
	minimum = 22
	maximum = 2305
Network latency average = 403.543
	minimum = 22
	maximum = 1967
Slowest packet = 16088
Flit latency average = 1544.6
	minimum = 5
	maximum = 4092
Slowest flit = 72962
Fragmentation average = 31.4533
	minimum = 0
	maximum = 589
Injected packet rate average = 0.0288594
	minimum = 0.004 (at node 94)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0157578
	minimum = 0.0085 (at node 36)
	maximum = 0.024 (at node 165)
Injected flit rate average = 0.519826
	minimum = 0.072 (at node 94)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.28657
	minimum = 0.157 (at node 36)
	maximum = 0.4295 (at node 165)
Injected packet length average = 18.0124
Accepted packet length average = 18.1859
Total in-flight flits = 220574 (176690 measured)
latency change    = 0.51866
throughput change = 0.00947811
Class 0:
Packet latency average = 1266.57
	minimum = 22
	maximum = 3709
Network latency average = 1068.41
	minimum = 22
	maximum = 2980
Slowest packet = 16088
Flit latency average = 1763.15
	minimum = 5
	maximum = 4843
Slowest flit = 96623
Fragmentation average = 65.4036
	minimum = 0
	maximum = 665
Injected packet rate average = 0.0281146
	minimum = 0.005 (at node 111)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0157517
	minimum = 0.00933333 (at node 36)
	maximum = 0.0213333 (at node 123)
Injected flit rate average = 0.506476
	minimum = 0.0893333 (at node 111)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.286483
	minimum = 0.169667 (at node 36)
	maximum = 0.392 (at node 129)
Injected packet length average = 18.0147
Accepted packet length average = 18.1874
Total in-flight flits = 257637 (239509 measured)
latency change    = 0.518911
throughput change = 0.000306035
Class 0:
Packet latency average = 1866.31
	minimum = 22
	maximum = 5162
Network latency average = 1657.06
	minimum = 22
	maximum = 3952
Slowest packet = 16088
Flit latency average = 1973.65
	minimum = 5
	maximum = 5635
Slowest flit = 122743
Fragmentation average = 98.4316
	minimum = 0
	maximum = 933
Injected packet rate average = 0.0281888
	minimum = 0.01025 (at node 27)
	maximum = 0.05375 (at node 110)
Accepted packet rate average = 0.0157188
	minimum = 0.01 (at node 36)
	maximum = 0.022 (at node 165)
Injected flit rate average = 0.507451
	minimum = 0.182 (at node 27)
	maximum = 0.97075 (at node 110)
Accepted flit rate average= 0.285819
	minimum = 0.18925 (at node 36)
	maximum = 0.38625 (at node 165)
Injected packet length average = 18.0018
Accepted packet length average = 18.1833
Total in-flight flits = 301350 (293445 measured)
latency change    = 0.321355
throughput change = 0.00232185
Class 0:
Packet latency average = 2316.14
	minimum = 22
	maximum = 6091
Network latency average = 2103.85
	minimum = 22
	maximum = 4924
Slowest packet = 16088
Flit latency average = 2191.46
	minimum = 5
	maximum = 6324
Slowest flit = 151613
Fragmentation average = 122.696
	minimum = 0
	maximum = 933
Injected packet rate average = 0.0281052
	minimum = 0.0096 (at node 148)
	maximum = 0.0494 (at node 181)
Accepted packet rate average = 0.0157583
	minimum = 0.012 (at node 86)
	maximum = 0.0204 (at node 128)
Injected flit rate average = 0.506004
	minimum = 0.1728 (at node 148)
	maximum = 0.888 (at node 181)
Accepted flit rate average= 0.28616
	minimum = 0.2178 (at node 86)
	maximum = 0.3656 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 18.1593
Total in-flight flits = 342085 (338602 measured)
latency change    = 0.194212
throughput change = 0.00119306
Class 0:
Packet latency average = 2676.77
	minimum = 22
	maximum = 7007
Network latency average = 2463.51
	minimum = 22
	maximum = 5885
Slowest packet = 16088
Flit latency average = 2402.32
	minimum = 5
	maximum = 6897
Slowest flit = 193729
Fragmentation average = 138.002
	minimum = 0
	maximum = 1105
Injected packet rate average = 0.028178
	minimum = 0.00916667 (at node 148)
	maximum = 0.0503333 (at node 181)
Accepted packet rate average = 0.0158325
	minimum = 0.0131667 (at node 35)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.507274
	minimum = 0.165 (at node 148)
	maximum = 0.903667 (at node 181)
Accepted flit rate average= 0.286424
	minimum = 0.235333 (at node 35)
	maximum = 0.367 (at node 128)
Injected packet length average = 18.0025
Accepted packet length average = 18.091
Total in-flight flits = 385532 (384170 measured)
latency change    = 0.134727
throughput change = 0.000921927
Class 0:
Packet latency average = 2972.49
	minimum = 22
	maximum = 7935
Network latency average = 2758.44
	minimum = 22
	maximum = 6662
Slowest packet = 16088
Flit latency average = 2607.71
	minimum = 5
	maximum = 7643
Slowest flit = 218515
Fragmentation average = 147.994
	minimum = 0
	maximum = 1166
Injected packet rate average = 0.0282135
	minimum = 0.00957143 (at node 148)
	maximum = 0.0497143 (at node 181)
Accepted packet rate average = 0.0158073
	minimum = 0.0131429 (at node 102)
	maximum = 0.0197143 (at node 128)
Injected flit rate average = 0.507919
	minimum = 0.172286 (at node 148)
	maximum = 0.894857 (at node 181)
Accepted flit rate average= 0.286359
	minimum = 0.239714 (at node 86)
	maximum = 0.351143 (at node 128)
Injected packet length average = 18.0027
Accepted packet length average = 18.1157
Total in-flight flits = 428924 (428548 measured)
latency change    = 0.099484
throughput change = 0.000227351
Draining all recorded packets ...
Class 0:
Remaining flits: 264857 264858 264859 264860 264861 264862 264863 264864 264865 264866 [...] (468695 flits)
Measured flits: 291880 291881 291882 291883 291884 291885 291886 291887 300030 300031 [...] (406001 flits)
Class 0:
Remaining flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (503878 flits)
Measured flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (361232 flits)
Class 0:
Remaining flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (540605 flits)
Measured flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (316495 flits)
Class 0:
Remaining flits: 310036 310037 310038 310039 310040 310041 310042 310043 310044 310045 [...] (572980 flits)
Measured flits: 310036 310037 310038 310039 310040 310041 310042 310043 310044 310045 [...] (271367 flits)
Class 0:
Remaining flits: 372078 372079 372080 372081 372082 372083 372084 372085 372086 372087 [...] (607037 flits)
Measured flits: 372078 372079 372080 372081 372082 372083 372084 372085 372086 372087 [...] (226277 flits)
Class 0:
Remaining flits: 372084 372085 372086 372087 372088 372089 372090 372091 372092 372093 [...] (633537 flits)
Measured flits: 372084 372085 372086 372087 372088 372089 372090 372091 372092 372093 [...] (180655 flits)
Class 0:
Remaining flits: 403470 403471 403472 403473 403474 403475 403476 403477 403478 403479 [...] (663411 flits)
Measured flits: 403470 403471 403472 403473 403474 403475 403476 403477 403478 403479 [...] (141010 flits)
Class 0:
Remaining flits: 432630 432631 432632 432633 432634 432635 432636 432637 432638 432639 [...] (687140 flits)
Measured flits: 432630 432631 432632 432633 432634 432635 432636 432637 432638 432639 [...] (109644 flits)
Class 0:
Remaining flits: 436032 436033 436034 436035 436036 436037 436038 436039 436040 436041 [...] (705316 flits)
Measured flits: 436032 436033 436034 436035 436036 436037 436038 436039 436040 436041 [...] (82977 flits)
Class 0:
Remaining flits: 436045 436046 436047 436048 436049 439498 439499 439500 439501 439502 [...] (724257 flits)
Measured flits: 436045 436046 436047 436048 436049 439498 439499 439500 439501 439502 [...] (61889 flits)
Class 0:
Remaining flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (744151 flits)
Measured flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (46233 flits)
Class 0:
Remaining flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (762433 flits)
Measured flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (34026 flits)
Class 0:
Remaining flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (775029 flits)
Measured flits: 484578 484579 484580 484581 484582 484583 484584 484585 484586 484587 [...] (24185 flits)
Class 0:
Remaining flits: 527148 527149 527150 527151 527152 527153 527154 527155 527156 527157 [...] (787755 flits)
Measured flits: 527148 527149 527150 527151 527152 527153 527154 527155 527156 527157 [...] (17330 flits)
Class 0:
Remaining flits: 527148 527149 527150 527151 527152 527153 527154 527155 527156 527157 [...] (796266 flits)
Measured flits: 527148 527149 527150 527151 527152 527153 527154 527155 527156 527157 [...] (12324 flits)
Class 0:
Remaining flits: 591786 591787 591788 591789 591790 591791 591792 591793 591794 591795 [...] (805868 flits)
Measured flits: 591786 591787 591788 591789 591790 591791 591792 591793 591794 591795 [...] (8503 flits)
Class 0:
Remaining flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (817298 flits)
Measured flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (6041 flits)
Class 0:
Remaining flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (821612 flits)
Measured flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (4217 flits)
Class 0:
Remaining flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (829499 flits)
Measured flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (2875 flits)
Class 0:
Remaining flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (832550 flits)
Measured flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (1896 flits)
Class 0:
Remaining flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (839671 flits)
Measured flits: 682686 682687 682688 682689 682690 682691 682692 682693 682694 682695 [...] (1185 flits)
Class 0:
Remaining flits: 808866 808867 808868 808869 808870 808871 808872 808873 808874 808875 [...] (844933 flits)
Measured flits: 808866 808867 808868 808869 808870 808871 808872 808873 808874 808875 [...] (773 flits)
Class 0:
Remaining flits: 808866 808867 808868 808869 808870 808871 808872 808873 808874 808875 [...] (849574 flits)
Measured flits: 808866 808867 808868 808869 808870 808871 808872 808873 808874 808875 [...] (456 flits)
Class 0:
Remaining flits: 958248 958249 958250 958251 958252 958253 958254 958255 958256 958257 [...] (852809 flits)
Measured flits: 958248 958249 958250 958251 958252 958253 958254 958255 958256 958257 [...] (324 flits)
Class 0:
Remaining flits: 992880 992881 992882 992883 992884 992885 992886 992887 992888 992889 [...] (861030 flits)
Measured flits: 992880 992881 992882 992883 992884 992885 992886 992887 992888 992889 [...] (154 flits)
Class 0:
Remaining flits: 996408 996409 996410 996411 996412 996413 996414 996415 996416 996417 [...] (865678 flits)
Measured flits: 996408 996409 996410 996411 996412 996413 996414 996415 996416 996417 [...] (54 flits)
Class 0:
Remaining flits: 1107522 1107523 1107524 1107525 1107526 1107527 1107528 1107529 1107530 1107531 [...] (863326 flits)
Measured flits: 1190178 1190179 1190180 1190181 1190182 1190183 1190184 1190185 1190186 1190187 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1204200 1204201 1204202 1204203 1204204 1204205 1204206 1204207 1204208 1204209 [...] (812098 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1204200 1204201 1204202 1204203 1204204 1204205 1204206 1204207 1204208 1204209 [...] (761952 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1210806 1210807 1210808 1210809 1210810 1210811 1210812 1210813 1210814 1210815 [...] (711863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1210806 1210807 1210808 1210809 1210810 1210811 1210812 1210813 1210814 1210815 [...] (661724 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1210806 1210807 1210808 1210809 1210810 1210811 1210812 1210813 1210814 1210815 [...] (611721 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1371276 1371277 1371278 1371279 1371280 1371281 1371282 1371283 1371284 1371285 [...] (562103 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379916 1379917 1379918 1379919 1379920 1379921 1379922 1379923 1379924 1379925 [...] (513166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1411701 1411702 1411703 1456074 1456075 1456076 1456077 1456078 1456079 1456080 [...] (464707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1456090 1456091 1472994 1472995 1472996 1472997 1472998 1472999 1473000 1473001 [...] (416378 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1646845 1646846 1646847 1646848 1646849 1646850 1646851 1646852 1646853 1646854 [...] (368551 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1707430 1707431 1707432 1707433 1707434 1707435 1707436 1707437 1707438 1707439 [...] (320772 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1741608 1741609 1741610 1741611 1741612 1741613 1741614 1741615 1741616 1741617 [...] (273108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1772676 1772677 1772678 1772679 1772680 1772681 1772682 1772683 1772684 1772685 [...] (225315 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1772676 1772677 1772678 1772679 1772680 1772681 1772682 1772683 1772684 1772685 [...] (177563 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1879977 1879978 1879979 1879980 1879981 1879982 1879983 1879984 1879985 1879986 [...] (130585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1910844 1910845 1910846 1910847 1910848 1910849 1910850 1910851 1910852 1910853 [...] (83538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1910850 1910851 1910852 1910853 1910854 1910855 1910856 1910857 1910858 1910859 [...] (37934 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2258010 2258011 2258012 2258013 2258014 2258015 2258016 2258017 2258018 2258019 [...] (9909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2322475 2322476 2322477 2322478 2322479 2322480 2322481 2322482 2322483 2322484 [...] (1307 flits)
Measured flits: (0 flits)
Time taken is 56980 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6381.94 (1 samples)
	minimum = 22 (1 samples)
	maximum = 28247 (1 samples)
Network latency average = 6025.62 (1 samples)
	minimum = 22 (1 samples)
	maximum = 26407 (1 samples)
Flit latency average = 11104.5 (1 samples)
	minimum = 5 (1 samples)
	maximum = 33069 (1 samples)
Fragmentation average = 178.001 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2152 (1 samples)
Injected packet rate average = 0.0282135 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0497143 (1 samples)
Accepted packet rate average = 0.0158073 (1 samples)
	minimum = 0.0131429 (1 samples)
	maximum = 0.0197143 (1 samples)
Injected flit rate average = 0.507919 (1 samples)
	minimum = 0.172286 (1 samples)
	maximum = 0.894857 (1 samples)
Accepted flit rate average = 0.286359 (1 samples)
	minimum = 0.239714 (1 samples)
	maximum = 0.351143 (1 samples)
Injected packet size average = 18.0027 (1 samples)
Accepted packet size average = 18.1157 (1 samples)
Hops average = 5.08385 (1 samples)
Total run time 100.002
