BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 189.391
	minimum = 23
	maximum = 892
Network latency average = 156.169
	minimum = 23
	maximum = 779
Slowest packet = 367
Flit latency average = 118.815
	minimum = 6
	maximum = 790
Slowest flit = 8704
Fragmentation average = 43.2087
	minimum = 0
	maximum = 124
Injected packet rate average = 0.00982292
	minimum = 0 (at node 17)
	maximum = 0.032 (at node 13)
Accepted packet rate average = 0.0075625
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 73)
Injected flit rate average = 0.175427
	minimum = 0 (at node 17)
	maximum = 0.576 (at node 13)
Accepted flit rate average= 0.142036
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 73)
Injected packet length average = 17.859
Accepted packet length average = 18.7817
Total in-flight flits = 6677 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 240.916
	minimum = 23
	maximum = 1261
Network latency average = 206.031
	minimum = 23
	maximum = 1145
Slowest packet = 1297
Flit latency average = 162.794
	minimum = 6
	maximum = 1128
Slowest flit = 28799
Fragmentation average = 51.9054
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00913542
	minimum = 0.0005 (at node 34)
	maximum = 0.022 (at node 88)
Accepted packet rate average = 0.007875
	minimum = 0.004 (at node 174)
	maximum = 0.013 (at node 44)
Injected flit rate average = 0.163831
	minimum = 0.009 (at node 34)
	maximum = 0.3905 (at node 88)
Accepted flit rate average= 0.14475
	minimum = 0.078 (at node 174)
	maximum = 0.234 (at node 44)
Injected packet length average = 17.9336
Accepted packet length average = 18.381
Total in-flight flits = 7560 (0 measured)
latency change    = 0.213869
throughput change = 0.0187464
Class 0:
Packet latency average = 352.771
	minimum = 27
	maximum = 1956
Network latency average = 310.302
	minimum = 27
	maximum = 1794
Slowest packet = 1696
Flit latency average = 262.026
	minimum = 6
	maximum = 1777
Slowest flit = 30545
Fragmentation average = 65.4979
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0100208
	minimum = 0 (at node 14)
	maximum = 0.04 (at node 15)
Accepted packet rate average = 0.00878646
	minimum = 0.001 (at node 153)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.180245
	minimum = 0 (at node 14)
	maximum = 0.725 (at node 15)
Accepted flit rate average= 0.158474
	minimum = 0.018 (at node 153)
	maximum = 0.323 (at node 78)
Injected packet length average = 17.987
Accepted packet length average = 18.0362
Total in-flight flits = 11765 (0 measured)
latency change    = 0.317077
throughput change = 0.0866007
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 285.629
	minimum = 26
	maximum = 1428
Network latency average = 244.659
	minimum = 25
	maximum = 910
Slowest packet = 5437
Flit latency average = 336.501
	minimum = 6
	maximum = 1952
Slowest flit = 57671
Fragmentation average = 59.358
	minimum = 0
	maximum = 143
Injected packet rate average = 0.00898958
	minimum = 0 (at node 67)
	maximum = 0.04 (at node 187)
Accepted packet rate average = 0.00883854
	minimum = 0.002 (at node 184)
	maximum = 0.015 (at node 24)
Injected flit rate average = 0.161443
	minimum = 0 (at node 67)
	maximum = 0.72 (at node 187)
Accepted flit rate average= 0.159151
	minimum = 0.036 (at node 184)
	maximum = 0.281 (at node 56)
Injected packet length average = 17.9589
Accepted packet length average = 18.0065
Total in-flight flits = 12276 (11233 measured)
latency change    = 0.235066
throughput change = 0.00425434
Class 0:
Packet latency average = 382.701
	minimum = 26
	maximum = 1947
Network latency average = 341.905
	minimum = 25
	maximum = 1787
Slowest packet = 5437
Flit latency average = 367.153
	minimum = 6
	maximum = 2886
Slowest flit = 58283
Fragmentation average = 60.7991
	minimum = 0
	maximum = 146
Injected packet rate average = 0.00898958
	minimum = 0 (at node 67)
	maximum = 0.0225 (at node 17)
Accepted packet rate average = 0.0088151
	minimum = 0.0045 (at node 118)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.16181
	minimum = 0 (at node 67)
	maximum = 0.408 (at node 17)
Accepted flit rate average= 0.158815
	minimum = 0.0795 (at node 163)
	maximum = 0.301 (at node 16)
Injected packet length average = 17.9997
Accepted packet length average = 18.0162
Total in-flight flits = 12916 (12868 measured)
latency change    = 0.253648
throughput change = 0.00211527
Class 0:
Packet latency average = 418.514
	minimum = 26
	maximum = 2703
Network latency average = 380.395
	minimum = 25
	maximum = 2687
Slowest packet = 5543
Flit latency average = 377.866
	minimum = 6
	maximum = 2886
Slowest flit = 58283
Fragmentation average = 61.6637
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00886458
	minimum = 0.000333333 (at node 67)
	maximum = 0.0223333 (at node 1)
Accepted packet rate average = 0.0087934
	minimum = 0.005 (at node 190)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.159609
	minimum = 0.006 (at node 67)
	maximum = 0.403 (at node 1)
Accepted flit rate average= 0.158066
	minimum = 0.09 (at node 190)
	maximum = 0.285667 (at node 16)
Injected packet length average = 18.0053
Accepted packet length average = 17.9755
Total in-flight flits = 12627 (12627 measured)
latency change    = 0.085573
throughput change = 0.00473936
Draining remaining packets ...
Class 0:
Remaining flits: 162324 162325 162326 162327 162328 162329 162330 162331 162332 162333 [...] (594 flits)
Measured flits: 162324 162325 162326 162327 162328 162329 162330 162331 162332 162333 [...] (594 flits)
Time taken is 7602 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 484.228 (1 samples)
	minimum = 26 (1 samples)
	maximum = 3812 (1 samples)
Network latency average = 444.867 (1 samples)
	minimum = 25 (1 samples)
	maximum = 3184 (1 samples)
Flit latency average = 427.913 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3138 (1 samples)
Fragmentation average = 60.3414 (1 samples)
	minimum = 0 (1 samples)
	maximum = 151 (1 samples)
Injected packet rate average = 0.00886458 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0087934 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.159609 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.403 (1 samples)
Accepted flit rate average = 0.158066 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.285667 (1 samples)
Injected packet size average = 18.0053 (1 samples)
Accepted packet size average = 17.9755 (1 samples)
Hops average = 5.06874 (1 samples)
Total run time 3.02243
