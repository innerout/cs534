BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 381.114
	minimum = 22
	maximum = 901
Network latency average = 339.862
	minimum = 22
	maximum = 858
Slowest packet = 802
Flit latency average = 291.051
	minimum = 5
	maximum = 912
Slowest flit = 10038
Fragmentation average = 94.068
	minimum = 0
	maximum = 349
Injected packet rate average = 0.0290156
	minimum = 0.017 (at node 28)
	maximum = 0.038 (at node 51)
Accepted packet rate average = 0.0142396
	minimum = 0.006 (at node 64)
	maximum = 0.023 (at node 22)
Injected flit rate average = 0.515698
	minimum = 0.306 (at node 28)
	maximum = 0.68 (at node 51)
Accepted flit rate average= 0.270703
	minimum = 0.129 (at node 108)
	maximum = 0.415 (at node 48)
Injected packet length average = 17.7731
Accepted packet length average = 19.0106
Total in-flight flits = 49005 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 740.636
	minimum = 22
	maximum = 1791
Network latency average = 586.651
	minimum = 22
	maximum = 1791
Slowest packet = 1124
Flit latency average = 529.119
	minimum = 5
	maximum = 1774
Slowest flit = 20249
Fragmentation average = 89.9319
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0221979
	minimum = 0.0125 (at node 80)
	maximum = 0.0295 (at node 74)
Accepted packet rate average = 0.0145599
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.396841
	minimum = 0.224 (at node 80)
	maximum = 0.53 (at node 74)
Accepted flit rate average= 0.268589
	minimum = 0.1475 (at node 153)
	maximum = 0.382 (at node 78)
Injected packet length average = 17.8774
Accepted packet length average = 18.4471
Total in-flight flits = 51194 (0 measured)
latency change    = 0.485423
throughput change = 0.00787295
Class 0:
Packet latency average = 1813.79
	minimum = 966
	maximum = 2631
Network latency average = 985.854
	minimum = 22
	maximum = 2545
Slowest packet = 2234
Flit latency average = 920.356
	minimum = 5
	maximum = 2528
Slowest flit = 61829
Fragmentation average = 71.9822
	minimum = 0
	maximum = 346
Injected packet rate average = 0.0150469
	minimum = 0.003 (at node 134)
	maximum = 0.027 (at node 30)
Accepted packet rate average = 0.0149115
	minimum = 0.006 (at node 143)
	maximum = 0.03 (at node 34)
Injected flit rate average = 0.270688
	minimum = 0.069 (at node 134)
	maximum = 0.49 (at node 30)
Accepted flit rate average= 0.267943
	minimum = 0.122 (at node 143)
	maximum = 0.532 (at node 34)
Injected packet length average = 17.9896
Accepted packet length average = 17.9689
Total in-flight flits = 51787 (0 measured)
latency change    = 0.591664
throughput change = 0.00241034
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2578.74
	minimum = 1716
	maximum = 3346
Network latency average = 365.172
	minimum = 22
	maximum = 971
Slowest packet = 11482
Flit latency average = 950.856
	minimum = 5
	maximum = 3283
Slowest flit = 53909
Fragmentation average = 37.6815
	minimum = 0
	maximum = 250
Injected packet rate average = 0.014974
	minimum = 0.003 (at node 114)
	maximum = 0.026 (at node 15)
Accepted packet rate average = 0.0150208
	minimum = 0.006 (at node 4)
	maximum = 0.026 (at node 59)
Injected flit rate average = 0.268792
	minimum = 0.059 (at node 114)
	maximum = 0.458 (at node 15)
Accepted flit rate average= 0.269547
	minimum = 0.108 (at node 161)
	maximum = 0.46 (at node 160)
Injected packet length average = 17.9506
Accepted packet length average = 17.9449
Total in-flight flits = 51622 (41051 measured)
latency change    = 0.296636
throughput change = 0.00595135
Class 0:
Packet latency average = 3151.79
	minimum = 1716
	maximum = 4093
Network latency average = 765.422
	minimum = 22
	maximum = 1923
Slowest packet = 11482
Flit latency average = 957.714
	minimum = 5
	maximum = 3283
Slowest flit = 53909
Fragmentation average = 61.7158
	minimum = 0
	maximum = 267
Injected packet rate average = 0.0150547
	minimum = 0.004 (at node 164)
	maximum = 0.022 (at node 133)
Accepted packet rate average = 0.0148958
	minimum = 0.009 (at node 4)
	maximum = 0.023 (at node 0)
Injected flit rate average = 0.270914
	minimum = 0.072 (at node 164)
	maximum = 0.396 (at node 133)
Accepted flit rate average= 0.268586
	minimum = 0.1705 (at node 4)
	maximum = 0.406 (at node 0)
Injected packet length average = 17.9953
Accepted packet length average = 18.0309
Total in-flight flits = 52600 (52036 measured)
latency change    = 0.181817
throughput change = 0.00357777
Class 0:
Packet latency average = 3584.51
	minimum = 1716
	maximum = 4986
Network latency average = 890.86
	minimum = 22
	maximum = 2841
Slowest packet = 11482
Flit latency average = 959.535
	minimum = 5
	maximum = 3879
Slowest flit = 147545
Fragmentation average = 68.1722
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0150538
	minimum = 0.00766667 (at node 164)
	maximum = 0.0196667 (at node 10)
Accepted packet rate average = 0.0149236
	minimum = 0.00933333 (at node 135)
	maximum = 0.019 (at node 28)
Injected flit rate average = 0.270969
	minimum = 0.138 (at node 164)
	maximum = 0.355 (at node 17)
Accepted flit rate average= 0.269101
	minimum = 0.174 (at node 135)
	maximum = 0.351333 (at node 138)
Injected packet length average = 18
Accepted packet length average = 18.0319
Total in-flight flits = 52737 (52713 measured)
latency change    = 0.12072
throughput change = 0.00191288
Class 0:
Packet latency average = 3970.11
	minimum = 1716
	maximum = 5797
Network latency average = 944.255
	minimum = 22
	maximum = 3374
Slowest packet = 11482
Flit latency average = 964.573
	minimum = 5
	maximum = 3879
Slowest flit = 147545
Fragmentation average = 71.5349
	minimum = 0
	maximum = 309
Injected packet rate average = 0.014918
	minimum = 0.00875 (at node 112)
	maximum = 0.021 (at node 133)
Accepted packet rate average = 0.0148633
	minimum = 0.01075 (at node 135)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.268514
	minimum = 0.1565 (at node 112)
	maximum = 0.378 (at node 133)
Accepted flit rate average= 0.267901
	minimum = 0.197 (at node 135)
	maximum = 0.34025 (at node 128)
Injected packet length average = 17.9994
Accepted packet length average = 18.0244
Total in-flight flits = 52319 (52319 measured)
latency change    = 0.0971248
throughput change = 0.00447797
Class 0:
Packet latency average = 4351.57
	minimum = 1716
	maximum = 6600
Network latency average = 967.292
	minimum = 22
	maximum = 3672
Slowest packet = 11482
Flit latency average = 964.136
	minimum = 5
	maximum = 3879
Slowest flit = 147545
Fragmentation average = 73.5559
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0149125
	minimum = 0.01 (at node 112)
	maximum = 0.019 (at node 115)
Accepted packet rate average = 0.0148688
	minimum = 0.0116 (at node 80)
	maximum = 0.0188 (at node 138)
Injected flit rate average = 0.268516
	minimum = 0.1788 (at node 112)
	maximum = 0.345 (at node 178)
Accepted flit rate average= 0.268034
	minimum = 0.2088 (at node 80)
	maximum = 0.3384 (at node 138)
Injected packet length average = 18.0061
Accepted packet length average = 18.0267
Total in-flight flits = 52072 (52072 measured)
latency change    = 0.0876596
throughput change = 0.000497449
Class 0:
Packet latency average = 4730.51
	minimum = 1716
	maximum = 7362
Network latency average = 983.29
	minimum = 22
	maximum = 3751
Slowest packet = 11482
Flit latency average = 965.178
	minimum = 5
	maximum = 3879
Slowest flit = 147545
Fragmentation average = 74.6436
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0149288
	minimum = 0.00966667 (at node 176)
	maximum = 0.0191667 (at node 115)
Accepted packet rate average = 0.0149132
	minimum = 0.0113333 (at node 79)
	maximum = 0.0186667 (at node 138)
Injected flit rate average = 0.268565
	minimum = 0.1735 (at node 176)
	maximum = 0.345333 (at node 190)
Accepted flit rate average= 0.268824
	minimum = 0.210167 (at node 79)
	maximum = 0.338833 (at node 138)
Injected packet length average = 17.9897
Accepted packet length average = 18.0259
Total in-flight flits = 51450 (51450 measured)
latency change    = 0.0801053
throughput change = 0.00293653
Class 0:
Packet latency average = 5099.6
	minimum = 1716
	maximum = 8322
Network latency average = 987.975
	minimum = 22
	maximum = 4115
Slowest packet = 11482
Flit latency average = 961.084
	minimum = 5
	maximum = 4031
Slowest flit = 332225
Fragmentation average = 75.8263
	minimum = 0
	maximum = 380
Injected packet rate average = 0.0149509
	minimum = 0.01 (at node 112)
	maximum = 0.0202857 (at node 133)
Accepted packet rate average = 0.0149397
	minimum = 0.0117143 (at node 79)
	maximum = 0.0182857 (at node 138)
Injected flit rate average = 0.268988
	minimum = 0.178286 (at node 112)
	maximum = 0.364857 (at node 133)
Accepted flit rate average= 0.268983
	minimum = 0.212 (at node 190)
	maximum = 0.329143 (at node 138)
Injected packet length average = 17.9914
Accepted packet length average = 18.0045
Total in-flight flits = 51714 (51714 measured)
latency change    = 0.0723765
throughput change = 0.000591496
Draining all recorded packets ...
Class 0:
Remaining flits: 384828 384829 384830 384831 384832 384833 384834 384835 384836 384837 [...] (50380 flits)
Measured flits: 384828 384829 384830 384831 384832 384833 384834 384835 384836 384837 [...] (50380 flits)
Class 0:
Remaining flits: 419076 419077 419078 419079 419080 419081 419082 419083 419084 419085 [...] (50987 flits)
Measured flits: 419076 419077 419078 419079 419080 419081 419082 419083 419084 419085 [...] (50987 flits)
Class 0:
Remaining flits: 494316 494317 494318 494319 494320 494321 494322 494323 494324 494325 [...] (50829 flits)
Measured flits: 494316 494317 494318 494319 494320 494321 494322 494323 494324 494325 [...] (50829 flits)
Class 0:
Remaining flits: 524880 524881 524882 524883 524884 524885 524886 524887 524888 524889 [...] (50834 flits)
Measured flits: 524880 524881 524882 524883 524884 524885 524886 524887 524888 524889 [...] (50834 flits)
Class 0:
Remaining flits: 594684 594685 594686 594687 594688 594689 594690 594691 594692 594693 [...] (50367 flits)
Measured flits: 594684 594685 594686 594687 594688 594689 594690 594691 594692 594693 [...] (50367 flits)
Class 0:
Remaining flits: 594684 594685 594686 594687 594688 594689 594690 594691 594692 594693 [...] (50831 flits)
Measured flits: 594684 594685 594686 594687 594688 594689 594690 594691 594692 594693 [...] (50831 flits)
Class 0:
Remaining flits: 699996 699997 699998 699999 700000 700001 715968 715969 715970 715971 [...] (50237 flits)
Measured flits: 699996 699997 699998 699999 700000 700001 715968 715969 715970 715971 [...] (50237 flits)
Class 0:
Remaining flits: 751819 751820 751821 751822 751823 757233 757234 757235 757236 757237 [...] (50098 flits)
Measured flits: 751819 751820 751821 751822 751823 757233 757234 757235 757236 757237 [...] (50098 flits)
Class 0:
Remaining flits: 760356 760357 760358 760359 760360 760361 760362 760363 760364 760365 [...] (49924 flits)
Measured flits: 760356 760357 760358 760359 760360 760361 760362 760363 760364 760365 [...] (49924 flits)
Class 0:
Remaining flits: 849456 849457 849458 849459 849460 849461 849462 849463 849464 849465 [...] (49485 flits)
Measured flits: 849456 849457 849458 849459 849460 849461 849462 849463 849464 849465 [...] (49485 flits)
Class 0:
Remaining flits: 882054 882055 882056 882057 882058 882059 882060 882061 882062 882063 [...] (48886 flits)
Measured flits: 882054 882055 882056 882057 882058 882059 882060 882061 882062 882063 [...] (48886 flits)
Class 0:
Remaining flits: 919656 919657 919658 919659 919660 919661 919662 919663 919664 919665 [...] (48590 flits)
Measured flits: 919656 919657 919658 919659 919660 919661 919662 919663 919664 919665 [...] (48590 flits)
Class 0:
Remaining flits: 989838 989839 989840 989841 989842 989843 989844 989845 989846 989847 [...] (49080 flits)
Measured flits: 989838 989839 989840 989841 989842 989843 989844 989845 989846 989847 [...] (49080 flits)
Class 0:
Remaining flits: 1013040 1013041 1013042 1013043 1013044 1013045 1013046 1013047 1013048 1013049 [...] (49229 flits)
Measured flits: 1013040 1013041 1013042 1013043 1013044 1013045 1013046 1013047 1013048 1013049 [...] (49229 flits)
Class 0:
Remaining flits: 1013040 1013041 1013042 1013043 1013044 1013045 1013046 1013047 1013048 1013049 [...] (49598 flits)
Measured flits: 1013040 1013041 1013042 1013043 1013044 1013045 1013046 1013047 1013048 1013049 [...] (49598 flits)
Class 0:
Remaining flits: 1143252 1143253 1143254 1143255 1143256 1143257 1143258 1143259 1143260 1143261 [...] (49253 flits)
Measured flits: 1143252 1143253 1143254 1143255 1143256 1143257 1143258 1143259 1143260 1143261 [...] (49253 flits)
Class 0:
Remaining flits: 1143252 1143253 1143254 1143255 1143256 1143257 1143258 1143259 1143260 1143261 [...] (48908 flits)
Measured flits: 1143252 1143253 1143254 1143255 1143256 1143257 1143258 1143259 1143260 1143261 [...] (48908 flits)
Class 0:
Remaining flits: 1164550 1164551 1164552 1164553 1164554 1164555 1164556 1164557 1164558 1164559 [...] (48316 flits)
Measured flits: 1164550 1164551 1164552 1164553 1164554 1164555 1164556 1164557 1164558 1164559 [...] (48316 flits)
Class 0:
Remaining flits: 1283508 1283509 1283510 1283511 1283512 1283513 1283514 1283515 1283516 1283517 [...] (48384 flits)
Measured flits: 1283508 1283509 1283510 1283511 1283512 1283513 1283514 1283515 1283516 1283517 [...] (48384 flits)
Class 0:
Remaining flits: 1283511 1283512 1283513 1283514 1283515 1283516 1283517 1283518 1283519 1283520 [...] (48897 flits)
Measured flits: 1283511 1283512 1283513 1283514 1283515 1283516 1283517 1283518 1283519 1283520 [...] (48717 flits)
Class 0:
Remaining flits: 1416690 1416691 1416692 1416693 1416694 1416695 1416696 1416697 1416698 1416699 [...] (49267 flits)
Measured flits: 1416690 1416691 1416692 1416693 1416694 1416695 1416696 1416697 1416698 1416699 [...] (47567 flits)
Class 0:
Remaining flits: 1446966 1446967 1446968 1446969 1446970 1446971 1446972 1446973 1446974 1446975 [...] (48815 flits)
Measured flits: 1446966 1446967 1446968 1446969 1446970 1446971 1446972 1446973 1446974 1446975 [...] (45018 flits)
Class 0:
Remaining flits: 1508994 1508995 1508996 1508997 1508998 1508999 1509000 1509001 1509002 1509003 [...] (48377 flits)
Measured flits: 1508994 1508995 1508996 1508997 1508998 1508999 1509000 1509001 1509002 1509003 [...] (40302 flits)
Class 0:
Remaining flits: 1549692 1549693 1549694 1549695 1549696 1549697 1549698 1549699 1549700 1549701 [...] (47812 flits)
Measured flits: 1549692 1549693 1549694 1549695 1549696 1549697 1549698 1549699 1549700 1549701 [...] (35279 flits)
Class 0:
Remaining flits: 1549692 1549693 1549694 1549695 1549696 1549697 1549698 1549699 1549700 1549701 [...] (48644 flits)
Measured flits: 1549692 1549693 1549694 1549695 1549696 1549697 1549698 1549699 1549700 1549701 [...] (29506 flits)
Class 0:
Remaining flits: 1659852 1659853 1659854 1659855 1659856 1659857 1659858 1659859 1659860 1659861 [...] (49146 flits)
Measured flits: 1659852 1659853 1659854 1659855 1659856 1659857 1659858 1659859 1659860 1659861 [...] (21906 flits)
Class 0:
Remaining flits: 1693944 1693945 1693946 1693947 1693948 1693949 1693950 1693951 1693952 1693953 [...] (48916 flits)
Measured flits: 1693944 1693945 1693946 1693947 1693948 1693949 1693950 1693951 1693952 1693953 [...] (16657 flits)
Class 0:
Remaining flits: 1693944 1693945 1693946 1693947 1693948 1693949 1693950 1693951 1693952 1693953 [...] (47443 flits)
Measured flits: 1693944 1693945 1693946 1693947 1693948 1693949 1693950 1693951 1693952 1693953 [...] (12885 flits)
Class 0:
Remaining flits: 1794060 1794061 1794062 1794063 1794064 1794065 1794066 1794067 1794068 1794069 [...] (48269 flits)
Measured flits: 1794060 1794061 1794062 1794063 1794064 1794065 1794066 1794067 1794068 1794069 [...] (10950 flits)
Class 0:
Remaining flits: 1810962 1810963 1810964 1810965 1810966 1810967 1810968 1810969 1810970 1810971 [...] (48207 flits)
Measured flits: 1834920 1834921 1834922 1834923 1834924 1834925 1834926 1834927 1834928 1834929 [...] (8851 flits)
Class 0:
Remaining flits: 1899072 1899073 1899074 1899075 1899076 1899077 1899078 1899079 1899080 1899081 [...] (48507 flits)
Measured flits: 1978539 1978540 1978541 1979820 1979821 1979822 1979823 1979824 1979825 1979826 [...] (7003 flits)
Class 0:
Remaining flits: 1935549 1935550 1935551 1935552 1935553 1935554 1935555 1935556 1935557 1974204 [...] (48171 flits)
Measured flits: 2047860 2047861 2047862 2047863 2047864 2047865 2047866 2047867 2047868 2047869 [...] (5505 flits)
Class 0:
Remaining flits: 2007270 2007271 2007272 2007273 2007274 2007275 2007276 2007277 2007278 2007279 [...] (48379 flits)
Measured flits: 2078585 2078676 2078677 2078678 2078679 2078680 2078681 2078682 2078683 2078684 [...] (3598 flits)
Class 0:
Remaining flits: 2021585 2021586 2021587 2021588 2021589 2021590 2021591 2021592 2021593 2021594 [...] (48634 flits)
Measured flits: 2178558 2178559 2178560 2178561 2178562 2178563 2178564 2178565 2178566 2178567 [...] (3644 flits)
Class 0:
Remaining flits: 2066346 2066347 2066348 2066349 2066350 2066351 2066352 2066353 2066354 2066355 [...] (48371 flits)
Measured flits: 2203365 2203366 2203367 2203368 2203369 2203370 2203371 2203372 2203373 2203374 [...] (2149 flits)
Class 0:
Remaining flits: 2066346 2066347 2066348 2066349 2066350 2066351 2066352 2066353 2066354 2066355 [...] (48657 flits)
Measured flits: 2206781 2221542 2221543 2221544 2221545 2221546 2221547 2221548 2221549 2221550 [...] (1939 flits)
Class 0:
Remaining flits: 2175974 2175975 2175976 2175977 2175978 2175979 2175980 2175981 2175982 2175983 [...] (48578 flits)
Measured flits: 2272986 2272987 2272988 2272989 2272990 2272991 2272992 2272993 2272994 2272995 [...] (1005 flits)
Class 0:
Remaining flits: 2264012 2264013 2264014 2264015 2264016 2264017 2264018 2264019 2264020 2264021 [...] (47993 flits)
Measured flits: 2343024 2343025 2343026 2343027 2343028 2343029 2343030 2343031 2343032 2343033 [...] (927 flits)
Class 0:
Remaining flits: 2343024 2343025 2343026 2343027 2343028 2343029 2343030 2343031 2343032 2343033 [...] (48884 flits)
Measured flits: 2343024 2343025 2343026 2343027 2343028 2343029 2343030 2343031 2343032 2343033 [...] (883 flits)
Class 0:
Remaining flits: 2353194 2353195 2353196 2353197 2353198 2353199 2353200 2353201 2353202 2353203 [...] (47707 flits)
Measured flits: 2490948 2490949 2490950 2490951 2490952 2490953 2490954 2490955 2490956 2490957 [...] (729 flits)
Class 0:
Remaining flits: 2396814 2396815 2396816 2396817 2396818 2396819 2396820 2396821 2396822 2396823 [...] (48353 flits)
Measured flits: 2544156 2544157 2544158 2544159 2544160 2544161 2544162 2544163 2544164 2544165 [...] (659 flits)
Class 0:
Remaining flits: 2487708 2487709 2487710 2487711 2487712 2487713 2487714 2487715 2487716 2487717 [...] (48459 flits)
Measured flits: 2601396 2601397 2601398 2601399 2601400 2601401 2601402 2601403 2601404 2601405 [...] (45 flits)
Class 0:
Remaining flits: 2491561 2491562 2491563 2491564 2491565 2491566 2491567 2491568 2491569 2491570 [...] (48875 flits)
Measured flits: 2662552 2662553 2662554 2662555 2662556 2662557 2662558 2662559 (8 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2500308 2500309 2500310 2500311 2500312 2500313 2500314 2500315 2500316 2500317 [...] (9039 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2588742 2588743 2588744 2588745 2588746 2588747 2588748 2588749 2588750 2588751 [...] (626 flits)
Measured flits: (0 flits)
Time taken is 55528 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15477.9 (1 samples)
	minimum = 1716 (1 samples)
	maximum = 43091 (1 samples)
Network latency average = 988.936 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6631 (1 samples)
Flit latency average = 929.761 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6653 (1 samples)
Fragmentation average = 76.3093 (1 samples)
	minimum = 0 (1 samples)
	maximum = 382 (1 samples)
Injected packet rate average = 0.0149509 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0202857 (1 samples)
Accepted packet rate average = 0.0149397 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.268988 (1 samples)
	minimum = 0.178286 (1 samples)
	maximum = 0.364857 (1 samples)
Accepted flit rate average = 0.268983 (1 samples)
	minimum = 0.212 (1 samples)
	maximum = 0.329143 (1 samples)
Injected packet size average = 17.9914 (1 samples)
Accepted packet size average = 18.0045 (1 samples)
Hops average = 5.06375 (1 samples)
Total run time 76.4887
