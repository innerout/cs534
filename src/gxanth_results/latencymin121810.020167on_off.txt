BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 303.487
	minimum = 23
	maximum = 958
Network latency average = 212.395
	minimum = 23
	maximum = 788
Slowest packet = 3
Flit latency average = 171.365
	minimum = 6
	maximum = 815
Slowest flit = 11027
Fragmentation average = 46.0706
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0122083
	minimum = 0 (at node 10)
	maximum = 0.029 (at node 11)
Accepted packet rate average = 0.00855208
	minimum = 0.001 (at node 174)
	maximum = 0.016 (at node 165)
Injected flit rate average = 0.21675
	minimum = 0 (at node 10)
	maximum = 0.522 (at node 11)
Accepted flit rate average= 0.160125
	minimum = 0.033 (at node 174)
	maximum = 0.288 (at node 165)
Injected packet length average = 17.7543
Accepted packet length average = 18.7235
Total in-flight flits = 12618 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 528.767
	minimum = 23
	maximum = 1950
Network latency average = 299.532
	minimum = 23
	maximum = 1575
Slowest packet = 3
Flit latency average = 253.221
	minimum = 6
	maximum = 1834
Slowest flit = 9807
Fragmentation average = 50.9757
	minimum = 0
	maximum = 121
Injected packet rate average = 0.010888
	minimum = 0.001 (at node 137)
	maximum = 0.023 (at node 190)
Accepted packet rate average = 0.00866927
	minimum = 0.0045 (at node 174)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.194107
	minimum = 0.018 (at node 137)
	maximum = 0.4065 (at node 190)
Accepted flit rate average= 0.159174
	minimum = 0.0815 (at node 174)
	maximum = 0.252 (at node 22)
Injected packet length average = 17.8276
Accepted packet length average = 18.3608
Total in-flight flits = 15701 (0 measured)
latency change    = 0.426048
throughput change = 0.00597157
Class 0:
Packet latency average = 1252.58
	minimum = 31
	maximum = 2898
Network latency average = 485.437
	minimum = 23
	maximum = 1998
Slowest packet = 544
Flit latency average = 425.385
	minimum = 6
	maximum = 1981
Slowest flit = 34271
Fragmentation average = 58.0844
	minimum = 0
	maximum = 132
Injected packet rate average = 0.00927083
	minimum = 0 (at node 122)
	maximum = 0.029 (at node 123)
Accepted packet rate average = 0.00907292
	minimum = 0.004 (at node 28)
	maximum = 0.016 (at node 13)
Injected flit rate average = 0.167281
	minimum = 0 (at node 122)
	maximum = 0.522 (at node 123)
Accepted flit rate average= 0.162885
	minimum = 0.065 (at node 94)
	maximum = 0.288 (at node 13)
Injected packet length average = 18.0438
Accepted packet length average = 17.9529
Total in-flight flits = 16755 (0 measured)
latency change    = 0.577859
throughput change = 0.0227825
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1683.92
	minimum = 31
	maximum = 3666
Network latency average = 314.746
	minimum = 25
	maximum = 986
Slowest packet = 6074
Flit latency average = 447.092
	minimum = 6
	maximum = 2339
Slowest flit = 58271
Fragmentation average = 54.5412
	minimum = 0
	maximum = 137
Injected packet rate average = 0.00908854
	minimum = 0 (at node 17)
	maximum = 0.023 (at node 127)
Accepted packet rate average = 0.00904688
	minimum = 0.003 (at node 45)
	maximum = 0.02 (at node 51)
Injected flit rate average = 0.163323
	minimum = 0 (at node 37)
	maximum = 0.427 (at node 127)
Accepted flit rate average= 0.163057
	minimum = 0.06 (at node 45)
	maximum = 0.36 (at node 51)
Injected packet length average = 17.9702
Accepted packet length average = 18.0236
Total in-flight flits = 16660 (15363 measured)
latency change    = 0.256149
throughput change = 0.00105408
Class 0:
Packet latency average = 2058.33
	minimum = 31
	maximum = 4600
Network latency average = 431.926
	minimum = 25
	maximum = 1594
Slowest packet = 6074
Flit latency average = 457.539
	minimum = 6
	maximum = 2445
Slowest flit = 85633
Fragmentation average = 57.8233
	minimum = 0
	maximum = 143
Injected packet rate average = 0.00898177
	minimum = 0.0005 (at node 164)
	maximum = 0.016 (at node 25)
Accepted packet rate average = 0.00889063
	minimum = 0.0035 (at node 190)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.161406
	minimum = 0.009 (at node 164)
	maximum = 0.291 (at node 25)
Accepted flit rate average= 0.160112
	minimum = 0.0655 (at node 190)
	maximum = 0.3 (at node 16)
Injected packet length average = 17.9704
Accepted packet length average = 18.0091
Total in-flight flits = 17354 (17255 measured)
latency change    = 0.1819
throughput change = 0.0183953
Class 0:
Packet latency average = 2374.17
	minimum = 27
	maximum = 5535
Network latency average = 480.591
	minimum = 23
	maximum = 2581
Slowest packet = 6074
Flit latency average = 470.082
	minimum = 6
	maximum = 2508
Slowest flit = 109758
Fragmentation average = 56.5005
	minimum = 0
	maximum = 143
Injected packet rate average = 0.00889063
	minimum = 0.003 (at node 58)
	maximum = 0.016 (at node 25)
Accepted packet rate average = 0.00881944
	minimum = 0.00466667 (at node 190)
	maximum = 0.0136667 (at node 16)
Injected flit rate average = 0.159689
	minimum = 0.054 (at node 58)
	maximum = 0.288333 (at node 25)
Accepted flit rate average= 0.158762
	minimum = 0.084 (at node 190)
	maximum = 0.250333 (at node 16)
Injected packet length average = 17.9615
Accepted packet length average = 18.0014
Total in-flight flits = 17378 (17360 measured)
latency change    = 0.133034
throughput change = 0.00850219
Class 0:
Packet latency average = 2677.17
	minimum = 27
	maximum = 6516
Network latency average = 496.921
	minimum = 23
	maximum = 3313
Slowest packet = 6074
Flit latency average = 470.992
	minimum = 6
	maximum = 3296
Slowest flit = 122057
Fragmentation average = 56.9438
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00896615
	minimum = 0.004 (at node 58)
	maximum = 0.015 (at node 25)
Accepted packet rate average = 0.0088724
	minimum = 0.005 (at node 4)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.161241
	minimum = 0.06825 (at node 58)
	maximum = 0.27075 (at node 25)
Accepted flit rate average= 0.159875
	minimum = 0.08975 (at node 4)
	maximum = 0.23825 (at node 16)
Injected packet length average = 17.9833
Accepted packet length average = 18.0194
Total in-flight flits = 17811 (17811 measured)
latency change    = 0.113178
throughput change = 0.00696073
Class 0:
Packet latency average = 2962.02
	minimum = 27
	maximum = 7354
Network latency average = 508.757
	minimum = 23
	maximum = 3313
Slowest packet = 6074
Flit latency average = 474.209
	minimum = 6
	maximum = 3296
Slowest flit = 122057
Fragmentation average = 57.175
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00897604
	minimum = 0.0048 (at node 12)
	maximum = 0.014 (at node 25)
Accepted packet rate average = 0.00892813
	minimum = 0.0058 (at node 4)
	maximum = 0.0128 (at node 16)
Injected flit rate average = 0.161542
	minimum = 0.086 (at node 12)
	maximum = 0.2546 (at node 25)
Accepted flit rate average= 0.160771
	minimum = 0.1066 (at node 4)
	maximum = 0.2336 (at node 16)
Injected packet length average = 17.997
Accepted packet length average = 18.0072
Total in-flight flits = 17611 (17611 measured)
latency change    = 0.0961692
throughput change = 0.00557211
Class 0:
Packet latency average = 3255.67
	minimum = 27
	maximum = 8216
Network latency average = 516.304
	minimum = 23
	maximum = 3313
Slowest packet = 6074
Flit latency average = 476.495
	minimum = 6
	maximum = 3296
Slowest flit = 122057
Fragmentation average = 57.3181
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00893576
	minimum = 0.005 (at node 103)
	maximum = 0.0131667 (at node 80)
Accepted packet rate average = 0.00891059
	minimum = 0.006 (at node 36)
	maximum = 0.0121667 (at node 128)
Injected flit rate average = 0.160806
	minimum = 0.0921667 (at node 14)
	maximum = 0.235667 (at node 80)
Accepted flit rate average= 0.16031
	minimum = 0.110333 (at node 36)
	maximum = 0.221167 (at node 128)
Injected packet length average = 17.9957
Accepted packet length average = 17.9909
Total in-flight flits = 17514 (17514 measured)
latency change    = 0.0901946
throughput change = 0.00287529
Class 0:
Packet latency average = 3509.26
	minimum = 27
	maximum = 8383
Network latency average = 520.378
	minimum = 23
	maximum = 3313
Slowest packet = 6074
Flit latency average = 476.662
	minimum = 6
	maximum = 3296
Slowest flit = 122057
Fragmentation average = 57.8237
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00896949
	minimum = 0.00471429 (at node 103)
	maximum = 0.0134286 (at node 176)
Accepted packet rate average = 0.00892113
	minimum = 0.00628571 (at node 36)
	maximum = 0.0117143 (at node 128)
Injected flit rate average = 0.161454
	minimum = 0.0872857 (at node 103)
	maximum = 0.240286 (at node 176)
Accepted flit rate average= 0.160727
	minimum = 0.113429 (at node 36)
	maximum = 0.213143 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 18.0164
Total in-flight flits = 17728 (17728 measured)
latency change    = 0.0722624
throughput change = 0.0025947
Draining all recorded packets ...
Class 0:
Remaining flits: 278136 278137 278138 278139 278140 278141 278142 278143 278144 278145 [...] (17933 flits)
Measured flits: 278136 278137 278138 278139 278140 278141 278142 278143 278144 278145 [...] (17843 flits)
Class 0:
Remaining flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (17505 flits)
Measured flits: 330192 330193 330194 330195 330196 330197 330198 330199 330200 330201 [...] (17407 flits)
Class 0:
Remaining flits: 352260 352261 352262 352263 352264 352265 352266 352267 352268 352269 [...] (17896 flits)
Measured flits: 352260 352261 352262 352263 352264 352265 352266 352267 352268 352269 [...] (17770 flits)
Class 0:
Remaining flits: 394002 394003 394004 394005 394006 394007 394008 394009 394010 394011 [...] (17814 flits)
Measured flits: 394002 394003 394004 394005 394006 394007 394008 394009 394010 394011 [...] (17219 flits)
Class 0:
Remaining flits: 414426 414427 414428 414429 414430 414431 414979 414980 414981 414982 [...] (17961 flits)
Measured flits: 414426 414427 414428 414429 414430 414431 414979 414980 414981 414982 [...] (16993 flits)
Class 0:
Remaining flits: 428238 428239 428240 428241 428242 428243 428244 428245 428246 428247 [...] (18126 flits)
Measured flits: 428238 428239 428240 428241 428242 428243 428244 428245 428246 428247 [...] (15988 flits)
Class 0:
Remaining flits: 480384 480385 480386 480387 480388 480389 480390 480391 480392 480393 [...] (17961 flits)
Measured flits: 480384 480385 480386 480387 480388 480389 480390 480391 480392 480393 [...] (14709 flits)
Class 0:
Remaining flits: 523476 523477 523478 523479 523480 523481 523482 523483 523484 523485 [...] (17628 flits)
Measured flits: 523476 523477 523478 523479 523480 523481 523482 523483 523484 523485 [...] (13545 flits)
Class 0:
Remaining flits: 533129 533130 533131 533132 533133 533134 533135 533136 533137 533138 [...] (17649 flits)
Measured flits: 533129 533130 533131 533132 533133 533134 533135 533136 533137 533138 [...] (12431 flits)
Class 0:
Remaining flits: 565587 565588 565589 565590 565591 565592 565593 565594 565595 569754 [...] (17320 flits)
Measured flits: 569754 569755 569756 569757 569758 569759 569760 569761 569762 569763 [...] (11085 flits)
Class 0:
Remaining flits: 589374 589375 589376 589377 589378 589379 589380 589381 589382 589383 [...] (17766 flits)
Measured flits: 589374 589375 589376 589377 589378 589379 589380 589381 589382 589383 [...] (10249 flits)
Class 0:
Remaining flits: 637164 637165 637166 637167 637168 637169 637170 637171 637172 637173 [...] (18023 flits)
Measured flits: 637164 637165 637166 637167 637168 637169 637170 637171 637172 637173 [...] (9368 flits)
Class 0:
Remaining flits: 666954 666955 666956 666957 666958 666959 666960 666961 666962 666963 [...] (17809 flits)
Measured flits: 666954 666955 666956 666957 666958 666959 666960 666961 666962 666963 [...] (7781 flits)
Class 0:
Remaining flits: 700740 700741 700742 700743 700744 700745 700746 700747 700748 700749 [...] (17550 flits)
Measured flits: 700740 700741 700742 700743 700744 700745 700746 700747 700748 700749 [...] (5840 flits)
Class 0:
Remaining flits: 728100 728101 728102 728103 728104 728105 728106 728107 728108 728109 [...] (17857 flits)
Measured flits: 728100 728101 728102 728103 728104 728105 728106 728107 728108 728109 [...] (5219 flits)
Class 0:
Remaining flits: 757134 757135 757136 757137 757138 757139 757140 757141 757142 757143 [...] (17926 flits)
Measured flits: 762613 762614 762615 762616 762617 762618 762619 762620 762621 762622 [...] (4334 flits)
Class 0:
Remaining flits: 789210 789211 789212 789213 789214 789215 789216 789217 789218 789219 [...] (18126 flits)
Measured flits: 798930 798931 798932 798933 798934 798935 798936 798937 798938 798939 [...] (3143 flits)
Class 0:
Remaining flits: 798930 798931 798932 798933 798934 798935 798936 798937 798938 798939 [...] (17704 flits)
Measured flits: 798930 798931 798932 798933 798934 798935 798936 798937 798938 798939 [...] (2195 flits)
Class 0:
Remaining flits: 802314 802315 802316 802317 802318 802319 802320 802321 802322 802323 [...] (17907 flits)
Measured flits: 802314 802315 802316 802317 802318 802319 802320 802321 802322 802323 [...] (2259 flits)
Class 0:
Remaining flits: 813456 813457 813458 813459 813460 813461 813462 813463 813464 813465 [...] (17710 flits)
Measured flits: 813456 813457 813458 813459 813460 813461 813462 813463 813464 813465 [...] (1561 flits)
Class 0:
Remaining flits: 909756 909757 909758 909759 909760 909761 909762 909763 909764 909765 [...] (17435 flits)
Measured flits: 930960 930961 930962 930963 930964 930965 930966 930967 930968 930969 [...] (1078 flits)
Class 0:
Remaining flits: 938411 939474 939475 939476 939477 939478 939479 939480 939481 939482 [...] (17691 flits)
Measured flits: 957052 957053 957054 957055 957056 957057 957058 957059 960181 960182 [...] (758 flits)
Class 0:
Remaining flits: 957508 957509 967979 967980 967981 967982 967983 967984 967985 972252 [...] (17887 flits)
Measured flits: 993888 993889 993890 993891 993892 993893 993894 993895 993896 993897 [...] (598 flits)
Class 0:
Remaining flits: 986107 986108 986109 986110 986111 1000080 1000081 1000082 1000083 1000084 [...] (17589 flits)
Measured flits: 1037178 1037179 1037180 1037181 1037182 1037183 1037184 1037185 1037186 1037187 [...] (297 flits)
Class 0:
Remaining flits: 1036875 1036876 1036877 1036878 1036879 1036880 1036881 1036882 1036883 1036884 [...] (17556 flits)
Measured flits: 1076543 1077701 1077702 1077703 1077704 1077705 1077706 1077707 1077708 1077709 [...] (86 flits)
Class 0:
Remaining flits: 1047024 1047025 1047026 1047027 1047028 1047029 1047030 1047031 1047032 1047033 [...] (17734 flits)
Measured flits: 1117134 1117135 1117136 1117137 1117138 1117139 1117140 1117141 1117142 1117143 [...] (90 flits)
Class 0:
Remaining flits: 1059768 1059769 1059770 1059771 1059772 1059773 1059774 1059775 1059776 1059777 [...] (17794 flits)
Measured flits: 1120716 1120717 1120718 1120719 1120720 1120721 1120722 1120723 1120724 1120725 [...] (108 flits)
Class 0:
Remaining flits: 1108512 1108513 1108514 1108515 1108516 1108517 1108518 1108519 1108520 1108521 [...] (17863 flits)
Measured flits: 1170828 1170829 1170830 1170831 1170832 1170833 1170834 1170835 1170836 1170837 [...] (118 flits)
Class 0:
Remaining flits: 1163877 1163878 1163879 1165410 1165411 1165412 1165413 1165414 1165415 1165416 [...] (18065 flits)
Measured flits: 1189746 1189747 1189748 1189749 1189750 1189751 1189752 1189753 1189754 1189755 [...] (90 flits)
Class 0:
Remaining flits: 1190574 1190575 1190576 1190577 1190578 1190579 1190580 1190581 1190582 1190583 [...] (17831 flits)
Measured flits: 1239352 1239353 1240272 1240273 1240274 1240275 1240276 1240277 1240278 1240279 [...] (38 flits)
Draining remaining packets ...
Time taken is 41217 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8051.19 (1 samples)
	minimum = 27 (1 samples)
	maximum = 30342 (1 samples)
Network latency average = 546.238 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4126 (1 samples)
Flit latency average = 486.615 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4087 (1 samples)
Fragmentation average = 58.7594 (1 samples)
	minimum = 0 (1 samples)
	maximum = 155 (1 samples)
Injected packet rate average = 0.00896949 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0134286 (1 samples)
Accepted packet rate average = 0.00892113 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0117143 (1 samples)
Injected flit rate average = 0.161454 (1 samples)
	minimum = 0.0872857 (1 samples)
	maximum = 0.240286 (1 samples)
Accepted flit rate average = 0.160727 (1 samples)
	minimum = 0.113429 (1 samples)
	maximum = 0.213143 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0164 (1 samples)
Hops average = 5.06883 (1 samples)
Total run time 27.4392
