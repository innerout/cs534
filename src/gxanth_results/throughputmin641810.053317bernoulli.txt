BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 415.63
	minimum = 23
	maximum = 987
Network latency average = 377.254
	minimum = 23
	maximum = 959
Slowest packet = 272
Flit latency average = 279.919
	minimum = 6
	maximum = 950
Slowest flit = 3616
Fragmentation average = 264.49
	minimum = 0
	maximum = 725
Injected packet rate average = 0.0307135
	minimum = 0.017 (at node 32)
	maximum = 0.04 (at node 81)
Accepted packet rate average = 0.009625
	minimum = 0.003 (at node 106)
	maximum = 0.017 (at node 131)
Injected flit rate average = 0.547458
	minimum = 0.304 (at node 32)
	maximum = 0.718 (at node 81)
Accepted flit rate average= 0.220984
	minimum = 0.112 (at node 30)
	maximum = 0.389 (at node 132)
Injected packet length average = 17.8247
Accepted packet length average = 22.9594
Total in-flight flits = 63789 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 821.847
	minimum = 23
	maximum = 1818
Network latency average = 690.466
	minimum = 23
	maximum = 1818
Slowest packet = 756
Flit latency average = 552.742
	minimum = 6
	maximum = 1905
Slowest flit = 9127
Fragmentation average = 325.764
	minimum = 0
	maximum = 1499
Injected packet rate average = 0.0219974
	minimum = 0.0105 (at node 168)
	maximum = 0.031 (at node 51)
Accepted packet rate average = 0.011026
	minimum = 0.0045 (at node 153)
	maximum = 0.0175 (at node 14)
Injected flit rate average = 0.391935
	minimum = 0.184 (at node 168)
	maximum = 0.5505 (at node 51)
Accepted flit rate average= 0.220562
	minimum = 0.106 (at node 153)
	maximum = 0.348 (at node 14)
Injected packet length average = 17.8173
Accepted packet length average = 20.0038
Total in-flight flits = 67440 (0 measured)
latency change    = 0.494273
throughput change = 0.00191272
Class 0:
Packet latency average = 1835.76
	minimum = 777
	maximum = 2882
Network latency average = 1249.3
	minimum = 25
	maximum = 2882
Slowest packet = 496
Flit latency average = 1109.86
	minimum = 6
	maximum = 2865
Slowest flit = 8945
Fragmentation average = 331.625
	minimum = 2
	maximum = 2587
Injected packet rate average = 0.00843229
	minimum = 0 (at node 1)
	maximum = 0.032 (at node 97)
Accepted packet rate average = 0.0121458
	minimum = 0.003 (at node 45)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.151557
	minimum = 0 (at node 1)
	maximum = 0.575 (at node 97)
Accepted flit rate average= 0.214156
	minimum = 0.065 (at node 45)
	maximum = 0.411 (at node 120)
Injected packet length average = 17.9734
Accepted packet length average = 17.6321
Total in-flight flits = 55590 (0 measured)
latency change    = 0.552313
throughput change = 0.0299139
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2559.06
	minimum = 1611
	maximum = 3535
Network latency average = 333.708
	minimum = 27
	maximum = 951
Slowest packet = 10079
Flit latency average = 1183.19
	minimum = 6
	maximum = 3891
Slowest flit = 14474
Fragmentation average = 150.641
	minimum = 0
	maximum = 818
Injected packet rate average = 0.00933854
	minimum = 0 (at node 0)
	maximum = 0.039 (at node 147)
Accepted packet rate average = 0.0117813
	minimum = 0.003 (at node 4)
	maximum = 0.023 (at node 167)
Injected flit rate average = 0.168438
	minimum = 0 (at node 1)
	maximum = 0.706 (at node 147)
Accepted flit rate average= 0.208151
	minimum = 0.063 (at node 4)
	maximum = 0.396 (at node 103)
Injected packet length average = 18.0368
Accepted packet length average = 17.668
Total in-flight flits = 47899 (19773 measured)
latency change    = 0.282643
throughput change = 0.0288502
Class 0:
Packet latency average = 3053.66
	minimum = 1611
	maximum = 4367
Network latency average = 510.774
	minimum = 25
	maximum = 1949
Slowest packet = 10079
Flit latency average = 1171.75
	minimum = 6
	maximum = 4949
Slowest flit = 5346
Fragmentation average = 184.234
	minimum = 0
	maximum = 1475
Injected packet rate average = 0.0103932
	minimum = 0 (at node 8)
	maximum = 0.036 (at node 147)
Accepted packet rate average = 0.0116432
	minimum = 0.005 (at node 113)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.18718
	minimum = 0 (at node 8)
	maximum = 0.6485 (at node 147)
Accepted flit rate average= 0.206604
	minimum = 0.0895 (at node 113)
	maximum = 0.3435 (at node 103)
Injected packet length average = 18.0098
Accepted packet length average = 17.7446
Total in-flight flits = 47984 (31818 measured)
latency change    = 0.161967
throughput change = 0.00748714
Class 0:
Packet latency average = 3507.26
	minimum = 1611
	maximum = 5453
Network latency average = 614.83
	minimum = 24
	maximum = 2841
Slowest packet = 10079
Flit latency average = 1132.18
	minimum = 6
	maximum = 5846
Slowest flit = 20630
Fragmentation average = 204.51
	minimum = 0
	maximum = 2269
Injected packet rate average = 0.0112031
	minimum = 0 (at node 22)
	maximum = 0.0363333 (at node 147)
Accepted packet rate average = 0.0116372
	minimum = 0.00666667 (at node 113)
	maximum = 0.0183333 (at node 129)
Injected flit rate average = 0.201712
	minimum = 0 (at node 22)
	maximum = 0.655667 (at node 147)
Accepted flit rate average= 0.208153
	minimum = 0.120667 (at node 113)
	maximum = 0.310333 (at node 129)
Injected packet length average = 18.005
Accepted packet length average = 17.8869
Total in-flight flits = 51740 (41243 measured)
latency change    = 0.129332
throughput change = 0.00743978
Draining remaining packets ...
Class 0:
Remaining flits: 6241 6242 6243 6244 6245 7920 7921 7922 7923 7924 [...] (18155 flits)
Measured flits: 181449 181450 181451 181452 181453 181454 181455 181456 181457 181739 [...] (13369 flits)
Class 0:
Remaining flits: 30306 30307 30308 30309 30310 30311 83477 83478 83479 83480 [...] (710 flits)
Measured flits: 211314 211315 211316 211317 211318 211319 214805 214806 214807 214808 [...] (635 flits)
Time taken is 8280 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4156.22 (1 samples)
	minimum = 1611 (1 samples)
	maximum = 7386 (1 samples)
Network latency average = 1026.91 (1 samples)
	minimum = 24 (1 samples)
	maximum = 4778 (1 samples)
Flit latency average = 1496.61 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7850 (1 samples)
Fragmentation average = 220.654 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3650 (1 samples)
Injected packet rate average = 0.0112031 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0363333 (1 samples)
Accepted packet rate average = 0.0116372 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.201712 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.655667 (1 samples)
Accepted flit rate average = 0.208153 (1 samples)
	minimum = 0.120667 (1 samples)
	maximum = 0.310333 (1 samples)
Injected packet size average = 18.005 (1 samples)
Accepted packet size average = 17.8869 (1 samples)
Hops average = 5.13215 (1 samples)
Total run time 11.5113
