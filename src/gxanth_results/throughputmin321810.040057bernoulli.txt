BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.164
	minimum = 23
	maximum = 952
Network latency average = 338.315
	minimum = 23
	maximum = 932
Slowest packet = 276
Flit latency average = 266.112
	minimum = 6
	maximum = 957
Slowest flit = 2246
Fragmentation average = 169.36
	minimum = 0
	maximum = 860
Injected packet rate average = 0.0220781
	minimum = 0.008 (at node 20)
	maximum = 0.036 (at node 137)
Accepted packet rate average = 0.010125
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.389453
	minimum = 0.135 (at node 20)
	maximum = 0.642 (at node 137)
Accepted flit rate average= 0.208417
	minimum = 0.086 (at node 41)
	maximum = 0.364 (at node 131)
Injected packet length average = 17.6398
Accepted packet length average = 20.5844
Total in-flight flits = 36430 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 722.742
	minimum = 23
	maximum = 1891
Network latency average = 568.507
	minimum = 23
	maximum = 1876
Slowest packet = 386
Flit latency average = 467.064
	minimum = 6
	maximum = 1896
Slowest flit = 11323
Fragmentation average = 192.465
	minimum = 0
	maximum = 1624
Injected packet rate average = 0.0165625
	minimum = 0.0055 (at node 16)
	maximum = 0.0275 (at node 191)
Accepted packet rate average = 0.0107318
	minimum = 0.0055 (at node 23)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.294518
	minimum = 0.0925 (at node 184)
	maximum = 0.4865 (at node 191)
Accepted flit rate average= 0.20438
	minimum = 0.11 (at node 164)
	maximum = 0.301 (at node 22)
Injected packet length average = 17.7822
Accepted packet length average = 19.0444
Total in-flight flits = 36376 (0 measured)
latency change    = 0.485067
throughput change = 0.0197498
Class 0:
Packet latency average = 1750.65
	minimum = 612
	maximum = 2935
Network latency average = 958.718
	minimum = 28
	maximum = 2844
Slowest packet = 629
Flit latency average = 826.728
	minimum = 6
	maximum = 2801
Slowest flit = 21323
Fragmentation average = 210.889
	minimum = 1
	maximum = 2671
Injected packet rate average = 0.01075
	minimum = 0 (at node 3)
	maximum = 0.028 (at node 11)
Accepted packet rate average = 0.010875
	minimum = 0.005 (at node 10)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.193635
	minimum = 0 (at node 3)
	maximum = 0.499 (at node 11)
Accepted flit rate average= 0.196057
	minimum = 0.096 (at node 184)
	maximum = 0.384 (at node 152)
Injected packet length average = 18.0126
Accepted packet length average = 18.0283
Total in-flight flits = 36011 (0 measured)
latency change    = 0.587157
throughput change = 0.0424515
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2466.9
	minimum = 934
	maximum = 3557
Network latency average = 358.735
	minimum = 27
	maximum = 976
Slowest packet = 8469
Flit latency average = 882.738
	minimum = 6
	maximum = 3684
Slowest flit = 26207
Fragmentation average = 125.065
	minimum = 0
	maximum = 778
Injected packet rate average = 0.0108229
	minimum = 0 (at node 2)
	maximum = 0.028 (at node 45)
Accepted packet rate average = 0.010724
	minimum = 0.005 (at node 7)
	maximum = 0.018 (at node 59)
Injected flit rate average = 0.195344
	minimum = 0 (at node 46)
	maximum = 0.501 (at node 45)
Accepted flit rate average= 0.192974
	minimum = 0.065 (at node 7)
	maximum = 0.336 (at node 59)
Injected packet length average = 18.0491
Accepted packet length average = 17.9947
Total in-flight flits = 36382 (24990 measured)
latency change    = 0.290345
throughput change = 0.015978
Class 0:
Packet latency average = 2957.65
	minimum = 934
	maximum = 4646
Network latency average = 595.388
	minimum = 24
	maximum = 1957
Slowest packet = 8469
Flit latency average = 902.73
	minimum = 6
	maximum = 4703
Slowest flit = 29393
Fragmentation average = 158.479
	minimum = 0
	maximum = 1384
Injected packet rate average = 0.0110391
	minimum = 0 (at node 93)
	maximum = 0.023 (at node 94)
Accepted packet rate average = 0.0107708
	minimum = 0.0065 (at node 85)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.19849
	minimum = 0.0005 (at node 120)
	maximum = 0.409 (at node 94)
Accepted flit rate average= 0.193112
	minimum = 0.1165 (at node 17)
	maximum = 0.33 (at node 129)
Injected packet length average = 17.9807
Accepted packet length average = 17.9292
Total in-flight flits = 38032 (34756 measured)
latency change    = 0.165926
throughput change = 0.000714719
Class 0:
Packet latency average = 3408.58
	minimum = 934
	maximum = 5304
Network latency average = 751.331
	minimum = 24
	maximum = 2985
Slowest packet = 8469
Flit latency average = 910.549
	minimum = 6
	maximum = 5094
Slowest flit = 30419
Fragmentation average = 173.203
	minimum = 0
	maximum = 2051
Injected packet rate average = 0.0107135
	minimum = 0 (at node 93)
	maximum = 0.0223333 (at node 94)
Accepted packet rate average = 0.0107431
	minimum = 0.00733333 (at node 35)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.192861
	minimum = 0.00433333 (at node 140)
	maximum = 0.4 (at node 143)
Accepted flit rate average= 0.193333
	minimum = 0.13 (at node 35)
	maximum = 0.291333 (at node 129)
Injected packet length average = 18.0016
Accepted packet length average = 17.9961
Total in-flight flits = 35657 (34373 measured)
latency change    = 0.132292
throughput change = 0.00114494
Draining remaining packets ...
Class 0:
Remaining flits: 32465 32466 32467 32468 32469 32470 32471 105488 105489 105490 [...] (4395 flits)
Measured flits: 155878 155879 160143 160144 160145 160857 160858 160859 160860 160861 [...] (4184 flits)
Time taken is 7595 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3880.7 (1 samples)
	minimum = 934 (1 samples)
	maximum = 6527 (1 samples)
Network latency average = 977.247 (1 samples)
	minimum = 24 (1 samples)
	maximum = 3893 (1 samples)
Flit latency average = 1026.25 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6736 (1 samples)
Fragmentation average = 179.575 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2444 (1 samples)
Injected packet rate average = 0.0107135 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0107431 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.192861 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.4 (1 samples)
Accepted flit rate average = 0.193333 (1 samples)
	minimum = 0.13 (1 samples)
	maximum = 0.291333 (1 samples)
Injected packet size average = 18.0016 (1 samples)
Accepted packet size average = 17.9961 (1 samples)
Hops average = 5.13297 (1 samples)
Total run time 9.63275
