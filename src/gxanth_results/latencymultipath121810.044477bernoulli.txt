BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 381.218
	minimum = 27
	maximum = 943
Network latency average = 301.997
	minimum = 23
	maximum = 890
Slowest packet = 454
Flit latency average = 255.685
	minimum = 6
	maximum = 910
Slowest flit = 10261
Fragmentation average = 56.1454
	minimum = 0
	maximum = 141
Injected packet rate average = 0.014026
	minimum = 0.004 (at node 116)
	maximum = 0.024 (at node 25)
Accepted packet rate average = 0.00898958
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248516
	minimum = 0.065 (at node 116)
	maximum = 0.432 (at node 113)
Accepted flit rate average= 0.168177
	minimum = 0.041 (at node 174)
	maximum = 0.289 (at node 48)
Injected packet length average = 17.7182
Accepted packet length average = 18.708
Total in-flight flits = 18164 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 768.465
	minimum = 27
	maximum = 1821
Network latency average = 419.854
	minimum = 23
	maximum = 1714
Slowest packet = 744
Flit latency average = 363.826
	minimum = 6
	maximum = 1693
Slowest flit = 22445
Fragmentation average = 58.4739
	minimum = 0
	maximum = 141
Injected packet rate average = 0.011513
	minimum = 0.0035 (at node 116)
	maximum = 0.0185 (at node 49)
Accepted packet rate average = 0.00898958
	minimum = 0.005 (at node 43)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.205422
	minimum = 0.063 (at node 116)
	maximum = 0.333 (at node 49)
Accepted flit rate average= 0.165138
	minimum = 0.09 (at node 43)
	maximum = 0.267 (at node 22)
Injected packet length average = 17.8426
Accepted packet length average = 18.3699
Total in-flight flits = 18001 (0 measured)
latency change    = 0.503923
throughput change = 0.0184032
Class 0:
Packet latency average = 1953.18
	minimum = 998
	maximum = 2753
Network latency average = 550.067
	minimum = 23
	maximum = 2249
Slowest packet = 916
Flit latency average = 488.239
	minimum = 6
	maximum = 2184
Slowest flit = 36179
Fragmentation average = 57.6899
	minimum = 0
	maximum = 133
Injected packet rate average = 0.00882292
	minimum = 0.001 (at node 2)
	maximum = 0.022 (at node 114)
Accepted packet rate average = 0.00898438
	minimum = 0.003 (at node 10)
	maximum = 0.017 (at node 174)
Injected flit rate average = 0.158615
	minimum = 0.005 (at node 98)
	maximum = 0.39 (at node 114)
Accepted flit rate average= 0.161016
	minimum = 0.042 (at node 153)
	maximum = 0.296 (at node 174)
Injected packet length average = 17.9776
Accepted packet length average = 17.9217
Total in-flight flits = 17686 (0 measured)
latency change    = 0.606556
throughput change = 0.0256025
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2818.45
	minimum = 1861
	maximum = 3530
Network latency average = 316.317
	minimum = 23
	maximum = 910
Slowest packet = 6223
Flit latency average = 480.78
	minimum = 6
	maximum = 2748
Slowest flit = 51407
Fragmentation average = 51.5374
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00894792
	minimum = 0 (at node 21)
	maximum = 0.022 (at node 99)
Accepted packet rate average = 0.00884375
	minimum = 0.001 (at node 184)
	maximum = 0.017 (at node 187)
Injected flit rate average = 0.1615
	minimum = 0.002 (at node 21)
	maximum = 0.393 (at node 99)
Accepted flit rate average= 0.161135
	minimum = 0.018 (at node 190)
	maximum = 0.307 (at node 59)
Injected packet length average = 18.0489
Accepted packet length average = 18.2203
Total in-flight flits = 17618 (15799 measured)
latency change    = 0.307004
throughput change = 0.000743422
Class 0:
Packet latency average = 3307.83
	minimum = 1861
	maximum = 4641
Network latency average = 435.797
	minimum = 23
	maximum = 1735
Slowest packet = 6223
Flit latency average = 480.817
	minimum = 6
	maximum = 2893
Slowest flit = 52218
Fragmentation average = 56.7238
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0090599
	minimum = 0.001 (at node 55)
	maximum = 0.017 (at node 79)
Accepted packet rate average = 0.00901302
	minimum = 0.0035 (at node 163)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.163224
	minimum = 0.018 (at node 55)
	maximum = 0.311 (at node 79)
Accepted flit rate average= 0.162464
	minimum = 0.0565 (at node 163)
	maximum = 0.316 (at node 16)
Injected packet length average = 18.0161
Accepted packet length average = 18.0254
Total in-flight flits = 17958 (17762 measured)
latency change    = 0.147944
throughput change = 0.00817491
Class 0:
Packet latency average = 3740.03
	minimum = 1861
	maximum = 5549
Network latency average = 485.516
	minimum = 23
	maximum = 2109
Slowest packet = 6223
Flit latency average = 489.679
	minimum = 6
	maximum = 3377
Slowest flit = 68292
Fragmentation average = 58.657
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00899132
	minimum = 0.00133333 (at node 54)
	maximum = 0.016 (at node 79)
Accepted packet rate average = 0.00897222
	minimum = 0.00533333 (at node 154)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.161764
	minimum = 0.024 (at node 54)
	maximum = 0.286333 (at node 79)
Accepted flit rate average= 0.161311
	minimum = 0.096 (at node 154)
	maximum = 0.283 (at node 16)
Injected packet length average = 17.9911
Accepted packet length average = 17.9789
Total in-flight flits = 17957 (17939 measured)
latency change    = 0.115562
throughput change = 0.00714632
Class 0:
Packet latency average = 4154.77
	minimum = 1861
	maximum = 6242
Network latency average = 506.022
	minimum = 23
	maximum = 3095
Slowest packet = 6223
Flit latency average = 490.441
	minimum = 6
	maximum = 3377
Slowest flit = 68292
Fragmentation average = 59.0377
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0089375
	minimum = 0.0035 (at node 75)
	maximum = 0.0145 (at node 183)
Accepted packet rate average = 0.00892057
	minimum = 0.00525 (at node 174)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.160798
	minimum = 0.066 (at node 54)
	maximum = 0.262 (at node 183)
Accepted flit rate average= 0.16063
	minimum = 0.0945 (at node 174)
	maximum = 0.2415 (at node 16)
Injected packet length average = 17.9914
Accepted packet length average = 18.0067
Total in-flight flits = 17748 (17748 measured)
latency change    = 0.0998226
throughput change = 0.00423678
Class 0:
Packet latency average = 4567.31
	minimum = 1861
	maximum = 7135
Network latency average = 517.646
	minimum = 23
	maximum = 3098
Slowest packet = 6223
Flit latency average = 491.408
	minimum = 6
	maximum = 3377
Slowest flit = 68292
Fragmentation average = 59.1534
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00891979
	minimum = 0.005 (at node 75)
	maximum = 0.0138 (at node 66)
Accepted packet rate average = 0.00891042
	minimum = 0.0058 (at node 163)
	maximum = 0.0128 (at node 16)
Injected flit rate average = 0.160608
	minimum = 0.091 (at node 75)
	maximum = 0.2508 (at node 145)
Accepted flit rate average= 0.16047
	minimum = 0.1054 (at node 163)
	maximum = 0.2336 (at node 129)
Injected packet length average = 18.0058
Accepted packet length average = 18.0092
Total in-flight flits = 17715 (17715 measured)
latency change    = 0.090324
throughput change = 0.000999669
Class 0:
Packet latency average = 4985.82
	minimum = 1861
	maximum = 7926
Network latency average = 525.768
	minimum = 23
	maximum = 3098
Slowest packet = 6223
Flit latency average = 491.588
	minimum = 6
	maximum = 3377
Slowest flit = 68292
Fragmentation average = 59.2738
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00898003
	minimum = 0.00566667 (at node 75)
	maximum = 0.0133333 (at node 72)
Accepted packet rate average = 0.00894444
	minimum = 0.00633333 (at node 36)
	maximum = 0.0121667 (at node 128)
Injected flit rate average = 0.161694
	minimum = 0.101167 (at node 75)
	maximum = 0.239833 (at node 183)
Accepted flit rate average= 0.161198
	minimum = 0.112833 (at node 36)
	maximum = 0.221167 (at node 128)
Injected packet length average = 18.006
Accepted packet length average = 18.0221
Total in-flight flits = 18232 (18232 measured)
latency change    = 0.0839398
throughput change = 0.00451696
Class 0:
Packet latency average = 5386.45
	minimum = 1861
	maximum = 8728
Network latency average = 530.262
	minimum = 23
	maximum = 3098
Slowest packet = 6223
Flit latency average = 491.914
	minimum = 6
	maximum = 3377
Slowest flit = 68292
Fragmentation average = 59.1161
	minimum = 0
	maximum = 147
Injected packet rate average = 0.00894345
	minimum = 0.00557143 (at node 75)
	maximum = 0.0131429 (at node 10)
Accepted packet rate average = 0.00893378
	minimum = 0.00628571 (at node 163)
	maximum = 0.0122857 (at node 129)
Injected flit rate average = 0.160967
	minimum = 0.101857 (at node 75)
	maximum = 0.238 (at node 10)
Accepted flit rate average= 0.160931
	minimum = 0.114714 (at node 36)
	maximum = 0.221714 (at node 129)
Injected packet length average = 17.9983
Accepted packet length average = 18.0137
Total in-flight flits = 17611 (17611 measured)
latency change    = 0.0743775
throughput change = 0.0016598
Draining all recorded packets ...
Class 0:
Remaining flits: 297306 297307 297308 297309 297310 297311 297312 297313 297314 297315 [...] (17403 flits)
Measured flits: 297306 297307 297308 297309 297310 297311 297312 297313 297314 297315 [...] (17403 flits)
Class 0:
Remaining flits: 312444 312445 312446 312447 312448 312449 312450 312451 312452 312453 [...] (17781 flits)
Measured flits: 312444 312445 312446 312447 312448 312449 312450 312451 312452 312453 [...] (17781 flits)
Class 0:
Remaining flits: 351191 351192 351193 351194 351195 351196 351197 354258 354259 354260 [...] (18159 flits)
Measured flits: 351191 351192 351193 351194 351195 351196 351197 354258 354259 354260 [...] (18159 flits)
Class 0:
Remaining flits: 395118 395119 395120 395121 395122 395123 395124 395125 395126 395127 [...] (17776 flits)
Measured flits: 395118 395119 395120 395121 395122 395123 395124 395125 395126 395127 [...] (17776 flits)
Class 0:
Remaining flits: 407934 407935 407936 407937 407938 407939 407940 407941 407942 407943 [...] (17899 flits)
Measured flits: 407934 407935 407936 407937 407938 407939 407940 407941 407942 407943 [...] (17899 flits)
Class 0:
Remaining flits: 434411 442080 442081 442082 442083 442084 442085 442086 442087 442088 [...] (17318 flits)
Measured flits: 434411 442080 442081 442082 442083 442084 442085 442086 442087 442088 [...] (17318 flits)
Class 0:
Remaining flits: 475250 475251 475252 475253 481456 481457 481458 481459 481460 481461 [...] (18047 flits)
Measured flits: 475250 475251 475252 475253 481456 481457 481458 481459 481460 481461 [...] (18047 flits)
Class 0:
Remaining flits: 505476 505477 505478 505479 505480 505481 505482 505483 505484 505485 [...] (17794 flits)
Measured flits: 505476 505477 505478 505479 505480 505481 505482 505483 505484 505485 [...] (17794 flits)
Class 0:
Remaining flits: 537038 537039 537040 537041 537042 537043 537044 537045 537046 537047 [...] (17442 flits)
Measured flits: 537038 537039 537040 537041 537042 537043 537044 537045 537046 537047 [...] (17442 flits)
Class 0:
Remaining flits: 574504 574505 578880 578881 578882 578883 578884 578885 578886 578887 [...] (17762 flits)
Measured flits: 574504 574505 578880 578881 578882 578883 578884 578885 578886 578887 [...] (17762 flits)
Class 0:
Remaining flits: 598140 598141 598142 598143 598144 598145 598146 598147 598148 598149 [...] (17661 flits)
Measured flits: 598140 598141 598142 598143 598144 598145 598146 598147 598148 598149 [...] (17661 flits)
Class 0:
Remaining flits: 611622 611623 611624 611625 611626 611627 611628 611629 611630 611631 [...] (17862 flits)
Measured flits: 611622 611623 611624 611625 611626 611627 611628 611629 611630 611631 [...] (17862 flits)
Class 0:
Remaining flits: 655088 655089 655090 655091 664560 664561 664562 664563 664564 664565 [...] (17505 flits)
Measured flits: 655088 655089 655090 655091 664560 664561 664562 664563 664564 664565 [...] (17505 flits)
Class 0:
Remaining flits: 673398 673399 673400 673401 673402 673403 673404 673405 673406 673407 [...] (17589 flits)
Measured flits: 673398 673399 673400 673401 673402 673403 673404 673405 673406 673407 [...] (17589 flits)
Class 0:
Remaining flits: 717164 717165 717166 717167 717168 717169 717170 717171 717172 717173 [...] (18103 flits)
Measured flits: 717164 717165 717166 717167 717168 717169 717170 717171 717172 717173 [...] (18103 flits)
Class 0:
Remaining flits: 754044 754045 754046 754047 754048 754049 754050 754051 754052 754053 [...] (17619 flits)
Measured flits: 754044 754045 754046 754047 754048 754049 754050 754051 754052 754053 [...] (17619 flits)
Class 0:
Remaining flits: 781666 781667 782230 782231 782232 782233 782234 782235 782236 782237 [...] (17919 flits)
Measured flits: 781666 781667 782230 782231 782232 782233 782234 782235 782236 782237 [...] (17919 flits)
Class 0:
Remaining flits: 819270 819271 819272 819273 819274 819275 819276 819277 819278 819279 [...] (17879 flits)
Measured flits: 819270 819271 819272 819273 819274 819275 819276 819277 819278 819279 [...] (17879 flits)
Class 0:
Remaining flits: 844632 844633 844634 844635 844636 844637 844638 844639 844640 844641 [...] (17871 flits)
Measured flits: 844632 844633 844634 844635 844636 844637 844638 844639 844640 844641 [...] (17871 flits)
Class 0:
Remaining flits: 874782 874783 874784 874785 874786 874787 874788 874789 874790 874791 [...] (17599 flits)
Measured flits: 874782 874783 874784 874785 874786 874787 874788 874789 874790 874791 [...] (17599 flits)
Class 0:
Remaining flits: 901011 901012 901013 901014 901015 901016 901017 901018 901019 901020 [...] (17477 flits)
Measured flits: 901011 901012 901013 901014 901015 901016 901017 901018 901019 901020 [...] (17477 flits)
Class 0:
Remaining flits: 930499 930500 930501 930502 930503 930504 930505 930506 930507 930508 [...] (18012 flits)
Measured flits: 930499 930500 930501 930502 930503 930504 930505 930506 930507 930508 [...] (18012 flits)
Class 0:
Remaining flits: 956556 956557 956558 956559 956560 956561 956562 956563 956564 956565 [...] (17598 flits)
Measured flits: 956556 956557 956558 956559 956560 956561 956562 956563 956564 956565 [...] (17598 flits)
Class 0:
Remaining flits: 995508 995509 995510 995511 995512 995513 995514 995515 995516 995517 [...] (17492 flits)
Measured flits: 995508 995509 995510 995511 995512 995513 995514 995515 995516 995517 [...] (17492 flits)
Class 0:
Remaining flits: 1023768 1023769 1023770 1023771 1023772 1023773 1023774 1023775 1023776 1023777 [...] (17673 flits)
Measured flits: 1023768 1023769 1023770 1023771 1023772 1023773 1023774 1023775 1023776 1023777 [...] (17673 flits)
Class 0:
Remaining flits: 1043514 1043515 1043516 1043517 1043518 1043519 1043520 1043521 1043522 1043523 [...] (17505 flits)
Measured flits: 1043514 1043515 1043516 1043517 1043518 1043519 1043520 1043521 1043522 1043523 [...] (17505 flits)
Class 0:
Remaining flits: 1063584 1063585 1063586 1063587 1063588 1063589 1063590 1063591 1063592 1063593 [...] (17990 flits)
Measured flits: 1063584 1063585 1063586 1063587 1063588 1063589 1063590 1063591 1063592 1063593 [...] (17972 flits)
Class 0:
Remaining flits: 1111518 1111519 1111520 1111521 1111522 1111523 1111524 1111525 1111526 1111527 [...] (17236 flits)
Measured flits: 1111518 1111519 1111520 1111521 1111522 1111523 1111524 1111525 1111526 1111527 [...] (17146 flits)
Class 0:
Remaining flits: 1145772 1145773 1145774 1145775 1145776 1145777 1145778 1145779 1145780 1145781 [...] (18077 flits)
Measured flits: 1145772 1145773 1145774 1145775 1145776 1145777 1145778 1145779 1145780 1145781 [...] (17936 flits)
Class 0:
Remaining flits: 1176516 1176517 1176518 1176519 1176520 1176521 1176522 1176523 1176524 1176525 [...] (17837 flits)
Measured flits: 1176516 1176517 1176518 1176519 1176520 1176521 1176522 1176523 1176524 1176525 [...] (17755 flits)
Class 0:
Remaining flits: 1189656 1189657 1189658 1189659 1189660 1189661 1189662 1189663 1189664 1189665 [...] (17774 flits)
Measured flits: 1189656 1189657 1189658 1189659 1189660 1189661 1189662 1189663 1189664 1189665 [...] (17656 flits)
Class 0:
Remaining flits: 1243902 1243903 1243904 1243905 1243906 1243907 1245870 1245871 1245872 1245873 [...] (17842 flits)
Measured flits: 1243902 1243903 1243904 1243905 1243906 1243907 1245870 1245871 1245872 1245873 [...] (17734 flits)
Class 0:
Remaining flits: 1272438 1272439 1272440 1272441 1272442 1272443 1272444 1272445 1272446 1272447 [...] (17657 flits)
Measured flits: 1272438 1272439 1272440 1272441 1272442 1272443 1272444 1272445 1272446 1272447 [...] (17273 flits)
Class 0:
Remaining flits: 1272438 1272439 1272440 1272441 1272442 1272443 1272444 1272445 1272446 1272447 [...] (17737 flits)
Measured flits: 1272438 1272439 1272440 1272441 1272442 1272443 1272444 1272445 1272446 1272447 [...] (16799 flits)
Class 0:
Remaining flits: 1312614 1312615 1312616 1312617 1312618 1312619 1312620 1312621 1312622 1312623 [...] (17668 flits)
Measured flits: 1312614 1312615 1312616 1312617 1312618 1312619 1312620 1312621 1312622 1312623 [...] (15870 flits)
Class 0:
Remaining flits: 1333482 1333483 1333484 1333485 1333486 1333487 1333488 1333489 1333490 1333491 [...] (17205 flits)
Measured flits: 1333482 1333483 1333484 1333485 1333486 1333487 1333488 1333489 1333490 1333491 [...] (14853 flits)
Class 0:
Remaining flits: 1388826 1388827 1388828 1388829 1388830 1388831 1388832 1388833 1388834 1388835 [...] (17826 flits)
Measured flits: 1388826 1388827 1388828 1388829 1388830 1388831 1388832 1388833 1388834 1388835 [...] (14049 flits)
Class 0:
Remaining flits: 1403738 1403739 1403740 1403741 1403742 1403743 1403744 1403745 1403746 1403747 [...] (17886 flits)
Measured flits: 1403738 1403739 1403740 1403741 1403742 1403743 1403744 1403745 1403746 1403747 [...] (12045 flits)
Class 0:
Remaining flits: 1458774 1458775 1458776 1458777 1458778 1458779 1458780 1458781 1458782 1458783 [...] (17502 flits)
Measured flits: 1462914 1462915 1462916 1462917 1462918 1462919 1462920 1462921 1462922 1462923 [...] (9610 flits)
Class 0:
Remaining flits: 1480392 1480393 1480394 1480395 1480396 1480397 1480398 1480399 1480400 1480401 [...] (17713 flits)
Measured flits: 1480392 1480393 1480394 1480395 1480396 1480397 1480398 1480399 1480400 1480401 [...] (7944 flits)
Class 0:
Remaining flits: 1522476 1522477 1522478 1522479 1522480 1522481 1522482 1522483 1522484 1522485 [...] (17598 flits)
Measured flits: 1522476 1522477 1522478 1522479 1522480 1522481 1522482 1522483 1522484 1522485 [...] (6588 flits)
Class 0:
Remaining flits: 1558362 1558363 1558364 1558365 1558366 1558367 1561345 1561346 1561347 1561348 [...] (17747 flits)
Measured flits: 1568502 1568503 1568504 1568505 1568506 1568507 1568508 1568509 1568510 1568511 [...] (4423 flits)
Class 0:
Remaining flits: 1586394 1586395 1586396 1586397 1586398 1586399 1586400 1586401 1586402 1586403 [...] (17714 flits)
Measured flits: 1586394 1586395 1586396 1586397 1586398 1586399 1586400 1586401 1586402 1586403 [...] (2933 flits)
Class 0:
Remaining flits: 1618830 1618831 1618832 1618833 1618834 1618835 1618836 1618837 1618838 1618839 [...] (17586 flits)
Measured flits: 1631412 1631413 1631414 1631415 1631416 1631417 1631418 1631419 1631420 1631421 [...] (2191 flits)
Class 0:
Remaining flits: 1645632 1645633 1645634 1645635 1645636 1645637 1645638 1645639 1645640 1645641 [...] (18008 flits)
Measured flits: 1669554 1669555 1669556 1669557 1669558 1669559 1669560 1669561 1669562 1669563 [...] (1521 flits)
Class 0:
Remaining flits: 1651860 1651861 1651862 1651863 1651864 1651865 1651866 1651867 1651868 1651869 [...] (17668 flits)
Measured flits: 1694250 1694251 1694252 1694253 1694254 1694255 1694256 1694257 1694258 1694259 [...] (936 flits)
Class 0:
Remaining flits: 1664136 1664137 1664138 1664139 1664140 1664141 1664142 1664143 1664144 1664145 [...] (17614 flits)
Measured flits: 1699374 1699375 1699376 1699377 1699378 1699379 1711116 1711117 1711118 1711119 [...] (569 flits)
Class 0:
Remaining flits: 1725462 1725463 1725464 1725465 1725466 1725467 1725468 1725469 1725470 1725471 [...] (18074 flits)
Measured flits: 1725462 1725463 1725464 1725465 1725466 1725467 1725468 1725469 1725470 1725471 [...] (306 flits)
Class 0:
Remaining flits: 1773738 1773739 1773740 1773741 1773742 1773743 1773744 1773745 1773746 1773747 [...] (17768 flits)
Measured flits: 1778126 1778127 1778128 1778129 1785582 1785583 1785584 1785585 1785586 1785587 [...] (160 flits)
Draining remaining packets ...
Time taken is 60551 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 21550.1 (1 samples)
	minimum = 1861 (1 samples)
	maximum = 49756 (1 samples)
Network latency average = 551.978 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3615 (1 samples)
Flit latency average = 490.933 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3527 (1 samples)
Fragmentation average = 58.5981 (1 samples)
	minimum = 0 (1 samples)
	maximum = 157 (1 samples)
Injected packet rate average = 0.00894345 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0131429 (1 samples)
Accepted packet rate average = 0.00893378 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.160967 (1 samples)
	minimum = 0.101857 (1 samples)
	maximum = 0.238 (1 samples)
Accepted flit rate average = 0.160931 (1 samples)
	minimum = 0.114714 (1 samples)
	maximum = 0.221714 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 18.0137 (1 samples)
Hops average = 5.07259 (1 samples)
Total run time 45.2042
