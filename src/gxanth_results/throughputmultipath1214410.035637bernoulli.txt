BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.501
	minimum = 23
	maximum = 905
Network latency average = 314.468
	minimum = 23
	maximum = 905
Slowest packet = 471
Flit latency average = 282.629
	minimum = 6
	maximum = 906
Slowest flit = 8721
Fragmentation average = 54.6271
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0325104
	minimum = 0.02 (at node 9)
	maximum = 0.049 (at node 42)
Accepted packet rate average = 0.010099
	minimum = 0.004 (at node 174)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.580078
	minimum = 0.36 (at node 9)
	maximum = 0.875 (at node 42)
Accepted flit rate average= 0.189448
	minimum = 0.086 (at node 174)
	maximum = 0.341 (at node 152)
Injected packet length average = 17.8428
Accepted packet length average = 18.7592
Total in-flight flits = 76144 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 656.231
	minimum = 23
	maximum = 1835
Network latency average = 629.004
	minimum = 23
	maximum = 1788
Slowest packet = 1208
Flit latency average = 593.936
	minimum = 6
	maximum = 1858
Slowest flit = 14050
Fragmentation average = 61.7327
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0280469
	minimum = 0.013 (at node 48)
	maximum = 0.0355 (at node 46)
Accepted packet rate average = 0.0100156
	minimum = 0.0055 (at node 83)
	maximum = 0.015 (at node 131)
Injected flit rate average = 0.502096
	minimum = 0.234 (at node 48)
	maximum = 0.6305 (at node 46)
Accepted flit rate average= 0.184229
	minimum = 0.099 (at node 83)
	maximum = 0.2755 (at node 131)
Injected packet length average = 17.902
Accepted packet length average = 18.3942
Total in-flight flits = 124088 (0 measured)
latency change    = 0.502461
throughput change = 0.0283275
Class 0:
Packet latency average = 1733.44
	minimum = 32
	maximum = 2802
Network latency average = 1620.1
	minimum = 27
	maximum = 2721
Slowest packet = 1532
Flit latency average = 1587.71
	minimum = 6
	maximum = 2751
Slowest flit = 28352
Fragmentation average = 71.4091
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0107292
	minimum = 0 (at node 128)
	maximum = 0.039 (at node 38)
Accepted packet rate average = 0.00945833
	minimum = 0.003 (at node 100)
	maximum = 0.019 (at node 51)
Injected flit rate average = 0.196005
	minimum = 0 (at node 128)
	maximum = 0.702 (at node 38)
Accepted flit rate average= 0.169859
	minimum = 0.059 (at node 4)
	maximum = 0.328 (at node 51)
Injected packet length average = 18.2684
Accepted packet length average = 17.9587
Total in-flight flits = 129905 (0 measured)
latency change    = 0.621428
throughput change = 0.0845982
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1836.91
	minimum = 47
	maximum = 3114
Network latency average = 186.231
	minimum = 27
	maximum = 985
Slowest packet = 12991
Flit latency average = 2237.74
	minimum = 6
	maximum = 3560
Slowest flit = 47177
Fragmentation average = 26.6923
	minimum = 0
	maximum = 119
Injected packet rate average = 0.00788021
	minimum = 0 (at node 29)
	maximum = 0.027 (at node 16)
Accepted packet rate average = 0.009
	minimum = 0.003 (at node 98)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.141578
	minimum = 0 (at node 29)
	maximum = 0.486 (at node 16)
Accepted flit rate average= 0.161109
	minimum = 0.066 (at node 71)
	maximum = 0.299 (at node 34)
Injected packet length average = 17.9663
Accepted packet length average = 17.901
Total in-flight flits = 126188 (26026 measured)
latency change    = 0.0563277
throughput change = 0.0543109
Class 0:
Packet latency average = 2612.75
	minimum = 47
	maximum = 4060
Network latency average = 551.906
	minimum = 23
	maximum = 1911
Slowest packet = 12991
Flit latency average = 2518.28
	minimum = 6
	maximum = 4514
Slowest flit = 41272
Fragmentation average = 42.1915
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00828125
	minimum = 0.0005 (at node 42)
	maximum = 0.0235 (at node 121)
Accepted packet rate average = 0.00896094
	minimum = 0.004 (at node 71)
	maximum = 0.014 (at node 160)
Injected flit rate average = 0.148898
	minimum = 0.009 (at node 84)
	maximum = 0.4205 (at node 121)
Accepted flit rate average= 0.161466
	minimum = 0.069 (at node 71)
	maximum = 0.25 (at node 160)
Injected packet length average = 17.9802
Accepted packet length average = 18.0189
Total in-flight flits = 125052 (52710 measured)
latency change    = 0.296946
throughput change = 0.00220957
Class 0:
Packet latency average = 3285.16
	minimum = 47
	maximum = 5045
Network latency average = 992.69
	minimum = 23
	maximum = 2966
Slowest packet = 12991
Flit latency average = 2753.97
	minimum = 6
	maximum = 5353
Slowest flit = 70577
Fragmentation average = 55.5488
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00824653
	minimum = 0.000666667 (at node 26)
	maximum = 0.0223333 (at node 121)
Accepted packet rate average = 0.0089375
	minimum = 0.00466667 (at node 36)
	maximum = 0.0136667 (at node 16)
Injected flit rate average = 0.148372
	minimum = 0.012 (at node 26)
	maximum = 0.400667 (at node 121)
Accepted flit rate average= 0.160925
	minimum = 0.0793333 (at node 36)
	maximum = 0.251667 (at node 16)
Injected packet length average = 17.992
Accepted packet length average = 18.0056
Total in-flight flits = 122712 (74410 measured)
latency change    = 0.20468
throughput change = 0.00336056
Class 0:
Packet latency average = 3888.25
	minimum = 47
	maximum = 6046
Network latency average = 1517.33
	minimum = 23
	maximum = 3938
Slowest packet = 12991
Flit latency average = 2981.02
	minimum = 6
	maximum = 6323
Slowest flit = 73259
Fragmentation average = 59.8292
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00835938
	minimum = 0.0015 (at node 28)
	maximum = 0.01775 (at node 121)
Accepted packet rate average = 0.00896354
	minimum = 0.0045 (at node 36)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.150434
	minimum = 0.027 (at node 28)
	maximum = 0.321 (at node 121)
Accepted flit rate average= 0.161355
	minimum = 0.0775 (at node 36)
	maximum = 0.2545 (at node 16)
Injected packet length average = 17.9958
Accepted packet length average = 18.0013
Total in-flight flits = 121508 (93342 measured)
latency change    = 0.155105
throughput change = 0.00266568
Draining remaining packets ...
Class 0:
Remaining flits: 81486 81487 81488 81489 81490 81491 81492 81493 81494 81495 [...] (91876 flits)
Measured flits: 233334 233335 233336 233337 233338 233339 233340 233341 233342 233343 [...] (77246 flits)
Class 0:
Remaining flits: 81486 81487 81488 81489 81490 81491 81492 81493 81494 81495 [...] (62598 flits)
Measured flits: 233334 233335 233336 233337 233338 233339 233340 233341 233342 233343 [...] (56151 flits)
Class 0:
Remaining flits: 97344 97345 97346 97347 97348 97349 97350 97351 97352 97353 [...] (33386 flits)
Measured flits: 233784 233785 233786 233787 233788 233789 233790 233791 233792 233793 [...] (30785 flits)
Class 0:
Remaining flits: 97344 97345 97346 97347 97348 97349 97350 97351 97352 97353 [...] (7683 flits)
Measured flits: 233910 233911 233912 233913 233914 233915 233916 233917 233918 233919 [...] (7087 flits)
Class 0:
Remaining flits: 250287 250288 250289 281070 281071 281072 281073 281074 281075 281076 [...] (68 flits)
Measured flits: 250287 250288 250289 281070 281071 281072 281073 281074 281075 281076 [...] (68 flits)
Time taken is 12075 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6266.11 (1 samples)
	minimum = 47 (1 samples)
	maximum = 9808 (1 samples)
Network latency average = 3589.46 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8522 (1 samples)
Flit latency average = 3729.38 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10490 (1 samples)
Fragmentation average = 66.1921 (1 samples)
	minimum = 0 (1 samples)
	maximum = 152 (1 samples)
Injected packet rate average = 0.00835938 (1 samples)
	minimum = 0.0015 (1 samples)
	maximum = 0.01775 (1 samples)
Accepted packet rate average = 0.00896354 (1 samples)
	minimum = 0.0045 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.150434 (1 samples)
	minimum = 0.027 (1 samples)
	maximum = 0.321 (1 samples)
Accepted flit rate average = 0.161355 (1 samples)
	minimum = 0.0775 (1 samples)
	maximum = 0.2545 (1 samples)
Injected packet size average = 17.9958 (1 samples)
Accepted packet size average = 18.0013 (1 samples)
Hops average = 5.07276 (1 samples)
Total run time 8.22349
