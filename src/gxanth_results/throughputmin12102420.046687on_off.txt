BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 350.038
	minimum = 22
	maximum = 970
Network latency average = 233.593
	minimum = 22
	maximum = 765
Slowest packet = 46
Flit latency average = 209.953
	minimum = 5
	maximum = 756
Slowest flit = 21175
Fragmentation average = 19.3001
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 150)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.261396
	minimum = 0.124 (at node 150)
	maximum = 0.433 (at node 49)
Injected packet length average = 17.8285
Accepted packet length average = 18.3704
Total in-flight flits = 52646 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 616.141
	minimum = 22
	maximum = 1917
Network latency average = 446.193
	minimum = 22
	maximum = 1521
Slowest packet = 46
Flit latency average = 423.338
	minimum = 5
	maximum = 1581
Slowest flit = 37366
Fragmentation average = 20.8235
	minimum = 0
	maximum = 106
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0150807
	minimum = 0.009 (at node 153)
	maximum = 0.022 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.27444
	minimum = 0.162 (at node 153)
	maximum = 0.396 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.1981
Total in-flight flits = 111389 (0 measured)
latency change    = 0.431885
throughput change = 0.0475305
Class 0:
Packet latency average = 1348.71
	minimum = 25
	maximum = 2809
Network latency average = 1065.08
	minimum = 22
	maximum = 2317
Slowest packet = 5974
Flit latency average = 1047.99
	minimum = 5
	maximum = 2300
Slowest flit = 62654
Fragmentation average = 22.0766
	minimum = 0
	maximum = 106
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0165156
	minimum = 0.008 (at node 64)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.29751
	minimum = 0.134 (at node 64)
	maximum = 0.546 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.0139
Total in-flight flits = 176433 (0 measured)
latency change    = 0.543162
throughput change = 0.0775446
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 430.716
	minimum = 22
	maximum = 2109
Network latency average = 42.34
	minimum = 22
	maximum = 531
Slowest packet = 18852
Flit latency average = 1493.3
	minimum = 5
	maximum = 2956
Slowest flit = 105605
Fragmentation average = 4.74909
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0366615
	minimum = 0.002 (at node 147)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.016526
	minimum = 0.006 (at node 184)
	maximum = 0.03 (at node 128)
Injected flit rate average = 0.660062
	minimum = 0.033 (at node 147)
	maximum = 1 (at node 12)
Accepted flit rate average= 0.297974
	minimum = 0.108 (at node 184)
	maximum = 0.54 (at node 128)
Injected packet length average = 18.0043
Accepted packet length average = 18.0306
Total in-flight flits = 245924 (116608 measured)
latency change    = 2.13131
throughput change = 0.00155564
Class 0:
Packet latency average = 512.233
	minimum = 22
	maximum = 2752
Network latency average = 107.5
	minimum = 22
	maximum = 1961
Slowest packet = 18852
Flit latency average = 1726.54
	minimum = 5
	maximum = 3739
Slowest flit = 126665
Fragmentation average = 5.65606
	minimum = 0
	maximum = 81
Injected packet rate average = 0.0369609
	minimum = 0.0085 (at node 112)
	maximum = 0.056 (at node 90)
Accepted packet rate average = 0.0164844
	minimum = 0.0105 (at node 5)
	maximum = 0.0235 (at node 128)
Injected flit rate average = 0.665266
	minimum = 0.153 (at node 112)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.296914
	minimum = 0.189 (at node 5)
	maximum = 0.423 (at node 128)
Injected packet length average = 17.9992
Accepted packet length average = 18.0118
Total in-flight flits = 317892 (234334 measured)
latency change    = 0.15914
throughput change = 0.00356971
Class 0:
Packet latency average = 943.179
	minimum = 22
	maximum = 3671
Network latency average = 558.87
	minimum = 22
	maximum = 2969
Slowest packet = 18852
Flit latency average = 1956.79
	minimum = 5
	maximum = 4478
Slowest flit = 153053
Fragmentation average = 9.68819
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0370694
	minimum = 0.0116667 (at node 81)
	maximum = 0.0556667 (at node 57)
Accepted packet rate average = 0.0164931
	minimum = 0.0103333 (at node 79)
	maximum = 0.0226667 (at node 165)
Injected flit rate average = 0.66766
	minimum = 0.215 (at node 81)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.296979
	minimum = 0.186 (at node 79)
	maximum = 0.406 (at node 138)
Injected packet length average = 18.0111
Accepted packet length average = 18.0063
Total in-flight flits = 389709 (344558 measured)
latency change    = 0.456908
throughput change = 0.000219221
Draining remaining packets ...
Class 0:
Remaining flits: 192112 192113 194004 194005 194006 194007 194008 194009 194010 194011 [...] (341235 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (319837 flits)
Class 0:
Remaining flits: 235188 235189 235190 235191 235192 235193 235194 235195 235196 235197 [...] (293552 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (284817 flits)
Class 0:
Remaining flits: 272646 272647 272648 272649 272650 272651 272652 272653 272654 272655 [...] (245829 flits)
Measured flits: 339462 339463 339464 339465 339466 339467 339468 339469 339470 339471 [...] (243378 flits)
Class 0:
Remaining flits: 317952 317953 317954 317955 317956 317957 317958 317959 317960 317961 [...] (198395 flits)
Measured flits: 339588 339589 339590 339591 339592 339593 339594 339595 339596 339597 [...] (198031 flits)
Class 0:
Remaining flits: 352926 352927 352928 352929 352930 352931 352932 352933 352934 352935 [...] (151282 flits)
Measured flits: 352926 352927 352928 352929 352930 352931 352932 352933 352934 352935 [...] (151282 flits)
Class 0:
Remaining flits: 360918 360919 360920 360921 360922 360923 360924 360925 360926 360927 [...] (104682 flits)
Measured flits: 360918 360919 360920 360921 360922 360923 360924 360925 360926 360927 [...] (104682 flits)
Class 0:
Remaining flits: 406603 406604 406605 406606 406607 406608 406609 406610 406611 406612 [...] (61209 flits)
Measured flits: 406603 406604 406605 406606 406607 406608 406609 406610 406611 406612 [...] (61209 flits)
Class 0:
Remaining flits: 493062 493063 493064 493065 493066 493067 493068 493069 493070 493071 [...] (28357 flits)
Measured flits: 493062 493063 493064 493065 493066 493067 493068 493069 493070 493071 [...] (28357 flits)
Class 0:
Remaining flits: 542534 542535 542536 542537 549522 549523 549524 549525 549526 549527 [...] (11715 flits)
Measured flits: 542534 542535 542536 542537 549522 549523 549524 549525 549526 549527 [...] (11715 flits)
Class 0:
Remaining flits: 610488 610489 610490 610491 610492 610493 610494 610495 610496 610497 [...] (3344 flits)
Measured flits: 610488 610489 610490 610491 610492 610493 610494 610495 610496 610497 [...] (3344 flits)
Class 0:
Remaining flits: 687939 687940 687941 694818 694819 694820 694821 694822 694823 694824 [...] (201 flits)
Measured flits: 687939 687940 687941 694818 694819 694820 694821 694822 694823 694824 [...] (201 flits)
Time taken is 17252 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5946.48 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13755 (1 samples)
Network latency average = 5492.95 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11299 (1 samples)
Flit latency average = 4668.79 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11282 (1 samples)
Fragmentation average = 24.4277 (1 samples)
	minimum = 0 (1 samples)
	maximum = 258 (1 samples)
Injected packet rate average = 0.0370694 (1 samples)
	minimum = 0.0116667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0164931 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.66766 (1 samples)
	minimum = 0.215 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.296979 (1 samples)
	minimum = 0.186 (1 samples)
	maximum = 0.406 (1 samples)
Injected packet size average = 18.0111 (1 samples)
Accepted packet size average = 18.0063 (1 samples)
Hops average = 5.09236 (1 samples)
Total run time 14.5267
