BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.091
	minimum = 23
	maximum = 977
Network latency average = 268.121
	minimum = 23
	maximum = 944
Slowest packet = 85
Flit latency average = 194.435
	minimum = 6
	maximum = 960
Slowest flit = 2831
Fragmentation average = 156.482
	minimum = 0
	maximum = 798
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.00986458
	minimum = 0.002 (at node 93)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.206818
	minimum = 0.072 (at node 150)
	maximum = 0.36 (at node 44)
Injected packet length average = 17.8353
Accepted packet length average = 20.9657
Total in-flight flits = 43991 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.172
	minimum = 23
	maximum = 1961
Network latency average = 455.798
	minimum = 23
	maximum = 1802
Slowest packet = 85
Flit latency average = 360.907
	minimum = 6
	maximum = 1941
Slowest flit = 3307
Fragmentation average = 219.747
	minimum = 0
	maximum = 1396
Injected packet rate average = 0.0244427
	minimum = 0.002 (at node 174)
	maximum = 0.055 (at node 91)
Accepted packet rate average = 0.0110885
	minimum = 0.0055 (at node 150)
	maximum = 0.016 (at node 51)
Injected flit rate average = 0.438031
	minimum = 0.036 (at node 174)
	maximum = 0.99 (at node 91)
Accepted flit rate average= 0.219435
	minimum = 0.122 (at node 83)
	maximum = 0.3125 (at node 51)
Injected packet length average = 17.9207
Accepted packet length average = 19.7893
Total in-flight flits = 84685 (0 measured)
latency change    = 0.380979
throughput change = 0.0574985
Class 0:
Packet latency average = 1105.44
	minimum = 29
	maximum = 2960
Network latency average = 952.74
	minimum = 26
	maximum = 2871
Slowest packet = 231
Flit latency average = 854.631
	minimum = 6
	maximum = 2901
Slowest flit = 6733
Fragmentation average = 322.226
	minimum = 0
	maximum = 2403
Injected packet rate average = 0.0250104
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.0131562
	minimum = 0.005 (at node 87)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.450036
	minimum = 0 (at node 100)
	maximum = 1 (at node 15)
Accepted flit rate average= 0.242297
	minimum = 0.101 (at node 79)
	maximum = 0.392 (at node 44)
Injected packet length average = 17.994
Accepted packet length average = 18.4169
Total in-flight flits = 124600 (0 measured)
latency change    = 0.486928
throughput change = 0.0943552
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 366.44
	minimum = 32
	maximum = 2040
Network latency average = 190.515
	minimum = 23
	maximum = 925
Slowest packet = 14245
Flit latency average = 1262.77
	minimum = 6
	maximum = 3878
Slowest flit = 9116
Fragmentation average = 98.3143
	minimum = 0
	maximum = 825
Injected packet rate average = 0.0251563
	minimum = 0 (at node 121)
	maximum = 0.056 (at node 98)
Accepted packet rate average = 0.0132969
	minimum = 0.005 (at node 146)
	maximum = 0.023 (at node 33)
Injected flit rate average = 0.452573
	minimum = 0 (at node 121)
	maximum = 1 (at node 85)
Accepted flit rate average= 0.242047
	minimum = 0.099 (at node 32)
	maximum = 0.424 (at node 182)
Injected packet length average = 17.9905
Accepted packet length average = 18.2033
Total in-flight flits = 165067 (76011 measured)
latency change    = 2.01671
throughput change = 0.00103286
Class 0:
Packet latency average = 686.578
	minimum = 31
	maximum = 2537
Network latency average = 514.449
	minimum = 23
	maximum = 1958
Slowest packet = 14245
Flit latency average = 1464.59
	minimum = 6
	maximum = 4797
Slowest flit = 15202
Fragmentation average = 187.084
	minimum = 0
	maximum = 1439
Injected packet rate average = 0.025125
	minimum = 0.0045 (at node 53)
	maximum = 0.0555 (at node 101)
Accepted packet rate average = 0.0132422
	minimum = 0.0075 (at node 36)
	maximum = 0.021 (at node 78)
Injected flit rate average = 0.452253
	minimum = 0.081 (at node 53)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.240198
	minimum = 0.141 (at node 36)
	maximum = 0.3535 (at node 182)
Injected packet length average = 18.0001
Accepted packet length average = 18.1388
Total in-flight flits = 206028 (142281 measured)
latency change    = 0.466281
throughput change = 0.00769765
Class 0:
Packet latency average = 1046.13
	minimum = 27
	maximum = 3829
Network latency average = 870.22
	minimum = 23
	maximum = 2991
Slowest packet = 14245
Flit latency average = 1680.98
	minimum = 6
	maximum = 5689
Slowest flit = 19645
Fragmentation average = 233.424
	minimum = 0
	maximum = 2064
Injected packet rate average = 0.0247865
	minimum = 0.00733333 (at node 38)
	maximum = 0.0523333 (at node 102)
Accepted packet rate average = 0.0131458
	minimum = 0.008 (at node 36)
	maximum = 0.0193333 (at node 129)
Injected flit rate average = 0.445943
	minimum = 0.132 (at node 38)
	maximum = 0.945667 (at node 102)
Accepted flit rate average= 0.238387
	minimum = 0.156333 (at node 36)
	maximum = 0.335667 (at node 129)
Injected packet length average = 17.9914
Accepted packet length average = 18.134
Total in-flight flits = 244275 (199114 measured)
latency change    = 0.343695
throughput change = 0.0075959
Class 0:
Packet latency average = 1447.72
	minimum = 27
	maximum = 5004
Network latency average = 1257
	minimum = 23
	maximum = 3929
Slowest packet = 14245
Flit latency average = 1906.77
	minimum = 6
	maximum = 6534
Slowest flit = 13512
Fragmentation average = 267.255
	minimum = 0
	maximum = 3336
Injected packet rate average = 0.0245195
	minimum = 0.00725 (at node 53)
	maximum = 0.0525 (at node 101)
Accepted packet rate average = 0.0130638
	minimum = 0.00925 (at node 17)
	maximum = 0.0175 (at node 78)
Injected flit rate average = 0.441299
	minimum = 0.1305 (at node 53)
	maximum = 0.9415 (at node 101)
Accepted flit rate average= 0.23653
	minimum = 0.173 (at node 140)
	maximum = 0.32175 (at node 181)
Injected packet length average = 17.9979
Accepted packet length average = 18.1058
Total in-flight flits = 281903 (250022 measured)
latency change    = 0.277398
throughput change = 0.00785188
Class 0:
Packet latency average = 1842.13
	minimum = 27
	maximum = 6247
Network latency average = 1630.18
	minimum = 23
	maximum = 4951
Slowest packet = 14245
Flit latency average = 2132.42
	minimum = 6
	maximum = 7921
Slowest flit = 3632
Fragmentation average = 289.464
	minimum = 0
	maximum = 4153
Injected packet rate average = 0.0243406
	minimum = 0.0116 (at node 9)
	maximum = 0.0486 (at node 101)
Accepted packet rate average = 0.0129927
	minimum = 0.0096 (at node 36)
	maximum = 0.0176 (at node 103)
Injected flit rate average = 0.43811
	minimum = 0.2066 (at node 9)
	maximum = 0.8746 (at node 101)
Accepted flit rate average= 0.234789
	minimum = 0.1784 (at node 143)
	maximum = 0.3196 (at node 103)
Injected packet length average = 17.9991
Accepted packet length average = 18.0708
Total in-flight flits = 319809 (297056 measured)
latency change    = 0.214104
throughput change = 0.00741691
Class 0:
Packet latency average = 2224.98
	minimum = 27
	maximum = 7195
Network latency average = 1989.96
	minimum = 23
	maximum = 5907
Slowest packet = 14245
Flit latency average = 2355.79
	minimum = 6
	maximum = 8596
Slowest flit = 31729
Fragmentation average = 302.547
	minimum = 0
	maximum = 4171
Injected packet rate average = 0.0242396
	minimum = 0.0116667 (at node 139)
	maximum = 0.0436667 (at node 101)
Accepted packet rate average = 0.0129158
	minimum = 0.00916667 (at node 144)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.436156
	minimum = 0.21 (at node 139)
	maximum = 0.786333 (at node 101)
Accepted flit rate average= 0.233208
	minimum = 0.170167 (at node 144)
	maximum = 0.303167 (at node 181)
Injected packet length average = 17.9936
Accepted packet length average = 18.0561
Total in-flight flits = 358594 (342140 measured)
latency change    = 0.172068
throughput change = 0.00677595
Class 0:
Packet latency average = 2581.34
	minimum = 26
	maximum = 7779
Network latency average = 2317.44
	minimum = 23
	maximum = 6814
Slowest packet = 14245
Flit latency average = 2568.23
	minimum = 6
	maximum = 9452
Slowest flit = 43212
Fragmentation average = 312.796
	minimum = 0
	maximum = 4734
Injected packet rate average = 0.024125
	minimum = 0.0124286 (at node 56)
	maximum = 0.0397143 (at node 30)
Accepted packet rate average = 0.0128333
	minimum = 0.00957143 (at node 143)
	maximum = 0.0165714 (at node 151)
Injected flit rate average = 0.434154
	minimum = 0.224286 (at node 56)
	maximum = 0.714571 (at node 30)
Accepted flit rate average= 0.231551
	minimum = 0.170429 (at node 143)
	maximum = 0.304 (at node 151)
Injected packet length average = 17.996
Accepted packet length average = 18.043
Total in-flight flits = 397063 (384752 measured)
latency change    = 0.138055
throughput change = 0.00715605
Draining all recorded packets ...
Class 0:
Remaining flits: 21148 21149 32526 32527 32528 32529 32530 32531 32532 32533 [...] (433726 flits)
Measured flits: 255404 255405 255406 255407 255408 255409 255410 255411 255412 255413 [...] (376310 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (467699 flits)
Measured flits: 255708 255709 255710 255711 255712 255713 255714 255715 255716 255717 [...] (354796 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (496724 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (331101 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (531244 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (307432 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (559824 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (284334 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (589058 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (260480 flits)
Class 0:
Remaining flits: 35190 35191 35192 35193 35194 35195 35196 35197 35198 35199 [...] (614572 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (236545 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (636016 flits)
Measured flits: 257562 257563 257564 257565 257566 257567 257568 257569 257570 257571 [...] (214187 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (658394 flits)
Measured flits: 257562 257563 257564 257565 257566 257567 257568 257569 257570 257571 [...] (194400 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (679164 flits)
Measured flits: 257562 257563 257564 257565 257566 257567 257568 257569 257570 257571 [...] (175426 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (694127 flits)
Measured flits: 257562 257563 257564 257565 257566 257567 257568 257569 257570 257571 [...] (157923 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (706163 flits)
Measured flits: 257562 257563 257564 257565 257566 257567 257568 257569 257570 257571 [...] (141322 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (718140 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (126457 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (719365 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (112875 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (719786 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (99170 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (722792 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (87761 flits)
Class 0:
Remaining flits: 65298 65299 65300 65301 65302 65303 117468 117469 117470 117471 [...] (724189 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (77129 flits)
Class 0:
Remaining flits: 117468 117469 117470 117471 117472 117473 117474 117475 117476 117477 [...] (726438 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (67996 flits)
Class 0:
Remaining flits: 117468 117469 117470 117471 117472 117473 117474 117475 117476 117477 [...] (724549 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (60219 flits)
Class 0:
Remaining flits: 117468 117469 117470 117471 117472 117473 117474 117475 117476 117477 [...] (722499 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (53256 flits)
Class 0:
Remaining flits: 117468 117469 117470 117471 117472 117473 117474 117475 117476 117477 [...] (722208 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (46733 flits)
Class 0:
Remaining flits: 117468 117469 117470 117471 117472 117473 117474 117475 117476 117477 [...] (719265 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (41183 flits)
Class 0:
Remaining flits: 117472 117473 117474 117475 117476 117477 117478 117479 117480 117481 [...] (721725 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (36287 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (717297 flits)
Measured flits: 258930 258931 258932 258933 258934 258935 258936 258937 258938 258939 [...] (31820 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (715420 flits)
Measured flits: 258932 258933 258934 258935 258936 258937 258938 258939 258940 258941 [...] (27460 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (713808 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (23926 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (713040 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (21025 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (708804 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (18531 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (707088 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (15784 flits)
Class 0:
Remaining flits: 127800 127801 127802 127803 127804 127805 127806 127807 127808 127809 [...] (706601 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (13554 flits)
Class 0:
Remaining flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (707877 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (11717 flits)
Class 0:
Remaining flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (711109 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (10201 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (714066 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (8777 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (713800 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (7848 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (713524 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (6842 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (714898 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (5921 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (719630 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (5266 flits)
Class 0:
Remaining flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (721386 flits)
Measured flits: 272592 272593 272594 272595 272596 272597 272598 272599 272600 272601 [...] (4643 flits)
Class 0:
Remaining flits: 316242 316243 316244 316245 316246 316247 316248 316249 316250 316251 [...] (720540 flits)
Measured flits: 316242 316243 316244 316245 316246 316247 316248 316249 316250 316251 [...] (4058 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (720009 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (3592 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (717360 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (3024 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (714781 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (2650 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (711339 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (2402 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (708316 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (2136 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (706138 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1920 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (705882 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1824 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (705982 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1666 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (706433 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1556 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (707268 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1274 flits)
Class 0:
Remaining flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (705219 flits)
Measured flits: 394956 394957 394958 394959 394960 394961 394962 394963 394964 394965 [...] (1123 flits)
Class 0:
Remaining flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (704332 flits)
Measured flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (961 flits)
Class 0:
Remaining flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (702247 flits)
Measured flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (880 flits)
Class 0:
Remaining flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (704432 flits)
Measured flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (743 flits)
Class 0:
Remaining flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (702510 flits)
Measured flits: 394963 394964 394965 394966 394967 394968 394969 394970 394971 394972 [...] (668 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (698910 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (521 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (695003 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (414 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (696173 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (371 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (695712 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (367 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (697026 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (360 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (699542 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (332 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (700563 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (302 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (696576 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (216 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (687597 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (200 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (682591 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (185 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (680971 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (144 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (686273 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (144 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (684445 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (126 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (682893 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (123 flits)
Class 0:
Remaining flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (682014 flits)
Measured flits: 406332 406333 406334 406335 406336 406337 406338 406339 406340 406341 [...] (108 flits)
Class 0:
Remaining flits: 406346 406347 406348 406349 800370 800371 800372 800373 800374 800375 [...] (686812 flits)
Measured flits: 406346 406347 406348 406349 800370 800371 800372 800373 800374 800375 [...] (94 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (690729 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (72 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (691658 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (54 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (696179 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (54 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (696784 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (54 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (695410 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (54 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (692682 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (690406 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (686341 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (684764 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (681482 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (677221 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (18 flits)
Class 0:
Remaining flits: 800377 800378 800379 800380 800381 800382 800383 800384 800385 800386 [...] (676754 flits)
Measured flits: 800377 800378 800379 800380 800381 800382 800383 800384 800385 800386 [...] (11 flits)
Class 0:
Remaining flits: 800387 1267650 1267651 1267652 1267653 1267654 1267655 1267656 1267657 1267658 [...] (680014 flits)
Measured flits: 800387 (1 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (646861 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (613481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (580336 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (547233 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (513278 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (479902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1388736 1388737 1388738 1388739 1388740 1388741 1388742 1388743 1388744 1388745 [...] (446944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1579356 1579357 1579358 1579359 1579360 1579361 1579362 1579363 1579364 1579365 [...] (414144 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1579357 1579358 1579359 1579360 1579361 1579362 1579363 1579364 1579365 1579366 [...] (381123 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (347655 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (315364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (283814 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (253126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (223908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (194465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (166038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (138150 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (111326 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613646 1613647 1613648 1613649 1613650 1613651 1613652 1613653 1613654 1613655 [...] (85369 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1613662 1613663 1802376 1802377 1802378 1802379 1802380 1802381 1802382 1802383 [...] (61421 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1871226 1871227 1871228 1871229 1871230 1871231 1871232 1871233 1871234 1871235 [...] (40129 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2544174 2544175 2544176 2544177 2544178 2544179 2544180 2544181 2544182 2544183 [...] (22147 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2602440 2602441 2602442 2602443 2602444 2602445 2602446 2602447 2602448 2602449 [...] (11227 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2699496 2699497 2699498 2699499 2699500 2699501 2699502 2699503 2699504 2699505 [...] (4770 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2930939 3180924 3180925 3180926 3180927 3180928 3180929 3180930 3180931 3180932 [...] (1143 flits)
Measured flits: (0 flits)
Time taken is 118640 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9642.11 (1 samples)
	minimum = 26 (1 samples)
	maximum = 83501 (1 samples)
Network latency average = 8826.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 83501 (1 samples)
Flit latency average = 16636.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 90866 (1 samples)
Fragmentation average = 318.589 (1 samples)
	minimum = 0 (1 samples)
	maximum = 12602 (1 samples)
Injected packet rate average = 0.024125 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0397143 (1 samples)
Accepted packet rate average = 0.0128333 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0165714 (1 samples)
Injected flit rate average = 0.434154 (1 samples)
	minimum = 0.224286 (1 samples)
	maximum = 0.714571 (1 samples)
Accepted flit rate average = 0.231551 (1 samples)
	minimum = 0.170429 (1 samples)
	maximum = 0.304 (1 samples)
Injected packet size average = 17.996 (1 samples)
Accepted packet size average = 18.043 (1 samples)
Hops average = 5.08384 (1 samples)
Total run time 240.876
