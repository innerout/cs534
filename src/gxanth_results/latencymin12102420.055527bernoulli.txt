BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 333.636
	minimum = 22
	maximum = 898
Network latency average = 303.545
	minimum = 22
	maximum = 820
Slowest packet = 389
Flit latency average = 280.268
	minimum = 5
	maximum = 803
Slowest flit = 23885
Fragmentation average = 25.1712
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0157604
	minimum = 0.007 (at node 108)
	maximum = 0.027 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.290464
	minimum = 0.128 (at node 108)
	maximum = 0.503 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 18.4299
Total in-flight flits = 117337 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.105
	minimum = 22
	maximum = 1719
Network latency average = 567.108
	minimum = 22
	maximum = 1603
Slowest packet = 2361
Flit latency average = 544.39
	minimum = 5
	maximum = 1621
Slowest flit = 59022
Fragmentation average = 25.0804
	minimum = 0
	maximum = 112
Injected packet rate average = 0.051625
	minimum = 0.04 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0168333
	minimum = 0.0105 (at node 116)
	maximum = 0.0245 (at node 154)
Injected flit rate average = 0.925266
	minimum = 0.713 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.30669
	minimum = 0.1945 (at node 116)
	maximum = 0.441 (at node 154)
Injected packet length average = 17.9228
Accepted packet length average = 18.2192
Total in-flight flits = 239063 (0 measured)
latency change    = 0.459353
throughput change = 0.0529087
Class 0:
Packet latency average = 1469.35
	minimum = 22
	maximum = 2463
Network latency average = 1382.51
	minimum = 22
	maximum = 2377
Slowest packet = 3279
Flit latency average = 1369.29
	minimum = 5
	maximum = 2360
Slowest flit = 101735
Fragmentation average = 25.4482
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0525521
	minimum = 0.034 (at node 24)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0175104
	minimum = 0.008 (at node 40)
	maximum = 0.028 (at node 67)
Injected flit rate average = 0.945495
	minimum = 0.62 (at node 24)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.314771
	minimum = 0.133 (at node 190)
	maximum = 0.498 (at node 67)
Injected packet length average = 17.9916
Accepted packet length average = 17.9762
Total in-flight flits = 360247 (0 measured)
latency change    = 0.580016
throughput change = 0.0256718
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 400.583
	minimum = 25
	maximum = 1070
Network latency average = 174.753
	minimum = 22
	maximum = 721
Slowest packet = 29966
Flit latency average = 2068.73
	minimum = 5
	maximum = 3038
Slowest flit = 165333
Fragmentation average = 5.47233
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0484635
	minimum = 0.026 (at node 60)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0165469
	minimum = 0.005 (at node 48)
	maximum = 0.027 (at node 90)
Injected flit rate average = 0.872229
	minimum = 0.461 (at node 128)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.297802
	minimum = 0.084 (at node 48)
	maximum = 0.487 (at node 90)
Injected packet length average = 17.9976
Accepted packet length average = 17.9975
Total in-flight flits = 470559 (158224 measured)
latency change    = 2.66804
throughput change = 0.05698
Class 0:
Packet latency average = 576.944
	minimum = 25
	maximum = 1634
Network latency average = 293.112
	minimum = 22
	maximum = 1268
Slowest packet = 29966
Flit latency average = 2401.61
	minimum = 5
	maximum = 3889
Slowest flit = 191956
Fragmentation average = 5.16263
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0483333
	minimum = 0.027 (at node 104)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.016401
	minimum = 0.01 (at node 36)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.870031
	minimum = 0.491 (at node 176)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.295234
	minimum = 0.1715 (at node 36)
	maximum = 0.444 (at node 90)
Injected packet length average = 18.0006
Accepted packet length average = 18.001
Total in-flight flits = 580957 (315351 measured)
latency change    = 0.305681
throughput change = 0.00869719
Class 0:
Packet latency average = 766.492
	minimum = 25
	maximum = 2252
Network latency average = 406.302
	minimum = 22
	maximum = 1872
Slowest packet = 29966
Flit latency average = 2732.97
	minimum = 5
	maximum = 4603
Slowest flit = 245216
Fragmentation average = 5.29485
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0482639
	minimum = 0.0276667 (at node 16)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0163403
	minimum = 0.0106667 (at node 36)
	maximum = 0.0236667 (at node 128)
Injected flit rate average = 0.868934
	minimum = 0.503 (at node 16)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.293913
	minimum = 0.192 (at node 36)
	maximum = 0.426667 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 17.987
Total in-flight flits = 691371 (472747 measured)
latency change    = 0.247294
throughput change = 0.00449514
Class 0:
Packet latency average = 948.666
	minimum = 25
	maximum = 2589
Network latency average = 523.55
	minimum = 22
	maximum = 2207
Slowest packet = 29966
Flit latency average = 3047.36
	minimum = 5
	maximum = 5336
Slowest flit = 291364
Fragmentation average = 5.37422
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0482956
	minimum = 0.028 (at node 16)
	maximum = 0.05575 (at node 29)
Accepted packet rate average = 0.0163594
	minimum = 0.01225 (at node 162)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.86951
	minimum = 0.50575 (at node 60)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.294337
	minimum = 0.219 (at node 162)
	maximum = 0.3865 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 17.992
Total in-flight flits = 801834 (630006 measured)
latency change    = 0.192031
throughput change = 0.00144068
Class 0:
Packet latency average = 1134.56
	minimum = 25
	maximum = 3353
Network latency average = 654.074
	minimum = 22
	maximum = 2818
Slowest packet = 29966
Flit latency average = 3375.72
	minimum = 5
	maximum = 6075
Slowest flit = 341090
Fragmentation average = 5.46682
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0483719
	minimum = 0.0284 (at node 44)
	maximum = 0.0556 (at node 2)
Accepted packet rate average = 0.0163802
	minimum = 0.0128 (at node 63)
	maximum = 0.022 (at node 128)
Injected flit rate average = 0.870639
	minimum = 0.5086 (at node 144)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.294832
	minimum = 0.2266 (at node 63)
	maximum = 0.3956 (at node 128)
Injected packet length average = 17.9989
Accepted packet length average = 17.9993
Total in-flight flits = 913074 (788780 measured)
latency change    = 0.163844
throughput change = 0.0016791
Class 0:
Packet latency average = 1299.28
	minimum = 25
	maximum = 3811
Network latency average = 759.473
	minimum = 22
	maximum = 3170
Slowest packet = 29966
Flit latency average = 3694.68
	minimum = 5
	maximum = 6899
Slowest flit = 364913
Fragmentation average = 5.58485
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0481441
	minimum = 0.0253333 (at node 72)
	maximum = 0.0556667 (at node 23)
Accepted packet rate average = 0.0163932
	minimum = 0.0126667 (at node 67)
	maximum = 0.0211667 (at node 11)
Injected flit rate average = 0.866732
	minimum = 0.4565 (at node 72)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.294999
	minimum = 0.228 (at node 67)
	maximum = 0.383 (at node 11)
Injected packet length average = 18.0029
Accepted packet length average = 17.9952
Total in-flight flits = 1018724 (941664 measured)
latency change    = 0.126783
throughput change = 0.000565562
Class 0:
Packet latency average = 1592.33
	minimum = 25
	maximum = 7424
Network latency average = 1031.29
	minimum = 22
	maximum = 6969
Slowest packet = 30188
Flit latency average = 4034.68
	minimum = 5
	maximum = 7580
Slowest flit = 429821
Fragmentation average = 6.5581
	minimum = 0
	maximum = 88
Injected packet rate average = 0.0456644
	minimum = 0.0252857 (at node 96)
	maximum = 0.0555714 (at node 2)
Accepted packet rate average = 0.0162768
	minimum = 0.0125714 (at node 4)
	maximum = 0.0205714 (at node 128)
Injected flit rate average = 0.822513
	minimum = 0.456857 (at node 96)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.292973
	minimum = 0.224143 (at node 63)
	maximum = 0.371429 (at node 128)
Injected packet length average = 18.0121
Accepted packet length average = 17.9995
Total in-flight flits = 1071205 (1039301 measured)
latency change    = 0.184035
throughput change = 0.00691503
Draining all recorded packets ...
Class 0:
Remaining flits: 450918 450919 450920 450921 450922 450923 450924 450925 450926 450927 [...] (1075403 flits)
Measured flits: 538470 538471 538472 538473 538474 538475 538476 538477 538478 538479 [...] (1061958 flits)
Class 0:
Remaining flits: 502938 502939 502940 502941 502942 502943 502944 502945 502946 502947 [...] (1072986 flits)
Measured flits: 538506 538507 538508 538509 538510 538511 538512 538513 538514 538515 [...] (1053498 flits)
Class 0:
Remaining flits: 560196 560197 560198 560199 560200 560201 560202 560203 560204 560205 [...] (1072966 flits)
Measured flits: 560196 560197 560198 560199 560200 560201 560202 560203 560204 560205 [...] (1035084 flits)
Class 0:
Remaining flits: 599724 599725 599726 599727 599728 599729 599730 599731 599732 599733 [...] (1072090 flits)
Measured flits: 599724 599725 599726 599727 599728 599729 599730 599731 599732 599733 [...] (1008870 flits)
Class 0:
Remaining flits: 638712 638713 638714 638715 638716 638717 638718 638719 638720 638721 [...] (1072885 flits)
Measured flits: 638712 638713 638714 638715 638716 638717 638718 638719 638720 638721 [...] (977377 flits)
Class 0:
Remaining flits: 703764 703765 703766 703767 703768 703769 703770 703771 703772 703773 [...] (1077241 flits)
Measured flits: 703764 703765 703766 703767 703768 703769 703770 703771 703772 703773 [...] (945139 flits)
Class 0:
Remaining flits: 730422 730423 730424 730425 730426 730427 730428 730429 730430 730431 [...] (1078501 flits)
Measured flits: 730422 730423 730424 730425 730426 730427 730428 730429 730430 730431 [...] (910201 flits)
Class 0:
Remaining flits: 773639 776268 776269 776270 776271 776272 776273 776274 776275 776276 [...] (1081405 flits)
Measured flits: 773639 776268 776269 776270 776271 776272 776273 776274 776275 776276 [...] (873868 flits)
Class 0:
Remaining flits: 818802 818803 818804 818805 818806 818807 818808 818809 818810 818811 [...] (1078799 flits)
Measured flits: 818802 818803 818804 818805 818806 818807 818808 818809 818810 818811 [...] (836051 flits)
Class 0:
Remaining flits: 840114 840115 840116 840117 840118 840119 840120 840121 840122 840123 [...] (1075863 flits)
Measured flits: 840114 840115 840116 840117 840118 840119 840120 840121 840122 840123 [...] (797655 flits)
Class 0:
Remaining flits: 881280 881281 881282 881283 881284 881285 881286 881287 881288 881289 [...] (1074584 flits)
Measured flits: 881280 881281 881282 881283 881284 881285 881286 881287 881288 881289 [...] (758201 flits)
Class 0:
Remaining flits: 901332 901333 901334 901335 901336 901337 901338 901339 901340 901341 [...] (1072420 flits)
Measured flits: 901332 901333 901334 901335 901336 901337 901338 901339 901340 901341 [...] (717693 flits)
Class 0:
Remaining flits: 950544 950545 950546 950547 950548 950549 950550 950551 950552 950553 [...] (1070216 flits)
Measured flits: 950544 950545 950546 950547 950548 950549 950550 950551 950552 950553 [...] (675794 flits)
Class 0:
Remaining flits: 953172 953173 953174 953175 953176 953177 953178 953179 953180 953181 [...] (1072513 flits)
Measured flits: 953172 953173 953174 953175 953176 953177 953178 953179 953180 953181 [...] (633179 flits)
Class 0:
Remaining flits: 998766 998767 998768 998769 998770 998771 998772 998773 998774 998775 [...] (1069416 flits)
Measured flits: 998766 998767 998768 998769 998770 998771 998772 998773 998774 998775 [...] (589605 flits)
Class 0:
Remaining flits: 1034730 1034731 1034732 1034733 1034734 1034735 1034736 1034737 1034738 1034739 [...] (1066353 flits)
Measured flits: 1034730 1034731 1034732 1034733 1034734 1034735 1034736 1034737 1034738 1034739 [...] (544729 flits)
Class 0:
Remaining flits: 1074078 1074079 1074080 1074081 1074082 1074083 1074084 1074085 1074086 1074087 [...] (1066116 flits)
Measured flits: 1074078 1074079 1074080 1074081 1074082 1074083 1074084 1074085 1074086 1074087 [...] (499345 flits)
Class 0:
Remaining flits: 1099691 1108800 1108801 1108802 1108803 1108804 1108805 1108806 1108807 1108808 [...] (1066441 flits)
Measured flits: 1099691 1108800 1108801 1108802 1108803 1108804 1108805 1108806 1108807 1108808 [...] (451888 flits)
Class 0:
Remaining flits: 1108800 1108801 1108802 1108803 1108804 1108805 1108806 1108807 1108808 1108809 [...] (1064146 flits)
Measured flits: 1108800 1108801 1108802 1108803 1108804 1108805 1108806 1108807 1108808 1108809 [...] (404153 flits)
Class 0:
Remaining flits: 1164384 1164385 1164386 1164387 1164388 1164389 1164390 1164391 1164392 1164393 [...] (1064860 flits)
Measured flits: 1164384 1164385 1164386 1164387 1164388 1164389 1164390 1164391 1164392 1164393 [...] (356695 flits)
Class 0:
Remaining flits: 1182348 1182349 1182350 1182351 1182352 1182353 1182354 1182355 1182356 1182357 [...] (1070159 flits)
Measured flits: 1182348 1182349 1182350 1182351 1182352 1182353 1182354 1182355 1182356 1182357 [...] (309826 flits)
Class 0:
Remaining flits: 1255266 1255267 1255268 1255269 1255270 1255271 1255272 1255273 1255274 1255275 [...] (1071346 flits)
Measured flits: 1255266 1255267 1255268 1255269 1255270 1255271 1255272 1255273 1255274 1255275 [...] (263315 flits)
Class 0:
Remaining flits: 1264266 1264267 1264268 1264269 1264270 1264271 1264272 1264273 1264274 1264275 [...] (1069537 flits)
Measured flits: 1264266 1264267 1264268 1264269 1264270 1264271 1264272 1264273 1264274 1264275 [...] (218356 flits)
Class 0:
Remaining flits: 1336482 1336483 1336484 1336485 1336486 1336487 1336488 1336489 1336490 1336491 [...] (1069302 flits)
Measured flits: 1336482 1336483 1336484 1336485 1336486 1336487 1336488 1336489 1336490 1336491 [...] (174401 flits)
Class 0:
Remaining flits: 1359900 1359901 1359902 1359903 1359904 1359905 1359906 1359907 1359908 1359909 [...] (1070702 flits)
Measured flits: 1359900 1359901 1359902 1359903 1359904 1359905 1359906 1359907 1359908 1359909 [...] (134355 flits)
Class 0:
Remaining flits: 1422882 1422883 1422884 1422885 1422886 1422887 1422888 1422889 1422890 1422891 [...] (1069146 flits)
Measured flits: 1422882 1422883 1422884 1422885 1422886 1422887 1422888 1422889 1422890 1422891 [...] (99844 flits)
Class 0:
Remaining flits: 1462356 1462357 1462358 1462359 1462360 1462361 1462362 1462363 1462364 1462365 [...] (1068766 flits)
Measured flits: 1462356 1462357 1462358 1462359 1462360 1462361 1462362 1462363 1462364 1462365 [...] (70513 flits)
Class 0:
Remaining flits: 1499454 1499455 1499456 1499457 1499458 1499459 1499460 1499461 1499462 1499463 [...] (1065495 flits)
Measured flits: 1499454 1499455 1499456 1499457 1499458 1499459 1499460 1499461 1499462 1499463 [...] (46418 flits)
Class 0:
Remaining flits: 1524996 1524997 1524998 1524999 1525000 1525001 1525002 1525003 1525004 1525005 [...] (1067078 flits)
Measured flits: 1524996 1524997 1524998 1524999 1525000 1525001 1525002 1525003 1525004 1525005 [...] (28588 flits)
Class 0:
Remaining flits: 1561518 1561519 1561520 1561521 1561522 1561523 1561524 1561525 1561526 1561527 [...] (1069522 flits)
Measured flits: 1561518 1561519 1561520 1561521 1561522 1561523 1561524 1561525 1561526 1561527 [...] (15752 flits)
Class 0:
Remaining flits: 1664028 1664029 1664030 1664031 1664032 1664033 1664034 1664035 1664036 1664037 [...] (1073248 flits)
Measured flits: 1664028 1664029 1664030 1664031 1664032 1664033 1664034 1664035 1664036 1664037 [...] (8248 flits)
Class 0:
Remaining flits: 1702600 1702601 1722945 1722946 1722947 1722948 1722949 1722950 1722951 1722952 [...] (1069036 flits)
Measured flits: 1765224 1765225 1765226 1765227 1765228 1765229 1765230 1765231 1765232 1765233 [...] (4338 flits)
Class 0:
Remaining flits: 1734048 1734049 1734050 1734051 1734052 1734053 1734054 1734055 1734056 1734057 [...] (1068491 flits)
Measured flits: 1794492 1794493 1794494 1794495 1794496 1794497 1794498 1794499 1794500 1794501 [...] (1826 flits)
Class 0:
Remaining flits: 1735536 1735537 1735538 1735539 1735540 1735541 1771272 1771273 1771274 1771275 [...] (1068113 flits)
Measured flits: 1845450 1845451 1845452 1845453 1845454 1845455 1845456 1845457 1845458 1845459 [...] (682 flits)
Class 0:
Remaining flits: 1803906 1803907 1803908 1803909 1803910 1803911 1803912 1803913 1803914 1803915 [...] (1069841 flits)
Measured flits: 1913364 1913365 1913366 1913367 1913368 1913369 1913370 1913371 1913372 1913373 [...] (187 flits)
Class 0:
Remaining flits: 1838052 1838053 1838054 1838055 1838056 1838057 1838058 1838059 1838060 1838061 [...] (1068178 flits)
Measured flits: 2068416 2068417 2068418 2068419 2068420 2068421 2068422 2068423 2068424 2068425 [...] (64 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1955592 1955593 1955594 1955595 1955596 1955597 1955598 1955599 1955600 1955601 [...] (1020792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2026602 2026603 2026604 2026605 2026606 2026607 2026608 2026609 2026610 2026611 [...] (970975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2070270 2070271 2070272 2070273 2070274 2070275 2070276 2070277 2070278 2070279 [...] (920351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2073690 2073691 2073692 2073693 2073694 2073695 2073696 2073697 2073698 2073699 [...] (871160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2194669 2194670 2194671 2194672 2194673 2194674 2194675 2194676 2194677 2194678 [...] (820894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2201940 2201941 2201942 2201943 2201944 2201945 2201946 2201947 2201948 2201949 [...] (770536 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2259522 2259523 2259524 2259525 2259526 2259527 2259528 2259529 2259530 2259531 [...] (720157 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2273058 2273059 2273060 2273061 2273062 2273063 2273064 2273065 2273066 2273067 [...] (669449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2283426 2283427 2283428 2283429 2283430 2283431 2283432 2283433 2283434 2283435 [...] (619690 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2286918 2286919 2286920 2286921 2286922 2286923 2286924 2286925 2286926 2286927 [...] (571166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2316852 2316853 2316854 2316855 2316856 2316857 2316858 2316859 2316860 2316861 [...] (522595 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2408148 2408149 2408150 2408151 2408152 2408153 2408154 2408155 2408156 2408157 [...] (474896 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2540466 2540467 2540468 2540469 2540470 2540471 2540472 2540473 2540474 2540475 [...] (427231 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2547936 2547937 2547938 2547939 2547940 2547941 2547942 2547943 2547944 2547945 [...] (379759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2618190 2618191 2618192 2618193 2618194 2618195 2618196 2618197 2618198 2618199 [...] (332017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2694096 2694097 2694098 2694099 2694100 2694101 2694102 2694103 2694104 2694105 [...] (284236 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2765448 2765449 2765450 2765451 2765452 2765453 2765454 2765455 2765456 2765457 [...] (236297 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2830612 2830613 2830614 2830615 2830616 2830617 2830618 2830619 2830620 2830621 [...] (188274 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2871756 2871757 2871758 2871759 2871760 2871761 2871762 2871763 2871764 2871765 [...] (140441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2888910 2888911 2888912 2888913 2888914 2888915 2888916 2888917 2888918 2888919 [...] (92448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2888924 2888925 2888926 2888927 2994624 2994625 2994626 2994627 2994628 2994629 [...] (45324 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3039721 3039722 3039723 3039724 3039725 3039726 3039727 3039728 3039729 3039730 [...] (12662 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3329028 3329029 3329030 3329031 3329032 3329033 3329034 3329035 3329036 3329037 [...] (1275 flits)
Measured flits: (0 flits)
Time taken is 70480 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16789.8 (1 samples)
	minimum = 25 (1 samples)
	maximum = 36700 (1 samples)
Network latency average = 15082.8 (1 samples)
	minimum = 22 (1 samples)
	maximum = 31616 (1 samples)
Flit latency average = 16790.5 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34677 (1 samples)
Fragmentation average = 28.2904 (1 samples)
	minimum = 0 (1 samples)
	maximum = 860 (1 samples)
Injected packet rate average = 0.0456644 (1 samples)
	minimum = 0.0252857 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.0162768 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.0205714 (1 samples)
Injected flit rate average = 0.822513 (1 samples)
	minimum = 0.456857 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.292973 (1 samples)
	minimum = 0.224143 (1 samples)
	maximum = 0.371429 (1 samples)
Injected packet size average = 18.0121 (1 samples)
Accepted packet size average = 17.9995 (1 samples)
Hops average = 5.07025 (1 samples)
Total run time 69.9655
