BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 334.956
	minimum = 23
	maximum = 965
Network latency average = 312.73
	minimum = 23
	maximum = 948
Slowest packet = 107
Flit latency average = 238.361
	minimum = 6
	maximum = 955
Slowest flit = 2811
Fragmentation average = 162.77
	minimum = 0
	maximum = 883
Injected packet rate average = 0.0211927
	minimum = 0.006 (at node 100)
	maximum = 0.033 (at node 178)
Accepted packet rate average = 0.00996875
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 165)
Injected flit rate average = 0.374411
	minimum = 0.1 (at node 100)
	maximum = 0.593 (at node 178)
Accepted flit rate average= 0.204453
	minimum = 0.072 (at node 174)
	maximum = 0.329 (at node 70)
Injected packet length average = 17.667
Accepted packet length average = 20.5094
Total in-flight flits = 34239 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 640.906
	minimum = 23
	maximum = 1876
Network latency average = 534.497
	minimum = 23
	maximum = 1864
Slowest packet = 702
Flit latency average = 435.725
	minimum = 6
	maximum = 1927
Slowest flit = 5696
Fragmentation average = 192.718
	minimum = 0
	maximum = 1745
Injected packet rate average = 0.0163932
	minimum = 0.0045 (at node 60)
	maximum = 0.026 (at node 67)
Accepted packet rate average = 0.0105443
	minimum = 0.006 (at node 120)
	maximum = 0.016 (at node 76)
Injected flit rate average = 0.291401
	minimum = 0.0765 (at node 68)
	maximum = 0.468 (at node 67)
Accepted flit rate average= 0.200732
	minimum = 0.11 (at node 153)
	maximum = 0.2935 (at node 76)
Injected packet length average = 17.7757
Accepted packet length average = 19.037
Total in-flight flits = 36589 (0 measured)
latency change    = 0.477372
throughput change = 0.0185389
Class 0:
Packet latency average = 1560.36
	minimum = 113
	maximum = 2954
Network latency average = 951.853
	minimum = 23
	maximum = 2954
Slowest packet = 126
Flit latency average = 832.965
	minimum = 6
	maximum = 2937
Slowest flit = 2285
Fragmentation average = 210.355
	minimum = 0
	maximum = 2914
Injected packet rate average = 0.0109531
	minimum = 0 (at node 30)
	maximum = 0.031 (at node 173)
Accepted packet rate average = 0.0107917
	minimum = 0.003 (at node 138)
	maximum = 0.022 (at node 97)
Injected flit rate average = 0.197594
	minimum = 0 (at node 60)
	maximum = 0.545 (at node 173)
Accepted flit rate average= 0.193849
	minimum = 0.067 (at node 138)
	maximum = 0.372 (at node 97)
Injected packet length average = 18.0399
Accepted packet length average = 17.9628
Total in-flight flits = 37260 (0 measured)
latency change    = 0.589258
throughput change = 0.0355061
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2131.17
	minimum = 251
	maximum = 3452
Network latency average = 336.702
	minimum = 26
	maximum = 977
Slowest packet = 8521
Flit latency average = 894.18
	minimum = 6
	maximum = 3820
Slowest flit = 15147
Fragmentation average = 121.39
	minimum = 0
	maximum = 598
Injected packet rate average = 0.0107969
	minimum = 0 (at node 4)
	maximum = 0.033 (at node 33)
Accepted packet rate average = 0.0108073
	minimum = 0.004 (at node 77)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.195375
	minimum = 0 (at node 4)
	maximum = 0.592 (at node 33)
Accepted flit rate average= 0.195052
	minimum = 0.069 (at node 77)
	maximum = 0.342 (at node 122)
Injected packet length average = 18.0955
Accepted packet length average = 18.0482
Total in-flight flits = 37286 (25188 measured)
latency change    = 0.267838
throughput change = 0.00616822
Class 0:
Packet latency average = 2631.63
	minimum = 251
	maximum = 4512
Network latency average = 593.347
	minimum = 26
	maximum = 1921
Slowest packet = 8521
Flit latency average = 911.431
	minimum = 6
	maximum = 4703
Slowest flit = 27069
Fragmentation average = 158.669
	minimum = 0
	maximum = 1328
Injected packet rate average = 0.0108828
	minimum = 0 (at node 70)
	maximum = 0.0225 (at node 18)
Accepted packet rate average = 0.0107396
	minimum = 0.006 (at node 36)
	maximum = 0.016 (at node 59)
Injected flit rate average = 0.196008
	minimum = 0 (at node 76)
	maximum = 0.412 (at node 18)
Accepted flit rate average= 0.193909
	minimum = 0.111 (at node 36)
	maximum = 0.29 (at node 59)
Injected packet length average = 18.0108
Accepted packet length average = 18.0555
Total in-flight flits = 38039 (34228 measured)
latency change    = 0.190169
throughput change = 0.0058957
Class 0:
Packet latency average = 3041.14
	minimum = 251
	maximum = 5090
Network latency average = 757.019
	minimum = 26
	maximum = 2905
Slowest packet = 8521
Flit latency average = 928.536
	minimum = 6
	maximum = 5670
Slowest flit = 17278
Fragmentation average = 173.865
	minimum = 0
	maximum = 2019
Injected packet rate average = 0.0106719
	minimum = 0.000666667 (at node 93)
	maximum = 0.0203333 (at node 18)
Accepted packet rate average = 0.0107135
	minimum = 0.00666667 (at node 5)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.192097
	minimum = 0.0103333 (at node 93)
	maximum = 0.371333 (at node 18)
Accepted flit rate average= 0.192057
	minimum = 0.119 (at node 48)
	maximum = 0.282333 (at node 44)
Injected packet length average = 18.0003
Accepted packet length average = 17.9266
Total in-flight flits = 37533 (36074 measured)
latency change    = 0.134659
throughput change = 0.00964068
Draining remaining packets ...
Class 0:
Remaining flits: 73814 73815 73816 73817 76259 76260 76261 76262 76263 76264 [...] (7263 flits)
Measured flits: 153504 153505 153506 153507 153508 153509 153510 153511 153512 153513 [...] (6949 flits)
Class 0:
Remaining flits: 106272 106273 106274 106275 106276 106277 106278 106279 106280 106281 [...] (89 flits)
Measured flits: 198846 198847 198848 198849 198850 198851 198852 198853 198854 198855 [...] (69 flits)
Time taken is 8091 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3500.26 (1 samples)
	minimum = 251 (1 samples)
	maximum = 6721 (1 samples)
Network latency average = 1009.28 (1 samples)
	minimum = 26 (1 samples)
	maximum = 4527 (1 samples)
Flit latency average = 1067.27 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6581 (1 samples)
Fragmentation average = 168.773 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2301 (1 samples)
Injected packet rate average = 0.0106719 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.0203333 (1 samples)
Accepted packet rate average = 0.0107135 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.192097 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.371333 (1 samples)
Accepted flit rate average = 0.192057 (1 samples)
	minimum = 0.119 (1 samples)
	maximum = 0.282333 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.9266 (1 samples)
Hops average = 5.12904 (1 samples)
Total run time 12.74
