BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.992
	minimum = 22
	maximum = 961
Network latency average = 237.321
	minimum = 22
	maximum = 738
Slowest packet = 46
Flit latency average = 214.475
	minimum = 5
	maximum = 779
Slowest flit = 19478
Fragmentation average = 19.5274
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0289167
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0138542
	minimum = 0.007 (at node 28)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.515651
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.254932
	minimum = 0.126 (at node 64)
	maximum = 0.414 (at node 48)
Injected packet length average = 17.8323
Accepted packet length average = 18.4011
Total in-flight flits = 51061 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.637
	minimum = 22
	maximum = 1944
Network latency average = 463.794
	minimum = 22
	maximum = 1508
Slowest packet = 46
Flit latency average = 440.592
	minimum = 5
	maximum = 1503
Slowest flit = 45490
Fragmentation average = 21.2573
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0291094
	minimum = 0.007 (at node 179)
	maximum = 0.0545 (at node 175)
Accepted packet rate average = 0.0146068
	minimum = 0.0095 (at node 30)
	maximum = 0.021 (at node 51)
Injected flit rate average = 0.521898
	minimum = 0.126 (at node 179)
	maximum = 0.9725 (at node 175)
Accepted flit rate average= 0.26582
	minimum = 0.171 (at node 164)
	maximum = 0.378 (at node 51)
Injected packet length average = 17.9289
Accepted packet length average = 18.1984
Total in-flight flits = 99777 (0 measured)
latency change    = 0.439813
throughput change = 0.0409601
Class 0:
Packet latency average = 1432.19
	minimum = 25
	maximum = 2858
Network latency average = 1166.29
	minimum = 22
	maximum = 2239
Slowest packet = 3578
Flit latency average = 1147.19
	minimum = 5
	maximum = 2301
Slowest flit = 65959
Fragmentation average = 20.8789
	minimum = 0
	maximum = 194
Injected packet rate average = 0.022375
	minimum = 0.001 (at node 93)
	maximum = 0.055 (at node 95)
Accepted packet rate average = 0.0150573
	minimum = 0.007 (at node 5)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.402625
	minimum = 0.018 (at node 93)
	maximum = 0.987 (at node 95)
Accepted flit rate average= 0.2705
	minimum = 0.126 (at node 5)
	maximum = 0.513 (at node 16)
Injected packet length average = 17.9944
Accepted packet length average = 17.9647
Total in-flight flits = 125691 (0 measured)
latency change    = 0.568746
throughput change = 0.0173001
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 736.397
	minimum = 22
	maximum = 2846
Network latency average = 73.8547
	minimum = 22
	maximum = 758
Slowest packet = 15544
Flit latency average = 1595.3
	minimum = 5
	maximum = 2940
Slowest flit = 99396
Fragmentation average = 4.51955
	minimum = 0
	maximum = 20
Injected packet rate average = 0.019651
	minimum = 0.002 (at node 55)
	maximum = 0.053 (at node 191)
Accepted packet rate average = 0.0150313
	minimum = 0.005 (at node 22)
	maximum = 0.025 (at node 78)
Injected flit rate average = 0.354417
	minimum = 0.036 (at node 55)
	maximum = 0.941 (at node 191)
Accepted flit rate average= 0.27012
	minimum = 0.087 (at node 22)
	maximum = 0.45 (at node 78)
Injected packet length average = 18.0355
Accepted packet length average = 17.9705
Total in-flight flits = 142264 (65136 measured)
latency change    = 0.94486
throughput change = 0.00140755
Class 0:
Packet latency average = 1552.22
	minimum = 22
	maximum = 4413
Network latency average = 833.6
	minimum = 22
	maximum = 1944
Slowest packet = 15544
Flit latency average = 1795.49
	minimum = 5
	maximum = 3805
Slowest flit = 120347
Fragmentation average = 11.2585
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0180938
	minimum = 0.0055 (at node 127)
	maximum = 0.0335 (at node 3)
Accepted packet rate average = 0.0149948
	minimum = 0.009 (at node 36)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.326453
	minimum = 0.099 (at node 127)
	maximum = 0.603 (at node 3)
Accepted flit rate average= 0.269742
	minimum = 0.162 (at node 36)
	maximum = 0.372 (at node 178)
Injected packet length average = 18.0423
Accepted packet length average = 17.9891
Total in-flight flits = 148182 (111960 measured)
latency change    = 0.525585
throughput change = 0.00139987
Class 0:
Packet latency average = 2308.22
	minimum = 22
	maximum = 5217
Network latency average = 1450.19
	minimum = 22
	maximum = 2990
Slowest packet = 15544
Flit latency average = 1978.21
	minimum = 5
	maximum = 4551
Slowest flit = 138888
Fragmentation average = 15.3854
	minimum = 0
	maximum = 245
Injected packet rate average = 0.0172083
	minimum = 0.008 (at node 127)
	maximum = 0.029 (at node 191)
Accepted packet rate average = 0.014934
	minimum = 0.0106667 (at node 114)
	maximum = 0.0196667 (at node 138)
Injected flit rate average = 0.309908
	minimum = 0.144 (at node 127)
	maximum = 0.522333 (at node 191)
Accepted flit rate average= 0.268835
	minimum = 0.192 (at node 114)
	maximum = 0.352667 (at node 138)
Injected packet length average = 18.0092
Accepted packet length average = 18.0015
Total in-flight flits = 149942 (137732 measured)
latency change    = 0.327523
throughput change = 0.00337425
Draining remaining packets ...
Class 0:
Remaining flits: 172170 172171 172172 172173 172174 172175 172176 172177 172178 172179 [...] (100404 flits)
Measured flits: 279738 279739 279740 279741 279742 279743 279744 279745 279746 279747 [...] (98555 flits)
Class 0:
Remaining flits: 273819 273820 273821 273822 273823 273824 273825 273826 273827 273828 [...] (51885 flits)
Measured flits: 280584 280585 280586 280587 280588 280589 280590 280591 280592 280593 [...] (51870 flits)
Class 0:
Remaining flits: 320526 320527 320528 320529 320530 320531 320532 320533 320534 320535 [...] (6616 flits)
Measured flits: 320526 320527 320528 320529 320530 320531 320532 320533 320534 320535 [...] (6616 flits)
Time taken is 9764 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3887.78 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8106 (1 samples)
Network latency average = 2634.28 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5564 (1 samples)
Flit latency average = 2497.58 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5547 (1 samples)
Fragmentation average = 18.1226 (1 samples)
	minimum = 0 (1 samples)
	maximum = 245 (1 samples)
Injected packet rate average = 0.0172083 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.029 (1 samples)
Accepted packet rate average = 0.014934 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0196667 (1 samples)
Injected flit rate average = 0.309908 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.522333 (1 samples)
Accepted flit rate average = 0.268835 (1 samples)
	minimum = 0.192 (1 samples)
	maximum = 0.352667 (1 samples)
Injected packet size average = 18.0092 (1 samples)
Accepted packet size average = 18.0015 (1 samples)
Hops average = 5.07296 (1 samples)
Total run time 8.02532
