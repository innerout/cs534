BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.69
	minimum = 22
	maximum = 961
Network latency average = 185.329
	minimum = 22
	maximum = 741
Slowest packet = 64
Flit latency average = 158.641
	minimum = 5
	maximum = 719
Slowest flit = 11393
Fragmentation average = 18.3926
	minimum = 0
	maximum = 107
Injected packet rate average = 0.017349
	minimum = 0 (at node 2)
	maximum = 0.041 (at node 110)
Accepted packet rate average = 0.0131198
	minimum = 0.006 (at node 41)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.30851
	minimum = 0 (at node 2)
	maximum = 0.727 (at node 110)
Accepted flit rate average= 0.241313
	minimum = 0.108 (at node 150)
	maximum = 0.414 (at node 44)
Injected packet length average = 17.7826
Accepted packet length average = 18.393
Total in-flight flits = 14832 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 570.052
	minimum = 22
	maximum = 1971
Network latency average = 246.649
	minimum = 22
	maximum = 1348
Slowest packet = 64
Flit latency average = 216.511
	minimum = 5
	maximum = 1316
Slowest flit = 28481
Fragmentation average = 20.5109
	minimum = 0
	maximum = 133
Injected packet rate average = 0.0158464
	minimum = 0.0035 (at node 63)
	maximum = 0.032 (at node 86)
Accepted packet rate average = 0.0134505
	minimum = 0.0075 (at node 116)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.28343
	minimum = 0.063 (at node 63)
	maximum = 0.576 (at node 86)
Accepted flit rate average= 0.244586
	minimum = 0.135 (at node 153)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.8861
Accepted packet length average = 18.1841
Total in-flight flits = 17301 (0 measured)
latency change    = 0.426913
throughput change = 0.0133836
Class 0:
Packet latency average = 1279.53
	minimum = 31
	maximum = 2927
Network latency average = 348.777
	minimum = 22
	maximum = 1572
Slowest packet = 5739
Flit latency average = 313.448
	minimum = 5
	maximum = 1552
Slowest flit = 66275
Fragmentation average = 22.1977
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0138229
	minimum = 0 (at node 145)
	maximum = 0.028 (at node 132)
Accepted packet rate average = 0.0138542
	minimum = 0.005 (at node 163)
	maximum = 0.026 (at node 34)
Injected flit rate average = 0.248682
	minimum = 0 (at node 145)
	maximum = 0.514 (at node 132)
Accepted flit rate average= 0.248839
	minimum = 0.09 (at node 163)
	maximum = 0.465 (at node 34)
Injected packet length average = 17.9906
Accepted packet length average = 17.9613
Total in-flight flits = 17494 (0 measured)
latency change    = 0.554485
throughput change = 0.0170898
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1836.92
	minimum = 38
	maximum = 3729
Network latency average = 281.93
	minimum = 22
	maximum = 910
Slowest packet = 8884
Flit latency average = 318.486
	minimum = 5
	maximum = 1457
Slowest flit = 131237
Fragmentation average = 21.3473
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0143438
	minimum = 0.004 (at node 6)
	maximum = 0.026 (at node 155)
Accepted packet rate average = 0.0138802
	minimum = 0.006 (at node 48)
	maximum = 0.022 (at node 6)
Injected flit rate average = 0.258068
	minimum = 0.072 (at node 6)
	maximum = 0.455 (at node 164)
Accepted flit rate average= 0.251589
	minimum = 0.108 (at node 71)
	maximum = 0.406 (at node 78)
Injected packet length average = 17.9916
Accepted packet length average = 18.1257
Total in-flight flits = 18815 (18743 measured)
latency change    = 0.303433
throughput change = 0.0109305
Class 0:
Packet latency average = 2172.19
	minimum = 38
	maximum = 4781
Network latency average = 329.29
	minimum = 22
	maximum = 1459
Slowest packet = 8884
Flit latency average = 320.983
	minimum = 5
	maximum = 1712
Slowest flit = 130830
Fragmentation average = 22.5724
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0140417
	minimum = 0.006 (at node 94)
	maximum = 0.022 (at node 128)
Accepted packet rate average = 0.0139271
	minimum = 0.009 (at node 86)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.252492
	minimum = 0.1025 (at node 94)
	maximum = 0.3885 (at node 128)
Accepted flit rate average= 0.251026
	minimum = 0.1555 (at node 86)
	maximum = 0.3905 (at node 129)
Injected packet length average = 17.9816
Accepted packet length average = 18.0243
Total in-flight flits = 17904 (17904 measured)
latency change    = 0.15435
throughput change = 0.0022408
Class 0:
Packet latency average = 2457.68
	minimum = 31
	maximum = 5666
Network latency average = 339.314
	minimum = 22
	maximum = 1735
Slowest packet = 8884
Flit latency average = 320.642
	minimum = 5
	maximum = 1712
Slowest flit = 130830
Fragmentation average = 22.3773
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0141094
	minimum = 0.00766667 (at node 94)
	maximum = 0.021 (at node 104)
Accepted packet rate average = 0.0140104
	minimum = 0.00933333 (at node 36)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.253786
	minimum = 0.138 (at node 94)
	maximum = 0.378 (at node 104)
Accepted flit rate average= 0.252389
	minimum = 0.167333 (at node 89)
	maximum = 0.375667 (at node 128)
Injected packet length average = 17.9871
Accepted packet length average = 18.0144
Total in-flight flits = 18278 (18278 measured)
latency change    = 0.116162
throughput change = 0.00539979
Class 0:
Packet latency average = 2737.59
	minimum = 31
	maximum = 6681
Network latency average = 345.029
	minimum = 22
	maximum = 1735
Slowest packet = 8884
Flit latency average = 321.088
	minimum = 5
	maximum = 1712
Slowest flit = 130830
Fragmentation average = 22.6441
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0141146
	minimum = 0.00775 (at node 86)
	maximum = 0.02075 (at node 118)
Accepted packet rate average = 0.0140521
	minimum = 0.01 (at node 89)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.254176
	minimum = 0.1395 (at node 86)
	maximum = 0.37125 (at node 118)
Accepted flit rate average= 0.253143
	minimum = 0.17675 (at node 89)
	maximum = 0.3615 (at node 128)
Injected packet length average = 18.008
Accepted packet length average = 18.0146
Total in-flight flits = 18290 (18290 measured)
latency change    = 0.102245
throughput change = 0.0029799
Class 0:
Packet latency average = 3017.96
	minimum = 31
	maximum = 7490
Network latency average = 347.827
	minimum = 22
	maximum = 1735
Slowest packet = 8884
Flit latency average = 321.244
	minimum = 5
	maximum = 1712
Slowest flit = 130830
Fragmentation average = 22.6959
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0141469
	minimum = 0.009 (at node 26)
	maximum = 0.0196 (at node 128)
Accepted packet rate average = 0.0140917
	minimum = 0.0104 (at node 89)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.254643
	minimum = 0.162 (at node 84)
	maximum = 0.3528 (at node 128)
Accepted flit rate average= 0.253793
	minimum = 0.188 (at node 89)
	maximum = 0.3314 (at node 128)
Injected packet length average = 17.9999
Accepted packet length average = 18.0101
Total in-flight flits = 18419 (18419 measured)
latency change    = 0.0928997
throughput change = 0.00255909
Class 0:
Packet latency average = 3278.06
	minimum = 31
	maximum = 7779
Network latency average = 349.958
	minimum = 22
	maximum = 1906
Slowest packet = 8884
Flit latency average = 321.575
	minimum = 5
	maximum = 1871
Slowest flit = 343944
Fragmentation average = 22.6114
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0141111
	minimum = 0.00916667 (at node 86)
	maximum = 0.0195 (at node 112)
Accepted packet rate average = 0.0140764
	minimum = 0.0111667 (at node 42)
	maximum = 0.0181667 (at node 138)
Injected flit rate average = 0.253975
	minimum = 0.163333 (at node 86)
	maximum = 0.349333 (at node 112)
Accepted flit rate average= 0.253489
	minimum = 0.201667 (at node 80)
	maximum = 0.329 (at node 138)
Injected packet length average = 17.9982
Accepted packet length average = 18.0081
Total in-flight flits = 17993 (17993 measured)
latency change    = 0.0793466
throughput change = 0.00119924
Class 0:
Packet latency average = 3528.72
	minimum = 31
	maximum = 8695
Network latency average = 351.719
	minimum = 22
	maximum = 1906
Slowest packet = 8884
Flit latency average = 322.617
	minimum = 5
	maximum = 1871
Slowest flit = 343944
Fragmentation average = 22.431
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0139725
	minimum = 0.00985714 (at node 158)
	maximum = 0.0184286 (at node 150)
Accepted packet rate average = 0.0139598
	minimum = 0.0111429 (at node 79)
	maximum = 0.0178571 (at node 128)
Injected flit rate average = 0.251613
	minimum = 0.177429 (at node 158)
	maximum = 0.331714 (at node 150)
Accepted flit rate average= 0.251344
	minimum = 0.200571 (at node 79)
	maximum = 0.322143 (at node 138)
Injected packet length average = 18.0078
Accepted packet length average = 18.0048
Total in-flight flits = 17872 (17872 measured)
latency change    = 0.0710361
throughput change = 0.00853399
Draining all recorded packets ...
Class 0:
Remaining flits: 485136 485137 485138 485139 485140 485141 485142 485143 485144 485145 [...] (18043 flits)
Measured flits: 485136 485137 485138 485139 485140 485141 485142 485143 485144 485145 [...] (18025 flits)
Class 0:
Remaining flits: 531252 531253 531254 531255 531256 531257 531258 531259 531260 531261 [...] (17961 flits)
Measured flits: 531252 531253 531254 531255 531256 531257 531258 531259 531260 531261 [...] (17801 flits)
Class 0:
Remaining flits: 564444 564445 564446 564447 564448 564449 564450 564451 564452 564453 [...] (18407 flits)
Measured flits: 564444 564445 564446 564447 564448 564449 564450 564451 564452 564453 [...] (17889 flits)
Class 0:
Remaining flits: 595356 595357 595358 595359 595360 595361 595362 595363 595364 595365 [...] (18638 flits)
Measured flits: 595356 595357 595358 595359 595360 595361 595362 595363 595364 595365 [...] (18069 flits)
Class 0:
Remaining flits: 667746 667747 667748 667749 667750 667751 667752 667753 667754 667755 [...] (18459 flits)
Measured flits: 678618 678619 678620 678621 678622 678623 678624 678625 678626 678627 [...] (17438 flits)
Class 0:
Remaining flits: 722504 722505 722506 722507 722508 722509 722510 722511 722512 722513 [...] (18334 flits)
Measured flits: 722504 722505 722506 722507 722508 722509 722510 722511 722512 722513 [...] (16386 flits)
Class 0:
Remaining flits: 753582 753583 753584 753585 753586 753587 755859 755860 755861 755862 [...] (18130 flits)
Measured flits: 753582 753583 753584 753585 753586 753587 755859 755860 755861 755862 [...] (15239 flits)
Class 0:
Remaining flits: 816894 816895 816896 816897 816898 816899 816900 816901 816902 816903 [...] (18495 flits)
Measured flits: 816894 816895 816896 816897 816898 816899 816900 816901 816902 816903 [...] (14457 flits)
Class 0:
Remaining flits: 844115 844116 844117 844118 844119 844120 844121 844122 844123 844124 [...] (18521 flits)
Measured flits: 868565 868566 868567 868568 868569 868570 868571 870660 870661 870662 [...] (13361 flits)
Class 0:
Remaining flits: 903404 903405 903406 903407 903408 903409 903410 903411 903412 903413 [...] (18619 flits)
Measured flits: 915354 915355 915356 915357 915358 915359 915360 915361 915362 915363 [...] (11656 flits)
Class 0:
Remaining flits: 968544 968545 968546 968547 968548 968549 968550 968551 968552 968553 [...] (18561 flits)
Measured flits: 974016 974017 974018 974019 974020 974021 974022 974023 974024 974025 [...] (10393 flits)
Class 0:
Remaining flits: 1004580 1004581 1004582 1004583 1004584 1004585 1004586 1004587 1004588 1004589 [...] (17874 flits)
Measured flits: 1008102 1008103 1008104 1008105 1008106 1008107 1014084 1014085 1014086 1014087 [...] (8561 flits)
Class 0:
Remaining flits: 1032984 1032985 1032986 1032987 1032988 1032989 1032990 1032991 1032992 1032993 [...] (18194 flits)
Measured flits: 1032984 1032985 1032986 1032987 1032988 1032989 1032990 1032991 1032992 1032993 [...] (7382 flits)
Class 0:
Remaining flits: 1099062 1099063 1099064 1099065 1099066 1099067 1099068 1099069 1099070 1099071 [...] (18310 flits)
Measured flits: 1099062 1099063 1099064 1099065 1099066 1099067 1099068 1099069 1099070 1099071 [...] (5888 flits)
Class 0:
Remaining flits: 1145358 1145359 1145360 1145361 1145362 1145363 1145364 1145365 1145366 1145367 [...] (18059 flits)
Measured flits: 1145970 1145971 1145972 1145973 1145974 1145975 1145976 1145977 1145978 1145979 [...] (4763 flits)
Class 0:
Remaining flits: 1192428 1192429 1192430 1192431 1192432 1192433 1192434 1192435 1192436 1192437 [...] (18654 flits)
Measured flits: 1215262 1215263 1215264 1215265 1215266 1215267 1215268 1215269 1217599 1217600 [...] (3987 flits)
Class 0:
Remaining flits: 1206954 1206955 1206956 1206957 1206958 1206959 1206960 1206961 1206962 1206963 [...] (18495 flits)
Measured flits: 1246878 1246879 1246880 1246881 1246882 1246883 1246884 1246885 1246886 1246887 [...] (3213 flits)
Class 0:
Remaining flits: 1299078 1299079 1299080 1299081 1299082 1299083 1299084 1299085 1299086 1299087 [...] (18534 flits)
Measured flits: 1303002 1303003 1303004 1303005 1303006 1303007 1303008 1303009 1303010 1303011 [...] (2601 flits)
Class 0:
Remaining flits: 1347120 1347121 1347122 1347123 1347124 1347125 1347126 1347127 1347128 1347129 [...] (18202 flits)
Measured flits: 1374102 1374103 1374104 1374105 1374106 1374107 1374108 1374109 1374110 1374111 [...] (1940 flits)
Class 0:
Remaining flits: 1386972 1386973 1386974 1386975 1386976 1386977 1386978 1386979 1386980 1386981 [...] (18428 flits)
Measured flits: 1424158 1424159 1427497 1427498 1427499 1427500 1427501 1427502 1427503 1427504 [...] (968 flits)
Class 0:
Remaining flits: 1420033 1420034 1420035 1420036 1420037 1430636 1430637 1430638 1430639 1441798 [...] (18489 flits)
Measured flits: 1465957 1465958 1465959 1465960 1465961 1465962 1465963 1465964 1465965 1465966 [...] (758 flits)
Class 0:
Remaining flits: 1480032 1480033 1480034 1480035 1480036 1480037 1480038 1480039 1480040 1480041 [...] (18820 flits)
Measured flits: 1501722 1501723 1501724 1501725 1501726 1501727 1501728 1501729 1501730 1501731 [...] (258 flits)
Class 0:
Remaining flits: 1531998 1531999 1532000 1532001 1532002 1532003 1532004 1532005 1532006 1532007 [...] (18122 flits)
Measured flits: 1554444 1554445 1554446 1554447 1554448 1554449 1554450 1554451 1554452 1554453 [...] (121 flits)
Class 0:
Remaining flits: 1561457 1561458 1561459 1561460 1561461 1561462 1561463 1572318 1572319 1572320 [...] (18369 flits)
Measured flits: 1619208 1619209 1619210 1619211 1619212 1619213 1619214 1619215 1619216 1619217 [...] (113 flits)
Draining remaining packets ...
Time taken is 35518 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7818.77 (1 samples)
	minimum = 31 (1 samples)
	maximum = 25151 (1 samples)
Network latency average = 359.833 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2086 (1 samples)
Flit latency average = 328.483 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2350 (1 samples)
Fragmentation average = 22.6504 (1 samples)
	minimum = 0 (1 samples)
	maximum = 149 (1 samples)
Injected packet rate average = 0.0139725 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0184286 (1 samples)
Accepted packet rate average = 0.0139598 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.251613 (1 samples)
	minimum = 0.177429 (1 samples)
	maximum = 0.331714 (1 samples)
Accepted flit rate average = 0.251344 (1 samples)
	minimum = 0.200571 (1 samples)
	maximum = 0.322143 (1 samples)
Injected packet size average = 18.0078 (1 samples)
Accepted packet size average = 18.0048 (1 samples)
Hops average = 5.05885 (1 samples)
Total run time 33.5566
