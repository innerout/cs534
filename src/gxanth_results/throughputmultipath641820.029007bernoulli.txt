BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 271.392
	minimum = 22
	maximum = 805
Network latency average = 263.15
	minimum = 22
	maximum = 796
Slowest packet = 894
Flit latency average = 230.033
	minimum = 5
	maximum = 796
Slowest flit = 17396
Fragmentation average = 62.3455
	minimum = 0
	maximum = 434
Injected packet rate average = 0.0285469
	minimum = 0.011 (at node 157)
	maximum = 0.042 (at node 81)
Accepted packet rate average = 0.0135365
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.50901
	minimum = 0.198 (at node 157)
	maximum = 0.75 (at node 81)
Accepted flit rate average= 0.259391
	minimum = 0.09 (at node 174)
	maximum = 0.414 (at node 76)
Injected packet length average = 17.8307
Accepted packet length average = 19.1624
Total in-flight flits = 48945 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.708
	minimum = 22
	maximum = 1531
Network latency average = 485.774
	minimum = 22
	maximum = 1528
Slowest packet = 2180
Flit latency average = 443.484
	minimum = 5
	maximum = 1542
Slowest flit = 44083
Fragmentation average = 79.6744
	minimum = 0
	maximum = 572
Injected packet rate average = 0.0271198
	minimum = 0.019 (at node 12)
	maximum = 0.0385 (at node 66)
Accepted packet rate average = 0.0145
	minimum = 0.0095 (at node 116)
	maximum = 0.0205 (at node 71)
Injected flit rate average = 0.485451
	minimum = 0.3335 (at node 20)
	maximum = 0.687 (at node 66)
Accepted flit rate average= 0.269562
	minimum = 0.171 (at node 116)
	maximum = 0.376 (at node 71)
Injected packet length average = 17.9002
Accepted packet length average = 18.5905
Total in-flight flits = 84372 (0 measured)
latency change    = 0.455809
throughput change = 0.0377348
Class 0:
Packet latency average = 1180.18
	minimum = 24
	maximum = 2274
Network latency average = 1132.29
	minimum = 23
	maximum = 2255
Slowest packet = 3877
Flit latency average = 1086.56
	minimum = 5
	maximum = 2244
Slowest flit = 73725
Fragmentation average = 107.246
	minimum = 0
	maximum = 725
Injected packet rate average = 0.0200521
	minimum = 0.002 (at node 144)
	maximum = 0.032 (at node 23)
Accepted packet rate average = 0.0150781
	minimum = 0.005 (at node 146)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.360589
	minimum = 0.036 (at node 144)
	maximum = 0.587 (at node 23)
Accepted flit rate average= 0.273109
	minimum = 0.095 (at node 146)
	maximum = 0.509 (at node 103)
Injected packet length average = 17.9826
Accepted packet length average = 18.113
Total in-flight flits = 101325 (0 measured)
latency change    = 0.577431
throughput change = 0.012987
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 629.721
	minimum = 25
	maximum = 2035
Network latency average = 70.309
	minimum = 22
	maximum = 988
Slowest packet = 14302
Flit latency average = 1490.38
	minimum = 5
	maximum = 2742
Slowest flit = 119945
Fragmentation average = 7.24034
	minimum = 0
	maximum = 35
Injected packet rate average = 0.015776
	minimum = 0.002 (at node 176)
	maximum = 0.029 (at node 98)
Accepted packet rate average = 0.0148542
	minimum = 0.006 (at node 79)
	maximum = 0.025 (at node 140)
Injected flit rate average = 0.284062
	minimum = 0.039 (at node 176)
	maximum = 0.525 (at node 98)
Accepted flit rate average= 0.267901
	minimum = 0.101 (at node 79)
	maximum = 0.459 (at node 140)
Injected packet length average = 18.0059
Accepted packet length average = 18.0354
Total in-flight flits = 104464 (50247 measured)
latency change    = 0.874134
throughput change = 0.0194413
Class 0:
Packet latency average = 1489.79
	minimum = 25
	maximum = 3341
Network latency average = 679.157
	minimum = 22
	maximum = 1969
Slowest packet = 14302
Flit latency average = 1630.17
	minimum = 5
	maximum = 4074
Slowest flit = 90281
Fragmentation average = 47.03
	minimum = 0
	maximum = 659
Injected packet rate average = 0.015526
	minimum = 0.0055 (at node 0)
	maximum = 0.024 (at node 1)
Accepted packet rate average = 0.0148307
	minimum = 0.0085 (at node 79)
	maximum = 0.021 (at node 178)
Injected flit rate average = 0.279383
	minimum = 0.104 (at node 0)
	maximum = 0.431 (at node 105)
Accepted flit rate average= 0.267104
	minimum = 0.168 (at node 79)
	maximum = 0.4045 (at node 0)
Injected packet length average = 17.9945
Accepted packet length average = 18.0102
Total in-flight flits = 106235 (90375 measured)
latency change    = 0.577309
throughput change = 0.00298339
Class 0:
Packet latency average = 2214.91
	minimum = 25
	maximum = 3842
Network latency average = 1398.93
	minimum = 22
	maximum = 2950
Slowest packet = 14302
Flit latency average = 1737.8
	minimum = 5
	maximum = 4114
Slowest flit = 90287
Fragmentation average = 84.3381
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151875
	minimum = 0.005 (at node 176)
	maximum = 0.022 (at node 103)
Accepted packet rate average = 0.0148316
	minimum = 0.00933333 (at node 79)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.273415
	minimum = 0.095 (at node 176)
	maximum = 0.393667 (at node 103)
Accepted flit rate average= 0.267132
	minimum = 0.168667 (at node 79)
	maximum = 0.36 (at node 128)
Injected packet length average = 18.0026
Accepted packet length average = 18.011
Total in-flight flits = 105155 (103551 measured)
latency change    = 0.327381
throughput change = 0.000103985
Draining remaining packets ...
Class 0:
Remaining flits: 247446 247447 247448 247449 247450 247451 247452 247453 247454 247455 [...] (57464 flits)
Measured flits: 258480 258481 258482 258483 258484 258485 258486 258487 258488 258489 [...] (57446 flits)
Class 0:
Remaining flits: 293454 293455 293456 293457 293458 293459 293460 293461 293462 293463 [...] (11257 flits)
Measured flits: 293454 293455 293456 293457 293458 293459 293460 293461 293462 293463 [...] (11257 flits)
Time taken is 8827 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3082.87 (1 samples)
	minimum = 25 (1 samples)
	maximum = 6016 (1 samples)
Network latency average = 2048.15 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4888 (1 samples)
Flit latency average = 1961.49 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4908 (1 samples)
Fragmentation average = 104.536 (1 samples)
	minimum = 0 (1 samples)
	maximum = 726 (1 samples)
Injected packet rate average = 0.0151875 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0148316 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.273415 (1 samples)
	minimum = 0.095 (1 samples)
	maximum = 0.393667 (1 samples)
Accepted flit rate average = 0.267132 (1 samples)
	minimum = 0.168667 (1 samples)
	maximum = 0.36 (1 samples)
Injected packet size average = 18.0026 (1 samples)
Accepted packet size average = 18.011 (1 samples)
Hops average = 5.13412 (1 samples)
Total run time 13.6008
