BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.164
	minimum = 23
	maximum = 952
Network latency average = 338.315
	minimum = 23
	maximum = 932
Slowest packet = 276
Flit latency average = 266.112
	minimum = 6
	maximum = 957
Slowest flit = 2246
Fragmentation average = 169.36
	minimum = 0
	maximum = 860
Injected packet rate average = 0.0220781
	minimum = 0.008 (at node 20)
	maximum = 0.036 (at node 137)
Accepted packet rate average = 0.010125
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.389453
	minimum = 0.135 (at node 20)
	maximum = 0.642 (at node 137)
Accepted flit rate average= 0.208417
	minimum = 0.086 (at node 41)
	maximum = 0.364 (at node 131)
Injected packet length average = 17.6398
Accepted packet length average = 20.5844
Total in-flight flits = 36430 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 722.742
	minimum = 23
	maximum = 1891
Network latency average = 568.507
	minimum = 23
	maximum = 1876
Slowest packet = 386
Flit latency average = 467.064
	minimum = 6
	maximum = 1896
Slowest flit = 11323
Fragmentation average = 192.465
	minimum = 0
	maximum = 1624
Injected packet rate average = 0.0165625
	minimum = 0.0055 (at node 16)
	maximum = 0.0275 (at node 191)
Accepted packet rate average = 0.0107318
	minimum = 0.0055 (at node 23)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.294518
	minimum = 0.0925 (at node 184)
	maximum = 0.4865 (at node 191)
Accepted flit rate average= 0.20438
	minimum = 0.11 (at node 164)
	maximum = 0.301 (at node 22)
Injected packet length average = 17.7822
Accepted packet length average = 19.0444
Total in-flight flits = 36376 (0 measured)
latency change    = 0.485067
throughput change = 0.0197498
Class 0:
Packet latency average = 1750.65
	minimum = 612
	maximum = 2935
Network latency average = 958.718
	minimum = 28
	maximum = 2844
Slowest packet = 629
Flit latency average = 826.728
	minimum = 6
	maximum = 2801
Slowest flit = 21323
Fragmentation average = 210.889
	minimum = 1
	maximum = 2671
Injected packet rate average = 0.01075
	minimum = 0 (at node 3)
	maximum = 0.028 (at node 11)
Accepted packet rate average = 0.010875
	minimum = 0.005 (at node 10)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.193635
	minimum = 0 (at node 3)
	maximum = 0.499 (at node 11)
Accepted flit rate average= 0.196057
	minimum = 0.096 (at node 184)
	maximum = 0.384 (at node 152)
Injected packet length average = 18.0126
Accepted packet length average = 18.0283
Total in-flight flits = 36011 (0 measured)
latency change    = 0.587157
throughput change = 0.0424515
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2466.9
	minimum = 934
	maximum = 3557
Network latency average = 358.735
	minimum = 27
	maximum = 976
Slowest packet = 8469
Flit latency average = 882.738
	minimum = 6
	maximum = 3684
Slowest flit = 26207
Fragmentation average = 125.065
	minimum = 0
	maximum = 778
Injected packet rate average = 0.0108229
	minimum = 0 (at node 2)
	maximum = 0.028 (at node 45)
Accepted packet rate average = 0.010724
	minimum = 0.005 (at node 7)
	maximum = 0.018 (at node 59)
Injected flit rate average = 0.195344
	minimum = 0 (at node 46)
	maximum = 0.501 (at node 45)
Accepted flit rate average= 0.192974
	minimum = 0.065 (at node 7)
	maximum = 0.336 (at node 59)
Injected packet length average = 18.0491
Accepted packet length average = 17.9947
Total in-flight flits = 36382 (24990 measured)
latency change    = 0.290345
throughput change = 0.015978
Class 0:
Packet latency average = 2957.65
	minimum = 934
	maximum = 4646
Network latency average = 595.388
	minimum = 24
	maximum = 1957
Slowest packet = 8469
Flit latency average = 902.73
	minimum = 6
	maximum = 4703
Slowest flit = 29393
Fragmentation average = 158.479
	minimum = 0
	maximum = 1384
Injected packet rate average = 0.0110391
	minimum = 0 (at node 93)
	maximum = 0.023 (at node 94)
Accepted packet rate average = 0.0107708
	minimum = 0.0065 (at node 85)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.19849
	minimum = 0.0005 (at node 120)
	maximum = 0.409 (at node 94)
Accepted flit rate average= 0.193112
	minimum = 0.1165 (at node 17)
	maximum = 0.33 (at node 129)
Injected packet length average = 17.9807
Accepted packet length average = 17.9292
Total in-flight flits = 38032 (34756 measured)
latency change    = 0.165926
throughput change = 0.000714719
Class 0:
Packet latency average = 3408.58
	minimum = 934
	maximum = 5304
Network latency average = 751.331
	minimum = 24
	maximum = 2985
Slowest packet = 8469
Flit latency average = 910.549
	minimum = 6
	maximum = 5094
Slowest flit = 30419
Fragmentation average = 173.203
	minimum = 0
	maximum = 2051
Injected packet rate average = 0.0107135
	minimum = 0 (at node 93)
	maximum = 0.0223333 (at node 94)
Accepted packet rate average = 0.0107431
	minimum = 0.00733333 (at node 35)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.192861
	minimum = 0.00433333 (at node 140)
	maximum = 0.4 (at node 143)
Accepted flit rate average= 0.193333
	minimum = 0.13 (at node 35)
	maximum = 0.291333 (at node 129)
Injected packet length average = 18.0016
Accepted packet length average = 17.9961
Total in-flight flits = 35657 (34373 measured)
latency change    = 0.132292
throughput change = 0.00114494
Class 0:
Packet latency average = 3827.26
	minimum = 934
	maximum = 6332
Network latency average = 838.457
	minimum = 24
	maximum = 3568
Slowest packet = 8469
Flit latency average = 918.288
	minimum = 6
	maximum = 6673
Slowest flit = 32471
Fragmentation average = 182.761
	minimum = 0
	maximum = 2051
Injected packet rate average = 0.0107799
	minimum = 0.00325 (at node 144)
	maximum = 0.02075 (at node 94)
Accepted packet rate average = 0.0107617
	minimum = 0.00675 (at node 35)
	maximum = 0.01575 (at node 129)
Injected flit rate average = 0.194095
	minimum = 0.05825 (at node 144)
	maximum = 0.37425 (at node 94)
Accepted flit rate average= 0.193798
	minimum = 0.12725 (at node 35)
	maximum = 0.2845 (at node 129)
Injected packet length average = 18.0052
Accepted packet length average = 18.0081
Total in-flight flits = 36142 (35671 measured)
latency change    = 0.109396
throughput change = 0.0023986
Class 0:
Packet latency average = 4219.24
	minimum = 934
	maximum = 7160
Network latency average = 890.123
	minimum = 24
	maximum = 4649
Slowest packet = 8469
Flit latency average = 919.56
	minimum = 6
	maximum = 6673
Slowest flit = 32471
Fragmentation average = 189.024
	minimum = 0
	maximum = 2429
Injected packet rate average = 0.0107187
	minimum = 0.003 (at node 172)
	maximum = 0.0218 (at node 94)
Accepted packet rate average = 0.0107656
	minimum = 0.0074 (at node 36)
	maximum = 0.0158 (at node 129)
Injected flit rate average = 0.193092
	minimum = 0.0524 (at node 172)
	maximum = 0.3924 (at node 94)
Accepted flit rate average= 0.193866
	minimum = 0.1318 (at node 36)
	maximum = 0.278 (at node 129)
Injected packet length average = 18.0144
Accepted packet length average = 18.0078
Total in-flight flits = 35030 (34795 measured)
latency change    = 0.0929028
throughput change = 0.000347911
Class 0:
Packet latency average = 4610.02
	minimum = 934
	maximum = 7912
Network latency average = 923.301
	minimum = 24
	maximum = 4923
Slowest packet = 8469
Flit latency average = 920.952
	minimum = 6
	maximum = 6844
Slowest flit = 92825
Fragmentation average = 193.167
	minimum = 0
	maximum = 2671
Injected packet rate average = 0.0108385
	minimum = 0.0035 (at node 172)
	maximum = 0.0206667 (at node 94)
Accepted packet rate average = 0.0107839
	minimum = 0.0075 (at node 64)
	maximum = 0.0148333 (at node 129)
Injected flit rate average = 0.195204
	minimum = 0.063 (at node 172)
	maximum = 0.3715 (at node 94)
Accepted flit rate average= 0.19421
	minimum = 0.136833 (at node 64)
	maximum = 0.265833 (at node 129)
Injected packet length average = 18.0102
Accepted packet length average = 18.0093
Total in-flight flits = 36921 (36886 measured)
latency change    = 0.0847679
throughput change = 0.00177357
Class 0:
Packet latency average = 4985.6
	minimum = 934
	maximum = 8837
Network latency average = 948.471
	minimum = 24
	maximum = 6102
Slowest packet = 8469
Flit latency average = 922.211
	minimum = 6
	maximum = 6853
Slowest flit = 146303
Fragmentation average = 195.639
	minimum = 0
	maximum = 4236
Injected packet rate average = 0.01084
	minimum = 0.003 (at node 172)
	maximum = 0.0194286 (at node 94)
Accepted packet rate average = 0.0107753
	minimum = 0.008 (at node 64)
	maximum = 0.0142857 (at node 128)
Injected flit rate average = 0.195083
	minimum = 0.054 (at node 172)
	maximum = 0.348 (at node 94)
Accepted flit rate average= 0.194152
	minimum = 0.142571 (at node 64)
	maximum = 0.253714 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 18.0182
Total in-flight flits = 37187 (37187 measured)
latency change    = 0.0753329
throughput change = 0.000300197
Draining all recorded packets ...
Class 0:
Remaining flits: 215811 215812 215813 215814 215815 215816 215817 215818 215819 217896 [...] (36902 flits)
Measured flits: 215811 215812 215813 215814 215815 215816 215817 215818 215819 217896 [...] (36902 flits)
Class 0:
Remaining flits: 242178 242179 242180 242181 242182 242183 242184 242185 242186 242187 [...] (35443 flits)
Measured flits: 242178 242179 242180 242181 242182 242183 242184 242185 242186 242187 [...] (35443 flits)
Class 0:
Remaining flits: 285697 285698 285699 285700 285701 285702 285703 285704 285705 285706 [...] (36549 flits)
Measured flits: 285697 285698 285699 285700 285701 285702 285703 285704 285705 285706 [...] (36549 flits)
Class 0:
Remaining flits: 345884 345885 345886 345887 351612 351613 351614 351615 351616 351617 [...] (35166 flits)
Measured flits: 345884 345885 345886 345887 351612 351613 351614 351615 351616 351617 [...] (35166 flits)
Class 0:
Remaining flits: 345885 345886 345887 355050 355051 355052 355053 355054 355055 355056 [...] (37836 flits)
Measured flits: 345885 345886 345887 355050 355051 355052 355053 355054 355055 355056 [...] (37836 flits)
Class 0:
Remaining flits: 355065 355066 355067 366210 366211 366212 366213 366214 366215 366216 [...] (36594 flits)
Measured flits: 355065 355066 355067 366210 366211 366212 366213 366214 366215 366216 [...] (36594 flits)
Class 0:
Remaining flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (35508 flits)
Measured flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (35508 flits)
Class 0:
Remaining flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (37017 flits)
Measured flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (37017 flits)
Class 0:
Remaining flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (34897 flits)
Measured flits: 412416 412417 412418 412419 412420 412421 412422 412423 412424 412425 [...] (34897 flits)
Class 0:
Remaining flits: 524484 524485 524486 524487 524488 524489 524490 524491 524492 524493 [...] (36190 flits)
Measured flits: 524484 524485 524486 524487 524488 524489 524490 524491 524492 524493 [...] (36190 flits)
Class 0:
Remaining flits: 524501 532944 532945 532946 532947 532948 532949 532950 532951 532952 [...] (35950 flits)
Measured flits: 524501 532944 532945 532946 532947 532948 532949 532950 532951 532952 [...] (35950 flits)
Class 0:
Remaining flits: 532944 532945 532946 532947 532948 532949 532950 532951 532952 532953 [...] (37297 flits)
Measured flits: 532944 532945 532946 532947 532948 532949 532950 532951 532952 532953 [...] (37297 flits)
Class 0:
Remaining flits: 593226 593227 593228 593229 593230 593231 593232 593233 593234 593235 [...] (37585 flits)
Measured flits: 593226 593227 593228 593229 593230 593231 593232 593233 593234 593235 [...] (37585 flits)
Class 0:
Remaining flits: 650304 650305 650306 650307 650308 650309 650310 650311 650312 650313 [...] (37221 flits)
Measured flits: 650304 650305 650306 650307 650308 650309 650310 650311 650312 650313 [...] (37221 flits)
Class 0:
Remaining flits: 650304 650305 650306 650307 650308 650309 650310 650311 650312 650313 [...] (37568 flits)
Measured flits: 650304 650305 650306 650307 650308 650309 650310 650311 650312 650313 [...] (37568 flits)
Class 0:
Remaining flits: 667620 667621 667622 667623 667624 667625 667626 667627 667628 667629 [...] (36986 flits)
Measured flits: 667620 667621 667622 667623 667624 667625 667626 667627 667628 667629 [...] (36878 flits)
Class 0:
Remaining flits: 727463 727464 727465 727466 727467 727468 727469 741939 741940 741941 [...] (36186 flits)
Measured flits: 727463 727464 727465 727466 727467 727468 727469 741939 741940 741941 [...] (36042 flits)
Class 0:
Remaining flits: 796374 796375 796376 796377 796378 796379 796380 796381 796382 796383 [...] (36746 flits)
Measured flits: 796374 796375 796376 796377 796378 796379 796380 796381 796382 796383 [...] (35868 flits)
Class 0:
Remaining flits: 807387 807388 807389 822312 822313 822314 822315 822316 822317 822318 [...] (36704 flits)
Measured flits: 807387 807388 807389 822312 822313 822314 822315 822316 822317 822318 [...] (35475 flits)
Class 0:
Remaining flits: 874471 874472 874473 874474 874475 874606 874607 874608 874609 874610 [...] (35199 flits)
Measured flits: 874471 874472 874473 874474 874475 874606 874607 874608 874609 874610 [...] (32954 flits)
Class 0:
Remaining flits: 874471 874472 874473 874474 874475 882324 882325 882326 882327 882328 [...] (35887 flits)
Measured flits: 874471 874472 874473 874474 874475 882324 882325 882326 882327 882328 [...] (32011 flits)
Class 0:
Remaining flits: 927128 927129 927130 927131 927132 927133 927134 927135 927136 927137 [...] (35082 flits)
Measured flits: 927128 927129 927130 927131 927132 927133 927134 927135 927136 927137 [...] (27103 flits)
Class 0:
Remaining flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (37226 flits)
Measured flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (25016 flits)
Class 0:
Remaining flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (35822 flits)
Measured flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (21393 flits)
Class 0:
Remaining flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (36266 flits)
Measured flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (17738 flits)
Class 0:
Remaining flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (37584 flits)
Measured flits: 975798 975799 975800 975801 975802 975803 975804 975805 975806 975807 [...] (15681 flits)
Class 0:
Remaining flits: 1096830 1096831 1096832 1096833 1096834 1096835 1096836 1096837 1096838 1096839 [...] (36615 flits)
Measured flits: 1102758 1102759 1102760 1102761 1102762 1102763 1102764 1102765 1102766 1102767 [...] (13824 flits)
Class 0:
Remaining flits: 1102765 1102766 1102767 1102768 1102769 1108622 1108623 1108624 1108625 1108626 [...] (35615 flits)
Measured flits: 1102765 1102766 1102767 1102768 1102769 1108622 1108623 1108624 1108625 1108626 [...] (11207 flits)
Class 0:
Remaining flits: 1129284 1129285 1129286 1129287 1129288 1129289 1129290 1129291 1129292 1129293 [...] (37211 flits)
Measured flits: 1170305 1170983 1170984 1170985 1170986 1170987 1170988 1170989 1187782 1187783 [...] (9953 flits)
Class 0:
Remaining flits: 1170983 1170984 1170985 1170986 1170987 1170988 1170989 1183788 1183789 1183790 [...] (38137 flits)
Measured flits: 1170983 1170984 1170985 1170986 1170987 1170988 1170989 1205131 1205132 1205133 [...] (8960 flits)
Class 0:
Remaining flits: 1183788 1183789 1183790 1183791 1183792 1183793 1183794 1183795 1183796 1183797 [...] (36760 flits)
Measured flits: 1205131 1205132 1205133 1205134 1205135 1243008 1243009 1243010 1243011 1243012 [...] (7879 flits)
Class 0:
Remaining flits: 1183788 1183789 1183790 1183791 1183792 1183793 1183794 1183795 1183796 1183797 [...] (37727 flits)
Measured flits: 1243025 1278666 1278667 1278668 1278669 1278670 1278671 1278672 1278673 1278674 [...] (5887 flits)
Class 0:
Remaining flits: 1183788 1183789 1183790 1183791 1183792 1183793 1183794 1183795 1183796 1183797 [...] (36837 flits)
Measured flits: 1310994 1310995 1310996 1310997 1310998 1310999 1311000 1311001 1311002 1311003 [...] (4978 flits)
Class 0:
Remaining flits: 1183794 1183795 1183796 1183797 1183798 1183799 1183800 1183801 1183802 1183803 [...] (36732 flits)
Measured flits: 1310994 1310995 1310996 1310997 1310998 1310999 1311000 1311001 1311002 1311003 [...] (4342 flits)
Class 0:
Remaining flits: 1310994 1310995 1310996 1310997 1310998 1310999 1311000 1311001 1311002 1311003 [...] (36786 flits)
Measured flits: 1310994 1310995 1310996 1310997 1310998 1310999 1311000 1311001 1311002 1311003 [...] (3610 flits)
Class 0:
Remaining flits: 1311011 1365984 1365985 1365986 1365987 1365988 1365989 1365990 1365991 1365992 [...] (35336 flits)
Measured flits: 1311011 1512126 1512127 1512128 1512129 1512130 1512131 1512132 1512133 1512134 [...] (2736 flits)
Class 0:
Remaining flits: 1468854 1468855 1468856 1468857 1468858 1468859 1468860 1468861 1468862 1468863 [...] (36359 flits)
Measured flits: 1514038 1514039 1514040 1514041 1514042 1514043 1514044 1514045 1514046 1514047 [...] (2968 flits)
Class 0:
Remaining flits: 1518744 1518745 1518746 1518747 1518748 1518749 1536336 1536337 1536338 1536339 [...] (36057 flits)
Measured flits: 1613052 1613053 1613054 1613055 1613056 1613057 1613058 1613059 1613060 1613061 [...] (2418 flits)
Class 0:
Remaining flits: 1570883 1570884 1570885 1570886 1570887 1570888 1570889 1570890 1570891 1570892 [...] (37479 flits)
Measured flits: 1665414 1665415 1665416 1665417 1665418 1665419 1665420 1665421 1665422 1665423 [...] (1837 flits)
Class 0:
Remaining flits: 1570892 1570893 1570894 1570895 1596590 1596591 1596592 1596593 1596594 1596595 [...] (34541 flits)
Measured flits: 1707460 1707461 1716678 1716679 1716680 1716681 1716682 1716683 1716684 1716685 [...] (1655 flits)
Class 0:
Remaining flits: 1618812 1618813 1618814 1618815 1618816 1618817 1618818 1618819 1618820 1618821 [...] (36224 flits)
Measured flits: 1716695 1786392 1786393 1786394 1786395 1786396 1786397 1786398 1786399 1786400 [...] (1186 flits)
Class 0:
Remaining flits: 1696458 1696459 1696460 1696461 1696462 1696463 1707165 1707166 1707167 1707168 [...] (37049 flits)
Measured flits: 1716695 1786395 1786396 1786397 1786398 1786399 1786400 1786401 1786402 1786403 [...] (935 flits)
Class 0:
Remaining flits: 1696458 1696459 1696460 1696461 1696462 1696463 1716695 1733004 1733005 1733006 [...] (36446 flits)
Measured flits: 1716695 1854254 1854255 1854256 1854257 1854258 1854259 1854260 1854261 1854262 [...] (906 flits)
Class 0:
Remaining flits: 1716695 1773615 1773616 1773617 1773618 1773619 1773620 1773621 1773622 1773623 [...] (35922 flits)
Measured flits: 1716695 1876194 1876195 1876196 1876197 1876198 1876199 1876200 1876201 1876202 [...] (659 flits)
Class 0:
Remaining flits: 1716695 1798866 1798867 1798868 1798869 1798870 1798871 1798872 1798873 1798874 [...] (36128 flits)
Measured flits: 1716695 1880730 1880731 1880732 1880733 1880734 1880735 1880736 1880737 1880738 [...] (609 flits)
Class 0:
Remaining flits: 1798879 1798880 1798881 1798882 1798883 1820844 1820845 1820846 1820847 1820848 [...] (38668 flits)
Measured flits: 1893762 1893763 1893764 1893765 1893766 1893767 1893768 1893769 1893770 1893771 [...] (549 flits)
Class 0:
Remaining flits: 1848672 1848673 1848674 1848675 1848676 1848677 1848678 1848679 1848680 1848681 [...] (38398 flits)
Measured flits: 2010789 2010790 2010791 2010792 2010793 2010794 2010795 2010796 2010797 2051948 [...] (513 flits)
Class 0:
Remaining flits: 1848687 1848688 1848689 1907460 1907461 1907462 1907463 1907464 1907465 1907466 [...] (37986 flits)
Measured flits: 2051961 2051962 2051963 2077647 2077648 2077649 2081788 2081789 2082726 2082727 [...] (214 flits)
Class 0:
Remaining flits: 1848687 1848688 1848689 1907460 1907461 1907462 1907463 1907464 1907465 1907466 [...] (37732 flits)
Measured flits: 2100726 2100727 2100728 2100729 2100730 2100731 2100732 2100733 2100734 2100735 [...] (327 flits)
Class 0:
Remaining flits: 1848687 1848688 1848689 1971306 1971307 1971308 1971309 1971310 1971311 1971312 [...] (35573 flits)
Measured flits: 2100726 2100727 2100728 2100729 2100730 2100731 2100732 2100733 2100734 2100735 [...] (288 flits)
Class 0:
Remaining flits: 1971306 1971307 1971308 1971309 1971310 1971311 1971312 1971313 1971314 1971315 [...] (36211 flits)
Measured flits: 2102238 2102239 2102240 2102241 2102242 2102243 2102244 2102245 2102246 2102247 [...] (267 flits)
Class 0:
Remaining flits: 2007738 2007739 2007740 2007741 2007742 2007743 2007744 2007745 2007746 2007747 [...] (37026 flits)
Measured flits: 2219580 2219581 2219582 2219583 2219584 2219585 2219586 2219587 2219588 2219589 [...] (320 flits)
Class 0:
Remaining flits: 2007738 2007739 2007740 2007741 2007742 2007743 2007744 2007745 2007746 2007747 [...] (36839 flits)
Measured flits: 2219580 2219581 2219582 2219583 2219584 2219585 2219586 2219587 2219588 2219589 [...] (265 flits)
Class 0:
Remaining flits: 2040668 2040669 2040670 2040671 2040672 2040673 2040674 2040675 2040676 2040677 [...] (36498 flits)
Measured flits: 2219580 2219581 2219582 2219583 2219584 2219585 2219586 2219587 2219588 2219589 [...] (218 flits)
Class 0:
Remaining flits: 2040675 2040676 2040677 2114550 2114551 2114552 2114553 2114554 2114555 2114556 [...] (37147 flits)
Measured flits: 2219580 2219581 2219582 2219583 2219584 2219585 2219586 2219587 2219588 2219589 [...] (137 flits)
Class 0:
Remaining flits: 2114550 2114551 2114552 2114553 2114554 2114555 2114556 2114557 2114558 2114559 [...] (38797 flits)
Measured flits: 2227572 2227573 2227574 2227575 2227576 2227577 2227578 2227579 2227580 2227581 [...] (151 flits)
Class 0:
Remaining flits: 2114550 2114551 2114552 2114553 2114554 2114555 2114556 2114557 2114558 2114559 [...] (38442 flits)
Measured flits: 2227572 2227573 2227574 2227575 2227576 2227577 2227578 2227579 2227580 2227581 [...] (110 flits)
Class 0:
Remaining flits: 2215674 2215675 2215676 2215677 2215678 2215679 2215680 2215681 2215682 2215683 [...] (37838 flits)
Measured flits: 2292450 2292451 2292452 2292453 2292454 2292455 2292456 2292457 2292458 2292459 [...] (227 flits)
Class 0:
Remaining flits: 2304990 2304991 2304992 2304993 2304994 2304995 2304996 2304997 2304998 2304999 [...] (35801 flits)
Measured flits: 2500128 2500129 2500130 2500131 2500132 2500133 2500134 2500135 2500136 2500137 [...] (120 flits)
Class 0:
Remaining flits: 2338560 2338561 2338562 2338563 2338564 2338565 2338566 2338567 2338568 2338569 [...] (37002 flits)
Measured flits: 2516382 2516383 2516384 2516385 2516386 2516387 2516388 2516389 2516390 2516391 [...] (131 flits)
Class 0:
Remaining flits: 2351322 2351323 2351324 2351325 2351326 2351327 2351328 2351329 2351330 2351331 [...] (38282 flits)
Measured flits: 2561161 2561162 2561163 2561164 2561165 2590578 2590579 2590580 2590581 2590582 [...] (59 flits)
Class 0:
Remaining flits: 2351322 2351323 2351324 2351325 2351326 2351327 2351328 2351329 2351330 2351331 [...] (37234 flits)
Measured flits: 2603736 2603737 2603738 2603739 2603740 2603741 2603742 2603743 2603744 2603745 [...] (36 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (35589 flits)
Measured flits: 2662380 2662381 2662382 2662383 2662384 2662385 2662386 2662387 2662388 2662389 [...] (18 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (35163 flits)
Measured flits: 2662380 2662381 2662382 2662383 2662384 2662385 2662386 2662387 2662388 2662389 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2585813 2585814 2585815 2585816 2585817 2585818 2585819 2585820 2585821 2585822 [...] (4613 flits)
Measured flits: (0 flits)
Time taken is 76707 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15908.4 (1 samples)
	minimum = 934 (1 samples)
	maximum = 64806 (1 samples)
Network latency average = 1065.27 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11972 (1 samples)
Flit latency average = 956.942 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13136 (1 samples)
Fragmentation average = 206.297 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5760 (1 samples)
Injected packet rate average = 0.01084 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.0194286 (1 samples)
Accepted packet rate average = 0.0107753 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0142857 (1 samples)
Injected flit rate average = 0.195083 (1 samples)
	minimum = 0.054 (1 samples)
	maximum = 0.348 (1 samples)
Accepted flit rate average = 0.194152 (1 samples)
	minimum = 0.142571 (1 samples)
	maximum = 0.253714 (1 samples)
Injected packet size average = 17.9965 (1 samples)
Accepted packet size average = 18.0182 (1 samples)
Hops average = 5.05867 (1 samples)
Total run time 83.143
