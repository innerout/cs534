BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.002487
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 38.0638
	minimum = 22
	maximum = 70
Network latency average = 37.877
	minimum = 22
	maximum = 70
Slowest packet = 178
Flit latency average = 19.2663
	minimum = 5
	maximum = 53
Slowest flit = 3221
Fragmentation average = 2.83827
	minimum = 0
	maximum = 34
Injected packet rate average = 0.00238542
	minimum = 0 (at node 8)
	maximum = 0.007 (at node 124)
Accepted packet rate average = 0.00228646
	minimum = 0 (at node 11)
	maximum = 0.008 (at node 140)
Injected flit rate average = 0.0425625
	minimum = 0 (at node 8)
	maximum = 0.126 (at node 124)
Accepted flit rate average= 0.0416458
	minimum = 0 (at node 11)
	maximum = 0.144 (at node 140)
Injected packet length average = 17.8428
Accepted packet length average = 18.2141
Total in-flight flits = 248 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 38.6534
	minimum = 22
	maximum = 105
Network latency average = 38.3898
	minimum = 22
	maximum = 105
Slowest packet = 513
Flit latency average = 19.6769
	minimum = 5
	maximum = 88
Slowest flit = 9251
Fragmentation average = 3.05426
	minimum = 0
	maximum = 37
Injected packet rate average = 0.00238021
	minimum = 0 (at node 81)
	maximum = 0.0055 (at node 161)
Accepted packet rate average = 0.00235156
	minimum = 0.0005 (at node 35)
	maximum = 0.0055 (at node 140)
Injected flit rate average = 0.0427266
	minimum = 0 (at node 81)
	maximum = 0.099 (at node 161)
Accepted flit rate average= 0.042513
	minimum = 0.009 (at node 41)
	maximum = 0.099 (at node 140)
Injected packet length average = 17.9508
Accepted packet length average = 18.0786
Total in-flight flits = 127 (0 measured)
latency change    = 0.0152534
throughput change = 0.0203982
Class 0:
Packet latency average = 38.525
	minimum = 22
	maximum = 74
Network latency average = 37.975
	minimum = 22
	maximum = 67
Slowest packet = 1040
Flit latency average = 19.4306
	minimum = 5
	maximum = 50
Slowest flit = 18737
Fragmentation average = 2.75417
	minimum = 0
	maximum = 32
Injected packet rate average = 0.00256771
	minimum = 0 (at node 0)
	maximum = 0.006 (at node 18)
Accepted packet rate average = 0.0025
	minimum = 0 (at node 5)
	maximum = 0.007 (at node 22)
Injected flit rate average = 0.0460937
	minimum = 0 (at node 0)
	maximum = 0.108 (at node 18)
Accepted flit rate average= 0.0452656
	minimum = 0 (at node 10)
	maximum = 0.126 (at node 22)
Injected packet length average = 17.9513
Accepted packet length average = 18.1062
Total in-flight flits = 310 (0 measured)
latency change    = 0.00333232
throughput change = 0.06081
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 39.3192
	minimum = 22
	maximum = 85
Network latency average = 39.0063
	minimum = 22
	maximum = 85
Slowest packet = 1525
Flit latency average = 20.3641
	minimum = 5
	maximum = 68
Slowest flit = 27467
Fragmentation average = 3.28541
	minimum = 0
	maximum = 46
Injected packet rate average = 0.00255208
	minimum = 0 (at node 13)
	maximum = 0.008 (at node 120)
Accepted packet rate average = 0.00258854
	minimum = 0 (at node 32)
	maximum = 0.007 (at node 45)
Injected flit rate average = 0.045849
	minimum = 0 (at node 13)
	maximum = 0.148 (at node 177)
Accepted flit rate average= 0.0463594
	minimum = 0 (at node 32)
	maximum = 0.126 (at node 45)
Injected packet length average = 17.9653
Accepted packet length average = 17.9095
Total in-flight flits = 229 (229 measured)
latency change    = 0.0201998
throughput change = 0.0235929
Class 0:
Packet latency average = 39.0598
	minimum = 22
	maximum = 85
Network latency average = 38.7553
	minimum = 22
	maximum = 85
Slowest packet = 1525
Flit latency average = 20.1281
	minimum = 5
	maximum = 68
Slowest flit = 27467
Fragmentation average = 3.10363
	minimum = 0
	maximum = 46
Injected packet rate average = 0.00248177
	minimum = 0 (at node 134)
	maximum = 0.0065 (at node 120)
Accepted packet rate average = 0.0025
	minimum = 0 (at node 116)
	maximum = 0.006 (at node 65)
Injected flit rate average = 0.0446901
	minimum = 0 (at node 134)
	maximum = 0.117 (at node 120)
Accepted flit rate average= 0.0449245
	minimum = 0 (at node 116)
	maximum = 0.108 (at node 65)
Injected packet length average = 18.0073
Accepted packet length average = 17.9698
Total in-flight flits = 213 (213 measured)
latency change    = 0.00664135
throughput change = 0.0319402
Class 0:
Packet latency average = 39.1055
	minimum = 22
	maximum = 85
Network latency average = 38.7854
	minimum = 22
	maximum = 85
Slowest packet = 1525
Flit latency average = 20.0664
	minimum = 5
	maximum = 68
Slowest flit = 27467
Fragmentation average = 3.20467
	minimum = 0
	maximum = 46
Injected packet rate average = 0.00246875
	minimum = 0.000333333 (at node 134)
	maximum = 0.00566667 (at node 55)
Accepted packet rate average = 0.00249306
	minimum = 0.000333333 (at node 116)
	maximum = 0.00566667 (at node 44)
Injected flit rate average = 0.0444896
	minimum = 0.006 (at node 134)
	maximum = 0.102 (at node 55)
Accepted flit rate average= 0.0447292
	minimum = 0.006 (at node 116)
	maximum = 0.0993333 (at node 44)
Injected packet length average = 18.0211
Accepted packet length average = 17.9415
Total in-flight flits = 142 (142 measured)
latency change    = 0.00116851
throughput change = 0.00436656
Draining remaining packets ...
Time taken is 6038 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 39.1048 (1 samples)
	minimum = 22 (1 samples)
	maximum = 85 (1 samples)
Network latency average = 38.7764 (1 samples)
	minimum = 22 (1 samples)
	maximum = 85 (1 samples)
Flit latency average = 20.0606 (1 samples)
	minimum = 5 (1 samples)
	maximum = 68 (1 samples)
Fragmentation average = 3.19902 (1 samples)
	minimum = 0 (1 samples)
	maximum = 46 (1 samples)
Injected packet rate average = 0.00246875 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Accepted packet rate average = 0.00249306 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Injected flit rate average = 0.0444896 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.102 (1 samples)
Accepted flit rate average = 0.0447292 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0993333 (1 samples)
Injected packet size average = 18.0211 (1 samples)
Accepted packet size average = 17.9415 (1 samples)
Hops average = 5.06751 (1 samples)
Total run time 1.72122
