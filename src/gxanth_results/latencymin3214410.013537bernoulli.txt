BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 189.089
	minimum = 23
	maximum = 911
Network latency average = 186.794
	minimum = 23
	maximum = 889
Slowest packet = 191
Flit latency average = 121.347
	minimum = 6
	maximum = 872
Slowest flit = 3455
Fragmentation average = 113.611
	minimum = 0
	maximum = 730
Injected packet rate average = 0.0136094
	minimum = 0.006 (at node 87)
	maximum = 0.022 (at node 123)
Accepted packet rate average = 0.00896875
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 49)
Injected flit rate average = 0.242297
	minimum = 0.1 (at node 87)
	maximum = 0.396 (at node 123)
Accepted flit rate average= 0.181
	minimum = 0.061 (at node 174)
	maximum = 0.308 (at node 132)
Injected packet length average = 17.8037
Accepted packet length average = 20.1812
Total in-flight flits = 12282 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 292.645
	minimum = 23
	maximum = 1717
Network latency average = 290.168
	minimum = 23
	maximum = 1717
Slowest packet = 582
Flit latency average = 208.074
	minimum = 6
	maximum = 1700
Slowest flit = 10493
Fragmentation average = 152.179
	minimum = 0
	maximum = 1362
Injected packet rate average = 0.0133906
	minimum = 0.0075 (at node 161)
	maximum = 0.019 (at node 77)
Accepted packet rate average = 0.00991406
	minimum = 0.005 (at node 62)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.239911
	minimum = 0.135 (at node 161)
	maximum = 0.342 (at node 77)
Accepted flit rate average= 0.190432
	minimum = 0.1135 (at node 79)
	maximum = 0.3015 (at node 166)
Injected packet length average = 17.9164
Accepted packet length average = 19.2083
Total in-flight flits = 19430 (0 measured)
latency change    = 0.35386
throughput change = 0.0495309
Class 0:
Packet latency average = 523.857
	minimum = 23
	maximum = 2603
Network latency average = 520.994
	minimum = 23
	maximum = 2589
Slowest packet = 1011
Flit latency average = 415.111
	minimum = 6
	maximum = 2572
Slowest flit = 18215
Fragmentation average = 208.157
	minimum = 0
	maximum = 2177
Injected packet rate average = 0.0134844
	minimum = 0.004 (at node 136)
	maximum = 0.025 (at node 191)
Accepted packet rate average = 0.0112917
	minimum = 0.003 (at node 184)
	maximum = 0.019 (at node 68)
Injected flit rate average = 0.242443
	minimum = 0.064 (at node 136)
	maximum = 0.457 (at node 191)
Accepted flit rate average= 0.204641
	minimum = 0.058 (at node 184)
	maximum = 0.33 (at node 187)
Injected packet length average = 17.9795
Accepted packet length average = 18.1232
Total in-flight flits = 26741 (0 measured)
latency change    = 0.441365
throughput change = 0.0694307
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 324.541
	minimum = 25
	maximum = 943
Network latency average = 321.939
	minimum = 25
	maximum = 943
Slowest packet = 7812
Flit latency average = 545.444
	minimum = 6
	maximum = 3455
Slowest flit = 24753
Fragmentation average = 144.981
	minimum = 0
	maximum = 804
Injected packet rate average = 0.0140469
	minimum = 0.004 (at node 150)
	maximum = 0.025 (at node 84)
Accepted packet rate average = 0.0115469
	minimum = 0.003 (at node 180)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.252427
	minimum = 0.072 (at node 150)
	maximum = 0.45 (at node 84)
Accepted flit rate average= 0.209255
	minimum = 0.073 (at node 74)
	maximum = 0.401 (at node 16)
Injected packet length average = 17.9703
Accepted packet length average = 18.1222
Total in-flight flits = 35110 (28131 measured)
latency change    = 0.614147
throughput change = 0.0220524
Class 0:
Packet latency average = 507.099
	minimum = 24
	maximum = 1896
Network latency average = 504.383
	minimum = 24
	maximum = 1887
Slowest packet = 7836
Flit latency average = 612.693
	minimum = 6
	maximum = 4249
Slowest flit = 34462
Fragmentation average = 171.075
	minimum = 0
	maximum = 1501
Injected packet rate average = 0.0136901
	minimum = 0.0085 (at node 10)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.011599
	minimum = 0.0055 (at node 17)
	maximum = 0.0205 (at node 137)
Injected flit rate average = 0.246563
	minimum = 0.153 (at node 10)
	maximum = 0.392 (at node 126)
Accepted flit rate average= 0.209013
	minimum = 0.1025 (at node 17)
	maximum = 0.3515 (at node 137)
Injected packet length average = 18.0103
Accepted packet length average = 18.02
Total in-flight flits = 41106 (38666 measured)
latency change    = 0.360005
throughput change = 0.00115872
Class 0:
Packet latency average = 653.809
	minimum = 24
	maximum = 2955
Network latency average = 651.141
	minimum = 24
	maximum = 2947
Slowest packet = 7770
Flit latency average = 687.746
	minimum = 6
	maximum = 4652
Slowest flit = 62041
Fragmentation average = 186.893
	minimum = 0
	maximum = 2289
Injected packet rate average = 0.013658
	minimum = 0.00866667 (at node 18)
	maximum = 0.0196667 (at node 116)
Accepted packet rate average = 0.0115417
	minimum = 0.00766667 (at node 36)
	maximum = 0.0166667 (at node 0)
Injected flit rate average = 0.245995
	minimum = 0.156 (at node 18)
	maximum = 0.354 (at node 116)
Accepted flit rate average= 0.207187
	minimum = 0.136667 (at node 4)
	maximum = 0.297333 (at node 0)
Injected packet length average = 18.0111
Accepted packet length average = 17.9513
Total in-flight flits = 49007 (47941 measured)
latency change    = 0.224394
throughput change = 0.00881096
Class 0:
Packet latency average = 767.841
	minimum = 23
	maximum = 3839
Network latency average = 765.186
	minimum = 23
	maximum = 3823
Slowest packet = 7888
Flit latency average = 757.251
	minimum = 6
	maximum = 5195
Slowest flit = 65627
Fragmentation average = 193.51
	minimum = 0
	maximum = 2994
Injected packet rate average = 0.0136523
	minimum = 0.0085 (at node 172)
	maximum = 0.01775 (at node 126)
Accepted packet rate average = 0.0114935
	minimum = 0.00775 (at node 5)
	maximum = 0.015 (at node 66)
Injected flit rate average = 0.245932
	minimum = 0.15675 (at node 172)
	maximum = 0.3195 (at node 126)
Accepted flit rate average= 0.207303
	minimum = 0.139 (at node 5)
	maximum = 0.2775 (at node 129)
Injected packet length average = 18.0139
Accepted packet length average = 18.0366
Total in-flight flits = 56262 (55756 measured)
latency change    = 0.148509
throughput change = 0.000559014
Class 0:
Packet latency average = 865.002
	minimum = 23
	maximum = 4853
Network latency average = 862.325
	minimum = 23
	maximum = 4853
Slowest packet = 8025
Flit latency average = 828.492
	minimum = 6
	maximum = 6300
Slowest flit = 66725
Fragmentation average = 201.257
	minimum = 0
	maximum = 2994
Injected packet rate average = 0.0135729
	minimum = 0.01 (at node 58)
	maximum = 0.0172 (at node 116)
Accepted packet rate average = 0.0115125
	minimum = 0.0082 (at node 36)
	maximum = 0.0148 (at node 83)
Injected flit rate average = 0.244479
	minimum = 0.18 (at node 58)
	maximum = 0.3096 (at node 116)
Accepted flit rate average= 0.20743
	minimum = 0.1526 (at node 36)
	maximum = 0.2628 (at node 128)
Injected packet length average = 18.0123
Accepted packet length average = 18.0178
Total in-flight flits = 62148 (61930 measured)
latency change    = 0.112325
throughput change = 0.0006114
Class 0:
Packet latency average = 957.856
	minimum = 23
	maximum = 5496
Network latency average = 955.178
	minimum = 23
	maximum = 5496
Slowest packet = 8828
Flit latency average = 899.125
	minimum = 6
	maximum = 7162
Slowest flit = 82561
Fragmentation average = 203.221
	minimum = 0
	maximum = 2994
Injected packet rate average = 0.013599
	minimum = 0.0106667 (at node 163)
	maximum = 0.0165 (at node 4)
Accepted packet rate average = 0.0115043
	minimum = 0.00883333 (at node 2)
	maximum = 0.0143333 (at node 90)
Injected flit rate average = 0.244844
	minimum = 0.192167 (at node 163)
	maximum = 0.297 (at node 4)
Accepted flit rate average= 0.206982
	minimum = 0.1615 (at node 31)
	maximum = 0.258 (at node 128)
Injected packet length average = 18.0046
Accepted packet length average = 17.9916
Total in-flight flits = 70286 (70185 measured)
latency change    = 0.0969388
throughput change = 0.00216656
Class 0:
Packet latency average = 1047.3
	minimum = 23
	maximum = 6468
Network latency average = 1044.57
	minimum = 23
	maximum = 6458
Slowest packet = 8028
Flit latency average = 972.049
	minimum = 6
	maximum = 7565
Slowest flit = 82565
Fragmentation average = 206.277
	minimum = 0
	maximum = 2994
Injected packet rate average = 0.0135759
	minimum = 0.0105714 (at node 34)
	maximum = 0.0175714 (at node 80)
Accepted packet rate average = 0.0114814
	minimum = 0.00914286 (at node 79)
	maximum = 0.0138571 (at node 37)
Injected flit rate average = 0.24437
	minimum = 0.190286 (at node 34)
	maximum = 0.313857 (at node 80)
Accepted flit rate average= 0.206121
	minimum = 0.166571 (at node 149)
	maximum = 0.251857 (at node 90)
Injected packet length average = 18.0003
Accepted packet length average = 17.9526
Total in-flight flits = 78142 (78124 measured)
latency change    = 0.085401
throughput change = 0.00417468
Draining all recorded packets ...
Class 0:
Remaining flits: 135432 135433 135434 135435 135436 135437 135438 135439 135440 135441 [...] (87746 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (49545 flits)
Class 0:
Remaining flits: 173160 173161 173162 173163 173164 173165 173166 173167 173168 173169 [...] (94370 flits)
Measured flits: 173160 173161 173162 173163 173164 173165 173166 173167 173168 173169 [...] (29775 flits)
Class 0:
Remaining flits: 173862 173863 173864 173865 173866 173867 173868 173869 173870 173871 [...] (101368 flits)
Measured flits: 173862 173863 173864 173865 173866 173867 173868 173869 173870 173871 [...] (17252 flits)
Class 0:
Remaining flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (109257 flits)
Measured flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (10189 flits)
Class 0:
Remaining flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (116552 flits)
Measured flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (5569 flits)
Class 0:
Remaining flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (126713 flits)
Measured flits: 183456 183457 183458 183459 183460 183461 183462 183463 183464 183465 [...] (3568 flits)
Class 0:
Remaining flits: 183459 183460 183461 183462 183463 183464 183465 183466 183467 183468 [...] (133259 flits)
Measured flits: 183459 183460 183461 183462 183463 183464 183465 183466 183467 183468 [...] (2159 flits)
Class 0:
Remaining flits: 233838 233839 233840 233841 233842 233843 233844 233845 233846 233847 [...] (140858 flits)
Measured flits: 233838 233839 233840 233841 233842 233843 233844 233845 233846 233847 [...] (1375 flits)
Class 0:
Remaining flits: 233838 233839 233840 233841 233842 233843 233844 233845 233846 233847 [...] (150047 flits)
Measured flits: 233838 233839 233840 233841 233842 233843 233844 233845 233846 233847 [...] (866 flits)
Class 0:
Remaining flits: 330461 366570 366571 366572 366573 366574 366575 366576 366577 366578 [...] (158674 flits)
Measured flits: 330461 366570 366571 366572 366573 366574 366575 366576 366577 366578 [...] (490 flits)
Class 0:
Remaining flits: 366989 366990 366991 366992 366993 366994 366995 366996 366997 366998 [...] (165555 flits)
Measured flits: 366989 366990 366991 366992 366993 366994 366995 366996 366997 366998 [...] (264 flits)
Class 0:
Remaining flits: 398884 398885 398886 398887 398888 398889 398890 398891 398892 398893 [...] (171430 flits)
Measured flits: 398884 398885 398886 398887 398888 398889 398890 398891 398892 398893 [...] (143 flits)
Class 0:
Remaining flits: 417526 417527 434349 434350 434351 434352 434353 434354 434355 434356 [...] (177669 flits)
Measured flits: 417526 417527 434349 434350 434351 434352 434353 434354 434355 434356 [...] (99 flits)
Class 0:
Remaining flits: 445615 445616 445617 445618 445619 445620 445621 445622 445623 445624 [...] (184322 flits)
Measured flits: 445615 445616 445617 445618 445619 445620 445621 445622 445623 445624 [...] (47 flits)
Class 0:
Remaining flits: 456228 456229 456230 456231 456232 456233 456234 456235 456236 456237 [...] (190410 flits)
Measured flits: 456228 456229 456230 456231 456232 456233 456234 456235 456236 456237 [...] (36 flits)
Class 0:
Remaining flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (195255 flits)
Measured flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (18 flits)
Class 0:
Remaining flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (202812 flits)
Measured flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 536292 536293 536294 536295 536296 536297 536298 536299 536300 536301 [...] (175242 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 536292 536293 536294 536295 536296 536297 536298 536299 536300 536301 [...] (141841 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 558277 558278 558279 558280 558281 558282 558283 558284 558285 558286 [...] (109585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 560700 560701 560702 560703 560704 560705 560706 560707 560708 560709 [...] (78572 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 560700 560701 560702 560703 560704 560705 560706 560707 560708 560709 [...] (51992 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 560700 560701 560702 560703 560704 560705 560706 560707 560708 560709 [...] (28675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 560700 560701 560702 560703 560704 560705 560706 560707 560708 560709 [...] (10647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 560700 560701 560702 560703 560704 560705 560706 560707 560708 560709 [...] (1527 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609426 609427 609428 609429 609430 609431 609432 609433 609434 609435 [...] (140 flits)
Measured flits: (0 flits)
Time taken is 37146 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1651.83 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18100 (1 samples)
Network latency average = 1648.66 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18100 (1 samples)
Flit latency average = 3192.17 (1 samples)
	minimum = 6 (1 samples)
	maximum = 24810 (1 samples)
Fragmentation average = 221.845 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3549 (1 samples)
Injected packet rate average = 0.0135759 (1 samples)
	minimum = 0.0105714 (1 samples)
	maximum = 0.0175714 (1 samples)
Accepted packet rate average = 0.0114814 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.0138571 (1 samples)
Injected flit rate average = 0.24437 (1 samples)
	minimum = 0.190286 (1 samples)
	maximum = 0.313857 (1 samples)
Accepted flit rate average = 0.206121 (1 samples)
	minimum = 0.166571 (1 samples)
	maximum = 0.251857 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 17.9526 (1 samples)
Hops average = 5.06509 (1 samples)
Total run time 34.2688
