BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 342.631
	minimum = 24
	maximum = 977
Network latency average = 232.654
	minimum = 22
	maximum = 791
Slowest packet = 57
Flit latency average = 203.552
	minimum = 5
	maximum = 820
Slowest flit = 13822
Fragmentation average = 42.2157
	minimum = 0
	maximum = 504
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0137604
	minimum = 0.007 (at node 23)
	maximum = 0.024 (at node 76)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.258875
	minimum = 0.126 (at node 41)
	maximum = 0.432 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.813
Total in-flight flits = 50736 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 607.205
	minimum = 24
	maximum = 1920
Network latency average = 448.754
	minimum = 22
	maximum = 1517
Slowest packet = 57
Flit latency average = 415.571
	minimum = 5
	maximum = 1513
Slowest flit = 42040
Fragmentation average = 67.4219
	minimum = 0
	maximum = 739
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.014763
	minimum = 0.0075 (at node 116)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.275078
	minimum = 0.1385 (at node 116)
	maximum = 0.3985 (at node 152)
Injected packet length average = 17.9205
Accepted packet length average = 18.6329
Total in-flight flits = 105114 (0 measured)
latency change    = 0.435724
throughput change = 0.0589037
Class 0:
Packet latency average = 1322.47
	minimum = 24
	maximum = 2847
Network latency average = 1076.39
	minimum = 22
	maximum = 2322
Slowest packet = 4656
Flit latency average = 1042.06
	minimum = 5
	maximum = 2452
Slowest flit = 47515
Fragmentation average = 120.056
	minimum = 0
	maximum = 820
Injected packet rate average = 0.031875
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.0160417
	minimum = 0.007 (at node 61)
	maximum = 0.026 (at node 57)
Injected flit rate average = 0.573807
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.293229
	minimum = 0.141 (at node 94)
	maximum = 0.483 (at node 16)
Injected packet length average = 18.0018
Accepted packet length average = 18.2792
Total in-flight flits = 158974 (0 measured)
latency change    = 0.540856
throughput change = 0.0619005
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 351.804
	minimum = 24
	maximum = 1476
Network latency average = 34.5833
	minimum = 22
	maximum = 77
Slowest packet = 17887
Flit latency average = 1495.09
	minimum = 5
	maximum = 3267
Slowest flit = 57653
Fragmentation average = 6.09325
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0326719
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0161875
	minimum = 0.007 (at node 21)
	maximum = 0.028 (at node 103)
Injected flit rate average = 0.587896
	minimum = 0 (at node 5)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292792
	minimum = 0.136 (at node 21)
	maximum = 0.477 (at node 151)
Injected packet length average = 17.9939
Accepted packet length average = 18.0875
Total in-flight flits = 215672 (103757 measured)
latency change    = 2.75912
throughput change = 0.00149424
Class 0:
Packet latency average = 395.257
	minimum = 24
	maximum = 2572
Network latency average = 89.3954
	minimum = 22
	maximum = 1984
Slowest packet = 17887
Flit latency average = 1708.19
	minimum = 5
	maximum = 4101
Slowest flit = 86506
Fragmentation average = 8.93954
	minimum = 0
	maximum = 315
Injected packet rate average = 0.032362
	minimum = 0.004 (at node 123)
	maximum = 0.0555 (at node 0)
Accepted packet rate average = 0.0160677
	minimum = 0.007 (at node 134)
	maximum = 0.0225 (at node 27)
Injected flit rate average = 0.582534
	minimum = 0.0715 (at node 123)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.292549
	minimum = 0.1375 (at node 134)
	maximum = 0.421 (at node 128)
Injected packet length average = 18.0006
Accepted packet length average = 18.2073
Total in-flight flits = 270321 (204569 measured)
latency change    = 0.109938
throughput change = 0.000827851
Class 0:
Packet latency average = 972.794
	minimum = 23
	maximum = 3786
Network latency average = 701.185
	minimum = 22
	maximum = 2952
Slowest packet = 17887
Flit latency average = 1927.02
	minimum = 5
	maximum = 4980
Slowest flit = 97001
Fragmentation average = 39.913
	minimum = 0
	maximum = 627
Injected packet rate average = 0.0321302
	minimum = 0.00866667 (at node 95)
	maximum = 0.0556667 (at node 25)
Accepted packet rate average = 0.0160938
	minimum = 0.011 (at node 36)
	maximum = 0.022 (at node 47)
Injected flit rate average = 0.578356
	minimum = 0.156 (at node 95)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.293045
	minimum = 0.201 (at node 36)
	maximum = 0.396333 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 18.2086
Total in-flight flits = 323306 (292852 measured)
latency change    = 0.593689
throughput change = 0.00169141
Draining remaining packets ...
Class 0:
Remaining flits: 146240 146241 146242 146243 146244 146245 146246 146247 146248 146249 [...] (275809 flits)
Measured flits: 321138 321139 321140 321141 321142 321143 321144 321145 321146 321147 [...] (261331 flits)
Class 0:
Remaining flits: 150435 150436 150437 150438 150439 150440 150441 150442 150443 152514 [...] (228569 flits)
Measured flits: 321138 321139 321140 321141 321142 321143 321144 321145 321146 321147 [...] (221838 flits)
Class 0:
Remaining flits: 152531 180312 180313 180314 180315 180316 180317 180318 180319 180320 [...] (181192 flits)
Measured flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (178596 flits)
Class 0:
Remaining flits: 182195 212347 212348 212349 212350 212351 212352 212353 212354 212355 [...] (134012 flits)
Measured flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (133197 flits)
Class 0:
Remaining flits: 241046 241047 241048 241049 241050 241051 241052 241053 241054 241055 [...] (88327 flits)
Measured flits: 325782 325783 325784 325785 325786 325787 325788 325789 325790 325791 [...] (88158 flits)
Class 0:
Remaining flits: 303116 303117 303118 303119 307254 307255 307256 307257 307258 307259 [...] (47267 flits)
Measured flits: 325944 325945 325946 325947 325948 325949 325950 325951 325952 325953 [...] (47239 flits)
Class 0:
Remaining flits: 342895 342896 342897 342898 342899 344884 344885 344886 344887 344888 [...] (17439 flits)
Measured flits: 342895 342896 342897 342898 342899 344884 344885 344886 344887 344888 [...] (17439 flits)
Class 0:
Remaining flits: 394025 394026 394027 394028 394029 394030 394031 394032 394033 394034 [...] (4153 flits)
Measured flits: 394025 394026 394027 394028 394029 394030 394031 394032 394033 394034 [...] (4153 flits)
Class 0:
Remaining flits: 433344 433345 433346 433347 433348 433349 441504 441505 441506 441507 [...] (835 flits)
Measured flits: 433344 433345 433346 433347 433348 433349 441504 441505 441506 441507 [...] (835 flits)
Time taken is 15857 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5092.23 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12250 (1 samples)
Network latency average = 4794.59 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11370 (1 samples)
Flit latency average = 4040.99 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11353 (1 samples)
Fragmentation average = 239.879 (1 samples)
	minimum = 0 (1 samples)
	maximum = 958 (1 samples)
Injected packet rate average = 0.0321302 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0160938 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.578356 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.293045 (1 samples)
	minimum = 0.201 (1 samples)
	maximum = 0.396333 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 18.2086 (1 samples)
Hops average = 5.06452 (1 samples)
Total run time 15.1565
