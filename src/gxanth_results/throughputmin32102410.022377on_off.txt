BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.13
	minimum = 25
	maximum = 981
Network latency average = 252.532
	minimum = 25
	maximum = 947
Slowest packet = 140
Flit latency average = 187.92
	minimum = 6
	maximum = 979
Slowest flit = 1258
Fragmentation average = 135.299
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00939583
	minimum = 0.004 (at node 108)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.193828
	minimum = 0.087 (at node 108)
	maximum = 0.339 (at node 44)
Injected packet length average = 17.8417
Accepted packet length average = 20.6292
Total in-flight flits = 35559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.382
	minimum = 23
	maximum = 1920
Network latency average = 435.747
	minimum = 23
	maximum = 1838
Slowest packet = 438
Flit latency average = 356.202
	minimum = 6
	maximum = 1874
Slowest flit = 6706
Fragmentation average = 175.996
	minimum = 0
	maximum = 1671
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0107786
	minimum = 0.006 (at node 83)
	maximum = 0.017 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.208141
	minimum = 0.1175 (at node 96)
	maximum = 0.324 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 19.3105
Total in-flight flits = 70482 (0 measured)
latency change    = 0.394878
throughput change = 0.0687636
Class 0:
Packet latency average = 1084.14
	minimum = 27
	maximum = 2898
Network latency average = 946.909
	minimum = 23
	maximum = 2825
Slowest packet = 372
Flit latency average = 869.103
	minimum = 6
	maximum = 2864
Slowest flit = 8997
Fragmentation average = 225.292
	minimum = 0
	maximum = 1908
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0125208
	minimum = 0.004 (at node 190)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.22451
	minimum = 0.071 (at node 190)
	maximum = 0.403 (at node 9)
Injected packet length average = 18
Accepted packet length average = 17.9309
Total in-flight flits = 105280 (0 measured)
latency change    = 0.51355
throughput change = 0.0729133
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 303.616
	minimum = 23
	maximum = 1135
Network latency average = 187.798
	minimum = 23
	maximum = 940
Slowest packet = 12702
Flit latency average = 1258.12
	minimum = 6
	maximum = 3634
Slowest flit = 21149
Fragmentation average = 66.7566
	minimum = 0
	maximum = 522
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0124167
	minimum = 0.004 (at node 180)
	maximum = 0.025 (at node 128)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.222484
	minimum = 0.096 (at node 180)
	maximum = 0.483 (at node 128)
Injected packet length average = 17.9946
Accepted packet length average = 17.9182
Total in-flight flits = 138829 (67149 measured)
latency change    = 2.57077
throughput change = 0.00910644
Class 0:
Packet latency average = 607.776
	minimum = 23
	maximum = 2299
Network latency average = 498.142
	minimum = 23
	maximum = 1960
Slowest packet = 12702
Flit latency average = 1450.13
	minimum = 6
	maximum = 4657
Slowest flit = 19603
Fragmentation average = 111.149
	minimum = 0
	maximum = 1034
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0123854
	minimum = 0.006 (at node 91)
	maximum = 0.0195 (at node 175)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.223297
	minimum = 0.112 (at node 91)
	maximum = 0.3395 (at node 175)
Injected packet length average = 17.9936
Accepted packet length average = 18.029
Total in-flight flits = 172444 (125718 measured)
latency change    = 0.500447
throughput change = 0.00363865
Class 0:
Packet latency average = 1001.07
	minimum = 23
	maximum = 3200
Network latency average = 889.198
	minimum = 23
	maximum = 2930
Slowest packet = 12702
Flit latency average = 1631.68
	minimum = 6
	maximum = 5141
Slowest flit = 43798
Fragmentation average = 140.521
	minimum = 0
	maximum = 1481
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0123299
	minimum = 0.00766667 (at node 146)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.221319
	minimum = 0.136333 (at node 31)
	maximum = 0.337667 (at node 128)
Injected packet length average = 18.0055
Accepted packet length average = 17.9499
Total in-flight flits = 204852 (173855 measured)
latency change    = 0.392873
throughput change = 0.00893473
Draining remaining packets ...
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (169640 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (149117 flits)
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (134576 flits)
Measured flits: 228323 228324 228325 228326 228327 228328 228329 228334 228335 228336 [...] (121258 flits)
Class 0:
Remaining flits: 16902 16903 16904 16905 16906 16907 16908 16909 16910 16911 [...] (100218 flits)
Measured flits: 228351 228352 228353 228354 228355 228356 228357 228358 228359 228360 [...] (91420 flits)
Class 0:
Remaining flits: 16916 16917 16918 16919 81036 81037 81038 81039 81040 81041 [...] (67299 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (61808 flits)
Class 0:
Remaining flits: 99582 99583 99584 99585 99586 99587 99588 99589 99590 99591 [...] (37142 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (34419 flits)
Class 0:
Remaining flits: 123205 123206 123207 123208 123209 137178 137179 137180 137181 137182 [...] (13274 flits)
Measured flits: 228366 228367 228368 228369 228370 228371 228372 228373 228374 228375 [...] (12335 flits)
Class 0:
Remaining flits: 213787 213788 213789 213790 213791 213792 213793 213794 213795 213796 [...] (1136 flits)
Measured flits: 252536 252537 252538 252539 260010 260011 260012 260013 260014 260015 [...] (1119 flits)
Time taken is 13580 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3943.34 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10090 (1 samples)
Network latency average = 3800 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9980 (1 samples)
Flit latency average = 3550.45 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10776 (1 samples)
Fragmentation average = 184.415 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2534 (1 samples)
Injected packet rate average = 0.0218993 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0123299 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.394309 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.926 (1 samples)
Accepted flit rate average = 0.221319 (1 samples)
	minimum = 0.136333 (1 samples)
	maximum = 0.337667 (1 samples)
Injected packet size average = 18.0055 (1 samples)
Accepted packet size average = 17.9499 (1 samples)
Hops average = 5.08776 (1 samples)
Total run time 12.0499
