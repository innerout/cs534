BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 138.239
	minimum = 22
	maximum = 558
Network latency average = 134.557
	minimum = 22
	maximum = 539
Slowest packet = 844
Flit latency average = 103.607
	minimum = 5
	maximum = 522
Slowest flit = 15209
Fragmentation average = 30.2424
	minimum = 0
	maximum = 196
Injected packet rate average = 0.015875
	minimum = 0.007 (at node 185)
	maximum = 0.029 (at node 68)
Accepted packet rate average = 0.0126771
	minimum = 0.005 (at node 41)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.282448
	minimum = 0.126 (at node 185)
	maximum = 0.522 (at node 68)
Accepted flit rate average= 0.234187
	minimum = 0.095 (at node 41)
	maximum = 0.396 (at node 44)
Injected packet length average = 17.792
Accepted packet length average = 18.4733
Total in-flight flits = 9900 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 192.537
	minimum = 22
	maximum = 917
Network latency average = 189.011
	minimum = 22
	maximum = 917
Slowest packet = 3100
Flit latency average = 155.702
	minimum = 5
	maximum = 900
Slowest flit = 55817
Fragmentation average = 34.3368
	minimum = 0
	maximum = 232
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 33)
	maximum = 0.0255 (at node 68)
Accepted packet rate average = 0.0134922
	minimum = 0.0075 (at node 116)
	maximum = 0.02 (at node 71)
Injected flit rate average = 0.280359
	minimum = 0.1565 (at node 33)
	maximum = 0.459 (at node 68)
Accepted flit rate average= 0.246458
	minimum = 0.135 (at node 116)
	maximum = 0.36 (at node 71)
Injected packet length average = 17.9191
Accepted packet length average = 18.2667
Total in-flight flits = 13504 (0 measured)
latency change    = 0.282014
throughput change = 0.0497887
Class 0:
Packet latency average = 310.675
	minimum = 22
	maximum = 1092
Network latency average = 307.24
	minimum = 22
	maximum = 1071
Slowest packet = 4914
Flit latency average = 269.356
	minimum = 5
	maximum = 1102
Slowest flit = 102691
Fragmentation average = 43.4304
	minimum = 0
	maximum = 257
Injected packet rate average = 0.015625
	minimum = 0.005 (at node 4)
	maximum = 0.028 (at node 76)
Accepted packet rate average = 0.0145938
	minimum = 0.007 (at node 4)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.281099
	minimum = 0.09 (at node 4)
	maximum = 0.518 (at node 76)
Accepted flit rate average= 0.263292
	minimum = 0.121 (at node 163)
	maximum = 0.492 (at node 16)
Injected packet length average = 17.9903
Accepted packet length average = 18.0414
Total in-flight flits = 16952 (0 measured)
latency change    = 0.380262
throughput change = 0.0639342
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 297.24
	minimum = 22
	maximum = 1099
Network latency average = 292.189
	minimum = 22
	maximum = 942
Slowest packet = 9056
Flit latency average = 330.526
	minimum = 5
	maximum = 1335
Slowest flit = 138455
Fragmentation average = 41.1448
	minimum = 0
	maximum = 264
Injected packet rate average = 0.0162604
	minimum = 0.008 (at node 14)
	maximum = 0.029 (at node 131)
Accepted packet rate average = 0.0146615
	minimum = 0.007 (at node 1)
	maximum = 0.025 (at node 123)
Injected flit rate average = 0.292068
	minimum = 0.144 (at node 14)
	maximum = 0.514 (at node 131)
Accepted flit rate average= 0.266469
	minimum = 0.109 (at node 88)
	maximum = 0.459 (at node 123)
Injected packet length average = 17.9619
Accepted packet length average = 18.1748
Total in-flight flits = 22022 (21944 measured)
latency change    = 0.0451972
throughput change = 0.0119229
Class 0:
Packet latency average = 397.604
	minimum = 22
	maximum = 1581
Network latency average = 392.657
	minimum = 22
	maximum = 1563
Slowest packet = 9604
Flit latency average = 373.509
	minimum = 5
	maximum = 1546
Slowest flit = 172889
Fragmentation average = 49.8607
	minimum = 0
	maximum = 322
Injected packet rate average = 0.0158958
	minimum = 0.0085 (at node 129)
	maximum = 0.022 (at node 39)
Accepted packet rate average = 0.014638
	minimum = 0.009 (at node 36)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.286315
	minimum = 0.153 (at node 129)
	maximum = 0.396 (at node 39)
Accepted flit rate average= 0.26474
	minimum = 0.171 (at node 36)
	maximum = 0.4225 (at node 129)
Injected packet length average = 18.012
Accepted packet length average = 18.0857
Total in-flight flits = 25182 (25182 measured)
latency change    = 0.252421
throughput change = 0.00653158
Class 0:
Packet latency average = 451.009
	minimum = 22
	maximum = 2056
Network latency average = 445.926
	minimum = 22
	maximum = 2056
Slowest packet = 11795
Flit latency average = 411.496
	minimum = 5
	maximum = 2039
Slowest flit = 212327
Fragmentation average = 55.0034
	minimum = 0
	maximum = 378
Injected packet rate average = 0.0158785
	minimum = 0.01 (at node 129)
	maximum = 0.023 (at node 174)
Accepted packet rate average = 0.0145712
	minimum = 0.00933333 (at node 36)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.285835
	minimum = 0.18 (at node 129)
	maximum = 0.416 (at node 174)
Accepted flit rate average= 0.263524
	minimum = 0.175 (at node 36)
	maximum = 0.360667 (at node 128)
Injected packet length average = 18.0014
Accepted packet length average = 18.0853
Total in-flight flits = 29790 (29790 measured)
latency change    = 0.118413
throughput change = 0.00461163
Draining remaining packets ...
Class 0:
Remaining flits: 277110 277111 277112 277113 277114 277115 277116 277117 277118 277119 [...] (219 flits)
Measured flits: 277110 277111 277112 277113 277114 277115 277116 277117 277118 277119 [...] (219 flits)
Time taken is 7200 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 509.181 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2056 (1 samples)
Network latency average = 504.016 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2056 (1 samples)
Flit latency average = 458.748 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2039 (1 samples)
Fragmentation average = 56.7842 (1 samples)
	minimum = 0 (1 samples)
	maximum = 378 (1 samples)
Injected packet rate average = 0.0158785 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.023 (1 samples)
Accepted packet rate average = 0.0145712 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.285835 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.416 (1 samples)
Accepted flit rate average = 0.263524 (1 samples)
	minimum = 0.175 (1 samples)
	maximum = 0.360667 (1 samples)
Injected packet size average = 18.0014 (1 samples)
Accepted packet size average = 18.0853 (1 samples)
Hops average = 5.07249 (1 samples)
Total run time 6.5474
