BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 320.97
	minimum = 22
	maximum = 963
Network latency average = 220.839
	minimum = 22
	maximum = 817
Slowest packet = 8
Flit latency average = 191.36
	minimum = 5
	maximum = 800
Slowest flit = 11141
Fragmentation average = 40.4912
	minimum = 0
	maximum = 551
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0133906
	minimum = 0.006 (at node 64)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.250292
	minimum = 0.122 (at node 64)
	maximum = 0.399 (at node 152)
Injected packet length average = 17.8353
Accepted packet length average = 18.6916
Total in-flight flits = 35644 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 529.112
	minimum = 22
	maximum = 1890
Network latency average = 400.959
	minimum = 22
	maximum = 1399
Slowest packet = 8
Flit latency average = 366.318
	minimum = 5
	maximum = 1547
Slowest flit = 35432
Fragmentation average = 54.9443
	minimum = 0
	maximum = 742
Injected packet rate average = 0.024125
	minimum = 0 (at node 30)
	maximum = 0.0545 (at node 91)
Accepted packet rate average = 0.0142083
	minimum = 0.007 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.432654
	minimum = 0 (at node 30)
	maximum = 0.974 (at node 91)
Accepted flit rate average= 0.261534
	minimum = 0.1285 (at node 30)
	maximum = 0.414 (at node 152)
Injected packet length average = 17.9338
Accepted packet length average = 18.4071
Total in-flight flits = 66323 (0 measured)
latency change    = 0.393379
throughput change = 0.0429856
Class 0:
Packet latency average = 1060.56
	minimum = 23
	maximum = 2794
Network latency average = 892.904
	minimum = 22
	maximum = 2296
Slowest packet = 2545
Flit latency average = 858.238
	minimum = 5
	maximum = 2434
Slowest flit = 44345
Fragmentation average = 83.4112
	minimum = 0
	maximum = 656
Injected packet rate average = 0.0251198
	minimum = 0 (at node 46)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0152135
	minimum = 0.005 (at node 146)
	maximum = 0.027 (at node 63)
Injected flit rate average = 0.451083
	minimum = 0 (at node 46)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.278281
	minimum = 0.103 (at node 146)
	maximum = 0.498 (at node 106)
Injected packet length average = 17.9573
Accepted packet length average = 18.2917
Total in-flight flits = 99707 (0 measured)
latency change    = 0.5011
throughput change = 0.0601815
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 258.615
	minimum = 25
	maximum = 1061
Network latency average = 90.4548
	minimum = 22
	maximum = 960
Slowest packet = 14113
Flit latency average = 1204.85
	minimum = 5
	maximum = 2924
Slowest flit = 89342
Fragmentation average = 8.3109
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0257135
	minimum = 0 (at node 93)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0157292
	minimum = 0.006 (at node 23)
	maximum = 0.03 (at node 59)
Injected flit rate average = 0.463339
	minimum = 0.013 (at node 96)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.283167
	minimum = 0.117 (at node 185)
	maximum = 0.531 (at node 59)
Injected packet length average = 18.0192
Accepted packet length average = 18.0026
Total in-flight flits = 134205 (80893 measured)
latency change    = 3.10091
throughput change = 0.0172528
Class 0:
Packet latency average = 782.372
	minimum = 22
	maximum = 2441
Network latency average = 616.173
	minimum = 22
	maximum = 1994
Slowest packet = 14113
Flit latency average = 1365.31
	minimum = 5
	maximum = 3830
Slowest flit = 95273
Fragmentation average = 34.1172
	minimum = 0
	maximum = 534
Injected packet rate average = 0.0262682
	minimum = 0.0045 (at node 190)
	maximum = 0.0555 (at node 53)
Accepted packet rate average = 0.0156875
	minimum = 0.009 (at node 4)
	maximum = 0.023 (at node 59)
Injected flit rate average = 0.472961
	minimum = 0.087 (at node 190)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.284497
	minimum = 0.1625 (at node 4)
	maximum = 0.413 (at node 178)
Injected packet length average = 18.0051
Accepted packet length average = 18.1353
Total in-flight flits = 172026 (152087 measured)
latency change    = 0.669448
throughput change = 0.00467747
Class 0:
Packet latency average = 1350.01
	minimum = 22
	maximum = 3450
Network latency average = 1188.22
	minimum = 22
	maximum = 2951
Slowest packet = 14113
Flit latency average = 1544.36
	minimum = 5
	maximum = 4528
Slowest flit = 116783
Fragmentation average = 58.1135
	minimum = 0
	maximum = 750
Injected packet rate average = 0.0263455
	minimum = 0.00566667 (at node 48)
	maximum = 0.0553333 (at node 53)
Accepted packet rate average = 0.0157344
	minimum = 0.01 (at node 64)
	maximum = 0.0223333 (at node 157)
Injected flit rate average = 0.474405
	minimum = 0.102 (at node 48)
	maximum = 1 (at node 123)
Accepted flit rate average= 0.284516
	minimum = 0.183333 (at node 64)
	maximum = 0.400667 (at node 157)
Injected packet length average = 18.0071
Accepted packet length average = 18.0824
Total in-flight flits = 208976 (203018 measured)
latency change    = 0.420468
throughput change = 6.40709e-05
Class 0:
Packet latency average = 1774.84
	minimum = 22
	maximum = 4931
Network latency average = 1602.47
	minimum = 22
	maximum = 3919
Slowest packet = 14113
Flit latency average = 1730.1
	minimum = 5
	maximum = 5295
Slowest flit = 139380
Fragmentation average = 75.5292
	minimum = 0
	maximum = 750
Injected packet rate average = 0.0263372
	minimum = 0.007 (at node 46)
	maximum = 0.05275 (at node 123)
Accepted packet rate average = 0.0157135
	minimum = 0.0105 (at node 64)
	maximum = 0.02025 (at node 157)
Injected flit rate average = 0.474254
	minimum = 0.126 (at node 46)
	maximum = 0.9535 (at node 123)
Accepted flit rate average= 0.284191
	minimum = 0.19825 (at node 64)
	maximum = 0.363 (at node 129)
Injected packet length average = 18.007
Accepted packet length average = 18.0858
Total in-flight flits = 245534 (244206 measured)
latency change    = 0.239364
throughput change = 0.00114085
Class 0:
Packet latency average = 2107.57
	minimum = 22
	maximum = 5642
Network latency average = 1927.02
	minimum = 22
	maximum = 4850
Slowest packet = 14113
Flit latency average = 1910.47
	minimum = 5
	maximum = 5806
Slowest flit = 181333
Fragmentation average = 93.9849
	minimum = 0
	maximum = 800
Injected packet rate average = 0.0265667
	minimum = 0.0088 (at node 46)
	maximum = 0.0518 (at node 60)
Accepted packet rate average = 0.0157281
	minimum = 0.012 (at node 23)
	maximum = 0.021 (at node 103)
Injected flit rate average = 0.478122
	minimum = 0.1584 (at node 46)
	maximum = 0.9318 (at node 60)
Accepted flit rate average= 0.284522
	minimum = 0.2188 (at node 23)
	maximum = 0.3768 (at node 103)
Injected packet length average = 17.9971
Accepted packet length average = 18.09
Total in-flight flits = 285638 (285240 measured)
latency change    = 0.157876
throughput change = 0.00116149
Class 0:
Packet latency average = 2382.39
	minimum = 22
	maximum = 6613
Network latency average = 2197.24
	minimum = 22
	maximum = 5832
Slowest packet = 14113
Flit latency average = 2095.55
	minimum = 5
	maximum = 6636
Slowest flit = 193561
Fragmentation average = 109.139
	minimum = 0
	maximum = 800
Injected packet rate average = 0.0267813
	minimum = 0.0085 (at node 46)
	maximum = 0.0453333 (at node 60)
Accepted packet rate average = 0.015737
	minimum = 0.0118333 (at node 80)
	maximum = 0.0205 (at node 103)
Injected flit rate average = 0.482087
	minimum = 0.153 (at node 46)
	maximum = 0.816 (at node 170)
Accepted flit rate average= 0.284885
	minimum = 0.218 (at node 80)
	maximum = 0.369167 (at node 103)
Injected packet length average = 18.0009
Accepted packet length average = 18.1029
Total in-flight flits = 326855 (326803 measured)
latency change    = 0.115353
throughput change = 0.0012761
Class 0:
Packet latency average = 2631.37
	minimum = 22
	maximum = 7156
Network latency average = 2442.62
	minimum = 22
	maximum = 6608
Slowest packet = 14113
Flit latency average = 2281.33
	minimum = 5
	maximum = 7462
Slowest flit = 209987
Fragmentation average = 119.035
	minimum = 0
	maximum = 800
Injected packet rate average = 0.0268036
	minimum = 0.01 (at node 46)
	maximum = 0.0461429 (at node 162)
Accepted packet rate average = 0.0157664
	minimum = 0.0115714 (at node 80)
	maximum = 0.019 (at node 90)
Injected flit rate average = 0.482461
	minimum = 0.18 (at node 46)
	maximum = 0.830857 (at node 162)
Accepted flit rate average= 0.285152
	minimum = 0.214714 (at node 80)
	maximum = 0.342 (at node 70)
Injected packet length average = 17.9999
Accepted packet length average = 18.0861
Total in-flight flits = 364895 (364895 measured)
latency change    = 0.0946194
throughput change = 0.000934131
Draining all recorded packets ...
Class 0:
Remaining flits: 286539 286540 286541 288162 288163 288164 288165 288166 288167 288168 [...] (406339 flits)
Measured flits: 286539 286540 286541 288162 288163 288164 288165 288166 288167 288168 [...] (337400 flits)
Class 0:
Remaining flits: 305637 305638 305639 313628 313629 313630 313631 315206 315207 315208 [...] (452001 flits)
Measured flits: 305637 305638 305639 313628 313629 313630 313631 315206 315207 315208 [...] (290425 flits)
Class 0:
Remaining flits: 347358 347359 347360 347361 347362 347363 358863 358864 358865 360684 [...] (487805 flits)
Measured flits: 347358 347359 347360 347361 347362 347363 358863 358864 358865 360684 [...] (243033 flits)
Class 0:
Remaining flits: 375640 375641 387905 387906 387907 387908 387909 387910 387911 387912 [...] (524146 flits)
Measured flits: 375640 375641 387905 387906 387907 387908 387909 387910 387911 387912 [...] (196170 flits)
Class 0:
Remaining flits: 392838 392839 392840 392841 392842 392843 392844 392845 392846 392847 [...] (560293 flits)
Measured flits: 392838 392839 392840 392841 392842 392843 392844 392845 392846 392847 [...] (148937 flits)
Class 0:
Remaining flits: 449842 449843 449844 449845 449846 449847 449848 449849 449850 449851 [...] (594910 flits)
Measured flits: 449842 449843 449844 449845 449846 449847 449848 449849 449850 449851 [...] (103922 flits)
Class 0:
Remaining flits: 462255 462256 462257 481828 481829 481830 481831 481832 481833 481834 [...] (626516 flits)
Measured flits: 462255 462256 462257 481828 481829 481830 481831 481832 481833 481834 [...] (66283 flits)
Class 0:
Remaining flits: 488423 488424 488425 488426 488427 488428 488429 503745 503746 503747 [...] (667567 flits)
Measured flits: 488423 488424 488425 488426 488427 488428 488429 503745 503746 503747 [...] (39764 flits)
Class 0:
Remaining flits: 528309 528310 528311 528312 528313 528314 528315 528316 528317 529891 [...] (703986 flits)
Measured flits: 528309 528310 528311 528312 528313 528314 528315 528316 528317 529891 [...] (22392 flits)
Class 0:
Remaining flits: 536400 536401 536402 536403 536404 536405 536406 536407 536408 536409 [...] (735370 flits)
Measured flits: 536400 536401 536402 536403 536404 536405 536406 536407 536408 536409 [...] (12516 flits)
Class 0:
Remaining flits: 546296 546297 546298 546299 647136 647137 647138 647139 647140 647141 [...] (768335 flits)
Measured flits: 546296 546297 546298 546299 647136 647137 647138 647139 647140 647141 [...] (7520 flits)
Class 0:
Remaining flits: 667879 667880 667881 667882 667883 667884 667885 667886 667887 667888 [...] (804608 flits)
Measured flits: 667879 667880 667881 667882 667883 667884 667885 667886 667887 667888 [...] (4569 flits)
Class 0:
Remaining flits: 772056 772057 772058 772059 772060 772061 772062 772063 772064 772065 [...] (839235 flits)
Measured flits: 772056 772057 772058 772059 772060 772061 772062 772063 772064 772065 [...] (2517 flits)
Class 0:
Remaining flits: 799681 799682 799683 799684 799685 810954 810955 810956 810957 810958 [...] (880553 flits)
Measured flits: 799681 799682 799683 799684 799685 810954 810955 810956 810957 810958 [...] (1224 flits)
Class 0:
Remaining flits: 830244 830245 830246 830247 830248 830249 842778 842779 842780 842781 [...] (923554 flits)
Measured flits: 830244 830245 830246 830247 830248 830249 842778 842779 842780 842781 [...] (626 flits)
Class 0:
Remaining flits: 852174 852175 852176 852177 852178 852179 852180 852181 852182 852183 [...] (961801 flits)
Measured flits: 852174 852175 852176 852177 852178 852179 852180 852181 852182 852183 [...] (398 flits)
Class 0:
Remaining flits: 865709 875430 875431 875432 875433 875434 875435 875436 875437 875438 [...] (993057 flits)
Measured flits: 865709 875430 875431 875432 875433 875434 875435 875436 875437 875438 [...] (212 flits)
Class 0:
Remaining flits: 875446 875447 888336 888337 888338 888339 888340 888341 888342 888343 [...] (1029593 flits)
Measured flits: 875446 875447 888336 888337 888338 888339 888340 888341 888342 888343 [...] (96 flits)
Class 0:
Remaining flits: 912372 912373 912374 912375 912376 912377 912378 912379 912380 912381 [...] (1064220 flits)
Measured flits: 916650 916651 916652 916653 916654 916655 916656 916657 916658 916659 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 978837 978838 978839 993816 993817 993818 993819 993820 993821 993822 [...] (1037832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1013082 1013083 1013084 1013085 1013086 1013087 1013088 1013089 1013090 1013091 [...] (990191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1037185 1037186 1037187 1037188 1037189 1037190 1037191 1037192 1037193 1037194 [...] (942615 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1210716 1210717 1210718 1210719 1210720 1210721 1210722 1210723 1210724 1210725 [...] (894962 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1214059 1214060 1214061 1214062 1214063 1228428 1228429 1228430 1228431 1228432 [...] (847351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1248313 1248314 1248315 1248316 1248317 1253995 1253996 1253997 1253998 1253999 [...] (799643 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1261772 1261773 1261774 1261775 1261776 1261777 1261778 1261779 1261780 1261781 [...] (752330 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1273075 1273076 1273077 1273078 1273079 1273080 1273081 1273082 1273083 1273084 [...] (704567 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1287108 1287109 1287110 1287111 1287112 1287113 1287114 1287115 1287116 1287117 [...] (657040 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310806 1310807 1310808 1310809 1310810 1310811 1310812 1310813 1322208 1322209 [...] (609322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1362238 1362239 1382443 1382444 1382445 1382446 1382447 1382448 1382449 1382450 [...] (561550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1383922 1383923 1383924 1383925 1383926 1383927 1383928 1383929 1387396 1387397 [...] (513782 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1409825 1409826 1409827 1409828 1409829 1409830 1409831 1441279 1441280 1441281 [...] (466322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1506834 1506835 1506836 1506837 1506838 1506839 1506840 1506841 1506842 1506843 [...] (418668 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1506834 1506835 1506836 1506837 1506838 1506839 1506840 1506841 1506842 1506843 [...] (370830 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1573984 1573985 1573986 1573987 1573988 1573989 1573990 1573991 1597104 1597105 [...] (323900 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1597104 1597105 1597106 1597107 1597108 1597109 1597110 1597111 1597112 1597113 [...] (276922 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1597119 1597120 1597121 1642050 1642051 1642052 1642053 1642054 1642055 1642056 [...] (230076 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1642058 1642059 1642060 1642061 1642062 1642063 1642064 1642065 1642066 1642067 [...] (184925 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1764884 1764885 1764886 1764887 1764888 1764889 1764890 1764891 1764892 1764893 [...] (143075 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1777410 1777411 1777412 1777413 1777414 1777415 1777416 1777417 1777418 1777419 [...] (105305 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1780538 1780539 1780540 1780541 1796634 1796635 1796636 1796637 1796638 1796639 [...] (77532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1859030 1859031 1859032 1859033 1859034 1859035 1859036 1859037 1859038 1859039 [...] (56577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1958550 1958551 1958552 1958553 1958554 1958555 1958556 1958557 1958558 1958559 [...] (39897 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2043403 2043404 2043405 2043406 2043407 2043408 2043409 2043410 2043411 2043412 [...] (27102 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2067180 2067181 2067182 2067183 2067184 2067185 2067186 2067187 2067188 2067189 [...] (19123 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084791 2084792 2084793 2084794 2084795 2105280 2105281 2105282 2105283 2105284 [...] (13617 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122383 2122384 2122385 2122386 2122387 2122388 2122389 2122390 2122391 2122392 [...] (9218 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2192505 2192506 2192507 2208240 2208241 2208242 2208243 2208244 2208245 2208246 [...] (5598 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2318570 2318571 2318572 2318573 2318574 2318575 2318576 2318577 2318578 2318579 [...] (2602 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2455848 2455849 2455850 2455851 2455852 2455853 2455854 2455855 2455856 2455857 [...] (800 flits)
Measured flits: (0 flits)
Time taken is 61470 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4993.98 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19729 (1 samples)
Network latency average = 4782.61 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19503 (1 samples)
Flit latency average = 11223.7 (1 samples)
	minimum = 5 (1 samples)
	maximum = 34951 (1 samples)
Fragmentation average = 181.332 (1 samples)
	minimum = 0 (1 samples)
	maximum = 925 (1 samples)
Injected packet rate average = 0.0268036 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0461429 (1 samples)
Accepted packet rate average = 0.0157664 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.482461 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.830857 (1 samples)
Accepted flit rate average = 0.285152 (1 samples)
	minimum = 0.214714 (1 samples)
	maximum = 0.342 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 18.0861 (1 samples)
Hops average = 5.06903 (1 samples)
Total run time 61.1699
