BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.6
	minimum = 22
	maximum = 864
Network latency average = 289.344
	minimum = 22
	maximum = 793
Slowest packet = 463
Flit latency average = 252.15
	minimum = 5
	maximum = 807
Slowest flit = 18358
Fragmentation average = 80.7835
	minimum = 0
	maximum = 524
Injected packet rate average = 0.0333177
	minimum = 0.021 (at node 76)
	maximum = 0.049 (at node 167)
Accepted packet rate average = 0.014
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.594911
	minimum = 0.376 (at node 76)
	maximum = 0.882 (at node 167)
Accepted flit rate average= 0.272693
	minimum = 0.072 (at node 174)
	maximum = 0.438 (at node 88)
Injected packet length average = 17.8557
Accepted packet length average = 19.4781
Total in-flight flits = 62987 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 570.58
	minimum = 22
	maximum = 1627
Network latency average = 545.787
	minimum = 22
	maximum = 1617
Slowest packet = 2392
Flit latency average = 498.11
	minimum = 5
	maximum = 1600
Slowest flit = 43073
Fragmentation average = 101.914
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0293802
	minimum = 0.0165 (at node 76)
	maximum = 0.0395 (at node 167)
Accepted packet rate average = 0.0148438
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 56)
Injected flit rate average = 0.525568
	minimum = 0.2925 (at node 76)
	maximum = 0.704 (at node 167)
Accepted flit rate average= 0.277781
	minimum = 0.1575 (at node 153)
	maximum = 0.4035 (at node 152)
Injected packet length average = 17.8885
Accepted packet length average = 18.7137
Total in-flight flits = 96768 (0 measured)
latency change    = 0.469663
throughput change = 0.0183185
Class 0:
Packet latency average = 1423.85
	minimum = 27
	maximum = 2310
Network latency average = 1311.06
	minimum = 22
	maximum = 2308
Slowest packet = 4169
Flit latency average = 1262.22
	minimum = 5
	maximum = 2398
Slowest flit = 70305
Fragmentation average = 128.166
	minimum = 0
	maximum = 724
Injected packet rate average = 0.0159948
	minimum = 0 (at node 55)
	maximum = 0.031 (at node 74)
Accepted packet rate average = 0.0150052
	minimum = 0.005 (at node 61)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.288073
	minimum = 0 (at node 55)
	maximum = 0.565 (at node 74)
Accepted flit rate average= 0.268844
	minimum = 0.099 (at node 132)
	maximum = 0.453 (at node 159)
Injected packet length average = 18.0104
Accepted packet length average = 17.9167
Total in-flight flits = 100770 (0 measured)
latency change    = 0.59927
throughput change = 0.0332442
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1169.53
	minimum = 105
	maximum = 2283
Network latency average = 77.2939
	minimum = 22
	maximum = 942
Slowest packet = 14404
Flit latency average = 1640.57
	minimum = 5
	maximum = 3045
Slowest flit = 84972
Fragmentation average = 8.39474
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0155417
	minimum = 0.001 (at node 66)
	maximum = 0.031 (at node 55)
Accepted packet rate average = 0.0149271
	minimum = 0.006 (at node 71)
	maximum = 0.027 (at node 78)
Injected flit rate average = 0.28026
	minimum = 0.022 (at node 66)
	maximum = 0.563 (at node 142)
Accepted flit rate average= 0.266953
	minimum = 0.112 (at node 4)
	maximum = 0.492 (at node 78)
Injected packet length average = 18.0328
Accepted packet length average = 17.8838
Total in-flight flits = 103047 (49333 measured)
latency change    = 0.217457
throughput change = 0.00708224
Class 0:
Packet latency average = 2063.63
	minimum = 105
	maximum = 3681
Network latency average = 684.418
	minimum = 22
	maximum = 1970
Slowest packet = 14404
Flit latency average = 1739.74
	minimum = 5
	maximum = 3786
Slowest flit = 96389
Fragmentation average = 50.2306
	minimum = 0
	maximum = 576
Injected packet rate average = 0.0150885
	minimum = 0.0025 (at node 131)
	maximum = 0.025 (at node 142)
Accepted packet rate average = 0.0148906
	minimum = 0.009 (at node 79)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.271773
	minimum = 0.0425 (at node 131)
	maximum = 0.452 (at node 142)
Accepted flit rate average= 0.267961
	minimum = 0.1715 (at node 122)
	maximum = 0.3935 (at node 128)
Injected packet length average = 18.0119
Accepted packet length average = 17.9953
Total in-flight flits = 102219 (86017 measured)
latency change    = 0.433266
throughput change = 0.00376104
Class 0:
Packet latency average = 2770.8
	minimum = 105
	maximum = 4385
Network latency average = 1348.68
	minimum = 22
	maximum = 2973
Slowest packet = 14404
Flit latency average = 1806.4
	minimum = 5
	maximum = 4429
Slowest flit = 106145
Fragmentation average = 85.6152
	minimum = 0
	maximum = 672
Injected packet rate average = 0.0153576
	minimum = 0.00833333 (at node 20)
	maximum = 0.0226667 (at node 46)
Accepted packet rate average = 0.0149774
	minimum = 0.0103333 (at node 64)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.276656
	minimum = 0.153 (at node 20)
	maximum = 0.412 (at node 46)
Accepted flit rate average= 0.268793
	minimum = 0.18 (at node 64)
	maximum = 0.368 (at node 128)
Injected packet length average = 18.0142
Accepted packet length average = 17.9466
Total in-flight flits = 105281 (103627 measured)
latency change    = 0.255223
throughput change = 0.00309705
Class 0:
Packet latency average = 3227.22
	minimum = 105
	maximum = 5299
Network latency average = 1652.22
	minimum = 22
	maximum = 3917
Slowest packet = 14404
Flit latency average = 1839.73
	minimum = 5
	maximum = 5017
Slowest flit = 190619
Fragmentation average = 95.6845
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0152292
	minimum = 0.00775 (at node 108)
	maximum = 0.02125 (at node 127)
Accepted packet rate average = 0.014974
	minimum = 0.01 (at node 64)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.27413
	minimum = 0.139 (at node 108)
	maximum = 0.38525 (at node 142)
Accepted flit rate average= 0.268887
	minimum = 0.1805 (at node 64)
	maximum = 0.35675 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 17.957
Total in-flight flits = 104739 (104560 measured)
latency change    = 0.141426
throughput change = 0.000347046
Class 0:
Packet latency average = 3591.77
	minimum = 105
	maximum = 5889
Network latency average = 1784.03
	minimum = 22
	maximum = 4570
Slowest packet = 14404
Flit latency average = 1865.61
	minimum = 5
	maximum = 5816
Slowest flit = 214311
Fragmentation average = 98.8317
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151937
	minimum = 0.0074 (at node 108)
	maximum = 0.0202 (at node 127)
Accepted packet rate average = 0.0149927
	minimum = 0.0112 (at node 80)
	maximum = 0.0194 (at node 181)
Injected flit rate average = 0.273656
	minimum = 0.1342 (at node 108)
	maximum = 0.3656 (at node 142)
Accepted flit rate average= 0.269366
	minimum = 0.1966 (at node 80)
	maximum = 0.3432 (at node 181)
Injected packet length average = 18.0111
Accepted packet length average = 17.9664
Total in-flight flits = 104889 (104867 measured)
latency change    = 0.101498
throughput change = 0.0017779
Class 0:
Packet latency average = 3903.17
	minimum = 105
	maximum = 6854
Network latency average = 1857.79
	minimum = 22
	maximum = 5425
Slowest packet = 14404
Flit latency average = 1886.43
	minimum = 5
	maximum = 5946
Slowest flit = 214325
Fragmentation average = 100.855
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151745
	minimum = 0.0095 (at node 96)
	maximum = 0.0211667 (at node 127)
Accepted packet rate average = 0.0149462
	minimum = 0.0113333 (at node 80)
	maximum = 0.019 (at node 181)
Injected flit rate average = 0.27327
	minimum = 0.169167 (at node 96)
	maximum = 0.378833 (at node 127)
Accepted flit rate average= 0.269082
	minimum = 0.202167 (at node 80)
	maximum = 0.342167 (at node 138)
Injected packet length average = 18.0085
Accepted packet length average = 18.0034
Total in-flight flits = 105590 (105590 measured)
latency change    = 0.0797812
throughput change = 0.00105555
Class 0:
Packet latency average = 4217.85
	minimum = 105
	maximum = 7550
Network latency average = 1902.47
	minimum = 22
	maximum = 5425
Slowest packet = 14404
Flit latency average = 1901.13
	minimum = 5
	maximum = 5946
Slowest flit = 214325
Fragmentation average = 102.385
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151875
	minimum = 0.00942857 (at node 108)
	maximum = 0.0198571 (at node 46)
Accepted packet rate average = 0.0149472
	minimum = 0.0117143 (at node 80)
	maximum = 0.0188571 (at node 128)
Injected flit rate average = 0.273388
	minimum = 0.170429 (at node 108)
	maximum = 0.357 (at node 127)
Accepted flit rate average= 0.26874
	minimum = 0.207571 (at node 80)
	maximum = 0.336429 (at node 181)
Injected packet length average = 18.0008
Accepted packet length average = 17.9793
Total in-flight flits = 106999 (106999 measured)
latency change    = 0.0746067
throughput change = 0.00126989
Draining all recorded packets ...
Class 0:
Remaining flits: 452043 452044 452045 452046 452047 452048 452049 452050 452051 461633 [...] (105065 flits)
Measured flits: 452043 452044 452045 452046 452047 452048 452049 452050 452051 461633 [...] (105065 flits)
Class 0:
Remaining flits: 466158 466159 466160 466161 466162 466163 467522 467523 467524 467525 [...] (107230 flits)
Measured flits: 466158 466159 466160 466161 466162 466163 467522 467523 467524 467525 [...] (107230 flits)
Class 0:
Remaining flits: 515952 515953 515954 515955 515956 515957 515958 515959 515960 515961 [...] (107335 flits)
Measured flits: 515952 515953 515954 515955 515956 515957 515958 515959 515960 515961 [...] (107335 flits)
Class 0:
Remaining flits: 584730 584731 584732 584733 584734 584735 584736 584737 584738 584739 [...] (108347 flits)
Measured flits: 584730 584731 584732 584733 584734 584735 584736 584737 584738 584739 [...] (108347 flits)
Class 0:
Remaining flits: 638874 638875 638876 638877 638878 638879 638880 638881 638882 638883 [...] (106946 flits)
Measured flits: 638874 638875 638876 638877 638878 638879 638880 638881 638882 638883 [...] (106946 flits)
Class 0:
Remaining flits: 679839 679840 679841 690660 690661 690662 690663 690664 690665 690666 [...] (106581 flits)
Measured flits: 679839 679840 679841 690660 690661 690662 690663 690664 690665 690666 [...] (106581 flits)
Class 0:
Remaining flits: 727812 727813 727814 727815 727816 727817 727818 727819 727820 727821 [...] (104952 flits)
Measured flits: 727812 727813 727814 727815 727816 727817 727818 727819 727820 727821 [...] (104952 flits)
Class 0:
Remaining flits: 734541 734542 734543 791260 791261 798963 798964 798965 802725 802726 [...] (106995 flits)
Measured flits: 734541 734542 734543 791260 791261 798963 798964 798965 802725 802726 [...] (106325 flits)
Class 0:
Remaining flits: 825378 825379 825380 825381 825382 825383 825384 825385 825386 825387 [...] (107208 flits)
Measured flits: 825378 825379 825380 825381 825382 825383 825384 825385 825386 825387 [...] (102495 flits)
Class 0:
Remaining flits: 867384 867385 867386 867387 867388 867389 867390 867391 867392 867393 [...] (104891 flits)
Measured flits: 867384 867385 867386 867387 867388 867389 867390 867391 867392 867393 [...] (89483 flits)
Class 0:
Remaining flits: 894300 894301 894302 894303 894304 894305 894306 894307 894308 894309 [...] (104201 flits)
Measured flits: 894300 894301 894302 894303 894304 894305 894306 894307 894308 894309 [...] (70234 flits)
Class 0:
Remaining flits: 918234 918235 918236 918237 918238 918239 918240 918241 918242 918243 [...] (104594 flits)
Measured flits: 918234 918235 918236 918237 918238 918239 918240 918241 918242 918243 [...] (47686 flits)
Class 0:
Remaining flits: 980226 980227 980228 980229 980230 980231 980232 980233 980234 980235 [...] (104913 flits)
Measured flits: 980226 980227 980228 980229 980230 980231 980232 980233 980234 980235 [...] (28817 flits)
Class 0:
Remaining flits: 1036998 1036999 1037000 1037001 1037002 1037003 1037004 1037005 1037006 1037007 [...] (104812 flits)
Measured flits: 1036998 1036999 1037000 1037001 1037002 1037003 1037004 1037005 1037006 1037007 [...] (19839 flits)
Class 0:
Remaining flits: 1037009 1037010 1037011 1037012 1037013 1037014 1037015 1069128 1069129 1069130 [...] (102974 flits)
Measured flits: 1037009 1037010 1037011 1037012 1037013 1037014 1037015 1069128 1069129 1069130 [...] (14737 flits)
Class 0:
Remaining flits: 1069128 1069129 1069130 1069131 1069132 1069133 1069134 1069135 1069136 1069137 [...] (102732 flits)
Measured flits: 1069128 1069129 1069130 1069131 1069132 1069133 1069134 1069135 1069136 1069137 [...] (13364 flits)
Class 0:
Remaining flits: 1185696 1185697 1185698 1185699 1185700 1185701 1185702 1185703 1185704 1185705 [...] (103564 flits)
Measured flits: 1185696 1185697 1185698 1185699 1185700 1185701 1185702 1185703 1185704 1185705 [...] (10979 flits)
Class 0:
Remaining flits: 1233234 1233235 1233236 1233237 1233238 1233239 1233240 1233241 1233242 1233243 [...] (104209 flits)
Measured flits: 1340586 1340587 1340588 1340589 1340590 1340591 1340592 1340593 1340594 1340595 [...] (9243 flits)
Class 0:
Remaining flits: 1233234 1233235 1233236 1233237 1233238 1233239 1233240 1233241 1233242 1233243 [...] (104046 flits)
Measured flits: 1399231 1399232 1399233 1399234 1399235 1399236 1399237 1399238 1399239 1399240 [...] (8056 flits)
Class 0:
Remaining flits: 1233234 1233235 1233236 1233237 1233238 1233239 1233240 1233241 1233242 1233243 [...] (103631 flits)
Measured flits: 1435608 1435609 1435610 1435611 1435612 1435613 1435614 1435615 1435616 1435617 [...] (5799 flits)
Class 0:
Remaining flits: 1311300 1311301 1311302 1311303 1311304 1311305 1311306 1311307 1311308 1311309 [...] (104595 flits)
Measured flits: 1436666 1436667 1436668 1436669 1459925 1477908 1477909 1477910 1477911 1477912 [...] (3612 flits)
Class 0:
Remaining flits: 1349064 1349065 1349066 1349067 1349068 1349069 1349070 1349071 1349072 1349073 [...] (103170 flits)
Measured flits: 1528830 1528831 1528832 1528833 1528834 1528835 1528836 1528837 1528838 1528839 [...] (2243 flits)
Class 0:
Remaining flits: 1500231 1500232 1500233 1500234 1500235 1500236 1500237 1500238 1500239 1500240 [...] (103209 flits)
Measured flits: 1528835 1528836 1528837 1528838 1528839 1528840 1528841 1528842 1528843 1528844 [...] (1253 flits)
Class 0:
Remaining flits: 1507356 1507357 1507358 1507359 1507360 1507361 1507362 1507363 1507364 1507365 [...] (102746 flits)
Measured flits: 1666926 1666927 1666928 1666929 1666930 1666931 1666932 1666933 1666934 1666935 [...] (621 flits)
Class 0:
Remaining flits: 1544073 1544074 1544075 1597266 1597267 1597268 1597269 1597270 1597271 1597272 [...] (105068 flits)
Measured flits: 1667322 1667323 1667324 1667325 1667326 1667327 1667328 1667329 1667330 1667331 [...] (199 flits)
Class 0:
Remaining flits: 1620036 1620037 1620038 1620039 1620040 1620041 1620042 1620043 1620044 1620045 [...] (104414 flits)
Measured flits: 1683738 1683739 1683740 1683741 1683742 1683743 1683744 1683745 1683746 1683747 [...] (22 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1651878 1651879 1651880 1651881 1651882 1651883 1651884 1651885 1651886 1651887 [...] (55770 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1747377 1747378 1747379 1747380 1747381 1747382 1747383 1747384 1747385 1783710 [...] (12487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1854666 1854667 1854668 1854669 1854670 1854671 1854672 1854673 1854674 1854675 [...] (506 flits)
Measured flits: (0 flits)
Time taken is 39432 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8725.23 (1 samples)
	minimum = 105 (1 samples)
	maximum = 26504 (1 samples)
Network latency average = 2057.62 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7682 (1 samples)
Flit latency average = 1993.03 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9055 (1 samples)
Fragmentation average = 122.965 (1 samples)
	minimum = 0 (1 samples)
	maximum = 726 (1 samples)
Injected packet rate average = 0.0151875 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.0149472 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.273388 (1 samples)
	minimum = 0.170429 (1 samples)
	maximum = 0.357 (1 samples)
Accepted flit rate average = 0.26874 (1 samples)
	minimum = 0.207571 (1 samples)
	maximum = 0.336429 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 17.9793 (1 samples)
Hops average = 5.05642 (1 samples)
Total run time 79.9842
