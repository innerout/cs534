BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.002487
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 46.5759
	minimum = 22
	maximum = 120
Network latency average = 40.4093
	minimum = 22
	maximum = 108
Slowest packet = 24
Flit latency average = 21.0952
	minimum = 5
	maximum = 91
Slowest flit = 863
Fragmentation average = 4.29325
	minimum = 0
	maximum = 51
Injected packet rate average = 0.00258333
	minimum = 0 (at node 5)
	maximum = 0.015 (at node 173)
Accepted packet rate average = 0.00246875
	minimum = 0 (at node 11)
	maximum = 0.008 (at node 140)
Injected flit rate average = 0.046026
	minimum = 0 (at node 5)
	maximum = 0.27 (at node 173)
Accepted flit rate average= 0.044875
	minimum = 0 (at node 11)
	maximum = 0.144 (at node 140)
Injected packet length average = 17.8165
Accepted packet length average = 18.1772
Total in-flight flits = 312 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 44.9732
	minimum = 22
	maximum = 127
Network latency average = 39.1935
	minimum = 22
	maximum = 108
Slowest packet = 24
Flit latency average = 20.2644
	minimum = 5
	maximum = 91
Slowest flit = 863
Fragmentation average = 3.52125
	minimum = 0
	maximum = 51
Injected packet rate average = 0.00238021
	minimum = 0 (at node 18)
	maximum = 0.008 (at node 0)
Accepted packet rate average = 0.00232812
	minimum = 0.0005 (at node 14)
	maximum = 0.0055 (at node 140)
Injected flit rate average = 0.0426458
	minimum = 0 (at node 18)
	maximum = 0.144 (at node 0)
Accepted flit rate average= 0.0422109
	minimum = 0.009 (at node 35)
	maximum = 0.099 (at node 140)
Injected packet length average = 17.9168
Accepted packet length average = 18.1309
Total in-flight flits = 243 (0 measured)
latency change    = 0.0356389
throughput change = 0.0631131
Class 0:
Packet latency average = 46.4529
	minimum = 22
	maximum = 127
Network latency average = 39.7535
	minimum = 22
	maximum = 97
Slowest packet = 916
Flit latency average = 20.9887
	minimum = 5
	maximum = 80
Slowest flit = 18014
Fragmentation average = 3.67535
	minimum = 0
	maximum = 36
Injected packet rate average = 0.00260417
	minimum = 0 (at node 4)
	maximum = 0.012 (at node 1)
Accepted packet rate average = 0.00259896
	minimum = 0 (at node 10)
	maximum = 0.007 (at node 22)
Injected flit rate average = 0.047099
	minimum = 0 (at node 4)
	maximum = 0.216 (at node 1)
Accepted flit rate average= 0.0468854
	minimum = 0 (at node 10)
	maximum = 0.126 (at node 22)
Injected packet length average = 18.086
Accepted packet length average = 18.0401
Total in-flight flits = 241 (0 measured)
latency change    = 0.0318549
throughput change = 0.0997001
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 48.8712
	minimum = 25
	maximum = 187
Network latency average = 40.809
	minimum = 22
	maximum = 133
Slowest packet = 1414
Flit latency average = 21.9524
	minimum = 5
	maximum = 116
Slowest flit = 27520
Fragmentation average = 4.00858
	minimum = 0
	maximum = 38
Injected packet rate average = 0.00250521
	minimum = 0 (at node 2)
	maximum = 0.009 (at node 5)
Accepted packet rate average = 0.00253646
	minimum = 0 (at node 32)
	maximum = 0.007 (at node 45)
Injected flit rate average = 0.0447135
	minimum = 0 (at node 2)
	maximum = 0.162 (at node 5)
Accepted flit rate average= 0.0451094
	minimum = 0 (at node 32)
	maximum = 0.126 (at node 182)
Injected packet length average = 17.8482
Accepted packet length average = 17.7844
Total in-flight flits = 238 (238 measured)
latency change    = 0.0494839
throughput change = 0.0393719
Class 0:
Packet latency average = 49.0797
	minimum = 22
	maximum = 187
Network latency average = 40.4842
	minimum = 22
	maximum = 133
Slowest packet = 1414
Flit latency average = 21.5252
	minimum = 5
	maximum = 116
Slowest flit = 27520
Fragmentation average = 3.98672
	minimum = 0
	maximum = 38
Injected packet rate average = 0.00259635
	minimum = 0 (at node 86)
	maximum = 0.008 (at node 132)
Accepted packet rate average = 0.00260417
	minimum = 0 (at node 116)
	maximum = 0.006 (at node 65)
Injected flit rate average = 0.0466354
	minimum = 0 (at node 86)
	maximum = 0.144 (at node 132)
Accepted flit rate average= 0.0467161
	minimum = 0 (at node 116)
	maximum = 0.108 (at node 65)
Injected packet length average = 17.9619
Accepted packet length average = 17.939
Total in-flight flits = 248 (248 measured)
latency change    = 0.00424674
throughput change = 0.0343943
Class 0:
Packet latency average = 49.5425
	minimum = 22
	maximum = 203
Network latency average = 41.0553
	minimum = 22
	maximum = 147
Slowest packet = 1414
Flit latency average = 21.9834
	minimum = 5
	maximum = 130
Slowest flit = 44964
Fragmentation average = 3.96406
	minimum = 0
	maximum = 38
Injected packet rate average = 0.00252604
	minimum = 0.000333333 (at node 24)
	maximum = 0.006 (at node 44)
Accepted packet rate average = 0.00254861
	minimum = 0.000333333 (at node 116)
	maximum = 0.00566667 (at node 173)
Injected flit rate average = 0.0454601
	minimum = 0.006 (at node 24)
	maximum = 0.108 (at node 44)
Accepted flit rate average= 0.0457118
	minimum = 0.006 (at node 116)
	maximum = 0.102 (at node 173)
Injected packet length average = 17.9966
Accepted packet length average = 17.936
Total in-flight flits = 101 (101 measured)
latency change    = 0.00934205
throughput change = 0.0219711
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6116 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 49.5281 (1 samples)
	minimum = 22 (1 samples)
	maximum = 203 (1 samples)
Network latency average = 41 (1 samples)
	minimum = 22 (1 samples)
	maximum = 147 (1 samples)
Flit latency average = 21.861 (1 samples)
	minimum = 5 (1 samples)
	maximum = 130 (1 samples)
Fragmentation average = 3.94376 (1 samples)
	minimum = 0 (1 samples)
	maximum = 38 (1 samples)
Injected packet rate average = 0.00252604 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.006 (1 samples)
Accepted packet rate average = 0.00254861 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Injected flit rate average = 0.0454601 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.108 (1 samples)
Accepted flit rate average = 0.0457118 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.102 (1 samples)
Injected packet size average = 17.9966 (1 samples)
Accepted packet size average = 17.936 (1 samples)
Hops average = 5.05967 (1 samples)
Total run time 1.59377
