BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 317.699
	minimum = 23
	maximum = 961
Network latency average = 306.265
	minimum = 23
	maximum = 961
Slowest packet = 167
Flit latency average = 233.577
	minimum = 6
	maximum = 967
Slowest flit = 2162
Fragmentation average = 181.945
	minimum = 0
	maximum = 872
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.0106354
	minimum = 0.003 (at node 115)
	maximum = 0.019 (at node 76)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.227432
	minimum = 0.075 (at node 93)
	maximum = 0.377 (at node 152)
Injected packet length average = 17.8324
Accepted packet length average = 21.3844
Total in-flight flits = 78103 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.028
	minimum = 23
	maximum = 1929
Network latency average = 553.578
	minimum = 23
	maximum = 1929
Slowest packet = 321
Flit latency average = 461.555
	minimum = 6
	maximum = 1937
Slowest flit = 5373
Fragmentation average = 247.824
	minimum = 0
	maximum = 1494
Injected packet rate average = 0.0354115
	minimum = 0.0255 (at node 2)
	maximum = 0.0445 (at node 44)
Accepted packet rate average = 0.0121068
	minimum = 0.007 (at node 93)
	maximum = 0.02 (at node 136)
Injected flit rate average = 0.635091
	minimum = 0.4535 (at node 2)
	maximum = 0.7985 (at node 44)
Accepted flit rate average= 0.24124
	minimum = 0.1405 (at node 153)
	maximum = 0.383 (at node 136)
Injected packet length average = 17.9346
Accepted packet length average = 19.926
Total in-flight flits = 152128 (0 measured)
latency change    = 0.439712
throughput change = 0.0572348
Class 0:
Packet latency average = 1229.22
	minimum = 26
	maximum = 2874
Network latency average = 1211.11
	minimum = 24
	maximum = 2857
Slowest packet = 569
Flit latency average = 1128.99
	minimum = 6
	maximum = 2926
Slowest flit = 6149
Fragmentation average = 345.068
	minimum = 1
	maximum = 2123
Injected packet rate average = 0.0322344
	minimum = 0.011 (at node 44)
	maximum = 0.049 (at node 58)
Accepted packet rate average = 0.0136979
	minimum = 0.005 (at node 161)
	maximum = 0.026 (at node 172)
Injected flit rate average = 0.579656
	minimum = 0.196 (at node 148)
	maximum = 0.872 (at node 58)
Accepted flit rate average= 0.253745
	minimum = 0.101 (at node 161)
	maximum = 0.473 (at node 172)
Injected packet length average = 17.9825
Accepted packet length average = 18.5243
Total in-flight flits = 214811 (0 measured)
latency change    = 0.538709
throughput change = 0.0492826
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 187.456
	minimum = 25
	maximum = 1456
Network latency average = 88.4769
	minimum = 25
	maximum = 917
Slowest packet = 19863
Flit latency average = 1697.23
	minimum = 6
	maximum = 3863
Slowest flit = 13630
Fragmentation average = 24.7407
	minimum = 1
	maximum = 523
Injected packet rate average = 0.0306458
	minimum = 0.005 (at node 160)
	maximum = 0.055 (at node 137)
Accepted packet rate average = 0.0137865
	minimum = 0.003 (at node 6)
	maximum = 0.023 (at node 0)
Injected flit rate average = 0.551146
	minimum = 0.093 (at node 160)
	maximum = 1 (at node 137)
Accepted flit rate average= 0.249385
	minimum = 0.085 (at node 149)
	maximum = 0.395 (at node 157)
Injected packet length average = 17.9844
Accepted packet length average = 18.0892
Total in-flight flits = 272859 (97886 measured)
latency change    = 5.55737
throughput change = 0.0174805
Class 0:
Packet latency average = 398.658
	minimum = 25
	maximum = 2286
Network latency average = 237.91
	minimum = 25
	maximum = 1897
Slowest packet = 19863
Flit latency average = 2042.77
	minimum = 6
	maximum = 4849
Slowest flit = 13967
Fragmentation average = 53.6257
	minimum = 1
	maximum = 901
Injected packet rate average = 0.030388
	minimum = 0.009 (at node 36)
	maximum = 0.0495 (at node 138)
Accepted packet rate average = 0.013526
	minimum = 0.007 (at node 100)
	maximum = 0.0185 (at node 102)
Injected flit rate average = 0.546667
	minimum = 0.16 (at node 36)
	maximum = 0.89 (at node 138)
Accepted flit rate average= 0.243313
	minimum = 0.143 (at node 6)
	maximum = 0.3315 (at node 181)
Injected packet length average = 17.9895
Accepted packet length average = 17.9884
Total in-flight flits = 331421 (194150 measured)
latency change    = 0.529782
throughput change = 0.0249593
Class 0:
Packet latency average = 806.775
	minimum = 25
	maximum = 3531
Network latency average = 586.151
	minimum = 25
	maximum = 2969
Slowest packet = 19863
Flit latency average = 2380.66
	minimum = 6
	maximum = 5809
Slowest flit = 21134
Fragmentation average = 97.4387
	minimum = 1
	maximum = 1257
Injected packet rate average = 0.0301285
	minimum = 0.00966667 (at node 36)
	maximum = 0.0456667 (at node 163)
Accepted packet rate average = 0.0133802
	minimum = 0.00866667 (at node 100)
	maximum = 0.0186667 (at node 103)
Injected flit rate average = 0.542257
	minimum = 0.174333 (at node 36)
	maximum = 0.823 (at node 163)
Accepted flit rate average= 0.240378
	minimum = 0.163333 (at node 100)
	maximum = 0.340333 (at node 103)
Injected packet length average = 17.9982
Accepted packet length average = 17.9652
Total in-flight flits = 388725 (286632 measured)
latency change    = 0.505863
throughput change = 0.0122059
Draining remaining packets ...
Class 0:
Remaining flits: 10614 10615 10616 10617 10618 10619 16848 16849 16850 16851 [...] (347374 flits)
Measured flits: 356184 356185 356186 356187 356188 356189 356190 356191 356192 356193 [...] (275647 flits)
Class 0:
Remaining flits: 16848 16849 16850 16851 16852 16853 16854 16855 16856 16857 [...] (306458 flits)
Measured flits: 356189 356190 356191 356192 356193 356194 356195 356196 356197 356198 [...] (258819 flits)
Class 0:
Remaining flits: 16848 16849 16850 16851 16852 16853 16854 16855 16856 16857 [...] (267145 flits)
Measured flits: 356256 356257 356258 356259 356260 356261 356262 356263 356264 356265 [...] (232747 flits)
Class 0:
Remaining flits: 16865 16920 16921 16922 16923 16924 16925 16926 16927 16928 [...] (229194 flits)
Measured flits: 356295 356296 356297 356298 356299 356300 356301 356302 356303 356304 [...] (203033 flits)
Class 0:
Remaining flits: 16920 16921 16922 16923 16924 16925 16926 16927 16928 16929 [...] (192050 flits)
Measured flits: 356392 356393 356394 356395 356396 356397 356398 356399 356400 356401 [...] (171941 flits)
Class 0:
Remaining flits: 16920 16921 16922 16923 16924 16925 16926 16927 16928 16929 [...] (156125 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (139947 flits)
Class 0:
Remaining flits: 16920 16921 16922 16923 16924 16925 16926 16927 16928 16929 [...] (121391 flits)
Measured flits: 356400 356401 356402 356403 356404 356405 356406 356407 356408 356409 [...] (109072 flits)
Class 0:
Remaining flits: 16920 16921 16922 16923 16924 16925 16926 16927 16928 16929 [...] (88113 flits)
Measured flits: 356413 356414 356415 356416 356417 356508 356509 356510 356511 356512 [...] (79381 flits)
Class 0:
Remaining flits: 43466 43467 43468 43469 57785 57786 57787 57788 57789 57790 [...] (56814 flits)
Measured flits: 356414 356415 356416 356417 356516 356517 356518 356519 356520 356521 [...] (51387 flits)
Class 0:
Remaining flits: 57785 57786 57787 57788 57789 57790 57791 57792 57793 57794 [...] (27146 flits)
Measured flits: 356526 356527 356528 356529 356530 356531 356532 356533 356534 356535 [...] (24477 flits)
Class 0:
Remaining flits: 105485 105486 105487 105488 105489 105490 105491 105492 105493 105494 [...] (6639 flits)
Measured flits: 356886 356887 356888 356889 356890 356891 356892 356893 356894 356895 [...] (5880 flits)
Class 0:
Remaining flits: 424348 424349 568458 568459 568460 568461 568462 568463 568464 568465 [...] (56 flits)
Measured flits: 424348 424349 568458 568459 568460 568461 568462 568463 568464 568465 [...] (56 flits)
Time taken is 18061 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7132.93 (1 samples)
	minimum = 25 (1 samples)
	maximum = 15352 (1 samples)
Network latency average = 6972.13 (1 samples)
	minimum = 25 (1 samples)
	maximum = 14919 (1 samples)
Flit latency average = 5939.97 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16360 (1 samples)
Fragmentation average = 277.026 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8320 (1 samples)
Injected packet rate average = 0.0301285 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0456667 (1 samples)
Accepted packet rate average = 0.0133802 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.542257 (1 samples)
	minimum = 0.174333 (1 samples)
	maximum = 0.823 (1 samples)
Accepted flit rate average = 0.240378 (1 samples)
	minimum = 0.163333 (1 samples)
	maximum = 0.340333 (1 samples)
Injected packet size average = 17.9982 (1 samples)
Accepted packet size average = 17.9652 (1 samples)
Hops average = 5.20658 (1 samples)
Total run time 29.5539
