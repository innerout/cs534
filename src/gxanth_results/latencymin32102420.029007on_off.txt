BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.713
	minimum = 22
	maximum = 963
Network latency average = 219.891
	minimum = 22
	maximum = 764
Slowest packet = 21
Flit latency average = 190.545
	minimum = 5
	maximum = 747
Slowest flit = 10493
Fragmentation average = 40.3813
	minimum = 0
	maximum = 374
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135365
	minimum = 0.005 (at node 25)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.253609
	minimum = 0.116 (at node 25)
	maximum = 0.425 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 18.7353
Total in-flight flits = 39525 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 550.771
	minimum = 22
	maximum = 1912
Network latency average = 406.781
	minimum = 22
	maximum = 1508
Slowest packet = 21
Flit latency average = 375.469
	minimum = 5
	maximum = 1491
Slowest flit = 39635
Fragmentation average = 54.9928
	minimum = 0
	maximum = 374
Injected packet rate average = 0.0268229
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.0144766
	minimum = 0.008 (at node 153)
	maximum = 0.02 (at node 166)
Injected flit rate average = 0.480539
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.267695
	minimum = 0.144 (at node 153)
	maximum = 0.366 (at node 166)
Injected packet length average = 17.9152
Accepted packet length average = 18.4916
Total in-flight flits = 82605 (0 measured)
latency change    = 0.410439
throughput change = 0.0526193
Class 0:
Packet latency average = 1146.54
	minimum = 23
	maximum = 2805
Network latency average = 943.97
	minimum = 22
	maximum = 2280
Slowest packet = 6000
Flit latency average = 908.085
	minimum = 5
	maximum = 2295
Slowest flit = 56800
Fragmentation average = 85.2331
	minimum = 0
	maximum = 440
Injected packet rate average = 0.0294115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0159792
	minimum = 0.007 (at node 164)
	maximum = 0.026 (at node 99)
Injected flit rate average = 0.529307
	minimum = 0 (at node 8)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.289536
	minimum = 0.152 (at node 4)
	maximum = 0.469 (at node 99)
Injected packet length average = 17.9966
Accepted packet length average = 18.1196
Total in-flight flits = 128660 (0 measured)
latency change    = 0.519625
throughput change = 0.0754349
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 291.53
	minimum = 25
	maximum = 1415
Network latency average = 71.33
	minimum = 22
	maximum = 782
Slowest packet = 15951
Flit latency average = 1325.98
	minimum = 5
	maximum = 3009
Slowest flit = 85438
Fragmentation average = 7.91903
	minimum = 0
	maximum = 69
Injected packet rate average = 0.0308802
	minimum = 0.001 (at node 58)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0160208
	minimum = 0.007 (at node 35)
	maximum = 0.03 (at node 62)
Injected flit rate average = 0.555734
	minimum = 0.018 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.289115
	minimum = 0.129 (at node 35)
	maximum = 0.534 (at node 62)
Injected packet length average = 17.9965
Accepted packet length average = 18.0462
Total in-flight flits = 179872 (97721 measured)
latency change    = 2.93284
throughput change = 0.0014592
Class 0:
Packet latency average = 605.696
	minimum = 25
	maximum = 2631
Network latency average = 386.831
	minimum = 22
	maximum = 1959
Slowest packet = 15951
Flit latency average = 1524.98
	minimum = 5
	maximum = 3703
Slowest flit = 113273
Fragmentation average = 23.82
	minimum = 0
	maximum = 309
Injected packet rate average = 0.0296953
	minimum = 0.003 (at node 58)
	maximum = 0.0555 (at node 6)
Accepted packet rate average = 0.0159948
	minimum = 0.008 (at node 35)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.53482
	minimum = 0.054 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.288755
	minimum = 0.1625 (at node 35)
	maximum = 0.4315 (at node 128)
Injected packet length average = 18.0103
Accepted packet length average = 18.0531
Total in-flight flits = 223032 (180703 measured)
latency change    = 0.518686
throughput change = 0.00124457
Class 0:
Packet latency average = 1194.58
	minimum = 22
	maximum = 4043
Network latency average = 983.481
	minimum = 22
	maximum = 2970
Slowest packet = 15951
Flit latency average = 1732.72
	minimum = 5
	maximum = 4434
Slowest flit = 143579
Fragmentation average = 51.6547
	minimum = 0
	maximum = 365
Injected packet rate average = 0.0294514
	minimum = 0.00466667 (at node 58)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0160087
	minimum = 0.0106667 (at node 173)
	maximum = 0.024 (at node 123)
Injected flit rate average = 0.530122
	minimum = 0.084 (at node 58)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.289255
	minimum = 0.196667 (at node 173)
	maximum = 0.425667 (at node 123)
Injected packet length average = 17.9999
Accepted packet length average = 18.0686
Total in-flight flits = 267401 (251572 measured)
latency change    = 0.492965
throughput change = 0.00172858
Class 0:
Packet latency average = 1825.55
	minimum = 22
	maximum = 4940
Network latency average = 1611.11
	minimum = 22
	maximum = 3932
Slowest packet = 15951
Flit latency average = 1943.48
	minimum = 5
	maximum = 5269
Slowest flit = 158343
Fragmentation average = 74.8024
	minimum = 0
	maximum = 429
Injected packet rate average = 0.0295482
	minimum = 0.00875 (at node 58)
	maximum = 0.05325 (at node 112)
Accepted packet rate average = 0.0160339
	minimum = 0.0115 (at node 173)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.532022
	minimum = 0.1575 (at node 58)
	maximum = 0.962 (at node 112)
Accepted flit rate average= 0.289451
	minimum = 0.2075 (at node 173)
	maximum = 0.39125 (at node 123)
Injected packet length average = 18.0052
Accepted packet length average = 18.0525
Total in-flight flits = 314836 (308856 measured)
latency change    = 0.34563
throughput change = 0.00067477
Class 0:
Packet latency average = 2304.33
	minimum = 22
	maximum = 5701
Network latency average = 2087.9
	minimum = 22
	maximum = 4985
Slowest packet = 15951
Flit latency average = 2165.68
	minimum = 5
	maximum = 5966
Slowest flit = 182068
Fragmentation average = 86.9137
	minimum = 0
	maximum = 429
Injected packet rate average = 0.0292979
	minimum = 0.0124 (at node 58)
	maximum = 0.0492 (at node 17)
Accepted packet rate average = 0.0160062
	minimum = 0.0124 (at node 155)
	maximum = 0.0216 (at node 128)
Injected flit rate average = 0.52731
	minimum = 0.2232 (at node 58)
	maximum = 0.889 (at node 17)
Accepted flit rate average= 0.288964
	minimum = 0.2222 (at node 190)
	maximum = 0.3826 (at node 128)
Injected packet length average = 17.9982
Accepted packet length average = 18.0532
Total in-flight flits = 357523 (354751 measured)
latency change    = 0.207773
throughput change = 0.00168526
Class 0:
Packet latency average = 2666.28
	minimum = 22
	maximum = 7419
Network latency average = 2444.37
	minimum = 22
	maximum = 5905
Slowest packet = 15951
Flit latency average = 2379.86
	minimum = 5
	maximum = 6787
Slowest flit = 204363
Fragmentation average = 95.0029
	minimum = 0
	maximum = 436
Injected packet rate average = 0.0294175
	minimum = 0.0133333 (at node 55)
	maximum = 0.0478333 (at node 109)
Accepted packet rate average = 0.0160582
	minimum = 0.0125 (at node 64)
	maximum = 0.0198333 (at node 115)
Injected flit rate average = 0.529619
	minimum = 0.24 (at node 55)
	maximum = 0.861 (at node 109)
Accepted flit rate average= 0.28946
	minimum = 0.224667 (at node 64)
	maximum = 0.359 (at node 115)
Injected packet length average = 18.0035
Accepted packet length average = 18.0257
Total in-flight flits = 405204 (404087 measured)
latency change    = 0.135752
throughput change = 0.00171536
Class 0:
Packet latency average = 2980.41
	minimum = 22
	maximum = 8251
Network latency average = 2752.5
	minimum = 22
	maximum = 6902
Slowest packet = 15951
Flit latency average = 2597.01
	minimum = 5
	maximum = 7541
Slowest flit = 215531
Fragmentation average = 99.1414
	minimum = 0
	maximum = 486
Injected packet rate average = 0.02925
	minimum = 0.0131429 (at node 96)
	maximum = 0.0485714 (at node 174)
Accepted packet rate average = 0.0160335
	minimum = 0.0125714 (at node 190)
	maximum = 0.02 (at node 70)
Injected flit rate average = 0.526545
	minimum = 0.236571 (at node 96)
	maximum = 0.876286 (at node 174)
Accepted flit rate average= 0.289225
	minimum = 0.228 (at node 80)
	maximum = 0.361429 (at node 128)
Injected packet length average = 18.0016
Accepted packet length average = 18.0388
Total in-flight flits = 447557 (447200 measured)
latency change    = 0.1054
throughput change = 0.000811212
Draining all recorded packets ...
Class 0:
Remaining flits: 263075 263076 263077 263078 263079 263080 263081 263082 263083 263084 [...] (492966 flits)
Measured flits: 288126 288127 288128 288129 288130 288131 288132 288133 288134 288135 [...] (421636 flits)
Class 0:
Remaining flits: 285876 285877 285878 285879 285880 285881 285882 285883 285884 285885 [...] (538463 flits)
Measured flits: 292010 292011 292012 292013 293616 293617 293618 293619 293620 293621 [...] (374761 flits)
Class 0:
Remaining flits: 340744 340745 340746 340747 340748 340749 340750 340751 340752 340753 [...] (584437 flits)
Measured flits: 340744 340745 340746 340747 340748 340749 340750 340751 340752 340753 [...] (327574 flits)
Class 0:
Remaining flits: 352404 352405 352406 352407 352408 352409 352410 352411 352412 352413 [...] (630700 flits)
Measured flits: 352404 352405 352406 352407 352408 352409 352410 352411 352412 352413 [...] (280301 flits)
Class 0:
Remaining flits: 396808 396809 400626 400627 400628 400629 400630 400631 400632 400633 [...] (678055 flits)
Measured flits: 396808 396809 400626 400627 400628 400629 400630 400631 400632 400633 [...] (233390 flits)
Class 0:
Remaining flits: 411373 411374 411375 411376 411377 411378 411379 411380 411381 411382 [...] (726491 flits)
Measured flits: 411373 411374 411375 411376 411377 411378 411379 411380 411381 411382 [...] (187074 flits)
Class 0:
Remaining flits: 424036 424037 424038 424039 424040 424041 424042 424043 429531 429532 [...] (766353 flits)
Measured flits: 424036 424037 424038 424039 424040 424041 424042 424043 429531 429532 [...] (142415 flits)
Class 0:
Remaining flits: 477396 477397 477398 477399 477400 477401 477402 477403 477404 477405 [...] (813895 flits)
Measured flits: 477396 477397 477398 477399 477400 477401 477402 477403 477404 477405 [...] (101376 flits)
Class 0:
Remaining flits: 486162 486163 486164 486165 486166 486167 486168 486169 486170 486171 [...] (859533 flits)
Measured flits: 486162 486163 486164 486165 486166 486167 486168 486169 486170 486171 [...] (68003 flits)
Class 0:
Remaining flits: 525168 525169 525170 525171 525172 525173 525174 525175 525176 525177 [...] (903527 flits)
Measured flits: 525168 525169 525170 525171 525172 525173 525174 525175 525176 525177 [...] (42648 flits)
Class 0:
Remaining flits: 558900 558901 558902 558903 558904 558905 558906 558907 558908 558909 [...] (945305 flits)
Measured flits: 558900 558901 558902 558903 558904 558905 558906 558907 558908 558909 [...] (26690 flits)
Class 0:
Remaining flits: 593946 593947 593948 593949 593950 593951 593952 593953 593954 593955 [...] (989029 flits)
Measured flits: 593946 593947 593948 593949 593950 593951 593952 593953 593954 593955 [...] (17000 flits)
Class 0:
Remaining flits: 611496 611497 611498 611499 611500 611501 611502 611503 611504 611505 [...] (1033329 flits)
Measured flits: 611496 611497 611498 611499 611500 611501 611502 611503 611504 611505 [...] (10366 flits)
Class 0:
Remaining flits: 630316 630317 630318 630319 630320 630321 630322 630323 637038 637039 [...] (1080821 flits)
Measured flits: 630316 630317 630318 630319 630320 630321 630322 630323 637038 637039 [...] (6101 flits)
Class 0:
Remaining flits: 684851 684852 684853 684854 684855 684856 684857 684858 684859 684860 [...] (1125215 flits)
Measured flits: 684851 684852 684853 684854 684855 684856 684857 684858 684859 684860 [...] (3526 flits)
Class 0:
Remaining flits: 849152 849153 849154 849155 849156 849157 849158 849159 849160 849161 [...] (1177427 flits)
Measured flits: 849152 849153 849154 849155 849156 849157 849158 849159 849160 849161 [...] (1842 flits)
Class 0:
Remaining flits: 884880 884881 884882 884883 884884 884885 884886 884887 884888 884889 [...] (1214420 flits)
Measured flits: 884880 884881 884882 884883 884884 884885 884886 884887 884888 884889 [...] (898 flits)
Class 0:
Remaining flits: 903478 903479 903480 903481 903482 903483 903484 903485 903486 903487 [...] (1263174 flits)
Measured flits: 903478 903479 903480 903481 903482 903483 903484 903485 903486 903487 [...] (416 flits)
Class 0:
Remaining flits: 918288 918289 918290 918291 918292 918293 918294 918295 918296 918297 [...] (1310876 flits)
Measured flits: 918288 918289 918290 918291 918292 918293 918294 918295 918296 918297 [...] (197 flits)
Class 0:
Remaining flits: 976284 976285 976286 976287 976288 976289 976290 976291 976292 976293 [...] (1355447 flits)
Measured flits: 976284 976285 976286 976287 976288 976289 976290 976291 976292 976293 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1079568 1079569 1079570 1079571 1079572 1079573 1079574 1079575 1079576 1079577 [...] (1320254 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1123072 1123073 1150362 1150363 1150364 1150365 1150366 1150367 1150368 1150369 [...] (1272624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1174426 1174427 1189620 1189621 1189622 1189623 1189624 1189625 1189626 1189627 [...] (1224826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1197765 1197766 1197767 1197768 1197769 1197770 1197771 1197772 1197773 1228806 [...] (1177237 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1228806 1228807 1228808 1228809 1228810 1228811 1228812 1228813 1228814 1228815 [...] (1129613 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1279782 1279783 1279784 1279785 1279786 1279787 1279788 1279789 1279790 1279791 [...] (1081763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1286982 1286983 1286984 1286985 1286986 1286987 1286988 1286989 1286990 1286991 [...] (1033921 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1286995 1286996 1286997 1286998 1286999 1288782 1288783 1288784 1288785 1288786 [...] (986524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1305468 1305469 1305470 1305471 1305472 1305473 1305474 1305475 1305476 1305477 [...] (939099 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1307304 1307305 1307306 1307307 1307308 1307309 1307310 1307311 1307312 1307313 [...] (891701 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1325988 1325989 1325990 1325991 1325992 1325993 1325994 1325995 1325996 1325997 [...] (843993 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1354014 1354015 1354016 1354017 1354018 1354019 1354020 1354021 1354022 1354023 [...] (796080 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1363293 1363294 1363295 1363296 1363297 1363298 1363299 1363300 1363301 1375794 [...] (748393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1383102 1383103 1383104 1383105 1383106 1383107 1383108 1383109 1383110 1383111 [...] (700520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1383108 1383109 1383110 1383111 1383112 1383113 1383114 1383115 1383116 1383117 [...] (652874 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1421964 1421965 1421966 1421967 1421968 1421969 1421970 1421971 1421972 1421973 [...] (605421 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1463310 1463311 1463312 1463313 1463314 1463315 1463316 1463317 1463318 1463319 [...] (557606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1522476 1522477 1522478 1522479 1522480 1522481 1522482 1522483 1522484 1522485 [...] (509729 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1555830 1555831 1555832 1555833 1555834 1555835 1555836 1555837 1555838 1555839 [...] (461853 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1557666 1557667 1557668 1557669 1557670 1557671 1557672 1557673 1557674 1557675 [...] (414507 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1566468 1566469 1566470 1566471 1566472 1566473 1566474 1566475 1566476 1566477 [...] (367028 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1575000 1575001 1575002 1575003 1575004 1575005 1575006 1575007 1575008 1575009 [...] (319060 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1575016 1575017 1677900 1677901 1677902 1677903 1677904 1677905 1690128 1690129 [...] (271400 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1693440 1693441 1693442 1693443 1693444 1693445 1693446 1693447 1693448 1693449 [...] (223913 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1701684 1701685 1701686 1701687 1701688 1701689 1701690 1701691 1701692 1701693 [...] (177534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1712538 1712539 1712540 1712541 1712542 1712543 1712544 1712545 1712546 1712547 [...] (133412 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1779440 1779441 1779442 1779443 1781424 1781425 1781426 1781427 1781428 1781429 [...] (94838 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1781424 1781425 1781426 1781427 1781428 1781429 1781430 1781431 1781432 1781433 [...] (63950 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2046312 2046313 2046314 2046315 2046316 2046317 2046318 2046319 2046320 2046321 [...] (41955 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2114352 2114353 2114354 2114355 2114356 2114357 2114358 2114359 2114360 2114361 [...] (27160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2184894 2184895 2184896 2184897 2184898 2184899 2184900 2184901 2184902 2184903 [...] (17197 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2184894 2184895 2184896 2184897 2184898 2184899 2184900 2184901 2184902 2184903 [...] (10404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2375478 2375479 2375480 2375481 2375482 2375483 2375484 2375485 2375486 2375487 [...] (5776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2435184 2435185 2435186 2435187 2435188 2435189 2435190 2435191 2435192 2435193 [...] (2772 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2544174 2544175 2544176 2544177 2544178 2544179 2544180 2544181 2544182 2544183 [...] (834 flits)
Measured flits: (0 flits)
Time taken is 66277 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6206.67 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20506 (1 samples)
Network latency average = 5952.12 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20489 (1 samples)
Flit latency average = 13964.6 (1 samples)
	minimum = 5 (1 samples)
	maximum = 40664 (1 samples)
Fragmentation average = 128.424 (1 samples)
	minimum = 0 (1 samples)
	maximum = 493 (1 samples)
Injected packet rate average = 0.02925 (1 samples)
	minimum = 0.0131429 (1 samples)
	maximum = 0.0485714 (1 samples)
Accepted packet rate average = 0.0160335 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.526545 (1 samples)
	minimum = 0.236571 (1 samples)
	maximum = 0.876286 (1 samples)
Accepted flit rate average = 0.289225 (1 samples)
	minimum = 0.228 (1 samples)
	maximum = 0.361429 (1 samples)
Injected packet size average = 18.0016 (1 samples)
Accepted packet size average = 18.0388 (1 samples)
Hops average = 5.05993 (1 samples)
Total run time 61.7131
