BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 350.038
	minimum = 22
	maximum = 970
Network latency average = 233.593
	minimum = 22
	maximum = 765
Slowest packet = 46
Flit latency average = 209.953
	minimum = 5
	maximum = 756
Slowest flit = 21175
Fragmentation average = 19.3001
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 150)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.261396
	minimum = 0.124 (at node 150)
	maximum = 0.433 (at node 49)
Injected packet length average = 17.8285
Accepted packet length average = 18.3704
Total in-flight flits = 52646 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 616.141
	minimum = 22
	maximum = 1917
Network latency average = 446.193
	minimum = 22
	maximum = 1521
Slowest packet = 46
Flit latency average = 423.338
	minimum = 5
	maximum = 1581
Slowest flit = 37366
Fragmentation average = 20.8235
	minimum = 0
	maximum = 106
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0150807
	minimum = 0.009 (at node 153)
	maximum = 0.022 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.27444
	minimum = 0.162 (at node 153)
	maximum = 0.396 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.1981
Total in-flight flits = 111389 (0 measured)
latency change    = 0.431885
throughput change = 0.0475305
Class 0:
Packet latency average = 1348.71
	minimum = 25
	maximum = 2809
Network latency average = 1065.08
	minimum = 22
	maximum = 2317
Slowest packet = 5974
Flit latency average = 1047.99
	minimum = 5
	maximum = 2300
Slowest flit = 62654
Fragmentation average = 22.0766
	minimum = 0
	maximum = 106
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0165156
	minimum = 0.008 (at node 64)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.29751
	minimum = 0.134 (at node 64)
	maximum = 0.546 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.0139
Total in-flight flits = 176433 (0 measured)
latency change    = 0.543162
throughput change = 0.0775446
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 430.716
	minimum = 22
	maximum = 2109
Network latency average = 42.34
	minimum = 22
	maximum = 531
Slowest packet = 18852
Flit latency average = 1493.3
	minimum = 5
	maximum = 2956
Slowest flit = 105605
Fragmentation average = 4.74909
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0366615
	minimum = 0.002 (at node 147)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.016526
	minimum = 0.006 (at node 184)
	maximum = 0.03 (at node 128)
Injected flit rate average = 0.660062
	minimum = 0.033 (at node 147)
	maximum = 1 (at node 12)
Accepted flit rate average= 0.297974
	minimum = 0.108 (at node 184)
	maximum = 0.54 (at node 128)
Injected packet length average = 18.0043
Accepted packet length average = 18.0306
Total in-flight flits = 245924 (116608 measured)
latency change    = 2.13131
throughput change = 0.00155564
Class 0:
Packet latency average = 512.233
	minimum = 22
	maximum = 2752
Network latency average = 107.5
	minimum = 22
	maximum = 1961
Slowest packet = 18852
Flit latency average = 1726.54
	minimum = 5
	maximum = 3739
Slowest flit = 126665
Fragmentation average = 5.65606
	minimum = 0
	maximum = 81
Injected packet rate average = 0.0369609
	minimum = 0.0085 (at node 112)
	maximum = 0.056 (at node 90)
Accepted packet rate average = 0.0164844
	minimum = 0.0105 (at node 5)
	maximum = 0.0235 (at node 128)
Injected flit rate average = 0.665266
	minimum = 0.153 (at node 112)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.296914
	minimum = 0.189 (at node 5)
	maximum = 0.423 (at node 128)
Injected packet length average = 17.9992
Accepted packet length average = 18.0118
Total in-flight flits = 317892 (234334 measured)
latency change    = 0.15914
throughput change = 0.00356971
Class 0:
Packet latency average = 943.179
	minimum = 22
	maximum = 3671
Network latency average = 558.87
	minimum = 22
	maximum = 2969
Slowest packet = 18852
Flit latency average = 1956.79
	minimum = 5
	maximum = 4478
Slowest flit = 153053
Fragmentation average = 9.68819
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0370694
	minimum = 0.0116667 (at node 81)
	maximum = 0.0556667 (at node 57)
Accepted packet rate average = 0.0164931
	minimum = 0.0103333 (at node 79)
	maximum = 0.0226667 (at node 165)
Injected flit rate average = 0.66766
	minimum = 0.215 (at node 81)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.296979
	minimum = 0.186 (at node 79)
	maximum = 0.406 (at node 138)
Injected packet length average = 18.0111
Accepted packet length average = 18.0063
Total in-flight flits = 389709 (344558 measured)
latency change    = 0.456908
throughput change = 0.000219221
Class 0:
Packet latency average = 1697.39
	minimum = 22
	maximum = 5297
Network latency average = 1343.22
	minimum = 22
	maximum = 3974
Slowest packet = 18852
Flit latency average = 2204.72
	minimum = 5
	maximum = 5201
Slowest flit = 192111
Fragmentation average = 15.0022
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0367135
	minimum = 0.01475 (at node 81)
	maximum = 0.0555 (at node 33)
Accepted packet rate average = 0.0165
	minimum = 0.01125 (at node 36)
	maximum = 0.02175 (at node 70)
Injected flit rate average = 0.660687
	minimum = 0.2685 (at node 81)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.296905
	minimum = 0.20175 (at node 36)
	maximum = 0.3935 (at node 70)
Injected packet length average = 17.9957
Accepted packet length average = 17.9942
Total in-flight flits = 455938 (434507 measured)
latency change    = 0.444335
throughput change = 0.000249975
Class 0:
Packet latency average = 2353.24
	minimum = 22
	maximum = 6253
Network latency average = 2002.75
	minimum = 22
	maximum = 4989
Slowest packet = 18852
Flit latency average = 2456.41
	minimum = 5
	maximum = 5820
Slowest flit = 233118
Fragmentation average = 18.1624
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0368792
	minimum = 0.0152 (at node 58)
	maximum = 0.0556 (at node 33)
Accepted packet rate average = 0.0165156
	minimum = 0.012 (at node 149)
	maximum = 0.0208 (at node 70)
Injected flit rate average = 0.663794
	minimum = 0.2748 (at node 58)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.297342
	minimum = 0.216 (at node 149)
	maximum = 0.3744 (at node 70)
Injected packet length average = 17.9992
Accepted packet length average = 18.0037
Total in-flight flits = 528257 (519510 measured)
latency change    = 0.278703
throughput change = 0.00146874
Class 0:
Packet latency average = 2890.63
	minimum = 22
	maximum = 7161
Network latency average = 2537.62
	minimum = 22
	maximum = 5940
Slowest packet = 18852
Flit latency average = 2707.77
	minimum = 5
	maximum = 6498
Slowest flit = 276102
Fragmentation average = 20.0176
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0369479
	minimum = 0.0155 (at node 58)
	maximum = 0.0555 (at node 33)
Accepted packet rate average = 0.0165417
	minimum = 0.0123333 (at node 104)
	maximum = 0.0211667 (at node 70)
Injected flit rate average = 0.665066
	minimum = 0.28 (at node 58)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.297724
	minimum = 0.222 (at node 104)
	maximum = 0.381 (at node 70)
Injected packet length average = 18.0001
Accepted packet length average = 17.9984
Total in-flight flits = 599607 (597157 measured)
latency change    = 0.185908
throughput change = 0.00128405
Class 0:
Packet latency average = 3376.46
	minimum = 22
	maximum = 8616
Network latency average = 3008.93
	minimum = 22
	maximum = 6898
Slowest packet = 18852
Flit latency average = 2964.78
	minimum = 5
	maximum = 7177
Slowest flit = 313469
Fragmentation average = 21.4656
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0367426
	minimum = 0.0157143 (at node 58)
	maximum = 0.0555714 (at node 33)
Accepted packet rate average = 0.016506
	minimum = 0.0118571 (at node 104)
	maximum = 0.0208571 (at node 70)
Injected flit rate average = 0.661359
	minimum = 0.282714 (at node 58)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.297122
	minimum = 0.213429 (at node 104)
	maximum = 0.375429 (at node 70)
Injected packet length average = 17.9998
Accepted packet length average = 18.0009
Total in-flight flits = 665977 (665608 measured)
latency change    = 0.143886
throughput change = 0.00202588
Draining all recorded packets ...
Class 0:
Remaining flits: 352926 352927 352928 352929 352930 352931 352932 352933 352934 352935 [...] (731819 flits)
Measured flits: 352926 352927 352928 352929 352930 352931 352932 352933 352934 352935 [...] (666398 flits)
Class 0:
Remaining flits: 360918 360919 360920 360921 360922 360923 360924 360925 360926 360927 [...] (788567 flits)
Measured flits: 360918 360919 360920 360921 360922 360923 360924 360925 360926 360927 [...] (630269 flits)
Class 0:
Remaining flits: 416196 416197 416198 416199 416200 416201 416202 416203 416204 416205 [...] (840163 flits)
Measured flits: 416196 416197 416198 416199 416200 416201 416202 416203 416204 416205 [...] (586960 flits)
Class 0:
Remaining flits: 429534 429535 429536 429537 429538 429539 429540 429541 429542 429543 [...] (883780 flits)
Measured flits: 429534 429535 429536 429537 429538 429539 429540 429541 429542 429543 [...] (542838 flits)
Class 0:
Remaining flits: 506959 506960 506961 506962 506963 506964 506965 506966 506967 506968 [...] (923622 flits)
Measured flits: 506959 506960 506961 506962 506963 506964 506965 506966 506967 506968 [...] (497802 flits)
Class 0:
Remaining flits: 528529 528530 528531 528532 528533 537930 537931 537932 537933 537934 [...] (958053 flits)
Measured flits: 528529 528530 528531 528532 528533 537930 537931 537932 537933 537934 [...] (453595 flits)
Class 0:
Remaining flits: 570636 570637 570638 570639 570640 570641 570642 570643 570644 570645 [...] (986335 flits)
Measured flits: 570636 570637 570638 570639 570640 570641 570642 570643 570644 570645 [...] (408263 flits)
Class 0:
Remaining flits: 603810 603811 603812 603813 603814 603815 603816 603817 603818 603819 [...] (1010497 flits)
Measured flits: 603810 603811 603812 603813 603814 603815 603816 603817 603818 603819 [...] (361219 flits)
Class 0:
Remaining flits: 610488 610489 610490 610491 610492 610493 610494 610495 610496 610497 [...] (1032013 flits)
Measured flits: 610488 610489 610490 610491 610492 610493 610494 610495 610496 610497 [...] (314830 flits)
Class 0:
Remaining flits: 678384 678385 678386 678387 678388 678389 678390 678391 678392 678393 [...] (1044144 flits)
Measured flits: 678384 678385 678386 678387 678388 678389 678390 678391 678392 678393 [...] (270702 flits)
Class 0:
Remaining flits: 709200 709201 709202 709203 709204 709205 709206 709207 709208 709209 [...] (1050679 flits)
Measured flits: 709200 709201 709202 709203 709204 709205 709206 709207 709208 709209 [...] (228749 flits)
Class 0:
Remaining flits: 722808 722809 722810 722811 722812 722813 722814 722815 722816 722817 [...] (1055774 flits)
Measured flits: 722808 722809 722810 722811 722812 722813 722814 722815 722816 722817 [...] (190764 flits)
Class 0:
Remaining flits: 759996 759997 759998 759999 760000 760001 760002 760003 760004 760005 [...] (1056190 flits)
Measured flits: 759996 759997 759998 759999 760000 760001 760002 760003 760004 760005 [...] (158623 flits)
Class 0:
Remaining flits: 842526 842527 842528 842529 842530 842531 842532 842533 842534 842535 [...] (1061243 flits)
Measured flits: 842526 842527 842528 842529 842530 842531 842532 842533 842534 842535 [...] (129559 flits)
Class 0:
Remaining flits: 868410 868411 868412 868413 868414 868415 868416 868417 868418 868419 [...] (1065813 flits)
Measured flits: 868410 868411 868412 868413 868414 868415 868416 868417 868418 868419 [...] (104138 flits)
Class 0:
Remaining flits: 894240 894241 894242 894243 894244 894245 894246 894247 894248 894249 [...] (1067641 flits)
Measured flits: 894240 894241 894242 894243 894244 894245 894246 894247 894248 894249 [...] (82945 flits)
Class 0:
Remaining flits: 901422 901423 901424 901425 901426 901427 901428 901429 901430 901431 [...] (1068600 flits)
Measured flits: 901422 901423 901424 901425 901426 901427 901428 901429 901430 901431 [...] (65929 flits)
Class 0:
Remaining flits: 943200 943201 943202 943203 943204 943205 943206 943207 943208 943209 [...] (1073596 flits)
Measured flits: 943200 943201 943202 943203 943204 943205 943206 943207 943208 943209 [...] (52096 flits)
Class 0:
Remaining flits: 969192 969193 969194 969195 969196 969197 969198 969199 969200 969201 [...] (1073155 flits)
Measured flits: 969192 969193 969194 969195 969196 969197 969198 969199 969200 969201 [...] (40117 flits)
Class 0:
Remaining flits: 1019124 1019125 1019126 1019127 1019128 1019129 1019130 1019131 1019132 1019133 [...] (1069502 flits)
Measured flits: 1019124 1019125 1019126 1019127 1019128 1019129 1019130 1019131 1019132 1019133 [...] (30020 flits)
Class 0:
Remaining flits: 1068822 1068823 1068824 1068825 1068826 1068827 1068828 1068829 1068830 1068831 [...] (1063173 flits)
Measured flits: 1068822 1068823 1068824 1068825 1068826 1068827 1068828 1068829 1068830 1068831 [...] (22414 flits)
Class 0:
Remaining flits: 1087524 1087525 1087526 1087527 1087528 1087529 1087530 1087531 1087532 1087533 [...] (1060330 flits)
Measured flits: 1087524 1087525 1087526 1087527 1087528 1087529 1087530 1087531 1087532 1087533 [...] (16676 flits)
Class 0:
Remaining flits: 1116108 1116109 1116110 1116111 1116112 1116113 1116114 1116115 1116116 1116117 [...] (1065675 flits)
Measured flits: 1116108 1116109 1116110 1116111 1116112 1116113 1116114 1116115 1116116 1116117 [...] (11905 flits)
Class 0:
Remaining flits: 1149804 1149805 1149806 1149807 1149808 1149809 1149810 1149811 1149812 1149813 [...] (1063958 flits)
Measured flits: 1149804 1149805 1149806 1149807 1149808 1149809 1149810 1149811 1149812 1149813 [...] (7886 flits)
Class 0:
Remaining flits: 1156122 1156123 1156124 1156125 1156126 1156127 1156128 1156129 1156130 1156131 [...] (1068018 flits)
Measured flits: 1156122 1156123 1156124 1156125 1156126 1156127 1156128 1156129 1156130 1156131 [...] (5150 flits)
Class 0:
Remaining flits: 1186614 1186615 1186616 1186617 1186618 1186619 1186620 1186621 1186622 1186623 [...] (1068722 flits)
Measured flits: 1186614 1186615 1186616 1186617 1186618 1186619 1186620 1186621 1186622 1186623 [...] (3308 flits)
Class 0:
Remaining flits: 1199628 1199629 1199630 1199631 1199632 1199633 1199634 1199635 1199636 1199637 [...] (1076259 flits)
Measured flits: 1199628 1199629 1199630 1199631 1199632 1199633 1199634 1199635 1199636 1199637 [...] (2081 flits)
Class 0:
Remaining flits: 1283778 1283779 1283780 1283781 1283782 1283783 1283784 1283785 1283786 1283787 [...] (1074878 flits)
Measured flits: 1283778 1283779 1283780 1283781 1283782 1283783 1283784 1283785 1283786 1283787 [...] (1170 flits)
Class 0:
Remaining flits: 1327097 1327098 1327099 1327100 1327101 1327102 1327103 1344438 1344439 1344440 [...] (1071391 flits)
Measured flits: 1327097 1327098 1327099 1327100 1327101 1327102 1327103 1344438 1344439 1344440 [...] (331 flits)
Class 0:
Remaining flits: 1371546 1371547 1371548 1371549 1371550 1371551 1371552 1371553 1371554 1371555 [...] (1070018 flits)
Measured flits: 1589112 1589113 1589114 1589115 1589116 1589117 1589118 1589119 1589120 1589121 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1392246 1392247 1392248 1392249 1392250 1392251 1392252 1392253 1392254 1392255 [...] (1019312 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1555812 1555813 1555814 1555815 1555816 1555817 1555818 1555819 1555820 1555821 [...] (969084 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1624302 1624303 1624304 1624305 1624306 1624307 1624308 1624309 1624310 1624311 [...] (918066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1662678 1662679 1662680 1662681 1662682 1662683 1662684 1662685 1662686 1662687 [...] (867931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1662678 1662679 1662680 1662681 1662682 1662683 1662684 1662685 1662686 1662687 [...] (817359 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1782558 1782559 1782560 1782561 1782562 1782563 1782564 1782565 1782566 1782567 [...] (767109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1844064 1844065 1844066 1844067 1844068 1844069 1844070 1844071 1844072 1844073 [...] (716451 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1945512 1945513 1945514 1945515 1945516 1945517 1945518 1945519 1945520 1945521 [...] (665869 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1962300 1962301 1962302 1962303 1962304 1962305 1966464 1966465 1966466 1966467 [...] (616021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1985436 1985437 1985438 1985439 1985440 1985441 1985442 1985443 1985444 1985445 [...] (566881 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2004804 2004805 2004806 2004807 2004808 2004809 2004810 2004811 2004812 2004813 [...] (518896 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2069892 2069893 2069894 2069895 2069896 2069897 2069898 2069899 2069900 2069901 [...] (471096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2148660 2148661 2148662 2148663 2148664 2148665 2148666 2148667 2148668 2148669 [...] (423281 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2184624 2184625 2184626 2184627 2184628 2184629 2184630 2184631 2184632 2184633 [...] (375363 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2217258 2217259 2217260 2217261 2217262 2217263 2217264 2217265 2217266 2217267 [...] (327556 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2299662 2299663 2299664 2299665 2299666 2299667 2299668 2299669 2299670 2299671 [...] (280034 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2382858 2382859 2382860 2382861 2382862 2382863 2382864 2382865 2382866 2382867 [...] (232397 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2464128 2464129 2464130 2464131 2464132 2464133 2464134 2464135 2464136 2464137 [...] (184512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2529108 2529109 2529110 2529111 2529112 2529113 2529114 2529115 2529116 2529117 [...] (136658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2552436 2552437 2552438 2552439 2552440 2552441 2552442 2552443 2552444 2552445 [...] (88773 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2666880 2666881 2666882 2666883 2666884 2666885 2666886 2666887 2666888 2666889 [...] (41671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2856618 2856619 2856620 2856621 2856622 2856623 2856624 2856625 2856626 2856627 [...] (8760 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2919600 2919601 2919602 2919603 2919604 2919605 2919606 2919607 2919608 2919609 [...] (552 flits)
Measured flits: (0 flits)
Time taken is 63725 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9729.21 (1 samples)
	minimum = 22 (1 samples)
	maximum = 30917 (1 samples)
Network latency average = 9042.29 (1 samples)
	minimum = 22 (1 samples)
	maximum = 28193 (1 samples)
Flit latency average = 14853.3 (1 samples)
	minimum = 5 (1 samples)
	maximum = 33851 (1 samples)
Fragmentation average = 27.4314 (1 samples)
	minimum = 0 (1 samples)
	maximum = 773 (1 samples)
Injected packet rate average = 0.0367426 (1 samples)
	minimum = 0.0157143 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.016506 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0208571 (1 samples)
Injected flit rate average = 0.661359 (1 samples)
	minimum = 0.282714 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.297122 (1 samples)
	minimum = 0.213429 (1 samples)
	maximum = 0.375429 (1 samples)
Injected packet size average = 17.9998 (1 samples)
Accepted packet size average = 18.0009 (1 samples)
Hops average = 5.07537 (1 samples)
Total run time 60.1122
