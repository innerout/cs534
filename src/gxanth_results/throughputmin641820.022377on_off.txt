BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 270.444
	minimum = 25
	maximum = 969
Network latency average = 193.462
	minimum = 22
	maximum = 852
Slowest packet = 69
Flit latency average = 161.303
	minimum = 5
	maximum = 835
Slowest flit = 11087
Fragmentation average = 45.1954
	minimum = 0
	maximum = 484
Injected packet rate average = 0.020375
	minimum = 0.001 (at node 54)
	maximum = 0.055 (at node 22)
Accepted packet rate average = 0.0126875
	minimum = 0.004 (at node 64)
	maximum = 0.022 (at node 22)
Injected flit rate average = 0.363568
	minimum = 0.018 (at node 54)
	maximum = 0.979 (at node 22)
Accepted flit rate average= 0.239016
	minimum = 0.091 (at node 174)
	maximum = 0.418 (at node 22)
Injected packet length average = 17.8438
Accepted packet length average = 18.8387
Total in-flight flits = 24633 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 446.803
	minimum = 25
	maximum = 1904
Network latency average = 338.221
	minimum = 22
	maximum = 1474
Slowest packet = 69
Flit latency average = 300.981
	minimum = 5
	maximum = 1486
Slowest flit = 33415
Fragmentation average = 58.8371
	minimum = 0
	maximum = 532
Injected packet rate average = 0.0206276
	minimum = 0.004 (at node 45)
	maximum = 0.0495 (at node 22)
Accepted packet rate average = 0.0135599
	minimum = 0.0075 (at node 116)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.369247
	minimum = 0.0695 (at node 189)
	maximum = 0.885 (at node 22)
Accepted flit rate average= 0.251526
	minimum = 0.144 (at node 116)
	maximum = 0.39 (at node 152)
Injected packet length average = 17.9006
Accepted packet length average = 18.5493
Total in-flight flits = 46136 (0 measured)
latency change    = 0.394713
throughput change = 0.0497381
Class 0:
Packet latency average = 925.369
	minimum = 24
	maximum = 2553
Network latency average = 747.234
	minimum = 22
	maximum = 2034
Slowest packet = 2133
Flit latency average = 703.856
	minimum = 5
	maximum = 2017
Slowest flit = 52847
Fragmentation average = 86.3497
	minimum = 0
	maximum = 731
Injected packet rate average = 0.0196198
	minimum = 0 (at node 96)
	maximum = 0.05 (at node 57)
Accepted packet rate average = 0.0151042
	minimum = 0.006 (at node 31)
	maximum = 0.027 (at node 120)
Injected flit rate average = 0.353958
	minimum = 0 (at node 96)
	maximum = 0.893 (at node 57)
Accepted flit rate average= 0.274141
	minimum = 0.106 (at node 134)
	maximum = 0.503 (at node 120)
Injected packet length average = 18.0409
Accepted packet length average = 18.15
Total in-flight flits = 61433 (0 measured)
latency change    = 0.517163
throughput change = 0.0824926
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 555.927
	minimum = 22
	maximum = 2318
Network latency average = 308.125
	minimum = 22
	maximum = 977
Slowest packet = 11714
Flit latency average = 978.545
	minimum = 5
	maximum = 2748
Slowest flit = 73025
Fragmentation average = 29.8119
	minimum = 0
	maximum = 305
Injected packet rate average = 0.017776
	minimum = 0 (at node 3)
	maximum = 0.05 (at node 26)
Accepted packet rate average = 0.0150625
	minimum = 0.007 (at node 88)
	maximum = 0.028 (at node 129)
Injected flit rate average = 0.318937
	minimum = 0 (at node 14)
	maximum = 0.899 (at node 26)
Accepted flit rate average= 0.269932
	minimum = 0.141 (at node 55)
	maximum = 0.477 (at node 181)
Injected packet length average = 17.942
Accepted packet length average = 17.9208
Total in-flight flits = 71040 (51165 measured)
latency change    = 0.664552
throughput change = 0.0155903
Class 0:
Packet latency average = 1063.34
	minimum = 22
	maximum = 4383
Network latency average = 774.243
	minimum = 22
	maximum = 1978
Slowest packet = 11714
Flit latency average = 1069.96
	minimum = 5
	maximum = 3083
Slowest flit = 104399
Fragmentation average = 70.2275
	minimum = 0
	maximum = 536
Injected packet rate average = 0.0179036
	minimum = 0.001 (at node 95)
	maximum = 0.039 (at node 26)
Accepted packet rate average = 0.0150964
	minimum = 0.009 (at node 36)
	maximum = 0.022 (at node 140)
Injected flit rate average = 0.321846
	minimum = 0.018 (at node 95)
	maximum = 0.7095 (at node 26)
Accepted flit rate average= 0.271651
	minimum = 0.156 (at node 36)
	maximum = 0.3965 (at node 140)
Injected packet length average = 17.9766
Accepted packet length average = 17.9945
Total in-flight flits = 81067 (78563 measured)
latency change    = 0.477189
throughput change = 0.00632705
Class 0:
Packet latency average = 1445.25
	minimum = 22
	maximum = 4992
Network latency average = 1082.51
	minimum = 22
	maximum = 2940
Slowest packet = 11714
Flit latency average = 1163.42
	minimum = 5
	maximum = 3705
Slowest flit = 149957
Fragmentation average = 86.1655
	minimum = 0
	maximum = 671
Injected packet rate average = 0.0176024
	minimum = 0.00233333 (at node 125)
	maximum = 0.0326667 (at node 35)
Accepted packet rate average = 0.0150799
	minimum = 0.00966667 (at node 36)
	maximum = 0.0203333 (at node 129)
Injected flit rate average = 0.316233
	minimum = 0.042 (at node 125)
	maximum = 0.588 (at node 179)
Accepted flit rate average= 0.271323
	minimum = 0.178333 (at node 36)
	maximum = 0.369667 (at node 103)
Injected packet length average = 17.9653
Accepted packet length average = 17.9924
Total in-flight flits = 87797 (87670 measured)
latency change    = 0.264249
throughput change = 0.00120935
Draining remaining packets ...
Class 0:
Remaining flits: 226008 226009 226010 226011 226012 226013 226014 226015 226016 226017 [...] (40733 flits)
Measured flits: 226008 226009 226010 226011 226012 226013 226014 226015 226016 226017 [...] (40733 flits)
Class 0:
Remaining flits: 293894 293895 293896 293897 293898 293899 293900 293901 293902 293903 [...] (2809 flits)
Measured flits: 293894 293895 293896 293897 293898 293899 293900 293901 293902 293903 [...] (2809 flits)
Time taken is 8578 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1997.17 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6084 (1 samples)
Network latency average = 1498.06 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4034 (1 samples)
Flit latency average = 1410.3 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4000 (1 samples)
Fragmentation average = 95.8332 (1 samples)
	minimum = 0 (1 samples)
	maximum = 671 (1 samples)
Injected packet rate average = 0.0176024 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.0326667 (1 samples)
Accepted packet rate average = 0.0150799 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.316233 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.588 (1 samples)
Accepted flit rate average = 0.271323 (1 samples)
	minimum = 0.178333 (1 samples)
	maximum = 0.369667 (1 samples)
Injected packet size average = 17.9653 (1 samples)
Accepted packet size average = 17.9924 (1 samples)
Hops average = 5.11057 (1 samples)
Total run time 10.5753
