BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.059
	minimum = 22
	maximum = 879
Network latency average = 297.674
	minimum = 22
	maximum = 834
Slowest packet = 688
Flit latency average = 261.714
	minimum = 5
	maximum = 866
Slowest flit = 14877
Fragmentation average = 78.0424
	minimum = 0
	maximum = 413
Injected packet rate average = 0.042901
	minimum = 0.03 (at node 51)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0149688
	minimum = 0.005 (at node 115)
	maximum = 0.024 (at node 132)
Injected flit rate average = 0.76549
	minimum = 0.531 (at node 145)
	maximum = 0.992 (at node 55)
Accepted flit rate average= 0.288943
	minimum = 0.125 (at node 115)
	maximum = 0.441 (at node 44)
Injected packet length average = 17.8431
Accepted packet length average = 19.3031
Total in-flight flits = 92789 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 578.041
	minimum = 22
	maximum = 1700
Network latency average = 555.374
	minimum = 22
	maximum = 1633
Slowest packet = 970
Flit latency average = 516.13
	minimum = 5
	maximum = 1699
Slowest flit = 39022
Fragmentation average = 107.36
	minimum = 0
	maximum = 413
Injected packet rate average = 0.0438047
	minimum = 0.029 (at node 51)
	maximum = 0.055 (at node 74)
Accepted packet rate average = 0.0157526
	minimum = 0.0085 (at node 153)
	maximum = 0.0215 (at node 46)
Injected flit rate average = 0.784995
	minimum = 0.5145 (at node 51)
	maximum = 0.984 (at node 74)
Accepted flit rate average= 0.297073
	minimum = 0.1565 (at node 153)
	maximum = 0.4135 (at node 46)
Injected packet length average = 17.9203
Accepted packet length average = 18.8587
Total in-flight flits = 188702 (0 measured)
latency change    = 0.454953
throughput change = 0.0273677
Class 0:
Packet latency average = 1358.97
	minimum = 23
	maximum = 2471
Network latency average = 1328.2
	minimum = 22
	maximum = 2443
Slowest packet = 2179
Flit latency average = 1291.84
	minimum = 5
	maximum = 2556
Slowest flit = 62101
Fragmentation average = 145.647
	minimum = 0
	maximum = 418
Injected packet rate average = 0.0442917
	minimum = 0.032 (at node 35)
	maximum = 0.056 (at node 95)
Accepted packet rate average = 0.0173021
	minimum = 0.007 (at node 91)
	maximum = 0.037 (at node 16)
Injected flit rate average = 0.797573
	minimum = 0.562 (at node 35)
	maximum = 1 (at node 55)
Accepted flit rate average= 0.310854
	minimum = 0.131 (at node 144)
	maximum = 0.671 (at node 16)
Injected packet length average = 18.0073
Accepted packet length average = 17.9663
Total in-flight flits = 282090 (0 measured)
latency change    = 0.574647
throughput change = 0.0443335
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 70.542
	minimum = 23
	maximum = 226
Network latency average = 36.9905
	minimum = 22
	maximum = 147
Slowest packet = 25354
Flit latency average = 1850.15
	minimum = 5
	maximum = 3267
Slowest flit = 105847
Fragmentation average = 5.88431
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0448646
	minimum = 0.027 (at node 173)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0167188
	minimum = 0.007 (at node 67)
	maximum = 0.025 (at node 14)
Injected flit rate average = 0.80726
	minimum = 0.486 (at node 173)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.303052
	minimum = 0.145 (at node 67)
	maximum = 0.451 (at node 69)
Injected packet length average = 17.9933
Accepted packet length average = 18.1265
Total in-flight flits = 378956 (143586 measured)
latency change    = 18.2647
throughput change = 0.025745
Class 0:
Packet latency average = 72.9816
	minimum = 23
	maximum = 355
Network latency average = 38.6245
	minimum = 22
	maximum = 307
Slowest packet = 25354
Flit latency average = 2111.99
	minimum = 5
	maximum = 3970
Slowest flit = 149936
Fragmentation average = 6.07126
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0447057
	minimum = 0.034 (at node 102)
	maximum = 0.056 (at node 143)
Accepted packet rate average = 0.0169531
	minimum = 0.01 (at node 4)
	maximum = 0.0235 (at node 123)
Injected flit rate average = 0.804466
	minimum = 0.6095 (at node 102)
	maximum = 1 (at node 143)
Accepted flit rate average= 0.305385
	minimum = 0.167 (at node 4)
	maximum = 0.4105 (at node 182)
Injected packet length average = 17.9947
Accepted packet length average = 18.0135
Total in-flight flits = 473828 (285375 measured)
latency change    = 0.0334278
throughput change = 0.00764062
Class 0:
Packet latency average = 73.5517
	minimum = 22
	maximum = 459
Network latency average = 39.5755
	minimum = 22
	maximum = 307
Slowest packet = 25354
Flit latency average = 2364.35
	minimum = 5
	maximum = 4682
Slowest flit = 196246
Fragmentation average = 6.42652
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0445851
	minimum = 0.0343333 (at node 102)
	maximum = 0.0556667 (at node 143)
Accepted packet rate average = 0.017092
	minimum = 0.0113333 (at node 80)
	maximum = 0.0236667 (at node 128)
Injected flit rate average = 0.802568
	minimum = 0.617333 (at node 102)
	maximum = 1 (at node 143)
Accepted flit rate average= 0.307507
	minimum = 0.200667 (at node 80)
	maximum = 0.431667 (at node 128)
Injected packet length average = 18.0008
Accepted packet length average = 17.9913
Total in-flight flits = 567224 (425737 measured)
latency change    = 0.00775098
throughput change = 0.00689912
Draining remaining packets ...
Class 0:
Remaining flits: 226102 226103 226104 226105 226106 226107 226108 226109 226110 226111 [...] (519319 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (424942 flits)
Class 0:
Remaining flits: 264913 264914 264915 264916 264917 264918 264919 264920 264921 264922 [...] (472294 flits)
Measured flits: 455850 455851 455852 455853 455854 455855 455856 455857 455858 455859 [...] (420498 flits)
Class 0:
Remaining flits: 288342 288343 288344 288345 288346 288347 288348 288349 288350 288351 [...] (425066 flits)
Measured flits: 455868 455869 455870 455871 455872 455873 455874 455875 455876 455877 [...] (404649 flits)
Class 0:
Remaining flits: 345633 345634 345635 356520 356521 356522 356523 356524 356525 359575 [...] (378065 flits)
Measured flits: 455886 455887 455888 455889 455890 455891 455892 455893 455894 455895 [...] (372258 flits)
Class 0:
Remaining flits: 380394 380395 380396 380397 380398 380399 380400 380401 380402 380403 [...] (331189 flits)
Measured flits: 456300 456301 456302 456303 456304 456305 456306 456307 456308 456309 [...] (330213 flits)
Class 0:
Remaining flits: 384547 384548 384549 384550 384551 409252 409253 409254 409255 409256 [...] (284140 flits)
Measured flits: 456300 456301 456302 456303 456304 456305 456306 456307 456308 456309 [...] (283973 flits)
Class 0:
Remaining flits: 456300 456301 456302 456303 456304 456305 456306 456307 456308 456309 [...] (236491 flits)
Measured flits: 456300 456301 456302 456303 456304 456305 456306 456307 456308 456309 [...] (236491 flits)
Class 0:
Remaining flits: 495414 495415 495416 495417 495418 495419 495420 495421 495422 495423 [...] (189157 flits)
Measured flits: 495414 495415 495416 495417 495418 495419 495420 495421 495422 495423 [...] (189157 flits)
Class 0:
Remaining flits: 549317 549318 549319 549320 549321 549322 549323 566784 566785 566786 [...] (142144 flits)
Measured flits: 549317 549318 549319 549320 549321 549322 549323 566784 566785 566786 [...] (142144 flits)
Class 0:
Remaining flits: 566797 566798 566799 566800 566801 569574 569575 569576 569577 569578 [...] (94738 flits)
Measured flits: 566797 566798 566799 566800 566801 569574 569575 569576 569577 569578 [...] (94738 flits)
Class 0:
Remaining flits: 573593 573594 573595 573596 573597 573598 573599 573600 573601 573602 [...] (47291 flits)
Measured flits: 573593 573594 573595 573596 573597 573598 573599 573600 573601 573602 [...] (47291 flits)
Class 0:
Remaining flits: 690233 690234 690235 690236 690237 690238 690239 690240 690241 690242 [...] (8580 flits)
Measured flits: 690233 690234 690235 690236 690237 690238 690239 690240 690241 690242 [...] (8580 flits)
Class 0:
Remaining flits: 823904 823905 823906 823907 823908 823909 823910 823911 823912 823913 [...] (122 flits)
Measured flits: 823904 823905 823906 823907 823908 823909 823910 823911 823912 823913 [...] (122 flits)
Time taken is 19104 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8370.22 (1 samples)
	minimum = 22 (1 samples)
	maximum = 14068 (1 samples)
Network latency average = 8336.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13836 (1 samples)
Flit latency average = 6670.49 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13819 (1 samples)
Fragmentation average = 161.01 (1 samples)
	minimum = 0 (1 samples)
	maximum = 546 (1 samples)
Injected packet rate average = 0.0445851 (1 samples)
	minimum = 0.0343333 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.017092 (1 samples)
	minimum = 0.0113333 (1 samples)
	maximum = 0.0236667 (1 samples)
Injected flit rate average = 0.802568 (1 samples)
	minimum = 0.617333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.307507 (1 samples)
	minimum = 0.200667 (1 samples)
	maximum = 0.431667 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 17.9913 (1 samples)
Hops average = 5.07219 (1 samples)
Total run time 19.355
