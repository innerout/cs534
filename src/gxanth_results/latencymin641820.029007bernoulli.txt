BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 271.392
	minimum = 22
	maximum = 805
Network latency average = 263.15
	minimum = 22
	maximum = 796
Slowest packet = 894
Flit latency average = 230.033
	minimum = 5
	maximum = 796
Slowest flit = 17396
Fragmentation average = 62.3455
	minimum = 0
	maximum = 434
Injected packet rate average = 0.0285469
	minimum = 0.011 (at node 157)
	maximum = 0.042 (at node 81)
Accepted packet rate average = 0.0135365
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.50901
	minimum = 0.198 (at node 157)
	maximum = 0.75 (at node 81)
Accepted flit rate average= 0.259391
	minimum = 0.09 (at node 174)
	maximum = 0.414 (at node 76)
Injected packet length average = 17.8307
Accepted packet length average = 19.1624
Total in-flight flits = 48945 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.708
	minimum = 22
	maximum = 1531
Network latency average = 485.774
	minimum = 22
	maximum = 1528
Slowest packet = 2180
Flit latency average = 443.484
	minimum = 5
	maximum = 1542
Slowest flit = 44083
Fragmentation average = 79.6744
	minimum = 0
	maximum = 572
Injected packet rate average = 0.0271198
	minimum = 0.019 (at node 12)
	maximum = 0.0385 (at node 66)
Accepted packet rate average = 0.0145
	minimum = 0.0095 (at node 116)
	maximum = 0.0205 (at node 71)
Injected flit rate average = 0.485451
	minimum = 0.3335 (at node 20)
	maximum = 0.687 (at node 66)
Accepted flit rate average= 0.269562
	minimum = 0.171 (at node 116)
	maximum = 0.376 (at node 71)
Injected packet length average = 17.9002
Accepted packet length average = 18.5905
Total in-flight flits = 84372 (0 measured)
latency change    = 0.455809
throughput change = 0.0377348
Class 0:
Packet latency average = 1180.18
	minimum = 24
	maximum = 2274
Network latency average = 1132.29
	minimum = 23
	maximum = 2255
Slowest packet = 3877
Flit latency average = 1086.56
	minimum = 5
	maximum = 2244
Slowest flit = 73725
Fragmentation average = 107.246
	minimum = 0
	maximum = 725
Injected packet rate average = 0.0200521
	minimum = 0.002 (at node 144)
	maximum = 0.032 (at node 23)
Accepted packet rate average = 0.0150781
	minimum = 0.005 (at node 146)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.360589
	minimum = 0.036 (at node 144)
	maximum = 0.587 (at node 23)
Accepted flit rate average= 0.273109
	minimum = 0.095 (at node 146)
	maximum = 0.509 (at node 103)
Injected packet length average = 17.9826
Accepted packet length average = 18.113
Total in-flight flits = 101325 (0 measured)
latency change    = 0.577431
throughput change = 0.012987
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 629.721
	minimum = 25
	maximum = 2035
Network latency average = 70.309
	minimum = 22
	maximum = 988
Slowest packet = 14302
Flit latency average = 1490.38
	minimum = 5
	maximum = 2742
Slowest flit = 119945
Fragmentation average = 7.24034
	minimum = 0
	maximum = 35
Injected packet rate average = 0.015776
	minimum = 0.002 (at node 176)
	maximum = 0.029 (at node 98)
Accepted packet rate average = 0.0148542
	minimum = 0.006 (at node 79)
	maximum = 0.025 (at node 140)
Injected flit rate average = 0.284062
	minimum = 0.039 (at node 176)
	maximum = 0.525 (at node 98)
Accepted flit rate average= 0.267901
	minimum = 0.101 (at node 79)
	maximum = 0.459 (at node 140)
Injected packet length average = 18.0059
Accepted packet length average = 18.0354
Total in-flight flits = 104464 (50247 measured)
latency change    = 0.874134
throughput change = 0.0194413
Class 0:
Packet latency average = 1489.79
	minimum = 25
	maximum = 3341
Network latency average = 679.157
	minimum = 22
	maximum = 1969
Slowest packet = 14302
Flit latency average = 1630.17
	minimum = 5
	maximum = 4074
Slowest flit = 90281
Fragmentation average = 47.03
	minimum = 0
	maximum = 659
Injected packet rate average = 0.015526
	minimum = 0.0055 (at node 0)
	maximum = 0.024 (at node 1)
Accepted packet rate average = 0.0148307
	minimum = 0.0085 (at node 79)
	maximum = 0.021 (at node 178)
Injected flit rate average = 0.279383
	minimum = 0.104 (at node 0)
	maximum = 0.431 (at node 105)
Accepted flit rate average= 0.267104
	minimum = 0.168 (at node 79)
	maximum = 0.4045 (at node 0)
Injected packet length average = 17.9945
Accepted packet length average = 18.0102
Total in-flight flits = 106235 (90375 measured)
latency change    = 0.577309
throughput change = 0.00298339
Class 0:
Packet latency average = 2214.91
	minimum = 25
	maximum = 3842
Network latency average = 1398.93
	minimum = 22
	maximum = 2950
Slowest packet = 14302
Flit latency average = 1737.8
	minimum = 5
	maximum = 4114
Slowest flit = 90287
Fragmentation average = 84.3381
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151875
	minimum = 0.005 (at node 176)
	maximum = 0.022 (at node 103)
Accepted packet rate average = 0.0148316
	minimum = 0.00933333 (at node 79)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.273415
	minimum = 0.095 (at node 176)
	maximum = 0.393667 (at node 103)
Accepted flit rate average= 0.267132
	minimum = 0.168667 (at node 79)
	maximum = 0.36 (at node 128)
Injected packet length average = 18.0026
Accepted packet length average = 18.011
Total in-flight flits = 105155 (103551 measured)
latency change    = 0.327381
throughput change = 0.000103985
Class 0:
Packet latency average = 2640.95
	minimum = 25
	maximum = 5006
Network latency average = 1701.25
	minimum = 22
	maximum = 3952
Slowest packet = 14302
Flit latency average = 1796.1
	minimum = 5
	maximum = 4269
Slowest flit = 195443
Fragmentation average = 96.5675
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0153034
	minimum = 0.00825 (at node 176)
	maximum = 0.021 (at node 103)
Accepted packet rate average = 0.0149349
	minimum = 0.01075 (at node 35)
	maximum = 0.01975 (at node 118)
Injected flit rate average = 0.275392
	minimum = 0.14825 (at node 176)
	maximum = 0.37575 (at node 103)
Accepted flit rate average= 0.268173
	minimum = 0.1925 (at node 104)
	maximum = 0.35525 (at node 118)
Injected packet length average = 17.9955
Accepted packet length average = 17.9561
Total in-flight flits = 107138 (107120 measured)
latency change    = 0.16132
throughput change = 0.00388269
Class 0:
Packet latency average = 2966.9
	minimum = 25
	maximum = 5913
Network latency average = 1831.11
	minimum = 22
	maximum = 4902
Slowest packet = 14302
Flit latency average = 1837.58
	minimum = 5
	maximum = 4885
Slowest flit = 258497
Fragmentation average = 101.32
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151083
	minimum = 0.0088 (at node 148)
	maximum = 0.0208 (at node 103)
Accepted packet rate average = 0.0148917
	minimum = 0.0104 (at node 104)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.271781
	minimum = 0.1606 (at node 148)
	maximum = 0.3756 (at node 103)
Accepted flit rate average= 0.268068
	minimum = 0.1924 (at node 104)
	maximum = 0.341 (at node 118)
Injected packet length average = 17.9888
Accepted packet length average = 18.0012
Total in-flight flits = 105142 (105142 measured)
latency change    = 0.109863
throughput change = 0.000393441
Class 0:
Packet latency average = 3252.71
	minimum = 25
	maximum = 6293
Network latency average = 1899.47
	minimum = 22
	maximum = 5082
Slowest packet = 14302
Flit latency average = 1866.73
	minimum = 5
	maximum = 5065
Slowest flit = 293471
Fragmentation average = 104.296
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0151267
	minimum = 0.00933333 (at node 164)
	maximum = 0.0208333 (at node 103)
Accepted packet rate average = 0.0148828
	minimum = 0.0111667 (at node 79)
	maximum = 0.0188333 (at node 138)
Injected flit rate average = 0.272224
	minimum = 0.170167 (at node 164)
	maximum = 0.373833 (at node 103)
Accepted flit rate average= 0.268302
	minimum = 0.201333 (at node 79)
	maximum = 0.336 (at node 138)
Injected packet length average = 17.9962
Accepted packet length average = 18.0276
Total in-flight flits = 106089 (106089 measured)
latency change    = 0.0878683
throughput change = 0.000873549
Class 0:
Packet latency average = 3520.34
	minimum = 25
	maximum = 6839
Network latency average = 1947.13
	minimum = 22
	maximum = 5082
Slowest packet = 14302
Flit latency average = 1891.84
	minimum = 5
	maximum = 5065
Slowest flit = 293471
Fragmentation average = 107.067
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0150625
	minimum = 0.00928571 (at node 64)
	maximum = 0.0201429 (at node 103)
Accepted packet rate average = 0.0148862
	minimum = 0.0111429 (at node 79)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.271049
	minimum = 0.168286 (at node 64)
	maximum = 0.362429 (at node 103)
Accepted flit rate average= 0.26794
	minimum = 0.201714 (at node 79)
	maximum = 0.336714 (at node 70)
Injected packet length average = 17.995
Accepted packet length average = 17.9993
Total in-flight flits = 105551 (105551 measured)
latency change    = 0.0760229
throughput change = 0.00134958
Draining all recorded packets ...
Class 0:
Remaining flits: 410832 410833 410834 410835 410836 410837 410838 410839 410840 410841 [...] (107418 flits)
Measured flits: 410832 410833 410834 410835 410836 410837 410838 410839 410840 410841 [...] (107418 flits)
Class 0:
Remaining flits: 474192 474193 474194 474195 474196 474197 474198 474199 474200 474201 [...] (107182 flits)
Measured flits: 474192 474193 474194 474195 474196 474197 474198 474199 474200 474201 [...] (107182 flits)
Class 0:
Remaining flits: 474192 474193 474194 474195 474196 474197 474198 474199 474200 474201 [...] (106300 flits)
Measured flits: 474192 474193 474194 474195 474196 474197 474198 474199 474200 474201 [...] (106300 flits)
Class 0:
Remaining flits: 580797 580798 580799 580800 580801 580802 580803 580804 580805 581490 [...] (106946 flits)
Measured flits: 580797 580798 580799 580800 580801 580802 580803 580804 580805 581490 [...] (106586 flits)
Class 0:
Remaining flits: 634377 634378 634379 634380 634381 634382 634383 634384 634385 634386 [...] (108917 flits)
Measured flits: 634377 634378 634379 634380 634381 634382 634383 634384 634385 634386 [...] (104644 flits)
Class 0:
Remaining flits: 670408 670409 674370 674371 674372 674373 674374 674375 674376 674377 [...] (107087 flits)
Measured flits: 670408 670409 674370 674371 674372 674373 674374 674375 674376 674377 [...] (91593 flits)
Class 0:
Remaining flits: 691938 691939 691940 691941 691942 691943 691944 691945 691946 691947 [...] (105887 flits)
Measured flits: 691938 691939 691940 691941 691942 691943 691944 691945 691946 691947 [...] (69958 flits)
Class 0:
Remaining flits: 766727 781495 781496 781497 781498 781499 781500 781501 781502 781503 [...] (107127 flits)
Measured flits: 766727 781495 781496 781497 781498 781499 781500 781501 781502 781503 [...] (44531 flits)
Class 0:
Remaining flits: 801226 801227 801228 801229 801230 801231 801232 801233 827064 827065 [...] (106309 flits)
Measured flits: 801226 801227 801228 801229 801230 801231 801232 801233 827064 827065 [...] (25999 flits)
Class 0:
Remaining flits: 827075 827076 827077 827078 827079 827080 827081 883266 883267 883268 [...] (105039 flits)
Measured flits: 827075 827076 827077 827078 827079 827080 827081 883266 883267 883268 [...] (16774 flits)
Class 0:
Remaining flits: 890403 890404 890405 915418 915419 915420 915421 915422 915423 915424 [...] (105821 flits)
Measured flits: 890403 890404 890405 915418 915419 915420 915421 915422 915423 915424 [...] (13964 flits)
Class 0:
Remaining flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (104698 flits)
Measured flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (11375 flits)
Class 0:
Remaining flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (105828 flits)
Measured flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (7999 flits)
Class 0:
Remaining flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (105587 flits)
Measured flits: 932508 932509 932510 932511 932512 932513 932514 932515 932516 932517 [...] (4941 flits)
Class 0:
Remaining flits: 932519 932520 932521 932522 932523 932524 932525 1061046 1061047 1061048 [...] (105420 flits)
Measured flits: 932519 932520 932521 932522 932523 932524 932525 1061046 1061047 1061048 [...] (2277 flits)
Class 0:
Remaining flits: 1061046 1061047 1061048 1061049 1061050 1061051 1061052 1061053 1061054 1061055 [...] (104693 flits)
Measured flits: 1061046 1061047 1061048 1061049 1061050 1061051 1061052 1061053 1061054 1061055 [...] (1018 flits)
Class 0:
Remaining flits: 1061050 1061051 1061052 1061053 1061054 1061055 1061056 1061057 1061058 1061059 [...] (104485 flits)
Measured flits: 1061050 1061051 1061052 1061053 1061054 1061055 1061056 1061057 1061058 1061059 [...] (635 flits)
Class 0:
Remaining flits: 1178424 1178425 1178426 1178427 1178428 1178429 1178430 1178431 1178432 1178433 [...] (106540 flits)
Measured flits: 1430252 1430253 1430254 1430255 1430256 1430257 1430258 1430259 1430260 1430261 [...] (118 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1291017 1291018 1291019 1291020 1291021 1291022 1291023 1291024 1291025 1291026 [...] (57679 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1352268 1352269 1352270 1352271 1352272 1352273 1352274 1352275 1352276 1352277 [...] (14502 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1469091 1469092 1469093 1469094 1469095 1469096 1469097 1469098 1469099 1469100 [...] (404 flits)
Measured flits: (0 flits)
Time taken is 32264 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6244.91 (1 samples)
	minimum = 25 (1 samples)
	maximum = 19087 (1 samples)
Network latency average = 2073.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9049 (1 samples)
Flit latency average = 2011.23 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9025 (1 samples)
Fragmentation average = 127.718 (1 samples)
	minimum = 0 (1 samples)
	maximum = 726 (1 samples)
Injected packet rate average = 0.0150625 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0201429 (1 samples)
Accepted packet rate average = 0.0148862 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.271049 (1 samples)
	minimum = 0.168286 (1 samples)
	maximum = 0.362429 (1 samples)
Accepted flit rate average = 0.26794 (1 samples)
	minimum = 0.201714 (1 samples)
	maximum = 0.336714 (1 samples)
Injected packet size average = 17.995 (1 samples)
Accepted packet size average = 17.9993 (1 samples)
Hops average = 5.05518 (1 samples)
Total run time 61.7848
