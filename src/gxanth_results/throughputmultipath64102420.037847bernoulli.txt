BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.457
	minimum = 22
	maximum = 880
Network latency average = 283.823
	minimum = 22
	maximum = 833
Slowest packet = 913
Flit latency average = 253.461
	minimum = 5
	maximum = 848
Slowest flit = 15819
Fragmentation average = 69.21
	minimum = 0
	maximum = 649
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0145104
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.282552
	minimum = 0.145 (at node 135)
	maximum = 0.443 (at node 48)
Injected packet length average = 17.8477
Accepted packet length average = 19.4724
Total in-flight flits = 74576 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 560.064
	minimum = 22
	maximum = 1661
Network latency average = 545.015
	minimum = 22
	maximum = 1603
Slowest packet = 2147
Flit latency average = 502.767
	minimum = 5
	maximum = 1586
Slowest flit = 38447
Fragmentation average = 107.183
	minimum = 0
	maximum = 870
Injected packet rate average = 0.0376797
	minimum = 0.0285 (at node 81)
	maximum = 0.05 (at node 151)
Accepted packet rate average = 0.0153359
	minimum = 0.0095 (at node 135)
	maximum = 0.022 (at node 136)
Injected flit rate average = 0.675784
	minimum = 0.513 (at node 81)
	maximum = 0.897 (at node 151)
Accepted flit rate average= 0.290185
	minimum = 0.1735 (at node 153)
	maximum = 0.4175 (at node 136)
Injected packet length average = 17.935
Accepted packet length average = 18.9219
Total in-flight flits = 149011 (0 measured)
latency change    = 0.470673
throughput change = 0.0263033
Class 0:
Packet latency average = 1290.56
	minimum = 23
	maximum = 2407
Network latency average = 1273.1
	minimum = 22
	maximum = 2399
Slowest packet = 4218
Flit latency average = 1228.97
	minimum = 5
	maximum = 2399
Slowest flit = 75105
Fragmentation average = 193.994
	minimum = 0
	maximum = 999
Injected packet rate average = 0.0375521
	minimum = 0.022 (at node 163)
	maximum = 0.049 (at node 110)
Accepted packet rate average = 0.0159583
	minimum = 0.007 (at node 20)
	maximum = 0.026 (at node 92)
Injected flit rate average = 0.675307
	minimum = 0.397 (at node 163)
	maximum = 0.883 (at node 110)
Accepted flit rate average= 0.295151
	minimum = 0.148 (at node 154)
	maximum = 0.482 (at node 120)
Injected packet length average = 17.9832
Accepted packet length average = 18.4951
Total in-flight flits = 222122 (0 measured)
latency change    = 0.566032
throughput change = 0.0168258
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 55.086
	minimum = 24
	maximum = 199
Network latency average = 35.1197
	minimum = 22
	maximum = 101
Slowest packet = 21707
Flit latency average = 1710.83
	minimum = 5
	maximum = 3280
Slowest flit = 90780
Fragmentation average = 6.24115
	minimum = 0
	maximum = 28
Injected packet rate average = 0.0384792
	minimum = 0.023 (at node 16)
	maximum = 0.053 (at node 104)
Accepted packet rate average = 0.0162552
	minimum = 0.007 (at node 124)
	maximum = 0.029 (at node 42)
Injected flit rate average = 0.692391
	minimum = 0.414 (at node 77)
	maximum = 0.967 (at node 104)
Accepted flit rate average= 0.301349
	minimum = 0.114 (at node 4)
	maximum = 0.533 (at node 123)
Injected packet length average = 17.9939
Accepted packet length average = 18.5386
Total in-flight flits = 297247 (122200 measured)
latency change    = 22.4282
throughput change = 0.0205672
Class 0:
Packet latency average = 55.3429
	minimum = 23
	maximum = 293
Network latency average = 36.4315
	minimum = 22
	maximum = 271
Slowest packet = 28946
Flit latency average = 1946.62
	minimum = 5
	maximum = 4027
Slowest flit = 123040
Fragmentation average = 6.516
	minimum = 0
	maximum = 50
Injected packet rate average = 0.0380859
	minimum = 0.0255 (at node 176)
	maximum = 0.0485 (at node 38)
Accepted packet rate average = 0.0164297
	minimum = 0.01 (at node 141)
	maximum = 0.024 (at node 123)
Injected flit rate average = 0.685562
	minimum = 0.4625 (at node 176)
	maximum = 0.8805 (at node 106)
Accepted flit rate average= 0.301589
	minimum = 0.188 (at node 106)
	maximum = 0.448 (at node 123)
Injected packet length average = 18.0004
Accepted packet length average = 18.3563
Total in-flight flits = 369562 (241192 measured)
latency change    = 0.00464198
throughput change = 0.000794405
Class 0:
Packet latency average = 55.2798
	minimum = 23
	maximum = 410
Network latency average = 37.2743
	minimum = 22
	maximum = 399
Slowest packet = 37841
Flit latency average = 2191.47
	minimum = 5
	maximum = 4855
Slowest flit = 135053
Fragmentation average = 6.4531
	minimum = 0
	maximum = 50
Injected packet rate average = 0.0379688
	minimum = 0.0276667 (at node 95)
	maximum = 0.0466667 (at node 120)
Accepted packet rate average = 0.0163872
	minimum = 0.0116667 (at node 4)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.683458
	minimum = 0.498 (at node 95)
	maximum = 0.844333 (at node 120)
Accepted flit rate average= 0.300943
	minimum = 0.204667 (at node 4)
	maximum = 0.409 (at node 123)
Injected packet length average = 18.0005
Accepted packet length average = 18.3646
Total in-flight flits = 442439 (360759 measured)
latency change    = 0.00114229
throughput change = 0.00214603
Draining remaining packets ...
Class 0:
Remaining flits: 131435 143498 143499 143500 143501 143502 143503 143504 143505 143506 [...] (395029 flits)
Measured flits: 390240 390241 390242 390243 390244 390245 390246 390247 390248 390249 [...] (352673 flits)
Class 0:
Remaining flits: 170850 170851 170852 170853 170854 170855 173228 173229 173230 173231 [...] (348110 flits)
Measured flits: 390241 390242 390243 390244 390245 390246 390247 390248 390249 390250 [...] (328728 flits)
Class 0:
Remaining flits: 218337 218338 218339 222984 222985 222986 222987 222988 222989 222990 [...] (300893 flits)
Measured flits: 390348 390349 390350 390351 390352 390353 390354 390355 390356 390357 [...] (293635 flits)
Class 0:
Remaining flits: 263757 263758 263759 263760 263761 263762 263763 263764 263765 263766 [...] (254069 flits)
Measured flits: 390358 390359 390360 390361 390362 390363 390364 390365 390424 390425 [...] (251904 flits)
Class 0:
Remaining flits: 276957 276958 276959 276960 276961 276962 276963 276964 276965 288359 [...] (206775 flits)
Measured flits: 391932 391933 391934 391935 391936 391937 391938 391939 391940 391941 [...] (206301 flits)
Class 0:
Remaining flits: 338054 338055 338056 338057 343404 343405 343406 343407 343408 343409 [...] (159403 flits)
Measured flits: 392287 392288 392289 392290 392291 392703 392704 392705 396432 396433 [...] (159329 flits)
Class 0:
Remaining flits: 348210 348211 348212 348213 348214 348215 348216 348217 348218 348219 [...] (112428 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (112410 flits)
Class 0:
Remaining flits: 465012 465013 465014 465015 465016 465017 465018 465019 465020 465021 [...] (65390 flits)
Measured flits: 465012 465013 465014 465015 465016 465017 465018 465019 465020 465021 [...] (65390 flits)
Class 0:
Remaining flits: 495392 495393 495394 495395 540225 540226 540227 540228 540229 540230 [...] (21143 flits)
Measured flits: 495392 495393 495394 495395 540225 540226 540227 540228 540229 540230 [...] (21143 flits)
Class 0:
Remaining flits: 576305 607860 607861 607862 607863 607864 607865 607866 607867 607868 [...] (1708 flits)
Measured flits: 576305 607860 607861 607862 607863 607864 607865 607866 607867 607868 [...] (1708 flits)
Time taken is 16716 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6611.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11769 (1 samples)
Network latency average = 6593.09 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11742 (1 samples)
Flit latency average = 5332.93 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11725 (1 samples)
Fragmentation average = 368.416 (1 samples)
	minimum = 0 (1 samples)
	maximum = 945 (1 samples)
Injected packet rate average = 0.0379688 (1 samples)
	minimum = 0.0276667 (1 samples)
	maximum = 0.0466667 (1 samples)
Accepted packet rate average = 0.0163872 (1 samples)
	minimum = 0.0116667 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.683458 (1 samples)
	minimum = 0.498 (1 samples)
	maximum = 0.844333 (1 samples)
Accepted flit rate average = 0.300943 (1 samples)
	minimum = 0.204667 (1 samples)
	maximum = 0.409 (1 samples)
Injected packet size average = 18.0005 (1 samples)
Accepted packet size average = 18.3646 (1 samples)
Hops average = 5.06164 (1 samples)
Total run time 20.6081
