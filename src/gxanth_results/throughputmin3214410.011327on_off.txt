BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 235.266
	minimum = 27
	maximum = 962
Network latency average = 190.925
	minimum = 23
	maximum = 900
Slowest packet = 168
Flit latency average = 125.621
	minimum = 6
	maximum = 883
Slowest flit = 5147
Fragmentation average = 107.401
	minimum = 0
	maximum = 706
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00814063
	minimum = 0.002 (at node 41)
	maximum = 0.015 (at node 70)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.162182
	minimum = 0.036 (at node 174)
	maximum = 0.3 (at node 70)
Injected packet length average = 17.8411
Accepted packet length average = 19.9226
Total in-flight flits = 9991 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 322.133
	minimum = 27
	maximum = 1787
Network latency average = 273.109
	minimum = 23
	maximum = 1703
Slowest packet = 672
Flit latency average = 194.743
	minimum = 6
	maximum = 1786
Slowest flit = 5986
Fragmentation average = 138.944
	minimum = 0
	maximum = 1434
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00895313
	minimum = 0.004 (at node 115)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.170805
	minimum = 0.0785 (at node 115)
	maximum = 0.2805 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 19.0777
Total in-flight flits = 14007 (0 measured)
latency change    = 0.269663
throughput change = 0.050481
Class 0:
Packet latency average = 486.991
	minimum = 27
	maximum = 2356
Network latency average = 439.116
	minimum = 25
	maximum = 2262
Slowest packet = 332
Flit latency average = 335.004
	minimum = 6
	maximum = 2587
Slowest flit = 16540
Fragmentation average = 185.142
	minimum = 0
	maximum = 2065
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0105937
	minimum = 0.003 (at node 26)
	maximum = 0.02 (at node 177)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.191062
	minimum = 0.072 (at node 26)
	maximum = 0.327 (at node 68)
Injected packet length average = 17.9936
Accepted packet length average = 18.0354
Total in-flight flits = 16419 (0 measured)
latency change    = 0.338525
throughput change = 0.106027
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 350.374
	minimum = 23
	maximum = 1126
Network latency average = 303.675
	minimum = 23
	maximum = 963
Slowest packet = 6604
Flit latency average = 396.571
	minimum = 6
	maximum = 3255
Slowest flit = 24569
Fragmentation average = 150.042
	minimum = 0
	maximum = 668
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.0105625
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 182)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.192135
	minimum = 0.066 (at node 121)
	maximum = 0.332 (at node 182)
Injected packet length average = 18.0247
Accepted packet length average = 18.1903
Total in-flight flits = 20281 (17020 measured)
latency change    = 0.389918
throughput change = 0.00558417
Class 0:
Packet latency average = 464.569
	minimum = 23
	maximum = 1978
Network latency average = 415.231
	minimum = 23
	maximum = 1930
Slowest packet = 6604
Flit latency average = 445.645
	minimum = 6
	maximum = 3704
Slowest flit = 43073
Fragmentation average = 169.795
	minimum = 0
	maximum = 1447
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.0105937
	minimum = 0.0045 (at node 17)
	maximum = 0.018 (at node 182)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.192073
	minimum = 0.0765 (at node 17)
	maximum = 0.3175 (at node 182)
Injected packet length average = 17.9755
Accepted packet length average = 18.1308
Total in-flight flits = 22781 (21857 measured)
latency change    = 0.245808
throughput change = 0.000325397
Class 0:
Packet latency average = 551.774
	minimum = 23
	maximum = 3109
Network latency average = 500.73
	minimum = 23
	maximum = 2937
Slowest packet = 6645
Flit latency average = 485.187
	minimum = 6
	maximum = 4316
Slowest flit = 62423
Fragmentation average = 184.292
	minimum = 0
	maximum = 1862
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0106441
	minimum = 0.00566667 (at node 4)
	maximum = 0.017 (at node 129)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.192028
	minimum = 0.100667 (at node 4)
	maximum = 0.288 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 18.0408
Total in-flight flits = 22721 (22428 measured)
latency change    = 0.158046
throughput change = 0.000235064
Draining remaining packets ...
Class 0:
Remaining flits: 73818 73819 73820 73821 73822 73823 73824 73825 73826 73827 [...] (2385 flits)
Measured flits: 121167 121168 121169 121170 121171 121172 121173 121174 121175 128098 [...] (2314 flits)
Class 0:
Remaining flits: 221473 221474 221475 221476 221477 221478 221479 221480 221481 221482 [...] (33 flits)
Measured flits: 221473 221474 221475 221476 221477 221478 221479 221480 221481 221482 [...] (33 flits)
Time taken is 8033 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 712.266 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4601 (1 samples)
Network latency average = 657.371 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4492 (1 samples)
Flit latency average = 605.392 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5418 (1 samples)
Fragmentation average = 189.678 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1862 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0106441 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.192028 (1 samples)
	minimum = 0.100667 (1 samples)
	maximum = 0.288 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 18.0408 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 5.09965
