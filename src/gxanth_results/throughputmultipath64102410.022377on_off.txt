BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 316.582
	minimum = 25
	maximum = 978
Network latency average = 250.366
	minimum = 25
	maximum = 965
Slowest packet = 3
Flit latency average = 180.753
	minimum = 6
	maximum = 948
Slowest flit = 71
Fragmentation average = 144.568
	minimum = 0
	maximum = 903
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00932812
	minimum = 0.002 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.19599
	minimum = 0.073 (at node 174)
	maximum = 0.324 (at node 22)
Injected packet length average = 17.8417
Accepted packet length average = 21.0106
Total in-flight flits = 35144 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 512.045
	minimum = 23
	maximum = 1899
Network latency average = 418.235
	minimum = 23
	maximum = 1782
Slowest packet = 209
Flit latency average = 327.553
	minimum = 6
	maximum = 1886
Slowest flit = 8408
Fragmentation average = 204.382
	minimum = 0
	maximum = 1567
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.010849
	minimum = 0.0045 (at node 116)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.213648
	minimum = 0.118 (at node 116)
	maximum = 0.328 (at node 70)
Injected packet length average = 17.9252
Accepted packet length average = 19.693
Total in-flight flits = 68367 (0 measured)
latency change    = 0.38173
throughput change = 0.0826538
Class 0:
Packet latency average = 1001.19
	minimum = 23
	maximum = 2980
Network latency average = 867.187
	minimum = 23
	maximum = 2936
Slowest packet = 499
Flit latency average = 770.417
	minimum = 6
	maximum = 2919
Slowest flit = 5237
Fragmentation average = 300.876
	minimum = 0
	maximum = 2339
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0127969
	minimum = 0.005 (at node 40)
	maximum = 0.024 (at node 73)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.235714
	minimum = 0.12 (at node 72)
	maximum = 0.425 (at node 16)
Injected packet length average = 18
Accepted packet length average = 18.4196
Total in-flight flits = 101014 (0 measured)
latency change    = 0.488563
throughput change = 0.0936098
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 359.693
	minimum = 25
	maximum = 1217
Network latency average = 247.571
	minimum = 23
	maximum = 966
Slowest packet = 12702
Flit latency average = 1102.44
	minimum = 6
	maximum = 3794
Slowest flit = 13908
Fragmentation average = 126.949
	minimum = 0
	maximum = 687
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0129792
	minimum = 0.004 (at node 36)
	maximum = 0.027 (at node 95)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.236578
	minimum = 0.081 (at node 36)
	maximum = 0.462 (at node 95)
Injected packet length average = 17.9946
Accepted packet length average = 18.2275
Total in-flight flits = 131857 (63538 measured)
latency change    = 1.78345
throughput change = 0.00365454
Class 0:
Packet latency average = 657.045
	minimum = 25
	maximum = 2287
Network latency average = 550.852
	minimum = 23
	maximum = 1946
Slowest packet = 12702
Flit latency average = 1265.56
	minimum = 6
	maximum = 4842
Slowest flit = 11215
Fragmentation average = 208.733
	minimum = 0
	maximum = 1491
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0129531
	minimum = 0.006 (at node 79)
	maximum = 0.021 (at node 95)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.237258
	minimum = 0.1145 (at node 31)
	maximum = 0.39 (at node 95)
Injected packet length average = 17.9936
Accepted packet length average = 18.3166
Total in-flight flits = 162817 (115152 measured)
latency change    = 0.452559
throughput change = 0.00286476
Class 0:
Packet latency average = 959.769
	minimum = 25
	maximum = 3273
Network latency average = 846.405
	minimum = 23
	maximum = 2906
Slowest packet = 12702
Flit latency average = 1461.76
	minimum = 6
	maximum = 5908
Slowest flit = 6366
Fragmentation average = 243.109
	minimum = 0
	maximum = 1942
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0129566
	minimum = 0.00766667 (at node 79)
	maximum = 0.02 (at node 95)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.236726
	minimum = 0.134667 (at node 146)
	maximum = 0.360333 (at node 95)
Injected packet length average = 18.0055
Accepted packet length average = 18.2707
Total in-flight flits = 191712 (158910 measured)
latency change    = 0.315414
throughput change = 0.00224783
Draining remaining packets ...
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (151722 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (128982 flits)
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (113244 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (97860 flits)
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (76774 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (66819 flits)
Class 0:
Remaining flits: 14634 14635 14636 14637 14638 14639 14640 14641 14642 14643 [...] (44787 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (38403 flits)
Class 0:
Remaining flits: 37056 37057 37058 37059 37060 37061 40014 40015 40016 40017 [...] (19415 flits)
Measured flits: 228312 228313 228314 228315 228316 228317 228318 228319 228320 228321 [...] (16054 flits)
Class 0:
Remaining flits: 56791 56792 56793 56794 56795 56796 56797 56798 56799 56800 [...] (3876 flits)
Measured flits: 228726 228727 228728 228729 228730 228731 228732 228733 228734 228735 [...] (3267 flits)
Time taken is 12936 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3346.33 (1 samples)
	minimum = 25 (1 samples)
	maximum = 9862 (1 samples)
Network latency average = 3202.98 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9506 (1 samples)
Flit latency average = 3151.25 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11691 (1 samples)
Fragmentation average = 306.249 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7186 (1 samples)
Injected packet rate average = 0.0218993 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0129566 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.394309 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.926 (1 samples)
Accepted flit rate average = 0.236726 (1 samples)
	minimum = 0.134667 (1 samples)
	maximum = 0.360333 (1 samples)
Injected packet size average = 18.0055 (1 samples)
Accepted packet size average = 18.2707 (1 samples)
Hops average = 5.08776 (1 samples)
Total run time 16.0854
