BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 359.636
	minimum = 25
	maximum = 901
Network latency average = 254.032
	minimum = 22
	maximum = 857
Slowest packet = 580
Flit latency average = 223.54
	minimum = 5
	maximum = 840
Slowest flit = 16163
Fragmentation average = 22.9639
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0187396
	minimum = 0.009 (at node 123)
	maximum = 0.031 (at node 99)
Accepted packet rate average = 0.0135781
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.333219
	minimum = 0.147 (at node 188)
	maximum = 0.558 (at node 99)
Accepted flit rate average= 0.249464
	minimum = 0.09 (at node 174)
	maximum = 0.445 (at node 44)
Injected packet length average = 17.7815
Accepted packet length average = 18.3725
Total in-flight flits = 18721 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 709.66
	minimum = 25
	maximum = 1748
Network latency average = 309.334
	minimum = 22
	maximum = 1521
Slowest packet = 882
Flit latency average = 275.764
	minimum = 5
	maximum = 1503
Slowest flit = 39635
Fragmentation average = 22.2994
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0161484
	minimum = 0.008 (at node 55)
	maximum = 0.025 (at node 66)
Accepted packet rate average = 0.0136536
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 22)
Injected flit rate average = 0.288771
	minimum = 0.144 (at node 55)
	maximum = 0.444 (at node 66)
Accepted flit rate average= 0.248544
	minimum = 0.126 (at node 153)
	maximum = 0.36 (at node 22)
Injected packet length average = 17.8823
Accepted packet length average = 18.2035
Total in-flight flits = 18139 (0 measured)
latency change    = 0.493228
throughput change = 0.00369862
Class 0:
Packet latency average = 1768.09
	minimum = 1001
	maximum = 2621
Network latency average = 360.29
	minimum = 22
	maximum = 1696
Slowest packet = 5849
Flit latency average = 324.818
	minimum = 5
	maximum = 1667
Slowest flit = 38015
Fragmentation average = 22.7826
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0142865
	minimum = 0.004 (at node 85)
	maximum = 0.026 (at node 118)
Accepted packet rate average = 0.0142292
	minimum = 0.005 (at node 91)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.257125
	minimum = 0.072 (at node 85)
	maximum = 0.469 (at node 118)
Accepted flit rate average= 0.255896
	minimum = 0.098 (at node 91)
	maximum = 0.438 (at node 16)
Injected packet length average = 17.9978
Accepted packet length average = 17.9839
Total in-flight flits = 18291 (0 measured)
latency change    = 0.598629
throughput change = 0.0287287
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2556.01
	minimum = 1644
	maximum = 3416
Network latency average = 284.145
	minimum = 22
	maximum = 961
Slowest packet = 9072
Flit latency average = 320.348
	minimum = 5
	maximum = 1321
Slowest flit = 124116
Fragmentation average = 21.8644
	minimum = 0
	maximum = 98
Injected packet rate average = 0.014599
	minimum = 0.002 (at node 16)
	maximum = 0.031 (at node 151)
Accepted packet rate average = 0.0144167
	minimum = 0.006 (at node 48)
	maximum = 0.025 (at node 19)
Injected flit rate average = 0.262464
	minimum = 0.036 (at node 16)
	maximum = 0.567 (at node 151)
Accepted flit rate average= 0.259854
	minimum = 0.108 (at node 48)
	maximum = 0.45 (at node 19)
Injected packet length average = 17.9782
Accepted packet length average = 18.0246
Total in-flight flits = 18871 (18743 measured)
latency change    = 0.308261
throughput change = 0.0152329
Class 0:
Packet latency average = 2939.9
	minimum = 1644
	maximum = 4115
Network latency average = 328.052
	minimum = 22
	maximum = 1702
Slowest packet = 9072
Flit latency average = 321.078
	minimum = 5
	maximum = 1658
Slowest flit = 173681
Fragmentation average = 22.5199
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0141536
	minimum = 0.0055 (at node 153)
	maximum = 0.024 (at node 87)
Accepted packet rate average = 0.0142005
	minimum = 0.0095 (at node 36)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.254958
	minimum = 0.0965 (at node 171)
	maximum = 0.438 (at node 87)
Accepted flit rate average= 0.255453
	minimum = 0.1655 (at node 36)
	maximum = 0.416 (at node 129)
Injected packet length average = 18.0136
Accepted packet length average = 17.989
Total in-flight flits = 18243 (18243 measured)
latency change    = 0.13058
throughput change = 0.0172284
Class 0:
Packet latency average = 3296.22
	minimum = 1644
	maximum = 5020
Network latency average = 340.627
	minimum = 22
	maximum = 1778
Slowest packet = 9072
Flit latency average = 322.894
	minimum = 5
	maximum = 1761
Slowest flit = 175842
Fragmentation average = 22.0757
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0140313
	minimum = 0.00566667 (at node 57)
	maximum = 0.0233333 (at node 72)
Accepted packet rate average = 0.0140694
	minimum = 0.00866667 (at node 36)
	maximum = 0.0203333 (at node 129)
Injected flit rate average = 0.252661
	minimum = 0.102 (at node 57)
	maximum = 0.423 (at node 72)
Accepted flit rate average= 0.253125
	minimum = 0.156 (at node 36)
	maximum = 0.361333 (at node 129)
Injected packet length average = 18.0071
Accepted packet length average = 17.9911
Total in-flight flits = 18219 (18219 measured)
latency change    = 0.108101
throughput change = 0.00919753
Draining remaining packets ...
Time taken is 6783 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3452.42 (1 samples)
	minimum = 1644 (1 samples)
	maximum = 5554 (1 samples)
Network latency average = 356.877 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1778 (1 samples)
Flit latency average = 335.052 (1 samples)
	minimum = 5 (1 samples)
	maximum = 1761 (1 samples)
Fragmentation average = 21.7557 (1 samples)
	minimum = 0 (1 samples)
	maximum = 149 (1 samples)
Injected packet rate average = 0.0140313 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0233333 (1 samples)
Accepted packet rate average = 0.0140694 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.252661 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.423 (1 samples)
Accepted flit rate average = 0.253125 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 0.361333 (1 samples)
Injected packet size average = 18.0071 (1 samples)
Accepted packet size average = 17.9911 (1 samples)
Hops average = 5.04879 (1 samples)
Total run time 5.82862
