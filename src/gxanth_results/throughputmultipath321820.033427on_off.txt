BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 336.19
	minimum = 24
	maximum = 974
Network latency average = 230.064
	minimum = 22
	maximum = 772
Slowest packet = 57
Flit latency average = 197.063
	minimum = 5
	maximum = 793
Slowest flit = 16583
Fragmentation average = 46.5459
	minimum = 0
	maximum = 314
Injected packet rate average = 0.0239115
	minimum = 0 (at node 30)
	maximum = 0.051 (at node 162)
Accepted packet rate average = 0.0135
	minimum = 0.005 (at node 41)
	maximum = 0.022 (at node 76)
Injected flit rate average = 0.425854
	minimum = 0 (at node 30)
	maximum = 0.912 (at node 162)
Accepted flit rate average= 0.25401
	minimum = 0.105 (at node 41)
	maximum = 0.409 (at node 44)
Injected packet length average = 17.8096
Accepted packet length average = 18.8156
Total in-flight flits = 34282 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 591.057
	minimum = 24
	maximum = 1913
Network latency average = 410.891
	minimum = 22
	maximum = 1467
Slowest packet = 57
Flit latency average = 371.97
	minimum = 5
	maximum = 1524
Slowest flit = 37364
Fragmentation average = 54.7044
	minimum = 0
	maximum = 314
Injected packet rate average = 0.020901
	minimum = 0.0015 (at node 33)
	maximum = 0.034 (at node 62)
Accepted packet rate average = 0.0142109
	minimum = 0.0085 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.373737
	minimum = 0.027 (at node 33)
	maximum = 0.609 (at node 62)
Accepted flit rate average= 0.261242
	minimum = 0.1585 (at node 153)
	maximum = 0.3915 (at node 152)
Injected packet length average = 17.8813
Accepted packet length average = 18.3832
Total in-flight flits = 44781 (0 measured)
latency change    = 0.431206
throughput change = 0.0276822
Class 0:
Packet latency average = 1337.1
	minimum = 25
	maximum = 2794
Network latency average = 828.681
	minimum = 22
	maximum = 2256
Slowest packet = 6103
Flit latency average = 777.917
	minimum = 5
	maximum = 2239
Slowest flit = 47339
Fragmentation average = 60.6326
	minimum = 0
	maximum = 287
Injected packet rate average = 0.0154896
	minimum = 0.001 (at node 54)
	maximum = 0.032 (at node 22)
Accepted packet rate average = 0.0146458
	minimum = 0.005 (at node 70)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.278906
	minimum = 0.018 (at node 54)
	maximum = 0.593 (at node 22)
Accepted flit rate average= 0.264255
	minimum = 0.101 (at node 70)
	maximum = 0.457 (at node 34)
Injected packet length average = 18.0061
Accepted packet length average = 18.043
Total in-flight flits = 47936 (0 measured)
latency change    = 0.557957
throughput change = 0.0114019
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1499.17
	minimum = 30
	maximum = 3531
Network latency average = 380.757
	minimum = 22
	maximum = 974
Slowest packet = 11072
Flit latency average = 852.843
	minimum = 5
	maximum = 2885
Slowest flit = 58985
Fragmentation average = 32.6386
	minimum = 0
	maximum = 217
Injected packet rate average = 0.0156458
	minimum = 0 (at node 12)
	maximum = 0.032 (at node 138)
Accepted packet rate average = 0.0150417
	minimum = 0.008 (at node 5)
	maximum = 0.027 (at node 3)
Injected flit rate average = 0.28149
	minimum = 0 (at node 12)
	maximum = 0.576 (at node 138)
Accepted flit rate average= 0.271057
	minimum = 0.139 (at node 48)
	maximum = 0.477 (at node 3)
Injected packet length average = 17.9913
Accepted packet length average = 18.0204
Total in-flight flits = 50109 (41934 measured)
latency change    = 0.108104
throughput change = 0.0250946
Class 0:
Packet latency average = 2051.04
	minimum = 30
	maximum = 4420
Network latency average = 739.98
	minimum = 22
	maximum = 1899
Slowest packet = 11072
Flit latency average = 879.016
	minimum = 5
	maximum = 3486
Slowest flit = 95237
Fragmentation average = 56.1423
	minimum = 0
	maximum = 287
Injected packet rate average = 0.0155495
	minimum = 0.0065 (at node 66)
	maximum = 0.0255 (at node 138)
Accepted packet rate average = 0.0149688
	minimum = 0.009 (at node 86)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.279518
	minimum = 0.117 (at node 76)
	maximum = 0.456 (at node 138)
Accepted flit rate average= 0.269706
	minimum = 0.162 (at node 86)
	maximum = 0.3885 (at node 152)
Injected packet length average = 17.9761
Accepted packet length average = 18.0179
Total in-flight flits = 51685 (51250 measured)
latency change    = 0.26907
throughput change = 0.00501125
Class 0:
Packet latency average = 2415.08
	minimum = 30
	maximum = 5304
Network latency average = 870.226
	minimum = 22
	maximum = 2772
Slowest packet = 11072
Flit latency average = 905.897
	minimum = 5
	maximum = 3659
Slowest flit = 127447
Fragmentation average = 64.3337
	minimum = 0
	maximum = 306
Injected packet rate average = 0.015349
	minimum = 0.00433333 (at node 76)
	maximum = 0.0223333 (at node 138)
Accepted packet rate average = 0.0149444
	minimum = 0.0103333 (at node 35)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.275943
	minimum = 0.078 (at node 76)
	maximum = 0.400667 (at node 138)
Accepted flit rate average= 0.26938
	minimum = 0.186667 (at node 64)
	maximum = 0.365667 (at node 129)
Injected packet length average = 17.9779
Accepted packet length average = 18.0254
Total in-flight flits = 51857 (51857 measured)
latency change    = 0.150736
throughput change = 0.00120841
Draining remaining packets ...
Class 0:
Remaining flits: 250992 250993 250994 250995 250996 250997 250998 250999 251000 251001 [...] (6505 flits)
Measured flits: 250992 250993 250994 250995 250996 250997 250998 250999 251000 251001 [...] (6505 flits)
Time taken is 7738 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2763.68 (1 samples)
	minimum = 30 (1 samples)
	maximum = 6604 (1 samples)
Network latency average = 991.056 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3236 (1 samples)
Flit latency average = 967.91 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3659 (1 samples)
Fragmentation average = 68.6483 (1 samples)
	minimum = 0 (1 samples)
	maximum = 306 (1 samples)
Injected packet rate average = 0.015349 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0149444 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.275943 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.400667 (1 samples)
Accepted flit rate average = 0.26938 (1 samples)
	minimum = 0.186667 (1 samples)
	maximum = 0.365667 (1 samples)
Injected packet size average = 17.9779 (1 samples)
Accepted packet size average = 18.0254 (1 samples)
Hops average = 5.09753 (1 samples)
Total run time 8.8359
