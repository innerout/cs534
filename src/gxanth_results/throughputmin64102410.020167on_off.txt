BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 322.353
	minimum = 23
	maximum = 976
Network latency average = 252.194
	minimum = 23
	maximum = 879
Slowest packet = 14
Flit latency average = 177.884
	minimum = 6
	maximum = 980
Slowest flit = 67
Fragmentation average = 146.788
	minimum = 0
	maximum = 749
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.00926042
	minimum = 0.003 (at node 43)
	maximum = 0.017 (at node 103)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.193062
	minimum = 0.07 (at node 43)
	maximum = 0.337 (at node 103)
Injected packet length average = 17.8591
Accepted packet length average = 20.8481
Total in-flight flits = 30270 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.927
	minimum = 23
	maximum = 1951
Network latency average = 411.072
	minimum = 23
	maximum = 1902
Slowest packet = 247
Flit latency average = 317.917
	minimum = 6
	maximum = 1914
Slowest flit = 5622
Fragmentation average = 198.924
	minimum = 0
	maximum = 1709
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0105781
	minimum = 0.006 (at node 30)
	maximum = 0.0165 (at node 166)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.207984
	minimum = 0.1175 (at node 116)
	maximum = 0.3245 (at node 166)
Injected packet length average = 17.9339
Accepted packet length average = 19.6617
Total in-flight flits = 54576 (0 measured)
latency change    = 0.353907
throughput change = 0.0717452
Class 0:
Packet latency average = 911.842
	minimum = 25
	maximum = 2930
Network latency average = 798.719
	minimum = 25
	maximum = 2879
Slowest packet = 238
Flit latency average = 690.901
	minimum = 6
	maximum = 2902
Slowest flit = 5626
Fragmentation average = 285.163
	minimum = 0
	maximum = 2742
Injected packet rate average = 0.0207448
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0128542
	minimum = 0.002 (at node 153)
	maximum = 0.024 (at node 55)
Injected flit rate average = 0.372271
	minimum = 0 (at node 7)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.237708
	minimum = 0.067 (at node 153)
	maximum = 0.444 (at node 78)
Injected packet length average = 17.9453
Accepted packet length average = 18.4927
Total in-flight flits = 80630 (0 measured)
latency change    = 0.452836
throughput change = 0.125044
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 365.975
	minimum = 28
	maximum = 1226
Network latency average = 277.525
	minimum = 23
	maximum = 979
Slowest packet = 11465
Flit latency average = 987.71
	minimum = 6
	maximum = 3874
Slowest flit = 7612
Fragmentation average = 146.143
	minimum = 0
	maximum = 656
Injected packet rate average = 0.020276
	minimum = 0 (at node 77)
	maximum = 0.055 (at node 34)
Accepted packet rate average = 0.0128438
	minimum = 0.005 (at node 70)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.36649
	minimum = 0 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.23438
	minimum = 0.102 (at node 79)
	maximum = 0.417 (at node 44)
Injected packet length average = 18.075
Accepted packet length average = 18.2486
Total in-flight flits = 105703 (55512 measured)
latency change    = 1.49154
throughput change = 0.0141997
Class 0:
Packet latency average = 669.42
	minimum = 28
	maximum = 2244
Network latency average = 577.471
	minimum = 23
	maximum = 1966
Slowest packet = 11465
Flit latency average = 1137.7
	minimum = 6
	maximum = 4909
Slowest flit = 6185
Fragmentation average = 213.781
	minimum = 0
	maximum = 1280
Injected packet rate average = 0.0198438
	minimum = 0.001 (at node 191)
	maximum = 0.051 (at node 11)
Accepted packet rate average = 0.0129714
	minimum = 0.0065 (at node 75)
	maximum = 0.0195 (at node 44)
Injected flit rate average = 0.357513
	minimum = 0.018 (at node 191)
	maximum = 0.918 (at node 11)
Accepted flit rate average= 0.236544
	minimum = 0.131 (at node 75)
	maximum = 0.3585 (at node 66)
Injected packet length average = 18.0164
Accepted packet length average = 18.2359
Total in-flight flits = 126957 (94388 measured)
latency change    = 0.453295
throughput change = 0.00914866
Class 0:
Packet latency average = 929.157
	minimum = 28
	maximum = 3351
Network latency average = 831.964
	minimum = 23
	maximum = 2960
Slowest packet = 11465
Flit latency average = 1291.49
	minimum = 6
	maximum = 5752
Slowest flit = 14892
Fragmentation average = 250.761
	minimum = 0
	maximum = 1714
Injected packet rate average = 0.0199097
	minimum = 0.004 (at node 131)
	maximum = 0.042 (at node 55)
Accepted packet rate average = 0.0130017
	minimum = 0.00866667 (at node 63)
	maximum = 0.018 (at node 66)
Injected flit rate average = 0.358792
	minimum = 0.072 (at node 131)
	maximum = 0.756 (at node 55)
Accepted flit rate average= 0.236536
	minimum = 0.160667 (at node 162)
	maximum = 0.317667 (at node 66)
Injected packet length average = 18.0209
Accepted packet length average = 18.1927
Total in-flight flits = 150809 (129639 measured)
latency change    = 0.279541
throughput change = 3.30287e-05
Draining remaining packets ...
Class 0:
Remaining flits: 5472 5473 5474 5475 5476 5477 5478 5479 5480 5481 [...] (111755 flits)
Measured flits: 206159 206160 206161 206162 206163 206164 206165 206166 206167 206168 [...] (97835 flits)
Class 0:
Remaining flits: 5472 5473 5474 5475 5476 5477 5478 5479 5480 5481 [...] (74410 flits)
Measured flits: 206425 206426 206427 206428 206429 206430 206431 206432 206433 206434 [...] (65074 flits)
Class 0:
Remaining flits: 5472 5473 5474 5475 5476 5477 5478 5479 5480 5481 [...] (41475 flits)
Measured flits: 206425 206426 206427 206428 206429 206430 206431 206432 206433 206434 [...] (35788 flits)
Class 0:
Remaining flits: 28494 28495 28496 28497 28498 28499 28500 28501 28502 28503 [...] (16745 flits)
Measured flits: 206426 206427 206428 206429 206430 206431 206432 206433 206434 206435 [...] (14407 flits)
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (3875 flits)
Measured flits: 208134 208135 208136 208137 208138 208139 208140 208141 208142 208143 [...] (3159 flits)
Time taken is 11981 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2687.38 (1 samples)
	minimum = 28 (1 samples)
	maximum = 8889 (1 samples)
Network latency average = 2565.8 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8509 (1 samples)
Flit latency average = 2562.17 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10504 (1 samples)
Fragmentation average = 306.432 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5939 (1 samples)
Injected packet rate average = 0.0199097 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.042 (1 samples)
Accepted packet rate average = 0.0130017 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.358792 (1 samples)
	minimum = 0.072 (1 samples)
	maximum = 0.756 (1 samples)
Accepted flit rate average = 0.236536 (1 samples)
	minimum = 0.160667 (1 samples)
	maximum = 0.317667 (1 samples)
Injected packet size average = 18.0209 (1 samples)
Accepted packet size average = 18.1927 (1 samples)
Hops average = 5.08912 (1 samples)
Total run time 13.9078
