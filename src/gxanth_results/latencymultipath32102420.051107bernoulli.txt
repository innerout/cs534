BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.2
	minimum = 22
	maximum = 879
Network latency average = 299.369
	minimum = 22
	maximum = 832
Slowest packet = 237
Flit latency average = 264.628
	minimum = 5
	maximum = 861
Slowest flit = 16357
Fragmentation average = 84.9049
	minimum = 0
	maximum = 447
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0150573
	minimum = 0.007 (at node 115)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.294193
	minimum = 0.141 (at node 150)
	maximum = 0.436 (at node 70)
Injected packet length average = 17.8433
Accepted packet length average = 19.5382
Total in-flight flits = 107351 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.013
	minimum = 22
	maximum = 1762
Network latency average = 565.972
	minimum = 22
	maximum = 1697
Slowest packet = 1065
Flit latency average = 525.223
	minimum = 5
	maximum = 1701
Slowest flit = 42908
Fragmentation average = 113.121
	minimum = 0
	maximum = 455
Injected packet rate average = 0.0490573
	minimum = 0.039 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0162995
	minimum = 0.01 (at node 83)
	maximum = 0.0245 (at node 152)
Injected flit rate average = 0.878964
	minimum = 0.698 (at node 43)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.307635
	minimum = 0.183 (at node 83)
	maximum = 0.45 (at node 177)
Injected packet length average = 17.9171
Accepted packet length average = 18.8739
Total in-flight flits = 220952 (0 measured)
latency change    = 0.462368
throughput change = 0.0436969
Class 0:
Packet latency average = 1402.09
	minimum = 23
	maximum = 2619
Network latency average = 1343.67
	minimum = 22
	maximum = 2527
Slowest packet = 3074
Flit latency average = 1306.76
	minimum = 5
	maximum = 2539
Slowest flit = 68845
Fragmentation average = 150.309
	minimum = 0
	maximum = 487
Injected packet rate average = 0.050625
	minimum = 0.036 (at node 119)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0179063
	minimum = 0.008 (at node 20)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.91126
	minimum = 0.651 (at node 119)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.320365
	minimum = 0.156 (at node 20)
	maximum = 0.544 (at node 151)
Injected packet length average = 18.0002
Accepted packet length average = 17.8912
Total in-flight flits = 334402 (0 measured)
latency change    = 0.569919
throughput change = 0.0397334
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 123.699
	minimum = 25
	maximum = 565
Network latency average = 40.6645
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 1859.65
	minimum = 5
	maximum = 3262
Slowest flit = 114371
Fragmentation average = 6.60634
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0511719
	minimum = 0.037 (at node 10)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0176042
	minimum = 0.007 (at node 39)
	maximum = 0.028 (at node 123)
Injected flit rate average = 0.921703
	minimum = 0.68 (at node 10)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.319031
	minimum = 0.158 (at node 180)
	maximum = 0.512 (at node 121)
Injected packet length average = 18.0119
Accepted packet length average = 18.1225
Total in-flight flits = 449998 (163052 measured)
latency change    = 10.3347
throughput change = 0.00417932
Class 0:
Packet latency average = 129.195
	minimum = 23
	maximum = 583
Network latency average = 41.4117
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 2123.26
	minimum = 5
	maximum = 4095
Slowest flit = 144801
Fragmentation average = 6.65756
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0512031
	minimum = 0.0395 (at node 43)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0176771
	minimum = 0.0115 (at node 36)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.921833
	minimum = 0.71 (at node 43)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.319669
	minimum = 0.212 (at node 60)
	maximum = 0.462 (at node 90)
Injected packet length average = 18.0035
Accepted packet length average = 18.0838
Total in-flight flits = 565565 (325470 measured)
latency change    = 0.0425421
throughput change = 0.00199588
Class 0:
Packet latency average = 127.103
	minimum = 23
	maximum = 671
Network latency average = 41.2017
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 2390.3
	minimum = 5
	maximum = 4883
Slowest flit = 180516
Fragmentation average = 6.63235
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0510625
	minimum = 0.041 (at node 43)
	maximum = 0.0556667 (at node 15)
Accepted packet rate average = 0.0177083
	minimum = 0.0116667 (at node 36)
	maximum = 0.026 (at node 128)
Injected flit rate average = 0.918997
	minimum = 0.737333 (at node 43)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.319351
	minimum = 0.213667 (at node 36)
	maximum = 0.461667 (at node 128)
Injected packet length average = 17.9975
Accepted packet length average = 18.0339
Total in-flight flits = 679872 (486463 measured)
latency change    = 0.0164599
throughput change = 0.000997575
Class 0:
Packet latency average = 130.226
	minimum = 23
	maximum = 3946
Network latency average = 42.3559
	minimum = 22
	maximum = 3892
Slowest packet = 29168
Flit latency average = 2662.4
	minimum = 5
	maximum = 5603
Slowest flit = 230776
Fragmentation average = 6.72833
	minimum = 0
	maximum = 224
Injected packet rate average = 0.0510625
	minimum = 0.042 (at node 8)
	maximum = 0.05575 (at node 18)
Accepted packet rate average = 0.017776
	minimum = 0.013 (at node 36)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.91913
	minimum = 0.755 (at node 8)
	maximum = 1 (at node 18)
Accepted flit rate average= 0.320355
	minimum = 0.241 (at node 36)
	maximum = 0.42875 (at node 128)
Injected packet length average = 18.0001
Accepted packet length average = 18.0218
Total in-flight flits = 794257 (648209 measured)
latency change    = 0.0239793
throughput change = 0.00313644
Draining all recorded packets ...
Class 0:
Remaining flits: 274422 274423 274424 274425 274426 274427 274747 274748 274749 274750 [...] (910461 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (664094 flits)
Class 0:
Remaining flits: 325044 325045 325046 325047 325048 325049 325050 325051 325052 325053 [...] (1024446 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (660576 flits)
Class 0:
Remaining flits: 362897 369405 369406 369407 369408 369409 369410 369411 369412 369413 [...] (1134667 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (646272 flits)
Class 0:
Remaining flits: 378918 378919 378920 378921 378922 378923 378924 378925 378926 378927 [...] (1237528 flits)
Measured flits: 514048 514049 514050 514051 514052 514053 514054 514055 514056 514057 [...] (615542 flits)
Class 0:
Remaining flits: 427968 427969 427970 427971 427972 427973 427974 427975 427976 427977 [...] (1336849 flits)
Measured flits: 514111 514112 514113 514114 514115 514278 514279 514280 514281 514282 [...] (573103 flits)
Class 0:
Remaining flits: 476298 476299 476300 476301 476302 476303 476304 476305 476306 476307 [...] (1436533 flits)
Measured flits: 514890 514891 514892 514893 514894 514895 514896 514897 514898 514899 [...] (526082 flits)
Class 0:
Remaining flits: 536616 536617 536618 536619 536620 536621 536622 536623 536624 536625 [...] (1536514 flits)
Measured flits: 536616 536617 536618 536619 536620 536621 536622 536623 536624 536625 [...] (478940 flits)
Class 0:
Remaining flits: 558499 558500 558501 558502 558503 564984 564985 564986 564987 564988 [...] (1637873 flits)
Measured flits: 558499 558500 558501 558502 558503 564984 564985 564986 564987 564988 [...] (431644 flits)
Class 0:
Remaining flits: 577782 577783 577784 577785 577786 577787 577788 577789 577790 577791 [...] (1736772 flits)
Measured flits: 577782 577783 577784 577785 577786 577787 577788 577789 577790 577791 [...] (384142 flits)
Class 0:
Remaining flits: 663596 663597 663598 663599 663600 663601 663602 663603 663604 663605 [...] (1836927 flits)
Measured flits: 663596 663597 663598 663599 663600 663601 663602 663603 663604 663605 [...] (336529 flits)
Class 0:
Remaining flits: 679760 679761 679762 679763 679764 679765 679766 679767 679768 679769 [...] (1936961 flits)
Measured flits: 679760 679761 679762 679763 679764 679765 679766 679767 679768 679769 [...] (289401 flits)
Class 0:
Remaining flits: 762139 762140 762141 762142 762143 762144 762145 762146 762147 762148 [...] (2035078 flits)
Measured flits: 762139 762140 762141 762142 762143 762144 762145 762146 762147 762148 [...] (242564 flits)
Class 0:
Remaining flits: 805517 807402 807403 807404 807405 807406 807407 822942 822943 822944 [...] (2130670 flits)
Measured flits: 805517 807402 807403 807404 807405 807406 807407 822942 822943 822944 [...] (196009 flits)
Class 0:
Remaining flits: 855082 855083 855084 855085 855086 855087 855088 855089 858168 858169 [...] (2225690 flits)
Measured flits: 855082 855083 855084 855085 855086 855087 855088 855089 858168 858169 [...] (150413 flits)
Class 0:
Remaining flits: 870904 870905 870906 870907 870908 870909 870910 870911 887467 887468 [...] (2316131 flits)
Measured flits: 870904 870905 870906 870907 870908 870909 870910 870911 887467 887468 [...] (107413 flits)
Class 0:
Remaining flits: 932396 932397 932398 932399 935834 935835 935836 935837 937152 937153 [...] (2406591 flits)
Measured flits: 932396 932397 932398 932399 935834 935835 935836 935837 937152 937153 [...] (70221 flits)
Class 0:
Remaining flits: 956070 956071 956072 956073 956074 956075 956076 956077 956078 956079 [...] (2495431 flits)
Measured flits: 956070 956071 956072 956073 956074 956075 956076 956077 956078 956079 [...] (40626 flits)
Class 0:
Remaining flits: 1015848 1015849 1015850 1015851 1015852 1015853 1015854 1015855 1015856 1015857 [...] (2581138 flits)
Measured flits: 1015848 1015849 1015850 1015851 1015852 1015853 1015854 1015855 1015856 1015857 [...] (20783 flits)
Class 0:
Remaining flits: 1038073 1038074 1038075 1038076 1038077 1047636 1047637 1047638 1047639 1047640 [...] (2650997 flits)
Measured flits: 1038073 1038074 1038075 1038076 1038077 1047636 1047637 1047638 1047639 1047640 [...] (9491 flits)
Class 0:
Remaining flits: 1134054 1134055 1134056 1134057 1134058 1134059 1134060 1134061 1134062 1134063 [...] (2693661 flits)
Measured flits: 1134054 1134055 1134056 1134057 1134058 1134059 1134060 1134061 1134062 1134063 [...] (3681 flits)
Class 0:
Remaining flits: 1163664 1163665 1163666 1163667 1163668 1163669 1163670 1163671 1163672 1163673 [...] (2708067 flits)
Measured flits: 1163664 1163665 1163666 1163667 1163668 1163669 1163670 1163671 1163672 1163673 [...] (1001 flits)
Class 0:
Remaining flits: 1231578 1231579 1231580 1231581 1231582 1231583 1231584 1231585 1231586 1231587 [...] (2710594 flits)
Measured flits: 1235394 1235395 1235396 1235397 1235398 1235399 1235400 1235401 1235402 1235403 [...] (209 flits)
Class 0:
Remaining flits: 1248153 1248154 1248155 1251234 1251235 1251236 1251237 1251238 1251239 1251240 [...] (2706991 flits)
Measured flits: 1248153 1248154 1248155 (3 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1301624 1301625 1301626 1301627 1301628 1301629 1301630 1301631 1301632 1301633 [...] (2656679 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1355656 1355657 1355658 1355659 1355660 1355661 1355662 1355663 1355664 1355665 [...] (2606134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1374718 1374719 1374720 1374721 1374722 1374723 1374724 1374725 1374726 1374727 [...] (2556038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1413575 1438290 1438291 1438292 1438293 1438294 1438295 1438296 1438297 1438298 [...] (2505969 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1480538 1480539 1480540 1480541 1480542 1480543 1480544 1480545 1480546 1480547 [...] (2455959 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1511064 1511065 1511066 1511067 1511068 1511069 1511070 1511071 1511072 1511073 [...] (2405635 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1564398 1564399 1564400 1564401 1564402 1564403 1564404 1564405 1564406 1564407 [...] (2355291 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630350 1630351 1630352 1630353 1630354 1630355 1630356 1630357 1630358 1630359 [...] (2304782 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1633480 1633481 1666140 1666141 1666142 1666143 1666144 1666145 1666146 1666147 [...] (2254570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1733202 1733203 1733204 1733205 1733206 1733207 1733208 1733209 1733210 1733211 [...] (2204373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1752822 1752823 1752824 1752825 1752826 1752827 1752828 1752829 1752830 1752831 [...] (2153973 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1752829 1752830 1752831 1752832 1752833 1752834 1752835 1752836 1752837 1752838 [...] (2103379 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1780036 1780037 1830024 1830025 1830026 1830027 1830028 1830029 1830030 1830031 [...] (2052099 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1830024 1830025 1830026 1830027 1830028 1830029 1830030 1830031 1830032 1830033 [...] (2001499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1849014 1849015 1849016 1849017 1849018 1849019 1849020 1849021 1849022 1849023 [...] (1950569 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1853748 1853749 1853750 1853751 1853752 1853753 1853754 1853755 1853756 1853757 [...] (1899304 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1853748 1853749 1853750 1853751 1853752 1853753 1853754 1853755 1853756 1853757 [...] (1848597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1964142 1964143 1964144 1964145 1964146 1964147 1964148 1964149 1964150 1964151 [...] (1797936 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1997037 1997038 1997039 1997040 1997041 1997042 1997043 1997044 1997045 2008278 [...] (1747244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2065786 2065787 2073114 2073115 2073116 2073117 2073118 2073119 2073120 2073121 [...] (1696496 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2075922 2075923 2075924 2075925 2075926 2075927 2075928 2075929 2075930 2075931 [...] (1646417 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2087964 2087965 2087966 2087967 2087968 2087969 2087970 2087971 2087972 2087973 [...] (1596835 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2130042 2130043 2130044 2130045 2130046 2130047 2160504 2160505 2160506 2160507 [...] (1547422 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2164320 2164321 2164322 2164323 2164324 2164325 2164326 2164327 2164328 2164329 [...] (1498649 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2190960 2190961 2190962 2190963 2190964 2190965 2190966 2190967 2190968 2190969 [...] (1450113 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2190976 2190977 2224048 2224049 2224050 2224051 2224052 2224053 2224054 2224055 [...] (1402062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2269116 2269117 2269118 2269119 2269120 2269121 2269122 2269123 2269124 2269125 [...] (1354407 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289492 2289493 2289494 2289495 2289496 2289497 2289498 2289499 2289500 2289501 [...] (1306610 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289492 2289493 2289494 2289495 2289496 2289497 2289498 2289499 2289500 2289501 [...] (1258878 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289492 2289493 2289494 2289495 2289496 2289497 2289498 2289499 2289500 2289501 [...] (1211029 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2389590 2389591 2389592 2389593 2389594 2389595 2389596 2389597 2389598 2389599 [...] (1162979 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2429892 2429893 2429894 2429895 2429896 2429897 2429898 2429899 2429900 2429901 [...] (1115450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2458188 2458189 2458190 2458191 2458192 2458193 2458194 2458195 2458196 2458197 [...] (1067990 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2458188 2458189 2458190 2458191 2458192 2458193 2458194 2458195 2458196 2458197 [...] (1020420 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2458188 2458189 2458190 2458191 2458192 2458193 2458194 2458195 2458196 2458197 [...] (973110 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2532510 2532511 2532512 2532513 2532514 2532515 2532516 2532517 2532518 2532519 [...] (925393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2582658 2582659 2582660 2582661 2582662 2582663 2582664 2582665 2582666 2582667 [...] (877759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2605266 2605267 2605268 2605269 2605270 2605271 2605272 2605273 2605274 2605275 [...] (829986 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2670840 2670841 2670842 2670843 2670844 2670845 2670846 2670847 2670848 2670849 [...] (782029 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2722968 2722969 2722970 2722971 2722972 2722973 2722974 2722975 2722976 2722977 [...] (734173 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2722968 2722969 2722970 2722971 2722972 2722973 2722974 2722975 2722976 2722977 [...] (686577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2796552 2796553 2796554 2796555 2796556 2796557 2796558 2796559 2796560 2796561 [...] (638936 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2827062 2827063 2827064 2827065 2827066 2827067 2827068 2827069 2827070 2827071 [...] (591788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2827078 2827079 2921382 2921383 2921384 2921385 2921386 2921387 2921388 2921389 [...] (543969 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2921382 2921383 2921384 2921385 2921386 2921387 2921388 2921389 2921390 2921391 [...] (496336 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3004632 3004633 3004634 3004635 3004636 3004637 3004638 3004639 3004640 3004641 [...] (448384 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3004632 3004633 3004634 3004635 3004636 3004637 3004638 3004639 3004640 3004641 [...] (400541 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3004632 3004633 3004634 3004635 3004636 3004637 3004638 3004639 3004640 3004641 [...] (352791 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3004646 3004647 3004648 3004649 3057174 3057175 3057176 3057177 3057178 3057179 [...] (305397 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3004646 3004647 3004648 3004649 3057174 3057175 3057176 3057177 3057178 3057179 [...] (257326 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3057174 3057175 3057176 3057177 3057178 3057179 3057180 3057181 3057182 3057183 [...] (209679 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3201318 3201319 3201320 3201321 3201322 3201323 3201324 3201325 3201326 3201327 [...] (162740 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3376206 3376207 3376208 3376209 3376210 3376211 3376212 3376213 3376214 3376215 [...] (115237 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3376206 3376207 3376208 3376209 3376210 3376211 3376212 3376213 3376214 3376215 [...] (68535 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3449754 3449755 3449756 3449757 3449758 3449759 3449760 3449761 3449762 3449763 [...] (28328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3449770 3449771 3550122 3550123 3550124 3550125 3550126 3550127 3550128 3550129 [...] (7608 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4171698 4171699 4171700 4171701 4171702 4171703 4171704 4171705 4171706 4171707 [...] (398 flits)
Measured flits: (0 flits)
Time taken is 87381 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11349.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 23028 (1 samples)
Network latency average = 11255.7 (1 samples)
	minimum = 22 (1 samples)
	maximum = 22861 (1 samples)
Flit latency average = 28524.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 65164 (1 samples)
Fragmentation average = 174.889 (1 samples)
	minimum = 0 (1 samples)
	maximum = 512 (1 samples)
Injected packet rate average = 0.0510625 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.05575 (1 samples)
Accepted packet rate average = 0.017776 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.024 (1 samples)
Injected flit rate average = 0.91913 (1 samples)
	minimum = 0.755 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.320355 (1 samples)
	minimum = 0.241 (1 samples)
	maximum = 0.42875 (1 samples)
Injected packet size average = 18.0001 (1 samples)
Accepted packet size average = 18.0218 (1 samples)
Hops average = 5.07154 (1 samples)
Total run time 118.104
