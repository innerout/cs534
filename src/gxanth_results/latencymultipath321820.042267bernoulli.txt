BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 331.535
	minimum = 22
	maximum = 908
Network latency average = 307.669
	minimum = 22
	maximum = 883
Slowest packet = 631
Flit latency average = 268.715
	minimum = 5
	maximum = 866
Slowest flit = 11519
Fragmentation average = 75.8773
	minimum = 0
	maximum = 339
Injected packet rate average = 0.0291875
	minimum = 0.016 (at node 48)
	maximum = 0.043 (at node 51)
Accepted packet rate average = 0.0142604
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.518823
	minimum = 0.288 (at node 48)
	maximum = 0.774 (at node 51)
Accepted flit rate average= 0.271479
	minimum = 0.138 (at node 174)
	maximum = 0.444 (at node 152)
Injected packet length average = 17.7755
Accepted packet length average = 19.0373
Total in-flight flits = 49288 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 650.308
	minimum = 22
	maximum = 1745
Network latency average = 552.097
	minimum = 22
	maximum = 1694
Slowest packet = 1487
Flit latency average = 501.414
	minimum = 5
	maximum = 1709
Slowest flit = 37791
Fragmentation average = 76.2494
	minimum = 0
	maximum = 362
Injected packet rate average = 0.02225
	minimum = 0.015 (at node 68)
	maximum = 0.029 (at node 53)
Accepted packet rate average = 0.014638
	minimum = 0.0095 (at node 81)
	maximum = 0.022 (at node 177)
Injected flit rate average = 0.397484
	minimum = 0.267 (at node 68)
	maximum = 0.5215 (at node 53)
Accepted flit rate average= 0.269078
	minimum = 0.171 (at node 81)
	maximum = 0.396 (at node 177)
Injected packet length average = 17.8645
Accepted packet length average = 18.3821
Total in-flight flits = 51456 (0 measured)
latency change    = 0.490188
throughput change = 0.00892321
Class 0:
Packet latency average = 1613.24
	minimum = 622
	maximum = 2433
Network latency average = 995.825
	minimum = 22
	maximum = 2358
Slowest packet = 2471
Flit latency average = 936.573
	minimum = 5
	maximum = 2418
Slowest flit = 67930
Fragmentation average = 70.9225
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0151146
	minimum = 0.005 (at node 36)
	maximum = 0.031 (at node 178)
Accepted packet rate average = 0.0148438
	minimum = 0.007 (at node 113)
	maximum = 0.025 (at node 160)
Injected flit rate average = 0.273141
	minimum = 0.081 (at node 36)
	maximum = 0.55 (at node 178)
Accepted flit rate average= 0.268313
	minimum = 0.116 (at node 138)
	maximum = 0.465 (at node 160)
Injected packet length average = 18.0713
Accepted packet length average = 18.0758
Total in-flight flits = 52284 (0 measured)
latency change    = 0.596892
throughput change = 0.00285348
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2180.33
	minimum = 977
	maximum = 3013
Network latency average = 364.937
	minimum = 22
	maximum = 954
Slowest packet = 11512
Flit latency average = 960.189
	minimum = 5
	maximum = 3241
Slowest flit = 85498
Fragmentation average = 37.0242
	minimum = 0
	maximum = 233
Injected packet rate average = 0.0148125
	minimum = 0.002 (at node 129)
	maximum = 0.026 (at node 58)
Accepted packet rate average = 0.0148854
	minimum = 0.006 (at node 31)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.26562
	minimum = 0.036 (at node 129)
	maximum = 0.468 (at node 173)
Accepted flit rate average= 0.267917
	minimum = 0.105 (at node 31)
	maximum = 0.435 (at node 34)
Injected packet length average = 17.9321
Accepted packet length average = 17.9986
Total in-flight flits = 51892 (41133 measured)
latency change    = 0.260095
throughput change = 0.00147745
Class 0:
Packet latency average = 2756.32
	minimum = 977
	maximum = 3828
Network latency average = 775.397
	minimum = 22
	maximum = 1941
Slowest packet = 11512
Flit latency average = 971.495
	minimum = 5
	maximum = 3662
Slowest flit = 94871
Fragmentation average = 60.9715
	minimum = 0
	maximum = 306
Injected packet rate average = 0.0149844
	minimum = 0.0045 (at node 84)
	maximum = 0.0235 (at node 173)
Accepted packet rate average = 0.0148464
	minimum = 0.0085 (at node 2)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.269216
	minimum = 0.081 (at node 84)
	maximum = 0.423 (at node 173)
Accepted flit rate average= 0.267135
	minimum = 0.153 (at node 2)
	maximum = 0.3805 (at node 129)
Injected packet length average = 17.9665
Accepted packet length average = 17.9933
Total in-flight flits = 53204 (52606 measured)
latency change    = 0.208972
throughput change = 0.00292455
Class 0:
Packet latency average = 3154.38
	minimum = 977
	maximum = 4813
Network latency average = 902.428
	minimum = 22
	maximum = 2882
Slowest packet = 11512
Flit latency average = 971.968
	minimum = 5
	maximum = 3662
Slowest flit = 94871
Fragmentation average = 67.3075
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0149479
	minimum = 0.008 (at node 84)
	maximum = 0.0206667 (at node 38)
Accepted packet rate average = 0.0148733
	minimum = 0.00933333 (at node 2)
	maximum = 0.0203333 (at node 138)
Injected flit rate average = 0.26884
	minimum = 0.142 (at node 84)
	maximum = 0.372667 (at node 38)
Accepted flit rate average= 0.26821
	minimum = 0.167667 (at node 2)
	maximum = 0.367667 (at node 138)
Injected packet length average = 17.9851
Accepted packet length average = 18.033
Total in-flight flits = 52721 (52721 measured)
latency change    = 0.126192
throughput change = 0.00400676
Class 0:
Packet latency average = 3507.41
	minimum = 977
	maximum = 5530
Network latency average = 947.335
	minimum = 22
	maximum = 3309
Slowest packet = 11512
Flit latency average = 970.647
	minimum = 5
	maximum = 3662
Slowest flit = 94871
Fragmentation average = 70.7592
	minimum = 0
	maximum = 359
Injected packet rate average = 0.0148229
	minimum = 0.00825 (at node 84)
	maximum = 0.021 (at node 38)
Accepted packet rate average = 0.0148919
	minimum = 0.0105 (at node 89)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.266587
	minimum = 0.14875 (at node 84)
	maximum = 0.37825 (at node 38)
Accepted flit rate average= 0.268138
	minimum = 0.19075 (at node 89)
	maximum = 0.36 (at node 129)
Injected packet length average = 17.9848
Accepted packet length average = 18.0056
Total in-flight flits = 50978 (50978 measured)
latency change    = 0.100653
throughput change = 0.0002687
Class 0:
Packet latency average = 3842.72
	minimum = 977
	maximum = 6233
Network latency average = 966.418
	minimum = 22
	maximum = 3951
Slowest packet = 11512
Flit latency average = 966.131
	minimum = 5
	maximum = 3934
Slowest flit = 250109
Fragmentation average = 72.4486
	minimum = 0
	maximum = 359
Injected packet rate average = 0.014874
	minimum = 0.0078 (at node 8)
	maximum = 0.0196 (at node 149)
Accepted packet rate average = 0.0148979
	minimum = 0.0116 (at node 80)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.267395
	minimum = 0.1416 (at node 8)
	maximum = 0.3526 (at node 149)
Accepted flit rate average= 0.26821
	minimum = 0.2132 (at node 89)
	maximum = 0.345 (at node 103)
Injected packet length average = 17.9774
Accepted packet length average = 18.0032
Total in-flight flits = 51662 (51662 measured)
latency change    = 0.0872594
throughput change = 0.000269922
Class 0:
Packet latency average = 4166.76
	minimum = 977
	maximum = 6966
Network latency average = 979.633
	minimum = 22
	maximum = 3951
Slowest packet = 11512
Flit latency average = 964.403
	minimum = 5
	maximum = 3934
Slowest flit = 250109
Fragmentation average = 74.6125
	minimum = 0
	maximum = 359
Injected packet rate average = 0.0148863
	minimum = 0.008 (at node 8)
	maximum = 0.0203333 (at node 149)
Accepted packet rate average = 0.0148707
	minimum = 0.0115 (at node 80)
	maximum = 0.0186667 (at node 138)
Injected flit rate average = 0.267744
	minimum = 0.144333 (at node 8)
	maximum = 0.365667 (at node 149)
Accepted flit rate average= 0.268007
	minimum = 0.211 (at node 80)
	maximum = 0.339167 (at node 138)
Injected packet length average = 17.9859
Accepted packet length average = 18.0225
Total in-flight flits = 51952 (51952 measured)
latency change    = 0.0777675
throughput change = 0.000759205
Class 0:
Packet latency average = 4494.15
	minimum = 977
	maximum = 8073
Network latency average = 986.41
	minimum = 22
	maximum = 4274
Slowest packet = 11512
Flit latency average = 962.088
	minimum = 5
	maximum = 4253
Slowest flit = 343780
Fragmentation average = 75.9883
	minimum = 0
	maximum = 359
Injected packet rate average = 0.0148921
	minimum = 0.00885714 (at node 84)
	maximum = 0.0205714 (at node 149)
Accepted packet rate average = 0.0148713
	minimum = 0.0115714 (at node 80)
	maximum = 0.0182857 (at node 181)
Injected flit rate average = 0.267972
	minimum = 0.161 (at node 84)
	maximum = 0.369857 (at node 149)
Accepted flit rate average= 0.268111
	minimum = 0.210714 (at node 80)
	maximum = 0.330429 (at node 103)
Injected packet length average = 17.9942
Accepted packet length average = 18.0288
Total in-flight flits = 51871 (51871 measured)
latency change    = 0.0728473
throughput change = 0.000387596
Draining all recorded packets ...
Class 0:
Remaining flits: 395910 395911 395912 395913 395914 395915 395916 395917 395918 395919 [...] (51359 flits)
Measured flits: 395910 395911 395912 395913 395914 395915 395916 395917 395918 395919 [...] (51359 flits)
Class 0:
Remaining flits: 448254 448255 448256 448257 448258 448259 448260 448261 448262 448263 [...] (51085 flits)
Measured flits: 448254 448255 448256 448257 448258 448259 448260 448261 448262 448263 [...] (51085 flits)
Class 0:
Remaining flits: 463570 463571 479322 479323 479324 479325 479326 479327 479328 479329 [...] (50094 flits)
Measured flits: 463570 463571 479322 479323 479324 479325 479326 479327 479328 479329 [...] (50094 flits)
Class 0:
Remaining flits: 505278 505279 505280 505281 505282 505283 505284 505285 505286 505287 [...] (50395 flits)
Measured flits: 505278 505279 505280 505281 505282 505283 505284 505285 505286 505287 [...] (50395 flits)
Class 0:
Remaining flits: 531396 531397 531398 531399 531400 531401 531402 531403 531404 531405 [...] (50352 flits)
Measured flits: 531396 531397 531398 531399 531400 531401 531402 531403 531404 531405 [...] (50352 flits)
Class 0:
Remaining flits: 531410 531411 531412 531413 545310 545311 545312 545313 545314 545315 [...] (50063 flits)
Measured flits: 531410 531411 531412 531413 545310 545311 545312 545313 545314 545315 [...] (50063 flits)
Class 0:
Remaining flits: 701334 701335 701336 701337 701338 701339 701340 701341 701342 701343 [...] (50645 flits)
Measured flits: 701334 701335 701336 701337 701338 701339 701340 701341 701342 701343 [...] (50645 flits)
Class 0:
Remaining flits: 707140 707141 707142 707143 707144 707145 707146 707147 730872 730873 [...] (49698 flits)
Measured flits: 707140 707141 707142 707143 707144 707145 707146 707147 730872 730873 [...] (49698 flits)
Class 0:
Remaining flits: 779292 779293 779294 779295 779296 779297 779298 779299 779300 779301 [...] (50744 flits)
Measured flits: 779292 779293 779294 779295 779296 779297 779298 779299 779300 779301 [...] (50744 flits)
Class 0:
Remaining flits: 849654 849655 849656 849657 849658 849659 849660 849661 849662 849663 [...] (49994 flits)
Measured flits: 849654 849655 849656 849657 849658 849659 849660 849661 849662 849663 [...] (49994 flits)
Class 0:
Remaining flits: 849654 849655 849656 849657 849658 849659 849660 849661 849662 849663 [...] (49044 flits)
Measured flits: 849654 849655 849656 849657 849658 849659 849660 849661 849662 849663 [...] (49026 flits)
Class 0:
Remaining flits: 849671 906966 906967 906968 906969 906970 906971 906972 906973 906974 [...] (48940 flits)
Measured flits: 849671 906966 906967 906968 906969 906970 906971 906972 906973 906974 [...] (48408 flits)
Class 0:
Remaining flits: 938394 938395 938396 938397 938398 938399 938400 938401 938402 938403 [...] (49557 flits)
Measured flits: 938394 938395 938396 938397 938398 938399 938400 938401 938402 938403 [...] (47713 flits)
Class 0:
Remaining flits: 1008522 1008523 1008524 1008525 1008526 1008527 1008528 1008529 1008530 1008531 [...] (49809 flits)
Measured flits: 1008522 1008523 1008524 1008525 1008526 1008527 1008528 1008529 1008530 1008531 [...] (44950 flits)
Class 0:
Remaining flits: 1060326 1060327 1060328 1060329 1060330 1060331 1060332 1060333 1060334 1060335 [...] (49723 flits)
Measured flits: 1060326 1060327 1060328 1060329 1060330 1060331 1060332 1060333 1060334 1060335 [...] (41249 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (49844 flits)
Measured flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (34773 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (50921 flits)
Measured flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (26522 flits)
Class 0:
Remaining flits: 1246842 1246843 1246844 1246845 1246846 1246847 1246848 1246849 1246850 1246851 [...] (50318 flits)
Measured flits: 1246842 1246843 1246844 1246845 1246846 1246847 1246848 1246849 1246850 1246851 [...] (19982 flits)
Class 0:
Remaining flits: 1304054 1304055 1304056 1304057 1304058 1304059 1304060 1304061 1304062 1304063 [...] (49649 flits)
Measured flits: 1304054 1304055 1304056 1304057 1304058 1304059 1304060 1304061 1304062 1304063 [...] (13932 flits)
Class 0:
Remaining flits: 1307358 1307359 1307360 1307361 1307362 1307363 1307364 1307365 1307366 1307367 [...] (49253 flits)
Measured flits: 1314296 1314297 1314298 1314299 1314300 1314301 1314302 1314303 1314304 1314305 [...] (10000 flits)
Class 0:
Remaining flits: 1307358 1307359 1307360 1307361 1307362 1307363 1307364 1307365 1307366 1307367 [...] (48478 flits)
Measured flits: 1330671 1330672 1330673 1330674 1330675 1330676 1330677 1330678 1330679 1330680 [...] (7129 flits)
Class 0:
Remaining flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (48780 flits)
Measured flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (5340 flits)
Class 0:
Remaining flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (48751 flits)
Measured flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (3626 flits)
Class 0:
Remaining flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (50067 flits)
Measured flits: 1383984 1383985 1383986 1383987 1383988 1383989 1383990 1383991 1383992 1383993 [...] (2723 flits)
Class 0:
Remaining flits: 1575252 1575253 1575254 1575255 1575256 1575257 1575258 1575259 1575260 1575261 [...] (50286 flits)
Measured flits: 1575252 1575253 1575254 1575255 1575256 1575257 1575258 1575259 1575260 1575261 [...] (1574 flits)
Class 0:
Remaining flits: 1622831 1622832 1622833 1622834 1622835 1622836 1622837 1622838 1622839 1622840 [...] (49828 flits)
Measured flits: 1732086 1732087 1732088 1732089 1732090 1732091 1732092 1732093 1732094 1732095 [...] (1262 flits)
Class 0:
Remaining flits: 1694592 1694593 1694594 1694595 1694596 1694597 1694598 1694599 1694600 1694601 [...] (49286 flits)
Measured flits: 1778256 1778257 1778258 1778259 1778260 1778261 1778262 1778263 1778264 1778265 [...] (741 flits)
Class 0:
Remaining flits: 1705878 1705879 1705880 1705881 1705882 1705883 1705884 1705885 1705886 1705887 [...] (48959 flits)
Measured flits: 1780182 1780183 1780184 1780185 1780186 1780187 1780188 1780189 1780190 1780191 [...] (469 flits)
Class 0:
Remaining flits: 1705878 1705879 1705880 1705881 1705882 1705883 1705884 1705885 1705886 1705887 [...] (48342 flits)
Measured flits: 1780182 1780183 1780184 1780185 1780186 1780187 1780188 1780189 1780190 1780191 [...] (253 flits)
Class 0:
Remaining flits: 1705878 1705879 1705880 1705881 1705882 1705883 1705884 1705885 1705886 1705887 [...] (47866 flits)
Measured flits: 1780187 1780188 1780189 1780190 1780191 1780192 1780193 1780194 1780195 1780196 [...] (123 flits)
Class 0:
Remaining flits: 1705878 1705879 1705880 1705881 1705882 1705883 1705884 1705885 1705886 1705887 [...] (48632 flits)
Measured flits: 1958456 1958457 1958458 1958459 1958460 1958461 1958462 1958463 1958464 1958465 [...] (34 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1705883 1705884 1705885 1705886 1705887 1705888 1705889 1705890 1705891 1705892 [...] (9656 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1980860 1980861 1980862 1980863 2009772 2009773 2009774 2009775 2009776 2009777 [...] (623 flits)
Measured flits: (0 flits)
Time taken is 44355 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10803.2 (1 samples)
	minimum = 977 (1 samples)
	maximum = 31899 (1 samples)
Network latency average = 1000.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8039 (1 samples)
Flit latency average = 942.641 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10682 (1 samples)
Fragmentation average = 76.9118 (1 samples)
	minimum = 0 (1 samples)
	maximum = 365 (1 samples)
Injected packet rate average = 0.0148921 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0205714 (1 samples)
Accepted packet rate average = 0.0148713 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.267972 (1 samples)
	minimum = 0.161 (1 samples)
	maximum = 0.369857 (1 samples)
Accepted flit rate average = 0.268111 (1 samples)
	minimum = 0.210714 (1 samples)
	maximum = 0.330429 (1 samples)
Injected packet size average = 17.9942 (1 samples)
Accepted packet size average = 18.0288 (1 samples)
Hops average = 5.06148 (1 samples)
Total run time 63.5098
