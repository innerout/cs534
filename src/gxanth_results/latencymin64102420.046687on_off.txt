BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.847
	minimum = 22
	maximum = 981
Network latency average = 236.79
	minimum = 22
	maximum = 820
Slowest packet = 46
Flit latency average = 205.513
	minimum = 5
	maximum = 803
Slowest flit = 8027
Fragmentation average = 47.9443
	minimum = 0
	maximum = 614
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139427
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.263448
	minimum = 0.118 (at node 150)
	maximum = 0.432 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 18.895
Total in-flight flits = 52252 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 615.723
	minimum = 22
	maximum = 1946
Network latency average = 448.502
	minimum = 22
	maximum = 1723
Slowest packet = 46
Flit latency average = 416.053
	minimum = 5
	maximum = 1713
Slowest flit = 25334
Fragmentation average = 73.1419
	minimum = 0
	maximum = 750
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0147578
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.276404
	minimum = 0.1655 (at node 164)
	maximum = 0.393 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.7293
Total in-flight flits = 110635 (0 measured)
latency change    = 0.428563
throughput change = 0.0468725
Class 0:
Packet latency average = 1349.24
	minimum = 27
	maximum = 2824
Network latency average = 1070.27
	minimum = 22
	maximum = 2425
Slowest packet = 3663
Flit latency average = 1031.52
	minimum = 5
	maximum = 2514
Slowest flit = 43806
Fragmentation average = 134.255
	minimum = 0
	maximum = 923
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0163438
	minimum = 0.007 (at node 190)
	maximum = 0.035 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.298167
	minimum = 0.12 (at node 113)
	maximum = 0.634 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.2435
Total in-flight flits = 175553 (0 measured)
latency change    = 0.543653
throughput change = 0.0729894
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 443.186
	minimum = 26
	maximum = 2070
Network latency average = 35.5084
	minimum = 22
	maximum = 75
Slowest packet = 18852
Flit latency average = 1451.73
	minimum = 5
	maximum = 3301
Slowest flit = 65730
Fragmentation average = 6.97993
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0164219
	minimum = 0.006 (at node 5)
	maximum = 0.026 (at node 9)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.300307
	minimum = 0.128 (at node 48)
	maximum = 0.465 (at node 46)
Injected packet length average = 18.0134
Accepted packet length average = 18.287
Total in-flight flits = 247278 (118528 measured)
latency change    = 2.04442
throughput change = 0.00712812
Class 0:
Packet latency average = 503.759
	minimum = 26
	maximum = 2596
Network latency average = 93.6119
	minimum = 22
	maximum = 1986
Slowest packet = 18852
Flit latency average = 1684.76
	minimum = 5
	maximum = 4249
Slowest flit = 72837
Fragmentation average = 10.482
	minimum = 0
	maximum = 373
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0164167
	minimum = 0.0085 (at node 48)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.29994
	minimum = 0.159 (at node 48)
	maximum = 0.4595 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 18.2705
Total in-flight flits = 315166 (232313 measured)
latency change    = 0.120243
throughput change = 0.0012242
Class 0:
Packet latency average = 894.306
	minimum = 25
	maximum = 3594
Network latency average = 513.567
	minimum = 22
	maximum = 2966
Slowest packet = 18852
Flit latency average = 1914.25
	minimum = 5
	maximum = 5153
Slowest flit = 76841
Fragmentation average = 40.7993
	minimum = 0
	maximum = 659
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0163194
	minimum = 0.011 (at node 48)
	maximum = 0.023 (at node 103)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.29984
	minimum = 0.202667 (at node 48)
	maximum = 0.413 (at node 103)
Injected packet length average = 18.0004
Accepted packet length average = 18.3732
Total in-flight flits = 383941 (338441 measured)
latency change    = 0.436704
throughput change = 0.000332932
Class 0:
Packet latency average = 1619.06
	minimum = 25
	maximum = 5303
Network latency average = 1267.19
	minimum = 22
	maximum = 3992
Slowest packet = 18852
Flit latency average = 2154.31
	minimum = 5
	maximum = 6176
Slowest flit = 80955
Fragmentation average = 97.4371
	minimum = 0
	maximum = 899
Injected packet rate average = 0.036543
	minimum = 0.01325 (at node 36)
	maximum = 0.0555 (at node 33)
Accepted packet rate average = 0.0163958
	minimum = 0.0105 (at node 48)
	maximum = 0.02175 (at node 103)
Injected flit rate average = 0.65787
	minimum = 0.2385 (at node 36)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.300289
	minimum = 0.197 (at node 48)
	maximum = 0.3995 (at node 103)
Injected packet length average = 18.0026
Accepted packet length average = 18.315
Total in-flight flits = 450101 (426163 measured)
latency change    = 0.447637
throughput change = 0.00149451
Class 0:
Packet latency average = 2281.65
	minimum = 25
	maximum = 6207
Network latency average = 1929.48
	minimum = 22
	maximum = 4986
Slowest packet = 18852
Flit latency average = 2399.81
	minimum = 5
	maximum = 6888
Slowest flit = 112749
Fragmentation average = 150.65
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0363698
	minimum = 0.0128 (at node 36)
	maximum = 0.0556 (at node 33)
Accepted packet rate average = 0.016476
	minimum = 0.0114 (at node 48)
	maximum = 0.0206 (at node 128)
Injected flit rate average = 0.65476
	minimum = 0.2304 (at node 36)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.300787
	minimum = 0.2186 (at node 48)
	maximum = 0.3792 (at node 138)
Injected packet length average = 18.0029
Accepted packet length average = 18.2561
Total in-flight flits = 515267 (502687 measured)
latency change    = 0.290401
throughput change = 0.00165711
Class 0:
Packet latency average = 2795.75
	minimum = 25
	maximum = 7712
Network latency average = 2436.3
	minimum = 22
	maximum = 5942
Slowest packet = 18852
Flit latency average = 2647.37
	minimum = 5
	maximum = 7364
Slowest flit = 174733
Fragmentation average = 186.568
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0365964
	minimum = 0.0131667 (at node 36)
	maximum = 0.0555 (at node 162)
Accepted packet rate average = 0.0165009
	minimum = 0.0128333 (at node 48)
	maximum = 0.0206667 (at node 147)
Injected flit rate average = 0.658522
	minimum = 0.234167 (at node 36)
	maximum = 1 (at node 162)
Accepted flit rate average= 0.300946
	minimum = 0.235 (at node 48)
	maximum = 0.377333 (at node 147)
Injected packet length average = 17.9942
Accepted packet length average = 18.2382
Total in-flight flits = 587725 (582004 measured)
latency change    = 0.183888
throughput change = 0.000527272
Class 0:
Packet latency average = 3232.67
	minimum = 24
	maximum = 8532
Network latency average = 2865.59
	minimum = 22
	maximum = 6933
Slowest packet = 18852
Flit latency average = 2893.22
	minimum = 5
	maximum = 8170
Slowest flit = 195236
Fragmentation average = 211.013
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0365521
	minimum = 0.0151429 (at node 153)
	maximum = 0.0555714 (at node 162)
Accepted packet rate average = 0.0164963
	minimum = 0.0121429 (at node 48)
	maximum = 0.0208571 (at node 138)
Injected flit rate average = 0.657914
	minimum = 0.273286 (at node 153)
	maximum = 1 (at node 162)
Accepted flit rate average= 0.30079
	minimum = 0.226571 (at node 48)
	maximum = 0.382857 (at node 138)
Injected packet length average = 17.9994
Accepted packet length average = 18.2338
Total in-flight flits = 655559 (653432 measured)
latency change    = 0.135156
throughput change = 0.000518641
Draining all recorded packets ...
Class 0:
Remaining flits: 256664 256665 256666 256667 256668 256669 256670 256671 256672 256673 [...] (721871 flits)
Measured flits: 339127 339128 339129 339130 339131 339132 339133 339134 339135 339136 [...] (653856 flits)
Class 0:
Remaining flits: 303864 303865 303866 303867 303868 303869 303870 303871 303872 303873 [...] (796291 flits)
Measured flits: 341820 341821 341822 341823 341824 341825 341826 341827 341828 341829 [...] (614306 flits)
Class 0:
Remaining flits: 317969 341832 341833 341834 341835 341836 341837 353725 353726 353727 [...] (868469 flits)
Measured flits: 341832 341833 341834 341835 341836 341837 353725 353726 353727 353728 [...] (569499 flits)
Class 0:
Remaining flits: 368127 368128 368129 368130 368131 368132 368133 368134 368135 376700 [...] (941378 flits)
Measured flits: 368127 368128 368129 368130 368131 368132 368133 368134 368135 376700 [...] (523318 flits)
Class 0:
Remaining flits: 403274 403275 403276 403277 403278 403279 403280 403281 403282 403283 [...] (1009709 flits)
Measured flits: 403274 403275 403276 403277 403278 403279 403280 403281 403282 403283 [...] (476719 flits)
Class 0:
Remaining flits: 404043 404044 404045 445584 445585 445586 445587 445588 445589 450144 [...] (1081570 flits)
Measured flits: 404043 404044 404045 445584 445585 445586 445587 445588 445589 450144 [...] (429643 flits)
Class 0:
Remaining flits: 463680 463681 463682 463683 463684 463685 463686 463687 463688 463689 [...] (1146938 flits)
Measured flits: 463680 463681 463682 463683 463684 463685 463686 463687 463688 463689 [...] (382134 flits)
Class 0:
Remaining flits: 463689 463690 463691 463692 463693 463694 463695 463696 463697 487296 [...] (1220812 flits)
Measured flits: 463689 463690 463691 463692 463693 463694 463695 463696 463697 487296 [...] (334360 flits)
Class 0:
Remaining flits: 489859 489860 489861 489862 489863 489864 489865 489866 489867 489868 [...] (1296673 flits)
Measured flits: 489859 489860 489861 489862 489863 489864 489865 489866 489867 489868 [...] (287500 flits)
Class 0:
Remaining flits: 565595 574258 574259 574260 574261 574262 574263 574264 574265 574266 [...] (1367199 flits)
Measured flits: 565595 574258 574259 574260 574261 574262 574263 574264 574265 574266 [...] (241674 flits)
Class 0:
Remaining flits: 583712 583713 583714 583715 583716 583717 583718 583719 583720 583721 [...] (1430660 flits)
Measured flits: 583712 583713 583714 583715 583716 583717 583718 583719 583720 583721 [...] (199867 flits)
Class 0:
Remaining flits: 588261 588262 588263 588264 588265 588266 588267 588268 588269 588270 [...] (1497156 flits)
Measured flits: 588261 588262 588263 588264 588265 588266 588267 588268 588269 588270 [...] (164267 flits)
Class 0:
Remaining flits: 625842 625843 625844 625845 625846 625847 625848 625849 625850 625851 [...] (1566929 flits)
Measured flits: 625842 625843 625844 625845 625846 625847 625848 625849 625850 625851 [...] (134698 flits)
Class 0:
Remaining flits: 643840 643841 694242 694243 694244 694245 694246 694247 694248 694249 [...] (1639217 flits)
Measured flits: 643840 643841 694242 694243 694244 694245 694246 694247 694248 694249 [...] (109293 flits)
Class 0:
Remaining flits: 715338 715339 715340 715341 715342 715343 715344 715345 715346 715347 [...] (1706500 flits)
Measured flits: 715338 715339 715340 715341 715342 715343 715344 715345 715346 715347 [...] (87815 flits)
Class 0:
Remaining flits: 753324 753325 753326 753327 753328 753329 753330 753331 753332 753333 [...] (1781099 flits)
Measured flits: 753324 753325 753326 753327 753328 753329 753330 753331 753332 753333 [...] (70485 flits)
Class 0:
Remaining flits: 770957 786481 786482 786483 786484 786485 786486 786487 786488 786489 [...] (1854641 flits)
Measured flits: 770957 786481 786482 786483 786484 786485 786486 786487 786488 786489 [...] (56283 flits)
Class 0:
Remaining flits: 786630 786631 786632 786633 786634 786635 788922 788923 788924 788925 [...] (1927828 flits)
Measured flits: 786630 786631 786632 786633 786634 786635 788922 788923 788924 788925 [...] (44995 flits)
Class 0:
Remaining flits: 788934 788935 788936 788937 788938 788939 805168 805169 805170 805171 [...] (2000427 flits)
Measured flits: 788934 788935 788936 788937 788938 788939 805168 805169 805170 805171 [...] (35893 flits)
Class 0:
Remaining flits: 889148 889149 889150 889151 889152 889153 889154 889155 889156 889157 [...] (2074417 flits)
Measured flits: 889148 889149 889150 889151 889152 889153 889154 889155 889156 889157 [...] (28617 flits)
Class 0:
Remaining flits: 902261 902262 902263 902264 902265 902266 902267 904571 919980 919981 [...] (2148910 flits)
Measured flits: 902261 902262 902263 902264 902265 902266 902267 904571 919980 919981 [...] (22419 flits)
Class 0:
Remaining flits: 919994 919995 919996 919997 937206 937207 937208 937209 937210 937211 [...] (2215418 flits)
Measured flits: 919994 919995 919996 919997 937206 937207 937208 937209 937210 937211 [...] (17967 flits)
Class 0:
Remaining flits: 956333 956334 956335 956336 956337 956338 956339 979992 979993 979994 [...] (2287846 flits)
Measured flits: 956333 956334 956335 956336 956337 956338 956339 979992 979993 979994 [...] (14390 flits)
Class 0:
Remaining flits: 996734 996735 996736 996737 996738 996739 996740 996741 996742 996743 [...] (2358356 flits)
Measured flits: 996734 996735 996736 996737 996738 996739 996740 996741 996742 996743 [...] (11380 flits)
Class 0:
Remaining flits: 1022796 1022797 1022798 1022799 1022800 1022801 1022802 1022803 1022804 1022805 [...] (2428045 flits)
Measured flits: 1022796 1022797 1022798 1022799 1022800 1022801 1022802 1022803 1022804 1022805 [...] (8945 flits)
Class 0:
Remaining flits: 1059210 1059211 1059212 1059213 1059214 1059215 1059216 1059217 1059218 1059219 [...] (2500352 flits)
Measured flits: 1059210 1059211 1059212 1059213 1059214 1059215 1059216 1059217 1059218 1059219 [...] (7220 flits)
Class 0:
Remaining flits: 1059210 1059211 1059212 1059213 1059214 1059215 1059216 1059217 1059218 1059219 [...] (2565901 flits)
Measured flits: 1059210 1059211 1059212 1059213 1059214 1059215 1059216 1059217 1059218 1059219 [...] (5735 flits)
Class 0:
Remaining flits: 1114350 1114351 1114352 1114353 1114354 1114355 1114356 1114357 1114358 1114359 [...] (2632289 flits)
Measured flits: 1114350 1114351 1114352 1114353 1114354 1114355 1114356 1114357 1114358 1114359 [...] (4481 flits)
Class 0:
Remaining flits: 1172502 1172503 1172504 1172505 1172506 1172507 1172508 1172509 1172510 1172511 [...] (2704848 flits)
Measured flits: 1172502 1172503 1172504 1172505 1172506 1172507 1172508 1172509 1172510 1172511 [...] (3376 flits)
Class 0:
Remaining flits: 1172502 1172503 1172504 1172505 1172506 1172507 1172508 1172509 1172510 1172511 [...] (2772640 flits)
Measured flits: 1172502 1172503 1172504 1172505 1172506 1172507 1172508 1172509 1172510 1172511 [...] (2738 flits)
Class 0:
Remaining flits: 1265040 1265041 1265042 1265043 1265044 1265045 1265046 1265047 1265048 1265049 [...] (2842863 flits)
Measured flits: 1292999 1293000 1293001 1293002 1293003 1293004 1293005 1293006 1293007 1293008 [...] (2230 flits)
Class 0:
Remaining flits: 1265040 1265041 1265042 1265043 1265044 1265045 1265046 1265047 1265048 1265049 [...] (2906636 flits)
Measured flits: 1314035 1341090 1341091 1341092 1341093 1341094 1341095 1341096 1341097 1341098 [...] (1780 flits)
Class 0:
Remaining flits: 1265049 1265050 1265051 1265052 1265053 1265054 1265055 1265056 1265057 1288728 [...] (2971567 flits)
Measured flits: 1341099 1341100 1341101 1341102 1341103 1341104 1341105 1341106 1341107 1352520 [...] (1458 flits)
Class 0:
Remaining flits: 1304700 1304701 1304702 1304703 1304704 1304705 1304706 1304707 1304708 1304709 [...] (3038720 flits)
Measured flits: 1352534 1352535 1352536 1352537 1366666 1366667 1371456 1371457 1371458 1371459 [...] (1231 flits)
Class 0:
Remaining flits: 1318662 1318663 1318664 1318665 1318666 1318667 1318668 1318669 1318670 1318671 [...] (3103504 flits)
Measured flits: 1371456 1371457 1371458 1371459 1371460 1371461 1371462 1371463 1371464 1371465 [...] (964 flits)
Class 0:
Remaining flits: 1354788 1354789 1354790 1354791 1354792 1354793 1354794 1354795 1354796 1354797 [...] (3171537 flits)
Measured flits: 1381592 1381593 1381594 1381595 1381596 1381597 1381598 1381599 1381600 1381601 [...] (785 flits)
Class 0:
Remaining flits: 1400938 1400939 1462680 1462681 1462682 1462683 1462684 1462685 1462686 1462687 [...] (3233418 flits)
Measured flits: 1400938 1400939 1462680 1462681 1462682 1462683 1462684 1462685 1462686 1462687 [...] (614 flits)
Class 0:
Remaining flits: 1462696 1462697 1474686 1474687 1474688 1474689 1474690 1474691 1474692 1474693 [...] (3293775 flits)
Measured flits: 1462696 1462697 1474686 1474687 1474688 1474689 1474690 1474691 1474692 1474693 [...] (447 flits)
Class 0:
Remaining flits: 1522188 1522189 1522190 1522191 1522192 1522193 1522194 1522195 1522196 1522197 [...] (3350413 flits)
Measured flits: 1523016 1523017 1523018 1523019 1523020 1523021 1523022 1523023 1523024 1523025 [...] (307 flits)
Class 0:
Remaining flits: 1523016 1523017 1523018 1523019 1523020 1523021 1523022 1523023 1523024 1523025 [...] (3408461 flits)
Measured flits: 1523016 1523017 1523018 1523019 1523020 1523021 1523022 1523023 1523024 1523025 [...] (213 flits)
Class 0:
Remaining flits: 1523025 1523026 1523027 1523028 1523029 1523030 1523031 1523032 1523033 1593782 [...] (3465217 flits)
Measured flits: 1523025 1523026 1523027 1523028 1523029 1523030 1523031 1523032 1523033 1636548 [...] (163 flits)
Class 0:
Remaining flits: 1632494 1632495 1632496 1632497 1632498 1632499 1632500 1632501 1632502 1632503 [...] (3519758 flits)
Measured flits: 1689813 1689814 1689815 1689816 1689817 1689818 1689819 1689820 1689821 1704150 [...] (63 flits)
Class 0:
Remaining flits: 1673761 1673762 1673763 1673764 1673765 1688058 1688059 1688060 1688061 1688062 [...] (3577319 flits)
Measured flits: 1706526 1706527 1706528 1706529 1706530 1706531 1706532 1706533 1706534 1706535 [...] (36 flits)
Class 0:
Remaining flits: 1697760 1697761 1697762 1697763 1697764 1697765 1697766 1697767 1697768 1697769 [...] (3633409 flits)
Measured flits: 1713726 1713727 1713728 1713729 1713730 1713731 1713732 1713733 1713734 1713735 [...] (18 flits)
Class 0:
Remaining flits: 1697770 1697771 1697772 1697773 1697774 1697775 1697776 1697777 1700118 1700119 [...] (3697391 flits)
Measured flits: 1713740 1713741 1713742 1713743 (4 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1743210 1743211 1743212 1743213 1743214 1743215 1743216 1743217 1743218 1743219 [...] (3653484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1753074 1753075 1753076 1753077 1753078 1753079 1753080 1753081 1753082 1753083 [...] (3605115 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1753075 1753076 1753077 1753078 1753079 1753080 1753081 1753082 1753083 1753084 [...] (3556300 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1835319 1835320 1835321 1835322 1835323 1835324 1835325 1835326 1835327 1835328 [...] (3508001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1887210 1887211 1887212 1887213 1887214 1887215 1887216 1887217 1887218 1887219 [...] (3459368 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1887222 1887223 1887224 1887225 1887226 1887227 1928452 1928453 1928454 1928455 [...] (3411057 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1960992 1960993 1960994 1960995 1960996 1960997 1960998 1960999 1961000 1961001 [...] (3362547 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1982106 1982107 1982108 1982109 1982110 1982111 1982112 1982113 1982114 1982115 [...] (3314255 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2005722 2005723 2005724 2005725 2005726 2005727 2005728 2005729 2005730 2005731 [...] (3266446 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2012693 2012694 2012695 2012696 2012697 2012698 2012699 2012700 2012701 2012702 [...] (3218667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2024532 2024533 2024534 2024535 2024536 2024537 2024538 2024539 2024540 2024541 [...] (3170689 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2052434 2052435 2052436 2052437 2052438 2052439 2052440 2052441 2052442 2052443 [...] (3122660 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2078347 2078348 2078349 2078350 2078351 2098314 2098315 2098316 2098317 2098318 [...] (3074557 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2102562 2102563 2102564 2102565 2102566 2102567 2102568 2102569 2102570 2102571 [...] (3026734 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2102574 2102575 2102576 2102577 2102578 2102579 2194612 2194613 2206818 2206819 [...] (2979104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2253522 2253523 2253524 2253525 2253526 2253527 2285244 2285245 2285246 2285247 [...] (2931376 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2329405 2329406 2329407 2329408 2329409 2329410 2329411 2329412 2329413 2329414 [...] (2883789 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2428020 2428021 2428022 2428023 2428024 2428025 2428026 2428027 2428028 2428029 [...] (2835769 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2428020 2428021 2428022 2428023 2428024 2428025 2428026 2428027 2428028 2428029 [...] (2787976 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2465855 2557037 2557038 2557039 2557040 2557041 2557042 2557043 2565126 2565127 [...] (2740112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2567284 2567285 2589948 2589949 2589950 2589951 2589952 2589953 2589954 2589955 [...] (2692297 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2627712 2627713 2627714 2627715 2627716 2627717 2627718 2627719 2627720 2627721 [...] (2644404 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2704536 2704537 2704538 2704539 2704540 2704541 2704542 2704543 2704544 2704545 [...] (2596705 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2763359 2777904 2777905 2777906 2777907 2777908 2777909 2777910 2777911 2777912 [...] (2548981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2825838 2825839 2825840 2825841 2825842 2825843 2825844 2825845 2825846 2825847 [...] (2501274 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2825854 2825855 2842902 2842903 2842904 2842905 2842906 2842907 2842908 2842909 [...] (2453824 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2901887 2928299 2928300 2928301 2928302 2928303 2928304 2928305 2928306 2928307 [...] (2406056 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2933036 2933037 2933038 2933039 2933040 2933041 2933042 2933043 2933044 2933045 [...] (2358480 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2983482 2983483 2983484 2983485 2983486 2983487 2983488 2983489 2983490 2983491 [...] (2310864 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2983498 2983499 3027573 3027574 3027575 3027576 3027577 3027578 3027579 3027580 [...] (2263119 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3119372 3119373 3119374 3119375 3119376 3119377 3119378 3119379 3119380 3119381 [...] (2215603 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3165562 3165563 3165564 3165565 3165566 3165567 3165568 3165569 3187980 3187981 [...] (2167833 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3213322 3213323 3241296 3241297 3241298 3241299 3241300 3241301 3241302 3241303 [...] (2120267 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3271518 3271519 3271520 3271521 3271522 3271523 3271524 3271525 3271526 3271527 [...] (2072356 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3360616 3360617 3366766 3366767 3366768 3366769 3366770 3366771 3366772 3366773 [...] (2024555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3370174 3370175 3393126 3393127 3393128 3393129 3393130 3393131 3393132 3393133 [...] (1976721 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3402943 3402944 3402945 3402946 3402947 3402948 3402949 3402950 3402951 3402952 [...] (1928919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3402953 3417381 3417382 3417383 3417384 3417385 3417386 3417387 3417388 3417389 [...] (1881363 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3464381 3464382 3464383 3464384 3464385 3464386 3464387 3503556 3503557 3503558 [...] (1833924 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3510990 3510991 3510992 3510993 3510994 3510995 3510996 3510997 3510998 3510999 [...] (1786332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3583656 3583657 3583658 3583659 3583660 3583661 3583662 3583663 3583664 3583665 [...] (1738538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3596146 3596147 3601098 3601099 3601100 3601101 3601102 3601103 3601104 3601105 [...] (1691005 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3722598 3722599 3722600 3722601 3722602 3722603 3722604 3722605 3722606 3722607 [...] (1642935 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3722610 3722611 3722612 3722613 3722614 3722615 3786552 3786553 3786554 3786555 [...] (1595231 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3873021 3873022 3873023 3873561 3873562 3873563 3882960 3882961 3882962 3882963 [...] (1547938 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3882965 3882966 3882967 3882968 3882969 3882970 3882971 3882972 3882973 3882974 [...] (1499976 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3882976 3882977 3891978 3891979 3891980 3891981 3891982 3891983 3891984 3891985 [...] (1451848 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3919894 3919895 3934131 3934132 3934133 3939066 3939067 3939068 3939069 3939070 [...] (1404094 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3919894 3919895 3961818 3961819 3961820 3961821 3961822 3961823 3961824 3961825 [...] (1356271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3998898 3998899 3998900 3998901 3998902 3998903 3998904 3998905 3998906 3998907 [...] (1308253 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4004622 4004623 4004624 4004625 4004626 4004627 4004628 4004629 4004630 4004631 [...] (1260324 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4005016 4005017 4009410 4009411 4009412 4009413 4009414 4009415 4009416 4009417 [...] (1212532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4009410 4009411 4009412 4009413 4009414 4009415 4009416 4009417 4009418 4009419 [...] (1165174 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101660 4101661 4101662 4101663 4101664 4101665 4101666 4101667 4101668 4101669 [...] (1117529 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101660 4101661 4101662 4101663 4101664 4101665 4101666 4101667 4101668 4101669 [...] (1069232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101676 4101677 4115592 4115593 4115594 4115595 4115596 4115597 4115598 4115599 [...] (1021172 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101676 4101677 4115592 4115593 4115594 4115595 4115596 4115597 4115598 4115599 [...] (973334 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4101676 4101677 4115592 4115593 4115594 4115595 4115596 4115597 4115598 4115599 [...] (925497 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4115592 4115593 4115594 4115595 4115596 4115597 4115598 4115599 4115600 4115601 [...] (877654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4115599 4115600 4115601 4115602 4115603 4115604 4115605 4115606 4115607 4115608 [...] (829272 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4115608 4115609 4246830 4246831 4246832 4246833 4246834 4246835 4246836 4246837 [...] (781407 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4303168 4303169 4318740 4318741 4318742 4318743 4318744 4318745 4318746 4318747 [...] (733602 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4303168 4303169 4318740 4318741 4318742 4318743 4318744 4318745 4318746 4318747 [...] (685752 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4318740 4318741 4318742 4318743 4318744 4318745 4318746 4318747 4318748 4318749 [...] (637816 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4318740 4318741 4318742 4318743 4318744 4318745 4318746 4318747 4318748 4318749 [...] (590058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4318740 4318741 4318742 4318743 4318744 4318745 4318746 4318747 4318748 4318749 [...] (542427 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4318756 4318757 4321044 4321045 4321046 4321047 4321048 4321049 4321050 4321051 [...] (494591 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4318756 4318757 4321044 4321045 4321046 4321047 4321048 4321049 4321050 4321051 [...] (446761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4321044 4321045 4321046 4321047 4321048 4321049 4321050 4321051 4321052 4321053 [...] (399447 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4547824 4547825 4568310 4568311 4568312 4568313 4568314 4568315 4568316 4568317 [...] (352607 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4547824 4547825 4666248 4666249 4666250 4666251 4666252 4666253 4666254 4666255 [...] (306107 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4666264 4666265 4697615 4697616 4697617 4697618 4697619 4697620 4697621 4726314 [...] (262003 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (219796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (180994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (144629 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (113600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (86428 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726314 4726315 4726316 4726317 4726318 4726319 4726320 4726321 4726322 4726323 [...] (65330 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4726330 4726331 5202684 5202685 5202686 5202687 5202688 5202689 5202690 5202691 [...] (49652 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5202684 5202685 5202686 5202687 5202688 5202689 5202690 5202691 5202692 5202693 [...] (38808 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5202684 5202685 5202686 5202687 5202688 5202689 5202690 5202691 5202692 5202693 [...] (30723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5202684 5202685 5202686 5202687 5202688 5202689 5202690 5202691 5202692 5202693 [...] (23746 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5515038 5515039 5515040 5515041 5515042 5515043 5515044 5515045 5515046 5515047 [...] (17368 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5515038 5515039 5515040 5515041 5515042 5515043 5515044 5515045 5515046 5515047 [...] (12077 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5847606 5847607 5847608 5847609 5847610 5847611 5847612 5847613 5847614 5847615 [...] (9065 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6148422 6148423 6148424 6148425 6148426 6148427 6148428 6148429 6148430 6148431 [...] (6075 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6150523 6150524 6150525 6150526 6150527 6177330 6177331 6177332 6177333 6177334 [...] (3074 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6340860 6340861 6340862 6340863 6340864 6340865 6340866 6340867 6340868 6340869 [...] (561 flits)
Measured flits: (0 flits)
Time taken is 143837 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9579.81 (1 samples)
	minimum = 24 (1 samples)
	maximum = 45512 (1 samples)
Network latency average = 9106.02 (1 samples)
	minimum = 22 (1 samples)
	maximum = 41289 (1 samples)
Flit latency average = 36810.7 (1 samples)
	minimum = 5 (1 samples)
	maximum = 96942 (1 samples)
Fragmentation average = 378.067 (1 samples)
	minimum = 0 (1 samples)
	maximum = 972 (1 samples)
Injected packet rate average = 0.0365521 (1 samples)
	minimum = 0.0151429 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.0164963 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0208571 (1 samples)
Injected flit rate average = 0.657914 (1 samples)
	minimum = 0.273286 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.30079 (1 samples)
	minimum = 0.226571 (1 samples)
	maximum = 0.382857 (1 samples)
Injected packet size average = 17.9994 (1 samples)
Accepted packet size average = 18.2338 (1 samples)
Hops average = 5.07204 (1 samples)
Total run time 206.374
