BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 373.82
	minimum = 23
	maximum = 997
Network latency average = 277.066
	minimum = 23
	maximum = 980
Slowest packet = 160
Flit latency average = 210.274
	minimum = 6
	maximum = 963
Slowest flit = 2897
Fragmentation average = 144.515
	minimum = 0
	maximum = 798
Injected packet rate average = 0.0287448
	minimum = 0 (at node 75)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0103125
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.51226
	minimum = 0 (at node 75)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.210255
	minimum = 0.079 (at node 41)
	maximum = 0.346 (at node 76)
Injected packet length average = 17.821
Accepted packet length average = 20.3884
Total in-flight flits = 58973 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 643.161
	minimum = 23
	maximum = 1971
Network latency average = 510.181
	minimum = 23
	maximum = 1770
Slowest packet = 242
Flit latency average = 435.75
	minimum = 6
	maximum = 1911
Slowest flit = 6729
Fragmentation average = 183.769
	minimum = 0
	maximum = 1453
Injected packet rate average = 0.0292812
	minimum = 0.005 (at node 181)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0113984
	minimum = 0.007 (at node 8)
	maximum = 0.0175 (at node 137)
Injected flit rate average = 0.524406
	minimum = 0.0885 (at node 181)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.219068
	minimum = 0.139 (at node 30)
	maximum = 0.3305 (at node 137)
Injected packet length average = 17.9093
Accepted packet length average = 19.2191
Total in-flight flits = 118288 (0 measured)
latency change    = 0.418777
throughput change = 0.0402273
Class 0:
Packet latency average = 1406.17
	minimum = 25
	maximum = 2903
Network latency average = 1205.93
	minimum = 25
	maximum = 2861
Slowest packet = 2067
Flit latency average = 1142.44
	minimum = 6
	maximum = 2844
Slowest flit = 9107
Fragmentation average = 225.417
	minimum = 0
	maximum = 1918
Injected packet rate average = 0.0300833
	minimum = 0.001 (at node 11)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0126771
	minimum = 0.006 (at node 20)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.541021
	minimum = 0.018 (at node 59)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.226729
	minimum = 0.115 (at node 36)
	maximum = 0.438 (at node 16)
Injected packet length average = 17.9841
Accepted packet length average = 17.885
Total in-flight flits = 178724 (0 measured)
latency change    = 0.542616
throughput change = 0.0337912
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 484.252
	minimum = 29
	maximum = 3010
Network latency average = 150.823
	minimum = 27
	maximum = 925
Slowest packet = 17052
Flit latency average = 1675.66
	minimum = 6
	maximum = 3603
Slowest flit = 32984
Fragmentation average = 37.4291
	minimum = 1
	maximum = 563
Injected packet rate average = 0.0255365
	minimum = 0 (at node 124)
	maximum = 0.056 (at node 35)
Accepted packet rate average = 0.0121563
	minimum = 0.004 (at node 126)
	maximum = 0.022 (at node 183)
Injected flit rate average = 0.459969
	minimum = 0 (at node 124)
	maximum = 1 (at node 35)
Accepted flit rate average= 0.218083
	minimum = 0.065 (at node 126)
	maximum = 0.36 (at node 39)
Injected packet length average = 18.0122
Accepted packet length average = 17.94
Total in-flight flits = 225124 (82963 measured)
latency change    = 1.9038
throughput change = 0.0396446
Class 0:
Packet latency average = 859.359
	minimum = 28
	maximum = 3910
Network latency average = 481.626
	minimum = 27
	maximum = 1956
Slowest packet = 17052
Flit latency average = 1948.84
	minimum = 6
	maximum = 4668
Slowest flit = 28245
Fragmentation average = 71.236
	minimum = 1
	maximum = 644
Injected packet rate average = 0.0247474
	minimum = 0.002 (at node 124)
	maximum = 0.0555 (at node 65)
Accepted packet rate average = 0.0120078
	minimum = 0.0045 (at node 86)
	maximum = 0.018 (at node 59)
Injected flit rate average = 0.445208
	minimum = 0.036 (at node 124)
	maximum = 1 (at node 65)
Accepted flit rate average= 0.216271
	minimum = 0.0835 (at node 86)
	maximum = 0.3345 (at node 181)
Injected packet length average = 17.9901
Accepted packet length average = 18.0108
Total in-flight flits = 266802 (157068 measured)
latency change    = 0.436496
throughput change = 0.0083807
Class 0:
Packet latency average = 1432.91
	minimum = 27
	maximum = 5080
Network latency average = 1002.88
	minimum = 27
	maximum = 2966
Slowest packet = 17052
Flit latency average = 2215.05
	minimum = 6
	maximum = 5488
Slowest flit = 43874
Fragmentation average = 103.645
	minimum = 0
	maximum = 1097
Injected packet rate average = 0.023217
	minimum = 0.00566667 (at node 154)
	maximum = 0.0473333 (at node 11)
Accepted packet rate average = 0.0119705
	minimum = 0.007 (at node 17)
	maximum = 0.0166667 (at node 59)
Injected flit rate average = 0.417858
	minimum = 0.102 (at node 154)
	maximum = 0.848667 (at node 11)
Accepted flit rate average= 0.215122
	minimum = 0.120667 (at node 17)
	maximum = 0.307667 (at node 181)
Injected packet length average = 17.9979
Accepted packet length average = 17.971
Total in-flight flits = 295690 (212240 measured)
latency change    = 0.40027
throughput change = 0.00534259
Draining remaining packets ...
Class 0:
Remaining flits: 43920 43921 43922 43923 43924 43925 43926 43927 43928 43929 [...] (257810 flits)
Measured flits: 306396 306397 306398 306399 306400 306401 306402 306403 306404 306405 [...] (194128 flits)
Class 0:
Remaining flits: 43920 43921 43922 43923 43924 43925 43926 43927 43928 43929 [...] (220635 flits)
Measured flits: 306432 306433 306434 306435 306436 306437 306438 306439 306440 306441 [...] (172991 flits)
Class 0:
Remaining flits: 47646 47647 47648 47649 47650 47651 47652 47653 47654 47655 [...] (184707 flits)
Measured flits: 306432 306433 306434 306435 306436 306437 306438 306439 306440 306441 [...] (150337 flits)
Class 0:
Remaining flits: 53591 53592 53593 53594 53595 53596 53597 53598 53599 53600 [...] (150022 flits)
Measured flits: 306448 306449 306486 306487 306488 306489 306490 306491 306492 306493 [...] (125852 flits)
Class 0:
Remaining flits: 73692 73693 73694 73695 73696 73697 73698 73699 73700 73701 [...] (116586 flits)
Measured flits: 306486 306487 306488 306489 306490 306491 306492 306493 306494 306495 [...] (99980 flits)
Class 0:
Remaining flits: 104580 104581 104582 104583 104584 104585 104586 104587 104588 104589 [...] (83375 flits)
Measured flits: 306486 306487 306488 306489 306490 306491 306492 306493 306494 306495 [...] (72875 flits)
Class 0:
Remaining flits: 104580 104581 104582 104583 104584 104585 104586 104587 104588 104589 [...] (51262 flits)
Measured flits: 306653 306654 306655 306656 306657 306658 306659 306660 306661 306662 [...] (45707 flits)
Class 0:
Remaining flits: 158004 158005 158006 158007 158008 158009 158010 158011 158012 158013 [...] (22978 flits)
Measured flits: 306774 306775 306776 306777 306778 306779 306780 306781 306782 306783 [...] (20553 flits)
Class 0:
Remaining flits: 209068 209069 210312 210313 210314 210315 210316 210317 210318 210319 [...] (3846 flits)
Measured flits: 307980 307981 307982 307983 307984 307985 307986 307987 307988 307989 [...] (3343 flits)
Class 0:
Remaining flits: 232380 232381 232382 232383 232384 232385 232386 232387 232388 232389 [...] (911 flits)
Measured flits: 309222 309223 309224 309225 309226 309227 309228 309229 309230 309231 [...] (800 flits)
Time taken is 16806 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6263.7 (1 samples)
	minimum = 27 (1 samples)
	maximum = 14446 (1 samples)
Network latency average = 5663.84 (1 samples)
	minimum = 27 (1 samples)
	maximum = 13729 (1 samples)
Flit latency average = 5102.41 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14218 (1 samples)
Fragmentation average = 173.191 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2750 (1 samples)
Injected packet rate average = 0.023217 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0473333 (1 samples)
Accepted packet rate average = 0.0119705 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.417858 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.848667 (1 samples)
Accepted flit rate average = 0.215122 (1 samples)
	minimum = 0.120667 (1 samples)
	maximum = 0.307667 (1 samples)
Injected packet size average = 17.9979 (1 samples)
Accepted packet size average = 17.971 (1 samples)
Hops average = 5.14392 (1 samples)
Total run time 16.6009
