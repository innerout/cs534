BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 359.138
	minimum = 23
	maximum = 981
Network latency average = 246.227
	minimum = 23
	maximum = 862
Slowest packet = 21
Flit latency average = 203.939
	minimum = 6
	maximum = 903
Slowest flit = 7582
Fragmentation average = 52.5254
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0135052
	minimum = 0 (at node 115)
	maximum = 0.028 (at node 75)
Accepted packet rate average = 0.00903125
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 132)
Injected flit rate average = 0.239839
	minimum = 0 (at node 115)
	maximum = 0.504 (at node 75)
Accepted flit rate average= 0.168734
	minimum = 0.036 (at node 41)
	maximum = 0.306 (at node 132)
Injected packet length average = 17.759
Accepted packet length average = 18.6834
Total in-flight flits = 15879 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 681.555
	minimum = 23
	maximum = 1979
Network latency average = 351.05
	minimum = 23
	maximum = 1553
Slowest packet = 21
Flit latency average = 299.888
	minimum = 6
	maximum = 1536
Slowest flit = 16307
Fragmentation average = 54.9126
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0115547
	minimum = 0.0035 (at node 169)
	maximum = 0.0245 (at node 142)
Accepted packet rate average = 0.00914323
	minimum = 0.0045 (at node 30)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.206211
	minimum = 0.063 (at node 169)
	maximum = 0.4375 (at node 142)
Accepted flit rate average= 0.167852
	minimum = 0.084 (at node 30)
	maximum = 0.27 (at node 44)
Injected packet length average = 17.8465
Accepted packet length average = 18.358
Total in-flight flits = 17301 (0 measured)
latency change    = 0.473061
throughput change = 0.00525948
Class 0:
Packet latency average = 1723.03
	minimum = 38
	maximum = 2966
Network latency average = 527.429
	minimum = 23
	maximum = 2209
Slowest packet = 3255
Flit latency average = 465.964
	minimum = 6
	maximum = 2192
Slowest flit = 13661
Fragmentation average = 57.5182
	minimum = 0
	maximum = 136
Injected packet rate average = 0.00935417
	minimum = 0.002 (at node 83)
	maximum = 0.025 (at node 51)
Accepted packet rate average = 0.00916667
	minimum = 0.001 (at node 28)
	maximum = 0.016 (at node 166)
Injected flit rate average = 0.16749
	minimum = 0.026 (at node 83)
	maximum = 0.447 (at node 51)
Accepted flit rate average= 0.165021
	minimum = 0.034 (at node 28)
	maximum = 0.275 (at node 68)
Injected packet length average = 17.9053
Accepted packet length average = 18.0023
Total in-flight flits = 17909 (0 measured)
latency change    = 0.604445
throughput change = 0.0171538
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2546.64
	minimum = 40
	maximum = 3841
Network latency average = 336.904
	minimum = 27
	maximum = 926
Slowest packet = 6404
Flit latency average = 470.887
	minimum = 6
	maximum = 1970
Slowest flit = 24678
Fragmentation average = 52.0561
	minimum = 0
	maximum = 126
Injected packet rate average = 0.00930208
	minimum = 0.001 (at node 32)
	maximum = 0.019 (at node 144)
Accepted packet rate average = 0.00919792
	minimum = 0.003 (at node 38)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.167146
	minimum = 0.018 (at node 188)
	maximum = 0.329 (at node 156)
Accepted flit rate average= 0.165661
	minimum = 0.04 (at node 190)
	maximum = 0.299 (at node 16)
Injected packet length average = 17.9686
Accepted packet length average = 18.0108
Total in-flight flits = 18034 (16941 measured)
latency change    = 0.323408
throughput change = 0.00386707
Class 0:
Packet latency average = 2986.31
	minimum = 40
	maximum = 4872
Network latency average = 453.717
	minimum = 23
	maximum = 1812
Slowest packet = 6404
Flit latency average = 471.987
	minimum = 6
	maximum = 2178
Slowest flit = 98801
Fragmentation average = 54.8357
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00927083
	minimum = 0.002 (at node 22)
	maximum = 0.017 (at node 66)
Accepted packet rate average = 0.00930208
	minimum = 0.005 (at node 22)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.167052
	minimum = 0.036 (at node 22)
	maximum = 0.3105 (at node 122)
Accepted flit rate average= 0.167599
	minimum = 0.083 (at node 190)
	maximum = 0.311 (at node 16)
Injected packet length average = 18.0191
Accepted packet length average = 18.0174
Total in-flight flits = 17595 (17577 measured)
latency change    = 0.14723
throughput change = 0.0115603
Class 0:
Packet latency average = 3362.58
	minimum = 40
	maximum = 5763
Network latency average = 491.645
	minimum = 23
	maximum = 2720
Slowest packet = 6404
Flit latency average = 475.601
	minimum = 6
	maximum = 2699
Slowest flit = 122507
Fragmentation average = 58.1974
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0092309
	minimum = 0.00233333 (at node 22)
	maximum = 0.0143333 (at node 175)
Accepted packet rate average = 0.00918229
	minimum = 0.00533333 (at node 17)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.166167
	minimum = 0.042 (at node 22)
	maximum = 0.263333 (at node 175)
Accepted flit rate average= 0.165476
	minimum = 0.0983333 (at node 4)
	maximum = 0.276333 (at node 16)
Injected packet length average = 18.0011
Accepted packet length average = 18.0212
Total in-flight flits = 18229 (18229 measured)
latency change    = 0.111899
throughput change = 0.0128313
Class 0:
Packet latency average = 3739.62
	minimum = 40
	maximum = 6764
Network latency average = 508.127
	minimum = 23
	maximum = 2846
Slowest packet = 6404
Flit latency average = 478.154
	minimum = 6
	maximum = 2763
Slowest flit = 123218
Fragmentation average = 57.3927
	minimum = 0
	maximum = 162
Injected packet rate average = 0.00916797
	minimum = 0.00275 (at node 22)
	maximum = 0.01475 (at node 175)
Accepted packet rate average = 0.00919531
	minimum = 0.0055 (at node 4)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.165311
	minimum = 0.04575 (at node 22)
	maximum = 0.2695 (at node 175)
Accepted flit rate average= 0.16537
	minimum = 0.0955 (at node 4)
	maximum = 0.25675 (at node 16)
Injected packet length average = 18.0314
Accepted packet length average = 17.9841
Total in-flight flits = 17823 (17823 measured)
latency change    = 0.100823
throughput change = 0.0006404
Class 0:
Packet latency average = 4116.29
	minimum = 40
	maximum = 7765
Network latency average = 514.059
	minimum = 23
	maximum = 2846
Slowest packet = 6404
Flit latency average = 476.992
	minimum = 6
	maximum = 2763
Slowest flit = 123218
Fragmentation average = 56.9223
	minimum = 0
	maximum = 162
Injected packet rate average = 0.00914792
	minimum = 0.0026 (at node 22)
	maximum = 0.0144 (at node 175)
Accepted packet rate average = 0.00916771
	minimum = 0.0066 (at node 132)
	maximum = 0.0136 (at node 16)
Injected flit rate average = 0.164848
	minimum = 0.046 (at node 22)
	maximum = 0.2624 (at node 175)
Accepted flit rate average= 0.165006
	minimum = 0.1188 (at node 132)
	maximum = 0.2422 (at node 16)
Injected packet length average = 18.0203
Accepted packet length average = 17.9986
Total in-flight flits = 17687 (17687 measured)
latency change    = 0.0915065
throughput change = 0.0022032
Class 0:
Packet latency average = 4484.37
	minimum = 40
	maximum = 8491
Network latency average = 521.479
	minimum = 23
	maximum = 3187
Slowest packet = 6404
Flit latency average = 479.418
	minimum = 6
	maximum = 3138
Slowest flit = 198395
Fragmentation average = 57.7193
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0090816
	minimum = 0.00316667 (at node 22)
	maximum = 0.0141667 (at node 119)
Accepted packet rate average = 0.00909288
	minimum = 0.00633333 (at node 36)
	maximum = 0.0125 (at node 128)
Injected flit rate average = 0.163442
	minimum = 0.057 (at node 22)
	maximum = 0.255167 (at node 119)
Accepted flit rate average= 0.163536
	minimum = 0.111333 (at node 36)
	maximum = 0.225833 (at node 128)
Injected packet length average = 17.997
Accepted packet length average = 17.9851
Total in-flight flits = 17831 (17831 measured)
latency change    = 0.082081
throughput change = 0.00898755
Class 0:
Packet latency average = 4861.18
	minimum = 40
	maximum = 9554
Network latency average = 526.633
	minimum = 23
	maximum = 3187
Slowest packet = 6404
Flit latency average = 480.659
	minimum = 6
	maximum = 3138
Slowest flit = 198395
Fragmentation average = 58.3347
	minimum = 0
	maximum = 162
Injected packet rate average = 0.00902976
	minimum = 0.00414286 (at node 22)
	maximum = 0.0148571 (at node 119)
Accepted packet rate average = 0.00905804
	minimum = 0.00614286 (at node 36)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.162711
	minimum = 0.0745714 (at node 22)
	maximum = 0.268857 (at node 119)
Accepted flit rate average= 0.163033
	minimum = 0.109429 (at node 36)
	maximum = 0.218429 (at node 128)
Injected packet length average = 18.0194
Accepted packet length average = 17.9988
Total in-flight flits = 17438 (17438 measured)
latency change    = 0.0775129
throughput change = 0.00308511
Draining all recorded packets ...
Class 0:
Remaining flits: 267732 267733 267734 267735 267736 267737 267738 267739 267740 267741 [...] (17468 flits)
Measured flits: 267732 267733 267734 267735 267736 267737 267738 267739 267740 267741 [...] (17468 flits)
Class 0:
Remaining flits: 298782 298783 298784 298785 298786 298787 298788 298789 298790 298791 [...] (17834 flits)
Measured flits: 298782 298783 298784 298785 298786 298787 298788 298789 298790 298791 [...] (17834 flits)
Class 0:
Remaining flits: 353346 353347 353348 353349 353350 353351 353352 353353 353354 353355 [...] (17568 flits)
Measured flits: 353346 353347 353348 353349 353350 353351 353352 353353 353354 353355 [...] (17568 flits)
Class 0:
Remaining flits: 377352 377353 377354 377355 377356 377357 377358 377359 377360 377361 [...] (17688 flits)
Measured flits: 377352 377353 377354 377355 377356 377357 377358 377359 377360 377361 [...] (17688 flits)
Class 0:
Remaining flits: 394541 406505 406506 406507 406508 406509 406510 406511 417790 417791 [...] (17955 flits)
Measured flits: 394541 406505 406506 406507 406508 406509 406510 406511 417790 417791 [...] (17955 flits)
Class 0:
Remaining flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (17736 flits)
Measured flits: 443700 443701 443702 443703 443704 443705 443706 443707 443708 443709 [...] (17736 flits)
Class 0:
Remaining flits: 490140 490141 490142 490143 490144 490145 490146 490147 490148 490149 [...] (17844 flits)
Measured flits: 490140 490141 490142 490143 490144 490145 490146 490147 490148 490149 [...] (17844 flits)
Class 0:
Remaining flits: 501454 501455 501456 501457 501458 501459 501460 501461 505049 505050 [...] (17197 flits)
Measured flits: 501454 501455 501456 501457 501458 501459 501460 501461 505049 505050 [...] (17197 flits)
Class 0:
Remaining flits: 529632 529633 529634 529635 529636 529637 529638 529639 529640 529641 [...] (17770 flits)
Measured flits: 529632 529633 529634 529635 529636 529637 529638 529639 529640 529641 [...] (17770 flits)
Class 0:
Remaining flits: 567198 567199 567200 567201 567202 567203 567204 567205 567206 567207 [...] (17599 flits)
Measured flits: 567198 567199 567200 567201 567202 567203 567204 567205 567206 567207 [...] (17599 flits)
Class 0:
Remaining flits: 604854 604855 604856 604857 604858 604859 604860 604861 604862 604863 [...] (17729 flits)
Measured flits: 604854 604855 604856 604857 604858 604859 604860 604861 604862 604863 [...] (17729 flits)
Class 0:
Remaining flits: 638532 638533 638534 638535 638536 638537 638538 638539 638540 638541 [...] (17572 flits)
Measured flits: 638532 638533 638534 638535 638536 638537 638538 638539 638540 638541 [...] (17518 flits)
Class 0:
Remaining flits: 675558 675559 675560 675561 675562 675563 675564 675565 675566 675567 [...] (17657 flits)
Measured flits: 675558 675559 675560 675561 675562 675563 675564 675565 675566 675567 [...] (17235 flits)
Class 0:
Remaining flits: 702864 702865 702866 702867 702868 702869 702870 702871 702872 702873 [...] (17785 flits)
Measured flits: 702864 702865 702866 702867 702868 702869 702870 702871 702872 702873 [...] (17203 flits)
Class 0:
Remaining flits: 721566 721567 721568 721569 721570 721571 721572 721573 721574 721575 [...] (17656 flits)
Measured flits: 721566 721567 721568 721569 721570 721571 721572 721573 721574 721575 [...] (16556 flits)
Class 0:
Remaining flits: 738180 738181 738182 738183 738184 738185 738186 738187 738188 738189 [...] (17673 flits)
Measured flits: 738180 738181 738182 738183 738184 738185 738186 738187 738188 738189 [...] (16405 flits)
Class 0:
Remaining flits: 774872 774873 774874 774875 774876 774877 774878 774879 774880 774881 [...] (17737 flits)
Measured flits: 774872 774873 774874 774875 774876 774877 774878 774879 774880 774881 [...] (16255 flits)
Class 0:
Remaining flits: 817223 817224 817225 817226 817227 817228 817229 817230 817231 817232 [...] (17739 flits)
Measured flits: 817223 817224 817225 817226 817227 817228 817229 817230 817231 817232 [...] (15802 flits)
Class 0:
Remaining flits: 845046 845047 845048 845049 845050 845051 845052 845053 845054 845055 [...] (17950 flits)
Measured flits: 845046 845047 845048 845049 845050 845051 845052 845053 845054 845055 [...] (15691 flits)
Class 0:
Remaining flits: 890352 890353 890354 890355 890356 890357 890358 890359 890360 890361 [...] (17890 flits)
Measured flits: 890352 890353 890354 890355 890356 890357 890358 890359 890360 890361 [...] (15393 flits)
Class 0:
Remaining flits: 918122 918123 918124 918125 920862 920863 920864 920865 920866 920867 [...] (17309 flits)
Measured flits: 918122 918123 918124 918125 920862 920863 920864 920865 920866 920867 [...] (14284 flits)
Class 0:
Remaining flits: 931977 931978 931979 931980 931981 931982 931983 931984 931985 935919 [...] (17199 flits)
Measured flits: 931977 931978 931979 931980 931981 931982 931983 931984 931985 935919 [...] (13585 flits)
Class 0:
Remaining flits: 949176 949177 949178 949179 949180 949181 949182 949183 949184 949185 [...] (17580 flits)
Measured flits: 960822 960823 960824 960825 960826 960827 960828 960829 960830 960831 [...] (13478 flits)
Class 0:
Remaining flits: 949176 949177 949178 949179 949180 949181 949182 949183 949184 949185 [...] (17421 flits)
Measured flits: 970326 970327 970328 970329 970330 970331 970332 970333 970334 970335 [...] (12564 flits)
Class 0:
Remaining flits: 978174 978175 978176 978177 978178 978179 978180 978181 978182 978183 [...] (17877 flits)
Measured flits: 978174 978175 978176 978177 978178 978179 978180 978181 978182 978183 [...] (12101 flits)
Class 0:
Remaining flits: 1047024 1047025 1047026 1047027 1047028 1047029 1047030 1047031 1047032 1047033 [...] (17691 flits)
Measured flits: 1047024 1047025 1047026 1047027 1047028 1047029 1047030 1047031 1047032 1047033 [...] (11298 flits)
Class 0:
Remaining flits: 1072458 1072459 1072460 1072461 1072462 1072463 1072464 1072465 1072466 1072467 [...] (18195 flits)
Measured flits: 1072458 1072459 1072460 1072461 1072462 1072463 1072464 1072465 1072466 1072467 [...] (10612 flits)
Class 0:
Remaining flits: 1074690 1074691 1074692 1074693 1074694 1074695 1074696 1074697 1074698 1074699 [...] (17714 flits)
Measured flits: 1074690 1074691 1074692 1074693 1074694 1074695 1074696 1074697 1074698 1074699 [...] (9494 flits)
Class 0:
Remaining flits: 1156446 1156447 1156448 1156449 1156450 1156451 1156452 1156453 1156454 1156455 [...] (17996 flits)
Measured flits: 1156446 1156447 1156448 1156449 1156450 1156451 1156452 1156453 1156454 1156455 [...] (8647 flits)
Class 0:
Remaining flits: 1167264 1167265 1167266 1167267 1167268 1167269 1167270 1167271 1167272 1167273 [...] (17841 flits)
Measured flits: 1197021 1197022 1197023 1197024 1197025 1197026 1197027 1197028 1197029 1197030 [...] (8099 flits)
Class 0:
Remaining flits: 1199687 1199688 1199689 1199690 1199691 1199692 1199693 1199694 1199695 1199696 [...] (17873 flits)
Measured flits: 1199687 1199688 1199689 1199690 1199691 1199692 1199693 1199694 1199695 1199696 [...] (7356 flits)
Class 0:
Remaining flits: 1262682 1262683 1262684 1262685 1262686 1262687 1262688 1262689 1262690 1262691 [...] (17918 flits)
Measured flits: 1267200 1267201 1267202 1267203 1267204 1267205 1267206 1267207 1267208 1267209 [...] (6396 flits)
Class 0:
Remaining flits: 1286500 1286501 1286502 1286503 1286504 1286505 1286506 1286507 1286508 1286509 [...] (17712 flits)
Measured flits: 1287972 1287973 1287974 1287975 1287976 1287977 1287978 1287979 1287980 1287981 [...] (5755 flits)
Class 0:
Remaining flits: 1312506 1312507 1312508 1312509 1312510 1312511 1312512 1312513 1312514 1312515 [...] (17753 flits)
Measured flits: 1312506 1312507 1312508 1312509 1312510 1312511 1312512 1312513 1312514 1312515 [...] (5152 flits)
Class 0:
Remaining flits: 1347696 1347697 1347698 1347699 1347700 1347701 1347702 1347703 1347704 1347705 [...] (17759 flits)
Measured flits: 1347696 1347697 1347698 1347699 1347700 1347701 1347702 1347703 1347704 1347705 [...] (4522 flits)
Class 0:
Remaining flits: 1349370 1349371 1349372 1349373 1349374 1349375 1349376 1349377 1349378 1349379 [...] (17833 flits)
Measured flits: 1349370 1349371 1349372 1349373 1349374 1349375 1349376 1349377 1349378 1349379 [...] (3710 flits)
Class 0:
Remaining flits: 1399626 1399627 1399628 1399629 1399630 1399631 1399632 1399633 1399634 1399635 [...] (17633 flits)
Measured flits: 1416402 1416403 1416404 1416405 1416406 1416407 1416408 1416409 1416410 1416411 [...] (2942 flits)
Class 0:
Remaining flits: 1408122 1408123 1408124 1408125 1408126 1408127 1408128 1408129 1408130 1408131 [...] (17788 flits)
Measured flits: 1453231 1453232 1453233 1453234 1453235 1453236 1453237 1453238 1453239 1453240 [...] (2407 flits)
Class 0:
Remaining flits: 1451983 1451984 1451985 1451986 1451987 1455246 1455247 1455248 1455249 1455250 [...] (17482 flits)
Measured flits: 1455246 1455247 1455248 1455249 1455250 1455251 1455252 1455253 1455254 1455255 [...] (2192 flits)
Class 0:
Remaining flits: 1465992 1465993 1465994 1465995 1465996 1465997 1465998 1465999 1466000 1466001 [...] (17670 flits)
Measured flits: 1472472 1472473 1472474 1472475 1472476 1472477 1472478 1472479 1472480 1472481 [...] (1901 flits)
Class 0:
Remaining flits: 1521968 1521969 1521970 1521971 1529604 1529605 1529606 1529607 1529608 1529609 [...] (17999 flits)
Measured flits: 1529604 1529605 1529606 1529607 1529608 1529609 1529610 1529611 1529612 1529613 [...] (1416 flits)
Class 0:
Remaining flits: 1558386 1558387 1558388 1558389 1558390 1558391 1558392 1558393 1558394 1558395 [...] (17902 flits)
Measured flits: 1566972 1566973 1566974 1566975 1566976 1566977 1566978 1566979 1566980 1566981 [...] (1273 flits)
Class 0:
Remaining flits: 1604709 1604710 1604711 1604712 1604713 1604714 1604715 1604716 1604717 1604718 [...] (17726 flits)
Measured flits: 1628208 1628209 1628210 1628211 1628212 1628213 1628214 1628215 1628216 1628217 [...] (1153 flits)
Class 0:
Remaining flits: 1611072 1611073 1611074 1611075 1611076 1611077 1611078 1611079 1611080 1611081 [...] (17576 flits)
Measured flits: 1646460 1646461 1646462 1646463 1646464 1646465 1646466 1646467 1646468 1646469 [...] (837 flits)
Class 0:
Remaining flits: 1618434 1618435 1618436 1618437 1618438 1618439 1618440 1618441 1618442 1618443 [...] (17495 flits)
Measured flits: 1654958 1654959 1654960 1654961 1654962 1654963 1654964 1654965 1654966 1654967 [...] (560 flits)
Class 0:
Remaining flits: 1684440 1684441 1684442 1684443 1684444 1684445 1684446 1684447 1684448 1684449 [...] (18129 flits)
Measured flits: 1698552 1698553 1698554 1698555 1698556 1698557 1698558 1698559 1698560 1698561 [...] (431 flits)
Class 0:
Remaining flits: 1715126 1715127 1715128 1715129 1715267 1715268 1715269 1715270 1715271 1715272 [...] (17984 flits)
Measured flits: 1735830 1735831 1735832 1735833 1735834 1735835 1735836 1735837 1735838 1735839 [...] (283 flits)
Class 0:
Remaining flits: 1739592 1739593 1739594 1739595 1739596 1739597 1739598 1739599 1739600 1739601 [...] (17481 flits)
Measured flits: 1779696 1779697 1779698 1779699 1779700 1779701 1779702 1779703 1779704 1779705 [...] (223 flits)
Class 0:
Remaining flits: 1764504 1764505 1764506 1764507 1764508 1764509 1764510 1764511 1764512 1764513 [...] (17821 flits)
Measured flits: 1806150 1806151 1806152 1806153 1806154 1806155 1808388 1808389 1808390 1808391 [...] (175 flits)
Class 0:
Remaining flits: 1804140 1804141 1804142 1804143 1804144 1804145 1804146 1804147 1804148 1804149 [...] (17483 flits)
Measured flits: 1852686 1852687 1852688 1852689 1852690 1852691 1852692 1852693 1852694 1852695 [...] (160 flits)
Class 0:
Remaining flits: 1839026 1839027 1839028 1839029 1839030 1839031 1839032 1839033 1839034 1839035 [...] (18097 flits)
Measured flits: 1876842 1876843 1876844 1876845 1876846 1876847 1876848 1876849 1876850 1876851 [...] (103 flits)
Draining remaining packets ...
Time taken is 62128 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 17094.6 (1 samples)
	minimum = 40 (1 samples)
	maximum = 51836 (1 samples)
Network latency average = 547.827 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3946 (1 samples)
Flit latency average = 489.323 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3864 (1 samples)
Fragmentation average = 58.247 (1 samples)
	minimum = 0 (1 samples)
	maximum = 162 (1 samples)
Injected packet rate average = 0.00902976 (1 samples)
	minimum = 0.00414286 (1 samples)
	maximum = 0.0148571 (1 samples)
Accepted packet rate average = 0.00905804 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.162711 (1 samples)
	minimum = 0.0745714 (1 samples)
	maximum = 0.268857 (1 samples)
Accepted flit rate average = 0.163033 (1 samples)
	minimum = 0.109429 (1 samples)
	maximum = 0.218429 (1 samples)
Injected packet size average = 18.0194 (1 samples)
Accepted packet size average = 17.9988 (1 samples)
Hops average = 5.06212 (1 samples)
Total run time 45.9637
