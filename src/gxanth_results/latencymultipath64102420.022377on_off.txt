BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 273.118
	minimum = 22
	maximum = 939
Network latency average = 195.608
	minimum = 22
	maximum = 747
Slowest packet = 33
Flit latency average = 166.283
	minimum = 5
	maximum = 747
Slowest flit = 16512
Fragmentation average = 35.7032
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0127396
	minimum = 0.003 (at node 174)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.237979
	minimum = 0.081 (at node 174)
	maximum = 0.395 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6803
Total in-flight flits = 27082 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 441.234
	minimum = 22
	maximum = 1735
Network latency average = 337.051
	minimum = 22
	maximum = 1376
Slowest packet = 33
Flit latency average = 306.677
	minimum = 5
	maximum = 1413
Slowest flit = 37999
Fragmentation average = 49.3536
	minimum = 0
	maximum = 632
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0136901
	minimum = 0.008 (at node 81)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253096
	minimum = 0.144 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 18.4875
Total in-flight flits = 53219 (0 measured)
latency change    = 0.381013
throughput change = 0.059729
Class 0:
Packet latency average = 961.88
	minimum = 22
	maximum = 2514
Network latency average = 801.966
	minimum = 22
	maximum = 2174
Slowest packet = 3488
Flit latency average = 758.516
	minimum = 5
	maximum = 2201
Slowest flit = 56593
Fragmentation average = 77.5543
	minimum = 0
	maximum = 698
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0152969
	minimum = 0.005 (at node 135)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.274599
	minimum = 0.108 (at node 135)
	maximum = 0.51 (at node 16)
Injected packet length average = 18
Accepted packet length average = 17.9513
Total in-flight flits = 78400 (0 measured)
latency change    = 0.54128
throughput change = 0.0783055
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 360.386
	minimum = 22
	maximum = 1313
Network latency average = 228.795
	minimum = 22
	maximum = 992
Slowest packet = 12702
Flit latency average = 1059.41
	minimum = 5
	maximum = 2773
Slowest flit = 88856
Fragmentation average = 18.4801
	minimum = 0
	maximum = 215
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0153229
	minimum = 0.007 (at node 179)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.275198
	minimum = 0.127 (at node 179)
	maximum = 0.466 (at node 129)
Injected packet length average = 17.9946
Accepted packet length average = 17.9599
Total in-flight flits = 101828 (67278 measured)
latency change    = 1.66903
throughput change = 0.00217646
Class 0:
Packet latency average = 832.83
	minimum = 22
	maximum = 2182
Network latency average = 714.281
	minimum = 22
	maximum = 1985
Slowest packet = 12702
Flit latency average = 1207.56
	minimum = 5
	maximum = 3616
Slowest flit = 101634
Fragmentation average = 34.4221
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0152708
	minimum = 0.0085 (at node 22)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.275659
	minimum = 0.164 (at node 22)
	maximum = 0.4305 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0513
Total in-flight flits = 125457 (114964 measured)
latency change    = 0.567276
throughput change = 0.00167213
Class 0:
Packet latency average = 1250.1
	minimum = 22
	maximum = 3203
Network latency average = 1129.9
	minimum = 22
	maximum = 2982
Slowest packet = 12702
Flit latency average = 1363.92
	minimum = 5
	maximum = 4282
Slowest flit = 125831
Fragmentation average = 51.1329
	minimum = 0
	maximum = 651
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0152535
	minimum = 0.01 (at node 84)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.275453
	minimum = 0.189 (at node 84)
	maximum = 0.383 (at node 138)
Injected packet length average = 18.0055
Accepted packet length average = 18.0584
Total in-flight flits = 146791 (145342 measured)
latency change    = 0.333791
throughput change = 0.000746875
Class 0:
Packet latency average = 1584.89
	minimum = 22
	maximum = 4305
Network latency average = 1454.83
	minimum = 22
	maximum = 3996
Slowest packet = 12702
Flit latency average = 1516.1
	minimum = 5
	maximum = 5071
Slowest flit = 143096
Fragmentation average = 69.5155
	minimum = 0
	maximum = 760
Injected packet rate average = 0.0222187
	minimum = 0.00625 (at node 90)
	maximum = 0.04875 (at node 9)
Accepted packet rate average = 0.01525
	minimum = 0.01025 (at node 84)
	maximum = 0.0205 (at node 123)
Injected flit rate average = 0.39987
	minimum = 0.1125 (at node 90)
	maximum = 0.878 (at node 9)
Accepted flit rate average= 0.275999
	minimum = 0.18875 (at node 84)
	maximum = 0.3645 (at node 103)
Injected packet length average = 17.997
Accepted packet length average = 18.0983
Total in-flight flits = 173585 (173426 measured)
latency change    = 0.211236
throughput change = 0.00197672
Class 0:
Packet latency average = 1851.43
	minimum = 22
	maximum = 5055
Network latency average = 1712.18
	minimum = 22
	maximum = 4943
Slowest packet = 12702
Flit latency average = 1670.32
	minimum = 5
	maximum = 5653
Slowest flit = 164087
Fragmentation average = 79.0817
	minimum = 0
	maximum = 765
Injected packet rate average = 0.0221542
	minimum = 0.008 (at node 136)
	maximum = 0.044 (at node 117)
Accepted packet rate average = 0.0153542
	minimum = 0.0106 (at node 104)
	maximum = 0.0196 (at node 103)
Injected flit rate average = 0.398649
	minimum = 0.144 (at node 136)
	maximum = 0.7894 (at node 117)
Accepted flit rate average= 0.276843
	minimum = 0.1948 (at node 104)
	maximum = 0.352 (at node 123)
Injected packet length average = 17.9943
Accepted packet length average = 18.0305
Total in-flight flits = 195455 (195402 measured)
latency change    = 0.143966
throughput change = 0.0030487
Class 0:
Packet latency average = 2055.14
	minimum = 22
	maximum = 5710
Network latency average = 1913.07
	minimum = 22
	maximum = 5595
Slowest packet = 13979
Flit latency average = 1821.71
	minimum = 5
	maximum = 5881
Slowest flit = 234844
Fragmentation average = 83.3453
	minimum = 0
	maximum = 851
Injected packet rate average = 0.0222352
	minimum = 0.00816667 (at node 136)
	maximum = 0.0435 (at node 117)
Accepted packet rate average = 0.0153307
	minimum = 0.0115 (at node 104)
	maximum = 0.0195 (at node 138)
Injected flit rate average = 0.400199
	minimum = 0.147 (at node 136)
	maximum = 0.7815 (at node 117)
Accepted flit rate average= 0.276693
	minimum = 0.211667 (at node 104)
	maximum = 0.348167 (at node 138)
Injected packet length average = 17.9984
Accepted packet length average = 18.0482
Total in-flight flits = 220720 (220720 measured)
latency change    = 0.0991227
throughput change = 0.000542118
Class 0:
Packet latency average = 2231.31
	minimum = 22
	maximum = 6134
Network latency average = 2090.72
	minimum = 22
	maximum = 5919
Slowest packet = 13046
Flit latency average = 1965.72
	minimum = 5
	maximum = 5902
Slowest flit = 234845
Fragmentation average = 86.8143
	minimum = 0
	maximum = 851
Injected packet rate average = 0.0224353
	minimum = 0.00985714 (at node 90)
	maximum = 0.04 (at node 117)
Accepted packet rate average = 0.0153557
	minimum = 0.0111429 (at node 104)
	maximum = 0.0187143 (at node 138)
Injected flit rate average = 0.403841
	minimum = 0.176143 (at node 90)
	maximum = 0.717571 (at node 117)
Accepted flit rate average= 0.277004
	minimum = 0.201857 (at node 104)
	maximum = 0.337571 (at node 138)
Injected packet length average = 18.0003
Accepted packet length average = 18.0392
Total in-flight flits = 248861 (248861 measured)
latency change    = 0.078954
throughput change = 0.00112277
Draining all recorded packets ...
Class 0:
Remaining flits: 394919 403362 403363 403364 403365 403366 403367 403368 403369 403370 [...] (275402 flits)
Measured flits: 394919 403362 403363 403364 403365 403366 403367 403368 403369 403370 [...] (212928 flits)
Class 0:
Remaining flits: 475596 475597 475598 475599 475600 475601 475602 475603 475604 475605 [...] (299047 flits)
Measured flits: 475596 475597 475598 475599 475600 475601 475602 475603 475604 475605 [...] (166064 flits)
Class 0:
Remaining flits: 517887 517888 517889 517890 517891 517892 517893 517894 517895 533898 [...] (322289 flits)
Measured flits: 517887 517888 517889 517890 517891 517892 517893 517894 517895 533898 [...] (120348 flits)
Class 0:
Remaining flits: 543870 543871 543872 543873 543874 543875 543876 543877 543878 543879 [...] (349051 flits)
Measured flits: 543870 543871 543872 543873 543874 543875 543876 543877 543878 543879 [...] (77686 flits)
Class 0:
Remaining flits: 552042 552043 552044 552045 552046 552047 552048 552049 552050 552051 [...] (376006 flits)
Measured flits: 552042 552043 552044 552045 552046 552047 552048 552049 552050 552051 [...] (41342 flits)
Class 0:
Remaining flits: 644112 644113 644114 644115 644116 644117 644118 644119 644120 644121 [...] (400630 flits)
Measured flits: 644112 644113 644114 644115 644116 644117 644118 644119 644120 644121 [...] (18296 flits)
Class 0:
Remaining flits: 679732 679733 685443 685444 685445 685446 685447 685448 685449 685450 [...] (419425 flits)
Measured flits: 679732 679733 685443 685444 685445 685446 685447 685448 685449 685450 [...] (7136 flits)
Class 0:
Remaining flits: 714975 714976 714977 723055 723056 723057 723058 723059 731574 731575 [...] (442065 flits)
Measured flits: 714975 714976 714977 723055 723056 723057 723058 723059 731574 731575 [...] (2547 flits)
Class 0:
Remaining flits: 772665 772666 772667 773797 773798 773799 773800 773801 775394 775395 [...] (461892 flits)
Measured flits: 772665 772666 772667 773797 773798 773799 773800 773801 775394 775395 [...] (452 flits)
Class 0:
Remaining flits: 782995 782996 782997 782998 782999 789917 789918 789919 789920 789921 [...] (482786 flits)
Measured flits: 789917 789918 789919 789920 789921 789922 789923 789924 789925 789926 [...] (21 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 832224 832225 832226 832227 832228 832229 839315 839316 839317 839318 [...] (450187 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860202 860203 860204 860205 860206 860207 860208 860209 860210 860211 [...] (402961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 955525 955526 955527 955528 955529 964690 964691 965754 965755 965756 [...] (356013 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 984307 984308 984309 984310 984311 994428 994429 994430 994431 994432 [...] (308567 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1028375 1042327 1042328 1042329 1042330 1042331 1042332 1042333 1042334 1042335 [...] (260938 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1081559 1081560 1081561 1081562 1081563 1081564 1081565 1104660 1104661 1104662 [...] (213428 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1137542 1137543 1137544 1137545 1150866 1150867 1150868 1150869 1150870 1150871 [...] (166367 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1200868 1200869 1208700 1208701 1208702 1208703 1208704 1208705 1208706 1208707 [...] (120469 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1256346 1256347 1256348 1256349 1256350 1256351 1256352 1256353 1256354 1256355 [...] (77550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1316412 1316413 1316414 1316415 1316416 1316417 1316418 1316419 1316420 1316421 [...] (40761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1373456 1373457 1373458 1373459 1373460 1373461 1373462 1373463 1373464 1373465 [...] (17060 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1412482 1412483 1412484 1412485 1412486 1412487 1412488 1412489 1412490 1412491 [...] (6540 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1438458 1438459 1438460 1438461 1438462 1438463 1438464 1438465 1438466 1438467 [...] (2667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1492815 1492816 1492817 1492818 1492819 1492820 1492821 1492822 1492823 1492824 [...] (1554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1550320 1550321 1561665 1561666 1561667 1561668 1561669 1561670 1561671 1561672 [...] (555 flits)
Measured flits: (0 flits)
Time taken is 36142 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3487.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10636 (1 samples)
Network latency average = 3336.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10338 (1 samples)
Flit latency average = 5449.42 (1 samples)
	minimum = 5 (1 samples)
	maximum = 15724 (1 samples)
Fragmentation average = 106.755 (1 samples)
	minimum = 0 (1 samples)
	maximum = 863 (1 samples)
Injected packet rate average = 0.0224353 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0153557 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.403841 (1 samples)
	minimum = 0.176143 (1 samples)
	maximum = 0.717571 (1 samples)
Accepted flit rate average = 0.277004 (1 samples)
	minimum = 0.201857 (1 samples)
	maximum = 0.337571 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0392 (1 samples)
Hops average = 5.07806 (1 samples)
Total run time 33.8143
