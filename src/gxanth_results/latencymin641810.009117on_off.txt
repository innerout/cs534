BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 204.255
	minimum = 23
	maximum = 839
Network latency average = 170.626
	minimum = 23
	maximum = 748
Slowest packet = 306
Flit latency average = 106.026
	minimum = 6
	maximum = 839
Slowest flit = 6945
Fragmentation average = 98.1364
	minimum = 0
	maximum = 653
Injected packet rate average = 0.00950521
	minimum = 0 (at node 34)
	maximum = 0.034 (at node 127)
Accepted packet rate average = 0.00767708
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 91)
Injected flit rate average = 0.169833
	minimum = 0 (at node 34)
	maximum = 0.599 (at node 127)
Accepted flit rate average= 0.148865
	minimum = 0.018 (at node 41)
	maximum = 0.27 (at node 91)
Injected packet length average = 17.8674
Accepted packet length average = 19.3908
Total in-flight flits = 4268 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 233.292
	minimum = 23
	maximum = 1233
Network latency average = 196.091
	minimum = 23
	maximum = 1143
Slowest packet = 159
Flit latency average = 124.953
	minimum = 6
	maximum = 1394
Slowest flit = 20730
Fragmentation average = 110.955
	minimum = 0
	maximum = 1051
Injected packet rate average = 0.00915365
	minimum = 0.001 (at node 181)
	maximum = 0.02 (at node 29)
Accepted packet rate average = 0.00816927
	minimum = 0.0045 (at node 25)
	maximum = 0.0135 (at node 44)
Injected flit rate average = 0.164042
	minimum = 0.018 (at node 181)
	maximum = 0.36 (at node 29)
Accepted flit rate average= 0.151974
	minimum = 0.087 (at node 30)
	maximum = 0.243 (at node 44)
Injected packet length average = 17.9209
Accepted packet length average = 18.6031
Total in-flight flits = 4912 (0 measured)
latency change    = 0.124466
throughput change = 0.0204599
Class 0:
Packet latency average = 286.228
	minimum = 26
	maximum = 1804
Network latency average = 246.877
	minimum = 23
	maximum = 1776
Slowest packet = 1417
Flit latency average = 164.462
	minimum = 6
	maximum = 1759
Slowest flit = 25523
Fragmentation average = 137.024
	minimum = 0
	maximum = 952
Injected packet rate average = 0.00919792
	minimum = 0 (at node 12)
	maximum = 0.033 (at node 19)
Accepted packet rate average = 0.00836979
	minimum = 0.001 (at node 153)
	maximum = 0.018 (at node 179)
Injected flit rate average = 0.165583
	minimum = 0 (at node 12)
	maximum = 0.594 (at node 19)
Accepted flit rate average= 0.154812
	minimum = 0.018 (at node 153)
	maximum = 0.311 (at node 179)
Injected packet length average = 18.0023
Accepted packet length average = 18.4966
Total in-flight flits = 6976 (0 measured)
latency change    = 0.184945
throughput change = 0.0183354
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 246.695
	minimum = 25
	maximum = 1152
Network latency average = 203.119
	minimum = 23
	maximum = 939
Slowest packet = 5298
Flit latency average = 198.278
	minimum = 6
	maximum = 2603
Slowest flit = 39058
Fragmentation average = 119.949
	minimum = 0
	maximum = 693
Injected packet rate average = 0.00828646
	minimum = 0 (at node 28)
	maximum = 0.025 (at node 135)
Accepted packet rate average = 0.00867708
	minimum = 0.002 (at node 184)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.148875
	minimum = 0 (at node 28)
	maximum = 0.45 (at node 135)
Accepted flit rate average= 0.155333
	minimum = 0.048 (at node 184)
	maximum = 0.322 (at node 16)
Injected packet length average = 17.9661
Accepted packet length average = 17.9016
Total in-flight flits = 5790 (5325 measured)
latency change    = 0.160254
throughput change = 0.003353
Class 0:
Packet latency average = 285.662
	minimum = 25
	maximum = 1768
Network latency average = 237.51
	minimum = 23
	maximum = 1751
Slowest packet = 5581
Flit latency average = 195.946
	minimum = 6
	maximum = 3497
Slowest flit = 40445
Fragmentation average = 134.7
	minimum = 0
	maximum = 1353
Injected packet rate average = 0.00875521
	minimum = 0 (at node 35)
	maximum = 0.0255 (at node 81)
Accepted packet rate average = 0.00875781
	minimum = 0.004 (at node 163)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.157674
	minimum = 0 (at node 35)
	maximum = 0.459 (at node 81)
Accepted flit rate average= 0.157924
	minimum = 0.068 (at node 163)
	maximum = 0.319 (at node 16)
Injected packet length average = 18.0092
Accepted packet length average = 18.0324
Total in-flight flits = 6849 (6716 measured)
latency change    = 0.136412
throughput change = 0.0164075
Class 0:
Packet latency average = 297.924
	minimum = 25
	maximum = 2365
Network latency average = 251.875
	minimum = 23
	maximum = 2335
Slowest packet = 5576
Flit latency average = 197.81
	minimum = 6
	maximum = 3801
Slowest flit = 55061
Fragmentation average = 138.719
	minimum = 0
	maximum = 1491
Injected packet rate average = 0.00869271
	minimum = 0.000666667 (at node 69)
	maximum = 0.024 (at node 40)
Accepted packet rate average = 0.00891146
	minimum = 0.00566667 (at node 4)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.156507
	minimum = 0.012 (at node 69)
	maximum = 0.426333 (at node 40)
Accepted flit rate average= 0.159566
	minimum = 0.101 (at node 163)
	maximum = 0.285 (at node 16)
Injected packet length average = 18.0044
Accepted packet length average = 17.9057
Total in-flight flits = 5192 (5184 measured)
latency change    = 0.0411566
throughput change = 0.0102872
Class 0:
Packet latency average = 303.661
	minimum = 25
	maximum = 2976
Network latency average = 257.874
	minimum = 23
	maximum = 2976
Slowest packet = 5392
Flit latency average = 194.702
	minimum = 6
	maximum = 3801
Slowest flit = 55061
Fragmentation average = 141.777
	minimum = 0
	maximum = 1670
Injected packet rate average = 0.0088776
	minimum = 0.00125 (at node 69)
	maximum = 0.0185 (at node 40)
Accepted packet rate average = 0.00882292
	minimum = 0.0045 (at node 4)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.15977
	minimum = 0.0225 (at node 69)
	maximum = 0.333 (at node 40)
Accepted flit rate average= 0.159353
	minimum = 0.08025 (at node 4)
	maximum = 0.24625 (at node 16)
Injected packet length average = 17.9969
Accepted packet length average = 18.0612
Total in-flight flits = 7317 (7317 measured)
latency change    = 0.0188938
throughput change = 0.00133733
Class 0:
Packet latency average = 312.179
	minimum = 23
	maximum = 2976
Network latency average = 267.2
	minimum = 23
	maximum = 2976
Slowest packet = 5392
Flit latency average = 197.075
	minimum = 6
	maximum = 3801
Slowest flit = 55061
Fragmentation average = 144.921
	minimum = 0
	maximum = 1850
Injected packet rate average = 0.00893125
	minimum = 0.0022 (at node 69)
	maximum = 0.0198 (at node 7)
Accepted packet rate average = 0.00883333
	minimum = 0.005 (at node 4)
	maximum = 0.0132 (at node 16)
Injected flit rate average = 0.160636
	minimum = 0.0396 (at node 69)
	maximum = 0.3562 (at node 7)
Accepted flit rate average= 0.159372
	minimum = 0.0992 (at node 4)
	maximum = 0.2388 (at node 16)
Injected packet length average = 17.9859
Accepted packet length average = 18.0421
Total in-flight flits = 8311 (8311 measured)
latency change    = 0.0272844
throughput change = 0.000119283
Draining all recorded packets ...
Class 0:
Remaining flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (8563 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (565 flits)
Class 0:
Remaining flits: 226843 226844 226845 226846 226847 226848 226849 226850 226851 226852 [...] (8033 flits)
Measured flits: 226843 226844 226845 226846 226847 226848 226849 226850 226851 226852 [...] (107 flits)
Class 0:
Remaining flits: 226843 226844 226845 226846 226847 226848 226849 226850 226851 226852 [...] (6670 flits)
Measured flits: 226843 226844 226845 226846 226847 226848 226849 226850 226851 226852 [...] (29 flits)
Class 0:
Remaining flits: 278543 278544 278545 278546 278547 278548 278549 280651 280652 280653 [...] (9203 flits)
Measured flits: 280651 280652 280653 280654 280655 (5 flits)
Draining remaining packets ...
Time taken is 12585 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 340.47 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4347 (1 samples)
Network latency average = 292.499 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4184 (1 samples)
Flit latency average = 220.26 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4143 (1 samples)
Fragmentation average = 153.695 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2571 (1 samples)
Injected packet rate average = 0.00893125 (1 samples)
	minimum = 0.0022 (1 samples)
	maximum = 0.0198 (1 samples)
Accepted packet rate average = 0.00883333 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0132 (1 samples)
Injected flit rate average = 0.160636 (1 samples)
	minimum = 0.0396 (1 samples)
	maximum = 0.3562 (1 samples)
Accepted flit rate average = 0.159372 (1 samples)
	minimum = 0.0992 (1 samples)
	maximum = 0.2388 (1 samples)
Injected packet size average = 17.9859 (1 samples)
Accepted packet size average = 18.0421 (1 samples)
Hops average = 5.0789 (1 samples)
Total run time 9.18463
