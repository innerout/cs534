BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.847
	minimum = 22
	maximum = 981
Network latency average = 236.899
	minimum = 22
	maximum = 820
Slowest packet = 46
Flit latency average = 205.553
	minimum = 5
	maximum = 803
Slowest flit = 8027
Fragmentation average = 47.9638
	minimum = 0
	maximum = 614
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139375
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26337
	minimum = 0.118 (at node 150)
	maximum = 0.432 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 18.8965
Total in-flight flits = 52267 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.551
	minimum = 22
	maximum = 1947
Network latency average = 447.509
	minimum = 22
	maximum = 1713
Slowest packet = 46
Flit latency average = 415.643
	minimum = 5
	maximum = 1706
Slowest flit = 25331
Fragmentation average = 73.3836
	minimum = 0
	maximum = 755
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.014737
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.276185
	minimum = 0.1655 (at node 164)
	maximum = 0.3965 (at node 152)
Injected packet length average = 17.9125
Accepted packet length average = 18.7409
Total in-flight flits = 110719 (0 measured)
latency change    = 0.427473
throughput change = 0.0464005
Class 0:
Packet latency average = 1350.49
	minimum = 25
	maximum = 2851
Network latency average = 1079.87
	minimum = 22
	maximum = 2376
Slowest packet = 2239
Flit latency average = 1040.17
	minimum = 5
	maximum = 2508
Slowest flit = 43803
Fragmentation average = 137.575
	minimum = 0
	maximum = 919
Injected packet rate average = 0.0345885
	minimum = 0 (at node 29)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0162083
	minimum = 0.007 (at node 190)
	maximum = 0.034 (at node 16)
Injected flit rate average = 0.623354
	minimum = 0 (at node 29)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.29574
	minimum = 0.126 (at node 113)
	maximum = 0.591 (at node 16)
Injected packet length average = 18.022
Accepted packet length average = 18.2461
Total in-flight flits = 173475 (0 measured)
latency change    = 0.544944
throughput change = 0.0661213
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 422.054
	minimum = 25
	maximum = 2039
Network latency average = 36.4162
	minimum = 22
	maximum = 107
Slowest packet = 18691
Flit latency average = 1462.5
	minimum = 5
	maximum = 3267
Slowest flit = 68912
Fragmentation average = 6.53299
	minimum = 0
	maximum = 51
Injected packet rate average = 0.0359115
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0164844
	minimum = 0.007 (at node 5)
	maximum = 0.027 (at node 136)
Injected flit rate average = 0.645875
	minimum = 0 (at node 100)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.301057
	minimum = 0.121 (at node 5)
	maximum = 0.467 (at node 128)
Injected packet length average = 17.9852
Accepted packet length average = 18.2632
Total in-flight flits = 239782 (113314 measured)
latency change    = 2.19981
throughput change = 0.0176634
Class 0:
Packet latency average = 507.284
	minimum = 25
	maximum = 3058
Network latency average = 118.825
	minimum = 22
	maximum = 1981
Slowest packet = 18691
Flit latency average = 1694.51
	minimum = 5
	maximum = 4246
Slowest flit = 72839
Fragmentation average = 11.085
	minimum = 0
	maximum = 372
Injected packet rate average = 0.0352786
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0162656
	minimum = 0.009 (at node 5)
	maximum = 0.0245 (at node 128)
Injected flit rate average = 0.634745
	minimum = 0 (at node 100)
	maximum = 1 (at node 10)
Accepted flit rate average= 0.298245
	minimum = 0.1925 (at node 5)
	maximum = 0.4445 (at node 128)
Injected packet length average = 17.9923
Accepted packet length average = 18.3359
Total in-flight flits = 302795 (221303 measured)
latency change    = 0.168013
throughput change = 0.00943017
Class 0:
Packet latency average = 907.169
	minimum = 23
	maximum = 3884
Network latency average = 538.797
	minimum = 22
	maximum = 2963
Slowest packet = 18691
Flit latency average = 1933.76
	minimum = 5
	maximum = 5027
Slowest flit = 97682
Fragmentation average = 40.304
	minimum = 0
	maximum = 625
Injected packet rate average = 0.0347708
	minimum = 0.00566667 (at node 100)
	maximum = 0.0556667 (at node 31)
Accepted packet rate average = 0.016224
	minimum = 0.011 (at node 2)
	maximum = 0.0216667 (at node 136)
Injected flit rate average = 0.625705
	minimum = 0.102 (at node 100)
	maximum = 1 (at node 31)
Accepted flit rate average= 0.297352
	minimum = 0.203 (at node 171)
	maximum = 0.401 (at node 138)
Injected packet length average = 17.9951
Accepted packet length average = 18.328
Total in-flight flits = 362704 (318923 measured)
latency change    = 0.440805
throughput change = 0.00300102
Draining remaining packets ...
Class 0:
Remaining flits: 133487 136832 136833 136834 136835 137581 137582 137583 137584 137585 [...] (315302 flits)
Measured flits: 336337 336338 336339 336340 336341 336342 336343 336344 336345 336346 [...] (293575 flits)
Class 0:
Remaining flits: 166451 166452 166453 166454 166455 166456 166457 166458 166459 166460 [...] (268013 flits)
Measured flits: 336598 336599 336708 336709 336710 336711 336712 336713 336714 336715 [...] (257178 flits)
Class 0:
Remaining flits: 203956 203957 206756 206757 206758 206759 206760 206761 206762 206763 [...] (220611 flits)
Measured flits: 336708 336709 336710 336711 336712 336713 336714 336715 336716 336717 [...] (216365 flits)
Class 0:
Remaining flits: 247868 247869 247870 247871 247872 247873 247874 247875 247876 247877 [...] (173176 flits)
Measured flits: 336708 336709 336710 336711 336712 336713 336714 336715 336716 336717 [...] (171884 flits)
Class 0:
Remaining flits: 304559 304575 304576 304577 310426 310427 315479 315480 315481 315482 [...] (125978 flits)
Measured flits: 336722 336723 336724 336725 337270 337271 337272 337273 337274 337275 [...] (125886 flits)
Class 0:
Remaining flits: 339597 339598 339599 339600 339601 339602 339603 339604 339605 352902 [...] (81537 flits)
Measured flits: 339597 339598 339599 339600 339601 339602 339603 339604 339605 352902 [...] (81537 flits)
Class 0:
Remaining flits: 380641 380642 380643 380644 380645 385359 385360 385361 390520 390521 [...] (42380 flits)
Measured flits: 380641 380642 380643 380644 380645 385359 385360 385361 390520 390521 [...] (42380 flits)
Class 0:
Remaining flits: 402588 402589 402590 402591 402592 402593 402594 402595 402596 402597 [...] (20456 flits)
Measured flits: 402588 402589 402590 402591 402592 402593 402594 402595 402596 402597 [...] (20456 flits)
Class 0:
Remaining flits: 456966 456967 456968 456969 456970 456971 456972 456973 456974 456975 [...] (7945 flits)
Measured flits: 456966 456967 456968 456969 456970 456971 456972 456973 456974 456975 [...] (7945 flits)
Class 0:
Remaining flits: 552132 552133 552134 552135 552136 552137 552138 552139 552140 552141 [...] (1018 flits)
Measured flits: 552132 552133 552134 552135 552136 552137 552138 552139 552140 552141 [...] (1018 flits)
Time taken is 16653 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5751.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13092 (1 samples)
Network latency average = 5331.17 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11630 (1 samples)
Flit latency average = 4466.15 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11613 (1 samples)
Fragmentation average = 252.226 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1441 (1 samples)
Injected packet rate average = 0.0347708 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.016224 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.625705 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.297352 (1 samples)
	minimum = 0.203 (1 samples)
	maximum = 0.401 (1 samples)
Injected packet size average = 17.9951 (1 samples)
Accepted packet size average = 18.328 (1 samples)
Hops average = 5.07544 (1 samples)
Total run time 16.3846
