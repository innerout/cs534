BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.058
	minimum = 22
	maximum = 739
Network latency average = 210.392
	minimum = 22
	maximum = 739
Slowest packet = 1081
Flit latency average = 178.152
	minimum = 5
	maximum = 722
Slowest flit = 19475
Fragmentation average = 41.0084
	minimum = 0
	maximum = 199
Injected packet rate average = 0.0220677
	minimum = 0.012 (at node 120)
	maximum = 0.034 (at node 29)
Accepted packet rate average = 0.013625
	minimum = 0.004 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.393286
	minimum = 0.209 (at node 120)
	maximum = 0.612 (at node 29)
Accepted flit rate average= 0.254682
	minimum = 0.072 (at node 174)
	maximum = 0.45 (at node 44)
Injected packet length average = 17.8218
Accepted packet length average = 18.6923
Total in-flight flits = 27529 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 376.22
	minimum = 22
	maximum = 1384
Network latency average = 362.97
	minimum = 22
	maximum = 1384
Slowest packet = 2622
Flit latency average = 325.988
	minimum = 5
	maximum = 1451
Slowest flit = 42018
Fragmentation average = 48.7344
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0207917
	minimum = 0.011 (at node 72)
	maximum = 0.027 (at node 22)
Accepted packet rate average = 0.0143932
	minimum = 0.0085 (at node 153)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.372336
	minimum = 0.195 (at node 120)
	maximum = 0.4855 (at node 22)
Accepted flit rate average= 0.26407
	minimum = 0.158 (at node 153)
	maximum = 0.3805 (at node 22)
Injected packet length average = 17.9079
Accepted packet length average = 18.3468
Total in-flight flits = 42867 (0 measured)
latency change    = 0.425713
throughput change = 0.0355512
Class 0:
Packet latency average = 830.333
	minimum = 22
	maximum = 1971
Network latency average = 741.328
	minimum = 22
	maximum = 1971
Slowest packet = 4166
Flit latency average = 697.343
	minimum = 5
	maximum = 1936
Slowest flit = 74993
Fragmentation average = 60.9862
	minimum = 0
	maximum = 318
Injected packet rate average = 0.0168542
	minimum = 0 (at node 71)
	maximum = 0.032 (at node 77)
Accepted packet rate average = 0.0151146
	minimum = 0.006 (at node 40)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.302063
	minimum = 0 (at node 71)
	maximum = 0.576 (at node 77)
Accepted flit rate average= 0.27288
	minimum = 0.127 (at node 20)
	maximum = 0.481 (at node 16)
Injected packet length average = 17.9221
Accepted packet length average = 18.0541
Total in-flight flits = 49010 (0 measured)
latency change    = 0.546904
throughput change = 0.0322848
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 836.263
	minimum = 25
	maximum = 2289
Network latency average = 366.722
	minimum = 22
	maximum = 954
Slowest packet = 11279
Flit latency average = 847.504
	minimum = 5
	maximum = 2652
Slowest flit = 100423
Fragmentation average = 34.6817
	minimum = 0
	maximum = 202
Injected packet rate average = 0.0154115
	minimum = 0.001 (at node 132)
	maximum = 0.026 (at node 174)
Accepted packet rate average = 0.0149948
	minimum = 0.006 (at node 4)
	maximum = 0.028 (at node 129)
Injected flit rate average = 0.277177
	minimum = 0.001 (at node 132)
	maximum = 0.467 (at node 174)
Accepted flit rate average= 0.27176
	minimum = 0.114 (at node 74)
	maximum = 0.5 (at node 129)
Injected packet length average = 17.9851
Accepted packet length average = 18.1237
Total in-flight flits = 50238 (41333 measured)
latency change    = 0.00709121
throughput change = 0.00412051
Class 0:
Packet latency average = 1272.36
	minimum = 25
	maximum = 3128
Network latency average = 745.441
	minimum = 22
	maximum = 1916
Slowest packet = 11279
Flit latency average = 885.759
	minimum = 5
	maximum = 3061
Slowest flit = 118061
Fragmentation average = 58.8197
	minimum = 0
	maximum = 277
Injected packet rate average = 0.0155417
	minimum = 0.0045 (at node 96)
	maximum = 0.023 (at node 133)
Accepted packet rate average = 0.0150104
	minimum = 0.0105 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.279799
	minimum = 0.081 (at node 96)
	maximum = 0.4135 (at node 141)
Accepted flit rate average= 0.270685
	minimum = 0.184 (at node 42)
	maximum = 0.4315 (at node 129)
Injected packet length average = 18.0032
Accepted packet length average = 18.0331
Total in-flight flits = 52617 (52197 measured)
latency change    = 0.342744
throughput change = 0.00397333
Class 0:
Packet latency average = 1521.83
	minimum = 25
	maximum = 3718
Network latency average = 863.459
	minimum = 22
	maximum = 2771
Slowest packet = 11279
Flit latency average = 903.571
	minimum = 5
	maximum = 3654
Slowest flit = 162791
Fragmentation average = 64.9986
	minimum = 0
	maximum = 296
Injected packet rate average = 0.0153038
	minimum = 0.00533333 (at node 132)
	maximum = 0.0223333 (at node 34)
Accepted packet rate average = 0.0149757
	minimum = 0.01 (at node 135)
	maximum = 0.0216667 (at node 129)
Injected flit rate average = 0.275175
	minimum = 0.0943333 (at node 132)
	maximum = 0.403667 (at node 34)
Accepted flit rate average= 0.270571
	minimum = 0.181 (at node 135)
	maximum = 0.386667 (at node 129)
Injected packet length average = 17.9808
Accepted packet length average = 18.0674
Total in-flight flits = 51795 (51741 measured)
latency change    = 0.163929
throughput change = 0.000420279
Class 0:
Packet latency average = 1723.9
	minimum = 25
	maximum = 4254
Network latency average = 918.077
	minimum = 22
	maximum = 3660
Slowest packet = 11279
Flit latency average = 916.952
	minimum = 5
	maximum = 3660
Slowest flit = 183905
Fragmentation average = 68.6891
	minimum = 0
	maximum = 317
Injected packet rate average = 0.015224
	minimum = 0.0055 (at node 132)
	maximum = 0.0215 (at node 134)
Accepted packet rate average = 0.0149492
	minimum = 0.01075 (at node 186)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.273786
	minimum = 0.0975 (at node 132)
	maximum = 0.389 (at node 134)
Accepted flit rate average= 0.269924
	minimum = 0.19725 (at node 186)
	maximum = 0.35225 (at node 95)
Injected packet length average = 17.9839
Accepted packet length average = 18.0561
Total in-flight flits = 52128 (52128 measured)
latency change    = 0.117218
throughput change = 0.00239586
Class 0:
Packet latency average = 1906.49
	minimum = 25
	maximum = 4674
Network latency average = 942.078
	minimum = 22
	maximum = 3894
Slowest packet = 11279
Flit latency average = 922.007
	minimum = 5
	maximum = 3830
Slowest flit = 251981
Fragmentation average = 71.4221
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0151385
	minimum = 0.0078 (at node 132)
	maximum = 0.0204 (at node 34)
Accepted packet rate average = 0.0149667
	minimum = 0.0112 (at node 42)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.272384
	minimum = 0.1404 (at node 132)
	maximum = 0.3684 (at node 133)
Accepted flit rate average= 0.2698
	minimum = 0.2022 (at node 42)
	maximum = 0.344 (at node 138)
Injected packet length average = 17.9928
Accepted packet length average = 18.0267
Total in-flight flits = 51668 (51668 measured)
latency change    = 0.0957722
throughput change = 0.000461376
Class 0:
Packet latency average = 2073.99
	minimum = 25
	maximum = 5756
Network latency average = 960.492
	minimum = 22
	maximum = 3894
Slowest packet = 11279
Flit latency average = 928.509
	minimum = 5
	maximum = 4023
Slowest flit = 308404
Fragmentation average = 73.8184
	minimum = 0
	maximum = 317
Injected packet rate average = 0.0150391
	minimum = 0.00966667 (at node 132)
	maximum = 0.0193333 (at node 131)
Accepted packet rate average = 0.0148941
	minimum = 0.0115 (at node 42)
	maximum = 0.0196667 (at node 138)
Injected flit rate average = 0.27063
	minimum = 0.174 (at node 132)
	maximum = 0.349 (at node 142)
Accepted flit rate average= 0.268873
	minimum = 0.207833 (at node 42)
	maximum = 0.360333 (at node 138)
Injected packet length average = 17.9952
Accepted packet length average = 18.0523
Total in-flight flits = 51118 (51118 measured)
latency change    = 0.0807639
throughput change = 0.00344674
Class 0:
Packet latency average = 2248.84
	minimum = 25
	maximum = 5947
Network latency average = 971.655
	minimum = 22
	maximum = 4387
Slowest packet = 11279
Flit latency average = 932.512
	minimum = 5
	maximum = 4284
Slowest flit = 268676
Fragmentation average = 75.1143
	minimum = 0
	maximum = 317
Injected packet rate average = 0.015029
	minimum = 0.0101429 (at node 132)
	maximum = 0.021 (at node 34)
Accepted packet rate average = 0.01491
	minimum = 0.0117143 (at node 79)
	maximum = 0.0184286 (at node 70)
Injected flit rate average = 0.270407
	minimum = 0.180429 (at node 132)
	maximum = 0.378571 (at node 34)
Accepted flit rate average= 0.268851
	minimum = 0.209857 (at node 79)
	maximum = 0.331714 (at node 138)
Injected packet length average = 17.9923
Accepted packet length average = 18.0316
Total in-flight flits = 51166 (51166 measured)
latency change    = 0.0777482
throughput change = 8.21027e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 402714 402715 402716 402717 402718 402719 402720 402721 402722 402723 [...] (50905 flits)
Measured flits: 402714 402715 402716 402717 402718 402719 402720 402721 402722 402723 [...] (50081 flits)
Class 0:
Remaining flits: 402714 402715 402716 402717 402718 402719 402720 402721 402722 402723 [...] (50819 flits)
Measured flits: 402714 402715 402716 402717 402718 402719 402720 402721 402722 402723 [...] (47038 flits)
Class 0:
Remaining flits: 438120 438121 438122 438123 438124 438125 438126 438127 438128 438129 [...] (50114 flits)
Measured flits: 438120 438121 438122 438123 438124 438125 438126 438127 438128 438129 [...] (39330 flits)
Class 0:
Remaining flits: 567378 567379 567380 567381 567382 567383 567384 567385 567386 567387 [...] (50426 flits)
Measured flits: 567378 567379 567380 567381 567382 567383 567384 567385 567386 567387 [...] (28804 flits)
Class 0:
Remaining flits: 591930 591931 591932 591933 591934 591935 591936 591937 591938 591939 [...] (50725 flits)
Measured flits: 591930 591931 591932 591933 591934 591935 591936 591937 591938 591939 [...] (17247 flits)
Class 0:
Remaining flits: 591930 591931 591932 591933 591934 591935 591936 591937 591938 591939 [...] (50894 flits)
Measured flits: 591930 591931 591932 591933 591934 591935 591936 591937 591938 591939 [...] (9544 flits)
Class 0:
Remaining flits: 665172 665173 665174 665175 665176 665177 665178 665179 665180 665181 [...] (50854 flits)
Measured flits: 665172 665173 665174 665175 665176 665177 665178 665179 665180 665181 [...] (5008 flits)
Class 0:
Remaining flits: 759240 759241 759242 759243 759244 759245 759246 759247 759248 759249 [...] (50471 flits)
Measured flits: 798696 798697 798698 798699 798700 798701 798702 798703 798704 798705 [...] (2395 flits)
Class 0:
Remaining flits: 787932 787933 787934 787935 787936 787937 787938 787939 787940 787941 [...] (50685 flits)
Measured flits: 799614 799615 799616 799617 799618 799619 799620 799621 799622 799623 [...] (958 flits)
Class 0:
Remaining flits: 810522 810523 810524 810525 810526 810527 810528 810529 810530 810531 [...] (50039 flits)
Measured flits: 952470 952471 952472 952473 952474 952475 952476 952477 952478 952479 [...] (311 flits)
Class 0:
Remaining flits: 909306 909307 909308 909309 909310 909311 909312 909313 909314 909315 [...] (50375 flits)
Measured flits: 1094091 1094092 1094093 1098172 1098173 1098174 1098175 1098176 1098177 1098178 [...] (129 flits)
Class 0:
Remaining flits: 976230 976231 976232 976233 976234 976235 976236 976237 976238 976239 [...] (49557 flits)
Measured flits: 1123488 1123489 1123490 1123491 1123492 1123493 1123494 1123495 1123496 1123497 [...] (177 flits)
Class 0:
Remaining flits: 998532 998533 998534 998535 998536 998537 998538 998539 998540 998541 [...] (48551 flits)
Measured flits: 1180656 1180657 1180658 1180659 1180660 1180661 1180662 1180663 1180664 1180665 [...] (108 flits)
Class 0:
Remaining flits: 999036 999037 999038 999039 999040 999041 999042 999043 999044 999045 [...] (49811 flits)
Measured flits: 1180656 1180657 1180658 1180659 1180660 1180661 1180662 1180663 1180664 1180665 [...] (54 flits)
Class 0:
Remaining flits: 999047 999048 999049 999050 999051 999052 999053 1079280 1079281 1079282 [...] (49232 flits)
Measured flits: 1180665 1180666 1180667 1180668 1180669 1180670 1180671 1180672 1180673 (9 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1085310 1085311 1085312 1085313 1085314 1085315 1085316 1085317 1085318 1085319 [...] (9156 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1224792 1224793 1224794 1224795 1224796 1224797 1224798 1224799 1224800 1224801 [...] (404 flits)
Measured flits: (0 flits)
Time taken is 27414 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3398.97 (1 samples)
	minimum = 25 (1 samples)
	maximum = 15748 (1 samples)
Network latency average = 1009.95 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5939 (1 samples)
Flit latency average = 952.562 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6922 (1 samples)
Fragmentation average = 80.1948 (1 samples)
	minimum = 0 (1 samples)
	maximum = 317 (1 samples)
Injected packet rate average = 0.015029 (1 samples)
	minimum = 0.0101429 (1 samples)
	maximum = 0.021 (1 samples)
Accepted packet rate average = 0.01491 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0184286 (1 samples)
Injected flit rate average = 0.270407 (1 samples)
	minimum = 0.180429 (1 samples)
	maximum = 0.378571 (1 samples)
Accepted flit rate average = 0.268851 (1 samples)
	minimum = 0.209857 (1 samples)
	maximum = 0.331714 (1 samples)
Injected packet size average = 17.9923 (1 samples)
Accepted packet size average = 18.0316 (1 samples)
Hops average = 5.05531 (1 samples)
Total run time 35.6417
