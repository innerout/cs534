BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 281.21
	minimum = 23
	maximum = 928
Network latency average = 201.737
	minimum = 22
	maximum = 836
Slowest packet = 85
Flit latency average = 168.552
	minimum = 5
	maximum = 840
Slowest flit = 11388
Fragmentation average = 43.359
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0209063
	minimum = 0 (at node 59)
	maximum = 0.045 (at node 37)
Accepted packet rate average = 0.0127396
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.372312
	minimum = 0 (at node 59)
	maximum = 0.81 (at node 37)
Accepted flit rate average= 0.238234
	minimum = 0.084 (at node 174)
	maximum = 0.41 (at node 44)
Injected packet length average = 17.8087
Accepted packet length average = 18.7003
Total in-flight flits = 26673 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 480.233
	minimum = 22
	maximum = 1791
Network latency average = 354.668
	minimum = 22
	maximum = 1580
Slowest packet = 85
Flit latency average = 317.268
	minimum = 5
	maximum = 1563
Slowest flit = 27305
Fragmentation average = 49.6183
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0190599
	minimum = 0.002 (at node 180)
	maximum = 0.038 (at node 185)
Accepted packet rate average = 0.0136172
	minimum = 0.008 (at node 153)
	maximum = 0.0195 (at node 44)
Injected flit rate average = 0.340672
	minimum = 0.036 (at node 180)
	maximum = 0.6805 (at node 185)
Accepted flit rate average= 0.250331
	minimum = 0.1495 (at node 153)
	maximum = 0.351 (at node 44)
Injected packet length average = 17.8738
Accepted packet length average = 18.3834
Total in-flight flits = 35957 (0 measured)
latency change    = 0.414431
throughput change = 0.0483215
Class 0:
Packet latency average = 995.348
	minimum = 25
	maximum = 2682
Network latency average = 682.463
	minimum = 22
	maximum = 2129
Slowest packet = 2477
Flit latency average = 635.225
	minimum = 5
	maximum = 2085
Slowest flit = 48185
Fragmentation average = 58.8506
	minimum = 0
	maximum = 267
Injected packet rate average = 0.0172552
	minimum = 0 (at node 22)
	maximum = 0.04 (at node 6)
Accepted packet rate average = 0.0147135
	minimum = 0.005 (at node 190)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.310698
	minimum = 0 (at node 22)
	maximum = 0.715 (at node 6)
Accepted flit rate average= 0.26576
	minimum = 0.091 (at node 190)
	maximum = 0.461 (at node 16)
Injected packet length average = 18.006
Accepted packet length average = 18.0623
Total in-flight flits = 44871 (0 measured)
latency change    = 0.517522
throughput change = 0.0580586
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1015.43
	minimum = 24
	maximum = 3348
Network latency average = 386.352
	minimum = 22
	maximum = 963
Slowest packet = 10698
Flit latency average = 773.359
	minimum = 5
	maximum = 2484
Slowest flit = 100109
Fragmentation average = 41.8866
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0157969
	minimum = 0.001 (at node 8)
	maximum = 0.036 (at node 71)
Accepted packet rate average = 0.014901
	minimum = 0.006 (at node 4)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.283557
	minimum = 0.018 (at node 8)
	maximum = 0.638 (at node 71)
Accepted flit rate average= 0.269589
	minimum = 0.12 (at node 4)
	maximum = 0.442 (at node 129)
Injected packet length average = 17.9502
Accepted packet length average = 18.0919
Total in-flight flits = 47722 (40764 measured)
latency change    = 0.0197768
throughput change = 0.0141999
Class 0:
Packet latency average = 1445.44
	minimum = 24
	maximum = 4315
Network latency average = 710.294
	minimum = 22
	maximum = 1896
Slowest packet = 10698
Flit latency average = 812.363
	minimum = 5
	maximum = 3248
Slowest flit = 85427
Fragmentation average = 61.0727
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0154219
	minimum = 0.005 (at node 143)
	maximum = 0.0275 (at node 71)
Accepted packet rate average = 0.0150234
	minimum = 0.0095 (at node 4)
	maximum = 0.023 (at node 66)
Injected flit rate average = 0.277396
	minimum = 0.0915 (at node 143)
	maximum = 0.494 (at node 71)
Accepted flit rate average= 0.269984
	minimum = 0.169 (at node 4)
	maximum = 0.4145 (at node 66)
Injected packet length average = 17.9872
Accepted packet length average = 17.9709
Total in-flight flits = 47973 (47657 measured)
latency change    = 0.297496
throughput change = 0.00146613
Class 0:
Packet latency average = 1711.91
	minimum = 24
	maximum = 4953
Network latency average = 813.477
	minimum = 22
	maximum = 2762
Slowest packet = 10698
Flit latency average = 834.123
	minimum = 5
	maximum = 3775
Slowest flit = 116026
Fragmentation average = 66.131
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0155781
	minimum = 0.00633333 (at node 143)
	maximum = 0.026 (at node 71)
Accepted packet rate average = 0.0149774
	minimum = 0.0106667 (at node 2)
	maximum = 0.021 (at node 66)
Injected flit rate average = 0.280021
	minimum = 0.115 (at node 143)
	maximum = 0.462333 (at node 71)
Accepted flit rate average= 0.270418
	minimum = 0.186333 (at node 2)
	maximum = 0.371 (at node 66)
Injected packet length average = 17.9753
Accepted packet length average = 18.0551
Total in-flight flits = 50660 (50647 measured)
latency change    = 0.155652
throughput change = 0.00160502
Draining remaining packets ...
Class 0:
Remaining flits: 271998 271999 272000 272001 272002 272003 272004 272005 272006 272007 [...] (5188 flits)
Measured flits: 271998 271999 272000 272001 272002 272003 272004 272005 272006 272007 [...] (5188 flits)
Time taken is 7611 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1979.41 (1 samples)
	minimum = 24 (1 samples)
	maximum = 6157 (1 samples)
Network latency average = 930.703 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3440 (1 samples)
Flit latency average = 900.278 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3775 (1 samples)
Fragmentation average = 69.3097 (1 samples)
	minimum = 0 (1 samples)
	maximum = 313 (1 samples)
Injected packet rate average = 0.0155781 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0149774 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.280021 (1 samples)
	minimum = 0.115 (1 samples)
	maximum = 0.462333 (1 samples)
Accepted flit rate average = 0.270418 (1 samples)
	minimum = 0.186333 (1 samples)
	maximum = 0.371 (1 samples)
Injected packet size average = 17.9753 (1 samples)
Accepted packet size average = 18.0551 (1 samples)
Hops average = 5.08847 (1 samples)
Total run time 10.5896
