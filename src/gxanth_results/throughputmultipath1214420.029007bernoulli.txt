BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 270.542
	minimum = 22
	maximum = 781
Network latency average = 262.436
	minimum = 22
	maximum = 762
Slowest packet = 1150
Flit latency average = 238.653
	minimum = 5
	maximum = 745
Slowest flit = 20484
Fragmentation average = 21.8069
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0141042
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.260042
	minimum = 0.105 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.4372
Total in-flight flits = 49720 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 493.446
	minimum = 22
	maximum = 1483
Network latency average = 484.16
	minimum = 22
	maximum = 1403
Slowest packet = 2856
Flit latency average = 460.059
	minimum = 5
	maximum = 1386
Slowest flit = 59022
Fragmentation average = 22.5122
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0282891
	minimum = 0.0175 (at node 134)
	maximum = 0.0375 (at node 66)
Accepted packet rate average = 0.0149323
	minimum = 0.0075 (at node 153)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.507083
	minimum = 0.315 (at node 134)
	maximum = 0.671 (at node 66)
Accepted flit rate average= 0.27151
	minimum = 0.135 (at node 153)
	maximum = 0.4155 (at node 152)
Injected packet length average = 17.9251
Accepted packet length average = 18.1828
Total in-flight flits = 91562 (0 measured)
latency change    = 0.451729
throughput change = 0.0422406
Class 0:
Packet latency average = 1180.7
	minimum = 22
	maximum = 2227
Network latency average = 1166.23
	minimum = 22
	maximum = 2201
Slowest packet = 4091
Flit latency average = 1146.29
	minimum = 5
	maximum = 2232
Slowest flit = 75625
Fragmentation average = 21.2953
	minimum = 0
	maximum = 186
Injected packet rate average = 0.024526
	minimum = 0.003 (at node 64)
	maximum = 0.039 (at node 83)
Accepted packet rate average = 0.0149583
	minimum = 0.007 (at node 180)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.44174
	minimum = 0.064 (at node 64)
	maximum = 0.706 (at node 83)
Accepted flit rate average= 0.268776
	minimum = 0.137 (at node 180)
	maximum = 0.464 (at node 182)
Injected packet length average = 18.011
Accepted packet length average = 17.9683
Total in-flight flits = 125385 (0 measured)
latency change    = 0.582074
throughput change = 0.0101734
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 454.218
	minimum = 22
	maximum = 1703
Network latency average = 126.234
	minimum = 22
	maximum = 742
Slowest packet = 15643
Flit latency average = 1551.02
	minimum = 5
	maximum = 2789
Slowest flit = 117126
Fragmentation average = 6.10106
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0202292
	minimum = 0.003 (at node 8)
	maximum = 0.035 (at node 49)
Accepted packet rate average = 0.0151302
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 83)
Injected flit rate average = 0.365073
	minimum = 0.054 (at node 8)
	maximum = 0.628 (at node 37)
Accepted flit rate average= 0.272427
	minimum = 0.126 (at node 86)
	maximum = 0.5 (at node 83)
Injected packet length average = 18.0469
Accepted packet length average = 18.0055
Total in-flight flits = 143837 (67295 measured)
latency change    = 1.59942
throughput change = 0.0134019
Class 0:
Packet latency average = 1205.95
	minimum = 22
	maximum = 3091
Network latency average = 774.477
	minimum = 22
	maximum = 1971
Slowest packet = 15643
Flit latency average = 1747.15
	minimum = 5
	maximum = 3415
Slowest flit = 155447
Fragmentation average = 12.386
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0185182
	minimum = 0.0065 (at node 68)
	maximum = 0.0355 (at node 22)
Accepted packet rate average = 0.0150417
	minimum = 0.009 (at node 4)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.333852
	minimum = 0.117 (at node 68)
	maximum = 0.644 (at node 22)
Accepted flit rate average= 0.270755
	minimum = 0.1605 (at node 4)
	maximum = 0.394 (at node 123)
Injected packet length average = 18.0283
Accepted packet length average = 18.0003
Total in-flight flits = 150565 (117058 measured)
latency change    = 0.623353
throughput change = 0.00617486
Class 0:
Packet latency average = 1978.79
	minimum = 22
	maximum = 3881
Network latency average = 1526.59
	minimum = 22
	maximum = 2969
Slowest packet = 15643
Flit latency average = 1936.45
	minimum = 5
	maximum = 3986
Slowest flit = 193643
Fragmentation average = 16.2805
	minimum = 0
	maximum = 166
Injected packet rate average = 0.017441
	minimum = 0.00733333 (at node 68)
	maximum = 0.032 (at node 22)
Accepted packet rate average = 0.0149375
	minimum = 0.0103333 (at node 158)
	maximum = 0.021 (at node 118)
Injected flit rate average = 0.314116
	minimum = 0.132 (at node 68)
	maximum = 0.573667 (at node 22)
Accepted flit rate average= 0.268991
	minimum = 0.189333 (at node 88)
	maximum = 0.381667 (at node 118)
Injected packet length average = 18.0103
Accepted packet length average = 18.0078
Total in-flight flits = 152372 (142925 measured)
latency change    = 0.390561
throughput change = 0.00655742
Draining remaining packets ...
Class 0:
Remaining flits: 218682 218683 218684 218685 218686 218687 218688 218689 218690 218691 [...] (102585 flits)
Measured flits: 281304 281305 281306 281307 281308 281309 281310 281311 281312 281313 [...] (101060 flits)
Class 0:
Remaining flits: 272718 272719 272720 272721 272722 272723 272724 272725 272726 272727 [...] (53989 flits)
Measured flits: 281931 281932 281933 283392 283393 283394 283395 283396 283397 283398 [...] (53955 flits)
Class 0:
Remaining flits: 299214 299215 299216 299217 299218 299219 299220 299221 299222 299223 [...] (8645 flits)
Measured flits: 299214 299215 299216 299217 299218 299219 299220 299221 299222 299223 [...] (8645 flits)
Time taken is 9779 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3313.54 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6219 (1 samples)
Network latency average = 2699.29 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6046 (1 samples)
Flit latency average = 2480.87 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6029 (1 samples)
Fragmentation average = 17.9467 (1 samples)
	minimum = 0 (1 samples)
	maximum = 257 (1 samples)
Injected packet rate average = 0.017441 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.032 (1 samples)
Accepted packet rate average = 0.0149375 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.314116 (1 samples)
	minimum = 0.132 (1 samples)
	maximum = 0.573667 (1 samples)
Accepted flit rate average = 0.268991 (1 samples)
	minimum = 0.189333 (1 samples)
	maximum = 0.381667 (1 samples)
Injected packet size average = 18.0103 (1 samples)
Accepted packet size average = 18.0078 (1 samples)
Hops average = 5.10122 (1 samples)
Total run time 8.0419
