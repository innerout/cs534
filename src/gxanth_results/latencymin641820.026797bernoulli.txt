BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 252.088
	minimum = 22
	maximum = 772
Network latency average = 245.175
	minimum = 22
	maximum = 752
Slowest packet = 1143
Flit latency average = 212.146
	minimum = 5
	maximum = 763
Slowest flit = 19670
Fragmentation average = 53.9706
	minimum = 0
	maximum = 393
Injected packet rate average = 0.026651
	minimum = 0.011 (at node 190)
	maximum = 0.038 (at node 82)
Accepted packet rate average = 0.0137969
	minimum = 0.007 (at node 115)
	maximum = 0.023 (at node 131)
Injected flit rate average = 0.475339
	minimum = 0.198 (at node 190)
	maximum = 0.684 (at node 82)
Accepted flit rate average= 0.261896
	minimum = 0.126 (at node 115)
	maximum = 0.428 (at node 44)
Injected packet length average = 17.8356
Accepted packet length average = 18.9823
Total in-flight flits = 41876 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 462.042
	minimum = 22
	maximum = 1533
Network latency average = 451.803
	minimum = 22
	maximum = 1519
Slowest packet = 2410
Flit latency average = 411.496
	minimum = 5
	maximum = 1592
Slowest flit = 34657
Fragmentation average = 70.6695
	minimum = 0
	maximum = 566
Injected packet rate average = 0.0257604
	minimum = 0.016 (at node 24)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0145599
	minimum = 0.0085 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.461453
	minimum = 0.288 (at node 24)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.269698
	minimum = 0.153 (at node 116)
	maximum = 0.3825 (at node 152)
Injected packet length average = 17.9133
Accepted packet length average = 18.5233
Total in-flight flits = 74834 (0 measured)
latency change    = 0.454405
throughput change = 0.028929
Class 0:
Packet latency average = 1065.28
	minimum = 23
	maximum = 2154
Network latency average = 1030.81
	minimum = 22
	maximum = 2147
Slowest packet = 4190
Flit latency average = 985.456
	minimum = 5
	maximum = 2159
Slowest flit = 74612
Fragmentation average = 96.5111
	minimum = 0
	maximum = 588
Injected packet rate average = 0.0213385
	minimum = 0.001 (at node 148)
	maximum = 0.035 (at node 38)
Accepted packet rate average = 0.015276
	minimum = 0.005 (at node 17)
	maximum = 0.029 (at node 34)
Injected flit rate average = 0.383417
	minimum = 0.011 (at node 148)
	maximum = 0.644 (at node 49)
Accepted flit rate average= 0.278297
	minimum = 0.099 (at node 17)
	maximum = 0.52 (at node 34)
Injected packet length average = 17.9683
Accepted packet length average = 18.2179
Total in-flight flits = 95345 (0 measured)
latency change    = 0.566273
throughput change = 0.0308985
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 460.557
	minimum = 22
	maximum = 2177
Network latency average = 74.2764
	minimum = 22
	maximum = 973
Slowest packet = 14097
Flit latency average = 1374.96
	minimum = 5
	maximum = 2835
Slowest flit = 105353
Fragmentation average = 7.42276
	minimum = 0
	maximum = 64
Injected packet rate average = 0.0171042
	minimum = 0.002 (at node 166)
	maximum = 0.031 (at node 149)
Accepted packet rate average = 0.0151354
	minimum = 0.006 (at node 36)
	maximum = 0.026 (at node 128)
Injected flit rate average = 0.30663
	minimum = 0.04 (at node 0)
	maximum = 0.557 (at node 149)
Accepted flit rate average= 0.270172
	minimum = 0.103 (at node 36)
	maximum = 0.468 (at node 123)
Injected packet length average = 17.9272
Accepted packet length average = 17.8503
Total in-flight flits = 102584 (54522 measured)
latency change    = 1.31303
throughput change = 0.0300734
Class 0:
Packet latency average = 1266.99
	minimum = 22
	maximum = 3167
Network latency average = 737.444
	minimum = 22
	maximum = 1983
Slowest packet = 14097
Flit latency average = 1509.48
	minimum = 5
	maximum = 3175
Slowest flit = 132623
Fragmentation average = 47.976
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0163021
	minimum = 0.0025 (at node 48)
	maximum = 0.025 (at node 118)
Accepted packet rate average = 0.0150365
	minimum = 0.0085 (at node 36)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.293206
	minimum = 0.0535 (at node 48)
	maximum = 0.45 (at node 118)
Accepted flit rate average= 0.270107
	minimum = 0.162 (at node 36)
	maximum = 0.4195 (at node 128)
Injected packet length average = 17.9858
Accepted packet length average = 17.9635
Total in-flight flits = 104556 (92502 measured)
latency change    = 0.636496
throughput change = 0.000241031
Class 0:
Packet latency average = 1941.38
	minimum = 22
	maximum = 3871
Network latency average = 1377.52
	minimum = 22
	maximum = 2971
Slowest packet = 14097
Flit latency average = 1621
	minimum = 5
	maximum = 4100
Slowest flit = 146213
Fragmentation average = 78.7359
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0158785
	minimum = 0.00433333 (at node 48)
	maximum = 0.0246667 (at node 2)
Accepted packet rate average = 0.0150226
	minimum = 0.01 (at node 36)
	maximum = 0.0226667 (at node 128)
Injected flit rate average = 0.285436
	minimum = 0.0836667 (at node 48)
	maximum = 0.438333 (at node 2)
Accepted flit rate average= 0.269703
	minimum = 0.186667 (at node 36)
	maximum = 0.407333 (at node 128)
Injected packet length average = 17.9763
Accepted packet length average = 17.9532
Total in-flight flits = 104912 (103802 measured)
latency change    = 0.347376
throughput change = 0.00149663
Class 0:
Packet latency average = 2328.62
	minimum = 22
	maximum = 4727
Network latency average = 1659.48
	minimum = 22
	maximum = 3890
Slowest packet = 14097
Flit latency average = 1702.86
	minimum = 5
	maximum = 4551
Slowest flit = 209861
Fragmentation average = 89.9503
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0157279
	minimum = 0.00675 (at node 48)
	maximum = 0.02325 (at node 2)
Accepted packet rate average = 0.0149648
	minimum = 0.011 (at node 104)
	maximum = 0.0205 (at node 95)
Injected flit rate average = 0.282928
	minimum = 0.12325 (at node 48)
	maximum = 0.41475 (at node 2)
Accepted flit rate average= 0.268958
	minimum = 0.19925 (at node 64)
	maximum = 0.37025 (at node 95)
Injected packet length average = 17.989
Accepted packet length average = 17.9727
Total in-flight flits = 106441 (106421 measured)
latency change    = 0.166295
throughput change = 0.00276917
Class 0:
Packet latency average = 2631.5
	minimum = 22
	maximum = 5254
Network latency average = 1791.06
	minimum = 22
	maximum = 4735
Slowest packet = 14097
Flit latency average = 1759.93
	minimum = 5
	maximum = 4718
Slowest flit = 255563
Fragmentation average = 94.8374
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0154719
	minimum = 0.0062 (at node 60)
	maximum = 0.021 (at node 119)
Accepted packet rate average = 0.0149406
	minimum = 0.0108 (at node 52)
	maximum = 0.0202 (at node 128)
Injected flit rate average = 0.278327
	minimum = 0.1128 (at node 60)
	maximum = 0.378 (at node 119)
Accepted flit rate average= 0.268469
	minimum = 0.1928 (at node 52)
	maximum = 0.364 (at node 128)
Injected packet length average = 17.9892
Accepted packet length average = 17.969
Total in-flight flits = 105185 (105185 measured)
latency change    = 0.115097
throughput change = 0.00182361
Class 0:
Packet latency average = 2886.88
	minimum = 22
	maximum = 6121
Network latency average = 1866.07
	minimum = 22
	maximum = 5366
Slowest packet = 14097
Flit latency average = 1802.33
	minimum = 5
	maximum = 5349
Slowest flit = 269855
Fragmentation average = 97.6861
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0155278
	minimum = 0.00783333 (at node 60)
	maximum = 0.0203333 (at node 2)
Accepted packet rate average = 0.014947
	minimum = 0.012 (at node 144)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.279431
	minimum = 0.139833 (at node 60)
	maximum = 0.365833 (at node 119)
Accepted flit rate average= 0.268892
	minimum = 0.213833 (at node 64)
	maximum = 0.360167 (at node 128)
Injected packet length average = 17.9955
Accepted packet length average = 17.9897
Total in-flight flits = 107763 (107763 measured)
latency change    = 0.0884637
throughput change = 0.00157539
Class 0:
Packet latency average = 3129.31
	minimum = 22
	maximum = 6559
Network latency average = 1914.06
	minimum = 22
	maximum = 5901
Slowest packet = 14097
Flit latency average = 1833.01
	minimum = 5
	maximum = 5884
Slowest flit = 288395
Fragmentation average = 100.647
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0154025
	minimum = 0.00771429 (at node 60)
	maximum = 0.0207143 (at node 2)
Accepted packet rate average = 0.0149293
	minimum = 0.0117143 (at node 42)
	maximum = 0.0184286 (at node 128)
Injected flit rate average = 0.277118
	minimum = 0.139 (at node 60)
	maximum = 0.371714 (at node 2)
Accepted flit rate average= 0.268711
	minimum = 0.209143 (at node 42)
	maximum = 0.332857 (at node 128)
Injected packet length average = 17.9917
Accepted packet length average = 17.9989
Total in-flight flits = 106852 (106852 measured)
latency change    = 0.0774719
throughput change = 0.000676548
Draining all recorded packets ...
Class 0:
Remaining flits: 407088 407089 407090 407091 407092 407093 407094 407095 407096 407097 [...] (107368 flits)
Measured flits: 407088 407089 407090 407091 407092 407093 407094 407095 407096 407097 [...] (107368 flits)
Class 0:
Remaining flits: 501030 501031 501032 501033 501034 501035 501036 501037 501038 501039 [...] (105984 flits)
Measured flits: 501030 501031 501032 501033 501034 501035 501036 501037 501038 501039 [...] (105732 flits)
Class 0:
Remaining flits: 539954 539955 539956 539957 539958 539959 539960 539961 539962 539963 [...] (106976 flits)
Measured flits: 539954 539955 539956 539957 539958 539959 539960 539961 539962 539963 [...] (105770 flits)
Class 0:
Remaining flits: 591223 591224 591225 591226 591227 602424 602425 602426 602427 602428 [...] (107322 flits)
Measured flits: 591223 591224 591225 591226 591227 602424 602425 602426 602427 602428 [...] (99736 flits)
Class 0:
Remaining flits: 602424 602425 602426 602427 602428 602429 602430 602431 602432 602433 [...] (105540 flits)
Measured flits: 602424 602425 602426 602427 602428 602429 602430 602431 602432 602433 [...] (82807 flits)
Class 0:
Remaining flits: 693540 693541 693542 693543 693544 693545 693546 693547 693548 693549 [...] (105030 flits)
Measured flits: 693540 693541 693542 693543 693544 693545 693546 693547 693548 693549 [...] (57217 flits)
Class 0:
Remaining flits: 721949 721950 721951 721952 721953 721954 721955 721956 721957 721958 [...] (108423 flits)
Measured flits: 721949 721950 721951 721952 721953 721954 721955 721956 721957 721958 [...] (35571 flits)
Class 0:
Remaining flits: 760805 788251 788252 788253 788254 788255 793604 793605 793606 793607 [...] (106427 flits)
Measured flits: 760805 788251 788252 788253 788254 788255 793604 793605 793606 793607 [...] (20637 flits)
Class 0:
Remaining flits: 828558 828559 828560 828561 828562 828563 828564 828565 828566 828567 [...] (104747 flits)
Measured flits: 836730 836731 836732 836733 836734 836735 836736 836737 836738 836739 [...] (13592 flits)
Class 0:
Remaining flits: 876035 876036 876037 876038 876039 876040 876041 892278 892279 892280 [...] (105885 flits)
Measured flits: 898434 898435 898436 898437 898438 898439 898440 898441 898442 898443 [...] (10841 flits)
Class 0:
Remaining flits: 898445 898446 898447 898448 898449 898450 898451 907164 907165 907166 [...] (108000 flits)
Measured flits: 898445 898446 898447 898448 898449 898450 898451 1021284 1021285 1021286 [...] (7886 flits)
Class 0:
Remaining flits: 945054 945055 945056 945057 945058 945059 945060 945061 945062 945063 [...] (106381 flits)
Measured flits: 1065060 1065061 1065062 1065063 1065064 1065065 1065066 1065067 1065068 1065069 [...] (4923 flits)
Class 0:
Remaining flits: 999985 999986 999987 999988 999989 1018307 1018308 1018309 1018310 1018311 [...] (104123 flits)
Measured flits: 1116000 1116001 1116002 1116003 1116004 1116005 1116006 1116007 1116008 1116009 [...] (2307 flits)
Class 0:
Remaining flits: 1080360 1080361 1080362 1080363 1080364 1080365 1080366 1080367 1080368 1080369 [...] (105516 flits)
Measured flits: 1131516 1131517 1131518 1131519 1131520 1131521 1131522 1131523 1131524 1131525 [...] (922 flits)
Class 0:
Remaining flits: 1127202 1127203 1127204 1127205 1127206 1127207 1127208 1127209 1127210 1127211 [...] (103011 flits)
Measured flits: 1278774 1278775 1278776 1278777 1278778 1278779 1278780 1278781 1278782 1278783 [...] (159 flits)
Class 0:
Remaining flits: 1151460 1151461 1151462 1151463 1151464 1151465 1151466 1151467 1151468 1151469 [...] (103794 flits)
Measured flits: 1333116 1333117 1333118 1333119 1333120 1333121 1333122 1333123 1333124 1333125 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1251612 1251613 1251614 1251615 1251616 1251617 1251618 1251619 1251620 1251621 [...] (56539 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1251629 1288874 1288875 1288876 1288877 1288878 1288879 1288880 1288881 1288882 [...] (13555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1305511 1305512 1305513 1305514 1305515 1305516 1305517 1305518 1305519 1305520 [...] (349 flits)
Measured flits: (0 flits)
Time taken is 29818 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5278.81 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16704 (1 samples)
Network latency average = 2047.91 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6366 (1 samples)
Flit latency average = 1985.37 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6540 (1 samples)
Fragmentation average = 117.688 (1 samples)
	minimum = 0 (1 samples)
	maximum = 661 (1 samples)
Injected packet rate average = 0.0154025 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0207143 (1 samples)
Accepted packet rate average = 0.0149293 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0184286 (1 samples)
Injected flit rate average = 0.277118 (1 samples)
	minimum = 0.139 (1 samples)
	maximum = 0.371714 (1 samples)
Accepted flit rate average = 0.268711 (1 samples)
	minimum = 0.209143 (1 samples)
	maximum = 0.332857 (1 samples)
Injected packet size average = 17.9917 (1 samples)
Accepted packet size average = 17.9989 (1 samples)
Hops average = 5.0529 (1 samples)
Total run time 56.6873
