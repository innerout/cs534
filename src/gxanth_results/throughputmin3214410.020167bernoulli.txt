BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 255.722
	minimum = 23
	maximum = 843
Network latency average = 251.42
	minimum = 23
	maximum = 843
Slowest packet = 407
Flit latency average = 185.999
	minimum = 6
	maximum = 875
Slowest flit = 6999
Fragmentation average = 138.6
	minimum = 0
	maximum = 704
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00968229
	minimum = 0.003 (at node 174)
	maximum = 0.018 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.201333
	minimum = 0.073 (at node 174)
	maximum = 0.339 (at node 48)
Injected packet length average = 17.8234
Accepted packet length average = 20.794
Total in-flight flits = 30950 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 443.845
	minimum = 23
	maximum = 1707
Network latency average = 439.074
	minimum = 23
	maximum = 1699
Slowest packet = 747
Flit latency average = 352.508
	minimum = 6
	maximum = 1840
Slowest flit = 9839
Fragmentation average = 186.237
	minimum = 0
	maximum = 1391
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0109896
	minimum = 0.0065 (at node 135)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.211503
	minimum = 0.131 (at node 135)
	maximum = 0.328 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.2457
Total in-flight flits = 56987 (0 measured)
latency change    = 0.423849
throughput change = 0.0480811
Class 0:
Packet latency average = 922.183
	minimum = 23
	maximum = 2756
Network latency average = 917.119
	minimum = 23
	maximum = 2748
Slowest packet = 255
Flit latency average = 825.986
	minimum = 6
	maximum = 2771
Slowest flit = 14534
Fragmentation average = 233.192
	minimum = 0
	maximum = 1654
Injected packet rate average = 0.0200938
	minimum = 0.011 (at node 12)
	maximum = 0.033 (at node 114)
Accepted packet rate average = 0.0121302
	minimum = 0.004 (at node 62)
	maximum = 0.023 (at node 55)
Injected flit rate average = 0.36162
	minimum = 0.198 (at node 117)
	maximum = 0.594 (at node 114)
Accepted flit rate average= 0.218849
	minimum = 0.087 (at node 62)
	maximum = 0.372 (at node 55)
Injected packet length average = 17.9966
Accepted packet length average = 18.0416
Total in-flight flits = 84412 (0 measured)
latency change    = 0.518701
throughput change = 0.0335681
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 209.064
	minimum = 24
	maximum = 977
Network latency average = 201.917
	minimum = 24
	maximum = 977
Slowest packet = 11583
Flit latency average = 1169.08
	minimum = 6
	maximum = 3663
Slowest flit = 22425
Fragmentation average = 63.6848
	minimum = 0
	maximum = 406
Injected packet rate average = 0.0205052
	minimum = 0.01 (at node 152)
	maximum = 0.034 (at node 71)
Accepted packet rate average = 0.0122917
	minimum = 0.002 (at node 22)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.368589
	minimum = 0.182 (at node 152)
	maximum = 0.601 (at node 71)
Accepted flit rate average= 0.219193
	minimum = 0.072 (at node 22)
	maximum = 0.428 (at node 16)
Injected packet length average = 17.9754
Accepted packet length average = 17.8326
Total in-flight flits = 113193 (62448 measured)
latency change    = 3.41101
throughput change = 0.00156825
Class 0:
Packet latency average = 645.421
	minimum = 23
	maximum = 1991
Network latency average = 636.17
	minimum = 23
	maximum = 1991
Slowest packet = 11536
Flit latency average = 1363.04
	minimum = 6
	maximum = 4367
Slowest flit = 37907
Fragmentation average = 117.145
	minimum = 0
	maximum = 822
Injected packet rate average = 0.0200365
	minimum = 0.012 (at node 152)
	maximum = 0.028 (at node 71)
Accepted packet rate average = 0.0121146
	minimum = 0.0075 (at node 72)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.360339
	minimum = 0.2155 (at node 152)
	maximum = 0.504 (at node 71)
Accepted flit rate average= 0.217966
	minimum = 0.1365 (at node 126)
	maximum = 0.359 (at node 16)
Injected packet length average = 17.9841
Accepted packet length average = 17.992
Total in-flight flits = 139223 (110668 measured)
latency change    = 0.676081
throughput change = 0.00562731
Class 0:
Packet latency average = 1042.07
	minimum = 23
	maximum = 2964
Network latency average = 1029.35
	minimum = 23
	maximum = 2953
Slowest packet = 11685
Flit latency average = 1527
	minimum = 6
	maximum = 5253
Slowest flit = 31211
Fragmentation average = 145.08
	minimum = 0
	maximum = 1509
Injected packet rate average = 0.0198941
	minimum = 0.0123333 (at node 168)
	maximum = 0.0276667 (at node 7)
Accepted packet rate average = 0.0121372
	minimum = 0.008 (at node 17)
	maximum = 0.018 (at node 129)
Injected flit rate average = 0.357776
	minimum = 0.226333 (at node 168)
	maximum = 0.493333 (at node 7)
Accepted flit rate average= 0.217816
	minimum = 0.140333 (at node 139)
	maximum = 0.322 (at node 129)
Injected packet length average = 17.984
Accepted packet length average = 17.9462
Total in-flight flits = 165266 (148693 measured)
latency change    = 0.380636
throughput change = 0.000689452
Draining remaining packets ...
Class 0:
Remaining flits: 46080 46081 46082 46083 46084 46085 46086 46087 46088 46089 [...] (129132 flits)
Measured flits: 207702 207703 207704 207705 207706 207707 207708 207709 207710 207711 [...] (119068 flits)
Class 0:
Remaining flits: 57793 57794 57795 57796 57797 62153 67266 67267 67268 67269 [...] (94821 flits)
Measured flits: 207702 207703 207704 207705 207706 207707 207708 207709 207710 207711 [...] (88947 flits)
Class 0:
Remaining flits: 67266 67267 67268 67269 67270 67271 67272 67273 67274 67275 [...] (61459 flits)
Measured flits: 207702 207703 207704 207705 207706 207707 207708 207709 207710 207711 [...] (58389 flits)
Class 0:
Remaining flits: 67266 67267 67268 67269 67270 67271 67272 67273 67274 67275 [...] (29296 flits)
Measured flits: 207810 207811 207812 207813 207814 207815 207816 207817 207818 207819 [...] (28084 flits)
Class 0:
Remaining flits: 67279 67280 67281 67282 67283 76751 85572 85573 85574 85575 [...] (4665 flits)
Measured flits: 208998 208999 209000 209001 209002 209003 209004 209005 209006 209007 [...] (4325 flits)
Time taken is 11632 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3127.86 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8367 (1 samples)
Network latency average = 3105.87 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8320 (1 samples)
Flit latency average = 2891.76 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10094 (1 samples)
Fragmentation average = 173.651 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2595 (1 samples)
Injected packet rate average = 0.0198941 (1 samples)
	minimum = 0.0123333 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0121372 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.357776 (1 samples)
	minimum = 0.226333 (1 samples)
	maximum = 0.493333 (1 samples)
Accepted flit rate average = 0.217816 (1 samples)
	minimum = 0.140333 (1 samples)
	maximum = 0.322 (1 samples)
Injected packet size average = 17.984 (1 samples)
Accepted packet size average = 17.9462 (1 samples)
Hops average = 5.09283 (1 samples)
Total run time 11.1288
