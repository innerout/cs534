BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.283
	minimum = 23
	maximum = 929
Network latency average = 278.554
	minimum = 23
	maximum = 893
Slowest packet = 584
Flit latency average = 235.471
	minimum = 6
	maximum = 950
Slowest flit = 3768
Fragmentation average = 54.0824
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0137917
	minimum = 0.004 (at node 184)
	maximum = 0.026 (at node 95)
Accepted packet rate average = 0.00891146
	minimum = 0.002 (at node 41)
	maximum = 0.015 (at node 73)
Injected flit rate average = 0.244964
	minimum = 0.062 (at node 184)
	maximum = 0.464 (at node 95)
Accepted flit rate average= 0.166698
	minimum = 0.036 (at node 41)
	maximum = 0.284 (at node 76)
Injected packet length average = 17.7617
Accepted packet length average = 18.706
Total in-flight flits = 17746 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 676.31
	minimum = 23
	maximum = 1760
Network latency average = 403.841
	minimum = 23
	maximum = 1602
Slowest packet = 789
Flit latency average = 351.987
	minimum = 6
	maximum = 1741
Slowest flit = 21505
Fragmentation average = 54.7954
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0114661
	minimum = 0.0035 (at node 184)
	maximum = 0.022 (at node 170)
Accepted packet rate average = 0.009
	minimum = 0.0045 (at node 115)
	maximum = 0.014 (at node 70)
Injected flit rate average = 0.204354
	minimum = 0.063 (at node 184)
	maximum = 0.396 (at node 170)
Accepted flit rate average= 0.165766
	minimum = 0.086 (at node 116)
	maximum = 0.2605 (at node 152)
Injected packet length average = 17.8224
Accepted packet length average = 18.4184
Total in-flight flits = 17328 (0 measured)
latency change    = 0.517554
throughput change = 0.00562416
Class 0:
Packet latency average = 1729.81
	minimum = 324
	maximum = 2671
Network latency average = 533.453
	minimum = 27
	maximum = 2286
Slowest packet = 1283
Flit latency average = 469.149
	minimum = 6
	maximum = 2241
Slowest flit = 34541
Fragmentation average = 61.3232
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00901563
	minimum = 0 (at node 188)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.00908854
	minimum = 0.003 (at node 83)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.162937
	minimum = 0 (at node 188)
	maximum = 0.399 (at node 126)
Accepted flit rate average= 0.161958
	minimum = 0.054 (at node 153)
	maximum = 0.318 (at node 63)
Injected packet length average = 18.0728
Accepted packet length average = 17.8201
Total in-flight flits = 17624 (0 measured)
latency change    = 0.609027
throughput change = 0.0235078
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2478.93
	minimum = 700
	maximum = 3389
Network latency average = 312.323
	minimum = 23
	maximum = 977
Slowest packet = 6262
Flit latency average = 482.015
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 51.8061
	minimum = 0
	maximum = 130
Injected packet rate average = 0.00903125
	minimum = 0 (at node 14)
	maximum = 0.022 (at node 7)
Accepted packet rate average = 0.00896875
	minimum = 0.001 (at node 184)
	maximum = 0.017 (at node 34)
Injected flit rate average = 0.16138
	minimum = 0 (at node 14)
	maximum = 0.38 (at node 7)
Accepted flit rate average= 0.161635
	minimum = 0.018 (at node 184)
	maximum = 0.31 (at node 34)
Injected packet length average = 17.8691
Accepted packet length average = 18.0221
Total in-flight flits = 17622 (15947 measured)
latency change    = 0.302192
throughput change = 0.00199781
Class 0:
Packet latency average = 2919.55
	minimum = 700
	maximum = 4398
Network latency average = 435.866
	minimum = 23
	maximum = 1866
Slowest packet = 6262
Flit latency average = 486.118
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 55.9713
	minimum = 0
	maximum = 132
Injected packet rate average = 0.009
	minimum = 0.002 (at node 14)
	maximum = 0.0165 (at node 6)
Accepted packet rate average = 0.00894792
	minimum = 0.0045 (at node 4)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.161956
	minimum = 0.036 (at node 14)
	maximum = 0.2915 (at node 6)
Accepted flit rate average= 0.161664
	minimum = 0.079 (at node 4)
	maximum = 0.3185 (at node 34)
Injected packet length average = 17.9951
Accepted packet length average = 18.0672
Total in-flight flits = 17735 (17653 measured)
latency change    = 0.150921
throughput change = 0.000177194
Class 0:
Packet latency average = 3312.13
	minimum = 700
	maximum = 5233
Network latency average = 482.535
	minimum = 23
	maximum = 2689
Slowest packet = 6262
Flit latency average = 486.39
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 56.9959
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0089566
	minimum = 0.003 (at node 14)
	maximum = 0.0153333 (at node 174)
Accepted packet rate average = 0.00894965
	minimum = 0.00533333 (at node 1)
	maximum = 0.0143333 (at node 16)
Injected flit rate average = 0.16097
	minimum = 0.0516667 (at node 14)
	maximum = 0.274333 (at node 174)
Accepted flit rate average= 0.161174
	minimum = 0.0943333 (at node 1)
	maximum = 0.258 (at node 16)
Injected packet length average = 17.9723
Accepted packet length average = 18.0089
Total in-flight flits = 17506 (17506 measured)
latency change    = 0.118529
throughput change = 0.003043
Class 0:
Packet latency average = 3687.02
	minimum = 700
	maximum = 6259
Network latency average = 501.718
	minimum = 23
	maximum = 2689
Slowest packet = 6262
Flit latency average = 487.105
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 56.4958
	minimum = 0
	maximum = 137
Injected packet rate average = 0.00897135
	minimum = 0.004 (at node 14)
	maximum = 0.0155 (at node 174)
Accepted packet rate average = 0.00894661
	minimum = 0.0045 (at node 4)
	maximum = 0.01325 (at node 34)
Injected flit rate average = 0.161345
	minimum = 0.072 (at node 14)
	maximum = 0.28 (at node 174)
Accepted flit rate average= 0.161082
	minimum = 0.079 (at node 4)
	maximum = 0.23925 (at node 34)
Injected packet length average = 17.9845
Accepted packet length average = 18.0048
Total in-flight flits = 17825 (17825 measured)
latency change    = 0.101679
throughput change = 0.000568529
Class 0:
Packet latency average = 4048.2
	minimum = 700
	maximum = 6988
Network latency average = 513.021
	minimum = 23
	maximum = 2838
Slowest packet = 6262
Flit latency average = 487.069
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 56.7811
	minimum = 0
	maximum = 137
Injected packet rate average = 0.00892708
	minimum = 0.005 (at node 148)
	maximum = 0.0142 (at node 51)
Accepted packet rate average = 0.00892708
	minimum = 0.0048 (at node 4)
	maximum = 0.0124 (at node 16)
Injected flit rate average = 0.160651
	minimum = 0.0912 (at node 148)
	maximum = 0.2568 (at node 51)
Accepted flit rate average= 0.160879
	minimum = 0.0882 (at node 4)
	maximum = 0.2264 (at node 16)
Injected packet length average = 17.9959
Accepted packet length average = 18.0215
Total in-flight flits = 17458 (17458 measured)
latency change    = 0.0892197
throughput change = 0.00126097
Class 0:
Packet latency average = 4402.75
	minimum = 700
	maximum = 7665
Network latency average = 518.669
	minimum = 23
	maximum = 2838
Slowest packet = 6262
Flit latency average = 486.24
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 57.3371
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00891233
	minimum = 0.00516667 (at node 75)
	maximum = 0.0146667 (at node 3)
Accepted packet rate average = 0.0088941
	minimum = 0.006 (at node 36)
	maximum = 0.0125 (at node 128)
Injected flit rate average = 0.160353
	minimum = 0.093 (at node 75)
	maximum = 0.264667 (at node 3)
Accepted flit rate average= 0.160219
	minimum = 0.108 (at node 36)
	maximum = 0.2225 (at node 128)
Injected packet length average = 17.9923
Accepted packet length average = 18.0141
Total in-flight flits = 17804 (17804 measured)
latency change    = 0.0805279
throughput change = 0.00412197
Class 0:
Packet latency average = 4762.43
	minimum = 700
	maximum = 8454
Network latency average = 522.714
	minimum = 23
	maximum = 2838
Slowest packet = 6262
Flit latency average = 485.347
	minimum = 6
	maximum = 2899
Slowest flit = 40255
Fragmentation average = 57.8552
	minimum = 0
	maximum = 157
Injected packet rate average = 0.00886607
	minimum = 0.00485714 (at node 191)
	maximum = 0.0151429 (at node 3)
Accepted packet rate average = 0.00888095
	minimum = 0.006 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.159585
	minimum = 0.0874286 (at node 191)
	maximum = 0.273143 (at node 3)
Accepted flit rate average= 0.159757
	minimum = 0.11 (at node 36)
	maximum = 0.216 (at node 128)
Injected packet length average = 17.9995
Accepted packet length average = 17.9887
Total in-flight flits = 17579 (17579 measured)
latency change    = 0.0755242
throughput change = 0.00289223
Draining all recorded packets ...
Class 0:
Remaining flits: 242550 242551 242552 242553 242554 242555 242556 242557 242558 242559 [...] (17447 flits)
Measured flits: 242550 242551 242552 242553 242554 242555 242556 242557 242558 242559 [...] (17447 flits)
Class 0:
Remaining flits: 283662 283663 283664 283665 283666 283667 283668 283669 283670 283671 [...] (17404 flits)
Measured flits: 283662 283663 283664 283665 283666 283667 283668 283669 283670 283671 [...] (17404 flits)
Class 0:
Remaining flits: 322866 322867 322868 322869 322870 322871 322872 322873 322874 322875 [...] (17947 flits)
Measured flits: 322866 322867 322868 322869 322870 322871 322872 322873 322874 322875 [...] (17947 flits)
Class 0:
Remaining flits: 383742 383743 383744 383745 383746 383747 383748 383749 383750 383751 [...] (17981 flits)
Measured flits: 383742 383743 383744 383745 383746 383747 383748 383749 383750 383751 [...] (17981 flits)
Class 0:
Remaining flits: 388825 388826 388827 388828 388829 388830 388831 388832 388833 388834 [...] (17648 flits)
Measured flits: 388825 388826 388827 388828 388829 388830 388831 388832 388833 388834 [...] (17648 flits)
Class 0:
Remaining flits: 406224 406225 406226 406227 406228 406229 406230 406231 406232 406233 [...] (17635 flits)
Measured flits: 406224 406225 406226 406227 406228 406229 406230 406231 406232 406233 [...] (17635 flits)
Class 0:
Remaining flits: 439762 439763 439764 439765 439766 439767 439768 439769 439770 439771 [...] (17709 flits)
Measured flits: 439762 439763 439764 439765 439766 439767 439768 439769 439770 439771 [...] (17709 flits)
Class 0:
Remaining flits: 506016 506017 506018 506019 506020 506021 506022 506023 506024 506025 [...] (17375 flits)
Measured flits: 506016 506017 506018 506019 506020 506021 506022 506023 506024 506025 [...] (17375 flits)
Class 0:
Remaining flits: 522576 522577 522578 522579 522580 522581 522582 522583 522584 522585 [...] (17745 flits)
Measured flits: 522576 522577 522578 522579 522580 522581 522582 522583 522584 522585 [...] (17745 flits)
Class 0:
Remaining flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (18030 flits)
Measured flits: 570528 570529 570530 570531 570532 570533 570534 570535 570536 570537 [...] (18030 flits)
Class 0:
Remaining flits: 595638 595639 595640 595641 595642 595643 595644 595645 595646 595647 [...] (17785 flits)
Measured flits: 595638 595639 595640 595641 595642 595643 595644 595645 595646 595647 [...] (17785 flits)
Class 0:
Remaining flits: 627912 627913 627914 627915 627916 627917 627918 627919 627920 627921 [...] (17585 flits)
Measured flits: 627912 627913 627914 627915 627916 627917 627918 627919 627920 627921 [...] (17585 flits)
Class 0:
Remaining flits: 658800 658801 658802 658803 658804 658805 658806 658807 658808 658809 [...] (17650 flits)
Measured flits: 658800 658801 658802 658803 658804 658805 658806 658807 658808 658809 [...] (17650 flits)
Class 0:
Remaining flits: 686574 686575 686576 686577 686578 686579 686580 686581 686582 686583 [...] (17343 flits)
Measured flits: 686574 686575 686576 686577 686578 686579 686580 686581 686582 686583 [...] (17343 flits)
Class 0:
Remaining flits: 692226 692227 692228 692229 692230 692231 692232 692233 692234 692235 [...] (17894 flits)
Measured flits: 692226 692227 692228 692229 692230 692231 692232 692233 692234 692235 [...] (17894 flits)
Class 0:
Remaining flits: 744066 744067 744068 744069 744070 744071 744072 744073 744074 744075 [...] (17596 flits)
Measured flits: 744066 744067 744068 744069 744070 744071 744072 744073 744074 744075 [...] (17506 flits)
Class 0:
Remaining flits: 784548 784549 784550 784551 784552 784553 784554 784555 784556 784557 [...] (17817 flits)
Measured flits: 784548 784549 784550 784551 784552 784553 784554 784555 784556 784557 [...] (17501 flits)
Class 0:
Remaining flits: 825845 825846 825847 825848 825849 825850 825851 825852 825853 825854 [...] (17781 flits)
Measured flits: 825845 825846 825847 825848 825849 825850 825851 825852 825853 825854 [...] (17393 flits)
Class 0:
Remaining flits: 843480 843481 843482 843483 843484 843485 843486 843487 843488 843489 [...] (17500 flits)
Measured flits: 843480 843481 843482 843483 843484 843485 843486 843487 843488 843489 [...] (16822 flits)
Class 0:
Remaining flits: 885295 885296 885297 885298 885299 885300 885301 885302 885303 885304 [...] (17555 flits)
Measured flits: 885295 885296 885297 885298 885299 885300 885301 885302 885303 885304 [...] (15876 flits)
Class 0:
Remaining flits: 912330 912331 912332 912333 912334 912335 912336 912337 912338 912339 [...] (17567 flits)
Measured flits: 912330 912331 912332 912333 912334 912335 912336 912337 912338 912339 [...] (15147 flits)
Class 0:
Remaining flits: 931032 931033 931034 931035 931036 931037 931038 931039 931040 931041 [...] (17451 flits)
Measured flits: 931032 931033 931034 931035 931036 931037 931038 931039 931040 931041 [...] (13738 flits)
Class 0:
Remaining flits: 931032 931033 931034 931035 931036 931037 931038 931039 931040 931041 [...] (17592 flits)
Measured flits: 931032 931033 931034 931035 931036 931037 931038 931039 931040 931041 [...] (12429 flits)
Class 0:
Remaining flits: 946746 946747 946748 946749 946750 946751 946752 946753 946754 946755 [...] (17053 flits)
Measured flits: 946746 946747 946748 946749 946750 946751 946752 946753 946754 946755 [...] (10335 flits)
Class 0:
Remaining flits: 1021716 1021717 1021718 1021719 1021720 1021721 1021722 1021723 1021724 1021725 [...] (17744 flits)
Measured flits: 1021716 1021717 1021718 1021719 1021720 1021721 1021722 1021723 1021724 1021725 [...] (8724 flits)
Class 0:
Remaining flits: 1041444 1041445 1041446 1041447 1041448 1041449 1041450 1041451 1041452 1041453 [...] (17407 flits)
Measured flits: 1041444 1041445 1041446 1041447 1041448 1041449 1041450 1041451 1041452 1041453 [...] (5952 flits)
Class 0:
Remaining flits: 1075236 1075237 1075238 1075239 1075240 1075241 1075242 1075243 1075244 1075245 [...] (17795 flits)
Measured flits: 1083600 1083601 1083602 1083603 1083604 1083605 1083606 1083607 1083608 1083609 [...] (4756 flits)
Class 0:
Remaining flits: 1131318 1131319 1131320 1131321 1131322 1131323 1131324 1131325 1131326 1131327 [...] (17503 flits)
Measured flits: 1141596 1141597 1141598 1141599 1141600 1141601 1141602 1141603 1141604 1141605 [...] (3130 flits)
Class 0:
Remaining flits: 1146654 1146655 1146656 1146657 1146658 1146659 1146660 1146661 1146662 1146663 [...] (18340 flits)
Measured flits: 1164078 1164079 1164080 1164081 1164082 1164083 1164084 1164085 1164086 1164087 [...] (2206 flits)
Class 0:
Remaining flits: 1185120 1185121 1185122 1185123 1185124 1185125 1185126 1185127 1185128 1185129 [...] (18044 flits)
Measured flits: 1202508 1202509 1202510 1202511 1202512 1202513 1202514 1202515 1202516 1202517 [...] (1392 flits)
Class 0:
Remaining flits: 1210818 1210819 1210820 1210821 1210822 1210823 1213127 1218168 1218169 1218170 [...] (17962 flits)
Measured flits: 1235016 1235017 1235018 1235019 1235020 1235021 1235022 1235023 1235024 1235025 [...] (593 flits)
Class 0:
Remaining flits: 1248426 1248427 1248428 1248429 1248430 1248431 1248432 1248433 1248434 1248435 [...] (17722 flits)
Measured flits: 1275048 1275049 1275050 1275051 1275052 1275053 1275054 1275055 1275056 1275057 [...] (234 flits)
Class 0:
Remaining flits: 1267488 1267489 1267490 1267491 1267492 1267493 1267494 1267495 1267496 1267497 [...] (17903 flits)
Measured flits: 1320331 1320332 1320333 1320334 1320335 1334304 1334305 1334306 1334307 1334308 [...] (95 flits)
Class 0:
Remaining flits: 1308078 1308079 1308080 1308081 1308082 1308083 1308084 1308085 1308086 1308087 [...] (17955 flits)
Measured flits: 1349118 1349119 1349120 1349121 1349122 1349123 1349124 1349125 1349126 1349127 [...] (90 flits)
Class 0:
Remaining flits: 1326222 1326223 1326224 1326225 1326226 1326227 1326228 1326229 1326230 1326231 [...] (17763 flits)
Measured flits: 1357902 1357903 1357904 1357905 1357906 1357907 1357908 1357909 1357910 1357911 [...] (90 flits)
Draining remaining packets ...
Time taken is 46506 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13982.6 (1 samples)
	minimum = 700 (1 samples)
	maximum = 35585 (1 samples)
Network latency average = 548.391 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4584 (1 samples)
Flit latency average = 488.234 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4506 (1 samples)
Fragmentation average = 57.4952 (1 samples)
	minimum = 0 (1 samples)
	maximum = 160 (1 samples)
Injected packet rate average = 0.00886607 (1 samples)
	minimum = 0.00485714 (1 samples)
	maximum = 0.0151429 (1 samples)
Accepted packet rate average = 0.00888095 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.159585 (1 samples)
	minimum = 0.0874286 (1 samples)
	maximum = 0.273143 (1 samples)
Accepted flit rate average = 0.159757 (1 samples)
	minimum = 0.11 (1 samples)
	maximum = 0.216 (1 samples)
Injected packet size average = 17.9995 (1 samples)
Accepted packet size average = 17.9887 (1 samples)
Hops average = 5.0585 (1 samples)
Total run time 31.2416
