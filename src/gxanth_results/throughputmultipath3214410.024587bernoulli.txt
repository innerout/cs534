BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 280.255
	minimum = 23
	maximum = 900
Network latency average = 274.558
	minimum = 23
	maximum = 900
Slowest packet = 299
Flit latency average = 209.395
	minimum = 6
	maximum = 914
Slowest flit = 3922
Fragmentation average = 147.299
	minimum = 0
	maximum = 726
Injected packet rate average = 0.0246823
	minimum = 0.014 (at node 179)
	maximum = 0.039 (at node 160)
Accepted packet rate average = 0.0101198
	minimum = 0.003 (at node 174)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.440411
	minimum = 0.237 (at node 179)
	maximum = 0.695 (at node 160)
Accepted flit rate average= 0.210089
	minimum = 0.089 (at node 64)
	maximum = 0.357 (at node 131)
Injected packet length average = 17.8432
Accepted packet length average = 20.7602
Total in-flight flits = 44965 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 504.537
	minimum = 23
	maximum = 1803
Network latency average = 498.366
	minimum = 23
	maximum = 1803
Slowest packet = 515
Flit latency average = 417.004
	minimum = 6
	maximum = 1848
Slowest flit = 6151
Fragmentation average = 189.046
	minimum = 0
	maximum = 1480
Injected packet rate average = 0.0244323
	minimum = 0.018 (at node 1)
	maximum = 0.0315 (at node 123)
Accepted packet rate average = 0.0112995
	minimum = 0.0065 (at node 81)
	maximum = 0.0165 (at node 152)
Injected flit rate average = 0.437922
	minimum = 0.324 (at node 1)
	maximum = 0.567 (at node 123)
Accepted flit rate average= 0.217094
	minimum = 0.1335 (at node 81)
	maximum = 0.326 (at node 63)
Injected packet length average = 17.9239
Accepted packet length average = 19.2127
Total in-flight flits = 85512 (0 measured)
latency change    = 0.444529
throughput change = 0.0322681
Class 0:
Packet latency average = 1114.94
	minimum = 25
	maximum = 2709
Network latency average = 1106.58
	minimum = 25
	maximum = 2676
Slowest packet = 1480
Flit latency average = 1045.97
	minimum = 6
	maximum = 2829
Slowest flit = 12983
Fragmentation average = 224.862
	minimum = 0
	maximum = 2181
Injected packet rate average = 0.0236823
	minimum = 0.012 (at node 113)
	maximum = 0.041 (at node 185)
Accepted packet rate average = 0.0124792
	minimum = 0.003 (at node 91)
	maximum = 0.022 (at node 95)
Injected flit rate average = 0.425495
	minimum = 0.21 (at node 113)
	maximum = 0.739 (at node 185)
Accepted flit rate average= 0.225688
	minimum = 0.057 (at node 91)
	maximum = 0.397 (at node 159)
Injected packet length average = 17.9668
Accepted packet length average = 18.0851
Total in-flight flits = 124062 (0 measured)
latency change    = 0.547476
throughput change = 0.0380781
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 149.629
	minimum = 24
	maximum = 1082
Network latency average = 94.7173
	minimum = 24
	maximum = 905
Slowest packet = 13960
Flit latency average = 1473.68
	minimum = 6
	maximum = 3708
Slowest flit = 20339
Fragmentation average = 24.1915
	minimum = 0
	maximum = 277
Injected packet rate average = 0.0229375
	minimum = 0 (at node 124)
	maximum = 0.035 (at node 94)
Accepted packet rate average = 0.0124792
	minimum = 0.003 (at node 47)
	maximum = 0.024 (at node 110)
Injected flit rate average = 0.413021
	minimum = 0.015 (at node 124)
	maximum = 0.63 (at node 173)
Accepted flit rate average= 0.224083
	minimum = 0.079 (at node 190)
	maximum = 0.432 (at node 110)
Injected packet length average = 18.0064
Accepted packet length average = 17.9566
Total in-flight flits = 160310 (73045 measured)
latency change    = 6.45134
throughput change = 0.0071588
Class 0:
Packet latency average = 548.347
	minimum = 24
	maximum = 2740
Network latency average = 474.615
	minimum = 24
	maximum = 1968
Slowest packet = 14261
Flit latency average = 1705.23
	minimum = 6
	maximum = 4472
Slowest flit = 37809
Fragmentation average = 70.0223
	minimum = 0
	maximum = 1025
Injected packet rate average = 0.022526
	minimum = 0.0045 (at node 124)
	maximum = 0.033 (at node 93)
Accepted packet rate average = 0.0123281
	minimum = 0.0065 (at node 77)
	maximum = 0.0185 (at node 178)
Injected flit rate average = 0.405648
	minimum = 0.084 (at node 124)
	maximum = 0.5945 (at node 93)
Accepted flit rate average= 0.221255
	minimum = 0.119 (at node 89)
	maximum = 0.321 (at node 110)
Injected packet length average = 18.008
Accepted packet length average = 17.9472
Total in-flight flits = 194818 (138341 measured)
latency change    = 0.727127
throughput change = 0.0127822
Class 0:
Packet latency average = 1161.99
	minimum = 24
	maximum = 3703
Network latency average = 1071.87
	minimum = 24
	maximum = 2961
Slowest packet = 14261
Flit latency average = 1934.91
	minimum = 6
	maximum = 5339
Slowest flit = 55146
Fragmentation average = 116.487
	minimum = 0
	maximum = 1786
Injected packet rate average = 0.0222795
	minimum = 0.00766667 (at node 68)
	maximum = 0.0323333 (at node 93)
Accepted packet rate average = 0.0122135
	minimum = 0.00733333 (at node 89)
	maximum = 0.0176667 (at node 178)
Injected flit rate average = 0.400948
	minimum = 0.140667 (at node 68)
	maximum = 0.585333 (at node 93)
Accepted flit rate average= 0.218872
	minimum = 0.121667 (at node 89)
	maximum = 0.297667 (at node 8)
Injected packet length average = 17.9963
Accepted packet length average = 17.9204
Total in-flight flits = 228986 (193622 measured)
latency change    = 0.528096
throughput change = 0.0108908
Draining remaining packets ...
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (191180 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (168930 flits)
Class 0:
Remaining flits: 42786 42787 42788 42789 42790 42791 42792 42793 42794 42795 [...] (155271 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (141424 flits)
Class 0:
Remaining flits: 42792 42793 42794 42795 42796 42797 42798 42799 42800 42801 [...] (121145 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (112684 flits)
Class 0:
Remaining flits: 78022 78023 78024 78025 78026 78027 78028 78029 87501 87502 [...] (87866 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (83096 flits)
Class 0:
Remaining flits: 138672 138673 138674 138675 138676 138677 138678 138679 138680 138681 [...] (55116 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (52571 flits)
Class 0:
Remaining flits: 138730 138731 138732 138733 138734 138735 138736 138737 138738 138739 [...] (25707 flits)
Measured flits: 251751 251752 251753 251754 251755 251756 251757 251758 251759 251760 [...] (24632 flits)
Class 0:
Remaining flits: 167580 167581 167582 167583 167584 167585 167586 167587 167588 167589 [...] (4090 flits)
Measured flits: 252954 252955 252956 252957 252958 252959 252960 252961 252962 252963 [...] (3952 flits)
Time taken is 13906 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4502.15 (1 samples)
	minimum = 24 (1 samples)
	maximum = 10446 (1 samples)
Network latency average = 4393.94 (1 samples)
	minimum = 24 (1 samples)
	maximum = 10443 (1 samples)
Flit latency average = 3920.55 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11531 (1 samples)
Fragmentation average = 175.226 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2767 (1 samples)
Injected packet rate average = 0.0222795 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0122135 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.400948 (1 samples)
	minimum = 0.140667 (1 samples)
	maximum = 0.585333 (1 samples)
Accepted flit rate average = 0.218872 (1 samples)
	minimum = 0.121667 (1 samples)
	maximum = 0.297667 (1 samples)
Injected packet size average = 17.9963 (1 samples)
Accepted packet size average = 17.9204 (1 samples)
Hops average = 5.14845 (1 samples)
Total run time 14.1724
