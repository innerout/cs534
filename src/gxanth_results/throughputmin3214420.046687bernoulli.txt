BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.64
	minimum = 24
	maximum = 867
Network latency average = 307.527
	minimum = 22
	maximum = 817
Slowest packet = 660
Flit latency average = 270.726
	minimum = 5
	maximum = 862
Slowest flit = 17354
Fragmentation average = 84.3751
	minimum = 0
	maximum = 420
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0146771
	minimum = 0.006 (at node 41)
	maximum = 0.025 (at node 168)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.28474
	minimum = 0.129 (at node 64)
	maximum = 0.462 (at node 22)
Injected packet length average = 17.8602
Accepted packet length average = 19.4003
Total in-flight flits = 99698 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 596.455
	minimum = 23
	maximum = 1770
Network latency average = 570.239
	minimum = 22
	maximum = 1677
Slowest packet = 964
Flit latency average = 527.641
	minimum = 5
	maximum = 1743
Slowest flit = 33941
Fragmentation average = 112.337
	minimum = 0
	maximum = 466
Injected packet rate average = 0.0449609
	minimum = 0.0345 (at node 154)
	maximum = 0.055 (at node 42)
Accepted packet rate average = 0.0157578
	minimum = 0.009 (at node 83)
	maximum = 0.0225 (at node 76)
Injected flit rate average = 0.805891
	minimum = 0.62 (at node 154)
	maximum = 0.9885 (at node 53)
Accepted flit rate average= 0.29693
	minimum = 0.166 (at node 83)
	maximum = 0.4225 (at node 152)
Injected packet length average = 17.9242
Accepted packet length average = 18.8433
Total in-flight flits = 196785 (0 measured)
latency change    = 0.452365
throughput change = 0.0410538
Class 0:
Packet latency average = 1478.55
	minimum = 24
	maximum = 2628
Network latency average = 1436.24
	minimum = 23
	maximum = 2582
Slowest packet = 3225
Flit latency average = 1400.03
	minimum = 5
	maximum = 2658
Slowest flit = 49217
Fragmentation average = 158.356
	minimum = 0
	maximum = 453
Injected packet rate average = 0.041625
	minimum = 0.01 (at node 148)
	maximum = 0.056 (at node 53)
Accepted packet rate average = 0.0164792
	minimum = 0.005 (at node 40)
	maximum = 0.03 (at node 68)
Injected flit rate average = 0.75038
	minimum = 0.172 (at node 148)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.297057
	minimum = 0.119 (at node 163)
	maximum = 0.525 (at node 68)
Injected packet length average = 18.0272
Accepted packet length average = 18.0262
Total in-flight flits = 283714 (0 measured)
latency change    = 0.596596
throughput change = 0.000429561
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 356.486
	minimum = 23
	maximum = 1565
Network latency average = 137.389
	minimum = 22
	maximum = 933
Slowest packet = 25268
Flit latency average = 2096.72
	minimum = 5
	maximum = 3240
Slowest flit = 110213
Fragmentation average = 6.47569
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0376042
	minimum = 0 (at node 36)
	maximum = 0.056 (at node 81)
Accepted packet rate average = 0.0155625
	minimum = 0.005 (at node 36)
	maximum = 0.024 (at node 63)
Injected flit rate average = 0.675818
	minimum = 0 (at node 36)
	maximum = 1 (at node 81)
Accepted flit rate average= 0.279
	minimum = 0.083 (at node 36)
	maximum = 0.451 (at node 63)
Injected packet length average = 17.9719
Accepted packet length average = 17.9277
Total in-flight flits = 360412 (125014 measured)
latency change    = 3.14757
throughput change = 0.0647215
Class 0:
Packet latency average = 620.367
	minimum = 23
	maximum = 2589
Network latency average = 316.17
	minimum = 22
	maximum = 1824
Slowest packet = 25268
Flit latency average = 2441.76
	minimum = 5
	maximum = 4159
Slowest flit = 126195
Fragmentation average = 7.01873
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0311719
	minimum = 0.0005 (at node 152)
	maximum = 0.0475 (at node 83)
Accepted packet rate average = 0.0154063
	minimum = 0.0085 (at node 36)
	maximum = 0.0225 (at node 44)
Injected flit rate average = 0.560891
	minimum = 0.011 (at node 152)
	maximum = 0.849 (at node 83)
Accepted flit rate average= 0.275711
	minimum = 0.1435 (at node 36)
	maximum = 0.4 (at node 123)
Injected packet length average = 17.9935
Accepted packet length average = 17.896
Total in-flight flits = 393787 (206304 measured)
latency change    = 0.425363
throughput change = 0.0119294
Class 0:
Packet latency average = 978.646
	minimum = 22
	maximum = 3229
Network latency average = 563.911
	minimum = 22
	maximum = 2891
Slowest packet = 25268
Flit latency average = 2764.58
	minimum = 5
	maximum = 4874
Slowest flit = 170999
Fragmentation average = 7.6
	minimum = 0
	maximum = 176
Injected packet rate average = 0.0264948
	minimum = 0.00466667 (at node 176)
	maximum = 0.0416667 (at node 83)
Accepted packet rate average = 0.0153542
	minimum = 0.0106667 (at node 5)
	maximum = 0.0213333 (at node 118)
Injected flit rate average = 0.476821
	minimum = 0.085 (at node 176)
	maximum = 0.75 (at node 83)
Accepted flit rate average= 0.273142
	minimum = 0.182667 (at node 36)
	maximum = 0.379 (at node 118)
Injected packet length average = 17.9968
Accepted packet length average = 17.7895
Total in-flight flits = 401874 (261430 measured)
latency change    = 0.366097
throughput change = 0.0094038
Class 0:
Packet latency average = 1541.73
	minimum = 22
	maximum = 4852
Network latency average = 930.802
	minimum = 22
	maximum = 3939
Slowest packet = 25268
Flit latency average = 3083.83
	minimum = 5
	maximum = 5671
Slowest flit = 203932
Fragmentation average = 13.2426
	minimum = 0
	maximum = 336
Injected packet rate average = 0.0237422
	minimum = 0.007 (at node 176)
	maximum = 0.037 (at node 83)
Accepted packet rate average = 0.0152357
	minimum = 0.0105 (at node 36)
	maximum = 0.0205 (at node 118)
Injected flit rate average = 0.427257
	minimum = 0.12675 (at node 176)
	maximum = 0.6645 (at node 83)
Accepted flit rate average= 0.271868
	minimum = 0.182 (at node 36)
	maximum = 0.36675 (at node 118)
Injected packet length average = 17.9957
Accepted packet length average = 17.8442
Total in-flight flits = 403941 (309195 measured)
latency change    = 0.365229
throughput change = 0.00468562
Draining remaining packets ...
Class 0:
Remaining flits: 184264 184265 243188 243189 243190 243191 243192 243193 243194 243195 [...] (353438 flits)
Measured flits: 454770 454771 454772 454773 454774 454775 454776 454777 454778 454779 [...] (297333 flits)
Class 0:
Remaining flits: 270594 270595 270596 270597 270598 270599 270600 270601 270602 270603 [...] (303415 flits)
Measured flits: 454770 454771 454772 454773 454774 454775 454776 454777 454778 454779 [...] (274683 flits)
Class 0:
Remaining flits: 278406 278407 278408 278409 278410 278411 278412 278413 278414 278415 [...] (253530 flits)
Measured flits: 454770 454771 454772 454773 454774 454775 454776 454777 454778 454779 [...] (240569 flits)
Class 0:
Remaining flits: 307334 307335 307336 307337 307338 307339 307340 307341 307342 307343 [...] (205241 flits)
Measured flits: 454932 454933 454934 454935 454936 454937 454938 454939 454940 454941 [...] (200745 flits)
Class 0:
Remaining flits: 342072 342073 342074 342075 342076 342077 342078 342079 342080 342081 [...] (157723 flits)
Measured flits: 454968 454969 454970 454971 454972 454973 454974 454975 454976 454977 [...] (156400 flits)
Class 0:
Remaining flits: 363474 363475 363476 363477 363478 363479 363480 363481 363482 363483 [...] (110324 flits)
Measured flits: 454968 454969 454970 454971 454972 454973 454974 454975 454976 454977 [...] (109967 flits)
Class 0:
Remaining flits: 411354 411355 411356 411357 411358 411359 411360 411361 411362 411363 [...] (63265 flits)
Measured flits: 457092 457093 457094 457095 457096 457097 457098 457099 457100 457101 [...] (63205 flits)
Class 0:
Remaining flits: 485442 485443 485444 485445 485446 485447 485448 485449 485450 485451 [...] (17099 flits)
Measured flits: 485442 485443 485444 485445 485446 485447 485448 485449 485450 485451 [...] (17099 flits)
Class 0:
Remaining flits: 518166 518167 518168 518169 518170 518171 518172 518173 518174 518175 [...] (156 flits)
Measured flits: 518166 518167 518168 518169 518170 518171 518172 518173 518174 518175 [...] (156 flits)
Time taken is 16164 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7564.09 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12621 (1 samples)
Network latency average = 6923.83 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12584 (1 samples)
Flit latency average = 5639.48 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12567 (1 samples)
Fragmentation average = 57.7166 (1 samples)
	minimum = 0 (1 samples)
	maximum = 921 (1 samples)
Injected packet rate average = 0.0237422 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.037 (1 samples)
Accepted packet rate average = 0.0152357 (1 samples)
	minimum = 0.0105 (1 samples)
	maximum = 0.0205 (1 samples)
Injected flit rate average = 0.427257 (1 samples)
	minimum = 0.12675 (1 samples)
	maximum = 0.6645 (1 samples)
Accepted flit rate average = 0.271868 (1 samples)
	minimum = 0.182 (1 samples)
	maximum = 0.36675 (1 samples)
Injected packet size average = 17.9957 (1 samples)
Accepted packet size average = 17.8442 (1 samples)
Hops average = 5.15745 (1 samples)
Total run time 19.5191
