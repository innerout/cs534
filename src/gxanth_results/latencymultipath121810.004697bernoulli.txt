BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 54.6244
	minimum = 23
	maximum = 176
Network latency average = 53.7045
	minimum = 23
	maximum = 176
Slowest packet = 575
Flit latency average = 31.2362
	minimum = 6
	maximum = 159
Slowest flit = 10367
Fragmentation average = 10.1471
	minimum = 0
	maximum = 60
Injected packet rate average = 0.00458333
	minimum = 0 (at node 33)
	maximum = 0.013 (at node 28)
Accepted packet rate average = 0.00435417
	minimum = 0.001 (at node 4)
	maximum = 0.011 (at node 140)
Injected flit rate average = 0.0817552
	minimum = 0 (at node 33)
	maximum = 0.234 (at node 28)
Accepted flit rate average= 0.0793333
	minimum = 0.018 (at node 4)
	maximum = 0.198 (at node 140)
Injected packet length average = 17.8375
Accepted packet length average = 18.2201
Total in-flight flits = 608 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 54.2039
	minimum = 23
	maximum = 226
Network latency average = 53.4477
	minimum = 23
	maximum = 211
Slowest packet = 923
Flit latency average = 31.126
	minimum = 6
	maximum = 194
Slowest flit = 16627
Fragmentation average = 9.85256
	minimum = 0
	maximum = 60
Injected packet rate average = 0.00447656
	minimum = 0.001 (at node 33)
	maximum = 0.0085 (at node 127)
Accepted packet rate average = 0.00438021
	minimum = 0.0005 (at node 41)
	maximum = 0.008 (at node 48)
Injected flit rate average = 0.080276
	minimum = 0.018 (at node 33)
	maximum = 0.153 (at node 127)
Accepted flit rate average= 0.0794115
	minimum = 0.009 (at node 41)
	maximum = 0.144 (at node 48)
Injected packet length average = 17.9325
Accepted packet length average = 18.1296
Total in-flight flits = 448 (0 measured)
latency change    = 0.00775734
throughput change = 0.0009838
Class 0:
Packet latency average = 52.3624
	minimum = 23
	maximum = 131
Network latency average = 51.5547
	minimum = 23
	maximum = 122
Slowest packet = 2190
Flit latency average = 29.7551
	minimum = 6
	maximum = 105
Slowest flit = 39432
Fragmentation average = 8.98011
	minimum = 0
	maximum = 40
Injected packet rate average = 0.00477604
	minimum = 0 (at node 51)
	maximum = 0.013 (at node 23)
Accepted packet rate average = 0.00471354
	minimum = 0 (at node 17)
	maximum = 0.011 (at node 168)
Injected flit rate average = 0.0858698
	minimum = 0 (at node 51)
	maximum = 0.234 (at node 23)
Accepted flit rate average= 0.0849063
	minimum = 0 (at node 17)
	maximum = 0.198 (at node 173)
Injected packet length average = 17.9793
Accepted packet length average = 18.0133
Total in-flight flits = 652 (0 measured)
latency change    = 0.0351682
throughput change = 0.064716
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 54.7195
	minimum = 23
	maximum = 144
Network latency average = 53.9095
	minimum = 23
	maximum = 129
Slowest packet = 2739
Flit latency average = 31.3677
	minimum = 6
	maximum = 127
Slowest flit = 47069
Fragmentation average = 10.7183
	minimum = 0
	maximum = 44
Injected packet rate average = 0.00483854
	minimum = 0 (at node 58)
	maximum = 0.011 (at node 189)
Accepted packet rate average = 0.00485937
	minimum = 0 (at node 18)
	maximum = 0.013 (at node 22)
Injected flit rate average = 0.087026
	minimum = 0 (at node 58)
	maximum = 0.198 (at node 189)
Accepted flit rate average= 0.0871667
	minimum = 0 (at node 18)
	maximum = 0.234 (at node 22)
Injected packet length average = 17.986
Accepted packet length average = 17.9378
Total in-flight flits = 638 (638 measured)
latency change    = 0.0430747
throughput change = 0.0259321
Class 0:
Packet latency average = 54.0868
	minimum = 23
	maximum = 158
Network latency average = 53.2758
	minimum = 23
	maximum = 143
Slowest packet = 2739
Flit latency average = 30.9728
	minimum = 6
	maximum = 127
Slowest flit = 47069
Fragmentation average = 10.1605
	minimum = 0
	maximum = 51
Injected packet rate average = 0.00467187
	minimum = 0.0005 (at node 183)
	maximum = 0.0095 (at node 131)
Accepted packet rate average = 0.0046875
	minimum = 0.001 (at node 24)
	maximum = 0.0085 (at node 22)
Injected flit rate average = 0.0840313
	minimum = 0.009 (at node 183)
	maximum = 0.1705 (at node 131)
Accepted flit rate average= 0.0843281
	minimum = 0.018 (at node 24)
	maximum = 0.153 (at node 22)
Injected packet length average = 17.9866
Accepted packet length average = 17.99
Total in-flight flits = 562 (562 measured)
latency change    = 0.0116969
throughput change = 0.0336607
Class 0:
Packet latency average = 53.9396
	minimum = 23
	maximum = 178
Network latency average = 53.1793
	minimum = 23
	maximum = 178
Slowest packet = 4461
Flit latency average = 30.9132
	minimum = 6
	maximum = 161
Slowest flit = 80315
Fragmentation average = 10.0672
	minimum = 0
	maximum = 51
Injected packet rate average = 0.00464583
	minimum = 0.00133333 (at node 59)
	maximum = 0.00833333 (at node 138)
Accepted packet rate average = 0.00465625
	minimum = 0.00133333 (at node 153)
	maximum = 0.00733333 (at node 97)
Injected flit rate average = 0.0837483
	minimum = 0.024 (at node 59)
	maximum = 0.15 (at node 138)
Accepted flit rate average= 0.0838854
	minimum = 0.024 (at node 153)
	maximum = 0.132 (at node 97)
Injected packet length average = 18.0265
Accepted packet length average = 18.0157
Total in-flight flits = 502 (502 measured)
latency change    = 0.00272888
throughput change = 0.00527754
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6189 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 54.127 (1 samples)
	minimum = 23 (1 samples)
	maximum = 178 (1 samples)
Network latency average = 53.3511 (1 samples)
	minimum = 23 (1 samples)
	maximum = 178 (1 samples)
Flit latency average = 31.1177 (1 samples)
	minimum = 6 (1 samples)
	maximum = 161 (1 samples)
Fragmentation average = 10.1688 (1 samples)
	minimum = 0 (1 samples)
	maximum = 51 (1 samples)
Injected packet rate average = 0.00464583 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.00833333 (1 samples)
Accepted packet rate average = 0.00465625 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.00733333 (1 samples)
Injected flit rate average = 0.0837483 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.15 (1 samples)
Accepted flit rate average = 0.0838854 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.132 (1 samples)
Injected packet size average = 18.0265 (1 samples)
Accepted packet size average = 18.0157 (1 samples)
Hops average = 5.09488 (1 samples)
Total run time 1.58063
