BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 228.166
	minimum = 24
	maximum = 889
Network latency average = 162.996
	minimum = 22
	maximum = 717
Slowest packet = 21
Flit latency average = 139.956
	minimum = 5
	maximum = 728
Slowest flit = 16573
Fragmentation average = 15.6822
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0123229
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22688
	minimum = 0.108 (at node 41)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.4112
Total in-flight flits = 18449 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 354.558
	minimum = 22
	maximum = 1839
Network latency average = 273.277
	minimum = 22
	maximum = 1287
Slowest packet = 21
Flit latency average = 249.266
	minimum = 5
	maximum = 1270
Slowest flit = 38663
Fragmentation average = 17.0856
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0132604
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241073
	minimum = 0.148 (at node 83)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.1799
Total in-flight flits = 27020 (0 measured)
latency change    = 0.356478
throughput change = 0.0588731
Class 0:
Packet latency average = 622.755
	minimum = 22
	maximum = 2135
Network latency average = 521.076
	minimum = 22
	maximum = 1828
Slowest packet = 3034
Flit latency average = 496.442
	minimum = 5
	maximum = 1902
Slowest flit = 67297
Fragmentation average = 17.6736
	minimum = 0
	maximum = 83
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145208
	minimum = 0.004 (at node 118)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261328
	minimum = 0.072 (at node 118)
	maximum = 0.433 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9968
Total in-flight flits = 39215 (0 measured)
latency change    = 0.430662
throughput change = 0.0775087
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.18
	minimum = 25
	maximum = 1442
Network latency average = 314.33
	minimum = 22
	maximum = 988
Slowest packet = 10139
Flit latency average = 644.804
	minimum = 5
	maximum = 2293
Slowest flit = 81000
Fragmentation average = 14.258
	minimum = 0
	maximum = 83
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0148542
	minimum = 0.007 (at node 57)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267365
	minimum = 0.126 (at node 141)
	maximum = 0.468 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 17.9993
Total in-flight flits = 49819 (41687 measured)
latency change    = 0.525685
throughput change = 0.0225776
Class 0:
Packet latency average = 670.787
	minimum = 22
	maximum = 2173
Network latency average = 573.148
	minimum = 22
	maximum = 1937
Slowest packet = 10139
Flit latency average = 725.444
	minimum = 5
	maximum = 2403
Slowest flit = 152729
Fragmentation average = 16.631
	minimum = 0
	maximum = 84
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0148359
	minimum = 0.0095 (at node 5)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.266992
	minimum = 0.171 (at node 5)
	maximum = 0.413 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 17.9963
Total in-flight flits = 56336 (55285 measured)
latency change    = 0.39149
throughput change = 0.00139478
Class 0:
Packet latency average = 834.625
	minimum = 22
	maximum = 3447
Network latency average = 737.197
	minimum = 22
	maximum = 2906
Slowest packet = 10139
Flit latency average = 801.277
	minimum = 5
	maximum = 3036
Slowest flit = 178928
Fragmentation average = 16.9403
	minimum = 0
	maximum = 95
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0148594
	minimum = 0.0103333 (at node 135)
	maximum = 0.021 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267509
	minimum = 0.188667 (at node 135)
	maximum = 0.382667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.0027
Total in-flight flits = 66264 (66255 measured)
latency change    = 0.196302
throughput change = 0.00193075
Draining remaining packets ...
Class 0:
Remaining flits: 217393 217394 217395 217396 217397 217398 217399 217400 217401 217402 [...] (24436 flits)
Measured flits: 217393 217394 217395 217396 217397 217398 217399 217400 217401 217402 [...] (24436 flits)
Class 0:
Remaining flits: 285459 285460 285461 286488 286489 286490 286491 286492 286493 286494 [...] (4476 flits)
Measured flits: 285459 285460 285461 286488 286489 286490 286491 286492 286493 286494 [...] (4476 flits)
Class 0:
Remaining flits: 355873 355874 355875 355876 355877 356233 356234 356235 356236 356237 [...] (409 flits)
Measured flits: 355873 355874 355875 355876 355877 356233 356234 356235 356236 356237 [...] (409 flits)
Time taken is 9264 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1189.05 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3832 (1 samples)
Network latency average = 1085.01 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3631 (1 samples)
Flit latency average = 1058.61 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3614 (1 samples)
Fragmentation average = 16.5098 (1 samples)
	minimum = 0 (1 samples)
	maximum = 95 (1 samples)
Injected packet rate average = 0.0174705 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0148594 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.314714 (1 samples)
	minimum = 0.049 (1 samples)
	maximum = 0.725333 (1 samples)
Accepted flit rate average = 0.267509 (1 samples)
	minimum = 0.188667 (1 samples)
	maximum = 0.382667 (1 samples)
Injected packet size average = 18.014 (1 samples)
Accepted packet size average = 18.0027 (1 samples)
Hops average = 5.07761 (1 samples)
Total run time 4.78888
