BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 292.199
	minimum = 23
	maximum = 963
Network latency average = 285.752
	minimum = 23
	maximum = 951
Slowest packet = 186
Flit latency average = 218.81
	minimum = 6
	maximum = 945
Slowest flit = 2265
Fragmentation average = 153.014
	minimum = 0
	maximum = 815
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0103125
	minimum = 0.003 (at node 106)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.213286
	minimum = 0.078 (at node 106)
	maximum = 0.34 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 20.6823
Total in-flight flits = 51659 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 525.203
	minimum = 23
	maximum = 1778
Network latency average = 517.964
	minimum = 23
	maximum = 1746
Slowest packet = 970
Flit latency average = 441.647
	minimum = 6
	maximum = 1871
Slowest flit = 10224
Fragmentation average = 187.383
	minimum = 0
	maximum = 1679
Injected packet rate average = 0.026526
	minimum = 0.0175 (at node 41)
	maximum = 0.0335 (at node 187)
Accepted packet rate average = 0.0114375
	minimum = 0.0065 (at node 40)
	maximum = 0.017 (at node 51)
Injected flit rate average = 0.475794
	minimum = 0.312 (at node 72)
	maximum = 0.603 (at node 187)
Accepted flit rate average= 0.219875
	minimum = 0.127 (at node 40)
	maximum = 0.327 (at node 51)
Injected packet length average = 17.9369
Accepted packet length average = 19.224
Total in-flight flits = 98952 (0 measured)
latency change    = 0.443645
throughput change = 0.0299649
Class 0:
Packet latency average = 1196.33
	minimum = 24
	maximum = 2822
Network latency average = 1186.15
	minimum = 24
	maximum = 2822
Slowest packet = 761
Flit latency average = 1123.5
	minimum = 6
	maximum = 2840
Slowest flit = 12050
Fragmentation average = 221.632
	minimum = 1
	maximum = 1935
Injected packet rate average = 0.0252865
	minimum = 0.011 (at node 116)
	maximum = 0.043 (at node 102)
Accepted packet rate average = 0.012599
	minimum = 0.004 (at node 153)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.453964
	minimum = 0.197 (at node 116)
	maximum = 0.782 (at node 102)
Accepted flit rate average= 0.226432
	minimum = 0.089 (at node 32)
	maximum = 0.451 (at node 16)
Injected packet length average = 17.9528
Accepted packet length average = 17.9723
Total in-flight flits = 142831 (0 measured)
latency change    = 0.560987
throughput change = 0.0289592
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 206.809
	minimum = 25
	maximum = 1755
Network latency average = 124.299
	minimum = 25
	maximum = 835
Slowest packet = 15085
Flit latency average = 1659.07
	minimum = 6
	maximum = 3708
Slowest flit = 25740
Fragmentation average = 18.8058
	minimum = 1
	maximum = 207
Injected packet rate average = 0.0240521
	minimum = 0.008 (at node 80)
	maximum = 0.042 (at node 134)
Accepted packet rate average = 0.0122396
	minimum = 0.003 (at node 17)
	maximum = 0.022 (at node 56)
Injected flit rate average = 0.431943
	minimum = 0.14 (at node 188)
	maximum = 0.771 (at node 134)
Accepted flit rate average= 0.219432
	minimum = 0.05 (at node 17)
	maximum = 0.38 (at node 56)
Injected packet length average = 17.9586
Accepted packet length average = 17.9281
Total in-flight flits = 183860 (77973 measured)
latency change    = 4.78468
throughput change = 0.0319005
Class 0:
Packet latency average = 493.774
	minimum = 25
	maximum = 2900
Network latency average = 349.174
	minimum = 25
	maximum = 1951
Slowest packet = 15085
Flit latency average = 1896.82
	minimum = 6
	maximum = 4617
Slowest flit = 28943
Fragmentation average = 42.4033
	minimum = 1
	maximum = 411
Injected packet rate average = 0.0240026
	minimum = 0.006 (at node 188)
	maximum = 0.0355 (at node 139)
Accepted packet rate average = 0.0120833
	minimum = 0.0045 (at node 17)
	maximum = 0.018 (at node 151)
Injected flit rate average = 0.431948
	minimum = 0.101 (at node 188)
	maximum = 0.6365 (at node 139)
Accepted flit rate average= 0.216003
	minimum = 0.089 (at node 17)
	maximum = 0.317 (at node 187)
Injected packet length average = 17.9959
Accepted packet length average = 17.8761
Total in-flight flits = 225918 (153365 measured)
latency change    = 0.581166
throughput change = 0.015878
Class 0:
Packet latency average = 1174.78
	minimum = 23
	maximum = 3832
Network latency average = 1003.59
	minimum = 23
	maximum = 2960
Slowest packet = 15085
Flit latency average = 2133.4
	minimum = 6
	maximum = 5496
Slowest flit = 18431
Fragmentation average = 88.6697
	minimum = 0
	maximum = 1046
Injected packet rate average = 0.0237101
	minimum = 0.007 (at node 96)
	maximum = 0.0336667 (at node 145)
Accepted packet rate average = 0.0119184
	minimum = 0.007 (at node 36)
	maximum = 0.0183333 (at node 181)
Injected flit rate average = 0.426696
	minimum = 0.126667 (at node 96)
	maximum = 0.610333 (at node 145)
Accepted flit rate average= 0.21275
	minimum = 0.128333 (at node 17)
	maximum = 0.311 (at node 181)
Injected packet length average = 17.9964
Accepted packet length average = 17.8505
Total in-flight flits = 266221 (218449 measured)
latency change    = 0.579689
throughput change = 0.0152884
Draining remaining packets ...
Class 0:
Remaining flits: 37592 37593 37594 37595 37596 37597 37598 37599 37600 37601 [...] (229518 flits)
Measured flits: 270738 270739 270740 270741 270742 270743 270744 270745 270746 270747 [...] (197625 flits)
Class 0:
Remaining flits: 39048 39049 39050 39051 39052 39053 39054 39055 39056 39057 [...] (193409 flits)
Measured flits: 270748 270749 270750 270751 270752 270753 270754 270755 270774 270775 [...] (172272 flits)
Class 0:
Remaining flits: 69030 69031 69032 69033 69034 69035 69036 69037 69038 69039 [...] (158285 flits)
Measured flits: 270774 270775 270776 270777 270778 270779 270780 270781 270782 270783 [...] (145028 flits)
Class 0:
Remaining flits: 69030 69031 69032 69033 69034 69035 69036 69037 69038 69039 [...] (124646 flits)
Measured flits: 270774 270775 270776 270777 270778 270779 270780 270781 270782 270783 [...] (116492 flits)
Class 0:
Remaining flits: 84546 84547 84548 84549 84550 84551 84552 84553 84554 84555 [...] (90841 flits)
Measured flits: 270828 270829 270830 270831 270832 270833 270834 270835 270836 270837 [...] (86035 flits)
Class 0:
Remaining flits: 84546 84547 84548 84549 84550 84551 84552 84553 84554 84555 [...] (57648 flits)
Measured flits: 270843 270844 270845 271008 271009 271010 271011 271012 271013 271014 [...] (54950 flits)
Class 0:
Remaining flits: 136836 136837 136838 136839 136840 136841 136842 136843 136844 136845 [...] (26655 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (25460 flits)
Class 0:
Remaining flits: 175283 199134 199135 199136 199137 199138 199139 199140 199141 199142 [...] (4622 flits)
Measured flits: 272790 272791 272792 272793 272794 272795 272796 272797 272798 272799 [...] (4495 flits)
Time taken is 14773 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5332.41 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11327 (1 samples)
Network latency average = 5185.94 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11236 (1 samples)
Flit latency average = 4535.58 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12086 (1 samples)
Fragmentation average = 166.72 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2820 (1 samples)
Injected packet rate average = 0.0237101 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0336667 (1 samples)
Accepted packet rate average = 0.0119184 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.426696 (1 samples)
	minimum = 0.126667 (1 samples)
	maximum = 0.610333 (1 samples)
Accepted flit rate average = 0.21275 (1 samples)
	minimum = 0.128333 (1 samples)
	maximum = 0.311 (1 samples)
Injected packet size average = 17.9964 (1 samples)
Accepted packet size average = 17.8505 (1 samples)
Hops average = 5.15816 (1 samples)
Total run time 15.4828
