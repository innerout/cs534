BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.368
	minimum = 23
	maximum = 962
Network latency average = 315.415
	minimum = 23
	maximum = 918
Slowest packet = 451
Flit latency average = 245.75
	minimum = 6
	maximum = 947
Slowest flit = 4293
Fragmentation average = 193.481
	minimum = 0
	maximum = 779
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0114792
	minimum = 0.004 (at node 61)
	maximum = 0.021 (at node 91)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.248453
	minimum = 0.129 (at node 162)
	maximum = 0.396 (at node 119)
Injected packet length average = 17.8388
Accepted packet length average = 21.6438
Total in-flight flits = 121281 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.067
	minimum = 23
	maximum = 1946
Network latency average = 573.581
	minimum = 23
	maximum = 1900
Slowest packet = 487
Flit latency average = 488.97
	minimum = 6
	maximum = 1938
Slowest flit = 6448
Fragmentation average = 261.076
	minimum = 0
	maximum = 1704
Injected packet rate average = 0.0481458
	minimum = 0.0355 (at node 112)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0129141
	minimum = 0.0075 (at node 96)
	maximum = 0.0185 (at node 67)
Injected flit rate average = 0.862406
	minimum = 0.6345 (at node 144)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.259281
	minimum = 0.1635 (at node 79)
	maximum = 0.358 (at node 131)
Injected packet length average = 17.9124
Accepted packet length average = 20.0774
Total in-flight flits = 233220 (0 measured)
latency change    = 0.443549
throughput change = 0.0417621
Class 0:
Packet latency average = 1513.88
	minimum = 32
	maximum = 2825
Network latency average = 1434.33
	minimum = 28
	maximum = 2825
Slowest packet = 780
Flit latency average = 1389.31
	minimum = 6
	maximum = 2843
Slowest flit = 16425
Fragmentation average = 379.114
	minimum = 1
	maximum = 2495
Injected packet rate average = 0.0422135
	minimum = 0.006 (at node 56)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0137396
	minimum = 0.006 (at node 32)
	maximum = 0.023 (at node 34)
Injected flit rate average = 0.760484
	minimum = 0.121 (at node 56)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.249995
	minimum = 0.118 (at node 190)
	maximum = 0.401 (at node 181)
Injected packet length average = 18.0152
Accepted packet length average = 18.1952
Total in-flight flits = 331111 (0 measured)
latency change    = 0.592394
throughput change = 0.0371466
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 715.769
	minimum = 32
	maximum = 2175
Network latency average = 89.3077
	minimum = 28
	maximum = 946
Slowest packet = 26623
Flit latency average = 2214.9
	minimum = 6
	maximum = 3816
Slowest flit = 25317
Fragmentation average = 12.4142
	minimum = 4
	maximum = 125
Injected packet rate average = 0.0426979
	minimum = 0.007 (at node 64)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0128958
	minimum = 0.006 (at node 171)
	maximum = 0.026 (at node 50)
Injected flit rate average = 0.768698
	minimum = 0.138 (at node 64)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.231464
	minimum = 0.11 (at node 102)
	maximum = 0.447 (at node 50)
Injected packet length average = 18.0032
Accepted packet length average = 17.9487
Total in-flight flits = 434234 (144439 measured)
latency change    = 1.11504
throughput change = 0.0800612
Class 0:
Packet latency average = 989.56
	minimum = 31
	maximum = 3233
Network latency average = 341.817
	minimum = 28
	maximum = 1866
Slowest packet = 26623
Flit latency average = 2579.4
	minimum = 6
	maximum = 4798
Slowest flit = 29455
Fragmentation average = 22.7822
	minimum = 3
	maximum = 591
Injected packet rate average = 0.0386432
	minimum = 0.0105 (at node 24)
	maximum = 0.0555 (at node 33)
Accepted packet rate average = 0.0129896
	minimum = 0.007 (at node 35)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.695753
	minimum = 0.192 (at node 68)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.232872
	minimum = 0.122 (at node 35)
	maximum = 0.3605 (at node 177)
Injected packet length average = 18.0045
Accepted packet length average = 17.9276
Total in-flight flits = 508880 (259259 measured)
latency change    = 0.276679
throughput change = 0.0060499
Class 0:
Packet latency average = 1501.18
	minimum = 31
	maximum = 4426
Network latency average = 681.881
	minimum = 28
	maximum = 2906
Slowest packet = 26623
Flit latency average = 2957.96
	minimum = 6
	maximum = 5861
Slowest flit = 18435
Fragmentation average = 52.5973
	minimum = 3
	maximum = 1189
Injected packet rate average = 0.0316771
	minimum = 0.0106667 (at node 12)
	maximum = 0.046 (at node 7)
Accepted packet rate average = 0.0127726
	minimum = 0.00833333 (at node 100)
	maximum = 0.018 (at node 107)
Injected flit rate average = 0.570425
	minimum = 0.192667 (at node 12)
	maximum = 0.829333 (at node 7)
Accepted flit rate average= 0.229161
	minimum = 0.154667 (at node 62)
	maximum = 0.308667 (at node 95)
Injected packet length average = 18.0075
Accepted packet length average = 17.9417
Total in-flight flits = 527614 (315635 measured)
latency change    = 0.340814
throughput change = 0.0161935
Class 0:
Packet latency average = 2288.35
	minimum = 31
	maximum = 5449
Network latency average = 1192.68
	minimum = 28
	maximum = 3861
Slowest packet = 26623
Flit latency average = 3299.8
	minimum = 6
	maximum = 6931
Slowest flit = 7071
Fragmentation average = 98.3151
	minimum = 3
	maximum = 1543
Injected packet rate average = 0.0276953
	minimum = 0.0115 (at node 4)
	maximum = 0.04025 (at node 7)
Accepted packet rate average = 0.0127018
	minimum = 0.0095 (at node 5)
	maximum = 0.01775 (at node 175)
Injected flit rate average = 0.498868
	minimum = 0.20625 (at node 4)
	maximum = 0.72525 (at node 7)
Accepted flit rate average= 0.227805
	minimum = 0.1625 (at node 64)
	maximum = 0.31475 (at node 177)
Injected packet length average = 18.0127
Accepted packet length average = 17.9348
Total in-flight flits = 539197 (362299 measured)
latency change    = 0.343987
throughput change = 0.00595585
Draining remaining packets ...
Class 0:
Remaining flits: 18144 18145 18146 18147 18148 18149 18150 18151 18152 18153 [...] (498653 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (354130 flits)
Class 0:
Remaining flits: 18144 18145 18146 18147 18148 18149 18150 18151 18152 18153 [...] (458210 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (343207 flits)
Class 0:
Remaining flits: 18144 18145 18146 18147 18148 18149 18150 18151 18152 18153 [...] (418591 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (328736 flits)
Class 0:
Remaining flits: 18161 26346 26347 26348 26349 26350 26351 31464 31465 31466 [...] (379701 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (309394 flits)
Class 0:
Remaining flits: 18161 26346 26347 26348 26349 26350 26351 33698 33699 33700 [...] (341172 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (284284 flits)
Class 0:
Remaining flits: 18161 26346 26347 26348 26349 26350 26351 35280 35281 35282 [...] (304569 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (258520 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (269736 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (231639 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (235778 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (203900 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (201876 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (175831 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (169288 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (148458 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (136855 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (120400 flits)
Class 0:
Remaining flits: 35280 35281 35282 35283 35284 35285 35286 35287 35288 35289 [...] (103861 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (91486 flits)
Class 0:
Remaining flits: 39168 39169 39170 39171 39172 39173 39174 39175 39176 39177 [...] (71245 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (62884 flits)
Class 0:
Remaining flits: 103158 103159 103160 103161 103162 103163 103164 103165 103166 103167 [...] (41203 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (36237 flits)
Class 0:
Remaining flits: 175698 175699 175700 175701 175702 175703 175704 175705 175706 175707 [...] (16935 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (14492 flits)
Class 0:
Remaining flits: 282769 282770 282771 282772 282773 282774 282775 282776 282777 282778 [...] (2261 flits)
Measured flits: 478674 478675 478676 478677 478678 478679 478680 478681 478682 478683 [...] (1905 flits)
Time taken is 23944 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11360.2 (1 samples)
	minimum = 31 (1 samples)
	maximum = 21548 (1 samples)
Network latency average = 10705.6 (1 samples)
	minimum = 28 (1 samples)
	maximum = 20865 (1 samples)
Flit latency average = 8716.92 (1 samples)
	minimum = 6 (1 samples)
	maximum = 21863 (1 samples)
Fragmentation average = 224.344 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6261 (1 samples)
Injected packet rate average = 0.0276953 (1 samples)
	minimum = 0.0115 (1 samples)
	maximum = 0.04025 (1 samples)
Accepted packet rate average = 0.0127018 (1 samples)
	minimum = 0.0095 (1 samples)
	maximum = 0.01775 (1 samples)
Injected flit rate average = 0.498868 (1 samples)
	minimum = 0.20625 (1 samples)
	maximum = 0.72525 (1 samples)
Accepted flit rate average = 0.227805 (1 samples)
	minimum = 0.1625 (1 samples)
	maximum = 0.31475 (1 samples)
Injected packet size average = 18.0127 (1 samples)
Accepted packet size average = 17.9348 (1 samples)
Hops average = 5.18567 (1 samples)
Total run time 40.6479
