BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.548
	minimum = 22
	maximum = 982
Network latency average = 235.255
	minimum = 22
	maximum = 849
Slowest packet = 46
Flit latency average = 205.837
	minimum = 5
	maximum = 877
Slowest flit = 9662
Fragmentation average = 47.4145
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139479
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26375
	minimum = 0.118 (at node 150)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8285
Accepted packet length average = 18.9096
Total in-flight flits = 52194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 615.81
	minimum = 22
	maximum = 1951
Network latency average = 446.831
	minimum = 22
	maximum = 1652
Slowest packet = 46
Flit latency average = 415.099
	minimum = 5
	maximum = 1740
Slowest flit = 22884
Fragmentation average = 68.4541
	minimum = 0
	maximum = 401
Injected packet rate average = 0.0313932
	minimum = 0.002 (at node 160)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0147969
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.562247
	minimum = 0.036 (at node 160)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.275737
	minimum = 0.1575 (at node 116)
	maximum = 0.398 (at node 63)
Injected packet length average = 17.9098
Accepted packet length average = 18.6348
Total in-flight flits = 111107 (0 measured)
latency change    = 0.429129
throughput change = 0.0434725
Class 0:
Packet latency average = 1371.3
	minimum = 25
	maximum = 2853
Network latency average = 1104.38
	minimum = 22
	maximum = 2431
Slowest packet = 2780
Flit latency average = 1066.52
	minimum = 5
	maximum = 2417
Slowest flit = 54500
Fragmentation average = 106.562
	minimum = 0
	maximum = 443
Injected packet rate average = 0.0344115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.0162552
	minimum = 0.008 (at node 52)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.619802
	minimum = 0 (at node 8)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.291917
	minimum = 0.142 (at node 36)
	maximum = 0.487 (at node 92)
Injected packet length average = 18.0115
Accepted packet length average = 17.9583
Total in-flight flits = 174021 (0 measured)
latency change    = 0.55093
throughput change = 0.0554257
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 323.924
	minimum = 26
	maximum = 1525
Network latency average = 45.2695
	minimum = 22
	maximum = 792
Slowest packet = 18695
Flit latency average = 1543.12
	minimum = 5
	maximum = 3071
Slowest flit = 92241
Fragmentation average = 6.50334
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0331667
	minimum = 0.001 (at node 7)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0159063
	minimum = 0.003 (at node 32)
	maximum = 0.027 (at node 33)
Injected flit rate average = 0.596375
	minimum = 0.001 (at node 7)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.289292
	minimum = 0.095 (at node 32)
	maximum = 0.526 (at node 123)
Injected packet length average = 17.9812
Accepted packet length average = 18.1873
Total in-flight flits = 233155 (106551 measured)
latency change    = 3.2334
throughput change = 0.00907389
Class 0:
Packet latency average = 516.797
	minimum = 26
	maximum = 3039
Network latency average = 193.613
	minimum = 22
	maximum = 1980
Slowest packet = 18695
Flit latency average = 1804.61
	minimum = 5
	maximum = 3756
Slowest flit = 115163
Fragmentation average = 9.50223
	minimum = 0
	maximum = 173
Injected packet rate average = 0.0320677
	minimum = 0.0065 (at node 136)
	maximum = 0.0555 (at node 11)
Accepted packet rate average = 0.015888
	minimum = 0.0095 (at node 71)
	maximum = 0.0275 (at node 123)
Injected flit rate average = 0.577284
	minimum = 0.117 (at node 136)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.286393
	minimum = 0.1685 (at node 150)
	maximum = 0.501 (at node 123)
Injected packet length average = 18.002
Accepted packet length average = 18.0257
Total in-flight flits = 285950 (205570 measured)
latency change    = 0.373208
throughput change = 0.0101205
Class 0:
Packet latency average = 1172.14
	minimum = 24
	maximum = 4076
Network latency average = 862.715
	minimum = 22
	maximum = 2960
Slowest packet = 18695
Flit latency average = 2066.06
	minimum = 5
	maximum = 4585
Slowest flit = 126791
Fragmentation average = 27.0655
	minimum = 0
	maximum = 505
Injected packet rate average = 0.0302361
	minimum = 0.00866667 (at node 136)
	maximum = 0.0536667 (at node 75)
Accepted packet rate average = 0.0156927
	minimum = 0.0103333 (at node 71)
	maximum = 0.023 (at node 123)
Injected flit rate average = 0.544292
	minimum = 0.151667 (at node 136)
	maximum = 0.962667 (at node 75)
Accepted flit rate average= 0.282377
	minimum = 0.19 (at node 71)
	maximum = 0.414667 (at node 123)
Injected packet length average = 18.0014
Accepted packet length average = 17.9941
Total in-flight flits = 325238 (281671 measured)
latency change    = 0.559099
throughput change = 0.0142239
Class 0:
Packet latency average = 2052.37
	minimum = 24
	maximum = 5385
Network latency average = 1745.42
	minimum = 22
	maximum = 3964
Slowest packet = 18695
Flit latency average = 2316.2
	minimum = 5
	maximum = 5357
Slowest flit = 163709
Fragmentation average = 47.2468
	minimum = 0
	maximum = 546
Injected packet rate average = 0.0284935
	minimum = 0.01 (at node 120)
	maximum = 0.046 (at node 161)
Accepted packet rate average = 0.0156224
	minimum = 0.01175 (at node 92)
	maximum = 0.021 (at node 70)
Injected flit rate average = 0.512884
	minimum = 0.18 (at node 120)
	maximum = 0.829 (at node 161)
Accepted flit rate average= 0.280257
	minimum = 0.209 (at node 92)
	maximum = 0.3795 (at node 70)
Injected packet length average = 18
Accepted packet length average = 17.9394
Total in-flight flits = 353146 (329505 measured)
latency change    = 0.428885
throughput change = 0.0075653
Class 0:
Packet latency average = 2694.43
	minimum = 24
	maximum = 6495
Network latency average = 2363.83
	minimum = 22
	maximum = 4987
Slowest packet = 18695
Flit latency average = 2574.9
	minimum = 5
	maximum = 6341
Slowest flit = 175084
Fragmentation average = 56.6936
	minimum = 0
	maximum = 546
Injected packet rate average = 0.026876
	minimum = 0.012 (at node 120)
	maximum = 0.0438 (at node 21)
Accepted packet rate average = 0.01555
	minimum = 0.0118 (at node 144)
	maximum = 0.0214 (at node 70)
Injected flit rate average = 0.48397
	minimum = 0.2146 (at node 120)
	maximum = 0.7884 (at node 21)
Accepted flit rate average= 0.27858
	minimum = 0.2132 (at node 144)
	maximum = 0.3862 (at node 70)
Injected packet length average = 18.0075
Accepted packet length average = 17.9151
Total in-flight flits = 371992 (359806 measured)
latency change    = 0.238295
throughput change = 0.00601731
Class 0:
Packet latency average = 3215.7
	minimum = 24
	maximum = 7667
Network latency average = 2844.89
	minimum = 22
	maximum = 5929
Slowest packet = 18695
Flit latency average = 2831.13
	minimum = 5
	maximum = 7280
Slowest flit = 183279
Fragmentation average = 60.6277
	minimum = 0
	maximum = 593
Injected packet rate average = 0.0254766
	minimum = 0.0121667 (at node 152)
	maximum = 0.0423333 (at node 11)
Accepted packet rate average = 0.0154696
	minimum = 0.0121667 (at node 144)
	maximum = 0.0201667 (at node 103)
Injected flit rate average = 0.458694
	minimum = 0.221667 (at node 152)
	maximum = 0.7615 (at node 11)
Accepted flit rate average= 0.277504
	minimum = 0.215 (at node 152)
	maximum = 0.362667 (at node 103)
Injected packet length average = 18.0045
Accepted packet length average = 17.9387
Total in-flight flits = 383824 (377829 measured)
latency change    = 0.162099
throughput change = 0.00387694
Class 0:
Packet latency average = 3685.77
	minimum = 24
	maximum = 8263
Network latency average = 3263.75
	minimum = 22
	maximum = 6918
Slowest packet = 18695
Flit latency average = 3086.51
	minimum = 5
	maximum = 7942
Slowest flit = 223597
Fragmentation average = 61.7975
	minimum = 0
	maximum = 616
Injected packet rate average = 0.0244635
	minimum = 0.0108571 (at node 152)
	maximum = 0.041 (at node 11)
Accepted packet rate average = 0.0154315
	minimum = 0.0122857 (at node 104)
	maximum = 0.02 (at node 103)
Injected flit rate average = 0.440403
	minimum = 0.197714 (at node 152)
	maximum = 0.739143 (at node 11)
Accepted flit rate average= 0.276813
	minimum = 0.215286 (at node 104)
	maximum = 0.360571 (at node 103)
Injected packet length average = 18.0024
Accepted packet length average = 17.9381
Total in-flight flits = 394995 (392273 measured)
latency change    = 0.127537
throughput change = 0.00249931
Draining all recorded packets ...
Class 0:
Remaining flits: 257094 257095 257096 257097 257098 257099 257100 257101 257102 257103 [...] (403038 flits)
Measured flits: 336582 336583 336584 336585 336586 336587 336588 336589 336590 336591 [...] (392939 flits)
Class 0:
Remaining flits: 257094 257095 257096 257097 257098 257099 257100 257101 257102 257103 [...] (407166 flits)
Measured flits: 336582 336583 336584 336585 336586 336587 336588 336589 336590 336591 [...] (387207 flits)
Class 0:
Remaining flits: 309330 309331 309332 309333 309334 309335 309336 309337 309338 309339 [...] (411991 flits)
Measured flits: 337014 337015 337016 337017 337018 337019 337020 337021 337022 337023 [...] (376798 flits)
Class 0:
Remaining flits: 320310 320311 320312 320313 320314 320315 320316 320317 320318 320319 [...] (415159 flits)
Measured flits: 349542 349543 349544 349545 349546 349547 349548 349549 349550 349551 [...] (362329 flits)
Class 0:
Remaining flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (415913 flits)
Measured flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (345243 flits)
Class 0:
Remaining flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (414234 flits)
Measured flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (323131 flits)
Class 0:
Remaining flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (414464 flits)
Measured flits: 374562 374563 374564 374565 374566 374567 374568 374569 374570 374571 [...] (299238 flits)
Class 0:
Remaining flits: 441414 441415 441416 441417 441418 441419 441420 441421 441422 441423 [...] (415337 flits)
Measured flits: 441414 441415 441416 441417 441418 441419 441420 441421 441422 441423 [...] (273968 flits)
Class 0:
Remaining flits: 566082 566083 566084 566085 566086 566087 566088 566089 566090 566091 [...] (417620 flits)
Measured flits: 566082 566083 566084 566085 566086 566087 566088 566089 566090 566091 [...] (246940 flits)
Class 0:
Remaining flits: 566082 566083 566084 566085 566086 566087 566088 566089 566090 566091 [...] (418273 flits)
Measured flits: 566082 566083 566084 566085 566086 566087 566088 566089 566090 566091 [...] (219945 flits)
Class 0:
Remaining flits: 571338 571339 571340 571341 571342 571343 571344 571345 571346 571347 [...] (417082 flits)
Measured flits: 571338 571339 571340 571341 571342 571343 571344 571345 571346 571347 [...] (191668 flits)
Class 0:
Remaining flits: 578502 578503 578504 578505 578506 578507 578508 578509 578510 578511 [...] (416057 flits)
Measured flits: 578502 578503 578504 578505 578506 578507 578508 578509 578510 578511 [...] (162922 flits)
Class 0:
Remaining flits: 783612 783613 783614 783615 783616 783617 783618 783619 783620 783621 [...] (417927 flits)
Measured flits: 783612 783613 783614 783615 783616 783617 783618 783619 783620 783621 [...] (136839 flits)
Class 0:
Remaining flits: 783612 783613 783614 783615 783616 783617 783618 783619 783620 783621 [...] (420380 flits)
Measured flits: 783612 783613 783614 783615 783616 783617 783618 783619 783620 783621 [...] (114106 flits)
Class 0:
Remaining flits: 783624 783625 783626 783627 783628 783629 830610 830611 830612 830613 [...] (420480 flits)
Measured flits: 783624 783625 783626 783627 783628 783629 830610 830611 830612 830613 [...] (93775 flits)
Class 0:
Remaining flits: 853128 853129 853130 853131 853132 853133 853134 853135 853136 853137 [...] (418356 flits)
Measured flits: 853128 853129 853130 853131 853132 853133 853134 853135 853136 853137 [...] (75695 flits)
Class 0:
Remaining flits: 935982 935983 935984 935985 935986 935987 935988 935989 935990 935991 [...] (419695 flits)
Measured flits: 935982 935983 935984 935985 935986 935987 935988 935989 935990 935991 [...] (61741 flits)
Class 0:
Remaining flits: 935982 935983 935984 935985 935986 935987 935988 935989 935990 935991 [...] (422279 flits)
Measured flits: 935982 935983 935984 935985 935986 935987 935988 935989 935990 935991 [...] (47799 flits)
Class 0:
Remaining flits: 1015137 1015138 1015139 1015140 1015141 1015142 1015143 1015144 1015145 1051056 [...] (426121 flits)
Measured flits: 1070460 1070461 1070462 1070463 1070464 1070465 1070466 1070467 1070468 1070469 [...] (36601 flits)
Class 0:
Remaining flits: 1070460 1070461 1070462 1070463 1070464 1070465 1070466 1070467 1070468 1070469 [...] (424717 flits)
Measured flits: 1070460 1070461 1070462 1070463 1070464 1070465 1070466 1070467 1070468 1070469 [...] (26903 flits)
Class 0:
Remaining flits: 1169190 1169191 1169192 1169193 1169194 1169195 1169196 1169197 1169198 1169199 [...] (423781 flits)
Measured flits: 1169190 1169191 1169192 1169193 1169194 1169195 1169196 1169197 1169198 1169199 [...] (19312 flits)
Class 0:
Remaining flits: 1197342 1197343 1197344 1197345 1197346 1197347 1197348 1197349 1197350 1197351 [...] (422863 flits)
Measured flits: 1197342 1197343 1197344 1197345 1197346 1197347 1197348 1197349 1197350 1197351 [...] (14048 flits)
Class 0:
Remaining flits: 1303596 1303597 1303598 1303599 1303600 1303601 1303602 1303603 1303604 1303605 [...] (425280 flits)
Measured flits: 1303596 1303597 1303598 1303599 1303600 1303601 1303602 1303603 1303604 1303605 [...] (9916 flits)
Class 0:
Remaining flits: 1360638 1360639 1360640 1360641 1360642 1360643 1360644 1360645 1360646 1360647 [...] (424860 flits)
Measured flits: 1385478 1385479 1385480 1385481 1385482 1385483 1385484 1385485 1385486 1385487 [...] (7189 flits)
Class 0:
Remaining flits: 1360638 1360639 1360640 1360641 1360642 1360643 1360644 1360645 1360646 1360647 [...] (424600 flits)
Measured flits: 1449306 1449307 1449308 1449309 1449310 1449311 1449312 1449313 1449314 1449315 [...] (5375 flits)
Class 0:
Remaining flits: 1432944 1432945 1432946 1432947 1432948 1432949 1432950 1432951 1432952 1432953 [...] (423015 flits)
Measured flits: 1465812 1465813 1465814 1465815 1465816 1465817 1465818 1465819 1465820 1465821 [...] (4187 flits)
Class 0:
Remaining flits: 1434780 1434781 1434782 1434783 1434784 1434785 1434786 1434787 1434788 1434789 [...] (424615 flits)
Measured flits: 1588950 1588951 1588952 1588953 1588954 1588955 1588956 1588957 1588958 1588959 [...] (3388 flits)
Class 0:
Remaining flits: 1434780 1434781 1434782 1434783 1434784 1434785 1434786 1434787 1434788 1434789 [...] (424737 flits)
Measured flits: 1603656 1603657 1603658 1603659 1603660 1603661 1603662 1603663 1603664 1603665 [...] (2339 flits)
Class 0:
Remaining flits: 1526310 1526311 1526312 1526313 1526314 1526315 1526316 1526317 1526318 1526319 [...] (425778 flits)
Measured flits: 1603657 1603658 1603659 1603660 1603661 1603662 1603663 1603664 1603665 1603666 [...] (1542 flits)
Class 0:
Remaining flits: 1664856 1664857 1664858 1664859 1664860 1664861 1664862 1664863 1664864 1664865 [...] (424541 flits)
Measured flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (864 flits)
Class 0:
Remaining flits: 1664856 1664857 1664858 1664859 1664860 1664861 1664862 1664863 1664864 1664865 [...] (426430 flits)
Measured flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (474 flits)
Class 0:
Remaining flits: 1770714 1770715 1770716 1770717 1770718 1770719 1770720 1770721 1770722 1770723 [...] (428100 flits)
Measured flits: 1998702 1998703 1998704 1998705 1998706 1998707 1998708 1998709 1998710 1998711 [...] (342 flits)
Class 0:
Remaining flits: 1774044 1774045 1774046 1774047 1774048 1774049 1774050 1774051 1774052 1774053 [...] (424646 flits)
Measured flits: 1998702 1998703 1998704 1998705 1998706 1998707 1998708 1998709 1998710 1998711 [...] (216 flits)
Class 0:
Remaining flits: 1889042 1889043 1889044 1889045 1890900 1890901 1890902 1890903 1890904 1890905 [...] (422905 flits)
Measured flits: 1998702 1998703 1998704 1998705 1998706 1998707 1998708 1998709 1998710 1998711 [...] (112 flits)
Class 0:
Remaining flits: 1935774 1935775 1935776 1935777 1935778 1935779 1935780 1935781 1935782 1935783 [...] (426276 flits)
Measured flits: 2164032 2164033 2164034 2164035 2164036 2164037 2164038 2164039 2164040 2164041 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1950768 1950769 1950770 1950771 1950772 1950773 1950774 1950775 1950776 1950777 [...] (372973 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1950768 1950769 1950770 1950771 1950772 1950773 1950774 1950775 1950776 1950777 [...] (323366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1950768 1950769 1950770 1950771 1950772 1950773 1950774 1950775 1950776 1950777 [...] (273484 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2075724 2075725 2075726 2075727 2075728 2075729 2075730 2075731 2075732 2075733 [...] (225134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2075724 2075725 2075726 2075727 2075728 2075729 2075730 2075731 2075732 2075733 [...] (177046 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2194281 2194282 2194283 2194284 2194285 2194286 2194287 2194288 2194289 2219184 [...] (129571 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305962 2305963 2305964 2305965 2305966 2305967 2305968 2305969 2305970 2305971 [...] (81917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305975 2305976 2305977 2305978 2305979 2344268 2344269 2344270 2344271 2344272 [...] (35314 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2362284 2362285 2362286 2362287 2362288 2362289 2362290 2362291 2362292 2362293 [...] (8370 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2498214 2498215 2498216 2498217 2498218 2498219 2498724 2498725 2498726 2498727 [...] (2270 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2600735 2600736 2600737 2600738 2600739 2600740 2600741 2600742 2600743 2600744 [...] (315 flits)
Measured flits: (0 flits)
Time taken is 57031 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9847.69 (1 samples)
	minimum = 24 (1 samples)
	maximum = 35938 (1 samples)
Network latency average = 6642.84 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18242 (1 samples)
Flit latency average = 7228.44 (1 samples)
	minimum = 5 (1 samples)
	maximum = 19790 (1 samples)
Fragmentation average = 54.0992 (1 samples)
	minimum = 0 (1 samples)
	maximum = 779 (1 samples)
Injected packet rate average = 0.0244635 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.041 (1 samples)
Accepted packet rate average = 0.0154315 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.440403 (1 samples)
	minimum = 0.197714 (1 samples)
	maximum = 0.739143 (1 samples)
Accepted flit rate average = 0.276813 (1 samples)
	minimum = 0.215286 (1 samples)
	maximum = 0.360571 (1 samples)
Injected packet size average = 18.0024 (1 samples)
Accepted packet size average = 17.9381 (1 samples)
Hops average = 5.07024 (1 samples)
Total run time 81.4921
