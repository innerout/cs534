BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 170.651
	minimum = 22
	maximum = 615
Network latency average = 167.044
	minimum = 22
	maximum = 615
Slowest packet = 675
Flit latency average = 135.909
	minimum = 5
	maximum = 610
Slowest flit = 21399
Fragmentation average = 34.594
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0179479
	minimum = 0.008 (at node 50)
	maximum = 0.032 (at node 9)
Accepted packet rate average = 0.0129948
	minimum = 0.005 (at node 93)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319365
	minimum = 0.144 (at node 50)
	maximum = 0.576 (at node 9)
Accepted flit rate average= 0.24263
	minimum = 0.092 (at node 174)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.794
Accepted packet length average = 18.6713
Total in-flight flits = 15461 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 264.776
	minimum = 22
	maximum = 1129
Network latency average = 260.665
	minimum = 22
	maximum = 1110
Slowest packet = 1279
Flit latency average = 225.864
	minimum = 5
	maximum = 1093
Slowest flit = 23039
Fragmentation average = 40.9945
	minimum = 0
	maximum = 257
Injected packet rate average = 0.017651
	minimum = 0.011 (at node 24)
	maximum = 0.026 (at node 83)
Accepted packet rate average = 0.0138073
	minimum = 0.008 (at node 116)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.316568
	minimum = 0.198 (at node 24)
	maximum = 0.4665 (at node 83)
Accepted flit rate average= 0.253508
	minimum = 0.144 (at node 116)
	maximum = 0.358 (at node 152)
Injected packet length average = 17.9348
Accepted packet length average = 18.3604
Total in-flight flits = 24729 (0 measured)
latency change    = 0.355491
throughput change = 0.0429084
Class 0:
Packet latency average = 500.426
	minimum = 22
	maximum = 1772
Network latency average = 487.953
	minimum = 22
	maximum = 1582
Slowest packet = 4222
Flit latency average = 447.283
	minimum = 5
	maximum = 1524
Slowest flit = 50885
Fragmentation average = 58.8532
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0169531
	minimum = 0.007 (at node 80)
	maximum = 0.03 (at node 35)
Accepted packet rate average = 0.0148698
	minimum = 0.007 (at node 180)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.303729
	minimum = 0.141 (at node 80)
	maximum = 0.537 (at node 35)
Accepted flit rate average= 0.269943
	minimum = 0.126 (at node 180)
	maximum = 0.473 (at node 34)
Injected packet length average = 17.9158
Accepted packet length average = 18.1538
Total in-flight flits = 31616 (0 measured)
latency change    = 0.470897
throughput change = 0.0608829
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 459.597
	minimum = 22
	maximum = 1952
Network latency average = 402.958
	minimum = 22
	maximum = 983
Slowest packet = 10055
Flit latency average = 570.654
	minimum = 5
	maximum = 2173
Slowest flit = 101933
Fragmentation average = 49.6632
	minimum = 0
	maximum = 236
Injected packet rate average = 0.016724
	minimum = 0.002 (at node 144)
	maximum = 0.027 (at node 133)
Accepted packet rate average = 0.0150729
	minimum = 0.005 (at node 4)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.300417
	minimum = 0.042 (at node 144)
	maximum = 0.486 (at node 165)
Accepted flit rate average= 0.271036
	minimum = 0.106 (at node 4)
	maximum = 0.528 (at node 19)
Injected packet length average = 17.9633
Accepted packet length average = 17.9817
Total in-flight flits = 37573 (35396 measured)
latency change    = 0.0888363
throughput change = 0.00403543
Class 0:
Packet latency average = 689.673
	minimum = 22
	maximum = 2582
Network latency average = 595.549
	minimum = 22
	maximum = 1909
Slowest packet = 10055
Flit latency average = 617.127
	minimum = 5
	maximum = 2774
Slowest flit = 134893
Fragmentation average = 63.1857
	minimum = 0
	maximum = 284
Injected packet rate average = 0.0165234
	minimum = 0.009 (at node 140)
	maximum = 0.0255 (at node 190)
Accepted packet rate average = 0.0150495
	minimum = 0.009 (at node 36)
	maximum = 0.0235 (at node 165)
Injected flit rate average = 0.297216
	minimum = 0.1585 (at node 144)
	maximum = 0.459 (at node 190)
Accepted flit rate average= 0.271516
	minimum = 0.1745 (at node 5)
	maximum = 0.414 (at node 165)
Injected packet length average = 17.9875
Accepted packet length average = 18.0415
Total in-flight flits = 41690 (41656 measured)
latency change    = 0.333602
throughput change = 0.00176478
Class 0:
Packet latency average = 823.386
	minimum = 22
	maximum = 3267
Network latency average = 688.39
	minimum = 22
	maximum = 2361
Slowest packet = 10055
Flit latency average = 662.652
	minimum = 5
	maximum = 2947
Slowest flit = 134909
Fragmentation average = 68.6092
	minimum = 0
	maximum = 312
Injected packet rate average = 0.0162257
	minimum = 0.00966667 (at node 144)
	maximum = 0.0236667 (at node 190)
Accepted packet rate average = 0.015026
	minimum = 0.009 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.291811
	minimum = 0.173 (at node 184)
	maximum = 0.422 (at node 190)
Accepted flit rate average= 0.270509
	minimum = 0.175667 (at node 36)
	maximum = 0.352333 (at node 157)
Injected packet length average = 17.9845
Accepted packet length average = 18.0027
Total in-flight flits = 44301 (44301 measured)
latency change    = 0.162395
throughput change = 0.00372241
Class 0:
Packet latency average = 916.199
	minimum = 22
	maximum = 3678
Network latency average = 737.205
	minimum = 22
	maximum = 3031
Slowest packet = 10055
Flit latency average = 695.173
	minimum = 5
	maximum = 2998
Slowest flit = 228790
Fragmentation average = 70.7619
	minimum = 0
	maximum = 339
Injected packet rate average = 0.0159844
	minimum = 0.00925 (at node 184)
	maximum = 0.02175 (at node 190)
Accepted packet rate average = 0.015013
	minimum = 0.011 (at node 36)
	maximum = 0.01875 (at node 118)
Injected flit rate average = 0.287582
	minimum = 0.1625 (at node 184)
	maximum = 0.3915 (at node 190)
Accepted flit rate average= 0.270401
	minimum = 0.199 (at node 36)
	maximum = 0.33725 (at node 118)
Injected packet length average = 17.9914
Accepted packet length average = 18.0111
Total in-flight flits = 45366 (45366 measured)
latency change    = 0.101302
throughput change = 0.000398071
Class 0:
Packet latency average = 1008.39
	minimum = 22
	maximum = 3835
Network latency average = 773.23
	minimum = 22
	maximum = 3625
Slowest packet = 10055
Flit latency average = 721.097
	minimum = 5
	maximum = 3608
Slowest flit = 243953
Fragmentation average = 72.8164
	minimum = 0
	maximum = 339
Injected packet rate average = 0.0159875
	minimum = 0.0102 (at node 56)
	maximum = 0.021 (at node 77)
Accepted packet rate average = 0.0149969
	minimum = 0.0116 (at node 42)
	maximum = 0.0188 (at node 128)
Injected flit rate average = 0.287414
	minimum = 0.1826 (at node 184)
	maximum = 0.3792 (at node 77)
Accepted flit rate average= 0.270564
	minimum = 0.2122 (at node 42)
	maximum = 0.337 (at node 157)
Injected packet length average = 17.9774
Accepted packet length average = 18.0413
Total in-flight flits = 48427 (48427 measured)
latency change    = 0.09142
throughput change = 0.000600598
Class 0:
Packet latency average = 1097.06
	minimum = 22
	maximum = 4303
Network latency average = 804.164
	minimum = 22
	maximum = 3625
Slowest packet = 10055
Flit latency average = 745.873
	minimum = 5
	maximum = 3608
Slowest flit = 243953
Fragmentation average = 74.6837
	minimum = 0
	maximum = 339
Injected packet rate average = 0.0158264
	minimum = 0.0108333 (at node 56)
	maximum = 0.0201667 (at node 187)
Accepted packet rate average = 0.0150243
	minimum = 0.012 (at node 42)
	maximum = 0.0183333 (at node 115)
Injected flit rate average = 0.284533
	minimum = 0.195333 (at node 56)
	maximum = 0.361167 (at node 187)
Accepted flit rate average= 0.270566
	minimum = 0.215833 (at node 42)
	maximum = 0.3285 (at node 157)
Injected packet length average = 17.9784
Accepted packet length average = 18.0086
Total in-flight flits = 48568 (48568 measured)
latency change    = 0.0808298
throughput change = 8.98323e-06
Class 0:
Packet latency average = 1179.71
	minimum = 22
	maximum = 5375
Network latency average = 827.604
	minimum = 22
	maximum = 3650
Slowest packet = 10055
Flit latency average = 765.795
	minimum = 5
	maximum = 3633
Slowest flit = 366065
Fragmentation average = 75.9182
	minimum = 0
	maximum = 339
Injected packet rate average = 0.0157225
	minimum = 0.0112857 (at node 28)
	maximum = 0.0197143 (at node 77)
Accepted packet rate average = 0.0150238
	minimum = 0.0121429 (at node 67)
	maximum = 0.0182857 (at node 103)
Injected flit rate average = 0.282783
	minimum = 0.202714 (at node 136)
	maximum = 0.356429 (at node 77)
Accepted flit rate average= 0.270818
	minimum = 0.217143 (at node 80)
	maximum = 0.329143 (at node 103)
Injected packet length average = 17.9859
Accepted packet length average = 18.0259
Total in-flight flits = 48499 (48499 measured)
latency change    = 0.0700578
throughput change = 0.000929541
Draining all recorded packets ...
Class 0:
Remaining flits: 403668 403669 403670 403671 403672 403673 403674 403675 403676 403677 [...] (48125 flits)
Measured flits: 403668 403669 403670 403671 403672 403673 403674 403675 403676 403677 [...] (31670 flits)
Class 0:
Remaining flits: 429624 429625 429626 429627 429628 429629 429630 429631 429632 429633 [...] (48794 flits)
Measured flits: 429624 429625 429626 429627 429628 429629 429630 429631 429632 429633 [...] (16229 flits)
Class 0:
Remaining flits: 429632 429633 429634 429635 429636 429637 429638 429639 429640 429641 [...] (49731 flits)
Measured flits: 429632 429633 429634 429635 429636 429637 429638 429639 429640 429641 [...] (7241 flits)
Class 0:
Remaining flits: 572616 572617 572618 572619 572620 572621 572622 572623 572624 572625 [...] (50128 flits)
Measured flits: 573822 573823 573824 573825 573826 573827 573828 573829 573830 573831 [...] (2897 flits)
Class 0:
Remaining flits: 573822 573823 573824 573825 573826 573827 573828 573829 573830 573831 [...] (50116 flits)
Measured flits: 573822 573823 573824 573825 573826 573827 573828 573829 573830 573831 [...] (1189 flits)
Class 0:
Remaining flits: 620009 639810 639811 639812 639813 639814 639815 639816 639817 639818 [...] (50237 flits)
Measured flits: 620009 754344 754345 754346 754347 754348 754349 754350 754351 754352 [...] (824 flits)
Class 0:
Remaining flits: 639810 639811 639812 639813 639814 639815 639816 639817 639818 639819 [...] (47665 flits)
Measured flits: 754344 754345 754346 754347 754348 754349 754350 754351 754352 754353 [...] (285 flits)
Class 0:
Remaining flits: 668592 668593 668594 668595 668596 668597 668598 668599 668600 668601 [...] (49287 flits)
Measured flits: 839880 839881 839882 839883 839884 839885 839886 839887 839888 839889 [...] (54 flits)
Class 0:
Remaining flits: 706770 706771 706772 706773 706774 706775 706776 706777 706778 706779 [...] (48292 flits)
Measured flits: 839880 839881 839882 839883 839884 839885 839886 839887 839888 839889 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 857988 857989 857990 857991 857992 857993 857994 857995 857996 857997 [...] (9020 flits)
Measured flits: (0 flits)
Time taken is 21708 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1564.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10337 (1 samples)
Network latency average = 893.012 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6016 (1 samples)
Flit latency average = 874.341 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6452 (1 samples)
Fragmentation average = 79.4385 (1 samples)
	minimum = 0 (1 samples)
	maximum = 339 (1 samples)
Injected packet rate average = 0.0157225 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.0197143 (1 samples)
Accepted packet rate average = 0.0150238 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.282783 (1 samples)
	minimum = 0.202714 (1 samples)
	maximum = 0.356429 (1 samples)
Accepted flit rate average = 0.270818 (1 samples)
	minimum = 0.217143 (1 samples)
	maximum = 0.329143 (1 samples)
Injected packet size average = 17.9859 (1 samples)
Accepted packet size average = 18.0259 (1 samples)
Hops average = 5.06662 (1 samples)
Total run time 26.1487
