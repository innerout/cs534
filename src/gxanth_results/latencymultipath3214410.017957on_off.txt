BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 299.62
	minimum = 27
	maximum = 977
Network latency average = 240.246
	minimum = 25
	maximum = 912
Slowest packet = 21
Flit latency average = 169.943
	minimum = 6
	maximum = 895
Slowest flit = 5741
Fragmentation average = 131.44
	minimum = 0
	maximum = 716
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00923437
	minimum = 0.003 (at node 4)
	maximum = 0.019 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.188208
	minimum = 0.082 (at node 172)
	maximum = 0.371 (at node 76)
Injected packet length average = 17.8264
Accepted packet length average = 20.3813
Total in-flight flits = 25874 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 471.886
	minimum = 23
	maximum = 1883
Network latency average = 400.206
	minimum = 23
	maximum = 1753
Slowest packet = 21
Flit latency average = 317.038
	minimum = 6
	maximum = 1835
Slowest flit = 9490
Fragmentation average = 172.548
	minimum = 0
	maximum = 1489
Injected packet rate average = 0.0173698
	minimum = 0.002 (at node 48)
	maximum = 0.0385 (at node 6)
Accepted packet rate average = 0.0103281
	minimum = 0.0055 (at node 116)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.311279
	minimum = 0.036 (at node 48)
	maximum = 0.693 (at node 147)
Accepted flit rate average= 0.198755
	minimum = 0.1035 (at node 116)
	maximum = 0.308 (at node 152)
Injected packet length average = 17.9207
Accepted packet length average = 19.2441
Total in-flight flits = 43738 (0 measured)
latency change    = 0.365058
throughput change = 0.0530646
Class 0:
Packet latency average = 907.379
	minimum = 28
	maximum = 2799
Network latency average = 816.277
	minimum = 24
	maximum = 2711
Slowest packet = 527
Flit latency average = 720.078
	minimum = 6
	maximum = 2810
Slowest flit = 10976
Fragmentation average = 225.437
	minimum = 0
	maximum = 1906
Injected packet rate average = 0.0179948
	minimum = 0 (at node 83)
	maximum = 0.048 (at node 67)
Accepted packet rate average = 0.0118281
	minimum = 0.004 (at node 2)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.323969
	minimum = 0 (at node 83)
	maximum = 0.865 (at node 67)
Accepted flit rate average= 0.213771
	minimum = 0.083 (at node 2)
	maximum = 0.384 (at node 16)
Injected packet length average = 18.0035
Accepted packet length average = 18.0731
Total in-flight flits = 64884 (0 measured)
latency change    = 0.479946
throughput change = 0.0702417
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 367.629
	minimum = 28
	maximum = 1533
Network latency average = 286.375
	minimum = 23
	maximum = 977
Slowest packet = 10131
Flit latency average = 996.87
	minimum = 6
	maximum = 3736
Slowest flit = 16151
Fragmentation average = 102.9
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0189948
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 113)
Accepted packet rate average = 0.0118958
	minimum = 0.004 (at node 17)
	maximum = 0.024 (at node 81)
Injected flit rate average = 0.341885
	minimum = 0 (at node 72)
	maximum = 1 (at node 113)
Accepted flit rate average= 0.214115
	minimum = 0.079 (at node 17)
	maximum = 0.386 (at node 137)
Injected packet length average = 17.9989
Accepted packet length average = 17.9991
Total in-flight flits = 89420 (54674 measured)
latency change    = 1.46819
throughput change = 0.00160545
Class 0:
Packet latency average = 746.771
	minimum = 28
	maximum = 2580
Network latency average = 651.554
	minimum = 23
	maximum = 1958
Slowest packet = 10131
Flit latency average = 1136.26
	minimum = 6
	maximum = 4323
Slowest flit = 36791
Fragmentation average = 150.392
	minimum = 0
	maximum = 1433
Injected packet rate average = 0.0184427
	minimum = 0.001 (at node 87)
	maximum = 0.0425 (at node 93)
Accepted packet rate average = 0.011974
	minimum = 0.007 (at node 17)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.331404
	minimum = 0.013 (at node 87)
	maximum = 0.7695 (at node 93)
Accepted flit rate average= 0.214987
	minimum = 0.132 (at node 17)
	maximum = 0.314 (at node 87)
Injected packet length average = 17.9694
Accepted packet length average = 17.9545
Total in-flight flits = 109805 (90913 measured)
latency change    = 0.507708
throughput change = 0.0040579
Class 0:
Packet latency average = 1035.47
	minimum = 25
	maximum = 3519
Network latency average = 935.238
	minimum = 23
	maximum = 2961
Slowest packet = 10131
Flit latency average = 1278.76
	minimum = 6
	maximum = 5285
Slowest flit = 42123
Fragmentation average = 169.565
	minimum = 0
	maximum = 1758
Injected packet rate average = 0.017849
	minimum = 0.00466667 (at node 51)
	maximum = 0.0343333 (at node 35)
Accepted packet rate average = 0.0118767
	minimum = 0.007 (at node 105)
	maximum = 0.0163333 (at node 66)
Injected flit rate average = 0.321335
	minimum = 0.084 (at node 51)
	maximum = 0.623 (at node 35)
Accepted flit rate average= 0.213198
	minimum = 0.128 (at node 105)
	maximum = 0.291 (at node 66)
Injected packet length average = 18.003
Accepted packet length average = 17.9509
Total in-flight flits = 127140 (116399 measured)
latency change    = 0.27881
throughput change = 0.00839156
Class 0:
Packet latency average = 1305.64
	minimum = 25
	maximum = 4602
Network latency average = 1196.47
	minimum = 23
	maximum = 3971
Slowest packet = 10131
Flit latency average = 1434.04
	minimum = 6
	maximum = 6105
Slowest flit = 39797
Fragmentation average = 179.986
	minimum = 0
	maximum = 1860
Injected packet rate average = 0.0175859
	minimum = 0.005 (at node 120)
	maximum = 0.035 (at node 147)
Accepted packet rate average = 0.0118438
	minimum = 0.008 (at node 17)
	maximum = 0.01525 (at node 44)
Injected flit rate average = 0.316402
	minimum = 0.09025 (at node 149)
	maximum = 0.63 (at node 147)
Accepted flit rate average= 0.212245
	minimum = 0.14075 (at node 105)
	maximum = 0.277 (at node 44)
Injected packet length average = 17.9918
Accepted packet length average = 17.9204
Total in-flight flits = 145006 (139139 measured)
latency change    = 0.206927
throughput change = 0.00449069
Class 0:
Packet latency average = 1549.35
	minimum = 25
	maximum = 5655
Network latency average = 1433.66
	minimum = 23
	maximum = 4851
Slowest packet = 10131
Flit latency average = 1588.93
	minimum = 6
	maximum = 6601
Slowest flit = 86616
Fragmentation average = 185.198
	minimum = 0
	maximum = 2828
Injected packet rate average = 0.0174875
	minimum = 0.0042 (at node 149)
	maximum = 0.031 (at node 99)
Accepted packet rate average = 0.011799
	minimum = 0.0088 (at node 79)
	maximum = 0.015 (at node 115)
Injected flit rate average = 0.314622
	minimum = 0.0758 (at node 149)
	maximum = 0.558 (at node 99)
Accepted flit rate average= 0.211904
	minimum = 0.1632 (at node 79)
	maximum = 0.2712 (at node 115)
Injected packet length average = 17.9912
Accepted packet length average = 17.9596
Total in-flight flits = 163694 (160543 measured)
latency change    = 0.157298
throughput change = 0.00160745
Class 0:
Packet latency average = 1785.22
	minimum = 25
	maximum = 6573
Network latency average = 1663.36
	minimum = 23
	maximum = 5841
Slowest packet = 10131
Flit latency average = 1745.76
	minimum = 6
	maximum = 7432
Slowest flit = 72053
Fragmentation average = 188.404
	minimum = 0
	maximum = 2828
Injected packet rate average = 0.0175182
	minimum = 0.00783333 (at node 148)
	maximum = 0.0296667 (at node 155)
Accepted packet rate average = 0.0117222
	minimum = 0.00883333 (at node 79)
	maximum = 0.0146667 (at node 115)
Injected flit rate average = 0.315261
	minimum = 0.141 (at node 148)
	maximum = 0.534 (at node 155)
Accepted flit rate average= 0.210766
	minimum = 0.1575 (at node 79)
	maximum = 0.266333 (at node 115)
Injected packet length average = 17.9962
Accepted packet length average = 17.98
Total in-flight flits = 185502 (183691 measured)
latency change    = 0.132122
throughput change = 0.00540193
Class 0:
Packet latency average = 2021.9
	minimum = 25
	maximum = 7551
Network latency average = 1891.33
	minimum = 23
	maximum = 6849
Slowest packet = 10131
Flit latency average = 1911.91
	minimum = 6
	maximum = 8713
Slowest flit = 78963
Fragmentation average = 191.231
	minimum = 0
	maximum = 2855
Injected packet rate average = 0.0174323
	minimum = 0.00885714 (at node 53)
	maximum = 0.0285714 (at node 21)
Accepted packet rate average = 0.0116771
	minimum = 0.00885714 (at node 79)
	maximum = 0.015 (at node 181)
Injected flit rate average = 0.31374
	minimum = 0.159429 (at node 53)
	maximum = 0.515286 (at node 21)
Accepted flit rate average= 0.209612
	minimum = 0.158429 (at node 79)
	maximum = 0.271429 (at node 181)
Injected packet length average = 17.9977
Accepted packet length average = 17.9507
Total in-flight flits = 205085 (204113 measured)
latency change    = 0.117057
throughput change = 0.00550194
Draining all recorded packets ...
Class 0:
Remaining flits: 48268 48269 48270 48271 48272 48273 48274 48275 74412 74413 [...] (223067 flits)
Measured flits: 183006 183007 183008 183009 183010 183011 183012 183013 183014 183015 [...] (180583 flits)
Class 0:
Remaining flits: 74412 74413 74414 74415 74416 74417 74418 74419 74420 74421 [...] (242355 flits)
Measured flits: 183006 183007 183008 183009 183010 183011 183012 183013 183014 183015 [...] (153188 flits)
Class 0:
Remaining flits: 74412 74413 74414 74415 74416 74417 74418 74419 74420 74421 [...] (257454 flits)
Measured flits: 183006 183007 183008 183009 183010 183011 183012 183013 183014 183015 [...] (126717 flits)
Class 0:
Remaining flits: 146340 146341 146342 146343 146344 146345 146346 146347 146348 146349 [...] (268417 flits)
Measured flits: 187956 187957 187958 187959 187960 187961 187962 187963 187964 187965 [...] (102936 flits)
Class 0:
Remaining flits: 146340 146341 146342 146343 146344 146345 146346 146347 146348 146349 [...] (276288 flits)
Measured flits: 187956 187957 187958 187959 187960 187961 187962 187963 187964 187965 [...] (83044 flits)
Class 0:
Remaining flits: 146340 146341 146342 146343 146344 146345 146346 146347 146348 146349 [...] (285316 flits)
Measured flits: 200537 206604 206605 206606 206607 206608 206609 206610 206611 206612 [...] (65292 flits)
Class 0:
Remaining flits: 146340 146341 146342 146343 146344 146345 146346 146347 146348 146349 [...] (295028 flits)
Measured flits: 206604 206605 206606 206607 206608 206609 206610 206611 206612 206613 [...] (51435 flits)
Class 0:
Remaining flits: 146342 146343 146344 146345 146346 146347 146348 146349 146350 146351 [...] (298762 flits)
Measured flits: 206617 206618 206619 206620 206621 241506 241507 241508 241509 241510 [...] (39736 flits)
Class 0:
Remaining flits: 241506 241507 241508 241509 241510 241511 241512 241513 241514 241515 [...] (303046 flits)
Measured flits: 241506 241507 241508 241509 241510 241511 241512 241513 241514 241515 [...] (30229 flits)
Class 0:
Remaining flits: 241506 241507 241508 241509 241510 241511 241512 241513 241514 241515 [...] (305527 flits)
Measured flits: 241506 241507 241508 241509 241510 241511 241512 241513 241514 241515 [...] (23105 flits)
Class 0:
Remaining flits: 243684 243685 243686 243687 243688 243689 243690 243691 243692 243693 [...] (307505 flits)
Measured flits: 243684 243685 243686 243687 243688 243689 243690 243691 243692 243693 [...] (17768 flits)
Class 0:
Remaining flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (313220 flits)
Measured flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (13374 flits)
Class 0:
Remaining flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (314309 flits)
Measured flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (10338 flits)
Class 0:
Remaining flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (315790 flits)
Measured flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (7413 flits)
Class 0:
Remaining flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (317417 flits)
Measured flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (5589 flits)
Class 0:
Remaining flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (321434 flits)
Measured flits: 251946 251947 251948 251949 251950 251951 251952 251953 251954 251955 [...] (4516 flits)
Class 0:
Remaining flits: 281963 281964 281965 281966 281967 281968 281969 384120 384121 384122 [...] (321983 flits)
Measured flits: 281963 281964 281965 281966 281967 281968 281969 384120 384121 384122 [...] (3221 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (321780 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (2458 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (318588 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (1953 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (311722 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (1553 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (317115 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (1122 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (321497 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (899 flits)
Class 0:
Remaining flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (319335 flits)
Measured flits: 408078 408079 408080 408081 408082 408083 408084 408085 408086 408087 [...] (622 flits)
Class 0:
Remaining flits: 519903 519904 519905 519906 519907 519908 519909 519910 519911 571212 [...] (317549 flits)
Measured flits: 519903 519904 519905 519906 519907 519908 519909 519910 519911 571212 [...] (413 flits)
Class 0:
Remaining flits: 571212 571213 571214 571215 571216 571217 571218 571219 571220 571221 [...] (313672 flits)
Measured flits: 571212 571213 571214 571215 571216 571217 571218 571219 571220 571221 [...] (270 flits)
Class 0:
Remaining flits: 571212 571213 571214 571215 571216 571217 571218 571219 571220 571221 [...] (314292 flits)
Measured flits: 571212 571213 571214 571215 571216 571217 571218 571219 571220 571221 [...] (251 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (313427 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (102 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (308623 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (58 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (309878 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (36 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (313094 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (309695 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (310425 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (311133 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (313169 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (315341 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Class 0:
Remaining flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (314264 flits)
Measured flits: 577494 577495 577496 577497 577498 577499 577500 577501 577502 577503 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 830196 830197 830198 830199 830200 830201 830202 830203 830204 830205 [...] (279707 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 830196 830197 830198 830199 830200 830201 830202 830203 830204 830205 [...] (244605 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 830196 830197 830198 830199 830200 830201 830202 830203 830204 830205 [...] (209867 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 830196 830197 830198 830199 830200 830201 830202 830203 830204 830205 [...] (175172 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 830196 830197 830198 830199 830200 830201 830202 830203 830204 830205 [...] (140860 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 864972 864973 864974 864975 864976 864977 864978 864979 864980 864981 [...] (108608 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 864972 864973 864974 864975 864976 864977 864978 864979 864980 864981 [...] (78882 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 864972 864973 864974 864975 864976 864977 864978 864979 864980 864981 [...] (51984 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 864972 864973 864974 864975 864976 864977 864978 864979 864980 864981 [...] (30692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 864972 864973 864974 864975 864976 864977 864978 864979 864980 864981 [...] (16088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1391184 1391185 1391186 1391187 1391188 1391189 1391190 1391191 1391192 1391193 [...] (5875 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1540179 1540180 1540181 1540182 1540183 1540184 1540185 1540186 1540187 1565285 [...] (815 flits)
Measured flits: (0 flits)
Time taken is 59345 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4660.66 (1 samples)
	minimum = 25 (1 samples)
	maximum = 37405 (1 samples)
Network latency average = 4340.96 (1 samples)
	minimum = 23 (1 samples)
	maximum = 37213 (1 samples)
Flit latency average = 6965.95 (1 samples)
	minimum = 6 (1 samples)
	maximum = 41434 (1 samples)
Fragmentation average = 210.183 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4168 (1 samples)
Injected packet rate average = 0.0174323 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0285714 (1 samples)
Accepted packet rate average = 0.0116771 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.31374 (1 samples)
	minimum = 0.159429 (1 samples)
	maximum = 0.515286 (1 samples)
Accepted flit rate average = 0.209612 (1 samples)
	minimum = 0.158429 (1 samples)
	maximum = 0.271429 (1 samples)
Injected packet size average = 17.9977 (1 samples)
Accepted packet size average = 17.9507 (1 samples)
Hops average = 5.08062 (1 samples)
Total run time 71.6227
