BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 401.531
	minimum = 23
	maximum = 957
Network latency average = 353.294
	minimum = 23
	maximum = 954
Slowest packet = 188
Flit latency average = 279.757
	minimum = 6
	maximum = 956
Slowest flit = 2302
Fragmentation average = 171.621
	minimum = 0
	maximum = 824
Injected packet rate average = 0.022026
	minimum = 0.007 (at node 24)
	maximum = 0.034 (at node 41)
Accepted packet rate average = 0.0101719
	minimum = 0.004 (at node 81)
	maximum = 0.019 (at node 173)
Injected flit rate average = 0.38874
	minimum = 0.125 (at node 24)
	maximum = 0.599 (at node 41)
Accepted flit rate average= 0.208922
	minimum = 0.092 (at node 174)
	maximum = 0.368 (at node 48)
Injected packet length average = 17.6491
Accepted packet length average = 20.5392
Total in-flight flits = 36171 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 781.086
	minimum = 23
	maximum = 1921
Network latency average = 592.833
	minimum = 23
	maximum = 1921
Slowest packet = 320
Flit latency average = 489.908
	minimum = 6
	maximum = 1966
Slowest flit = 2134
Fragmentation average = 197.998
	minimum = 0
	maximum = 1629
Injected packet rate average = 0.0165521
	minimum = 0.0055 (at node 116)
	maximum = 0.0275 (at node 38)
Accepted packet rate average = 0.0106823
	minimum = 0.006 (at node 174)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.294206
	minimum = 0.0955 (at node 116)
	maximum = 0.495 (at node 38)
Accepted flit rate average= 0.203281
	minimum = 0.1235 (at node 116)
	maximum = 0.304 (at node 22)
Injected packet length average = 17.7745
Accepted packet length average = 19.0297
Total in-flight flits = 36654 (0 measured)
latency change    = 0.485933
throughput change = 0.0277479
Class 0:
Packet latency average = 1927.59
	minimum = 825
	maximum = 2936
Network latency average = 988.086
	minimum = 26
	maximum = 2936
Slowest packet = 82
Flit latency average = 854.992
	minimum = 6
	maximum = 2919
Slowest flit = 1493
Fragmentation average = 223.107
	minimum = 0
	maximum = 2509
Injected packet rate average = 0.010526
	minimum = 0 (at node 5)
	maximum = 0.032 (at node 35)
Accepted packet rate average = 0.0108125
	minimum = 0.003 (at node 190)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.189964
	minimum = 0 (at node 23)
	maximum = 0.577 (at node 35)
Accepted flit rate average= 0.19401
	minimum = 0.069 (at node 153)
	maximum = 0.356 (at node 99)
Injected packet length average = 18.047
Accepted packet length average = 17.9432
Total in-flight flits = 35890 (0 measured)
latency change    = 0.594785
throughput change = 0.0477852
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2722.43
	minimum = 1736
	maximum = 3594
Network latency average = 341.191
	minimum = 23
	maximum = 986
Slowest packet = 8435
Flit latency average = 879.73
	minimum = 6
	maximum = 3754
Slowest flit = 32563
Fragmentation average = 122.268
	minimum = 0
	maximum = 366
Injected packet rate average = 0.0107812
	minimum = 0 (at node 1)
	maximum = 0.026 (at node 130)
Accepted packet rate average = 0.0106198
	minimum = 0.004 (at node 39)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.194344
	minimum = 0 (at node 1)
	maximum = 0.474 (at node 130)
Accepted flit rate average= 0.192245
	minimum = 0.059 (at node 113)
	maximum = 0.421 (at node 16)
Injected packet length average = 18.0261
Accepted packet length average = 18.1025
Total in-flight flits = 36437 (24527 measured)
latency change    = 0.291961
throughput change = 0.00918425
Class 0:
Packet latency average = 3243.5
	minimum = 1736
	maximum = 4463
Network latency average = 583.628
	minimum = 23
	maximum = 1951
Slowest packet = 8435
Flit latency average = 896.197
	minimum = 6
	maximum = 4895
Slowest flit = 7811
Fragmentation average = 151.583
	minimum = 0
	maximum = 1114
Injected packet rate average = 0.0108984
	minimum = 0 (at node 48)
	maximum = 0.0245 (at node 3)
Accepted packet rate average = 0.0107578
	minimum = 0.006 (at node 88)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.196411
	minimum = 0 (at node 48)
	maximum = 0.4415 (at node 3)
Accepted flit rate average= 0.193794
	minimum = 0.1205 (at node 36)
	maximum = 0.302 (at node 182)
Injected packet length average = 18.022
Accepted packet length average = 18.0143
Total in-flight flits = 36929 (33085 measured)
latency change    = 0.160651
throughput change = 0.00799548
Class 0:
Packet latency average = 3703.24
	minimum = 1736
	maximum = 5439
Network latency average = 720.185
	minimum = 23
	maximum = 2822
Slowest packet = 8435
Flit latency average = 910.04
	minimum = 6
	maximum = 5795
Slowest flit = 25681
Fragmentation average = 165.478
	minimum = 0
	maximum = 1958
Injected packet rate average = 0.0108628
	minimum = 0.001 (at node 116)
	maximum = 0.024 (at node 157)
Accepted packet rate average = 0.0107344
	minimum = 0.00666667 (at node 36)
	maximum = 0.0156667 (at node 103)
Injected flit rate average = 0.195547
	minimum = 0.0206667 (at node 116)
	maximum = 0.432 (at node 157)
Accepted flit rate average= 0.193583
	minimum = 0.121 (at node 79)
	maximum = 0.291 (at node 129)
Injected packet length average = 18.0014
Accepted packet length average = 18.034
Total in-flight flits = 36940 (35946 measured)
latency change    = 0.124145
throughput change = 0.00108965
Class 0:
Packet latency average = 4160.41
	minimum = 1736
	maximum = 6411
Network latency average = 811.376
	minimum = 23
	maximum = 3884
Slowest packet = 8435
Flit latency average = 913.77
	minimum = 6
	maximum = 6189
Slowest flit = 68754
Fragmentation average = 173.993
	minimum = 0
	maximum = 2431
Injected packet rate average = 0.0106914
	minimum = 0.00225 (at node 12)
	maximum = 0.0225 (at node 157)
Accepted packet rate average = 0.0107448
	minimum = 0.007 (at node 36)
	maximum = 0.0145 (at node 0)
Injected flit rate average = 0.192441
	minimum = 0.03725 (at node 12)
	maximum = 0.406 (at node 157)
Accepted flit rate average= 0.193686
	minimum = 0.13275 (at node 36)
	maximum = 0.2625 (at node 129)
Injected packet length average = 17.9996
Accepted packet length average = 18.0261
Total in-flight flits = 34883 (34580 measured)
latency change    = 0.109885
throughput change = 0.000531089
Class 0:
Packet latency average = 4595.66
	minimum = 1736
	maximum = 7399
Network latency average = 870.785
	minimum = 23
	maximum = 4740
Slowest packet = 8435
Flit latency average = 913.545
	minimum = 6
	maximum = 7032
Slowest flit = 67175
Fragmentation average = 184.598
	minimum = 0
	maximum = 2530
Injected packet rate average = 0.0108344
	minimum = 0.002 (at node 0)
	maximum = 0.0208 (at node 157)
Accepted packet rate average = 0.0107667
	minimum = 0.0078 (at node 36)
	maximum = 0.0146 (at node 103)
Injected flit rate average = 0.194991
	minimum = 0.0378 (at node 0)
	maximum = 0.3738 (at node 157)
Accepted flit rate average= 0.193691
	minimum = 0.144 (at node 36)
	maximum = 0.259 (at node 103)
Injected packet length average = 17.9974
Accepted packet length average = 17.9898
Total in-flight flits = 37201 (37108 measured)
latency change    = 0.0947086
throughput change = 2.28565e-05
Class 0:
Packet latency average = 5005.75
	minimum = 1736
	maximum = 7995
Network latency average = 906.633
	minimum = 23
	maximum = 5640
Slowest packet = 8435
Flit latency average = 914.237
	minimum = 6
	maximum = 7769
Slowest flit = 63551
Fragmentation average = 188.841
	minimum = 0
	maximum = 2530
Injected packet rate average = 0.0107656
	minimum = 0.00166667 (at node 0)
	maximum = 0.0196667 (at node 157)
Accepted packet rate average = 0.0107274
	minimum = 0.008 (at node 64)
	maximum = 0.0146667 (at node 128)
Injected flit rate average = 0.19376
	minimum = 0.0315 (at node 0)
	maximum = 0.354667 (at node 157)
Accepted flit rate average= 0.193082
	minimum = 0.146 (at node 64)
	maximum = 0.262 (at node 128)
Injected packet length average = 17.998
Accepted packet length average = 17.9989
Total in-flight flits = 36767 (36756 measured)
latency change    = 0.0819243
throughput change = 0.00314974
Class 0:
Packet latency average = 5412.97
	minimum = 1736
	maximum = 9183
Network latency average = 930.845
	minimum = 23
	maximum = 6044
Slowest packet = 8435
Flit latency average = 911.541
	minimum = 6
	maximum = 7769
Slowest flit = 63551
Fragmentation average = 192.081
	minimum = 0
	maximum = 2658
Injected packet rate average = 0.0107693
	minimum = 0.00228571 (at node 0)
	maximum = 0.0198571 (at node 157)
Accepted packet rate average = 0.0107321
	minimum = 0.00785714 (at node 79)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.193833
	minimum = 0.042 (at node 0)
	maximum = 0.356429 (at node 157)
Accepted flit rate average= 0.193182
	minimum = 0.142857 (at node 79)
	maximum = 0.250714 (at node 128)
Injected packet length average = 17.9985
Accepted packet length average = 18.0003
Total in-flight flits = 36893 (36893 measured)
latency change    = 0.0752306
throughput change = 0.000516747
Draining all recorded packets ...
Class 0:
Remaining flits: 169667 184481 201461 201462 201463 201464 201465 201466 201467 201468 [...] (37626 flits)
Measured flits: 169667 184481 201461 201462 201463 201464 201465 201466 201467 201468 [...] (37626 flits)
Class 0:
Remaining flits: 169667 257990 257991 257992 257993 267408 267409 267410 267411 267412 [...] (36334 flits)
Measured flits: 169667 257990 257991 257992 257993 267408 267409 267410 267411 267412 [...] (36334 flits)
Class 0:
Remaining flits: 268182 268183 268184 268185 268186 268187 268188 268189 268190 268191 [...] (35747 flits)
Measured flits: 268182 268183 268184 268185 268186 268187 268188 268189 268190 268191 [...] (35747 flits)
Class 0:
Remaining flits: 291473 300870 300871 300872 300873 300874 300875 300876 300877 300878 [...] (36483 flits)
Measured flits: 291473 300870 300871 300872 300873 300874 300875 300876 300877 300878 [...] (36483 flits)
Class 0:
Remaining flits: 320148 320149 320150 320151 320152 320153 320154 320155 320156 320157 [...] (37120 flits)
Measured flits: 320148 320149 320150 320151 320152 320153 320154 320155 320156 320157 [...] (37120 flits)
Class 0:
Remaining flits: 347040 347041 347042 347043 347044 347045 347046 347047 347048 347049 [...] (35675 flits)
Measured flits: 347040 347041 347042 347043 347044 347045 347046 347047 347048 347049 [...] (35675 flits)
Class 0:
Remaining flits: 365994 365995 365996 365997 365998 365999 366000 366001 366002 366003 [...] (34928 flits)
Measured flits: 365994 365995 365996 365997 365998 365999 366000 366001 366002 366003 [...] (34928 flits)
Class 0:
Remaining flits: 439441 439442 439443 439444 439445 439446 439447 439448 439449 439450 [...] (36135 flits)
Measured flits: 439441 439442 439443 439444 439445 439446 439447 439448 439449 439450 [...] (36135 flits)
Class 0:
Remaining flits: 466596 466597 466598 466599 466600 466601 466602 466603 466604 466605 [...] (35620 flits)
Measured flits: 466596 466597 466598 466599 466600 466601 466602 466603 466604 466605 [...] (35620 flits)
Class 0:
Remaining flits: 466596 466597 466598 466599 466600 466601 466602 466603 466604 466605 [...] (35982 flits)
Measured flits: 466596 466597 466598 466599 466600 466601 466602 466603 466604 466605 [...] (35982 flits)
Class 0:
Remaining flits: 491472 491473 491474 491475 491476 491477 491478 491479 491480 491481 [...] (35150 flits)
Measured flits: 491472 491473 491474 491475 491476 491477 491478 491479 491480 491481 [...] (35150 flits)
Class 0:
Remaining flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (36390 flits)
Measured flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (36390 flits)
Class 0:
Remaining flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35444 flits)
Measured flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35444 flits)
Class 0:
Remaining flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35787 flits)
Measured flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35787 flits)
Class 0:
Remaining flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35391 flits)
Measured flits: 565704 565705 565706 565707 565708 565709 565710 565711 565712 565713 [...] (35391 flits)
Class 0:
Remaining flits: 612666 612667 612668 612669 612670 612671 612672 612673 612674 612675 [...] (36822 flits)
Measured flits: 612666 612667 612668 612669 612670 612671 612672 612673 612674 612675 [...] (36822 flits)
Class 0:
Remaining flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (37443 flits)
Measured flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (37443 flits)
Class 0:
Remaining flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (35977 flits)
Measured flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (35977 flits)
Class 0:
Remaining flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (36022 flits)
Measured flits: 635400 635401 635402 635403 635404 635405 635406 635407 635408 635409 [...] (36022 flits)
Class 0:
Remaining flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35656 flits)
Measured flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35656 flits)
Class 0:
Remaining flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35534 flits)
Measured flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35534 flits)
Class 0:
Remaining flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35795 flits)
Measured flits: 727236 727237 727238 727239 727240 727241 727242 727243 727244 727245 [...] (35795 flits)
Class 0:
Remaining flits: 972119 972120 972121 972122 972123 972124 972125 1000890 1000891 1000892 [...] (36633 flits)
Measured flits: 972119 972120 972121 972122 972123 972124 972125 1000890 1000891 1000892 [...] (36633 flits)
Class 0:
Remaining flits: 1009746 1009747 1009748 1009749 1009750 1009751 1009752 1009753 1009754 1009755 [...] (37282 flits)
Measured flits: 1009746 1009747 1009748 1009749 1009750 1009751 1009752 1009753 1009754 1009755 [...] (37095 flits)
Class 0:
Remaining flits: 1040852 1040853 1040854 1040855 1040856 1040857 1040858 1040859 1040860 1040861 [...] (36259 flits)
Measured flits: 1040852 1040853 1040854 1040855 1040856 1040857 1040858 1040859 1040860 1040861 [...] (36082 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (35108 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (34689 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (36119 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (35294 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (36517 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (34598 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (36717 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (33276 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (36567 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (31793 flits)
Class 0:
Remaining flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (34905 flits)
Measured flits: 1086084 1086085 1086086 1086087 1086088 1086089 1086090 1086091 1086092 1086093 [...] (29523 flits)
Class 0:
Remaining flits: 1242154 1242155 1242156 1242157 1242158 1242159 1242160 1242161 1243998 1243999 [...] (36625 flits)
Measured flits: 1242154 1242155 1242156 1242157 1242158 1242159 1242160 1242161 1243998 1243999 [...] (27627 flits)
Class 0:
Remaining flits: 1243998 1243999 1244000 1244001 1244002 1244003 1244004 1244005 1244006 1244007 [...] (36294 flits)
Measured flits: 1243998 1243999 1244000 1244001 1244002 1244003 1244004 1244005 1244006 1244007 [...] (25913 flits)
Class 0:
Remaining flits: 1243998 1243999 1244000 1244001 1244002 1244003 1244004 1244005 1244006 1244007 [...] (35791 flits)
Measured flits: 1243998 1243999 1244000 1244001 1244002 1244003 1244004 1244005 1244006 1244007 [...] (22741 flits)
Class 0:
Remaining flits: 1380890 1380891 1380892 1380893 1380894 1380895 1380896 1380897 1380898 1380899 [...] (36877 flits)
Measured flits: 1380890 1380891 1380892 1380893 1380894 1380895 1380896 1380897 1380898 1380899 [...] (21192 flits)
Class 0:
Remaining flits: 1412673 1412674 1412675 1479443 1479444 1479445 1479446 1479447 1479448 1479449 [...] (35810 flits)
Measured flits: 1412673 1412674 1412675 1479443 1479444 1479445 1479446 1479447 1479448 1479449 [...] (18818 flits)
Class 0:
Remaining flits: 1479445 1479446 1479447 1479448 1479449 1479450 1479451 1479452 1479453 1479454 [...] (36752 flits)
Measured flits: 1479445 1479446 1479447 1479448 1479449 1479450 1479451 1479452 1479453 1479454 [...] (17029 flits)
Class 0:
Remaining flits: 1479445 1479446 1479447 1479448 1479449 1479450 1479451 1479452 1479453 1479454 [...] (37995 flits)
Measured flits: 1479445 1479446 1479447 1479448 1479449 1479450 1479451 1479452 1479453 1479454 [...] (14178 flits)
Class 0:
Remaining flits: 1524255 1524256 1524257 1526184 1526185 1526186 1526187 1526188 1526189 1526190 [...] (36013 flits)
Measured flits: 1540692 1540693 1540694 1540695 1540696 1540697 1540698 1540699 1540700 1540701 [...] (11091 flits)
Class 0:
Remaining flits: 1526184 1526185 1526186 1526187 1526188 1526189 1526190 1526191 1526192 1526193 [...] (35070 flits)
Measured flits: 1540692 1540693 1540694 1540695 1540696 1540697 1540698 1540699 1540700 1540701 [...] (8241 flits)
Class 0:
Remaining flits: 1618164 1618165 1618166 1618167 1618168 1618169 1618170 1618171 1618172 1618173 [...] (35845 flits)
Measured flits: 1649592 1649593 1649594 1649595 1649596 1649597 1649598 1649599 1649600 1649601 [...] (7583 flits)
Class 0:
Remaining flits: 1649592 1649593 1649594 1649595 1649596 1649597 1649598 1649599 1649600 1649601 [...] (35410 flits)
Measured flits: 1649592 1649593 1649594 1649595 1649596 1649597 1649598 1649599 1649600 1649601 [...] (5762 flits)
Class 0:
Remaining flits: 1649592 1649593 1649594 1649595 1649596 1649597 1649598 1649599 1649600 1649601 [...] (35488 flits)
Measured flits: 1649592 1649593 1649594 1649595 1649596 1649597 1649598 1649599 1649600 1649601 [...] (5191 flits)
Class 0:
Remaining flits: 1654394 1654395 1654396 1654397 1744229 1744230 1744231 1744232 1744233 1744234 [...] (35771 flits)
Measured flits: 1786854 1786855 1786856 1786857 1786858 1786859 1793898 1793899 1793900 1793901 [...] (4350 flits)
Class 0:
Remaining flits: 1780344 1780345 1780346 1780347 1780348 1780349 1780350 1780351 1780352 1780353 [...] (36829 flits)
Measured flits: 1786858 1786859 1816864 1816865 1816884 1816885 1816886 1816887 1816888 1816889 [...] (4260 flits)
Class 0:
Remaining flits: 1780344 1780345 1780346 1780347 1780348 1780349 1780350 1780351 1780352 1780353 [...] (35775 flits)
Measured flits: 1816884 1816885 1816886 1816887 1816888 1816889 1816890 1816891 1816892 1816893 [...] (3889 flits)
Class 0:
Remaining flits: 1780360 1780361 1792170 1792171 1792172 1792173 1792174 1792175 1792176 1792177 [...] (36435 flits)
Measured flits: 1816884 1816885 1816886 1816887 1816888 1816889 1816890 1816891 1816892 1816893 [...] (3377 flits)
Class 0:
Remaining flits: 1792181 1792182 1792183 1792184 1792185 1792186 1792187 1816884 1816885 1816886 [...] (34198 flits)
Measured flits: 1816884 1816885 1816886 1816887 1816888 1816889 1816890 1816891 1816892 1816893 [...] (2575 flits)
Class 0:
Remaining flits: 1832634 1832635 1832636 1832637 1832638 1832639 1832640 1832641 1832642 1832643 [...] (36504 flits)
Measured flits: 2040732 2040733 2040734 2040735 2040736 2040737 2040738 2040739 2040740 2040741 [...] (3074 flits)
Class 0:
Remaining flits: 1921428 1921429 1921430 1921431 1921432 1921433 1921434 1921435 1921436 1921437 [...] (37195 flits)
Measured flits: 2069866 2069867 2069868 2069869 2069870 2069871 2069872 2069873 2071368 2071369 [...] (2418 flits)
Class 0:
Remaining flits: 1921428 1921429 1921430 1921431 1921432 1921433 1921434 1921435 1921436 1921437 [...] (34255 flits)
Measured flits: 2071372 2071373 2071374 2071375 2071376 2071377 2071378 2071379 2071380 2071381 [...] (1943 flits)
Class 0:
Remaining flits: 1921439 1921440 1921441 1921442 1921443 1921444 1921445 2042134 2042135 2059272 [...] (36466 flits)
Measured flits: 2075148 2075149 2075150 2075151 2075152 2075153 2075154 2075155 2075156 2075157 [...] (1697 flits)
Class 0:
Remaining flits: 2059272 2059273 2059274 2059275 2059276 2059277 2059278 2059279 2059280 2059281 [...] (36543 flits)
Measured flits: 2075148 2075149 2075150 2075151 2075152 2075153 2075154 2075155 2075156 2075157 [...] (1480 flits)
Class 0:
Remaining flits: 2075161 2075162 2075163 2075164 2075165 2087796 2087797 2087798 2087799 2087800 [...] (36209 flits)
Measured flits: 2075161 2075162 2075163 2075164 2075165 2227910 2227911 2227912 2227913 2238678 [...] (1400 flits)
Class 0:
Remaining flits: 2145652 2145653 2151558 2151559 2151560 2151561 2151562 2151563 2151564 2151565 [...] (36039 flits)
Measured flits: 2238678 2238679 2238680 2238681 2238682 2238683 2238684 2238685 2238686 2238687 [...] (1208 flits)
Class 0:
Remaining flits: 2166164 2166165 2166166 2166167 2166168 2166169 2166170 2166171 2166172 2166173 [...] (36138 flits)
Measured flits: 2238680 2238681 2238682 2238683 2238684 2238685 2238686 2238687 2238688 2238689 [...] (1150 flits)
Class 0:
Remaining flits: 2166164 2166165 2166166 2166167 2166168 2166169 2166170 2166171 2166172 2166173 [...] (36823 flits)
Measured flits: 2313679 2313680 2313681 2313682 2313683 2332206 2332207 2332208 2332209 2332210 [...] (698 flits)
Class 0:
Remaining flits: 2270986 2270987 2278368 2278369 2278370 2278371 2278372 2278373 2278374 2278375 [...] (37011 flits)
Measured flits: 2313683 2332222 2332223 2341307 2341308 2341309 2341310 2341311 2341312 2341313 [...] (800 flits)
Class 0:
Remaining flits: 2278368 2278369 2278370 2278371 2278372 2278373 2278374 2278375 2278376 2278377 [...] (37723 flits)
Measured flits: 2406041 2427840 2427841 2427842 2427843 2427844 2427845 2427846 2427847 2427848 [...] (462 flits)
Class 0:
Remaining flits: 2278368 2278369 2278370 2278371 2278372 2278373 2278374 2278375 2278376 2278377 [...] (35873 flits)
Measured flits: 2499162 2499163 2499164 2499165 2499166 2499167 2499168 2499169 2499170 2499171 [...] (163 flits)
Class 0:
Remaining flits: 2278385 2364336 2364337 2364338 2364339 2364340 2364341 2364342 2364343 2364344 [...] (36668 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (108 flits)
Class 0:
Remaining flits: 2437650 2437651 2437652 2437653 2437654 2437655 2437656 2437657 2437658 2437659 [...] (36943 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (94 flits)
Class 0:
Remaining flits: 2437650 2437651 2437652 2437653 2437654 2437655 2437656 2437657 2437658 2437659 [...] (36213 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (90 flits)
Class 0:
Remaining flits: 2503782 2503783 2503784 2503785 2503786 2503787 2503788 2503789 2503790 2503791 [...] (36341 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (61 flits)
Class 0:
Remaining flits: 2503798 2503799 2554265 2554266 2554267 2554268 2554269 2554270 2554271 2554723 [...] (36483 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (36 flits)
Class 0:
Remaining flits: 2557314 2557315 2557316 2557317 2557318 2557319 2557320 2557321 2557322 2557323 [...] (38885 flits)
Measured flits: 2579958 2579959 2579960 2579961 2579962 2579963 2579964 2579965 2579966 2579967 [...] (36 flits)
Class 0:
Remaining flits: 2557326 2557327 2557328 2557329 2557330 2557331 2564878 2564879 2564880 2564881 [...] (38047 flits)
Measured flits: 2579975 2696606 2696607 2696608 2696609 2696610 2696611 2696612 2696613 2696614 [...] (122 flits)
Class 0:
Remaining flits: 2641084 2641085 2641662 2641663 2641664 2641665 2641666 2641667 2641668 2641669 [...] (37410 flits)
Measured flits: 2852838 2852839 2852840 2852841 2852842 2852843 2852844 2852845 2852846 2852847 [...] (118 flits)
Class 0:
Remaining flits: 2641669 2641670 2641671 2641672 2641673 2641674 2641675 2641676 2641677 2641678 [...] (37093 flits)
Measured flits: 2870244 2870245 2870246 2870247 2870248 2870249 2870250 2870251 2870252 2870253 [...] (46 flits)
Class 0:
Remaining flits: 2662668 2662669 2662670 2662671 2662672 2662673 2662674 2662675 2662676 2662677 [...] (38252 flits)
Measured flits: 2870247 2870248 2870249 2870250 2870251 2870252 2870253 2870254 2870255 2870256 [...] (94 flits)
Class 0:
Remaining flits: 2706316 2706317 2707059 2707060 2707061 2707062 2707063 2707064 2707065 2707066 [...] (36291 flits)
Measured flits: 2961936 2961937 2961938 2961939 2961940 2961941 2961942 2961943 2961944 2961945 [...] (116 flits)
Class 0:
Remaining flits: 2741004 2741005 2741006 2741007 2741008 2741009 2741010 2741011 2741012 2741013 [...] (37712 flits)
Measured flits: 2961936 2961937 2961938 2961939 2961940 2961941 2961942 2961943 2961944 2961945 [...] (71 flits)
Class 0:
Remaining flits: 2763126 2763127 2763128 2763129 2763130 2763131 2763132 2763133 2763134 2763135 [...] (36783 flits)
Measured flits: 2961936 2961937 2961938 2961939 2961940 2961941 2961942 2961943 2961944 2961945 [...] (36 flits)
Class 0:
Remaining flits: 2763126 2763127 2763128 2763129 2763130 2763131 2763132 2763133 2763134 2763135 [...] (36751 flits)
Measured flits: 2961938 2961939 2961940 2961941 2961942 2961943 2961944 2961945 2961946 2961947 [...] (34 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2881634 2881635 2881636 2881637 2884817 2884818 2884819 2884820 2884821 2884822 [...] (7913 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2964672 2964673 2964674 2964675 2964676 2964677 2964678 2964679 2964680 2964681 [...] (461 flits)
Measured flits: (0 flits)
Time taken is 87155 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 21458.7 (1 samples)
	minimum = 1736 (1 samples)
	maximum = 75367 (1 samples)
Network latency average = 1063.11 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13945 (1 samples)
Flit latency average = 949.826 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13928 (1 samples)
Fragmentation average = 203.256 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6020 (1 samples)
Injected packet rate average = 0.0107693 (1 samples)
	minimum = 0.00228571 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.0107321 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.193833 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.356429 (1 samples)
Accepted flit rate average = 0.193182 (1 samples)
	minimum = 0.142857 (1 samples)
	maximum = 0.250714 (1 samples)
Injected packet size average = 17.9985 (1 samples)
Accepted packet size average = 18.0003 (1 samples)
Hops average = 5.05817 (1 samples)
Total run time 96.4121
