BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 264.814
	minimum = 23
	maximum = 934
Network latency average = 213.009
	minimum = 23
	maximum = 868
Slowest packet = 64
Flit latency average = 175.488
	minimum = 6
	maximum = 851
Slowest flit = 5237
Fragmentation average = 52.6225
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.00903646
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.170365
	minimum = 0.036 (at node 174)
	maximum = 0.316 (at node 152)
Injected packet length average = 17.8661
Accepted packet length average = 18.853
Total in-flight flits = 21884 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 429.915
	minimum = 23
	maximum = 1860
Network latency average = 368.307
	minimum = 23
	maximum = 1533
Slowest packet = 64
Flit latency average = 326.748
	minimum = 6
	maximum = 1516
Slowest flit = 21185
Fragmentation average = 60.013
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.00940885
	minimum = 0.005 (at node 108)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.172831
	minimum = 0.09 (at node 153)
	maximum = 0.265 (at node 22)
Injected packet length average = 17.9137
Accepted packet length average = 18.3689
Total in-flight flits = 41273 (0 measured)
latency change    = 0.384031
throughput change = 0.0142691
Class 0:
Packet latency average = 886.736
	minimum = 27
	maximum = 2750
Network latency average = 810.12
	minimum = 27
	maximum = 2517
Slowest packet = 1441
Flit latency average = 770.932
	minimum = 6
	maximum = 2500
Slowest flit = 24263
Fragmentation average = 70.5192
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.00975
	minimum = 0.002 (at node 151)
	maximum = 0.019 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.176943
	minimum = 0.036 (at node 184)
	maximum = 0.345 (at node 56)
Injected packet length average = 18.0167
Accepted packet length average = 18.148
Total in-flight flits = 63280 (0 measured)
latency change    = 0.515171
throughput change = 0.023239
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 290.968
	minimum = 23
	maximum = 1047
Network latency average = 220.867
	minimum = 23
	maximum = 976
Slowest packet = 9099
Flit latency average = 1114.15
	minimum = 6
	maximum = 3142
Slowest flit = 31049
Fragmentation average = 37.0461
	minimum = 0
	maximum = 131
Injected packet rate average = 0.0153438
	minimum = 0 (at node 35)
	maximum = 0.049 (at node 138)
Accepted packet rate average = 0.0101198
	minimum = 0.003 (at node 36)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.276078
	minimum = 0 (at node 35)
	maximum = 0.885 (at node 138)
Accepted flit rate average= 0.182854
	minimum = 0.053 (at node 36)
	maximum = 0.323 (at node 97)
Injected packet length average = 17.9929
Accepted packet length average = 18.069
Total in-flight flits = 81200 (45068 measured)
latency change    = 2.04753
throughput change = 0.0323288
Class 0:
Packet latency average = 631.934
	minimum = 23
	maximum = 2195
Network latency average = 561.103
	minimum = 23
	maximum = 1975
Slowest packet = 9099
Flit latency average = 1265.91
	minimum = 6
	maximum = 3957
Slowest flit = 35675
Fragmentation average = 53.3728
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0151641
	minimum = 0 (at node 80)
	maximum = 0.0405 (at node 39)
Accepted packet rate average = 0.00995573
	minimum = 0.0035 (at node 36)
	maximum = 0.0155 (at node 145)
Injected flit rate average = 0.273049
	minimum = 0 (at node 80)
	maximum = 0.7205 (at node 39)
Accepted flit rate average= 0.179068
	minimum = 0.0625 (at node 36)
	maximum = 0.277 (at node 145)
Injected packet length average = 18.0064
Accepted packet length average = 17.9864
Total in-flight flits = 99332 (80049 measured)
latency change    = 0.539559
throughput change = 0.0211454
Class 0:
Packet latency average = 998.921
	minimum = 23
	maximum = 3086
Network latency average = 926.171
	minimum = 23
	maximum = 2983
Slowest packet = 9099
Flit latency average = 1439
	minimum = 6
	maximum = 4948
Slowest flit = 49535
Fragmentation average = 61.6495
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0149444
	minimum = 0.000666667 (at node 68)
	maximum = 0.032 (at node 41)
Accepted packet rate average = 0.00980729
	minimum = 0.00366667 (at node 36)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.269177
	minimum = 0.012 (at node 68)
	maximum = 0.576 (at node 41)
Accepted flit rate average= 0.176179
	minimum = 0.0683333 (at node 36)
	maximum = 0.254667 (at node 151)
Injected packet length average = 18.0118
Accepted packet length average = 17.9641
Total in-flight flits = 116745 (107069 measured)
latency change    = 0.367383
throughput change = 0.0163975
Class 0:
Packet latency average = 1343.8
	minimum = 23
	maximum = 4201
Network latency average = 1268.21
	minimum = 23
	maximum = 3961
Slowest packet = 9099
Flit latency average = 1613.94
	minimum = 6
	maximum = 5299
Slowest flit = 83861
Fragmentation average = 65.0657
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0152031
	minimum = 0.0025 (at node 68)
	maximum = 0.032 (at node 66)
Accepted packet rate average = 0.00980339
	minimum = 0.005 (at node 36)
	maximum = 0.014 (at node 182)
Injected flit rate average = 0.273595
	minimum = 0.0425 (at node 68)
	maximum = 0.576 (at node 66)
Accepted flit rate average= 0.176379
	minimum = 0.0905 (at node 36)
	maximum = 0.253 (at node 182)
Injected packet length average = 17.996
Accepted packet length average = 17.9916
Total in-flight flits = 137989 (133694 measured)
latency change    = 0.256645
throughput change = 0.00113441
Class 0:
Packet latency average = 1646.82
	minimum = 23
	maximum = 5045
Network latency average = 1568.23
	minimum = 23
	maximum = 4876
Slowest packet = 9139
Flit latency average = 1784.69
	minimum = 6
	maximum = 6116
Slowest flit = 99107
Fragmentation average = 66.6385
	minimum = 0
	maximum = 165
Injected packet rate average = 0.015376
	minimum = 0.005 (at node 109)
	maximum = 0.0302 (at node 58)
Accepted packet rate average = 0.00979271
	minimum = 0.0052 (at node 36)
	maximum = 0.0138 (at node 129)
Injected flit rate average = 0.276754
	minimum = 0.09 (at node 109)
	maximum = 0.5436 (at node 58)
Accepted flit rate average= 0.176144
	minimum = 0.0934 (at node 36)
	maximum = 0.246 (at node 129)
Injected packet length average = 17.9991
Accepted packet length average = 17.9872
Total in-flight flits = 159880 (158220 measured)
latency change    = 0.184004
throughput change = 0.00133502
Class 0:
Packet latency average = 1923.26
	minimum = 23
	maximum = 6237
Network latency average = 1843.96
	minimum = 23
	maximum = 5905
Slowest packet = 9130
Flit latency average = 1953.1
	minimum = 6
	maximum = 7084
Slowest flit = 96893
Fragmentation average = 67.2172
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0153151
	minimum = 0.006 (at node 122)
	maximum = 0.0306667 (at node 190)
Accepted packet rate average = 0.00980122
	minimum = 0.00633333 (at node 36)
	maximum = 0.0131667 (at node 78)
Injected flit rate average = 0.275634
	minimum = 0.108 (at node 122)
	maximum = 0.551167 (at node 190)
Accepted flit rate average= 0.176355
	minimum = 0.1165 (at node 36)
	maximum = 0.2355 (at node 151)
Injected packet length average = 17.9975
Accepted packet length average = 17.9932
Total in-flight flits = 177693 (177008 measured)
latency change    = 0.143733
throughput change = 0.00119806
Class 0:
Packet latency average = 2170.59
	minimum = 23
	maximum = 7090
Network latency average = 2090.53
	minimum = 23
	maximum = 6816
Slowest packet = 9130
Flit latency average = 2119.34
	minimum = 6
	maximum = 7449
Slowest flit = 100979
Fragmentation average = 67.3797
	minimum = 0
	maximum = 165
Injected packet rate average = 0.015529
	minimum = 0.00642857 (at node 122)
	maximum = 0.0308571 (at node 190)
Accepted packet rate average = 0.00982664
	minimum = 0.00642857 (at node 36)
	maximum = 0.013 (at node 128)
Injected flit rate average = 0.279579
	minimum = 0.113714 (at node 122)
	maximum = 0.553 (at node 190)
Accepted flit rate average= 0.176824
	minimum = 0.115571 (at node 36)
	maximum = 0.234143 (at node 128)
Injected packet length average = 18.0036
Accepted packet length average = 17.9943
Total in-flight flits = 201307 (201059 measured)
latency change    = 0.113947
throughput change = 0.00265024
Draining all recorded packets ...
Class 0:
Remaining flits: 115885 115886 115887 115888 115889 115890 115891 115892 115893 115894 [...] (221720 flits)
Measured flits: 164232 164233 164234 164235 164236 164237 164238 164239 164240 164241 [...] (175450 flits)
Class 0:
Remaining flits: 131364 131365 131366 131367 131368 131369 131370 131371 131372 131373 [...] (243310 flits)
Measured flits: 174690 174691 174692 174693 174694 174695 174696 174697 174698 174699 [...] (147414 flits)
Class 0:
Remaining flits: 131364 131365 131366 131367 131368 131369 131370 131371 131372 131373 [...] (262431 flits)
Measured flits: 183258 183259 183260 183261 183262 183263 183264 183265 183266 183267 [...] (120293 flits)
Class 0:
Remaining flits: 191160 191161 191162 191163 191164 191165 191166 191167 191168 191169 [...] (282047 flits)
Measured flits: 191160 191161 191162 191163 191164 191165 191166 191167 191168 191169 [...] (95979 flits)
Class 0:
Remaining flits: 191160 191161 191162 191163 191164 191165 191166 191167 191168 191169 [...] (303901 flits)
Measured flits: 191160 191161 191162 191163 191164 191165 191166 191167 191168 191169 [...] (74342 flits)
Class 0:
Remaining flits: 208710 208711 208712 208713 208714 208715 208716 208717 208718 208719 [...] (325825 flits)
Measured flits: 208710 208711 208712 208713 208714 208715 208716 208717 208718 208719 [...] (55156 flits)
Class 0:
Remaining flits: 217398 217399 217400 217401 217402 217403 219510 219511 219512 219513 [...] (345662 flits)
Measured flits: 217398 217399 217400 217401 217402 217403 219510 219511 219512 219513 [...] (40345 flits)
Class 0:
Remaining flits: 228996 228997 228998 228999 229000 229001 229002 229003 229004 229005 [...] (369047 flits)
Measured flits: 228996 228997 228998 228999 229000 229001 229002 229003 229004 229005 [...] (28509 flits)
Class 0:
Remaining flits: 230058 230059 230060 230061 230062 230063 230064 230065 230066 230067 [...] (389273 flits)
Measured flits: 230058 230059 230060 230061 230062 230063 230064 230065 230066 230067 [...] (19540 flits)
Class 0:
Remaining flits: 237636 237637 237638 237639 237640 237641 237642 237643 237644 237645 [...] (409534 flits)
Measured flits: 237636 237637 237638 237639 237640 237641 237642 237643 237644 237645 [...] (13144 flits)
Class 0:
Remaining flits: 242640 242641 242642 242643 242644 242645 242646 242647 242648 242649 [...] (430349 flits)
Measured flits: 242640 242641 242642 242643 242644 242645 242646 242647 242648 242649 [...] (8631 flits)
Class 0:
Remaining flits: 242640 242641 242642 242643 242644 242645 242646 242647 242648 242649 [...] (450449 flits)
Measured flits: 242640 242641 242642 242643 242644 242645 242646 242647 242648 242649 [...] (5634 flits)
Class 0:
Remaining flits: 249498 249499 249500 249501 249502 249503 249504 249505 249506 249507 [...] (470018 flits)
Measured flits: 249498 249499 249500 249501 249502 249503 249504 249505 249506 249507 [...] (3789 flits)
Class 0:
Remaining flits: 253278 253279 253280 253281 253282 253283 253284 253285 253286 253287 [...] (490384 flits)
Measured flits: 253278 253279 253280 253281 253282 253283 253284 253285 253286 253287 [...] (2404 flits)
Class 0:
Remaining flits: 253278 253279 253280 253281 253282 253283 253284 253285 253286 253287 [...] (511500 flits)
Measured flits: 253278 253279 253280 253281 253282 253283 253284 253285 253286 253287 [...] (1407 flits)
Class 0:
Remaining flits: 453618 453619 453620 453621 453622 453623 453624 453625 453626 453627 [...] (533830 flits)
Measured flits: 453618 453619 453620 453621 453622 453623 453624 453625 453626 453627 [...] (683 flits)
Class 0:
Remaining flits: 500796 500797 500798 500799 500800 500801 500802 500803 500804 500805 [...] (550730 flits)
Measured flits: 500796 500797 500798 500799 500800 500801 500802 500803 500804 500805 [...] (280 flits)
Class 0:
Remaining flits: 517572 517573 517574 517575 517576 517577 517578 517579 517580 517581 [...] (568969 flits)
Measured flits: 517572 517573 517574 517575 517576 517577 517578 517579 517580 517581 [...] (126 flits)
Class 0:
Remaining flits: 543870 543871 543872 543873 543874 543875 543876 543877 543878 543879 [...] (586166 flits)
Measured flits: 543870 543871 543872 543873 543874 543875 543876 543877 543878 543879 [...] (90 flits)
Class 0:
Remaining flits: 549234 549235 549236 549237 549238 549239 549240 549241 549242 549243 [...] (603558 flits)
Measured flits: 549234 549235 549236 549237 549238 549239 549240 549241 549242 549243 [...] (72 flits)
Class 0:
Remaining flits: 549234 549235 549236 549237 549238 549239 549240 549241 549242 549243 [...] (619818 flits)
Measured flits: 549234 549235 549236 549237 549238 549239 549240 549241 549242 549243 [...] (54 flits)
Class 0:
Remaining flits: 560376 560377 560378 560379 560380 560381 560382 560383 560384 560385 [...] (635814 flits)
Measured flits: 560376 560377 560378 560379 560380 560381 560382 560383 560384 560385 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 591642 591643 591644 591645 591646 591647 591648 591649 591650 591651 [...] (615107 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 594414 594415 594416 594417 594418 594419 594420 594421 594422 594423 [...] (584842 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 595296 595297 595298 595299 595300 595301 595302 595303 595304 595305 [...] (554043 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 600408 600409 600410 600411 600412 600413 600414 600415 600416 600417 [...] (524307 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 658152 658153 658154 658155 658156 658157 658158 658159 658160 658161 [...] (494917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 658152 658153 658154 658155 658156 658157 658158 658159 658160 658161 [...] (465855 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 729054 729055 729056 729057 729058 729059 729060 729061 729062 729063 [...] (435275 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 729054 729055 729056 729057 729058 729059 729060 729061 729062 729063 [...] (405815 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 795456 795457 795458 795459 795460 795461 795462 795463 795464 795465 [...] (376408 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 819360 819361 819362 819363 819364 819365 819366 819367 819368 819369 [...] (346944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 819360 819361 819362 819363 819364 819365 819366 819367 819368 819369 [...] (317047 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 820260 820261 820262 820263 820264 820265 820266 820267 820268 820269 [...] (287441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824166 824167 824168 824169 824170 824171 824172 824173 824174 824175 [...] (257489 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 901098 901099 901100 901101 901102 901103 901104 901105 901106 901107 [...] (228083 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 916722 916723 916724 916725 916726 916727 916728 916729 916730 916731 [...] (198127 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 921924 921925 921926 921927 921928 921929 921930 921931 921932 921933 [...] (168761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 946890 946891 946892 946893 946894 946895 946896 946897 946898 946899 [...] (141104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 983340 983341 983342 983343 983344 983345 983346 983347 983348 983349 [...] (113372 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1051488 1051489 1051490 1051491 1051492 1051493 1051494 1051495 1051496 1051497 [...] (87222 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1064052 1064053 1064054 1064055 1064056 1064057 1064058 1064059 1064060 1064061 [...] (62730 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1086750 1086751 1086752 1086753 1086754 1086755 1086756 1086757 1086758 1086759 [...] (40381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1086750 1086751 1086752 1086753 1086754 1086755 1086756 1086757 1086758 1086759 [...] (22528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1086754 1086755 1086756 1086757 1086758 1086759 1086760 1086761 1086762 1086763 [...] (9988 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1273342 1273343 1273344 1273345 1273346 1273347 1273348 1273349 1273350 1273351 [...] (2902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1607892 1607893 1607894 1607895 1607896 1607897 1607898 1607899 1607900 1607901 [...] (30 flits)
Measured flits: (0 flits)
Time taken is 57562 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4600.56 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22663 (1 samples)
Network latency average = 4515.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 22128 (1 samples)
Flit latency average = 10812 (1 samples)
	minimum = 6 (1 samples)
	maximum = 35540 (1 samples)
Fragmentation average = 72.7762 (1 samples)
	minimum = 0 (1 samples)
	maximum = 165 (1 samples)
Injected packet rate average = 0.015529 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.0308571 (1 samples)
Accepted packet rate average = 0.00982664 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.279579 (1 samples)
	minimum = 0.113714 (1 samples)
	maximum = 0.553 (1 samples)
Accepted flit rate average = 0.176824 (1 samples)
	minimum = 0.115571 (1 samples)
	maximum = 0.234143 (1 samples)
Injected packet size average = 18.0036 (1 samples)
Accepted packet size average = 17.9943 (1 samples)
Hops average = 5.08621 (1 samples)
Total run time 36.5814
