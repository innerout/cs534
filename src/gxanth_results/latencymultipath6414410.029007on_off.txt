BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.176
	minimum = 23
	maximum = 985
Network latency average = 265.845
	minimum = 23
	maximum = 896
Slowest packet = 170
Flit latency average = 193.046
	minimum = 6
	maximum = 933
Slowest flit = 3804
Fragmentation average = 158.941
	minimum = 0
	maximum = 841
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.00983333
	minimum = 0.003 (at node 37)
	maximum = 0.018 (at node 132)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.20899
	minimum = 0.093 (at node 112)
	maximum = 0.378 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 21.2532
Total in-flight flits = 48092 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 584.09
	minimum = 23
	maximum = 1979
Network latency average = 460.063
	minimum = 23
	maximum = 1877
Slowest packet = 112
Flit latency average = 369.127
	minimum = 6
	maximum = 1945
Slowest flit = 3304
Fragmentation average = 223.364
	minimum = 0
	maximum = 1652
Injected packet rate average = 0.0267552
	minimum = 0.004 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0113906
	minimum = 0.0065 (at node 79)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.479417
	minimum = 0.0705 (at node 38)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.225992
	minimum = 0.1325 (at node 135)
	maximum = 0.3265 (at node 63)
Injected packet length average = 17.9186
Accepted packet length average = 19.8402
Total in-flight flits = 98151 (0 measured)
latency change    = 0.395339
throughput change = 0.0752354
Class 0:
Packet latency average = 1186
	minimum = 30
	maximum = 2933
Network latency average = 998.829
	minimum = 25
	maximum = 2817
Slowest packet = 618
Flit latency average = 897.349
	minimum = 6
	maximum = 2800
Slowest flit = 14543
Fragmentation average = 331.889
	minimum = 2
	maximum = 2379
Injected packet rate average = 0.0280312
	minimum = 0 (at node 25)
	maximum = 0.056 (at node 121)
Accepted packet rate average = 0.0131302
	minimum = 0.005 (at node 45)
	maximum = 0.024 (at node 136)
Injected flit rate average = 0.504229
	minimum = 0 (at node 25)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.242703
	minimum = 0.105 (at node 54)
	maximum = 0.446 (at node 136)
Injected packet length average = 17.9881
Accepted packet length average = 18.4843
Total in-flight flits = 148428 (0 measured)
latency change    = 0.507514
throughput change = 0.0688534
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 413.487
	minimum = 27
	maximum = 2568
Network latency average = 179.599
	minimum = 25
	maximum = 962
Slowest packet = 15740
Flit latency average = 1335.07
	minimum = 6
	maximum = 3830
Slowest flit = 13683
Fragmentation average = 83.9644
	minimum = 0
	maximum = 674
Injected packet rate average = 0.0281979
	minimum = 0.001 (at node 77)
	maximum = 0.056 (at node 34)
Accepted packet rate average = 0.0135365
	minimum = 0.006 (at node 126)
	maximum = 0.024 (at node 33)
Injected flit rate average = 0.507589
	minimum = 0.031 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.245812
	minimum = 0.122 (at node 2)
	maximum = 0.426 (at node 27)
Injected packet length average = 18.0009
Accepted packet length average = 18.1593
Total in-flight flits = 198684 (86456 measured)
latency change    = 1.8683
throughput change = 0.0126494
Class 0:
Packet latency average = 723.725
	minimum = 27
	maximum = 3696
Network latency average = 489.046
	minimum = 25
	maximum = 1982
Slowest packet = 15740
Flit latency average = 1580.57
	minimum = 6
	maximum = 4814
Slowest flit = 13675
Fragmentation average = 159.217
	minimum = 0
	maximum = 1280
Injected packet rate average = 0.0272161
	minimum = 0.0045 (at node 153)
	maximum = 0.056 (at node 167)
Accepted packet rate average = 0.0133698
	minimum = 0.0065 (at node 126)
	maximum = 0.021 (at node 103)
Injected flit rate average = 0.489771
	minimum = 0.081 (at node 153)
	maximum = 1 (at node 50)
Accepted flit rate average= 0.242789
	minimum = 0.1285 (at node 88)
	maximum = 0.356 (at node 136)
Injected packet length average = 17.9956
Accepted packet length average = 18.1595
Total in-flight flits = 243315 (159603 measured)
latency change    = 0.428669
throughput change = 0.0124529
Class 0:
Packet latency average = 1100.09
	minimum = 27
	maximum = 4565
Network latency average = 855.345
	minimum = 23
	maximum = 2967
Slowest packet = 15740
Flit latency average = 1843.26
	minimum = 6
	maximum = 5861
Slowest flit = 9219
Fragmentation average = 214.486
	minimum = 0
	maximum = 2049
Injected packet rate average = 0.0262813
	minimum = 0.00533333 (at node 132)
	maximum = 0.0553333 (at node 50)
Accepted packet rate average = 0.0132882
	minimum = 0.008 (at node 44)
	maximum = 0.0193333 (at node 103)
Injected flit rate average = 0.473009
	minimum = 0.096 (at node 132)
	maximum = 1 (at node 50)
Accepted flit rate average= 0.240382
	minimum = 0.159667 (at node 171)
	maximum = 0.334667 (at node 128)
Injected packet length average = 17.998
Accepted packet length average = 18.0899
Total in-flight flits = 282470 (221923 measured)
latency change    = 0.342123
throughput change = 0.0100137
Class 0:
Packet latency average = 1517.32
	minimum = 25
	maximum = 5729
Network latency average = 1254.82
	minimum = 23
	maximum = 3930
Slowest packet = 15740
Flit latency average = 2100.31
	minimum = 6
	maximum = 6512
Slowest flit = 32993
Fragmentation average = 249.844
	minimum = 0
	maximum = 2696
Injected packet rate average = 0.0256523
	minimum = 0.00675 (at node 132)
	maximum = 0.05325 (at node 41)
Accepted packet rate average = 0.0131589
	minimum = 0.0085 (at node 126)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.461786
	minimum = 0.1215 (at node 132)
	maximum = 0.95725 (at node 41)
Accepted flit rate average= 0.23798
	minimum = 0.165 (at node 88)
	maximum = 0.329 (at node 103)
Injected packet length average = 18.0017
Accepted packet length average = 18.0852
Total in-flight flits = 320277 (277544 measured)
latency change    = 0.274978
throughput change = 0.0100911
Class 0:
Packet latency average = 1963.46
	minimum = 25
	maximum = 6433
Network latency average = 1667.58
	minimum = 23
	maximum = 4963
Slowest packet = 15740
Flit latency average = 2355.29
	minimum = 6
	maximum = 7574
Slowest flit = 25748
Fragmentation average = 275.673
	minimum = 0
	maximum = 3408
Injected packet rate average = 0.0253594
	minimum = 0.0098 (at node 87)
	maximum = 0.0446 (at node 170)
Accepted packet rate average = 0.0130906
	minimum = 0.0086 (at node 88)
	maximum = 0.0188 (at node 103)
Injected flit rate average = 0.456472
	minimum = 0.1786 (at node 87)
	maximum = 0.8018 (at node 170)
Accepted flit rate average= 0.236421
	minimum = 0.1676 (at node 88)
	maximum = 0.3346 (at node 103)
Injected packet length average = 18.0001
Accepted packet length average = 18.0603
Total in-flight flits = 359674 (330267 measured)
latency change    = 0.22722
throughput change = 0.00659686
Class 0:
Packet latency average = 2434.08
	minimum = 25
	maximum = 7728
Network latency average = 2106.7
	minimum = 23
	maximum = 5922
Slowest packet = 15740
Flit latency average = 2600.28
	minimum = 6
	maximum = 8529
Slowest flit = 25752
Fragmentation average = 295.224
	minimum = 0
	maximum = 5266
Injected packet rate average = 0.0251458
	minimum = 0.0085 (at node 160)
	maximum = 0.0425 (at node 150)
Accepted packet rate average = 0.0130217
	minimum = 0.009 (at node 42)
	maximum = 0.0185 (at node 103)
Injected flit rate average = 0.452462
	minimum = 0.1525 (at node 160)
	maximum = 0.7635 (at node 150)
Accepted flit rate average= 0.234679
	minimum = 0.164333 (at node 42)
	maximum = 0.331833 (at node 103)
Injected packet length average = 17.9935
Accepted packet length average = 18.0221
Total in-flight flits = 399502 (378395 measured)
latency change    = 0.193347
throughput change = 0.00742297
Class 0:
Packet latency average = 2888.42
	minimum = 25
	maximum = 7946
Network latency average = 2516.63
	minimum = 23
	maximum = 6884
Slowest packet = 15740
Flit latency average = 2835.31
	minimum = 6
	maximum = 9291
Slowest flit = 58150
Fragmentation average = 306.791
	minimum = 0
	maximum = 5266
Injected packet rate average = 0.0248437
	minimum = 0.00928571 (at node 160)
	maximum = 0.0401429 (at node 170)
Accepted packet rate average = 0.0129129
	minimum = 0.00928571 (at node 42)
	maximum = 0.0178571 (at node 103)
Injected flit rate average = 0.44713
	minimum = 0.167143 (at node 160)
	maximum = 0.721571 (at node 170)
Accepted flit rate average= 0.232499
	minimum = 0.174429 (at node 42)
	maximum = 0.317286 (at node 103)
Injected packet length average = 17.9977
Accepted packet length average = 18.0051
Total in-flight flits = 436969 (421057 measured)
latency change    = 0.157296
throughput change = 0.0093745
Draining all recorded packets ...
Class 0:
Remaining flits: 21820 21821 21822 21823 21824 21825 21826 21827 21828 21829 [...] (470702 flits)
Measured flits: 281898 281899 281900 281901 281902 281903 281904 281905 281906 281907 [...] (416079 flits)
Class 0:
Remaining flits: 21820 21821 21822 21823 21824 21825 21826 21827 21828 21829 [...] (499454 flits)
Measured flits: 281898 281899 281900 281901 281902 281903 281904 281905 281906 281907 [...] (396881 flits)
Class 0:
Remaining flits: 21820 21821 21822 21823 21824 21825 21826 21827 21828 21829 [...] (531311 flits)
Measured flits: 281898 281899 281900 281901 281902 281903 281904 281905 281906 281907 [...] (376668 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (558715 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (353818 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (583552 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (330983 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (604177 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (308963 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (627655 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (285324 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (646579 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (262548 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (665999 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (240264 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (682416 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (221509 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (702817 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (204025 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (717375 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (186780 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (726448 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (169685 flits)
Class 0:
Remaining flits: 32112 32113 32114 32115 32116 32117 32118 32119 32120 32121 [...] (734296 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (153362 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (734631 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (138267 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (734797 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (124801 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (733228 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (112473 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (729406 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (100369 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (722898 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (89864 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (718989 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (80294 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (716156 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (71933 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (717782 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (63584 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (716518 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (56741 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (712786 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (50464 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (715363 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (44480 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (718753 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (39663 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (719914 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (34583 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (720047 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (30259 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (721182 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (27165 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (721182 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (23746 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (719263 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (20843 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (722038 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (18261 flits)
Class 0:
Remaining flits: 112014 112015 112016 112017 112018 112019 112020 112021 112022 112023 [...] (720085 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (15695 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (723237 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (13279 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (721976 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (11297 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (719401 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (9598 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (718615 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (8191 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (714566 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (7078 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (710849 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (5993 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (707814 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (5177 flits)
Class 0:
Remaining flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (711120 flits)
Measured flits: 281952 281953 281954 281955 281956 281957 281958 281959 281960 281961 [...] (4377 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (707873 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (3691 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (705335 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (3205 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (710056 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (2534 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (710335 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (2080 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (705503 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (1879 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (702932 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (1705 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (700600 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (1438 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (701391 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (1192 flits)
Class 0:
Remaining flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (705135 flits)
Measured flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (941 flits)
Class 0:
Remaining flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (702585 flits)
Measured flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (909 flits)
Class 0:
Remaining flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (697877 flits)
Measured flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (830 flits)
Class 0:
Remaining flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (693749 flits)
Measured flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (792 flits)
Class 0:
Remaining flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (695954 flits)
Measured flits: 528444 528445 528446 528447 528448 528449 528450 528451 528452 528453 [...] (683 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (695988 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (553 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (688882 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (522 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (686997 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (504 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (684033 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (486 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (686157 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (441 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (688246 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (306 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (692180 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (230 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (693518 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (201 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (693123 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (183 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (694646 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (162 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (695285 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (143 flits)
Class 0:
Remaining flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (693887 flits)
Measured flits: 679086 679087 679088 679089 679090 679091 679092 679093 679094 679095 [...] (107 flits)
Class 0:
Remaining flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (692713 flits)
Measured flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (89 flits)
Class 0:
Remaining flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (693986 flits)
Measured flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (89 flits)
Class 0:
Remaining flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (691734 flits)
Measured flits: 684900 684901 684902 684903 684904 684905 684906 684907 684908 684909 [...] (89 flits)
Class 0:
Remaining flits: 966546 966547 966548 966549 966550 966551 966552 966553 966554 966555 [...] (689444 flits)
Measured flits: 1352755 1352756 1352757 1352758 1352759 1352760 1352761 1352762 1352763 1352764 [...] (53 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (687941 flits)
Measured flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (36 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (685858 flits)
Measured flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (36 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (681601 flits)
Measured flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (36 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (679450 flits)
Measured flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (36 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (678171 flits)
Measured flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (36 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (676349 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (677228 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1117620 1117621 1117622 1117623 1117624 1117625 1117626 1117627 1117628 1117629 [...] (674464 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1117629 1117630 1117631 1117632 1117633 1117634 1117635 1117636 1117637 1350918 [...] (676704 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1350918 1350919 1350920 1350921 1350922 1350923 1350924 1350925 1350926 1350927 [...] (675498 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1350918 1350919 1350920 1350921 1350922 1350923 1350924 1350925 1350926 1350927 [...] (672976 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Class 0:
Remaining flits: 1350918 1350919 1350920 1350921 1350922 1350923 1350924 1350925 1350926 1350927 [...] (671832 flits)
Measured flits: 2109798 2109799 2109800 2109801 2109802 2109803 2109804 2109805 2109806 2109807 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1350918 1350919 1350920 1350921 1350922 1350923 1350924 1350925 1350926 1350927 [...] (638498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (603586 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (568772 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (534074 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (501170 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (467486 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (435564 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (403082 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1397574 1397575 1397576 1397577 1397578 1397579 1397580 1397581 1397582 1397583 [...] (370804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (338393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (306412 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (275625 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (245880 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (218449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1510902 1510903 1510904 1510905 1510906 1510907 1510908 1510909 1510910 1510911 [...] (190048 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1518174 1518175 1518176 1518177 1518178 1518179 1518180 1518181 1518182 1518183 [...] (161405 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1518174 1518175 1518176 1518177 1518178 1518179 1518180 1518181 1518182 1518183 [...] (134349 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1518174 1518175 1518176 1518177 1518178 1518179 1518180 1518181 1518182 1518183 [...] (108143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1918800 1918801 1918802 1918803 1918804 1918805 1918806 1918807 1918808 1918809 [...] (82546 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1979172 1979173 1979174 1979175 1979176 1979177 1979178 1979179 1979180 1979181 [...] (58321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2069190 2069191 2069192 2069193 2069194 2069195 2069196 2069197 2069198 2069199 [...] (36917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2069190 2069191 2069192 2069193 2069194 2069195 2069196 2069197 2069198 2069199 [...] (19219 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2293578 2293579 2293580 2293581 2293582 2293583 2293584 2293585 2293586 2293587 [...] (6909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2902212 2902213 2902214 2902215 2902216 2902217 2902218 2902219 2902220 2902221 [...] (896 flits)
Measured flits: (0 flits)
Time taken is 117665 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11232.2 (1 samples)
	minimum = 25 (1 samples)
	maximum = 83031 (1 samples)
Network latency average = 9788.83 (1 samples)
	minimum = 23 (1 samples)
	maximum = 71746 (1 samples)
Flit latency average = 16760.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 91780 (1 samples)
Fragmentation average = 304.229 (1 samples)
	minimum = 0 (1 samples)
	maximum = 12098 (1 samples)
Injected packet rate average = 0.0248437 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0401429 (1 samples)
Accepted packet rate average = 0.0129129 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0178571 (1 samples)
Injected flit rate average = 0.44713 (1 samples)
	minimum = 0.167143 (1 samples)
	maximum = 0.721571 (1 samples)
Accepted flit rate average = 0.232499 (1 samples)
	minimum = 0.174429 (1 samples)
	maximum = 0.317286 (1 samples)
Injected packet size average = 17.9977 (1 samples)
Accepted packet size average = 18.0051 (1 samples)
Hops average = 5.06512 (1 samples)
Total run time 444.984
