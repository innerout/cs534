BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 256.65
	minimum = 22
	maximum = 806
Network latency average = 250.065
	minimum = 22
	maximum = 760
Slowest packet = 1143
Flit latency average = 218.791
	minimum = 5
	maximum = 743
Slowest flit = 20591
Fragmentation average = 44.0753
	minimum = 0
	maximum = 474
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0139115
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.260104
	minimum = 0.126 (at node 174)
	maximum = 0.443 (at node 131)
Injected packet length average = 17.8601
Accepted packet length average = 18.6971
Total in-flight flits = 42670 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.877
	minimum = 22
	maximum = 1353
Network latency average = 457.415
	minimum = 22
	maximum = 1340
Slowest packet = 1918
Flit latency average = 425.167
	minimum = 5
	maximum = 1323
Slowest flit = 59453
Fragmentation average = 52.6204
	minimum = 0
	maximum = 652
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0145781
	minimum = 0.0085 (at node 30)
	maximum = 0.0205 (at node 14)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.268758
	minimum = 0.162 (at node 83)
	maximum = 0.38 (at node 14)
Injected packet length average = 17.9276
Accepted packet length average = 18.4357
Total in-flight flits = 81225 (0 measured)
latency change    = 0.447918
throughput change = 0.0321987
Class 0:
Packet latency average = 1042.48
	minimum = 22
	maximum = 2051
Network latency average = 1034.58
	minimum = 22
	maximum = 2029
Slowest packet = 4514
Flit latency average = 1003.78
	minimum = 5
	maximum = 2012
Slowest flit = 81269
Fragmentation average = 72.017
	minimum = 0
	maximum = 755
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0156094
	minimum = 0.006 (at node 154)
	maximum = 0.031 (at node 16)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.282208
	minimum = 0.132 (at node 146)
	maximum = 0.553 (at node 16)
Injected packet length average = 17.9863
Accepted packet length average = 18.0794
Total in-flight flits = 119327 (0 measured)
latency change    = 0.554067
throughput change = 0.0476617
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 42.8067
	minimum = 22
	maximum = 114
Network latency average = 34.4177
	minimum = 22
	maximum = 60
Slowest packet = 15419
Flit latency average = 1390.17
	minimum = 5
	maximum = 2526
Slowest flit = 115595
Fragmentation average = 6.08115
	minimum = 0
	maximum = 26
Injected packet rate average = 0.0271719
	minimum = 0.013 (at node 159)
	maximum = 0.046 (at node 26)
Accepted packet rate average = 0.0159896
	minimum = 0.007 (at node 4)
	maximum = 0.029 (at node 90)
Injected flit rate average = 0.488312
	minimum = 0.25 (at node 159)
	maximum = 0.822 (at node 26)
Accepted flit rate average= 0.287682
	minimum = 0.129 (at node 4)
	maximum = 0.553 (at node 123)
Injected packet length average = 17.9712
Accepted packet length average = 17.9919
Total in-flight flits = 157998 (86240 measured)
latency change    = 23.3532
throughput change = 0.0190278
Class 0:
Packet latency average = 127.307
	minimum = 22
	maximum = 1980
Network latency average = 119.19
	minimum = 22
	maximum = 1980
Slowest packet = 15377
Flit latency average = 1576.33
	minimum = 5
	maximum = 2993
Slowest flit = 184002
Fragmentation average = 8.85861
	minimum = 0
	maximum = 219
Injected packet rate average = 0.02675
	minimum = 0.0185 (at node 28)
	maximum = 0.038 (at node 87)
Accepted packet rate average = 0.0158568
	minimum = 0.008 (at node 79)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.481448
	minimum = 0.333 (at node 28)
	maximum = 0.692 (at node 87)
Accepted flit rate average= 0.284875
	minimum = 0.1525 (at node 79)
	maximum = 0.424 (at node 129)
Injected packet length average = 17.9981
Accepted packet length average = 17.9655
Total in-flight flits = 194831 (168839 measured)
latency change    = 0.663752
throughput change = 0.00985447
Class 0:
Packet latency average = 1233.77
	minimum = 22
	maximum = 2968
Network latency average = 1225.89
	minimum = 22
	maximum = 2965
Slowest packet = 15476
Flit latency average = 1765.51
	minimum = 5
	maximum = 3826
Slowest flit = 198730
Fragmentation average = 39.6082
	minimum = 0
	maximum = 572
Injected packet rate average = 0.0267986
	minimum = 0.0193333 (at node 28)
	maximum = 0.034 (at node 26)
Accepted packet rate average = 0.015901
	minimum = 0.011 (at node 36)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.48247
	minimum = 0.348 (at node 28)
	maximum = 0.611667 (at node 26)
Accepted flit rate average= 0.285979
	minimum = 0.2 (at node 36)
	maximum = 0.384667 (at node 129)
Injected packet length average = 18.0036
Accepted packet length average = 17.9849
Total in-flight flits = 232451 (229984 measured)
latency change    = 0.896815
throughput change = 0.003861
Class 0:
Packet latency average = 1922.98
	minimum = 22
	maximum = 3945
Network latency average = 1915
	minimum = 22
	maximum = 3940
Slowest packet = 15462
Flit latency average = 1957.11
	minimum = 5
	maximum = 4624
Slowest flit = 217086
Fragmentation average = 58.7259
	minimum = 0
	maximum = 725
Injected packet rate average = 0.0268294
	minimum = 0.02025 (at node 28)
	maximum = 0.03325 (at node 46)
Accepted packet rate average = 0.0158529
	minimum = 0.01175 (at node 71)
	maximum = 0.0205 (at node 129)
Injected flit rate average = 0.482862
	minimum = 0.3645 (at node 28)
	maximum = 0.60125 (at node 46)
Accepted flit rate average= 0.285561
	minimum = 0.208 (at node 71)
	maximum = 0.36575 (at node 129)
Injected packet length average = 17.9975
Accepted packet length average = 18.0132
Total in-flight flits = 270906 (270612 measured)
latency change    = 0.358407
throughput change = 0.00146367
Class 0:
Packet latency average = 2276.57
	minimum = 22
	maximum = 4786
Network latency average = 2268.38
	minimum = 22
	maximum = 4784
Slowest packet = 16470
Flit latency average = 2140.06
	minimum = 5
	maximum = 5336
Slowest flit = 244132
Fragmentation average = 66.1991
	minimum = 0
	maximum = 744
Injected packet rate average = 0.026875
	minimum = 0.0206 (at node 52)
	maximum = 0.0334 (at node 46)
Accepted packet rate average = 0.0159156
	minimum = 0.0122 (at node 52)
	maximum = 0.0192 (at node 103)
Injected flit rate average = 0.483729
	minimum = 0.372 (at node 52)
	maximum = 0.6034 (at node 46)
Accepted flit rate average= 0.28647
	minimum = 0.221 (at node 89)
	maximum = 0.3548 (at node 138)
Injected packet length average = 17.9992
Accepted packet length average = 17.9993
Total in-flight flits = 308752 (308697 measured)
latency change    = 0.155318
throughput change = 0.00317169
Class 0:
Packet latency average = 2549.27
	minimum = 22
	maximum = 5903
Network latency average = 2541.05
	minimum = 22
	maximum = 5899
Slowest packet = 15839
Flit latency average = 2328.1
	minimum = 5
	maximum = 6288
Slowest flit = 249578
Fragmentation average = 71.9489
	minimum = 0
	maximum = 938
Injected packet rate average = 0.0268229
	minimum = 0.0213333 (at node 164)
	maximum = 0.0318333 (at node 46)
Accepted packet rate average = 0.0158837
	minimum = 0.0123333 (at node 63)
	maximum = 0.0205 (at node 138)
Injected flit rate average = 0.482895
	minimum = 0.382333 (at node 164)
	maximum = 0.572 (at node 46)
Accepted flit rate average= 0.286123
	minimum = 0.220167 (at node 63)
	maximum = 0.371667 (at node 138)
Injected packet length average = 18.0031
Accepted packet length average = 18.0137
Total in-flight flits = 345931 (345922 measured)
latency change    = 0.106973
throughput change = 0.00121111
Class 0:
Packet latency average = 2792.88
	minimum = 22
	maximum = 6711
Network latency average = 2784.65
	minimum = 22
	maximum = 6711
Slowest packet = 16650
Flit latency average = 2518.06
	minimum = 5
	maximum = 6694
Slowest flit = 299717
Fragmentation average = 75.6713
	minimum = 0
	maximum = 1211
Injected packet rate average = 0.0267307
	minimum = 0.0215714 (at node 164)
	maximum = 0.0312857 (at node 46)
Accepted packet rate average = 0.0158921
	minimum = 0.0125714 (at node 63)
	maximum = 0.0194286 (at node 40)
Injected flit rate average = 0.481054
	minimum = 0.388286 (at node 164)
	maximum = 0.563 (at node 46)
Accepted flit rate average= 0.28605
	minimum = 0.224571 (at node 63)
	maximum = 0.349714 (at node 138)
Injected packet length average = 17.9964
Accepted packet length average = 17.9995
Total in-flight flits = 381616 (381616 measured)
latency change    = 0.087224
throughput change = 0.000256643
Draining all recorded packets ...
Class 0:
Remaining flits: 308178 308179 308180 308181 308182 308183 308184 308185 308186 308187 [...] (418752 flits)
Measured flits: 308178 308179 308180 308181 308182 308183 308184 308185 308186 308187 [...] (335675 flits)
Class 0:
Remaining flits: 397800 397801 397802 397803 397804 397805 397806 397807 397808 397809 [...] (449041 flits)
Measured flits: 397800 397801 397802 397803 397804 397805 397806 397807 397808 397809 [...] (288608 flits)
Class 0:
Remaining flits: 429822 429823 429824 429825 429826 429827 429828 429829 429830 429831 [...] (482233 flits)
Measured flits: 429822 429823 429824 429825 429826 429827 429828 429829 429830 429831 [...] (241353 flits)
Class 0:
Remaining flits: 429822 429823 429824 429825 429826 429827 429828 429829 429830 429831 [...] (511999 flits)
Measured flits: 429822 429823 429824 429825 429826 429827 429828 429829 429830 429831 [...] (193914 flits)
Class 0:
Remaining flits: 436338 436339 436340 436341 436342 436343 436344 436345 436346 436347 [...] (545335 flits)
Measured flits: 436338 436339 436340 436341 436342 436343 436344 436345 436346 436347 [...] (146934 flits)
Class 0:
Remaining flits: 436341 436342 436343 436344 436345 436346 436347 436348 436349 436350 [...] (572638 flits)
Measured flits: 436341 436342 436343 436344 436345 436346 436347 436348 436349 436350 [...] (100327 flits)
Class 0:
Remaining flits: 513918 513919 513920 513921 513922 513923 513924 513925 513926 513927 [...] (602163 flits)
Measured flits: 513918 513919 513920 513921 513922 513923 513924 513925 513926 513927 [...] (63516 flits)
Class 0:
Remaining flits: 555408 555409 555410 555411 555412 555413 555414 555415 555416 555417 [...] (629822 flits)
Measured flits: 555408 555409 555410 555411 555412 555413 555414 555415 555416 555417 [...] (39238 flits)
Class 0:
Remaining flits: 555408 555409 555410 555411 555412 555413 555414 555415 555416 555417 [...] (659546 flits)
Measured flits: 555408 555409 555410 555411 555412 555413 555414 555415 555416 555417 [...] (23939 flits)
Class 0:
Remaining flits: 561258 561259 561260 561261 561262 561263 561264 561265 561266 561267 [...] (688652 flits)
Measured flits: 561258 561259 561260 561261 561262 561263 561264 561265 561266 561267 [...] (13721 flits)
Class 0:
Remaining flits: 572945 572946 572947 572948 572949 572950 572951 572952 572953 572954 [...] (718606 flits)
Measured flits: 572945 572946 572947 572948 572949 572950 572951 572952 572953 572954 [...] (8073 flits)
Class 0:
Remaining flits: 638010 638011 638012 638013 638014 638015 638016 638017 638018 638019 [...] (746028 flits)
Measured flits: 638010 638011 638012 638013 638014 638015 638016 638017 638018 638019 [...] (4595 flits)
Class 0:
Remaining flits: 639828 639829 639830 639831 639832 639833 639834 639835 639836 639837 [...] (770369 flits)
Measured flits: 639828 639829 639830 639831 639832 639833 639834 639835 639836 639837 [...] (2387 flits)
Class 0:
Remaining flits: 795679 795680 795681 795682 795683 795684 795685 795686 795687 795688 [...] (792479 flits)
Measured flits: 795679 795680 795681 795682 795683 795684 795685 795686 795687 795688 [...] (1041 flits)
Class 0:
Remaining flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (809310 flits)
Measured flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (419 flits)
Class 0:
Remaining flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (826090 flits)
Measured flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (255 flits)
Class 0:
Remaining flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (838149 flits)
Measured flits: 819666 819667 819668 819669 819670 819671 819672 819673 819674 819675 [...] (162 flits)
Class 0:
Remaining flits: 850104 850105 850106 850107 850108 850109 850110 850111 850112 850113 [...] (841282 flits)
Measured flits: 850104 850105 850106 850107 850108 850109 850110 850111 850112 850113 [...] (72 flits)
Class 0:
Remaining flits: 850119 850120 850121 895760 895761 895762 895763 895764 895765 895766 [...] (846912 flits)
Measured flits: 850119 850120 850121 895760 895761 895762 895763 895764 895765 895766 [...] (31 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1015848 1015849 1015850 1015851 1015852 1015853 1015854 1015855 1015856 1015857 [...] (805254 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1015848 1015849 1015850 1015851 1015852 1015853 1015854 1015855 1015856 1015857 [...] (755024 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073016 1073017 1073018 1073019 1073020 1073021 1073022 1073023 1073024 1073025 [...] (704450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (654117 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (603963 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (554162 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (505108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (456647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274868 1274869 1274870 1274871 1274872 1274873 1274874 1274875 1274876 1274877 [...] (408988 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274868 1274869 1274870 1274871 1274872 1274873 1274874 1274875 1274876 1274877 [...] (361575 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274868 1274869 1274870 1274871 1274872 1274873 1274874 1274875 1274876 1274877 [...] (314323 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424124 1424125 1424126 1424127 1424128 1424129 1424130 1424131 1424132 1424133 [...] (266965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424124 1424125 1424126 1424127 1424128 1424129 1424130 1424131 1424132 1424133 [...] (219396 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1488042 1488043 1488044 1488045 1488046 1488047 1488048 1488049 1488050 1488051 [...] (171871 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1623612 1623613 1623614 1623615 1623616 1623617 1658178 1658179 1658180 1658181 [...] (124426 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1658178 1658179 1658180 1658181 1658182 1658183 1658184 1658185 1658186 1658187 [...] (76960 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1738908 1738909 1738910 1738911 1738912 1738913 1738914 1738915 1738916 1738917 [...] (31888 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1986873 1986874 1986875 2000070 2000071 2000072 2000073 2000074 2000075 2000076 [...] (6166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2125710 2125711 2125712 2125713 2125714 2125715 2125716 2125717 2125718 2125719 [...] (355 flits)
Measured flits: (0 flits)
Time taken is 49164 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5085.55 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20064 (1 samples)
Network latency average = 5073.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20054 (1 samples)
Flit latency average = 9841.88 (1 samples)
	minimum = 5 (1 samples)
	maximum = 27775 (1 samples)
Fragmentation average = 93.9258 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1630 (1 samples)
Injected packet rate average = 0.0267307 (1 samples)
	minimum = 0.0215714 (1 samples)
	maximum = 0.0312857 (1 samples)
Accepted packet rate average = 0.0158921 (1 samples)
	minimum = 0.0125714 (1 samples)
	maximum = 0.0194286 (1 samples)
Injected flit rate average = 0.481054 (1 samples)
	minimum = 0.388286 (1 samples)
	maximum = 0.563 (1 samples)
Accepted flit rate average = 0.28605 (1 samples)
	minimum = 0.224571 (1 samples)
	maximum = 0.349714 (1 samples)
Injected packet size average = 17.9964 (1 samples)
Accepted packet size average = 17.9995 (1 samples)
Hops average = 5.07268 (1 samples)
Total run time 164.329
