BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 341.583
	minimum = 22
	maximum = 836
Network latency average = 314.946
	minimum = 22
	maximum = 795
Slowest packet = 491
Flit latency average = 292.115
	minimum = 5
	maximum = 799
Slowest flit = 29344
Fragmentation average = 25.4639
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0463333
	minimum = 0.032 (at node 92)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0152135
	minimum = 0.008 (at node 135)
	maximum = 0.023 (at node 2)
Injected flit rate average = 0.827083
	minimum = 0.576 (at node 92)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.280401
	minimum = 0.153 (at node 135)
	maximum = 0.429 (at node 76)
Injected packet length average = 17.8507
Accepted packet length average = 18.431
Total in-flight flits = 106525 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 684.51
	minimum = 22
	maximum = 1657
Network latency average = 640.917
	minimum = 22
	maximum = 1560
Slowest packet = 491
Flit latency average = 616.319
	minimum = 5
	maximum = 1543
Slowest flit = 69858
Fragmentation average = 26.9687
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0363177
	minimum = 0.0215 (at node 80)
	maximum = 0.048 (at node 155)
Accepted packet rate average = 0.0150495
	minimum = 0.008 (at node 153)
	maximum = 0.022 (at node 71)
Injected flit rate average = 0.652503
	minimum = 0.387 (at node 80)
	maximum = 0.864 (at node 155)
Accepted flit rate average= 0.273846
	minimum = 0.152 (at node 153)
	maximum = 0.396 (at node 71)
Injected packet length average = 17.9665
Accepted packet length average = 18.1964
Total in-flight flits = 148211 (0 measured)
latency change    = 0.500982
throughput change = 0.0239356
Class 0:
Packet latency average = 1756.83
	minimum = 67
	maximum = 2529
Network latency average = 1616.55
	minimum = 22
	maximum = 2335
Slowest packet = 3176
Flit latency average = 1600.81
	minimum = 5
	maximum = 2377
Slowest flit = 98218
Fragmentation average = 21.4368
	minimum = 0
	maximum = 171
Injected packet rate average = 0.0154531
	minimum = 0.003 (at node 3)
	maximum = 0.036 (at node 126)
Accepted packet rate average = 0.0148802
	minimum = 0.007 (at node 71)
	maximum = 0.026 (at node 34)
Injected flit rate average = 0.277896
	minimum = 0.065 (at node 3)
	maximum = 0.648 (at node 126)
Accepted flit rate average= 0.267557
	minimum = 0.126 (at node 71)
	maximum = 0.454 (at node 34)
Injected packet length average = 17.9831
Accepted packet length average = 17.9807
Total in-flight flits = 150192 (0 measured)
latency change    = 0.610372
throughput change = 0.0235055
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1932.79
	minimum = 814
	maximum = 2692
Network latency average = 73.0388
	minimum = 22
	maximum = 971
Slowest packet = 17067
Flit latency average = 2136.46
	minimum = 5
	maximum = 3142
Slowest flit = 136284
Fragmentation average = 5.25243
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0149688
	minimum = 0.003 (at node 129)
	maximum = 0.034 (at node 17)
Accepted packet rate average = 0.0148281
	minimum = 0.006 (at node 20)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.269135
	minimum = 0.054 (at node 129)
	maximum = 0.612 (at node 17)
Accepted flit rate average= 0.266583
	minimum = 0.111 (at node 20)
	maximum = 0.459 (at node 16)
Injected packet length average = 17.9798
Accepted packet length average = 17.9782
Total in-flight flits = 150722 (49820 measured)
latency change    = 0.0910372
throughput change = 0.00365349
Class 0:
Packet latency average = 2688
	minimum = 814
	maximum = 3994
Network latency average = 695.933
	minimum = 22
	maximum = 1961
Slowest packet = 17067
Flit latency average = 2373.31
	minimum = 5
	maximum = 3963
Slowest flit = 157085
Fragmentation average = 10.1
	minimum = 0
	maximum = 63
Injected packet rate average = 0.0147057
	minimum = 0.005 (at node 186)
	maximum = 0.0285 (at node 46)
Accepted packet rate average = 0.0147865
	minimum = 0.0075 (at node 36)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.264768
	minimum = 0.093 (at node 186)
	maximum = 0.5095 (at node 46)
Accepted flit rate average= 0.265536
	minimum = 0.1345 (at node 36)
	maximum = 0.388 (at node 129)
Injected packet length average = 18.0044
Accepted packet length average = 17.9581
Total in-flight flits = 149908 (94466 measured)
latency change    = 0.280957
throughput change = 0.00394249
Class 0:
Packet latency average = 3498.28
	minimum = 814
	maximum = 4844
Network latency average = 1383.54
	minimum = 22
	maximum = 2976
Slowest packet = 17067
Flit latency average = 2512.81
	minimum = 5
	maximum = 4868
Slowest flit = 178288
Fragmentation average = 14.4219
	minimum = 0
	maximum = 84
Injected packet rate average = 0.0149705
	minimum = 0.00666667 (at node 191)
	maximum = 0.0276667 (at node 189)
Accepted packet rate average = 0.0147743
	minimum = 0.00933333 (at node 36)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.269113
	minimum = 0.12 (at node 191)
	maximum = 0.500667 (at node 189)
Accepted flit rate average= 0.26572
	minimum = 0.164667 (at node 36)
	maximum = 0.388333 (at node 128)
Injected packet length average = 17.9762
Accepted packet length average = 17.9853
Total in-flight flits = 152099 (129800 measured)
latency change    = 0.231623
throughput change = 0.000692561
Draining remaining packets ...
Class 0:
Remaining flits: 200682 200683 200684 200685 200686 200687 200688 200689 200690 200691 [...] (102312 flits)
Measured flits: 306792 306793 306794 306795 306796 306797 306798 306799 306800 306801 [...] (97870 flits)
Class 0:
Remaining flits: 264960 264961 264962 264963 264964 264965 264966 264967 264968 264969 [...] (53691 flits)
Measured flits: 307026 307027 307028 307029 307030 307031 307032 307033 307034 307035 [...] (53222 flits)
Class 0:
Remaining flits: 335610 335611 335612 335613 335614 335615 335616 335617 335618 335619 [...] (8183 flits)
Measured flits: 335610 335611 335612 335613 335614 335615 335616 335617 335618 335619 [...] (8183 flits)
Class 0:
Remaining flits: 375786 375787 375788 375789 375790 375791 375792 375793 375794 375795 [...] (72 flits)
Measured flits: 375786 375787 375788 375789 375790 375791 375792 375793 375794 375795 [...] (72 flits)
Time taken is 10101 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5210.51 (1 samples)
	minimum = 814 (1 samples)
	maximum = 7842 (1 samples)
Network latency average = 2780.83 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5952 (1 samples)
Flit latency average = 2841.96 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6166 (1 samples)
Fragmentation average = 17.7333 (1 samples)
	minimum = 0 (1 samples)
	maximum = 182 (1 samples)
Injected packet rate average = 0.0149705 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0147743 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.269113 (1 samples)
	minimum = 0.12 (1 samples)
	maximum = 0.500667 (1 samples)
Accepted flit rate average = 0.26572 (1 samples)
	minimum = 0.164667 (1 samples)
	maximum = 0.388333 (1 samples)
Injected packet size average = 17.9762 (1 samples)
Accepted packet size average = 17.9853 (1 samples)
Hops average = 5.0834 (1 samples)
Total run time 8.62131
