BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 287.546
	minimum = 23
	maximum = 921
Network latency average = 280.349
	minimum = 23
	maximum = 912
Slowest packet = 290
Flit latency average = 248.208
	minimum = 6
	maximum = 941
Slowest flit = 4345
Fragmentation average = 55.3644
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0101198
	minimum = 0.003 (at node 64)
	maximum = 0.018 (at node 124)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.190859
	minimum = 0.065 (at node 64)
	maximum = 0.324 (at node 124)
Injected packet length average = 17.8589
Accepted packet length average = 18.86
Total in-flight flits = 63003 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 543.741
	minimum = 23
	maximum = 1854
Network latency average = 535.502
	minimum = 23
	maximum = 1834
Slowest packet = 840
Flit latency average = 500.944
	minimum = 6
	maximum = 1843
Slowest flit = 14151
Fragmentation average = 60.0771
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0105104
	minimum = 0.0055 (at node 174)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.192935
	minimum = 0.105 (at node 174)
	maximum = 0.3075 (at node 22)
Injected packet length average = 17.9228
Accepted packet length average = 18.3565
Total in-flight flits = 124975 (0 measured)
latency change    = 0.471171
throughput change = 0.0107576
Class 0:
Packet latency average = 1307.86
	minimum = 23
	maximum = 2620
Network latency average = 1298.62
	minimum = 23
	maximum = 2605
Slowest packet = 2008
Flit latency average = 1273.9
	minimum = 6
	maximum = 2614
Slowest flit = 37352
Fragmentation average = 61.6853
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0287344
	minimum = 0.017 (at node 53)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0107917
	minimum = 0.004 (at node 121)
	maximum = 0.021 (at node 63)
Injected flit rate average = 0.516859
	minimum = 0.306 (at node 53)
	maximum = 0.827 (at node 69)
Accepted flit rate average= 0.195458
	minimum = 0.079 (at node 111)
	maximum = 0.378 (at node 63)
Injected packet length average = 17.9875
Accepted packet length average = 18.112
Total in-flight flits = 186753 (0 measured)
latency change    = 0.584252
throughput change = 0.0129104
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 76.8136
	minimum = 23
	maximum = 493
Network latency average = 61.6465
	minimum = 23
	maximum = 340
Slowest packet = 17279
Flit latency average = 1813.45
	minimum = 6
	maximum = 3573
Slowest flit = 40742
Fragmentation average = 10.0581
	minimum = 0
	maximum = 39
Injected packet rate average = 0.0288385
	minimum = 0.014 (at node 36)
	maximum = 0.05 (at node 86)
Accepted packet rate average = 0.0109583
	minimum = 0.005 (at node 2)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.51838
	minimum = 0.244 (at node 84)
	maximum = 0.888 (at node 86)
Accepted flit rate average= 0.196135
	minimum = 0.089 (at node 113)
	maximum = 0.351 (at node 16)
Injected packet length average = 17.9753
Accepted packet length average = 17.8983
Total in-flight flits = 248761 (92117 measured)
latency change    = 16.0264
throughput change = 0.00345212
Class 0:
Packet latency average = 109.297
	minimum = 23
	maximum = 1896
Network latency average = 84.3852
	minimum = 23
	maximum = 1883
Slowest packet = 16964
Flit latency average = 2126.49
	minimum = 6
	maximum = 4245
Slowest flit = 73811
Fragmentation average = 9.92737
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0278125
	minimum = 0.0125 (at node 84)
	maximum = 0.0425 (at node 86)
Accepted packet rate average = 0.010638
	minimum = 0.0055 (at node 96)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.500419
	minimum = 0.224 (at node 84)
	maximum = 0.7645 (at node 86)
Accepted flit rate average= 0.19119
	minimum = 0.1035 (at node 96)
	maximum = 0.296 (at node 16)
Injected packet length average = 17.9926
Accepted packet length average = 17.9723
Total in-flight flits = 305576 (178309 measured)
latency change    = 0.297204
throughput change = 0.0258659
Class 0:
Packet latency average = 241.631
	minimum = 23
	maximum = 2843
Network latency average = 192.426
	minimum = 23
	maximum = 2843
Slowest packet = 16881
Flit latency average = 2439.88
	minimum = 6
	maximum = 5245
Slowest flit = 71909
Fragmentation average = 12.2619
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0271458
	minimum = 0.013 (at node 84)
	maximum = 0.0373333 (at node 86)
Accepted packet rate average = 0.0104132
	minimum = 0.006 (at node 36)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.488587
	minimum = 0.232667 (at node 84)
	maximum = 0.672667 (at node 86)
Accepted flit rate average= 0.187177
	minimum = 0.103667 (at node 36)
	maximum = 0.281333 (at node 19)
Injected packet length average = 17.9986
Accepted packet length average = 17.975
Total in-flight flits = 360423 (261841 measured)
latency change    = 0.547671
throughput change = 0.0214397
Class 0:
Packet latency average = 627.137
	minimum = 23
	maximum = 3963
Network latency average = 552.3
	minimum = 23
	maximum = 3963
Slowest packet = 16728
Flit latency average = 2752.96
	minimum = 6
	maximum = 5853
Slowest flit = 111473
Fragmentation average = 18.5296
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0267734
	minimum = 0.01375 (at node 84)
	maximum = 0.0355 (at node 191)
Accepted packet rate average = 0.0102565
	minimum = 0.00625 (at node 4)
	maximum = 0.015 (at node 136)
Injected flit rate average = 0.481637
	minimum = 0.24375 (at node 84)
	maximum = 0.6405 (at node 191)
Accepted flit rate average= 0.184397
	minimum = 0.114 (at node 4)
	maximum = 0.26875 (at node 136)
Injected packet length average = 17.9893
Accepted packet length average = 17.9785
Total in-flight flits = 415252 (342803 measured)
latency change    = 0.614707
throughput change = 0.0150759
Class 0:
Packet latency average = 1255.52
	minimum = 23
	maximum = 5003
Network latency average = 1160.55
	minimum = 23
	maximum = 4967
Slowest packet = 16704
Flit latency average = 3069.82
	minimum = 6
	maximum = 7071
Slowest flit = 90646
Fragmentation average = 27.6544
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0266844
	minimum = 0.0136 (at node 84)
	maximum = 0.035 (at node 182)
Accepted packet rate average = 0.0101542
	minimum = 0.006 (at node 4)
	maximum = 0.0142 (at node 136)
Injected flit rate average = 0.480328
	minimum = 0.2426 (at node 84)
	maximum = 0.6296 (at node 31)
Accepted flit rate average= 0.182647
	minimum = 0.1108 (at node 4)
	maximum = 0.2558 (at node 136)
Injected packet length average = 18.0004
Accepted packet length average = 17.9874
Total in-flight flits = 472536 (422802 measured)
latency change    = 0.500497
throughput change = 0.00958276
Class 0:
Packet latency average = 2024.53
	minimum = 23
	maximum = 5959
Network latency average = 1906.71
	minimum = 23
	maximum = 5959
Slowest packet = 16730
Flit latency average = 3378.27
	minimum = 6
	maximum = 7563
Slowest flit = 129347
Fragmentation average = 35.6114
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0264349
	minimum = 0.0135 (at node 84)
	maximum = 0.0351667 (at node 182)
Accepted packet rate average = 0.0101424
	minimum = 0.00666667 (at node 4)
	maximum = 0.0136667 (at node 136)
Injected flit rate average = 0.475657
	minimum = 0.242833 (at node 84)
	maximum = 0.630833 (at node 182)
Accepted flit rate average= 0.182324
	minimum = 0.120667 (at node 4)
	maximum = 0.246667 (at node 136)
Injected packet length average = 17.9935
Accepted packet length average = 17.9765
Total in-flight flits = 524888 (493570 measured)
latency change    = 0.379845
throughput change = 0.00177207
Class 0:
Packet latency average = 2828.05
	minimum = 23
	maximum = 6946
Network latency average = 2704.11
	minimum = 23
	maximum = 6946
Slowest packet = 16599
Flit latency average = 3701.36
	minimum = 6
	maximum = 8487
Slowest flit = 128339
Fragmentation average = 42.5483
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0263185
	minimum = 0.0138571 (at node 84)
	maximum = 0.0345714 (at node 9)
Accepted packet rate average = 0.0101146
	minimum = 0.00671429 (at node 4)
	maximum = 0.0128571 (at node 66)
Injected flit rate average = 0.473695
	minimum = 0.248714 (at node 84)
	maximum = 0.621286 (at node 9)
Accepted flit rate average= 0.181959
	minimum = 0.120857 (at node 4)
	maximum = 0.230714 (at node 136)
Injected packet length average = 17.9986
Accepted packet length average = 17.9898
Total in-flight flits = 578914 (561359 measured)
latency change    = 0.284126
throughput change = 0.00200434
Draining all recorded packets ...
Class 0:
Remaining flits: 153558 153559 153560 153561 153562 153563 153564 153565 153566 153567 [...] (633639 flits)
Measured flits: 298566 298567 298568 298569 298570 298571 298572 298573 298574 298575 [...] (549853 flits)
Class 0:
Remaining flits: 181447 181448 181449 181450 181451 181452 181453 181454 181455 181456 [...] (688976 flits)
Measured flits: 298566 298567 298568 298569 298570 298571 298572 298573 298574 298575 [...] (534950 flits)
Class 0:
Remaining flits: 204066 204067 204068 204069 204070 204071 204072 204073 204074 204075 [...] (741534 flits)
Measured flits: 298566 298567 298568 298569 298570 298571 298572 298573 298574 298575 [...] (517209 flits)
Class 0:
Remaining flits: 211536 211537 211538 211539 211540 211541 211542 211543 211544 211545 [...] (796237 flits)
Measured flits: 298908 298909 298910 298911 298912 298913 298914 298915 298916 298917 [...] (497289 flits)
Class 0:
Remaining flits: 237168 237169 237170 237171 237172 237173 237174 237175 237176 237177 [...] (846256 flits)
Measured flits: 299502 299503 299504 299505 299506 299507 299508 299509 299510 299511 [...] (475200 flits)
Class 0:
Remaining flits: 261216 261217 261218 261219 261220 261221 261222 261223 261224 261225 [...] (887020 flits)
Measured flits: 299502 299503 299504 299505 299506 299507 299508 299509 299510 299511 [...] (449478 flits)
Class 0:
Remaining flits: 261216 261217 261218 261219 261220 261221 261222 261223 261224 261225 [...] (918084 flits)
Measured flits: 303521 303522 303523 303524 303525 303526 303527 303528 303529 303530 [...] (421677 flits)
Class 0:
Remaining flits: 261216 261217 261218 261219 261220 261221 261222 261223 261224 261225 [...] (941148 flits)
Measured flits: 311508 311509 311510 311511 311512 311513 311514 311515 311516 311517 [...] (392333 flits)
Class 0:
Remaining flits: 282204 282205 282206 282207 282208 282209 282210 282211 282212 282213 [...] (957405 flits)
Measured flits: 326106 326107 326108 326109 326110 326111 326112 326113 326114 326115 [...] (362986 flits)
Class 0:
Remaining flits: 288864 288865 288866 288867 288868 288869 288870 288871 288872 288873 [...] (971023 flits)
Measured flits: 346338 346339 346340 346341 346342 346343 346344 346345 346346 346347 [...] (334286 flits)
Class 0:
Remaining flits: 346338 346339 346340 346341 346342 346343 346344 346345 346346 346347 [...] (979437 flits)
Measured flits: 346338 346339 346340 346341 346342 346343 346344 346345 346346 346347 [...] (304883 flits)
Class 0:
Remaining flits: 375174 375175 375176 375177 375178 375179 375180 375181 375182 375183 [...] (985536 flits)
Measured flits: 375174 375175 375176 375177 375178 375179 375180 375181 375182 375183 [...] (275359 flits)
Class 0:
Remaining flits: 395172 395173 395174 395175 395176 395177 395178 395179 395180 395181 [...] (988304 flits)
Measured flits: 395172 395173 395174 395175 395176 395177 395178 395179 395180 395181 [...] (246247 flits)
Class 0:
Remaining flits: 490212 490213 490214 490215 490216 490217 490218 490219 490220 490221 [...] (984056 flits)
Measured flits: 490212 490213 490214 490215 490216 490217 490218 490219 490220 490221 [...] (217516 flits)
Class 0:
Remaining flits: 492804 492805 492806 492807 492808 492809 492810 492811 492812 492813 [...] (979678 flits)
Measured flits: 492804 492805 492806 492807 492808 492809 492810 492811 492812 492813 [...] (189980 flits)
Class 0:
Remaining flits: 520596 520597 520598 520599 520600 520601 520602 520603 520604 520605 [...] (977347 flits)
Measured flits: 520596 520597 520598 520599 520600 520601 520602 520603 520604 520605 [...] (163557 flits)
Class 0:
Remaining flits: 555372 555373 555374 555375 555376 555377 555378 555379 555380 555381 [...] (972815 flits)
Measured flits: 555372 555373 555374 555375 555376 555377 555378 555379 555380 555381 [...] (138187 flits)
Class 0:
Remaining flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (970718 flits)
Measured flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (114857 flits)
Class 0:
Remaining flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (968386 flits)
Measured flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (93396 flits)
Class 0:
Remaining flits: 570652 570653 596430 596431 596432 596433 596434 596435 596436 596437 [...] (964772 flits)
Measured flits: 570652 570653 596430 596431 596432 596433 596434 596435 596436 596437 [...] (73442 flits)
Class 0:
Remaining flits: 613367 614826 614827 614828 614829 614830 614831 614832 614833 614834 [...] (961467 flits)
Measured flits: 613367 614826 614827 614828 614829 614830 614831 614832 614833 614834 [...] (56919 flits)
Class 0:
Remaining flits: 614826 614827 614828 614829 614830 614831 614832 614833 614834 614835 [...] (957953 flits)
Measured flits: 614826 614827 614828 614829 614830 614831 614832 614833 614834 614835 [...] (42826 flits)
Class 0:
Remaining flits: 614826 614827 614828 614829 614830 614831 614832 614833 614834 614835 [...] (951744 flits)
Measured flits: 614826 614827 614828 614829 614830 614831 614832 614833 614834 614835 [...] (31480 flits)
Class 0:
Remaining flits: 663343 663344 663345 663346 663347 663348 663349 663350 663351 663352 [...] (948293 flits)
Measured flits: 663343 663344 663345 663346 663347 663348 663349 663350 663351 663352 [...] (22285 flits)
Class 0:
Remaining flits: 678510 678511 678512 678513 678514 678515 678516 678517 678518 678519 [...] (945487 flits)
Measured flits: 678510 678511 678512 678513 678514 678515 678516 678517 678518 678519 [...] (15759 flits)
Class 0:
Remaining flits: 726138 726139 726140 726141 726142 726143 726144 726145 726146 726147 [...] (941664 flits)
Measured flits: 726138 726139 726140 726141 726142 726143 726144 726145 726146 726147 [...] (10933 flits)
Class 0:
Remaining flits: 726138 726139 726140 726141 726142 726143 726144 726145 726146 726147 [...] (940564 flits)
Measured flits: 726138 726139 726140 726141 726142 726143 726144 726145 726146 726147 [...] (7082 flits)
Class 0:
Remaining flits: 745308 745309 745310 745311 745312 745313 745314 745315 745316 745317 [...] (938459 flits)
Measured flits: 745308 745309 745310 745311 745312 745313 745314 745315 745316 745317 [...] (4458 flits)
Class 0:
Remaining flits: 745308 745309 745310 745311 745312 745313 745314 745315 745316 745317 [...] (937169 flits)
Measured flits: 745308 745309 745310 745311 745312 745313 745314 745315 745316 745317 [...] (2639 flits)
Class 0:
Remaining flits: 771948 771949 771950 771951 771952 771953 771954 771955 771956 771957 [...] (934433 flits)
Measured flits: 771948 771949 771950 771951 771952 771953 771954 771955 771956 771957 [...] (1750 flits)
Class 0:
Remaining flits: 798390 798391 798392 798393 798394 798395 798396 798397 798398 798399 [...] (933697 flits)
Measured flits: 798390 798391 798392 798393 798394 798395 798396 798397 798398 798399 [...] (900 flits)
Class 0:
Remaining flits: 813294 813295 813296 813297 813298 813299 813300 813301 813302 813303 [...] (930528 flits)
Measured flits: 813294 813295 813296 813297 813298 813299 813300 813301 813302 813303 [...] (506 flits)
Class 0:
Remaining flits: 844884 844885 844886 844887 844888 844889 844890 844891 844892 844893 [...] (929171 flits)
Measured flits: 844884 844885 844886 844887 844888 844889 844890 844891 844892 844893 [...] (270 flits)
Class 0:
Remaining flits: 867876 867877 867878 867879 867880 867881 867882 867883 867884 867885 [...] (926095 flits)
Measured flits: 867876 867877 867878 867879 867880 867881 867882 867883 867884 867885 [...] (138 flits)
Class 0:
Remaining flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (921450 flits)
Measured flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (90 flits)
Class 0:
Remaining flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (916391 flits)
Measured flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (90 flits)
Class 0:
Remaining flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (918059 flits)
Measured flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (72 flits)
Class 0:
Remaining flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (916158 flits)
Measured flits: 905562 905563 905564 905565 905566 905567 905568 905569 905570 905571 [...] (54 flits)
Class 0:
Remaining flits: 940338 940339 940340 940341 940342 940343 940344 940345 940346 940347 [...] (916400 flits)
Measured flits: 1414062 1414063 1414064 1414065 1414066 1414067 1414068 1414069 1414070 1414071 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 940353 940354 940355 993024 993025 993026 993027 993028 993029 993030 [...] (883488 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 993024 993025 993026 993027 993028 993029 993030 993031 993032 993033 [...] (852967 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1087470 1087471 1087472 1087473 1087474 1087475 1087476 1087477 1087478 1087479 [...] (821911 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091754 1091755 1091756 1091757 1091758 1091759 1091760 1091761 1091762 1091763 [...] (790856 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1105416 1105417 1105418 1105419 1105420 1105421 1105422 1105423 1105424 1105425 [...] (760692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1105416 1105417 1105418 1105419 1105420 1105421 1105422 1105423 1105424 1105425 [...] (729330 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1182888 1182889 1182890 1182891 1182892 1182893 1182894 1182895 1182896 1182897 [...] (698062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1182888 1182889 1182890 1182891 1182892 1182893 1182894 1182895 1182896 1182897 [...] (666609 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1190070 1190071 1190072 1190073 1190074 1190075 1190076 1190077 1190078 1190079 [...] (635544 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1190070 1190071 1190072 1190073 1190074 1190075 1190076 1190077 1190078 1190079 [...] (604239 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1190070 1190071 1190072 1190073 1190074 1190075 1190076 1190077 1190078 1190079 [...] (572923 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1243188 1243189 1243190 1243191 1243192 1243193 1243194 1243195 1243196 1243197 [...] (541800 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274436 1274437 1274438 1274439 1274440 1274441 1274442 1274443 1274444 1274445 [...] (510036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1276146 1276147 1276148 1276149 1276150 1276151 1276152 1276153 1276154 1276155 [...] (478388 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1276146 1276147 1276148 1276149 1276150 1276151 1276152 1276153 1276154 1276155 [...] (447344 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1289934 1289935 1289936 1289937 1289938 1289939 1289940 1289941 1289942 1289943 [...] (417483 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337418 1337419 1337420 1337421 1337422 1337423 1337424 1337425 1337426 1337427 [...] (387063 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1405224 1405225 1405226 1405227 1405228 1405229 1405230 1405231 1405232 1405233 [...] (357303 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1411416 1411417 1411418 1411419 1411420 1411421 1411422 1411423 1411424 1411425 [...] (327244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1411416 1411417 1411418 1411419 1411420 1411421 1411422 1411423 1411424 1411425 [...] (297321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1450656 1450657 1450658 1450659 1450660 1450661 1450662 1450663 1450664 1450665 [...] (268524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1450656 1450657 1450658 1450659 1450660 1450661 1450662 1450663 1450664 1450665 [...] (239597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1541250 1541251 1541252 1541253 1541254 1541255 1541256 1541257 1541258 1541259 [...] (211097 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1552446 1552447 1552448 1552449 1552450 1552451 1552452 1552453 1552454 1552455 [...] (182244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1604502 1604503 1604504 1604505 1604506 1604507 1604508 1604509 1604510 1604511 [...] (153639 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651212 1651213 1651214 1651215 1651216 1651217 1651218 1651219 1651220 1651221 [...] (126685 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1669932 1669933 1669934 1669935 1669936 1669937 1669938 1669939 1669940 1669941 [...] (99585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1681020 1681021 1681022 1681023 1681024 1681025 1681026 1681027 1681028 1681029 [...] (72442 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1776924 1776925 1776926 1776927 1776928 1776929 1776930 1776931 1776932 1776933 [...] (45467 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1853658 1853659 1853660 1853661 1853662 1853663 1853664 1853665 1853666 1853667 [...] (22588 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2038194 2038195 2038196 2038197 2038198 2038199 2038200 2038201 2038202 2038203 [...] (6781 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2406762 2406763 2406764 2406765 2406766 2406767 2406768 2406769 2406770 2406771 [...] (188 flits)
Measured flits: (0 flits)
Time taken is 81716 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13247.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 39713 (1 samples)
Network latency average = 12700.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 38875 (1 samples)
Flit latency average = 21609.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 57656 (1 samples)
Fragmentation average = 71.8948 (1 samples)
	minimum = 0 (1 samples)
	maximum = 168 (1 samples)
Injected packet rate average = 0.0263185 (1 samples)
	minimum = 0.0138571 (1 samples)
	maximum = 0.0345714 (1 samples)
Accepted packet rate average = 0.0101146 (1 samples)
	minimum = 0.00671429 (1 samples)
	maximum = 0.0128571 (1 samples)
Injected flit rate average = 0.473695 (1 samples)
	minimum = 0.248714 (1 samples)
	maximum = 0.621286 (1 samples)
Accepted flit rate average = 0.181959 (1 samples)
	minimum = 0.120857 (1 samples)
	maximum = 0.230714 (1 samples)
Injected packet size average = 17.9986 (1 samples)
Accepted packet size average = 17.9898 (1 samples)
Hops average = 5.06831 (1 samples)
Total run time 64.445
