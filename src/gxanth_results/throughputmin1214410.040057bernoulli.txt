BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 336.014
	minimum = 23
	maximum = 914
Network latency average = 321.08
	minimum = 23
	maximum = 890
Slowest packet = 472
Flit latency average = 290.433
	minimum = 6
	maximum = 930
Slowest flit = 7241
Fragmentation average = 54.9438
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0351042
	minimum = 0.017 (at node 125)
	maximum = 0.052 (at node 169)
Accepted packet rate average = 0.0101979
	minimum = 0.003 (at node 96)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.625271
	minimum = 0.306 (at node 125)
	maximum = 0.93 (at node 169)
Accepted flit rate average= 0.191198
	minimum = 0.054 (at node 96)
	maximum = 0.335 (at node 140)
Injected packet length average = 17.8119
Accepted packet length average = 18.7487
Total in-flight flits = 84736 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 680.363
	minimum = 23
	maximum = 1873
Network latency average = 641.17
	minimum = 23
	maximum = 1867
Slowest packet = 430
Flit latency average = 604.967
	minimum = 6
	maximum = 1850
Slowest flit = 7757
Fragmentation average = 61.4895
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0283958
	minimum = 0.0135 (at node 76)
	maximum = 0.0385 (at node 102)
Accepted packet rate average = 0.0100286
	minimum = 0.005 (at node 93)
	maximum = 0.015 (at node 152)
Injected flit rate average = 0.508375
	minimum = 0.243 (at node 76)
	maximum = 0.692 (at node 102)
Accepted flit rate average= 0.184365
	minimum = 0.099 (at node 135)
	maximum = 0.273 (at node 152)
Injected packet length average = 17.9032
Accepted packet length average = 18.3838
Total in-flight flits = 126718 (0 measured)
latency change    = 0.506126
throughput change = 0.0370642
Class 0:
Packet latency average = 1814.54
	minimum = 43
	maximum = 2753
Network latency average = 1660.19
	minimum = 27
	maximum = 2734
Slowest packet = 1326
Flit latency average = 1634.29
	minimum = 6
	maximum = 2814
Slowest flit = 22520
Fragmentation average = 70.8119
	minimum = 0
	maximum = 169
Injected packet rate average = 0.00985938
	minimum = 0 (at node 35)
	maximum = 0.028 (at node 98)
Accepted packet rate average = 0.00941667
	minimum = 0.003 (at node 20)
	maximum = 0.018 (at node 177)
Injected flit rate average = 0.180328
	minimum = 0 (at node 35)
	maximum = 0.51 (at node 98)
Accepted flit rate average= 0.168807
	minimum = 0.057 (at node 65)
	maximum = 0.307 (at node 177)
Injected packet length average = 18.29
Accepted packet length average = 17.9264
Total in-flight flits = 129497 (0 measured)
latency change    = 0.625049
throughput change = 0.0921601
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1970.09
	minimum = 752
	maximum = 2883
Network latency average = 151.211
	minimum = 31
	maximum = 848
Slowest packet = 12971
Flit latency average = 2300.03
	minimum = 6
	maximum = 3585
Slowest flit = 52311
Fragmentation average = 21.3158
	minimum = 0
	maximum = 92
Injected packet rate average = 0.007875
	minimum = 0 (at node 3)
	maximum = 0.04 (at node 150)
Accepted packet rate average = 0.00893229
	minimum = 0.002 (at node 4)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.141859
	minimum = 0 (at node 3)
	maximum = 0.71 (at node 150)
Accepted flit rate average= 0.161255
	minimum = 0.036 (at node 4)
	maximum = 0.315 (at node 78)
Injected packet length average = 18.0139
Accepted packet length average = 18.0531
Total in-flight flits = 125698 (26068 measured)
latency change    = 0.0789568
throughput change = 0.0468331
Class 0:
Packet latency average = 2665.43
	minimum = 752
	maximum = 3848
Network latency average = 558.303
	minimum = 23
	maximum = 1883
Slowest packet = 12971
Flit latency average = 2612.35
	minimum = 6
	maximum = 4584
Slowest flit = 45431
Fragmentation average = 42.3487
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0082474
	minimum = 0 (at node 72)
	maximum = 0.0295 (at node 167)
Accepted packet rate average = 0.00904948
	minimum = 0.0035 (at node 4)
	maximum = 0.0165 (at node 151)
Injected flit rate average = 0.148003
	minimum = 0 (at node 72)
	maximum = 0.531 (at node 167)
Accepted flit rate average= 0.163117
	minimum = 0.0645 (at node 4)
	maximum = 0.291 (at node 151)
Injected packet length average = 17.9454
Accepted packet length average = 18.025
Total in-flight flits = 123686 (53127 measured)
latency change    = 0.260875
throughput change = 0.011415
Class 0:
Packet latency average = 3422.83
	minimum = 752
	maximum = 4975
Network latency average = 1103.26
	minimum = 23
	maximum = 2946
Slowest packet = 12971
Flit latency average = 2834.05
	minimum = 6
	maximum = 5523
Slowest flit = 40661
Fragmentation average = 55.5017
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00836285
	minimum = 0.001 (at node 78)
	maximum = 0.0256667 (at node 167)
Accepted packet rate average = 0.00905556
	minimum = 0.005 (at node 36)
	maximum = 0.0143333 (at node 16)
Injected flit rate average = 0.150297
	minimum = 0.018 (at node 78)
	maximum = 0.457333 (at node 167)
Accepted flit rate average= 0.162885
	minimum = 0.087 (at node 36)
	maximum = 0.257667 (at node 16)
Injected packet length average = 17.972
Accepted packet length average = 17.9873
Total in-flight flits = 122129 (75780 measured)
latency change    = 0.221278
throughput change = 0.00142291
Draining remaining packets ...
Class 0:
Remaining flits: 81180 81181 81182 81183 81184 81185 81186 81187 81188 81189 [...] (92014 flits)
Measured flits: 232704 232705 232706 232707 232708 232709 232710 232711 232712 232713 [...] (65184 flits)
Class 0:
Remaining flits: 81180 81181 81182 81183 81184 81185 81186 81187 81188 81189 [...] (61284 flits)
Measured flits: 232704 232705 232706 232707 232708 232709 232710 232711 232712 232713 [...] (47937 flits)
Class 0:
Remaining flits: 102564 102565 102566 102567 102568 102569 102570 102571 102572 102573 [...] (31946 flits)
Measured flits: 232938 232939 232940 232941 232942 232943 232944 232945 232946 232947 [...] (26921 flits)
Class 0:
Remaining flits: 164070 164071 164072 164073 164074 164075 164076 164077 164078 164079 [...] (7276 flits)
Measured flits: 233298 233299 233300 233301 233302 233303 233304 233305 233306 233307 [...] (6425 flits)
Class 0:
Remaining flits: 177408 177409 177410 177411 177412 177413 177414 177415 177416 177417 [...] (152 flits)
Measured flits: 282475 282476 282477 282478 282479 282480 282481 282482 282483 282484 [...] (90 flits)
Time taken is 11142 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5965.66 (1 samples)
	minimum = 752 (1 samples)
	maximum = 9144 (1 samples)
Network latency average = 3435.94 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7428 (1 samples)
Flit latency average = 3694.26 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9422 (1 samples)
Fragmentation average = 62.7029 (1 samples)
	minimum = 0 (1 samples)
	maximum = 157 (1 samples)
Injected packet rate average = 0.00836285 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.0256667 (1 samples)
Accepted packet rate average = 0.00905556 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0143333 (1 samples)
Injected flit rate average = 0.150297 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.457333 (1 samples)
Accepted flit rate average = 0.162885 (1 samples)
	minimum = 0.087 (1 samples)
	maximum = 0.257667 (1 samples)
Injected packet size average = 17.972 (1 samples)
Accepted packet size average = 17.9873 (1 samples)
Hops average = 5.08765 (1 samples)
Total run time 12.4432
