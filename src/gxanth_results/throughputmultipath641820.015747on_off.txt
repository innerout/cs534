BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 221.148
	minimum = 22
	maximum = 914
Network latency average = 155.9
	minimum = 22
	maximum = 708
Slowest packet = 64
Flit latency average = 125.6
	minimum = 5
	maximum = 691
Slowest flit = 11879
Fragmentation average = 32.6801
	minimum = 0
	maximum = 343
Injected packet rate average = 0.015974
	minimum = 0 (at node 10)
	maximum = 0.054 (at node 25)
Accepted packet rate average = 0.0117865
	minimum = 0.004 (at node 41)
	maximum = 0.019 (at node 48)
Injected flit rate average = 0.285146
	minimum = 0 (at node 10)
	maximum = 0.971 (at node 25)
Accepted flit rate average= 0.219844
	minimum = 0.09 (at node 115)
	maximum = 0.369 (at node 132)
Injected packet length average = 17.8507
Accepted packet length average = 18.6522
Total in-flight flits = 12996 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 317.395
	minimum = 22
	maximum = 1439
Network latency average = 238.206
	minimum = 22
	maximum = 1137
Slowest packet = 64
Flit latency average = 202.946
	minimum = 5
	maximum = 1120
Slowest flit = 46331
Fragmentation average = 41.3183
	minimum = 0
	maximum = 410
Injected packet rate average = 0.0157005
	minimum = 0.001 (at node 63)
	maximum = 0.0445 (at node 49)
Accepted packet rate average = 0.0127474
	minimum = 0.007 (at node 96)
	maximum = 0.0185 (at node 88)
Injected flit rate average = 0.28132
	minimum = 0.018 (at node 63)
	maximum = 0.8005 (at node 49)
Accepted flit rate average= 0.233979
	minimum = 0.132 (at node 116)
	maximum = 0.3405 (at node 78)
Injected packet length average = 17.9179
Accepted packet length average = 18.3551
Total in-flight flits = 18728 (0 measured)
latency change    = 0.30324
throughput change = 0.0604131
Class 0:
Packet latency average = 493.22
	minimum = 22
	maximum = 2130
Network latency average = 392.665
	minimum = 22
	maximum = 1670
Slowest packet = 3622
Flit latency average = 351.771
	minimum = 5
	maximum = 1653
Slowest flit = 65699
Fragmentation average = 52.5595
	minimum = 0
	maximum = 446
Injected packet rate average = 0.0165573
	minimum = 0 (at node 101)
	maximum = 0.048 (at node 30)
Accepted packet rate average = 0.0139635
	minimum = 0.007 (at node 31)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.297885
	minimum = 0 (at node 101)
	maximum = 0.874 (at node 91)
Accepted flit rate average= 0.252391
	minimum = 0.103 (at node 91)
	maximum = 0.506 (at node 16)
Injected packet length average = 17.9912
Accepted packet length average = 18.075
Total in-flight flits = 27509 (0 measured)
latency change    = 0.356484
throughput change = 0.0729483
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 386.455
	minimum = 22
	maximum = 1593
Network latency average = 292.302
	minimum = 22
	maximum = 961
Slowest packet = 9233
Flit latency average = 487.538
	minimum = 5
	maximum = 2289
Slowest flit = 83248
Fragmentation average = 44.214
	minimum = 0
	maximum = 330
Injected packet rate average = 0.0154531
	minimum = 0 (at node 14)
	maximum = 0.051 (at node 165)
Accepted packet rate average = 0.0142708
	minimum = 0.005 (at node 79)
	maximum = 0.025 (at node 182)
Injected flit rate average = 0.278271
	minimum = 0 (at node 14)
	maximum = 0.922 (at node 165)
Accepted flit rate average= 0.257609
	minimum = 0.109 (at node 1)
	maximum = 0.437 (at node 182)
Injected packet length average = 18.0074
Accepted packet length average = 18.0515
Total in-flight flits = 31454 (28107 measured)
latency change    = 0.276268
throughput change = 0.0202584
Class 0:
Packet latency average = 582.295
	minimum = 22
	maximum = 2544
Network latency average = 468.401
	minimum = 22
	maximum = 1876
Slowest packet = 9233
Flit latency average = 538.201
	minimum = 5
	maximum = 2500
Slowest flit = 135759
Fragmentation average = 54.6956
	minimum = 0
	maximum = 441
Injected packet rate average = 0.0157813
	minimum = 0.0015 (at node 21)
	maximum = 0.042 (at node 165)
Accepted packet rate average = 0.0143516
	minimum = 0.0075 (at node 86)
	maximum = 0.021 (at node 19)
Injected flit rate average = 0.28412
	minimum = 0.025 (at node 74)
	maximum = 0.757 (at node 165)
Accepted flit rate average= 0.258971
	minimum = 0.1305 (at node 35)
	maximum = 0.38 (at node 19)
Injected packet length average = 18.0036
Accepted packet length average = 18.0448
Total in-flight flits = 37234 (37045 measured)
latency change    = 0.336324
throughput change = 0.00525919
Class 0:
Packet latency average = 708.549
	minimum = 22
	maximum = 3119
Network latency average = 580.745
	minimum = 22
	maximum = 2677
Slowest packet = 9233
Flit latency average = 591.05
	minimum = 5
	maximum = 2725
Slowest flit = 164249
Fragmentation average = 60.0098
	minimum = 0
	maximum = 483
Injected packet rate average = 0.0155885
	minimum = 0.00166667 (at node 84)
	maximum = 0.0373333 (at node 165)
Accepted packet rate average = 0.0143785
	minimum = 0.00833333 (at node 35)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.280556
	minimum = 0.03 (at node 84)
	maximum = 0.671667 (at node 165)
Accepted flit rate average= 0.259203
	minimum = 0.149 (at node 35)
	maximum = 0.36 (at node 129)
Injected packet length average = 17.9975
Accepted packet length average = 18.0272
Total in-flight flits = 39812 (39812 measured)
latency change    = 0.178187
throughput change = 0.000894167
Draining remaining packets ...
Class 0:
Remaining flits: 212508 212509 212510 212511 212512 212513 212514 212515 212516 212517 [...] (5336 flits)
Measured flits: 212508 212509 212510 212511 212512 212513 212514 212515 212516 212517 [...] (5336 flits)
Time taken is 7806 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 880.631 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3979 (1 samples)
Network latency average = 726.864 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3225 (1 samples)
Flit latency average = 699.02 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3208 (1 samples)
Fragmentation average = 61.362 (1 samples)
	minimum = 0 (1 samples)
	maximum = 483 (1 samples)
Injected packet rate average = 0.0155885 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.0373333 (1 samples)
Accepted packet rate average = 0.0143785 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.280556 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.671667 (1 samples)
Accepted flit rate average = 0.259203 (1 samples)
	minimum = 0.149 (1 samples)
	maximum = 0.36 (1 samples)
Injected packet size average = 17.9975 (1 samples)
Accepted packet size average = 18.0272 (1 samples)
Hops average = 5.06516 (1 samples)
Total run time 7.17924
