BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 382.658
	minimum = 27
	maximum = 966
Network latency average = 356.036
	minimum = 27
	maximum = 943
Slowest packet = 235
Flit latency average = 260.353
	minimum = 6
	maximum = 950
Slowest flit = 3184
Fragmentation average = 251.917
	minimum = 0
	maximum = 850
Injected packet rate average = 0.0303437
	minimum = 0.016 (at node 160)
	maximum = 0.04 (at node 77)
Accepted packet rate average = 0.00976562
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 63)
Injected flit rate average = 0.540714
	minimum = 0.288 (at node 160)
	maximum = 0.718 (at node 183)
Accepted flit rate average= 0.221625
	minimum = 0.087 (at node 81)
	maximum = 0.366 (at node 76)
Injected packet length average = 17.8196
Accepted packet length average = 22.6944
Total in-flight flits = 62424 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 747.275
	minimum = 27
	maximum = 1910
Network latency average = 647.622
	minimum = 27
	maximum = 1910
Slowest packet = 76
Flit latency average = 515.956
	minimum = 6
	maximum = 1933
Slowest flit = 5821
Fragmentation average = 312.17
	minimum = 0
	maximum = 1449
Injected packet rate average = 0.021862
	minimum = 0.01 (at node 56)
	maximum = 0.0295 (at node 57)
Accepted packet rate average = 0.0111146
	minimum = 0.0055 (at node 120)
	maximum = 0.0175 (at node 44)
Injected flit rate average = 0.38957
	minimum = 0.1755 (at node 56)
	maximum = 0.5275 (at node 57)
Accepted flit rate average= 0.221708
	minimum = 0.139 (at node 120)
	maximum = 0.3295 (at node 131)
Injected packet length average = 17.8195
Accepted packet length average = 19.9475
Total in-flight flits = 66208 (0 measured)
latency change    = 0.487929
throughput change = 0.000375869
Class 0:
Packet latency average = 1676.86
	minimum = 451
	maximum = 2882
Network latency average = 1201.08
	minimum = 28
	maximum = 2882
Slowest packet = 694
Flit latency average = 1064.78
	minimum = 6
	maximum = 2865
Slowest flit = 12509
Fragmentation average = 325.986
	minimum = 3
	maximum = 2622
Injected packet rate average = 0.00925
	minimum = 0 (at node 1)
	maximum = 0.031 (at node 25)
Accepted packet rate average = 0.0123229
	minimum = 0.004 (at node 117)
	maximum = 0.02 (at node 10)
Injected flit rate average = 0.166818
	minimum = 0 (at node 1)
	maximum = 0.558 (at node 151)
Accepted flit rate average= 0.217401
	minimum = 0.077 (at node 138)
	maximum = 0.356 (at node 10)
Injected packet length average = 18.0343
Accepted packet length average = 17.642
Total in-flight flits = 56507 (0 measured)
latency change    = 0.55436
throughput change = 0.0198127
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2213.91
	minimum = 1052
	maximum = 3442
Network latency average = 346.317
	minimum = 29
	maximum = 957
Slowest packet = 10204
Flit latency average = 1222.87
	minimum = 6
	maximum = 3918
Slowest flit = 7960
Fragmentation average = 152.753
	minimum = 2
	maximum = 624
Injected packet rate average = 0.00978125
	minimum = 0 (at node 0)
	maximum = 0.043 (at node 63)
Accepted packet rate average = 0.0116406
	minimum = 0.001 (at node 115)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.175406
	minimum = 0 (at node 0)
	maximum = 0.762 (at node 63)
Accepted flit rate average= 0.206849
	minimum = 0.044 (at node 115)
	maximum = 0.35 (at node 123)
Injected packet length average = 17.9329
Accepted packet length average = 17.7696
Total in-flight flits = 50524 (21987 measured)
latency change    = 0.242579
throughput change = 0.0510135
Class 0:
Packet latency average = 2681.32
	minimum = 1052
	maximum = 4250
Network latency average = 534.662
	minimum = 28
	maximum = 1970
Slowest packet = 10204
Flit latency average = 1218.83
	minimum = 6
	maximum = 4830
Slowest flit = 21174
Fragmentation average = 185.4
	minimum = 1
	maximum = 1507
Injected packet rate average = 0.00990104
	minimum = 0 (at node 8)
	maximum = 0.028 (at node 114)
Accepted packet rate average = 0.0115078
	minimum = 0.0055 (at node 36)
	maximum = 0.0175 (at node 181)
Injected flit rate average = 0.17788
	minimum = 0 (at node 8)
	maximum = 0.5025 (at node 114)
Accepted flit rate average= 0.20525
	minimum = 0.0925 (at node 2)
	maximum = 0.3035 (at node 181)
Injected packet length average = 17.9658
Accepted packet length average = 17.8357
Total in-flight flits = 45983 (30292 measured)
latency change    = 0.174324
throughput change = 0.0077903
Class 0:
Packet latency average = 3158.21
	minimum = 1052
	maximum = 5181
Network latency average = 631.646
	minimum = 25
	maximum = 2963
Slowest packet = 10204
Flit latency average = 1163.37
	minimum = 6
	maximum = 5868
Slowest flit = 10223
Fragmentation average = 201.365
	minimum = 1
	maximum = 2362
Injected packet rate average = 0.0109913
	minimum = 0 (at node 8)
	maximum = 0.0286667 (at node 85)
Accepted packet rate average = 0.0115885
	minimum = 0.006 (at node 2)
	maximum = 0.0166667 (at node 128)
Injected flit rate average = 0.197552
	minimum = 0 (at node 8)
	maximum = 0.514667 (at node 85)
Accepted flit rate average= 0.207236
	minimum = 0.097 (at node 2)
	maximum = 0.291667 (at node 128)
Injected packet length average = 17.9735
Accepted packet length average = 17.8828
Total in-flight flits = 50971 (41017 measured)
latency change    = 0.151
throughput change = 0.00958381
Class 0:
Packet latency average = 3587.82
	minimum = 1052
	maximum = 6254
Network latency average = 723.478
	minimum = 25
	maximum = 3836
Slowest packet = 10204
Flit latency average = 1138.76
	minimum = 6
	maximum = 6841
Slowest flit = 18119
Fragmentation average = 217.294
	minimum = 1
	maximum = 2362
Injected packet rate average = 0.0114193
	minimum = 0 (at node 29)
	maximum = 0.02525 (at node 134)
Accepted packet rate average = 0.0116432
	minimum = 0.0065 (at node 2)
	maximum = 0.0165 (at node 151)
Injected flit rate average = 0.205493
	minimum = 0 (at node 29)
	maximum = 0.4545 (at node 134)
Accepted flit rate average= 0.208792
	minimum = 0.117 (at node 2)
	maximum = 0.28925 (at node 128)
Injected packet length average = 17.9953
Accepted packet length average = 17.9325
Total in-flight flits = 53871 (47132 measured)
latency change    = 0.119741
throughput change = 0.00745028
Draining remaining packets ...
Class 0:
Remaining flits: 13920 13921 13922 13923 13924 13925 13926 13927 13928 13929 [...] (19831 flits)
Measured flits: 184878 184879 184880 184881 184882 184883 184884 184885 184886 184887 [...] (16473 flits)
Class 0:
Remaining flits: 122382 122383 122384 122385 122386 122387 122388 122389 122390 122391 [...] (924 flits)
Measured flits: 204588 204589 204590 204591 204592 204593 204594 204595 204596 204597 [...] (863 flits)
Time taken is 9758 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4237.32 (1 samples)
	minimum = 1052 (1 samples)
	maximum = 8948 (1 samples)
Network latency average = 1107.71 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5744 (1 samples)
Flit latency average = 1446.95 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8640 (1 samples)
Fragmentation average = 236.071 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4330 (1 samples)
Injected packet rate average = 0.0114193 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.02525 (1 samples)
Accepted packet rate average = 0.0116432 (1 samples)
	minimum = 0.0065 (1 samples)
	maximum = 0.0165 (1 samples)
Injected flit rate average = 0.205493 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.4545 (1 samples)
Accepted flit rate average = 0.208792 (1 samples)
	minimum = 0.117 (1 samples)
	maximum = 0.28925 (1 samples)
Injected packet size average = 17.9953 (1 samples)
Accepted packet size average = 17.9325 (1 samples)
Hops average = 5.13102 (1 samples)
Total run time 13.1849
