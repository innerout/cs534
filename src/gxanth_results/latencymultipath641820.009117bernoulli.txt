BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 60.9234
	minimum = 22
	maximum = 173
Network latency average = 59.3354
	minimum = 22
	maximum = 163
Slowest packet = 235
Flit latency average = 34.8469
	minimum = 5
	maximum = 146
Slowest flit = 12059
Fragmentation average = 14.3348
	minimum = 0
	maximum = 78
Injected packet rate average = 0.00905729
	minimum = 0.002 (at node 135)
	maximum = 0.02 (at node 10)
Accepted packet rate average = 0.00843229
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.161458
	minimum = 0.036 (at node 135)
	maximum = 0.343 (at node 10)
Accepted flit rate average= 0.154943
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 48)
Injected packet length average = 17.8263
Accepted packet length average = 18.3749
Total in-flight flits = 1553 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 64.0795
	minimum = 22
	maximum = 248
Network latency average = 62.5267
	minimum = 22
	maximum = 242
Slowest packet = 1682
Flit latency average = 37.6552
	minimum = 5
	maximum = 225
Slowest flit = 36593
Fragmentation average = 14.6257
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00891927
	minimum = 0.0035 (at node 122)
	maximum = 0.017 (at node 10)
Accepted packet rate average = 0.00868229
	minimum = 0.005 (at node 30)
	maximum = 0.0135 (at node 22)
Injected flit rate average = 0.159792
	minimum = 0.063 (at node 122)
	maximum = 0.306 (at node 10)
Accepted flit rate average= 0.157443
	minimum = 0.09 (at node 30)
	maximum = 0.243 (at node 22)
Injected packet length average = 17.9153
Accepted packet length average = 18.1338
Total in-flight flits = 1192 (0 measured)
latency change    = 0.0492525
throughput change = 0.0158788
Class 0:
Packet latency average = 66.535
	minimum = 22
	maximum = 219
Network latency average = 64.8324
	minimum = 22
	maximum = 218
Slowest packet = 4327
Flit latency average = 39.7975
	minimum = 5
	maximum = 201
Slowest flit = 78588
Fragmentation average = 15.1764
	minimum = 0
	maximum = 93
Injected packet rate average = 0.00898958
	minimum = 0.002 (at node 94)
	maximum = 0.018 (at node 52)
Accepted packet rate average = 0.00891667
	minimum = 0.001 (at node 121)
	maximum = 0.017 (at node 179)
Injected flit rate average = 0.162042
	minimum = 0.036 (at node 94)
	maximum = 0.308 (at node 52)
Accepted flit rate average= 0.160885
	minimum = 0.018 (at node 121)
	maximum = 0.305 (at node 179)
Injected packet length average = 18.0255
Accepted packet length average = 18.0432
Total in-flight flits = 1370 (0 measured)
latency change    = 0.0369063
throughput change = 0.0213985
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 64.1733
	minimum = 22
	maximum = 218
Network latency average = 62.772
	minimum = 22
	maximum = 206
Slowest packet = 6005
Flit latency average = 38.2863
	minimum = 5
	maximum = 189
Slowest flit = 108106
Fragmentation average = 14.5153
	minimum = 0
	maximum = 80
Injected packet rate average = 0.00943229
	minimum = 0.003 (at node 131)
	maximum = 0.018 (at node 0)
Accepted packet rate average = 0.00941146
	minimum = 0.001 (at node 184)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.169318
	minimum = 0.054 (at node 131)
	maximum = 0.324 (at node 0)
Accepted flit rate average= 0.16913
	minimum = 0.018 (at node 184)
	maximum = 0.351 (at node 56)
Injected packet length average = 17.9509
Accepted packet length average = 17.9707
Total in-flight flits = 1495 (1495 measured)
latency change    = 0.0368022
throughput change = 0.0487482
Class 0:
Packet latency average = 62.2101
	minimum = 22
	maximum = 218
Network latency average = 60.8349
	minimum = 22
	maximum = 206
Slowest packet = 6005
Flit latency average = 36.4257
	minimum = 5
	maximum = 189
Slowest flit = 108106
Fragmentation average = 14.1493
	minimum = 0
	maximum = 95
Injected packet rate average = 0.00911979
	minimum = 0.0035 (at node 178)
	maximum = 0.015 (at node 149)
Accepted packet rate average = 0.00917187
	minimum = 0.0045 (at node 190)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.164026
	minimum = 0.063 (at node 178)
	maximum = 0.27 (at node 149)
Accepted flit rate average= 0.164875
	minimum = 0.081 (at node 190)
	maximum = 0.333 (at node 16)
Injected packet length average = 17.9857
Accepted packet length average = 17.9761
Total in-flight flits = 1094 (1094 measured)
latency change    = 0.0315576
throughput change = 0.0258087
Class 0:
Packet latency average = 61.9601
	minimum = 22
	maximum = 218
Network latency average = 60.486
	minimum = 22
	maximum = 206
Slowest packet = 6005
Flit latency average = 36.0391
	minimum = 5
	maximum = 189
Slowest flit = 108106
Fragmentation average = 14.2047
	minimum = 0
	maximum = 95
Injected packet rate average = 0.00910764
	minimum = 0.00466667 (at node 153)
	maximum = 0.0136667 (at node 38)
Accepted packet rate average = 0.00910243
	minimum = 0.00566667 (at node 134)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.164038
	minimum = 0.084 (at node 153)
	maximum = 0.247333 (at node 106)
Accepted flit rate average= 0.163944
	minimum = 0.102 (at node 134)
	maximum = 0.299333 (at node 16)
Injected packet length average = 18.0111
Accepted packet length average = 18.0111
Total in-flight flits = 1366 (1366 measured)
latency change    = 0.00403525
throughput change = 0.00567604
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6191 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 62.252 (1 samples)
	minimum = 22 (1 samples)
	maximum = 218 (1 samples)
Network latency average = 60.7524 (1 samples)
	minimum = 22 (1 samples)
	maximum = 206 (1 samples)
Flit latency average = 36.2374 (1 samples)
	minimum = 5 (1 samples)
	maximum = 189 (1 samples)
Fragmentation average = 14.3032 (1 samples)
	minimum = 0 (1 samples)
	maximum = 95 (1 samples)
Injected packet rate average = 0.00910764 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0136667 (1 samples)
Accepted packet rate average = 0.00910243 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.164038 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.247333 (1 samples)
Accepted flit rate average = 0.163944 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.299333 (1 samples)
Injected packet size average = 18.0111 (1 samples)
Accepted packet size average = 18.0111 (1 samples)
Hops average = 5.0577 (1 samples)
Total run time 3.60246
