BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 251.805
	minimum = 25
	maximum = 950
Network latency average = 244.589
	minimum = 24
	maximum = 950
Slowest packet = 185
Flit latency average = 173.361
	minimum = 6
	maximum = 933
Slowest flit = 3347
Fragmentation average = 138.148
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0183281
	minimum = 0.007 (at node 16)
	maximum = 0.031 (at node 155)
Accepted packet rate average = 0.00988021
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 49)
Injected flit rate average = 0.325964
	minimum = 0.125 (at node 16)
	maximum = 0.558 (at node 155)
Accepted flit rate average= 0.200286
	minimum = 0.038 (at node 174)
	maximum = 0.341 (at node 49)
Injected packet length average = 17.7849
Accepted packet length average = 20.2715
Total in-flight flits = 24977 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 444.987
	minimum = 25
	maximum = 1869
Network latency average = 406.82
	minimum = 23
	maximum = 1869
Slowest packet = 409
Flit latency average = 317.881
	minimum = 6
	maximum = 1852
Slowest flit = 7379
Fragmentation average = 172.335
	minimum = 0
	maximum = 1672
Injected packet rate average = 0.0158047
	minimum = 0.004 (at node 44)
	maximum = 0.024 (at node 150)
Accepted packet rate average = 0.0105391
	minimum = 0.006 (at node 83)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.280971
	minimum = 0.067 (at node 44)
	maximum = 0.432 (at node 150)
Accepted flit rate average= 0.201781
	minimum = 0.108 (at node 83)
	maximum = 0.2985 (at node 22)
Injected packet length average = 17.7777
Accepted packet length average = 19.146
Total in-flight flits = 31920 (0 measured)
latency change    = 0.434129
throughput change = 0.00740798
Class 0:
Packet latency average = 1062.36
	minimum = 28
	maximum = 2671
Network latency average = 812.008
	minimum = 23
	maximum = 2655
Slowest packet = 968
Flit latency average = 702.028
	minimum = 6
	maximum = 2867
Slowest flit = 7625
Fragmentation average = 222.325
	minimum = 0
	maximum = 2366
Injected packet rate average = 0.0121458
	minimum = 0 (at node 3)
	maximum = 0.03 (at node 118)
Accepted packet rate average = 0.0110729
	minimum = 0.003 (at node 4)
	maximum = 0.02 (at node 97)
Injected flit rate average = 0.218307
	minimum = 0 (at node 21)
	maximum = 0.543 (at node 118)
Accepted flit rate average= 0.199036
	minimum = 0.067 (at node 4)
	maximum = 0.324 (at node 56)
Injected packet length average = 17.9738
Accepted packet length average = 17.9751
Total in-flight flits = 35699 (0 measured)
latency change    = 0.581133
throughput change = 0.0137904
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1272.84
	minimum = 34
	maximum = 2972
Network latency average = 372.779
	minimum = 27
	maximum = 990
Slowest packet = 8411
Flit latency average = 826.706
	minimum = 6
	maximum = 3668
Slowest flit = 20447
Fragmentation average = 127.591
	minimum = 0
	maximum = 765
Injected packet rate average = 0.0112708
	minimum = 0 (at node 5)
	maximum = 0.028 (at node 87)
Accepted packet rate average = 0.0109844
	minimum = 0.005 (at node 36)
	maximum = 0.019 (at node 30)
Injected flit rate average = 0.20313
	minimum = 0 (at node 5)
	maximum = 0.488 (at node 87)
Accepted flit rate average= 0.196339
	minimum = 0.056 (at node 138)
	maximum = 0.326 (at node 133)
Injected packet length average = 18.0226
Accepted packet length average = 17.8743
Total in-flight flits = 36990 (26164 measured)
latency change    = 0.165366
throughput change = 0.0137411
Class 0:
Packet latency average = 1654.8
	minimum = 34
	maximum = 3915
Network latency average = 612.172
	minimum = 24
	maximum = 1963
Slowest packet = 8411
Flit latency average = 850.064
	minimum = 6
	maximum = 4507
Slowest flit = 24929
Fragmentation average = 159.763
	minimum = 0
	maximum = 1573
Injected packet rate average = 0.0108255
	minimum = 0 (at node 5)
	maximum = 0.022 (at node 50)
Accepted packet rate average = 0.0108464
	minimum = 0.0045 (at node 36)
	maximum = 0.0185 (at node 19)
Injected flit rate average = 0.195357
	minimum = 0 (at node 32)
	maximum = 0.4 (at node 50)
Accepted flit rate average= 0.194602
	minimum = 0.073 (at node 36)
	maximum = 0.3325 (at node 19)
Injected packet length average = 18.0459
Accepted packet length average = 17.9417
Total in-flight flits = 36050 (32366 measured)
latency change    = 0.230817
throughput change = 0.00892582
Class 0:
Packet latency average = 1946.83
	minimum = 34
	maximum = 4526
Network latency average = 744.599
	minimum = 24
	maximum = 2961
Slowest packet = 8411
Flit latency average = 875.275
	minimum = 6
	maximum = 5296
Slowest flit = 45939
Fragmentation average = 172.8
	minimum = 0
	maximum = 1962
Injected packet rate average = 0.010783
	minimum = 0 (at node 32)
	maximum = 0.0193333 (at node 55)
Accepted packet rate average = 0.0107951
	minimum = 0.00633333 (at node 35)
	maximum = 0.0153333 (at node 90)
Injected flit rate average = 0.194123
	minimum = 0.00133333 (at node 32)
	maximum = 0.343667 (at node 55)
Accepted flit rate average= 0.194444
	minimum = 0.114 (at node 35)
	maximum = 0.277333 (at node 128)
Injected packet length average = 18.0027
Accepted packet length average = 18.0122
Total in-flight flits = 35587 (34336 measured)
latency change    = 0.150005
throughput change = 0.000808036
Class 0:
Packet latency average = 2238.81
	minimum = 34
	maximum = 5548
Network latency average = 828.011
	minimum = 24
	maximum = 3708
Slowest packet = 8411
Flit latency average = 889.86
	minimum = 6
	maximum = 5820
Slowest flit = 45948
Fragmentation average = 183.975
	minimum = 0
	maximum = 2038
Injected packet rate average = 0.0107904
	minimum = 0.00225 (at node 140)
	maximum = 0.01925 (at node 29)
Accepted packet rate average = 0.0108385
	minimum = 0.007 (at node 36)
	maximum = 0.01575 (at node 128)
Injected flit rate average = 0.194467
	minimum = 0.04375 (at node 140)
	maximum = 0.34775 (at node 29)
Accepted flit rate average= 0.195007
	minimum = 0.12175 (at node 36)
	maximum = 0.284 (at node 128)
Injected packet length average = 18.0223
Accepted packet length average = 17.992
Total in-flight flits = 35262 (35032 measured)
latency change    = 0.130418
throughput change = 0.00288229
Class 0:
Packet latency average = 2510.54
	minimum = 34
	maximum = 6143
Network latency average = 881.622
	minimum = 24
	maximum = 4665
Slowest packet = 8411
Flit latency average = 893.731
	minimum = 6
	maximum = 6787
Slowest flit = 45953
Fragmentation average = 191.612
	minimum = 0
	maximum = 3560
Injected packet rate average = 0.0108583
	minimum = 0.0022 (at node 32)
	maximum = 0.019 (at node 29)
Accepted packet rate average = 0.0108281
	minimum = 0.0066 (at node 36)
	maximum = 0.0164 (at node 128)
Injected flit rate average = 0.195436
	minimum = 0.0406 (at node 32)
	maximum = 0.3402 (at node 29)
Accepted flit rate average= 0.194818
	minimum = 0.1182 (at node 36)
	maximum = 0.2908 (at node 128)
Injected packet length average = 17.9988
Accepted packet length average = 17.9918
Total in-flight flits = 36432 (36401 measured)
latency change    = 0.108235
throughput change = 0.000969122
Class 0:
Packet latency average = 2761.82
	minimum = 34
	maximum = 6935
Network latency average = 913.584
	minimum = 24
	maximum = 5241
Slowest packet = 8411
Flit latency average = 895.681
	minimum = 6
	maximum = 6787
Slowest flit = 45953
Fragmentation average = 194.801
	minimum = 0
	maximum = 3560
Injected packet rate average = 0.0107769
	minimum = 0.0025 (at node 32)
	maximum = 0.0168333 (at node 35)
Accepted packet rate average = 0.0108325
	minimum = 0.00766667 (at node 64)
	maximum = 0.0151667 (at node 128)
Injected flit rate average = 0.194046
	minimum = 0.0446667 (at node 32)
	maximum = 0.303167 (at node 35)
Accepted flit rate average= 0.194517
	minimum = 0.141667 (at node 64)
	maximum = 0.269833 (at node 128)
Injected packet length average = 18.0057
Accepted packet length average = 17.9569
Total in-flight flits = 35229 (35226 measured)
latency change    = 0.0909819
throughput change = 0.00154406
Class 0:
Packet latency average = 3017.66
	minimum = 34
	maximum = 7555
Network latency average = 933.911
	minimum = 24
	maximum = 5949
Slowest packet = 8411
Flit latency average = 894.971
	minimum = 6
	maximum = 6787
Slowest flit = 45953
Fragmentation average = 197.388
	minimum = 0
	maximum = 4024
Injected packet rate average = 0.0107433
	minimum = 0.00214286 (at node 32)
	maximum = 0.0174286 (at node 35)
Accepted packet rate average = 0.0108043
	minimum = 0.00771429 (at node 71)
	maximum = 0.0142857 (at node 128)
Injected flit rate average = 0.193499
	minimum = 0.0385714 (at node 32)
	maximum = 0.314714 (at node 35)
Accepted flit rate average= 0.194231
	minimum = 0.144 (at node 71)
	maximum = 0.254571 (at node 128)
Injected packet length average = 18.0112
Accepted packet length average = 17.9772
Total in-flight flits = 34806 (34806 measured)
latency change    = 0.0847813
throughput change = 0.00147228
Draining all recorded packets ...
Class 0:
Remaining flits: 213875 233298 233299 233300 233301 233302 233303 233304 233305 233306 [...] (34713 flits)
Measured flits: 213875 233298 233299 233300 233301 233302 233303 233304 233305 233306 [...] (34695 flits)
Class 0:
Remaining flits: 238482 238483 238484 238485 238486 238487 238488 238489 238490 238491 [...] (36864 flits)
Measured flits: 238482 238483 238484 238485 238486 238487 238488 238489 238490 238491 [...] (36512 flits)
Class 0:
Remaining flits: 238482 238483 238484 238485 238486 238487 238488 238489 238490 238491 [...] (38504 flits)
Measured flits: 238482 238483 238484 238485 238486 238487 238488 238489 238490 238491 [...] (37915 flits)
Class 0:
Remaining flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (38626 flits)
Measured flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (36032 flits)
Class 0:
Remaining flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (38058 flits)
Measured flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (32294 flits)
Class 0:
Remaining flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (37169 flits)
Measured flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (26426 flits)
Class 0:
Remaining flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (38167 flits)
Measured flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (24029 flits)
Class 0:
Remaining flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (35842 flits)
Measured flits: 301554 301555 301556 301557 301558 301559 301560 301561 301562 301563 [...] (18649 flits)
Class 0:
Remaining flits: 301556 301557 301558 301559 301560 301561 301562 301563 301564 301565 [...] (35683 flits)
Measured flits: 301556 301557 301558 301559 301560 301561 301562 301563 301564 301565 [...] (13489 flits)
Class 0:
Remaining flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (36991 flits)
Measured flits: 454176 454177 454178 454179 454180 454181 454182 454183 454184 454185 [...] (10615 flits)
Class 0:
Remaining flits: 480672 480673 480674 480675 480676 480677 480678 480679 480680 480681 [...] (37621 flits)
Measured flits: 480672 480673 480674 480675 480676 480677 480678 480679 480680 480681 [...] (7524 flits)
Class 0:
Remaining flits: 480683 480684 480685 480686 480687 480688 480689 544523 544524 544525 [...] (36665 flits)
Measured flits: 480683 480684 480685 480686 480687 480688 480689 544523 544524 544525 [...] (5383 flits)
Class 0:
Remaining flits: 556760 556761 556762 556763 556764 556765 556766 556767 556768 556769 [...] (36642 flits)
Measured flits: 556760 556761 556762 556763 556764 556765 556766 556767 556768 556769 [...] (3288 flits)
Class 0:
Remaining flits: 575136 575137 575138 575139 575140 575141 575142 575143 575144 575145 [...] (36260 flits)
Measured flits: 575136 575137 575138 575139 575140 575141 575142 575143 575144 575145 [...] (2474 flits)
Class 0:
Remaining flits: 575136 575137 575138 575139 575140 575141 575142 575143 575144 575145 [...] (36517 flits)
Measured flits: 575136 575137 575138 575139 575140 575141 575142 575143 575144 575145 [...] (2273 flits)
Class 0:
Remaining flits: 658800 658801 658802 658803 658804 658805 658806 658807 658808 658809 [...] (35569 flits)
Measured flits: 658800 658801 658802 658803 658804 658805 658806 658807 658808 658809 [...] (1717 flits)
Class 0:
Remaining flits: 716004 716005 716006 716007 716008 716009 716010 716011 716012 716013 [...] (37624 flits)
Measured flits: 754829 777600 777601 777602 777603 777604 777605 777606 777607 777608 [...] (1264 flits)
Class 0:
Remaining flits: 775098 775099 775100 775101 775102 775103 775104 775105 775106 775107 [...] (36812 flits)
Measured flits: 777600 777601 777602 777603 777604 777605 777606 777607 777608 777609 [...] (826 flits)
Class 0:
Remaining flits: 775098 775099 775100 775101 775102 775103 775104 775105 775106 775107 [...] (36910 flits)
Measured flits: 898721 960890 960891 960892 960893 969553 969554 969555 969556 969557 [...] (588 flits)
Class 0:
Remaining flits: 796479 796480 796481 822868 822869 825048 825049 825050 825051 825052 [...] (36073 flits)
Measured flits: 982223 1010160 1010161 1010162 1010163 1010164 1010165 1010166 1010167 1010168 [...] (364 flits)
Class 0:
Remaining flits: 856746 856747 856748 856749 856750 856751 856752 856753 856754 856755 [...] (37403 flits)
Measured flits: 1010161 1010162 1010163 1010164 1010165 1010166 1010167 1010168 1010169 1010170 [...] (439 flits)
Class 0:
Remaining flits: 893106 893107 893108 893109 893110 893111 893112 893113 893114 893115 [...] (36681 flits)
Measured flits: 1020870 1020871 1020872 1020873 1020874 1020875 1020876 1020877 1020878 1020879 [...] (211 flits)
Class 0:
Remaining flits: 948581 962550 962551 962552 962553 962554 962555 962556 962557 962558 [...] (35962 flits)
Measured flits: 1174464 1174465 1174466 1174467 1174468 1174469 1174470 1174471 1174472 1174473 [...] (280 flits)
Class 0:
Remaining flits: 1010916 1010917 1010918 1010919 1010920 1010921 1010922 1010923 1010924 1010925 [...] (35493 flits)
Measured flits: 1230012 1230013 1230014 1230015 1230016 1230017 1230018 1230019 1230020 1230021 [...] (143 flits)
Class 0:
Remaining flits: 1010916 1010917 1010918 1010919 1010920 1010921 1010922 1010923 1010924 1010925 [...] (36746 flits)
Measured flits: 1230012 1230013 1230014 1230015 1230016 1230017 1230018 1230019 1230020 1230021 [...] (80 flits)
Class 0:
Remaining flits: 1038026 1038027 1038028 1038029 1038030 1038031 1038032 1038033 1038034 1038035 [...] (37207 flits)
Measured flits: 1230029 1265219 1342512 1342513 1342514 1342515 1342516 1342517 1342518 1342519 [...] (140 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1065474 1065475 1065476 1065477 1065478 1065479 1065480 1065481 1065482 1065483 [...] (8112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1254833 1263852 1263853 1263854 1263855 1263856 1263857 1263858 1263859 1263860 [...] (330 flits)
Measured flits: (0 flits)
Time taken is 39143 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5857.3 (1 samples)
	minimum = 34 (1 samples)
	maximum = 27252 (1 samples)
Network latency average = 1077.36 (1 samples)
	minimum = 24 (1 samples)
	maximum = 12602 (1 samples)
Flit latency average = 963.195 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12585 (1 samples)
Fragmentation average = 208.197 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4606 (1 samples)
Injected packet rate average = 0.0107433 (1 samples)
	minimum = 0.00214286 (1 samples)
	maximum = 0.0174286 (1 samples)
Accepted packet rate average = 0.0108043 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0142857 (1 samples)
Injected flit rate average = 0.193499 (1 samples)
	minimum = 0.0385714 (1 samples)
	maximum = 0.314714 (1 samples)
Accepted flit rate average = 0.194231 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.254571 (1 samples)
Injected packet size average = 18.0112 (1 samples)
Accepted packet size average = 17.9772 (1 samples)
Hops average = 5.05479 (1 samples)
Total run time 41.0302
