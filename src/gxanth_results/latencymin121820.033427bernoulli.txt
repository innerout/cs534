BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 293.512
	minimum = 22
	maximum = 824
Network latency average = 237.634
	minimum = 22
	maximum = 761
Slowest packet = 733
Flit latency average = 209.052
	minimum = 5
	maximum = 744
Slowest flit = 22913
Fragmentation average = 21.7405
	minimum = 0
	maximum = 116
Injected packet rate average = 0.0187552
	minimum = 0.01 (at node 47)
	maximum = 0.027 (at node 134)
Accepted packet rate average = 0.0135885
	minimum = 0.005 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.333656
	minimum = 0.18 (at node 47)
	maximum = 0.478 (at node 134)
Accepted flit rate average= 0.250063
	minimum = 0.09 (at node 174)
	maximum = 0.416 (at node 44)
Injected packet length average = 17.7901
Accepted packet length average = 18.4025
Total in-flight flits = 18678 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 572.576
	minimum = 22
	maximum = 1695
Network latency average = 299.01
	minimum = 22
	maximum = 1297
Slowest packet = 1056
Flit latency average = 266.365
	minimum = 5
	maximum = 1533
Slowest flit = 37335
Fragmentation average = 22.3764
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0164427
	minimum = 0.007 (at node 132)
	maximum = 0.0245 (at node 3)
Accepted packet rate average = 0.0139427
	minimum = 0.0075 (at node 153)
	maximum = 0.02 (at node 177)
Injected flit rate average = 0.293807
	minimum = 0.126 (at node 132)
	maximum = 0.441 (at node 3)
Accepted flit rate average= 0.253245
	minimum = 0.135 (at node 153)
	maximum = 0.36 (at node 177)
Injected packet length average = 17.8685
Accepted packet length average = 18.1632
Total in-flight flits = 18314 (0 measured)
latency change    = 0.487384
throughput change = 0.0125661
Class 0:
Packet latency average = 1437.58
	minimum = 495
	maximum = 2419
Network latency average = 365.87
	minimum = 22
	maximum = 1777
Slowest packet = 2074
Flit latency average = 330.004
	minimum = 5
	maximum = 1736
Slowest flit = 48986
Fragmentation average = 22.5893
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0138698
	minimum = 0 (at node 179)
	maximum = 0.035 (at node 135)
Accepted packet rate average = 0.0137969
	minimum = 0.006 (at node 96)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.249938
	minimum = 0 (at node 179)
	maximum = 0.632 (at node 135)
Accepted flit rate average= 0.249604
	minimum = 0.115 (at node 190)
	maximum = 0.469 (at node 34)
Injected packet length average = 18.0203
Accepted packet length average = 18.0914
Total in-flight flits = 18306 (0 measured)
latency change    = 0.601708
throughput change = 0.0145856
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2060.02
	minimum = 933
	maximum = 2986
Network latency average = 290.418
	minimum = 22
	maximum = 857
Slowest packet = 9094
Flit latency average = 330.261
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 21.6772
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0140729
	minimum = 0.004 (at node 41)
	maximum = 0.027 (at node 49)
Accepted packet rate average = 0.0140365
	minimum = 0.007 (at node 5)
	maximum = 0.024 (at node 123)
Injected flit rate average = 0.253125
	minimum = 0.061 (at node 177)
	maximum = 0.491 (at node 49)
Accepted flit rate average= 0.25226
	minimum = 0.117 (at node 71)
	maximum = 0.434 (at node 129)
Injected packet length average = 17.9867
Accepted packet length average = 17.9718
Total in-flight flits = 18508 (18436 measured)
latency change    = 0.302152
throughput change = 0.0105298
Class 0:
Packet latency average = 2390.31
	minimum = 933
	maximum = 3727
Network latency average = 337.251
	minimum = 22
	maximum = 1483
Slowest packet = 9094
Flit latency average = 329.988
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 22.7194
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0139427
	minimum = 0.007 (at node 152)
	maximum = 0.0255 (at node 39)
Accepted packet rate average = 0.013974
	minimum = 0.0075 (at node 79)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.25087
	minimum = 0.13 (at node 152)
	maximum = 0.451 (at node 39)
Accepted flit rate average= 0.251401
	minimum = 0.136 (at node 79)
	maximum = 0.4085 (at node 129)
Injected packet length average = 17.9929
Accepted packet length average = 17.9907
Total in-flight flits = 18014 (18014 measured)
latency change    = 0.138179
throughput change = 0.00341834
Class 0:
Packet latency average = 2679.93
	minimum = 933
	maximum = 4451
Network latency average = 346.143
	minimum = 22
	maximum = 1483
Slowest packet = 9094
Flit latency average = 328.295
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 23.0227
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0138698
	minimum = 0.00733333 (at node 130)
	maximum = 0.0216667 (at node 39)
Accepted packet rate average = 0.0139063
	minimum = 0.00933333 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.249908
	minimum = 0.136333 (at node 130)
	maximum = 0.386 (at node 39)
Accepted flit rate average= 0.250182
	minimum = 0.168 (at node 36)
	maximum = 0.355667 (at node 128)
Injected packet length average = 18.0181
Accepted packet length average = 17.9906
Total in-flight flits = 18183 (18183 measured)
latency change    = 0.108071
throughput change = 0.00487145
Class 0:
Packet latency average = 2976.15
	minimum = 933
	maximum = 5252
Network latency average = 351.458
	minimum = 22
	maximum = 1763
Slowest packet = 9094
Flit latency average = 328.106
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 23.0597
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0139102
	minimum = 0.00875 (at node 124)
	maximum = 0.02025 (at node 13)
Accepted packet rate average = 0.0139049
	minimum = 0.00975 (at node 89)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.250319
	minimum = 0.1575 (at node 124)
	maximum = 0.36325 (at node 39)
Accepted flit rate average= 0.250141
	minimum = 0.17775 (at node 89)
	maximum = 0.33875 (at node 128)
Injected packet length average = 17.9954
Accepted packet length average = 17.9893
Total in-flight flits = 18564 (18564 measured)
latency change    = 0.0995324
throughput change = 0.000166573
Class 0:
Packet latency average = 3277.87
	minimum = 933
	maximum = 6088
Network latency average = 355.231
	minimum = 22
	maximum = 1841
Slowest packet = 9094
Flit latency average = 328.818
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 23.162
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0139865
	minimum = 0.0088 (at node 36)
	maximum = 0.0202 (at node 31)
Accepted packet rate average = 0.0139583
	minimum = 0.0106 (at node 162)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.251626
	minimum = 0.1584 (at node 36)
	maximum = 0.3608 (at node 31)
Accepted flit rate average= 0.2512
	minimum = 0.1908 (at node 162)
	maximum = 0.334 (at node 128)
Injected packet length average = 17.9907
Accepted packet length average = 17.9964
Total in-flight flits = 18678 (18678 measured)
latency change    = 0.092048
throughput change = 0.00421726
Class 0:
Packet latency average = 3571.4
	minimum = 933
	maximum = 6728
Network latency average = 355.855
	minimum = 22
	maximum = 1841
Slowest packet = 9094
Flit latency average = 327.675
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 23.2329
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0140252
	minimum = 0.00883333 (at node 36)
	maximum = 0.0213333 (at node 39)
Accepted packet rate average = 0.0140156
	minimum = 0.011 (at node 79)
	maximum = 0.0181667 (at node 128)
Injected flit rate average = 0.252389
	minimum = 0.157833 (at node 36)
	maximum = 0.383833 (at node 39)
Accepted flit rate average= 0.252206
	minimum = 0.199 (at node 79)
	maximum = 0.326667 (at node 128)
Injected packet length average = 17.9954
Accepted packet length average = 17.9946
Total in-flight flits = 18537 (18537 measured)
latency change    = 0.0821893
throughput change = 0.00398773
Class 0:
Packet latency average = 3863.03
	minimum = 933
	maximum = 7537
Network latency average = 357.731
	minimum = 22
	maximum = 1841
Slowest packet = 9094
Flit latency average = 328.311
	minimum = 5
	maximum = 2192
Slowest flit = 91151
Fragmentation average = 23.256
	minimum = 0
	maximum = 122
Injected packet rate average = 0.0140089
	minimum = 0.00885714 (at node 36)
	maximum = 0.0198571 (at node 39)
Accepted packet rate average = 0.014006
	minimum = 0.0108571 (at node 79)
	maximum = 0.0182857 (at node 138)
Injected flit rate average = 0.25215
	minimum = 0.159429 (at node 36)
	maximum = 0.357429 (at node 39)
Accepted flit rate average= 0.251935
	minimum = 0.195429 (at node 79)
	maximum = 0.325286 (at node 138)
Injected packet length average = 17.9993
Accepted packet length average = 17.9877
Total in-flight flits = 18664 (18664 measured)
latency change    = 0.0754912
throughput change = 0.00107649
Draining all recorded packets ...
Class 0:
Remaining flits: 484398 484399 484400 484401 484402 484403 484404 484405 484406 484407 [...] (18267 flits)
Measured flits: 484398 484399 484400 484401 484402 484403 484404 484405 484406 484407 [...] (18267 flits)
Class 0:
Remaining flits: 539730 539731 539732 539733 539734 539735 539736 539737 539738 539739 [...] (18201 flits)
Measured flits: 539730 539731 539732 539733 539734 539735 539736 539737 539738 539739 [...] (18201 flits)
Class 0:
Remaining flits: 547866 547867 547868 547869 547870 547871 547872 547873 547874 547875 [...] (18211 flits)
Measured flits: 547866 547867 547868 547869 547870 547871 547872 547873 547874 547875 [...] (18211 flits)
Class 0:
Remaining flits: 614610 614611 614612 614613 614614 614615 614616 614617 614618 614619 [...] (17989 flits)
Measured flits: 614610 614611 614612 614613 614614 614615 614616 614617 614618 614619 [...] (17989 flits)
Class 0:
Remaining flits: 672408 672409 672410 672411 672412 672413 672414 672415 672416 672417 [...] (18689 flits)
Measured flits: 672408 672409 672410 672411 672412 672413 672414 672415 672416 672417 [...] (18689 flits)
Class 0:
Remaining flits: 716706 716707 716708 716709 716710 716711 716712 716713 716714 716715 [...] (18374 flits)
Measured flits: 716706 716707 716708 716709 716710 716711 716712 716713 716714 716715 [...] (18374 flits)
Class 0:
Remaining flits: 762784 762785 772086 772087 772088 772089 772090 772091 772146 772147 [...] (18467 flits)
Measured flits: 762784 762785 772086 772087 772088 772089 772090 772091 772146 772147 [...] (18467 flits)
Class 0:
Remaining flits: 821142 821143 821144 821145 821146 821147 821148 821149 821150 821151 [...] (17746 flits)
Measured flits: 821142 821143 821144 821145 821146 821147 821148 821149 821150 821151 [...] (17746 flits)
Class 0:
Remaining flits: 861228 861229 861230 861231 861232 861233 861234 861235 861236 861237 [...] (18431 flits)
Measured flits: 861228 861229 861230 861231 861232 861233 861234 861235 861236 861237 [...] (18341 flits)
Class 0:
Remaining flits: 920376 920377 920378 920379 920380 920381 920382 920383 920384 920385 [...] (18755 flits)
Measured flits: 920376 920377 920378 920379 920380 920381 920382 920383 920384 920385 [...] (17719 flits)
Class 0:
Remaining flits: 965790 965791 965792 965793 965794 965795 965796 965797 965798 965799 [...] (18615 flits)
Measured flits: 965790 965791 965792 965793 965794 965795 965796 965797 965798 965799 [...] (16785 flits)
Class 0:
Remaining flits: 1017662 1017663 1017664 1017665 1022202 1022203 1022204 1022205 1022206 1022207 [...] (17964 flits)
Measured flits: 1017662 1017663 1017664 1017665 1022202 1022203 1022204 1022205 1022206 1022207 [...] (14363 flits)
Class 0:
Remaining flits: 1053972 1053973 1053974 1053975 1053976 1053977 1053978 1053979 1053980 1053981 [...] (18476 flits)
Measured flits: 1056366 1056367 1056368 1056369 1056370 1056371 1056372 1056373 1056374 1056375 [...] (11405 flits)
Class 0:
Remaining flits: 1089774 1089775 1089776 1089777 1089778 1089779 1089780 1089781 1089782 1089783 [...] (18692 flits)
Measured flits: 1089774 1089775 1089776 1089777 1089778 1089779 1089780 1089781 1089782 1089783 [...] (8558 flits)
Class 0:
Remaining flits: 1158390 1158391 1158392 1158393 1158394 1158395 1158396 1158397 1158398 1158399 [...] (18289 flits)
Measured flits: 1158390 1158391 1158392 1158393 1158394 1158395 1158396 1158397 1158398 1158399 [...] (5838 flits)
Class 0:
Remaining flits: 1205208 1205209 1205210 1205211 1205212 1205213 1205214 1205215 1205216 1205217 [...] (18170 flits)
Measured flits: 1214406 1214407 1214408 1214409 1214410 1214411 1214412 1214413 1214414 1214415 [...] (3161 flits)
Class 0:
Remaining flits: 1257642 1257643 1257644 1257645 1257646 1257647 1257648 1257649 1257650 1257651 [...] (18662 flits)
Measured flits: 1264806 1264807 1264808 1264809 1264810 1264811 1264812 1264813 1264814 1264815 [...] (1575 flits)
Class 0:
Remaining flits: 1305558 1305559 1305560 1305561 1305562 1305563 1305564 1305565 1305566 1305567 [...] (18452 flits)
Measured flits: 1325808 1325809 1325810 1325811 1325812 1325813 1325814 1325815 1325816 1325817 [...] (817 flits)
Class 0:
Remaining flits: 1345019 1345020 1345021 1345022 1345023 1345024 1345025 1345026 1345027 1345028 [...] (18770 flits)
Measured flits: 1389132 1389133 1389134 1389135 1389136 1389137 1389138 1389139 1389140 1389141 [...] (413 flits)
Draining remaining packets ...
Time taken is 30504 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8089.97 (1 samples)
	minimum = 933 (1 samples)
	maximum = 19763 (1 samples)
Network latency average = 364.839 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2555 (1 samples)
Flit latency average = 329.172 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2538 (1 samples)
Fragmentation average = 22.9221 (1 samples)
	minimum = 0 (1 samples)
	maximum = 142 (1 samples)
Injected packet rate average = 0.0140089 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.014006 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.25215 (1 samples)
	minimum = 0.159429 (1 samples)
	maximum = 0.357429 (1 samples)
Accepted flit rate average = 0.251935 (1 samples)
	minimum = 0.195429 (1 samples)
	maximum = 0.325286 (1 samples)
Injected packet size average = 17.9993 (1 samples)
Accepted packet size average = 17.9877 (1 samples)
Hops average = 5.06968 (1 samples)
Total run time 27.0212
