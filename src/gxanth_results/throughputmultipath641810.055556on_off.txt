BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 395.93
	minimum = 23
	maximum = 990
Network latency average = 297.755
	minimum = 23
	maximum = 904
Slowest packet = 46
Flit latency average = 210.354
	minimum = 6
	maximum = 950
Slowest flit = 3625
Fragmentation average = 200.415
	minimum = 0
	maximum = 787
Injected packet rate average = 0.0237604
	minimum = 0 (at node 17)
	maximum = 0.046 (at node 38)
Accepted packet rate average = 0.00951042
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.422917
	minimum = 0 (at node 17)
	maximum = 0.827 (at node 38)
Accepted flit rate average= 0.20887
	minimum = 0.047 (at node 174)
	maximum = 0.352 (at node 44)
Injected packet length average = 17.7992
Accepted packet length average = 21.9622
Total in-flight flits = 42067 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 694.487
	minimum = 23
	maximum = 1969
Network latency average = 515.712
	minimum = 23
	maximum = 1892
Slowest packet = 160
Flit latency average = 400.391
	minimum = 6
	maximum = 1929
Slowest flit = 4979
Fragmentation average = 268.346
	minimum = 0
	maximum = 1677
Injected packet rate average = 0.0205
	minimum = 0.0025 (at node 82)
	maximum = 0.0385 (at node 83)
Accepted packet rate average = 0.0110078
	minimum = 0.0055 (at node 116)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.365648
	minimum = 0.045 (at node 82)
	maximum = 0.6875 (at node 83)
Accepted flit rate average= 0.219208
	minimum = 0.1155 (at node 116)
	maximum = 0.3225 (at node 119)
Injected packet length average = 17.8365
Accepted packet length average = 19.9139
Total in-flight flits = 57754 (0 measured)
latency change    = 0.429895
throughput change = 0.0471631
Class 0:
Packet latency average = 1520.17
	minimum = 42
	maximum = 2975
Network latency average = 992.289
	minimum = 28
	maximum = 2867
Slowest packet = 392
Flit latency average = 867.079
	minimum = 6
	maximum = 2903
Slowest flit = 6639
Fragmentation average = 312.969
	minimum = 1
	maximum = 2474
Injected packet rate average = 0.0116458
	minimum = 0 (at node 9)
	maximum = 0.042 (at node 5)
Accepted packet rate average = 0.0124271
	minimum = 0.003 (at node 96)
	maximum = 0.021 (at node 78)
Injected flit rate average = 0.20849
	minimum = 0 (at node 14)
	maximum = 0.749 (at node 5)
Accepted flit rate average= 0.219911
	minimum = 0.087 (at node 96)
	maximum = 0.366 (at node 78)
Injected packet length average = 17.9025
Accepted packet length average = 17.6961
Total in-flight flits = 55743 (0 measured)
latency change    = 0.543151
throughput change = 0.00319731
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1841.79
	minimum = 39
	maximum = 3770
Network latency average = 295.592
	minimum = 29
	maximum = 948
Slowest packet = 10122
Flit latency average = 1028.91
	minimum = 6
	maximum = 3866
Slowest flit = 10367
Fragmentation average = 142.593
	minimum = 2
	maximum = 628
Injected packet rate average = 0.0118125
	minimum = 0 (at node 1)
	maximum = 0.039 (at node 43)
Accepted packet rate average = 0.0120729
	minimum = 0.004 (at node 54)
	maximum = 0.024 (at node 182)
Injected flit rate average = 0.212641
	minimum = 0 (at node 1)
	maximum = 0.699 (at node 43)
Accepted flit rate average= 0.215714
	minimum = 0.087 (at node 74)
	maximum = 0.396 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 17.8676
Total in-flight flits = 55204 (27090 measured)
latency change    = 0.174626
throughput change = 0.0194606
Class 0:
Packet latency average = 2306.57
	minimum = 39
	maximum = 4765
Network latency average = 529.443
	minimum = 25
	maximum = 1963
Slowest packet = 10122
Flit latency average = 1054.67
	minimum = 6
	maximum = 4742
Slowest flit = 17567
Fragmentation average = 185.134
	minimum = 2
	maximum = 1503
Injected packet rate average = 0.0111354
	minimum = 0 (at node 14)
	maximum = 0.031 (at node 34)
Accepted packet rate average = 0.0118594
	minimum = 0.0065 (at node 7)
	maximum = 0.0205 (at node 129)
Injected flit rate average = 0.200513
	minimum = 0 (at node 16)
	maximum = 0.5585 (at node 34)
Accepted flit rate average= 0.210958
	minimum = 0.101 (at node 74)
	maximum = 0.365 (at node 129)
Injected packet length average = 18.0068
Accepted packet length average = 17.7883
Total in-flight flits = 51811 (35040 measured)
latency change    = 0.201504
throughput change = 0.022541
Class 0:
Packet latency average = 2779.36
	minimum = 39
	maximum = 5600
Network latency average = 672.511
	minimum = 25
	maximum = 2930
Slowest packet = 10122
Flit latency average = 1076.71
	minimum = 6
	maximum = 5755
Slowest flit = 21357
Fragmentation average = 200.455
	minimum = 0
	maximum = 2225
Injected packet rate average = 0.0113056
	minimum = 0 (at node 14)
	maximum = 0.0276667 (at node 145)
Accepted packet rate average = 0.0117986
	minimum = 0.007 (at node 141)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.203498
	minimum = 0 (at node 20)
	maximum = 0.495333 (at node 145)
Accepted flit rate average= 0.210573
	minimum = 0.127 (at node 141)
	maximum = 0.309667 (at node 128)
Injected packet length average = 17.9998
Accepted packet length average = 17.8473
Total in-flight flits = 51849 (40996 measured)
latency change    = 0.170106
throughput change = 0.00183032
Draining remaining packets ...
Class 0:
Remaining flits: 9342 9343 9344 9345 9346 9347 9348 9349 9350 9351 [...] (18782 flits)
Measured flits: 182163 182164 182165 182166 182167 182168 182169 182170 182171 182172 [...] (13977 flits)
Class 0:
Remaining flits: 62630 62631 62632 62633 62634 62635 62636 62637 62638 62639 [...] (752 flits)
Measured flits: 184690 184691 184692 184693 184694 184695 184696 184697 194569 194570 [...] (662 flits)
Time taken is 8273 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3419.69 (1 samples)
	minimum = 39 (1 samples)
	maximum = 7519 (1 samples)
Network latency average = 1105.81 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4967 (1 samples)
Flit latency average = 1479.29 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7452 (1 samples)
Fragmentation average = 212.247 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3819 (1 samples)
Injected packet rate average = 0.0113056 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0117986 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.203498 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.495333 (1 samples)
Accepted flit rate average = 0.210573 (1 samples)
	minimum = 0.127 (1 samples)
	maximum = 0.309667 (1 samples)
Injected packet size average = 17.9998 (1 samples)
Accepted packet size average = 17.8473 (1 samples)
Hops average = 5.13999 (1 samples)
Total run time 11.1937
