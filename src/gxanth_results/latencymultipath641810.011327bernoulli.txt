BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 156.667
	minimum = 23
	maximum = 843
Network latency average = 154.661
	minimum = 23
	maximum = 843
Slowest packet = 157
Flit latency average = 93.7277
	minimum = 6
	maximum = 949
Slowest flit = 1247
Fragmentation average = 94.8754
	minimum = 0
	maximum = 648
Injected packet rate average = 0.0112344
	minimum = 0.003 (at node 14)
	maximum = 0.022 (at node 115)
Accepted packet rate average = 0.00835938
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 132)
Injected flit rate average = 0.19987
	minimum = 0.054 (at node 14)
	maximum = 0.391 (at node 115)
Accepted flit rate average= 0.164766
	minimum = 0.036 (at node 174)
	maximum = 0.312 (at node 140)
Injected packet length average = 17.7909
Accepted packet length average = 19.7103
Total in-flight flits = 7191 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 217.252
	minimum = 23
	maximum = 1750
Network latency average = 213.854
	minimum = 23
	maximum = 1737
Slowest packet = 396
Flit latency average = 139.425
	minimum = 6
	maximum = 1720
Slowest flit = 7145
Fragmentation average = 125.529
	minimum = 0
	maximum = 1064
Injected packet rate average = 0.0110625
	minimum = 0.005 (at node 177)
	maximum = 0.0205 (at node 137)
Accepted packet rate average = 0.0092474
	minimum = 0.0055 (at node 30)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.198203
	minimum = 0.09 (at node 177)
	maximum = 0.361 (at node 137)
Accepted flit rate average= 0.175349
	minimum = 0.099 (at node 30)
	maximum = 0.3015 (at node 22)
Injected packet length average = 17.9167
Accepted packet length average = 18.962
Total in-flight flits = 9130 (0 measured)
latency change    = 0.278868
throughput change = 0.0603558
Class 0:
Packet latency average = 337.734
	minimum = 23
	maximum = 2233
Network latency average = 332.432
	minimum = 23
	maximum = 2233
Slowest packet = 1331
Flit latency average = 230.931
	minimum = 6
	maximum = 2604
Slowest flit = 11103
Fragmentation average = 177.038
	minimum = 0
	maximum = 1702
Injected packet rate average = 0.0111927
	minimum = 0.003 (at node 158)
	maximum = 0.022 (at node 181)
Accepted packet rate average = 0.0103073
	minimum = 0.003 (at node 96)
	maximum = 0.02 (at node 97)
Injected flit rate average = 0.201646
	minimum = 0.054 (at node 158)
	maximum = 0.408 (at node 181)
Accepted flit rate average= 0.187448
	minimum = 0.065 (at node 9)
	maximum = 0.339 (at node 97)
Injected packet length average = 18.0158
Accepted packet length average = 18.186
Total in-flight flits = 11822 (0 measured)
latency change    = 0.356737
throughput change = 0.0645457
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 264.246
	minimum = 23
	maximum = 1220
Network latency average = 250.665
	minimum = 23
	maximum = 855
Slowest packet = 6400
Flit latency average = 283.866
	minimum = 6
	maximum = 3555
Slowest flit = 11105
Fragmentation average = 152.475
	minimum = 0
	maximum = 740
Injected packet rate average = 0.0115677
	minimum = 0 (at node 138)
	maximum = 0.022 (at node 31)
Accepted packet rate average = 0.0106458
	minimum = 0.001 (at node 163)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.207698
	minimum = 0 (at node 138)
	maximum = 0.402 (at node 31)
Accepted flit rate average= 0.193469
	minimum = 0.035 (at node 163)
	maximum = 0.384 (at node 34)
Injected packet length average = 17.955
Accepted packet length average = 18.1732
Total in-flight flits = 14708 (12419 measured)
latency change    = 0.278101
throughput change = 0.0311204
Class 0:
Packet latency average = 334.656
	minimum = 23
	maximum = 1931
Network latency average = 321.587
	minimum = 23
	maximum = 1931
Slowest packet = 6490
Flit latency average = 305.302
	minimum = 6
	maximum = 4346
Slowest flit = 23273
Fragmentation average = 172.802
	minimum = 0
	maximum = 1419
Injected packet rate average = 0.0112422
	minimum = 0.001 (at node 138)
	maximum = 0.0175 (at node 22)
Accepted packet rate average = 0.0107318
	minimum = 0.005 (at node 17)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.202271
	minimum = 0.015 (at node 138)
	maximum = 0.321 (at node 22)
Accepted flit rate average= 0.194297
	minimum = 0.0965 (at node 17)
	maximum = 0.3255 (at node 16)
Injected packet length average = 17.9921
Accepted packet length average = 18.1048
Total in-flight flits = 14954 (13792 measured)
latency change    = 0.210395
throughput change = 0.00426216
Class 0:
Packet latency average = 389.534
	minimum = 23
	maximum = 2935
Network latency average = 363.562
	minimum = 23
	maximum = 2935
Slowest packet = 6492
Flit latency average = 324.555
	minimum = 6
	maximum = 4902
Slowest flit = 31697
Fragmentation average = 184.581
	minimum = 0
	maximum = 1896
Injected packet rate average = 0.011283
	minimum = 0.00533333 (at node 64)
	maximum = 0.0156667 (at node 49)
Accepted packet rate average = 0.010717
	minimum = 0.00533333 (at node 4)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.202743
	minimum = 0.097 (at node 64)
	maximum = 0.278333 (at node 34)
Accepted flit rate average= 0.195002
	minimum = 0.0966667 (at node 4)
	maximum = 0.299667 (at node 16)
Injected packet length average = 17.9689
Accepted packet length average = 18.1955
Total in-flight flits = 16501 (15934 measured)
latency change    = 0.14088
throughput change = 0.00361464
Class 0:
Packet latency average = 428.954
	minimum = 23
	maximum = 3915
Network latency average = 398.35
	minimum = 23
	maximum = 3911
Slowest packet = 6442
Flit latency average = 341.745
	minimum = 6
	maximum = 5781
Slowest flit = 45449
Fragmentation average = 194.776
	minimum = 0
	maximum = 2692
Injected packet rate average = 0.011237
	minimum = 0.0065 (at node 17)
	maximum = 0.01525 (at node 34)
Accepted packet rate average = 0.0107786
	minimum = 0.00725 (at node 4)
	maximum = 0.01575 (at node 16)
Injected flit rate average = 0.202161
	minimum = 0.117 (at node 17)
	maximum = 0.27625 (at node 34)
Accepted flit rate average= 0.195487
	minimum = 0.1305 (at node 4)
	maximum = 0.2755 (at node 59)
Injected packet length average = 17.9907
Accepted packet length average = 18.1365
Total in-flight flits = 17064 (16725 measured)
latency change    = 0.0918992
throughput change = 0.00248223
Class 0:
Packet latency average = 460.421
	minimum = 23
	maximum = 4603
Network latency average = 425.636
	minimum = 23
	maximum = 4603
Slowest packet = 7040
Flit latency average = 355.342
	minimum = 6
	maximum = 6916
Slowest flit = 34124
Fragmentation average = 202.707
	minimum = 0
	maximum = 2729
Injected packet rate average = 0.0111729
	minimum = 0.0054 (at node 64)
	maximum = 0.015 (at node 28)
Accepted packet rate average = 0.0108333
	minimum = 0.0074 (at node 36)
	maximum = 0.015 (at node 128)
Injected flit rate average = 0.200997
	minimum = 0.0958 (at node 64)
	maximum = 0.269 (at node 28)
Accepted flit rate average= 0.195944
	minimum = 0.1362 (at node 36)
	maximum = 0.2694 (at node 128)
Injected packet length average = 17.9897
Accepted packet length average = 18.0871
Total in-flight flits = 16802 (16612 measured)
latency change    = 0.0683434
throughput change = 0.00233113
Class 0:
Packet latency average = 486.261
	minimum = 23
	maximum = 5725
Network latency average = 448.43
	minimum = 23
	maximum = 5725
Slowest packet = 7004
Flit latency average = 369.187
	minimum = 6
	maximum = 7077
Slowest flit = 67805
Fragmentation average = 208.512
	minimum = 0
	maximum = 3495
Injected packet rate average = 0.0111727
	minimum = 0.00666667 (at node 16)
	maximum = 0.0141667 (at node 28)
Accepted packet rate average = 0.0108273
	minimum = 0.00783333 (at node 135)
	maximum = 0.0143333 (at node 128)
Injected flit rate average = 0.200872
	minimum = 0.117667 (at node 16)
	maximum = 0.257167 (at node 162)
Accepted flit rate average= 0.195894
	minimum = 0.145333 (at node 135)
	maximum = 0.259667 (at node 128)
Injected packet length average = 17.9788
Accepted packet length average = 18.0927
Total in-flight flits = 17848 (17745 measured)
latency change    = 0.0531407
throughput change = 0.000253467
Class 0:
Packet latency average = 511.61
	minimum = 23
	maximum = 6377
Network latency average = 464.095
	minimum = 23
	maximum = 6377
Slowest packet = 7075
Flit latency average = 378.066
	minimum = 6
	maximum = 7133
Slowest flit = 105083
Fragmentation average = 213.656
	minimum = 0
	maximum = 4761
Injected packet rate average = 0.0111429
	minimum = 0.00714286 (at node 41)
	maximum = 0.014 (at node 28)
Accepted packet rate average = 0.0108564
	minimum = 0.00814286 (at node 64)
	maximum = 0.0141429 (at node 128)
Injected flit rate average = 0.200435
	minimum = 0.128 (at node 41)
	maximum = 0.250429 (at node 28)
Accepted flit rate average= 0.196181
	minimum = 0.147286 (at node 86)
	maximum = 0.255286 (at node 128)
Injected packet length average = 17.9877
Accepted packet length average = 18.0705
Total in-flight flits = 17777 (17777 measured)
latency change    = 0.0495464
throughput change = 0.00146144
Draining all recorded packets ...
Class 0:
Remaining flits: 157739 157740 157741 157742 157743 157744 157745 157746 157747 157748 [...] (19045 flits)
Measured flits: 157739 157740 157741 157742 157743 157744 157745 157746 157747 157748 [...] (7237 flits)
Class 0:
Remaining flits: 157739 157740 157741 157742 157743 157744 157745 157746 157747 157748 [...] (18625 flits)
Measured flits: 157739 157740 157741 157742 157743 157744 157745 157746 157747 157748 [...] (3350 flits)
Class 0:
Remaining flits: 164538 164539 164540 164541 164542 164543 164544 164545 164546 164547 [...] (19371 flits)
Measured flits: 164538 164539 164540 164541 164542 164543 164544 164545 164546 164547 [...] (2006 flits)
Class 0:
Remaining flits: 175176 175177 175178 175179 175180 175181 175182 175183 175184 175185 [...] (19083 flits)
Measured flits: 175176 175177 175178 175179 175180 175181 175182 175183 175184 175185 [...] (1099 flits)
Class 0:
Remaining flits: 206439 206440 206441 217188 217189 217190 217191 217192 217193 217194 [...] (20206 flits)
Measured flits: 206439 206440 206441 217188 217189 217190 217191 217192 217193 217194 [...] (615 flits)
Class 0:
Remaining flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (22688 flits)
Measured flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (451 flits)
Class 0:
Remaining flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (21060 flits)
Measured flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (338 flits)
Class 0:
Remaining flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (21732 flits)
Measured flits: 217188 217189 217190 217191 217192 217193 217194 217195 217196 217197 [...] (195 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (21704 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (102 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (21496 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (63 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (21094 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (33 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (22484 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (31 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (22529 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (31 flits)
Class 0:
Remaining flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (21973 flits)
Measured flits: 239747 239748 239749 239750 239751 239752 239753 239754 239755 239756 [...] (13 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 777636 777637 777638 777639 777640 777641 777642 777643 777644 777645 [...] (755 flits)
Measured flits: (0 flits)
Time taken is 26314 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 672.049 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18640 (1 samples)
Network latency average = 593.482 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18631 (1 samples)
Flit latency average = 495.722 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18614 (1 samples)
Fragmentation average = 242.731 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8383 (1 samples)
Injected packet rate average = 0.0111429 (1 samples)
	minimum = 0.00714286 (1 samples)
	maximum = 0.014 (1 samples)
Accepted packet rate average = 0.0108564 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0141429 (1 samples)
Injected flit rate average = 0.200435 (1 samples)
	minimum = 0.128 (1 samples)
	maximum = 0.250429 (1 samples)
Accepted flit rate average = 0.196181 (1 samples)
	minimum = 0.147286 (1 samples)
	maximum = 0.255286 (1 samples)
Injected packet size average = 17.9877 (1 samples)
Accepted packet size average = 18.0705 (1 samples)
Hops average = 5.06439 (1 samples)
Total run time 27.3968
