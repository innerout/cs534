BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.992
	minimum = 22
	maximum = 961
Network latency average = 237.321
	minimum = 22
	maximum = 738
Slowest packet = 46
Flit latency average = 214.475
	minimum = 5
	maximum = 779
Slowest flit = 19478
Fragmentation average = 19.5274
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0289167
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0138542
	minimum = 0.007 (at node 28)
	maximum = 0.023 (at node 48)
Injected flit rate average = 0.515651
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.254932
	minimum = 0.126 (at node 64)
	maximum = 0.414 (at node 48)
Injected packet length average = 17.8323
Accepted packet length average = 18.4011
Total in-flight flits = 51061 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.637
	minimum = 22
	maximum = 1944
Network latency average = 463.794
	minimum = 22
	maximum = 1508
Slowest packet = 46
Flit latency average = 440.592
	minimum = 5
	maximum = 1503
Slowest flit = 45490
Fragmentation average = 21.2573
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0291094
	minimum = 0.007 (at node 179)
	maximum = 0.0545 (at node 175)
Accepted packet rate average = 0.0146068
	minimum = 0.0095 (at node 30)
	maximum = 0.021 (at node 51)
Injected flit rate average = 0.521898
	minimum = 0.126 (at node 179)
	maximum = 0.9725 (at node 175)
Accepted flit rate average= 0.26582
	minimum = 0.171 (at node 164)
	maximum = 0.378 (at node 51)
Injected packet length average = 17.9289
Accepted packet length average = 18.1984
Total in-flight flits = 99777 (0 measured)
latency change    = 0.439813
throughput change = 0.0409601
Class 0:
Packet latency average = 1432.19
	minimum = 25
	maximum = 2858
Network latency average = 1166.29
	minimum = 22
	maximum = 2239
Slowest packet = 3578
Flit latency average = 1147.19
	minimum = 5
	maximum = 2301
Slowest flit = 65959
Fragmentation average = 20.8789
	minimum = 0
	maximum = 194
Injected packet rate average = 0.022375
	minimum = 0.001 (at node 93)
	maximum = 0.055 (at node 95)
Accepted packet rate average = 0.0150573
	minimum = 0.007 (at node 5)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.402625
	minimum = 0.018 (at node 93)
	maximum = 0.987 (at node 95)
Accepted flit rate average= 0.2705
	minimum = 0.126 (at node 5)
	maximum = 0.513 (at node 16)
Injected packet length average = 17.9944
Accepted packet length average = 17.9647
Total in-flight flits = 125691 (0 measured)
latency change    = 0.568746
throughput change = 0.0173001
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 736.397
	minimum = 22
	maximum = 2846
Network latency average = 73.8547
	minimum = 22
	maximum = 758
Slowest packet = 15544
Flit latency average = 1595.3
	minimum = 5
	maximum = 2940
Slowest flit = 99396
Fragmentation average = 4.51955
	minimum = 0
	maximum = 20
Injected packet rate average = 0.019651
	minimum = 0.002 (at node 55)
	maximum = 0.053 (at node 191)
Accepted packet rate average = 0.0150313
	minimum = 0.005 (at node 22)
	maximum = 0.025 (at node 78)
Injected flit rate average = 0.354417
	minimum = 0.036 (at node 55)
	maximum = 0.941 (at node 191)
Accepted flit rate average= 0.27012
	minimum = 0.087 (at node 22)
	maximum = 0.45 (at node 78)
Injected packet length average = 18.0355
Accepted packet length average = 17.9705
Total in-flight flits = 142264 (65136 measured)
latency change    = 0.94486
throughput change = 0.00140755
Class 0:
Packet latency average = 1552.22
	minimum = 22
	maximum = 4413
Network latency average = 833.6
	minimum = 22
	maximum = 1944
Slowest packet = 15544
Flit latency average = 1795.49
	minimum = 5
	maximum = 3805
Slowest flit = 120347
Fragmentation average = 11.2585
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0180938
	minimum = 0.0055 (at node 127)
	maximum = 0.0335 (at node 3)
Accepted packet rate average = 0.0149948
	minimum = 0.009 (at node 36)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.326453
	minimum = 0.099 (at node 127)
	maximum = 0.603 (at node 3)
Accepted flit rate average= 0.269742
	minimum = 0.162 (at node 36)
	maximum = 0.372 (at node 178)
Injected packet length average = 18.0423
Accepted packet length average = 17.9891
Total in-flight flits = 148182 (111960 measured)
latency change    = 0.525585
throughput change = 0.00139987
Class 0:
Packet latency average = 2308.22
	minimum = 22
	maximum = 5217
Network latency average = 1450.19
	minimum = 22
	maximum = 2990
Slowest packet = 15544
Flit latency average = 1978.21
	minimum = 5
	maximum = 4551
Slowest flit = 138888
Fragmentation average = 15.3854
	minimum = 0
	maximum = 245
Injected packet rate average = 0.0172083
	minimum = 0.008 (at node 127)
	maximum = 0.029 (at node 191)
Accepted packet rate average = 0.014934
	minimum = 0.0106667 (at node 114)
	maximum = 0.0196667 (at node 138)
Injected flit rate average = 0.309908
	minimum = 0.144 (at node 127)
	maximum = 0.522333 (at node 191)
Accepted flit rate average= 0.268835
	minimum = 0.192 (at node 114)
	maximum = 0.352667 (at node 138)
Injected packet length average = 18.0092
Accepted packet length average = 18.0015
Total in-flight flits = 149942 (137732 measured)
latency change    = 0.327523
throughput change = 0.00337425
Class 0:
Packet latency average = 2906.8
	minimum = 22
	maximum = 5929
Network latency average = 1903.67
	minimum = 22
	maximum = 3929
Slowest packet = 15544
Flit latency average = 2139.67
	minimum = 5
	maximum = 5051
Slowest flit = 187946
Fragmentation average = 16.8584
	minimum = 0
	maximum = 245
Injected packet rate average = 0.016763
	minimum = 0.0085 (at node 62)
	maximum = 0.028 (at node 113)
Accepted packet rate average = 0.0149297
	minimum = 0.011 (at node 161)
	maximum = 0.01925 (at node 138)
Injected flit rate average = 0.301969
	minimum = 0.15475 (at node 62)
	maximum = 0.5055 (at node 113)
Accepted flit rate average= 0.268602
	minimum = 0.198 (at node 161)
	maximum = 0.34975 (at node 138)
Injected packet length average = 18.014
Accepted packet length average = 17.9911
Total in-flight flits = 151983 (150134 measured)
latency change    = 0.205925
throughput change = 0.000869343
Class 0:
Packet latency average = 3399.63
	minimum = 22
	maximum = 7363
Network latency average = 2220.73
	minimum = 22
	maximum = 4756
Slowest packet = 15544
Flit latency average = 2268.63
	minimum = 5
	maximum = 5480
Slowest flit = 172187
Fragmentation average = 17.8252
	minimum = 0
	maximum = 245
Injected packet rate average = 0.0164063
	minimum = 0.0084 (at node 44)
	maximum = 0.0262 (at node 134)
Accepted packet rate average = 0.014951
	minimum = 0.0114 (at node 104)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.295543
	minimum = 0.1506 (at node 44)
	maximum = 0.4692 (at node 134)
Accepted flit rate average= 0.26905
	minimum = 0.2036 (at node 104)
	maximum = 0.3428 (at node 138)
Injected packet length average = 18.014
Accepted packet length average = 17.9954
Total in-flight flits = 151749 (151713 measured)
latency change    = 0.144966
throughput change = 0.00166674
Class 0:
Packet latency average = 3801.81
	minimum = 22
	maximum = 8312
Network latency average = 2416.53
	minimum = 22
	maximum = 5361
Slowest packet = 15544
Flit latency average = 2371.55
	minimum = 5
	maximum = 5480
Slowest flit = 172187
Fragmentation average = 18.338
	minimum = 0
	maximum = 245
Injected packet rate average = 0.0160521
	minimum = 0.0101667 (at node 40)
	maximum = 0.0256667 (at node 134)
Accepted packet rate average = 0.0149193
	minimum = 0.0116667 (at node 171)
	maximum = 0.0188333 (at node 128)
Injected flit rate average = 0.28903
	minimum = 0.181 (at node 40)
	maximum = 0.462 (at node 134)
Accepted flit rate average= 0.268464
	minimum = 0.21 (at node 171)
	maximum = 0.336667 (at node 128)
Injected packet length average = 18.0057
Accepted packet length average = 17.9944
Total in-flight flits = 150213 (150213 measured)
latency change    = 0.105787
throughput change = 0.0021845
Class 0:
Packet latency average = 4153.7
	minimum = 22
	maximum = 8778
Network latency average = 2525.33
	minimum = 22
	maximum = 5674
Slowest packet = 15544
Flit latency average = 2444.73
	minimum = 5
	maximum = 5657
Slowest flit = 320543
Fragmentation average = 18.8056
	minimum = 0
	maximum = 245
Injected packet rate average = 0.015939
	minimum = 0.00942857 (at node 62)
	maximum = 0.0238571 (at node 134)
Accepted packet rate average = 0.0149115
	minimum = 0.0108571 (at node 171)
	maximum = 0.019 (at node 138)
Injected flit rate average = 0.28713
	minimum = 0.170714 (at node 62)
	maximum = 0.427 (at node 134)
Accepted flit rate average= 0.268371
	minimum = 0.197714 (at node 171)
	maximum = 0.342286 (at node 138)
Injected packet length average = 18.0143
Accepted packet length average = 17.9976
Total in-flight flits = 151695 (151695 measured)
latency change    = 0.0847178
throughput change = 0.000346558
Draining all recorded packets ...
Class 0:
Remaining flits: 410814 410815 410816 410817 410818 410819 410820 410821 410822 410823 [...] (154025 flits)
Measured flits: 410814 410815 410816 410817 410818 410819 410820 410821 410822 410823 [...] (153611 flits)
Class 0:
Remaining flits: 427500 427501 427502 427503 427504 427505 427506 427507 427508 427509 [...] (152648 flits)
Measured flits: 427500 427501 427502 427503 427504 427505 427506 427507 427508 427509 [...] (151496 flits)
Class 0:
Remaining flits: 503946 503947 503948 503949 503950 503951 503952 503953 503954 503955 [...] (150467 flits)
Measured flits: 503946 503947 503948 503949 503950 503951 503952 503953 503954 503955 [...] (148055 flits)
Class 0:
Remaining flits: 547416 547417 547418 547419 547420 547421 547422 547423 547424 547425 [...] (150239 flits)
Measured flits: 547416 547417 547418 547419 547420 547421 547422 547423 547424 547425 [...] (145319 flits)
Class 0:
Remaining flits: 601416 601417 601418 601419 601420 601421 601422 601423 601424 601425 [...] (150168 flits)
Measured flits: 601416 601417 601418 601419 601420 601421 601422 601423 601424 601425 [...] (140897 flits)
Class 0:
Remaining flits: 644094 644095 644096 644097 644098 644099 644100 644101 644102 644103 [...] (148440 flits)
Measured flits: 644094 644095 644096 644097 644098 644099 644100 644101 644102 644103 [...] (132137 flits)
Class 0:
Remaining flits: 691092 691093 691094 691095 691096 691097 691098 691099 691100 691101 [...] (145989 flits)
Measured flits: 691092 691093 691094 691095 691096 691097 691098 691099 691100 691101 [...] (123102 flits)
Class 0:
Remaining flits: 721998 721999 722000 722001 722002 722003 722004 722005 722006 722007 [...] (145575 flits)
Measured flits: 721998 721999 722000 722001 722002 722003 722004 722005 722006 722007 [...] (114119 flits)
Class 0:
Remaining flits: 744030 744031 744032 744033 744034 744035 744036 744037 744038 744039 [...] (143304 flits)
Measured flits: 744030 744031 744032 744033 744034 744035 744036 744037 744038 744039 [...] (103414 flits)
Class 0:
Remaining flits: 775116 775117 775118 775119 775120 775121 775122 775123 775124 775125 [...] (141388 flits)
Measured flits: 775116 775117 775118 775119 775120 775121 775122 775123 775124 775125 [...] (94013 flits)
Class 0:
Remaining flits: 897372 897373 897374 897375 897376 897377 897378 897379 897380 897381 [...] (142211 flits)
Measured flits: 897372 897373 897374 897375 897376 897377 897378 897379 897380 897381 [...] (85313 flits)
Class 0:
Remaining flits: 908784 908785 908786 908787 908788 908789 908790 908791 908792 908793 [...] (143028 flits)
Measured flits: 908784 908785 908786 908787 908788 908789 908790 908791 908792 908793 [...] (77991 flits)
Class 0:
Remaining flits: 923436 923437 923438 923439 923440 923441 923442 923443 923444 923445 [...] (141271 flits)
Measured flits: 923436 923437 923438 923439 923440 923441 923442 923443 923444 923445 [...] (70470 flits)
Class 0:
Remaining flits: 1012060 1012061 1012062 1012063 1012064 1012065 1012066 1012067 1020870 1020871 [...] (142132 flits)
Measured flits: 1012060 1012061 1012062 1012063 1012064 1012065 1012066 1012067 1020870 1020871 [...] (62897 flits)
Class 0:
Remaining flits: 1050771 1050772 1050773 1050774 1050775 1050776 1050777 1050778 1050779 1050780 [...] (142049 flits)
Measured flits: 1050771 1050772 1050773 1050774 1050775 1050776 1050777 1050778 1050779 1050780 [...] (52202 flits)
Class 0:
Remaining flits: 1089864 1089865 1089866 1089867 1089868 1089869 1089870 1089871 1089872 1089873 [...] (145111 flits)
Measured flits: 1097190 1097191 1097192 1097193 1097194 1097195 1097196 1097197 1097198 1097199 [...] (43678 flits)
Class 0:
Remaining flits: 1169136 1169137 1169138 1169139 1169140 1169141 1169142 1169143 1169144 1169145 [...] (143653 flits)
Measured flits: 1189656 1189657 1189658 1189659 1189660 1189661 1189662 1189663 1189664 1189665 [...] (36743 flits)
Class 0:
Remaining flits: 1226034 1226035 1226036 1226037 1226038 1226039 1226040 1226041 1226042 1226043 [...] (141749 flits)
Measured flits: 1226034 1226035 1226036 1226037 1226038 1226039 1226040 1226041 1226042 1226043 [...] (30435 flits)
Class 0:
Remaining flits: 1245330 1245331 1245332 1245333 1245334 1245335 1245336 1245337 1245338 1245339 [...] (146370 flits)
Measured flits: 1311102 1311103 1311104 1311105 1311106 1311107 1311108 1311109 1311110 1311111 [...] (26261 flits)
Class 0:
Remaining flits: 1262466 1262467 1262468 1262469 1262470 1262471 1262472 1262473 1262474 1262475 [...] (146846 flits)
Measured flits: 1355832 1355833 1355834 1355835 1355836 1355837 1355838 1355839 1355840 1355841 [...] (21716 flits)
Class 0:
Remaining flits: 1370070 1370071 1370072 1370073 1370074 1370075 1370076 1370077 1370078 1370079 [...] (147668 flits)
Measured flits: 1373400 1373401 1373402 1373403 1373404 1373405 1373406 1373407 1373408 1373409 [...] (16910 flits)
Class 0:
Remaining flits: 1449828 1449829 1449830 1449831 1449832 1449833 1449834 1449835 1449836 1449837 [...] (147300 flits)
Measured flits: 1449828 1449829 1449830 1449831 1449832 1449833 1449834 1449835 1449836 1449837 [...] (14356 flits)
Class 0:
Remaining flits: 1531902 1531903 1531904 1531905 1531906 1531907 1544940 1544941 1544942 1544943 [...] (149024 flits)
Measured flits: 1531902 1531903 1531904 1531905 1531906 1531907 1544940 1544941 1544942 1544943 [...] (10806 flits)
Class 0:
Remaining flits: 1586502 1586503 1586504 1586505 1586506 1586507 1586508 1586509 1586510 1586511 [...] (148689 flits)
Measured flits: 1586502 1586503 1586504 1586505 1586506 1586507 1586508 1586509 1586510 1586511 [...] (7535 flits)
Class 0:
Remaining flits: 1592292 1592293 1592294 1592295 1592296 1592297 1665000 1665001 1665002 1665003 [...] (147445 flits)
Measured flits: 1592292 1592293 1592294 1592295 1592296 1592297 1707876 1707877 1707878 1707879 [...] (5853 flits)
Class 0:
Remaining flits: 1731600 1731601 1731602 1731603 1731604 1731605 1731606 1731607 1731608 1731609 [...] (148627 flits)
Measured flits: 1756458 1756459 1756460 1756461 1756462 1756463 1756464 1756465 1756466 1756467 [...] (4392 flits)
Class 0:
Remaining flits: 1755738 1755739 1755740 1755741 1755742 1755743 1755744 1755745 1755746 1755747 [...] (145911 flits)
Measured flits: 1764515 1764516 1764517 1764518 1764519 1764520 1764521 1786158 1786159 1786160 [...] (2907 flits)
Class 0:
Remaining flits: 1796364 1796365 1796366 1796367 1796368 1796369 1796370 1796371 1796372 1796373 [...] (146091 flits)
Measured flits: 1824840 1824841 1824842 1824843 1824844 1824845 1824846 1824847 1824848 1824849 [...] (1715 flits)
Class 0:
Remaining flits: 1821834 1821835 1821836 1821837 1821838 1821839 1821840 1821841 1821842 1821843 [...] (145836 flits)
Measured flits: 1926900 1926901 1926902 1926903 1926904 1926905 1926906 1926907 1926908 1926909 [...] (1116 flits)
Class 0:
Remaining flits: 1824678 1824679 1824680 1824681 1824682 1824683 1824684 1824685 1824686 1824687 [...] (142787 flits)
Measured flits: 1967454 1967455 1967456 1967457 1967458 1967459 1967460 1967461 1967462 1967463 [...] (968 flits)
Class 0:
Remaining flits: 1864944 1864945 1864946 1864947 1864948 1864949 1864950 1864951 1864952 1864953 [...] (141291 flits)
Measured flits: 2019690 2019691 2019692 2019693 2019694 2019695 2019696 2019697 2019698 2019699 [...] (726 flits)
Class 0:
Remaining flits: 1891332 1891333 1891334 1891335 1891336 1891337 1891338 1891339 1891340 1891341 [...] (144718 flits)
Measured flits: 2051676 2051677 2051678 2051679 2051680 2051681 2051682 2051683 2051684 2051685 [...] (414 flits)
Class 0:
Remaining flits: 2007504 2007505 2007506 2007507 2007508 2007509 2007510 2007511 2007512 2007513 [...] (142566 flits)
Measured flits: 2169108 2169109 2169110 2169111 2169112 2169113 2169114 2169115 2169116 2169117 [...] (90 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2090232 2090233 2090234 2090235 2090236 2090237 2090238 2090239 2090240 2090241 [...] (94263 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2140200 2140201 2140202 2140203 2140204 2140205 2140206 2140207 2140208 2140209 [...] (46416 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2145600 2145601 2145602 2145603 2145604 2145605 2145606 2145607 2145608 2145609 [...] (7893 flits)
Measured flits: (0 flits)
Time taken is 47798 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9907.71 (1 samples)
	minimum = 22 (1 samples)
	maximum = 34359 (1 samples)
Network latency average = 2806.03 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8724 (1 samples)
Flit latency average = 2754.89 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8707 (1 samples)
Fragmentation average = 20.2536 (1 samples)
	minimum = 0 (1 samples)
	maximum = 245 (1 samples)
Injected packet rate average = 0.015939 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0238571 (1 samples)
Accepted packet rate average = 0.0149115 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.28713 (1 samples)
	minimum = 0.170714 (1 samples)
	maximum = 0.427 (1 samples)
Accepted flit rate average = 0.268371 (1 samples)
	minimum = 0.197714 (1 samples)
	maximum = 0.342286 (1 samples)
Injected packet size average = 18.0143 (1 samples)
Accepted packet size average = 17.9976 (1 samples)
Hops average = 5.04989 (1 samples)
Total run time 50.3164
