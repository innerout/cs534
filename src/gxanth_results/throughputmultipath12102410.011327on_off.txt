BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 224.575
	minimum = 27
	maximum = 928
Network latency average = 180.389
	minimum = 23
	maximum = 801
Slowest packet = 72
Flit latency average = 141.175
	minimum = 6
	maximum = 820
Slowest flit = 8507
Fragmentation average = 49.7856
	minimum = 0
	maximum = 137
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00826042
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 165)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.155359
	minimum = 0.036 (at node 174)
	maximum = 0.288 (at node 165)
Injected packet length average = 17.8411
Accepted packet length average = 18.8077
Total in-flight flits = 11301 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 323.163
	minimum = 27
	maximum = 1566
Network latency average = 274.966
	minimum = 23
	maximum = 1347
Slowest packet = 672
Flit latency average = 231.708
	minimum = 6
	maximum = 1379
Slowest flit = 25141
Fragmentation average = 57.9249
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00869792
	minimum = 0.0045 (at node 115)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.160044
	minimum = 0.083 (at node 115)
	maximum = 0.252 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 18.4003
Total in-flight flits = 18139 (0 measured)
latency change    = 0.305072
throughput change = 0.0292725
Class 0:
Packet latency average = 566.768
	minimum = 23
	maximum = 2207
Network latency average = 518.34
	minimum = 23
	maximum = 1988
Slowest packet = 1396
Flit latency average = 471.834
	minimum = 6
	maximum = 2027
Slowest flit = 39799
Fragmentation average = 68.4002
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.00931771
	minimum = 0.003 (at node 18)
	maximum = 0.018 (at node 6)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.168052
	minimum = 0.054 (at node 18)
	maximum = 0.306 (at node 6)
Injected packet length average = 17.9936
Accepted packet length average = 18.0358
Total in-flight flits = 24969 (0 measured)
latency change    = 0.429815
throughput change = 0.0476508
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 360.079
	minimum = 23
	maximum = 1097
Network latency average = 313.008
	minimum = 23
	maximum = 956
Slowest packet = 6604
Flit latency average = 617.594
	minimum = 6
	maximum = 2919
Slowest flit = 41723
Fragmentation average = 62.1057
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.00957292
	minimum = 0.002 (at node 4)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.171859
	minimum = 0.048 (at node 163)
	maximum = 0.391 (at node 16)
Injected packet length average = 18.0247
Accepted packet length average = 17.9527
Total in-flight flits = 32724 (25708 measured)
latency change    = 0.57401
throughput change = 0.0221535
Class 0:
Packet latency average = 575.336
	minimum = 23
	maximum = 2097
Network latency average = 528.667
	minimum = 23
	maximum = 1931
Slowest packet = 6596
Flit latency average = 703.448
	minimum = 6
	maximum = 3425
Slowest flit = 62290
Fragmentation average = 65.9004
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.00948698
	minimum = 0.004 (at node 17)
	maximum = 0.0165 (at node 120)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.170836
	minimum = 0.07 (at node 17)
	maximum = 0.2935 (at node 120)
Injected packet length average = 17.9755
Accepted packet length average = 18.0074
Total in-flight flits = 39486 (37610 measured)
latency change    = 0.374142
throughput change = 0.00599076
Class 0:
Packet latency average = 750.278
	minimum = 23
	maximum = 2984
Network latency average = 701.122
	minimum = 23
	maximum = 2849
Slowest packet = 6596
Flit latency average = 784.25
	minimum = 6
	maximum = 4205
Slowest flit = 66185
Fragmentation average = 66.6152
	minimum = 0
	maximum = 156
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.00950347
	minimum = 0.00466667 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.171156
	minimum = 0.084 (at node 4)
	maximum = 0.248333 (at node 16)
Injected packet length average = 18.0002
Accepted packet length average = 18.0099
Total in-flight flits = 43293 (42556 measured)
latency change    = 0.23317
throughput change = 0.00187146
Draining remaining packets ...
Class 0:
Remaining flits: 73818 73819 73820 73821 73822 73823 73824 73825 73826 73827 [...] (15839 flits)
Measured flits: 120582 120583 120584 120585 120586 120587 120588 120589 120590 120591 [...] (15626 flits)
Class 0:
Remaining flits: 131472 131473 131474 131475 131476 131477 131478 131479 131480 131481 [...] (2328 flits)
Measured flits: 131472 131473 131474 131475 131476 131477 131478 131479 131480 131481 [...] (2328 flits)
Time taken is 8865 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1171.24 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4862 (1 samples)
Network latency average = 1116.35 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4813 (1 samples)
Flit latency average = 1100.09 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5340 (1 samples)
Fragmentation average = 65.9966 (1 samples)
	minimum = 0 (1 samples)
	maximum = 156 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.00950347 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.171156 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.248333 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 18.0099 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 4.10599
