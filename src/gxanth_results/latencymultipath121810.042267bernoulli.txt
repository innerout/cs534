BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 379.189
	minimum = 23
	maximum = 931
Network latency average = 302.617
	minimum = 23
	maximum = 916
Slowest packet = 438
Flit latency average = 258.045
	minimum = 6
	maximum = 921
Slowest flit = 7374
Fragmentation average = 55.4126
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0139323
	minimum = 0.004 (at node 24)
	maximum = 0.023 (at node 87)
Accepted packet rate average = 0.0089375
	minimum = 0.001 (at node 41)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.246745
	minimum = 0.072 (at node 24)
	maximum = 0.414 (at node 87)
Accepted flit rate average= 0.167375
	minimum = 0.018 (at node 41)
	maximum = 0.332 (at node 76)
Injected packet length average = 17.7103
Accepted packet length average = 18.7273
Total in-flight flits = 17850 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 756.399
	minimum = 23
	maximum = 1883
Network latency average = 410.633
	minimum = 23
	maximum = 1678
Slowest packet = 767
Flit latency average = 357.919
	minimum = 6
	maximum = 1884
Slowest flit = 11302
Fragmentation average = 57.7891
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0114896
	minimum = 0.005 (at node 8)
	maximum = 0.019 (at node 103)
Accepted packet rate average = 0.0090026
	minimum = 0.005 (at node 79)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.205063
	minimum = 0.09 (at node 8)
	maximum = 0.336 (at node 103)
Accepted flit rate average= 0.165456
	minimum = 0.09 (at node 79)
	maximum = 0.267 (at node 44)
Injected packet length average = 17.8477
Accepted packet length average = 18.3787
Total in-flight flits = 17951 (0 measured)
latency change    = 0.498692
throughput change = 0.0115999
Class 0:
Packet latency average = 1931.03
	minimum = 907
	maximum = 2775
Network latency average = 564.875
	minimum = 23
	maximum = 2697
Slowest packet = 934
Flit latency average = 498.919
	minimum = 6
	maximum = 2663
Slowest flit = 16829
Fragmentation average = 58.6655
	minimum = 0
	maximum = 124
Injected packet rate average = 0.00876042
	minimum = 0 (at node 0)
	maximum = 0.019 (at node 109)
Accepted packet rate average = 0.00878125
	minimum = 0.001 (at node 121)
	maximum = 0.015 (at node 71)
Injected flit rate average = 0.15799
	minimum = 0 (at node 0)
	maximum = 0.345 (at node 116)
Accepted flit rate average= 0.158714
	minimum = 0.024 (at node 153)
	maximum = 0.295 (at node 98)
Injected packet length average = 18.0345
Accepted packet length average = 18.0741
Total in-flight flits = 17718 (0 measured)
latency change    = 0.608292
throughput change = 0.0424802
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2795.54
	minimum = 1747
	maximum = 3587
Network latency average = 330.994
	minimum = 23
	maximum = 929
Slowest packet = 6242
Flit latency average = 475.61
	minimum = 6
	maximum = 3081
Slowest flit = 43847
Fragmentation average = 51.8621
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00924479
	minimum = 0 (at node 144)
	maximum = 0.023 (at node 7)
Accepted packet rate average = 0.00932292
	minimum = 0.003 (at node 18)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.165635
	minimum = 0 (at node 144)
	maximum = 0.417 (at node 7)
Accepted flit rate average= 0.166969
	minimum = 0.04 (at node 91)
	maximum = 0.317 (at node 16)
Injected packet length average = 17.9166
Accepted packet length average = 17.9095
Total in-flight flits = 17394 (16062 measured)
latency change    = 0.309248
throughput change = 0.0494416
Class 0:
Packet latency average = 3276.74
	minimum = 1747
	maximum = 4423
Network latency average = 431.908
	minimum = 23
	maximum = 1708
Slowest packet = 6242
Flit latency average = 469.945
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 55.4822
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00929427
	minimum = 0.001 (at node 144)
	maximum = 0.019 (at node 66)
Accepted packet rate average = 0.00926823
	minimum = 0.0045 (at node 113)
	maximum = 0.0165 (at node 34)
Injected flit rate average = 0.167078
	minimum = 0.018 (at node 144)
	maximum = 0.3445 (at node 66)
Accepted flit rate average= 0.16663
	minimum = 0.079 (at node 113)
	maximum = 0.2945 (at node 34)
Injected packet length average = 17.9765
Accepted packet length average = 17.9786
Total in-flight flits = 17794 (17760 measured)
latency change    = 0.146853
throughput change = 0.00203169
Class 0:
Packet latency average = 3694.43
	minimum = 1747
	maximum = 5287
Network latency average = 480.17
	minimum = 23
	maximum = 2345
Slowest packet = 6242
Flit latency average = 476.125
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 56.9504
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00918576
	minimum = 0.00366667 (at node 149)
	maximum = 0.0183333 (at node 7)
Accepted packet rate average = 0.00920139
	minimum = 0.00533333 (at node 1)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.16504
	minimum = 0.0606667 (at node 149)
	maximum = 0.328 (at node 7)
Accepted flit rate average= 0.165
	minimum = 0.0913333 (at node 36)
	maximum = 0.272667 (at node 16)
Injected packet length average = 17.9669
Accepted packet length average = 17.9321
Total in-flight flits = 17808 (17808 measured)
latency change    = 0.11306
throughput change = 0.00988005
Class 0:
Packet latency average = 4093.9
	minimum = 1747
	maximum = 6268
Network latency average = 498.484
	minimum = 23
	maximum = 2403
Slowest packet = 6242
Flit latency average = 477.199
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 57.5462
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0091224
	minimum = 0.00475 (at node 123)
	maximum = 0.017 (at node 7)
Accepted packet rate average = 0.0091263
	minimum = 0.00525 (at node 36)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.16382
	minimum = 0.08525 (at node 123)
	maximum = 0.307 (at node 7)
Accepted flit rate average= 0.163951
	minimum = 0.091 (at node 36)
	maximum = 0.24125 (at node 16)
Injected packet length average = 17.958
Accepted packet length average = 17.9646
Total in-flight flits = 17498 (17498 measured)
latency change    = 0.0975755
throughput change = 0.00640119
Class 0:
Packet latency average = 4486.4
	minimum = 1747
	maximum = 7091
Network latency average = 509.473
	minimum = 23
	maximum = 2403
Slowest packet = 6242
Flit latency average = 478.903
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 57.6494
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00901458
	minimum = 0.0046 (at node 130)
	maximum = 0.0152 (at node 66)
Accepted packet rate average = 0.00905833
	minimum = 0.0058 (at node 132)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.162255
	minimum = 0.0806 (at node 130)
	maximum = 0.2722 (at node 66)
Accepted flit rate average= 0.162809
	minimum = 0.1044 (at node 132)
	maximum = 0.2366 (at node 129)
Injected packet length average = 17.9992
Accepted packet length average = 17.9734
Total in-flight flits = 17283 (17283 measured)
latency change    = 0.0874865
throughput change = 0.00700909
Class 0:
Packet latency average = 4883.24
	minimum = 1747
	maximum = 8000
Network latency average = 515.477
	minimum = 23
	maximum = 2845
Slowest packet = 6242
Flit latency average = 479.478
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 57.7356
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00902257
	minimum = 0.0055 (at node 22)
	maximum = 0.0141667 (at node 66)
Accepted packet rate average = 0.00902865
	minimum = 0.006 (at node 36)
	maximum = 0.0121667 (at node 128)
Injected flit rate average = 0.162388
	minimum = 0.0995 (at node 141)
	maximum = 0.255833 (at node 66)
Accepted flit rate average= 0.162352
	minimum = 0.105667 (at node 36)
	maximum = 0.219333 (at node 128)
Injected packet length average = 17.998
Accepted packet length average = 17.9819
Total in-flight flits = 17762 (17762 measured)
latency change    = 0.0812661
throughput change = 0.00281452
Class 0:
Packet latency average = 5279.45
	minimum = 1747
	maximum = 8762
Network latency average = 523.671
	minimum = 23
	maximum = 2845
Slowest packet = 6242
Flit latency average = 483.279
	minimum = 6
	maximum = 3240
Slowest flit = 44549
Fragmentation average = 57.3456
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00902604
	minimum = 0.00542857 (at node 22)
	maximum = 0.013 (at node 66)
Accepted packet rate average = 0.00901711
	minimum = 0.00585714 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.162382
	minimum = 0.0977143 (at node 60)
	maximum = 0.234714 (at node 66)
Accepted flit rate average= 0.162234
	minimum = 0.105 (at node 36)
	maximum = 0.216857 (at node 128)
Injected packet length average = 17.9904
Accepted packet length average = 17.9917
Total in-flight flits = 17872 (17872 measured)
latency change    = 0.0750481
throughput change = 0.000732275
Draining all recorded packets ...
Class 0:
Remaining flits: 278711 280440 280441 280442 280443 280444 280445 280446 280447 280448 [...] (17380 flits)
Measured flits: 278711 280440 280441 280442 280443 280444 280445 280446 280447 280448 [...] (17380 flits)
Class 0:
Remaining flits: 328788 328789 328790 328791 328792 328793 328794 328795 328796 328797 [...] (17575 flits)
Measured flits: 328788 328789 328790 328791 328792 328793 328794 328795 328796 328797 [...] (17575 flits)
Class 0:
Remaining flits: 343008 343009 343010 343011 343012 343013 343014 343015 343016 343017 [...] (17969 flits)
Measured flits: 343008 343009 343010 343011 343012 343013 343014 343015 343016 343017 [...] (17969 flits)
Class 0:
Remaining flits: 344692 344693 344694 344695 344696 344697 344698 344699 366300 366301 [...] (17718 flits)
Measured flits: 344692 344693 344694 344695 344696 344697 344698 344699 366300 366301 [...] (17718 flits)
Class 0:
Remaining flits: 380298 380299 380300 380301 380302 380303 420566 420567 420568 420569 [...] (17697 flits)
Measured flits: 380298 380299 380300 380301 380302 380303 420566 420567 420568 420569 [...] (17697 flits)
Class 0:
Remaining flits: 458688 458689 458690 458691 458692 458693 462060 462061 462062 462063 [...] (17569 flits)
Measured flits: 458688 458689 458690 458691 458692 458693 462060 462061 462062 462063 [...] (17569 flits)
Class 0:
Remaining flits: 465192 465193 465194 465195 465196 465197 465198 465199 465200 465201 [...] (17389 flits)
Measured flits: 465192 465193 465194 465195 465196 465197 465198 465199 465200 465201 [...] (17389 flits)
Class 0:
Remaining flits: 502578 502579 502580 502581 502582 502583 502584 502585 502586 502587 [...] (17220 flits)
Measured flits: 502578 502579 502580 502581 502582 502583 502584 502585 502586 502587 [...] (17220 flits)
Class 0:
Remaining flits: 509166 509167 509168 509169 509170 509171 509172 509173 509174 509175 [...] (17558 flits)
Measured flits: 509166 509167 509168 509169 509170 509171 509172 509173 509174 509175 [...] (17558 flits)
Class 0:
Remaining flits: 549126 549127 549128 549129 549130 549131 549132 549133 549134 549135 [...] (17785 flits)
Measured flits: 549126 549127 549128 549129 549130 549131 549132 549133 549134 549135 [...] (17785 flits)
Class 0:
Remaining flits: 581076 581077 581078 581079 581080 581081 581082 581083 581084 581085 [...] (17894 flits)
Measured flits: 581076 581077 581078 581079 581080 581081 581082 581083 581084 581085 [...] (17894 flits)
Class 0:
Remaining flits: 634158 634159 634160 634161 634162 634163 634164 634165 634166 634167 [...] (17880 flits)
Measured flits: 634158 634159 634160 634161 634162 634163 634164 634165 634166 634167 [...] (17880 flits)
Class 0:
Remaining flits: 645768 645769 645770 645771 645772 645773 645774 645775 645776 645777 [...] (18257 flits)
Measured flits: 645768 645769 645770 645771 645772 645773 645774 645775 645776 645777 [...] (18257 flits)
Class 0:
Remaining flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (17609 flits)
Measured flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (17609 flits)
Class 0:
Remaining flits: 731214 731215 731216 731217 731218 731219 731220 731221 731222 731223 [...] (17950 flits)
Measured flits: 731214 731215 731216 731217 731218 731219 731220 731221 731222 731223 [...] (17950 flits)
Class 0:
Remaining flits: 746427 746428 746429 746430 746431 746432 746433 746434 746435 746436 [...] (17633 flits)
Measured flits: 746427 746428 746429 746430 746431 746432 746433 746434 746435 746436 [...] (17633 flits)
Class 0:
Remaining flits: 775113 775114 775115 778913 780786 780787 780788 780789 780790 780791 [...] (17704 flits)
Measured flits: 775113 775114 775115 778913 780786 780787 780788 780789 780790 780791 [...] (17704 flits)
Class 0:
Remaining flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (17732 flits)
Measured flits: 811350 811351 811352 811353 811354 811355 811356 811357 811358 811359 [...] (17732 flits)
Class 0:
Remaining flits: 854136 854137 854138 854139 854140 854141 854142 854143 854144 854145 [...] (17901 flits)
Measured flits: 854136 854137 854138 854139 854140 854141 854142 854143 854144 854145 [...] (17901 flits)
Class 0:
Remaining flits: 870580 870581 870582 870583 870584 870585 870586 870587 888786 888787 [...] (17320 flits)
Measured flits: 870580 870581 870582 870583 870584 870585 870586 870587 888786 888787 [...] (17320 flits)
Class 0:
Remaining flits: 914652 914653 914654 914655 914656 914657 914658 914659 914660 914661 [...] (17768 flits)
Measured flits: 914652 914653 914654 914655 914656 914657 914658 914659 914660 914661 [...] (17768 flits)
Class 0:
Remaining flits: 951992 951993 951994 951995 951996 951997 951998 951999 952000 952001 [...] (17379 flits)
Measured flits: 951992 951993 951994 951995 951996 951997 951998 951999 952000 952001 [...] (17379 flits)
Class 0:
Remaining flits: 969678 969679 969680 969681 969682 969683 969684 969685 969686 969687 [...] (17715 flits)
Measured flits: 969678 969679 969680 969681 969682 969683 969684 969685 969686 969687 [...] (17715 flits)
Class 0:
Remaining flits: 1000656 1000657 1000658 1000659 1000660 1000661 1000662 1000663 1000664 1000665 [...] (17352 flits)
Measured flits: 1000656 1000657 1000658 1000659 1000660 1000661 1000662 1000663 1000664 1000665 [...] (17352 flits)
Class 0:
Remaining flits: 1032048 1032049 1032050 1032051 1032052 1032053 1032054 1032055 1032056 1032057 [...] (18020 flits)
Measured flits: 1032048 1032049 1032050 1032051 1032052 1032053 1032054 1032055 1032056 1032057 [...] (18020 flits)
Class 0:
Remaining flits: 1064034 1064035 1064036 1064037 1064038 1064039 1064040 1064041 1064042 1064043 [...] (17477 flits)
Measured flits: 1064034 1064035 1064036 1064037 1064038 1064039 1064040 1064041 1064042 1064043 [...] (17477 flits)
Class 0:
Remaining flits: 1077767 1091052 1091053 1091054 1091055 1091056 1091057 1091058 1091059 1091060 [...] (17667 flits)
Measured flits: 1077767 1091052 1091053 1091054 1091055 1091056 1091057 1091058 1091059 1091060 [...] (17667 flits)
Class 0:
Remaining flits: 1106211 1106212 1106213 1106214 1106215 1106216 1106217 1106218 1106219 1106220 [...] (17907 flits)
Measured flits: 1106211 1106212 1106213 1106214 1106215 1106216 1106217 1106218 1106219 1106220 [...] (17817 flits)
Class 0:
Remaining flits: 1167912 1167913 1167914 1167915 1167916 1167917 1167918 1167919 1167920 1167921 [...] (17941 flits)
Measured flits: 1167912 1167913 1167914 1167915 1167916 1167917 1167918 1167919 1167920 1167921 [...] (17761 flits)
Class 0:
Remaining flits: 1200186 1200187 1200188 1200189 1200190 1200191 1200192 1200193 1200194 1200195 [...] (17843 flits)
Measured flits: 1200186 1200187 1200188 1200189 1200190 1200191 1200192 1200193 1200194 1200195 [...] (17312 flits)
Class 0:
Remaining flits: 1236312 1236313 1236314 1236315 1236316 1236317 1236318 1236319 1236320 1236321 [...] (17672 flits)
Measured flits: 1236312 1236313 1236314 1236315 1236316 1236317 1236318 1236319 1236320 1236321 [...] (16801 flits)
Class 0:
Remaining flits: 1247454 1247455 1247456 1247457 1247458 1247459 1247460 1247461 1247462 1247463 [...] (17545 flits)
Measured flits: 1247454 1247455 1247456 1247457 1247458 1247459 1247460 1247461 1247462 1247463 [...] (15914 flits)
Class 0:
Remaining flits: 1248840 1248841 1248842 1248843 1248844 1248845 1248846 1248847 1248848 1248849 [...] (17849 flits)
Measured flits: 1248840 1248841 1248842 1248843 1248844 1248845 1248846 1248847 1248848 1248849 [...] (15164 flits)
Class 0:
Remaining flits: 1310653 1310654 1310655 1310656 1310657 1310658 1310659 1310660 1310661 1310662 [...] (18132 flits)
Measured flits: 1310653 1310654 1310655 1310656 1310657 1310658 1310659 1310660 1310661 1310662 [...] (13758 flits)
Class 0:
Remaining flits: 1358748 1358749 1358750 1358751 1358752 1358753 1358754 1358755 1358756 1358757 [...] (17787 flits)
Measured flits: 1358748 1358749 1358750 1358751 1358752 1358753 1358754 1358755 1358756 1358757 [...] (12304 flits)
Class 0:
Remaining flits: 1388556 1388557 1388558 1388559 1388560 1388561 1388562 1388563 1388564 1388565 [...] (17966 flits)
Measured flits: 1388556 1388557 1388558 1388559 1388560 1388561 1388562 1388563 1388564 1388565 [...] (10480 flits)
Class 0:
Remaining flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (17600 flits)
Measured flits: 1395144 1395145 1395146 1395147 1395148 1395149 1395150 1395151 1395152 1395153 [...] (8015 flits)
Class 0:
Remaining flits: 1421839 1421840 1421841 1421842 1421843 1421844 1421845 1421846 1421847 1421848 [...] (18134 flits)
Measured flits: 1421839 1421840 1421841 1421842 1421843 1421844 1421845 1421846 1421847 1421848 [...] (6901 flits)
Class 0:
Remaining flits: 1487083 1487084 1487085 1487086 1487087 1488204 1488205 1488206 1488207 1488208 [...] (17759 flits)
Measured flits: 1491463 1491464 1491465 1491466 1491467 1491468 1491469 1491470 1491471 1491472 [...] (4518 flits)
Class 0:
Remaining flits: 1513746 1513747 1513748 1513749 1513750 1513751 1513752 1513753 1513754 1513755 [...] (18011 flits)
Measured flits: 1521684 1521685 1521686 1521687 1521688 1521689 1521690 1521691 1521692 1521693 [...] (3117 flits)
Class 0:
Remaining flits: 1529352 1529353 1529354 1529355 1529356 1529357 1529358 1529359 1529360 1529361 [...] (18412 flits)
Measured flits: 1556189 1560132 1560133 1560134 1560135 1560136 1560137 1560138 1560139 1560140 [...] (2463 flits)
Class 0:
Remaining flits: 1578214 1578215 1578216 1578217 1578218 1578219 1578220 1578221 1582038 1582039 [...] (17880 flits)
Measured flits: 1578214 1578215 1578216 1578217 1578218 1578219 1578220 1578221 1592568 1592569 [...] (1303 flits)
Class 0:
Remaining flits: 1608066 1608067 1608068 1608069 1608070 1608071 1608072 1608073 1608074 1608075 [...] (17817 flits)
Measured flits: 1621512 1621513 1621514 1621515 1621516 1621517 1621518 1621519 1621520 1621521 [...] (637 flits)
Class 0:
Remaining flits: 1641528 1641529 1641530 1641531 1641532 1641533 1641534 1641535 1641536 1641537 [...] (17811 flits)
Measured flits: 1653722 1653723 1653724 1653725 1653726 1653727 1653728 1653729 1653730 1653731 [...] (401 flits)
Class 0:
Remaining flits: 1665936 1665937 1665938 1665939 1665940 1665941 1665942 1665943 1665944 1665945 [...] (17476 flits)
Measured flits: 1708848 1708849 1708850 1708851 1708852 1708853 1708854 1708855 1708856 1708857 [...] (258 flits)
Class 0:
Remaining flits: 1684638 1684639 1684640 1684641 1684642 1684643 1684644 1684645 1684646 1684647 [...] (17701 flits)
Measured flits: 1728953 1730070 1730071 1730072 1730073 1730074 1730075 1730076 1730077 1730078 [...] (235 flits)
Class 0:
Remaining flits: 1719324 1719325 1719326 1719327 1719328 1719329 1719330 1719331 1719332 1719333 [...] (17894 flits)
Measured flits: 1760832 1760833 1760834 1760835 1760836 1760837 1760838 1760839 1760840 1760841 [...] (36 flits)
Draining remaining packets ...
Time taken is 58104 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 20136.1 (1 samples)
	minimum = 1747 (1 samples)
	maximum = 47247 (1 samples)
Network latency average = 546.285 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3577 (1 samples)
Flit latency average = 486.356 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3549 (1 samples)
Fragmentation average = 58.0587 (1 samples)
	minimum = 0 (1 samples)
	maximum = 175 (1 samples)
Injected packet rate average = 0.00902604 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.013 (1 samples)
Accepted packet rate average = 0.00901711 (1 samples)
	minimum = 0.00585714 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.162382 (1 samples)
	minimum = 0.0977143 (1 samples)
	maximum = 0.234714 (1 samples)
Accepted flit rate average = 0.162234 (1 samples)
	minimum = 0.105 (1 samples)
	maximum = 0.216857 (1 samples)
Injected packet size average = 17.9904 (1 samples)
Accepted packet size average = 17.9917 (1 samples)
Hops average = 5.06322 (1 samples)
Total run time 43.3988
