BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 344.283
	minimum = 23
	maximum = 944
Network latency average = 316.123
	minimum = 23
	maximum = 936
Slowest packet = 184
Flit latency average = 246.59
	minimum = 6
	maximum = 960
Slowest flit = 3306
Fragmentation average = 192.774
	minimum = 0
	maximum = 783
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0115937
	minimum = 0.005 (at node 61)
	maximum = 0.021 (at node 91)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.249323
	minimum = 0.128 (at node 185)
	maximum = 0.395 (at node 91)
Injected packet length average = 17.8388
Accepted packet length average = 21.5049
Total in-flight flits = 121114 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 607.028
	minimum = 23
	maximum = 1919
Network latency average = 564.898
	minimum = 23
	maximum = 1919
Slowest packet = 487
Flit latency average = 480.835
	minimum = 6
	maximum = 1915
Slowest flit = 9644
Fragmentation average = 252.707
	minimum = 0
	maximum = 1733
Injected packet rate average = 0.0506302
	minimum = 0.04 (at node 59)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0131146
	minimum = 0.0075 (at node 4)
	maximum = 0.0195 (at node 173)
Injected flit rate average = 0.907271
	minimum = 0.7125 (at node 59)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.262456
	minimum = 0.143 (at node 4)
	maximum = 0.395 (at node 173)
Injected packet length average = 17.9196
Accepted packet length average = 20.0125
Total in-flight flits = 249173 (0 measured)
latency change    = 0.432838
throughput change = 0.0500382
Class 0:
Packet latency average = 1284.84
	minimum = 25
	maximum = 2939
Network latency average = 1210.22
	minimum = 25
	maximum = 2939
Slowest packet = 346
Flit latency average = 1161.65
	minimum = 6
	maximum = 2922
Slowest flit = 6245
Fragmentation average = 333.804
	minimum = 1
	maximum = 2079
Injected packet rate average = 0.052125
	minimum = 0.041 (at node 43)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0155729
	minimum = 0.007 (at node 32)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.938458
	minimum = 0.732 (at node 43)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.283953
	minimum = 0.12 (at node 139)
	maximum = 0.51 (at node 115)
Injected packet length average = 18.004
Accepted packet length average = 18.2338
Total in-flight flits = 374798 (0 measured)
latency change    = 0.527547
throughput change = 0.0757076
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 186.083
	minimum = 25
	maximum = 713
Network latency average = 56.1252
	minimum = 23
	maximum = 669
Slowest packet = 29946
Flit latency average = 1697.43
	minimum = 6
	maximum = 3907
Slowest flit = 10002
Fragmentation average = 18.5784
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0529427
	minimum = 0.034 (at node 163)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.016026
	minimum = 0.004 (at node 139)
	maximum = 0.027 (at node 10)
Injected flit rate average = 0.952562
	minimum = 0.607 (at node 163)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287714
	minimum = 0.118 (at node 168)
	maximum = 0.475 (at node 10)
Injected packet length average = 17.9923
Accepted packet length average = 17.9529
Total in-flight flits = 502527 (167850 measured)
latency change    = 5.9047
throughput change = 0.01307
Class 0:
Packet latency average = 194.248
	minimum = 25
	maximum = 777
Network latency average = 56.8465
	minimum = 23
	maximum = 669
Slowest packet = 29946
Flit latency average = 1978.71
	minimum = 6
	maximum = 4766
Slowest flit = 18521
Fragmentation average = 18.065
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0530859
	minimum = 0.044 (at node 18)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0158802
	minimum = 0.0085 (at node 79)
	maximum = 0.0245 (at node 107)
Injected flit rate average = 0.955115
	minimum = 0.7975 (at node 18)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.284977
	minimum = 0.1655 (at node 79)
	maximum = 0.4155 (at node 107)
Injected packet length average = 17.9919
Accepted packet length average = 17.9454
Total in-flight flits = 632297 (336634 measured)
latency change    = 0.042036
throughput change = 0.00960423
Class 0:
Packet latency average = 204.492
	minimum = 25
	maximum = 2941
Network latency average = 66.3495
	minimum = 23
	maximum = 2764
Slowest packet = 30064
Flit latency average = 2264.3
	minimum = 6
	maximum = 5812
Slowest flit = 24191
Fragmentation average = 19.5549
	minimum = 0
	maximum = 555
Injected packet rate average = 0.0529236
	minimum = 0.0436667 (at node 124)
	maximum = 0.0556667 (at node 4)
Accepted packet rate average = 0.0157899
	minimum = 0.0106667 (at node 168)
	maximum = 0.0223333 (at node 107)
Injected flit rate average = 0.952755
	minimum = 0.785667 (at node 124)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.283974
	minimum = 0.197667 (at node 46)
	maximum = 0.390333 (at node 105)
Injected packet length average = 18.0025
Accepted packet length average = 17.9845
Total in-flight flits = 759941 (502610 measured)
latency change    = 0.0500962
throughput change = 0.00353062
Draining remaining packets ...
Class 0:
Remaining flits: 12852 12853 12854 12855 12856 12857 12858 12859 12860 12861 [...] (720708 flits)
Measured flits: 530100 530101 530102 530103 530104 530105 530106 530107 530108 530109 [...] (501035 flits)
Class 0:
Remaining flits: 18433 18434 18435 18436 18437 18438 18439 18440 18441 18442 [...] (683114 flits)
Measured flits: 530111 530112 530113 530114 530115 530116 530117 530118 530119 530120 [...] (498847 flits)
Class 0:
Remaining flits: 18433 18434 18435 18436 18437 18438 18439 18440 18441 18442 [...] (645771 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (494157 flits)
Class 0:
Remaining flits: 18433 18434 18435 18436 18437 18438 18439 18440 18441 18442 [...] (608952 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (485644 flits)
Class 0:
Remaining flits: 18436 18437 18438 18439 18440 18441 18442 18443 18444 18445 [...] (572255 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (472284 flits)
Class 0:
Remaining flits: 18436 18437 18438 18439 18440 18441 18442 18443 18444 18445 [...] (536096 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (454939 flits)
Class 0:
Remaining flits: 18436 18437 18438 18439 18440 18441 18442 18443 18444 18445 [...] (501017 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (434890 flits)
Class 0:
Remaining flits: 18436 18437 18438 18439 18440 18441 18442 18443 18444 18445 [...] (466142 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (412364 flits)
Class 0:
Remaining flits: 18436 18437 18438 18439 18440 18441 18442 18443 18444 18445 [...] (431371 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (387296 flits)
Class 0:
Remaining flits: 47880 47881 47882 47883 47884 47885 47886 47887 47888 47889 [...] (396888 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (360288 flits)
Class 0:
Remaining flits: 47880 47881 47882 47883 47884 47885 47886 47887 47888 47889 [...] (362951 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (332662 flits)
Class 0:
Remaining flits: 47880 47881 47882 47883 47884 47885 47886 47887 47888 47889 [...] (330271 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (304789 flits)
Class 0:
Remaining flits: 47880 47881 47882 47883 47884 47885 47886 47887 47888 47889 [...] (297511 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (275896 flits)
Class 0:
Remaining flits: 47880 47881 47882 47883 47884 47885 47886 47887 47888 47889 [...] (265231 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (246946 flits)
Class 0:
Remaining flits: 82188 82189 82190 82191 82192 82193 82194 82195 82196 82197 [...] (232598 flits)
Measured flits: 530118 530119 530120 530121 530122 530123 530124 530125 530126 530127 [...] (217341 flits)
Class 0:
Remaining flits: 82188 82189 82190 82191 82192 82193 82194 82195 82196 82197 [...] (200610 flits)
Measured flits: 530119 530120 530121 530122 530123 530124 530125 530126 530127 530128 [...] (188425 flits)
Class 0:
Remaining flits: 82188 82189 82190 82191 82192 82193 82194 82195 82196 82197 [...] (168097 flits)
Measured flits: 530532 530533 530534 530535 530536 530537 530538 530539 530540 530541 [...] (158342 flits)
Class 0:
Remaining flits: 95220 95221 95222 95223 95224 95225 95226 95227 95228 95229 [...] (135543 flits)
Measured flits: 530532 530533 530534 530535 530536 530537 530538 530539 530540 530541 [...] (128082 flits)
Class 0:
Remaining flits: 95220 95221 95222 95223 95224 95225 95226 95227 95228 95229 [...] (102907 flits)
Measured flits: 530532 530533 530534 530535 530536 530537 530538 530539 530540 530541 [...] (97177 flits)
Class 0:
Remaining flits: 101646 101647 101648 101649 101650 101651 101652 101653 101654 101655 [...] (70473 flits)
Measured flits: 530532 530533 530534 530535 530536 530537 530538 530539 530540 530541 [...] (66351 flits)
Class 0:
Remaining flits: 101646 101647 101648 101649 101650 101651 101652 101653 101654 101655 [...] (41055 flits)
Measured flits: 531072 531073 531074 531075 531076 531077 531078 531079 531080 531081 [...] (38618 flits)
Class 0:
Remaining flits: 101646 101647 101648 101649 101650 101651 101652 101653 101654 101655 [...] (16350 flits)
Measured flits: 531524 531525 531526 531527 531528 531529 531530 531531 531532 531533 [...] (15497 flits)
Class 0:
Remaining flits: 101646 101647 101648 101649 101650 101651 101652 101653 101654 101655 [...] (3751 flits)
Measured flits: 540536 540537 540538 540539 541188 541189 541190 541191 541192 541193 [...] (3554 flits)
Time taken is 29921 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14076.1 (1 samples)
	minimum = 25 (1 samples)
	maximum = 26594 (1 samples)
Network latency average = 13942.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26458 (1 samples)
Flit latency average = 11157.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 28865 (1 samples)
Fragmentation average = 234.368 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7456 (1 samples)
Injected packet rate average = 0.0529236 (1 samples)
	minimum = 0.0436667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0157899 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0223333 (1 samples)
Injected flit rate average = 0.952755 (1 samples)
	minimum = 0.785667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.283974 (1 samples)
	minimum = 0.197667 (1 samples)
	maximum = 0.390333 (1 samples)
Injected packet size average = 18.0025 (1 samples)
Accepted packet size average = 17.9845 (1 samples)
Hops average = 5.06036 (1 samples)
Total run time 45.0092
