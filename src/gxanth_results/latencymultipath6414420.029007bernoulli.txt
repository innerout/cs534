BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 271.444
	minimum = 22
	maximum = 746
Network latency average = 263.446
	minimum = 22
	maximum = 727
Slowest packet = 952
Flit latency average = 233.25
	minimum = 5
	maximum = 732
Slowest flit = 24240
Fragmentation average = 49.4692
	minimum = 0
	maximum = 535
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0137865
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.261542
	minimum = 0.095 (at node 174)
	maximum = 0.439 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.9709
Total in-flight flits = 49432 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.935
	minimum = 22
	maximum = 1427
Network latency average = 490.133
	minimum = 22
	maximum = 1397
Slowest packet = 2551
Flit latency average = 455.261
	minimum = 5
	maximum = 1470
Slowest flit = 51410
Fragmentation average = 62.9522
	minimum = 0
	maximum = 674
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0147083
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.272073
	minimum = 0.16 (at node 153)
	maximum = 0.402 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 18.4979
Total in-flight flits = 94586 (0 measured)
latency change    = 0.455953
throughput change = 0.0387075
Class 0:
Packet latency average = 1117.45
	minimum = 22
	maximum = 2042
Network latency average = 1108.64
	minimum = 22
	maximum = 2042
Slowest packet = 5037
Flit latency average = 1075.71
	minimum = 5
	maximum = 2127
Slowest flit = 84920
Fragmentation average = 86.6457
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0287708
	minimum = 0.016 (at node 65)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0158021
	minimum = 0.007 (at node 113)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.518208
	minimum = 0.288 (at node 65)
	maximum = 0.818 (at node 69)
Accepted flit rate average= 0.287208
	minimum = 0.119 (at node 113)
	maximum = 0.506 (at node 34)
Injected packet length average = 18.0116
Accepted packet length average = 18.1753
Total in-flight flits = 138874 (0 measured)
latency change    = 0.553507
throughput change = 0.0526984
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.1894
	minimum = 22
	maximum = 136
Network latency average = 35.2511
	minimum = 22
	maximum = 67
Slowest packet = 16634
Flit latency average = 1478.81
	minimum = 5
	maximum = 2898
Slowest flit = 99989
Fragmentation average = 6.66079
	minimum = 0
	maximum = 33
Injected packet rate average = 0.029625
	minimum = 0.013 (at node 119)
	maximum = 0.045 (at node 150)
Accepted packet rate average = 0.0161094
	minimum = 0.007 (at node 162)
	maximum = 0.026 (at node 128)
Injected flit rate average = 0.532229
	minimum = 0.234 (at node 119)
	maximum = 0.815 (at node 150)
Accepted flit rate average= 0.289687
	minimum = 0.141 (at node 162)
	maximum = 0.454 (at node 128)
Injected packet length average = 17.9655
Accepted packet length average = 17.9825
Total in-flight flits = 185638 (94049 measured)
latency change    = 23.7282
throughput change = 0.00855807
Class 0:
Packet latency average = 54.8603
	minimum = 22
	maximum = 1955
Network latency average = 45.4145
	minimum = 22
	maximum = 1933
Slowest packet = 16606
Flit latency average = 1691.31
	minimum = 5
	maximum = 3514
Slowest flit = 143917
Fragmentation average = 7.02905
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0291536
	minimum = 0.0185 (at node 23)
	maximum = 0.0385 (at node 13)
Accepted packet rate average = 0.0158984
	minimum = 0.009 (at node 88)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.524307
	minimum = 0.333 (at node 109)
	maximum = 0.689 (at node 22)
Accepted flit rate average= 0.286008
	minimum = 0.177 (at node 116)
	maximum = 0.431 (at node 128)
Injected packet length average = 17.9843
Accepted packet length average = 17.9897
Total in-flight flits = 230557 (185303 measured)
latency change    = 0.176282
throughput change = 0.0128657
Class 0:
Packet latency average = 734.872
	minimum = 22
	maximum = 2993
Network latency average = 725.785
	minimum = 22
	maximum = 2972
Slowest packet = 16587
Flit latency average = 1899.22
	minimum = 5
	maximum = 4188
Slowest flit = 176946
Fragmentation average = 26.8249
	minimum = 0
	maximum = 478
Injected packet rate average = 0.0291302
	minimum = 0.021 (at node 23)
	maximum = 0.037 (at node 62)
Accepted packet rate average = 0.0159479
	minimum = 0.01 (at node 36)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.524156
	minimum = 0.374 (at node 23)
	maximum = 0.668667 (at node 62)
Accepted flit rate average= 0.286632
	minimum = 0.179667 (at node 36)
	maximum = 0.399 (at node 128)
Injected packet length average = 17.9936
Accepted packet length average = 17.973
Total in-flight flits = 275796 (267443 measured)
latency change    = 0.925347
throughput change = 0.00217747
Class 0:
Packet latency average = 1807.71
	minimum = 22
	maximum = 4044
Network latency average = 1798.47
	minimum = 22
	maximum = 3980
Slowest packet = 16611
Flit latency average = 2096.39
	minimum = 5
	maximum = 4699
Slowest flit = 178667
Fragmentation average = 63.5323
	minimum = 0
	maximum = 949
Injected packet rate average = 0.0291615
	minimum = 0.02225 (at node 23)
	maximum = 0.03575 (at node 145)
Accepted packet rate average = 0.0159583
	minimum = 0.011 (at node 162)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.52465
	minimum = 0.40025 (at node 23)
	maximum = 0.64025 (at node 145)
Accepted flit rate average= 0.288035
	minimum = 0.207 (at node 162)
	maximum = 0.38975 (at node 128)
Injected packet length average = 17.9912
Accepted packet length average = 18.0492
Total in-flight flits = 320827 (319703 measured)
latency change    = 0.59348
throughput change = 0.00487167
Class 0:
Packet latency average = 2344.03
	minimum = 22
	maximum = 4898
Network latency average = 2334.43
	minimum = 22
	maximum = 4867
Slowest packet = 16712
Flit latency average = 2301.71
	minimum = 5
	maximum = 5150
Slowest flit = 283538
Fragmentation average = 85.5932
	minimum = 0
	maximum = 949
Injected packet rate average = 0.029149
	minimum = 0.023 (at node 23)
	maximum = 0.036 (at node 145)
Accepted packet rate average = 0.0159698
	minimum = 0.0124 (at node 86)
	maximum = 0.021 (at node 138)
Injected flit rate average = 0.524728
	minimum = 0.4138 (at node 23)
	maximum = 0.6448 (at node 145)
Accepted flit rate average= 0.288378
	minimum = 0.2238 (at node 86)
	maximum = 0.3754 (at node 138)
Injected packet length average = 18.0016
Accepted packet length average = 18.0577
Total in-flight flits = 365761 (365583 measured)
latency change    = 0.228801
throughput change = 0.0011893
Class 0:
Packet latency average = 2706.88
	minimum = 22
	maximum = 5789
Network latency average = 2696.84
	minimum = 22
	maximum = 5780
Slowest packet = 17767
Flit latency average = 2506.74
	minimum = 5
	maximum = 5978
Slowest flit = 299112
Fragmentation average = 96.8174
	minimum = 0
	maximum = 1005
Injected packet rate average = 0.0290356
	minimum = 0.0216667 (at node 112)
	maximum = 0.0355 (at node 145)
Accepted packet rate average = 0.0159948
	minimum = 0.0123333 (at node 171)
	maximum = 0.0205 (at node 138)
Injected flit rate average = 0.522579
	minimum = 0.390333 (at node 112)
	maximum = 0.6375 (at node 145)
Accepted flit rate average= 0.288667
	minimum = 0.224 (at node 190)
	maximum = 0.364667 (at node 138)
Injected packet length average = 17.9979
Accepted packet length average = 18.0475
Total in-flight flits = 408484 (408466 measured)
latency change    = 0.134046
throughput change = 0.000999567
Class 0:
Packet latency average = 3015.29
	minimum = 22
	maximum = 6657
Network latency average = 3004.17
	minimum = 22
	maximum = 6624
Slowest packet = 16805
Flit latency average = 2719.35
	minimum = 5
	maximum = 6711
Slowest flit = 326887
Fragmentation average = 102.743
	minimum = 0
	maximum = 1102
Injected packet rate average = 0.0288274
	minimum = 0.0187143 (at node 112)
	maximum = 0.0354286 (at node 145)
Accepted packet rate average = 0.0159903
	minimum = 0.0131429 (at node 63)
	maximum = 0.0192857 (at node 50)
Injected flit rate average = 0.518773
	minimum = 0.337143 (at node 112)
	maximum = 0.637857 (at node 145)
Accepted flit rate average= 0.288054
	minimum = 0.234143 (at node 63)
	maximum = 0.344857 (at node 118)
Injected packet length average = 17.9958
Accepted packet length average = 18.0143
Total in-flight flits = 449355 (449355 measured)
latency change    = 0.102284
throughput change = 0.00212582
Draining all recorded packets ...
Class 0:
Remaining flits: 333882 333883 333884 333885 333886 333887 333888 333889 333890 333891 [...] (490760 flits)
Measured flits: 333882 333883 333884 333885 333886 333887 333888 333889 333890 333891 [...] (406102 flits)
Class 0:
Remaining flits: 333882 333883 333884 333885 333886 333887 333888 333889 333890 333891 [...] (527402 flits)
Measured flits: 333882 333883 333884 333885 333886 333887 333888 333889 333890 333891 [...] (360533 flits)
Class 0:
Remaining flits: 333886 333887 333888 333889 333890 333891 333892 333893 333894 333895 [...] (564295 flits)
Measured flits: 333886 333887 333888 333889 333890 333891 333892 333893 333894 333895 [...] (314686 flits)
Class 0:
Remaining flits: 351738 351739 351740 351741 351742 351743 351744 351745 351746 351747 [...] (596733 flits)
Measured flits: 351738 351739 351740 351741 351742 351743 351744 351745 351746 351747 [...] (267735 flits)
Class 0:
Remaining flits: 474102 474103 474104 474105 474106 474107 474108 474109 474110 474111 [...] (632574 flits)
Measured flits: 474102 474103 474104 474105 474106 474107 474108 474109 474110 474111 [...] (220313 flits)
Class 0:
Remaining flits: 487818 487819 487820 487821 487822 487823 487824 487825 487826 487827 [...] (667883 flits)
Measured flits: 487818 487819 487820 487821 487822 487823 487824 487825 487826 487827 [...] (172855 flits)
Class 0:
Remaining flits: 501408 501409 501410 501411 501412 501413 501414 501415 501416 501417 [...] (704186 flits)
Measured flits: 501408 501409 501410 501411 501412 501413 501414 501415 501416 501417 [...] (127183 flits)
Class 0:
Remaining flits: 517914 517915 517916 517917 517918 517919 517920 517921 517922 517923 [...] (734271 flits)
Measured flits: 517914 517915 517916 517917 517918 517919 517920 517921 517922 517923 [...] (89973 flits)
Class 0:
Remaining flits: 542952 542953 542954 542955 542956 542957 542958 542959 542960 542961 [...] (761104 flits)
Measured flits: 542952 542953 542954 542955 542956 542957 542958 542959 542960 542961 [...] (62334 flits)
Class 0:
Remaining flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (786415 flits)
Measured flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (41444 flits)
Class 0:
Remaining flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (807206 flits)
Measured flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (26480 flits)
Class 0:
Remaining flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (826400 flits)
Measured flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (15793 flits)
Class 0:
Remaining flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (843160 flits)
Measured flits: 563040 563041 563042 563043 563044 563045 563046 563047 563048 563049 [...] (8500 flits)
Class 0:
Remaining flits: 563045 563046 563047 563048 563049 563050 563051 563052 563053 563054 [...] (850483 flits)
Measured flits: 563045 563046 563047 563048 563049 563050 563051 563052 563053 563054 [...] (4724 flits)
Class 0:
Remaining flits: 770670 770671 770672 770673 770674 770675 770676 770677 770678 770679 [...] (859282 flits)
Measured flits: 770670 770671 770672 770673 770674 770675 770676 770677 770678 770679 [...] (2513 flits)
Class 0:
Remaining flits: 792756 792757 792758 792759 792760 792761 792762 792763 792764 792765 [...] (860623 flits)
Measured flits: 792756 792757 792758 792759 792760 792761 792762 792763 792764 792765 [...] (1465 flits)
Class 0:
Remaining flits: 869940 869941 869942 869943 869944 869945 869946 869947 869948 869949 [...] (861228 flits)
Measured flits: 869940 869941 869942 869943 869944 869945 869946 869947 869948 869949 [...] (573 flits)
Class 0:
Remaining flits: 869940 869941 869942 869943 869944 869945 869946 869947 869948 869949 [...] (862122 flits)
Measured flits: 869940 869941 869942 869943 869944 869945 869946 869947 869948 869949 [...] (260 flits)
Class 0:
Remaining flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (861384 flits)
Measured flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (72 flits)
Class 0:
Remaining flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (862316 flits)
Measured flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (72 flits)
Class 0:
Remaining flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (864196 flits)
Measured flits: 874818 874819 874820 874821 874822 874823 874824 874825 874826 874827 [...] (72 flits)
Class 0:
Remaining flits: 874820 874821 874822 874823 874824 874825 874826 874827 874828 874829 [...] (869796 flits)
Measured flits: 874820 874821 874822 874823 874824 874825 874826 874827 874828 874829 [...] (52 flits)
Class 0:
Remaining flits: 971118 971119 971120 971121 971122 971123 971124 971125 971126 971127 [...] (870324 flits)
Measured flits: 971118 971119 971120 971121 971122 971123 971124 971125 971126 971127 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1084770 1084771 1084772 1084773 1084774 1084775 1084776 1084777 1084778 1084779 [...] (821032 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1084770 1084771 1084772 1084773 1084774 1084775 1084776 1084777 1084778 1084779 [...] (771268 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1084770 1084771 1084772 1084773 1084774 1084775 1084776 1084777 1084778 1084779 [...] (720913 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1123002 1123003 1123004 1123005 1123006 1123007 1123008 1123009 1123010 1123011 [...] (670623 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1123002 1123003 1123004 1123005 1123006 1123007 1123008 1123009 1123010 1123011 [...] (619974 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1123002 1123003 1123004 1123005 1123006 1123007 1123008 1123009 1123010 1123011 [...] (570407 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1123002 1123003 1123004 1123005 1123006 1123007 1123008 1123009 1123010 1123011 [...] (521370 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1204956 1204957 1204958 1204959 1204960 1204961 1204962 1204963 1204964 1204965 [...] (472610 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1389600 1389601 1389602 1389603 1389604 1389605 1389606 1389607 1389608 1389609 [...] (424691 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1418112 1418113 1418114 1418115 1418116 1418117 1418118 1418119 1418120 1418121 [...] (376910 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1516626 1516627 1516628 1516629 1516630 1516631 1516632 1516633 1516634 1516635 [...] (329516 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1516626 1516627 1516628 1516629 1516630 1516631 1516632 1516633 1516634 1516635 [...] (282191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1621314 1621315 1621316 1621317 1621318 1621319 1621320 1621321 1621322 1621323 [...] (234512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1621314 1621315 1621316 1621317 1621318 1621319 1621320 1621321 1621322 1621323 [...] (187085 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1638126 1638127 1638128 1638129 1638130 1638131 1638132 1638133 1638134 1638135 [...] (139527 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805868 1805869 1805870 1805871 1805872 1805873 1805874 1805875 1805876 1805877 [...] (91768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1874358 1874359 1874360 1874361 1874362 1874363 1874364 1874365 1874366 1874367 [...] (44647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1902510 1902511 1902512 1902513 1902514 1902515 1902516 1902517 1902518 1902519 [...] (9434 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2325204 2325205 2325206 2325207 2325208 2325209 2325210 2325211 2325212 2325213 [...] (722 flits)
Measured flits: (0 flits)
Time taken is 52823 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6077.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23706 (1 samples)
Network latency average = 6043.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23706 (1 samples)
Flit latency average = 11096.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 31551 (1 samples)
Fragmentation average = 123.289 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2302 (1 samples)
Injected packet rate average = 0.0288274 (1 samples)
	minimum = 0.0187143 (1 samples)
	maximum = 0.0354286 (1 samples)
Accepted packet rate average = 0.0159903 (1 samples)
	minimum = 0.0131429 (1 samples)
	maximum = 0.0192857 (1 samples)
Injected flit rate average = 0.518773 (1 samples)
	minimum = 0.337143 (1 samples)
	maximum = 0.637857 (1 samples)
Accepted flit rate average = 0.288054 (1 samples)
	minimum = 0.234143 (1 samples)
	maximum = 0.344857 (1 samples)
Injected packet size average = 17.9958 (1 samples)
Accepted packet size average = 18.0143 (1 samples)
Hops average = 5.0672 (1 samples)
Total run time 201.174
