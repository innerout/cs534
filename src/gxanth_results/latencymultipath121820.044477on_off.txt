BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.88
	minimum = 22
	maximum = 988
Network latency average = 199.272
	minimum = 22
	maximum = 821
Slowest packet = 46
Flit latency average = 171.919
	minimum = 5
	maximum = 804
Slowest flit = 14364
Fragmentation average = 19.5188
	minimum = 0
	maximum = 116
Injected packet rate average = 0.017724
	minimum = 0 (at node 62)
	maximum = 0.039 (at node 29)
Accepted packet rate average = 0.0131615
	minimum = 0.003 (at node 174)
	maximum = 0.024 (at node 70)
Injected flit rate average = 0.315057
	minimum = 0 (at node 62)
	maximum = 0.702 (at node 29)
Accepted flit rate average= 0.24225
	minimum = 0.054 (at node 174)
	maximum = 0.432 (at node 70)
Injected packet length average = 17.7758
Accepted packet length average = 18.406
Total in-flight flits = 16236 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 622.703
	minimum = 22
	maximum = 1953
Network latency average = 261.363
	minimum = 22
	maximum = 1209
Slowest packet = 46
Flit latency average = 229.827
	minimum = 5
	maximum = 1192
Slowest flit = 44802
Fragmentation average = 21.2508
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0160885
	minimum = 0.001 (at node 62)
	maximum = 0.028 (at node 29)
Accepted packet rate average = 0.0136667
	minimum = 0.0075 (at node 153)
	maximum = 0.0195 (at node 71)
Injected flit rate average = 0.287552
	minimum = 0.018 (at node 62)
	maximum = 0.5025 (at node 29)
Accepted flit rate average= 0.248263
	minimum = 0.135 (at node 153)
	maximum = 0.351 (at node 71)
Injected packet length average = 17.8731
Accepted packet length average = 18.1656
Total in-flight flits = 17563 (0 measured)
latency change    = 0.447761
throughput change = 0.0242204
Class 0:
Packet latency average = 1492.52
	minimum = 48
	maximum = 2900
Network latency average = 349.07
	minimum = 22
	maximum = 1407
Slowest packet = 4855
Flit latency average = 314.241
	minimum = 5
	maximum = 1390
Slowest flit = 52469
Fragmentation average = 21.8414
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0142083
	minimum = 0.004 (at node 62)
	maximum = 0.034 (at node 109)
Accepted packet rate average = 0.0139219
	minimum = 0.006 (at node 4)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.255969
	minimum = 0.072 (at node 62)
	maximum = 0.596 (at node 109)
Accepted flit rate average= 0.252099
	minimum = 0.113 (at node 184)
	maximum = 0.464 (at node 34)
Injected packet length average = 18.0154
Accepted packet length average = 18.1081
Total in-flight flits = 18228 (0 measured)
latency change    = 0.582784
throughput change = 0.015216
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2146.4
	minimum = 22
	maximum = 3837
Network latency average = 283.413
	minimum = 22
	maximum = 861
Slowest packet = 9002
Flit latency average = 316.757
	minimum = 5
	maximum = 1401
Slowest flit = 116783
Fragmentation average = 21.558
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0139948
	minimum = 0 (at node 101)
	maximum = 0.031 (at node 163)
Accepted packet rate average = 0.014125
	minimum = 0.006 (at node 190)
	maximum = 0.026 (at node 90)
Injected flit rate average = 0.251641
	minimum = 0 (at node 101)
	maximum = 0.562 (at node 163)
Accepted flit rate average= 0.253234
	minimum = 0.123 (at node 190)
	maximum = 0.461 (at node 90)
Injected packet length average = 17.981
Accepted packet length average = 17.9281
Total in-flight flits = 18189 (17995 measured)
latency change    = 0.30464
throughput change = 0.00448366
Class 0:
Packet latency average = 2495.38
	minimum = 22
	maximum = 4812
Network latency average = 332.539
	minimum = 22
	maximum = 1465
Slowest packet = 9002
Flit latency average = 323.123
	minimum = 5
	maximum = 1566
Slowest flit = 183287
Fragmentation average = 21.7825
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138984
	minimum = 0.006 (at node 63)
	maximum = 0.026 (at node 163)
Accepted packet rate average = 0.0139297
	minimum = 0.0085 (at node 88)
	maximum = 0.0205 (at node 129)
Injected flit rate average = 0.250435
	minimum = 0.1135 (at node 63)
	maximum = 0.47 (at node 163)
Accepted flit rate average= 0.250312
	minimum = 0.158 (at node 88)
	maximum = 0.37 (at node 90)
Injected packet length average = 18.0189
Accepted packet length average = 17.9697
Total in-flight flits = 18516 (18516 measured)
latency change    = 0.139851
throughput change = 0.0116729
Class 0:
Packet latency average = 2836.79
	minimum = 22
	maximum = 5898
Network latency average = 345.234
	minimum = 22
	maximum = 1983
Slowest packet = 9002
Flit latency average = 325.66
	minimum = 5
	maximum = 1966
Slowest flit = 176525
Fragmentation average = 22.1213
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138819
	minimum = 0.007 (at node 65)
	maximum = 0.0236667 (at node 163)
Accepted packet rate average = 0.0139028
	minimum = 0.00933333 (at node 36)
	maximum = 0.0193333 (at node 129)
Injected flit rate average = 0.249903
	minimum = 0.126 (at node 65)
	maximum = 0.426667 (at node 163)
Accepted flit rate average= 0.250095
	minimum = 0.172 (at node 132)
	maximum = 0.349333 (at node 129)
Injected packet length average = 18.002
Accepted packet length average = 17.9889
Total in-flight flits = 18389 (18389 measured)
latency change    = 0.120353
throughput change = 0.000867724
Class 0:
Packet latency average = 3139
	minimum = 22
	maximum = 6919
Network latency average = 350.456
	minimum = 22
	maximum = 1983
Slowest packet = 9002
Flit latency average = 325.895
	minimum = 5
	maximum = 1966
Slowest flit = 176525
Fragmentation average = 22.3212
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138372
	minimum = 0.007 (at node 41)
	maximum = 0.022 (at node 163)
Accepted packet rate average = 0.0138568
	minimum = 0.01025 (at node 2)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.249172
	minimum = 0.126 (at node 41)
	maximum = 0.397 (at node 163)
Accepted flit rate average= 0.249107
	minimum = 0.18125 (at node 84)
	maximum = 0.33125 (at node 129)
Injected packet length average = 18.0073
Accepted packet length average = 17.9773
Total in-flight flits = 18632 (18632 measured)
latency change    = 0.0962744
throughput change = 0.00396904
Class 0:
Packet latency average = 3451.73
	minimum = 22
	maximum = 7624
Network latency average = 355.103
	minimum = 22
	maximum = 1983
Slowest packet = 9002
Flit latency average = 327.789
	minimum = 5
	maximum = 1966
Slowest flit = 176525
Fragmentation average = 22.5534
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0137948
	minimum = 0.0064 (at node 186)
	maximum = 0.0202 (at node 40)
Accepted packet rate average = 0.0138427
	minimum = 0.01 (at node 64)
	maximum = 0.0178 (at node 128)
Injected flit rate average = 0.248404
	minimum = 0.1156 (at node 186)
	maximum = 0.3652 (at node 40)
Accepted flit rate average= 0.248848
	minimum = 0.18 (at node 64)
	maximum = 0.3172 (at node 128)
Injected packet length average = 18.0071
Accepted packet length average = 17.9768
Total in-flight flits = 18158 (18158 measured)
latency change    = 0.0906001
throughput change = 0.00104021
Class 0:
Packet latency average = 3756.64
	minimum = 22
	maximum = 8645
Network latency average = 357.205
	minimum = 22
	maximum = 1983
Slowest packet = 9002
Flit latency average = 328.239
	minimum = 5
	maximum = 1966
Slowest flit = 176525
Fragmentation average = 22.5746
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0137873
	minimum = 0.0075 (at node 186)
	maximum = 0.0196667 (at node 113)
Accepted packet rate average = 0.0138099
	minimum = 0.011 (at node 42)
	maximum = 0.0173333 (at node 128)
Injected flit rate average = 0.248202
	minimum = 0.1325 (at node 186)
	maximum = 0.354167 (at node 113)
Accepted flit rate average= 0.248359
	minimum = 0.190667 (at node 42)
	maximum = 0.3095 (at node 129)
Injected packet length average = 18.0022
Accepted packet length average = 17.9842
Total in-flight flits = 18390 (18390 measured)
latency change    = 0.081167
throughput change = 0.00196708
Class 0:
Packet latency average = 4063.81
	minimum = 22
	maximum = 9595
Network latency average = 358.002
	minimum = 22
	maximum = 1983
Slowest packet = 9002
Flit latency average = 328.095
	minimum = 5
	maximum = 1966
Slowest flit = 176525
Fragmentation average = 22.446
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0137842
	minimum = 0.009 (at node 186)
	maximum = 0.0201429 (at node 113)
Accepted packet rate average = 0.0138058
	minimum = 0.0111429 (at node 64)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.248164
	minimum = 0.161 (at node 186)
	maximum = 0.360714 (at node 113)
Accepted flit rate average= 0.24841
	minimum = 0.200143 (at node 171)
	maximum = 0.324571 (at node 138)
Injected packet length average = 18.0035
Accepted packet length average = 17.9932
Total in-flight flits = 18247 (18247 measured)
latency change    = 0.075586
throughput change = 0.000203676
Draining all recorded packets ...
Class 0:
Remaining flits: 451109 451110 451111 451112 451113 451114 451115 462366 462367 462368 [...] (18550 flits)
Measured flits: 451109 451110 451111 451112 451113 451114 451115 462366 462367 462368 [...] (18550 flits)
Class 0:
Remaining flits: 511308 511309 511310 511311 511312 511313 511314 511315 511316 511317 [...] (18234 flits)
Measured flits: 511308 511309 511310 511311 511312 511313 511314 511315 511316 511317 [...] (18216 flits)
Class 0:
Remaining flits: 537053 537054 537055 537056 537057 537058 537059 537060 537061 537062 [...] (18228 flits)
Measured flits: 537053 537054 537055 537056 537057 537058 537059 537060 537061 537062 [...] (18120 flits)
Class 0:
Remaining flits: 618372 618373 618374 618375 618376 618377 618378 618379 618380 618381 [...] (18494 flits)
Measured flits: 618372 618373 618374 618375 618376 618377 618378 618379 618380 618381 [...] (18368 flits)
Class 0:
Remaining flits: 662094 662095 662096 662097 662098 662099 662100 662101 662102 662103 [...] (18345 flits)
Measured flits: 662094 662095 662096 662097 662098 662099 662100 662101 662102 662103 [...] (17859 flits)
Class 0:
Remaining flits: 675135 675136 675137 675138 675139 675140 675141 675142 675143 710380 [...] (18683 flits)
Measured flits: 675135 675136 675137 675138 675139 675140 675141 675142 675143 710380 [...] (18121 flits)
Class 0:
Remaining flits: 771945 771946 771947 773010 773011 773012 773013 773014 773015 773016 [...] (18051 flits)
Measured flits: 771945 771946 771947 773010 773011 773012 773013 773014 773015 773016 [...] (17098 flits)
Class 0:
Remaining flits: 808974 808975 808976 808977 808978 808979 808980 808981 808982 808983 [...] (18024 flits)
Measured flits: 808974 808975 808976 808977 808978 808979 808980 808981 808982 808983 [...] (16498 flits)
Class 0:
Remaining flits: 843174 843175 843176 843177 843178 843179 843180 843181 843182 843183 [...] (18564 flits)
Measured flits: 843174 843175 843176 843177 843178 843179 843180 843181 843182 843183 [...] (16772 flits)
Class 0:
Remaining flits: 896109 896110 896111 900324 900325 900326 900327 900328 900329 900330 [...] (18651 flits)
Measured flits: 896109 896110 896111 900324 900325 900326 900327 900328 900329 900330 [...] (16157 flits)
Class 0:
Remaining flits: 932904 932905 932906 932907 932908 932909 932910 932911 932912 932913 [...] (18714 flits)
Measured flits: 932904 932905 932906 932907 932908 932909 932910 932911 932912 932913 [...] (14941 flits)
Class 0:
Remaining flits: 988812 988813 988814 988815 988816 988817 988818 988819 988820 988821 [...] (18506 flits)
Measured flits: 988812 988813 988814 988815 988816 988817 988818 988819 988820 988821 [...] (13879 flits)
Class 0:
Remaining flits: 1047006 1047007 1047008 1047009 1047010 1047011 1047012 1047013 1047014 1047015 [...] (18295 flits)
Measured flits: 1048806 1048807 1048808 1048809 1048810 1048811 1048812 1048813 1048814 1048815 [...] (12754 flits)
Class 0:
Remaining flits: 1105866 1105867 1105868 1105869 1105870 1105871 1105872 1105873 1105874 1105875 [...] (18233 flits)
Measured flits: 1105866 1105867 1105868 1105869 1105870 1105871 1105872 1105873 1105874 1105875 [...] (11959 flits)
Class 0:
Remaining flits: 1135908 1135909 1135910 1135911 1135912 1135913 1135914 1135915 1135916 1135917 [...] (18543 flits)
Measured flits: 1135908 1135909 1135910 1135911 1135912 1135913 1135914 1135915 1135916 1135917 [...] (10585 flits)
Class 0:
Remaining flits: 1182744 1182745 1182746 1182747 1182748 1182749 1182750 1182751 1182752 1182753 [...] (18595 flits)
Measured flits: 1194606 1194607 1194608 1194609 1194610 1194611 1194612 1194613 1194614 1194615 [...] (9702 flits)
Class 0:
Remaining flits: 1215900 1215901 1215902 1215903 1215904 1215905 1215906 1215907 1215908 1215909 [...] (18090 flits)
Measured flits: 1246050 1246051 1246052 1246053 1246054 1246055 1246056 1246057 1246058 1246059 [...] (8375 flits)
Class 0:
Remaining flits: 1287037 1287038 1287039 1287040 1287041 1287042 1287043 1287044 1287045 1287046 [...] (19114 flits)
Measured flits: 1287037 1287038 1287039 1287040 1287041 1287042 1287043 1287044 1287045 1287046 [...] (7521 flits)
Class 0:
Remaining flits: 1339956 1339957 1339958 1339959 1339960 1339961 1339962 1339963 1339964 1339965 [...] (18852 flits)
Measured flits: 1345446 1345447 1345448 1345449 1345450 1345451 1345452 1345453 1345454 1345455 [...] (6214 flits)
Class 0:
Remaining flits: 1380942 1380943 1380944 1380945 1380946 1380947 1380948 1380949 1380950 1380951 [...] (18780 flits)
Measured flits: 1395630 1395631 1395632 1395633 1395634 1395635 1395636 1395637 1395638 1395639 [...] (4784 flits)
Class 0:
Remaining flits: 1415646 1415647 1415648 1415649 1415650 1415651 1415652 1415653 1415654 1415655 [...] (18093 flits)
Measured flits: 1444800 1444801 1444802 1444803 1444804 1444805 1448334 1448335 1448336 1448337 [...] (4132 flits)
Class 0:
Remaining flits: 1454775 1454776 1454777 1490113 1490114 1490115 1490116 1490117 1490118 1490119 [...] (18758 flits)
Measured flits: 1508795 1509372 1509373 1509374 1509375 1509376 1509377 1509378 1509379 1509380 [...] (3533 flits)
Class 0:
Remaining flits: 1527484 1527485 1527486 1527487 1527488 1527489 1527490 1527491 1527492 1527493 [...] (18858 flits)
Measured flits: 1544254 1544255 1544900 1544901 1544902 1544903 1546643 1546644 1546645 1546646 [...] (2859 flits)
Class 0:
Remaining flits: 1579338 1579339 1579340 1579341 1579342 1579343 1579344 1579345 1579346 1579347 [...] (18202 flits)
Measured flits: 1579338 1579339 1579340 1579341 1579342 1579343 1579344 1579345 1579346 1579347 [...] (2425 flits)
Class 0:
Remaining flits: 1626858 1626859 1626860 1626861 1626862 1626863 1626864 1626865 1626866 1626867 [...] (18311 flits)
Measured flits: 1661148 1661149 1661150 1661151 1661152 1661153 1661154 1661155 1661156 1661157 [...] (2110 flits)
Class 0:
Remaining flits: 1661454 1661455 1661456 1661457 1661458 1661459 1661460 1661461 1661462 1661463 [...] (18782 flits)
Measured flits: 1686638 1686639 1686640 1686641 1686642 1686643 1686644 1686645 1686646 1686647 [...] (1758 flits)
Class 0:
Remaining flits: 1741032 1741033 1741034 1741035 1741036 1741037 1741038 1741039 1741040 1741041 [...] (18203 flits)
Measured flits: 1754136 1754137 1754138 1754139 1754140 1754141 1754142 1754143 1754144 1754145 [...] (1130 flits)
Class 0:
Remaining flits: 1765787 1765788 1765789 1765790 1765791 1765792 1765793 1765794 1765795 1765796 [...] (18189 flits)
Measured flits: 1765787 1765788 1765789 1765790 1765791 1765792 1765793 1765794 1765795 1765796 [...] (803 flits)
Class 0:
Remaining flits: 1815174 1815175 1815176 1815177 1815178 1815179 1815180 1815181 1815182 1815183 [...] (18015 flits)
Measured flits: 1845666 1845667 1845668 1845669 1845670 1845671 1845672 1845673 1845674 1845675 [...] (398 flits)
Class 0:
Remaining flits: 1872000 1872001 1872002 1872003 1872004 1872005 1872006 1872007 1872008 1872009 [...] (18438 flits)
Measured flits: 1898046 1898047 1898048 1898049 1898050 1898051 1898052 1898053 1898054 1898055 [...] (306 flits)
Class 0:
Remaining flits: 1900836 1900837 1900838 1900839 1900840 1900841 1900842 1900843 1900844 1900845 [...] (18485 flits)
Measured flits: 1900836 1900837 1900838 1900839 1900840 1900841 1900842 1900843 1900844 1900845 [...] (324 flits)
Class 0:
Remaining flits: 1939847 1939848 1939849 1939850 1939851 1939852 1939853 1939854 1939855 1939856 [...] (18607 flits)
Measured flits: 1994124 1994125 1994126 1994127 1994128 1994129 2006550 2006551 2006552 2006553 [...] (294 flits)
Class 0:
Remaining flits: 2000916 2000917 2000918 2000919 2000920 2000921 2000922 2000923 2000924 2000925 [...] (17916 flits)
Measured flits: 2052504 2052505 2052506 2052507 2052508 2052509 2052510 2052511 2052512 2052513 [...] (251 flits)
Class 0:
Remaining flits: 2045610 2045611 2045612 2045613 2045614 2045615 2045616 2045617 2045618 2045619 [...] (18619 flits)
Measured flits: 2092680 2092681 2092682 2092683 2092684 2092685 2092686 2092687 2092688 2092689 [...] (207 flits)
Class 0:
Remaining flits: 2061558 2061559 2061560 2061561 2061562 2061563 2061564 2061565 2061566 2061567 [...] (18147 flits)
Measured flits: 2155068 2155069 2155070 2155071 2155072 2155073 2155074 2155075 2155076 2155077 [...] (198 flits)
Class 0:
Remaining flits: 2165310 2165311 2165312 2165313 2165314 2165315 2165316 2165317 2165318 2165319 [...] (18563 flits)
Measured flits: 2201397 2201398 2201399 2204530 2204531 2208636 2208637 2208638 2208639 2208640 [...] (159 flits)
Class 0:
Remaining flits: 2206494 2206495 2206496 2206497 2206498 2206499 2206500 2206501 2206502 2206503 [...] (18657 flits)
Measured flits: 2247048 2247049 2247050 2247051 2247052 2247053 2247054 2247055 2247056 2247057 [...] (108 flits)
Class 0:
Remaining flits: 2223252 2223253 2223254 2223255 2223256 2223257 2223258 2223259 2223260 2223261 [...] (18718 flits)
Measured flits: 2285208 2285209 2285210 2285211 2285212 2285213 2285214 2285215 2285216 2285217 [...] (90 flits)
Draining remaining packets ...
Time taken is 49091 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10407.8 (1 samples)
	minimum = 22 (1 samples)
	maximum = 38543 (1 samples)
Network latency average = 365.88 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2617 (1 samples)
Flit latency average = 329.537 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2568 (1 samples)
Fragmentation average = 22.6585 (1 samples)
	minimum = 0 (1 samples)
	maximum = 144 (1 samples)
Injected packet rate average = 0.0137842 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0201429 (1 samples)
Accepted packet rate average = 0.0138058 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.248164 (1 samples)
	minimum = 0.161 (1 samples)
	maximum = 0.360714 (1 samples)
Accepted flit rate average = 0.24841 (1 samples)
	minimum = 0.200143 (1 samples)
	maximum = 0.324571 (1 samples)
Injected packet size average = 18.0035 (1 samples)
Accepted packet size average = 17.9932 (1 samples)
Hops average = 5.06569 (1 samples)
Total run time 46.7378
