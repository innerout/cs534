BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.968
	minimum = 23
	maximum = 961
Network latency average = 304.619
	minimum = 23
	maximum = 961
Slowest packet = 167
Flit latency average = 245.19
	minimum = 6
	maximum = 949
Slowest flit = 3056
Fragmentation average = 153.576
	minimum = 0
	maximum = 866
Injected packet rate average = 0.0352292
	minimum = 0.018 (at node 99)
	maximum = 0.049 (at node 33)
Accepted packet rate average = 0.0108229
	minimum = 0.005 (at node 81)
	maximum = 0.019 (at node 15)
Injected flit rate average = 0.628219
	minimum = 0.324 (at node 99)
	maximum = 0.882 (at node 33)
Accepted flit rate average= 0.221672
	minimum = 0.108 (at node 127)
	maximum = 0.371 (at node 173)
Injected packet length average = 17.8323
Accepted packet length average = 20.4817
Total in-flight flits = 79191 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 591.263
	minimum = 23
	maximum = 1896
Network latency average = 577.141
	minimum = 23
	maximum = 1869
Slowest packet = 697
Flit latency average = 510.29
	minimum = 6
	maximum = 1899
Slowest flit = 6563
Fragmentation average = 185.415
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.0342578
	minimum = 0.0235 (at node 2)
	maximum = 0.046 (at node 17)
Accepted packet rate average = 0.0119297
	minimum = 0.006 (at node 116)
	maximum = 0.017 (at node 140)
Injected flit rate average = 0.614094
	minimum = 0.423 (at node 2)
	maximum = 0.826 (at node 17)
Accepted flit rate average= 0.22913
	minimum = 0.11 (at node 116)
	maximum = 0.3235 (at node 140)
Injected packet length average = 17.9257
Accepted packet length average = 19.2067
Total in-flight flits = 148822 (0 measured)
latency change    = 0.465604
throughput change = 0.0325506
Class 0:
Packet latency average = 1484.52
	minimum = 31
	maximum = 2755
Network latency average = 1457.03
	minimum = 27
	maximum = 2733
Slowest packet = 1061
Flit latency average = 1407.65
	minimum = 6
	maximum = 2793
Slowest flit = 20590
Fragmentation average = 226.938
	minimum = 1
	maximum = 1770
Injected packet rate average = 0.0304479
	minimum = 0.009 (at node 104)
	maximum = 0.047 (at node 109)
Accepted packet rate average = 0.0125104
	minimum = 0.005 (at node 163)
	maximum = 0.023 (at node 73)
Injected flit rate average = 0.546937
	minimum = 0.16 (at node 104)
	maximum = 0.84 (at node 109)
Accepted flit rate average= 0.223536
	minimum = 0.084 (at node 163)
	maximum = 0.382 (at node 148)
Injected packet length average = 17.9631
Accepted packet length average = 17.868
Total in-flight flits = 211131 (0 measured)
latency change    = 0.601716
throughput change = 0.0250239
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 494.335
	minimum = 28
	maximum = 1692
Network latency average = 91.6474
	minimum = 28
	maximum = 983
Slowest packet = 19039
Flit latency average = 2023.42
	minimum = 6
	maximum = 3844
Slowest flit = 16848
Fragmentation average = 12.4277
	minimum = 4
	maximum = 60
Injected packet rate average = 0.0297135
	minimum = 0.007 (at node 136)
	maximum = 0.049 (at node 34)
Accepted packet rate average = 0.0121146
	minimum = 0.005 (at node 4)
	maximum = 0.022 (at node 178)
Injected flit rate average = 0.535422
	minimum = 0.141 (at node 136)
	maximum = 0.886 (at node 34)
Accepted flit rate average= 0.217375
	minimum = 0.085 (at node 4)
	maximum = 0.39 (at node 99)
Injected packet length average = 18.0195
Accepted packet length average = 17.9433
Total in-flight flits = 272175 (99580 measured)
latency change    = 2.00307
throughput change = 0.0283448
Class 0:
Packet latency average = 840.815
	minimum = 28
	maximum = 3011
Network latency average = 397.83
	minimum = 28
	maximum = 1904
Slowest packet = 19039
Flit latency average = 2328.79
	minimum = 6
	maximum = 4778
Slowest flit = 18433
Fragmentation average = 19.7012
	minimum = 4
	maximum = 399
Injected packet rate average = 0.0266745
	minimum = 0.0035 (at node 136)
	maximum = 0.0415 (at node 34)
Accepted packet rate average = 0.0120469
	minimum = 0.0055 (at node 84)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.480174
	minimum = 0.0705 (at node 136)
	maximum = 0.753 (at node 34)
Accepted flit rate average= 0.21526
	minimum = 0.0955 (at node 84)
	maximum = 0.354 (at node 128)
Injected packet length average = 18.0013
Accepted packet length average = 17.8686
Total in-flight flits = 313025 (177110 measured)
latency change    = 0.412076
throughput change = 0.00982337
Class 0:
Packet latency average = 1506.28
	minimum = 28
	maximum = 4034
Network latency average = 939.225
	minimum = 28
	maximum = 2979
Slowest packet = 19039
Flit latency average = 2615.59
	minimum = 6
	maximum = 5778
Slowest flit = 24823
Fragmentation average = 51.715
	minimum = 1
	maximum = 973
Injected packet rate average = 0.0236076
	minimum = 0.00233333 (at node 136)
	maximum = 0.037 (at node 2)
Accepted packet rate average = 0.0119028
	minimum = 0.00566667 (at node 84)
	maximum = 0.0186667 (at node 76)
Injected flit rate average = 0.424809
	minimum = 0.047 (at node 136)
	maximum = 0.665 (at node 2)
Accepted flit rate average= 0.213123
	minimum = 0.108 (at node 84)
	maximum = 0.327333 (at node 128)
Injected packet length average = 17.9946
Accepted packet length average = 17.9053
Total in-flight flits = 333424 (230455 measured)
latency change    = 0.441792
throughput change = 0.0100278
Class 0:
Packet latency average = 2288.38
	minimum = 28
	maximum = 5069
Network latency average = 1585.47
	minimum = 23
	maximum = 3960
Slowest packet = 19039
Flit latency average = 2878.95
	minimum = 6
	maximum = 6617
Slowest flit = 41111
Fragmentation average = 83.8805
	minimum = 0
	maximum = 973
Injected packet rate average = 0.0214753
	minimum = 0.00175 (at node 136)
	maximum = 0.03275 (at node 2)
Accepted packet rate average = 0.0118034
	minimum = 0.00725 (at node 84)
	maximum = 0.01675 (at node 128)
Injected flit rate average = 0.386611
	minimum = 0.03525 (at node 136)
	maximum = 0.5895 (at node 2)
Accepted flit rate average= 0.211424
	minimum = 0.132 (at node 84)
	maximum = 0.305 (at node 128)
Injected packet length average = 18.0026
Accepted packet length average = 17.9122
Total in-flight flits = 346315 (270059 measured)
latency change    = 0.341772
throughput change = 0.00803495
Class 0:
Packet latency average = 2979.03
	minimum = 28
	maximum = 6019
Network latency average = 2188.8
	minimum = 23
	maximum = 4901
Slowest packet = 19039
Flit latency average = 3153.66
	minimum = 6
	maximum = 7604
Slowest flit = 35495
Fragmentation average = 110.488
	minimum = 0
	maximum = 1564
Injected packet rate average = 0.0198937
	minimum = 0.005 (at node 136)
	maximum = 0.0282 (at node 110)
Accepted packet rate average = 0.0116406
	minimum = 0.0076 (at node 84)
	maximum = 0.016 (at node 128)
Injected flit rate average = 0.358158
	minimum = 0.093 (at node 136)
	maximum = 0.507 (at node 110)
Accepted flit rate average= 0.209319
	minimum = 0.1408 (at node 2)
	maximum = 0.2948 (at node 128)
Injected packet length average = 18.0036
Accepted packet length average = 17.9817
Total in-flight flits = 354867 (299610 measured)
latency change    = 0.231835
throughput change = 0.0100599
Class 0:
Packet latency average = 3629.35
	minimum = 28
	maximum = 6928
Network latency average = 2801.99
	minimum = 23
	maximum = 5941
Slowest packet = 19039
Flit latency average = 3431.18
	minimum = 6
	maximum = 8274
Slowest flit = 69551
Fragmentation average = 130.756
	minimum = 0
	maximum = 2597
Injected packet rate average = 0.0186649
	minimum = 0.00416667 (at node 136)
	maximum = 0.028 (at node 139)
Accepted packet rate average = 0.0115729
	minimum = 0.0085 (at node 4)
	maximum = 0.0158333 (at node 128)
Injected flit rate average = 0.336089
	minimum = 0.0775 (at node 136)
	maximum = 0.506333 (at node 139)
Accepted flit rate average= 0.207597
	minimum = 0.151167 (at node 2)
	maximum = 0.284833 (at node 128)
Injected packet length average = 18.0065
Accepted packet length average = 17.9382
Total in-flight flits = 360095 (320788 measured)
latency change    = 0.179185
throughput change = 0.00829263
Class 0:
Packet latency average = 4209.87
	minimum = 28
	maximum = 7985
Network latency average = 3336.35
	minimum = 23
	maximum = 6983
Slowest packet = 19039
Flit latency average = 3714.94
	minimum = 6
	maximum = 8999
Slowest flit = 108539
Fragmentation average = 144.367
	minimum = 0
	maximum = 2597
Injected packet rate average = 0.0176592
	minimum = 0.00357143 (at node 136)
	maximum = 0.0252857 (at node 139)
Accepted packet rate average = 0.011497
	minimum = 0.00814286 (at node 2)
	maximum = 0.0147143 (at node 99)
Injected flit rate average = 0.317934
	minimum = 0.0664286 (at node 136)
	maximum = 0.456714 (at node 139)
Accepted flit rate average= 0.206202
	minimum = 0.139571 (at node 2)
	maximum = 0.270714 (at node 99)
Injected packet length average = 18.0038
Accepted packet length average = 17.9352
Total in-flight flits = 362540 (335750 measured)
latency change    = 0.137896
throughput change = 0.00676806
Draining all recorded packets ...
Class 0:
Remaining flits: 72918 72919 72920 72921 72922 72923 72924 72925 72926 72927 [...] (356946 flits)
Measured flits: 342036 342037 342038 342039 342040 342041 342042 342043 342044 342045 [...] (338875 flits)
Class 0:
Remaining flits: 72933 72934 72935 107136 107137 107138 107139 107140 107141 107142 [...] (353095 flits)
Measured flits: 342036 342037 342038 342039 342040 342041 342042 342043 342044 342045 [...] (341090 flits)
Class 0:
Remaining flits: 109226 109227 109228 109229 109230 109231 109232 109233 109234 109235 [...] (348124 flits)
Measured flits: 342036 342037 342038 342039 342040 342041 342042 342043 342044 342045 [...] (340300 flits)
Class 0:
Remaining flits: 116118 116119 116120 116121 116122 116123 116124 116125 116126 116127 [...] (346951 flits)
Measured flits: 342144 342145 342146 342147 342148 342149 342150 342151 342152 342153 [...] (341812 flits)
Class 0:
Remaining flits: 116119 116120 116121 116122 116123 116124 116125 116126 116127 116128 [...] (344168 flits)
Measured flits: 342144 342145 342146 342147 342148 342149 342150 342151 342152 342153 [...] (340929 flits)
Class 0:
Remaining flits: 158689 158690 158691 158692 158693 158694 158695 158696 158697 158698 [...] (343846 flits)
Measured flits: 342155 342156 342157 342158 342159 342160 342161 342864 342865 342866 [...] (341671 flits)
Class 0:
Remaining flits: 158689 158690 158691 158692 158693 158694 158695 158696 158697 158698 [...] (343199 flits)
Measured flits: 343080 343081 343082 343083 343084 343085 343086 343087 343088 343089 [...] (340253 flits)
Class 0:
Remaining flits: 181276 181277 227682 227683 227684 227685 227686 227687 227688 227689 [...] (341726 flits)
Measured flits: 343080 343081 343082 343083 343084 343085 343086 343087 343088 343089 [...] (336558 flits)
Class 0:
Remaining flits: 227682 227683 227684 227685 227686 227687 227688 227689 227690 227691 [...] (336034 flits)
Measured flits: 343314 343315 343316 343317 343318 343319 343320 343321 343322 343323 [...] (327008 flits)
Class 0:
Remaining flits: 227682 227683 227684 227685 227686 227687 227688 227689 227690 227691 [...] (335834 flits)
Measured flits: 343329 343330 343331 352620 352621 352622 352623 352624 352625 352626 [...] (320286 flits)
Class 0:
Remaining flits: 227690 227691 227692 227693 227694 227695 227696 227697 227698 227699 [...] (335838 flits)
Measured flits: 352620 352621 352622 352623 352624 352625 352626 352627 352628 352629 [...] (309543 flits)
Class 0:
Remaining flits: 257958 257959 257960 257961 257962 257963 257964 257965 257966 257967 [...] (336169 flits)
Measured flits: 357642 357643 357644 357645 357646 357647 357648 357649 357650 357651 [...] (293044 flits)
Class 0:
Remaining flits: 257959 257960 257961 257962 257963 257964 257965 257966 257967 257968 [...] (338872 flits)
Measured flits: 357642 357643 357644 357645 357646 357647 357648 357649 357650 357651 [...] (274753 flits)
Class 0:
Remaining flits: 373518 373519 373520 373521 373522 373523 373524 373525 373526 373527 [...] (339962 flits)
Measured flits: 373518 373519 373520 373521 373522 373523 373524 373525 373526 373527 [...] (253083 flits)
Class 0:
Remaining flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (337249 flits)
Measured flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (229277 flits)
Class 0:
Remaining flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (334465 flits)
Measured flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (205459 flits)
Class 0:
Remaining flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (331207 flits)
Measured flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (182776 flits)
Class 0:
Remaining flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (328588 flits)
Measured flits: 389754 389755 389756 389757 389758 389759 389760 389761 389762 389763 [...] (160813 flits)
Class 0:
Remaining flits: 389763 389764 389765 389766 389767 389768 389769 389770 389771 440010 [...] (325063 flits)
Measured flits: 389763 389764 389765 389766 389767 389768 389769 389770 389771 440010 [...] (139779 flits)
Class 0:
Remaining flits: 440010 440011 440012 440013 440014 440015 440016 440017 440018 440019 [...] (322084 flits)
Measured flits: 440010 440011 440012 440013 440014 440015 440016 440017 440018 440019 [...] (119556 flits)
Class 0:
Remaining flits: 440010 440011 440012 440013 440014 440015 440016 440017 440018 440019 [...] (320620 flits)
Measured flits: 440010 440011 440012 440013 440014 440015 440016 440017 440018 440019 [...] (101325 flits)
Class 0:
Remaining flits: 444060 444061 444062 444063 444064 444065 444066 444067 444068 444069 [...] (317075 flits)
Measured flits: 444060 444061 444062 444063 444064 444065 444066 444067 444068 444069 [...] (85623 flits)
Class 0:
Remaining flits: 500220 500221 500222 500223 500224 500225 500226 500227 500228 500229 [...] (315640 flits)
Measured flits: 500220 500221 500222 500223 500224 500225 500226 500227 500228 500229 [...] (71360 flits)
Class 0:
Remaining flits: 500220 500221 500222 500223 500224 500225 500226 500227 500228 500229 [...] (319722 flits)
Measured flits: 500220 500221 500222 500223 500224 500225 500226 500227 500228 500229 [...] (57859 flits)
Class 0:
Remaining flits: 500237 516402 516403 516404 516405 516406 516407 516408 516409 516410 [...] (319586 flits)
Measured flits: 500237 516402 516403 516404 516405 516406 516407 516408 516409 516410 [...] (45965 flits)
Class 0:
Remaining flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (320513 flits)
Measured flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (37284 flits)
Class 0:
Remaining flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (320452 flits)
Measured flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (30261 flits)
Class 0:
Remaining flits: 560664 560665 560666 560667 560668 560669 560670 560671 560672 560673 [...] (322040 flits)
Measured flits: 560664 560665 560666 560667 560668 560669 560670 560671 560672 560673 [...] (24278 flits)
Class 0:
Remaining flits: 560666 560667 560668 560669 560670 560671 560672 560673 560674 560675 [...] (322784 flits)
Measured flits: 560666 560667 560668 560669 560670 560671 560672 560673 560674 560675 [...] (18794 flits)
Class 0:
Remaining flits: 587700 587701 587702 587703 587704 587705 587706 587707 587708 587709 [...] (319232 flits)
Measured flits: 587700 587701 587702 587703 587704 587705 587706 587707 587708 587709 [...] (15308 flits)
Class 0:
Remaining flits: 587703 587704 587705 587706 587707 587708 587709 587710 587711 587712 [...] (319744 flits)
Measured flits: 587703 587704 587705 587706 587707 587708 587709 587710 587711 587712 [...] (12186 flits)
Class 0:
Remaining flits: 763290 763291 763292 763293 763294 763295 763296 763297 763298 763299 [...] (322269 flits)
Measured flits: 763290 763291 763292 763293 763294 763295 763296 763297 763298 763299 [...] (9587 flits)
Class 0:
Remaining flits: 763290 763291 763292 763293 763294 763295 763296 763297 763298 763299 [...] (319869 flits)
Measured flits: 763290 763291 763292 763293 763294 763295 763296 763297 763298 763299 [...] (7582 flits)
Class 0:
Remaining flits: 846018 846019 846020 846021 846022 846023 846024 846025 846026 846027 [...] (317373 flits)
Measured flits: 846018 846019 846020 846021 846022 846023 846024 846025 846026 846027 [...] (6119 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (317944 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (4666 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (316287 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (3826 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (311871 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (2811 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (313622 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (1965 flits)
Class 0:
Remaining flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (314580 flits)
Measured flits: 855270 855271 855272 855273 855274 855275 855276 855277 855278 855279 [...] (1406 flits)
Class 0:
Remaining flits: 855283 855284 855285 855286 855287 1009458 1009459 1009460 1009461 1009462 [...] (312021 flits)
Measured flits: 855283 855284 855285 855286 855287 1009458 1009459 1009460 1009461 1009462 [...] (1117 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (313738 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (790 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (312980 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (602 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (312073 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (514 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (310897 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (379 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (311222 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (303 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (310220 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (128 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (309483 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (37 flits)
Class 0:
Remaining flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (306082 flits)
Measured flits: 1009458 1009459 1009460 1009461 1009462 1009463 1009464 1009465 1009466 1009467 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1362222 1362223 1362224 1362225 1362226 1362227 1362228 1362229 1362230 1362231 [...] (271999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1362222 1362223 1362224 1362225 1362226 1362227 1362228 1362229 1362230 1362231 [...] (238283 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1362222 1362223 1362224 1362225 1362226 1362227 1362228 1362229 1362230 1362231 [...] (204360 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1362222 1362223 1362224 1362225 1362226 1362227 1362228 1362229 1362230 1362231 [...] (171505 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1362222 1362223 1362224 1362225 1362226 1362227 1362228 1362229 1362230 1362231 [...] (139528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1427058 1427059 1427060 1427061 1427062 1427063 1427064 1427065 1427066 1427067 [...] (109415 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1427058 1427059 1427060 1427061 1427062 1427063 1427064 1427065 1427066 1427067 [...] (81418 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1427058 1427059 1427060 1427061 1427062 1427063 1427064 1427065 1427066 1427067 [...] (57322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1475136 1475137 1475138 1475139 1475140 1475141 1475142 1475143 1475144 1475145 [...] (38200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1475136 1475137 1475138 1475139 1475140 1475141 1475142 1475143 1475144 1475145 [...] (22414 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1718514 1718515 1718516 1718517 1718518 1718519 1718520 1718521 1718522 1718523 [...] (10522 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1748087 1805688 1805689 1805690 1805691 1805692 1805693 1805694 1805695 1805696 [...] (3888 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002122 2002123 2002124 2002125 2002126 2002127 2002128 2002129 2002130 2002131 [...] (549 flits)
Measured flits: (0 flits)
Time taken is 71797 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14589.1 (1 samples)
	minimum = 28 (1 samples)
	maximum = 49874 (1 samples)
Network latency average = 8856.39 (1 samples)
	minimum = 23 (1 samples)
	maximum = 41202 (1 samples)
Flit latency average = 8390.22 (1 samples)
	minimum = 6 (1 samples)
	maximum = 41185 (1 samples)
Fragmentation average = 200.797 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7182 (1 samples)
Injected packet rate average = 0.0176592 (1 samples)
	minimum = 0.00357143 (1 samples)
	maximum = 0.0252857 (1 samples)
Accepted packet rate average = 0.011497 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0147143 (1 samples)
Injected flit rate average = 0.317934 (1 samples)
	minimum = 0.0664286 (1 samples)
	maximum = 0.456714 (1 samples)
Accepted flit rate average = 0.206202 (1 samples)
	minimum = 0.139571 (1 samples)
	maximum = 0.270714 (1 samples)
Injected packet size average = 18.0038 (1 samples)
Accepted packet size average = 17.9352 (1 samples)
Hops average = 5.04737 (1 samples)
Total run time 92.9732
