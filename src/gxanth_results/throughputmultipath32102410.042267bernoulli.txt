BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 320.84
	minimum = 25
	maximum = 965
Network latency average = 304.613
	minimum = 23
	maximum = 942
Slowest packet = 252
Flit latency average = 247.425
	minimum = 6
	maximum = 950
Slowest flit = 1611
Fragmentation average = 150.304
	minimum = 0
	maximum = 845
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0115729
	minimum = 0.004 (at node 108)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.235568
	minimum = 0.094 (at node 108)
	maximum = 0.403 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 20.3551
Total in-flight flits = 96143 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 604.288
	minimum = 25
	maximum = 1927
Network latency average = 584.532
	minimum = 23
	maximum = 1886
Slowest packet = 557
Flit latency average = 521.266
	minimum = 6
	maximum = 1934
Slowest flit = 6793
Fragmentation average = 177.474
	minimum = 0
	maximum = 1539
Injected packet rate average = 0.0418438
	minimum = 0.032 (at node 50)
	maximum = 0.052 (at node 154)
Accepted packet rate average = 0.0126641
	minimum = 0.0065 (at node 108)
	maximum = 0.0215 (at node 177)
Injected flit rate average = 0.74988
	minimum = 0.5705 (at node 50)
	maximum = 0.93 (at node 154)
Accepted flit rate average= 0.242141
	minimum = 0.1295 (at node 135)
	maximum = 0.398 (at node 177)
Injected packet length average = 17.921
Accepted packet length average = 19.1203
Total in-flight flits = 196242 (0 measured)
latency change    = 0.469061
throughput change = 0.027145
Class 0:
Packet latency average = 1365.22
	minimum = 25
	maximum = 2849
Network latency average = 1340.42
	minimum = 23
	maximum = 2849
Slowest packet = 1019
Flit latency average = 1297.16
	minimum = 6
	maximum = 2871
Slowest flit = 7812
Fragmentation average = 195.434
	minimum = 0
	maximum = 2007
Injected packet rate average = 0.0418542
	minimum = 0.024 (at node 17)
	maximum = 0.056 (at node 48)
Accepted packet rate average = 0.0142813
	minimum = 0.007 (at node 38)
	maximum = 0.024 (at node 24)
Injected flit rate average = 0.753583
	minimum = 0.42 (at node 17)
	maximum = 1 (at node 48)
Accepted flit rate average= 0.255677
	minimum = 0.124 (at node 38)
	maximum = 0.421 (at node 160)
Injected packet length average = 18.005
Accepted packet length average = 17.903
Total in-flight flits = 291800 (0 measured)
latency change    = 0.557371
throughput change = 0.0529436
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 81.6198
	minimum = 27
	maximum = 323
Network latency average = 52.5592
	minimum = 23
	maximum = 292
Slowest packet = 29328
Flit latency average = 1803.54
	minimum = 6
	maximum = 3839
Slowest flit = 14932
Fragmentation average = 14.9778
	minimum = 0
	maximum = 59
Injected packet rate average = 0.0426406
	minimum = 0.028 (at node 181)
	maximum = 0.054 (at node 156)
Accepted packet rate average = 0.014125
	minimum = 0.006 (at node 79)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.767135
	minimum = 0.5 (at node 181)
	maximum = 0.984 (at node 167)
Accepted flit rate average= 0.254255
	minimum = 0.116 (at node 144)
	maximum = 0.434 (at node 16)
Injected packet length average = 17.9907
Accepted packet length average = 18.0004
Total in-flight flits = 390349 (135098 measured)
latency change    = 15.7266
throughput change = 0.00559231
Class 0:
Packet latency average = 81.6034
	minimum = 23
	maximum = 358
Network latency average = 51.4279
	minimum = 23
	maximum = 292
Slowest packet = 29328
Flit latency average = 2099.18
	minimum = 6
	maximum = 4753
Slowest flit = 28457
Fragmentation average = 14.4302
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0424661
	minimum = 0.0265 (at node 104)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0139505
	minimum = 0.0085 (at node 79)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.764622
	minimum = 0.479 (at node 104)
	maximum = 0.9545 (at node 118)
Accepted flit rate average= 0.250568
	minimum = 0.164 (at node 160)
	maximum = 0.3965 (at node 129)
Injected packet length average = 18.0055
Accepted packet length average = 17.9612
Total in-flight flits = 489108 (269776 measured)
latency change    = 0.000201784
throughput change = 0.0147166
Class 0:
Packet latency average = 81.7136
	minimum = 23
	maximum = 509
Network latency average = 52.8125
	minimum = 23
	maximum = 477
Slowest packet = 41107
Flit latency average = 2377.45
	minimum = 6
	maximum = 5704
Slowest flit = 38869
Fragmentation average = 14.8801
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0423819
	minimum = 0.0336667 (at node 104)
	maximum = 0.0506667 (at node 9)
Accepted packet rate average = 0.0138559
	minimum = 0.009 (at node 79)
	maximum = 0.0193333 (at node 129)
Injected flit rate average = 0.762951
	minimum = 0.604667 (at node 104)
	maximum = 0.912667 (at node 9)
Accepted flit rate average= 0.249609
	minimum = 0.164667 (at node 105)
	maximum = 0.349333 (at node 129)
Injected packet length average = 18.0018
Accepted packet length average = 18.0147
Total in-flight flits = 587441 (404102 measured)
latency change    = 0.00134948
throughput change = 0.00383933
Draining remaining packets ...
Class 0:
Remaining flits: 34002 34003 34004 34005 34006 34007 34008 34009 34010 34011 [...] (551086 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (403093 flits)
Class 0:
Remaining flits: 42840 42841 42842 42843 42844 42845 42846 42847 42848 42849 [...] (515736 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (400722 flits)
Class 0:
Remaining flits: 63954 63955 63956 63957 63958 63959 63960 63961 63962 63963 [...] (480617 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (393872 flits)
Class 0:
Remaining flits: 63954 63955 63956 63957 63958 63959 63960 63961 63962 63963 [...] (445402 flits)
Measured flits: 433872 433873 433874 433875 433876 433877 433878 433879 433880 433881 [...] (381043 flits)
Class 0:
Remaining flits: 93276 93277 93278 93279 93280 93281 93282 93283 93284 93285 [...] (410097 flits)
Measured flits: 433908 433909 433910 433911 433912 433913 433914 433915 433916 433917 [...] (362849 flits)
Class 0:
Remaining flits: 93276 93277 93278 93279 93280 93281 93282 93283 93284 93285 [...] (375924 flits)
Measured flits: 433908 433909 433910 433911 433912 433913 433914 433915 433916 433917 [...] (341359 flits)
Class 0:
Remaining flits: 118081 118082 118083 118084 118085 118086 118087 118088 118089 118090 [...] (341584 flits)
Measured flits: 434088 434089 434090 434091 434092 434093 434094 434095 434096 434097 [...] (316635 flits)
Class 0:
Remaining flits: 123300 123301 123302 123303 123304 123305 123306 123307 123308 123309 [...] (307657 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (289153 flits)
Class 0:
Remaining flits: 145566 145567 145568 145569 145570 145571 145572 145573 145574 145575 [...] (273476 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (260373 flits)
Class 0:
Remaining flits: 153180 153181 153182 153183 153184 153185 153186 153187 153188 153189 [...] (239572 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (229894 flits)
Class 0:
Remaining flits: 216018 216019 216020 216021 216022 216023 216024 216025 216026 216027 [...] (205632 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (199099 flits)
Class 0:
Remaining flits: 216018 216019 216020 216021 216022 216023 216024 216025 216026 216027 [...] (171734 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (167271 flits)
Class 0:
Remaining flits: 240174 240175 240176 240177 240178 240179 240180 240181 240182 240183 [...] (138339 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (135445 flits)
Class 0:
Remaining flits: 240174 240175 240176 240177 240178 240179 240180 240181 240182 240183 [...] (104857 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (103163 flits)
Class 0:
Remaining flits: 240188 240189 240190 240191 269046 269047 269048 269049 269050 269051 [...] (71886 flits)
Measured flits: 434178 434179 434180 434181 434182 434183 434184 434185 434186 434187 [...] (71067 flits)
Class 0:
Remaining flits: 305892 305893 305894 305895 305896 305897 305898 305899 305900 305901 [...] (40015 flits)
Measured flits: 435258 435259 435260 435261 435262 435263 435264 435265 435266 435267 [...] (39610 flits)
Class 0:
Remaining flits: 392922 392923 392924 392925 392926 392927 392928 392929 392930 392931 [...] (13895 flits)
Measured flits: 437526 437527 437528 437529 437530 437531 437532 437533 437534 437535 [...] (13828 flits)
Class 0:
Remaining flits: 429678 429679 429680 429681 429682 429683 429684 429685 429686 429687 [...] (2676 flits)
Measured flits: 437542 437543 489276 489277 489278 489279 489280 489281 489282 489283 [...] (2658 flits)
Class 0:
Remaining flits: 600876 600877 600878 600879 600880 600881 600882 600883 600884 600885 [...] (413 flits)
Measured flits: 600876 600877 600878 600879 600880 600881 600882 600883 600884 600885 [...] (413 flits)
Time taken is 25425 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11323.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21123 (1 samples)
Network latency average = 11295.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21056 (1 samples)
Flit latency average = 9052.69 (1 samples)
	minimum = 6 (1 samples)
	maximum = 21608 (1 samples)
Fragmentation average = 173.172 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3369 (1 samples)
Injected packet rate average = 0.0423819 (1 samples)
	minimum = 0.0336667 (1 samples)
	maximum = 0.0506667 (1 samples)
Accepted packet rate average = 0.0138559 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.762951 (1 samples)
	minimum = 0.604667 (1 samples)
	maximum = 0.912667 (1 samples)
Accepted flit rate average = 0.249609 (1 samples)
	minimum = 0.164667 (1 samples)
	maximum = 0.349333 (1 samples)
Injected packet size average = 18.0018 (1 samples)
Accepted packet size average = 18.0147 (1 samples)
Hops average = 5.06923 (1 samples)
Total run time 25.4808
