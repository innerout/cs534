BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 325.907
	minimum = 23
	maximum = 916
Network latency average = 314.779
	minimum = 23
	maximum = 899
Slowest packet = 431
Flit latency average = 225.317
	minimum = 6
	maximum = 944
Slowest flit = 4111
Fragmentation average = 209.622
	minimum = 0
	maximum = 715
Injected packet rate average = 0.0256823
	minimum = 0.009 (at node 116)
	maximum = 0.037 (at node 135)
Accepted packet rate average = 0.009625
	minimum = 0.003 (at node 25)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.457896
	minimum = 0.151 (at node 116)
	maximum = 0.661 (at node 135)
Accepted flit rate average= 0.215776
	minimum = 0.108 (at node 116)
	maximum = 0.359 (at node 44)
Injected packet length average = 17.8292
Accepted packet length average = 22.4183
Total in-flight flits = 47401 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.572
	minimum = 23
	maximum = 1921
Network latency average = 558.35
	minimum = 23
	maximum = 1921
Slowest packet = 261
Flit latency average = 436.686
	minimum = 6
	maximum = 1904
Slowest flit = 4715
Fragmentation average = 287.903
	minimum = 0
	maximum = 1466
Injected packet rate average = 0.0218542
	minimum = 0.0055 (at node 116)
	maximum = 0.0295 (at node 65)
Accepted packet rate average = 0.0110599
	minimum = 0.006 (at node 25)
	maximum = 0.018 (at node 103)
Injected flit rate average = 0.389909
	minimum = 0.098 (at node 116)
	maximum = 0.531 (at node 81)
Accepted flit rate average= 0.221312
	minimum = 0.139 (at node 96)
	maximum = 0.3445 (at node 103)
Injected packet length average = 17.8414
Accepted packet length average = 20.0104
Total in-flight flits = 66306 (0 measured)
latency change    = 0.460037
throughput change = 0.0250165
Class 0:
Packet latency average = 1305.53
	minimum = 40
	maximum = 2771
Network latency average = 1079.88
	minimum = 32
	maximum = 2761
Slowest packet = 681
Flit latency average = 958.607
	minimum = 6
	maximum = 2822
Slowest flit = 15739
Fragmentation average = 320.443
	minimum = 4
	maximum = 2104
Injected packet rate average = 0.010125
	minimum = 0 (at node 4)
	maximum = 0.032 (at node 42)
Accepted packet rate average = 0.0124479
	minimum = 0.004 (at node 7)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.181542
	minimum = 0 (at node 4)
	maximum = 0.576 (at node 42)
Accepted flit rate average= 0.219156
	minimum = 0.08 (at node 77)
	maximum = 0.427 (at node 16)
Injected packet length average = 17.93
Accepted packet length average = 17.6059
Total in-flight flits = 59292 (0 measured)
latency change    = 0.53768
throughput change = 0.00983887
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1495.52
	minimum = 44
	maximum = 2948
Network latency average = 324.372
	minimum = 29
	maximum = 916
Slowest packet = 10369
Flit latency average = 1161.88
	minimum = 6
	maximum = 3863
Slowest flit = 12222
Fragmentation average = 155.399
	minimum = 2
	maximum = 847
Injected packet rate average = 0.009
	minimum = 0 (at node 2)
	maximum = 0.041 (at node 29)
Accepted packet rate average = 0.0117448
	minimum = 0.004 (at node 85)
	maximum = 0.022 (at node 145)
Injected flit rate average = 0.16163
	minimum = 0 (at node 2)
	maximum = 0.734 (at node 29)
Accepted flit rate average= 0.209635
	minimum = 0.049 (at node 85)
	maximum = 0.39 (at node 129)
Injected packet length average = 17.9589
Accepted packet length average = 17.8492
Total in-flight flits = 50128 (20245 measured)
latency change    = 0.127041
throughput change = 0.0454161
Class 0:
Packet latency average = 1909.91
	minimum = 44
	maximum = 3960
Network latency average = 520.892
	minimum = 29
	maximum = 1901
Slowest packet = 10369
Flit latency average = 1162.91
	minimum = 6
	maximum = 4863
Slowest flit = 11544
Fragmentation average = 189.759
	minimum = 2
	maximum = 1538
Injected packet rate average = 0.0094974
	minimum = 0 (at node 6)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0116432
	minimum = 0.0065 (at node 1)
	maximum = 0.0175 (at node 99)
Injected flit rate average = 0.170909
	minimum = 0 (at node 10)
	maximum = 0.5505 (at node 42)
Accepted flit rate average= 0.206508
	minimum = 0.119 (at node 1)
	maximum = 0.3095 (at node 29)
Injected packet length average = 17.9953
Accepted packet length average = 17.7363
Total in-flight flits = 45639 (28464 measured)
latency change    = 0.216967
throughput change = 0.0151452
Class 0:
Packet latency average = 2256.27
	minimum = 44
	maximum = 4636
Network latency average = 629.102
	minimum = 27
	maximum = 2816
Slowest packet = 10369
Flit latency average = 1123.47
	minimum = 6
	maximum = 5727
Slowest flit = 25206
Fragmentation average = 205.892
	minimum = 0
	maximum = 2047
Injected packet rate average = 0.0103368
	minimum = 0 (at node 10)
	maximum = 0.03 (at node 166)
Accepted packet rate average = 0.0115503
	minimum = 0.00633333 (at node 1)
	maximum = 0.0163333 (at node 129)
Injected flit rate average = 0.185905
	minimum = 0 (at node 10)
	maximum = 0.539667 (at node 166)
Accepted flit rate average= 0.205951
	minimum = 0.114 (at node 1)
	maximum = 0.302333 (at node 129)
Injected packet length average = 17.9847
Accepted packet length average = 17.8308
Total in-flight flits = 47872 (36429 measured)
latency change    = 0.153511
throughput change = 0.00270172
Class 0:
Packet latency average = 2629.5
	minimum = 44
	maximum = 5956
Network latency average = 689.717
	minimum = 23
	maximum = 3862
Slowest packet = 10369
Flit latency average = 1105.75
	minimum = 6
	maximum = 6585
Slowest flit = 28331
Fragmentation average = 215.659
	minimum = 0
	maximum = 3212
Injected packet rate average = 0.0109362
	minimum = 0 (at node 10)
	maximum = 0.032 (at node 166)
Accepted packet rate average = 0.0116172
	minimum = 0.00625 (at node 1)
	maximum = 0.016 (at node 165)
Injected flit rate average = 0.196639
	minimum = 0 (at node 10)
	maximum = 0.576 (at node 166)
Accepted flit rate average= 0.207687
	minimum = 0.115 (at node 1)
	maximum = 0.2805 (at node 165)
Injected packet length average = 17.9806
Accepted packet length average = 17.8776
Total in-flight flits = 50916 (43271 measured)
latency change    = 0.14194
throughput change = 0.00835925
Class 0:
Packet latency average = 2955.3
	minimum = 44
	maximum = 7182
Network latency average = 770.879
	minimum = 23
	maximum = 4814
Slowest packet = 10369
Flit latency average = 1111.87
	minimum = 6
	maximum = 7775
Slowest flit = 20365
Fragmentation average = 226.92
	minimum = 0
	maximum = 3719
Injected packet rate average = 0.0112021
	minimum = 0 (at node 19)
	maximum = 0.0308 (at node 166)
Accepted packet rate average = 0.0116313
	minimum = 0.0078 (at node 2)
	maximum = 0.016 (at node 51)
Injected flit rate average = 0.201609
	minimum = 0 (at node 19)
	maximum = 0.5522 (at node 166)
Accepted flit rate average= 0.208293
	minimum = 0.1444 (at node 2)
	maximum = 0.2886 (at node 128)
Injected packet length average = 17.9975
Accepted packet length average = 17.908
Total in-flight flits = 52921 (47637 measured)
latency change    = 0.110241
throughput change = 0.00290557
Class 0:
Packet latency average = 3282.23
	minimum = 44
	maximum = 7997
Network latency average = 841.355
	minimum = 23
	maximum = 5636
Slowest packet = 10369
Flit latency average = 1122.9
	minimum = 6
	maximum = 8618
Slowest flit = 29735
Fragmentation average = 235.186
	minimum = 0
	maximum = 4279
Injected packet rate average = 0.0111701
	minimum = 0 (at node 19)
	maximum = 0.0293333 (at node 166)
Accepted packet rate average = 0.0116076
	minimum = 0.00816667 (at node 2)
	maximum = 0.0155 (at node 51)
Injected flit rate average = 0.201113
	minimum = 0 (at node 19)
	maximum = 0.525667 (at node 166)
Accepted flit rate average= 0.207936
	minimum = 0.145167 (at node 2)
	maximum = 0.278333 (at node 128)
Injected packet length average = 18.0045
Accepted packet length average = 17.9137
Total in-flight flits = 51410 (47724 measured)
latency change    = 0.0996075
throughput change = 0.00171661
Class 0:
Packet latency average = 3613.05
	minimum = 44
	maximum = 8765
Network latency average = 897.778
	minimum = 23
	maximum = 6709
Slowest packet = 10369
Flit latency average = 1133
	minimum = 6
	maximum = 9589
Slowest flit = 30422
Fragmentation average = 242.822
	minimum = 0
	maximum = 4954
Injected packet rate average = 0.0111741
	minimum = 0 (at node 19)
	maximum = 0.0251429 (at node 166)
Accepted packet rate average = 0.0115655
	minimum = 0.00857143 (at node 2)
	maximum = 0.0148571 (at node 95)
Injected flit rate average = 0.201083
	minimum = 0 (at node 19)
	maximum = 0.450571 (at node 166)
Accepted flit rate average= 0.207228
	minimum = 0.157143 (at node 171)
	maximum = 0.265857 (at node 128)
Injected packet length average = 17.9955
Accepted packet length average = 17.9178
Total in-flight flits = 51155 (48579 measured)
latency change    = 0.0915611
throughput change = 0.00341334
Draining all recorded packets ...
Class 0:
Remaining flits: 12987 12988 12989 12990 12991 12992 12993 12994 12995 19476 [...] (53312 flits)
Measured flits: 186570 186571 186572 186573 186574 186575 186576 186577 186578 186579 [...] (51335 flits)
Class 0:
Remaining flits: 12987 12988 12989 12990 12991 12992 12993 12994 12995 19486 [...] (51306 flits)
Measured flits: 186570 186571 186572 186573 186574 186575 186576 186577 186578 186579 [...] (49696 flits)
Class 0:
Remaining flits: 19486 19487 19488 19489 19490 19491 19492 19493 20369 20370 [...] (53205 flits)
Measured flits: 187758 187759 187760 187761 187762 187763 187764 187765 187766 187767 [...] (51466 flits)
Class 0:
Remaining flits: 19486 19487 19488 19489 19490 19491 19492 19493 20369 20370 [...] (55857 flits)
Measured flits: 207930 207931 207932 207933 207934 207935 214788 214789 214790 214791 [...] (53687 flits)
Class 0:
Remaining flits: 20370 20371 20372 20373 20374 20375 21898 21899 21900 21901 [...] (57173 flits)
Measured flits: 207934 207935 232110 232111 232112 232113 232114 232115 232116 232117 [...] (53985 flits)
Class 0:
Remaining flits: 20370 20371 20372 20373 20374 20375 22392 22393 22394 22395 [...] (54694 flits)
Measured flits: 207934 207935 232110 232111 232112 232113 232114 232115 232116 232117 [...] (51092 flits)
Class 0:
Remaining flits: 20370 20371 20372 20373 20374 20375 22392 22393 22394 22395 [...] (55896 flits)
Measured flits: 207934 207935 232125 232126 232127 236152 236153 236154 236155 236156 [...] (51262 flits)
Class 0:
Remaining flits: 22392 22393 22394 22395 22396 22397 22398 22399 22400 22401 [...] (57169 flits)
Measured flits: 236152 236153 236154 236155 236156 236157 236158 236159 236330 236331 [...] (50141 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (56756 flits)
Measured flits: 236152 236153 236154 236155 236156 236157 236158 236159 238878 238879 [...] (47621 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (58010 flits)
Measured flits: 236152 236153 236154 236155 236156 236157 236158 236159 238878 238879 [...] (45019 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (57881 flits)
Measured flits: 238879 238880 238881 238882 238883 238884 238885 238886 238887 238888 [...] (41237 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (58485 flits)
Measured flits: 238879 238880 238881 238882 238883 238884 238885 238886 238887 238888 [...] (38061 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (57676 flits)
Measured flits: 238879 238880 238881 238882 238883 238884 238885 238886 238887 238888 [...] (34703 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (58225 flits)
Measured flits: 251835 251836 251837 281257 281258 281259 281260 281261 281262 281263 [...] (31262 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (57117 flits)
Measured flits: 251835 251836 251837 281257 281258 281259 281260 281261 281262 281263 [...] (28985 flits)
Class 0:
Remaining flits: 43902 43903 43904 43905 43906 43907 43908 43909 43910 43911 [...] (55737 flits)
Measured flits: 251835 251836 251837 281257 281258 281259 281260 281261 281262 281263 [...] (25681 flits)
Class 0:
Remaining flits: 59138 59139 59140 59141 59142 59143 59144 59145 59146 59147 [...] (58459 flits)
Measured flits: 251835 251836 251837 281257 281258 281259 281260 281261 281262 281263 [...] (23955 flits)
Class 0:
Remaining flits: 178992 178993 178994 178995 178996 178997 178998 178999 179000 179001 [...] (60813 flits)
Measured flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (23209 flits)
Class 0:
Remaining flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (60530 flits)
Measured flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (20840 flits)
Class 0:
Remaining flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (57597 flits)
Measured flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (18094 flits)
Class 0:
Remaining flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (61050 flits)
Measured flits: 281257 281258 281259 281260 281261 281262 281263 281264 281265 281266 [...] (16908 flits)
Class 0:
Remaining flits: 281261 281262 281263 281264 281265 281266 281267 360936 360937 360938 [...] (62639 flits)
Measured flits: 281261 281262 281263 281264 281265 281266 281267 360936 360937 360938 [...] (14153 flits)
Class 0:
Remaining flits: 281261 281262 281263 281264 281265 281266 281267 360936 360937 360938 [...] (60633 flits)
Measured flits: 281261 281262 281263 281264 281265 281266 281267 360936 360937 360938 [...] (11614 flits)
Class 0:
Remaining flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (62452 flits)
Measured flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (10925 flits)
Class 0:
Remaining flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (62630 flits)
Measured flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (10452 flits)
Class 0:
Remaining flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (61270 flits)
Measured flits: 360936 360937 360938 360939 360940 360941 360942 360943 360944 360945 [...] (8893 flits)
Class 0:
Remaining flits: 360943 360944 360945 360946 360947 360948 360949 360950 360951 360952 [...] (61749 flits)
Measured flits: 360943 360944 360945 360946 360947 360948 360949 360950 360951 360952 [...] (7985 flits)
Class 0:
Remaining flits: 360943 360944 360945 360946 360947 360948 360949 360950 360951 360952 [...] (63156 flits)
Measured flits: 360943 360944 360945 360946 360947 360948 360949 360950 360951 360952 [...] (6630 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (64229 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (6359 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61632 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (5861 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61531 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (5299 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (62600 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (4543 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61672 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (3884 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61302 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (3728 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61583 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (3345 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (60887 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (3065 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (63062 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (2784 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (59989 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (2373 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (60832 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (1938 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61077 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (1570 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61566 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (1398 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61146 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (1198 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (60266 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (1184 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (61286 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (878 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (62729 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (736 flits)
Class 0:
Remaining flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (63804 flits)
Measured flits: 587790 587791 587792 587793 587794 587795 587796 587797 587798 587799 [...] (804 flits)
Class 0:
Remaining flits: 587801 587802 587803 587804 587805 587806 587807 754470 754471 754472 [...] (64071 flits)
Measured flits: 587801 587802 587803 587804 587805 587806 587807 754470 754471 754472 [...] (603 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (64346 flits)
Measured flits: 1324404 1324405 1324406 1324407 1324408 1324409 1324410 1324411 1324412 1324413 [...] (394 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (65419 flits)
Measured flits: 1324404 1324405 1324406 1324407 1324408 1324409 1324410 1324411 1324412 1324413 [...] (651 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (64598 flits)
Measured flits: 1324404 1324405 1324406 1324407 1324408 1324409 1324410 1324411 1324412 1324413 [...] (447 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (66401 flits)
Measured flits: 1337184 1337185 1337186 1337187 1337188 1337189 1337190 1337191 1337192 1337193 [...] (478 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (66263 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (359 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (66249 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (304 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (65445 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (282 flits)
Class 0:
Remaining flits: 791982 791983 791984 791985 791986 791987 791988 791989 791990 791991 [...] (63882 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (234 flits)
Class 0:
Remaining flits: 791983 791984 791985 791986 791987 791988 791989 791990 791991 791992 [...] (62826 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (234 flits)
Class 0:
Remaining flits: 791983 791984 791985 791986 791987 791988 791989 791990 791991 791992 [...] (63308 flits)
Measured flits: 1713816 1713817 1713818 1713819 1713820 1713821 1713822 1713823 1713824 1713825 [...] (286 flits)
Class 0:
Remaining flits: 791983 791984 791985 791986 791987 791988 791989 791990 791991 791992 [...] (63009 flits)
Measured flits: 1912446 1912447 1912448 1912449 1912450 1912451 1912452 1912453 1912454 1912455 [...] (187 flits)
Class 0:
Remaining flits: 791983 791984 791985 791986 791987 791988 791989 791990 791991 791992 [...] (62860 flits)
Measured flits: 1912446 1912447 1912448 1912449 1912450 1912451 1912452 1912453 1912454 1912455 [...] (113 flits)
Class 0:
Remaining flits: 1400886 1400887 1400888 1400889 1400890 1400891 1400892 1400893 1400894 1400895 [...] (64457 flits)
Measured flits: 1912459 1912460 1912461 1912462 1912463 2112255 2112256 2112257 2112258 2112259 [...] (68 flits)
Class 0:
Remaining flits: 1400886 1400887 1400888 1400889 1400890 1400891 1400892 1400893 1400894 1400895 [...] (63495 flits)
Measured flits: 2135016 2135017 2135018 2135019 2135020 2135021 2135022 2135023 2135024 2135025 [...] (54 flits)
Class 0:
Remaining flits: 1503306 1503307 1503308 1503309 1503310 1503311 1503312 1503313 1503314 1503315 [...] (60999 flits)
Measured flits: 2199924 2199925 2199926 2199927 2199928 2199929 2199930 2199931 2199932 2199933 [...] (36 flits)
Class 0:
Remaining flits: 1503306 1503307 1503308 1503309 1503310 1503311 1503312 1503313 1503314 1503315 [...] (63952 flits)
Measured flits: 2199924 2199925 2199926 2199927 2199928 2199929 2199930 2199931 2199932 2199933 [...] (36 flits)
Class 0:
Remaining flits: 1503306 1503307 1503308 1503309 1503310 1503311 1503312 1503313 1503314 1503315 [...] (64773 flits)
Measured flits: 2199924 2199925 2199926 2199927 2199928 2199929 2199930 2199931 2199932 2199933 [...] (36 flits)
Class 0:
Remaining flits: 1503306 1503307 1503308 1503309 1503310 1503311 1503312 1503313 1503314 1503315 [...] (61315 flits)
Measured flits: 2232216 2232217 2232218 2232219 2232220 2232221 2232222 2232223 2232224 2232225 [...] (18 flits)
Class 0:
Remaining flits: 1503317 1503318 1503319 1503320 1503321 1503322 1503323 1574496 1574497 1574498 [...] (61674 flits)
Measured flits: 2232216 2232217 2232218 2232219 2232220 2232221 2232222 2232223 2232224 2232225 [...] (18 flits)
Class 0:
Remaining flits: 1574496 1574497 1574498 1574499 1574500 1574501 1574502 1574503 1574504 1574505 [...] (62761 flits)
Measured flits: 2232216 2232217 2232218 2232219 2232220 2232221 2232222 2232223 2232224 2232225 [...] (18 flits)
Class 0:
Remaining flits: 1574496 1574497 1574498 1574499 1574500 1574501 1574502 1574503 1574504 1574505 [...] (64176 flits)
Measured flits: 2232216 2232217 2232218 2232219 2232220 2232221 2232222 2232223 2232224 2232225 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1574496 1574497 1574498 1574499 1574500 1574501 1574502 1574503 1574504 1574505 [...] (40676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1928646 1928647 1928648 1928649 1928650 1928651 1928652 1928653 1928654 1928655 [...] (27093 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1928660 1928661 1928662 1928663 1935234 1935235 1935236 1935237 1935238 1935239 [...] (15157 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1984464 1984465 1984466 1984467 1984468 1984469 1984470 1984471 1984472 1984473 [...] (7126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2254564 2254565 2254566 2254567 2254568 2254569 2254570 2254571 2354814 2354815 [...] (1907 flits)
Measured flits: (0 flits)
Time taken is 84923 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10974.5 (1 samples)
	minimum = 44 (1 samples)
	maximum = 69051 (1 samples)
Network latency average = 1598.07 (1 samples)
	minimum = 23 (1 samples)
	maximum = 43752 (1 samples)
Flit latency average = 1756.64 (1 samples)
	minimum = 6 (1 samples)
	maximum = 50839 (1 samples)
Fragmentation average = 276.868 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18171 (1 samples)
Injected packet rate average = 0.0111741 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0251429 (1 samples)
Accepted packet rate average = 0.0115655 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0148571 (1 samples)
Injected flit rate average = 0.201083 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.450571 (1 samples)
Accepted flit rate average = 0.207228 (1 samples)
	minimum = 0.157143 (1 samples)
	maximum = 0.265857 (1 samples)
Injected packet size average = 17.9955 (1 samples)
Accepted packet size average = 17.9178 (1 samples)
Hops average = 5.05656 (1 samples)
Total run time 123.302
