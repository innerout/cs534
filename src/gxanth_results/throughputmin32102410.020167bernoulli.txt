BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 255.722
	minimum = 23
	maximum = 843
Network latency average = 251.42
	minimum = 23
	maximum = 843
Slowest packet = 407
Flit latency average = 185.999
	minimum = 6
	maximum = 875
Slowest flit = 6999
Fragmentation average = 138.6
	minimum = 0
	maximum = 704
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00968229
	minimum = 0.003 (at node 174)
	maximum = 0.018 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.201333
	minimum = 0.073 (at node 174)
	maximum = 0.339 (at node 48)
Injected packet length average = 17.8234
Accepted packet length average = 20.794
Total in-flight flits = 30950 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 443.844
	minimum = 23
	maximum = 1707
Network latency average = 439.073
	minimum = 23
	maximum = 1699
Slowest packet = 747
Flit latency average = 352.507
	minimum = 6
	maximum = 1840
Slowest flit = 9839
Fragmentation average = 186.236
	minimum = 0
	maximum = 1391
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0109896
	minimum = 0.0065 (at node 135)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.211503
	minimum = 0.131 (at node 135)
	maximum = 0.328 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.2457
Total in-flight flits = 56987 (0 measured)
latency change    = 0.423848
throughput change = 0.0480811
Class 0:
Packet latency average = 916.14
	minimum = 23
	maximum = 2757
Network latency average = 911.081
	minimum = 23
	maximum = 2749
Slowest packet = 255
Flit latency average = 822.596
	minimum = 6
	maximum = 2771
Slowest flit = 14534
Fragmentation average = 232.21
	minimum = 0
	maximum = 1654
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0121823
	minimum = 0.004 (at node 62)
	maximum = 0.022 (at node 55)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.220073
	minimum = 0.08 (at node 62)
	maximum = 0.375 (at node 131)
Injected packet length average = 17.9811
Accepted packet length average = 18.065
Total in-flight flits = 84411 (0 measured)
latency change    = 0.515528
throughput change = 0.0389431
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 191.771
	minimum = 24
	maximum = 930
Network latency average = 186.979
	minimum = 24
	maximum = 930
Slowest packet = 11741
Flit latency average = 1152.94
	minimum = 6
	maximum = 3664
Slowest flit = 22418
Fragmentation average = 61.1939
	minimum = 1
	maximum = 607
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0122969
	minimum = 0.003 (at node 173)
	maximum = 0.022 (at node 46)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.220312
	minimum = 0.067 (at node 173)
	maximum = 0.38 (at node 46)
Injected packet length average = 17.9618
Accepted packet length average = 17.9161
Total in-flight flits = 113733 (62860 measured)
latency change    = 3.77727
throughput change = 0.00108747
Class 0:
Packet latency average = 637.039
	minimum = 24
	maximum = 1905
Network latency average = 632.335
	minimum = 23
	maximum = 1905
Slowest packet = 11590
Flit latency average = 1324.82
	minimum = 6
	maximum = 4482
Slowest flit = 34337
Fragmentation average = 121.834
	minimum = 0
	maximum = 1263
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0123281
	minimum = 0.0065 (at node 89)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.221065
	minimum = 0.105 (at node 98)
	maximum = 0.327 (at node 129)
Injected packet length average = 18.0096
Accepted packet length average = 17.9318
Total in-flight flits = 139634 (110112 measured)
latency change    = 0.698966
throughput change = 0.00340445
Class 0:
Packet latency average = 1011.78
	minimum = 24
	maximum = 2978
Network latency average = 1006.97
	minimum = 23
	maximum = 2968
Slowest packet = 11560
Flit latency average = 1497.83
	minimum = 6
	maximum = 5366
Slowest flit = 32381
Fragmentation average = 145.369
	minimum = 0
	maximum = 1266
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0122309
	minimum = 0.00733333 (at node 113)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.219903
	minimum = 0.126667 (at node 113)
	maximum = 0.316667 (at node 16)
Injected packet length average = 18.0047
Accepted packet length average = 17.9793
Total in-flight flits = 168527 (150871 measured)
latency change    = 0.370381
throughput change = 0.00528564
Draining remaining packets ...
Class 0:
Remaining flits: 57790 57791 57792 57793 57794 57795 57796 57797 61825 61826 [...] (133089 flits)
Measured flits: 207979 207980 207981 207982 207983 207984 207985 207986 207987 207988 [...] (122504 flits)
Class 0:
Remaining flits: 65826 65827 65828 65829 65830 65831 65832 65833 65834 65835 [...] (98669 flits)
Measured flits: 208116 208117 208118 208119 208120 208121 208122 208123 208124 208125 [...] (91930 flits)
Class 0:
Remaining flits: 76734 76735 76736 76737 76738 76739 76740 76741 76742 76743 [...] (65027 flits)
Measured flits: 208116 208117 208118 208119 208120 208121 208122 208123 208124 208125 [...] (61708 flits)
Class 0:
Remaining flits: 80442 80443 80444 80445 80446 80447 80448 80449 80450 80451 [...] (31974 flits)
Measured flits: 208116 208117 208118 208119 208120 208121 208122 208123 208124 208125 [...] (30705 flits)
Class 0:
Remaining flits: 128142 128143 128144 128145 128146 128147 128148 128149 128150 128151 [...] (5755 flits)
Measured flits: 211230 211231 211232 211233 211234 211235 211236 211237 211238 211239 [...] (5535 flits)
Class 0:
Remaining flits: 293578 293579 362124 362125 362126 362127 362128 362129 362130 362131 [...] (56 flits)
Measured flits: 293578 293579 362124 362125 362126 362127 362128 362129 362130 362131 [...] (56 flits)
Time taken is 12061 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3120.51 (1 samples)
	minimum = 24 (1 samples)
	maximum = 8549 (1 samples)
Network latency average = 3115.56 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8534 (1 samples)
Flit latency average = 2913.24 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9420 (1 samples)
Fragmentation average = 174.712 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2739 (1 samples)
Injected packet rate average = 0.0203299 (1 samples)
	minimum = 0.0136667 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.0122309 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.366033 (1 samples)
	minimum = 0.246 (1 samples)
	maximum = 0.468333 (1 samples)
Accepted flit rate average = 0.219903 (1 samples)
	minimum = 0.126667 (1 samples)
	maximum = 0.316667 (1 samples)
Injected packet size average = 18.0047 (1 samples)
Accepted packet size average = 17.9793 (1 samples)
Hops average = 5.07797 (1 samples)
Total run time 10.9919
