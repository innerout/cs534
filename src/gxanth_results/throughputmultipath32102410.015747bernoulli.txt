BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 224.082
	minimum = 23
	maximum = 816
Network latency average = 221.016
	minimum = 23
	maximum = 801
Slowest packet = 304
Flit latency average = 150.324
	minimum = 6
	maximum = 965
Slowest flit = 1161
Fragmentation average = 130.516
	minimum = 0
	maximum = 749
Injected packet rate average = 0.0158542
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00930208
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.282297
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.18824
	minimum = 0.048 (at node 174)
	maximum = 0.315 (at node 48)
Injected packet length average = 17.8058
Accepted packet length average = 20.2363
Total in-flight flits = 18650 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 356.952
	minimum = 23
	maximum = 1808
Network latency average = 353.736
	minimum = 23
	maximum = 1808
Slowest packet = 445
Flit latency average = 266.202
	minimum = 6
	maximum = 1791
Slowest flit = 8027
Fragmentation average = 169.455
	minimum = 0
	maximum = 1661
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 164)
	maximum = 0.0235 (at node 44)
Accepted packet rate average = 0.010375
	minimum = 0.005 (at node 116)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.280391
	minimum = 0.162 (at node 164)
	maximum = 0.423 (at node 44)
Accepted flit rate average= 0.199396
	minimum = 0.1055 (at node 164)
	maximum = 0.287 (at node 22)
Injected packet length average = 17.9211
Accepted packet length average = 19.2189
Total in-flight flits = 31576 (0 measured)
latency change    = 0.372235
throughput change = 0.0559503
Class 0:
Packet latency average = 658.587
	minimum = 27
	maximum = 2785
Network latency average = 655.428
	minimum = 27
	maximum = 2785
Slowest packet = 107
Flit latency average = 555.753
	minimum = 6
	maximum = 2768
Slowest flit = 1943
Fragmentation average = 225.692
	minimum = 0
	maximum = 2143
Injected packet rate average = 0.0156458
	minimum = 0.005 (at node 67)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.0116615
	minimum = 0.004 (at node 4)
	maximum = 0.023 (at node 47)
Injected flit rate average = 0.281401
	minimum = 0.101 (at node 67)
	maximum = 0.504 (at node 35)
Accepted flit rate average= 0.212917
	minimum = 0.078 (at node 4)
	maximum = 0.376 (at node 119)
Injected packet length average = 17.9857
Accepted packet length average = 18.2582
Total in-flight flits = 44768 (0 measured)
latency change    = 0.458004
throughput change = 0.0635029
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 331.138
	minimum = 25
	maximum = 969
Network latency average = 327.972
	minimum = 25
	maximum = 969
Slowest packet = 9023
Flit latency average = 768.067
	minimum = 6
	maximum = 3680
Slowest flit = 13390
Fragmentation average = 132.446
	minimum = 0
	maximum = 804
Injected packet rate average = 0.016276
	minimum = 0.008 (at node 154)
	maximum = 0.027 (at node 155)
Accepted packet rate average = 0.012026
	minimum = 0.005 (at node 54)
	maximum = 0.022 (at node 3)
Injected flit rate average = 0.292349
	minimum = 0.144 (at node 154)
	maximum = 0.486 (at node 155)
Accepted flit rate average= 0.214667
	minimum = 0.078 (at node 174)
	maximum = 0.41 (at node 152)
Injected packet length average = 17.9619
Accepted packet length average = 17.8502
Total in-flight flits = 59802 (40172 measured)
latency change    = 0.98886
throughput change = 0.00815217
Class 0:
Packet latency average = 582.28
	minimum = 25
	maximum = 1971
Network latency average = 578.849
	minimum = 25
	maximum = 1971
Slowest packet = 9043
Flit latency average = 879.444
	minimum = 6
	maximum = 4344
Slowest flit = 13391
Fragmentation average = 159.602
	minimum = 0
	maximum = 1086
Injected packet rate average = 0.0159089
	minimum = 0.008 (at node 154)
	maximum = 0.023 (at node 150)
Accepted packet rate average = 0.0119167
	minimum = 0.007 (at node 26)
	maximum = 0.022 (at node 0)
Injected flit rate average = 0.286482
	minimum = 0.144 (at node 154)
	maximum = 0.414 (at node 150)
Accepted flit rate average= 0.212815
	minimum = 0.1115 (at node 26)
	maximum = 0.3905 (at node 0)
Injected packet length average = 18.0077
Accepted packet length average = 17.8586
Total in-flight flits = 73009 (63128 measured)
latency change    = 0.431309
throughput change = 0.00870033
Class 0:
Packet latency average = 782.579
	minimum = 24
	maximum = 2970
Network latency average = 779.1
	minimum = 24
	maximum = 2970
Slowest packet = 9060
Flit latency average = 984.133
	minimum = 6
	maximum = 4900
Slowest flit = 60339
Fragmentation average = 171.406
	minimum = 0
	maximum = 1651
Injected packet rate average = 0.0158767
	minimum = 0.00966667 (at node 118)
	maximum = 0.0243333 (at node 3)
Accepted packet rate average = 0.0118403
	minimum = 0.00733333 (at node 54)
	maximum = 0.018 (at node 0)
Injected flit rate average = 0.285889
	minimum = 0.172667 (at node 118)
	maximum = 0.438 (at node 3)
Accepted flit rate average= 0.211641
	minimum = 0.129 (at node 71)
	maximum = 0.317 (at node 0)
Injected packet length average = 18.0068
Accepted packet length average = 17.8746
Total in-flight flits = 87473 (82118 measured)
latency change    = 0.255947
throughput change = 0.0055494
Draining remaining packets ...
Class 0:
Remaining flits: 62748 62749 62750 62751 62752 62753 62754 62755 62756 62757 [...] (52658 flits)
Measured flits: 162270 162271 162272 162273 162274 162275 162276 162277 162278 162279 [...] (50095 flits)
Class 0:
Remaining flits: 84906 84907 84908 84909 84910 84911 84912 84913 84914 84915 [...] (20156 flits)
Measured flits: 162270 162271 162272 162273 162274 162275 162276 162277 162278 162279 [...] (19682 flits)
Class 0:
Remaining flits: 224239 224240 224241 224242 224243 238140 238141 238142 238143 238144 [...] (165 flits)
Measured flits: 224239 224240 224241 224242 224243 238140 238141 238142 238143 238144 [...] (165 flits)
Time taken is 9078 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1655.77 (1 samples)
	minimum = 24 (1 samples)
	maximum = 5600 (1 samples)
Network latency average = 1652.33 (1 samples)
	minimum = 24 (1 samples)
	maximum = 5600 (1 samples)
Flit latency average = 1628.01 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6876 (1 samples)
Fragmentation average = 176.243 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2510 (1 samples)
Injected packet rate average = 0.0158767 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0118403 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.285889 (1 samples)
	minimum = 0.172667 (1 samples)
	maximum = 0.438 (1 samples)
Accepted flit rate average = 0.211641 (1 samples)
	minimum = 0.129 (1 samples)
	maximum = 0.317 (1 samples)
Injected packet size average = 18.0068 (1 samples)
Accepted packet size average = 17.8746 (1 samples)
Hops average = 5.06802 (1 samples)
Total run time 8.14585
