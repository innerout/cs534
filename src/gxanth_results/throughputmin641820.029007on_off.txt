BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 310.272
	minimum = 22
	maximum = 954
Network latency average = 216.928
	minimum = 22
	maximum = 756
Slowest packet = 7
Flit latency average = 181.979
	minimum = 5
	maximum = 739
Slowest flit = 13283
Fragmentation average = 49.3617
	minimum = 0
	maximum = 420
Injected packet rate average = 0.0242656
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0135052
	minimum = 0.006 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.431812
	minimum = 0 (at node 100)
	maximum = 0.995 (at node 27)
Accepted flit rate average= 0.253948
	minimum = 0.108 (at node 116)
	maximum = 0.415 (at node 44)
Injected packet length average = 17.7952
Accepted packet length average = 18.8037
Total in-flight flits = 35140 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 530.537
	minimum = 22
	maximum = 1941
Network latency average = 387.923
	minimum = 22
	maximum = 1396
Slowest packet = 7
Flit latency average = 348.754
	minimum = 5
	maximum = 1453
Slowest flit = 40277
Fragmentation average = 68.6013
	minimum = 0
	maximum = 513
Injected packet rate average = 0.0241693
	minimum = 0.0065 (at node 180)
	maximum = 0.049 (at node 23)
Accepted packet rate average = 0.0142708
	minimum = 0.008 (at node 135)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.432669
	minimum = 0.1095 (at node 180)
	maximum = 0.875 (at node 23)
Accepted flit rate average= 0.264813
	minimum = 0.144 (at node 153)
	maximum = 0.4185 (at node 152)
Injected packet length average = 17.9016
Accepted packet length average = 18.5562
Total in-flight flits = 65604 (0 measured)
latency change    = 0.415173
throughput change = 0.0410275
Class 0:
Packet latency average = 1228.48
	minimum = 29
	maximum = 2833
Network latency average = 967.608
	minimum = 22
	maximum = 2116
Slowest packet = 3808
Flit latency average = 919
	minimum = 5
	maximum = 2127
Slowest flit = 69384
Fragmentation average = 105.096
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0206615
	minimum = 0.001 (at node 175)
	maximum = 0.044 (at node 7)
Accepted packet rate average = 0.0150156
	minimum = 0.005 (at node 96)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.371271
	minimum = 0.018 (at node 175)
	maximum = 0.796 (at node 7)
Accepted flit rate average= 0.271719
	minimum = 0.105 (at node 96)
	maximum = 0.426 (at node 56)
Injected packet length average = 17.9692
Accepted packet length average = 18.0957
Total in-flight flits = 85074 (0 measured)
latency change    = 0.568136
throughput change = 0.0254169
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 823.95
	minimum = 25
	maximum = 2932
Network latency average = 210.994
	minimum = 22
	maximum = 991
Slowest packet = 13325
Flit latency average = 1262.23
	minimum = 5
	maximum = 2598
Slowest flit = 114533
Fragmentation average = 20.9375
	minimum = 0
	maximum = 282
Injected packet rate average = 0.0177708
	minimum = 0 (at node 186)
	maximum = 0.046 (at node 5)
Accepted packet rate average = 0.0148594
	minimum = 0.004 (at node 4)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.320078
	minimum = 0 (at node 186)
	maximum = 0.82 (at node 5)
Accepted flit rate average= 0.267495
	minimum = 0.104 (at node 4)
	maximum = 0.475 (at node 16)
Injected packet length average = 18.0114
Accepted packet length average = 18.0018
Total in-flight flits = 95185 (55355 measured)
latency change    = 0.490965
throughput change = 0.0157908
Class 0:
Packet latency average = 1530.9
	minimum = 25
	maximum = 4317
Network latency average = 795.065
	minimum = 22
	maximum = 1929
Slowest packet = 13325
Flit latency average = 1385.96
	minimum = 5
	maximum = 3441
Slowest flit = 132698
Fragmentation average = 61.6676
	minimum = 0
	maximum = 461
Injected packet rate average = 0.0168776
	minimum = 0.001 (at node 22)
	maximum = 0.0345 (at node 5)
Accepted packet rate average = 0.0149453
	minimum = 0.0085 (at node 113)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.304109
	minimum = 0.0235 (at node 22)
	maximum = 0.6245 (at node 5)
Accepted flit rate average= 0.26937
	minimum = 0.155 (at node 113)
	maximum = 0.4095 (at node 128)
Injected packet length average = 18.0185
Accepted packet length average = 18.0237
Total in-flight flits = 98474 (88981 measured)
latency change    = 0.461786
throughput change = 0.00696069
Class 0:
Packet latency average = 2136.35
	minimum = 25
	maximum = 5546
Network latency average = 1295.2
	minimum = 22
	maximum = 2921
Slowest packet = 13325
Flit latency average = 1501.67
	minimum = 5
	maximum = 4336
Slowest flit = 140122
Fragmentation average = 85.889
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0164965
	minimum = 0.005 (at node 32)
	maximum = 0.0303333 (at node 5)
Accepted packet rate average = 0.0149757
	minimum = 0.01 (at node 79)
	maximum = 0.0233333 (at node 128)
Injected flit rate average = 0.296844
	minimum = 0.0903333 (at node 32)
	maximum = 0.546667 (at node 7)
Accepted flit rate average= 0.26984
	minimum = 0.182333 (at node 64)
	maximum = 0.408667 (at node 128)
Injected packet length average = 17.9943
Accepted packet length average = 18.0185
Total in-flight flits = 100934 (100340 measured)
latency change    = 0.283407
throughput change = 0.00174357
Draining remaining packets ...
Class 0:
Remaining flits: 182814 182815 182816 182817 182818 182819 182820 182821 182822 182823 [...] (53253 flits)
Measured flits: 240624 240625 240626 240627 240628 240629 240630 240631 240632 240633 [...] (53210 flits)
Class 0:
Remaining flits: 236664 236665 236666 236667 236668 236669 236670 236671 236672 236673 [...] (7826 flits)
Measured flits: 291060 291061 291062 291063 291064 291065 291066 291067 291068 291069 [...] (7808 flits)
Time taken is 8632 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2958.43 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7510 (1 samples)
Network latency average = 1853.33 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4605 (1 samples)
Flit latency average = 1756.48 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5084 (1 samples)
Fragmentation average = 98.171 (1 samples)
	minimum = 0 (1 samples)
	maximum = 605 (1 samples)
Injected packet rate average = 0.0164965 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0303333 (1 samples)
Accepted packet rate average = 0.0149757 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0233333 (1 samples)
Injected flit rate average = 0.296844 (1 samples)
	minimum = 0.0903333 (1 samples)
	maximum = 0.546667 (1 samples)
Accepted flit rate average = 0.26984 (1 samples)
	minimum = 0.182333 (1 samples)
	maximum = 0.408667 (1 samples)
Injected packet size average = 17.9943 (1 samples)
Accepted packet size average = 18.0185 (1 samples)
Hops average = 5.1281 (1 samples)
Total run time 12.7473
