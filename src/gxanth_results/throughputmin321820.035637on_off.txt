BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 341.627
	minimum = 22
	maximum = 973
Network latency average = 234.843
	minimum = 22
	maximum = 822
Slowest packet = 90
Flit latency average = 199.651
	minimum = 5
	maximum = 864
Slowest flit = 10776
Fragmentation average = 50.0735
	minimum = 0
	maximum = 316
Injected packet rate average = 0.024599
	minimum = 0 (at node 106)
	maximum = 0.054 (at node 171)
Accepted packet rate average = 0.0138802
	minimum = 0.007 (at node 96)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.437786
	minimum = 0 (at node 106)
	maximum = 0.97 (at node 171)
Accepted flit rate average= 0.259698
	minimum = 0.126 (at node 96)
	maximum = 0.422 (at node 44)
Injected packet length average = 17.797
Accepted packet length average = 18.7099
Total in-flight flits = 35494 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.031
	minimum = 22
	maximum = 1939
Network latency average = 421.876
	minimum = 22
	maximum = 1497
Slowest packet = 90
Flit latency average = 382.596
	minimum = 5
	maximum = 1480
Slowest flit = 41939
Fragmentation average = 56.1402
	minimum = 0
	maximum = 417
Injected packet rate average = 0.0215104
	minimum = 0.001 (at node 169)
	maximum = 0.04 (at node 177)
Accepted packet rate average = 0.0143724
	minimum = 0.008 (at node 164)
	maximum = 0.021 (at node 166)
Injected flit rate average = 0.384383
	minimum = 0.018 (at node 169)
	maximum = 0.7195 (at node 177)
Accepted flit rate average= 0.264203
	minimum = 0.153 (at node 116)
	maximum = 0.378 (at node 166)
Injected packet length average = 17.8696
Accepted packet length average = 18.3827
Total in-flight flits = 47928 (0 measured)
latency change    = 0.433483
throughput change = 0.0170521
Class 0:
Packet latency average = 1408.55
	minimum = 31
	maximum = 2954
Network latency average = 874.79
	minimum = 22
	maximum = 2245
Slowest packet = 4754
Flit latency average = 825.639
	minimum = 5
	maximum = 2228
Slowest flit = 53063
Fragmentation average = 64.8883
	minimum = 0
	maximum = 299
Injected packet rate average = 0.0152135
	minimum = 0.001 (at node 22)
	maximum = 0.032 (at node 79)
Accepted packet rate average = 0.0146406
	minimum = 0.007 (at node 4)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.273625
	minimum = 0.018 (at node 68)
	maximum = 0.567 (at node 79)
Accepted flit rate average= 0.264323
	minimum = 0.104 (at node 163)
	maximum = 0.504 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.0541
Total in-flight flits = 49918 (0 measured)
latency change    = 0.571877
throughput change = 0.000453202
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1663.64
	minimum = 35
	maximum = 3675
Network latency average = 386.169
	minimum = 22
	maximum = 970
Slowest packet = 11236
Flit latency average = 909.416
	minimum = 5
	maximum = 2505
Slowest flit = 119062
Fragmentation average = 35.6185
	minimum = 0
	maximum = 203
Injected packet rate average = 0.0153542
	minimum = 0 (at node 121)
	maximum = 0.03 (at node 158)
Accepted packet rate average = 0.0149115
	minimum = 0.006 (at node 71)
	maximum = 0.026 (at node 83)
Injected flit rate average = 0.276505
	minimum = 0 (at node 121)
	maximum = 0.536 (at node 158)
Accepted flit rate average= 0.268651
	minimum = 0.108 (at node 71)
	maximum = 0.488 (at node 123)
Injected packet length average = 18.0085
Accepted packet length average = 18.0164
Total in-flight flits = 51401 (42241 measured)
latency change    = 0.153337
throughput change = 0.0161106
Class 0:
Packet latency average = 2253.94
	minimum = 26
	maximum = 4697
Network latency average = 771.459
	minimum = 22
	maximum = 1937
Slowest packet = 11236
Flit latency average = 925.25
	minimum = 5
	maximum = 2849
Slowest flit = 140339
Fragmentation average = 58.2603
	minimum = 0
	maximum = 248
Injected packet rate average = 0.0153021
	minimum = 0.0075 (at node 88)
	maximum = 0.023 (at node 85)
Accepted packet rate average = 0.0149583
	minimum = 0.0075 (at node 36)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.275659
	minimum = 0.135 (at node 88)
	maximum = 0.4155 (at node 85)
Accepted flit rate average= 0.269323
	minimum = 0.147 (at node 36)
	maximum = 0.4245 (at node 129)
Injected packet length average = 18.0145
Accepted packet length average = 18.0049
Total in-flight flits = 52428 (52076 measured)
latency change    = 0.261893
throughput change = 0.00249468
Class 0:
Packet latency average = 2660.45
	minimum = 26
	maximum = 5384
Network latency average = 892.477
	minimum = 22
	maximum = 2779
Slowest packet = 11236
Flit latency average = 938.977
	minimum = 5
	maximum = 3455
Slowest flit = 143261
Fragmentation average = 64.7045
	minimum = 0
	maximum = 298
Injected packet rate average = 0.0152882
	minimum = 0.009 (at node 152)
	maximum = 0.0216667 (at node 85)
Accepted packet rate average = 0.0149045
	minimum = 0.01 (at node 36)
	maximum = 0.0213333 (at node 129)
Injected flit rate average = 0.275028
	minimum = 0.161667 (at node 152)
	maximum = 0.392333 (at node 85)
Accepted flit rate average= 0.268872
	minimum = 0.183333 (at node 135)
	maximum = 0.367667 (at node 128)
Injected packet length average = 17.9896
Accepted packet length average = 18.0396
Total in-flight flits = 53448 (53448 measured)
latency change    = 0.152798
throughput change = 0.00167883
Draining remaining packets ...
Class 0:
Remaining flits: 223812 223813 223814 223815 223816 223817 223818 223819 223820 223821 [...] (7383 flits)
Measured flits: 223812 223813 223814 223815 223816 223817 223818 223819 223820 223821 [...] (7383 flits)
Time taken is 7749 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3044.41 (1 samples)
	minimum = 26 (1 samples)
	maximum = 6839 (1 samples)
Network latency average = 1018.58 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3951 (1 samples)
Flit latency average = 1000.41 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3890 (1 samples)
Fragmentation average = 69.4673 (1 samples)
	minimum = 0 (1 samples)
	maximum = 309 (1 samples)
Injected packet rate average = 0.0152882 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0216667 (1 samples)
Accepted packet rate average = 0.0149045 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.275028 (1 samples)
	minimum = 0.161667 (1 samples)
	maximum = 0.392333 (1 samples)
Accepted flit rate average = 0.268872 (1 samples)
	minimum = 0.183333 (1 samples)
	maximum = 0.367667 (1 samples)
Injected packet size average = 17.9896 (1 samples)
Accepted packet size average = 18.0396 (1 samples)
Hops average = 5.1058 (1 samples)
Total run time 11.7255
