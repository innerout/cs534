BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.79
	minimum = 22
	maximum = 908
Network latency average = 308.19
	minimum = 22
	maximum = 846
Slowest packet = 83
Flit latency average = 271.341
	minimum = 5
	maximum = 867
Slowest flit = 18774
Fragmentation average = 88.2512
	minimum = 0
	maximum = 422
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0152188
	minimum = 0.005 (at node 174)
	maximum = 0.026 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.296667
	minimum = 0.125 (at node 174)
	maximum = 0.49 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 19.4935
Total in-flight flits = 116146 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 619.885
	minimum = 22
	maximum = 1830
Network latency average = 569.177
	minimum = 22
	maximum = 1715
Slowest packet = 1141
Flit latency average = 526.827
	minimum = 5
	maximum = 1724
Slowest flit = 42147
Fragmentation average = 114.564
	minimum = 0
	maximum = 439
Injected packet rate average = 0.051625
	minimum = 0.04 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0165286
	minimum = 0.01 (at node 116)
	maximum = 0.023 (at node 78)
Injected flit rate average = 0.925266
	minimum = 0.713 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.310945
	minimum = 0.1985 (at node 116)
	maximum = 0.4275 (at node 103)
Injected packet length average = 17.9228
Accepted packet length average = 18.8125
Total in-flight flits = 237429 (0 measured)
latency change    = 0.453463
throughput change = 0.0459201
Class 0:
Packet latency average = 1458.69
	minimum = 24
	maximum = 2739
Network latency average = 1373.73
	minimum = 22
	maximum = 2648
Slowest packet = 2415
Flit latency average = 1339.71
	minimum = 5
	maximum = 2715
Slowest flit = 43461
Fragmentation average = 146.898
	minimum = 0
	maximum = 491
Injected packet rate average = 0.0538177
	minimum = 0.043 (at node 63)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0176094
	minimum = 0.007 (at node 149)
	maximum = 0.027 (at node 3)
Injected flit rate average = 0.968359
	minimum = 0.78 (at node 63)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.317604
	minimum = 0.114 (at node 149)
	maximum = 0.498 (at node 3)
Injected packet length average = 17.9933
Accepted packet length average = 18.0361
Total in-flight flits = 362443 (0 measured)
latency change    = 0.575041
throughput change = 0.0209659
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 225.639
	minimum = 28
	maximum = 679
Network latency average = 41.2127
	minimum = 22
	maximum = 334
Slowest packet = 30185
Flit latency average = 1871.55
	minimum = 5
	maximum = 3351
Slowest flit = 104901
Fragmentation average = 6.44866
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0543333
	minimum = 0.045 (at node 18)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0179531
	minimum = 0.009 (at node 55)
	maximum = 0.031 (at node 0)
Injected flit rate average = 0.978068
	minimum = 0.808 (at node 18)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.323589
	minimum = 0.158 (at node 55)
	maximum = 0.561 (at node 181)
Injected packet length average = 18.0012
Accepted packet length average = 18.0241
Total in-flight flits = 488090 (172948 measured)
latency change    = 5.4647
throughput change = 0.0184938
Class 0:
Packet latency average = 237.179
	minimum = 24
	maximum = 836
Network latency average = 41.6503
	minimum = 22
	maximum = 405
Slowest packet = 30185
Flit latency average = 2142.47
	minimum = 5
	maximum = 4056
Slowest flit = 151091
Fragmentation average = 6.38852
	minimum = 0
	maximum = 41
Injected packet rate average = 0.054349
	minimum = 0.0465 (at node 18)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0180391
	minimum = 0.0105 (at node 86)
	maximum = 0.026 (at node 0)
Injected flit rate average = 0.978453
	minimum = 0.8385 (at node 18)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.32506
	minimum = 0.195 (at node 86)
	maximum = 0.4565 (at node 0)
Injected packet length average = 18.0032
Accepted packet length average = 18.0198
Total in-flight flits = 613280 (345334 measured)
latency change    = 0.0486526
throughput change = 0.00452641
Class 0:
Packet latency average = 247.029
	minimum = 24
	maximum = 896
Network latency average = 41.385
	minimum = 22
	maximum = 405
Slowest packet = 30185
Flit latency average = 2401.73
	minimum = 5
	maximum = 4908
Slowest flit = 184385
Fragmentation average = 6.54039
	minimum = 0
	maximum = 43
Injected packet rate average = 0.0542639
	minimum = 0.047 (at node 162)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0181823
	minimum = 0.0123333 (at node 144)
	maximum = 0.0256667 (at node 129)
Injected flit rate average = 0.976606
	minimum = 0.846333 (at node 162)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.326995
	minimum = 0.216333 (at node 144)
	maximum = 0.449333 (at node 129)
Injected packet length average = 17.9973
Accepted packet length average = 17.9842
Total in-flight flits = 736702 (515879 measured)
latency change    = 0.039875
throughput change = 0.00591721
Draining remaining packets ...
Class 0:
Remaining flits: 209242 209243 209244 209245 209246 209247 209248 209249 212490 212491 [...] (689228 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (515448 flits)
Class 0:
Remaining flits: 252753 252754 252755 268427 268428 268429 268430 268431 268432 268433 [...] (642167 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (515398 flits)
Class 0:
Remaining flits: 301752 301753 301754 301755 301756 301757 301758 301759 301760 301761 [...] (594712 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (514218 flits)
Class 0:
Remaining flits: 362376 362377 362378 362379 362380 362381 362382 362383 362384 362385 [...] (547958 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (507191 flits)
Class 0:
Remaining flits: 407725 407726 407727 407728 407729 407730 407731 407732 407733 407734 [...] (500895 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (487418 flits)
Class 0:
Remaining flits: 468029 468030 468031 468032 468033 468034 468035 471366 471367 471368 [...] (453542 flits)
Measured flits: 543006 543007 543008 543009 543010 543011 543012 543013 543014 543015 [...] (451151 flits)
Class 0:
Remaining flits: 478106 478107 478108 478109 478110 478111 478112 478113 478114 478115 [...] (406264 flits)
Measured flits: 543144 543145 543146 543147 543148 543149 545094 545095 545096 545097 [...] (406047 flits)
Class 0:
Remaining flits: 563184 563185 563186 563187 563188 563189 563190 563191 563192 563193 [...] (358887 flits)
Measured flits: 563184 563185 563186 563187 563188 563189 563190 563191 563192 563193 [...] (358887 flits)
Class 0:
Remaining flits: 580111 580112 580113 580114 580115 580116 580117 580118 580119 580120 [...] (311577 flits)
Measured flits: 580111 580112 580113 580114 580115 580116 580117 580118 580119 580120 [...] (311577 flits)
Class 0:
Remaining flits: 583452 583453 583454 583455 583456 583457 583458 583459 583460 583461 [...] (264293 flits)
Measured flits: 583452 583453 583454 583455 583456 583457 583458 583459 583460 583461 [...] (264293 flits)
Class 0:
Remaining flits: 689634 689635 689636 689637 689638 689639 689640 689641 689642 689643 [...] (217147 flits)
Measured flits: 689634 689635 689636 689637 689638 689639 689640 689641 689642 689643 [...] (217147 flits)
Class 0:
Remaining flits: 707706 707707 707708 707709 707710 707711 707712 707713 707714 707715 [...] (169548 flits)
Measured flits: 707706 707707 707708 707709 707710 707711 707712 707713 707714 707715 [...] (169548 flits)
Class 0:
Remaining flits: 759357 759358 759359 759360 759361 759362 759363 759364 759365 776340 [...] (122243 flits)
Measured flits: 759357 759358 759359 759360 759361 759362 759363 759364 759365 776340 [...] (122243 flits)
Class 0:
Remaining flits: 776340 776341 776342 776343 776344 776345 776346 776347 776348 776349 [...] (74985 flits)
Measured flits: 776340 776341 776342 776343 776344 776345 776346 776347 776348 776349 [...] (74985 flits)
Class 0:
Remaining flits: 847226 847227 847228 847229 847230 847231 847232 847233 847234 847235 [...] (28029 flits)
Measured flits: 847226 847227 847228 847229 847230 847231 847232 847233 847234 847235 [...] (28029 flits)
Class 0:
Remaining flits: 985482 985483 985484 985485 985486 985487 985488 985489 985490 985491 [...] (1851 flits)
Measured flits: 985482 985483 985484 985485 985486 985487 985488 985489 985490 985491 [...] (1851 flits)
Time taken is 22814 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10923.9 (1 samples)
	minimum = 24 (1 samples)
	maximum = 17836 (1 samples)
Network latency average = 10722.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17137 (1 samples)
Flit latency average = 8407.74 (1 samples)
	minimum = 5 (1 samples)
	maximum = 17120 (1 samples)
Fragmentation average = 164.995 (1 samples)
	minimum = 0 (1 samples)
	maximum = 518 (1 samples)
Injected packet rate average = 0.0542639 (1 samples)
	minimum = 0.047 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0181823 (1 samples)
	minimum = 0.0123333 (1 samples)
	maximum = 0.0256667 (1 samples)
Injected flit rate average = 0.976606 (1 samples)
	minimum = 0.846333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.326995 (1 samples)
	minimum = 0.216333 (1 samples)
	maximum = 0.449333 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 17.9842 (1 samples)
Hops average = 5.06434 (1 samples)
Total run time 23.6399
