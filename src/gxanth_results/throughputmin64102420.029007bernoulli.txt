BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 271.444
	minimum = 22
	maximum = 746
Network latency average = 263.446
	minimum = 22
	maximum = 727
Slowest packet = 952
Flit latency average = 233.25
	minimum = 5
	maximum = 732
Slowest flit = 24240
Fragmentation average = 49.4692
	minimum = 0
	maximum = 535
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0137865
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.261542
	minimum = 0.095 (at node 174)
	maximum = 0.439 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.9709
Total in-flight flits = 49432 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.978
	minimum = 22
	maximum = 1427
Network latency average = 490.172
	minimum = 22
	maximum = 1397
Slowest packet = 2551
Flit latency average = 455.289
	minimum = 5
	maximum = 1470
Slowest flit = 51410
Fragmentation average = 62.9534
	minimum = 0
	maximum = 674
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0147083
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.272073
	minimum = 0.16 (at node 153)
	maximum = 0.402 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 18.4979
Total in-flight flits = 94586 (0 measured)
latency change    = 0.456001
throughput change = 0.0387075
Class 0:
Packet latency average = 1119.06
	minimum = 23
	maximum = 2051
Network latency average = 1110.27
	minimum = 22
	maximum = 2051
Slowest packet = 5037
Flit latency average = 1076.38
	minimum = 5
	maximum = 2054
Slowest flit = 92185
Fragmentation average = 87.1353
	minimum = 0
	maximum = 667
Injected packet rate average = 0.0287708
	minimum = 0.016 (at node 65)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0158229
	minimum = 0.007 (at node 113)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.518208
	minimum = 0.288 (at node 65)
	maximum = 0.818 (at node 69)
Accepted flit rate average= 0.287089
	minimum = 0.119 (at node 113)
	maximum = 0.502 (at node 34)
Injected packet length average = 18.0116
Accepted packet length average = 18.1438
Total in-flight flits = 138897 (0 measured)
latency change    = 0.554109
throughput change = 0.0523031
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.2352
	minimum = 22
	maximum = 134
Network latency average = 35.3055
	minimum = 22
	maximum = 64
Slowest packet = 16634
Flit latency average = 1479.81
	minimum = 5
	maximum = 2806
Slowest flit = 99989
Fragmentation average = 6.74945
	minimum = 0
	maximum = 39
Injected packet rate average = 0.029625
	minimum = 0.013 (at node 119)
	maximum = 0.045 (at node 150)
Accepted packet rate average = 0.0161719
	minimum = 0.007 (at node 17)
	maximum = 0.027 (at node 78)
Injected flit rate average = 0.532229
	minimum = 0.234 (at node 119)
	maximum = 0.815 (at node 150)
Accepted flit rate average= 0.289458
	minimum = 0.139 (at node 162)
	maximum = 0.46 (at node 16)
Injected packet length average = 17.9655
Accepted packet length average = 17.8989
Total in-flight flits = 185705 (94057 measured)
latency change    = 23.7387
throughput change = 0.00818699
Class 0:
Packet latency average = 48.7194
	minimum = 22
	maximum = 2000
Network latency average = 39.2716
	minimum = 22
	maximum = 1978
Slowest packet = 16654
Flit latency average = 1694.18
	minimum = 5
	maximum = 3467
Slowest flit = 148959
Fragmentation average = 6.9147
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0291536
	minimum = 0.0185 (at node 23)
	maximum = 0.0385 (at node 13)
Accepted packet rate average = 0.0158281
	minimum = 0.009 (at node 36)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.524307
	minimum = 0.333 (at node 109)
	maximum = 0.689 (at node 22)
Accepted flit rate average= 0.285846
	minimum = 0.1615 (at node 88)
	maximum = 0.44 (at node 128)
Injected packet length average = 17.9843
Accepted packet length average = 18.0594
Total in-flight flits = 230642 (185334 measured)
latency change    = 0.0715167
throughput change = 0.0126361
Class 0:
Packet latency average = 604.422
	minimum = 22
	maximum = 2987
Network latency average = 595.411
	minimum = 22
	maximum = 2986
Slowest packet = 16584
Flit latency average = 1900.03
	minimum = 5
	maximum = 4211
Slowest flit = 176953
Fragmentation average = 22.8302
	minimum = 0
	maximum = 395
Injected packet rate average = 0.0291406
	minimum = 0.0196667 (at node 115)
	maximum = 0.0373333 (at node 136)
Accepted packet rate average = 0.015908
	minimum = 0.00966667 (at node 88)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.524429
	minimum = 0.354 (at node 115)
	maximum = 0.672 (at node 136)
Accepted flit rate average= 0.286918
	minimum = 0.179 (at node 88)
	maximum = 0.403667 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 18.0361
Total in-flight flits = 275762 (269151 measured)
latency change    = 0.919395
throughput change = 0.00373642
Draining remaining packets ...
Class 0:
Remaining flits: 233600 233601 233602 233603 244348 244349 253538 253539 253540 253541 [...] (228312 flits)
Measured flits: 299878 299879 300905 301503 301504 301505 301506 301507 301508 301509 [...] (227895 flits)
Class 0:
Remaining flits: 290516 290517 290518 290519 292426 292427 309269 309270 309271 309272 [...] (181330 flits)
Measured flits: 309269 309270 309271 309272 309273 309274 309275 310617 310618 310619 [...] (181324 flits)
Class 0:
Remaining flits: 328932 328933 328934 328935 328936 328937 328938 328939 328940 328941 [...] (134138 flits)
Measured flits: 328932 328933 328934 328935 328936 328937 328938 328939 328940 328941 [...] (134138 flits)
Class 0:
Remaining flits: 378363 378364 378365 378366 378367 378368 378369 378370 378371 378372 [...] (86970 flits)
Measured flits: 378363 378364 378365 378366 378367 378368 378369 378370 378371 378372 [...] (86970 flits)
Class 0:
Remaining flits: 412852 412853 412854 412855 412856 412857 412858 412859 412860 412861 [...] (39999 flits)
Measured flits: 412852 412853 412854 412855 412856 412857 412858 412859 412860 412861 [...] (39999 flits)
Class 0:
Remaining flits: 478354 478355 478356 478357 478358 478359 478360 478361 478362 478363 [...] (3371 flits)
Measured flits: 478354 478355 478356 478357 478358 478359 478360 478361 478362 478363 [...] (3371 flits)
Time taken is 12750 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4129.37 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7607 (1 samples)
Network latency average = 4119.81 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7591 (1 samples)
Flit latency average = 3477.75 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7574 (1 samples)
Fragmentation average = 137.398 (1 samples)
	minimum = 0 (1 samples)
	maximum = 820 (1 samples)
Injected packet rate average = 0.0291406 (1 samples)
	minimum = 0.0196667 (1 samples)
	maximum = 0.0373333 (1 samples)
Accepted packet rate average = 0.015908 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0223333 (1 samples)
Injected flit rate average = 0.524429 (1 samples)
	minimum = 0.354 (1 samples)
	maximum = 0.672 (1 samples)
Accepted flit rate average = 0.286918 (1 samples)
	minimum = 0.179 (1 samples)
	maximum = 0.403667 (1 samples)
Injected packet size average = 17.9965 (1 samples)
Accepted packet size average = 18.0361 (1 samples)
Hops average = 5.06661 (1 samples)
Total run time 12.3326
