BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.466
	minimum = 22
	maximum = 880
Network latency average = 283.832
	minimum = 22
	maximum = 833
Slowest packet = 913
Flit latency average = 253.472
	minimum = 5
	maximum = 848
Slowest flit = 15819
Fragmentation average = 69.2042
	minimum = 0
	maximum = 649
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0145104
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.282552
	minimum = 0.145 (at node 135)
	maximum = 0.443 (at node 48)
Injected packet length average = 17.8477
Accepted packet length average = 19.4724
Total in-flight flits = 74576 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 559.996
	minimum = 22
	maximum = 1746
Network latency average = 545.013
	minimum = 22
	maximum = 1688
Slowest packet = 2135
Flit latency average = 502.747
	minimum = 5
	maximum = 1671
Slowest flit = 38447
Fragmentation average = 107.715
	minimum = 0
	maximum = 872
Injected packet rate average = 0.0376797
	minimum = 0.0285 (at node 81)
	maximum = 0.05 (at node 151)
Accepted packet rate average = 0.0153307
	minimum = 0.0095 (at node 135)
	maximum = 0.022 (at node 136)
Injected flit rate average = 0.675784
	minimum = 0.513 (at node 81)
	maximum = 0.897 (at node 151)
Accepted flit rate average= 0.290229
	minimum = 0.1815 (at node 153)
	maximum = 0.416 (at node 136)
Injected packet length average = 17.935
Accepted packet length average = 18.9312
Total in-flight flits = 148994 (0 measured)
latency change    = 0.470592
throughput change = 0.0264518
Class 0:
Packet latency average = 1296.19
	minimum = 23
	maximum = 2487
Network latency average = 1278.66
	minimum = 22
	maximum = 2460
Slowest packet = 3661
Flit latency average = 1237.29
	minimum = 5
	maximum = 2443
Slowest flit = 65915
Fragmentation average = 196.568
	minimum = 0
	maximum = 1027
Injected packet rate average = 0.037625
	minimum = 0.023 (at node 127)
	maximum = 0.05 (at node 150)
Accepted packet rate average = 0.0157917
	minimum = 0.006 (at node 20)
	maximum = 0.027 (at node 159)
Injected flit rate average = 0.67613
	minimum = 0.429 (at node 127)
	maximum = 0.917 (at node 150)
Accepted flit rate average= 0.293667
	minimum = 0.146 (at node 61)
	maximum = 0.503 (at node 159)
Injected packet length average = 17.9702
Accepted packet length average = 18.5963
Total in-flight flits = 222642 (0 measured)
latency change    = 0.567966
throughput change = 0.0117054
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 58.0225
	minimum = 22
	maximum = 379
Network latency average = 35.8256
	minimum = 22
	maximum = 251
Slowest packet = 22187
Flit latency average = 1714.35
	minimum = 5
	maximum = 3283
Slowest flit = 90780
Fragmentation average = 6.24525
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0379271
	minimum = 0.025 (at node 5)
	maximum = 0.053 (at node 65)
Accepted packet rate average = 0.0162656
	minimum = 0.006 (at node 4)
	maximum = 0.029 (at node 70)
Injected flit rate average = 0.681859
	minimum = 0.436 (at node 20)
	maximum = 0.957 (at node 65)
Accepted flit rate average= 0.299333
	minimum = 0.133 (at node 4)
	maximum = 0.489 (at node 70)
Injected packet length average = 17.9782
Accepted packet length average = 18.4028
Total in-flight flits = 296246 (120567 measured)
latency change    = 21.3394
throughput change = 0.018931
Class 0:
Packet latency average = 66.6016
	minimum = 22
	maximum = 830
Network latency average = 36.4948
	minimum = 22
	maximum = 251
Slowest packet = 22187
Flit latency average = 1967.96
	minimum = 5
	maximum = 4047
Slowest flit = 116351
Fragmentation average = 6.24219
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0373516
	minimum = 0.021 (at node 140)
	maximum = 0.0515 (at node 177)
Accepted packet rate average = 0.0162917
	minimum = 0.008 (at node 141)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.672359
	minimum = 0.375 (at node 140)
	maximum = 0.932 (at node 177)
Accepted flit rate average= 0.298932
	minimum = 0.151 (at node 141)
	maximum = 0.427 (at node 66)
Injected packet length average = 18.0008
Accepted packet length average = 18.3488
Total in-flight flits = 366026 (237310 measured)
latency change    = 0.128812
throughput change = 0.00134158
Class 0:
Packet latency average = 74.3266
	minimum = 22
	maximum = 1063
Network latency average = 37.1327
	minimum = 22
	maximum = 251
Slowest packet = 22187
Flit latency average = 2212.26
	minimum = 5
	maximum = 4973
Slowest flit = 124397
Fragmentation average = 6.28621
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0368455
	minimum = 0.0273333 (at node 140)
	maximum = 0.0476667 (at node 161)
Accepted packet rate average = 0.0162674
	minimum = 0.00966667 (at node 36)
	maximum = 0.0226667 (at node 129)
Injected flit rate average = 0.663241
	minimum = 0.487 (at node 140)
	maximum = 0.858333 (at node 161)
Accepted flit rate average= 0.298181
	minimum = 0.176 (at node 36)
	maximum = 0.389333 (at node 177)
Injected packet length average = 18.0006
Accepted packet length average = 18.33
Total in-flight flits = 432904 (350727 measured)
latency change    = 0.103934
throughput change = 0.00252108
Draining remaining packets ...
Class 0:
Remaining flits: 170442 170443 170444 170445 170446 170447 170448 170449 170450 170451 [...] (385427 flits)
Measured flits: 390492 390493 390494 390495 390496 390497 390498 390499 390500 390501 [...] (342812 flits)
Class 0:
Remaining flits: 194130 194131 194132 194133 194134 194135 194136 194137 194138 194139 [...] (338001 flits)
Measured flits: 390492 390493 390494 390495 390496 390497 390498 390499 390500 390501 [...] (318856 flits)
Class 0:
Remaining flits: 243335 243336 243337 243338 243339 243340 243341 254788 254789 259251 [...] (290661 flits)
Measured flits: 390503 390504 390505 390506 390507 390508 390509 390510 390511 390512 [...] (283750 flits)
Class 0:
Remaining flits: 280129 280130 280131 280132 280133 299898 299899 299900 299901 299902 [...] (243494 flits)
Measured flits: 390528 390529 390530 390531 390532 390533 390534 390535 390536 390537 [...] (241945 flits)
Class 0:
Remaining flits: 336165 336166 336167 343961 367690 367691 367692 367693 367694 367695 [...] (196419 flits)
Measured flits: 390542 390543 390544 390545 390654 390655 390656 390657 390658 390659 [...] (196241 flits)
Class 0:
Remaining flits: 379008 379009 379010 379011 379012 379013 379014 379015 379016 379017 [...] (149536 flits)
Measured flits: 390663 390664 390665 390666 390667 390668 390669 390670 390671 399486 [...] (149509 flits)
Class 0:
Remaining flits: 379019 379020 379021 379022 379023 379024 379025 425664 425665 425666 [...] (102742 flits)
Measured flits: 425664 425665 425666 425667 425668 425669 425670 425671 425672 425673 [...] (102735 flits)
Class 0:
Remaining flits: 425664 425665 425666 425667 425668 425669 425670 425671 425672 425673 [...] (55805 flits)
Measured flits: 425664 425665 425666 425667 425668 425669 425670 425671 425672 425673 [...] (55805 flits)
Class 0:
Remaining flits: 525852 525853 525854 525855 525856 525857 525858 525859 525860 525861 [...] (12846 flits)
Measured flits: 525852 525853 525854 525855 525856 525857 525858 525859 525860 525861 [...] (12846 flits)
Class 0:
Remaining flits: 701676 701677 701678 701679 701680 701681 701682 701683 701684 701685 [...] (119 flits)
Measured flits: 701676 701677 701678 701679 701680 701681 701682 701683 701684 701685 [...] (119 flits)
Time taken is 16119 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6538.79 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11419 (1 samples)
Network latency average = 6500.04 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11419 (1 samples)
Flit latency average = 5260.24 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11402 (1 samples)
Fragmentation average = 331.281 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1732 (1 samples)
Injected packet rate average = 0.0368455 (1 samples)
	minimum = 0.0273333 (1 samples)
	maximum = 0.0476667 (1 samples)
Accepted packet rate average = 0.0162674 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.663241 (1 samples)
	minimum = 0.487 (1 samples)
	maximum = 0.858333 (1 samples)
Accepted flit rate average = 0.298181 (1 samples)
	minimum = 0.176 (1 samples)
	maximum = 0.389333 (1 samples)
Injected packet size average = 18.0006 (1 samples)
Accepted packet size average = 18.33 (1 samples)
Hops average = 5.09848 (1 samples)
Total run time 20.2327
