BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.791
	minimum = 22
	maximum = 908
Network latency average = 308.191
	minimum = 22
	maximum = 846
Slowest packet = 83
Flit latency average = 271.353
	minimum = 5
	maximum = 867
Slowest flit = 18774
Fragmentation average = 88.245
	minimum = 0
	maximum = 422
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0152188
	minimum = 0.005 (at node 174)
	maximum = 0.026 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.296656
	minimum = 0.125 (at node 174)
	maximum = 0.49 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 19.4928
Total in-flight flits = 116148 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 634.17
	minimum = 22
	maximum = 1830
Network latency average = 584.657
	minimum = 22
	maximum = 1715
Slowest packet = 1141
Flit latency average = 541.22
	minimum = 5
	maximum = 1724
Slowest flit = 42147
Fragmentation average = 117.53
	minimum = 0
	maximum = 442
Injected packet rate average = 0.0499115
	minimum = 0.04 (at node 56)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0160859
	minimum = 0.008 (at node 116)
	maximum = 0.0245 (at node 78)
Injected flit rate average = 0.894557
	minimum = 0.712 (at node 116)
	maximum = 0.997 (at node 91)
Accepted flit rate average= 0.302885
	minimum = 0.1625 (at node 116)
	maximum = 0.4485 (at node 78)
Injected packet length average = 17.9229
Accepted packet length average = 18.8292
Total in-flight flits = 228680 (0 measured)
latency change    = 0.465772
throughput change = 0.0205661
Class 0:
Packet latency average = 1626.54
	minimum = 26
	maximum = 2788
Network latency average = 1534.27
	minimum = 22
	maximum = 2664
Slowest packet = 2246
Flit latency average = 1500.82
	minimum = 5
	maximum = 2718
Slowest flit = 43460
Fragmentation average = 162.122
	minimum = 0
	maximum = 425
Injected packet rate average = 0.0461458
	minimum = 0.002 (at node 116)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0158958
	minimum = 0.008 (at node 54)
	maximum = 0.027 (at node 14)
Injected flit rate average = 0.830677
	minimum = 0.05 (at node 116)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.285839
	minimum = 0.142 (at node 149)
	maximum = 0.491 (at node 3)
Injected packet length average = 18.0011
Accepted packet length average = 17.982
Total in-flight flits = 333477 (0 measured)
latency change    = 0.610112
throughput change = 0.0596381
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 664.884
	minimum = 36
	maximum = 2023
Network latency average = 80.3256
	minimum = 23
	maximum = 735
Slowest packet = 28053
Flit latency average = 2248.55
	minimum = 5
	maximum = 3392
Slowest flit = 96063
Fragmentation average = 5.70349
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0328333
	minimum = 0.004 (at node 136)
	maximum = 0.056 (at node 81)
Accepted packet rate average = 0.0152604
	minimum = 0.007 (at node 48)
	maximum = 0.026 (at node 69)
Injected flit rate average = 0.592089
	minimum = 0.072 (at node 136)
	maximum = 1 (at node 81)
Accepted flit rate average= 0.274083
	minimum = 0.134 (at node 22)
	maximum = 0.51 (at node 90)
Injected packet length average = 18.0332
Accepted packet length average = 17.9604
Total in-flight flits = 394577 (110628 measured)
latency change    = 1.44636
throughput change = 0.0428892
Class 0:
Packet latency average = 1005.3
	minimum = 28
	maximum = 2928
Network latency average = 252.1
	minimum = 22
	maximum = 1969
Slowest packet = 28053
Flit latency average = 2619.74
	minimum = 5
	maximum = 4117
Slowest flit = 146893
Fragmentation average = 6.17363
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0245469
	minimum = 0.0025 (at node 136)
	maximum = 0.0445 (at node 121)
Accepted packet rate average = 0.0151484
	minimum = 0.0085 (at node 36)
	maximum = 0.024 (at node 70)
Injected flit rate average = 0.442622
	minimum = 0.0445 (at node 136)
	maximum = 0.8 (at node 121)
Accepted flit rate average= 0.270839
	minimum = 0.1565 (at node 36)
	maximum = 0.42 (at node 90)
Injected packet length average = 18.0317
Accepted packet length average = 17.879
Total in-flight flits = 399575 (164464 measured)
latency change    = 0.338621
throughput change = 0.0119805
Class 0:
Packet latency average = 1505.78
	minimum = 28
	maximum = 3641
Network latency average = 586.649
	minimum = 22
	maximum = 2803
Slowest packet = 28053
Flit latency average = 2958.22
	minimum = 5
	maximum = 4978
Slowest flit = 158309
Fragmentation average = 6.78486
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0218976
	minimum = 0.00733333 (at node 24)
	maximum = 0.037 (at node 65)
Accepted packet rate average = 0.0151563
	minimum = 0.00933333 (at node 5)
	maximum = 0.0226667 (at node 165)
Injected flit rate average = 0.394688
	minimum = 0.132667 (at node 24)
	maximum = 0.660333 (at node 65)
Accepted flit rate average= 0.269799
	minimum = 0.163667 (at node 5)
	maximum = 0.388333 (at node 165)
Injected packet length average = 18.0243
Accepted packet length average = 17.8011
Total in-flight flits = 405845 (218671 measured)
latency change    = 0.332374
throughput change = 0.00385447
Class 0:
Packet latency average = 2226.33
	minimum = 28
	maximum = 5569
Network latency average = 1108.5
	minimum = 22
	maximum = 3960
Slowest packet = 28053
Flit latency average = 3287.86
	minimum = 5
	maximum = 5844
Slowest flit = 143135
Fragmentation average = 13.4975
	minimum = 0
	maximum = 451
Injected packet rate average = 0.020181
	minimum = 0.00925 (at node 24)
	maximum = 0.03175 (at node 65)
Accepted packet rate average = 0.0151068
	minimum = 0.01025 (at node 42)
	maximum = 0.0205 (at node 165)
Injected flit rate average = 0.363435
	minimum = 0.16275 (at node 24)
	maximum = 0.57125 (at node 65)
Accepted flit rate average= 0.269702
	minimum = 0.1855 (at node 42)
	maximum = 0.36275 (at node 165)
Injected packet length average = 18.0088
Accepted packet length average = 17.853
Total in-flight flits = 405976 (265243 measured)
latency change    = 0.32365
throughput change = 0.000358871
Class 0:
Packet latency average = 3179.32
	minimum = 28
	maximum = 6171
Network latency average = 1872.32
	minimum = 22
	maximum = 4924
Slowest packet = 28053
Flit latency average = 3600.45
	minimum = 5
	maximum = 6650
Slowest flit = 225194
Fragmentation average = 27.0236
	minimum = 0
	maximum = 596
Injected packet rate average = 0.0192792
	minimum = 0.01 (at node 72)
	maximum = 0.0278 (at node 150)
Accepted packet rate average = 0.0151021
	minimum = 0.0116 (at node 139)
	maximum = 0.02 (at node 70)
Injected flit rate average = 0.34742
	minimum = 0.1784 (at node 72)
	maximum = 0.4996 (at node 150)
Accepted flit rate average= 0.269815
	minimum = 0.209 (at node 139)
	maximum = 0.3582 (at node 70)
Injected packet length average = 18.0205
Accepted packet length average = 17.8661
Total in-flight flits = 408247 (309543 measured)
latency change    = 0.299746
throughput change = 0.000417918
Class 0:
Packet latency average = 4152.05
	minimum = 28
	maximum = 7208
Network latency average = 2716.13
	minimum = 22
	maximum = 5989
Slowest packet = 28053
Flit latency average = 3903.48
	minimum = 5
	maximum = 7433
Slowest flit = 194273
Fragmentation average = 40.4221
	minimum = 0
	maximum = 596
Injected packet rate average = 0.0186432
	minimum = 0.0106667 (at node 56)
	maximum = 0.026 (at node 150)
Accepted packet rate average = 0.0150816
	minimum = 0.0116667 (at node 42)
	maximum = 0.0193333 (at node 103)
Injected flit rate average = 0.33597
	minimum = 0.190833 (at node 56)
	maximum = 0.466167 (at node 150)
Accepted flit rate average= 0.269396
	minimum = 0.206833 (at node 42)
	maximum = 0.345167 (at node 103)
Injected packet length average = 18.021
Accepted packet length average = 17.8626
Total in-flight flits = 410727 (347302 measured)
latency change    = 0.234276
throughput change = 0.0015544
Class 0:
Packet latency average = 5041.47
	minimum = 28
	maximum = 8116
Network latency average = 3553.59
	minimum = 22
	maximum = 6967
Slowest packet = 28053
Flit latency average = 4192.42
	minimum = 5
	maximum = 8243
Slowest flit = 303095
Fragmentation average = 50.053
	minimum = 0
	maximum = 596
Injected packet rate average = 0.0180007
	minimum = 0.0114286 (at node 40)
	maximum = 0.0242857 (at node 162)
Accepted packet rate average = 0.0150573
	minimum = 0.0115714 (at node 42)
	maximum = 0.0194286 (at node 138)
Injected flit rate average = 0.324358
	minimum = 0.205714 (at node 40)
	maximum = 0.438 (at node 162)
Accepted flit rate average= 0.269219
	minimum = 0.205571 (at node 42)
	maximum = 0.349143 (at node 138)
Injected packet length average = 18.0191
Accepted packet length average = 17.8797
Total in-flight flits = 408182 (371475 measured)
latency change    = 0.176422
throughput change = 0.000655002
Draining all recorded packets ...
Class 0:
Remaining flits: 307674 307675 307676 307677 307678 307679 307680 307681 307682 307683 [...] (407657 flits)
Measured flits: 504684 504685 504686 504687 504688 504689 504690 504691 504692 504693 [...] (388840 flits)
Class 0:
Remaining flits: 329301 329302 329303 329304 329305 329306 329307 329308 329309 341811 [...] (408586 flits)
Measured flits: 504684 504685 504686 504687 504688 504689 504690 504691 504692 504693 [...] (399686 flits)
Class 0:
Remaining flits: 366907 366908 366909 366910 366911 384462 384463 384464 384465 384466 [...] (409663 flits)
Measured flits: 504900 504901 504902 504903 504904 504905 504906 504907 504908 504909 [...] (405773 flits)
Class 0:
Remaining flits: 414507 414508 414509 414510 414511 414512 414513 414514 414515 414516 [...] (409054 flits)
Measured flits: 504900 504901 504902 504903 504904 504905 504906 504907 504908 504909 [...] (407597 flits)
Class 0:
Remaining flits: 415769 415770 415771 415772 415773 415774 415775 415776 415777 415778 [...] (414609 flits)
Measured flits: 505278 505279 505280 505281 505282 505283 505284 505285 505286 505287 [...] (414035 flits)
Class 0:
Remaining flits: 469170 469171 469172 469173 469174 469175 469176 469177 469178 469179 [...] (417444 flits)
Measured flits: 513270 513271 513272 513273 513274 513275 513276 513277 513278 513279 [...] (417246 flits)
Class 0:
Remaining flits: 493814 493815 493816 493817 493818 493819 493820 493821 493822 493823 [...] (416767 flits)
Measured flits: 519462 519463 519464 519465 519466 519467 519468 519469 519470 519471 [...] (416715 flits)
Class 0:
Remaining flits: 587484 587485 587486 587487 587488 587489 587490 587491 587492 587493 [...] (415914 flits)
Measured flits: 587484 587485 587486 587487 587488 587489 587490 587491 587492 587493 [...] (415914 flits)
Class 0:
Remaining flits: 587484 587485 587486 587487 587488 587489 587490 587491 587492 587493 [...] (411972 flits)
Measured flits: 587484 587485 587486 587487 587488 587489 587490 587491 587492 587493 [...] (411972 flits)
Class 0:
Remaining flits: 615240 615241 615242 615243 615244 615245 615246 615247 615248 615249 [...] (415842 flits)
Measured flits: 615240 615241 615242 615243 615244 615245 615246 615247 615248 615249 [...] (415842 flits)
Class 0:
Remaining flits: 684720 684721 684722 684723 684724 684725 684726 684727 684728 684729 [...] (417104 flits)
Measured flits: 684720 684721 684722 684723 684724 684725 684726 684727 684728 684729 [...] (416888 flits)
Class 0:
Remaining flits: 760194 760195 760196 760197 760198 760199 760200 760201 760202 760203 [...] (419177 flits)
Measured flits: 760194 760195 760196 760197 760198 760199 760200 760201 760202 760203 [...] (418655 flits)
Class 0:
Remaining flits: 767574 767575 767576 767577 767578 767579 767580 767581 767582 767583 [...] (417634 flits)
Measured flits: 767574 767575 767576 767577 767578 767579 767580 767581 767582 767583 [...] (415816 flits)
Class 0:
Remaining flits: 767574 767575 767576 767577 767578 767579 767580 767581 767582 767583 [...] (421122 flits)
Measured flits: 767574 767575 767576 767577 767578 767579 767580 767581 767582 767583 [...] (417342 flits)
Class 0:
Remaining flits: 767586 767587 767588 767589 767590 767591 862416 862417 862418 862419 [...] (421383 flits)
Measured flits: 767586 767587 767588 767589 767590 767591 862416 862417 862418 862419 [...] (413319 flits)
Class 0:
Remaining flits: 862416 862417 862418 862419 862420 862421 862422 862423 862424 862425 [...] (420964 flits)
Measured flits: 862416 862417 862418 862419 862420 862421 862422 862423 862424 862425 [...] (403378 flits)
Class 0:
Remaining flits: 862416 862417 862418 862419 862420 862421 862422 862423 862424 862425 [...] (417674 flits)
Measured flits: 862416 862417 862418 862419 862420 862421 862422 862423 862424 862425 [...] (385778 flits)
Class 0:
Remaining flits: 929078 929079 929080 929081 929082 929083 929084 929085 929086 929087 [...] (419556 flits)
Measured flits: 929078 929079 929080 929081 929082 929083 929084 929085 929086 929087 [...] (364666 flits)
Class 0:
Remaining flits: 1012734 1012735 1012736 1012737 1012738 1012739 1012740 1012741 1012742 1012743 [...] (420835 flits)
Measured flits: 1012734 1012735 1012736 1012737 1012738 1012739 1012740 1012741 1012742 1012743 [...] (338745 flits)
Class 0:
Remaining flits: 1112472 1112473 1112474 1112475 1112476 1112477 1112478 1112479 1112480 1112481 [...] (422098 flits)
Measured flits: 1112472 1112473 1112474 1112475 1112476 1112477 1112478 1112479 1112480 1112481 [...] (307875 flits)
Class 0:
Remaining flits: 1153224 1153225 1153226 1153227 1153228 1153229 1153230 1153231 1153232 1153233 [...] (425205 flits)
Measured flits: 1153224 1153225 1153226 1153227 1153228 1153229 1153230 1153231 1153232 1153233 [...] (276269 flits)
Class 0:
Remaining flits: 1166994 1166995 1166996 1166997 1166998 1166999 1167000 1167001 1167002 1167003 [...] (425482 flits)
Measured flits: 1166994 1166995 1166996 1166997 1166998 1166999 1167000 1167001 1167002 1167003 [...] (241443 flits)
Class 0:
Remaining flits: 1247094 1247095 1247096 1247097 1247098 1247099 1247100 1247101 1247102 1247103 [...] (425717 flits)
Measured flits: 1247094 1247095 1247096 1247097 1247098 1247099 1247100 1247101 1247102 1247103 [...] (202903 flits)
Class 0:
Remaining flits: 1344258 1344259 1344260 1344261 1344262 1344263 1344264 1344265 1344266 1344267 [...] (426417 flits)
Measured flits: 1344258 1344259 1344260 1344261 1344262 1344263 1344264 1344265 1344266 1344267 [...] (165903 flits)
Class 0:
Remaining flits: 1359450 1359451 1359452 1359453 1359454 1359455 1359456 1359457 1359458 1359459 [...] (425664 flits)
Measured flits: 1359450 1359451 1359452 1359453 1359454 1359455 1359456 1359457 1359458 1359459 [...] (129592 flits)
Class 0:
Remaining flits: 1423494 1423495 1423496 1423497 1423498 1423499 1423500 1423501 1423502 1423503 [...] (423891 flits)
Measured flits: 1423494 1423495 1423496 1423497 1423498 1423499 1423500 1423501 1423502 1423503 [...] (98090 flits)
Class 0:
Remaining flits: 1498572 1498573 1498574 1498575 1498576 1498577 1498578 1498579 1498580 1498581 [...] (424117 flits)
Measured flits: 1498572 1498573 1498574 1498575 1498576 1498577 1498578 1498579 1498580 1498581 [...] (70262 flits)
Class 0:
Remaining flits: 1544688 1544689 1544690 1544691 1544692 1544693 1544694 1544695 1544696 1544697 [...] (425223 flits)
Measured flits: 1544688 1544689 1544690 1544691 1544692 1544693 1544694 1544695 1544696 1544697 [...] (48302 flits)
Class 0:
Remaining flits: 1547532 1547533 1547534 1547535 1547536 1547537 1547538 1547539 1547540 1547541 [...] (422625 flits)
Measured flits: 1547532 1547533 1547534 1547535 1547536 1547537 1547538 1547539 1547540 1547541 [...] (30750 flits)
Class 0:
Remaining flits: 1607310 1607311 1607312 1607313 1607314 1607315 1607316 1607317 1607318 1607319 [...] (424661 flits)
Measured flits: 1607310 1607311 1607312 1607313 1607314 1607315 1607316 1607317 1607318 1607319 [...] (18610 flits)
Class 0:
Remaining flits: 1694196 1694197 1694198 1694199 1694200 1694201 1694202 1694203 1694204 1694205 [...] (425640 flits)
Measured flits: 1694196 1694197 1694198 1694199 1694200 1694201 1694202 1694203 1694204 1694205 [...] (10138 flits)
Class 0:
Remaining flits: 1694196 1694197 1694198 1694199 1694200 1694201 1694202 1694203 1694204 1694205 [...] (425102 flits)
Measured flits: 1694196 1694197 1694198 1694199 1694200 1694201 1694202 1694203 1694204 1694205 [...] (5046 flits)
Class 0:
Remaining flits: 1809576 1809577 1809578 1809579 1809580 1809581 1809582 1809583 1809584 1809585 [...] (426825 flits)
Measured flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (2629 flits)
Class 0:
Remaining flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (426017 flits)
Measured flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (1234 flits)
Class 0:
Remaining flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (427280 flits)
Measured flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (631 flits)
Class 0:
Remaining flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (427950 flits)
Measured flits: 1812060 1812061 1812062 1812063 1812064 1812065 1812066 1812067 1812068 1812069 [...] (216 flits)
Class 0:
Remaining flits: 2014380 2014381 2014382 2014383 2014384 2014385 2014386 2014387 2014388 2014389 [...] (427566 flits)
Measured flits: 2309742 2309743 2309744 2309745 2309746 2309747 2309748 2309749 2309750 2309751 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2017872 2017873 2017874 2017875 2017876 2017877 2017878 2017879 2017880 2017881 [...] (376329 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2030922 2030923 2030924 2030925 2030926 2030927 2030928 2030929 2030930 2030931 [...] (326036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2143548 2143549 2143550 2143551 2143552 2143553 2143554 2143555 2143556 2143557 [...] (276273 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2143548 2143549 2143550 2143551 2143552 2143553 2143554 2143555 2143556 2143557 [...] (227001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2143548 2143549 2143550 2143551 2143552 2143553 2143554 2143555 2143556 2143557 [...] (178568 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2239398 2239399 2239400 2239401 2239402 2239403 2239404 2239405 2239406 2239407 [...] (130926 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2239398 2239399 2239400 2239401 2239402 2239403 2239404 2239405 2239406 2239407 [...] (83002 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2404962 2404963 2404964 2404965 2404966 2404967 2404968 2404969 2404970 2404971 [...] (35617 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2489364 2489365 2489366 2489367 2489368 2489369 2489370 2489371 2489372 2489373 [...] (6619 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2645100 2645101 2645102 2645103 2645104 2645105 2645106 2645107 2645108 2645109 [...] (436 flits)
Measured flits: (0 flits)
Time taken is 57546 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 17038 (1 samples)
	minimum = 28 (1 samples)
	maximum = 37429 (1 samples)
Network latency average = 7805.42 (1 samples)
	minimum = 22 (1 samples)
	maximum = 19715 (1 samples)
Flit latency average = 7564.24 (1 samples)
	minimum = 5 (1 samples)
	maximum = 20044 (1 samples)
Fragmentation average = 49.1245 (1 samples)
	minimum = 0 (1 samples)
	maximum = 803 (1 samples)
Injected packet rate average = 0.0180007 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0242857 (1 samples)
Accepted packet rate average = 0.0150573 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0194286 (1 samples)
Injected flit rate average = 0.324358 (1 samples)
	minimum = 0.205714 (1 samples)
	maximum = 0.438 (1 samples)
Accepted flit rate average = 0.269219 (1 samples)
	minimum = 0.205571 (1 samples)
	maximum = 0.349143 (1 samples)
Injected packet size average = 18.0191 (1 samples)
Accepted packet size average = 17.8797 (1 samples)
Hops average = 5.0528 (1 samples)
Total run time 84.0443
