BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 345.793
	minimum = 27
	maximum = 941
Network latency average = 330.501
	minimum = 23
	maximum = 919
Slowest packet = 501
Flit latency average = 298.757
	minimum = 6
	maximum = 916
Slowest flit = 8685
Fragmentation average = 57.1658
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0367396
	minimum = 0.02 (at node 112)
	maximum = 0.051 (at node 51)
Accepted packet rate average = 0.0100833
	minimum = 0.005 (at node 4)
	maximum = 0.018 (at node 114)
Injected flit rate average = 0.654854
	minimum = 0.36 (at node 112)
	maximum = 0.916 (at node 77)
Accepted flit rate average= 0.18937
	minimum = 0.09 (at node 30)
	maximum = 0.34 (at node 114)
Injected packet length average = 17.8242
Accepted packet length average = 18.7805
Total in-flight flits = 90739 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 702.338
	minimum = 27
	maximum = 1849
Network latency average = 662.031
	minimum = 23
	maximum = 1826
Slowest packet = 572
Flit latency average = 627.86
	minimum = 6
	maximum = 1913
Slowest flit = 7970
Fragmentation average = 63.9031
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0283646
	minimum = 0.016 (at node 60)
	maximum = 0.0375 (at node 110)
Accepted packet rate average = 0.0100286
	minimum = 0.0055 (at node 96)
	maximum = 0.0145 (at node 177)
Injected flit rate average = 0.507971
	minimum = 0.288 (at node 60)
	maximum = 0.6705 (at node 110)
Accepted flit rate average= 0.184857
	minimum = 0.099 (at node 96)
	maximum = 0.264 (at node 177)
Injected packet length average = 17.9086
Accepted packet length average = 18.4329
Total in-flight flits = 126223 (0 measured)
latency change    = 0.507654
throughput change = 0.0244136
Class 0:
Packet latency average = 1843.33
	minimum = 309
	maximum = 2761
Network latency average = 1687.13
	minimum = 23
	maximum = 2700
Slowest packet = 1578
Flit latency average = 1656.04
	minimum = 6
	maximum = 2683
Slowest flit = 28799
Fragmentation average = 69.0695
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0109115
	minimum = 0 (at node 32)
	maximum = 0.036 (at node 115)
Accepted packet rate average = 0.00967187
	minimum = 0.003 (at node 9)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.198599
	minimum = 0 (at node 32)
	maximum = 0.646 (at node 115)
Accepted flit rate average= 0.172724
	minimum = 0.046 (at node 9)
	maximum = 0.336 (at node 152)
Injected packet length average = 18.201
Accepted packet length average = 17.8584
Total in-flight flits = 131976 (0 measured)
latency change    = 0.618985
throughput change = 0.0702439
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2130.07
	minimum = 728
	maximum = 3018
Network latency average = 197.483
	minimum = 31
	maximum = 808
Slowest packet = 13219
Flit latency average = 2312.28
	minimum = 6
	maximum = 3636
Slowest flit = 48637
Fragmentation average = 32.8667
	minimum = 1
	maximum = 130
Injected packet rate average = 0.00804167
	minimum = 0 (at node 3)
	maximum = 0.039 (at node 1)
Accepted packet rate average = 0.00892708
	minimum = 0.003 (at node 91)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.145016
	minimum = 0 (at node 3)
	maximum = 0.692 (at node 1)
Accepted flit rate average= 0.161245
	minimum = 0.057 (at node 91)
	maximum = 0.297 (at node 112)
Injected packet length average = 18.033
Accepted packet length average = 18.0624
Total in-flight flits = 128629 (26490 measured)
latency change    = 0.134613
throughput change = 0.0711909
Class 0:
Packet latency average = 2707.9
	minimum = 728
	maximum = 4113
Network latency average = 557.977
	minimum = 27
	maximum = 1938
Slowest packet = 13219
Flit latency average = 2609.1
	minimum = 6
	maximum = 4764
Slowest flit = 30124
Fragmentation average = 44.6326
	minimum = 0
	maximum = 136
Injected packet rate average = 0.00794531
	minimum = 0.0005 (at node 5)
	maximum = 0.026 (at node 1)
Accepted packet rate average = 0.00901042
	minimum = 0.0045 (at node 104)
	maximum = 0.016 (at node 78)
Injected flit rate average = 0.143211
	minimum = 0.0075 (at node 8)
	maximum = 0.463 (at node 1)
Accepted flit rate average= 0.161997
	minimum = 0.081 (at node 104)
	maximum = 0.286 (at node 78)
Injected packet length average = 18.0246
Accepted packet length average = 17.9789
Total in-flight flits = 124687 (50902 measured)
latency change    = 0.213387
throughput change = 0.00464578
Class 0:
Packet latency average = 3446.65
	minimum = 728
	maximum = 5013
Network latency average = 1062.19
	minimum = 23
	maximum = 2948
Slowest packet = 13219
Flit latency average = 2861.82
	minimum = 6
	maximum = 5540
Slowest flit = 59359
Fragmentation average = 53.3081
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00817535
	minimum = 0.000666667 (at node 191)
	maximum = 0.0203333 (at node 1)
Accepted packet rate average = 0.00900694
	minimum = 0.00533333 (at node 132)
	maximum = 0.0136667 (at node 16)
Injected flit rate average = 0.147038
	minimum = 0.012 (at node 191)
	maximum = 0.364 (at node 112)
Accepted flit rate average= 0.162241
	minimum = 0.098 (at node 132)
	maximum = 0.244667 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.0129
Total in-flight flits = 123017 (74144 measured)
latency change    = 0.214338
throughput change = 0.00150346
Class 0:
Packet latency average = 4144.9
	minimum = 728
	maximum = 5995
Network latency average = 1508.24
	minimum = 23
	maximum = 3892
Slowest packet = 13219
Flit latency average = 3031.2
	minimum = 6
	maximum = 6337
Slowest flit = 84453
Fragmentation average = 60.3535
	minimum = 0
	maximum = 170
Injected packet rate average = 0.00833854
	minimum = 0.0015 (at node 191)
	maximum = 0.01875 (at node 112)
Accepted packet rate average = 0.00895573
	minimum = 0.00625 (at node 35)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.150253
	minimum = 0.027 (at node 191)
	maximum = 0.3375 (at node 112)
Accepted flit rate average= 0.161376
	minimum = 0.113 (at node 35)
	maximum = 0.23075 (at node 16)
Injected packet length average = 18.0191
Accepted packet length average = 18.0193
Total in-flight flits = 123401 (93253 measured)
latency change    = 0.168461
throughput change = 0.00536025
Draining remaining packets ...
Class 0:
Remaining flits: 70596 70597 70598 70599 70600 70601 70602 70603 70604 70605 [...] (94062 flits)
Measured flits: 236214 236215 236216 236217 236218 236219 236220 236221 236222 236223 [...] (77669 flits)
Class 0:
Remaining flits: 107946 107947 107948 107949 107950 107951 107952 107953 107954 107955 [...] (64968 flits)
Measured flits: 236230 236231 236250 236251 236252 236253 236254 236255 236256 236257 [...] (57198 flits)
Class 0:
Remaining flits: 126744 126745 126746 126747 126748 126749 126750 126751 126752 126753 [...] (36430 flits)
Measured flits: 236484 236485 236486 236487 236488 236489 236490 236491 236492 236493 [...] (33776 flits)
Class 0:
Remaining flits: 129870 129871 129872 129873 129874 129875 129876 129877 129878 129879 [...] (11481 flits)
Measured flits: 239292 239293 239294 239295 239296 239297 239298 239299 239300 239301 [...] (11018 flits)
Class 0:
Remaining flits: 346471 346472 346473 346474 346475 346476 346477 346478 346479 346480 [...] (11 flits)
Measured flits: 346471 346472 346473 346474 346475 346476 346477 346478 346479 346480 [...] (11 flits)
Time taken is 12013 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6692.96 (1 samples)
	minimum = 728 (1 samples)
	maximum = 10247 (1 samples)
Network latency average = 3649.57 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8431 (1 samples)
Flit latency average = 3816.73 (1 samples)
	minimum = 6 (1 samples)
	maximum = 10593 (1 samples)
Fragmentation average = 64.7944 (1 samples)
	minimum = 0 (1 samples)
	maximum = 174 (1 samples)
Injected packet rate average = 0.00833854 (1 samples)
	minimum = 0.0015 (1 samples)
	maximum = 0.01875 (1 samples)
Accepted packet rate average = 0.00895573 (1 samples)
	minimum = 0.00625 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.150253 (1 samples)
	minimum = 0.027 (1 samples)
	maximum = 0.3375 (1 samples)
Accepted flit rate average = 0.161376 (1 samples)
	minimum = 0.113 (1 samples)
	maximum = 0.23075 (1 samples)
Injected packet size average = 18.0191 (1 samples)
Accepted packet size average = 18.0193 (1 samples)
Hops average = 5.0685 (1 samples)
Total run time 12.167
