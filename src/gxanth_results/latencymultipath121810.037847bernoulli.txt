BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 365.42
	minimum = 23
	maximum = 917
Network latency average = 291.897
	minimum = 23
	maximum = 896
Slowest packet = 494
Flit latency average = 249.47
	minimum = 6
	maximum = 909
Slowest flit = 8004
Fragmentation average = 54.8608
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0141406
	minimum = 0.006 (at node 100)
	maximum = 0.026 (at node 128)
Accepted packet rate average = 0.00916667
	minimum = 0.004 (at node 41)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.250391
	minimum = 0.108 (at node 100)
	maximum = 0.468 (at node 128)
Accepted flit rate average= 0.171115
	minimum = 0.072 (at node 41)
	maximum = 0.323 (at node 48)
Injected packet length average = 17.7072
Accepted packet length average = 18.667
Total in-flight flits = 17906 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 730.227
	minimum = 23
	maximum = 1794
Network latency average = 404.929
	minimum = 23
	maximum = 1751
Slowest packet = 562
Flit latency average = 352.295
	minimum = 6
	maximum = 1801
Slowest flit = 18466
Fragmentation average = 55.5829
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0116172
	minimum = 0.005 (at node 184)
	maximum = 0.0185 (at node 61)
Accepted packet rate average = 0.0091276
	minimum = 0.0055 (at node 79)
	maximum = 0.015 (at node 166)
Injected flit rate average = 0.206958
	minimum = 0.083 (at node 184)
	maximum = 0.333 (at node 61)
Accepted flit rate average= 0.167581
	minimum = 0.099 (at node 174)
	maximum = 0.2735 (at node 166)
Injected packet length average = 17.8148
Accepted packet length average = 18.3598
Total in-flight flits = 17693 (0 measured)
latency change    = 0.49958
throughput change = 0.0210875
Class 0:
Packet latency average = 1848.25
	minimum = 632
	maximum = 2752
Network latency average = 536.58
	minimum = 23
	maximum = 2404
Slowest packet = 956
Flit latency average = 471.229
	minimum = 6
	maximum = 2403
Slowest flit = 37371
Fragmentation average = 61.6665
	minimum = 0
	maximum = 137
Injected packet rate average = 0.00859896
	minimum = 0 (at node 1)
	maximum = 0.025 (at node 79)
Accepted packet rate average = 0.00871354
	minimum = 0.002 (at node 153)
	maximum = 0.015 (at node 6)
Injected flit rate average = 0.155219
	minimum = 0 (at node 1)
	maximum = 0.446 (at node 79)
Accepted flit rate average= 0.156323
	minimum = 0.043 (at node 117)
	maximum = 0.254 (at node 113)
Injected packet length average = 18.0509
Accepted packet length average = 17.9402
Total in-flight flits = 17595 (0 measured)
latency change    = 0.60491
throughput change = 0.0720164
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2692.75
	minimum = 1693
	maximum = 3632
Network latency average = 336.937
	minimum = 23
	maximum = 979
Slowest packet = 6224
Flit latency average = 521.373
	minimum = 6
	maximum = 2574
Slowest flit = 31093
Fragmentation average = 55.4396
	minimum = 0
	maximum = 133
Injected packet rate average = 0.00910417
	minimum = 0.001 (at node 159)
	maximum = 0.024 (at node 26)
Accepted packet rate average = 0.00901042
	minimum = 0.002 (at node 40)
	maximum = 0.017 (at node 160)
Injected flit rate average = 0.164151
	minimum = 0.018 (at node 159)
	maximum = 0.426 (at node 26)
Accepted flit rate average= 0.163089
	minimum = 0.036 (at node 40)
	maximum = 0.294 (at node 160)
Injected packet length average = 18.0303
Accepted packet length average = 18.1
Total in-flight flits = 17620 (16531 measured)
latency change    = 0.313619
throughput change = 0.0414844
Class 0:
Packet latency average = 3178.36
	minimum = 1693
	maximum = 4449
Network latency average = 453.638
	minimum = 23
	maximum = 1756
Slowest packet = 6224
Flit latency average = 499.06
	minimum = 6
	maximum = 2574
Slowest flit = 31093
Fragmentation average = 57.0546
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00907813
	minimum = 0.003 (at node 42)
	maximum = 0.016 (at node 82)
Accepted packet rate average = 0.00905469
	minimum = 0.0035 (at node 4)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.163482
	minimum = 0.051 (at node 42)
	maximum = 0.285 (at node 82)
Accepted flit rate average= 0.163104
	minimum = 0.063 (at node 4)
	maximum = 0.3 (at node 16)
Injected packet length average = 18.0083
Accepted packet length average = 18.0132
Total in-flight flits = 17567 (17562 measured)
latency change    = 0.152787
throughput change = 9.57977e-05
Class 0:
Packet latency average = 3575.38
	minimum = 1693
	maximum = 5268
Network latency average = 494.022
	minimum = 23
	maximum = 2365
Slowest packet = 6224
Flit latency average = 496.963
	minimum = 6
	maximum = 2574
Slowest flit = 31093
Fragmentation average = 57.724
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00890278
	minimum = 0.00266667 (at node 35)
	maximum = 0.0153333 (at node 107)
Accepted packet rate average = 0.00890972
	minimum = 0.005 (at node 4)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.160236
	minimum = 0.048 (at node 35)
	maximum = 0.274667 (at node 107)
Accepted flit rate average= 0.160314
	minimum = 0.0876667 (at node 36)
	maximum = 0.264667 (at node 16)
Injected packet length average = 17.9984
Accepted packet length average = 17.9932
Total in-flight flits = 17522 (17522 measured)
latency change    = 0.111043
throughput change = 0.0174029
Class 0:
Packet latency average = 3969.6
	minimum = 1693
	maximum = 6193
Network latency average = 511.14
	minimum = 23
	maximum = 2914
Slowest packet = 6224
Flit latency average = 495.064
	minimum = 6
	maximum = 2813
Slowest flit = 125082
Fragmentation average = 57.6326
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00886719
	minimum = 0.00325 (at node 35)
	maximum = 0.01525 (at node 146)
Accepted packet rate average = 0.0088776
	minimum = 0.004 (at node 4)
	maximum = 0.01325 (at node 78)
Injected flit rate average = 0.159746
	minimum = 0.0585 (at node 35)
	maximum = 0.27275 (at node 146)
Accepted flit rate average= 0.159906
	minimum = 0.075 (at node 4)
	maximum = 0.2385 (at node 78)
Injected packet length average = 18.0154
Accepted packet length average = 18.0123
Total in-flight flits = 17439 (17439 measured)
latency change    = 0.0993101
throughput change = 0.00255141
Class 0:
Packet latency average = 4361.46
	minimum = 1693
	maximum = 6864
Network latency average = 515.952
	minimum = 23
	maximum = 3868
Slowest packet = 6224
Flit latency average = 490.33
	minimum = 6
	maximum = 3841
Slowest flit = 129114
Fragmentation average = 56.8125
	minimum = 0
	maximum = 140
Injected packet rate average = 0.008875
	minimum = 0.0038 (at node 35)
	maximum = 0.0138 (at node 28)
Accepted packet rate average = 0.00889479
	minimum = 0.005 (at node 4)
	maximum = 0.0126 (at node 129)
Injected flit rate average = 0.159978
	minimum = 0.0684 (at node 35)
	maximum = 0.2486 (at node 28)
Accepted flit rate average= 0.160151
	minimum = 0.092 (at node 4)
	maximum = 0.2264 (at node 16)
Injected packet length average = 18.0257
Accepted packet length average = 18.005
Total in-flight flits = 17426 (17426 measured)
latency change    = 0.0898457
throughput change = 0.0015285
Class 0:
Packet latency average = 4752.19
	minimum = 1693
	maximum = 7777
Network latency average = 523.066
	minimum = 23
	maximum = 3868
Slowest packet = 6224
Flit latency average = 491.12
	minimum = 6
	maximum = 3841
Slowest flit = 129114
Fragmentation average = 56.5959
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00892969
	minimum = 0.00466667 (at node 164)
	maximum = 0.0136667 (at node 18)
Accepted packet rate average = 0.00890972
	minimum = 0.00566667 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.160611
	minimum = 0.0845 (at node 164)
	maximum = 0.246667 (at node 18)
Accepted flit rate average= 0.160376
	minimum = 0.100833 (at node 36)
	maximum = 0.218 (at node 128)
Injected packet length average = 17.9862
Accepted packet length average = 18.0001
Total in-flight flits = 17756 (17756 measured)
latency change    = 0.0822203
throughput change = 0.00140187
Class 0:
Packet latency average = 5131.4
	minimum = 1693
	maximum = 8611
Network latency average = 528.593
	minimum = 23
	maximum = 3868
Slowest packet = 6224
Flit latency average = 491.868
	minimum = 6
	maximum = 3841
Slowest flit = 129114
Fragmentation average = 56.864
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00888988
	minimum = 0.00514286 (at node 122)
	maximum = 0.0145714 (at node 132)
Accepted packet rate average = 0.00889732
	minimum = 0.00585714 (at node 36)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.160078
	minimum = 0.0925714 (at node 122)
	maximum = 0.260571 (at node 132)
Accepted flit rate average= 0.16023
	minimum = 0.104429 (at node 36)
	maximum = 0.22 (at node 128)
Injected packet length average = 18.0068
Accepted packet length average = 18.0088
Total in-flight flits = 17346 (17346 measured)
latency change    = 0.0739005
throughput change = 0.000910924
Draining all recorded packets ...
Class 0:
Remaining flits: 287226 287227 287228 287229 287230 287231 287232 287233 287234 287235 [...] (17560 flits)
Measured flits: 287226 287227 287228 287229 287230 287231 287232 287233 287234 287235 [...] (17560 flits)
Class 0:
Remaining flits: 316944 316945 316946 316947 316948 316949 316950 316951 316952 316953 [...] (17514 flits)
Measured flits: 316944 316945 316946 316947 316948 316949 316950 316951 316952 316953 [...] (17514 flits)
Class 0:
Remaining flits: 337860 337861 337862 337863 337864 337865 337866 337867 337868 337869 [...] (17864 flits)
Measured flits: 337860 337861 337862 337863 337864 337865 337866 337867 337868 337869 [...] (17864 flits)
Class 0:
Remaining flits: 343998 343999 344000 344001 344002 344003 344004 344005 344006 344007 [...] (17523 flits)
Measured flits: 343998 343999 344000 344001 344002 344003 344004 344005 344006 344007 [...] (17523 flits)
Class 0:
Remaining flits: 393948 393949 393950 393951 393952 393953 393954 393955 393956 393957 [...] (17642 flits)
Measured flits: 393948 393949 393950 393951 393952 393953 393954 393955 393956 393957 [...] (17642 flits)
Class 0:
Remaining flits: 395601 395602 395603 431046 431047 431048 431049 431050 431051 431052 [...] (17787 flits)
Measured flits: 395601 395602 395603 431046 431047 431048 431049 431050 431051 431052 [...] (17787 flits)
Class 0:
Remaining flits: 475956 475957 475958 475959 475960 475961 475962 475963 475964 475965 [...] (18007 flits)
Measured flits: 475956 475957 475958 475959 475960 475961 475962 475963 475964 475965 [...] (18007 flits)
Class 0:
Remaining flits: 486954 486955 486956 486957 486958 486959 486960 486961 486962 486963 [...] (17961 flits)
Measured flits: 486954 486955 486956 486957 486958 486959 486960 486961 486962 486963 [...] (17961 flits)
Class 0:
Remaining flits: 520308 520309 520310 520311 520312 520313 520314 520315 520316 520317 [...] (17493 flits)
Measured flits: 520308 520309 520310 520311 520312 520313 520314 520315 520316 520317 [...] (17493 flits)
Class 0:
Remaining flits: 557640 557641 557642 557643 557644 557645 557646 557647 557648 557649 [...] (18049 flits)
Measured flits: 557640 557641 557642 557643 557644 557645 557646 557647 557648 557649 [...] (18049 flits)
Class 0:
Remaining flits: 604688 604689 604690 604691 609300 609301 609302 609303 609304 609305 [...] (17610 flits)
Measured flits: 604688 604689 604690 604691 609300 609301 609302 609303 609304 609305 [...] (17610 flits)
Class 0:
Remaining flits: 619610 619611 619612 619613 621936 621937 621938 621939 621940 621941 [...] (17638 flits)
Measured flits: 619610 619611 619612 619613 621936 621937 621938 621939 621940 621941 [...] (17638 flits)
Class 0:
Remaining flits: 666654 666655 666656 666657 666658 666659 666660 666661 666662 666663 [...] (17953 flits)
Measured flits: 666654 666655 666656 666657 666658 666659 666660 666661 666662 666663 [...] (17953 flits)
Class 0:
Remaining flits: 680724 680725 680726 680727 680728 680729 680730 680731 680732 680733 [...] (17323 flits)
Measured flits: 680724 680725 680726 680727 680728 680729 680730 680731 680732 680733 [...] (17323 flits)
Class 0:
Remaining flits: 693252 693253 693254 693255 693256 693257 693258 693259 693260 693261 [...] (17610 flits)
Measured flits: 693252 693253 693254 693255 693256 693257 693258 693259 693260 693261 [...] (17610 flits)
Class 0:
Remaining flits: 729720 729721 729722 729723 729724 729725 729726 729727 729728 729729 [...] (18064 flits)
Measured flits: 729720 729721 729722 729723 729724 729725 729726 729727 729728 729729 [...] (18064 flits)
Class 0:
Remaining flits: 772794 772795 772796 772797 772798 772799 772800 772801 772802 772803 [...] (17345 flits)
Measured flits: 772794 772795 772796 772797 772798 772799 772800 772801 772802 772803 [...] (17345 flits)
Class 0:
Remaining flits: 821358 821359 821360 821361 821362 821363 821364 821365 821366 821367 [...] (17482 flits)
Measured flits: 821358 821359 821360 821361 821362 821363 821364 821365 821366 821367 [...] (17482 flits)
Class 0:
Remaining flits: 833017 833018 833019 833020 833021 839826 839827 839828 839829 839830 [...] (17802 flits)
Measured flits: 833017 833018 833019 833020 833021 839826 839827 839828 839829 839830 [...] (17802 flits)
Class 0:
Remaining flits: 852719 852720 852721 852722 852723 852724 852725 852726 852727 852728 [...] (18051 flits)
Measured flits: 852719 852720 852721 852722 852723 852724 852725 852726 852727 852728 [...] (18051 flits)
Class 0:
Remaining flits: 895986 895987 895988 895989 895990 895991 895992 895993 895994 895995 [...] (17991 flits)
Measured flits: 895986 895987 895988 895989 895990 895991 895992 895993 895994 895995 [...] (17991 flits)
Class 0:
Remaining flits: 900769 900770 900771 900772 900773 931913 933228 933229 933230 933231 [...] (17873 flits)
Measured flits: 900769 900770 900771 900772 900773 931913 933228 933229 933230 933231 [...] (17873 flits)
Class 0:
Remaining flits: 972126 972127 972128 972129 972130 972131 972132 972133 972134 972135 [...] (17545 flits)
Measured flits: 972126 972127 972128 972129 972130 972131 972132 972133 972134 972135 [...] (17545 flits)
Class 0:
Remaining flits: 994068 994069 994070 994071 994072 994073 994074 994075 994076 994077 [...] (17459 flits)
Measured flits: 994068 994069 994070 994071 994072 994073 994074 994075 994076 994077 [...] (17423 flits)
Class 0:
Remaining flits: 1006110 1006111 1006112 1006113 1006114 1006115 1006116 1006117 1006118 1006119 [...] (17604 flits)
Measured flits: 1006110 1006111 1006112 1006113 1006114 1006115 1006116 1006117 1006118 1006119 [...] (16970 flits)
Class 0:
Remaining flits: 1061478 1061479 1061480 1061481 1061482 1061483 1061484 1061485 1061486 1061487 [...] (17678 flits)
Measured flits: 1061478 1061479 1061480 1061481 1061482 1061483 1061484 1061485 1061486 1061487 [...] (16758 flits)
Class 0:
Remaining flits: 1092456 1092457 1092458 1092459 1092460 1092461 1092462 1092463 1092464 1092465 [...] (17958 flits)
Measured flits: 1092456 1092457 1092458 1092459 1092460 1092461 1092462 1092463 1092464 1092465 [...] (16478 flits)
Class 0:
Remaining flits: 1118700 1118701 1118702 1118703 1118704 1118705 1118706 1118707 1118708 1118709 [...] (17810 flits)
Measured flits: 1118700 1118701 1118702 1118703 1118704 1118705 1118706 1118707 1118708 1118709 [...] (15703 flits)
Class 0:
Remaining flits: 1141553 1141554 1141555 1141556 1141557 1141558 1141559 1149462 1149463 1149464 [...] (17559 flits)
Measured flits: 1141553 1141554 1141555 1141556 1141557 1141558 1141559 1150992 1150993 1150994 [...] (14229 flits)
Class 0:
Remaining flits: 1171160 1171161 1171162 1171163 1171164 1171165 1171166 1171167 1171168 1171169 [...] (17452 flits)
Measured flits: 1171160 1171161 1171162 1171163 1171164 1171165 1171166 1171167 1171168 1171169 [...] (12440 flits)
Class 0:
Remaining flits: 1209006 1209007 1209008 1209009 1209010 1209011 1209012 1209013 1209014 1209015 [...] (17911 flits)
Measured flits: 1209006 1209007 1209008 1209009 1209010 1209011 1209012 1209013 1209014 1209015 [...] (11253 flits)
Class 0:
Remaining flits: 1243267 1243268 1243269 1243270 1243271 1243272 1243273 1243274 1243275 1243276 [...] (17980 flits)
Measured flits: 1243267 1243268 1243269 1243270 1243271 1243272 1243273 1243274 1243275 1243276 [...] (9643 flits)
Class 0:
Remaining flits: 1280520 1280521 1280522 1280523 1280524 1280525 1280526 1280527 1280528 1280529 [...] (17828 flits)
Measured flits: 1280520 1280521 1280522 1280523 1280524 1280525 1280526 1280527 1280528 1280529 [...] (7958 flits)
Class 0:
Remaining flits: 1294884 1294885 1294886 1294887 1294888 1294889 1294890 1294891 1294892 1294893 [...] (17725 flits)
Measured flits: 1300238 1300239 1300240 1300241 1300242 1300243 1300244 1300245 1300246 1300247 [...] (5657 flits)
Class 0:
Remaining flits: 1327842 1327843 1327844 1327845 1327846 1327847 1327848 1327849 1327850 1327851 [...] (17571 flits)
Measured flits: 1337256 1337257 1337258 1337259 1337260 1337261 1337262 1337263 1337264 1337265 [...] (4175 flits)
Class 0:
Remaining flits: 1357902 1357903 1357904 1357905 1357906 1357907 1357908 1357909 1357910 1357911 [...] (17783 flits)
Measured flits: 1357902 1357903 1357904 1357905 1357906 1357907 1357908 1357909 1357910 1357911 [...] (3370 flits)
Class 0:
Remaining flits: 1386936 1386937 1386938 1386939 1386940 1386941 1386942 1386943 1386944 1386945 [...] (18015 flits)
Measured flits: 1393272 1393273 1393274 1393275 1393276 1393277 1393278 1393279 1393280 1393281 [...] (2623 flits)
Class 0:
Remaining flits: 1422531 1422532 1422533 1422534 1422535 1422536 1422537 1422538 1422539 1429332 [...] (18017 flits)
Measured flits: 1445724 1445725 1445726 1445727 1445728 1445729 1445730 1445731 1445732 1445733 [...] (1783 flits)
Class 0:
Remaining flits: 1452582 1452583 1452584 1452585 1452586 1452587 1452588 1452589 1452590 1452591 [...] (17822 flits)
Measured flits: 1474272 1474273 1474274 1474275 1474276 1474277 1474278 1474279 1474280 1474281 [...] (1140 flits)
Class 0:
Remaining flits: 1478898 1478899 1478900 1478901 1478902 1478903 1478904 1478905 1478906 1478907 [...] (18049 flits)
Measured flits: 1503816 1503817 1503818 1503819 1503820 1503821 1503822 1503823 1503824 1503825 [...] (749 flits)
Class 0:
Remaining flits: 1516878 1516879 1516880 1516881 1516882 1516883 1516884 1516885 1516886 1516887 [...] (17799 flits)
Measured flits: 1545120 1545121 1545122 1545123 1545124 1545125 1545126 1545127 1545128 1545129 [...] (329 flits)
Class 0:
Remaining flits: 1537488 1537489 1537490 1537491 1537492 1537493 1537494 1537495 1537496 1537497 [...] (17842 flits)
Measured flits: 1558062 1558063 1558064 1558065 1558066 1558067 1558068 1558069 1558070 1558071 [...] (98 flits)
Class 0:
Remaining flits: 1550898 1550899 1550900 1550901 1550902 1550903 1550904 1550905 1550906 1550907 [...] (17960 flits)
Measured flits: 1589346 1589347 1589348 1589349 1589350 1589351 1589352 1589353 1589354 1589355 [...] (72 flits)
Draining remaining packets ...
Time taken is 54398 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 17903.1 (1 samples)
	minimum = 1693 (1 samples)
	maximum = 43616 (1 samples)
Network latency average = 553.032 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3868 (1 samples)
Flit latency average = 491.196 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3841 (1 samples)
Fragmentation average = 58.0621 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.00888988 (1 samples)
	minimum = 0.00514286 (1 samples)
	maximum = 0.0145714 (1 samples)
Accepted packet rate average = 0.00889732 (1 samples)
	minimum = 0.00585714 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.160078 (1 samples)
	minimum = 0.0925714 (1 samples)
	maximum = 0.260571 (1 samples)
Accepted flit rate average = 0.16023 (1 samples)
	minimum = 0.104429 (1 samples)
	maximum = 0.22 (1 samples)
Injected packet size average = 18.0068 (1 samples)
Accepted packet size average = 18.0088 (1 samples)
Hops average = 5.06665 (1 samples)
Total run time 40.3146
