BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 234.711
	minimum = 23
	maximum = 836
Network latency average = 231.258
	minimum = 23
	maximum = 830
Slowest packet = 445
Flit latency average = 192.162
	minimum = 6
	maximum = 813
Slowest flit = 8027
Fragmentation average = 57.0389
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0179219
	minimum = 0.009 (at node 143)
	maximum = 0.029 (at node 167)
Accepted packet rate average = 0.00923958
	minimum = 0.004 (at node 23)
	maximum = 0.016 (at node 76)
Injected flit rate average = 0.319188
	minimum = 0.149 (at node 191)
	maximum = 0.522 (at node 167)
Accepted flit rate average= 0.174411
	minimum = 0.072 (at node 174)
	maximum = 0.304 (at node 76)
Injected packet length average = 17.8099
Accepted packet length average = 18.8766
Total in-flight flits = 28451 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 415.134
	minimum = 23
	maximum = 1661
Network latency average = 411.16
	minimum = 23
	maximum = 1660
Slowest packet = 1065
Flit latency average = 372.165
	minimum = 6
	maximum = 1643
Slowest flit = 19187
Fragmentation average = 62.6163
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0176224
	minimum = 0.0095 (at node 46)
	maximum = 0.0245 (at node 66)
Accepted packet rate average = 0.0095625
	minimum = 0.0055 (at node 30)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.316201
	minimum = 0.1695 (at node 46)
	maximum = 0.441 (at node 79)
Accepted flit rate average= 0.175927
	minimum = 0.099 (at node 83)
	maximum = 0.2705 (at node 76)
Injected packet length average = 17.9431
Accepted packet length average = 18.3976
Total in-flight flits = 54340 (0 measured)
latency change    = 0.434613
throughput change = 0.00861507
Class 0:
Packet latency average = 1020.34
	minimum = 24
	maximum = 2467
Network latency average = 1013.63
	minimum = 23
	maximum = 2467
Slowest packet = 1367
Flit latency average = 977.888
	minimum = 6
	maximum = 2450
Slowest flit = 24623
Fragmentation average = 68.9685
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0167604
	minimum = 0 (at node 160)
	maximum = 0.027 (at node 45)
Accepted packet rate average = 0.00975521
	minimum = 0.003 (at node 10)
	maximum = 0.019 (at node 68)
Injected flit rate average = 0.300516
	minimum = 0.01 (at node 160)
	maximum = 0.478 (at node 130)
Accepted flit rate average= 0.175896
	minimum = 0.052 (at node 153)
	maximum = 0.342 (at node 68)
Injected packet length average = 17.9301
Accepted packet length average = 18.031
Total in-flight flits = 78672 (0 measured)
latency change    = 0.59314
throughput change = 0.000177662
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 264.674
	minimum = 25
	maximum = 1508
Network latency average = 195.107
	minimum = 23
	maximum = 917
Slowest packet = 10007
Flit latency average = 1431.63
	minimum = 6
	maximum = 3362
Slowest flit = 32975
Fragmentation average = 25.8235
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0150417
	minimum = 0 (at node 24)
	maximum = 0.029 (at node 113)
Accepted packet rate average = 0.00943229
	minimum = 0.002 (at node 18)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.271078
	minimum = 0 (at node 24)
	maximum = 0.522 (at node 113)
Accepted flit rate average= 0.16901
	minimum = 0.036 (at node 18)
	maximum = 0.302 (at node 16)
Injected packet length average = 18.0218
Accepted packet length average = 17.9183
Total in-flight flits = 98818 (49082 measured)
latency change    = 2.85507
throughput change = 0.0407396
Class 0:
Packet latency average = 851.124
	minimum = 23
	maximum = 2991
Network latency average = 722.305
	minimum = 23
	maximum = 1934
Slowest packet = 10007
Flit latency average = 1629.92
	minimum = 6
	maximum = 4369
Slowest flit = 33713
Fragmentation average = 49.7443
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138151
	minimum = 0.0005 (at node 12)
	maximum = 0.025 (at node 103)
Accepted packet rate average = 0.00928906
	minimum = 0.003 (at node 117)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.248992
	minimum = 0.009 (at node 12)
	maximum = 0.45 (at node 113)
Accepted flit rate average= 0.167646
	minimum = 0.06 (at node 117)
	maximum = 0.2615 (at node 16)
Injected packet length average = 18.0232
Accepted packet length average = 18.0477
Total in-flight flits = 110956 (83353 measured)
latency change    = 0.68903
throughput change = 0.00813968
Class 0:
Packet latency average = 1463.97
	minimum = 23
	maximum = 3643
Network latency average = 1262.66
	minimum = 23
	maximum = 2960
Slowest packet = 10007
Flit latency average = 1845.33
	minimum = 6
	maximum = 4626
Slowest flit = 71963
Fragmentation average = 60.9511
	minimum = 0
	maximum = 144
Injected packet rate average = 0.012875
	minimum = 0.00166667 (at node 24)
	maximum = 0.022 (at node 39)
Accepted packet rate average = 0.00924306
	minimum = 0.00533333 (at node 31)
	maximum = 0.014 (at node 90)
Injected flit rate average = 0.231823
	minimum = 0.03 (at node 24)
	maximum = 0.396667 (at node 39)
Accepted flit rate average= 0.1663
	minimum = 0.096 (at node 31)
	maximum = 0.245 (at node 90)
Injected packet length average = 18.0057
Accepted packet length average = 17.9919
Total in-flight flits = 117811 (104925 measured)
latency change    = 0.418621
throughput change = 0.0080907
Class 0:
Packet latency average = 1985.68
	minimum = 23
	maximum = 4780
Network latency average = 1695.15
	minimum = 23
	maximum = 3990
Slowest packet = 10007
Flit latency average = 2038.25
	minimum = 6
	maximum = 5506
Slowest flit = 65663
Fragmentation average = 65.1504
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0121419
	minimum = 0.004 (at node 24)
	maximum = 0.02125 (at node 14)
Accepted packet rate average = 0.0091901
	minimum = 0.0055 (at node 48)
	maximum = 0.0135 (at node 78)
Injected flit rate average = 0.218608
	minimum = 0.072 (at node 24)
	maximum = 0.38125 (at node 14)
Accepted flit rate average= 0.165158
	minimum = 0.10125 (at node 48)
	maximum = 0.23875 (at node 78)
Injected packet length average = 18.0044
Accepted packet length average = 17.9712
Total in-flight flits = 121499 (116420 measured)
latency change    = 0.262735
throughput change = 0.00691942
Class 0:
Packet latency average = 2447.03
	minimum = 23
	maximum = 5632
Network latency average = 2088.77
	minimum = 23
	maximum = 4969
Slowest packet = 10007
Flit latency average = 2227.24
	minimum = 6
	maximum = 6842
Slowest flit = 71805
Fragmentation average = 67.1622
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0116385
	minimum = 0.0034 (at node 24)
	maximum = 0.0194 (at node 130)
Accepted packet rate average = 0.00920104
	minimum = 0.0058 (at node 48)
	maximum = 0.0132 (at node 128)
Injected flit rate average = 0.209602
	minimum = 0.0586 (at node 24)
	maximum = 0.3472 (at node 130)
Accepted flit rate average= 0.165794
	minimum = 0.1072 (at node 48)
	maximum = 0.2384 (at node 128)
Injected packet length average = 18.0093
Accepted packet length average = 18.019
Total in-flight flits = 122640 (120707 measured)
latency change    = 0.188534
throughput change = 0.00383729
Class 0:
Packet latency average = 2826.8
	minimum = 23
	maximum = 6578
Network latency average = 2396.18
	minimum = 23
	maximum = 5903
Slowest packet = 10007
Flit latency average = 2398.32
	minimum = 6
	maximum = 7148
Slowest flit = 107927
Fragmentation average = 68.5243
	minimum = 0
	maximum = 167
Injected packet rate average = 0.0111094
	minimum = 0.00433333 (at node 24)
	maximum = 0.0183333 (at node 130)
Accepted packet rate average = 0.00915278
	minimum = 0.00633333 (at node 64)
	maximum = 0.0126667 (at node 128)
Injected flit rate average = 0.199989
	minimum = 0.078 (at node 24)
	maximum = 0.331167 (at node 130)
Accepted flit rate average= 0.16453
	minimum = 0.116167 (at node 64)
	maximum = 0.227333 (at node 128)
Injected packet length average = 18.0018
Accepted packet length average = 17.976
Total in-flight flits = 121459 (120737 measured)
latency change    = 0.134348
throughput change = 0.00767863
Class 0:
Packet latency average = 3164.86
	minimum = 23
	maximum = 7357
Network latency average = 2622.78
	minimum = 23
	maximum = 6944
Slowest packet = 10007
Flit latency average = 2548.98
	minimum = 6
	maximum = 7804
Slowest flit = 114767
Fragmentation average = 68.9965
	minimum = 0
	maximum = 167
Injected packet rate average = 0.0107478
	minimum = 0.00471429 (at node 68)
	maximum = 0.0182857 (at node 82)
Accepted packet rate average = 0.0091131
	minimum = 0.00657143 (at node 31)
	maximum = 0.0117143 (at node 90)
Injected flit rate average = 0.193346
	minimum = 0.0848571 (at node 68)
	maximum = 0.329143 (at node 82)
Accepted flit rate average= 0.163969
	minimum = 0.118286 (at node 31)
	maximum = 0.21 (at node 128)
Injected packet length average = 17.9894
Accepted packet length average = 17.9927
Total in-flight flits = 120125 (119925 measured)
latency change    = 0.106814
throughput change = 0.00342068
Draining all recorded packets ...
Class 0:
Remaining flits: 159588 159589 159590 159591 159592 159593 159594 159595 159596 159597 [...] (122060 flits)
Measured flits: 183528 183529 183530 183531 183532 183533 183534 183535 183536 183537 [...] (121611 flits)
Class 0:
Remaining flits: 172962 172963 172964 172965 172966 172967 172968 172969 172970 172971 [...] (121193 flits)
Measured flits: 190728 190729 190730 190731 190732 190733 190734 190735 190736 190737 [...] (118277 flits)
Class 0:
Remaining flits: 190730 190731 190732 190733 190734 190735 190736 190737 190738 190739 [...] (120111 flits)
Measured flits: 190730 190731 190732 190733 190734 190735 190736 190737 190738 190739 [...] (112605 flits)
Class 0:
Remaining flits: 240676 240677 259974 259975 259976 259977 259978 259979 259980 259981 [...] (121208 flits)
Measured flits: 240676 240677 259974 259975 259976 259977 259978 259979 259980 259981 [...] (105798 flits)
Class 0:
Remaining flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (121971 flits)
Measured flits: 300240 300241 300242 300243 300244 300245 300246 300247 300248 300249 [...] (95151 flits)
Class 0:
Remaining flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (119988 flits)
Measured flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (82956 flits)
Class 0:
Remaining flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (121036 flits)
Measured flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (71347 flits)
Class 0:
Remaining flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (123626 flits)
Measured flits: 300978 300979 300980 300981 300982 300983 300984 300985 300986 300987 [...] (58544 flits)
Class 0:
Remaining flits: 371358 371359 371360 371361 371362 371363 371364 371365 371366 371367 [...] (121402 flits)
Measured flits: 371358 371359 371360 371361 371362 371363 371364 371365 371366 371367 [...] (45415 flits)
Class 0:
Remaining flits: 395982 395983 395984 395985 395986 395987 395988 395989 395990 395991 [...] (122191 flits)
Measured flits: 395982 395983 395984 395985 395986 395987 395988 395989 395990 395991 [...] (33260 flits)
Class 0:
Remaining flits: 396882 396883 396884 396885 396886 396887 396888 396889 396890 396891 [...] (120767 flits)
Measured flits: 396882 396883 396884 396885 396886 396887 396888 396889 396890 396891 [...] (23341 flits)
Class 0:
Remaining flits: 396882 396883 396884 396885 396886 396887 396888 396889 396890 396891 [...] (121833 flits)
Measured flits: 396882 396883 396884 396885 396886 396887 396888 396889 396890 396891 [...] (15549 flits)
Class 0:
Remaining flits: 431640 431641 431642 431643 431644 431645 431646 431647 431648 431649 [...] (122570 flits)
Measured flits: 431640 431641 431642 431643 431644 431645 431646 431647 431648 431649 [...] (9849 flits)
Class 0:
Remaining flits: 437292 437293 437294 437295 437296 437297 437298 437299 437300 437301 [...] (121589 flits)
Measured flits: 437292 437293 437294 437295 437296 437297 437298 437299 437300 437301 [...] (6491 flits)
Class 0:
Remaining flits: 537840 537841 537842 537843 537844 537845 537846 537847 537848 537849 [...] (118793 flits)
Measured flits: 537840 537841 537842 537843 537844 537845 537846 537847 537848 537849 [...] (4040 flits)
Class 0:
Remaining flits: 582606 582607 582608 582609 582610 582611 582612 582613 582614 582615 [...] (118713 flits)
Measured flits: 582606 582607 582608 582609 582610 582611 582612 582613 582614 582615 [...] (2842 flits)
Class 0:
Remaining flits: 612558 612559 612560 612561 612562 612563 612564 612565 612566 612567 [...] (121159 flits)
Measured flits: 612558 612559 612560 612561 612562 612563 612564 612565 612566 612567 [...] (2231 flits)
Class 0:
Remaining flits: 662616 662617 662618 662619 662620 662621 662622 662623 662624 662625 [...] (120802 flits)
Measured flits: 697122 697123 697124 697125 697126 697127 697128 697129 697130 697131 [...] (1424 flits)
Class 0:
Remaining flits: 690984 690985 690986 690987 690988 690989 690990 690991 690992 690993 [...] (120625 flits)
Measured flits: 697122 697123 697124 697125 697126 697127 697128 697129 697130 697131 [...] (1008 flits)
Class 0:
Remaining flits: 712386 712387 712388 712389 712390 712391 712392 712393 712394 712395 [...] (120258 flits)
Measured flits: 720126 720127 720128 720129 720130 720131 720132 720133 720134 720135 [...] (709 flits)
Class 0:
Remaining flits: 778752 778753 778754 778755 778756 778757 778758 778759 778760 778761 [...] (119794 flits)
Measured flits: 827964 827965 827966 827967 827968 827969 827970 827971 827972 827973 [...] (576 flits)
Class 0:
Remaining flits: 782154 782155 782156 782157 782158 782159 782160 782161 782162 782163 [...] (118006 flits)
Measured flits: 827964 827965 827966 827967 827968 827969 827970 827971 827972 827973 [...] (558 flits)
Class 0:
Remaining flits: 798858 798859 798860 798861 798862 798863 798864 798865 798866 798867 [...] (119158 flits)
Measured flits: 827964 827965 827966 827967 827968 827969 827970 827971 827972 827973 [...] (306 flits)
Class 0:
Remaining flits: 817542 817543 817544 817545 817546 817547 817548 817549 817550 817551 [...] (117431 flits)
Measured flits: 843120 843121 843122 843123 843124 843125 843126 843127 843128 843129 [...] (180 flits)
Class 0:
Remaining flits: 817558 817559 848880 848881 848882 848883 848884 848885 848886 848887 [...] (119286 flits)
Measured flits: 885204 885205 885206 885207 885208 885209 885210 885211 885212 885213 [...] (108 flits)
Class 0:
Remaining flits: 856296 856297 856298 856299 856300 856301 856302 856303 856304 856305 [...] (119206 flits)
Measured flits: 885204 885205 885206 885207 885208 885209 885210 885211 885212 885213 [...] (72 flits)
Class 0:
Remaining flits: 872622 872623 872624 872625 872626 872627 872628 872629 872630 872631 [...] (120208 flits)
Measured flits: 949338 949339 949340 949341 949342 949343 949344 949345 949346 949347 [...] (36 flits)
Class 0:
Remaining flits: 919397 919398 919399 919400 919401 919402 919403 920052 920053 920054 [...] (118603 flits)
Measured flits: 952110 952111 952112 952113 952114 952115 952116 952117 952118 952119 [...] (18 flits)
Class 0:
Remaining flits: 937710 937711 937712 937713 937714 937715 937716 937717 937718 937719 [...] (117647 flits)
Measured flits: 952124 952125 952126 952127 (4 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 964872 964873 964874 964875 964876 964877 964878 964879 964880 964881 [...] (88093 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1068678 1068679 1068680 1068681 1068682 1068683 1068684 1068685 1068686 1068687 [...] (59030 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1074312 1074313 1074314 1074315 1074316 1074317 1074318 1074319 1074320 1074321 [...] (30784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1099368 1099369 1099370 1099371 1099372 1099373 1099374 1099375 1099376 1099377 [...] (8092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1312755 1312756 1312757 (3 flits)
Measured flits: (0 flits)
Time taken is 44031 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6500.31 (1 samples)
	minimum = 23 (1 samples)
	maximum = 29032 (1 samples)
Network latency average = 3685.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14839 (1 samples)
Flit latency average = 3653.15 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14822 (1 samples)
Fragmentation average = 71.0217 (1 samples)
	minimum = 0 (1 samples)
	maximum = 167 (1 samples)
Injected packet rate average = 0.0107478 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0182857 (1 samples)
Accepted packet rate average = 0.0091131 (1 samples)
	minimum = 0.00657143 (1 samples)
	maximum = 0.0117143 (1 samples)
Injected flit rate average = 0.193346 (1 samples)
	minimum = 0.0848571 (1 samples)
	maximum = 0.329143 (1 samples)
Accepted flit rate average = 0.163969 (1 samples)
	minimum = 0.118286 (1 samples)
	maximum = 0.21 (1 samples)
Injected packet size average = 17.9894 (1 samples)
Accepted packet size average = 17.9927 (1 samples)
Hops average = 5.06881 (1 samples)
Total run time 34.6421
