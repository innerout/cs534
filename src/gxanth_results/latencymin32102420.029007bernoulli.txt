BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 274.087
	minimum = 22
	maximum = 735
Network latency average = 266.064
	minimum = 22
	maximum = 735
Slowest packet = 1346
Flit latency average = 233.501
	minimum = 5
	maximum = 718
Slowest flit = 24245
Fragmentation average = 50.961
	minimum = 0
	maximum = 318
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.013901
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 70)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.26176
	minimum = 0.096 (at node 174)
	maximum = 0.438 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.8303
Total in-flight flits = 49390 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 499.174
	minimum = 22
	maximum = 1459
Network latency average = 490.327
	minimum = 22
	maximum = 1425
Slowest packet = 2958
Flit latency average = 455.084
	minimum = 5
	maximum = 1408
Slowest flit = 53261
Fragmentation average = 62.8594
	minimum = 0
	maximum = 388
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.014724
	minimum = 0.0085 (at node 30)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.272099
	minimum = 0.153 (at node 153)
	maximum = 0.407 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 18.48
Total in-flight flits = 94576 (0 measured)
latency change    = 0.45092
throughput change = 0.0379955
Class 0:
Packet latency average = 1120.29
	minimum = 23
	maximum = 2122
Network latency average = 1111.59
	minimum = 22
	maximum = 2122
Slowest packet = 4436
Flit latency average = 1076.75
	minimum = 5
	maximum = 2220
Slowest flit = 75629
Fragmentation average = 78.7142
	minimum = 0
	maximum = 416
Injected packet rate average = 0.0287708
	minimum = 0.016 (at node 65)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0159271
	minimum = 0.008 (at node 113)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.518208
	minimum = 0.288 (at node 65)
	maximum = 0.818 (at node 69)
Accepted flit rate average= 0.287208
	minimum = 0.11 (at node 113)
	maximum = 0.5 (at node 34)
Injected packet length average = 18.0116
Accepted packet length average = 18.0327
Total in-flight flits = 138864 (0 measured)
latency change    = 0.554424
throughput change = 0.0526077
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 46.0441
	minimum = 22
	maximum = 171
Network latency average = 36.0991
	minimum = 22
	maximum = 171
Slowest packet = 16792
Flit latency average = 1481.19
	minimum = 5
	maximum = 2774
Slowest flit = 77363
Fragmentation average = 6.59912
	minimum = 0
	maximum = 36
Injected packet rate average = 0.029625
	minimum = 0.013 (at node 119)
	maximum = 0.045 (at node 150)
Accepted packet rate average = 0.0161302
	minimum = 0.007 (at node 17)
	maximum = 0.027 (at node 78)
Injected flit rate average = 0.532229
	minimum = 0.234 (at node 119)
	maximum = 0.815 (at node 150)
Accepted flit rate average= 0.289536
	minimum = 0.128 (at node 110)
	maximum = 0.476 (at node 90)
Injected packet length average = 17.9655
Accepted packet length average = 17.95
Total in-flight flits = 185657 (94069 measured)
latency change    = 23.3308
throughput change = 0.00804087
Class 0:
Packet latency average = 49.2716
	minimum = 22
	maximum = 1943
Network latency average = 39.8238
	minimum = 22
	maximum = 1938
Slowest packet = 16654
Flit latency average = 1694.8
	minimum = 5
	maximum = 3264
Slowest flit = 171534
Fragmentation average = 6.83502
	minimum = 0
	maximum = 238
Injected packet rate average = 0.0291536
	minimum = 0.0185 (at node 23)
	maximum = 0.0385 (at node 13)
Accepted packet rate average = 0.0158568
	minimum = 0.0095 (at node 36)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.524307
	minimum = 0.333 (at node 109)
	maximum = 0.689 (at node 22)
Accepted flit rate average= 0.286031
	minimum = 0.166 (at node 88)
	maximum = 0.4305 (at node 128)
Injected packet length average = 17.9843
Accepted packet length average = 18.0384
Total in-flight flits = 230538 (185328 measured)
latency change    = 0.0655053
throughput change = 0.0122546
Class 0:
Packet latency average = 609.745
	minimum = 22
	maximum = 2998
Network latency average = 600.754
	minimum = 22
	maximum = 2974
Slowest packet = 16613
Flit latency average = 1900.69
	minimum = 5
	maximum = 3911
Slowest flit = 199727
Fragmentation average = 22.0773
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0291406
	minimum = 0.0196667 (at node 115)
	maximum = 0.0373333 (at node 136)
Accepted packet rate average = 0.0159184
	minimum = 0.0103333 (at node 36)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.524429
	minimum = 0.354 (at node 115)
	maximum = 0.672 (at node 136)
Accepted flit rate average= 0.28722
	minimum = 0.187667 (at node 36)
	maximum = 0.400667 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 18.0433
Total in-flight flits = 275555 (269050 measured)
latency change    = 0.919193
throughput change = 0.0041405
Class 0:
Packet latency average = 1851.59
	minimum = 22
	maximum = 3932
Network latency average = 1842.37
	minimum = 22
	maximum = 3928
Slowest packet = 16780
Flit latency average = 2107.72
	minimum = 5
	maximum = 4435
Slowest flit = 253544
Fragmentation average = 59.7719
	minimum = 0
	maximum = 439
Injected packet rate average = 0.0291667
	minimum = 0.023 (at node 115)
	maximum = 0.0345 (at node 186)
Accepted packet rate average = 0.0159596
	minimum = 0.01125 (at node 162)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.524824
	minimum = 0.4105 (at node 115)
	maximum = 0.62425 (at node 186)
Accepted flit rate average= 0.287714
	minimum = 0.2075 (at node 162)
	maximum = 0.3705 (at node 130)
Injected packet length average = 17.994
Accepted packet length average = 18.0276
Total in-flight flits = 321100 (320787 measured)
latency change    = 0.67069
throughput change = 0.0017137
Class 0:
Packet latency average = 2384.89
	minimum = 22
	maximum = 4882
Network latency average = 2375.32
	minimum = 22
	maximum = 4882
Slowest packet = 17216
Flit latency average = 2312.42
	minimum = 5
	maximum = 4992
Slowest flit = 254699
Fragmentation average = 74.4353
	minimum = 0
	maximum = 449
Injected packet rate average = 0.029251
	minimum = 0.023 (at node 127)
	maximum = 0.035 (at node 71)
Accepted packet rate average = 0.0159469
	minimum = 0.0124 (at node 42)
	maximum = 0.0198 (at node 128)
Injected flit rate average = 0.526481
	minimum = 0.414 (at node 127)
	maximum = 0.63 (at node 71)
Accepted flit rate average= 0.287891
	minimum = 0.2238 (at node 42)
	maximum = 0.3574 (at node 128)
Injected packet length average = 17.9987
Accepted packet length average = 18.0531
Total in-flight flits = 367947 (367947 measured)
latency change    = 0.223619
throughput change = 0.000615106
Class 0:
Packet latency average = 2746.46
	minimum = 22
	maximum = 5502
Network latency average = 2736.92
	minimum = 22
	maximum = 5483
Slowest packet = 18217
Flit latency average = 2523.78
	minimum = 5
	maximum = 5668
Slowest flit = 331274
Fragmentation average = 82.4692
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0291953
	minimum = 0.0238333 (at node 127)
	maximum = 0.0341667 (at node 107)
Accepted packet rate average = 0.0159774
	minimum = 0.013 (at node 42)
	maximum = 0.0198333 (at node 138)
Injected flit rate average = 0.525501
	minimum = 0.428333 (at node 127)
	maximum = 0.6145 (at node 107)
Accepted flit rate average= 0.288321
	minimum = 0.232167 (at node 190)
	maximum = 0.358333 (at node 138)
Injected packet length average = 17.9995
Accepted packet length average = 18.0455
Total in-flight flits = 412112 (412112 measured)
latency change    = 0.131649
throughput change = 0.00149332
Class 0:
Packet latency average = 3046.45
	minimum = 22
	maximum = 6409
Network latency average = 3037.12
	minimum = 22
	maximum = 6392
Slowest packet = 19215
Flit latency average = 2736.86
	minimum = 5
	maximum = 6375
Slowest flit = 345887
Fragmentation average = 86.6818
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0291362
	minimum = 0.0241429 (at node 127)
	maximum = 0.0351429 (at node 107)
Accepted packet rate average = 0.0159807
	minimum = 0.0127143 (at node 42)
	maximum = 0.0201429 (at node 138)
Injected flit rate average = 0.524373
	minimum = 0.433143 (at node 127)
	maximum = 0.630571 (at node 107)
Accepted flit rate average= 0.288063
	minimum = 0.226286 (at node 102)
	maximum = 0.358857 (at node 138)
Injected packet length average = 17.9973
Accepted packet length average = 18.0257
Total in-flight flits = 456569 (456569 measured)
latency change    = 0.0984722
throughput change = 0.000895416
Draining all recorded packets ...
Class 0:
Remaining flits: 412850 412851 412852 412853 412854 412855 412856 412857 412858 412859 [...] (503151 flits)
Measured flits: 412850 412851 412852 412853 412854 412855 412856 412857 412858 412859 [...] (410741 flits)
Class 0:
Remaining flits: 495411 495412 495413 502508 502509 502510 502511 502512 502513 502514 [...] (546101 flits)
Measured flits: 495411 495412 495413 502508 502509 502510 502511 502512 502513 502514 [...] (363387 flits)
Class 0:
Remaining flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (591239 flits)
Measured flits: 516402 516403 516404 516405 516406 516407 516408 516409 516410 516411 [...] (315650 flits)
Class 0:
Remaining flits: 585717 585718 585719 587052 587053 587054 587055 587056 587057 587058 [...] (637478 flits)
Measured flits: 585717 585718 585719 587052 587053 587054 587055 587056 587057 587058 [...] (268164 flits)
Class 0:
Remaining flits: 634842 634843 634844 634845 634846 634847 634848 634849 634850 634851 [...] (682278 flits)
Measured flits: 634842 634843 634844 634845 634846 634847 634848 634849 634850 634851 [...] (221071 flits)
Class 0:
Remaining flits: 675791 677664 677665 677666 677667 677668 677669 677670 677671 677672 [...] (730173 flits)
Measured flits: 675791 677664 677665 677666 677667 677668 677669 677670 677671 677672 [...] (173988 flits)
Class 0:
Remaining flits: 723708 723709 723710 723711 723712 723713 723714 723715 723716 723717 [...] (774225 flits)
Measured flits: 723708 723709 723710 723711 723712 723713 723714 723715 723716 723717 [...] (126687 flits)
Class 0:
Remaining flits: 750402 750403 750404 750405 750406 750407 750408 750409 750410 750411 [...] (818721 flits)
Measured flits: 750402 750403 750404 750405 750406 750407 750408 750409 750410 750411 [...] (79169 flits)
Class 0:
Remaining flits: 815436 815437 815438 815439 815440 815441 815442 815443 815444 815445 [...] (862462 flits)
Measured flits: 815436 815437 815438 815439 815440 815441 815442 815443 815444 815445 [...] (33829 flits)
Class 0:
Remaining flits: 901854 901855 901856 901857 901858 901859 901860 901861 901862 901863 [...] (907672 flits)
Measured flits: 901854 901855 901856 901857 901858 901859 901860 901861 901862 901863 [...] (7170 flits)
Class 0:
Remaining flits: 934776 934777 934778 934779 934780 934781 934782 934783 934784 934785 [...] (951106 flits)
Measured flits: 934776 934777 934778 934779 934780 934781 934782 934783 934784 934785 [...] (656 flits)
Class 0:
Remaining flits: 1001160 1001161 1001162 1001163 1001164 1001165 1001166 1001167 1001168 1001169 [...] (995058 flits)
Measured flits: 1001160 1001161 1001162 1001163 1001164 1001165 1001166 1001167 1001168 1001169 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1086410 1086411 1086412 1086413 1086414 1086415 1086416 1086417 1086418 1086419 [...] (966446 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1132092 1132093 1132094 1132095 1132096 1132097 1132098 1132099 1132100 1132101 [...] (918801 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1168650 1168651 1168652 1168653 1168654 1168655 1168656 1168657 1168658 1168659 [...] (871464 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1217590 1217591 1221428 1221429 1221430 1221431 1221432 1221433 1221434 1221435 [...] (824004 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1281204 1281205 1281206 1281207 1281208 1281209 1281210 1281211 1281212 1281213 [...] (776345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1311048 1311049 1311050 1311051 1311052 1311053 1311054 1311055 1311056 1311057 [...] (729378 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1379448 1379449 1379450 1379451 1379452 1379453 1379454 1379455 1379456 1379457 [...] (682085 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1436294 1436295 1436296 1436297 1436298 1436299 1436300 1436301 1436302 1436303 [...] (634569 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1495998 1495999 1496000 1496001 1496002 1496003 1496004 1496005 1496006 1496007 [...] (587282 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1525554 1525555 1525556 1525557 1525558 1525559 1525560 1525561 1525562 1525563 [...] (539768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1571715 1571716 1571717 1571718 1571719 1571720 1571721 1571722 1571723 1582632 [...] (492435 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1624850 1624851 1624852 1624853 1624854 1624855 1624856 1624857 1624858 1624859 [...] (445058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1671334 1671335 1671352 1671353 1684415 1684416 1684417 1684418 1684419 1684420 [...] (397453 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1708848 1708849 1708850 1708851 1708852 1708853 1708854 1708855 1708856 1708857 [...] (350023 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1776803 1776804 1776805 1776806 1776807 1776808 1776809 1776810 1776811 1776812 [...] (302497 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1784176 1784177 1809558 1809559 1809560 1809561 1809562 1809563 1809564 1809565 [...] (254798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1814688 1814689 1814690 1814691 1814692 1814693 1814694 1814695 1814696 1814697 [...] (207462 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1846260 1846261 1846262 1846263 1846264 1846265 1846266 1846267 1846268 1846269 [...] (160081 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1905282 1905283 1905284 1905285 1905286 1905287 1905288 1905289 1905290 1905291 [...] (112400 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1911796 1911797 1959408 1959409 1959410 1959411 1959412 1959413 1959414 1959415 [...] (65002 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1971412 1971413 1986611 1986612 1986613 1986614 1986615 1986616 1986617 1986618 [...] (23041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2067984 2067985 2067986 2067987 2067988 2067989 2067990 2067991 2067992 2067993 [...] (4787 flits)
Measured flits: (0 flits)
Time taken is 45314 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5887.45 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12468 (1 samples)
Network latency average = 5877.97 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12452 (1 samples)
Flit latency average = 10604.8 (1 samples)
	minimum = 5 (1 samples)
	maximum = 24451 (1 samples)
Fragmentation average = 109.677 (1 samples)
	minimum = 0 (1 samples)
	maximum = 483 (1 samples)
Injected packet rate average = 0.0291362 (1 samples)
	minimum = 0.0241429 (1 samples)
	maximum = 0.0351429 (1 samples)
Accepted packet rate average = 0.0159807 (1 samples)
	minimum = 0.0127143 (1 samples)
	maximum = 0.0201429 (1 samples)
Injected flit rate average = 0.524373 (1 samples)
	minimum = 0.433143 (1 samples)
	maximum = 0.630571 (1 samples)
Accepted flit rate average = 0.288063 (1 samples)
	minimum = 0.226286 (1 samples)
	maximum = 0.358857 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 18.0257 (1 samples)
Hops average = 5.07298 (1 samples)
Total run time 41.9396
