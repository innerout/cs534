BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.548
	minimum = 22
	maximum = 982
Network latency average = 235.255
	minimum = 22
	maximum = 849
Slowest packet = 46
Flit latency average = 205.837
	minimum = 5
	maximum = 877
Slowest flit = 9662
Fragmentation average = 47.4145
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139479
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26375
	minimum = 0.118 (at node 150)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8285
Accepted packet length average = 18.9096
Total in-flight flits = 52194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 616.056
	minimum = 22
	maximum = 1951
Network latency average = 446.403
	minimum = 22
	maximum = 1615
Slowest packet = 46
Flit latency average = 415.17
	minimum = 5
	maximum = 1731
Slowest flit = 22883
Fragmentation average = 68.2237
	minimum = 0
	maximum = 410
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0147943
	minimum = 0.009 (at node 30)
	maximum = 0.021 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.275729
	minimum = 0.162 (at node 153)
	maximum = 0.391 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.6376
Total in-flight flits = 110894 (0 measured)
latency change    = 0.429357
throughput change = 0.0434454
Class 0:
Packet latency average = 1357.86
	minimum = 25
	maximum = 2820
Network latency average = 1077.45
	minimum = 22
	maximum = 2503
Slowest packet = 4200
Flit latency average = 1040.3
	minimum = 5
	maximum = 2486
Slowest flit = 43667
Fragmentation average = 103.866
	minimum = 0
	maximum = 436
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0165313
	minimum = 0.007 (at node 190)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.297781
	minimum = 0.128 (at node 113)
	maximum = 0.532 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.0132
Total in-flight flits = 175886 (0 measured)
latency change    = 0.546303
throughput change = 0.0740546
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 446.424
	minimum = 28
	maximum = 2067
Network latency average = 37.4346
	minimum = 22
	maximum = 203
Slowest packet = 18899
Flit latency average = 1471.51
	minimum = 5
	maximum = 3192
Slowest flit = 74411
Fragmentation average = 6.47483
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0167292
	minimum = 0.005 (at node 184)
	maximum = 0.03 (at node 128)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.302354
	minimum = 0.107 (at node 184)
	maximum = 0.536 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 18.0735
Total in-flight flits = 247218 (118532 measured)
latency change    = 2.04163
throughput change = 0.0151244
Class 0:
Packet latency average = 497.137
	minimum = 24
	maximum = 2581
Network latency average = 85.4031
	minimum = 22
	maximum = 1966
Slowest packet = 18899
Flit latency average = 1702.51
	minimum = 5
	maximum = 3809
Slowest flit = 123259
Fragmentation average = 8.90066
	minimum = 0
	maximum = 242
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0166276
	minimum = 0.009 (at node 5)
	maximum = 0.0255 (at node 103)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.299792
	minimum = 0.169 (at node 5)
	maximum = 0.46 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 18.0298
Total in-flight flits = 315556 (232486 measured)
latency change    = 0.102009
throughput change = 0.0085476
Class 0:
Packet latency average = 913.919
	minimum = 24
	maximum = 3575
Network latency average = 535.069
	minimum = 22
	maximum = 2959
Slowest packet = 18899
Flit latency average = 1934.44
	minimum = 5
	maximum = 4578
Slowest flit = 140975
Fragmentation average = 29.8077
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0166233
	minimum = 0.0113333 (at node 79)
	maximum = 0.0243333 (at node 128)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.299898
	minimum = 0.202 (at node 79)
	maximum = 0.439 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 18.0408
Total in-flight flits = 384241 (339260 measured)
latency change    = 0.456038
throughput change = 0.00035313
Draining remaining packets ...
Class 0:
Remaining flits: 190188 190189 190190 190191 190192 190193 190194 190195 190196 190197 [...] (336593 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (315366 flits)
Class 0:
Remaining flits: 209405 209406 209407 209408 209409 209410 209411 210230 210231 210232 [...] (289274 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (279631 flits)
Class 0:
Remaining flits: 220660 220661 235194 235195 235196 235197 235198 235199 235200 235201 [...] (241668 flits)
Measured flits: 339120 339121 339122 339123 339124 339125 339126 339127 339128 339129 [...] (238322 flits)
Class 0:
Remaining flits: 278801 280682 280683 280684 280685 280686 280687 280688 280689 280690 [...] (193988 flits)
Measured flits: 339588 339589 339590 339591 339592 339593 339594 339595 339596 339597 [...] (193235 flits)
Class 0:
Remaining flits: 321800 321801 321802 321803 330696 330697 330698 330699 330700 330701 [...] (146731 flits)
Measured flits: 339588 339589 339590 339591 339592 339593 339594 339595 339596 339597 [...] (146692 flits)
Class 0:
Remaining flits: 350550 350551 350552 350553 350554 350555 350556 350557 350558 350559 [...] (100653 flits)
Measured flits: 350550 350551 350552 350553 350554 350555 350556 350557 350558 350559 [...] (100653 flits)
Class 0:
Remaining flits: 372096 372097 372098 372099 372100 372101 372102 372103 372104 372105 [...] (57359 flits)
Measured flits: 372096 372097 372098 372099 372100 372101 372102 372103 372104 372105 [...] (57359 flits)
Class 0:
Remaining flits: 415170 415171 415172 415173 415174 415175 415176 415177 415178 415179 [...] (26624 flits)
Measured flits: 415170 415171 415172 415173 415174 415175 415176 415177 415178 415179 [...] (26624 flits)
Class 0:
Remaining flits: 477876 477877 477878 477879 477880 477881 484973 489669 489670 489671 [...] (8868 flits)
Measured flits: 477876 477877 477878 477879 477880 477881 484973 489669 489670 489671 [...] (8868 flits)
Class 0:
Remaining flits: 529272 529273 529274 529275 529276 529277 529278 529279 529280 529281 [...] (2745 flits)
Measured flits: 529272 529273 529274 529275 529276 529277 529278 529279 529280 529281 [...] (2745 flits)
Class 0:
Remaining flits: 552356 552357 552358 552359 552360 552361 552362 552363 552364 552365 [...] (1335 flits)
Measured flits: 552356 552357 552358 552359 552360 552361 552362 552363 552364 552365 [...] (1335 flits)
Class 0:
Remaining flits: 637114 637115 637116 637117 637118 637119 637120 637121 637122 637123 [...] (335 flits)
Measured flits: 637114 637115 637116 637117 637118 637119 637120 637121 637122 637123 [...] (335 flits)
Time taken is 18358 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5911.67 (1 samples)
	minimum = 24 (1 samples)
	maximum = 13948 (1 samples)
Network latency average = 5489.15 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12703 (1 samples)
Flit latency average = 4625.79 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12686 (1 samples)
Fragmentation average = 139.199 (1 samples)
	minimum = 0 (1 samples)
	maximum = 515 (1 samples)
Injected packet rate average = 0.0367569 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0166233 (1 samples)
	minimum = 0.0113333 (1 samples)
	maximum = 0.0243333 (1 samples)
Injected flit rate average = 0.661641 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.299898 (1 samples)
	minimum = 0.202 (1 samples)
	maximum = 0.439 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 18.0408 (1 samples)
Hops average = 5.07855 (1 samples)
Total run time 14.3152
