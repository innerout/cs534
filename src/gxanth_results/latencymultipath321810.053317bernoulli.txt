BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 403.434
	minimum = 23
	maximum = 962
Network latency average = 353.622
	minimum = 23
	maximum = 962
Slowest packet = 231
Flit latency average = 284.487
	minimum = 6
	maximum = 963
Slowest flit = 3302
Fragmentation average = 173.136
	minimum = 0
	maximum = 814
Injected packet rate average = 0.0224323
	minimum = 0.007 (at node 56)
	maximum = 0.034 (at node 147)
Accepted packet rate average = 0.0100052
	minimum = 0.003 (at node 4)
	maximum = 0.018 (at node 177)
Injected flit rate average = 0.395833
	minimum = 0.118 (at node 56)
	maximum = 0.596 (at node 147)
Accepted flit rate average= 0.207333
	minimum = 0.073 (at node 4)
	maximum = 0.364 (at node 27)
Injected packet length average = 17.6457
Accepted packet length average = 20.7225
Total in-flight flits = 37862 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 794.213
	minimum = 23
	maximum = 1886
Network latency average = 597.59
	minimum = 23
	maximum = 1860
Slowest packet = 407
Flit latency average = 497.934
	minimum = 6
	maximum = 1920
Slowest flit = 7072
Fragmentation average = 197.383
	minimum = 0
	maximum = 1725
Injected packet rate average = 0.0164844
	minimum = 0.0035 (at node 56)
	maximum = 0.025 (at node 121)
Accepted packet rate average = 0.0106224
	minimum = 0.0055 (at node 108)
	maximum = 0.0155 (at node 154)
Injected flit rate average = 0.293268
	minimum = 0.0625 (at node 56)
	maximum = 0.4465 (at node 121)
Accepted flit rate average= 0.203888
	minimum = 0.122 (at node 116)
	maximum = 0.303 (at node 22)
Injected packet length average = 17.7907
Accepted packet length average = 19.1942
Total in-flight flits = 36025 (0 measured)
latency change    = 0.492033
throughput change = 0.0168981
Class 0:
Packet latency average = 1930.43
	minimum = 1060
	maximum = 2792
Network latency average = 969.014
	minimum = 27
	maximum = 2726
Slowest packet = 1472
Flit latency average = 836.411
	minimum = 6
	maximum = 2801
Slowest flit = 25753
Fragmentation average = 212.385
	minimum = 0
	maximum = 2078
Injected packet rate average = 0.0111198
	minimum = 0 (at node 0)
	maximum = 0.031 (at node 99)
Accepted packet rate average = 0.0110677
	minimum = 0.002 (at node 96)
	maximum = 0.02 (at node 99)
Injected flit rate average = 0.200292
	minimum = 0 (at node 0)
	maximum = 0.547 (at node 99)
Accepted flit rate average= 0.198339
	minimum = 0.048 (at node 96)
	maximum = 0.385 (at node 99)
Injected packet length average = 18.0122
Accepted packet length average = 17.9205
Total in-flight flits = 36284 (0 measured)
latency change    = 0.588581
throughput change = 0.0279798
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2752.15
	minimum = 1597
	maximum = 3676
Network latency average = 342.571
	minimum = 25
	maximum = 958
Slowest packet = 8509
Flit latency average = 889.819
	minimum = 6
	maximum = 3781
Slowest flit = 25721
Fragmentation average = 122.159
	minimum = 0
	maximum = 597
Injected packet rate average = 0.0107083
	minimum = 0 (at node 6)
	maximum = 0.031 (at node 161)
Accepted packet rate average = 0.0110156
	minimum = 0.003 (at node 17)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.192974
	minimum = 0 (at node 6)
	maximum = 0.565 (at node 161)
Accepted flit rate average= 0.197854
	minimum = 0.051 (at node 180)
	maximum = 0.341 (at node 19)
Injected packet length average = 18.0209
Accepted packet length average = 17.9612
Total in-flight flits = 35484 (25015 measured)
latency change    = 0.298575
throughput change = 0.00244814
Class 0:
Packet latency average = 3303.29
	minimum = 1597
	maximum = 4416
Network latency average = 608.096
	minimum = 25
	maximum = 1976
Slowest packet = 8509
Flit latency average = 900.854
	minimum = 6
	maximum = 4764
Slowest flit = 31252
Fragmentation average = 156.714
	minimum = 0
	maximum = 1443
Injected packet rate average = 0.0106927
	minimum = 0 (at node 60)
	maximum = 0.023 (at node 39)
Accepted packet rate average = 0.0109245
	minimum = 0.004 (at node 180)
	maximum = 0.017 (at node 129)
Injected flit rate average = 0.192521
	minimum = 0 (at node 60)
	maximum = 0.4125 (at node 39)
Accepted flit rate average= 0.195703
	minimum = 0.0795 (at node 180)
	maximum = 0.307 (at node 19)
Injected packet length average = 18.0049
Accepted packet length average = 17.9142
Total in-flight flits = 35096 (32357 measured)
latency change    = 0.166846
throughput change = 0.0109914
Class 0:
Packet latency average = 3771.93
	minimum = 1597
	maximum = 5393
Network latency average = 730.773
	minimum = 25
	maximum = 2804
Slowest packet = 8509
Flit latency average = 893.91
	minimum = 6
	maximum = 5457
Slowest flit = 15082
Fragmentation average = 170.659
	minimum = 0
	maximum = 1850
Injected packet rate average = 0.0108281
	minimum = 0 (at node 60)
	maximum = 0.0213333 (at node 39)
Accepted packet rate average = 0.0108594
	minimum = 0.00566667 (at node 4)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.194844
	minimum = 0 (at node 60)
	maximum = 0.385 (at node 39)
Accepted flit rate average= 0.195236
	minimum = 0.108667 (at node 36)
	maximum = 0.279667 (at node 16)
Injected packet length average = 17.9942
Accepted packet length average = 17.9786
Total in-flight flits = 36238 (35587 measured)
latency change    = 0.124245
throughput change = 0.00239205
Class 0:
Packet latency average = 4208.11
	minimum = 1597
	maximum = 6322
Network latency average = 817.958
	minimum = 25
	maximum = 3758
Slowest packet = 8509
Flit latency average = 892.338
	minimum = 6
	maximum = 6315
Slowest flit = 23201
Fragmentation average = 183.731
	minimum = 0
	maximum = 3377
Injected packet rate average = 0.0106797
	minimum = 0 (at node 60)
	maximum = 0.01875 (at node 49)
Accepted packet rate average = 0.0108333
	minimum = 0.007 (at node 36)
	maximum = 0.01525 (at node 0)
Injected flit rate average = 0.19225
	minimum = 0 (at node 60)
	maximum = 0.336 (at node 170)
Accepted flit rate average= 0.194598
	minimum = 0.118 (at node 36)
	maximum = 0.2735 (at node 0)
Injected packet length average = 18.0015
Accepted packet length average = 17.9629
Total in-flight flits = 34613 (34353 measured)
latency change    = 0.103653
throughput change = 0.0032809
Class 0:
Packet latency average = 4635.95
	minimum = 1597
	maximum = 7385
Network latency average = 874.452
	minimum = 25
	maximum = 4424
Slowest packet = 8509
Flit latency average = 896.974
	minimum = 6
	maximum = 7193
Slowest flit = 49949
Fragmentation average = 189.359
	minimum = 0
	maximum = 3377
Injected packet rate average = 0.0107073
	minimum = 0.0018 (at node 60)
	maximum = 0.018 (at node 62)
Accepted packet rate average = 0.0107948
	minimum = 0.0068 (at node 36)
	maximum = 0.0146 (at node 129)
Injected flit rate average = 0.192653
	minimum = 0.033 (at node 60)
	maximum = 0.324 (at node 62)
Accepted flit rate average= 0.193979
	minimum = 0.1166 (at node 36)
	maximum = 0.2652 (at node 129)
Injected packet length average = 17.9927
Accepted packet length average = 17.9697
Total in-flight flits = 35266 (35152 measured)
latency change    = 0.0922876
throughput change = 0.00318843
Class 0:
Packet latency average = 5049.82
	minimum = 1597
	maximum = 8231
Network latency average = 913.158
	minimum = 25
	maximum = 5329
Slowest packet = 8509
Flit latency average = 902.887
	minimum = 6
	maximum = 7562
Slowest flit = 92209
Fragmentation average = 193.439
	minimum = 0
	maximum = 3377
Injected packet rate average = 0.0106918
	minimum = 0.00416667 (at node 85)
	maximum = 0.0171667 (at node 161)
Accepted packet rate average = 0.0107491
	minimum = 0.0075 (at node 36)
	maximum = 0.0143333 (at node 128)
Injected flit rate average = 0.192439
	minimum = 0.0763333 (at node 85)
	maximum = 0.309333 (at node 161)
Accepted flit rate average= 0.193191
	minimum = 0.135833 (at node 36)
	maximum = 0.256833 (at node 128)
Injected packet length average = 17.9987
Accepted packet length average = 17.9727
Total in-flight flits = 35506 (35466 measured)
latency change    = 0.0819567
throughput change = 0.00407987
Class 0:
Packet latency average = 5463.81
	minimum = 1597
	maximum = 9370
Network latency average = 936.845
	minimum = 25
	maximum = 5815
Slowest packet = 8509
Flit latency average = 903.618
	minimum = 6
	maximum = 8665
Slowest flit = 83753
Fragmentation average = 196.389
	minimum = 0
	maximum = 3458
Injected packet rate average = 0.0107254
	minimum = 0.00442857 (at node 36)
	maximum = 0.0167143 (at node 39)
Accepted packet rate average = 0.0107433
	minimum = 0.008 (at node 64)
	maximum = 0.0137143 (at node 128)
Injected flit rate average = 0.193135
	minimum = 0.0777143 (at node 36)
	maximum = 0.299857 (at node 39)
Accepted flit rate average= 0.193313
	minimum = 0.143143 (at node 64)
	maximum = 0.245286 (at node 129)
Injected packet length average = 18.0071
Accepted packet length average = 17.9938
Total in-flight flits = 36013 (36013 measured)
latency change    = 0.0757698
throughput change = 0.000632506
Draining all recorded packets ...
Class 0:
Remaining flits: 195097 195098 195099 195100 195101 218249 232665 232666 232667 243153 [...] (37508 flits)
Measured flits: 195097 195098 195099 195100 195101 218249 232665 232666 232667 243153 [...] (37508 flits)
Class 0:
Remaining flits: 195100 195101 261738 261739 261740 261741 261742 261743 261744 261745 [...] (34594 flits)
Measured flits: 195100 195101 261738 261739 261740 261741 261742 261743 261744 261745 [...] (34594 flits)
Class 0:
Remaining flits: 289260 289261 289262 289263 289264 289265 289266 289267 289268 289269 [...] (36418 flits)
Measured flits: 289260 289261 289262 289263 289264 289265 289266 289267 289268 289269 [...] (36418 flits)
Class 0:
Remaining flits: 343509 343510 343511 349236 349237 349238 349239 349240 349241 349242 [...] (36011 flits)
Measured flits: 343509 343510 343511 349236 349237 349238 349239 349240 349241 349242 [...] (36011 flits)
Class 0:
Remaining flits: 343509 343510 343511 349236 349237 349238 349239 349240 349241 349242 [...] (34950 flits)
Measured flits: 343509 343510 343511 349236 349237 349238 349239 349240 349241 349242 [...] (34950 flits)
Class 0:
Remaining flits: 349236 349237 349238 349239 349240 349241 349242 349243 349244 349245 [...] (37907 flits)
Measured flits: 349236 349237 349238 349239 349240 349241 349242 349243 349244 349245 [...] (37907 flits)
Class 0:
Remaining flits: 421968 421969 421970 421971 421972 421973 438090 438091 438092 438093 [...] (36474 flits)
Measured flits: 421968 421969 421970 421971 421972 421973 438090 438091 438092 438093 [...] (36474 flits)
Class 0:
Remaining flits: 460098 460099 460100 460101 460102 460103 460104 460105 460106 460107 [...] (36157 flits)
Measured flits: 460098 460099 460100 460101 460102 460103 460104 460105 460106 460107 [...] (36157 flits)
Class 0:
Remaining flits: 525204 525205 525206 525207 525208 525209 525210 525211 525212 525213 [...] (36322 flits)
Measured flits: 525204 525205 525206 525207 525208 525209 525210 525211 525212 525213 [...] (36322 flits)
Class 0:
Remaining flits: 532355 532356 532357 532358 532359 532360 532361 532362 532363 532364 [...] (36042 flits)
Measured flits: 532355 532356 532357 532358 532359 532360 532361 532362 532363 532364 [...] (36042 flits)
Class 0:
Remaining flits: 565656 565657 565658 565659 565660 565661 565662 565663 565664 565665 [...] (37462 flits)
Measured flits: 565656 565657 565658 565659 565660 565661 565662 565663 565664 565665 [...] (37462 flits)
Class 0:
Remaining flits: 585522 585523 585524 585525 585526 585527 585528 585529 585530 585531 [...] (36329 flits)
Measured flits: 585522 585523 585524 585525 585526 585527 585528 585529 585530 585531 [...] (36329 flits)
Class 0:
Remaining flits: 585522 585523 585524 585525 585526 585527 585528 585529 585530 585531 [...] (36881 flits)
Measured flits: 585522 585523 585524 585525 585526 585527 585528 585529 585530 585531 [...] (36881 flits)
Class 0:
Remaining flits: 662385 662386 662387 662388 662389 662390 662391 662392 662393 662394 [...] (34983 flits)
Measured flits: 662385 662386 662387 662388 662389 662390 662391 662392 662393 662394 [...] (34983 flits)
Class 0:
Remaining flits: 676398 676399 676400 676401 676402 676403 690842 690843 690844 690845 [...] (35705 flits)
Measured flits: 676398 676399 676400 676401 676402 676403 690842 690843 690844 690845 [...] (35705 flits)
Class 0:
Remaining flits: 691299 691300 691301 691302 691303 691304 691305 691306 691307 699102 [...] (37971 flits)
Measured flits: 691299 691300 691301 691302 691303 691304 691305 691306 691307 699102 [...] (37971 flits)
Class 0:
Remaining flits: 699119 724446 724447 724448 724449 724450 724451 724452 724453 724454 [...] (37321 flits)
Measured flits: 699119 724446 724447 724448 724449 724450 724451 724452 724453 724454 [...] (37321 flits)
Class 0:
Remaining flits: 724446 724447 724448 724449 724450 724451 724452 724453 724454 724455 [...] (36824 flits)
Measured flits: 724446 724447 724448 724449 724450 724451 724452 724453 724454 724455 [...] (36824 flits)
Class 0:
Remaining flits: 835465 835466 835467 835468 835469 838581 838582 838583 882594 882595 [...] (36548 flits)
Measured flits: 835465 835466 835467 835468 835469 838581 838582 838583 882594 882595 [...] (36548 flits)
Class 0:
Remaining flits: 897372 897373 897374 897375 897376 897377 897378 897379 897380 897381 [...] (36316 flits)
Measured flits: 897372 897373 897374 897375 897376 897377 897378 897379 897380 897381 [...] (36316 flits)
Class 0:
Remaining flits: 922939 922940 922941 922942 922943 922944 922945 922946 922947 922948 [...] (36840 flits)
Measured flits: 922939 922940 922941 922942 922943 922944 922945 922946 922947 922948 [...] (36840 flits)
Class 0:
Remaining flits: 950248 950249 950250 950251 950252 950253 950254 950255 989208 989209 [...] (36355 flits)
Measured flits: 950248 950249 950250 950251 950252 950253 950254 950255 989208 989209 [...] (36355 flits)
Class 0:
Remaining flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (35647 flits)
Measured flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (35647 flits)
Class 0:
Remaining flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (34615 flits)
Measured flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (34615 flits)
Class 0:
Remaining flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (35428 flits)
Measured flits: 989208 989209 989210 989211 989212 989213 989214 989215 989216 989217 [...] (35428 flits)
Class 0:
Remaining flits: 1066860 1066861 1066862 1066863 1066864 1066865 1066866 1066867 1066868 1066869 [...] (37463 flits)
Measured flits: 1066860 1066861 1066862 1066863 1066864 1066865 1066866 1066867 1066868 1066869 [...] (37183 flits)
Class 0:
Remaining flits: 1069362 1069363 1069364 1069365 1069366 1069367 1069368 1069369 1069370 1069371 [...] (35711 flits)
Measured flits: 1069362 1069363 1069364 1069365 1069366 1069367 1069368 1069369 1069370 1069371 [...] (34823 flits)
Class 0:
Remaining flits: 1069379 1163106 1163107 1163108 1163109 1163110 1163111 1163112 1163113 1163114 [...] (35368 flits)
Measured flits: 1069379 1163106 1163107 1163108 1163109 1163110 1163111 1163112 1163113 1163114 [...] (34401 flits)
Class 0:
Remaining flits: 1254780 1254781 1254782 1254783 1254784 1254785 1254786 1254787 1254788 1254789 [...] (36717 flits)
Measured flits: 1254780 1254781 1254782 1254783 1254784 1254785 1254786 1254787 1254788 1254789 [...] (35161 flits)
Class 0:
Remaining flits: 1254780 1254781 1254782 1254783 1254784 1254785 1254786 1254787 1254788 1254789 [...] (35433 flits)
Measured flits: 1254780 1254781 1254782 1254783 1254784 1254785 1254786 1254787 1254788 1254789 [...] (33096 flits)
Class 0:
Remaining flits: 1271448 1271449 1271450 1271451 1271452 1271453 1271454 1271455 1271456 1271457 [...] (37100 flits)
Measured flits: 1271448 1271449 1271450 1271451 1271452 1271453 1271454 1271455 1271456 1271457 [...] (33823 flits)
Class 0:
Remaining flits: 1343934 1343935 1343936 1343937 1343938 1343939 1343940 1343941 1343942 1343943 [...] (37427 flits)
Measured flits: 1343934 1343935 1343936 1343937 1343938 1343939 1343940 1343941 1343942 1343943 [...] (32561 flits)
Class 0:
Remaining flits: 1343934 1343935 1343936 1343937 1343938 1343939 1343940 1343941 1343942 1343943 [...] (36429 flits)
Measured flits: 1343934 1343935 1343936 1343937 1343938 1343939 1343940 1343941 1343942 1343943 [...] (30177 flits)
Class 0:
Remaining flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (37580 flits)
Measured flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (28014 flits)
Class 0:
Remaining flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (35768 flits)
Measured flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (23693 flits)
Class 0:
Remaining flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (36504 flits)
Measured flits: 1380474 1380475 1380476 1380477 1380478 1380479 1380480 1380481 1380482 1380483 [...] (22148 flits)
Class 0:
Remaining flits: 1445472 1445473 1445474 1445475 1445476 1445477 1445478 1445479 1445480 1445481 [...] (36997 flits)
Measured flits: 1445472 1445473 1445474 1445475 1445476 1445477 1445478 1445479 1445480 1445481 [...] (19429 flits)
Class 0:
Remaining flits: 1567334 1567335 1567336 1567337 1567338 1567339 1567340 1567341 1567342 1567343 [...] (35752 flits)
Measured flits: 1567334 1567335 1567336 1567337 1567338 1567339 1567340 1567341 1567342 1567343 [...] (14992 flits)
Class 0:
Remaining flits: 1585905 1585906 1585907 1592874 1592875 1592876 1592877 1592878 1592879 1592880 [...] (35296 flits)
Measured flits: 1585905 1585906 1585907 1592874 1592875 1592876 1592877 1592878 1592879 1592880 [...] (12394 flits)
Class 0:
Remaining flits: 1615121 1645506 1645507 1645508 1645509 1645510 1645511 1645512 1645513 1645514 [...] (34793 flits)
Measured flits: 1615121 1645506 1645507 1645508 1645509 1645510 1645511 1645512 1645513 1645514 [...] (10759 flits)
Class 0:
Remaining flits: 1646766 1646767 1646768 1646769 1646770 1646771 1646772 1646773 1646774 1646775 [...] (36156 flits)
Measured flits: 1646766 1646767 1646768 1646769 1646770 1646771 1646772 1646773 1646774 1646775 [...] (9066 flits)
Class 0:
Remaining flits: 1650637 1650638 1650639 1650640 1650641 1650642 1650643 1650644 1650645 1650646 [...] (36820 flits)
Measured flits: 1650637 1650638 1650639 1650640 1650641 1650642 1650643 1650644 1650645 1650646 [...] (8496 flits)
Class 0:
Remaining flits: 1746630 1746631 1746632 1746633 1746634 1746635 1746636 1746637 1746638 1746639 [...] (36330 flits)
Measured flits: 1754989 1754990 1754991 1754992 1754993 1754994 1754995 1754996 1754997 1754998 [...] (7480 flits)
Class 0:
Remaining flits: 1810386 1810387 1810388 1810389 1810390 1810391 1810392 1810393 1810394 1810395 [...] (36864 flits)
Measured flits: 1812492 1812493 1812494 1812495 1812496 1812497 1812498 1812499 1812500 1812501 [...] (5625 flits)
Class 0:
Remaining flits: 1810386 1810387 1810388 1810389 1810390 1810391 1810392 1810393 1810394 1810395 [...] (37254 flits)
Measured flits: 1885392 1885393 1885394 1885395 1885396 1885397 1885398 1885399 1885400 1885401 [...] (5223 flits)
Class 0:
Remaining flits: 1810392 1810393 1810394 1810395 1810396 1810397 1810398 1810399 1810400 1810401 [...] (37080 flits)
Measured flits: 1920771 1920772 1920773 1920774 1920775 1920776 1920777 1920778 1920779 1924791 [...] (4080 flits)
Class 0:
Remaining flits: 1924791 1924792 1924793 1933578 1933579 1933580 1933581 1933582 1933583 1933584 [...] (36315 flits)
Measured flits: 1924791 1924792 1924793 1984828 1984829 1984830 1984831 1984832 1984833 1984834 [...] (3898 flits)
Class 0:
Remaining flits: 1933578 1933579 1933580 1933581 1933582 1933583 1933584 1933585 1933586 1933587 [...] (37239 flits)
Measured flits: 1984838 1984839 1984840 1984841 1993138 1993139 2002716 2002717 2002718 2002719 [...] (3765 flits)
Class 0:
Remaining flits: 1933578 1933579 1933580 1933581 1933582 1933583 1933584 1933585 1933586 1933587 [...] (37104 flits)
Measured flits: 2037852 2037853 2037854 2037855 2037856 2037857 2037858 2037859 2037860 2037861 [...] (3743 flits)
Class 0:
Remaining flits: 1933585 1933586 1933587 1933588 1933589 1933590 1933591 1933592 1933593 1933594 [...] (36699 flits)
Measured flits: 2113182 2113183 2113184 2113185 2113186 2113187 2113188 2113189 2113190 2113191 [...] (3128 flits)
Class 0:
Remaining flits: 2040462 2040463 2040464 2040465 2040466 2040467 2040468 2040469 2040470 2040471 [...] (36560 flits)
Measured flits: 2113182 2113183 2113184 2113185 2113186 2113187 2113188 2113189 2113190 2113191 [...] (3085 flits)
Class 0:
Remaining flits: 2042928 2042929 2042930 2042931 2042932 2042933 2042934 2042935 2042936 2042937 [...] (37202 flits)
Measured flits: 2113182 2113183 2113184 2113185 2113186 2113187 2113188 2113189 2113190 2113191 [...] (2796 flits)
Class 0:
Remaining flits: 2042928 2042929 2042930 2042931 2042932 2042933 2042934 2042935 2042936 2042937 [...] (35499 flits)
Measured flits: 2203110 2203111 2203112 2203113 2203114 2203115 2203116 2203117 2203118 2203119 [...] (2157 flits)
Class 0:
Remaining flits: 2079144 2079145 2079146 2079147 2079148 2079149 2079150 2079151 2079152 2079153 [...] (36168 flits)
Measured flits: 2206836 2206837 2206838 2206839 2206840 2206841 2206842 2206843 2206844 2206845 [...] (1822 flits)
Class 0:
Remaining flits: 2079144 2079145 2079146 2079147 2079148 2079149 2079150 2079151 2079152 2079153 [...] (36478 flits)
Measured flits: 2304855 2304856 2304857 2304858 2304859 2304860 2304861 2304862 2304863 2313936 [...] (1847 flits)
Class 0:
Remaining flits: 2108250 2108251 2108252 2108253 2108254 2108255 2108256 2108257 2108258 2108259 [...] (35886 flits)
Measured flits: 2313936 2313937 2313938 2313939 2313940 2313941 2313942 2313943 2313944 2313945 [...] (1646 flits)
Class 0:
Remaining flits: 2222820 2222821 2222822 2222823 2222824 2222825 2222826 2222827 2222828 2222829 [...] (36227 flits)
Measured flits: 2332098 2332099 2332100 2332101 2332102 2332103 2332104 2332105 2332106 2332107 [...] (1270 flits)
Class 0:
Remaining flits: 2276928 2276929 2276930 2276931 2276932 2276933 2276934 2276935 2276936 2276937 [...] (37502 flits)
Measured flits: 2332098 2332099 2332100 2332101 2332102 2332103 2332104 2332105 2332106 2332107 [...] (1167 flits)
Class 0:
Remaining flits: 2311685 2319102 2319103 2319104 2319105 2319106 2319107 2319108 2319109 2319110 [...] (36798 flits)
Measured flits: 2394864 2394865 2394866 2394867 2394868 2394869 2394870 2394871 2394872 2394873 [...] (943 flits)
Class 0:
Remaining flits: 2319118 2319119 2356866 2356867 2356868 2356869 2356870 2356871 2356872 2356873 [...] (36602 flits)
Measured flits: 2394864 2394865 2394866 2394867 2394868 2394869 2394870 2394871 2394872 2394873 [...] (817 flits)
Class 0:
Remaining flits: 2356866 2356867 2356868 2356869 2356870 2356871 2356872 2356873 2356874 2356875 [...] (37871 flits)
Measured flits: 2489184 2489185 2489186 2489187 2489188 2489189 2489190 2489191 2489192 2489193 [...] (813 flits)
Class 0:
Remaining flits: 2356866 2356867 2356868 2356869 2356870 2356871 2356872 2356873 2356874 2356875 [...] (36056 flits)
Measured flits: 2489193 2489194 2489195 2489196 2489197 2489198 2489199 2489200 2489201 2559582 [...] (629 flits)
Class 0:
Remaining flits: 2356866 2356867 2356868 2356869 2356870 2356871 2356872 2356873 2356874 2356875 [...] (36085 flits)
Measured flits: 2572956 2572957 2572958 2572959 2572960 2572961 2572962 2572963 2572964 2572965 [...] (444 flits)
Class 0:
Remaining flits: 2356870 2356871 2356872 2356873 2356874 2356875 2356876 2356877 2356878 2356879 [...] (37781 flits)
Measured flits: 2621193 2621194 2621195 2631690 2631691 2631692 2631693 2631694 2631695 2631696 [...] (298 flits)
Class 0:
Remaining flits: 2559438 2559439 2559440 2559441 2559442 2559443 2559444 2559445 2559446 2559447 [...] (37132 flits)
Measured flits: 2699442 2699443 2699444 2699445 2699446 2699447 2699448 2699449 2699450 2699451 [...] (174 flits)
Class 0:
Remaining flits: 2580516 2580517 2580518 2580519 2580520 2580521 2580522 2580523 2580524 2580525 [...] (36246 flits)
Measured flits: 2763468 2763469 2763470 2763471 2763472 2763473 2763474 2763475 2763476 2763477 [...] (110 flits)
Class 0:
Remaining flits: 2580516 2580517 2580518 2580519 2580520 2580521 2580522 2580523 2580524 2580525 [...] (37458 flits)
Measured flits: 2830446 2830447 2830448 2830449 2830450 2830451 2830452 2830453 2830454 2830455 [...] (18 flits)
Class 0:
Remaining flits: 2642435 2643678 2643679 2643680 2643681 2643682 2643683 2643684 2643685 2643686 [...] (36987 flits)
Measured flits: 2830446 2830447 2830448 2830449 2830450 2830451 2830452 2830453 2830454 2830455 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2735226 2735227 2735228 2735229 2735230 2735231 2735232 2735233 2735234 2735235 [...] (8081 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2820690 2820691 2820692 2820693 2820694 2820695 2820696 2820697 2820698 2820699 [...] (109 flits)
Measured flits: (0 flits)
Time taken is 81020 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 22256.5 (1 samples)
	minimum = 1597 (1 samples)
	maximum = 69006 (1 samples)
Network latency average = 1060.43 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10395 (1 samples)
Flit latency average = 946.43 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11774 (1 samples)
Fragmentation average = 207.54 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5046 (1 samples)
Injected packet rate average = 0.0107254 (1 samples)
	minimum = 0.00442857 (1 samples)
	maximum = 0.0167143 (1 samples)
Accepted packet rate average = 0.0107433 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0137143 (1 samples)
Injected flit rate average = 0.193135 (1 samples)
	minimum = 0.0777143 (1 samples)
	maximum = 0.299857 (1 samples)
Accepted flit rate average = 0.193313 (1 samples)
	minimum = 0.143143 (1 samples)
	maximum = 0.245286 (1 samples)
Injected packet size average = 18.0071 (1 samples)
Accepted packet size average = 17.9938 (1 samples)
Hops average = 5.05883 (1 samples)
Total run time 92.5491
