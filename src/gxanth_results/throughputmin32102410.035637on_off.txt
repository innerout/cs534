BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.628
	minimum = 27
	maximum = 989
Network latency average = 280.957
	minimum = 23
	maximum = 877
Slowest packet = 21
Flit latency average = 216.461
	minimum = 6
	maximum = 960
Slowest flit = 2534
Fragmentation average = 146.352
	minimum = 0
	maximum = 756
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0101979
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 88)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.209714
	minimum = 0.089 (at node 41)
	maximum = 0.357 (at node 166)
Injected packet length average = 17.8246
Accepted packet length average = 20.5644
Total in-flight flits = 60697 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 644.069
	minimum = 27
	maximum = 1975
Network latency average = 504.588
	minimum = 23
	maximum = 1891
Slowest packet = 166
Flit latency average = 429.128
	minimum = 6
	maximum = 1874
Slowest flit = 7433
Fragmentation average = 182.173
	minimum = 0
	maximum = 1609
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0116693
	minimum = 0.007 (at node 24)
	maximum = 0.018 (at node 166)
Injected flit rate average = 0.561727
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 28)
Accepted flit rate average= 0.223971
	minimum = 0.135 (at node 135)
	maximum = 0.3395 (at node 166)
Injected packet length average = 17.9111
Accepted packet length average = 19.1933
Total in-flight flits = 130769 (0 measured)
latency change    = 0.416789
throughput change = 0.0636591
Class 0:
Packet latency average = 1362.37
	minimum = 31
	maximum = 2944
Network latency average = 1136
	minimum = 23
	maximum = 2741
Slowest packet = 2211
Flit latency average = 1087.53
	minimum = 6
	maximum = 2800
Slowest flit = 16206
Fragmentation average = 211.697
	minimum = 0
	maximum = 1684
Injected packet rate average = 0.0354427
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0133281
	minimum = 0.004 (at node 75)
	maximum = 0.023 (at node 24)
Injected flit rate average = 0.638953
	minimum = 0 (at node 14)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.240969
	minimum = 0.079 (at node 75)
	maximum = 0.386 (at node 177)
Injected packet length average = 18.0278
Accepted packet length average = 18.0797
Total in-flight flits = 206993 (0 measured)
latency change    = 0.527244
throughput change = 0.0705378
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 420.348
	minimum = 33
	maximum = 2329
Network latency average = 80.8846
	minimum = 25
	maximum = 841
Slowest packet = 18901
Flit latency average = 1519.62
	minimum = 6
	maximum = 3726
Slowest flit = 24787
Fragmentation average = 25.0679
	minimum = 0
	maximum = 349
Injected packet rate average = 0.03525
	minimum = 0 (at node 42)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0137083
	minimum = 0.003 (at node 52)
	maximum = 0.024 (at node 119)
Injected flit rate average = 0.633786
	minimum = 0 (at node 42)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.244958
	minimum = 0.088 (at node 52)
	maximum = 0.432 (at node 119)
Injected packet length average = 17.9798
Accepted packet length average = 17.8693
Total in-flight flits = 281785 (110890 measured)
latency change    = 2.24106
throughput change = 0.0162868
Class 0:
Packet latency average = 522.122
	minimum = 29
	maximum = 2329
Network latency average = 196.074
	minimum = 23
	maximum = 1955
Slowest packet = 18901
Flit latency average = 1779.15
	minimum = 6
	maximum = 4636
Slowest flit = 33627
Fragmentation average = 40.8225
	minimum = 0
	maximum = 790
Injected packet rate average = 0.0349375
	minimum = 0.002 (at node 86)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0135286
	minimum = 0.0065 (at node 36)
	maximum = 0.02 (at node 156)
Injected flit rate average = 0.62856
	minimum = 0.036 (at node 86)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.242378
	minimum = 0.121 (at node 36)
	maximum = 0.3625 (at node 156)
Injected packet length average = 17.991
Accepted packet length average = 17.9159
Total in-flight flits = 355408 (217545 measured)
latency change    = 0.194924
throughput change = 0.0106476
Class 0:
Packet latency average = 763.924
	minimum = 29
	maximum = 3406
Network latency average = 445.524
	minimum = 23
	maximum = 2982
Slowest packet = 18901
Flit latency average = 2037.27
	minimum = 6
	maximum = 5627
Slowest flit = 25316
Fragmentation average = 59.747
	minimum = 0
	maximum = 1037
Injected packet rate average = 0.0350694
	minimum = 0.00533333 (at node 38)
	maximum = 0.0556667 (at node 9)
Accepted packet rate average = 0.0134271
	minimum = 0.00833333 (at node 36)
	maximum = 0.0186667 (at node 53)
Injected flit rate average = 0.63112
	minimum = 0.0923333 (at node 38)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.241094
	minimum = 0.148333 (at node 36)
	maximum = 0.334667 (at node 53)
Injected packet length average = 17.9963
Accepted packet length average = 17.9558
Total in-flight flits = 431723 (323160 measured)
latency change    = 0.316526
throughput change = 0.00532512
Draining remaining packets ...
Class 0:
Remaining flits: 47070 47071 47072 47073 47074 47075 47076 47077 47078 47079 [...] (395736 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (311858 flits)
Class 0:
Remaining flits: 47087 52098 52099 52100 52101 52102 52103 52104 52105 52106 [...] (360388 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (297304 flits)
Class 0:
Remaining flits: 61326 61327 61328 61329 61330 61331 61332 61333 61334 61335 [...] (325979 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (279300 flits)
Class 0:
Remaining flits: 67050 67051 67052 67053 67054 67055 67056 67057 67058 67059 [...] (292174 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (256979 flits)
Class 0:
Remaining flits: 67050 67051 67052 67053 67054 67055 67056 67057 67058 67059 [...] (258279 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (232524 flits)
Class 0:
Remaining flits: 67050 67051 67052 67053 67054 67055 67056 67057 67058 67059 [...] (224693 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (205635 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (191205 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (177774 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (158464 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (148904 flits)
Class 0:
Remaining flits: 102444 102445 102446 102447 102448 102449 102450 102451 102452 102453 [...] (125356 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (118798 flits)
Class 0:
Remaining flits: 139266 139267 139268 139269 139270 139271 139272 139273 139274 139275 [...] (93609 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (89390 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (65777 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (63350 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (42238 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (40845 flits)
Class 0:
Remaining flits: 149046 149047 149048 149049 149050 149051 149052 149053 149054 149055 [...] (22484 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (21743 flits)
Class 0:
Remaining flits: 220737 220738 220739 220740 220741 220742 220743 220744 220745 220746 [...] (7059 flits)
Measured flits: 342846 342847 342848 342849 342850 342851 342852 342853 342854 342855 [...] (6896 flits)
Class 0:
Remaining flits: 434466 434467 434468 434469 434470 434471 434472 434473 434474 434475 [...] (359 flits)
Measured flits: 434466 434467 434468 434469 434470 434471 434472 434473 434474 434475 [...] (359 flits)
Time taken is 21348 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8448.24 (1 samples)
	minimum = 29 (1 samples)
	maximum = 19413 (1 samples)
Network latency average = 8087.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 17498 (1 samples)
Flit latency average = 6892.68 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18050 (1 samples)
Fragmentation average = 178.335 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3888 (1 samples)
Injected packet rate average = 0.0350694 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0134271 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.63112 (1 samples)
	minimum = 0.0923333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.241094 (1 samples)
	minimum = 0.148333 (1 samples)
	maximum = 0.334667 (1 samples)
Injected packet size average = 17.9963 (1 samples)
Accepted packet size average = 17.9558 (1 samples)
Hops average = 5.07658 (1 samples)
Total run time 19.7191
