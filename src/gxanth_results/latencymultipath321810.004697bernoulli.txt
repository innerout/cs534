BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 53.7964
	minimum = 23
	maximum = 181
Network latency average = 52.8922
	minimum = 23
	maximum = 181
Slowest packet = 85
Flit latency average = 29.1509
	minimum = 6
	maximum = 164
Slowest flit = 1547
Fragmentation average = 13.1641
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00458333
	minimum = 0 (at node 33)
	maximum = 0.01 (at node 39)
Accepted packet rate average = 0.00434896
	minimum = 0.001 (at node 4)
	maximum = 0.011 (at node 140)
Injected flit rate average = 0.0817552
	minimum = 0 (at node 33)
	maximum = 0.18 (at node 39)
Accepted flit rate average= 0.0793698
	minimum = 0.018 (at node 4)
	maximum = 0.198 (at node 140)
Injected packet length average = 17.8375
Accepted packet length average = 18.2503
Total in-flight flits = 601 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 54.0981
	minimum = 23
	maximum = 199
Network latency average = 53.3523
	minimum = 23
	maximum = 186
Slowest packet = 931
Flit latency average = 29.3525
	minimum = 6
	maximum = 169
Slowest flit = 16775
Fragmentation average = 13.5084
	minimum = 0
	maximum = 149
Injected packet rate average = 0.00447656
	minimum = 0.001 (at node 12)
	maximum = 0.0085 (at node 39)
Accepted packet rate average = 0.00435417
	minimum = 0.0005 (at node 41)
	maximum = 0.008 (at node 48)
Injected flit rate average = 0.080276
	minimum = 0.018 (at node 12)
	maximum = 0.153 (at node 39)
Accepted flit rate average= 0.0791328
	minimum = 0.009 (at node 41)
	maximum = 0.144 (at node 48)
Injected packet length average = 17.9325
Accepted packet length average = 18.174
Total in-flight flits = 555 (0 measured)
latency change    = 0.00557652
throughput change = 0.0029947
Class 0:
Packet latency average = 54.0602
	minimum = 23
	maximum = 157
Network latency average = 53.2243
	minimum = 23
	maximum = 157
Slowest packet = 2074
Flit latency average = 29.1001
	minimum = 6
	maximum = 140
Slowest flit = 37349
Fragmentation average = 13.5525
	minimum = 0
	maximum = 118
Injected packet rate average = 0.00477604
	minimum = 0 (at node 40)
	maximum = 0.013 (at node 121)
Accepted packet rate average = 0.00476042
	minimum = 0 (at node 17)
	maximum = 0.011 (at node 168)
Injected flit rate average = 0.0858698
	minimum = 0 (at node 40)
	maximum = 0.234 (at node 121)
Accepted flit rate average= 0.0855729
	minimum = 0 (at node 17)
	maximum = 0.198 (at node 168)
Injected packet length average = 17.9793
Accepted packet length average = 17.9759
Total in-flight flits = 631 (0 measured)
latency change    = 0.000701275
throughput change = 0.0752587
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 56.8753
	minimum = 23
	maximum = 173
Network latency average = 56.1349
	minimum = 23
	maximum = 173
Slowest packet = 3392
Flit latency average = 31.0649
	minimum = 6
	maximum = 156
Slowest flit = 61068
Fragmentation average = 15.8617
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00483854
	minimum = 0.001 (at node 0)
	maximum = 0.011 (at node 128)
Accepted packet rate average = 0.00485417
	minimum = 0 (at node 18)
	maximum = 0.013 (at node 22)
Injected flit rate average = 0.087026
	minimum = 0.018 (at node 0)
	maximum = 0.198 (at node 128)
Accepted flit rate average= 0.0871094
	minimum = 0 (at node 18)
	maximum = 0.234 (at node 22)
Injected packet length average = 17.986
Accepted packet length average = 17.9453
Total in-flight flits = 628 (628 measured)
latency change    = 0.0494962
throughput change = 0.0176383
Class 0:
Packet latency average = 55.49
	minimum = 23
	maximum = 173
Network latency average = 54.739
	minimum = 23
	maximum = 173
Slowest packet = 3392
Flit latency average = 30.2109
	minimum = 6
	maximum = 156
Slowest flit = 61068
Fragmentation average = 14.6271
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00467187
	minimum = 0.001 (at node 0)
	maximum = 0.01 (at node 128)
Accepted packet rate average = 0.0046901
	minimum = 0.001 (at node 24)
	maximum = 0.0085 (at node 22)
Injected flit rate average = 0.0840313
	minimum = 0.0095 (at node 0)
	maximum = 0.18 (at node 128)
Accepted flit rate average= 0.084237
	minimum = 0.018 (at node 24)
	maximum = 0.153 (at node 22)
Injected packet length average = 17.9866
Accepted packet length average = 17.9606
Total in-flight flits = 576 (576 measured)
latency change    = 0.0249645
throughput change = 0.034099
Class 0:
Packet latency average = 54.7105
	minimum = 23
	maximum = 173
Network latency average = 54.0335
	minimum = 23
	maximum = 173
Slowest packet = 3392
Flit latency average = 29.8411
	minimum = 6
	maximum = 156
Slowest flit = 61068
Fragmentation average = 13.9734
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00464583
	minimum = 0.000666667 (at node 0)
	maximum = 0.008 (at node 128)
Accepted packet rate average = 0.00465104
	minimum = 0.00133333 (at node 153)
	maximum = 0.00733333 (at node 97)
Injected flit rate average = 0.0837483
	minimum = 0.012 (at node 0)
	maximum = 0.144 (at node 128)
Accepted flit rate average= 0.0837413
	minimum = 0.024 (at node 153)
	maximum = 0.132 (at node 97)
Injected packet length average = 18.0265
Accepted packet length average = 18.0049
Total in-flight flits = 564 (564 measured)
latency change    = 0.0142472
throughput change = 0.00591894
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6180 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 55.031 (1 samples)
	minimum = 23 (1 samples)
	maximum = 198 (1 samples)
Network latency average = 54.3224 (1 samples)
	minimum = 23 (1 samples)
	maximum = 198 (1 samples)
Flit latency average = 30.0832 (1 samples)
	minimum = 6 (1 samples)
	maximum = 181 (1 samples)
Fragmentation average = 14.2421 (1 samples)
	minimum = 0 (1 samples)
	maximum = 145 (1 samples)
Injected packet rate average = 0.00464583 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.008 (1 samples)
Accepted packet rate average = 0.00465104 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.00733333 (1 samples)
Injected flit rate average = 0.0837483 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.144 (1 samples)
Accepted flit rate average = 0.0837413 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.132 (1 samples)
Injected packet size average = 18.0265 (1 samples)
Accepted packet size average = 18.0049 (1 samples)
Hops average = 5.09189 (1 samples)
Total run time 1.74707
