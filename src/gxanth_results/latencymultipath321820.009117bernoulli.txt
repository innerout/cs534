BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 60.9358
	minimum = 22
	maximum = 173
Network latency average = 59.3481
	minimum = 22
	maximum = 165
Slowest packet = 235
Flit latency average = 34.8297
	minimum = 5
	maximum = 148
Slowest flit = 24802
Fragmentation average = 14.3556
	minimum = 0
	maximum = 78
Injected packet rate average = 0.00905729
	minimum = 0.002 (at node 135)
	maximum = 0.02 (at node 10)
Accepted packet rate average = 0.0084375
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.161458
	minimum = 0.036 (at node 135)
	maximum = 0.343 (at node 10)
Accepted flit rate average= 0.154958
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 48)
Injected packet length average = 17.8263
Accepted packet length average = 18.3654
Total in-flight flits = 1550 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 64.0633
	minimum = 22
	maximum = 255
Network latency average = 62.5105
	minimum = 22
	maximum = 242
Slowest packet = 1682
Flit latency average = 37.6542
	minimum = 5
	maximum = 225
Slowest flit = 36593
Fragmentation average = 14.5957
	minimum = 0
	maximum = 138
Injected packet rate average = 0.00891927
	minimum = 0.0035 (at node 122)
	maximum = 0.017 (at node 10)
Accepted packet rate average = 0.00868229
	minimum = 0.005 (at node 30)
	maximum = 0.0135 (at node 22)
Injected flit rate average = 0.159792
	minimum = 0.063 (at node 122)
	maximum = 0.306 (at node 10)
Accepted flit rate average= 0.157443
	minimum = 0.09 (at node 30)
	maximum = 0.243 (at node 22)
Injected packet length average = 17.9153
Accepted packet length average = 18.1338
Total in-flight flits = 1192 (0 measured)
latency change    = 0.0488187
throughput change = 0.0157795
Class 0:
Packet latency average = 66.5658
	minimum = 22
	maximum = 252
Network latency average = 64.8691
	minimum = 22
	maximum = 241
Slowest packet = 3841
Flit latency average = 39.7987
	minimum = 5
	maximum = 224
Slowest flit = 69154
Fragmentation average = 15.2607
	minimum = 0
	maximum = 93
Injected packet rate average = 0.00898958
	minimum = 0.002 (at node 94)
	maximum = 0.018 (at node 52)
Accepted packet rate average = 0.00891146
	minimum = 0.001 (at node 121)
	maximum = 0.017 (at node 179)
Injected flit rate average = 0.162042
	minimum = 0.036 (at node 94)
	maximum = 0.308 (at node 52)
Accepted flit rate average= 0.16088
	minimum = 0.018 (at node 121)
	maximum = 0.305 (at node 179)
Injected packet length average = 18.0255
Accepted packet length average = 18.0532
Total in-flight flits = 1371 (0 measured)
latency change    = 0.0375939
throughput change = 0.0213668
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 64.1375
	minimum = 22
	maximum = 219
Network latency average = 62.7362
	minimum = 22
	maximum = 207
Slowest packet = 5959
Flit latency average = 38.2624
	minimum = 5
	maximum = 190
Slowest flit = 107279
Fragmentation average = 14.5088
	minimum = 0
	maximum = 80
Injected packet rate average = 0.00943229
	minimum = 0.003 (at node 131)
	maximum = 0.018 (at node 0)
Accepted packet rate average = 0.00941667
	minimum = 0.001 (at node 184)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.169318
	minimum = 0.054 (at node 131)
	maximum = 0.324 (at node 0)
Accepted flit rate average= 0.169135
	minimum = 0.018 (at node 184)
	maximum = 0.351 (at node 56)
Injected packet length average = 17.9509
Accepted packet length average = 17.9613
Total in-flight flits = 1495 (1495 measured)
latency change    = 0.0378603
throughput change = 0.0488083
Class 0:
Packet latency average = 62.1727
	minimum = 22
	maximum = 219
Network latency average = 60.7975
	minimum = 22
	maximum = 207
Slowest packet = 5959
Flit latency average = 36.3987
	minimum = 5
	maximum = 190
Slowest flit = 107279
Fragmentation average = 14.1375
	minimum = 0
	maximum = 95
Injected packet rate average = 0.00911979
	minimum = 0.0035 (at node 178)
	maximum = 0.015 (at node 149)
Accepted packet rate average = 0.00917448
	minimum = 0.0045 (at node 190)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.164026
	minimum = 0.063 (at node 178)
	maximum = 0.27 (at node 149)
Accepted flit rate average= 0.164878
	minimum = 0.081 (at node 190)
	maximum = 0.333 (at node 16)
Injected packet length average = 17.9857
Accepted packet length average = 17.9713
Total in-flight flits = 1094 (1094 measured)
latency change    = 0.0316026
throughput change = 0.0258241
Class 0:
Packet latency average = 61.9225
	minimum = 22
	maximum = 219
Network latency average = 60.4484
	minimum = 22
	maximum = 207
Slowest packet = 5959
Flit latency average = 36.0169
	minimum = 5
	maximum = 190
Slowest flit = 107279
Fragmentation average = 14.1779
	minimum = 0
	maximum = 95
Injected packet rate average = 0.00910764
	minimum = 0.00466667 (at node 153)
	maximum = 0.0136667 (at node 38)
Accepted packet rate average = 0.00910417
	minimum = 0.00566667 (at node 134)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.164038
	minimum = 0.084 (at node 153)
	maximum = 0.247333 (at node 106)
Accepted flit rate average= 0.163946
	minimum = 0.102 (at node 134)
	maximum = 0.299333 (at node 16)
Injected packet length average = 18.0111
Accepted packet length average = 18.0078
Total in-flight flits = 1366 (1366 measured)
latency change    = 0.00403937
throughput change = 0.00568128
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6189 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 62.216 (1 samples)
	minimum = 22 (1 samples)
	maximum = 219 (1 samples)
Network latency average = 60.7164 (1 samples)
	minimum = 22 (1 samples)
	maximum = 207 (1 samples)
Flit latency average = 36.2175 (1 samples)
	minimum = 5 (1 samples)
	maximum = 190 (1 samples)
Fragmentation average = 14.278 (1 samples)
	minimum = 0 (1 samples)
	maximum = 95 (1 samples)
Injected packet rate average = 0.00910764 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0136667 (1 samples)
Accepted packet rate average = 0.00910417 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.164038 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.247333 (1 samples)
Accepted flit rate average = 0.163946 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.299333 (1 samples)
Injected packet size average = 18.0111 (1 samples)
Accepted packet size average = 18.0078 (1 samples)
Hops average = 5.0577 (1 samples)
Total run time 3.00368
