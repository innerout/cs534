BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 238.991
	minimum = 23
	maximum = 905
Network latency average = 234.922
	minimum = 23
	maximum = 905
Slowest packet = 76
Flit latency average = 156.752
	minimum = 6
	maximum = 931
Slowest flit = 2950
Fragmentation average = 151.345
	minimum = 0
	maximum = 847
Injected packet rate average = 0.0176823
	minimum = 0.006 (at node 15)
	maximum = 0.027 (at node 51)
Accepted packet rate average = 0.00922917
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.315115
	minimum = 0.108 (at node 15)
	maximum = 0.486 (at node 51)
Accepted flit rate average= 0.195156
	minimum = 0.063 (at node 174)
	maximum = 0.391 (at node 44)
Injected packet length average = 17.8209
Accepted packet length average = 21.1456
Total in-flight flits = 23658 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 377.101
	minimum = 23
	maximum = 1810
Network latency average = 367.455
	minimum = 23
	maximum = 1798
Slowest packet = 363
Flit latency average = 266.641
	minimum = 6
	maximum = 1880
Slowest flit = 5946
Fragmentation average = 206.991
	minimum = 0
	maximum = 1622
Injected packet rate average = 0.0168802
	minimum = 0.006 (at node 40)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.010599
	minimum = 0.0045 (at node 178)
	maximum = 0.0155 (at node 67)
Injected flit rate average = 0.301789
	minimum = 0.1045 (at node 40)
	maximum = 0.459 (at node 135)
Accepted flit rate average= 0.207773
	minimum = 0.098 (at node 178)
	maximum = 0.3125 (at node 177)
Injected packet length average = 17.8783
Accepted packet length average = 19.6032
Total in-flight flits = 36981 (0 measured)
latency change    = 0.366242
throughput change = 0.0607257
Class 0:
Packet latency average = 712.531
	minimum = 25
	maximum = 2776
Network latency average = 658.666
	minimum = 25
	maximum = 2776
Slowest packet = 760
Flit latency average = 545.974
	minimum = 6
	maximum = 2807
Slowest flit = 10102
Fragmentation average = 268.559
	minimum = 1
	maximum = 2131
Injected packet rate average = 0.0139115
	minimum = 0 (at node 52)
	maximum = 0.03 (at node 131)
Accepted packet rate average = 0.0122917
	minimum = 0.004 (at node 60)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.248625
	minimum = 0 (at node 60)
	maximum = 0.542 (at node 131)
Accepted flit rate average= 0.220589
	minimum = 0.099 (at node 113)
	maximum = 0.403 (at node 16)
Injected packet length average = 17.872
Accepted packet length average = 17.9462
Total in-flight flits = 42724 (0 measured)
latency change    = 0.470758
throughput change = 0.0580951
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 565.07
	minimum = 27
	maximum = 2240
Network latency average = 308.673
	minimum = 27
	maximum = 953
Slowest packet = 9194
Flit latency average = 714.732
	minimum = 6
	maximum = 3756
Slowest flit = 14817
Fragmentation average = 157.491
	minimum = 2
	maximum = 815
Injected packet rate average = 0.0123073
	minimum = 0 (at node 4)
	maximum = 0.038 (at node 169)
Accepted packet rate average = 0.0121198
	minimum = 0.005 (at node 1)
	maximum = 0.023 (at node 81)
Injected flit rate average = 0.221599
	minimum = 0 (at node 4)
	maximum = 0.685 (at node 169)
Accepted flit rate average= 0.221005
	minimum = 0.095 (at node 1)
	maximum = 0.429 (at node 99)
Injected packet length average = 18.0055
Accepted packet length average = 18.2351
Total in-flight flits = 42933 (22798 measured)
latency change    = 0.26096
throughput change = 0.00188533
Class 0:
Packet latency average = 804.307
	minimum = 26
	maximum = 3367
Network latency average = 458.085
	minimum = 26
	maximum = 1952
Slowest packet = 9194
Flit latency average = 773.556
	minimum = 6
	maximum = 4851
Slowest flit = 7553
Fragmentation average = 191.413
	minimum = 1
	maximum = 1487
Injected packet rate average = 0.0126693
	minimum = 0 (at node 4)
	maximum = 0.0275 (at node 169)
Accepted packet rate average = 0.0120208
	minimum = 0.006 (at node 4)
	maximum = 0.02 (at node 107)
Injected flit rate average = 0.227992
	minimum = 0 (at node 49)
	maximum = 0.4955 (at node 169)
Accepted flit rate average= 0.217221
	minimum = 0.1185 (at node 4)
	maximum = 0.3455 (at node 99)
Injected packet length average = 17.9957
Accepted packet length average = 18.0704
Total in-flight flits = 47007 (35329 measured)
latency change    = 0.297444
throughput change = 0.0174193
Class 0:
Packet latency average = 1045.26
	minimum = 25
	maximum = 4204
Network latency average = 590.643
	minimum = 24
	maximum = 2918
Slowest packet = 9194
Flit latency average = 831.487
	minimum = 6
	maximum = 5727
Slowest flit = 13085
Fragmentation average = 211.443
	minimum = 1
	maximum = 1917
Injected packet rate average = 0.0126076
	minimum = 0 (at node 4)
	maximum = 0.0256667 (at node 169)
Accepted packet rate average = 0.0119375
	minimum = 0.008 (at node 135)
	maximum = 0.019 (at node 129)
Injected flit rate average = 0.226575
	minimum = 0 (at node 49)
	maximum = 0.460667 (at node 169)
Accepted flit rate average= 0.215231
	minimum = 0.137333 (at node 135)
	maximum = 0.325667 (at node 129)
Injected packet length average = 17.9712
Accepted packet length average = 18.0298
Total in-flight flits = 49593 (42229 measured)
latency change    = 0.230517
throughput change = 0.00924798
Class 0:
Packet latency average = 1259.65
	minimum = 25
	maximum = 5093
Network latency average = 693.361
	minimum = 24
	maximum = 3971
Slowest packet = 9194
Flit latency average = 868.838
	minimum = 6
	maximum = 6519
Slowest flit = 28926
Fragmentation average = 226.089
	minimum = 1
	maximum = 3333
Injected packet rate average = 0.0121263
	minimum = 0 (at node 48)
	maximum = 0.0225 (at node 94)
Accepted packet rate average = 0.0118464
	minimum = 0.0075 (at node 135)
	maximum = 0.01675 (at node 128)
Injected flit rate average = 0.217977
	minimum = 0 (at node 49)
	maximum = 0.406 (at node 94)
Accepted flit rate average= 0.21328
	minimum = 0.13075 (at node 135)
	maximum = 0.3 (at node 128)
Injected packet length average = 17.9755
Accepted packet length average = 18.0038
Total in-flight flits = 46685 (41506 measured)
latency change    = 0.170203
throughput change = 0.00914739
Class 0:
Packet latency average = 1463.2
	minimum = 25
	maximum = 5506
Network latency average = 765.683
	minimum = 24
	maximum = 4775
Slowest packet = 9194
Flit latency average = 895.718
	minimum = 6
	maximum = 7402
Slowest flit = 35615
Fragmentation average = 236.53
	minimum = 1
	maximum = 3409
Injected packet rate average = 0.0121344
	minimum = 0 (at node 49)
	maximum = 0.0226 (at node 94)
Accepted packet rate average = 0.0118125
	minimum = 0.0086 (at node 135)
	maximum = 0.016 (at node 128)
Injected flit rate average = 0.218159
	minimum = 0 (at node 112)
	maximum = 0.4074 (at node 94)
Accepted flit rate average= 0.212936
	minimum = 0.1544 (at node 135)
	maximum = 0.2848 (at node 128)
Injected packet length average = 17.9786
Accepted packet length average = 18.0264
Total in-flight flits = 48221 (44505 measured)
latency change    = 0.139111
throughput change = 0.00161311
Class 0:
Packet latency average = 1646.01
	minimum = 25
	maximum = 7070
Network latency average = 824.886
	minimum = 24
	maximum = 5890
Slowest packet = 9194
Flit latency average = 919.735
	minimum = 6
	maximum = 8651
Slowest flit = 15749
Fragmentation average = 245.949
	minimum = 1
	maximum = 4345
Injected packet rate average = 0.0119549
	minimum = 0 (at node 112)
	maximum = 0.0201667 (at node 169)
Accepted packet rate average = 0.0117734
	minimum = 0.009 (at node 42)
	maximum = 0.0155 (at node 129)
Injected flit rate average = 0.214972
	minimum = 0 (at node 112)
	maximum = 0.361833 (at node 169)
Accepted flit rate average= 0.211968
	minimum = 0.161333 (at node 104)
	maximum = 0.274 (at node 165)
Injected packet length average = 17.982
Accepted packet length average = 18.0039
Total in-flight flits = 46595 (43826 measured)
latency change    = 0.111064
throughput change = 0.00456945
Class 0:
Packet latency average = 1842.49
	minimum = 25
	maximum = 7490
Network latency average = 871.061
	minimum = 24
	maximum = 6515
Slowest packet = 9194
Flit latency average = 942.143
	minimum = 6
	maximum = 9671
Slowest flit = 18404
Fragmentation average = 252.733
	minimum = 1
	maximum = 4442
Injected packet rate average = 0.0120476
	minimum = 0 (at node 112)
	maximum = 0.02 (at node 98)
Accepted packet rate average = 0.0117254
	minimum = 0.00942857 (at node 42)
	maximum = 0.015 (at node 129)
Injected flit rate average = 0.216595
	minimum = 0 (at node 112)
	maximum = 0.359571 (at node 98)
Accepted flit rate average= 0.211112
	minimum = 0.166571 (at node 42)
	maximum = 0.269571 (at node 103)
Injected packet length average = 17.9783
Accepted packet length average = 18.0046
Total in-flight flits = 50536 (48495 measured)
latency change    = 0.106636
throughput change = 0.00405603
Draining all recorded packets ...
Class 0:
Remaining flits: 10512 10513 10514 10515 10516 10517 10518 10519 10520 10521 [...] (48577 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (42156 flits)
Class 0:
Remaining flits: 10512 10513 10514 10515 10516 10517 10518 10519 10520 10521 [...] (47017 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (36496 flits)
Class 0:
Remaining flits: 16044 16045 16046 16047 16048 16049 16050 16051 16052 16053 [...] (48648 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (33635 flits)
Class 0:
Remaining flits: 20662 20663 28783 28784 28785 28786 28787 28788 28789 28790 [...] (50968 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (31653 flits)
Class 0:
Remaining flits: 20663 35161 35162 35163 35164 35165 35166 35167 35168 35169 [...] (54277 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (27705 flits)
Class 0:
Remaining flits: 20663 35161 35162 35163 35164 35165 35166 35167 35168 35169 [...] (53104 flits)
Measured flits: 166680 166681 166682 166683 166684 166685 166686 166687 166688 166689 [...] (22135 flits)
Class 0:
Remaining flits: 20663 35163 35164 35165 35166 35167 35168 35169 35170 35171 [...] (53029 flits)
Measured flits: 166697 167240 167241 167242 167243 167244 167245 167246 167247 167248 [...] (16825 flits)
Class 0:
Remaining flits: 44822 44823 44824 44825 44826 44827 44828 44829 44830 44831 [...] (55614 flits)
Measured flits: 166697 167240 167241 167242 167243 167244 167245 167246 167247 167248 [...] (14519 flits)
Class 0:
Remaining flits: 44822 44823 44824 44825 44826 44827 44828 44829 44830 44831 [...] (55953 flits)
Measured flits: 166697 167240 167241 167242 167243 167244 167245 167246 167247 167248 [...] (11362 flits)
Class 0:
Remaining flits: 44822 44823 44824 44825 44826 44827 44828 44829 44830 44831 [...] (58623 flits)
Measured flits: 167240 167241 167242 167243 167244 167245 167246 167247 167248 167249 [...] (10220 flits)
Class 0:
Remaining flits: 44822 44823 44824 44825 44826 44827 44828 44829 44830 44831 [...] (54649 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (7810 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (57605 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (6980 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (59172 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (5735 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (59278 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (4663 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (59618 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (4221 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (59510 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (3480 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (59363 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (3085 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (56863 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (2822 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (58434 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (2290 flits)
Class 0:
Remaining flits: 95886 95887 95888 95889 95890 95891 95892 95893 95894 95895 [...] (57238 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (1752 flits)
Class 0:
Remaining flits: 164310 164311 164312 164313 164314 164315 164316 164317 164318 164319 [...] (57171 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (1243 flits)
Class 0:
Remaining flits: 164320 164321 190170 190171 190172 190173 190174 190175 190176 190177 [...] (58811 flits)
Measured flits: 190170 190171 190172 190173 190174 190175 190176 190177 190178 190179 [...] (1055 flits)
Class 0:
Remaining flits: 395892 395893 395894 395895 395896 395897 395898 395899 395900 395901 [...] (59416 flits)
Measured flits: 395892 395893 395894 395895 395896 395897 395898 395899 395900 395901 [...] (711 flits)
Class 0:
Remaining flits: 407728 407729 407730 407731 407732 407733 407734 407735 545235 545236 [...] (61022 flits)
Measured flits: 407728 407729 407730 407731 407732 407733 407734 407735 573696 573697 [...] (546 flits)
Class 0:
Remaining flits: 407728 407729 407730 407731 407732 407733 407734 407735 651742 651743 [...] (61150 flits)
Measured flits: 407728 407729 407730 407731 407732 407733 407734 407735 776952 776953 [...] (512 flits)
Class 0:
Remaining flits: 407728 407729 407730 407731 407732 407733 407734 407735 683064 683065 [...] (60934 flits)
Measured flits: 407728 407729 407730 407731 407732 407733 407734 407735 776952 776953 [...] (512 flits)
Class 0:
Remaining flits: 407728 407729 407730 407731 407732 407733 407734 407735 683064 683065 [...] (59870 flits)
Measured flits: 407728 407729 407730 407731 407732 407733 407734 407735 776952 776953 [...] (476 flits)
Class 0:
Remaining flits: 683064 683065 683066 683067 683068 683069 683070 683071 683072 683073 [...] (60129 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (347 flits)
Class 0:
Remaining flits: 709362 709363 709364 709365 709366 709367 709368 709369 709370 709371 [...] (60559 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (313 flits)
Class 0:
Remaining flits: 709362 709363 709364 709365 709366 709367 709368 709369 709370 709371 [...] (58810 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (162 flits)
Class 0:
Remaining flits: 709363 709364 709365 709366 709367 709368 709369 709370 709371 709372 [...] (58783 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (131 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (61499 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (126 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (61329 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (126 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (59077 flits)
Measured flits: 785826 785827 785828 785829 785830 785831 785832 785833 785834 785835 [...] (283 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (60768 flits)
Measured flits: 785827 785828 785829 785830 785831 785832 785833 785834 785835 785836 [...] (208 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (63634 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (104 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (63309 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (54 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (60962 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (54 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (61655 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (36 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (62802 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (36 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (61351 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (18 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (59928 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (18 flits)
Class 0:
Remaining flits: 719820 719821 719822 719823 719824 719825 719826 719827 719828 719829 [...] (60053 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (18 flits)
Class 0:
Remaining flits: 736452 736453 736454 736455 736456 736457 736458 736459 736460 736461 [...] (60457 flits)
Measured flits: 1000674 1000675 1000676 1000677 1000678 1000679 1000680 1000681 1000682 1000683 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 808020 808021 808022 808023 808024 808025 808026 808027 808028 808029 [...] (37730 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 808025 808026 808027 808028 808029 808030 808031 808032 808033 808034 [...] (26950 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1144404 1144405 1144406 1144407 1144408 1144409 1144410 1144411 1144412 1144413 [...] (16859 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1377324 1377325 1377326 1377327 1377328 1377329 1377330 1377331 1377332 1377333 [...] (8860 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1571904 1571905 1571906 1571907 1571908 1571909 1571910 1571911 1571912 1571913 [...] (2203 flits)
Measured flits: (0 flits)
Time taken is 60301 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4480.5 (1 samples)
	minimum = 25 (1 samples)
	maximum = 45590 (1 samples)
Network latency average = 1413.43 (1 samples)
	minimum = 24 (1 samples)
	maximum = 30410 (1 samples)
Flit latency average = 1577.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 38298 (1 samples)
Fragmentation average = 307.095 (1 samples)
	minimum = 0 (1 samples)
	maximum = 18888 (1 samples)
Injected packet rate average = 0.0120476 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.02 (1 samples)
Accepted packet rate average = 0.0117254 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.216595 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.359571 (1 samples)
Accepted flit rate average = 0.211112 (1 samples)
	minimum = 0.166571 (1 samples)
	maximum = 0.269571 (1 samples)
Injected packet size average = 17.9783 (1 samples)
Accepted packet size average = 18.0046 (1 samples)
Hops average = 5.06805 (1 samples)
Total run time 89.2835
