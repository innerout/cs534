BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 219.058
	minimum = 22
	maximum = 671
Network latency average = 213.75
	minimum = 22
	maximum = 671
Slowest packet = 1345
Flit latency average = 183.149
	minimum = 5
	maximum = 654
Slowest flit = 24227
Fragmentation average = 36.9191
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.0136458
	minimum = 0.006 (at node 41)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.253083
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.5466
Total in-flight flits = 28754 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 386.409
	minimum = 22
	maximum = 1231
Network latency average = 380.886
	minimum = 22
	maximum = 1198
Slowest packet = 3296
Flit latency average = 349.268
	minimum = 5
	maximum = 1229
Slowest flit = 58027
Fragmentation average = 38.072
	minimum = 0
	maximum = 332
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143854
	minimum = 0.008 (at node 116)
	maximum = 0.0205 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.262242
	minimum = 0.144 (at node 116)
	maximum = 0.369 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.2297
Total in-flight flits = 53055 (0 measured)
latency change    = 0.433094
throughput change = 0.0349252
Class 0:
Packet latency average = 844.545
	minimum = 22
	maximum = 1552
Network latency average = 838.305
	minimum = 22
	maximum = 1534
Slowest packet = 6144
Flit latency average = 809.521
	minimum = 5
	maximum = 1517
Slowest flit = 110609
Fragmentation average = 37.9594
	minimum = 0
	maximum = 278
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0151354
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.273536
	minimum = 0.126 (at node 61)
	maximum = 0.538 (at node 16)
Injected packet length average = 18.0068
Accepted packet length average = 18.0726
Total in-flight flits = 77450 (0 measured)
latency change    = 0.542464
throughput change = 0.0412898
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 40.7309
	minimum = 22
	maximum = 109
Network latency average = 35.4221
	minimum = 22
	maximum = 76
Slowest packet = 13024
Flit latency average = 1103.28
	minimum = 5
	maximum = 1802
Slowest flit = 159406
Fragmentation average = 6.68272
	minimum = 0
	maximum = 40
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0153385
	minimum = 0.006 (at node 105)
	maximum = 0.025 (at node 59)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.276688
	minimum = 0.115 (at node 1)
	maximum = 0.448 (at node 0)
Injected packet length average = 17.9469
Accepted packet length average = 18.0387
Total in-flight flits = 103670 (72929 measured)
latency change    = 19.7348
throughput change = 0.0113884
Class 0:
Packet latency average = 902.632
	minimum = 22
	maximum = 2022
Network latency average = 897.179
	minimum = 22
	maximum = 1981
Slowest packet = 12837
Flit latency average = 1256.94
	minimum = 5
	maximum = 2206
Slowest flit = 212885
Fragmentation average = 26.1842
	minimum = 0
	maximum = 181
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.0153906
	minimum = 0.0095 (at node 36)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.276854
	minimum = 0.1635 (at node 86)
	maximum = 0.391 (at node 129)
Injected packet length average = 17.9983
Accepted packet length average = 17.9885
Total in-flight flits = 126532 (126253 measured)
latency change    = 0.954875
throughput change = 0.000602002
Class 0:
Packet latency average = 1446.57
	minimum = 22
	maximum = 2649
Network latency average = 1440.91
	minimum = 22
	maximum = 2635
Slowest packet = 13805
Flit latency average = 1405.74
	minimum = 5
	maximum = 2678
Slowest flit = 255816
Fragmentation average = 37.1275
	minimum = 0
	maximum = 294
Injected packet rate average = 0.0225382
	minimum = 0.0146667 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0154149
	minimum = 0.0106667 (at node 36)
	maximum = 0.021 (at node 138)
Injected flit rate average = 0.405694
	minimum = 0.264 (at node 171)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.277694
	minimum = 0.177 (at node 36)
	maximum = 0.373667 (at node 103)
Injected packet length average = 18.0003
Accepted packet length average = 18.0146
Total in-flight flits = 151174 (151174 measured)
latency change    = 0.376018
throughput change = 0.00302591
Draining remaining packets ...
Class 0:
Remaining flits: 296898 296899 296900 296901 296902 296903 296904 296905 296906 296907 [...] (103914 flits)
Measured flits: 296898 296899 296900 296901 296902 296903 296904 296905 296906 296907 [...] (103914 flits)
Class 0:
Remaining flits: 338565 338566 338567 338568 338569 338570 338571 338572 338573 338574 [...] (57712 flits)
Measured flits: 338565 338566 338567 338568 338569 338570 338571 338572 338573 338574 [...] (57712 flits)
Class 0:
Remaining flits: 403156 403157 403158 403159 403160 403161 403162 403163 404020 404021 [...] (12943 flits)
Measured flits: 403156 403157 403158 403159 403160 403161 403162 403163 404020 404021 [...] (12943 flits)
Time taken is 9761 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2282.06 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4047 (1 samples)
Network latency average = 2276.32 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3982 (1 samples)
Flit latency average = 2027.13 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3965 (1 samples)
Fragmentation average = 43.9348 (1 samples)
	minimum = 0 (1 samples)
	maximum = 555 (1 samples)
Injected packet rate average = 0.0225382 (1 samples)
	minimum = 0.0146667 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0154149 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.405694 (1 samples)
	minimum = 0.264 (1 samples)
	maximum = 0.541333 (1 samples)
Accepted flit rate average = 0.277694 (1 samples)
	minimum = 0.177 (1 samples)
	maximum = 0.373667 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0146 (1 samples)
Hops average = 5.08088 (1 samples)
Total run time 8.97662
