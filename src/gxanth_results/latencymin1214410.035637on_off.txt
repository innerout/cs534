BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.389
	minimum = 27
	maximum = 981
Network latency average = 283.992
	minimum = 23
	maximum = 849
Slowest packet = 21
Flit latency average = 248.995
	minimum = 6
	maximum = 867
Slowest flit = 11742
Fragmentation average = 55.5765
	minimum = 0
	maximum = 156
Injected packet rate average = 0.02775
	minimum = 0 (at node 35)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.00983854
	minimum = 0.003 (at node 41)
	maximum = 0.016 (at node 20)
Injected flit rate average = 0.493891
	minimum = 0 (at node 35)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.185073
	minimum = 0.074 (at node 41)
	maximum = 0.298 (at node 20)
Injected packet length average = 17.7979
Accepted packet length average = 18.811
Total in-flight flits = 60478 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 670.319
	minimum = 27
	maximum = 1974
Network latency average = 539.381
	minimum = 23
	maximum = 1783
Slowest packet = 21
Flit latency average = 501.671
	minimum = 6
	maximum = 1829
Slowest flit = 14050
Fragmentation average = 62.4297
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0251458
	minimum = 0.0015 (at node 141)
	maximum = 0.0405 (at node 177)
Accepted packet rate average = 0.00993229
	minimum = 0.006 (at node 30)
	maximum = 0.0165 (at node 103)
Injected flit rate average = 0.449323
	minimum = 0.027 (at node 141)
	maximum = 0.729 (at node 177)
Accepted flit rate average= 0.182448
	minimum = 0.108 (at node 30)
	maximum = 0.297 (at node 103)
Injected packet length average = 17.8687
Accepted packet length average = 18.3692
Total in-flight flits = 104216 (0 measured)
latency change    = 0.44446
throughput change = 0.0143877
Class 0:
Packet latency average = 1649.85
	minimum = 30
	maximum = 2943
Network latency average = 1394.91
	minimum = 23
	maximum = 2613
Slowest packet = 2836
Flit latency average = 1363.2
	minimum = 6
	maximum = 2736
Slowest flit = 23584
Fragmentation average = 68.5752
	minimum = 0
	maximum = 157
Injected packet rate average = 0.0144375
	minimum = 0 (at node 0)
	maximum = 0.049 (at node 130)
Accepted packet rate average = 0.00958854
	minimum = 0.002 (at node 191)
	maximum = 0.016 (at node 151)
Injected flit rate average = 0.263094
	minimum = 0 (at node 0)
	maximum = 0.874 (at node 130)
Accepted flit rate average= 0.172432
	minimum = 0.036 (at node 191)
	maximum = 0.289 (at node 34)
Injected packet length average = 18.2229
Accepted packet length average = 17.9832
Total in-flight flits = 122247 (0 measured)
latency change    = 0.593709
throughput change = 0.0580844
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1200.28
	minimum = 74
	maximum = 3746
Network latency average = 208.073
	minimum = 23
	maximum = 872
Slowest packet = 12532
Flit latency average = 1976.55
	minimum = 6
	maximum = 3584
Slowest flit = 37043
Fragmentation average = 28.3303
	minimum = 0
	maximum = 134
Injected packet rate average = 0.01025
	minimum = 0 (at node 85)
	maximum = 0.048 (at node 178)
Accepted packet rate average = 0.00935417
	minimum = 0.002 (at node 38)
	maximum = 0.019 (at node 34)
Injected flit rate average = 0.185078
	minimum = 0 (at node 85)
	maximum = 0.852 (at node 178)
Accepted flit rate average= 0.169625
	minimum = 0.039 (at node 38)
	maximum = 0.33 (at node 34)
Injected packet length average = 18.0564
Accepted packet length average = 18.1336
Total in-flight flits = 125535 (33778 measured)
latency change    = 0.374558
throughput change = 0.01655
Class 0:
Packet latency average = 1789.72
	minimum = 46
	maximum = 4560
Network latency average = 609.291
	minimum = 23
	maximum = 1904
Slowest packet = 12532
Flit latency average = 2254.66
	minimum = 6
	maximum = 4413
Slowest flit = 52606
Fragmentation average = 46.651
	minimum = 0
	maximum = 163
Injected packet rate average = 0.00955208
	minimum = 0 (at node 122)
	maximum = 0.0305 (at node 178)
Accepted packet rate average = 0.00927604
	minimum = 0.0045 (at node 36)
	maximum = 0.0145 (at node 34)
Injected flit rate average = 0.171974
	minimum = 0 (at node 122)
	maximum = 0.549 (at node 178)
Accepted flit rate average= 0.167818
	minimum = 0.0785 (at node 36)
	maximum = 0.2515 (at node 34)
Injected packet length average = 18.0038
Accepted packet length average = 18.0915
Total in-flight flits = 124153 (59552 measured)
latency change    = 0.329349
throughput change = 0.0107694
Class 0:
Packet latency average = 2557.69
	minimum = 46
	maximum = 5755
Network latency average = 1107.37
	minimum = 23
	maximum = 2894
Slowest packet = 12532
Flit latency average = 2500.66
	minimum = 6
	maximum = 5369
Slowest flit = 53153
Fragmentation average = 56.1882
	minimum = 0
	maximum = 165
Injected packet rate average = 0.00939062
	minimum = 0.000666667 (at node 123)
	maximum = 0.0253333 (at node 178)
Accepted packet rate average = 0.00923611
	minimum = 0.00533333 (at node 4)
	maximum = 0.013 (at node 34)
Injected flit rate average = 0.169061
	minimum = 0.00633333 (at node 123)
	maximum = 0.452 (at node 178)
Accepted flit rate average= 0.166477
	minimum = 0.096 (at node 4)
	maximum = 0.231667 (at node 34)
Injected packet length average = 18.0031
Accepted packet length average = 18.0246
Total in-flight flits = 124204 (82172 measured)
latency change    = 0.300261
throughput change = 0.00805081
Class 0:
Packet latency average = 3219.95
	minimum = 46
	maximum = 6851
Network latency average = 1599.73
	minimum = 23
	maximum = 3938
Slowest packet = 12532
Flit latency average = 2710.03
	minimum = 6
	maximum = 6006
Slowest flit = 89676
Fragmentation average = 60.4212
	minimum = 0
	maximum = 165
Injected packet rate average = 0.00933594
	minimum = 0.0005 (at node 123)
	maximum = 0.0245 (at node 178)
Accepted packet rate average = 0.00917578
	minimum = 0.0055 (at node 4)
	maximum = 0.01325 (at node 76)
Injected flit rate average = 0.168134
	minimum = 0.009 (at node 123)
	maximum = 0.4385 (at node 178)
Accepted flit rate average= 0.165577
	minimum = 0.099 (at node 4)
	maximum = 0.23975 (at node 76)
Injected packet length average = 18.0093
Accepted packet length average = 18.045
Total in-flight flits = 124504 (99479 measured)
latency change    = 0.205672
throughput change = 0.00543921
Class 0:
Packet latency average = 3901.41
	minimum = 46
	maximum = 7857
Network latency average = 2034.95
	minimum = 23
	maximum = 4979
Slowest packet = 12532
Flit latency average = 2893.6
	minimum = 6
	maximum = 7139
Slowest flit = 78083
Fragmentation average = 64.3801
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0092875
	minimum = 0.0026 (at node 138)
	maximum = 0.0218 (at node 178)
Accepted packet rate average = 0.00917396
	minimum = 0.0058 (at node 86)
	maximum = 0.0136 (at node 16)
Injected flit rate average = 0.167366
	minimum = 0.0468 (at node 138)
	maximum = 0.3924 (at node 178)
Accepted flit rate average= 0.16511
	minimum = 0.1054 (at node 86)
	maximum = 0.2438 (at node 16)
Injected packet length average = 18.0205
Accepted packet length average = 17.9977
Total in-flight flits = 124913 (111686 measured)
latency change    = 0.174671
throughput change = 0.00282481
Class 0:
Packet latency average = 4534.99
	minimum = 46
	maximum = 8361
Network latency average = 2444.61
	minimum = 23
	maximum = 5773
Slowest packet = 12532
Flit latency average = 3049.76
	minimum = 6
	maximum = 8012
Slowest flit = 93434
Fragmentation average = 66.1556
	minimum = 0
	maximum = 176
Injected packet rate average = 0.00917187
	minimum = 0.00216667 (at node 138)
	maximum = 0.0185 (at node 178)
Accepted packet rate average = 0.00915799
	minimum = 0.00666667 (at node 64)
	maximum = 0.0128333 (at node 16)
Injected flit rate average = 0.165112
	minimum = 0.039 (at node 138)
	maximum = 0.331833 (at node 178)
Accepted flit rate average= 0.164946
	minimum = 0.122167 (at node 64)
	maximum = 0.230167 (at node 16)
Injected packet length average = 18.002
Accepted packet length average = 18.0112
Total in-flight flits = 122957 (116546 measured)
latency change    = 0.139709
throughput change = 0.000995695
Class 0:
Packet latency average = 5110.51
	minimum = 46
	maximum = 9377
Network latency average = 2745.63
	minimum = 23
	maximum = 6820
Slowest packet = 12532
Flit latency average = 3166.89
	minimum = 6
	maximum = 9151
Slowest flit = 72449
Fragmentation average = 67.2125
	minimum = 0
	maximum = 176
Injected packet rate average = 0.00909152
	minimum = 0.00257143 (at node 182)
	maximum = 0.0167143 (at node 153)
Accepted packet rate average = 0.0091317
	minimum = 0.00628571 (at node 89)
	maximum = 0.012 (at node 16)
Injected flit rate average = 0.163751
	minimum = 0.0458571 (at node 182)
	maximum = 0.301286 (at node 153)
Accepted flit rate average= 0.164491
	minimum = 0.114571 (at node 89)
	maximum = 0.215286 (at node 16)
Injected packet length average = 18.0115
Accepted packet length average = 18.0132
Total in-flight flits = 121725 (118602 measured)
latency change    = 0.112616
throughput change = 0.00276677
Draining all recorded packets ...
Class 0:
Remaining flits: 138726 138727 138728 138729 138730 138731 138732 138733 138734 138735 [...] (122624 flits)
Measured flits: 225810 225811 225812 225813 225814 225815 225816 225817 225818 225819 [...] (121217 flits)
Class 0:
Remaining flits: 138726 138727 138728 138729 138730 138731 138732 138733 138734 138735 [...] (121881 flits)
Measured flits: 225810 225811 225812 225813 225814 225815 225816 225817 225818 225819 [...] (121197 flits)
Class 0:
Remaining flits: 169884 169885 169886 169887 169888 169889 169890 169891 169892 169893 [...] (121062 flits)
Measured flits: 225810 225811 225812 225813 225814 225815 225816 225817 225818 225819 [...] (120921 flits)
Class 0:
Remaining flits: 185148 185149 185150 185151 185152 185153 185154 185155 185156 185157 [...] (120983 flits)
Measured flits: 225810 225811 225812 225813 225814 225815 225816 225817 225818 225819 [...] (120965 flits)
Class 0:
Remaining flits: 243558 243559 243560 243561 243562 243563 243564 243565 243566 243567 [...] (122823 flits)
Measured flits: 243558 243559 243560 243561 243562 243563 243564 243565 243566 243567 [...] (122661 flits)
Class 0:
Remaining flits: 288468 288469 288470 288471 288472 288473 288474 288475 288476 288477 [...] (122429 flits)
Measured flits: 288468 288469 288470 288471 288472 288473 288474 288475 288476 288477 [...] (122123 flits)
Class 0:
Remaining flits: 356004 356005 356006 356007 356008 356009 356010 356011 356012 356013 [...] (120742 flits)
Measured flits: 356004 356005 356006 356007 356008 356009 356010 356011 356012 356013 [...] (120148 flits)
Class 0:
Remaining flits: 366336 366337 366338 366339 366340 366341 366342 366343 366344 366345 [...] (122575 flits)
Measured flits: 366336 366337 366338 366339 366340 366341 366342 366343 366344 366345 [...] (121192 flits)
Class 0:
Remaining flits: 389808 389809 389810 389811 389812 389813 389814 389815 389816 389817 [...] (122278 flits)
Measured flits: 389808 389809 389810 389811 389812 389813 389814 389815 389816 389817 [...] (120530 flits)
Class 0:
Remaining flits: 434196 434197 434198 434199 434200 434201 434202 434203 434204 434205 [...] (122458 flits)
Measured flits: 434196 434197 434198 434199 434200 434201 434202 434203 434204 434205 [...] (120630 flits)
Class 0:
Remaining flits: 451170 451171 451172 451173 451174 451175 451176 451177 451178 451179 [...] (122760 flits)
Measured flits: 451170 451171 451172 451173 451174 451175 451176 451177 451178 451179 [...] (120780 flits)
Class 0:
Remaining flits: 451170 451171 451172 451173 451174 451175 451176 451177 451178 451179 [...] (121677 flits)
Measured flits: 451170 451171 451172 451173 451174 451175 451176 451177 451178 451179 [...] (118324 flits)
Class 0:
Remaining flits: 541764 541765 541766 541767 541768 541769 541770 541771 541772 541773 [...] (122011 flits)
Measured flits: 541764 541765 541766 541767 541768 541769 541770 541771 541772 541773 [...] (116649 flits)
Class 0:
Remaining flits: 544104 544105 544106 544107 544108 544109 544110 544111 544112 544113 [...] (122022 flits)
Measured flits: 544104 544105 544106 544107 544108 544109 544110 544111 544112 544113 [...] (113985 flits)
Class 0:
Remaining flits: 581778 581779 581780 581781 581782 581783 581784 581785 581786 581787 [...] (121149 flits)
Measured flits: 581778 581779 581780 581781 581782 581783 581784 581785 581786 581787 [...] (110437 flits)
Class 0:
Remaining flits: 585936 585937 585938 585939 585940 585941 585942 585943 585944 585945 [...] (121158 flits)
Measured flits: 585936 585937 585938 585939 585940 585941 585942 585943 585944 585945 [...] (107390 flits)
Class 0:
Remaining flits: 588096 588097 588098 588099 588100 588101 588102 588103 588104 588105 [...] (119926 flits)
Measured flits: 588096 588097 588098 588099 588100 588101 588102 588103 588104 588105 [...] (101828 flits)
Class 0:
Remaining flits: 591431 591432 591433 591434 591435 591436 591437 591438 591439 591440 [...] (118914 flits)
Measured flits: 591431 591432 591433 591434 591435 591436 591437 591438 591439 591440 [...] (96506 flits)
Class 0:
Remaining flits: 627444 627445 627446 627447 627448 627449 627450 627451 627452 627453 [...] (117808 flits)
Measured flits: 627444 627445 627446 627447 627448 627449 627450 627451 627452 627453 [...] (91689 flits)
Class 0:
Remaining flits: 715356 715357 715358 715359 715360 715361 715362 715363 715364 715365 [...] (118235 flits)
Measured flits: 715356 715357 715358 715359 715360 715361 715362 715363 715364 715365 [...] (88494 flits)
Class 0:
Remaining flits: 727272 727273 727274 727275 727276 727277 727278 727279 727280 727281 [...] (119523 flits)
Measured flits: 727272 727273 727274 727275 727276 727277 727278 727279 727280 727281 [...] (85188 flits)
Class 0:
Remaining flits: 793926 793927 793928 793929 793930 793931 793932 793933 793934 793935 [...] (120169 flits)
Measured flits: 793926 793927 793928 793929 793930 793931 793932 793933 793934 793935 [...] (80542 flits)
Class 0:
Remaining flits: 820116 820117 820118 820119 820120 820121 820122 820123 820124 820125 [...] (122502 flits)
Measured flits: 820116 820117 820118 820119 820120 820121 820122 820123 820124 820125 [...] (76761 flits)
Class 0:
Remaining flits: 844249 844250 844251 844252 844253 846756 846757 846758 846759 846760 [...] (122772 flits)
Measured flits: 844249 844250 844251 844252 844253 846756 846757 846758 846759 846760 [...] (73010 flits)
Class 0:
Remaining flits: 846756 846757 846758 846759 846760 846761 846762 846763 846764 846765 [...] (120081 flits)
Measured flits: 846756 846757 846758 846759 846760 846761 846762 846763 846764 846765 [...] (68204 flits)
Class 0:
Remaining flits: 889393 889394 889395 889396 889397 913986 913987 913988 913989 913990 [...] (119658 flits)
Measured flits: 889393 889394 889395 889396 889397 916218 916219 916220 916221 916222 [...] (63135 flits)
Class 0:
Remaining flits: 917496 917497 917498 917499 917500 917501 917502 917503 917504 917505 [...] (118593 flits)
Measured flits: 922986 922987 922988 922989 922990 922991 922992 922993 922994 922995 [...] (57773 flits)
Class 0:
Remaining flits: 989658 989659 989660 989661 989662 989663 989664 989665 989666 989667 [...] (118659 flits)
Measured flits: 989658 989659 989660 989661 989662 989663 989664 989665 989666 989667 [...] (52035 flits)
Class 0:
Remaining flits: 1034928 1034929 1034930 1034931 1034932 1034933 1034934 1034935 1034936 1034937 [...] (120444 flits)
Measured flits: 1034928 1034929 1034930 1034931 1034932 1034933 1034934 1034935 1034936 1034937 [...] (49503 flits)
Class 0:
Remaining flits: 1034928 1034929 1034930 1034931 1034932 1034933 1034934 1034935 1034936 1034937 [...] (120314 flits)
Measured flits: 1034928 1034929 1034930 1034931 1034932 1034933 1034934 1034935 1034936 1034937 [...] (45018 flits)
Class 0:
Remaining flits: 1041048 1041049 1041050 1041051 1041052 1041053 1041054 1041055 1041056 1041057 [...] (121259 flits)
Measured flits: 1041048 1041049 1041050 1041051 1041052 1041053 1041054 1041055 1041056 1041057 [...] (40709 flits)
Class 0:
Remaining flits: 1065366 1065367 1065368 1065369 1065370 1065371 1065372 1065373 1065374 1065375 [...] (121714 flits)
Measured flits: 1065366 1065367 1065368 1065369 1065370 1065371 1065372 1065373 1065374 1065375 [...] (36039 flits)
Class 0:
Remaining flits: 1100574 1100575 1100576 1100577 1100578 1100579 1100580 1100581 1100582 1100583 [...] (124563 flits)
Measured flits: 1117890 1117891 1117892 1117893 1117894 1117895 1117896 1117897 1117898 1117899 [...] (31346 flits)
Class 0:
Remaining flits: 1102158 1102159 1102160 1102161 1102162 1102163 1102164 1102165 1102166 1102167 [...] (125053 flits)
Measured flits: 1144854 1144855 1144856 1144857 1144858 1144859 1144860 1144861 1144862 1144863 [...] (28246 flits)
Class 0:
Remaining flits: 1186344 1186345 1186346 1186347 1186348 1186349 1186350 1186351 1186352 1186353 [...] (123584 flits)
Measured flits: 1186344 1186345 1186346 1186347 1186348 1186349 1186350 1186351 1186352 1186353 [...] (24814 flits)
Class 0:
Remaining flits: 1204128 1204129 1204130 1204131 1204132 1204133 1204134 1204135 1204136 1204137 [...] (121906 flits)
Measured flits: 1210302 1210303 1210304 1210305 1210306 1210307 1210308 1210309 1210310 1210311 [...] (22056 flits)
Class 0:
Remaining flits: 1210302 1210303 1210304 1210305 1210306 1210307 1210308 1210309 1210310 1210311 [...] (122214 flits)
Measured flits: 1210302 1210303 1210304 1210305 1210306 1210307 1210308 1210309 1210310 1210311 [...] (19321 flits)
Class 0:
Remaining flits: 1269162 1269163 1269164 1269165 1269166 1269167 1269168 1269169 1269170 1269171 [...] (122704 flits)
Measured flits: 1305702 1305703 1305704 1305705 1305706 1305707 1305708 1305709 1305710 1305711 [...] (16910 flits)
Class 0:
Remaining flits: 1269691 1269692 1269693 1269694 1269695 1269696 1269697 1269698 1269699 1269700 [...] (124162 flits)
Measured flits: 1305702 1305703 1305704 1305705 1305706 1305707 1305708 1305709 1305710 1305711 [...] (15008 flits)
Class 0:
Remaining flits: 1307124 1307125 1307126 1307127 1307128 1307129 1307130 1307131 1307132 1307133 [...] (121515 flits)
Measured flits: 1307124 1307125 1307126 1307127 1307128 1307129 1307130 1307131 1307132 1307133 [...] (12924 flits)
Class 0:
Remaining flits: 1307124 1307125 1307126 1307127 1307128 1307129 1307130 1307131 1307132 1307133 [...] (121623 flits)
Measured flits: 1307124 1307125 1307126 1307127 1307128 1307129 1307130 1307131 1307132 1307133 [...] (11071 flits)
Class 0:
Remaining flits: 1361862 1361863 1361864 1361865 1361866 1361867 1361868 1361869 1361870 1361871 [...] (120302 flits)
Measured flits: 1406307 1406308 1406309 1406310 1406311 1406312 1406313 1406314 1406315 1406316 [...] (9285 flits)
Class 0:
Remaining flits: 1368252 1368253 1368254 1368255 1368256 1368257 1368258 1368259 1368260 1368261 [...] (121040 flits)
Measured flits: 1460826 1460827 1460828 1460829 1460830 1460831 1460832 1460833 1460834 1460835 [...] (8126 flits)
Class 0:
Remaining flits: 1426500 1426501 1426502 1426503 1426504 1426505 1426506 1426507 1426508 1426509 [...] (121853 flits)
Measured flits: 1562436 1562437 1562438 1562439 1562440 1562441 1562442 1562443 1562444 1562445 [...] (7433 flits)
Class 0:
Remaining flits: 1477296 1477297 1477298 1477299 1477300 1477301 1477302 1477303 1477304 1477305 [...] (121244 flits)
Measured flits: 1581084 1581085 1581086 1581087 1581088 1581089 1581090 1581091 1581092 1581093 [...] (7270 flits)
Class 0:
Remaining flits: 1477296 1477297 1477298 1477299 1477300 1477301 1477302 1477303 1477304 1477305 [...] (121262 flits)
Measured flits: 1581778 1581779 1581780 1581781 1581782 1581783 1581784 1581785 1587978 1587979 [...] (7014 flits)
Class 0:
Remaining flits: 1573307 1574316 1574317 1574318 1574319 1574320 1574321 1574322 1574323 1574324 [...] (123004 flits)
Measured flits: 1596924 1596925 1596926 1596927 1596928 1596929 1596930 1596931 1596932 1596933 [...] (6560 flits)
Class 0:
Remaining flits: 1596924 1596925 1596926 1596927 1596928 1596929 1596930 1596931 1596932 1596933 [...] (122649 flits)
Measured flits: 1596924 1596925 1596926 1596927 1596928 1596929 1596930 1596931 1596932 1596933 [...] (5603 flits)
Class 0:
Remaining flits: 1623816 1623817 1623818 1623819 1623820 1623821 1623822 1623823 1623824 1623825 [...] (120810 flits)
Measured flits: 1703790 1703791 1703792 1703793 1703794 1703795 1703796 1703797 1703798 1703799 [...] (4596 flits)
Class 0:
Remaining flits: 1623816 1623817 1623818 1623819 1623820 1623821 1623822 1623823 1623824 1623825 [...] (121331 flits)
Measured flits: 1708110 1708111 1708112 1708113 1708114 1708115 1708116 1708117 1708118 1708119 [...] (3845 flits)
Class 0:
Remaining flits: 1708110 1708111 1708112 1708113 1708114 1708115 1708116 1708117 1708118 1708119 [...] (124127 flits)
Measured flits: 1708110 1708111 1708112 1708113 1708114 1708115 1708116 1708117 1708118 1708119 [...] (2937 flits)
Class 0:
Remaining flits: 1719833 1719834 1719835 1719836 1719837 1719838 1719839 1719840 1719841 1719842 [...] (123214 flits)
Measured flits: 1760472 1760473 1760474 1760475 1760476 1760477 1760478 1760479 1760480 1760481 [...] (2180 flits)
Class 0:
Remaining flits: 1751202 1751203 1751204 1751205 1751206 1751207 1751208 1751209 1751210 1751211 [...] (121864 flits)
Measured flits: 1779210 1779211 1779212 1779213 1779214 1779215 1779216 1779217 1779218 1779219 [...] (1595 flits)
Class 0:
Remaining flits: 1779210 1779211 1779212 1779213 1779214 1779215 1779216 1779217 1779218 1779219 [...] (118855 flits)
Measured flits: 1779210 1779211 1779212 1779213 1779214 1779215 1779216 1779217 1779218 1779219 [...] (1398 flits)
Class 0:
Remaining flits: 1779210 1779211 1779212 1779213 1779214 1779215 1779216 1779217 1779218 1779219 [...] (118036 flits)
Measured flits: 1779210 1779211 1779212 1779213 1779214 1779215 1779216 1779217 1779218 1779219 [...] (1271 flits)
Class 0:
Remaining flits: 1782072 1782073 1782074 1782075 1782076 1782077 1782078 1782079 1782080 1782081 [...] (119041 flits)
Measured flits: 1782072 1782073 1782074 1782075 1782076 1782077 1782078 1782079 1782080 1782081 [...] (1405 flits)
Class 0:
Remaining flits: 1791630 1791631 1791632 1791633 1791634 1791635 1791636 1791637 1791638 1791639 [...] (121095 flits)
Measured flits: 1791630 1791631 1791632 1791633 1791634 1791635 1791636 1791637 1791638 1791639 [...] (1280 flits)
Class 0:
Remaining flits: 1902222 1902223 1902224 1902225 1902226 1902227 1902228 1902229 1902230 1902231 [...] (120275 flits)
Measured flits: 1945044 1945045 1945046 1945047 1945048 1945049 1945050 1945051 1945052 1945053 [...] (1139 flits)
Class 0:
Remaining flits: 1941444 1941445 1941446 1941447 1941448 1941449 1941450 1941451 1941452 1941453 [...] (117909 flits)
Measured flits: 1991484 1991485 1991486 1991487 1991488 1991489 1991490 1991491 1991492 1991493 [...] (1170 flits)
Class 0:
Remaining flits: 1941444 1941445 1941446 1941447 1941448 1941449 1941450 1941451 1941452 1941453 [...] (118339 flits)
Measured flits: 2086092 2086093 2086094 2086095 2086096 2086097 2086098 2086099 2086100 2086101 [...] (1134 flits)
Class 0:
Remaining flits: 1995372 1995373 1995374 1995375 1995376 1995377 1995378 1995379 1995380 1995381 [...] (118400 flits)
Measured flits: 2096084 2096085 2096086 2096087 2096088 2096089 2096090 2096091 2096092 2096093 [...] (1165 flits)
Class 0:
Remaining flits: 1995372 1995373 1995374 1995375 1995376 1995377 1995378 1995379 1995380 1995381 [...] (121553 flits)
Measured flits: 2156328 2156329 2156330 2156331 2156332 2156333 2156334 2156335 2156336 2156337 [...] (855 flits)
Class 0:
Remaining flits: 2045862 2045863 2045864 2045865 2045866 2045867 2045868 2045869 2045870 2045871 [...] (121007 flits)
Measured flits: 2156328 2156329 2156330 2156331 2156332 2156333 2156334 2156335 2156336 2156337 [...] (540 flits)
Class 0:
Remaining flits: 2086614 2086615 2086616 2086617 2086618 2086619 2086620 2086621 2086622 2086623 [...] (120464 flits)
Measured flits: 2187918 2187919 2187920 2187921 2187922 2187923 2187924 2187925 2187926 2187927 [...] (270 flits)
Class 0:
Remaining flits: 2098782 2098783 2098784 2098785 2098786 2098787 2098788 2098789 2098790 2098791 [...] (121220 flits)
Measured flits: 2294208 2294209 2294210 2294211 2294212 2294213 2294214 2294215 2294216 2294217 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2129634 2129635 2129636 2129637 2129638 2129639 2129640 2129641 2129642 2129643 [...] (92389 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2129634 2129635 2129636 2129637 2129638 2129639 2129640 2129641 2129642 2129643 [...] (63179 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2216700 2216701 2216702 2216703 2216704 2216705 2216706 2216707 2216708 2216709 [...] (34165 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2273796 2273797 2273798 2273799 2273800 2273801 2273802 2273803 2273804 2273805 [...] (10144 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2399044 2399045 2399046 2399047 2399048 2399049 2399050 2399051 2399052 2399053 [...] (263 flits)
Measured flits: (0 flits)
Time taken is 80728 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 18561.2 (1 samples)
	minimum = 46 (1 samples)
	maximum = 65952 (1 samples)
Network latency average = 3881.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13866 (1 samples)
Flit latency average = 3793.91 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13839 (1 samples)
Fragmentation average = 70.8819 (1 samples)
	minimum = 0 (1 samples)
	maximum = 192 (1 samples)
Injected packet rate average = 0.00909152 (1 samples)
	minimum = 0.00257143 (1 samples)
	maximum = 0.0167143 (1 samples)
Accepted packet rate average = 0.0091317 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.163751 (1 samples)
	minimum = 0.0458571 (1 samples)
	maximum = 0.301286 (1 samples)
Accepted flit rate average = 0.164491 (1 samples)
	minimum = 0.114571 (1 samples)
	maximum = 0.215286 (1 samples)
Injected packet size average = 18.0115 (1 samples)
Accepted packet size average = 18.0132 (1 samples)
Hops average = 5.05926 (1 samples)
Total run time 61.9662
