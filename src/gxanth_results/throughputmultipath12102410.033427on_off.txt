BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 358.237
	minimum = 27
	maximum = 984
Network latency average = 266.551
	minimum = 27
	maximum = 883
Slowest packet = 67
Flit latency average = 233.19
	minimum = 6
	maximum = 901
Slowest flit = 8324
Fragmentation average = 52.539
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0102813
	minimum = 0.004 (at node 28)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.193745
	minimum = 0.073 (at node 178)
	maximum = 0.33 (at node 140)
Injected packet length average = 17.8118
Accepted packet length average = 18.8445
Total in-flight flits = 63241 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 624.99
	minimum = 23
	maximum = 1947
Network latency average = 493.343
	minimum = 23
	maximum = 1779
Slowest packet = 67
Flit latency average = 458.729
	minimum = 6
	maximum = 1857
Slowest flit = 11211
Fragmentation average = 57.3408
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0305885
	minimum = 0.003 (at node 92)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0108646
	minimum = 0.0065 (at node 45)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.54838
	minimum = 0.054 (at node 92)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.199961
	minimum = 0.117 (at node 83)
	maximum = 0.286 (at node 89)
Injected packet length average = 17.9276
Accepted packet length average = 18.4048
Total in-flight flits = 134643 (0 measured)
latency change    = 0.426811
throughput change = 0.0310868
Class 0:
Packet latency average = 1413.24
	minimum = 25
	maximum = 2954
Network latency average = 1212.97
	minimum = 23
	maximum = 2716
Slowest packet = 1272
Flit latency average = 1189.97
	minimum = 6
	maximum = 2722
Slowest flit = 24071
Fragmentation average = 62.1275
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0302552
	minimum = 0.001 (at node 42)
	maximum = 0.056 (at node 24)
Accepted packet rate average = 0.0107031
	minimum = 0.003 (at node 190)
	maximum = 0.018 (at node 50)
Injected flit rate average = 0.543885
	minimum = 0.002 (at node 42)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.191281
	minimum = 0.044 (at node 190)
	maximum = 0.307 (at node 34)
Injected packet length average = 17.9766
Accepted packet length average = 17.8715
Total in-flight flits = 202497 (0 measured)
latency change    = 0.557761
throughput change = 0.0453766
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 416.379
	minimum = 32
	maximum = 2591
Network latency average = 97.493
	minimum = 23
	maximum = 877
Slowest packet = 17568
Flit latency average = 1771.4
	minimum = 6
	maximum = 3517
Slowest flit = 41916
Fragmentation average = 14.5738
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0306042
	minimum = 0 (at node 135)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.010401
	minimum = 0.002 (at node 132)
	maximum = 0.02 (at node 87)
Injected flit rate average = 0.550193
	minimum = 0 (at node 135)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.188453
	minimum = 0.034 (at node 132)
	maximum = 0.36 (at node 87)
Injected packet length average = 17.9777
Accepted packet length average = 18.1187
Total in-flight flits = 272064 (99218 measured)
latency change    = 2.39412
throughput change = 0.015007
Class 0:
Packet latency average = 549.875
	minimum = 27
	maximum = 3407
Network latency average = 235.061
	minimum = 23
	maximum = 1961
Slowest packet = 17568
Flit latency average = 2090.84
	minimum = 6
	maximum = 4329
Slowest flit = 62518
Fragmentation average = 17.787
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0297057
	minimum = 0.0005 (at node 135)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0105208
	minimum = 0.0055 (at node 22)
	maximum = 0.0165 (at node 137)
Injected flit rate average = 0.534484
	minimum = 0.009 (at node 135)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.189596
	minimum = 0.0905 (at node 22)
	maximum = 0.294 (at node 137)
Injected packet length average = 17.9926
Accepted packet length average = 18.021
Total in-flight flits = 335000 (191327 measured)
latency change    = 0.242776
throughput change = 0.00602981
Class 0:
Packet latency average = 804.001
	minimum = 27
	maximum = 3883
Network latency average = 489.591
	minimum = 23
	maximum = 2940
Slowest packet = 17568
Flit latency average = 2401.55
	minimum = 6
	maximum = 5389
Slowest flit = 54533
Fragmentation average = 22.5666
	minimum = 0
	maximum = 136
Injected packet rate average = 0.0293003
	minimum = 0.004 (at node 135)
	maximum = 0.0556667 (at node 51)
Accepted packet rate average = 0.0104392
	minimum = 0.00566667 (at node 4)
	maximum = 0.0153333 (at node 147)
Injected flit rate average = 0.527472
	minimum = 0.0696667 (at node 135)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.188069
	minimum = 0.101667 (at node 4)
	maximum = 0.279667 (at node 147)
Injected packet length average = 18.0023
Accepted packet length average = 18.0156
Total in-flight flits = 397973 (281136 measured)
latency change    = 0.316076
throughput change = 0.00811886
Draining remaining packets ...
Class 0:
Remaining flits: 80604 80605 80606 80607 80608 80609 80610 80611 80612 80613 [...] (367069 flits)
Measured flits: 316008 316009 316010 316011 316012 316013 316014 316015 316016 316017 [...] (274437 flits)
Class 0:
Remaining flits: 86832 86833 86834 86835 86836 86837 86838 86839 86840 86841 [...] (336027 flits)
Measured flits: 316008 316009 316010 316011 316012 316013 316014 316015 316016 316017 [...] (264725 flits)
Class 0:
Remaining flits: 86832 86833 86834 86835 86836 86837 86838 86839 86840 86841 [...] (305434 flits)
Measured flits: 316026 316027 316028 316029 316030 316031 316032 316033 316034 316035 [...] (252588 flits)
Class 0:
Remaining flits: 117666 117667 117668 117669 117670 117671 117672 117673 117674 117675 [...] (274081 flits)
Measured flits: 316056 316057 316058 316059 316060 316061 316062 316063 316064 316065 [...] (236640 flits)
Class 0:
Remaining flits: 125622 125623 125624 125625 125626 125627 125628 125629 125630 125631 [...] (243814 flits)
Measured flits: 316062 316063 316064 316065 316066 316067 316068 316069 316070 316071 [...] (217756 flits)
Class 0:
Remaining flits: 141120 141121 141122 141123 141124 141125 141126 141127 141128 141129 [...] (214132 flits)
Measured flits: 316062 316063 316064 316065 316066 316067 316068 316069 316070 316071 [...] (196174 flits)
Class 0:
Remaining flits: 159894 159895 159896 159897 159898 159899 159900 159901 159902 159903 [...] (185120 flits)
Measured flits: 316098 316099 316100 316101 316102 316103 316104 316105 316106 316107 [...] (173251 flits)
Class 0:
Remaining flits: 166896 166897 166898 166899 166900 166901 166902 166903 166904 166905 [...] (156500 flits)
Measured flits: 316098 316099 316100 316101 316102 316103 316104 316105 316106 316107 [...] (149045 flits)
Class 0:
Remaining flits: 181926 181927 181928 181929 181930 181931 181932 181933 181934 181935 [...] (128007 flits)
Measured flits: 316129 316130 316131 316132 316133 316404 316405 316406 316407 316408 [...] (123610 flits)
Class 0:
Remaining flits: 196146 196147 196148 196149 196150 196151 196152 196153 196154 196155 [...] (99338 flits)
Measured flits: 316440 316441 316442 316443 316444 316445 316446 316447 316448 316449 [...] (96948 flits)
Class 0:
Remaining flits: 209484 209485 209486 209487 209488 209489 209490 209491 209492 209493 [...] (72379 flits)
Measured flits: 316449 316450 316451 316452 316453 316454 316455 316456 316457 317358 [...] (71311 flits)
Class 0:
Remaining flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (47088 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (46713 flits)
Class 0:
Remaining flits: 301734 301735 301736 301737 301738 301739 301740 301741 301742 301743 [...] (25119 flits)
Measured flits: 320580 320581 320582 320583 320584 320585 320586 320587 320588 320589 [...] (25047 flits)
Class 0:
Remaining flits: 337626 337627 337628 337629 337630 337631 337632 337633 337634 337635 [...] (10992 flits)
Measured flits: 337626 337627 337628 337629 337630 337631 337632 337633 337634 337635 [...] (10992 flits)
Class 0:
Remaining flits: 416214 416215 416216 416217 416218 416219 416220 416221 416222 416223 [...] (2657 flits)
Measured flits: 416214 416215 416216 416217 416218 416219 416220 416221 416222 416223 [...] (2657 flits)
Time taken is 21801 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9354.47 (1 samples)
	minimum = 27 (1 samples)
	maximum = 18307 (1 samples)
Network latency average = 8978.17 (1 samples)
	minimum = 23 (1 samples)
	maximum = 17272 (1 samples)
Flit latency average = 7471.35 (1 samples)
	minimum = 6 (1 samples)
	maximum = 17255 (1 samples)
Fragmentation average = 70.2693 (1 samples)
	minimum = 0 (1 samples)
	maximum = 176 (1 samples)
Injected packet rate average = 0.0293003 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0104392 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.527472 (1 samples)
	minimum = 0.0696667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.188069 (1 samples)
	minimum = 0.101667 (1 samples)
	maximum = 0.279667 (1 samples)
Injected packet size average = 18.0023 (1 samples)
Accepted packet size average = 18.0156 (1 samples)
Hops average = 5.14314 (1 samples)
Total run time 12.779
