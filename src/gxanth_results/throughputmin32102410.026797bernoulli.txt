BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 292.199
	minimum = 23
	maximum = 963
Network latency average = 285.752
	minimum = 23
	maximum = 951
Slowest packet = 186
Flit latency average = 218.81
	minimum = 6
	maximum = 945
Slowest flit = 2265
Fragmentation average = 153.014
	minimum = 0
	maximum = 815
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0103125
	minimum = 0.003 (at node 106)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.213286
	minimum = 0.078 (at node 106)
	maximum = 0.34 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 20.6823
Total in-flight flits = 51659 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 526.094
	minimum = 23
	maximum = 1776
Network latency average = 518.917
	minimum = 23
	maximum = 1771
Slowest packet = 729
Flit latency average = 441.38
	minimum = 6
	maximum = 1858
Slowest flit = 10004
Fragmentation average = 188.496
	minimum = 0
	maximum = 1662
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0114167
	minimum = 0.006 (at node 40)
	maximum = 0.017 (at node 51)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.219195
	minimum = 0.1145 (at node 40)
	maximum = 0.3255 (at node 51)
Injected packet length average = 17.9276
Accepted packet length average = 19.1996
Total in-flight flits = 100257 (0 measured)
latency change    = 0.444587
throughput change = 0.026957
Class 0:
Packet latency average = 1165.22
	minimum = 23
	maximum = 2743
Network latency average = 1157.27
	minimum = 23
	maximum = 2733
Slowest packet = 1299
Flit latency average = 1097.39
	minimum = 6
	maximum = 2721
Slowest flit = 23521
Fragmentation average = 214.367
	minimum = 0
	maximum = 2317
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0128542
	minimum = 0.005 (at node 30)
	maximum = 0.025 (at node 78)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.229573
	minimum = 0.086 (at node 146)
	maximum = 0.426 (at node 16)
Injected packet length average = 17.9863
Accepted packet length average = 17.8598
Total in-flight flits = 148465 (0 measured)
latency change    = 0.548501
throughput change = 0.045204
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 65.3549
	minimum = 23
	maximum = 813
Network latency average = 57.0552
	minimum = 23
	maximum = 813
Slowest packet = 15951
Flit latency average = 1552.23
	minimum = 6
	maximum = 3822
Slowest flit = 13708
Fragmentation average = 15.2566
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0271719
	minimum = 0.013 (at node 159)
	maximum = 0.046 (at node 26)
Accepted packet rate average = 0.0126875
	minimum = 0.004 (at node 134)
	maximum = 0.021 (at node 8)
Injected flit rate average = 0.488312
	minimum = 0.25 (at node 159)
	maximum = 0.822 (at node 26)
Accepted flit rate average= 0.228734
	minimum = 0.107 (at node 35)
	maximum = 0.391 (at node 8)
Injected packet length average = 17.9712
Accepted packet length average = 18.0283
Total in-flight flits = 198454 (86178 measured)
latency change    = 16.8291
throughput change = 0.00366601
Class 0:
Packet latency average = 188.738
	minimum = 23
	maximum = 1949
Network latency average = 180.68
	minimum = 23
	maximum = 1946
Slowest packet = 15614
Flit latency average = 1791.24
	minimum = 6
	maximum = 4668
Slowest flit = 25740
Fragmentation average = 32.5584
	minimum = 0
	maximum = 623
Injected packet rate average = 0.02675
	minimum = 0.0185 (at node 28)
	maximum = 0.038 (at node 87)
Accepted packet rate average = 0.0126042
	minimum = 0.0065 (at node 105)
	maximum = 0.02 (at node 19)
Injected flit rate average = 0.481448
	minimum = 0.333 (at node 28)
	maximum = 0.692 (at node 87)
Accepted flit rate average= 0.226742
	minimum = 0.1315 (at node 105)
	maximum = 0.343 (at node 181)
Injected packet length average = 17.9981
Accepted packet length average = 17.9895
Total in-flight flits = 246292 (167715 measured)
latency change    = 0.653727
throughput change = 0.00878614
Class 0:
Packet latency average = 654.092
	minimum = 23
	maximum = 2966
Network latency average = 646.371
	minimum = 23
	maximum = 2960
Slowest packet = 15480
Flit latency average = 2017.54
	minimum = 6
	maximum = 5704
Slowest flit = 25752
Fragmentation average = 69.3415
	minimum = 0
	maximum = 1787
Injected packet rate average = 0.0267986
	minimum = 0.0193333 (at node 28)
	maximum = 0.034 (at node 26)
Accepted packet rate average = 0.012566
	minimum = 0.007 (at node 134)
	maximum = 0.018 (at node 181)
Injected flit rate average = 0.48247
	minimum = 0.348 (at node 28)
	maximum = 0.611667 (at node 26)
Accepted flit rate average= 0.225502
	minimum = 0.124333 (at node 134)
	maximum = 0.325333 (at node 181)
Injected packet length average = 18.0036
Accepted packet length average = 17.9454
Total in-flight flits = 296424 (244154 measured)
latency change    = 0.71145
throughput change = 0.00550085
Draining remaining packets ...
Class 0:
Remaining flits: 37599 37600 37601 42066 42067 42068 42069 42070 42071 42072 [...] (261367 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (226779 flits)
Class 0:
Remaining flits: 42082 42083 59526 59527 59528 59529 59530 59531 59532 59533 [...] (225971 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (203139 flits)
Class 0:
Remaining flits: 82062 82063 82064 82065 82066 82067 82068 82069 82070 82071 [...] (190787 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (176155 flits)
Class 0:
Remaining flits: 82074 82075 82076 82077 82078 82079 100332 100333 100334 100335 [...] (155914 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (146599 flits)
Class 0:
Remaining flits: 104382 104383 104384 104385 104386 104387 104388 104389 104390 104391 [...] (121303 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (115508 flits)
Class 0:
Remaining flits: 104382 104383 104384 104385 104386 104387 104388 104389 104390 104391 [...] (87622 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (84510 flits)
Class 0:
Remaining flits: 144468 144469 144470 144471 144472 144473 144474 144475 144476 144477 [...] (54703 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (53253 flits)
Class 0:
Remaining flits: 153976 153977 153978 153979 153980 153981 153982 153983 153984 153985 [...] (23911 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (23360 flits)
Class 0:
Remaining flits: 232396 232397 252288 252289 252290 252291 252292 252293 252294 252295 [...] (4750 flits)
Measured flits: 288862 288863 299803 299804 299805 299806 299807 301114 301115 301116 [...] (4694 flits)
Class 0:
Remaining flits: 349128 349129 349130 349131 349132 349133 349134 349135 349136 349137 [...] (470 flits)
Measured flits: 349128 349129 349130 349131 349132 349133 349134 349135 349136 349137 [...] (470 flits)
Time taken is 16472 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5616.37 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12292 (1 samples)
Network latency average = 5608.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12279 (1 samples)
Flit latency average = 4868.46 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12798 (1 samples)
Fragmentation average = 169.912 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2375 (1 samples)
Injected packet rate average = 0.0267986 (1 samples)
	minimum = 0.0193333 (1 samples)
	maximum = 0.034 (1 samples)
Accepted packet rate average = 0.012566 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.48247 (1 samples)
	minimum = 0.348 (1 samples)
	maximum = 0.611667 (1 samples)
Accepted flit rate average = 0.225502 (1 samples)
	minimum = 0.124333 (1 samples)
	maximum = 0.325333 (1 samples)
Injected packet size average = 18.0036 (1 samples)
Accepted packet size average = 17.9454 (1 samples)
Hops average = 5.0837 (1 samples)
Total run time 15.324
