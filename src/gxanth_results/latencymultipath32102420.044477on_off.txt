BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.548
	minimum = 22
	maximum = 982
Network latency average = 235.255
	minimum = 22
	maximum = 849
Slowest packet = 46
Flit latency average = 205.837
	minimum = 5
	maximum = 877
Slowest flit = 9662
Fragmentation average = 47.4145
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139479
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26375
	minimum = 0.118 (at node 150)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8285
Accepted packet length average = 18.9096
Total in-flight flits = 52194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 616.056
	minimum = 22
	maximum = 1951
Network latency average = 446.403
	minimum = 22
	maximum = 1615
Slowest packet = 46
Flit latency average = 415.17
	minimum = 5
	maximum = 1731
Slowest flit = 22883
Fragmentation average = 68.2237
	minimum = 0
	maximum = 410
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0147943
	minimum = 0.009 (at node 30)
	maximum = 0.021 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.275729
	minimum = 0.162 (at node 153)
	maximum = 0.391 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.6376
Total in-flight flits = 110894 (0 measured)
latency change    = 0.429357
throughput change = 0.0434454
Class 0:
Packet latency average = 1357.86
	minimum = 25
	maximum = 2820
Network latency average = 1077.45
	minimum = 22
	maximum = 2503
Slowest packet = 4200
Flit latency average = 1040.3
	minimum = 5
	maximum = 2486
Slowest flit = 43667
Fragmentation average = 103.866
	minimum = 0
	maximum = 436
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0165313
	minimum = 0.007 (at node 190)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.297781
	minimum = 0.128 (at node 113)
	maximum = 0.532 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.0132
Total in-flight flits = 175886 (0 measured)
latency change    = 0.546303
throughput change = 0.0740546
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 446.424
	minimum = 28
	maximum = 2067
Network latency average = 37.4346
	minimum = 22
	maximum = 203
Slowest packet = 18899
Flit latency average = 1471.51
	minimum = 5
	maximum = 3192
Slowest flit = 74411
Fragmentation average = 6.47483
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0167292
	minimum = 0.005 (at node 184)
	maximum = 0.03 (at node 128)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.302354
	minimum = 0.107 (at node 184)
	maximum = 0.536 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 18.0735
Total in-flight flits = 247218 (118532 measured)
latency change    = 2.04163
throughput change = 0.0151244
Class 0:
Packet latency average = 497.137
	minimum = 24
	maximum = 2581
Network latency average = 85.4031
	minimum = 22
	maximum = 1966
Slowest packet = 18899
Flit latency average = 1702.51
	minimum = 5
	maximum = 3809
Slowest flit = 123259
Fragmentation average = 8.90066
	minimum = 0
	maximum = 242
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0166276
	minimum = 0.009 (at node 5)
	maximum = 0.0255 (at node 103)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.299792
	minimum = 0.169 (at node 5)
	maximum = 0.46 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 18.0298
Total in-flight flits = 315556 (232486 measured)
latency change    = 0.102009
throughput change = 0.0085476
Class 0:
Packet latency average = 913.919
	minimum = 24
	maximum = 3575
Network latency average = 535.069
	minimum = 22
	maximum = 2959
Slowest packet = 18899
Flit latency average = 1934.44
	minimum = 5
	maximum = 4578
Slowest flit = 140975
Fragmentation average = 29.8077
	minimum = 0
	maximum = 373
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0166233
	minimum = 0.0113333 (at node 79)
	maximum = 0.0243333 (at node 128)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.299898
	minimum = 0.202 (at node 79)
	maximum = 0.439 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 18.0408
Total in-flight flits = 384241 (339260 measured)
latency change    = 0.456038
throughput change = 0.00035313
Class 0:
Packet latency average = 1605.5
	minimum = 24
	maximum = 5352
Network latency average = 1258.49
	minimum = 22
	maximum = 3984
Slowest packet = 18899
Flit latency average = 2174.33
	minimum = 5
	maximum = 5232
Slowest flit = 186245
Fragmentation average = 60.6649
	minimum = 0
	maximum = 389
Injected packet rate average = 0.036543
	minimum = 0.01325 (at node 36)
	maximum = 0.0555 (at node 33)
Accepted packet rate average = 0.0166302
	minimum = 0.0115 (at node 48)
	maximum = 0.02225 (at node 103)
Injected flit rate average = 0.65787
	minimum = 0.2385 (at node 36)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.300409
	minimum = 0.20275 (at node 48)
	maximum = 0.3925 (at node 103)
Injected packet length average = 18.0026
Accepted packet length average = 18.064
Total in-flight flits = 450342 (429108 measured)
latency change    = 0.430758
throughput change = 0.00170196
Class 0:
Packet latency average = 2285.44
	minimum = 24
	maximum = 6195
Network latency average = 1934.27
	minimum = 22
	maximum = 4944
Slowest packet = 18899
Flit latency average = 2414.03
	minimum = 5
	maximum = 6051
Slowest flit = 209404
Fragmentation average = 84.5072
	minimum = 0
	maximum = 445
Injected packet rate average = 0.0363698
	minimum = 0.0128 (at node 36)
	maximum = 0.0556 (at node 33)
Accepted packet rate average = 0.0166365
	minimum = 0.0124 (at node 48)
	maximum = 0.022 (at node 138)
Injected flit rate average = 0.65476
	minimum = 0.2304 (at node 36)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.300502
	minimum = 0.2256 (at node 48)
	maximum = 0.3944 (at node 138)
Injected packet length average = 18.0029
Accepted packet length average = 18.0629
Total in-flight flits = 515874 (506223 measured)
latency change    = 0.297507
throughput change = 0.000310245
Class 0:
Packet latency average = 2813.34
	minimum = 24
	maximum = 7338
Network latency average = 2455.47
	minimum = 22
	maximum = 5944
Slowest packet = 18899
Flit latency average = 2660.58
	minimum = 5
	maximum = 6941
Slowest flit = 220659
Fragmentation average = 99.5333
	minimum = 0
	maximum = 445
Injected packet rate average = 0.0365964
	minimum = 0.0131667 (at node 36)
	maximum = 0.0555 (at node 162)
Accepted packet rate average = 0.0166198
	minimum = 0.0126667 (at node 80)
	maximum = 0.022 (at node 138)
Injected flit rate average = 0.658522
	minimum = 0.234167 (at node 36)
	maximum = 1 (at node 162)
Accepted flit rate average= 0.300535
	minimum = 0.2305 (at node 80)
	maximum = 0.397667 (at node 138)
Injected packet length average = 17.9942
Accepted packet length average = 18.0829
Total in-flight flits = 588532 (585182 measured)
latency change    = 0.187643
throughput change = 0.000108603
Class 0:
Packet latency average = 3277
	minimum = 22
	maximum = 8576
Network latency average = 2910.33
	minimum = 22
	maximum = 6909
Slowest packet = 18899
Flit latency average = 2906.49
	minimum = 5
	maximum = 7514
Slowest flit = 262925
Fragmentation average = 109.397
	minimum = 0
	maximum = 515
Injected packet rate average = 0.0365521
	minimum = 0.0151429 (at node 153)
	maximum = 0.0555714 (at node 162)
Accepted packet rate average = 0.0166577
	minimum = 0.013 (at node 48)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.657914
	minimum = 0.273286 (at node 153)
	maximum = 1 (at node 162)
Accepted flit rate average= 0.300722
	minimum = 0.232143 (at node 48)
	maximum = 0.378143 (at node 138)
Injected packet length average = 17.9994
Accepted packet length average = 18.053
Total in-flight flits = 655984 (655230 measured)
latency change    = 0.14149
throughput change = 0.000621851
Draining all recorded packets ...
Class 0:
Remaining flits: 321801 321802 321803 330696 330697 330698 330699 330700 330701 330702 [...] (721960 flits)
Measured flits: 339588 339589 339590 339591 339592 339593 339594 339595 339596 339597 [...] (654538 flits)
Class 0:
Remaining flits: 350550 350551 350552 350553 350554 350555 350556 350557 350558 350559 [...] (796461 flits)
Measured flits: 350550 350551 350552 350553 350554 350555 350556 350557 350558 350559 [...] (614555 flits)
Class 0:
Remaining flits: 372096 372097 372098 372099 372100 372101 372102 372103 372104 372105 [...] (868564 flits)
Measured flits: 372096 372097 372098 372099 372100 372101 372102 372103 372104 372105 [...] (569593 flits)
Class 0:
Remaining flits: 415170 415171 415172 415173 415174 415175 415176 415177 415178 415179 [...] (941469 flits)
Measured flits: 415170 415171 415172 415173 415174 415175 415176 415177 415178 415179 [...] (523385 flits)
Class 0:
Remaining flits: 415183 415184 415185 415186 415187 440946 440947 440948 440949 440950 [...] (1010027 flits)
Measured flits: 415183 415184 415185 415186 415187 440946 440947 440948 440949 440950 [...] (476900 flits)
Class 0:
Remaining flits: 484341 484342 484343 487296 487297 487298 487299 487300 487301 487302 [...] (1082017 flits)
Measured flits: 484341 484342 484343 487296 487297 487298 487299 487300 487301 487302 [...] (429901 flits)
Class 0:
Remaining flits: 508860 508861 508862 508863 508864 508865 508866 508867 508868 508869 [...] (1149752 flits)
Measured flits: 508860 508861 508862 508863 508864 508865 508866 508867 508868 508869 [...] (382305 flits)
Class 0:
Remaining flits: 513947 513948 513949 513950 513951 513952 513953 550081 550082 550083 [...] (1216903 flits)
Measured flits: 513947 513948 513949 513950 513951 513952 513953 550081 550082 550083 [...] (334906 flits)
Class 0:
Remaining flits: 580623 580624 580625 583714 583715 583716 583717 583718 583719 583720 [...] (1284316 flits)
Measured flits: 580623 580624 580625 583714 583715 583716 583717 583718 583719 583720 [...] (287325 flits)
Class 0:
Remaining flits: 605771 616716 616717 616718 616719 616720 616721 616722 616723 616724 [...] (1351030 flits)
Measured flits: 605771 616716 616717 616718 616719 616720 616721 616722 616723 616724 [...] (241424 flits)
Class 0:
Remaining flits: 641572 641573 653054 653055 653056 653057 655362 655363 655364 655365 [...] (1414486 flits)
Measured flits: 641572 641573 653054 653055 653056 653057 655362 655363 655364 655365 [...] (198943 flits)
Class 0:
Remaining flits: 675990 675991 675992 675993 675994 675995 675996 675997 675998 675999 [...] (1477009 flits)
Measured flits: 675990 675991 675992 675993 675994 675995 675996 675997 675998 675999 [...] (162066 flits)
Class 0:
Remaining flits: 682902 682903 682904 682905 682906 682907 682908 682909 682910 682911 [...] (1540045 flits)
Measured flits: 682902 682903 682904 682905 682906 682907 682908 682909 682910 682911 [...] (131872 flits)
Class 0:
Remaining flits: 742353 742354 742355 748872 748873 748874 748875 748876 748877 748878 [...] (1602230 flits)
Measured flits: 742353 742354 742355 748872 748873 748874 748875 748876 748877 748878 [...] (105927 flits)
Class 0:
Remaining flits: 751050 751051 751052 751053 751054 751055 751056 751057 751058 751059 [...] (1665647 flits)
Measured flits: 751050 751051 751052 751053 751054 751055 751056 751057 751058 751059 [...] (84002 flits)
Class 0:
Remaining flits: 800496 800497 800498 800499 800500 800501 800502 800503 800504 800505 [...] (1730972 flits)
Measured flits: 800496 800497 800498 800499 800500 800501 800502 800503 800504 800505 [...] (66088 flits)
Class 0:
Remaining flits: 800496 800497 800498 800499 800500 800501 800502 800503 800504 800505 [...] (1797349 flits)
Measured flits: 800496 800497 800498 800499 800500 800501 800502 800503 800504 800505 [...] (51507 flits)
Class 0:
Remaining flits: 966996 966997 966998 966999 967000 967001 967002 967003 967004 967005 [...] (1850569 flits)
Measured flits: 966996 966997 966998 966999 967000 967001 967002 967003 967004 967005 [...] (40101 flits)
Class 0:
Remaining flits: 999126 999127 999128 999129 999130 999131 999132 999133 999134 999135 [...] (1905810 flits)
Measured flits: 999126 999127 999128 999129 999130 999131 999132 999133 999134 999135 [...] (30759 flits)
Class 0:
Remaining flits: 1025082 1025083 1025084 1025085 1025086 1025087 1025088 1025089 1025090 1025091 [...] (1957873 flits)
Measured flits: 1025082 1025083 1025084 1025085 1025086 1025087 1025088 1025089 1025090 1025091 [...] (23724 flits)
Class 0:
Remaining flits: 1049829 1049830 1049831 1059210 1059211 1059212 1059213 1059214 1059215 1059216 [...] (2012725 flits)
Measured flits: 1049829 1049830 1049831 1059210 1059211 1059212 1059213 1059214 1059215 1059216 [...] (17696 flits)
Class 0:
Remaining flits: 1114344 1114345 1114346 1114347 1114348 1114349 1114350 1114351 1114352 1114353 [...] (2066314 flits)
Measured flits: 1114344 1114345 1114346 1114347 1114348 1114349 1114350 1114351 1114352 1114353 [...] (13093 flits)
Class 0:
Remaining flits: 1163682 1163683 1163684 1163685 1163686 1163687 1163688 1163689 1163690 1163691 [...] (2118728 flits)
Measured flits: 1163682 1163683 1163684 1163685 1163686 1163687 1163688 1163689 1163690 1163691 [...] (9868 flits)
Class 0:
Remaining flits: 1172509 1172510 1172511 1172512 1172513 1172514 1172515 1172516 1172517 1172518 [...] (2174312 flits)
Measured flits: 1172509 1172510 1172511 1172512 1172513 1172514 1172515 1172516 1172517 1172518 [...] (7568 flits)
Class 0:
Remaining flits: 1232112 1232113 1232114 1232115 1232116 1232117 1238184 1238185 1238186 1238187 [...] (2232903 flits)
Measured flits: 1232112 1232113 1232114 1232115 1232116 1232117 1238184 1238185 1238186 1238187 [...] (5872 flits)
Class 0:
Remaining flits: 1239578 1239579 1239580 1239581 1239582 1239583 1239584 1239585 1239586 1239587 [...] (2280862 flits)
Measured flits: 1239578 1239579 1239580 1239581 1239582 1239583 1239584 1239585 1239586 1239587 [...] (4521 flits)
Class 0:
Remaining flits: 1278072 1278073 1278074 1278075 1278076 1278077 1278078 1278079 1278080 1278081 [...] (2331904 flits)
Measured flits: 1278072 1278073 1278074 1278075 1278076 1278077 1278078 1278079 1278080 1278081 [...] (3815 flits)
Class 0:
Remaining flits: 1304784 1304785 1304786 1304787 1304788 1304789 1304790 1304791 1304792 1304793 [...] (2374752 flits)
Measured flits: 1304784 1304785 1304786 1304787 1304788 1304789 1304790 1304791 1304792 1304793 [...] (3083 flits)
Class 0:
Remaining flits: 1304800 1304801 1327842 1327843 1327844 1327845 1327846 1327847 1327848 1327849 [...] (2408237 flits)
Measured flits: 1304800 1304801 1327842 1327843 1327844 1327845 1327846 1327847 1327848 1327849 [...] (2682 flits)
Class 0:
Remaining flits: 1327842 1327843 1327844 1327845 1327846 1327847 1327848 1327849 1327850 1327851 [...] (2446257 flits)
Measured flits: 1327842 1327843 1327844 1327845 1327846 1327847 1327848 1327849 1327850 1327851 [...] (2299 flits)
Class 0:
Remaining flits: 1330092 1330093 1330094 1330095 1330096 1330097 1330098 1330099 1330100 1330101 [...] (2476873 flits)
Measured flits: 1330092 1330093 1330094 1330095 1330096 1330097 1330098 1330099 1330100 1330101 [...] (1943 flits)
Class 0:
Remaining flits: 1330103 1330104 1330105 1330106 1330107 1330108 1330109 1350234 1350235 1350236 [...] (2503891 flits)
Measured flits: 1330103 1330104 1330105 1330106 1330107 1330108 1330109 1350234 1350235 1350236 [...] (1419 flits)
Class 0:
Remaining flits: 1362042 1362043 1362044 1362045 1362046 1362047 1362048 1362049 1362050 1362051 [...] (2529990 flits)
Measured flits: 1362042 1362043 1362044 1362045 1362046 1362047 1362048 1362049 1362050 1362051 [...] (840 flits)
Class 0:
Remaining flits: 1403226 1403227 1403228 1403229 1403230 1403231 1403232 1403233 1403234 1403235 [...] (2551370 flits)
Measured flits: 1403226 1403227 1403228 1403229 1403230 1403231 1403232 1403233 1403234 1403235 [...] (474 flits)
Class 0:
Remaining flits: 1435356 1435357 1435358 1435359 1435360 1435361 1435362 1435363 1435364 1435365 [...] (2572104 flits)
Measured flits: 1515728 1515729 1515730 1515731 1515732 1515733 1515734 1515735 1515736 1515737 [...] (180 flits)
Class 0:
Remaining flits: 1435356 1435357 1435358 1435359 1435360 1435361 1435362 1435363 1435364 1435365 [...] (2590423 flits)
Measured flits: 1569438 1569439 1569440 1569441 1569442 1569443 1569444 1569445 1569446 1569447 [...] (20 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1468962 1468963 1468964 1468965 1468966 1468967 1468968 1468969 1468970 1468971 [...] (2551090 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1554156 1554157 1554158 1554159 1554160 1554161 1554162 1554163 1554164 1554165 [...] (2500255 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1554156 1554157 1554158 1554159 1554160 1554161 1554162 1554163 1554164 1554165 [...] (2449905 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1767114 1767115 1767116 1767117 1767118 1767119 1767120 1767121 1767122 1767123 [...] (2399797 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1778668 1778669 1783422 1783423 1783424 1783425 1783426 1783427 1783428 1783429 [...] (2348995 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1813464 1813465 1813466 1813467 1813468 1813469 1813470 1813471 1813472 1813473 [...] (2298680 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1852290 1852291 1852292 1852293 1852294 1852295 1852296 1852297 1852298 1852299 [...] (2247981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1852306 1852307 1894050 1894051 1894052 1894053 1894054 1894055 1894056 1894057 [...] (2197601 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1910052 1910053 1910054 1910055 1910056 1910057 1910058 1910059 1910060 1910061 [...] (2146886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1930698 1930699 1930700 1930701 1930702 1930703 1930704 1930705 1930706 1930707 [...] (2096069 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1956006 1956007 1956008 1956009 1956010 1956011 1956012 1956013 1956014 1956015 [...] (2045694 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1975246 1975247 1979874 1979875 1979876 1979877 1979878 1979879 1979880 1979881 [...] (1995600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1979890 1979891 1996074 1996075 1996076 1996077 1996078 1996079 1996080 1996081 [...] (1945216 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1996074 1996075 1996076 1996077 1996078 1996079 1996080 1996081 1996082 1996083 [...] (1894267 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1996074 1996075 1996076 1996077 1996078 1996079 1996080 1996081 1996082 1996083 [...] (1843917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2068632 2068633 2068634 2068635 2068636 2068637 2068638 2068639 2068640 2068641 [...] (1793516 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2086200 2086201 2086202 2086203 2086204 2086205 2086206 2086207 2086208 2086209 [...] (1742916 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2086200 2086201 2086202 2086203 2086204 2086205 2086206 2086207 2086208 2086209 [...] (1692728 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2123496 2123497 2123498 2123499 2123500 2123501 2123502 2123503 2123504 2123505 [...] (1642079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2123496 2123497 2123498 2123499 2123500 2123501 2123502 2123503 2123504 2123505 [...] (1592022 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2141280 2141281 2141282 2141283 2141284 2141285 2141286 2141287 2141288 2141289 [...] (1541807 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2141280 2141281 2141282 2141283 2141284 2141285 2141286 2141287 2141288 2141289 [...] (1492919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2141280 2141281 2141282 2141283 2141284 2141285 2141286 2141287 2141288 2141289 [...] (1444011 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2141294 2141295 2141296 2141297 2209788 2209789 2209790 2209791 2209792 2209793 [...] (1395667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2209804 2209805 2234502 2234503 2234504 2234505 2234506 2234507 2234508 2234509 [...] (1347373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2234516 2234517 2234518 2234519 2288394 2288395 2288396 2288397 2288398 2288399 [...] (1299352 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2288394 2288395 2288396 2288397 2288398 2288399 2288400 2288401 2288402 2288403 [...] (1251366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2288394 2288395 2288396 2288397 2288398 2288399 2288400 2288401 2288402 2288403 [...] (1203661 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2333069 2351682 2351683 2351684 2351685 2351686 2351687 2351688 2351689 2351690 [...] (1155550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2351682 2351683 2351684 2351685 2351686 2351687 2351688 2351689 2351690 2351691 [...] (1107898 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2411154 2411155 2411156 2411157 2411158 2411159 2411160 2411161 2411162 2411163 [...] (1060159 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2411154 2411155 2411156 2411157 2411158 2411159 2411160 2411161 2411162 2411163 [...] (1012435 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2464092 2464093 2464094 2464095 2464096 2464097 2464098 2464099 2464100 2464101 [...] (964435 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2464092 2464093 2464094 2464095 2464096 2464097 2464098 2464099 2464100 2464101 [...] (916674 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2464108 2464109 2557132 2557133 2626252 2626253 2643102 2643103 2643104 2643105 [...] (869187 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2776554 2776555 2776556 2776557 2776558 2776559 2776560 2776561 2776562 2776563 [...] (821507 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2791638 2791639 2791640 2791641 2791642 2791643 2791644 2791645 2791646 2791647 [...] (774153 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2876500 2876501 2876502 2876503 2876504 2876505 2876506 2876507 2955690 2955691 [...] (726411 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2955690 2955691 2955692 2955693 2955694 2955695 2955696 2955697 2955698 2955699 [...] (678903 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972358 2972359 2972360 2972361 2972362 2972363 2972364 2972365 2972366 2972367 [...] (631073 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972358 2972359 2972360 2972361 2972362 2972363 2972364 2972365 2972366 2972367 [...] (583345 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972358 2972359 2972360 2972361 2972362 2972363 2972364 2972365 2972366 2972367 [...] (535537 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972358 2972359 2972360 2972361 2972362 2972363 2972364 2972365 2972366 2972367 [...] (487816 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2972358 2972359 2972360 2972361 2972362 2972363 2972364 2972365 2972366 2972367 [...] (440005 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3159846 3159847 3159848 3159849 3159850 3159851 3159852 3159853 3159854 3159855 [...] (392258 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3159846 3159847 3159848 3159849 3159850 3159851 3159852 3159853 3159854 3159855 [...] (344519 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3159858 3159859 3159860 3159861 3159862 3159863 3257928 3257929 3257930 3257931 [...] (296769 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3291102 3291103 3291104 3291105 3291106 3291107 3291108 3291109 3291110 3291111 [...] (249247 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3481668 3481669 3481670 3481671 3481672 3481673 3481674 3481675 3481676 3481677 [...] (201733 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3586662 3586663 3586664 3586665 3586666 3586667 3586668 3586669 3586670 3586671 [...] (156459 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3600180 3600181 3600182 3600183 3600184 3600185 3600186 3600187 3600188 3600189 [...] (113016 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3774834 3774835 3774836 3774837 3774838 3774839 3774840 3774841 3774842 3774843 [...] (73301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3847248 3847249 3847250 3847251 3847252 3847253 3847254 3847255 3847256 3847257 [...] (39710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3941568 3941569 3941570 3941571 3941572 3941573 3941574 3941575 3941576 3941577 [...] (14224 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3996324 3996325 3996326 3996327 3996328 3996329 3996330 3996331 3996332 3996333 [...] (2449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4161636 4161637 4161638 4161639 4161640 4161641 4161642 4161643 4161644 4161645 [...] (644 flits)
Measured flits: (0 flits)
Time taken is 103397 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9410.23 (1 samples)
	minimum = 22 (1 samples)
	maximum = 37399 (1 samples)
Network latency average = 8936.44 (1 samples)
	minimum = 22 (1 samples)
	maximum = 34021 (1 samples)
Flit latency average = 27350.4 (1 samples)
	minimum = 5 (1 samples)
	maximum = 69316 (1 samples)
Fragmentation average = 163.975 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1662 (1 samples)
Injected packet rate average = 0.0365521 (1 samples)
	minimum = 0.0151429 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.0166577 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.657914 (1 samples)
	minimum = 0.273286 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.300722 (1 samples)
	minimum = 0.232143 (1 samples)
	maximum = 0.378143 (1 samples)
Injected packet size average = 17.9994 (1 samples)
Accepted packet size average = 18.053 (1 samples)
Hops average = 5.07204 (1 samples)
Total run time 136.778
