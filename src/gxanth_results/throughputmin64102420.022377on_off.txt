BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 273.118
	minimum = 22
	maximum = 939
Network latency average = 195.608
	minimum = 22
	maximum = 747
Slowest packet = 33
Flit latency average = 166.283
	minimum = 5
	maximum = 747
Slowest flit = 16512
Fragmentation average = 35.7032
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0127396
	minimum = 0.003 (at node 174)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.237979
	minimum = 0.081 (at node 174)
	maximum = 0.395 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6803
Total in-flight flits = 27082 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 441.234
	minimum = 22
	maximum = 1735
Network latency average = 337.051
	minimum = 22
	maximum = 1376
Slowest packet = 33
Flit latency average = 306.677
	minimum = 5
	maximum = 1413
Slowest flit = 37999
Fragmentation average = 49.3536
	minimum = 0
	maximum = 632
Injected packet rate average = 0.0217604
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.0136901
	minimum = 0.008 (at node 81)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.39006
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253096
	minimum = 0.144 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9252
Accepted packet length average = 18.4875
Total in-flight flits = 53219 (0 measured)
latency change    = 0.381013
throughput change = 0.059729
Class 0:
Packet latency average = 961.88
	minimum = 22
	maximum = 2514
Network latency average = 801.966
	minimum = 22
	maximum = 2174
Slowest packet = 3488
Flit latency average = 758.516
	minimum = 5
	maximum = 2201
Slowest flit = 56593
Fragmentation average = 77.5543
	minimum = 0
	maximum = 698
Injected packet rate average = 0.0225417
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0152969
	minimum = 0.005 (at node 135)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.40575
	minimum = 0 (at node 17)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.274599
	minimum = 0.108 (at node 135)
	maximum = 0.51 (at node 16)
Injected packet length average = 18
Accepted packet length average = 17.9513
Total in-flight flits = 78400 (0 measured)
latency change    = 0.54128
throughput change = 0.0783055
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 360.386
	minimum = 22
	maximum = 1313
Network latency average = 228.795
	minimum = 22
	maximum = 992
Slowest packet = 12702
Flit latency average = 1059.41
	minimum = 5
	maximum = 2773
Slowest flit = 88856
Fragmentation average = 18.4801
	minimum = 0
	maximum = 215
Injected packet rate average = 0.0220677
	minimum = 0 (at node 97)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0153229
	minimum = 0.007 (at node 179)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.397099
	minimum = 0 (at node 97)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.275198
	minimum = 0.127 (at node 179)
	maximum = 0.466 (at node 129)
Injected packet length average = 17.9946
Accepted packet length average = 17.9599
Total in-flight flits = 101828 (67278 measured)
latency change    = 1.66903
throughput change = 0.00217646
Class 0:
Packet latency average = 832.83
	minimum = 22
	maximum = 2182
Network latency average = 714.281
	minimum = 22
	maximum = 1985
Slowest packet = 12702
Flit latency average = 1207.56
	minimum = 5
	maximum = 3616
Slowest flit = 101634
Fragmentation average = 34.4221
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0221224
	minimum = 0.0025 (at node 40)
	maximum = 0.054 (at node 95)
Accepted packet rate average = 0.0152708
	minimum = 0.0085 (at node 22)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.398062
	minimum = 0.045 (at node 40)
	maximum = 0.9725 (at node 95)
Accepted flit rate average= 0.275659
	minimum = 0.164 (at node 22)
	maximum = 0.4305 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0513
Total in-flight flits = 125457 (114964 measured)
latency change    = 0.567276
throughput change = 0.00167213
Class 0:
Packet latency average = 1250.1
	minimum = 22
	maximum = 3203
Network latency average = 1129.9
	minimum = 22
	maximum = 2982
Slowest packet = 12702
Flit latency average = 1363.92
	minimum = 5
	maximum = 4282
Slowest flit = 125831
Fragmentation average = 51.1329
	minimum = 0
	maximum = 651
Injected packet rate average = 0.0218993
	minimum = 0.005 (at node 90)
	maximum = 0.0513333 (at node 95)
Accepted packet rate average = 0.0152535
	minimum = 0.01 (at node 84)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.394309
	minimum = 0.09 (at node 90)
	maximum = 0.926 (at node 95)
Accepted flit rate average= 0.275453
	minimum = 0.189 (at node 84)
	maximum = 0.383 (at node 138)
Injected packet length average = 18.0055
Accepted packet length average = 18.0584
Total in-flight flits = 146791 (145342 measured)
latency change    = 0.333791
throughput change = 0.000746875
Draining remaining packets ...
Class 0:
Remaining flits: 143097 143098 143099 164070 164071 164072 164073 164074 164075 164076 [...] (99804 flits)
Measured flits: 231264 231265 231266 231267 231268 231269 231270 231271 231272 231273 [...] (99646 flits)
Class 0:
Remaining flits: 190182 190183 190184 190185 190186 190187 211064 211065 211066 211067 [...] (53978 flits)
Measured flits: 232465 232466 232467 232468 232469 234828 234829 234830 234831 234832 [...] (53937 flits)
Class 0:
Remaining flits: 295312 295313 295314 295315 295316 295317 295318 295319 295320 295321 [...] (17941 flits)
Measured flits: 295312 295313 295314 295315 295316 295317 295318 295319 295320 295321 [...] (17941 flits)
Class 0:
Remaining flits: 359007 359008 359009 362989 362990 362991 362992 362993 362994 362995 [...] (2591 flits)
Measured flits: 359007 359008 359009 362989 362990 362991 362992 362993 362994 362995 [...] (2591 flits)
Class 0:
Remaining flits: 453584 453585 453586 453587 453588 453589 453590 453591 453592 453593 [...] (16 flits)
Measured flits: 453584 453585 453586 453587 453588 453589 453590 453591 453592 453593 [...] (16 flits)
Time taken is 11018 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2409.88 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5957 (1 samples)
Network latency average = 2266.53 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5744 (1 samples)
Flit latency average = 2067.42 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5727 (1 samples)
Fragmentation average = 88.6559 (1 samples)
	minimum = 0 (1 samples)
	maximum = 765 (1 samples)
Injected packet rate average = 0.0218993 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0152535 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.394309 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.926 (1 samples)
Accepted flit rate average = 0.275453 (1 samples)
	minimum = 0.189 (1 samples)
	maximum = 0.383 (1 samples)
Injected packet size average = 18.0055 (1 samples)
Accepted packet size average = 18.0584 (1 samples)
Hops average = 5.08776 (1 samples)
Total run time 8.88613
