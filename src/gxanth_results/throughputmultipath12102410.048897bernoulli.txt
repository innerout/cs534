BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 318.102
	minimum = 27
	maximum = 943
Network latency average = 296.576
	minimum = 23
	maximum = 884
Slowest packet = 527
Flit latency average = 267.198
	minimum = 6
	maximum = 956
Slowest flit = 5382
Fragmentation average = 50.7895
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0116771
	minimum = 0.002 (at node 174)
	maximum = 0.021 (at node 34)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.218875
	minimum = 0.036 (at node 174)
	maximum = 0.378 (at node 34)
Injected packet length average = 17.8227
Accepted packet length average = 18.744
Total in-flight flits = 118500 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 601.342
	minimum = 23
	maximum = 1876
Network latency average = 571.712
	minimum = 23
	maximum = 1850
Slowest packet = 1123
Flit latency average = 539.942
	minimum = 6
	maximum = 1833
Slowest flit = 20231
Fragmentation average = 54.067
	minimum = 0
	maximum = 157
Injected packet rate average = 0.0469583
	minimum = 0.0335 (at node 170)
	maximum = 0.0555 (at node 39)
Accepted packet rate average = 0.0120521
	minimum = 0.0065 (at node 23)
	maximum = 0.0175 (at node 44)
Injected flit rate average = 0.841487
	minimum = 0.599 (at node 170)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.221044
	minimum = 0.123 (at node 23)
	maximum = 0.322 (at node 44)
Injected packet length average = 17.9199
Accepted packet length average = 18.3408
Total in-flight flits = 239731 (0 measured)
latency change    = 0.471013
throughput change = 0.00981374
Class 0:
Packet latency average = 1625.24
	minimum = 28
	maximum = 2790
Network latency average = 1574.63
	minimum = 23
	maximum = 2762
Slowest packet = 1443
Flit latency average = 1562.88
	minimum = 6
	maximum = 2746
Slowest flit = 36815
Fragmentation average = 63.2548
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0409271
	minimum = 0.013 (at node 80)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.010651
	minimum = 0.003 (at node 128)
	maximum = 0.019 (at node 177)
Injected flit rate average = 0.736339
	minimum = 0.23 (at node 80)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.191734
	minimum = 0.066 (at node 128)
	maximum = 0.326 (at node 187)
Injected packet length average = 17.9915
Accepted packet length average = 18.0015
Total in-flight flits = 344344 (0 measured)
latency change    = 0.629997
throughput change = 0.152867
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 445.969
	minimum = 32
	maximum = 1708
Network latency average = 40.0154
	minimum = 23
	maximum = 123
Slowest packet = 26157
Flit latency average = 2395.21
	minimum = 6
	maximum = 3662
Slowest flit = 49798
Fragmentation average = 9.23077
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0404115
	minimum = 0.012 (at node 64)
	maximum = 0.056 (at node 54)
Accepted packet rate average = 0.0100729
	minimum = 0.003 (at node 96)
	maximum = 0.02 (at node 120)
Injected flit rate average = 0.728172
	minimum = 0.213 (at node 144)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.181443
	minimum = 0.054 (at node 96)
	maximum = 0.364 (at node 69)
Injected packet length average = 18.0189
Accepted packet length average = 18.0129
Total in-flight flits = 449223 (137321 measured)
latency change    = 2.64428
throughput change = 0.0567213
Class 0:
Packet latency average = 599.637
	minimum = 30
	maximum = 2544
Network latency average = 133.363
	minimum = 23
	maximum = 1936
Slowest packet = 26157
Flit latency average = 2756.8
	minimum = 6
	maximum = 4548
Slowest flit = 66131
Fragmentation average = 9.55732
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0405443
	minimum = 0.013 (at node 144)
	maximum = 0.056 (at node 157)
Accepted packet rate average = 0.010026
	minimum = 0.0045 (at node 4)
	maximum = 0.018 (at node 120)
Injected flit rate average = 0.730234
	minimum = 0.2355 (at node 144)
	maximum = 1 (at node 22)
Accepted flit rate average= 0.180435
	minimum = 0.088 (at node 96)
	maximum = 0.325 (at node 120)
Injected packet length average = 18.0108
Accepted packet length average = 17.9966
Total in-flight flits = 555353 (274607 measured)
latency change    = 0.256268
throughput change = 0.00558546
Class 0:
Packet latency average = 1056.4
	minimum = 23
	maximum = 3388
Network latency average = 494.556
	minimum = 23
	maximum = 2928
Slowest packet = 26157
Flit latency average = 3126.72
	minimum = 6
	maximum = 5357
Slowest flit = 99225
Fragmentation average = 9.99287
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0404549
	minimum = 0.0136667 (at node 160)
	maximum = 0.0556667 (at node 126)
Accepted packet rate average = 0.0101493
	minimum = 0.00533333 (at node 17)
	maximum = 0.0156667 (at node 69)
Injected flit rate average = 0.727905
	minimum = 0.245 (at node 160)
	maximum = 1 (at node 126)
Accepted flit rate average= 0.182359
	minimum = 0.0996667 (at node 36)
	maximum = 0.279667 (at node 69)
Injected packet length average = 17.993
Accepted packet length average = 17.9677
Total in-flight flits = 658741 (409300 measured)
latency change    = 0.432378
throughput change = 0.0105532
Class 0:
Packet latency average = 1478.05
	minimum = 23
	maximum = 4105
Network latency average = 831.663
	minimum = 23
	maximum = 3848
Slowest packet = 26157
Flit latency average = 3503.28
	minimum = 6
	maximum = 6304
Slowest flit = 109638
Fragmentation average = 9.90562
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0387344
	minimum = 0.01375 (at node 160)
	maximum = 0.05175 (at node 49)
Accepted packet rate average = 0.0101549
	minimum = 0.00525 (at node 4)
	maximum = 0.0135 (at node 19)
Injected flit rate average = 0.697608
	minimum = 0.24825 (at node 160)
	maximum = 0.9325 (at node 49)
Accepted flit rate average= 0.182604
	minimum = 0.096 (at node 4)
	maximum = 0.24125 (at node 69)
Injected packet length average = 18.0101
Accepted packet length average = 17.9818
Total in-flight flits = 739550 (520315 measured)
latency change    = 0.285272
throughput change = 0.00134056
Draining remaining packets ...
Class 0:
Remaining flits: 110592 110593 110594 110595 110596 110597 110598 110599 110600 110601 [...] (707387 flits)
Measured flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (517725 flits)
Class 0:
Remaining flits: 133848 133849 133850 133851 133852 133853 133854 133855 133856 133857 [...] (675527 flits)
Measured flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (514569 flits)
Class 0:
Remaining flits: 149274 149275 149276 149277 149278 149279 149280 149281 149282 149283 [...] (643753 flits)
Measured flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (510627 flits)
Class 0:
Remaining flits: 173160 173161 173162 173163 173164 173165 173166 173167 173168 173169 [...] (611774 flits)
Measured flits: 466038 466039 466040 466041 466042 466043 466044 466045 466046 466047 [...] (504610 flits)
Class 0:
Remaining flits: 179280 179281 179282 179283 179284 179285 179286 179287 179288 179289 [...] (579054 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (495164 flits)
Class 0:
Remaining flits: 221741 230220 230221 230222 230223 230224 230225 230226 230227 230228 [...] (546416 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (482976 flits)
Class 0:
Remaining flits: 260874 260875 260876 260877 260878 260879 260880 260881 260882 260883 [...] (512537 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (467368 flits)
Class 0:
Remaining flits: 271026 271027 271028 271029 271030 271031 271032 271033 271034 271035 [...] (479541 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (448386 flits)
Class 0:
Remaining flits: 277902 277903 277904 277905 277906 277907 277908 277909 277910 277911 [...] (447157 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (426324 flits)
Class 0:
Remaining flits: 301599 301600 301601 301602 301603 301604 301605 301606 301607 318150 [...] (415933 flits)
Measured flits: 466056 466057 466058 466059 466060 466061 466062 466063 466064 466065 [...] (402508 flits)
Class 0:
Remaining flits: 331920 331921 331922 331923 331924 331925 331926 331927 331928 331929 [...] (386641 flits)
Measured flits: 466128 466129 466130 466131 466132 466133 466134 466135 466136 466137 [...] (378776 flits)
Class 0:
Remaining flits: 336978 336979 336980 336981 336982 336983 336984 336985 336986 336987 [...] (356991 flits)
Measured flits: 466128 466129 466130 466131 466132 466133 466134 466135 466136 466137 [...] (352696 flits)
Class 0:
Remaining flits: 336992 336993 336994 336995 373338 373339 373340 373341 373342 373343 [...] (327685 flits)
Measured flits: 466128 466129 466130 466131 466132 466133 466134 466135 466136 466137 [...] (325619 flits)
Class 0:
Remaining flits: 377367 377368 377369 384354 384355 384356 384357 384358 384359 384360 [...] (298024 flits)
Measured flits: 466470 466471 466472 466473 466474 466475 466476 466477 466478 466479 [...] (297092 flits)
Class 0:
Remaining flits: 394657 394658 394659 394660 394661 394662 394663 394664 394665 394666 [...] (269125 flits)
Measured flits: 466470 466471 466472 466473 466474 466475 466476 466477 466478 466479 [...] (268812 flits)
Class 0:
Remaining flits: 429498 429499 429500 429501 429502 429503 429504 429505 429506 429507 [...] (239476 flits)
Measured flits: 466470 466471 466472 466473 466474 466475 466476 466477 466478 466479 [...] (239314 flits)
Class 0:
Remaining flits: 446742 446743 446744 446745 446746 446747 446748 446749 446750 446751 [...] (209609 flits)
Measured flits: 467622 467623 467624 467625 467626 467627 467628 467629 467630 467631 [...] (209572 flits)
Class 0:
Remaining flits: 484902 484903 484904 484905 484906 484907 484908 484909 484910 484911 [...] (180287 flits)
Measured flits: 484902 484903 484904 484905 484906 484907 484908 484909 484910 484911 [...] (180287 flits)
Class 0:
Remaining flits: 527913 527914 527915 527916 527917 527918 527919 527920 527921 528048 [...] (150841 flits)
Measured flits: 527913 527914 527915 527916 527917 527918 527919 527920 527921 528048 [...] (150841 flits)
Class 0:
Remaining flits: 550854 550855 550856 550857 550858 550859 550860 550861 550862 550863 [...] (121280 flits)
Measured flits: 550854 550855 550856 550857 550858 550859 550860 550861 550862 550863 [...] (121280 flits)
Class 0:
Remaining flits: 592002 592003 592004 592005 592006 592007 592008 592009 592010 592011 [...] (92146 flits)
Measured flits: 592002 592003 592004 592005 592006 592007 592008 592009 592010 592011 [...] (92146 flits)
Class 0:
Remaining flits: 616847 616848 616849 616850 616851 616852 616853 616854 616855 616856 [...] (62268 flits)
Measured flits: 616847 616848 616849 616850 616851 616852 616853 616854 616855 616856 [...] (62268 flits)
Class 0:
Remaining flits: 653958 653959 653960 653961 653962 653963 653964 653965 653966 653967 [...] (32920 flits)
Measured flits: 653958 653959 653960 653961 653962 653963 653964 653965 653966 653967 [...] (32920 flits)
Class 0:
Remaining flits: 743562 743563 743564 743565 743566 743567 743568 743569 743570 743571 [...] (7661 flits)
Measured flits: 743562 743563 743564 743565 743566 743567 743568 743569 743570 743571 [...] (7661 flits)
Class 0:
Remaining flits: 967464 967465 967466 967467 967468 967469 967470 967471 967472 967473 [...] (51 flits)
Measured flits: 967464 967465 967466 967467 967468 967469 967470 967471 967472 967473 [...] (51 flits)
Time taken is 32051 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16815.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26156 (1 samples)
Network latency average = 16544.8 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26147 (1 samples)
Flit latency average = 12898.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 26130 (1 samples)
Fragmentation average = 67.5687 (1 samples)
	minimum = 0 (1 samples)
	maximum = 190 (1 samples)
Injected packet rate average = 0.0387344 (1 samples)
	minimum = 0.01375 (1 samples)
	maximum = 0.05175 (1 samples)
Accepted packet rate average = 0.0101549 (1 samples)
	minimum = 0.00525 (1 samples)
	maximum = 0.0135 (1 samples)
Injected flit rate average = 0.697608 (1 samples)
	minimum = 0.24825 (1 samples)
	maximum = 0.9325 (1 samples)
Accepted flit rate average = 0.182604 (1 samples)
	minimum = 0.096 (1 samples)
	maximum = 0.24125 (1 samples)
Injected packet size average = 18.0101 (1 samples)
Accepted packet size average = 17.9818 (1 samples)
Hops average = 5.22651 (1 samples)
Total run time 21.6747
