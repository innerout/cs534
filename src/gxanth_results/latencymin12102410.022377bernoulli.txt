BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 261.714
	minimum = 23
	maximum = 855
Network latency average = 256.948
	minimum = 23
	maximum = 839
Slowest packet = 521
Flit latency average = 221.06
	minimum = 6
	maximum = 836
Slowest flit = 10867
Fragmentation average = 56.9317
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.00984375
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.185427
	minimum = 0.071 (at node 174)
	maximum = 0.323 (at node 124)
Injected packet length average = 17.8322
Accepted packet length average = 18.837
Total in-flight flits = 41744 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 483.595
	minimum = 23
	maximum = 1782
Network latency average = 478.349
	minimum = 23
	maximum = 1749
Slowest packet = 970
Flit latency average = 440.31
	minimum = 6
	maximum = 1732
Slowest flit = 17477
Fragmentation average = 61.0464
	minimum = 0
	maximum = 175
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0101693
	minimum = 0.006 (at node 30)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.186703
	minimum = 0.112 (at node 30)
	maximum = 0.2875 (at node 152)
Injected packet length average = 17.9257
Accepted packet length average = 18.3595
Total in-flight flits = 82062 (0 measured)
latency change    = 0.458815
throughput change = 0.0068346
Class 0:
Packet latency average = 1153.62
	minimum = 23
	maximum = 2449
Network latency average = 1147.93
	minimum = 23
	maximum = 2449
Slowest packet = 2005
Flit latency average = 1114.43
	minimum = 6
	maximum = 2590
Slowest flit = 31015
Fragmentation average = 67.3021
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0103438
	minimum = 0.003 (at node 135)
	maximum = 0.02 (at node 162)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.186339
	minimum = 0.06 (at node 135)
	maximum = 0.329 (at node 123)
Injected packet length average = 18.0068
Accepted packet length average = 18.0146
Total in-flight flits = 123199 (0 measured)
latency change    = 0.580801
throughput change = 0.00195656
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 70.8498
	minimum = 23
	maximum = 958
Network latency average = 65.6036
	minimum = 23
	maximum = 957
Slowest packet = 12927
Flit latency average = 1603.27
	minimum = 6
	maximum = 3503
Slowest flit = 35367
Fragmentation average = 10.1832
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.010474
	minimum = 0.004 (at node 66)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.1885
	minimum = 0.064 (at node 146)
	maximum = 0.33 (at node 16)
Injected packet length average = 17.9469
Accepted packet length average = 17.997
Total in-flight flits = 166351 (73215 measured)
latency change    = 15.2826
throughput change = 0.0114666
Class 0:
Packet latency average = 189.132
	minimum = 23
	maximum = 1950
Network latency average = 183.283
	minimum = 23
	maximum = 1950
Slowest packet = 12903
Flit latency average = 1824
	minimum = 6
	maximum = 4152
Slowest flit = 62099
Fragmentation average = 15.6842
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0224844
	minimum = 0.014 (at node 51)
	maximum = 0.031 (at node 139)
Accepted packet rate average = 0.0102865
	minimum = 0.005 (at node 36)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.404581
	minimum = 0.252 (at node 171)
	maximum = 0.558 (at node 139)
Accepted flit rate average= 0.185203
	minimum = 0.094 (at node 86)
	maximum = 0.285 (at node 16)
Injected packet length average = 17.9939
Accepted packet length average = 18.0046
Total in-flight flits = 207493 (141477 measured)
latency change    = 0.625394
throughput change = 0.0178014
Class 0:
Packet latency average = 573.177
	minimum = 23
	maximum = 2964
Network latency average = 567.426
	minimum = 23
	maximum = 2952
Slowest packet = 12986
Flit latency average = 2061.22
	minimum = 6
	maximum = 5121
Slowest flit = 67579
Fragmentation average = 27.4599
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0225312
	minimum = 0.0163333 (at node 179)
	maximum = 0.03 (at node 59)
Accepted packet rate average = 0.0102622
	minimum = 0.00566667 (at node 36)
	maximum = 0.0166667 (at node 16)
Injected flit rate average = 0.405436
	minimum = 0.291333 (at node 179)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.185073
	minimum = 0.107 (at node 36)
	maximum = 0.295333 (at node 16)
Injected packet length average = 17.9944
Accepted packet length average = 18.0345
Total in-flight flits = 250201 (207939 measured)
latency change    = 0.670029
throughput change = 0.000703552
Class 0:
Packet latency average = 1131.12
	minimum = 23
	maximum = 3981
Network latency average = 1125.56
	minimum = 23
	maximum = 3981
Slowest packet = 12895
Flit latency average = 2304.39
	minimum = 6
	maximum = 5766
Slowest flit = 94962
Fragmentation average = 37.4251
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0224648
	minimum = 0.017 (at node 84)
	maximum = 0.0305 (at node 151)
Accepted packet rate average = 0.010306
	minimum = 0.0065 (at node 86)
	maximum = 0.01475 (at node 16)
Injected flit rate average = 0.404393
	minimum = 0.3045 (at node 84)
	maximum = 0.55 (at node 151)
Accepted flit rate average= 0.185849
	minimum = 0.11825 (at node 96)
	maximum = 0.2645 (at node 16)
Injected packet length average = 18.0012
Accepted packet length average = 18.0331
Total in-flight flits = 291021 (267049 measured)
latency change    = 0.493264
throughput change = 0.00417566
Class 0:
Packet latency average = 1752.78
	minimum = 23
	maximum = 4951
Network latency average = 1746.08
	minimum = 23
	maximum = 4951
Slowest packet = 12833
Flit latency average = 2555.26
	minimum = 6
	maximum = 6461
Slowest flit = 113813
Fragmentation average = 45.5536
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0223854
	minimum = 0.0166 (at node 77)
	maximum = 0.0294 (at node 151)
Accepted packet rate average = 0.0103125
	minimum = 0.0066 (at node 36)
	maximum = 0.0144 (at node 128)
Injected flit rate average = 0.402777
	minimum = 0.2988 (at node 77)
	maximum = 0.53 (at node 151)
Accepted flit rate average= 0.185777
	minimum = 0.1204 (at node 36)
	maximum = 0.2596 (at node 128)
Injected packet length average = 17.9928
Accepted packet length average = 18.0147
Total in-flight flits = 331673 (319459 measured)
latency change    = 0.354674
throughput change = 0.000386888
Class 0:
Packet latency average = 2342.14
	minimum = 23
	maximum = 5961
Network latency average = 2333.52
	minimum = 23
	maximum = 5961
Slowest packet = 12885
Flit latency average = 2811.51
	minimum = 6
	maximum = 7664
Slowest flit = 102591
Fragmentation average = 50.4609
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0221979
	minimum = 0.0161667 (at node 77)
	maximum = 0.0281667 (at node 34)
Accepted packet rate average = 0.0103073
	minimum = 0.00666667 (at node 163)
	maximum = 0.0143333 (at node 128)
Injected flit rate average = 0.39933
	minimum = 0.291 (at node 77)
	maximum = 0.509667 (at node 34)
Accepted flit rate average= 0.185604
	minimum = 0.119667 (at node 163)
	maximum = 0.257667 (at node 128)
Injected packet length average = 17.9895
Accepted packet length average = 18.0071
Total in-flight flits = 369715 (364479 measured)
latency change    = 0.251631
throughput change = 0.000931642
Class 0:
Packet latency average = 2892.87
	minimum = 23
	maximum = 6939
Network latency average = 2880.82
	minimum = 23
	maximum = 6924
Slowest packet = 13124
Flit latency average = 3081.05
	minimum = 6
	maximum = 8129
Slowest flit = 143416
Fragmentation average = 53.8699
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0219918
	minimum = 0.0158571 (at node 77)
	maximum = 0.0274286 (at node 59)
Accepted packet rate average = 0.0102798
	minimum = 0.00685714 (at node 36)
	maximum = 0.0142857 (at node 128)
Injected flit rate average = 0.395686
	minimum = 0.285429 (at node 77)
	maximum = 0.492714 (at node 59)
Accepted flit rate average= 0.185081
	minimum = 0.124143 (at node 36)
	maximum = 0.255857 (at node 128)
Injected packet length average = 17.9924
Accepted packet length average = 18.0044
Total in-flight flits = 406494 (404541 measured)
latency change    = 0.190378
throughput change = 0.00282614
Draining all recorded packets ...
Class 0:
Remaining flits: 175896 175897 175898 175899 175900 175901 175902 175903 175904 175905 [...] (444997 flits)
Measured flits: 230688 230689 230690 230691 230692 230693 230694 230695 230696 230697 [...] (382303 flits)
Class 0:
Remaining flits: 187290 187291 187292 187293 187294 187295 187296 187297 187298 187299 [...] (480542 flits)
Measured flits: 231498 231499 231500 231501 231502 231503 231504 231505 231506 231507 [...] (354525 flits)
Class 0:
Remaining flits: 202752 202753 202754 202755 202756 202757 202758 202759 202760 202761 [...] (517792 flits)
Measured flits: 231498 231499 231500 231501 231502 231503 231504 231505 231506 231507 [...] (325037 flits)
Class 0:
Remaining flits: 256302 256303 256304 256305 256306 256307 256308 256309 256310 256311 [...] (554256 flits)
Measured flits: 256302 256303 256304 256305 256306 256307 256308 256309 256310 256311 [...] (296360 flits)
Class 0:
Remaining flits: 266832 266833 266834 266835 266836 266837 266838 266839 266840 266841 [...] (591392 flits)
Measured flits: 266832 266833 266834 266835 266836 266837 266838 266839 266840 266841 [...] (266884 flits)
Class 0:
Remaining flits: 266846 266847 266848 266849 271494 271495 271496 271497 271498 271499 [...] (628092 flits)
Measured flits: 266846 266847 266848 266849 271494 271495 271496 271497 271498 271499 [...] (236504 flits)
Class 0:
Remaining flits: 271494 271495 271496 271497 271498 271499 271500 271501 271502 271503 [...] (666201 flits)
Measured flits: 271494 271495 271496 271497 271498 271499 271500 271501 271502 271503 [...] (207322 flits)
Class 0:
Remaining flits: 271494 271495 271496 271497 271498 271499 271500 271501 271502 271503 [...] (701678 flits)
Measured flits: 271494 271495 271496 271497 271498 271499 271500 271501 271502 271503 [...] (179183 flits)
Class 0:
Remaining flits: 330948 330949 330950 330951 330952 330953 330954 330955 330956 330957 [...] (737336 flits)
Measured flits: 330948 330949 330950 330951 330952 330953 330954 330955 330956 330957 [...] (151244 flits)
Class 0:
Remaining flits: 330948 330949 330950 330951 330952 330953 330954 330955 330956 330957 [...] (772261 flits)
Measured flits: 330948 330949 330950 330951 330952 330953 330954 330955 330956 330957 [...] (125557 flits)
Class 0:
Remaining flits: 388962 388963 388964 388965 388966 388967 388968 388969 388970 388971 [...] (808348 flits)
Measured flits: 388962 388963 388964 388965 388966 388967 388968 388969 388970 388971 [...] (100744 flits)
Class 0:
Remaining flits: 388962 388963 388964 388965 388966 388967 388968 388969 388970 388971 [...] (842382 flits)
Measured flits: 388962 388963 388964 388965 388966 388967 388968 388969 388970 388971 [...] (78898 flits)
Class 0:
Remaining flits: 432738 432739 432740 432741 432742 432743 432744 432745 432746 432747 [...] (871372 flits)
Measured flits: 432738 432739 432740 432741 432742 432743 432744 432745 432746 432747 [...] (60542 flits)
Class 0:
Remaining flits: 447012 447013 447014 447015 447016 447017 447018 447019 447020 447021 [...] (898809 flits)
Measured flits: 447012 447013 447014 447015 447016 447017 447018 447019 447020 447021 [...] (44501 flits)
Class 0:
Remaining flits: 459162 459163 459164 459165 459166 459167 459168 459169 459170 459171 [...] (920860 flits)
Measured flits: 459162 459163 459164 459165 459166 459167 459168 459169 459170 459171 [...] (31885 flits)
Class 0:
Remaining flits: 460728 460729 460730 460731 460732 460733 460734 460735 460736 460737 [...] (934855 flits)
Measured flits: 460728 460729 460730 460731 460732 460733 460734 460735 460736 460737 [...] (22353 flits)
Class 0:
Remaining flits: 499734 499735 499736 499737 499738 499739 499740 499741 499742 499743 [...] (946566 flits)
Measured flits: 499734 499735 499736 499737 499738 499739 499740 499741 499742 499743 [...] (15163 flits)
Class 0:
Remaining flits: 520956 520957 520958 520959 520960 520961 520962 520963 520964 520965 [...] (951662 flits)
Measured flits: 520956 520957 520958 520959 520960 520961 520962 520963 520964 520965 [...] (10400 flits)
Class 0:
Remaining flits: 520956 520957 520958 520959 520960 520961 520962 520963 520964 520965 [...] (955059 flits)
Measured flits: 520956 520957 520958 520959 520960 520961 520962 520963 520964 520965 [...] (6884 flits)
Class 0:
Remaining flits: 570708 570709 570710 570711 570712 570713 570714 570715 570716 570717 [...] (955499 flits)
Measured flits: 570708 570709 570710 570711 570712 570713 570714 570715 570716 570717 [...] (4547 flits)
Class 0:
Remaining flits: 598086 598087 598088 598089 598090 598091 598092 598093 598094 598095 [...] (957100 flits)
Measured flits: 598086 598087 598088 598089 598090 598091 598092 598093 598094 598095 [...] (2633 flits)
Class 0:
Remaining flits: 630036 630037 630038 630039 630040 630041 630042 630043 630044 630045 [...] (955612 flits)
Measured flits: 630036 630037 630038 630039 630040 630041 630042 630043 630044 630045 [...] (1646 flits)
Class 0:
Remaining flits: 647964 647965 647966 647967 647968 647969 647970 647971 647972 647973 [...] (953911 flits)
Measured flits: 647964 647965 647966 647967 647968 647969 647970 647971 647972 647973 [...] (971 flits)
Class 0:
Remaining flits: 650016 650017 650018 650019 650020 650021 650022 650023 650024 650025 [...] (955102 flits)
Measured flits: 650016 650017 650018 650019 650020 650021 650022 650023 650024 650025 [...] (594 flits)
Class 0:
Remaining flits: 682398 682399 682400 682401 682402 682403 682404 682405 682406 682407 [...] (953111 flits)
Measured flits: 682398 682399 682400 682401 682402 682403 682404 682405 682406 682407 [...] (269 flits)
Class 0:
Remaining flits: 703782 703783 703784 703785 703786 703787 703788 703789 703790 703791 [...] (948414 flits)
Measured flits: 703782 703783 703784 703785 703786 703787 703788 703789 703790 703791 [...] (144 flits)
Class 0:
Remaining flits: 712890 712891 712892 712893 712894 712895 712896 712897 712898 712899 [...] (946079 flits)
Measured flits: 712890 712891 712892 712893 712894 712895 712896 712897 712898 712899 [...] (72 flits)
Class 0:
Remaining flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (941824 flits)
Measured flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (36 flits)
Class 0:
Remaining flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (938436 flits)
Measured flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (36 flits)
Class 0:
Remaining flits: 747939 747940 747941 747942 747943 747944 747945 747946 747947 747948 [...] (936443 flits)
Measured flits: 747939 747940 747941 747942 747943 747944 747945 747946 747947 747948 [...] (33 flits)
Class 0:
Remaining flits: 759582 759583 759584 759585 759586 759587 759588 759589 759590 759591 [...] (931664 flits)
Measured flits: 759582 759583 759584 759585 759586 759587 759588 759589 759590 759591 [...] (18 flits)
Class 0:
Remaining flits: 759582 759583 759584 759585 759586 759587 759588 759589 759590 759591 [...] (931624 flits)
Measured flits: 759582 759583 759584 759585 759586 759587 759588 759589 759590 759591 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 816462 816463 816464 816465 816466 816467 816468 816469 816470 816471 [...] (900114 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 841824 841825 841826 841827 841828 841829 841830 841831 841832 841833 [...] (869444 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 865656 865657 865658 865659 865660 865661 865662 865663 865664 865665 [...] (838654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 905310 905311 905312 905313 905314 905315 905316 905317 905318 905319 [...] (807679 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 949392 949393 949394 949395 949396 949397 949398 949399 949400 949401 [...] (775792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 953946 953947 953948 953949 953950 953951 953952 953953 953954 953955 [...] (744471 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 953946 953947 953948 953949 953950 953951 953952 953953 953954 953955 [...] (712561 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 998190 998191 998192 998193 998194 998195 998196 998197 998198 998199 [...] (681160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 998190 998191 998192 998193 998194 998195 998196 998197 998198 998199 [...] (649518 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1046592 1046593 1046594 1046595 1046596 1046597 1046598 1046599 1046600 1046601 [...] (617800 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1101312 1101313 1101314 1101315 1101316 1101317 1101318 1101319 1101320 1101321 [...] (586981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1113912 1113913 1113914 1113915 1113916 1113917 1113918 1113919 1113920 1113921 [...] (555545 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1113912 1113913 1113914 1113915 1113916 1113917 1113918 1113919 1113920 1113921 [...] (523947 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1113912 1113913 1113914 1113915 1113916 1113917 1113918 1113919 1113920 1113921 [...] (492858 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1217268 1217269 1217270 1217271 1217272 1217273 1217274 1217275 1217276 1217277 [...] (461995 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1217268 1217269 1217270 1217271 1217272 1217273 1217274 1217275 1217276 1217277 [...] (431354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1243908 1243909 1243910 1243911 1243912 1243913 1243914 1243915 1243916 1243917 [...] (400348 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1250352 1250353 1250354 1250355 1250356 1250357 1250358 1250359 1250360 1250361 [...] (370006 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274274 1274275 1274276 1274277 1274278 1274279 1274280 1274281 1274282 1274283 [...] (340133 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1274274 1274275 1274276 1274277 1274278 1274279 1274280 1274281 1274282 1274283 [...] (310385 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1322694 1322695 1322696 1322697 1322698 1322699 1322700 1322701 1322702 1322703 [...] (281000 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1327806 1327807 1327808 1327809 1327810 1327811 1327812 1327813 1327814 1327815 [...] (252368 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1356678 1356679 1356680 1356681 1356682 1356683 1356684 1356685 1356686 1356687 [...] (222688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1395162 1395163 1395164 1395165 1395166 1395167 1395168 1395169 1395170 1395171 [...] (194275 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1402128 1402129 1402130 1402131 1402132 1402133 1402134 1402135 1402136 1402137 [...] (165682 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1402128 1402129 1402130 1402131 1402132 1402133 1402134 1402135 1402136 1402137 [...] (138091 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1468550 1468551 1468552 1468553 1468554 1468555 1468556 1468557 1468558 1468559 [...] (110412 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1483200 1483201 1483202 1483203 1483204 1483205 1483206 1483207 1483208 1483209 [...] (82647 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505592 1505593 1505594 1505595 1505596 1505597 1505598 1505599 1505600 1505601 [...] (57372 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1628014 1628015 1628016 1628017 1628018 1628019 1628020 1628021 1628022 1628023 [...] (33523 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1743912 1743913 1743914 1743915 1743916 1743917 1743918 1743919 1743920 1743921 [...] (17057 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1743918 1743919 1743920 1743921 1743922 1743923 1743924 1743925 1743926 1743927 [...] (6820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1946592 1946593 1946594 1946595 1946596 1946597 1946598 1946599 1946600 1946601 [...] (1927 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2190942 2190943 2190944 2190945 2190946 2190947 2190948 2190949 2190950 2190951 [...] (177 flits)
Measured flits: (0 flits)
Time taken is 76551 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8720.62 (1 samples)
	minimum = 23 (1 samples)
	maximum = 32409 (1 samples)
Network latency average = 8672.68 (1 samples)
	minimum = 23 (1 samples)
	maximum = 32409 (1 samples)
Flit latency average = 19063.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 50937 (1 samples)
Fragmentation average = 71.7304 (1 samples)
	minimum = 0 (1 samples)
	maximum = 189 (1 samples)
Injected packet rate average = 0.0219918 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0274286 (1 samples)
Accepted packet rate average = 0.0102798 (1 samples)
	minimum = 0.00685714 (1 samples)
	maximum = 0.0142857 (1 samples)
Injected flit rate average = 0.395686 (1 samples)
	minimum = 0.285429 (1 samples)
	maximum = 0.492714 (1 samples)
Accepted flit rate average = 0.185081 (1 samples)
	minimum = 0.124143 (1 samples)
	maximum = 0.255857 (1 samples)
Injected packet size average = 17.9924 (1 samples)
Accepted packet size average = 18.0044 (1 samples)
Hops average = 5.08029 (1 samples)
Total run time 55.7863
