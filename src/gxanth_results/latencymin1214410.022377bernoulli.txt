BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 262.376
	minimum = 23
	maximum = 842
Network latency average = 257.562
	minimum = 23
	maximum = 842
Slowest packet = 595
Flit latency average = 222.123
	minimum = 6
	maximum = 861
Slowest flit = 9385
Fragmentation average = 58.9301
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0220677
	minimum = 0.013 (at node 75)
	maximum = 0.033 (at node 113)
Accepted packet rate average = 0.0096875
	minimum = 0.003 (at node 150)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.393516
	minimum = 0.234 (at node 75)
	maximum = 0.594 (at node 113)
Accepted flit rate average= 0.183115
	minimum = 0.067 (at node 174)
	maximum = 0.339 (at node 70)
Injected packet length average = 17.8322
Accepted packet length average = 18.9022
Total in-flight flits = 41162 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 502.956
	minimum = 23
	maximum = 1700
Network latency average = 493.672
	minimum = 23
	maximum = 1700
Slowest packet = 1238
Flit latency average = 455.367
	minimum = 6
	maximum = 1783
Slowest flit = 15098
Fragmentation average = 62.9593
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0213984
	minimum = 0.013 (at node 40)
	maximum = 0.0295 (at node 11)
Accepted packet rate average = 0.00999219
	minimum = 0.005 (at node 174)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.38343
	minimum = 0.234 (at node 40)
	maximum = 0.531 (at node 11)
Accepted flit rate average= 0.183602
	minimum = 0.09 (at node 174)
	maximum = 0.266 (at node 166)
Injected packet length average = 17.9186
Accepted packet length average = 18.3745
Total in-flight flits = 77583 (0 measured)
latency change    = 0.478332
throughput change = 0.00265237
Class 0:
Packet latency average = 1258.33
	minimum = 23
	maximum = 2570
Network latency average = 1222.32
	minimum = 23
	maximum = 2569
Slowest packet = 727
Flit latency average = 1187.73
	minimum = 6
	maximum = 2552
Slowest flit = 13103
Fragmentation average = 67.9075
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0183437
	minimum = 0 (at node 56)
	maximum = 0.032 (at node 121)
Accepted packet rate average = 0.00956771
	minimum = 0.003 (at node 2)
	maximum = 0.019 (at node 19)
Injected flit rate average = 0.330578
	minimum = 0 (at node 56)
	maximum = 0.582 (at node 121)
Accepted flit rate average= 0.173474
	minimum = 0.045 (at node 2)
	maximum = 0.351 (at node 19)
Injected packet length average = 18.0213
Accepted packet length average = 18.1312
Total in-flight flits = 108464 (0 measured)
latency change    = 0.600299
throughput change = 0.0583811
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 682.385
	minimum = 24
	maximum = 2066
Network latency average = 161.029
	minimum = 23
	maximum = 913
Slowest packet = 11798
Flit latency average = 1760.67
	minimum = 6
	maximum = 3426
Slowest flit = 43338
Fragmentation average = 16.8269
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0130156
	minimum = 0 (at node 23)
	maximum = 0.03 (at node 6)
Accepted packet rate average = 0.00952083
	minimum = 0.004 (at node 117)
	maximum = 0.017 (at node 50)
Injected flit rate average = 0.234604
	minimum = 0 (at node 34)
	maximum = 0.54 (at node 6)
Accepted flit rate average= 0.169156
	minimum = 0.072 (at node 117)
	maximum = 0.281 (at node 50)
Injected packet length average = 18.0248
Accepted packet length average = 17.767
Total in-flight flits = 121814 (43885 measured)
latency change    = 0.844019
throughput change = 0.025525
Class 0:
Packet latency average = 1216.98
	minimum = 24
	maximum = 3108
Network latency average = 640.702
	minimum = 23
	maximum = 1910
Slowest packet = 11798
Flit latency average = 2022.09
	minimum = 6
	maximum = 4074
Slowest flit = 69816
Fragmentation average = 38.8462
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0112448
	minimum = 0 (at node 35)
	maximum = 0.024 (at node 163)
Accepted packet rate average = 0.00907552
	minimum = 0.005 (at node 22)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.202687
	minimum = 0 (at node 35)
	maximum = 0.4365 (at node 163)
Accepted flit rate average= 0.162656
	minimum = 0.0785 (at node 22)
	maximum = 0.25 (at node 145)
Injected packet length average = 18.025
Accepted packet length average = 17.9225
Total in-flight flits = 124934 (73222 measured)
latency change    = 0.43928
throughput change = 0.0399616
Class 0:
Packet latency average = 1950.2
	minimum = 24
	maximum = 4314
Network latency average = 1289.29
	minimum = 23
	maximum = 2935
Slowest packet = 11798
Flit latency average = 2262.86
	minimum = 6
	maximum = 4980
Slowest flit = 77330
Fragmentation average = 54.8447
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0103819
	minimum = 0.000666667 (at node 35)
	maximum = 0.022 (at node 29)
Accepted packet rate average = 0.00898438
	minimum = 0.00566667 (at node 190)
	maximum = 0.0133333 (at node 16)
Injected flit rate average = 0.187026
	minimum = 0.012 (at node 35)
	maximum = 0.398 (at node 29)
Accepted flit rate average= 0.161285
	minimum = 0.100333 (at node 190)
	maximum = 0.240667 (at node 16)
Injected packet length average = 18.0145
Accepted packet length average = 17.9517
Total in-flight flits = 124590 (94357 measured)
latency change    = 0.375971
throughput change = 0.00850377
Class 0:
Packet latency average = 2624.24
	minimum = 24
	maximum = 5049
Network latency average = 1866.82
	minimum = 23
	maximum = 3914
Slowest packet = 11798
Flit latency average = 2483.83
	minimum = 6
	maximum = 5853
Slowest flit = 86769
Fragmentation average = 62.415
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0099974
	minimum = 0.00275 (at node 8)
	maximum = 0.021 (at node 59)
Accepted packet rate average = 0.00903516
	minimum = 0.00575 (at node 132)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.179796
	minimum = 0.0495 (at node 35)
	maximum = 0.3775 (at node 59)
Accepted flit rate average= 0.162141
	minimum = 0.1005 (at node 132)
	maximum = 0.233 (at node 16)
Injected packet length average = 17.9842
Accepted packet length average = 17.9455
Total in-flight flits = 123278 (107703 measured)
latency change    = 0.256853
throughput change = 0.00527877
Class 0:
Packet latency average = 3176.04
	minimum = 24
	maximum = 6047
Network latency average = 2287.21
	minimum = 23
	maximum = 4947
Slowest packet = 11798
Flit latency average = 2659.41
	minimum = 6
	maximum = 6721
Slowest flit = 96129
Fragmentation average = 65.4531
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0098
	minimum = 0.0024 (at node 35)
	maximum = 0.0184 (at node 59)
Accepted packet rate average = 0.00902812
	minimum = 0.006 (at node 163)
	maximum = 0.0122 (at node 129)
Injected flit rate average = 0.176404
	minimum = 0.0418 (at node 35)
	maximum = 0.3328 (at node 59)
Accepted flit rate average= 0.162161
	minimum = 0.1062 (at node 163)
	maximum = 0.2196 (at node 129)
Injected packet length average = 18.0004
Accepted packet length average = 17.9618
Total in-flight flits = 123357 (115813 measured)
latency change    = 0.173738
throughput change = 0.000128473
Class 0:
Packet latency average = 3682.08
	minimum = 24
	maximum = 6787
Network latency average = 2619.3
	minimum = 23
	maximum = 5917
Slowest packet = 11798
Flit latency average = 2814.86
	minimum = 6
	maximum = 7160
Slowest flit = 116027
Fragmentation average = 66.6736
	minimum = 0
	maximum = 168
Injected packet rate average = 0.00957552
	minimum = 0.00283333 (at node 35)
	maximum = 0.0171667 (at node 75)
Accepted packet rate average = 0.00903385
	minimum = 0.0065 (at node 36)
	maximum = 0.0121667 (at node 129)
Injected flit rate average = 0.172443
	minimum = 0.051 (at node 35)
	maximum = 0.306167 (at node 75)
Accepted flit rate average= 0.162431
	minimum = 0.117667 (at node 113)
	maximum = 0.22 (at node 129)
Injected packet length average = 18.0087
Accepted packet length average = 17.9803
Total in-flight flits = 121431 (118167 measured)
latency change    = 0.137433
throughput change = 0.00166203
Class 0:
Packet latency average = 4108.83
	minimum = 24
	maximum = 7754
Network latency average = 2882.15
	minimum = 23
	maximum = 6868
Slowest packet = 11798
Flit latency average = 2954.9
	minimum = 6
	maximum = 8064
Slowest flit = 98892
Fragmentation average = 67.9284
	minimum = 0
	maximum = 168
Injected packet rate average = 0.00950074
	minimum = 0.00385714 (at node 35)
	maximum = 0.0174286 (at node 123)
Accepted packet rate average = 0.00903869
	minimum = 0.00685714 (at node 36)
	maximum = 0.0118571 (at node 129)
Injected flit rate average = 0.171013
	minimum = 0.0674286 (at node 35)
	maximum = 0.313 (at node 123)
Accepted flit rate average= 0.162508
	minimum = 0.121714 (at node 132)
	maximum = 0.217 (at node 129)
Injected packet length average = 17.9999
Accepted packet length average = 17.9792
Total in-flight flits = 121155 (120038 measured)
latency change    = 0.103862
throughput change = 0.000472351
Draining all recorded packets ...
Class 0:
Remaining flits: 160326 160327 160328 160329 160330 160331 160332 160333 160334 160335 [...] (121108 flits)
Measured flits: 212706 212707 212708 212709 212710 212711 212712 212713 212714 212715 [...] (120771 flits)
Class 0:
Remaining flits: 179766 179767 179768 179769 179770 179771 179772 179773 179774 179775 [...] (120571 flits)
Measured flits: 212706 212707 212708 212709 212710 212711 212712 212713 212714 212715 [...] (120445 flits)
Class 0:
Remaining flits: 190548 190549 190550 190551 190552 190553 190554 190555 190556 190557 [...] (118051 flits)
Measured flits: 212717 212718 212719 212720 212721 212722 212723 224982 224983 224984 [...] (118015 flits)
Class 0:
Remaining flits: 240030 240031 240032 240033 240034 240035 240036 240037 240038 240039 [...] (120531 flits)
Measured flits: 240030 240031 240032 240033 240034 240035 240036 240037 240038 240039 [...] (120045 flits)
Class 0:
Remaining flits: 271535 271536 271537 271538 271539 271540 271541 271542 271543 271544 [...] (120032 flits)
Measured flits: 271535 271536 271537 271538 271539 271540 271541 271542 271543 271544 [...] (119042 flits)
Class 0:
Remaining flits: 306720 306721 306722 306723 306724 306725 306726 306727 306728 306729 [...] (119943 flits)
Measured flits: 306720 306721 306722 306723 306724 306725 306726 306727 306728 306729 [...] (117777 flits)
Class 0:
Remaining flits: 308520 308521 308522 308523 308524 308525 308526 308527 308528 308529 [...] (119082 flits)
Measured flits: 308520 308521 308522 308523 308524 308525 308526 308527 308528 308529 [...] (114304 flits)
Class 0:
Remaining flits: 340398 340399 340400 340401 340402 340403 340404 340405 340406 340407 [...] (120372 flits)
Measured flits: 340398 340399 340400 340401 340402 340403 340404 340405 340406 340407 [...] (109906 flits)
Class 0:
Remaining flits: 368076 368077 368078 368079 368080 368081 408389 408390 408391 408392 [...] (118797 flits)
Measured flits: 368076 368077 368078 368079 368080 368081 408389 408390 408391 408392 [...] (101144 flits)
Class 0:
Remaining flits: 414270 414271 414272 414273 414274 414275 414276 414277 414278 414279 [...] (119427 flits)
Measured flits: 414270 414271 414272 414273 414274 414275 414276 414277 414278 414279 [...] (90531 flits)
Class 0:
Remaining flits: 414270 414271 414272 414273 414274 414275 414276 414277 414278 414279 [...] (120297 flits)
Measured flits: 414270 414271 414272 414273 414274 414275 414276 414277 414278 414279 [...] (78572 flits)
Class 0:
Remaining flits: 417078 417079 417080 417081 417082 417083 417084 417085 417086 417087 [...] (122733 flits)
Measured flits: 417078 417079 417080 417081 417082 417083 417084 417085 417086 417087 [...] (66726 flits)
Class 0:
Remaining flits: 419148 419149 419150 419151 419152 419153 419154 419155 419156 419157 [...] (124171 flits)
Measured flits: 419148 419149 419150 419151 419152 419153 419154 419155 419156 419157 [...] (54514 flits)
Class 0:
Remaining flits: 527166 527167 527168 527169 527170 527171 527172 527173 527174 527175 [...] (122095 flits)
Measured flits: 527166 527167 527168 527169 527170 527171 527172 527173 527174 527175 [...] (43048 flits)
Class 0:
Remaining flits: 558630 558631 558632 558633 558634 558635 558636 558637 558638 558639 [...] (119810 flits)
Measured flits: 558630 558631 558632 558633 558634 558635 558636 558637 558638 558639 [...] (32096 flits)
Class 0:
Remaining flits: 558638 558639 558640 558641 558642 558643 558644 558645 558646 558647 [...] (120687 flits)
Measured flits: 558638 558639 558640 558641 558642 558643 558644 558645 558646 558647 [...] (23671 flits)
Class 0:
Remaining flits: 572436 572437 572438 572439 572440 572441 572442 572443 572444 572445 [...] (119855 flits)
Measured flits: 572436 572437 572438 572439 572440 572441 572442 572443 572444 572445 [...] (16985 flits)
Class 0:
Remaining flits: 607500 607501 607502 607503 607504 607505 607506 607507 607508 607509 [...] (116925 flits)
Measured flits: 607500 607501 607502 607503 607504 607505 607506 607507 607508 607509 [...] (11290 flits)
Class 0:
Remaining flits: 607500 607501 607502 607503 607504 607505 607506 607507 607508 607509 [...] (116825 flits)
Measured flits: 607500 607501 607502 607503 607504 607505 607506 607507 607508 607509 [...] (7086 flits)
Class 0:
Remaining flits: 704070 704071 704072 704073 704074 704075 704076 704077 704078 704079 [...] (119375 flits)
Measured flits: 718794 718795 718796 718797 718798 718799 718800 718801 718802 718803 [...] (5155 flits)
Class 0:
Remaining flits: 705042 705043 705044 705045 705046 705047 705048 705049 705050 705051 [...] (119689 flits)
Measured flits: 718794 718795 718796 718797 718798 718799 718800 718801 718802 718803 [...] (3434 flits)
Class 0:
Remaining flits: 768204 768205 768206 768207 768208 768209 768210 768211 768212 768213 [...] (119934 flits)
Measured flits: 768204 768205 768206 768207 768208 768209 768210 768211 768212 768213 [...] (2430 flits)
Class 0:
Remaining flits: 768204 768205 768206 768207 768208 768209 768210 768211 768212 768213 [...] (119590 flits)
Measured flits: 768204 768205 768206 768207 768208 768209 768210 768211 768212 768213 [...] (1626 flits)
Class 0:
Remaining flits: 828162 828163 828164 828165 828166 828167 828168 828169 828170 828171 [...] (119210 flits)
Measured flits: 828162 828163 828164 828165 828166 828167 828168 828169 828170 828171 [...] (963 flits)
Class 0:
Remaining flits: 828163 828164 828165 828166 828167 828168 828169 828170 828171 828172 [...] (118057 flits)
Measured flits: 828163 828164 828165 828166 828167 828168 828169 828170 828171 828172 [...] (695 flits)
Class 0:
Remaining flits: 919584 919585 919586 919587 919588 919589 919590 919591 919592 919593 [...] (117414 flits)
Measured flits: 976174 976175 981774 981775 981776 981777 981778 981779 981780 981781 [...] (434 flits)
Class 0:
Remaining flits: 919584 919585 919586 919587 919588 919589 919590 919591 919592 919593 [...] (117904 flits)
Measured flits: 1088316 1088317 1088318 1088319 1088320 1088321 1088322 1088323 1088324 1088325 [...] (270 flits)
Class 0:
Remaining flits: 932796 932797 932798 932799 932800 932801 932802 932803 932804 932805 [...] (120074 flits)
Measured flits: 1114992 1114993 1114994 1114995 1114996 1114997 1114998 1114999 1115000 1115001 [...] (90 flits)
Class 0:
Remaining flits: 932796 932797 932798 932799 932800 932801 932802 932803 932804 932805 [...] (118946 flits)
Measured flits: 1212367 1212368 1212369 1212370 1212371 1216476 1216477 1216478 1216479 1216480 [...] (23 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 974286 974287 974288 974289 974290 974291 974292 974293 974294 974295 [...] (90100 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1037538 1037539 1037540 1037541 1037542 1037543 1037544 1037545 1037546 1037547 [...] (62116 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1037543 1037544 1037545 1037546 1037547 1037548 1037549 1037550 1037551 1037552 [...] (33501 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1104858 1104859 1104860 1104861 1104862 1104863 1104864 1104865 1104866 1104867 [...] (9202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1203141 1203142 1203143 1203144 1203145 1203146 1203147 1203148 1203149 1203150 [...] (174 flits)
Measured flits: (0 flits)
Time taken is 44478 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9512.98 (1 samples)
	minimum = 24 (1 samples)
	maximum = 29393 (1 samples)
Network latency average = 3824.79 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14175 (1 samples)
Flit latency average = 3720.97 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14141 (1 samples)
Fragmentation average = 71.2987 (1 samples)
	minimum = 0 (1 samples)
	maximum = 177 (1 samples)
Injected packet rate average = 0.00950074 (1 samples)
	minimum = 0.00385714 (1 samples)
	maximum = 0.0174286 (1 samples)
Accepted packet rate average = 0.00903869 (1 samples)
	minimum = 0.00685714 (1 samples)
	maximum = 0.0118571 (1 samples)
Injected flit rate average = 0.171013 (1 samples)
	minimum = 0.0674286 (1 samples)
	maximum = 0.313 (1 samples)
Accepted flit rate average = 0.162508 (1 samples)
	minimum = 0.121714 (1 samples)
	maximum = 0.217 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 17.9792 (1 samples)
Hops average = 5.04926 (1 samples)
Total run time 33.0914
