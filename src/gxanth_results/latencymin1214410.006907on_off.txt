BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.006907
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 119.655
	minimum = 23
	maximum = 495
Network latency average = 94.318
	minimum = 23
	maximum = 495
Slowest packet = 491
Flit latency average = 63.5675
	minimum = 6
	maximum = 478
Slowest flit = 8855
Fragmentation average = 27.0759
	minimum = 0
	maximum = 90
Injected packet rate average = 0.00764583
	minimum = 0 (at node 3)
	maximum = 0.029 (at node 97)
Accepted packet rate average = 0.00679687
	minimum = 0.001 (at node 41)
	maximum = 0.013 (at node 48)
Injected flit rate average = 0.136563
	minimum = 0 (at node 3)
	maximum = 0.522 (at node 97)
Accepted flit rate average= 0.125557
	minimum = 0.018 (at node 41)
	maximum = 0.249 (at node 70)
Injected packet length average = 17.861
Accepted packet length average = 18.4728
Total in-flight flits = 2317 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 133.6
	minimum = 23
	maximum = 611
Network latency average = 106.848
	minimum = 23
	maximum = 553
Slowest packet = 491
Flit latency average = 73.6892
	minimum = 6
	maximum = 536
Slowest flit = 22409
Fragmentation average = 30.6056
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0072474
	minimum = 0.0005 (at node 9)
	maximum = 0.019 (at node 161)
Accepted packet rate average = 0.00685417
	minimum = 0.0035 (at node 25)
	maximum = 0.0115 (at node 44)
Injected flit rate average = 0.129961
	minimum = 0.009 (at node 9)
	maximum = 0.342 (at node 161)
Accepted flit rate average= 0.125344
	minimum = 0.063 (at node 25)
	maximum = 0.214 (at node 44)
Injected packet length average = 17.9321
Accepted packet length average = 18.2872
Total in-flight flits = 1962 (0 measured)
latency change    = 0.104379
throughput change = 0.00170365
Class 0:
Packet latency average = 154.051
	minimum = 23
	maximum = 626
Network latency average = 126.942
	minimum = 23
	maximum = 523
Slowest packet = 2433
Flit latency average = 90.2491
	minimum = 6
	maximum = 506
Slowest flit = 52955
Fragmentation average = 38.2034
	minimum = 0
	maximum = 111
Injected packet rate average = 0.00692188
	minimum = 0 (at node 9)
	maximum = 0.033 (at node 20)
Accepted packet rate average = 0.00683854
	minimum = 0.001 (at node 23)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.124344
	minimum = 0 (at node 9)
	maximum = 0.586 (at node 20)
Accepted flit rate average= 0.122635
	minimum = 0.018 (at node 23)
	maximum = 0.275 (at node 22)
Injected packet length average = 17.9639
Accepted packet length average = 17.933
Total in-flight flits = 2338 (0 measured)
latency change    = 0.132753
throughput change = 0.0220844
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 157.21
	minimum = 23
	maximum = 701
Network latency average = 127.34
	minimum = 23
	maximum = 551
Slowest packet = 4156
Flit latency average = 95.3991
	minimum = 6
	maximum = 552
Slowest flit = 84868
Fragmentation average = 36.1463
	minimum = 0
	maximum = 106
Injected packet rate average = 0.00702083
	minimum = 0 (at node 47)
	maximum = 0.031 (at node 79)
Accepted packet rate average = 0.00717188
	minimum = 0.001 (at node 121)
	maximum = 0.015 (at node 63)
Injected flit rate average = 0.126255
	minimum = 0 (at node 47)
	maximum = 0.57 (at node 79)
Accepted flit rate average= 0.128922
	minimum = 0.018 (at node 121)
	maximum = 0.257 (at node 63)
Injected packet length average = 17.9829
Accepted packet length average = 17.976
Total in-flight flits = 1849 (1849 measured)
latency change    = 0.0200934
throughput change = 0.0487618
Class 0:
Packet latency average = 154.427
	minimum = 23
	maximum = 806
Network latency average = 126.566
	minimum = 23
	maximum = 661
Slowest packet = 4156
Flit latency average = 92.5162
	minimum = 6
	maximum = 704
Slowest flit = 106470
Fragmentation average = 36.6684
	minimum = 0
	maximum = 106
Injected packet rate average = 0.00723177
	minimum = 0.0005 (at node 143)
	maximum = 0.0265 (at node 79)
Accepted packet rate average = 0.00710938
	minimum = 0.0035 (at node 10)
	maximum = 0.012 (at node 34)
Injected flit rate average = 0.130234
	minimum = 0.009 (at node 143)
	maximum = 0.4765 (at node 79)
Accepted flit rate average= 0.128076
	minimum = 0.058 (at node 184)
	maximum = 0.216 (at node 34)
Injected packet length average = 18.0086
Accepted packet length average = 18.015
Total in-flight flits = 3143 (3143 measured)
latency change    = 0.018022
throughput change = 0.00660824
Class 0:
Packet latency average = 155.178
	minimum = 23
	maximum = 933
Network latency average = 127.705
	minimum = 23
	maximum = 741
Slowest packet = 4156
Flit latency average = 92.628
	minimum = 6
	maximum = 724
Slowest flit = 106487
Fragmentation average = 36.7186
	minimum = 0
	maximum = 120
Injected packet rate average = 0.00697917
	minimum = 0.000666667 (at node 143)
	maximum = 0.0213333 (at node 79)
Accepted packet rate average = 0.00705903
	minimum = 0.00333333 (at node 153)
	maximum = 0.0123333 (at node 34)
Injected flit rate average = 0.125767
	minimum = 0.012 (at node 143)
	maximum = 0.388 (at node 79)
Accepted flit rate average= 0.126845
	minimum = 0.06 (at node 153)
	maximum = 0.222 (at node 34)
Injected packet length average = 18.0204
Accepted packet length average = 17.9693
Total in-flight flits = 1635 (1635 measured)
latency change    = 0.00484058
throughput change = 0.00969711
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6779 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 156.373 (1 samples)
	minimum = 23 (1 samples)
	maximum = 933 (1 samples)
Network latency average = 128.466 (1 samples)
	minimum = 23 (1 samples)
	maximum = 741 (1 samples)
Flit latency average = 93.6452 (1 samples)
	minimum = 6 (1 samples)
	maximum = 724 (1 samples)
Fragmentation average = 36.7494 (1 samples)
	minimum = 0 (1 samples)
	maximum = 120 (1 samples)
Injected packet rate average = 0.00697917 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.0213333 (1 samples)
Accepted packet rate average = 0.00705903 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.0123333 (1 samples)
Injected flit rate average = 0.125767 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.388 (1 samples)
Accepted flit rate average = 0.126845 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.222 (1 samples)
Injected packet size average = 18.0204 (1 samples)
Accepted packet size average = 17.9693 (1 samples)
Hops average = 5.07035 (1 samples)
Total run time 2.38262
