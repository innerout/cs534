BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 118.131
	minimum = 22
	maximum = 614
Network latency average = 83.9611
	minimum = 22
	maximum = 453
Slowest packet = 45
Flit latency average = 58.4325
	minimum = 5
	maximum = 436
Slowest flit = 12455
Fragmentation average = 16.4623
	minimum = 0
	maximum = 287
Injected packet rate average = 0.00939062
	minimum = 0 (at node 26)
	maximum = 0.033 (at node 37)
Accepted packet rate average = 0.00869792
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.167693
	minimum = 0 (at node 26)
	maximum = 0.594 (at node 37)
Accepted flit rate average= 0.159
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 48)
Injected packet length average = 17.8575
Accepted packet length average = 18.2802
Total in-flight flits = 1926 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 123.92
	minimum = 22
	maximum = 718
Network latency average = 87.4992
	minimum = 22
	maximum = 534
Slowest packet = 45
Flit latency average = 62.2481
	minimum = 5
	maximum = 517
Slowest flit = 41109
Fragmentation average = 15.7333
	minimum = 0
	maximum = 287
Injected packet rate average = 0.00889063
	minimum = 0.0005 (at node 184)
	maximum = 0.021 (at node 20)
Accepted packet rate average = 0.00847656
	minimum = 0.005 (at node 30)
	maximum = 0.0135 (at node 44)
Injected flit rate average = 0.159396
	minimum = 0.009 (at node 184)
	maximum = 0.371 (at node 20)
Accepted flit rate average= 0.153768
	minimum = 0.09 (at node 30)
	maximum = 0.243 (at node 44)
Injected packet length average = 17.9285
Accepted packet length average = 18.1404
Total in-flight flits = 2405 (0 measured)
latency change    = 0.0467203
throughput change = 0.0340237
Class 0:
Packet latency average = 146.798
	minimum = 25
	maximum = 703
Network latency average = 108.182
	minimum = 22
	maximum = 524
Slowest packet = 2891
Flit latency average = 82.7843
	minimum = 5
	maximum = 507
Slowest flit = 76535
Fragmentation average = 17.5558
	minimum = 0
	maximum = 218
Injected packet rate average = 0.00902604
	minimum = 0 (at node 5)
	maximum = 0.036 (at node 162)
Accepted packet rate average = 0.00882813
	minimum = 0.001 (at node 153)
	maximum = 0.017 (at node 6)
Injected flit rate average = 0.162359
	minimum = 0 (at node 5)
	maximum = 0.648 (at node 162)
Accepted flit rate average= 0.159432
	minimum = 0.018 (at node 153)
	maximum = 0.315 (at node 6)
Injected packet length average = 17.9879
Accepted packet length average = 18.0596
Total in-flight flits = 2988 (0 measured)
latency change    = 0.155847
throughput change = 0.0355264
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 145.987
	minimum = 22
	maximum = 855
Network latency average = 105.861
	minimum = 22
	maximum = 513
Slowest packet = 5160
Flit latency average = 90.3763
	minimum = 5
	maximum = 625
Slowest flit = 79919
Fragmentation average = 14.6188
	minimum = 0
	maximum = 172
Injected packet rate average = 0.00923958
	minimum = 0 (at node 47)
	maximum = 0.032 (at node 74)
Accepted packet rate average = 0.00929167
	minimum = 0.001 (at node 184)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.166469
	minimum = 0 (at node 47)
	maximum = 0.583 (at node 74)
Accepted flit rate average= 0.166828
	minimum = 0.018 (at node 184)
	maximum = 0.342 (at node 16)
Injected packet length average = 18.0169
Accepted packet length average = 17.9546
Total in-flight flits = 2889 (2889 measured)
latency change    = 0.00555847
throughput change = 0.0443321
Class 0:
Packet latency average = 147.464
	minimum = 22
	maximum = 855
Network latency average = 107.407
	minimum = 22
	maximum = 513
Slowest packet = 5160
Flit latency average = 86.4655
	minimum = 5
	maximum = 625
Slowest flit = 79919
Fragmentation average = 15.9076
	minimum = 0
	maximum = 185
Injected packet rate average = 0.00933333
	minimum = 0.001 (at node 49)
	maximum = 0.0235 (at node 37)
Accepted packet rate average = 0.00936719
	minimum = 0.0045 (at node 190)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.167958
	minimum = 0.018 (at node 49)
	maximum = 0.43 (at node 37)
Accepted flit rate average= 0.168773
	minimum = 0.081 (at node 190)
	maximum = 0.333 (at node 16)
Injected packet length average = 17.9955
Accepted packet length average = 18.0175
Total in-flight flits = 2691 (2691 measured)
latency change    = 0.0100203
throughput change = 0.0115262
Class 0:
Packet latency average = 140.77
	minimum = 22
	maximum = 855
Network latency average = 102.443
	minimum = 22
	maximum = 513
Slowest packet = 5160
Flit latency average = 80.3071
	minimum = 5
	maximum = 625
Slowest flit = 79919
Fragmentation average = 15.6952
	minimum = 0
	maximum = 185
Injected packet rate average = 0.00898611
	minimum = 0.001 (at node 49)
	maximum = 0.019 (at node 58)
Accepted packet rate average = 0.00910938
	minimum = 0.00533333 (at node 190)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.16178
	minimum = 0.018 (at node 49)
	maximum = 0.342 (at node 58)
Accepted flit rate average= 0.163771
	minimum = 0.098 (at node 190)
	maximum = 0.294 (at node 16)
Injected packet length average = 18.0033
Accepted packet length average = 17.9783
Total in-flight flits = 1824 (1824 measured)
latency change    = 0.0475549
throughput change = 0.0305464
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6756 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 142.09 (1 samples)
	minimum = 22 (1 samples)
	maximum = 855 (1 samples)
Network latency average = 102.887 (1 samples)
	minimum = 22 (1 samples)
	maximum = 513 (1 samples)
Flit latency average = 79.1403 (1 samples)
	minimum = 5 (1 samples)
	maximum = 625 (1 samples)
Fragmentation average = 15.763 (1 samples)
	minimum = 0 (1 samples)
	maximum = 185 (1 samples)
Injected packet rate average = 0.00898611 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.019 (1 samples)
Accepted packet rate average = 0.00910938 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.16178 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.342 (1 samples)
Accepted flit rate average = 0.163771 (1 samples)
	minimum = 0.098 (1 samples)
	maximum = 0.294 (1 samples)
Injected packet size average = 18.0033 (1 samples)
Accepted packet size average = 17.9783 (1 samples)
Hops average = 5.08822 (1 samples)
Total run time 3.87049
