BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.755
	minimum = 27
	maximum = 972
Network latency average = 244.012
	minimum = 23
	maximum = 815
Slowest packet = 3
Flit latency average = 205.827
	minimum = 6
	maximum = 884
Slowest flit = 8464
Fragmentation average = 55.318
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0187604
	minimum = 0 (at node 114)
	maximum = 0.052 (at node 186)
Accepted packet rate average = 0.00905729
	minimum = 0.002 (at node 150)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.334646
	minimum = 0 (at node 114)
	maximum = 0.92 (at node 186)
Accepted flit rate average= 0.17099
	minimum = 0.051 (at node 150)
	maximum = 0.288 (at node 34)
Injected packet length average = 17.8379
Accepted packet length average = 18.8787
Total in-flight flits = 32042 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 523.997
	minimum = 25
	maximum = 1901
Network latency average = 442.818
	minimum = 23
	maximum = 1704
Slowest packet = 3
Flit latency average = 400.403
	minimum = 6
	maximum = 1753
Slowest flit = 16259
Fragmentation average = 62.9628
	minimum = 0
	maximum = 150
Injected packet rate average = 0.018151
	minimum = 0.0005 (at node 140)
	maximum = 0.04 (at node 150)
Accepted packet rate average = 0.00946094
	minimum = 0.0045 (at node 174)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.325409
	minimum = 0.009 (at node 140)
	maximum = 0.7185 (at node 150)
Accepted flit rate average= 0.173771
	minimum = 0.081 (at node 174)
	maximum = 0.261 (at node 22)
Injected packet length average = 17.9278
Accepted packet length average = 18.3672
Total in-flight flits = 58894 (0 measured)
latency change    = 0.42222
throughput change = 0.0160053
Class 0:
Packet latency average = 1151.6
	minimum = 29
	maximum = 2932
Network latency average = 1018.65
	minimum = 23
	maximum = 2673
Slowest packet = 2306
Flit latency average = 985.828
	minimum = 6
	maximum = 2715
Slowest flit = 18314
Fragmentation average = 70.7106
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0157552
	minimum = 0 (at node 27)
	maximum = 0.051 (at node 171)
Accepted packet rate average = 0.00944792
	minimum = 0.003 (at node 32)
	maximum = 0.017 (at node 102)
Injected flit rate average = 0.283469
	minimum = 0 (at node 27)
	maximum = 0.923 (at node 171)
Accepted flit rate average= 0.17101
	minimum = 0.064 (at node 190)
	maximum = 0.296 (at node 102)
Injected packet length average = 17.9921
Accepted packet length average = 18.1003
Total in-flight flits = 80870 (0 measured)
latency change    = 0.544982
throughput change = 0.0161418
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 424.208
	minimum = 27
	maximum = 2713
Network latency average = 234.284
	minimum = 23
	maximum = 984
Slowest packet = 10038
Flit latency average = 1427.9
	minimum = 6
	maximum = 3470
Slowest flit = 32542
Fragmentation average = 32.5127
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0145
	minimum = 0 (at node 31)
	maximum = 0.045 (at node 165)
Accepted packet rate average = 0.00948958
	minimum = 0.003 (at node 72)
	maximum = 0.018 (at node 99)
Injected flit rate average = 0.260562
	minimum = 0 (at node 63)
	maximum = 0.807 (at node 165)
Accepted flit rate average= 0.169687
	minimum = 0.054 (at node 117)
	maximum = 0.312 (at node 99)
Injected packet length average = 17.9698
Accepted packet length average = 17.8814
Total in-flight flits = 98870 (46078 measured)
latency change    = 1.7147
throughput change = 0.00779619
Class 0:
Packet latency average = 1000.98
	minimum = 27
	maximum = 3705
Network latency average = 667.272
	minimum = 23
	maximum = 1974
Slowest packet = 10038
Flit latency average = 1643.28
	minimum = 6
	maximum = 4352
Slowest flit = 33713
Fragmentation average = 52.068
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0134635
	minimum = 0 (at node 99)
	maximum = 0.0325 (at node 165)
Accepted packet rate average = 0.00938542
	minimum = 0.0055 (at node 58)
	maximum = 0.0165 (at node 120)
Injected flit rate average = 0.242284
	minimum = 0 (at node 99)
	maximum = 0.5895 (at node 165)
Accepted flit rate average= 0.168932
	minimum = 0.093 (at node 138)
	maximum = 0.2945 (at node 120)
Injected packet length average = 17.9956
Accepted packet length average = 17.9994
Total in-flight flits = 110032 (78549 measured)
latency change    = 0.576207
throughput change = 0.00447048
Class 0:
Packet latency average = 1522.9
	minimum = 27
	maximum = 5174
Network latency average = 1137.33
	minimum = 23
	maximum = 2956
Slowest packet = 10038
Flit latency average = 1847.01
	minimum = 6
	maximum = 5274
Slowest flit = 45431
Fragmentation average = 60.9241
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0124601
	minimum = 0.00166667 (at node 140)
	maximum = 0.025 (at node 130)
Accepted packet rate average = 0.00931771
	minimum = 0.00533333 (at node 2)
	maximum = 0.0136667 (at node 50)
Injected flit rate average = 0.224361
	minimum = 0.03 (at node 161)
	maximum = 0.45 (at node 130)
Accepted flit rate average= 0.167748
	minimum = 0.092 (at node 138)
	maximum = 0.245333 (at node 50)
Injected packet length average = 18.0064
Accepted packet length average = 18.0032
Total in-flight flits = 114819 (97539 measured)
latency change    = 0.342716
throughput change = 0.00705836
Draining remaining packets ...
Class 0:
Remaining flits: 56861 57420 57421 57422 57423 57424 57425 57426 57427 57428 [...] (85107 flits)
Measured flits: 180486 180487 180488 180489 180490 180491 180492 180493 180494 180495 [...] (76824 flits)
Class 0:
Remaining flits: 67626 67627 67628 67629 67630 67631 67632 67633 67634 67635 [...] (56582 flits)
Measured flits: 180756 180757 180758 180759 180760 180761 180762 180763 180764 180765 [...] (53079 flits)
Class 0:
Remaining flits: 95634 95635 95636 95637 95638 95639 95640 95641 95642 95643 [...] (28145 flits)
Measured flits: 180972 180973 180974 180975 180976 180977 180978 180979 180980 180981 [...] (26666 flits)
Class 0:
Remaining flits: 115902 115903 115904 115905 115906 115907 115908 115909 115910 115911 [...] (5878 flits)
Measured flits: 184986 184987 184988 184989 184990 184991 184992 184993 184994 184995 [...] (5663 flits)
Time taken is 10986 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3559.67 (1 samples)
	minimum = 27 (1 samples)
	maximum = 8491 (1 samples)
Network latency average = 2968.54 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7286 (1 samples)
Flit latency average = 2865.46 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8324 (1 samples)
Fragmentation average = 67.1048 (1 samples)
	minimum = 0 (1 samples)
	maximum = 161 (1 samples)
Injected packet rate average = 0.0124601 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.025 (1 samples)
Accepted packet rate average = 0.00931771 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0136667 (1 samples)
Injected flit rate average = 0.224361 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.45 (1 samples)
Accepted flit rate average = 0.167748 (1 samples)
	minimum = 0.092 (1 samples)
	maximum = 0.245333 (1 samples)
Injected packet size average = 18.0064 (1 samples)
Accepted packet size average = 18.0032 (1 samples)
Hops average = 5.11001 (1 samples)
Total run time 6.68465
