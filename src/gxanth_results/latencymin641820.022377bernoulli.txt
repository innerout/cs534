BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 218.384
	minimum = 22
	maximum = 711
Network latency average = 213.001
	minimum = 22
	maximum = 711
Slowest packet = 1165
Flit latency average = 180.575
	minimum = 5
	maximum = 694
Slowest flit = 20987
Fragmentation average = 41.8356
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0223437
	minimum = 0.011 (at node 165)
	maximum = 0.037 (at node 152)
Accepted packet rate average = 0.0135885
	minimum = 0.004 (at node 174)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.398557
	minimum = 0.198 (at node 165)
	maximum = 0.651 (at node 152)
Accepted flit rate average= 0.254443
	minimum = 0.075 (at node 174)
	maximum = 0.468 (at node 44)
Injected packet length average = 17.8375
Accepted packet length average = 18.7248
Total in-flight flits = 28367 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 382.295
	minimum = 22
	maximum = 1321
Network latency average = 375.961
	minimum = 22
	maximum = 1252
Slowest packet = 2520
Flit latency average = 339.126
	minimum = 5
	maximum = 1369
Slowest flit = 48213
Fragmentation average = 55.3119
	minimum = 0
	maximum = 343
Injected packet rate average = 0.0220833
	minimum = 0.014 (at node 117)
	maximum = 0.0325 (at node 157)
Accepted packet rate average = 0.0142005
	minimum = 0.0075 (at node 116)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.396279
	minimum = 0.252 (at node 117)
	maximum = 0.585 (at node 157)
Accepted flit rate average= 0.261552
	minimum = 0.139 (at node 116)
	maximum = 0.369 (at node 22)
Injected packet length average = 17.9447
Accepted packet length average = 18.4185
Total in-flight flits = 52294 (0 measured)
latency change    = 0.428754
throughput change = 0.0271815
Class 0:
Packet latency average = 848.241
	minimum = 22
	maximum = 1900
Network latency average = 836.506
	minimum = 22
	maximum = 1897
Slowest packet = 4377
Flit latency average = 791.793
	minimum = 5
	maximum = 1880
Slowest flit = 79523
Fragmentation average = 84.5515
	minimum = 0
	maximum = 460
Injected packet rate average = 0.0207813
	minimum = 0.009 (at node 120)
	maximum = 0.033 (at node 154)
Accepted packet rate average = 0.015026
	minimum = 0.004 (at node 147)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.373042
	minimum = 0.162 (at node 120)
	maximum = 0.594 (at node 154)
Accepted flit rate average= 0.272813
	minimum = 0.085 (at node 190)
	maximum = 0.502 (at node 34)
Injected packet length average = 17.9509
Accepted packet length average = 18.156
Total in-flight flits = 71932 (0 measured)
latency change    = 0.549309
throughput change = 0.0412753
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 255.567
	minimum = 22
	maximum = 1865
Network latency average = 160.246
	minimum = 22
	maximum = 952
Slowest packet = 12487
Flit latency average = 1067.96
	minimum = 5
	maximum = 2445
Slowest flit = 113147
Fragmentation average = 13.0258
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0199479
	minimum = 0.005 (at node 154)
	maximum = 0.031 (at node 79)
Accepted packet rate average = 0.0151406
	minimum = 0.006 (at node 86)
	maximum = 0.026 (at node 90)
Injected flit rate average = 0.357771
	minimum = 0.078 (at node 154)
	maximum = 0.547 (at node 79)
Accepted flit rate average= 0.274453
	minimum = 0.117 (at node 36)
	maximum = 0.489 (at node 90)
Injected packet length average = 17.9352
Accepted packet length average = 18.1269
Total in-flight flits = 88321 (62352 measured)
latency change    = 2.31905
throughput change = 0.0059778
Class 0:
Packet latency average = 1009.06
	minimum = 22
	maximum = 2480
Network latency average = 880.707
	minimum = 22
	maximum = 1989
Slowest packet = 12487
Flit latency average = 1200.4
	minimum = 5
	maximum = 3055
Slowest flit = 147524
Fragmentation average = 64.2742
	minimum = 0
	maximum = 427
Injected packet rate average = 0.0189219
	minimum = 0.006 (at node 124)
	maximum = 0.0265 (at node 25)
Accepted packet rate average = 0.0151563
	minimum = 0.009 (at node 2)
	maximum = 0.0215 (at node 19)
Injected flit rate average = 0.339836
	minimum = 0.1005 (at node 124)
	maximum = 0.4755 (at node 79)
Accepted flit rate average= 0.27399
	minimum = 0.1685 (at node 2)
	maximum = 0.3845 (at node 107)
Injected packet length average = 17.96
Accepted packet length average = 18.0777
Total in-flight flits = 97922 (95172 measured)
latency change    = 0.746728
throughput change = 0.00169182
Class 0:
Packet latency average = 1451.67
	minimum = 22
	maximum = 3110
Network latency average = 1292.24
	minimum = 22
	maximum = 2977
Slowest packet = 12487
Flit latency average = 1322.64
	minimum = 5
	maximum = 3510
Slowest flit = 159929
Fragmentation average = 84.283
	minimum = 0
	maximum = 479
Injected packet rate average = 0.0179983
	minimum = 0.009 (at node 4)
	maximum = 0.0243333 (at node 46)
Accepted packet rate average = 0.0150694
	minimum = 0.0106667 (at node 2)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.323424
	minimum = 0.163667 (at node 4)
	maximum = 0.439667 (at node 46)
Accepted flit rate average= 0.272731
	minimum = 0.191 (at node 79)
	maximum = 0.369 (at node 128)
Injected packet length average = 17.9697
Accepted packet length average = 18.0983
Total in-flight flits = 101895 (101794 measured)
latency change    = 0.304895
throughput change = 0.0046151
Class 0:
Packet latency average = 1713.69
	minimum = 22
	maximum = 4171
Network latency average = 1494.23
	minimum = 22
	maximum = 3534
Slowest packet = 12487
Flit latency average = 1429.55
	minimum = 5
	maximum = 3590
Slowest flit = 253147
Fragmentation average = 91.3606
	minimum = 0
	maximum = 502
Injected packet rate average = 0.0172357
	minimum = 0.0105 (at node 92)
	maximum = 0.0235 (at node 71)
Accepted packet rate average = 0.0150273
	minimum = 0.01075 (at node 2)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.309762
	minimum = 0.191 (at node 92)
	maximum = 0.423 (at node 71)
Accepted flit rate average= 0.271516
	minimum = 0.19875 (at node 2)
	maximum = 0.362 (at node 128)
Injected packet length average = 17.9721
Accepted packet length average = 18.0681
Total in-flight flits = 102034 (102016 measured)
latency change    = 0.152898
throughput change = 0.0044759
Class 0:
Packet latency average = 1927.82
	minimum = 22
	maximum = 4554
Network latency average = 1620.61
	minimum = 22
	maximum = 4330
Slowest packet = 12794
Flit latency average = 1517.99
	minimum = 5
	maximum = 4313
Slowest flit = 263951
Fragmentation average = 95.3473
	minimum = 0
	maximum = 576
Injected packet rate average = 0.0170031
	minimum = 0.0096 (at node 88)
	maximum = 0.0214 (at node 67)
Accepted packet rate average = 0.0149958
	minimum = 0.0108 (at node 42)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.305523
	minimum = 0.1706 (at node 88)
	maximum = 0.3846 (at node 71)
Accepted flit rate average= 0.27098
	minimum = 0.199 (at node 42)
	maximum = 0.3542 (at node 128)
Injected packet length average = 17.9686
Accepted packet length average = 18.0704
Total in-flight flits = 105929 (105911 measured)
latency change    = 0.111073
throughput change = 0.00197585
Class 0:
Packet latency average = 2116.72
	minimum = 22
	maximum = 6061
Network latency average = 1709.11
	minimum = 22
	maximum = 5868
Slowest packet = 12862
Flit latency average = 1586.83
	minimum = 5
	maximum = 6054
Slowest flit = 217573
Fragmentation average = 98.4822
	minimum = 0
	maximum = 576
Injected packet rate average = 0.016605
	minimum = 0.0103333 (at node 36)
	maximum = 0.021 (at node 71)
Accepted packet rate average = 0.0149835
	minimum = 0.0113333 (at node 42)
	maximum = 0.0191667 (at node 128)
Injected flit rate average = 0.298552
	minimum = 0.187333 (at node 36)
	maximum = 0.3755 (at node 71)
Accepted flit rate average= 0.270458
	minimum = 0.206 (at node 42)
	maximum = 0.343833 (at node 128)
Injected packet length average = 17.9796
Accepted packet length average = 18.0504
Total in-flight flits = 105082 (105079 measured)
latency change    = 0.0892398
throughput change = 0.00192959
Class 0:
Packet latency average = 2296.48
	minimum = 22
	maximum = 6243
Network latency average = 1774.37
	minimum = 22
	maximum = 5868
Slowest packet = 12862
Flit latency average = 1644.25
	minimum = 5
	maximum = 6054
Slowest flit = 217573
Fragmentation average = 100.703
	minimum = 0
	maximum = 576
Injected packet rate average = 0.0163132
	minimum = 0.00957143 (at node 148)
	maximum = 0.0204286 (at node 190)
Accepted packet rate average = 0.0149799
	minimum = 0.012 (at node 42)
	maximum = 0.0187143 (at node 103)
Injected flit rate average = 0.293288
	minimum = 0.172286 (at node 148)
	maximum = 0.367714 (at node 190)
Accepted flit rate average= 0.270425
	minimum = 0.215 (at node 42)
	maximum = 0.334714 (at node 103)
Injected packet length average = 17.9785
Accepted packet length average = 18.0525
Total in-flight flits = 103473 (103473 measured)
latency change    = 0.0782781
throughput change = 0.000123813
Draining all recorded packets ...
Class 0:
Remaining flits: 235303 235304 235305 235306 235307 235308 235309 235310 235311 235312 [...] (105994 flits)
Measured flits: 235303 235304 235305 235306 235307 235308 235309 235310 235311 235312 [...] (103852 flits)
Class 0:
Remaining flits: 446796 446797 446798 446799 446800 446801 446802 446803 446804 446805 [...] (105572 flits)
Measured flits: 446796 446797 446798 446799 446800 446801 446802 446803 446804 446805 [...] (90740 flits)
Class 0:
Remaining flits: 503341 503342 503343 503344 503345 503346 503347 503348 503349 503350 [...] (105782 flits)
Measured flits: 503341 503342 503343 503344 503345 503346 503347 503348 503349 503350 [...] (66718 flits)
Class 0:
Remaining flits: 570396 570397 570398 570399 570400 570401 583926 583927 583928 583929 [...] (106023 flits)
Measured flits: 570396 570397 570398 570399 570400 570401 583926 583927 583928 583929 [...] (39415 flits)
Class 0:
Remaining flits: 586674 586675 586676 586677 586678 586679 586680 586681 586682 586683 [...] (105418 flits)
Measured flits: 586674 586675 586676 586677 586678 586679 586680 586681 586682 586683 [...] (22094 flits)
Class 0:
Remaining flits: 643152 643153 643154 643155 643156 643157 649962 649963 649964 649965 [...] (104435 flits)
Measured flits: 643152 643153 643154 643155 643156 643157 649962 649963 649964 649965 [...] (14264 flits)
Class 0:
Remaining flits: 689832 689833 689834 689835 689836 689837 689838 689839 689840 689841 [...] (106297 flits)
Measured flits: 706293 706294 706295 706296 706297 706298 706299 706300 706301 710426 [...] (9240 flits)
Class 0:
Remaining flits: 696740 696741 696742 696743 718951 718952 718953 718954 718955 723189 [...] (105002 flits)
Measured flits: 718951 718952 718953 718954 718955 728370 728371 728372 728373 728374 [...] (5800 flits)
Class 0:
Remaining flits: 761994 761995 761996 761997 761998 761999 762000 762001 762002 762003 [...] (104161 flits)
Measured flits: 812587 812588 812589 812590 812591 872730 872731 872732 872733 872734 [...] (2631 flits)
Class 0:
Remaining flits: 810285 810286 810287 822258 822259 822260 822261 822262 822263 822264 [...] (105199 flits)
Measured flits: 873196 873197 891440 891441 891442 891443 891444 891445 891446 891447 [...] (1115 flits)
Class 0:
Remaining flits: 895554 895555 895556 895557 895558 895559 895560 895561 895562 895563 [...] (105102 flits)
Measured flits: 1003968 1003969 1003970 1003971 1003972 1003973 1003974 1003975 1003976 1003977 [...] (245 flits)
Class 0:
Remaining flits: 916935 916936 916937 930888 930889 930890 930891 930892 930893 930894 [...] (103604 flits)
Measured flits: 1025082 1025083 1025084 1025085 1025086 1025087 1025088 1025089 1025090 1025091 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 961238 961239 961240 961241 961242 961243 961244 961245 961246 961247 [...] (57373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1064916 1064917 1064918 1064919 1064920 1064921 1064922 1064923 1064924 1064925 [...] (14572 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1110870 1110871 1110872 1110873 1110874 1110875 1110876 1110877 1110878 1110879 [...] (345 flits)
Measured flits: (0 flits)
Time taken is 25991 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3474.89 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12953 (1 samples)
Network latency average = 1948.21 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7906 (1 samples)
Flit latency average = 1913.93 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7808 (1 samples)
Fragmentation average = 113.075 (1 samples)
	minimum = 0 (1 samples)
	maximum = 576 (1 samples)
Injected packet rate average = 0.0163132 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0204286 (1 samples)
Accepted packet rate average = 0.0149799 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.293288 (1 samples)
	minimum = 0.172286 (1 samples)
	maximum = 0.367714 (1 samples)
Accepted flit rate average = 0.270425 (1 samples)
	minimum = 0.215 (1 samples)
	maximum = 0.334714 (1 samples)
Injected packet size average = 17.9785 (1 samples)
Accepted packet size average = 18.0525 (1 samples)
Hops average = 5.0642 (1 samples)
Total run time 47.6492
