BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 388.932
	minimum = 27
	maximum = 942
Network latency average = 347.028
	minimum = 23
	maximum = 921
Slowest packet = 237
Flit latency average = 277.497
	minimum = 6
	maximum = 977
Slowest flit = 1521
Fragmentation average = 164.4
	minimum = 0
	maximum = 798
Injected packet rate average = 0.0219479
	minimum = 0.007 (at node 92)
	maximum = 0.035 (at node 173)
Accepted packet rate average = 0.00998438
	minimum = 0.003 (at node 158)
	maximum = 0.019 (at node 20)
Injected flit rate average = 0.387089
	minimum = 0.114 (at node 92)
	maximum = 0.614 (at node 173)
Accepted flit rate average= 0.205917
	minimum = 0.075 (at node 174)
	maximum = 0.342 (at node 20)
Injected packet length average = 17.6367
Accepted packet length average = 20.6239
Total in-flight flits = 36478 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 756.353
	minimum = 27
	maximum = 1877
Network latency average = 585.891
	minimum = 23
	maximum = 1877
Slowest packet = 433
Flit latency average = 486.141
	minimum = 6
	maximum = 1936
Slowest flit = 6593
Fragmentation average = 193.675
	minimum = 0
	maximum = 1779
Injected packet rate average = 0.0162135
	minimum = 0.0035 (at node 92)
	maximum = 0.027 (at node 3)
Accepted packet rate average = 0.0105495
	minimum = 0.0045 (at node 79)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.288151
	minimum = 0.059 (at node 92)
	maximum = 0.4795 (at node 3)
Accepted flit rate average= 0.20243
	minimum = 0.0835 (at node 79)
	maximum = 0.3015 (at node 152)
Injected packet length average = 17.7722
Accepted packet length average = 19.1886
Total in-flight flits = 34713 (0 measured)
latency change    = 0.48578
throughput change = 0.0172256
Class 0:
Packet latency average = 1852.17
	minimum = 661
	maximum = 2954
Network latency average = 962.202
	minimum = 28
	maximum = 2954
Slowest packet = 112
Flit latency average = 814.997
	minimum = 6
	maximum = 2937
Slowest flit = 2033
Fragmentation average = 222.829
	minimum = 0
	maximum = 2912
Injected packet rate average = 0.010901
	minimum = 0 (at node 1)
	maximum = 0.027 (at node 22)
Accepted packet rate average = 0.0109115
	minimum = 0.003 (at node 77)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.195646
	minimum = 0 (at node 1)
	maximum = 0.48 (at node 189)
Accepted flit rate average= 0.194104
	minimum = 0.057 (at node 82)
	maximum = 0.363 (at node 16)
Injected packet length average = 17.9474
Accepted packet length average = 17.789
Total in-flight flits = 35065 (0 measured)
latency change    = 0.591639
throughput change = 0.042892
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2688.69
	minimum = 1400
	maximum = 3660
Network latency average = 334.736
	minimum = 28
	maximum = 930
Slowest packet = 8351
Flit latency average = 825.448
	minimum = 6
	maximum = 3921
Slowest flit = 6929
Fragmentation average = 129.57
	minimum = 0
	maximum = 763
Injected packet rate average = 0.011474
	minimum = 0 (at node 24)
	maximum = 0.033 (at node 91)
Accepted packet rate average = 0.0110052
	minimum = 0.004 (at node 52)
	maximum = 0.022 (at node 37)
Injected flit rate average = 0.207349
	minimum = 0 (at node 24)
	maximum = 0.595 (at node 91)
Accepted flit rate average= 0.197536
	minimum = 0.087 (at node 190)
	maximum = 0.393 (at node 37)
Injected packet length average = 18.0713
Accepted packet length average = 17.9494
Total in-flight flits = 36882 (25784 measured)
latency change    = 0.311128
throughput change = 0.0173755
Class 0:
Packet latency average = 3175.81
	minimum = 1400
	maximum = 4673
Network latency average = 569.046
	minimum = 23
	maximum = 1981
Slowest packet = 8351
Flit latency average = 847.337
	minimum = 6
	maximum = 4895
Slowest flit = 10649
Fragmentation average = 157.327
	minimum = 0
	maximum = 1339
Injected packet rate average = 0.0111953
	minimum = 0.001 (at node 73)
	maximum = 0.0265 (at node 91)
Accepted packet rate average = 0.0109271
	minimum = 0.005 (at node 86)
	maximum = 0.018 (at node 160)
Injected flit rate average = 0.201802
	minimum = 0.0115 (at node 87)
	maximum = 0.472 (at node 91)
Accepted flit rate average= 0.196151
	minimum = 0.0965 (at node 4)
	maximum = 0.3135 (at node 99)
Injected packet length average = 18.0256
Accepted packet length average = 17.9509
Total in-flight flits = 37287 (33337 measured)
latency change    = 0.153384
throughput change = 0.00706301
Class 0:
Packet latency average = 3618.92
	minimum = 1400
	maximum = 5279
Network latency average = 709.512
	minimum = 23
	maximum = 2873
Slowest packet = 8351
Flit latency average = 869.254
	minimum = 6
	maximum = 5651
Slowest flit = 20825
Fragmentation average = 174.613
	minimum = 0
	maximum = 1713
Injected packet rate average = 0.0110885
	minimum = 0.00133333 (at node 172)
	maximum = 0.0243333 (at node 190)
Accepted packet rate average = 0.0108385
	minimum = 0.00633333 (at node 4)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.20009
	minimum = 0.0286667 (at node 172)
	maximum = 0.434 (at node 190)
Accepted flit rate average= 0.194717
	minimum = 0.106 (at node 4)
	maximum = 0.265333 (at node 129)
Injected packet length average = 18.0448
Accepted packet length average = 17.9652
Total in-flight flits = 38144 (36674 measured)
latency change    = 0.122443
throughput change = 0.00736468
Draining remaining packets ...
Class 0:
Remaining flits: 106381 106382 106383 106384 106385 106386 106387 106388 106389 106390 [...] (7337 flits)
Measured flits: 150524 150525 150526 150527 150528 150529 150530 150531 150532 150533 [...] (7029 flits)
Time taken is 7870 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4140.17 (1 samples)
	minimum = 1400 (1 samples)
	maximum = 7015 (1 samples)
Network latency average = 964.46 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4410 (1 samples)
Flit latency average = 1015.93 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6185 (1 samples)
Fragmentation average = 176.398 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2706 (1 samples)
Injected packet rate average = 0.0110885 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0108385 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.20009 (1 samples)
	minimum = 0.0286667 (1 samples)
	maximum = 0.434 (1 samples)
Accepted flit rate average = 0.194717 (1 samples)
	minimum = 0.106 (1 samples)
	maximum = 0.265333 (1 samples)
Injected packet size average = 18.0448 (1 samples)
Accepted packet size average = 17.9652 (1 samples)
Hops average = 5.11231 (1 samples)
Total run time 7.82197
