BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 243.391
	minimum = 23
	maximum = 870
Network latency average = 239.788
	minimum = 23
	maximum = 870
Slowest packet = 236
Flit latency average = 169.146
	minimum = 6
	maximum = 949
Slowest flit = 2545
Fragmentation average = 136.609
	minimum = 0
	maximum = 720
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.00952604
	minimum = 0 (at node 41)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.194484
	minimum = 0.028 (at node 41)
	maximum = 0.328 (at node 152)
Injected packet length average = 17.7947
Accepted packet length average = 20.4161
Total in-flight flits = 24723 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 401.547
	minimum = 23
	maximum = 1780
Network latency average = 397.733
	minimum = 23
	maximum = 1780
Slowest packet = 586
Flit latency average = 309.979
	minimum = 6
	maximum = 1780
Slowest flit = 12156
Fragmentation average = 177.631
	minimum = 0
	maximum = 1484
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.0105078
	minimum = 0.005 (at node 83)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.202477
	minimum = 0.1085 (at node 83)
	maximum = 0.311 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 19.2691
Total in-flight flits = 44955 (0 measured)
latency change    = 0.393866
throughput change = 0.0394722
Class 0:
Packet latency average = 824.204
	minimum = 23
	maximum = 2642
Network latency average = 820.252
	minimum = 23
	maximum = 2642
Slowest packet = 1076
Flit latency average = 731.267
	minimum = 6
	maximum = 2731
Slowest flit = 15831
Fragmentation average = 235.2
	minimum = 0
	maximum = 2260
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0120781
	minimum = 0.004 (at node 108)
	maximum = 0.022 (at node 159)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.217531
	minimum = 0.072 (at node 121)
	maximum = 0.385 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 18.0103
Total in-flight flits = 64983 (0 measured)
latency change    = 0.512805
throughput change = 0.069207
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 283.144
	minimum = 23
	maximum = 975
Network latency average = 279.354
	minimum = 23
	maximum = 975
Slowest packet = 10262
Flit latency average = 981.871
	minimum = 6
	maximum = 3678
Slowest flit = 18735
Fragmentation average = 101.717
	minimum = 0
	maximum = 616
Injected packet rate average = 0.0185052
	minimum = 0.01 (at node 152)
	maximum = 0.03 (at node 43)
Accepted packet rate average = 0.0121927
	minimum = 0.005 (at node 70)
	maximum = 0.023 (at node 151)
Injected flit rate average = 0.332151
	minimum = 0.167 (at node 152)
	maximum = 0.54 (at node 43)
Accepted flit rate average= 0.220219
	minimum = 0.075 (at node 108)
	maximum = 0.41 (at node 160)
Injected packet length average = 17.9491
Accepted packet length average = 18.0615
Total in-flight flits = 86655 (52130 measured)
latency change    = 1.9109
throughput change = 0.0122038
Class 0:
Packet latency average = 640.818
	minimum = 23
	maximum = 1968
Network latency average = 636.68
	minimum = 23
	maximum = 1968
Slowest packet = 10301
Flit latency average = 1131.69
	minimum = 6
	maximum = 4287
Slowest flit = 43198
Fragmentation average = 143.046
	minimum = 0
	maximum = 1057
Injected packet rate average = 0.0181224
	minimum = 0.0095 (at node 160)
	maximum = 0.028 (at node 182)
Accepted packet rate average = 0.0121901
	minimum = 0.007 (at node 53)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.326365
	minimum = 0.1675 (at node 160)
	maximum = 0.507 (at node 182)
Accepted flit rate average= 0.219055
	minimum = 0.133 (at node 4)
	maximum = 0.3535 (at node 16)
Injected packet length average = 18.0089
Accepted packet length average = 17.9699
Total in-flight flits = 106146 (87776 measured)
latency change    = 0.558151
throughput change = 0.00531403
Class 0:
Packet latency average = 950.335
	minimum = 23
	maximum = 2891
Network latency average = 945.559
	minimum = 23
	maximum = 2891
Slowest packet = 10257
Flit latency average = 1287.98
	minimum = 6
	maximum = 5176
Slowest flit = 50158
Fragmentation average = 159.893
	minimum = 0
	maximum = 1616
Injected packet rate average = 0.018033
	minimum = 0.012 (at node 152)
	maximum = 0.0253333 (at node 17)
Accepted packet rate average = 0.0121076
	minimum = 0.00766667 (at node 4)
	maximum = 0.0183333 (at node 151)
Injected flit rate average = 0.32497
	minimum = 0.216 (at node 152)
	maximum = 0.456 (at node 17)
Accepted flit rate average= 0.217694
	minimum = 0.131333 (at node 4)
	maximum = 0.329333 (at node 151)
Injected packet length average = 18.0209
Accepted packet length average = 17.9799
Total in-flight flits = 126593 (116602 measured)
latency change    = 0.325693
throughput change = 0.00624841
Class 0:
Packet latency average = 1189.31
	minimum = 23
	maximum = 3924
Network latency average = 1183.77
	minimum = 23
	maximum = 3924
Slowest packet = 10442
Flit latency average = 1424.33
	minimum = 6
	maximum = 5834
Slowest flit = 71760
Fragmentation average = 168.868
	minimum = 0
	maximum = 2775
Injected packet rate average = 0.0179961
	minimum = 0.0125 (at node 116)
	maximum = 0.0245 (at node 17)
Accepted packet rate average = 0.0120104
	minimum = 0.0085 (at node 64)
	maximum = 0.01675 (at node 90)
Injected flit rate average = 0.323952
	minimum = 0.2285 (at node 116)
	maximum = 0.441 (at node 17)
Accepted flit rate average= 0.215878
	minimum = 0.1525 (at node 49)
	maximum = 0.304 (at node 90)
Injected packet length average = 18.0012
Accepted packet length average = 17.9742
Total in-flight flits = 148003 (142550 measured)
latency change    = 0.200934
throughput change = 0.00841607
Class 0:
Packet latency average = 1422.23
	minimum = 23
	maximum = 4873
Network latency average = 1415.06
	minimum = 23
	maximum = 4873
Slowest packet = 10382
Flit latency average = 1569.76
	minimum = 6
	maximum = 6850
Slowest flit = 70388
Fragmentation average = 174.89
	minimum = 0
	maximum = 2775
Injected packet rate average = 0.0178719
	minimum = 0.0108 (at node 144)
	maximum = 0.0238 (at node 9)
Accepted packet rate average = 0.0119573
	minimum = 0.0082 (at node 64)
	maximum = 0.016 (at node 157)
Injected flit rate average = 0.321674
	minimum = 0.195 (at node 144)
	maximum = 0.4284 (at node 9)
Accepted flit rate average= 0.214898
	minimum = 0.1548 (at node 64)
	maximum = 0.295 (at node 90)
Injected packet length average = 17.9989
Accepted packet length average = 17.9721
Total in-flight flits = 167651 (164540 measured)
latency change    = 0.163774
throughput change = 0.00455885
Class 0:
Packet latency average = 1654.43
	minimum = 23
	maximum = 5929
Network latency average = 1645.26
	minimum = 23
	maximum = 5929
Slowest packet = 10307
Flit latency average = 1721
	minimum = 6
	maximum = 7320
Slowest flit = 69749
Fragmentation average = 178.452
	minimum = 0
	maximum = 2930
Injected packet rate average = 0.0176901
	minimum = 0.0106667 (at node 132)
	maximum = 0.0228333 (at node 17)
Accepted packet rate average = 0.0119123
	minimum = 0.00916667 (at node 64)
	maximum = 0.0168333 (at node 90)
Injected flit rate average = 0.318393
	minimum = 0.1905 (at node 132)
	maximum = 0.411333 (at node 99)
Accepted flit rate average= 0.2134
	minimum = 0.161667 (at node 64)
	maximum = 0.2995 (at node 90)
Injected packet length average = 17.9984
Accepted packet length average = 17.9142
Total in-flight flits = 186166 (184291 measured)
latency change    = 0.140352
throughput change = 0.00701847
Class 0:
Packet latency average = 1881.16
	minimum = 23
	maximum = 6856
Network latency average = 1867.08
	minimum = 23
	maximum = 6856
Slowest packet = 10642
Flit latency average = 1887.85
	minimum = 6
	maximum = 7945
Slowest flit = 122579
Fragmentation average = 179.827
	minimum = 0
	maximum = 2930
Injected packet rate average = 0.0175082
	minimum = 0.00971429 (at node 8)
	maximum = 0.0224286 (at node 158)
Accepted packet rate average = 0.0117946
	minimum = 0.00857143 (at node 171)
	maximum = 0.0158571 (at node 90)
Injected flit rate average = 0.315097
	minimum = 0.176 (at node 8)
	maximum = 0.405571 (at node 158)
Accepted flit rate average= 0.211394
	minimum = 0.156143 (at node 171)
	maximum = 0.283571 (at node 90)
Injected packet length average = 17.9971
Accepted packet length average = 17.9228
Total in-flight flits = 204716 (203743 measured)
latency change    = 0.120526
throughput change = 0.00949212
Draining all recorded packets ...
Class 0:
Remaining flits: 107157 107158 107159 107160 107161 107162 107163 107164 107165 107166 [...] (226394 flits)
Measured flits: 185238 185239 185240 185241 185242 185243 185244 185245 185246 185247 [...] (176005 flits)
Class 0:
Remaining flits: 120886 120887 121716 121717 121718 121719 121720 121721 121722 121723 [...] (241499 flits)
Measured flits: 185238 185239 185240 185241 185242 185243 185244 185245 185246 185247 [...] (146851 flits)
Class 0:
Remaining flits: 125028 125029 125030 125031 125032 125033 125034 125035 125036 125037 [...] (260557 flits)
Measured flits: 188064 188065 188066 188067 188068 188069 188070 188071 188072 188073 [...] (118692 flits)
Class 0:
Remaining flits: 161688 161689 161690 161691 161692 161693 173162 173163 173164 173165 [...] (275487 flits)
Measured flits: 191682 191683 191684 191685 191686 191687 191688 191689 191690 191691 [...] (94099 flits)
Class 0:
Remaining flits: 161693 191682 191683 191684 191685 191686 191687 191688 191689 191690 [...] (289877 flits)
Measured flits: 191682 191683 191684 191685 191686 191687 191688 191689 191690 191691 [...] (71564 flits)
Class 0:
Remaining flits: 255747 255748 255749 255750 255751 255752 255753 255754 255755 255756 [...] (303979 flits)
Measured flits: 255747 255748 255749 255750 255751 255752 255753 255754 255755 255756 [...] (53263 flits)
Class 0:
Remaining flits: 255753 255754 255755 255756 255757 255758 255759 255760 255761 261090 [...] (314396 flits)
Measured flits: 255753 255754 255755 255756 255757 255758 255759 255760 255761 261090 [...] (38554 flits)
Class 0:
Remaining flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (323503 flits)
Measured flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (26920 flits)
Class 0:
Remaining flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (328636 flits)
Measured flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (18848 flits)
Class 0:
Remaining flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (334696 flits)
Measured flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (13428 flits)
Class 0:
Remaining flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (334883 flits)
Measured flits: 307440 307441 307442 307443 307444 307445 307446 307447 307448 307449 [...] (9471 flits)
Class 0:
Remaining flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (334522 flits)
Measured flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (6823 flits)
Class 0:
Remaining flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (333128 flits)
Measured flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (4444 flits)
Class 0:
Remaining flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (333692 flits)
Measured flits: 335178 335179 335180 335181 335182 335183 335184 335185 335186 335187 [...] (3092 flits)
Class 0:
Remaining flits: 335191 335192 335193 335194 335195 360306 360307 360308 360309 360310 [...] (335760 flits)
Measured flits: 335191 335192 335193 335194 335195 360306 360307 360308 360309 360310 [...] (1987 flits)
Class 0:
Remaining flits: 360306 360307 360308 360309 360310 360311 360312 360313 360314 360315 [...] (333824 flits)
Measured flits: 360306 360307 360308 360309 360310 360311 360312 360313 360314 360315 [...] (1393 flits)
Class 0:
Remaining flits: 438409 438410 438411 438412 438413 438414 438415 438416 438417 438418 [...] (331911 flits)
Measured flits: 438409 438410 438411 438412 438413 438414 438415 438416 438417 438418 [...] (1001 flits)
Class 0:
Remaining flits: 459684 459685 459686 459687 459688 459689 459690 459691 459692 459693 [...] (331295 flits)
Measured flits: 459684 459685 459686 459687 459688 459689 459690 459691 459692 459693 [...] (568 flits)
Class 0:
Remaining flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (331814 flits)
Measured flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (385 flits)
Class 0:
Remaining flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (328736 flits)
Measured flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (295 flits)
Class 0:
Remaining flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (327461 flits)
Measured flits: 483390 483391 483392 483393 483394 483395 483396 483397 483398 483399 [...] (131 flits)
Class 0:
Remaining flits: 574056 574057 574058 574059 574060 574061 574062 574063 574064 574065 [...] (324700 flits)
Measured flits: 574056 574057 574058 574059 574060 574061 574062 574063 574064 574065 [...] (62 flits)
Class 0:
Remaining flits: 574056 574057 574058 574059 574060 574061 574062 574063 574064 574065 [...] (323506 flits)
Measured flits: 574056 574057 574058 574059 574060 574061 574062 574063 574064 574065 [...] (36 flits)
Class 0:
Remaining flits: 574071 574072 574073 617166 617167 617168 617169 617170 617171 617172 [...] (326687 flits)
Measured flits: 574071 574072 574073 (3 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 617166 617167 617168 617169 617170 617171 617172 617173 617174 617175 [...] (290205 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 617166 617167 617168 617169 617170 617171 617172 617173 617174 617175 [...] (255318 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 617166 617167 617168 617169 617170 617171 617172 617173 617174 617175 [...] (220675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 642121 642122 642123 642124 642125 642126 642127 642128 642129 642130 [...] (185940 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 749106 749107 749108 749109 749110 749111 749112 749113 749114 749115 [...] (151190 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 749106 749107 749108 749109 749110 749111 749112 749113 749114 749115 [...] (117036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 749110 749111 749112 749113 749114 749115 749116 749117 749118 749119 [...] (85599 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 822024 822025 822026 822027 822028 822029 822030 822031 822032 822033 [...] (58168 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 822040 822041 901224 901225 901226 901227 901228 901229 901230 901231 [...] (35003 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 909216 909217 909218 909219 909220 909221 909222 909223 909224 909225 [...] (18682 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 909216 909217 909218 909219 909220 909221 909222 909223 909224 909225 [...] (6170 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1120874 1120875 1120876 1120877 1200394 1200395 1200396 1200397 1200398 1200399 [...] (1151 flits)
Measured flits: (0 flits)
Time taken is 47335 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4191.74 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24949 (1 samples)
Network latency average = 4092.55 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24949 (1 samples)
Flit latency average = 6694.16 (1 samples)
	minimum = 6 (1 samples)
	maximum = 29927 (1 samples)
Fragmentation average = 201.873 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4646 (1 samples)
Injected packet rate average = 0.0175082 (1 samples)
	minimum = 0.00971429 (1 samples)
	maximum = 0.0224286 (1 samples)
Accepted packet rate average = 0.0117946 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0158571 (1 samples)
Injected flit rate average = 0.315097 (1 samples)
	minimum = 0.176 (1 samples)
	maximum = 0.405571 (1 samples)
Accepted flit rate average = 0.211394 (1 samples)
	minimum = 0.156143 (1 samples)
	maximum = 0.283571 (1 samples)
Injected packet size average = 17.9971 (1 samples)
Accepted packet size average = 17.9228 (1 samples)
Hops average = 5.0706 (1 samples)
Total run time 55.5837
