BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 258
	minimum = 23
	maximum = 904
Network latency average = 253.733
	minimum = 23
	maximum = 873
Slowest packet = 431
Flit latency average = 178.983
	minimum = 6
	maximum = 927
Slowest flit = 4131
Fragmentation average = 152.856
	minimum = 0
	maximum = 773
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00961458
	minimum = 0.002 (at node 127)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.204323
	minimum = 0.072 (at node 174)
	maximum = 0.354 (at node 167)
Injected packet length average = 17.8234
Accepted packet length average = 21.2514
Total in-flight flits = 30376 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 436.169
	minimum = 23
	maximum = 1772
Network latency average = 431.391
	minimum = 23
	maximum = 1772
Slowest packet = 644
Flit latency average = 330.673
	minimum = 6
	maximum = 1890
Slowest flit = 5305
Fragmentation average = 219.108
	minimum = 0
	maximum = 1668
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0110443
	minimum = 0.006 (at node 81)
	maximum = 0.017 (at node 98)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.217737
	minimum = 0.1145 (at node 81)
	maximum = 0.3315 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.7149
Total in-flight flits = 54593 (0 measured)
latency change    = 0.408486
throughput change = 0.0616067
Class 0:
Packet latency average = 876.656
	minimum = 27
	maximum = 2855
Network latency average = 872.061
	minimum = 24
	maximum = 2855
Slowest packet = 189
Flit latency average = 749.383
	minimum = 6
	maximum = 2838
Slowest flit = 3419
Fragmentation average = 318.166
	minimum = 0
	maximum = 2797
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.012526
	minimum = 0.006 (at node 59)
	maximum = 0.023 (at node 145)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.232594
	minimum = 0.118 (at node 188)
	maximum = 0.372 (at node 56)
Injected packet length average = 17.9811
Accepted packet length average = 18.5688
Total in-flight flits = 79613 (0 measured)
latency change    = 0.502463
throughput change = 0.0638743
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 258.585
	minimum = 23
	maximum = 972
Network latency average = 253.643
	minimum = 23
	maximum = 972
Slowest packet = 11598
Flit latency average = 1024.73
	minimum = 6
	maximum = 3843
Slowest flit = 8706
Fragmentation average = 128.532
	minimum = 0
	maximum = 725
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.013
	minimum = 0.005 (at node 138)
	maximum = 0.025 (at node 47)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.236135
	minimum = 0.09 (at node 138)
	maximum = 0.459 (at node 182)
Injected packet length average = 17.9618
Accepted packet length average = 18.1643
Total in-flight flits = 105897 (59263 measured)
latency change    = 2.39021
throughput change = 0.0149985
Class 0:
Packet latency average = 611.574
	minimum = 23
	maximum = 1967
Network latency average = 606.49
	minimum = 23
	maximum = 1950
Slowest packet = 11617
Flit latency average = 1171.22
	minimum = 6
	maximum = 4542
Slowest flit = 26645
Fragmentation average = 226.351
	minimum = 0
	maximum = 1633
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0130286
	minimum = 0.0065 (at node 146)
	maximum = 0.022 (at node 0)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.236591
	minimum = 0.143 (at node 31)
	maximum = 0.4045 (at node 0)
Injected packet length average = 18.0096
Accepted packet length average = 18.1593
Total in-flight flits = 128874 (101071 measured)
latency change    = 0.577182
throughput change = 0.00192623
Class 0:
Packet latency average = 921.888
	minimum = 23
	maximum = 2958
Network latency average = 917.031
	minimum = 23
	maximum = 2958
Slowest packet = 11684
Flit latency average = 1331.34
	minimum = 6
	maximum = 5670
Slowest flit = 22418
Fragmentation average = 266.974
	minimum = 0
	maximum = 2347
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0129618
	minimum = 0.00866667 (at node 96)
	maximum = 0.02 (at node 6)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.235339
	minimum = 0.158333 (at node 20)
	maximum = 0.37 (at node 0)
Injected packet length average = 18.0047
Accepted packet length average = 18.1563
Total in-flight flits = 154838 (137907 measured)
latency change    = 0.336607
throughput change = 0.00532256
Class 0:
Packet latency average = 1218.87
	minimum = 23
	maximum = 3865
Network latency average = 1213.97
	minimum = 23
	maximum = 3865
Slowest packet = 12009
Flit latency average = 1499.91
	minimum = 6
	maximum = 6747
Slowest flit = 16946
Fragmentation average = 293.655
	minimum = 0
	maximum = 3223
Injected packet rate average = 0.0203372
	minimum = 0.01475 (at node 5)
	maximum = 0.026 (at node 156)
Accepted packet rate average = 0.0129622
	minimum = 0.0085 (at node 20)
	maximum = 0.01825 (at node 0)
Injected flit rate average = 0.366148
	minimum = 0.2655 (at node 5)
	maximum = 0.468 (at node 156)
Accepted flit rate average= 0.234991
	minimum = 0.159 (at node 20)
	maximum = 0.33375 (at node 0)
Injected packet length average = 18.0038
Accepted packet length average = 18.1289
Total in-flight flits = 180282 (169639 measured)
latency change    = 0.243655
throughput change = 0.00147945
Class 0:
Packet latency average = 1485.73
	minimum = 23
	maximum = 4874
Network latency average = 1480.83
	minimum = 23
	maximum = 4874
Slowest packet = 11925
Flit latency average = 1662.42
	minimum = 6
	maximum = 7622
Slowest flit = 24065
Fragmentation average = 308.738
	minimum = 0
	maximum = 3564
Injected packet rate average = 0.0203438
	minimum = 0.0154 (at node 139)
	maximum = 0.0248 (at node 113)
Accepted packet rate average = 0.0129458
	minimum = 0.0096 (at node 139)
	maximum = 0.0176 (at node 128)
Injected flit rate average = 0.366196
	minimum = 0.2772 (at node 139)
	maximum = 0.4464 (at node 113)
Accepted flit rate average= 0.23397
	minimum = 0.1774 (at node 139)
	maximum = 0.3112 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 18.073
Total in-flight flits = 206542 (199500 measured)
latency change    = 0.179614
throughput change = 0.00436421
Class 0:
Packet latency average = 1740.25
	minimum = 23
	maximum = 5956
Network latency average = 1735.33
	minimum = 23
	maximum = 5956
Slowest packet = 11574
Flit latency average = 1828.64
	minimum = 6
	maximum = 8350
Slowest flit = 44270
Fragmentation average = 322.52
	minimum = 0
	maximum = 4433
Injected packet rate average = 0.0203151
	minimum = 0.016 (at node 155)
	maximum = 0.0245 (at node 96)
Accepted packet rate average = 0.0128993
	minimum = 0.00983333 (at node 137)
	maximum = 0.0166667 (at node 157)
Injected flit rate average = 0.365752
	minimum = 0.289333 (at node 155)
	maximum = 0.441 (at node 96)
Accepted flit rate average= 0.232609
	minimum = 0.1805 (at node 137)
	maximum = 0.304 (at node 157)
Injected packet length average = 18.0039
Accepted packet length average = 18.0326
Total in-flight flits = 232902 (228118 measured)
latency change    = 0.146256
throughput change = 0.00585226
Class 0:
Packet latency average = 1968.47
	minimum = 23
	maximum = 6790
Network latency average = 1963.62
	minimum = 23
	maximum = 6790
Slowest packet = 11742
Flit latency average = 1990.24
	minimum = 6
	maximum = 9170
Slowest flit = 56500
Fragmentation average = 330.105
	minimum = 0
	maximum = 4669
Injected packet rate average = 0.0202984
	minimum = 0.0158571 (at node 155)
	maximum = 0.0248571 (at node 165)
Accepted packet rate average = 0.0128438
	minimum = 0.00942857 (at node 190)
	maximum = 0.0165714 (at node 157)
Injected flit rate average = 0.365336
	minimum = 0.287286 (at node 155)
	maximum = 0.445429 (at node 165)
Accepted flit rate average= 0.231816
	minimum = 0.176429 (at node 190)
	maximum = 0.300571 (at node 157)
Injected packet length average = 17.9983
Accepted packet length average = 18.049
Total in-flight flits = 259110 (255620 measured)
latency change    = 0.115938
throughput change = 0.00341774
Draining all recorded packets ...
Class 0:
Remaining flits: 21893 21894 21895 21896 21897 21898 21899 21900 21901 21902 [...] (285847 flits)
Measured flits: 208087 208088 208089 208090 208091 208092 208093 208094 208095 208096 [...] (219621 flits)
Class 0:
Remaining flits: 21893 21894 21895 21896 21897 21898 21899 21900 21901 21902 [...] (311448 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (185772 flits)
Class 0:
Remaining flits: 21893 21894 21895 21896 21897 21898 21899 21900 21901 21902 [...] (340332 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (155862 flits)
Class 0:
Remaining flits: 21902 21903 21904 21905 61825 61826 61827 61828 61829 68562 [...] (368492 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (129652 flits)
Class 0:
Remaining flits: 61825 61826 61827 61828 61829 68562 68563 68564 68565 68566 [...] (396633 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (107873 flits)
Class 0:
Remaining flits: 61825 61826 61827 61828 61829 68562 68563 68564 68565 68566 [...] (427738 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (90380 flits)
Class 0:
Remaining flits: 68562 68563 68564 68565 68566 68567 68568 68569 68570 68571 [...] (456193 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (76799 flits)
Class 0:
Remaining flits: 68562 68563 68564 68565 68566 68567 68568 68569 68570 68571 [...] (486810 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (65150 flits)
Class 0:
Remaining flits: 68562 68563 68564 68565 68566 68567 68568 68569 68570 68571 [...] (517457 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (54885 flits)
Class 0:
Remaining flits: 68562 68563 68564 68565 68566 68567 68568 68569 68570 68571 [...] (548583 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (46745 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (579706 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (39946 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (611245 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (34582 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (643019 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (29428 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (673615 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (25262 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (703658 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (21595 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (734289 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (18736 flits)
Class 0:
Remaining flits: 145098 145099 145100 145101 145102 145103 145104 145105 145106 145107 [...] (765420 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (15850 flits)
Class 0:
Remaining flits: 145103 145104 145105 145106 145107 145108 145109 145110 145111 145112 [...] (795837 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (13712 flits)
Class 0:
Remaining flits: 159858 159859 159860 159861 159862 159863 159864 159865 159866 159867 [...] (827735 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (11708 flits)
Class 0:
Remaining flits: 159858 159859 159860 159861 159862 159863 159864 159865 159866 159867 [...] (861196 flits)
Measured flits: 208170 208171 208172 208173 208174 208175 208176 208177 208178 208179 [...] (10057 flits)
Class 0:
Remaining flits: 191538 191539 191540 191541 191542 191543 191544 191545 191546 191547 [...] (893110 flits)
Measured flits: 208171 208172 208173 208174 208175 208176 208177 208178 208179 208180 [...] (8970 flits)
Class 0:
Remaining flits: 191551 191552 191553 191554 191555 209394 209395 209396 209397 209398 [...] (924477 flits)
Measured flits: 209394 209395 209396 209397 209398 209399 209400 209401 209402 209403 [...] (7732 flits)
Class 0:
Remaining flits: 191551 191552 191553 191554 191555 209394 209395 209396 209397 209398 [...] (955274 flits)
Measured flits: 209394 209395 209396 209397 209398 209399 209400 209401 209402 209403 [...] (6535 flits)
Class 0:
Remaining flits: 191551 191552 191553 191554 191555 209394 209395 209396 209397 209398 [...] (987831 flits)
Measured flits: 209394 209395 209396 209397 209398 209399 209400 209401 209402 209403 [...] (5796 flits)
Class 0:
Remaining flits: 191551 191552 191553 191554 191555 229086 229087 229088 229089 229090 [...] (1019844 flits)
Measured flits: 229086 229087 229088 229089 229090 229091 229092 229093 229094 229095 [...] (4905 flits)
Class 0:
Remaining flits: 191551 191552 191553 191554 191555 229086 229087 229088 229089 229090 [...] (1052295 flits)
Measured flits: 229086 229087 229088 229089 229090 229091 229092 229093 229094 229095 [...] (4018 flits)
Class 0:
Remaining flits: 229086 229087 229088 229089 229090 229091 229092 229093 229094 229095 [...] (1085300 flits)
Measured flits: 229086 229087 229088 229089 229090 229091 229092 229093 229094 229095 [...] (3546 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1118445 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (3011 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1152759 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (2327 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1184849 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1988 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1216019 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1719 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1249539 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1567 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1280757 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1366 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1312767 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1236 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1346196 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1188 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1378551 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1008 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1409910 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (966 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1442258 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (801 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1473159 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (714 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1505043 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (652 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1536151 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (534 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1567618 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (488 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1597073 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (389 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1624168 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (356 flits)
Class 0:
Remaining flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (1653942 flits)
Measured flits: 257346 257347 257348 257349 257350 257351 257352 257353 257354 257355 [...] (323 flits)
Class 0:
Remaining flits: 343422 343423 343424 343425 343426 343427 343428 343429 343430 343431 [...] (1683458 flits)
Measured flits: 343422 343423 343424 343425 343426 343427 343428 343429 343430 343431 [...] (211 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1713278 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (180 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1740281 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (162 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1766258 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (113 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1795098 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (95 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1822176 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (95 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1850545 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (90 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1876917 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (72 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1902735 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (72 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1929267 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (72 flits)
Class 0:
Remaining flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (1955394 flits)
Measured flits: 439560 439561 439562 439563 439564 439565 439566 439567 439568 439569 [...] (72 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (1980764 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (2006395 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (2033354 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (2059169 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (2085332 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (2113258 flits)
Measured flits: 558018 558019 558020 558021 558022 558023 558024 558025 558026 558027 [...] (36 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2140832 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2165768 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2192097 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2218840 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2245737 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2272836 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2300367 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2328607 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Class 0:
Remaining flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (2356117 flits)
Measured flits: 655416 655417 655418 655419 655420 655421 655422 655423 655424 655425 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2338851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2307559 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2276215 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2245911 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2215143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849816 849817 849818 849819 849820 849821 849822 849823 849824 849825 [...] (2183567 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 849829 849830 849831 849832 849833 914094 914095 914096 914097 914098 [...] (2151728 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914099 914100 914101 914102 914103 914104 914105 914106 914107 914108 [...] (2120393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914099 914100 914101 914102 914103 914104 914105 914106 914107 914108 [...] (2087461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914099 914100 914101 914102 914103 914104 914105 914106 914107 914108 [...] (2054221 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914099 914100 914101 914102 914103 914104 914105 914106 914107 914108 [...] (2022338 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1990312 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1957337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1924794 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1892465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1861361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1831223 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1799667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1768774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1738409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1707793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1678118 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1647690 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1617199 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1586532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1555437 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1523751 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1491507 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1459980 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1427796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1396768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1073556 1073557 1073558 1073559 1073560 1073561 1073562 1073563 1073564 1073565 [...] (1365328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1334769 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1304868 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1274740 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1244992 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1215461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1184505 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1153444 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1122349 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1091118 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1310400 1310401 1310402 1310403 1310404 1310405 1310406 1310407 1310408 1310409 [...] (1060818 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (1031276 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (1000788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (970343 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (939363 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (908123 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (876557 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1464246 1464247 1464248 1464249 1464250 1464251 1464252 1464253 1464254 1464255 [...] (844628 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1728322 1728323 1880082 1880083 1880084 1880085 1880086 1880087 1880088 1880089 [...] (813080 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1880082 1880083 1880084 1880085 1880086 1880087 1880088 1880089 1880090 1880091 [...] (780468 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1880082 1880083 1880084 1880085 1880086 1880087 1880088 1880089 1880090 1880091 [...] (747904 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1880082 1880083 1880084 1880085 1880086 1880087 1880088 1880089 1880090 1880091 [...] (714903 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1880082 1880083 1880084 1880085 1880086 1880087 1880088 1880089 1880090 1880091 [...] (681396 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (648493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (615709 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (583418 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (551943 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (519130 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (485980 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (453017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (420202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (386937 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (353234 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (320082 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (287259 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (254952 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1966986 1966987 1966988 1966989 1966990 1966991 1966992 1966993 1966994 1966995 [...] (223599 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1999800 1999801 1999802 1999803 1999804 1999805 1999806 1999807 1999808 1999809 [...] (193660 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2158308 2158309 2158310 2158311 2158312 2158313 2158314 2158315 2158316 2158317 [...] (163916 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2158308 2158309 2158310 2158311 2158312 2158313 2158314 2158315 2158316 2158317 [...] (135657 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2248110 2248111 2248112 2248113 2248114 2248115 2248116 2248117 2248118 2248119 [...] (107975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2487497 2487498 2487499 2487500 2487501 2487502 2487503 2487504 2487505 2487506 [...] (83409 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2829654 2829655 2829656 2829657 2829658 2829659 2829660 2829661 2829662 2829663 [...] (62058 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2829654 2829655 2829656 2829657 2829658 2829659 2829660 2829661 2829662 2829663 [...] (43547 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2829670 2829671 2889054 2889055 2889056 2889057 2889058 2889059 2889060 2889061 [...] (29408 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2946618 2946619 2946620 2946621 2946622 2946623 2946624 2946625 2946626 2946627 [...] (18636 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3127176 3127177 3127178 3127179 3127180 3127181 3127182 3127183 3127184 3127185 [...] (10232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3127185 3127186 3127187 3127188 3127189 3127190 3127191 3127192 3127193 3425918 [...] (5720 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3866616 3866617 3866618 3866619 3866620 3866621 3866622 3866623 3866624 3866625 [...] (2037 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4095600 4095601 4095602 4095603 4095604 4095605 4095606 4095607 4095608 4095609 [...] (282 flits)
Measured flits: (0 flits)
Time taken is 162749 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5430.83 (1 samples)
	minimum = 23 (1 samples)
	maximum = 72073 (1 samples)
Network latency average = 5425.84 (1 samples)
	minimum = 23 (1 samples)
	maximum = 72073 (1 samples)
Flit latency average = 34640.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 122191 (1 samples)
Fragmentation average = 383.421 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14144 (1 samples)
Injected packet rate average = 0.0202984 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0248571 (1 samples)
Accepted packet rate average = 0.0128438 (1 samples)
	minimum = 0.00942857 (1 samples)
	maximum = 0.0165714 (1 samples)
Injected flit rate average = 0.365336 (1 samples)
	minimum = 0.287286 (1 samples)
	maximum = 0.445429 (1 samples)
Accepted flit rate average = 0.231816 (1 samples)
	minimum = 0.176429 (1 samples)
	maximum = 0.300571 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 18.049 (1 samples)
Hops average = 5.07713 (1 samples)
Total run time 265.107
