BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 156.552
	minimum = 25
	maximum = 620
Network latency average = 108.999
	minimum = 22
	maximum = 508
Slowest packet = 33
Flit latency average = 82.4356
	minimum = 5
	maximum = 491
Slowest flit = 19601
Fragmentation average = 20.0582
	minimum = 0
	maximum = 172
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.0101198
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.186198
	minimum = 0.036 (at node 174)
	maximum = 0.324 (at node 140)
Injected packet length average = 17.8411
Accepted packet length average = 18.3994
Total in-flight flits = 5380 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 188.365
	minimum = 25
	maximum = 1111
Network latency average = 137.083
	minimum = 22
	maximum = 700
Slowest packet = 33
Flit latency average = 109.993
	minimum = 5
	maximum = 683
Slowest flit = 52631
Fragmentation average = 20.1475
	minimum = 0
	maximum = 244
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.0105599
	minimum = 0.0055 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.192174
	minimum = 0.099 (at node 30)
	maximum = 0.306 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 18.1985
Total in-flight flits = 5801 (0 measured)
latency change    = 0.168891
throughput change = 0.0310997
Class 0:
Packet latency average = 198.548
	minimum = 22
	maximum = 904
Network latency average = 154.189
	minimum = 22
	maximum = 772
Slowest packet = 3101
Flit latency average = 125.457
	minimum = 5
	maximum = 755
Slowest flit = 75023
Fragmentation average = 21.3748
	minimum = 0
	maximum = 198
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0113958
	minimum = 0.004 (at node 28)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.204729
	minimum = 0.065 (at node 184)
	maximum = 0.351 (at node 16)
Injected packet length average = 17.9936
Accepted packet length average = 17.9653
Total in-flight flits = 5589 (0 measured)
latency change    = 0.0512849
throughput change = 0.0613234
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 205.41
	minimum = 22
	maximum = 877
Network latency average = 155.24
	minimum = 22
	maximum = 799
Slowest packet = 6606
Flit latency average = 142.142
	minimum = 5
	maximum = 821
Slowest flit = 116855
Fragmentation average = 22.8353
	minimum = 0
	maximum = 181
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.0115365
	minimum = 0.005 (at node 150)
	maximum = 0.025 (at node 120)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.206839
	minimum = 0.09 (at node 163)
	maximum = 0.442 (at node 120)
Injected packet length average = 18.0247
Accepted packet length average = 17.9291
Total in-flight flits = 6628 (6628 measured)
latency change    = 0.0334058
throughput change = 0.0101982
Class 0:
Packet latency average = 231.708
	minimum = 22
	maximum = 1153
Network latency average = 179.056
	minimum = 22
	maximum = 941
Slowest packet = 6606
Flit latency average = 156.57
	minimum = 5
	maximum = 924
Slowest flit = 160703
Fragmentation average = 22.6729
	minimum = 0
	maximum = 255
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.0112552
	minimum = 0.0065 (at node 4)
	maximum = 0.019 (at node 182)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.20282
	minimum = 0.117 (at node 4)
	maximum = 0.343 (at node 182)
Injected packet length average = 17.9755
Accepted packet length average = 18.0201
Total in-flight flits = 7824 (7824 measured)
latency change    = 0.113498
throughput change = 0.0198118
Class 0:
Packet latency average = 244.279
	minimum = 22
	maximum = 1153
Network latency average = 191.244
	minimum = 22
	maximum = 1092
Slowest packet = 6606
Flit latency average = 165.773
	minimum = 5
	maximum = 1075
Slowest flit = 166876
Fragmentation average = 22.609
	minimum = 0
	maximum = 255
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0112378
	minimum = 0.00533333 (at node 4)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.202073
	minimum = 0.0976667 (at node 4)
	maximum = 0.297 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 17.9815
Total in-flight flits = 6105 (6105 measured)
latency change    = 0.0514607
throughput change = 0.00369864
Draining remaining packets ...
Class 0:
Remaining flits: 227409 227410 227411 227915 227992 227993 227994 227995 227996 227997 [...] (293 flits)
Measured flits: 227409 227410 227411 227915 227992 227993 227994 227995 227996 227997 [...] (293 flits)
Time taken is 7304 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 259.19 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1598 (1 samples)
Network latency average = 204.295 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1342 (1 samples)
Flit latency average = 178.186 (1 samples)
	minimum = 5 (1 samples)
	maximum = 1325 (1 samples)
Fragmentation average = 22.6725 (1 samples)
	minimum = 0 (1 samples)
	maximum = 255 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0112378 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.202073 (1 samples)
	minimum = 0.0976667 (1 samples)
	maximum = 0.297 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 17.9815 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 3.57518
