BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 171.4
	minimum = 22
	maximum = 510
Network latency average = 167.673
	minimum = 22
	maximum = 509
Slowest packet = 1296
Flit latency average = 138.194
	minimum = 5
	maximum = 539
Slowest flit = 28314
Fragmentation average = 29.6203
	minimum = 0
	maximum = 186
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.0129896
	minimum = 0.004 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.240182
	minimum = 0.072 (at node 41)
	maximum = 0.447 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 18.4904
Total in-flight flits = 15949 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 271.303
	minimum = 22
	maximum = 912
Network latency average = 267.424
	minimum = 22
	maximum = 912
Slowest packet = 3679
Flit latency average = 237.51
	minimum = 5
	maximum = 924
Slowest flit = 65658
Fragmentation average = 31.1164
	minimum = 0
	maximum = 190
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.0137344
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.251167
	minimum = 0.1385 (at node 153)
	maximum = 0.36 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 18.2874
Total in-flight flits = 26258 (0 measured)
latency change    = 0.368236
throughput change = 0.0437334
Class 0:
Packet latency average = 526.573
	minimum = 25
	maximum = 1195
Network latency average = 522.532
	minimum = 22
	maximum = 1179
Slowest packet = 6141
Flit latency average = 493.133
	minimum = 5
	maximum = 1163
Slowest flit = 112688
Fragmentation average = 32.3187
	minimum = 0
	maximum = 246
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0147917
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.26549
	minimum = 0.108 (at node 163)
	maximum = 0.479 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 17.9486
Total in-flight flits = 37078 (0 measured)
latency change    = 0.484775
throughput change = 0.0539491
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 456.187
	minimum = 22
	maximum = 989
Network latency average = 452.211
	minimum = 22
	maximum = 989
Slowest packet = 10256
Flit latency average = 649.513
	minimum = 5
	maximum = 1465
Slowest flit = 152081
Fragmentation average = 24.4834
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0151458
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.272281
	minimum = 0.14 (at node 154)
	maximum = 0.495 (at node 90)
Injected packet length average = 17.9626
Accepted packet length average = 17.9773
Total in-flight flits = 48790 (46736 measured)
latency change    = 0.154292
throughput change = 0.0249436
Class 0:
Packet latency average = 738.966
	minimum = 22
	maximum = 1917
Network latency average = 734.812
	minimum = 22
	maximum = 1900
Slowest packet = 10539
Flit latency average = 733.05
	minimum = 5
	maximum = 1883
Slowest flit = 189709
Fragmentation average = 31.8834
	minimum = 0
	maximum = 261
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.0150964
	minimum = 0.0105 (at node 23)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.271638
	minimum = 0.178 (at node 2)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 17.9936
Total in-flight flits = 58157 (58157 measured)
latency change    = 0.382669
throughput change = 0.00236796
Class 0:
Packet latency average = 866.965
	minimum = 22
	maximum = 2096
Network latency average = 862.721
	minimum = 22
	maximum = 2093
Slowest packet = 13109
Flit latency average = 820.855
	minimum = 5
	maximum = 2076
Slowest flit = 235979
Fragmentation average = 33.5077
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0150747
	minimum = 0.011 (at node 2)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.271691
	minimum = 0.184 (at node 2)
	maximum = 0.380333 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.023
Total in-flight flits = 68648 (68648 measured)
latency change    = 0.14764
throughput change = 0.000194896
Class 0:
Packet latency average = 962.096
	minimum = 22
	maximum = 2339
Network latency average = 957.833
	minimum = 22
	maximum = 2339
Slowest packet = 13589
Flit latency average = 901.376
	minimum = 5
	maximum = 2334
Slowest flit = 289715
Fragmentation average = 33.9009
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0181263
	minimum = 0.0125 (at node 55)
	maximum = 0.02475 (at node 104)
Accepted packet rate average = 0.0150404
	minimum = 0.011 (at node 104)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.326395
	minimum = 0.225 (at node 55)
	maximum = 0.4425 (at node 104)
Accepted flit rate average= 0.271159
	minimum = 0.19475 (at node 104)
	maximum = 0.36 (at node 128)
Injected packet length average = 18.0067
Accepted packet length average = 18.0287
Total in-flight flits = 79406 (79406 measured)
latency change    = 0.098879
throughput change = 0.00196238
Class 0:
Packet latency average = 1052.44
	minimum = 22
	maximum = 2610
Network latency average = 1048.2
	minimum = 22
	maximum = 2610
Slowest packet = 18561
Flit latency average = 982.967
	minimum = 5
	maximum = 2593
Slowest flit = 334115
Fragmentation average = 34.5982
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0180781
	minimum = 0.0132 (at node 121)
	maximum = 0.0226 (at node 104)
Accepted packet rate average = 0.0150979
	minimum = 0.0114 (at node 42)
	maximum = 0.0198 (at node 95)
Injected flit rate average = 0.325454
	minimum = 0.2376 (at node 121)
	maximum = 0.4068 (at node 104)
Accepted flit rate average= 0.271646
	minimum = 0.2052 (at node 42)
	maximum = 0.3534 (at node 95)
Injected packet length average = 18.0027
Accepted packet length average = 17.9923
Total in-flight flits = 88688 (88688 measured)
latency change    = 0.085838
throughput change = 0.0017927
Class 0:
Packet latency average = 1137.17
	minimum = 22
	maximum = 2938
Network latency average = 1132.98
	minimum = 22
	maximum = 2938
Slowest packet = 18115
Flit latency average = 1063.38
	minimum = 5
	maximum = 2921
Slowest flit = 326087
Fragmentation average = 34.9857
	minimum = 0
	maximum = 311
Injected packet rate average = 0.0180833
	minimum = 0.0145 (at node 69)
	maximum = 0.0225 (at node 104)
Accepted packet rate average = 0.0151111
	minimum = 0.0118333 (at node 42)
	maximum = 0.0195 (at node 103)
Injected flit rate average = 0.325551
	minimum = 0.261 (at node 69)
	maximum = 0.405 (at node 104)
Accepted flit rate average= 0.272113
	minimum = 0.2135 (at node 42)
	maximum = 0.351 (at node 103)
Injected packet length average = 18.0028
Accepted packet length average = 18.0075
Total in-flight flits = 98580 (98580 measured)
latency change    = 0.074513
throughput change = 0.00171625
Class 0:
Packet latency average = 1218.63
	minimum = 22
	maximum = 3173
Network latency average = 1214.46
	minimum = 22
	maximum = 3173
Slowest packet = 22588
Flit latency average = 1141.7
	minimum = 5
	maximum = 3183
Slowest flit = 423058
Fragmentation average = 35.2473
	minimum = 0
	maximum = 311
Injected packet rate average = 0.0180737
	minimum = 0.0144286 (at node 23)
	maximum = 0.022 (at node 104)
Accepted packet rate average = 0.0151451
	minimum = 0.0122857 (at node 80)
	maximum = 0.0185714 (at node 70)
Injected flit rate average = 0.325296
	minimum = 0.260571 (at node 55)
	maximum = 0.396 (at node 104)
Accepted flit rate average= 0.272613
	minimum = 0.223143 (at node 80)
	maximum = 0.334286 (at node 70)
Injected packet length average = 17.9984
Accepted packet length average = 18.0001
Total in-flight flits = 107924 (107924 measured)
latency change    = 0.066843
throughput change = 0.00183501
Draining all recorded packets ...
Class 0:
Remaining flits: 421434 421435 421436 421437 421438 421439 421440 421441 421442 421443 [...] (118247 flits)
Measured flits: 421434 421435 421436 421437 421438 421439 421440 421441 421442 421443 [...] (60698 flits)
Class 0:
Remaining flits: 499536 499537 499538 499539 499540 499541 499542 499543 499544 499545 [...] (125867 flits)
Measured flits: 499536 499537 499538 499539 499540 499541 499542 499543 499544 499545 [...] (17705 flits)
Class 0:
Remaining flits: 555891 555892 555893 556128 556129 556130 556131 556132 556133 556134 [...] (134415 flits)
Measured flits: 555891 555892 555893 556128 556129 556130 556131 556132 556133 556134 [...] (1479 flits)
Class 0:
Remaining flits: 597888 597889 597890 597891 597892 597893 597894 597895 597896 597897 [...] (143087 flits)
Measured flits: 597888 597889 597890 597891 597892 597893 597894 597895 597896 597897 [...] (90 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 670104 670105 670106 670107 670108 670109 670110 670111 670112 670113 [...] (98196 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 698598 698599 698600 698601 698602 698603 698604 698605 698606 698607 [...] (50767 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 711794 711795 711796 711797 711798 711799 711800 711801 711802 711803 [...] (10026 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 868998 868999 869000 869001 869002 869003 (6 flits)
Measured flits: (0 flits)
Time taken is 18271 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1467.85 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4530 (1 samples)
Network latency average = 1463.74 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4505 (1 samples)
Flit latency average = 1754.23 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6004 (1 samples)
Fragmentation average = 37.2628 (1 samples)
	minimum = 0 (1 samples)
	maximum = 353 (1 samples)
Injected packet rate average = 0.0180737 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0151451 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.325296 (1 samples)
	minimum = 0.260571 (1 samples)
	maximum = 0.396 (1 samples)
Accepted flit rate average = 0.272613 (1 samples)
	minimum = 0.223143 (1 samples)
	maximum = 0.334286 (1 samples)
Injected packet size average = 17.9984 (1 samples)
Accepted packet size average = 18.0001 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 13.9613
