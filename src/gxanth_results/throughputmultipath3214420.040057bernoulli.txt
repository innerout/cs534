BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 308.419
	minimum = 22
	maximum = 850
Network latency average = 293.447
	minimum = 22
	maximum = 797
Slowest packet = 1047
Flit latency average = 259.648
	minimum = 5
	maximum = 824
Slowest flit = 20531
Fragmentation average = 73.4197
	minimum = 0
	maximum = 454
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0146198
	minimum = 0.005 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.281995
	minimum = 0.105 (at node 174)
	maximum = 0.487 (at node 44)
Injected packet length average = 17.8489
Accepted packet length average = 19.2886
Total in-flight flits = 81145 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 569.866
	minimum = 22
	maximum = 1659
Network latency average = 552.039
	minimum = 22
	maximum = 1646
Slowest packet = 2363
Flit latency average = 512.374
	minimum = 5
	maximum = 1701
Slowest flit = 37077
Fragmentation average = 102.01
	minimum = 0
	maximum = 454
Injected packet rate average = 0.039651
	minimum = 0.029 (at node 32)
	maximum = 0.0515 (at node 161)
Accepted packet rate average = 0.0154089
	minimum = 0.008 (at node 153)
	maximum = 0.0225 (at node 63)
Injected flit rate average = 0.710776
	minimum = 0.522 (at node 32)
	maximum = 0.9215 (at node 161)
Accepted flit rate average= 0.290362
	minimum = 0.169 (at node 153)
	maximum = 0.4115 (at node 63)
Injected packet length average = 17.9258
Accepted packet length average = 18.8438
Total in-flight flits = 162569 (0 measured)
latency change    = 0.458787
throughput change = 0.0288164
Class 0:
Packet latency average = 1343
	minimum = 25
	maximum = 2477
Network latency average = 1318.95
	minimum = 22
	maximum = 2428
Slowest packet = 3860
Flit latency average = 1278.13
	minimum = 5
	maximum = 2459
Slowest flit = 68307
Fragmentation average = 148.953
	minimum = 0
	maximum = 434
Injected packet rate average = 0.0380312
	minimum = 0.018 (at node 39)
	maximum = 0.055 (at node 66)
Accepted packet rate average = 0.0165521
	minimum = 0.008 (at node 121)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.683969
	minimum = 0.333 (at node 39)
	maximum = 0.99 (at node 66)
Accepted flit rate average= 0.297641
	minimum = 0.116 (at node 147)
	maximum = 0.486 (at node 34)
Injected packet length average = 17.9844
Accepted packet length average = 17.9821
Total in-flight flits = 236984 (0 measured)
latency change    = 0.575677
throughput change = 0.0244545
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 150.974
	minimum = 23
	maximum = 1128
Network latency average = 76.2277
	minimum = 23
	maximum = 838
Slowest packet = 22575
Flit latency average = 1909.69
	minimum = 5
	maximum = 3100
Slowest flit = 111599
Fragmentation average = 6.88482
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0347135
	minimum = 0 (at node 0)
	maximum = 0.054 (at node 41)
Accepted packet rate average = 0.0156406
	minimum = 0.005 (at node 4)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.625443
	minimum = 0 (at node 0)
	maximum = 0.985 (at node 41)
Accepted flit rate average= 0.282401
	minimum = 0.093 (at node 36)
	maximum = 0.489 (at node 19)
Injected packet length average = 18.0173
Accepted packet length average = 18.0556
Total in-flight flits = 302823 (113115 measured)
latency change    = 7.89559
throughput change = 0.0539643
Class 0:
Packet latency average = 313.349
	minimum = 22
	maximum = 1811
Network latency average = 184.42
	minimum = 22
	maximum = 1651
Slowest packet = 23666
Flit latency average = 2236.04
	minimum = 5
	maximum = 3917
Slowest flit = 145109
Fragmentation average = 6.74734
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0331719
	minimum = 0.0025 (at node 120)
	maximum = 0.0495 (at node 103)
Accepted packet rate average = 0.0154115
	minimum = 0.007 (at node 36)
	maximum = 0.023 (at node 83)
Injected flit rate average = 0.597216
	minimum = 0.048 (at node 120)
	maximum = 0.8945 (at node 103)
Accepted flit rate average= 0.276763
	minimum = 0.129 (at node 36)
	maximum = 0.408 (at node 83)
Injected packet length average = 18.0037
Accepted packet length average = 17.9583
Total in-flight flits = 360207 (217560 measured)
latency change    = 0.518192
throughput change = 0.0203713
Class 0:
Packet latency average = 584.56
	minimum = 22
	maximum = 3358
Network latency average = 385.056
	minimum = 22
	maximum = 2971
Slowest packet = 22535
Flit latency average = 2527.36
	minimum = 5
	maximum = 4625
Slowest flit = 178811
Fragmentation average = 7.70103
	minimum = 0
	maximum = 117
Injected packet rate average = 0.0300139
	minimum = 0.00466667 (at node 120)
	maximum = 0.044 (at node 103)
Accepted packet rate average = 0.0154635
	minimum = 0.00766667 (at node 36)
	maximum = 0.021 (at node 83)
Injected flit rate average = 0.540625
	minimum = 0.086 (at node 120)
	maximum = 0.794 (at node 103)
Accepted flit rate average= 0.275417
	minimum = 0.138667 (at node 36)
	maximum = 0.372333 (at node 123)
Injected packet length average = 18.0125
Accepted packet length average = 17.8107
Total in-flight flits = 390248 (294307 measured)
latency change    = 0.463958
throughput change = 0.00488843
Class 0:
Packet latency average = 1337.68
	minimum = 22
	maximum = 4297
Network latency average = 1084.06
	minimum = 22
	maximum = 3983
Slowest packet = 22535
Flit latency average = 2832.34
	minimum = 5
	maximum = 5877
Slowest flit = 150253
Fragmentation average = 21.5168
	minimum = 0
	maximum = 365
Injected packet rate average = 0.0269102
	minimum = 0.0035 (at node 120)
	maximum = 0.03975 (at node 113)
Accepted packet rate average = 0.0153411
	minimum = 0.0095 (at node 36)
	maximum = 0.02 (at node 9)
Injected flit rate average = 0.48456
	minimum = 0.0645 (at node 120)
	maximum = 0.71475 (at node 113)
Accepted flit rate average= 0.273811
	minimum = 0.17625 (at node 36)
	maximum = 0.35725 (at node 9)
Injected packet length average = 18.0066
Accepted packet length average = 17.8482
Total in-flight flits = 399567 (345444 measured)
latency change    = 0.563003
throughput change = 0.00586342
Draining remaining packets ...
Class 0:
Remaining flits: 189267 189268 189269 229176 229177 229178 229179 229180 229181 229182 [...] (349514 flits)
Measured flits: 405666 405667 405668 405669 405670 405671 405672 405673 405674 405675 [...] (324282 flits)
Class 0:
Remaining flits: 229177 229178 229179 229180 229181 229182 229183 229184 229185 229186 [...] (299011 flits)
Measured flits: 405681 405682 405683 405698 405699 405700 405701 405756 405757 405758 [...] (288467 flits)
Class 0:
Remaining flits: 243607 243608 243609 243610 243611 259803 259804 259805 259806 259807 [...] (249682 flits)
Measured flits: 405756 405757 405758 405759 405760 405761 405762 405763 405764 405765 [...] (245859 flits)
Class 0:
Remaining flits: 263664 263665 263666 263667 263668 263669 263670 263671 263672 263673 [...] (201246 flits)
Measured flits: 405756 405757 405758 405759 405760 405761 405762 405763 405764 405765 [...] (200101 flits)
Class 0:
Remaining flits: 317934 317935 317936 317937 317938 317939 317940 317941 317942 317943 [...] (153749 flits)
Measured flits: 405756 405757 405758 405759 405760 405761 405762 405763 405764 405765 [...] (153464 flits)
Class 0:
Remaining flits: 377946 377947 377948 377949 377950 377951 377952 377953 377954 377955 [...] (106453 flits)
Measured flits: 417150 417151 417152 417153 417154 417155 417156 417157 417158 417159 [...] (106355 flits)
Class 0:
Remaining flits: 397134 397135 397136 397137 397138 397139 397140 397141 397142 397143 [...] (59581 flits)
Measured flits: 432288 432289 432290 432291 432292 432293 432294 432295 432296 432297 [...] (59563 flits)
Class 0:
Remaining flits: 455201 519660 519661 519662 519663 519664 519665 519666 519667 519668 [...] (14363 flits)
Measured flits: 455201 519660 519661 519662 519663 519664 519665 519666 519667 519668 [...] (14363 flits)
Class 0:
Remaining flits: 546039 546040 546041 546042 546043 546044 546045 546046 546047 626580 [...] (588 flits)
Measured flits: 546039 546040 546041 546042 546043 546044 546045 546046 546047 626580 [...] (588 flits)
Time taken is 16394 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6686.1 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11949 (1 samples)
Network latency average = 6389.14 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11896 (1 samples)
Flit latency average = 5295.35 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11879 (1 samples)
Fragmentation average = 61.8515 (1 samples)
	minimum = 0 (1 samples)
	maximum = 922 (1 samples)
Injected packet rate average = 0.0269102 (1 samples)
	minimum = 0.0035 (1 samples)
	maximum = 0.03975 (1 samples)
Accepted packet rate average = 0.0153411 (1 samples)
	minimum = 0.0095 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.48456 (1 samples)
	minimum = 0.0645 (1 samples)
	maximum = 0.71475 (1 samples)
Accepted flit rate average = 0.273811 (1 samples)
	minimum = 0.17625 (1 samples)
	maximum = 0.35725 (1 samples)
Injected packet size average = 18.0066 (1 samples)
Accepted packet size average = 17.8482 (1 samples)
Hops average = 5.17017 (1 samples)
Total run time 19.1441
