BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 171.4
	minimum = 22
	maximum = 510
Network latency average = 167.673
	minimum = 22
	maximum = 509
Slowest packet = 1296
Flit latency average = 138.194
	minimum = 5
	maximum = 539
Slowest flit = 28314
Fragmentation average = 29.6203
	minimum = 0
	maximum = 186
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.0129896
	minimum = 0.004 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.240182
	minimum = 0.072 (at node 41)
	maximum = 0.447 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 18.4904
Total in-flight flits = 15949 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 271.303
	minimum = 22
	maximum = 912
Network latency average = 267.424
	minimum = 22
	maximum = 912
Slowest packet = 3679
Flit latency average = 237.51
	minimum = 5
	maximum = 924
Slowest flit = 65658
Fragmentation average = 31.1164
	minimum = 0
	maximum = 190
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.0137344
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.251167
	minimum = 0.1385 (at node 153)
	maximum = 0.36 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 18.2874
Total in-flight flits = 26258 (0 measured)
latency change    = 0.368236
throughput change = 0.0437334
Class 0:
Packet latency average = 526.573
	minimum = 25
	maximum = 1195
Network latency average = 522.532
	minimum = 22
	maximum = 1179
Slowest packet = 6141
Flit latency average = 493.133
	minimum = 5
	maximum = 1163
Slowest flit = 112688
Fragmentation average = 32.3187
	minimum = 0
	maximum = 246
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0147917
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.26549
	minimum = 0.108 (at node 163)
	maximum = 0.479 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 17.9486
Total in-flight flits = 37078 (0 measured)
latency change    = 0.484775
throughput change = 0.0539491
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 456.13
	minimum = 22
	maximum = 989
Network latency average = 452.152
	minimum = 22
	maximum = 989
Slowest packet = 10256
Flit latency average = 649.569
	minimum = 5
	maximum = 1465
Slowest flit = 152081
Fragmentation average = 24.4346
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0151302
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.27226
	minimum = 0.14 (at node 154)
	maximum = 0.495 (at node 90)
Injected packet length average = 17.9626
Accepted packet length average = 17.9945
Total in-flight flits = 48794 (46749 measured)
latency change    = 0.154437
throughput change = 0.024869
Class 0:
Packet latency average = 739.224
	minimum = 22
	maximum = 1923
Network latency average = 735.065
	minimum = 22
	maximum = 1906
Slowest packet = 10539
Flit latency average = 733.224
	minimum = 5
	maximum = 1889
Slowest flit = 189719
Fragmentation average = 31.642
	minimum = 0
	maximum = 256
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.015099
	minimum = 0.0105 (at node 23)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.271641
	minimum = 0.178 (at node 2)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 17.9907
Total in-flight flits = 58156 (58156 measured)
latency change    = 0.382961
throughput change = 0.00228166
Class 0:
Packet latency average = 867.484
	minimum = 22
	maximum = 2103
Network latency average = 863.234
	minimum = 22
	maximum = 2100
Slowest packet = 13109
Flit latency average = 821.2
	minimum = 5
	maximum = 2083
Slowest flit = 235979
Fragmentation average = 33.3069
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0150781
	minimum = 0.011 (at node 2)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.271696
	minimum = 0.185667 (at node 2)
	maximum = 0.380333 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.0192
Total in-flight flits = 68645 (68645 measured)
latency change    = 0.147853
throughput change = 0.000204477
Class 0:
Packet latency average = 962.768
	minimum = 22
	maximum = 2319
Network latency average = 958.497
	minimum = 22
	maximum = 2282
Slowest packet = 15848
Flit latency average = 901.734
	minimum = 5
	maximum = 2265
Slowest flit = 285281
Fragmentation average = 33.7323
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0181263
	minimum = 0.0125 (at node 55)
	maximum = 0.02475 (at node 104)
Accepted packet rate average = 0.0150482
	minimum = 0.011 (at node 104)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.326395
	minimum = 0.225 (at node 55)
	maximum = 0.4425 (at node 104)
Accepted flit rate average= 0.271174
	minimum = 0.19825 (at node 104)
	maximum = 0.35775 (at node 128)
Injected packet length average = 18.0067
Accepted packet length average = 18.0204
Total in-flight flits = 79394 (79394 measured)
latency change    = 0.0989694
throughput change = 0.00192386
Class 0:
Packet latency average = 1053.22
	minimum = 22
	maximum = 2531
Network latency average = 1048.99
	minimum = 22
	maximum = 2531
Slowest packet = 18561
Flit latency average = 983.547
	minimum = 5
	maximum = 2514
Slowest flit = 334113
Fragmentation average = 34.1786
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0180781
	minimum = 0.0132 (at node 121)
	maximum = 0.0226 (at node 104)
Accepted packet rate average = 0.0150948
	minimum = 0.0114 (at node 42)
	maximum = 0.0196 (at node 95)
Injected flit rate average = 0.325454
	minimum = 0.2376 (at node 121)
	maximum = 0.4068 (at node 104)
Accepted flit rate average= 0.271511
	minimum = 0.2052 (at node 42)
	maximum = 0.3528 (at node 95)
Injected packet length average = 18.0027
Accepted packet length average = 17.9871
Total in-flight flits = 88817 (88817 measured)
latency change    = 0.0858827
throughput change = 0.00124112
Class 0:
Packet latency average = 1137.66
	minimum = 22
	maximum = 2656
Network latency average = 1133.47
	minimum = 22
	maximum = 2656
Slowest packet = 21685
Flit latency average = 1064
	minimum = 5
	maximum = 2663
Slowest flit = 392908
Fragmentation average = 34.0696
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0180833
	minimum = 0.0145 (at node 69)
	maximum = 0.0225 (at node 104)
Accepted packet rate average = 0.0151137
	minimum = 0.0118333 (at node 42)
	maximum = 0.0195 (at node 103)
Injected flit rate average = 0.325551
	minimum = 0.261 (at node 69)
	maximum = 0.405 (at node 104)
Accepted flit rate average= 0.27199
	minimum = 0.213 (at node 42)
	maximum = 0.351 (at node 103)
Injected packet length average = 18.0028
Accepted packet length average = 17.9963
Total in-flight flits = 98721 (98721 measured)
latency change    = 0.0742186
throughput change = 0.00176107
Class 0:
Packet latency average = 1220.03
	minimum = 22
	maximum = 2912
Network latency average = 1215.87
	minimum = 22
	maximum = 2912
Slowest packet = 23754
Flit latency average = 1142.91
	minimum = 5
	maximum = 2895
Slowest flit = 427589
Fragmentation average = 34.3918
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0180737
	minimum = 0.0144286 (at node 23)
	maximum = 0.022 (at node 104)
Accepted packet rate average = 0.0151458
	minimum = 0.0124286 (at node 63)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.325296
	minimum = 0.260571 (at node 55)
	maximum = 0.396 (at node 104)
Accepted flit rate average= 0.272461
	minimum = 0.223714 (at node 63)
	maximum = 0.339429 (at node 70)
Injected packet length average = 17.9984
Accepted packet length average = 17.9892
Total in-flight flits = 108128 (108128 measured)
latency change    = 0.0675196
throughput change = 0.00172817
Draining all recorded packets ...
Class 0:
Remaining flits: 493542 493543 493544 493545 493546 493547 493548 493549 493550 493551 [...] (118420 flits)
Measured flits: 493542 493543 493544 493545 493546 493547 493548 493549 493550 493551 [...] (60878 flits)
Class 0:
Remaining flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (126131 flits)
Measured flits: 534690 534691 534692 534693 534694 534695 534696 534697 534698 534699 [...] (17636 flits)
Class 0:
Remaining flits: 588546 588547 588548 588549 588550 588551 588552 588553 588554 588555 [...] (134844 flits)
Measured flits: 588546 588547 588548 588549 588550 588551 588552 588553 588554 588555 [...] (850 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 670117 670118 670119 670120 670121 672390 672391 672392 672393 672394 [...] (92347 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 705399 705400 705401 711792 711793 711794 711795 711796 711797 711798 [...] (45068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 761202 761203 761204 761205 761206 761207 761208 761209 761210 761211 [...] (6883 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 821419 821420 821421 821422 821423 821424 821425 821426 821427 821428 [...] (47 flits)
Measured flits: (0 flits)
Time taken is 17630 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1466.38 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3731 (1 samples)
Network latency average = 1462.27 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3731 (1 samples)
Flit latency average = 1697.51 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4684 (1 samples)
Fragmentation average = 35.3133 (1 samples)
	minimum = 0 (1 samples)
	maximum = 322 (1 samples)
Injected packet rate average = 0.0180737 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0151458 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.325296 (1 samples)
	minimum = 0.260571 (1 samples)
	maximum = 0.396 (1 samples)
Accepted flit rate average = 0.272461 (1 samples)
	minimum = 0.223714 (1 samples)
	maximum = 0.339429 (1 samples)
Injected packet size average = 17.9984 (1 samples)
Accepted packet size average = 17.9892 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 12.7929
