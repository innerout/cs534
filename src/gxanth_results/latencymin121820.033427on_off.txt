BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 309.937
	minimum = 25
	maximum = 975
Network latency average = 189.405
	minimum = 22
	maximum = 740
Slowest packet = 82
Flit latency average = 162.985
	minimum = 5
	maximum = 722
Slowest flit = 18773
Fragmentation average = 18.5051
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0170313
	minimum = 0 (at node 22)
	maximum = 0.037 (at node 74)
Accepted packet rate average = 0.0127969
	minimum = 0.003 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.303036
	minimum = 0 (at node 22)
	maximum = 0.65 (at node 74)
Accepted flit rate average= 0.235536
	minimum = 0.054 (at node 174)
	maximum = 0.417 (at node 44)
Injected packet length average = 17.793
Accepted packet length average = 18.4058
Total in-flight flits = 14861 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 563.885
	minimum = 25
	maximum = 1943
Network latency average = 253.339
	minimum = 22
	maximum = 1062
Slowest packet = 82
Flit latency average = 222.571
	minimum = 5
	maximum = 1045
Slowest flit = 31157
Fragmentation average = 20.3721
	minimum = 0
	maximum = 116
Injected packet rate average = 0.0157891
	minimum = 0.002 (at node 75)
	maximum = 0.0275 (at node 118)
Accepted packet rate average = 0.0134036
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.282589
	minimum = 0.036 (at node 75)
	maximum = 0.495 (at node 118)
Accepted flit rate average= 0.244062
	minimum = 0.1285 (at node 116)
	maximum = 0.3585 (at node 22)
Injected packet length average = 17.8977
Accepted packet length average = 18.2087
Total in-flight flits = 17034 (0 measured)
latency change    = 0.450355
throughput change = 0.0349338
Class 0:
Packet latency average = 1335.79
	minimum = 26
	maximum = 2818
Network latency average = 339.504
	minimum = 22
	maximum = 1564
Slowest packet = 4696
Flit latency average = 304.422
	minimum = 5
	maximum = 1521
Slowest flit = 47255
Fragmentation average = 22.2264
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0142708
	minimum = 0.003 (at node 15)
	maximum = 0.034 (at node 40)
Accepted packet rate average = 0.0140781
	minimum = 0.006 (at node 163)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.255943
	minimum = 0.054 (at node 15)
	maximum = 0.61 (at node 40)
Accepted flit rate average= 0.253
	minimum = 0.108 (at node 163)
	maximum = 0.486 (at node 16)
Injected packet length average = 17.9347
Accepted packet length average = 17.9711
Total in-flight flits = 17796 (0 measured)
latency change    = 0.577865
throughput change = 0.0353261
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1865.36
	minimum = 36
	maximum = 3681
Network latency average = 274.822
	minimum = 22
	maximum = 903
Slowest packet = 8966
Flit latency average = 312.199
	minimum = 5
	maximum = 1401
Slowest flit = 120761
Fragmentation average = 21.4474
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0143438
	minimum = 0 (at node 69)
	maximum = 0.028 (at node 172)
Accepted packet rate average = 0.0141771
	minimum = 0.006 (at node 173)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.257615
	minimum = 0 (at node 69)
	maximum = 0.496 (at node 90)
Accepted flit rate average= 0.255792
	minimum = 0.108 (at node 173)
	maximum = 0.488 (at node 129)
Injected packet length average = 17.9601
Accepted packet length average = 18.0426
Total in-flight flits = 18148 (18106 measured)
latency change    = 0.283893
throughput change = 0.0109138
Class 0:
Packet latency average = 2182.74
	minimum = 22
	maximum = 4336
Network latency average = 323.492
	minimum = 22
	maximum = 1598
Slowest packet = 8966
Flit latency average = 314.542
	minimum = 5
	maximum = 1566
Slowest flit = 173659
Fragmentation average = 22.703
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0143411
	minimum = 0.0045 (at node 129)
	maximum = 0.023 (at node 131)
Accepted packet rate average = 0.0142682
	minimum = 0.0075 (at node 5)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.258169
	minimum = 0.081 (at node 129)
	maximum = 0.418 (at node 131)
Accepted flit rate average= 0.256607
	minimum = 0.1295 (at node 5)
	maximum = 0.422 (at node 129)
Injected packet length average = 18.002
Accepted packet length average = 17.9845
Total in-flight flits = 18637 (18637 measured)
latency change    = 0.145407
throughput change = 0.00317647
Class 0:
Packet latency average = 2467.17
	minimum = 22
	maximum = 5336
Network latency average = 336.7
	minimum = 22
	maximum = 1605
Slowest packet = 8966
Flit latency average = 317.146
	minimum = 5
	maximum = 1578
Slowest flit = 209430
Fragmentation average = 22.6844
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0142656
	minimum = 0.00666667 (at node 21)
	maximum = 0.0213333 (at node 115)
Accepted packet rate average = 0.0141719
	minimum = 0.00966667 (at node 86)
	maximum = 0.0203333 (at node 0)
Injected flit rate average = 0.256684
	minimum = 0.118333 (at node 21)
	maximum = 0.384 (at node 115)
Accepted flit rate average= 0.255372
	minimum = 0.177667 (at node 36)
	maximum = 0.369 (at node 129)
Injected packet length average = 17.9932
Accepted packet length average = 18.0196
Total in-flight flits = 18770 (18770 measured)
latency change    = 0.115284
throughput change = 0.00483704
Class 0:
Packet latency average = 2754.97
	minimum = 22
	maximum = 6210
Network latency average = 342.943
	minimum = 22
	maximum = 1605
Slowest packet = 8966
Flit latency average = 318.855
	minimum = 5
	maximum = 1578
Slowest flit = 209430
Fragmentation average = 22.7238
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0141549
	minimum = 0.0075 (at node 19)
	maximum = 0.0215 (at node 47)
Accepted packet rate average = 0.0141367
	minimum = 0.01025 (at node 36)
	maximum = 0.01925 (at node 129)
Injected flit rate average = 0.254866
	minimum = 0.13325 (at node 19)
	maximum = 0.38675 (at node 47)
Accepted flit rate average= 0.25447
	minimum = 0.182 (at node 89)
	maximum = 0.3485 (at node 129)
Injected packet length average = 18.0054
Accepted packet length average = 18.0006
Total in-flight flits = 18365 (18365 measured)
latency change    = 0.104467
throughput change = 0.00354256
Class 0:
Packet latency average = 3041.25
	minimum = 22
	maximum = 7099
Network latency average = 347.016
	minimum = 22
	maximum = 1613
Slowest packet = 8966
Flit latency average = 320.306
	minimum = 5
	maximum = 1596
Slowest flit = 320687
Fragmentation average = 22.8704
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0141385
	minimum = 0.0086 (at node 19)
	maximum = 0.022 (at node 115)
Accepted packet rate average = 0.0140948
	minimum = 0.0102 (at node 86)
	maximum = 0.018 (at node 95)
Injected flit rate average = 0.254406
	minimum = 0.1526 (at node 19)
	maximum = 0.3966 (at node 115)
Accepted flit rate average= 0.253726
	minimum = 0.1836 (at node 86)
	maximum = 0.3262 (at node 95)
Injected packet length average = 17.9938
Accepted packet length average = 18.0014
Total in-flight flits = 18641 (18641 measured)
latency change    = 0.0941331
throughput change = 0.00293234
Class 0:
Packet latency average = 3329.62
	minimum = 22
	maximum = 7637
Network latency average = 349.949
	minimum = 22
	maximum = 1613
Slowest packet = 8966
Flit latency average = 321.392
	minimum = 5
	maximum = 1596
Slowest flit = 320687
Fragmentation average = 23.0292
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0140877
	minimum = 0.00933333 (at node 19)
	maximum = 0.0205 (at node 115)
Accepted packet rate average = 0.014079
	minimum = 0.0108333 (at node 86)
	maximum = 0.0181667 (at node 138)
Injected flit rate average = 0.253635
	minimum = 0.1675 (at node 19)
	maximum = 0.3695 (at node 115)
Accepted flit rate average= 0.253404
	minimum = 0.195 (at node 86)
	maximum = 0.327 (at node 138)
Injected packet length average = 18.004
Accepted packet length average = 17.9987
Total in-flight flits = 18231 (18231 measured)
latency change    = 0.0866062
throughput change = 0.00127226
Class 0:
Packet latency average = 3616.86
	minimum = 22
	maximum = 8207
Network latency average = 352.826
	minimum = 22
	maximum = 1681
Slowest packet = 8966
Flit latency average = 322.972
	minimum = 5
	maximum = 1664
Slowest flit = 373824
Fragmentation average = 22.9903
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0140112
	minimum = 0.00928571 (at node 83)
	maximum = 0.0194286 (at node 115)
Accepted packet rate average = 0.0140112
	minimum = 0.0112857 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.25226
	minimum = 0.168857 (at node 19)
	maximum = 0.348429 (at node 115)
Accepted flit rate average= 0.252182
	minimum = 0.205143 (at node 79)
	maximum = 0.324 (at node 138)
Injected packet length average = 18.0042
Accepted packet length average = 17.9986
Total in-flight flits = 18128 (18128 measured)
latency change    = 0.0794162
throughput change = 0.0048461
Draining all recorded packets ...
Class 0:
Remaining flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (18258 flits)
Measured flits: 461628 461629 461630 461631 461632 461633 461634 461635 461636 461637 [...] (18127 flits)
Class 0:
Remaining flits: 527022 527023 527024 527025 527026 527027 527028 527029 527030 527031 [...] (17806 flits)
Measured flits: 527022 527023 527024 527025 527026 527027 527028 527029 527030 527031 [...] (17566 flits)
Class 0:
Remaining flits: 571122 571123 571124 571125 571126 571127 571128 571129 571130 571131 [...] (18315 flits)
Measured flits: 571122 571123 571124 571125 571126 571127 571128 571129 571130 571131 [...] (17874 flits)
Class 0:
Remaining flits: 607626 607627 607628 607629 607630 607631 607632 607633 607634 607635 [...] (17982 flits)
Measured flits: 607626 607627 607628 607629 607630 607631 607632 607633 607634 607635 [...] (17352 flits)
Class 0:
Remaining flits: 679730 679731 679732 679733 682398 682399 682400 682401 682402 682403 [...] (18111 flits)
Measured flits: 679730 679731 679732 679733 682398 682399 682400 682401 682402 682403 [...] (16969 flits)
Class 0:
Remaining flits: 706266 706267 706268 706269 706270 706271 706272 706273 706274 706275 [...] (18826 flits)
Measured flits: 706266 706267 706268 706269 706270 706271 706272 706273 706274 706275 [...] (17274 flits)
Class 0:
Remaining flits: 770488 770489 771264 771265 771266 771267 771268 771269 771270 771271 [...] (18929 flits)
Measured flits: 770488 770489 771264 771265 771266 771267 771268 771269 771270 771271 [...] (17048 flits)
Class 0:
Remaining flits: 817542 817543 817544 817545 817546 817547 817548 817549 817550 817551 [...] (18434 flits)
Measured flits: 817542 817543 817544 817545 817546 817547 817548 817549 817550 817551 [...] (15986 flits)
Class 0:
Remaining flits: 860130 860131 860132 860133 860134 860135 860136 860137 860138 860139 [...] (18293 flits)
Measured flits: 860130 860131 860132 860133 860134 860135 860136 860137 860138 860139 [...] (14818 flits)
Class 0:
Remaining flits: 904716 904717 904718 904719 904720 904721 904722 904723 904724 904725 [...] (17580 flits)
Measured flits: 904716 904717 904718 904719 904720 904721 904722 904723 904724 904725 [...] (13256 flits)
Class 0:
Remaining flits: 915705 915706 915707 915708 915709 915710 915711 915712 915713 944622 [...] (18014 flits)
Measured flits: 915705 915706 915707 915708 915709 915710 915711 915712 915713 944622 [...] (12025 flits)
Class 0:
Remaining flits: 952218 952219 952220 952221 952222 952223 952224 952225 952226 952227 [...] (17830 flits)
Measured flits: 952218 952219 952220 952221 952222 952223 952224 952225 952226 952227 [...] (10495 flits)
Class 0:
Remaining flits: 974286 974287 974288 974289 974290 974291 974292 974293 974294 974295 [...] (18564 flits)
Measured flits: 1043676 1043677 1043678 1043679 1043680 1043681 1043682 1043683 1043684 1043685 [...] (9547 flits)
Class 0:
Remaining flits: 1048590 1048591 1048592 1048593 1048594 1048595 1048596 1048597 1048598 1048599 [...] (18628 flits)
Measured flits: 1048590 1048591 1048592 1048593 1048594 1048595 1048596 1048597 1048598 1048599 [...] (8249 flits)
Class 0:
Remaining flits: 1145499 1145500 1145501 1146456 1146457 1146458 1146459 1146460 1146461 1146462 [...] (18592 flits)
Measured flits: 1151460 1151461 1151462 1151463 1151464 1151465 1151466 1151467 1151468 1151469 [...] (7189 flits)
Class 0:
Remaining flits: 1192374 1192375 1192376 1192377 1192378 1192379 1192380 1192381 1192382 1192383 [...] (18763 flits)
Measured flits: 1193274 1193275 1193276 1193277 1193278 1193279 1193280 1193281 1193282 1193283 [...] (6081 flits)
Class 0:
Remaining flits: 1242342 1242343 1242344 1242345 1242346 1242347 1242348 1242349 1242350 1242351 [...] (18787 flits)
Measured flits: 1245077 1245402 1245403 1245404 1245405 1245406 1245407 1245408 1245409 1245410 [...] (4801 flits)
Class 0:
Remaining flits: 1281114 1281115 1281116 1281117 1281118 1281119 1281120 1281121 1281122 1281123 [...] (18567 flits)
Measured flits: 1281114 1281115 1281116 1281117 1281118 1281119 1281120 1281121 1281122 1281123 [...] (3881 flits)
Class 0:
Remaining flits: 1308240 1308241 1308242 1308243 1308244 1308245 1308246 1308247 1308248 1308249 [...] (18879 flits)
Measured flits: 1350576 1350577 1350578 1350579 1350580 1350581 1350582 1350583 1350584 1350585 [...] (3142 flits)
Class 0:
Remaining flits: 1382576 1382577 1382578 1382579 1383804 1383805 1383806 1383807 1383808 1383809 [...] (18356 flits)
Measured flits: 1406898 1406899 1406900 1406901 1406902 1406903 1406904 1406905 1406906 1406907 [...] (2310 flits)
Class 0:
Remaining flits: 1444086 1444087 1444088 1444089 1444090 1444091 1444092 1444093 1444094 1444095 [...] (18672 flits)
Measured flits: 1457478 1457479 1457480 1457481 1457482 1457483 1457484 1457485 1457486 1457487 [...] (1802 flits)
Class 0:
Remaining flits: 1488510 1488511 1488512 1488513 1488514 1488515 1488516 1488517 1488518 1488519 [...] (18401 flits)
Measured flits: 1496500 1496501 1513579 1513580 1513581 1513582 1513583 1516341 1516342 1516343 [...] (1396 flits)
Class 0:
Remaining flits: 1531383 1531384 1531385 1537610 1537611 1537612 1537613 1541628 1541629 1541630 [...] (18615 flits)
Measured flits: 1556795 1556796 1556797 1556798 1556799 1556800 1556801 1562832 1562833 1562834 [...] (903 flits)
Class 0:
Remaining flits: 1570644 1570645 1570646 1570647 1570648 1570649 1570650 1570651 1570652 1570653 [...] (18568 flits)
Measured flits: 1620936 1620937 1620938 1620939 1620940 1620941 1620942 1620943 1620944 1620945 [...] (721 flits)
Class 0:
Remaining flits: 1640186 1640187 1640188 1640189 1640190 1640191 1640192 1640193 1640194 1640195 [...] (18472 flits)
Measured flits: 1671750 1671751 1671752 1671753 1671754 1671755 1671756 1671757 1671758 1671759 [...] (545 flits)
Class 0:
Remaining flits: 1666026 1666027 1666028 1666029 1666030 1666031 1666032 1666033 1666034 1666035 [...] (18126 flits)
Measured flits: 1705104 1705105 1705106 1705107 1705108 1705109 1705110 1705111 1705112 1705113 [...] (488 flits)
Class 0:
Remaining flits: 1726472 1726473 1726474 1726475 1726476 1726477 1726478 1726479 1726480 1726481 [...] (18436 flits)
Measured flits: 1758816 1758817 1758818 1758819 1758820 1758821 1758822 1758823 1758824 1758825 [...] (461 flits)
Class 0:
Remaining flits: 1752789 1752790 1752791 1752792 1752793 1752794 1752795 1752796 1752797 1752798 [...] (18919 flits)
Measured flits: 1812168 1812169 1812170 1812171 1812172 1812173 1812174 1812175 1812176 1812177 [...] (198 flits)
Class 0:
Remaining flits: 1814616 1814617 1814618 1814619 1814620 1814621 1814622 1814623 1814624 1814625 [...] (18097 flits)
Measured flits: 1879755 1879756 1879757 1882134 1882135 1882136 1882137 1882138 1882139 1882140 [...] (93 flits)
Class 0:
Remaining flits: 1817334 1817335 1817336 1817337 1817338 1817339 1817340 1817341 1817342 1817343 [...] (18161 flits)
Measured flits: 1920690 1920691 1920692 1920693 1920694 1920695 1920696 1920697 1920698 1920699 [...] (126 flits)
Class 0:
Remaining flits: 1926054 1926055 1926056 1926057 1926058 1926059 1926060 1926061 1926062 1926063 [...] (18665 flits)
Measured flits: 1966896 1966897 1966898 1966899 1966900 1966901 1966902 1966903 1966904 1966905 [...] (85 flits)
Class 0:
Remaining flits: 1958295 1958296 1958297 1958298 1958299 1958300 1958301 1958302 1958303 1958304 [...] (18048 flits)
Measured flits: 2016648 2016649 2016650 2016651 2016652 2016653 2016654 2016655 2016656 2016657 [...] (18 flits)
Draining remaining packets ...
Time taken is 42761 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8759.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 32210 (1 samples)
Network latency average = 361.336 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2653 (1 samples)
Flit latency average = 328.226 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2636 (1 samples)
Fragmentation average = 22.4069 (1 samples)
	minimum = 0 (1 samples)
	maximum = 150 (1 samples)
Injected packet rate average = 0.0140112 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0194286 (1 samples)
Accepted packet rate average = 0.0140112 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.25226 (1 samples)
	minimum = 0.168857 (1 samples)
	maximum = 0.348429 (1 samples)
Accepted flit rate average = 0.252182 (1 samples)
	minimum = 0.205143 (1 samples)
	maximum = 0.324 (1 samples)
Injected packet size average = 18.0042 (1 samples)
Accepted packet size average = 17.9986 (1 samples)
Hops average = 5.06238 (1 samples)
Total run time 37.6294
