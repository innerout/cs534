BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 137.722
	minimum = 22
	maximum = 601
Network latency average = 129.171
	minimum = 22
	maximum = 595
Slowest packet = 475
Flit latency average = 103.188
	minimum = 5
	maximum = 578
Slowest flit = 8550
Fragmentation average = 18.1261
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0152813
	minimum = 0.004 (at node 12)
	maximum = 0.03 (at node 99)
Accepted packet rate average = 0.0126406
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.272714
	minimum = 0.072 (at node 12)
	maximum = 0.534 (at node 99)
Accepted flit rate average= 0.231979
	minimum = 0.072 (at node 174)
	maximum = 0.41 (at node 44)
Injected packet length average = 17.8463
Accepted packet length average = 18.3519
Total in-flight flits = 8632 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 194.65
	minimum = 22
	maximum = 1093
Network latency average = 168.756
	minimum = 22
	maximum = 1008
Slowest packet = 2432
Flit latency average = 140.727
	minimum = 5
	maximum = 991
Slowest flit = 45701
Fragmentation average = 19.327
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0149766
	minimum = 0.0075 (at node 96)
	maximum = 0.024 (at node 99)
Accepted packet rate average = 0.01325
	minimum = 0.0075 (at node 30)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.268109
	minimum = 0.135 (at node 96)
	maximum = 0.432 (at node 99)
Accepted flit rate average= 0.241104
	minimum = 0.1355 (at node 116)
	maximum = 0.351 (at node 22)
Injected packet length average = 17.9019
Accepted packet length average = 18.1965
Total in-flight flits = 11582 (0 measured)
latency change    = 0.292463
throughput change = 0.0378467
Class 0:
Packet latency average = 339.034
	minimum = 22
	maximum = 1640
Network latency average = 248.932
	minimum = 22
	maximum = 1370
Slowest packet = 3333
Flit latency average = 218.518
	minimum = 5
	maximum = 1353
Slowest flit = 76877
Fragmentation average = 21.0837
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0142604
	minimum = 0.005 (at node 11)
	maximum = 0.028 (at node 50)
Accepted packet rate average = 0.0139375
	minimum = 0.005 (at node 190)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.256406
	minimum = 0.088 (at node 11)
	maximum = 0.498 (at node 50)
Accepted flit rate average= 0.250016
	minimum = 0.09 (at node 190)
	maximum = 0.511 (at node 34)
Injected packet length average = 17.9803
Accepted packet length average = 17.9383
Total in-flight flits = 13151 (0 measured)
latency change    = 0.425869
throughput change = 0.0356436
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 416.577
	minimum = 22
	maximum = 1810
Network latency average = 234.578
	minimum = 22
	maximum = 942
Slowest packet = 8567
Flit latency average = 249.732
	minimum = 5
	maximum = 1648
Slowest flit = 119789
Fragmentation average = 20.7307
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0141354
	minimum = 0.001 (at node 19)
	maximum = 0.026 (at node 75)
Accepted packet rate average = 0.0137656
	minimum = 0.005 (at node 154)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.254141
	minimum = 0.018 (at node 19)
	maximum = 0.471 (at node 75)
Accepted flit rate average= 0.248271
	minimum = 0.103 (at node 154)
	maximum = 0.462 (at node 129)
Injected packet length average = 17.979
Accepted packet length average = 18.0356
Total in-flight flits = 14677 (14452 measured)
latency change    = 0.186144
throughput change = 0.00702778
Class 0:
Packet latency average = 512.28
	minimum = 22
	maximum = 2326
Network latency average = 278.11
	minimum = 22
	maximum = 1405
Slowest packet = 8567
Flit latency average = 266.611
	minimum = 5
	maximum = 1997
Slowest flit = 111671
Fragmentation average = 20.8367
	minimum = 0
	maximum = 104
Injected packet rate average = 0.0140078
	minimum = 0.005 (at node 81)
	maximum = 0.023 (at node 75)
Accepted packet rate average = 0.0135885
	minimum = 0.0055 (at node 4)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.251901
	minimum = 0.09 (at node 81)
	maximum = 0.4225 (at node 75)
Accepted flit rate average= 0.244766
	minimum = 0.1075 (at node 4)
	maximum = 0.414 (at node 129)
Injected packet length average = 17.9829
Accepted packet length average = 18.0126
Total in-flight flits = 16631 (16631 measured)
latency change    = 0.186818
throughput change = 0.0143207
Class 0:
Packet latency average = 596.63
	minimum = 22
	maximum = 2842
Network latency average = 299.322
	minimum = 22
	maximum = 1935
Slowest packet = 8567
Flit latency average = 278.141
	minimum = 5
	maximum = 1997
Slowest flit = 111671
Fragmentation average = 21.3982
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0138073
	minimum = 0.00566667 (at node 23)
	maximum = 0.02 (at node 75)
Accepted packet rate average = 0.0135608
	minimum = 0.00833333 (at node 36)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.248589
	minimum = 0.102 (at node 23)
	maximum = 0.363667 (at node 75)
Accepted flit rate average= 0.24412
	minimum = 0.155 (at node 36)
	maximum = 0.360667 (at node 129)
Injected packet length average = 18.0041
Accepted packet length average = 18.0019
Total in-flight flits = 16376 (16376 measured)
latency change    = 0.141378
throughput change = 0.00264556
Class 0:
Packet latency average = 662.993
	minimum = 22
	maximum = 3063
Network latency average = 308.698
	minimum = 22
	maximum = 2002
Slowest packet = 8567
Flit latency average = 283.441
	minimum = 5
	maximum = 1997
Slowest flit = 111671
Fragmentation average = 21.3486
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0137435
	minimum = 0.00675 (at node 62)
	maximum = 0.02025 (at node 10)
Accepted packet rate average = 0.0135599
	minimum = 0.01 (at node 64)
	maximum = 0.01925 (at node 129)
Injected flit rate average = 0.247358
	minimum = 0.1215 (at node 62)
	maximum = 0.36625 (at node 10)
Accepted flit rate average= 0.244142
	minimum = 0.177 (at node 162)
	maximum = 0.34925 (at node 129)
Injected packet length average = 17.9982
Accepted packet length average = 18.0047
Total in-flight flits = 16414 (16414 measured)
latency change    = 0.100096
throughput change = 9.06662e-05
Class 0:
Packet latency average = 727.977
	minimum = 22
	maximum = 3557
Network latency average = 314.791
	minimum = 22
	maximum = 2147
Slowest packet = 8567
Flit latency average = 287.589
	minimum = 5
	maximum = 2130
Slowest flit = 282527
Fragmentation average = 21.5028
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0137344
	minimum = 0.0066 (at node 98)
	maximum = 0.019 (at node 70)
Accepted packet rate average = 0.0135583
	minimum = 0.0096 (at node 64)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.24721
	minimum = 0.1188 (at node 98)
	maximum = 0.3394 (at node 70)
Accepted flit rate average= 0.244135
	minimum = 0.1728 (at node 64)
	maximum = 0.3356 (at node 128)
Injected packet length average = 17.9994
Accepted packet length average = 18.0063
Total in-flight flits = 16849 (16849 measured)
latency change    = 0.0892661
throughput change = 2.66672e-05
Class 0:
Packet latency average = 809.31
	minimum = 22
	maximum = 4167
Network latency average = 320.765
	minimum = 22
	maximum = 2678
Slowest packet = 8567
Flit latency average = 291.993
	minimum = 5
	maximum = 2627
Slowest flit = 283212
Fragmentation average = 21.5183
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0138099
	minimum = 0.00733333 (at node 98)
	maximum = 0.018 (at node 75)
Accepted packet rate average = 0.0136337
	minimum = 0.0105 (at node 79)
	maximum = 0.0178333 (at node 128)
Injected flit rate average = 0.248424
	minimum = 0.132 (at node 98)
	maximum = 0.326833 (at node 75)
Accepted flit rate average= 0.245448
	minimum = 0.189 (at node 79)
	maximum = 0.319 (at node 128)
Injected packet length average = 17.9889
Accepted packet length average = 18.0031
Total in-flight flits = 17369 (17369 measured)
latency change    = 0.100497
throughput change = 0.00534737
Class 0:
Packet latency average = 876.025
	minimum = 22
	maximum = 4167
Network latency average = 323.064
	minimum = 22
	maximum = 2678
Slowest packet = 8567
Flit latency average = 293.331
	minimum = 5
	maximum = 2627
Slowest flit = 283212
Fragmentation average = 21.6627
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0138713
	minimum = 0.00757143 (at node 98)
	maximum = 0.0177143 (at node 70)
Accepted packet rate average = 0.0137061
	minimum = 0.0108571 (at node 79)
	maximum = 0.0175714 (at node 128)
Injected flit rate average = 0.249632
	minimum = 0.136 (at node 98)
	maximum = 0.318857 (at node 70)
Accepted flit rate average= 0.246846
	minimum = 0.195429 (at node 79)
	maximum = 0.315143 (at node 138)
Injected packet length average = 17.9964
Accepted packet length average = 18.0099
Total in-flight flits = 17828 (17828 measured)
latency change    = 0.0761564
throughput change = 0.00566372
Draining all recorded packets ...
Class 0:
Remaining flits: 464292 464293 464294 464295 464296 464297 464298 464299 464300 464301 [...] (17818 flits)
Measured flits: 464292 464293 464294 464295 464296 464297 464298 464299 464300 464301 [...] (10499 flits)
Class 0:
Remaining flits: 519066 519067 519068 519069 519070 519071 519072 519073 519074 519075 [...] (17499 flits)
Measured flits: 519066 519067 519068 519069 519070 519071 519072 519073 519074 519075 [...] (4649 flits)
Class 0:
Remaining flits: 529794 529795 529796 529797 529798 529799 529800 529801 529802 529803 [...] (17562 flits)
Measured flits: 529794 529795 529796 529797 529798 529799 529800 529801 529802 529803 [...] (1276 flits)
Class 0:
Remaining flits: 615937 615938 615939 615940 615941 623700 623701 623702 623703 623704 [...] (17296 flits)
Measured flits: 631548 631549 631550 631551 631552 631553 631554 631555 631556 631557 [...] (540 flits)
Class 0:
Remaining flits: 636462 636463 636464 636465 636466 636467 636468 636469 636470 636471 [...] (17559 flits)
Measured flits: 700290 700291 700292 700293 700294 700295 700296 700297 700298 700299 [...] (180 flits)
Draining remaining packets ...
Time taken is 16638 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1095.97 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5953 (1 samples)
Network latency average = 336.335 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2678 (1 samples)
Flit latency average = 305.774 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2627 (1 samples)
Fragmentation average = 22.0287 (1 samples)
	minimum = 0 (1 samples)
	maximum = 130 (1 samples)
Injected packet rate average = 0.0138713 (1 samples)
	minimum = 0.00757143 (1 samples)
	maximum = 0.0177143 (1 samples)
Accepted packet rate average = 0.0137061 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.0175714 (1 samples)
Injected flit rate average = 0.249632 (1 samples)
	minimum = 0.136 (1 samples)
	maximum = 0.318857 (1 samples)
Accepted flit rate average = 0.246846 (1 samples)
	minimum = 0.195429 (1 samples)
	maximum = 0.315143 (1 samples)
Injected packet size average = 17.9964 (1 samples)
Accepted packet size average = 18.0099 (1 samples)
Hops average = 5.06826 (1 samples)
Total run time 13.5795
