BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 330.638
	minimum = 22
	maximum = 873
Network latency average = 307.419
	minimum = 22
	maximum = 793
Slowest packet = 578
Flit latency average = 284.828
	minimum = 5
	maximum = 807
Slowest flit = 25985
Fragmentation average = 25.2746
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0443958
	minimum = 0.031 (at node 153)
	maximum = 0.056 (at node 135)
Accepted packet rate average = 0.0151927
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.792385
	minimum = 0.544 (at node 178)
	maximum = 0.993 (at node 135)
Accepted flit rate average= 0.280094
	minimum = 0.108 (at node 174)
	maximum = 0.444 (at node 48)
Injected packet length average = 17.8482
Accepted packet length average = 18.4361
Total in-flight flits = 99960 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 657.232
	minimum = 22
	maximum = 1648
Network latency average = 621.439
	minimum = 22
	maximum = 1557
Slowest packet = 578
Flit latency average = 597.998
	minimum = 5
	maximum = 1580
Slowest flit = 63326
Fragmentation average = 26.1453
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0365495
	minimum = 0.022 (at node 172)
	maximum = 0.049 (at node 142)
Accepted packet rate average = 0.0152344
	minimum = 0.0095 (at node 96)
	maximum = 0.0215 (at node 22)
Injected flit rate average = 0.656115
	minimum = 0.396 (at node 172)
	maximum = 0.8805 (at node 142)
Accepted flit rate average= 0.277388
	minimum = 0.171 (at node 96)
	maximum = 0.387 (at node 22)
Injected packet length average = 17.9514
Accepted packet length average = 18.208
Total in-flight flits = 148111 (0 measured)
latency change    = 0.496924
throughput change = 0.00975431
Class 0:
Packet latency average = 1688.05
	minimum = 57
	maximum = 2505
Network latency average = 1573.36
	minimum = 22
	maximum = 2440
Slowest packet = 4705
Flit latency average = 1556.97
	minimum = 5
	maximum = 2423
Slowest flit = 84276
Fragmentation average = 22.7352
	minimum = 0
	maximum = 238
Injected packet rate average = 0.0149115
	minimum = 0.002 (at node 117)
	maximum = 0.037 (at node 19)
Accepted packet rate average = 0.0148125
	minimum = 0.003 (at node 36)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.269057
	minimum = 0.036 (at node 117)
	maximum = 0.653 (at node 19)
Accepted flit rate average= 0.265583
	minimum = 0.054 (at node 36)
	maximum = 0.538 (at node 16)
Injected packet length average = 18.0437
Accepted packet length average = 17.9297
Total in-flight flits = 148833 (0 measured)
latency change    = 0.610656
throughput change = 0.0444481
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1687.64
	minimum = 722
	maximum = 2637
Network latency average = 62.7172
	minimum = 22
	maximum = 910
Slowest packet = 17019
Flit latency average = 2119.1
	minimum = 5
	maximum = 3278
Slowest flit = 108645
Fragmentation average = 4.06061
	minimum = 0
	maximum = 17
Injected packet rate average = 0.0151823
	minimum = 0.002 (at node 183)
	maximum = 0.035 (at node 44)
Accepted packet rate average = 0.0149635
	minimum = 0.006 (at node 1)
	maximum = 0.025 (at node 166)
Injected flit rate average = 0.272979
	minimum = 0.035 (at node 183)
	maximum = 0.636 (at node 44)
Accepted flit rate average= 0.268635
	minimum = 0.108 (at node 135)
	maximum = 0.45 (at node 166)
Injected packet length average = 17.9801
Accepted packet length average = 17.9527
Total in-flight flits = 149743 (50658 measured)
latency change    = 0.000246306
throughput change = 0.0113614
Class 0:
Packet latency average = 2515.1
	minimum = 722
	maximum = 3779
Network latency average = 757.954
	minimum = 22
	maximum = 1961
Slowest packet = 17019
Flit latency average = 2318.45
	minimum = 5
	maximum = 4150
Slowest flit = 127924
Fragmentation average = 10.1682
	minimum = 0
	maximum = 110
Injected packet rate average = 0.0154063
	minimum = 0.0035 (at node 87)
	maximum = 0.03 (at node 101)
Accepted packet rate average = 0.0148984
	minimum = 0.0075 (at node 36)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.277682
	minimum = 0.063 (at node 87)
	maximum = 0.5385 (at node 101)
Accepted flit rate average= 0.268057
	minimum = 0.135 (at node 36)
	maximum = 0.4175 (at node 128)
Injected packet length average = 18.024
Accepted packet length average = 17.9923
Total in-flight flits = 152639 (98744 measured)
latency change    = 0.328999
throughput change = 0.00215672
Class 0:
Packet latency average = 3237.74
	minimum = 722
	maximum = 4719
Network latency average = 1388.76
	minimum = 22
	maximum = 2960
Slowest packet = 17019
Flit latency average = 2472.98
	minimum = 5
	maximum = 4802
Slowest flit = 182291
Fragmentation average = 14.5053
	minimum = 0
	maximum = 193
Injected packet rate average = 0.0152934
	minimum = 0.006 (at node 87)
	maximum = 0.0253333 (at node 135)
Accepted packet rate average = 0.0149097
	minimum = 0.00933333 (at node 36)
	maximum = 0.0203333 (at node 118)
Injected flit rate average = 0.275276
	minimum = 0.108 (at node 87)
	maximum = 0.456 (at node 135)
Accepted flit rate average= 0.268234
	minimum = 0.170333 (at node 36)
	maximum = 0.367333 (at node 118)
Injected packet length average = 17.9997
Accepted packet length average = 17.9906
Total in-flight flits = 153144 (132884 measured)
latency change    = 0.223192
throughput change = 0.000660181
Draining remaining packets ...
Class 0:
Remaining flits: 197478 197479 197480 197481 197482 197483 197484 197485 197486 197487 [...] (103798 flits)
Measured flits: 306519 306520 306521 306558 306559 306560 306561 306562 306563 306564 [...] (100258 flits)
Class 0:
Remaining flits: 273366 273367 273368 273369 273370 273371 273372 273373 273374 273375 [...] (55294 flits)
Measured flits: 308502 308503 308504 308505 308506 308507 308508 308509 308510 308511 [...] (55028 flits)
Class 0:
Remaining flits: 345561 345562 345563 346878 346879 346880 346881 346882 346883 346884 [...] (9246 flits)
Measured flits: 345561 345562 345563 346878 346879 346880 346881 346882 346883 346884 [...] (9246 flits)
Time taken is 9848 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5012.99 (1 samples)
	minimum = 722 (1 samples)
	maximum = 7360 (1 samples)
Network latency average = 2800.34 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5624 (1 samples)
Flit latency average = 2824.38 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6312 (1 samples)
Fragmentation average = 18.318 (1 samples)
	minimum = 0 (1 samples)
	maximum = 193 (1 samples)
Injected packet rate average = 0.0152934 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0253333 (1 samples)
Accepted packet rate average = 0.0149097 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.275276 (1 samples)
	minimum = 0.108 (1 samples)
	maximum = 0.456 (1 samples)
Accepted flit rate average = 0.268234 (1 samples)
	minimum = 0.170333 (1 samples)
	maximum = 0.367333 (1 samples)
Injected packet size average = 17.9997 (1 samples)
Accepted packet size average = 17.9906 (1 samples)
Hops average = 5.07106 (1 samples)
Total run time 13.8031
