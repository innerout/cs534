BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 269.2
	minimum = 22
	maximum = 836
Network latency average = 218.897
	minimum = 22
	maximum = 817
Slowest packet = 806
Flit latency average = 190.696
	minimum = 5
	maximum = 838
Slowest flit = 14367
Fragmentation average = 21.3276
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0184219
	minimum = 0.007 (at node 0)
	maximum = 0.031 (at node 111)
Accepted packet rate average = 0.0133854
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.327365
	minimum = 0.126 (at node 0)
	maximum = 0.555 (at node 111)
Accepted flit rate average= 0.246323
	minimum = 0.076 (at node 174)
	maximum = 0.412 (at node 44)
Injected packet length average = 17.7704
Accepted packet length average = 18.4023
Total in-flight flits = 18334 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 519.804
	minimum = 22
	maximum = 1511
Network latency average = 286.769
	minimum = 22
	maximum = 1362
Slowest packet = 891
Flit latency average = 254.52
	minimum = 5
	maximum = 1345
Slowest flit = 46331
Fragmentation average = 21.7451
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0163203
	minimum = 0.0085 (at node 146)
	maximum = 0.026 (at node 165)
Accepted packet rate average = 0.0137839
	minimum = 0.0075 (at node 116)
	maximum = 0.0205 (at node 166)
Injected flit rate average = 0.291604
	minimum = 0.153 (at node 146)
	maximum = 0.4645 (at node 165)
Accepted flit rate average= 0.2505
	minimum = 0.135 (at node 116)
	maximum = 0.369 (at node 166)
Injected packet length average = 17.8676
Accepted packet length average = 18.1734
Total in-flight flits = 18540 (0 measured)
latency change    = 0.482113
throughput change = 0.016675
Class 0:
Packet latency average = 1272.26
	minimum = 41
	maximum = 2224
Network latency average = 362.868
	minimum = 22
	maximum = 1672
Slowest packet = 5659
Flit latency average = 326.184
	minimum = 5
	maximum = 1645
Slowest flit = 69461
Fragmentation average = 22.8945
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140938
	minimum = 0.003 (at node 162)
	maximum = 0.025 (at node 153)
Accepted packet rate average = 0.013974
	minimum = 0.005 (at node 184)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.253786
	minimum = 0.067 (at node 162)
	maximum = 0.459 (at node 153)
Accepted flit rate average= 0.252068
	minimum = 0.098 (at node 184)
	maximum = 0.486 (at node 16)
Injected packet length average = 18.007
Accepted packet length average = 18.0384
Total in-flight flits = 18779 (0 measured)
latency change    = 0.591433
throughput change = 0.00621939
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1843.25
	minimum = 489
	maximum = 2928
Network latency average = 284.416
	minimum = 22
	maximum = 918
Slowest packet = 9101
Flit latency average = 328.024
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 21.0866
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0141563
	minimum = 0.003 (at node 75)
	maximum = 0.03 (at node 53)
Accepted packet rate average = 0.0140833
	minimum = 0.005 (at node 17)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.254672
	minimum = 0.054 (at node 75)
	maximum = 0.543 (at node 53)
Accepted flit rate average= 0.253724
	minimum = 0.102 (at node 17)
	maximum = 0.457 (at node 129)
Injected packet length average = 17.9901
Accepted packet length average = 18.0159
Total in-flight flits = 18844 (18645 measured)
latency change    = 0.309772
throughput change = 0.00652776
Class 0:
Packet latency average = 2113.72
	minimum = 489
	maximum = 3486
Network latency average = 327.971
	minimum = 22
	maximum = 1464
Slowest packet = 9101
Flit latency average = 325.077
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 21.9713
	minimum = 0
	maximum = 120
Injected packet rate average = 0.0140443
	minimum = 0.004 (at node 161)
	maximum = 0.0255 (at node 137)
Accepted packet rate average = 0.0141354
	minimum = 0.0095 (at node 2)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.252625
	minimum = 0.072 (at node 161)
	maximum = 0.4615 (at node 137)
Accepted flit rate average= 0.254083
	minimum = 0.168 (at node 2)
	maximum = 0.405 (at node 129)
Injected packet length average = 17.9878
Accepted packet length average = 17.9749
Total in-flight flits = 18321 (18321 measured)
latency change    = 0.127962
throughput change = 0.0014144
Class 0:
Packet latency average = 2374.36
	minimum = 489
	maximum = 4281
Network latency average = 341.01
	minimum = 22
	maximum = 1539
Slowest packet = 9101
Flit latency average = 325.373
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 22.158
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0139427
	minimum = 0.00566667 (at node 161)
	maximum = 0.0243333 (at node 137)
Accepted packet rate average = 0.0140191
	minimum = 0.009 (at node 36)
	maximum = 0.0206667 (at node 128)
Injected flit rate average = 0.251109
	minimum = 0.102 (at node 161)
	maximum = 0.439667 (at node 137)
Accepted flit rate average= 0.252415
	minimum = 0.166 (at node 36)
	maximum = 0.372 (at node 128)
Injected packet length average = 18.0101
Accepted packet length average = 18.0051
Total in-flight flits = 18036 (18036 measured)
latency change    = 0.109771
throughput change = 0.00660976
Class 0:
Packet latency average = 2635.26
	minimum = 489
	maximum = 4994
Network latency average = 346.881
	minimum = 22
	maximum = 1589
Slowest packet = 9101
Flit latency average = 325.275
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 22.3344
	minimum = 0
	maximum = 146
Injected packet rate average = 0.014013
	minimum = 0.00725 (at node 161)
	maximum = 0.0225 (at node 57)
Accepted packet rate average = 0.0140469
	minimum = 0.01 (at node 2)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.252432
	minimum = 0.1305 (at node 161)
	maximum = 0.402 (at node 57)
Accepted flit rate average= 0.252871
	minimum = 0.18325 (at node 64)
	maximum = 0.351 (at node 128)
Injected packet length average = 18.0141
Accepted packet length average = 18.0019
Total in-flight flits = 18488 (18488 measured)
latency change    = 0.0990044
throughput change = 0.00180394
Class 0:
Packet latency average = 2887.4
	minimum = 489
	maximum = 5796
Network latency average = 350.573
	minimum = 22
	maximum = 1667
Slowest packet = 9101
Flit latency average = 325.878
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 22.4029
	minimum = 0
	maximum = 173
Injected packet rate average = 0.0139865
	minimum = 0.007 (at node 161)
	maximum = 0.0214 (at node 57)
Accepted packet rate average = 0.0140156
	minimum = 0.0102 (at node 64)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.251854
	minimum = 0.126 (at node 161)
	maximum = 0.383 (at node 57)
Accepted flit rate average= 0.252371
	minimum = 0.1856 (at node 64)
	maximum = 0.3312 (at node 128)
Injected packet length average = 18.007
Accepted packet length average = 18.0064
Total in-flight flits = 18135 (18135 measured)
latency change    = 0.0873238
throughput change = 0.00198224
Class 0:
Packet latency average = 3152.24
	minimum = 489
	maximum = 6337
Network latency average = 353.048
	minimum = 22
	maximum = 1667
Slowest packet = 9101
Flit latency average = 326.268
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 22.5757
	minimum = 0
	maximum = 173
Injected packet rate average = 0.0139878
	minimum = 0.00883333 (at node 100)
	maximum = 0.02 (at node 57)
Accepted packet rate average = 0.0140139
	minimum = 0.0113333 (at node 102)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.251828
	minimum = 0.159 (at node 100)
	maximum = 0.358 (at node 57)
Accepted flit rate average= 0.252234
	minimum = 0.206167 (at node 64)
	maximum = 0.33 (at node 128)
Injected packet length average = 18.0034
Accepted packet length average = 17.9988
Total in-flight flits = 18276 (18276 measured)
latency change    = 0.0840178
throughput change = 0.000544442
Class 0:
Packet latency average = 3415.76
	minimum = 489
	maximum = 6736
Network latency average = 353.604
	minimum = 22
	maximum = 1667
Slowest packet = 9101
Flit latency average = 325.517
	minimum = 5
	maximum = 1931
Slowest flit = 71187
Fragmentation average = 22.5908
	minimum = 0
	maximum = 173
Injected packet rate average = 0.0140164
	minimum = 0.00985714 (at node 52)
	maximum = 0.0192857 (at node 57)
Accepted packet rate average = 0.0140253
	minimum = 0.0111429 (at node 79)
	maximum = 0.0181429 (at node 138)
Injected flit rate average = 0.252333
	minimum = 0.177429 (at node 52)
	maximum = 0.347143 (at node 57)
Accepted flit rate average= 0.252458
	minimum = 0.200571 (at node 79)
	maximum = 0.325714 (at node 138)
Injected packet length average = 18.0027
Accepted packet length average = 18.0002
Total in-flight flits = 18686 (18686 measured)
latency change    = 0.0771479
throughput change = 0.000887604
Draining all recorded packets ...
Class 0:
Remaining flits: 467118 467119 467120 467121 467122 467123 467124 467125 467126 467127 [...] (18168 flits)
Measured flits: 467118 467119 467120 467121 467122 467123 467124 467125 467126 467127 [...] (18168 flits)
Class 0:
Remaining flits: 531252 531253 531254 531255 531256 531257 531258 531259 531260 531261 [...] (18364 flits)
Measured flits: 531252 531253 531254 531255 531256 531257 531258 531259 531260 531261 [...] (18364 flits)
Class 0:
Remaining flits: 587268 587269 587270 587271 587272 587273 587274 587275 587276 587277 [...] (18299 flits)
Measured flits: 587268 587269 587270 587271 587272 587273 587274 587275 587276 587277 [...] (18299 flits)
Class 0:
Remaining flits: 620262 620263 620264 620265 620266 620267 620268 620269 620270 620271 [...] (18349 flits)
Measured flits: 620262 620263 620264 620265 620266 620267 620268 620269 620270 620271 [...] (18349 flits)
Class 0:
Remaining flits: 678816 678817 678818 678819 678820 678821 678822 678823 678824 678825 [...] (18182 flits)
Measured flits: 678816 678817 678818 678819 678820 678821 678822 678823 678824 678825 [...] (18056 flits)
Class 0:
Remaining flits: 731610 731611 731612 731613 731614 731615 731616 731617 731618 731619 [...] (18697 flits)
Measured flits: 731610 731611 731612 731613 731614 731615 731616 731617 731618 731619 [...] (18445 flits)
Class 0:
Remaining flits: 759634 759635 760841 775098 775099 775100 775101 775102 775103 775104 [...] (18187 flits)
Measured flits: 759634 759635 760841 775098 775099 775100 775101 775102 775103 775104 [...] (17448 flits)
Class 0:
Remaining flits: 805284 805285 805286 805287 805288 805289 805290 805291 805292 805293 [...] (18712 flits)
Measured flits: 805284 805285 805286 805287 805288 805289 805290 805291 805292 805293 [...] (17292 flits)
Class 0:
Remaining flits: 848502 848503 848504 848505 848506 848507 848508 848509 848510 848511 [...] (18413 flits)
Measured flits: 848502 848503 848504 848505 848506 848507 848508 848509 848510 848511 [...] (14681 flits)
Class 0:
Remaining flits: 914094 914095 914096 914097 914098 914099 914100 914101 914102 914103 [...] (18547 flits)
Measured flits: 927072 927073 927074 927075 927076 927077 927078 927079 927080 927081 [...] (11647 flits)
Class 0:
Remaining flits: 961884 961885 961886 961887 961888 961889 961890 961891 961892 961893 [...] (18290 flits)
Measured flits: 961884 961885 961886 961887 961888 961889 961890 961891 961892 961893 [...] (7686 flits)
Class 0:
Remaining flits: 1004256 1004257 1004258 1004259 1004260 1004261 1004262 1004263 1004264 1004265 [...] (18757 flits)
Measured flits: 1027080 1027081 1027082 1027083 1027084 1027085 1027086 1027087 1027088 1027089 [...] (4154 flits)
Class 0:
Remaining flits: 1062624 1062625 1062626 1062627 1062628 1062629 1069012 1069013 1069014 1069015 [...] (18676 flits)
Measured flits: 1077228 1077229 1077230 1077231 1077232 1077233 1077234 1077235 1077236 1077237 [...] (1923 flits)
Class 0:
Remaining flits: 1104264 1104265 1104266 1104267 1104268 1104269 1104270 1104271 1104272 1104273 [...] (18738 flits)
Measured flits: 1104264 1104265 1104266 1104267 1104268 1104269 1104270 1104271 1104272 1104273 [...] (689 flits)
Class 0:
Remaining flits: 1156668 1156669 1156670 1156671 1156672 1156673 1156674 1156675 1156676 1156677 [...] (18432 flits)
Measured flits: 1173510 1173511 1173512 1173513 1173514 1173515 1173516 1173517 1173518 1173519 [...] (252 flits)
Class 0:
Remaining flits: 1193364 1193365 1193366 1193367 1193368 1193369 1193370 1193371 1193372 1193373 [...] (18133 flits)
Measured flits: 1261242 1261243 1261244 1261245 1261246 1261247 1261248 1261249 1261250 1261251 [...] (72 flits)
Draining remaining packets ...
Time taken is 27076 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6367.86 (1 samples)
	minimum = 489 (1 samples)
	maximum = 16431 (1 samples)
Network latency average = 361.097 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2116 (1 samples)
Flit latency average = 327.931 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2099 (1 samples)
Fragmentation average = 22.7308 (1 samples)
	minimum = 0 (1 samples)
	maximum = 173 (1 samples)
Injected packet rate average = 0.0140164 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0192857 (1 samples)
Accepted packet rate average = 0.0140253 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.0181429 (1 samples)
Injected flit rate average = 0.252333 (1 samples)
	minimum = 0.177429 (1 samples)
	maximum = 0.347143 (1 samples)
Accepted flit rate average = 0.252458 (1 samples)
	minimum = 0.200571 (1 samples)
	maximum = 0.325714 (1 samples)
Injected packet size average = 18.0027 (1 samples)
Accepted packet size average = 18.0002 (1 samples)
Hops average = 5.06608 (1 samples)
Total run time 23.8207
