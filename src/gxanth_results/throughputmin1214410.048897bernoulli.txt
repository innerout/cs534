BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 356.56
	minimum = 27
	maximum = 943
Network latency average = 334.317
	minimum = 23
	maximum = 914
Slowest packet = 270
Flit latency average = 303.292
	minimum = 6
	maximum = 940
Slowest flit = 5429
Fragmentation average = 54.6519
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0397656
	minimum = 0.02 (at node 16)
	maximum = 0.053 (at node 170)
Accepted packet rate average = 0.0106823
	minimum = 0.004 (at node 10)
	maximum = 0.019 (at node 132)
Injected flit rate average = 0.709776
	minimum = 0.36 (at node 16)
	maximum = 0.95 (at node 170)
Accepted flit rate average= 0.199703
	minimum = 0.072 (at node 174)
	maximum = 0.342 (at node 132)
Injected packet length average = 17.849
Accepted packet length average = 18.6948
Total in-flight flits = 99375 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 725.472
	minimum = 27
	maximum = 1912
Network latency average = 672.37
	minimum = 23
	maximum = 1893
Slowest packet = 768
Flit latency average = 636.302
	minimum = 6
	maximum = 1876
Slowest flit = 13841
Fragmentation average = 62.2247
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0285234
	minimum = 0.015 (at node 168)
	maximum = 0.0375 (at node 62)
Accepted packet rate average = 0.0102578
	minimum = 0.0065 (at node 25)
	maximum = 0.016 (at node 103)
Injected flit rate average = 0.510521
	minimum = 0.2635 (at node 168)
	maximum = 0.675 (at node 62)
Accepted flit rate average= 0.188497
	minimum = 0.117 (at node 79)
	maximum = 0.295 (at node 103)
Injected packet length average = 17.8983
Accepted packet length average = 18.376
Total in-flight flits = 125923 (0 measured)
latency change    = 0.508513
throughput change = 0.0594477
Class 0:
Packet latency average = 1927.34
	minimum = 457
	maximum = 2803
Network latency average = 1713.52
	minimum = 26
	maximum = 2780
Slowest packet = 1626
Flit latency average = 1686.38
	minimum = 6
	maximum = 2805
Slowest flit = 25483
Fragmentation average = 71.7061
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0105521
	minimum = 0 (at node 7)
	maximum = 0.029 (at node 105)
Accepted packet rate average = 0.00933854
	minimum = 0.003 (at node 146)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.192578
	minimum = 0 (at node 7)
	maximum = 0.523 (at node 105)
Accepted flit rate average= 0.168036
	minimum = 0.047 (at node 134)
	maximum = 0.336 (at node 16)
Injected packet length average = 18.2502
Accepted packet length average = 17.9939
Total in-flight flits = 131244 (0 measured)
latency change    = 0.623589
throughput change = 0.121765
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2277.32
	minimum = 1378
	maximum = 3017
Network latency average = 152.509
	minimum = 25
	maximum = 740
Slowest packet = 13108
Flit latency average = 2414.13
	minimum = 6
	maximum = 3621
Slowest flit = 53973
Fragmentation average = 28.5263
	minimum = 0
	maximum = 133
Injected packet rate average = 0.00824479
	minimum = 0 (at node 31)
	maximum = 0.032 (at node 6)
Accepted packet rate average = 0.00921875
	minimum = 0.003 (at node 64)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.149255
	minimum = 0 (at node 31)
	maximum = 0.565 (at node 6)
Accepted flit rate average= 0.166135
	minimum = 0.043 (at node 153)
	maximum = 0.292 (at node 98)
Injected packet length average = 18.103
Accepted packet length average = 18.0215
Total in-flight flits = 127966 (27560 measured)
latency change    = 0.15368
throughput change = 0.0114427
Class 0:
Packet latency average = 2990.91
	minimum = 1378
	maximum = 4059
Network latency average = 612.353
	minimum = 25
	maximum = 1931
Slowest packet = 13108
Flit latency average = 2688.81
	minimum = 6
	maximum = 4474
Slowest flit = 73790
Fragmentation average = 46.2206
	minimum = 0
	maximum = 138
Injected packet rate average = 0.008125
	minimum = 0.0005 (at node 69)
	maximum = 0.0265 (at node 6)
Accepted packet rate average = 0.00915365
	minimum = 0.004 (at node 141)
	maximum = 0.014 (at node 24)
Injected flit rate average = 0.146534
	minimum = 0.005 (at node 114)
	maximum = 0.4775 (at node 6)
Accepted flit rate average= 0.164932
	minimum = 0.072 (at node 141)
	maximum = 0.252 (at node 24)
Injected packet length average = 18.0349
Accepted packet length average = 18.0182
Total in-flight flits = 124178 (52447 measured)
latency change    = 0.238587
throughput change = 0.00729466
Class 0:
Packet latency average = 3743.16
	minimum = 1378
	maximum = 5137
Network latency average = 1176.03
	minimum = 23
	maximum = 2858
Slowest packet = 13108
Flit latency average = 2922.42
	minimum = 6
	maximum = 5387
Slowest flit = 76211
Fragmentation average = 55.509
	minimum = 0
	maximum = 145
Injected packet rate average = 0.00834375
	minimum = 0.000666667 (at node 72)
	maximum = 0.0223333 (at node 6)
Accepted packet rate average = 0.00912674
	minimum = 0.00533333 (at node 31)
	maximum = 0.0126667 (at node 16)
Injected flit rate average = 0.150227
	minimum = 0.012 (at node 72)
	maximum = 0.398333 (at node 6)
Accepted flit rate average= 0.164372
	minimum = 0.0976667 (at node 36)
	maximum = 0.224667 (at node 152)
Injected packet length average = 18.0048
Accepted packet length average = 18.0099
Total in-flight flits = 123110 (76139 measured)
latency change    = 0.200967
throughput change = 0.00341156
Draining remaining packets ...
Class 0:
Remaining flits: 79056 79057 79058 79059 79060 79061 79062 79063 79064 79065 [...] (92397 flits)
Measured flits: 235925 235926 235927 235928 235929 235930 235931 235932 235933 235934 [...] (65199 flits)
Class 0:
Remaining flits: 109674 109675 109676 109677 109678 109679 109680 109681 109682 109683 [...] (61857 flits)
Measured flits: 235926 235927 235928 235929 235930 235931 235932 235933 235934 235935 [...] (48705 flits)
Class 0:
Remaining flits: 109674 109675 109676 109677 109678 109679 109680 109681 109682 109683 [...] (32423 flits)
Measured flits: 236034 236035 236036 236037 236038 236039 236040 236041 236042 236043 [...] (27459 flits)
Class 0:
Remaining flits: 170303 170304 170305 170306 170307 170308 170309 170310 170311 170312 [...] (7649 flits)
Measured flits: 236196 236197 236198 236199 236200 236201 236202 236203 236204 236205 [...] (6979 flits)
Time taken is 10754 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6365.52 (1 samples)
	minimum = 1378 (1 samples)
	maximum = 9353 (1 samples)
Network latency average = 3470.89 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7311 (1 samples)
Flit latency average = 3734.33 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9014 (1 samples)
Fragmentation average = 63.292 (1 samples)
	minimum = 0 (1 samples)
	maximum = 146 (1 samples)
Injected packet rate average = 0.00834375 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.00912674 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0126667 (1 samples)
Injected flit rate average = 0.150227 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.398333 (1 samples)
Accepted flit rate average = 0.164372 (1 samples)
	minimum = 0.0976667 (1 samples)
	maximum = 0.224667 (1 samples)
Injected packet size average = 18.0048 (1 samples)
Accepted packet size average = 18.0099 (1 samples)
Hops average = 5.09235 (1 samples)
Total run time 10.2125
