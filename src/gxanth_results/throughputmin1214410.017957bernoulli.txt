BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 234.711
	minimum = 23
	maximum = 836
Network latency average = 231.258
	minimum = 23
	maximum = 830
Slowest packet = 445
Flit latency average = 192.162
	minimum = 6
	maximum = 813
Slowest flit = 8027
Fragmentation average = 57.0389
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0179219
	minimum = 0.009 (at node 143)
	maximum = 0.029 (at node 167)
Accepted packet rate average = 0.00923958
	minimum = 0.004 (at node 23)
	maximum = 0.016 (at node 76)
Injected flit rate average = 0.319188
	minimum = 0.149 (at node 191)
	maximum = 0.522 (at node 167)
Accepted flit rate average= 0.174411
	minimum = 0.072 (at node 174)
	maximum = 0.304 (at node 76)
Injected packet length average = 17.8099
Accepted packet length average = 18.8766
Total in-flight flits = 28451 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 415.134
	minimum = 23
	maximum = 1661
Network latency average = 411.16
	minimum = 23
	maximum = 1660
Slowest packet = 1065
Flit latency average = 372.165
	minimum = 6
	maximum = 1643
Slowest flit = 19187
Fragmentation average = 62.6163
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0176224
	minimum = 0.0095 (at node 46)
	maximum = 0.0245 (at node 66)
Accepted packet rate average = 0.0095625
	minimum = 0.0055 (at node 30)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.316201
	minimum = 0.1695 (at node 46)
	maximum = 0.441 (at node 79)
Accepted flit rate average= 0.175927
	minimum = 0.099 (at node 83)
	maximum = 0.2705 (at node 76)
Injected packet length average = 17.9431
Accepted packet length average = 18.3976
Total in-flight flits = 54340 (0 measured)
latency change    = 0.434613
throughput change = 0.00861507
Class 0:
Packet latency average = 1020.34
	minimum = 24
	maximum = 2467
Network latency average = 1013.63
	minimum = 23
	maximum = 2467
Slowest packet = 1367
Flit latency average = 977.888
	minimum = 6
	maximum = 2450
Slowest flit = 24623
Fragmentation average = 68.9685
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0167604
	minimum = 0 (at node 160)
	maximum = 0.027 (at node 45)
Accepted packet rate average = 0.00975521
	minimum = 0.003 (at node 10)
	maximum = 0.019 (at node 68)
Injected flit rate average = 0.300516
	minimum = 0.01 (at node 160)
	maximum = 0.478 (at node 130)
Accepted flit rate average= 0.175896
	minimum = 0.052 (at node 153)
	maximum = 0.342 (at node 68)
Injected packet length average = 17.9301
Accepted packet length average = 18.031
Total in-flight flits = 78672 (0 measured)
latency change    = 0.59314
throughput change = 0.000177662
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 264.674
	minimum = 25
	maximum = 1508
Network latency average = 195.107
	minimum = 23
	maximum = 917
Slowest packet = 10007
Flit latency average = 1431.63
	minimum = 6
	maximum = 3362
Slowest flit = 32975
Fragmentation average = 25.8235
	minimum = 0
	maximum = 118
Injected packet rate average = 0.0150417
	minimum = 0 (at node 24)
	maximum = 0.029 (at node 113)
Accepted packet rate average = 0.00943229
	minimum = 0.002 (at node 18)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.271078
	minimum = 0 (at node 24)
	maximum = 0.522 (at node 113)
Accepted flit rate average= 0.16901
	minimum = 0.036 (at node 18)
	maximum = 0.302 (at node 16)
Injected packet length average = 18.0218
Accepted packet length average = 17.9183
Total in-flight flits = 98818 (49082 measured)
latency change    = 2.85507
throughput change = 0.0407396
Class 0:
Packet latency average = 851.124
	minimum = 23
	maximum = 2991
Network latency average = 722.305
	minimum = 23
	maximum = 1934
Slowest packet = 10007
Flit latency average = 1629.92
	minimum = 6
	maximum = 4369
Slowest flit = 33713
Fragmentation average = 49.7443
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138151
	minimum = 0.0005 (at node 12)
	maximum = 0.025 (at node 103)
Accepted packet rate average = 0.00928906
	minimum = 0.003 (at node 117)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.248992
	minimum = 0.009 (at node 12)
	maximum = 0.45 (at node 113)
Accepted flit rate average= 0.167646
	minimum = 0.06 (at node 117)
	maximum = 0.2615 (at node 16)
Injected packet length average = 18.0232
Accepted packet length average = 18.0477
Total in-flight flits = 110956 (83353 measured)
latency change    = 0.68903
throughput change = 0.00813968
Class 0:
Packet latency average = 1463.97
	minimum = 23
	maximum = 3643
Network latency average = 1262.66
	minimum = 23
	maximum = 2960
Slowest packet = 10007
Flit latency average = 1845.33
	minimum = 6
	maximum = 4626
Slowest flit = 71963
Fragmentation average = 60.9511
	minimum = 0
	maximum = 144
Injected packet rate average = 0.012875
	minimum = 0.00166667 (at node 24)
	maximum = 0.022 (at node 39)
Accepted packet rate average = 0.00924306
	minimum = 0.00533333 (at node 31)
	maximum = 0.014 (at node 90)
Injected flit rate average = 0.231823
	minimum = 0.03 (at node 24)
	maximum = 0.396667 (at node 39)
Accepted flit rate average= 0.1663
	minimum = 0.096 (at node 31)
	maximum = 0.245 (at node 90)
Injected packet length average = 18.0057
Accepted packet length average = 17.9919
Total in-flight flits = 117811 (104925 measured)
latency change    = 0.418621
throughput change = 0.0080907
Draining remaining packets ...
Class 0:
Remaining flits: 71802 71803 71804 71805 71806 71807 71808 71809 71810 71811 [...] (88235 flits)
Measured flits: 180036 180037 180038 180039 180040 180041 180042 180043 180044 180045 [...] (83245 flits)
Class 0:
Remaining flits: 96534 96535 96536 96537 96538 96539 96540 96541 96542 96543 [...] (57588 flits)
Measured flits: 180414 180415 180416 180417 180418 180419 180420 180421 180422 180423 [...] (55788 flits)
Class 0:
Remaining flits: 96534 96535 96536 96537 96538 96539 96540 96541 96542 96543 [...] (28261 flits)
Measured flits: 180792 180793 180794 180795 180796 180797 180798 180799 180800 180801 [...] (27844 flits)
Class 0:
Remaining flits: 159588 159589 159590 159591 159592 159593 159594 159595 159596 159597 [...] (4288 flits)
Measured flits: 183528 183529 183530 183531 183532 183533 183534 183535 183536 183537 [...] (4252 flits)
Time taken is 10563 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3326.08 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7139 (1 samples)
Network latency average = 3056.55 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7051 (1 samples)
Flit latency average = 2816.37 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7666 (1 samples)
Fragmentation average = 66.3714 (1 samples)
	minimum = 0 (1 samples)
	maximum = 170 (1 samples)
Injected packet rate average = 0.012875 (1 samples)
	minimum = 0.00166667 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.00924306 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.231823 (1 samples)
	minimum = 0.03 (1 samples)
	maximum = 0.396667 (1 samples)
Accepted flit rate average = 0.1663 (1 samples)
	minimum = 0.096 (1 samples)
	maximum = 0.245 (1 samples)
Injected packet size average = 18.0057 (1 samples)
Accepted packet size average = 17.9919 (1 samples)
Hops average = 5.1286 (1 samples)
Total run time 9.04588
