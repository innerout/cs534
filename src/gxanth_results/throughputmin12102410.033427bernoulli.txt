BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 293.935
	minimum = 23
	maximum = 933
Network latency average = 284.896
	minimum = 23
	maximum = 911
Slowest packet = 391
Flit latency average = 253.812
	minimum = 6
	maximum = 931
Slowest flit = 6339
Fragmentation average = 53.6281
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0329635
	minimum = 0.017 (at node 97)
	maximum = 0.048 (at node 118)
Accepted packet rate average = 0.0106302
	minimum = 0.004 (at node 81)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.587724
	minimum = 0.306 (at node 97)
	maximum = 0.862 (at node 118)
Accepted flit rate average= 0.199906
	minimum = 0.085 (at node 81)
	maximum = 0.331 (at node 132)
Injected packet length average = 17.8295
Accepted packet length average = 18.8055
Total in-flight flits = 75540 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.958
	minimum = 23
	maximum = 1821
Network latency average = 543.318
	minimum = 23
	maximum = 1805
Slowest packet = 763
Flit latency average = 512.143
	minimum = 6
	maximum = 1788
Slowest flit = 13751
Fragmentation average = 56.964
	minimum = 0
	maximum = 156
Injected packet rate average = 0.03325
	minimum = 0.0205 (at node 62)
	maximum = 0.043 (at node 117)
Accepted packet rate average = 0.0110781
	minimum = 0.006 (at node 153)
	maximum = 0.0165 (at node 44)
Injected flit rate average = 0.596021
	minimum = 0.369 (at node 62)
	maximum = 0.771 (at node 118)
Accepted flit rate average= 0.203974
	minimum = 0.109 (at node 174)
	maximum = 0.301 (at node 63)
Injected packet length average = 17.9254
Accepted packet length average = 18.4123
Total in-flight flits = 151498 (0 measured)
latency change    = 0.469391
throughput change = 0.0199423
Class 0:
Packet latency average = 1352.68
	minimum = 23
	maximum = 2684
Network latency average = 1339.57
	minimum = 23
	maximum = 2678
Slowest packet = 1361
Flit latency average = 1316.73
	minimum = 6
	maximum = 2680
Slowest flit = 34411
Fragmentation average = 59.4226
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0328594
	minimum = 0.018 (at node 111)
	maximum = 0.051 (at node 123)
Accepted packet rate average = 0.011276
	minimum = 0.005 (at node 20)
	maximum = 0.021 (at node 136)
Injected flit rate average = 0.591062
	minimum = 0.308 (at node 111)
	maximum = 0.925 (at node 123)
Accepted flit rate average= 0.202089
	minimum = 0.091 (at node 20)
	maximum = 0.368 (at node 136)
Injected packet length average = 17.9876
Accepted packet length average = 17.9219
Total in-flight flits = 226259 (0 measured)
latency change    = 0.590475
throughput change = 0.00932966
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 110.51
	minimum = 23
	maximum = 932
Network latency average = 83.2612
	minimum = 23
	maximum = 928
Slowest packet = 19174
Flit latency average = 1934.01
	minimum = 6
	maximum = 3550
Slowest flit = 49218
Fragmentation average = 10.092
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0311562
	minimum = 0.013 (at node 24)
	maximum = 0.053 (at node 26)
Accepted packet rate average = 0.0107917
	minimum = 0.002 (at node 76)
	maximum = 0.018 (at node 65)
Injected flit rate average = 0.560714
	minimum = 0.228 (at node 100)
	maximum = 0.956 (at node 26)
Accepted flit rate average= 0.193255
	minimum = 0.046 (at node 76)
	maximum = 0.336 (at node 95)
Injected packet length average = 17.9968
Accepted packet length average = 17.9078
Total in-flight flits = 296848 (100367 measured)
latency change    = 11.2404
throughput change = 0.0457081
Class 0:
Packet latency average = 227.345
	minimum = 23
	maximum = 1408
Network latency average = 168.545
	minimum = 23
	maximum = 1371
Slowest packet = 20993
Flit latency average = 2304.25
	minimum = 6
	maximum = 4405
Slowest flit = 61991
Fragmentation average = 9.80622
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0300234
	minimum = 0.014 (at node 100)
	maximum = 0.0455 (at node 26)
Accepted packet rate average = 0.010375
	minimum = 0.006 (at node 4)
	maximum = 0.0175 (at node 182)
Injected flit rate average = 0.540461
	minimum = 0.2495 (at node 100)
	maximum = 0.817 (at node 26)
Accepted flit rate average= 0.186984
	minimum = 0.108 (at node 4)
	maximum = 0.3275 (at node 182)
Injected packet length average = 18.0013
Accepted packet length average = 18.0226
Total in-flight flits = 362069 (194840 measured)
latency change    = 0.513911
throughput change = 0.0335367
Class 0:
Packet latency average = 401.938
	minimum = 23
	maximum = 2736
Network latency average = 300.004
	minimum = 23
	maximum = 2733
Slowest packet = 19889
Flit latency average = 2643.49
	minimum = 6
	maximum = 5376
Slowest flit = 69756
Fragmentation average = 9.93347
	minimum = 0
	maximum = 88
Injected packet rate average = 0.0294635
	minimum = 0.014 (at node 100)
	maximum = 0.0413333 (at node 39)
Accepted packet rate average = 0.0101424
	minimum = 0.00533333 (at node 4)
	maximum = 0.0153333 (at node 95)
Injected flit rate average = 0.53004
	minimum = 0.249333 (at node 100)
	maximum = 0.743667 (at node 39)
Accepted flit rate average= 0.182483
	minimum = 0.0986667 (at node 132)
	maximum = 0.283667 (at node 95)
Injected packet length average = 17.9897
Accepted packet length average = 17.9921
Total in-flight flits = 426663 (287825 measured)
latency change    = 0.434377
throughput change = 0.0246694
Draining remaining packets ...
Class 0:
Remaining flits: 77812 77813 82494 82495 82496 82497 82498 82499 82500 82501 [...] (394141 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (284089 flits)
Class 0:
Remaining flits: 104490 104491 104492 104493 104494 104495 104496 104497 104498 104499 [...] (361425 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (278678 flits)
Class 0:
Remaining flits: 104490 104491 104492 104493 104494 104495 104496 104497 104498 104499 [...] (331000 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (270564 flits)
Class 0:
Remaining flits: 122962 122963 122964 122965 122966 122967 122968 122969 122970 122971 [...] (300724 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (260553 flits)
Class 0:
Remaining flits: 132516 132517 132518 132519 132520 132521 132522 132523 132524 132525 [...] (271863 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (246492 flits)
Class 0:
Remaining flits: 182034 182035 182036 182037 182038 182039 182040 182041 182042 182043 [...] (242022 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (227011 flits)
Class 0:
Remaining flits: 199710 199711 199712 199713 199714 199715 199716 199717 199718 199719 [...] (212663 flits)
Measured flits: 343404 343405 343406 343407 343408 343409 343410 343411 343412 343413 [...] (204485 flits)
Class 0:
Remaining flits: 238209 238210 238211 242586 242587 242588 242589 242590 242591 242592 [...] (182324 flits)
Measured flits: 343530 343531 343532 343533 343534 343535 343536 343537 343538 343539 [...] (178227 flits)
Class 0:
Remaining flits: 250524 250525 250526 250527 250528 250529 250530 250531 250532 250533 [...] (152630 flits)
Measured flits: 343530 343531 343532 343533 343534 343535 343536 343537 343538 343539 [...] (150824 flits)
Class 0:
Remaining flits: 250524 250525 250526 250527 250528 250529 250530 250531 250532 250533 [...] (123199 flits)
Measured flits: 343980 343981 343982 343983 343984 343985 343986 343987 343988 343989 [...] (122389 flits)
Class 0:
Remaining flits: 250524 250525 250526 250527 250528 250529 250530 250531 250532 250533 [...] (94854 flits)
Measured flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (94482 flits)
Class 0:
Remaining flits: 318141 318142 318143 318144 318145 318146 318147 318148 318149 332982 [...] (66310 flits)
Measured flits: 358452 358453 358454 358455 358456 358457 358458 358459 358460 358461 [...] (66283 flits)
Class 0:
Remaining flits: 359194 359195 359196 359197 359198 359199 359200 359201 359202 359203 [...] (38226 flits)
Measured flits: 359194 359195 359196 359197 359198 359199 359200 359201 359202 359203 [...] (38226 flits)
Class 0:
Remaining flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (13353 flits)
Measured flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (13353 flits)
Class 0:
Remaining flits: 467473 467474 467475 467476 467477 489186 489187 489188 489189 489190 [...] (569 flits)
Measured flits: 467473 467474 467475 467476 467477 489186 489187 489188 489189 489190 [...] (569 flits)
Time taken is 21328 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10018.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16851 (1 samples)
Network latency average = 9928.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16851 (1 samples)
Flit latency average = 7944.55 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16834 (1 samples)
Fragmentation average = 66.5931 (1 samples)
	minimum = 0 (1 samples)
	maximum = 174 (1 samples)
Injected packet rate average = 0.0294635 (1 samples)
	minimum = 0.014 (1 samples)
	maximum = 0.0413333 (1 samples)
Accepted packet rate average = 0.0101424 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.53004 (1 samples)
	minimum = 0.249333 (1 samples)
	maximum = 0.743667 (1 samples)
Accepted flit rate average = 0.182483 (1 samples)
	minimum = 0.0986667 (1 samples)
	maximum = 0.283667 (1 samples)
Injected packet size average = 17.9897 (1 samples)
Accepted packet size average = 17.9921 (1 samples)
Hops average = 5.16621 (1 samples)
Total run time 18.7939
