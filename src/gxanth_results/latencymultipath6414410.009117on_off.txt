BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 192.438
	minimum = 23
	maximum = 817
Network latency average = 159.794
	minimum = 23
	maximum = 730
Slowest packet = 197
Flit latency average = 100.077
	minimum = 6
	maximum = 717
Slowest flit = 11173
Fragmentation average = 88.9827
	minimum = 0
	maximum = 591
Injected packet rate average = 0.00939062
	minimum = 0 (at node 26)
	maximum = 0.033 (at node 37)
Accepted packet rate average = 0.00750521
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 76)
Injected flit rate average = 0.167693
	minimum = 0 (at node 26)
	maximum = 0.594 (at node 37)
Accepted flit rate average= 0.146047
	minimum = 0.018 (at node 41)
	maximum = 0.27 (at node 76)
Injected packet length average = 17.8575
Accepted packet length average = 19.4594
Total in-flight flits = 4413 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 220.628
	minimum = 23
	maximum = 1342
Network latency average = 185.113
	minimum = 23
	maximum = 1283
Slowest packet = 550
Flit latency average = 119.511
	minimum = 6
	maximum = 1266
Slowest flit = 9917
Fragmentation average = 100.628
	minimum = 0
	maximum = 915
Injected packet rate average = 0.00889063
	minimum = 0.0005 (at node 184)
	maximum = 0.021 (at node 20)
Accepted packet rate average = 0.0078099
	minimum = 0.004 (at node 115)
	maximum = 0.0135 (at node 44)
Injected flit rate average = 0.159396
	minimum = 0.009 (at node 184)
	maximum = 0.371 (at node 20)
Accepted flit rate average= 0.146484
	minimum = 0.081 (at node 30)
	maximum = 0.243 (at node 44)
Injected packet length average = 17.9285
Accepted packet length average = 18.7563
Total in-flight flits = 5202 (0 measured)
latency change    = 0.12777
throughput change = 0.00298667
Class 0:
Packet latency average = 310.203
	minimum = 27
	maximum = 2244
Network latency average = 272.203
	minimum = 23
	maximum = 2137
Slowest packet = 1352
Flit latency average = 185.83
	minimum = 6
	maximum = 2120
Slowest flit = 24353
Fragmentation average = 145.376
	minimum = 0
	maximum = 1190
Injected packet rate average = 0.00902604
	minimum = 0 (at node 5)
	maximum = 0.036 (at node 162)
Accepted packet rate average = 0.00855208
	minimum = 0.002 (at node 153)
	maximum = 0.016 (at node 179)
Injected flit rate average = 0.162359
	minimum = 0 (at node 5)
	maximum = 0.648 (at node 162)
Accepted flit rate average= 0.155271
	minimum = 0.036 (at node 153)
	maximum = 0.287 (at node 179)
Injected packet length average = 17.9879
Accepted packet length average = 18.1559
Total in-flight flits = 6584 (0 measured)
latency change    = 0.288765
throughput change = 0.056588
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 264.9
	minimum = 23
	maximum = 1204
Network latency average = 228.328
	minimum = 23
	maximum = 857
Slowest packet = 5155
Flit latency average = 204.132
	minimum = 6
	maximum = 2080
Slowest flit = 39005
Fragmentation average = 129.153
	minimum = 0
	maximum = 617
Injected packet rate average = 0.00923958
	minimum = 0 (at node 47)
	maximum = 0.032 (at node 74)
Accepted packet rate average = 0.00888021
	minimum = 0.002 (at node 184)
	maximum = 0.016 (at node 24)
Injected flit rate average = 0.166469
	minimum = 0 (at node 47)
	maximum = 0.583 (at node 74)
Accepted flit rate average= 0.160766
	minimum = 0.041 (at node 184)
	maximum = 0.305 (at node 119)
Injected packet length average = 18.0169
Accepted packet length average = 18.1038
Total in-flight flits = 7649 (7287 measured)
latency change    = 0.171022
throughput change = 0.0341789
Class 0:
Packet latency average = 311.535
	minimum = 23
	maximum = 1949
Network latency average = 272.147
	minimum = 23
	maximum = 1758
Slowest packet = 5155
Flit latency average = 213.804
	minimum = 6
	maximum = 2550
Slowest flit = 71783
Fragmentation average = 145.483
	minimum = 0
	maximum = 1301
Injected packet rate average = 0.00933333
	minimum = 0.001 (at node 49)
	maximum = 0.0235 (at node 37)
Accepted packet rate average = 0.00901302
	minimum = 0.004 (at node 163)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.167958
	minimum = 0.018 (at node 49)
	maximum = 0.43 (at node 37)
Accepted flit rate average= 0.163432
	minimum = 0.082 (at node 117)
	maximum = 0.2915 (at node 16)
Injected packet length average = 17.9955
Accepted packet length average = 18.1329
Total in-flight flits = 8338 (8221 measured)
latency change    = 0.149694
throughput change = 0.0163166
Class 0:
Packet latency average = 331.757
	minimum = 23
	maximum = 2465
Network latency average = 293.801
	minimum = 23
	maximum = 2314
Slowest packet = 5358
Flit latency average = 223.835
	minimum = 6
	maximum = 3435
Slowest flit = 77147
Fragmentation average = 151.021
	minimum = 0
	maximum = 1501
Injected packet rate average = 0.00898611
	minimum = 0.001 (at node 49)
	maximum = 0.019 (at node 58)
Accepted packet rate average = 0.00896528
	minimum = 0.00533333 (at node 117)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.16178
	minimum = 0.018 (at node 49)
	maximum = 0.342 (at node 58)
Accepted flit rate average= 0.161717
	minimum = 0.0993333 (at node 190)
	maximum = 0.284333 (at node 16)
Injected packet length average = 18.0033
Accepted packet length average = 18.0381
Total in-flight flits = 6603 (6539 measured)
latency change    = 0.0609544
throughput change = 0.0106067
Class 0:
Packet latency average = 334.578
	minimum = 23
	maximum = 3450
Network latency average = 296.975
	minimum = 23
	maximum = 3363
Slowest packet = 5513
Flit latency average = 222.315
	minimum = 6
	maximum = 4473
Slowest flit = 72467
Fragmentation average = 151.288
	minimum = 0
	maximum = 2631
Injected packet rate average = 0.00886849
	minimum = 0.00225 (at node 66)
	maximum = 0.017 (at node 88)
Accepted packet rate average = 0.0089375
	minimum = 0.005 (at node 4)
	maximum = 0.01425 (at node 16)
Injected flit rate average = 0.159542
	minimum = 0.0405 (at node 66)
	maximum = 0.306 (at node 88)
Accepted flit rate average= 0.160591
	minimum = 0.087 (at node 4)
	maximum = 0.257 (at node 16)
Injected packet length average = 17.9897
Accepted packet length average = 17.9682
Total in-flight flits = 5848 (5848 measured)
latency change    = 0.00843323
throughput change = 0.00701077
Class 0:
Packet latency average = 332.545
	minimum = 23
	maximum = 3856
Network latency average = 295.584
	minimum = 23
	maximum = 3781
Slowest packet = 6827
Flit latency average = 217.654
	minimum = 6
	maximum = 4473
Slowest flit = 72467
Fragmentation average = 150.925
	minimum = 0
	maximum = 2631
Injected packet rate average = 0.00883958
	minimum = 0.0026 (at node 13)
	maximum = 0.0168 (at node 179)
Accepted packet rate average = 0.00878333
	minimum = 0.005 (at node 4)
	maximum = 0.0132 (at node 16)
Injected flit rate average = 0.159208
	minimum = 0.0468 (at node 13)
	maximum = 0.3022 (at node 179)
Accepted flit rate average= 0.158501
	minimum = 0.0946 (at node 4)
	maximum = 0.241 (at node 16)
Injected packet length average = 18.0108
Accepted packet length average = 18.0457
Total in-flight flits = 7171 (7171 measured)
latency change    = 0.00611384
throughput change = 0.0131867
Class 0:
Packet latency average = 336.779
	minimum = 23
	maximum = 4009
Network latency average = 299.668
	minimum = 23
	maximum = 3993
Slowest packet = 8664
Flit latency average = 218.588
	minimum = 6
	maximum = 4473
Slowest flit = 72467
Fragmentation average = 153.126
	minimum = 0
	maximum = 2631
Injected packet rate average = 0.00889583
	minimum = 0.00266667 (at node 82)
	maximum = 0.0156667 (at node 135)
Accepted packet rate average = 0.00883507
	minimum = 0.006 (at node 132)
	maximum = 0.0123333 (at node 128)
Injected flit rate average = 0.160128
	minimum = 0.048 (at node 82)
	maximum = 0.282833 (at node 179)
Accepted flit rate average= 0.159332
	minimum = 0.115 (at node 132)
	maximum = 0.225333 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 18.0341
Total in-flight flits = 7497 (7497 measured)
latency change    = 0.0125727
throughput change = 0.00521817
Draining all recorded packets ...
Class 0:
Remaining flits: 167688 167689 167690 167691 167692 167693 167694 167695 167696 167697 [...] (7799 flits)
Measured flits: 167688 167689 167690 167691 167692 167693 167694 167695 167696 167697 [...] (619 flits)
Class 0:
Remaining flits: 167688 167689 167690 167691 167692 167693 167694 167695 167696 167697 [...] (11018 flits)
Measured flits: 167688 167689 167690 167691 167692 167693 167694 167695 167696 167697 [...] (161 flits)
Class 0:
Remaining flits: 261636 261637 261638 261639 261640 261641 261642 261643 261644 261645 [...] (10399 flits)
Measured flits: 261636 261637 261638 261639 261640 261641 261642 261643 261644 261645 [...] (46 flits)
Class 0:
Remaining flits: 276184 276185 276186 276187 276188 276189 276190 276191 289638 289639 [...] (8570 flits)
Measured flits: 276184 276185 276186 276187 276188 276189 276190 276191 (8 flits)
Draining remaining packets ...
Time taken is 14384 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 360.073 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5942 (1 samples)
Network latency average = 322.147 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5859 (1 samples)
Flit latency average = 249.684 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5842 (1 samples)
Fragmentation average = 159.707 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2631 (1 samples)
Injected packet rate average = 0.00889583 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.0156667 (1 samples)
Accepted packet rate average = 0.00883507 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0123333 (1 samples)
Injected flit rate average = 0.160128 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.282833 (1 samples)
Accepted flit rate average = 0.159332 (1 samples)
	minimum = 0.115 (1 samples)
	maximum = 0.225333 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0341 (1 samples)
Hops average = 5.08631 (1 samples)
Total run time 10.5557
