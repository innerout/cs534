BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 274.84
	minimum = 22
	maximum = 937
Network latency average = 196.988
	minimum = 22
	maximum = 784
Slowest packet = 69
Flit latency average = 166.613
	minimum = 5
	maximum = 767
Slowest flit = 12131
Fragmentation average = 36.1574
	minimum = 0
	maximum = 292
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.012776
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 48)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.238
	minimum = 0.076 (at node 174)
	maximum = 0.396 (at node 48)
Injected packet length average = 17.8417
Accepted packet length average = 18.6286
Total in-flight flits = 27078 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 444.105
	minimum = 22
	maximum = 1723
Network latency average = 339.312
	minimum = 22
	maximum = 1411
Slowest packet = 69
Flit latency average = 307.422
	minimum = 5
	maximum = 1486
Slowest flit = 33853
Fragmentation average = 47.9795
	minimum = 0
	maximum = 350
Injected packet rate average = 0.0217734
	minimum = 0.0005 (at node 9)
	maximum = 0.0535 (at node 87)
Accepted packet rate average = 0.01375
	minimum = 0.0075 (at node 153)
	maximum = 0.0205 (at node 98)
Injected flit rate average = 0.390318
	minimum = 0.009 (at node 9)
	maximum = 0.9585 (at node 87)
Accepted flit rate average= 0.253206
	minimum = 0.14 (at node 153)
	maximum = 0.369 (at node 98)
Injected packet length average = 17.9263
Accepted packet length average = 18.415
Total in-flight flits = 53267 (0 measured)
latency change    = 0.381137
throughput change = 0.0600529
Class 0:
Packet latency average = 960.167
	minimum = 24
	maximum = 2563
Network latency average = 799.821
	minimum = 22
	maximum = 2088
Slowest packet = 3212
Flit latency average = 762.667
	minimum = 5
	maximum = 2077
Slowest flit = 64868
Fragmentation average = 66.4145
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0219635
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.0151667
	minimum = 0.007 (at node 134)
	maximum = 0.031 (at node 16)
Injected flit rate average = 0.39499
	minimum = 0 (at node 5)
	maximum = 1 (at node 40)
Accepted flit rate average= 0.271891
	minimum = 0.126 (at node 190)
	maximum = 0.551 (at node 16)
Injected packet length average = 17.9839
Accepted packet length average = 17.9269
Total in-flight flits = 76970 (0 measured)
latency change    = 0.537471
throughput change = 0.0687221
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 295.106
	minimum = 22
	maximum = 1128
Network latency average = 185.144
	minimum = 22
	maximum = 951
Slowest packet = 12589
Flit latency average = 1052.29
	minimum = 5
	maximum = 2708
Slowest flit = 94361
Fragmentation average = 15.4113
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0220104
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 156)
Accepted packet rate average = 0.0150938
	minimum = 0.006 (at node 138)
	maximum = 0.028 (at node 34)
Injected flit rate average = 0.396823
	minimum = 0 (at node 32)
	maximum = 0.999 (at node 156)
Accepted flit rate average= 0.273844
	minimum = 0.119 (at node 84)
	maximum = 0.481 (at node 115)
Injected packet length average = 18.0289
Accepted packet length average = 18.1429
Total in-flight flits = 100478 (65573 measured)
latency change    = 2.25363
throughput change = 0.00713226
Class 0:
Packet latency average = 804.949
	minimum = 22
	maximum = 2789
Network latency average = 684.309
	minimum = 22
	maximum = 1941
Slowest packet = 12589
Flit latency average = 1207.34
	minimum = 5
	maximum = 3615
Slowest flit = 100200
Fragmentation average = 37.0212
	minimum = 0
	maximum = 337
Injected packet rate average = 0.021763
	minimum = 0.001 (at node 120)
	maximum = 0.045 (at node 92)
Accepted packet rate average = 0.0151328
	minimum = 0.0095 (at node 132)
	maximum = 0.023 (at node 6)
Injected flit rate average = 0.391966
	minimum = 0.018 (at node 120)
	maximum = 0.8145 (at node 188)
Accepted flit rate average= 0.273602
	minimum = 0.1855 (at node 2)
	maximum = 0.408 (at node 6)
Injected packet length average = 18.0106
Accepted packet length average = 18.08
Total in-flight flits = 122369 (113145 measured)
latency change    = 0.633385
throughput change = 0.000885183
Class 0:
Packet latency average = 1252.54
	minimum = 22
	maximum = 3545
Network latency average = 1130.24
	minimum = 22
	maximum = 2940
Slowest packet = 12589
Flit latency average = 1351.36
	minimum = 5
	maximum = 4201
Slowest flit = 106001
Fragmentation average = 51.7272
	minimum = 0
	maximum = 403
Injected packet rate average = 0.0214427
	minimum = 0.00466667 (at node 120)
	maximum = 0.045 (at node 90)
Accepted packet rate average = 0.0151354
	minimum = 0.00966667 (at node 64)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.386125
	minimum = 0.084 (at node 120)
	maximum = 0.807 (at node 90)
Accepted flit rate average= 0.272995
	minimum = 0.171667 (at node 36)
	maximum = 0.391 (at node 123)
Injected packet length average = 18.0073
Accepted packet length average = 18.0368
Total in-flight flits = 142043 (140223 measured)
latency change    = 0.357348
throughput change = 0.00222265
Class 0:
Packet latency average = 1557.73
	minimum = 22
	maximum = 4528
Network latency average = 1431.71
	minimum = 22
	maximum = 3971
Slowest packet = 12589
Flit latency average = 1493.29
	minimum = 5
	maximum = 4801
Slowest flit = 164913
Fragmentation average = 56.2568
	minimum = 0
	maximum = 593
Injected packet rate average = 0.0215924
	minimum = 0.0085 (at node 76)
	maximum = 0.04175 (at node 63)
Accepted packet rate average = 0.0151771
	minimum = 0.0105 (at node 143)
	maximum = 0.0205 (at node 178)
Injected flit rate average = 0.388753
	minimum = 0.14925 (at node 76)
	maximum = 0.748 (at node 63)
Accepted flit rate average= 0.27348
	minimum = 0.18725 (at node 36)
	maximum = 0.3715 (at node 178)
Injected packet length average = 18.0041
Accepted packet length average = 18.0193
Total in-flight flits = 165539 (165041 measured)
latency change    = 0.195915
throughput change = 0.00177591
Class 0:
Packet latency average = 1781.2
	minimum = 22
	maximum = 4956
Network latency average = 1651.16
	minimum = 22
	maximum = 4712
Slowest packet = 12589
Flit latency average = 1631.99
	minimum = 5
	maximum = 5676
Slowest flit = 174692
Fragmentation average = 59.024
	minimum = 0
	maximum = 593
Injected packet rate average = 0.021449
	minimum = 0.0102 (at node 91)
	maximum = 0.038 (at node 101)
Accepted packet rate average = 0.0152125
	minimum = 0.0116 (at node 31)
	maximum = 0.0194 (at node 95)
Injected flit rate average = 0.386075
	minimum = 0.1814 (at node 162)
	maximum = 0.6822 (at node 101)
Accepted flit rate average= 0.274008
	minimum = 0.2074 (at node 143)
	maximum = 0.3492 (at node 95)
Injected packet length average = 17.9997
Accepted packet length average = 18.0121
Total in-flight flits = 184650 (184524 measured)
latency change    = 0.125463
throughput change = 0.00192645
Class 0:
Packet latency average = 1971.8
	minimum = 22
	maximum = 6206
Network latency average = 1837.65
	minimum = 22
	maximum = 5959
Slowest packet = 12614
Flit latency average = 1770.36
	minimum = 5
	maximum = 6526
Slowest flit = 178991
Fragmentation average = 58.542
	minimum = 0
	maximum = 593
Injected packet rate average = 0.0217769
	minimum = 0.00966667 (at node 162)
	maximum = 0.0375 (at node 101)
Accepted packet rate average = 0.015263
	minimum = 0.0118333 (at node 104)
	maximum = 0.0193333 (at node 123)
Injected flit rate average = 0.391908
	minimum = 0.175 (at node 162)
	maximum = 0.673 (at node 101)
Accepted flit rate average= 0.274749
	minimum = 0.217667 (at node 104)
	maximum = 0.342 (at node 68)
Injected packet length average = 17.9965
Accepted packet length average = 18.001
Total in-flight flits = 212169 (212133 measured)
latency change    = 0.0966651
throughput change = 0.00269627
Class 0:
Packet latency average = 2149.49
	minimum = 22
	maximum = 6988
Network latency average = 2011.4
	minimum = 22
	maximum = 6821
Slowest packet = 13287
Flit latency average = 1911.99
	minimum = 5
	maximum = 6804
Slowest flit = 239183
Fragmentation average = 58.5566
	minimum = 0
	maximum = 593
Injected packet rate average = 0.021756
	minimum = 0.00985714 (at node 121)
	maximum = 0.0368571 (at node 101)
Accepted packet rate average = 0.0152626
	minimum = 0.0121429 (at node 67)
	maximum = 0.0187143 (at node 68)
Injected flit rate average = 0.391552
	minimum = 0.177857 (at node 121)
	maximum = 0.663429 (at node 101)
Accepted flit rate average= 0.274882
	minimum = 0.220143 (at node 67)
	maximum = 0.343 (at node 68)
Injected packet length average = 17.9975
Accepted packet length average = 18.0101
Total in-flight flits = 234046 (234046 measured)
latency change    = 0.0826652
throughput change = 0.000484966
Draining all recorded packets ...
Class 0:
Remaining flits: 304282 304283 304284 304285 304286 304287 304288 304289 309420 309421 [...] (252229 flits)
Measured flits: 304282 304283 304284 304285 304286 304287 304288 304289 309420 309421 [...] (201570 flits)
Class 0:
Remaining flits: 309420 309421 309422 309423 309424 309425 309426 309427 309428 309429 [...] (266891 flits)
Measured flits: 309420 309421 309422 309423 309424 309425 309426 309427 309428 309429 [...] (158091 flits)
Class 0:
Remaining flits: 339858 339859 339860 339861 339862 339863 339864 339865 339866 339867 [...] (282444 flits)
Measured flits: 339858 339859 339860 339861 339862 339863 339864 339865 339866 339867 [...] (115439 flits)
Class 0:
Remaining flits: 349677 349678 349679 349680 349681 349682 349683 349684 349685 385398 [...] (297486 flits)
Measured flits: 349677 349678 349679 349680 349681 349682 349683 349684 349685 385398 [...] (79692 flits)
Class 0:
Remaining flits: 396000 396001 396002 396003 396004 396005 396006 396007 396008 396009 [...] (310403 flits)
Measured flits: 396000 396001 396002 396003 396004 396005 396006 396007 396008 396009 [...] (52570 flits)
Class 0:
Remaining flits: 430308 430309 430310 430311 430312 430313 430314 430315 430316 430317 [...] (326534 flits)
Measured flits: 430308 430309 430310 430311 430312 430313 430314 430315 430316 430317 [...] (34085 flits)
Class 0:
Remaining flits: 451944 451945 451946 451947 451948 451949 451950 451951 451952 451953 [...] (336719 flits)
Measured flits: 451944 451945 451946 451947 451948 451949 451950 451951 451952 451953 [...] (21334 flits)
Class 0:
Remaining flits: 471528 471529 471530 471531 471532 471533 471534 471535 471536 471537 [...] (341866 flits)
Measured flits: 471528 471529 471530 471531 471532 471533 471534 471535 471536 471537 [...] (11915 flits)
Class 0:
Remaining flits: 471540 471541 471542 471543 471544 471545 514458 514459 514460 514461 [...] (351654 flits)
Measured flits: 471540 471541 471542 471543 471544 471545 514458 514459 514460 514461 [...] (6739 flits)
Class 0:
Remaining flits: 584784 584785 584786 584787 584788 584789 584790 584791 584792 584793 [...] (362619 flits)
Measured flits: 584784 584785 584786 584787 584788 584789 584790 584791 584792 584793 [...] (3399 flits)
Class 0:
Remaining flits: 588402 588403 588404 588405 588406 588407 588408 588409 588410 588411 [...] (369927 flits)
Measured flits: 588402 588403 588404 588405 588406 588407 588408 588409 588410 588411 [...] (1798 flits)
Class 0:
Remaining flits: 588402 588403 588404 588405 588406 588407 588408 588409 588410 588411 [...] (374702 flits)
Measured flits: 588402 588403 588404 588405 588406 588407 588408 588409 588410 588411 [...] (927 flits)
Class 0:
Remaining flits: 638175 638176 638177 638178 638179 638180 638181 638182 638183 638184 [...] (381213 flits)
Measured flits: 638175 638176 638177 638178 638179 638180 638181 638182 638183 638184 [...] (339 flits)
Class 0:
Remaining flits: 729522 729523 729524 729525 729526 729527 729528 729529 729530 729531 [...] (383552 flits)
Measured flits: 729522 729523 729524 729525 729526 729527 729528 729529 729530 729531 [...] (90 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 829154 829155 829156 829157 829158 829159 829160 829161 829162 829163 [...] (339433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903816 903817 903818 903819 903820 903821 903822 903823 903824 903825 [...] (289804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1007928 1007929 1007930 1007931 1007932 1007933 1007934 1007935 1007936 1007937 [...] (240237 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1019124 1019125 1019126 1019127 1019128 1019129 1019130 1019131 1019132 1019133 [...] (191656 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1052244 1052245 1052246 1052247 1052248 1052249 1052250 1052251 1052252 1052253 [...] (143934 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1115481 1115482 1115483 1115484 1115485 1115486 1115487 1115488 1115489 1115490 [...] (96900 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1157130 1157131 1157132 1157133 1157134 1157135 1157136 1157137 1157138 1157139 [...] (51228 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1269324 1269325 1269326 1269327 1269328 1269329 1269330 1269331 1269332 1269333 [...] (13715 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1548997 1548998 1548999 1549000 1549001 1549002 1549003 1549004 1549005 1549006 [...] (265 flits)
Measured flits: (0 flits)
Time taken is 34086 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3531.17 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15397 (1 samples)
Network latency average = 3309.25 (1 samples)
	minimum = 22 (1 samples)
	maximum = 14964 (1 samples)
Flit latency average = 4930.98 (1 samples)
	minimum = 5 (1 samples)
	maximum = 16282 (1 samples)
Fragmentation average = 58.1766 (1 samples)
	minimum = 0 (1 samples)
	maximum = 663 (1 samples)
Injected packet rate average = 0.021756 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0368571 (1 samples)
Accepted packet rate average = 0.0152626 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.391552 (1 samples)
	minimum = 0.177857 (1 samples)
	maximum = 0.663429 (1 samples)
Accepted flit rate average = 0.274882 (1 samples)
	minimum = 0.220143 (1 samples)
	maximum = 0.343 (1 samples)
Injected packet size average = 17.9975 (1 samples)
Accepted packet size average = 18.0101 (1 samples)
Hops average = 5.07708 (1 samples)
Total run time 40.2884
