BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 275.643
	minimum = 27
	maximum = 972
Network latency average = 225.685
	minimum = 23
	maximum = 884
Slowest packet = 73
Flit latency average = 185.64
	minimum = 6
	maximum = 867
Slowest flit = 7847
Fragmentation average = 53.5348
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0143854
	minimum = 0 (at node 18)
	maximum = 0.041 (at node 87)
Accepted packet rate average = 0.00867708
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 49)
Injected flit rate average = 0.256479
	minimum = 0 (at node 18)
	maximum = 0.738 (at node 87)
Accepted flit rate average= 0.163266
	minimum = 0.036 (at node 174)
	maximum = 0.288 (at node 49)
Injected packet length average = 17.8291
Accepted packet length average = 18.8157
Total in-flight flits = 18387 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 424.424
	minimum = 27
	maximum = 1857
Network latency average = 363.749
	minimum = 23
	maximum = 1626
Slowest packet = 877
Flit latency average = 320.77
	minimum = 6
	maximum = 1702
Slowest flit = 16879
Fragmentation average = 61.6764
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0137578
	minimum = 0.001 (at node 112)
	maximum = 0.03 (at node 87)
Accepted packet rate average = 0.00904427
	minimum = 0.0045 (at node 174)
	maximum = 0.014 (at node 137)
Injected flit rate average = 0.246411
	minimum = 0.018 (at node 112)
	maximum = 0.54 (at node 102)
Accepted flit rate average= 0.166234
	minimum = 0.081 (at node 174)
	maximum = 0.252 (at node 137)
Injected packet length average = 17.9107
Accepted packet length average = 18.3801
Total in-flight flits = 31260 (0 measured)
latency change    = 0.350549
throughput change = 0.0178588
Class 0:
Packet latency average = 813.498
	minimum = 23
	maximum = 2416
Network latency average = 729.461
	minimum = 23
	maximum = 2227
Slowest packet = 886
Flit latency average = 686.279
	minimum = 6
	maximum = 2233
Slowest flit = 38453
Fragmentation average = 65.9206
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0137135
	minimum = 0 (at node 25)
	maximum = 0.039 (at node 161)
Accepted packet rate average = 0.00977083
	minimum = 0.002 (at node 189)
	maximum = 0.015 (at node 63)
Injected flit rate average = 0.246885
	minimum = 0 (at node 25)
	maximum = 0.699 (at node 161)
Accepted flit rate average= 0.176953
	minimum = 0.034 (at node 189)
	maximum = 0.296 (at node 106)
Injected packet length average = 18.003
Accepted packet length average = 18.1103
Total in-flight flits = 44841 (0 measured)
latency change    = 0.478273
throughput change = 0.060574
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 395.044
	minimum = 23
	maximum = 1867
Network latency average = 307.329
	minimum = 23
	maximum = 974
Slowest packet = 7956
Flit latency average = 956.435
	minimum = 6
	maximum = 3419
Slowest flit = 29272
Fragmentation average = 52.8848
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0125417
	minimum = 0 (at node 8)
	maximum = 0.035 (at node 54)
Accepted packet rate average = 0.00938021
	minimum = 0.003 (at node 36)
	maximum = 0.019 (at node 78)
Injected flit rate average = 0.225781
	minimum = 0 (at node 8)
	maximum = 0.629 (at node 54)
Accepted flit rate average= 0.167755
	minimum = 0.04 (at node 77)
	maximum = 0.334 (at node 78)
Injected packet length average = 18.0025
Accepted packet length average = 17.884
Total in-flight flits = 56084 (35011 measured)
latency change    = 1.05926
throughput change = 0.0548294
Class 0:
Packet latency average = 725.386
	minimum = 23
	maximum = 2938
Network latency average = 625.102
	minimum = 23
	maximum = 1979
Slowest packet = 7956
Flit latency average = 1098.29
	minimum = 6
	maximum = 3882
Slowest flit = 42569
Fragmentation average = 61.4729
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0121276
	minimum = 0.0015 (at node 36)
	maximum = 0.031 (at node 173)
Accepted packet rate average = 0.0093099
	minimum = 0.004 (at node 36)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.218664
	minimum = 0.027 (at node 36)
	maximum = 0.5545 (at node 173)
Accepted flit rate average= 0.16688
	minimum = 0.0745 (at node 36)
	maximum = 0.3195 (at node 16)
Injected packet length average = 18.0303
Accepted packet length average = 17.925
Total in-flight flits = 64873 (56616 measured)
latency change    = 0.455402
throughput change = 0.00524328
Class 0:
Packet latency average = 1024.54
	minimum = 23
	maximum = 3988
Network latency average = 919.856
	minimum = 23
	maximum = 2918
Slowest packet = 7956
Flit latency average = 1210.63
	minimum = 6
	maximum = 4107
Slowest flit = 80117
Fragmentation average = 65.7702
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0117743
	minimum = 0.001 (at node 36)
	maximum = 0.0273333 (at node 173)
Accepted packet rate average = 0.00908507
	minimum = 0.005 (at node 4)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.212063
	minimum = 0.018 (at node 36)
	maximum = 0.491 (at node 173)
Accepted flit rate average= 0.163523
	minimum = 0.0906667 (at node 4)
	maximum = 0.280667 (at node 16)
Injected packet length average = 18.0106
Accepted packet length average = 17.999
Total in-flight flits = 73052 (69226 measured)
latency change    = 0.291991
throughput change = 0.0205332
Class 0:
Packet latency average = 1311.36
	minimum = 23
	maximum = 5273
Network latency average = 1179.74
	minimum = 23
	maximum = 3858
Slowest packet = 7956
Flit latency average = 1349.01
	minimum = 6
	maximum = 5140
Slowest flit = 85841
Fragmentation average = 67.716
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0115299
	minimum = 0.00375 (at node 53)
	maximum = 0.02175 (at node 173)
Accepted packet rate average = 0.00908854
	minimum = 0.00475 (at node 4)
	maximum = 0.01375 (at node 16)
Injected flit rate average = 0.207665
	minimum = 0.0675 (at node 53)
	maximum = 0.388 (at node 173)
Accepted flit rate average= 0.163646
	minimum = 0.0895 (at node 4)
	maximum = 0.24825 (at node 16)
Injected packet length average = 18.011
Accepted packet length average = 18.0057
Total in-flight flits = 79235 (77599 measured)
latency change    = 0.218717
throughput change = 0.000753236
Draining remaining packets ...
Class 0:
Remaining flits: 98658 98659 98660 98661 98662 98663 98664 98665 98666 98667 [...] (50715 flits)
Measured flits: 142920 142921 142922 142923 142924 142925 142926 142927 142928 142929 [...] (50005 flits)
Class 0:
Remaining flits: 120402 120403 120404 120405 120406 120407 120408 120409 120410 120411 [...] (22770 flits)
Measured flits: 147132 147133 147134 147135 147136 147137 147138 147139 147140 147141 [...] (22644 flits)
Class 0:
Remaining flits: 137988 137989 137990 137991 137992 137993 137994 137995 137996 137997 [...] (3663 flits)
Measured flits: 180936 180937 180938 180939 180940 180941 180942 180943 180944 180945 [...] (3645 flits)
Time taken is 10911 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2198.39 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8653 (1 samples)
Network latency average = 1999.58 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6511 (1 samples)
Flit latency average = 1930.69 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7660 (1 samples)
Fragmentation average = 67.7011 (1 samples)
	minimum = 0 (1 samples)
	maximum = 172 (1 samples)
Injected packet rate average = 0.0115299 (1 samples)
	minimum = 0.00375 (1 samples)
	maximum = 0.02175 (1 samples)
Accepted packet rate average = 0.00908854 (1 samples)
	minimum = 0.00475 (1 samples)
	maximum = 0.01375 (1 samples)
Injected flit rate average = 0.207665 (1 samples)
	minimum = 0.0675 (1 samples)
	maximum = 0.388 (1 samples)
Accepted flit rate average = 0.163646 (1 samples)
	minimum = 0.0895 (1 samples)
	maximum = 0.24825 (1 samples)
Injected packet size average = 18.011 (1 samples)
Accepted packet size average = 18.0057 (1 samples)
Hops average = 5.10896 (1 samples)
Total run time 8.14538
