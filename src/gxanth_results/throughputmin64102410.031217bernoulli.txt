BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.134
	minimum = 23
	maximum = 947
Network latency average = 306.611
	minimum = 23
	maximum = 947
Slowest packet = 49
Flit latency average = 224.818
	minimum = 6
	maximum = 946
Slowest flit = 3800
Fragmentation average = 186.598
	minimum = 0
	maximum = 908
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0105052
	minimum = 0.004 (at node 174)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.223417
	minimum = 0.113 (at node 120)
	maximum = 0.368 (at node 152)
Injected packet length average = 17.8483
Accepted packet length average = 21.2672
Total in-flight flits = 63556 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 550.352
	minimum = 23
	maximum = 1883
Network latency average = 540.524
	minimum = 23
	maximum = 1860
Slowest packet = 619
Flit latency average = 446.363
	minimum = 6
	maximum = 1891
Slowest flit = 7122
Fragmentation average = 243.791
	minimum = 0
	maximum = 1571
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0117865
	minimum = 0.007 (at node 4)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.234534
	minimum = 0.1335 (at node 79)
	maximum = 0.367 (at node 70)
Injected packet length average = 17.9239
Accepted packet length average = 19.8986
Total in-flight flits = 124517 (0 measured)
latency change    = 0.427394
throughput change = 0.0474012
Class 0:
Packet latency average = 1159.72
	minimum = 25
	maximum = 2867
Network latency average = 1149.05
	minimum = 24
	maximum = 2844
Slowest packet = 492
Flit latency average = 1046.73
	minimum = 6
	maximum = 2865
Slowest flit = 11595
Fragmentation average = 347.09
	minimum = 0
	maximum = 2342
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0135365
	minimum = 0.006 (at node 26)
	maximum = 0.022 (at node 161)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.250193
	minimum = 0.117 (at node 184)
	maximum = 0.422 (at node 177)
Injected packet length average = 18.0012
Accepted packet length average = 18.4829
Total in-flight flits = 183400 (0 measured)
latency change    = 0.525443
throughput change = 0.0625872
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 123.701
	minimum = 23
	maximum = 977
Network latency average = 113.605
	minimum = 23
	maximum = 960
Slowest packet = 18084
Flit latency average = 1492.95
	minimum = 6
	maximum = 3817
Slowest flit = 16586
Fragmentation average = 48.4549
	minimum = 0
	maximum = 669
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0138177
	minimum = 0.004 (at node 110)
	maximum = 0.023 (at node 183)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.248818
	minimum = 0.076 (at node 110)
	maximum = 0.406 (at node 183)
Injected packet length average = 17.9869
Accepted packet length average = 18.0072
Total in-flight flits = 245607 (99897 measured)
latency change    = 8.37519
throughput change = 0.00552613
Class 0:
Packet latency average = 351.367
	minimum = 23
	maximum = 1986
Network latency average = 341.392
	minimum = 23
	maximum = 1969
Slowest packet = 17881
Flit latency average = 1742.68
	minimum = 6
	maximum = 4745
Slowest flit = 20519
Fragmentation average = 104.304
	minimum = 0
	maximum = 859
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.0137578
	minimum = 0.0045 (at node 115)
	maximum = 0.02 (at node 95)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.249581
	minimum = 0.108 (at node 115)
	maximum = 0.3665 (at node 183)
Injected packet length average = 17.9923
Accepted packet length average = 18.141
Total in-flight flits = 304209 (191219 measured)
latency change    = 0.647945
throughput change = 0.00305721
Class 0:
Packet latency average = 671.412
	minimum = 23
	maximum = 2970
Network latency average = 661.141
	minimum = 23
	maximum = 2937
Slowest packet = 17918
Flit latency average = 2001.66
	minimum = 6
	maximum = 5731
Slowest flit = 26684
Fragmentation average = 151.755
	minimum = 0
	maximum = 1697
Injected packet rate average = 0.0313576
	minimum = 0.024 (at node 15)
	maximum = 0.0396667 (at node 67)
Accepted packet rate average = 0.0137986
	minimum = 0.009 (at node 92)
	maximum = 0.0183333 (at node 151)
Injected flit rate average = 0.564382
	minimum = 0.435667 (at node 15)
	maximum = 0.715667 (at node 67)
Accepted flit rate average= 0.248915
	minimum = 0.152 (at node 92)
	maximum = 0.338 (at node 103)
Injected packet length average = 17.9982
Accepted packet length average = 18.0391
Total in-flight flits = 365141 (280379 measured)
latency change    = 0.476675
throughput change = 0.0026748
Draining remaining packets ...
Class 0:
Remaining flits: 12468 12469 12470 12471 12472 12473 15408 15409 15410 15411 [...] (326211 flits)
Measured flits: 321498 321499 321500 321501 321502 321503 321504 321505 321506 321507 [...] (264740 flits)
Class 0:
Remaining flits: 12468 12469 12470 12471 12472 12473 15408 15409 15410 15411 [...] (288424 flits)
Measured flits: 321510 321511 321512 321513 321514 321515 321516 321517 321518 321519 [...] (244361 flits)
Class 0:
Remaining flits: 12468 12469 12470 12471 12472 12473 15408 15409 15410 15411 [...] (251177 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (219526 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (214240 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (189907 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (177972 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (158632 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (142707 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (127358 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (108137 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (96859 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (74657 flits)
Measured flits: 321516 321517 321518 321519 321520 321521 321522 321523 321524 321525 [...] (66821 flits)
Class 0:
Remaining flits: 37026 37027 37028 37029 37030 37031 37032 37033 37034 37035 [...] (44024 flits)
Measured flits: 321606 321607 321608 321609 321610 321611 321612 321613 321614 321615 [...] (39443 flits)
Class 0:
Remaining flits: 67392 67393 67394 67395 67396 67397 67398 67399 67400 67401 [...] (18437 flits)
Measured flits: 321822 321823 321824 321825 321826 321827 321828 321829 321830 321831 [...] (16704 flits)
Class 0:
Remaining flits: 169402 169403 169404 169405 169406 169407 169408 169409 169410 169411 [...] (4036 flits)
Measured flits: 323266 323267 323268 323269 323270 323271 323272 323273 323274 323275 [...] (3795 flits)
Class 0:
Remaining flits: 396570 396571 396572 396573 396574 396575 501858 501859 501860 501861 [...] (115 flits)
Measured flits: 396570 396571 396572 396573 396574 396575 501858 501859 501860 501861 [...] (115 flits)
Time taken is 18121 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6312.21 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14803 (1 samples)
Network latency average = 6301.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14803 (1 samples)
Flit latency average = 5579.49 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15721 (1 samples)
Fragmentation average = 290.408 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7967 (1 samples)
Injected packet rate average = 0.0313576 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.0396667 (1 samples)
Accepted packet rate average = 0.0137986 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.564382 (1 samples)
	minimum = 0.435667 (1 samples)
	maximum = 0.715667 (1 samples)
Accepted flit rate average = 0.248915 (1 samples)
	minimum = 0.152 (1 samples)
	maximum = 0.338 (1 samples)
Injected packet size average = 17.9982 (1 samples)
Accepted packet size average = 18.0391 (1 samples)
Hops average = 5.07231 (1 samples)
Total run time 24.9574
