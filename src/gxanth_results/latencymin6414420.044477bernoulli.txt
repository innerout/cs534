BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 308.162
	minimum = 22
	maximum = 834
Network latency average = 290.924
	minimum = 22
	maximum = 806
Slowest packet = 687
Flit latency average = 261.31
	minimum = 5
	maximum = 803
Slowest flit = 22642
Fragmentation average = 74.9407
	minimum = 0
	maximum = 614
Injected packet rate average = 0.042901
	minimum = 0.03 (at node 51)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0145885
	minimum = 0.006 (at node 115)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.76549
	minimum = 0.531 (at node 145)
	maximum = 0.992 (at node 55)
Accepted flit rate average= 0.288005
	minimum = 0.118 (at node 115)
	maximum = 0.466 (at node 44)
Injected packet length average = 17.8431
Accepted packet length average = 19.7419
Total in-flight flits = 92969 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 580.402
	minimum = 22
	maximum = 1721
Network latency average = 557.727
	minimum = 22
	maximum = 1653
Slowest packet = 2010
Flit latency average = 515.647
	minimum = 5
	maximum = 1705
Slowest flit = 39403
Fragmentation average = 129.586
	minimum = 0
	maximum = 817
Injected packet rate average = 0.0437917
	minimum = 0.029 (at node 51)
	maximum = 0.055 (at node 74)
Accepted packet rate average = 0.0155078
	minimum = 0.009 (at node 153)
	maximum = 0.022 (at node 63)
Injected flit rate average = 0.784956
	minimum = 0.5145 (at node 51)
	maximum = 0.984 (at node 74)
Accepted flit rate average= 0.297143
	minimum = 0.1825 (at node 153)
	maximum = 0.4155 (at node 152)
Injected packet length average = 17.9248
Accepted packet length average = 19.1609
Total in-flight flits = 188585 (0 measured)
latency change    = 0.469054
throughput change = 0.0307529
Class 0:
Packet latency average = 1347.4
	minimum = 22
	maximum = 2552
Network latency average = 1316.53
	minimum = 22
	maximum = 2475
Slowest packet = 3032
Flit latency average = 1268.89
	minimum = 5
	maximum = 2584
Slowest flit = 56064
Fragmentation average = 236.826
	minimum = 0
	maximum = 919
Injected packet rate average = 0.0431458
	minimum = 0.026 (at node 70)
	maximum = 0.056 (at node 93)
Accepted packet rate average = 0.0167292
	minimum = 0.007 (at node 132)
	maximum = 0.029 (at node 33)
Injected flit rate average = 0.776417
	minimum = 0.455 (at node 70)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.308818
	minimum = 0.139 (at node 132)
	maximum = 0.466 (at node 33)
Injected packet length average = 17.9952
Accepted packet length average = 18.4598
Total in-flight flits = 278404 (0 measured)
latency change    = 0.569243
throughput change = 0.0378038
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 113.291
	minimum = 23
	maximum = 1027
Network latency average = 41.2272
	minimum = 22
	maximum = 574
Slowest packet = 25121
Flit latency average = 1803.48
	minimum = 5
	maximum = 3464
Slowest flit = 75519
Fragmentation average = 6.7898
	minimum = 0
	maximum = 36
Injected packet rate average = 0.041651
	minimum = 0.02 (at node 12)
	maximum = 0.056 (at node 46)
Accepted packet rate average = 0.0165781
	minimum = 0.007 (at node 124)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.749323
	minimum = 0.355 (at node 12)
	maximum = 1 (at node 46)
Accepted flit rate average= 0.306943
	minimum = 0.127 (at node 124)
	maximum = 0.528 (at node 0)
Injected packet length average = 17.9905
Accepted packet length average = 18.5149
Total in-flight flits = 363417 (132133 measured)
latency change    = 10.8933
throughput change = 0.00610863
Class 0:
Packet latency average = 145.883
	minimum = 23
	maximum = 1317
Network latency average = 46.3014
	minimum = 22
	maximum = 622
Slowest packet = 25121
Flit latency average = 2076.67
	minimum = 5
	maximum = 4295
Slowest flit = 97089
Fragmentation average = 7.16139
	minimum = 0
	maximum = 41
Injected packet rate average = 0.041138
	minimum = 0.023 (at node 12)
	maximum = 0.0545 (at node 75)
Accepted packet rate average = 0.016526
	minimum = 0.009 (at node 36)
	maximum = 0.0255 (at node 16)
Injected flit rate average = 0.740651
	minimum = 0.414 (at node 12)
	maximum = 0.9835 (at node 75)
Accepted flit rate average= 0.304477
	minimum = 0.164 (at node 36)
	maximum = 0.433 (at node 129)
Injected packet length average = 18.0041
Accepted packet length average = 18.424
Total in-flight flits = 445831 (261491 measured)
latency change    = 0.223414
throughput change = 0.00809962
Class 0:
Packet latency average = 192.47
	minimum = 23
	maximum = 1802
Network latency average = 59.9807
	minimum = 22
	maximum = 1103
Slowest packet = 25121
Flit latency average = 2353.2
	minimum = 5
	maximum = 5153
Slowest flit = 122290
Fragmentation average = 7.17517
	minimum = 0
	maximum = 43
Injected packet rate average = 0.0408437
	minimum = 0.025 (at node 12)
	maximum = 0.0546667 (at node 75)
Accepted packet rate average = 0.0165347
	minimum = 0.0103333 (at node 36)
	maximum = 0.0226667 (at node 172)
Injected flit rate average = 0.735059
	minimum = 0.446333 (at node 12)
	maximum = 0.989 (at node 75)
Accepted flit rate average= 0.303094
	minimum = 0.202333 (at node 36)
	maximum = 0.412333 (at node 128)
Injected packet length average = 17.9969
Accepted packet length average = 18.3307
Total in-flight flits = 527308 (389842 measured)
latency change    = 0.242047
throughput change = 0.00456233
Class 0:
Packet latency average = 336.393
	minimum = 23
	maximum = 4241
Network latency average = 181.973
	minimum = 22
	maximum = 3953
Slowest packet = 25119
Flit latency average = 2644.96
	minimum = 5
	maximum = 5946
Slowest flit = 145187
Fragmentation average = 17.4663
	minimum = 0
	maximum = 604
Injected packet rate average = 0.0405182
	minimum = 0.02175 (at node 188)
	maximum = 0.0525 (at node 147)
Accepted packet rate average = 0.0164727
	minimum = 0.012 (at node 31)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.729245
	minimum = 0.39175 (at node 188)
	maximum = 0.94675 (at node 171)
Accepted flit rate average= 0.301182
	minimum = 0.228 (at node 67)
	maximum = 0.39575 (at node 128)
Injected packet length average = 17.9979
Accepted packet length average = 18.2838
Total in-flight flits = 607238 (514091 measured)
latency change    = 0.427843
throughput change = 0.00634652
Class 0:
Packet latency average = 933.598
	minimum = 23
	maximum = 5214
Network latency average = 759.032
	minimum = 22
	maximum = 4976
Slowest packet = 25119
Flit latency average = 2969.44
	minimum = 5
	maximum = 6716
Slowest flit = 187032
Fragmentation average = 70.4156
	minimum = 0
	maximum = 723
Injected packet rate average = 0.0400833
	minimum = 0.022 (at node 152)
	maximum = 0.0522 (at node 11)
Accepted packet rate average = 0.0163062
	minimum = 0.0124 (at node 52)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.721616
	minimum = 0.3946 (at node 152)
	maximum = 0.9402 (at node 11)
Accepted flit rate average= 0.297311
	minimum = 0.2288 (at node 139)
	maximum = 0.3804 (at node 128)
Injected packet length average = 18.0029
Accepted packet length average = 18.233
Total in-flight flits = 685733 (629925 measured)
latency change    = 0.639681
throughput change = 0.0130195
Class 0:
Packet latency average = 1802.39
	minimum = 23
	maximum = 6403
Network latency average = 1621.8
	minimum = 22
	maximum = 5992
Slowest packet = 25119
Flit latency average = 3310.56
	minimum = 5
	maximum = 7452
Slowest flit = 223667
Fragmentation average = 137.019
	minimum = 0
	maximum = 800
Injected packet rate average = 0.0391224
	minimum = 0.0198333 (at node 148)
	maximum = 0.0495 (at node 133)
Accepted packet rate average = 0.0161085
	minimum = 0.0121667 (at node 4)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.704197
	minimum = 0.358167 (at node 148)
	maximum = 0.888333 (at node 133)
Accepted flit rate average= 0.293298
	minimum = 0.224167 (at node 4)
	maximum = 0.372833 (at node 118)
Injected packet length average = 17.9998
Accepted packet length average = 18.2076
Total in-flight flits = 751893 (724649 measured)
latency change    = 0.482024
throughput change = 0.0136848
Class 0:
Packet latency average = 2839.77
	minimum = 23
	maximum = 7197
Network latency average = 2642.03
	minimum = 22
	maximum = 6963
Slowest packet = 25119
Flit latency average = 3643.84
	minimum = 5
	maximum = 8118
Slowest flit = 276945
Fragmentation average = 205.183
	minimum = 0
	maximum = 873
Injected packet rate average = 0.0374375
	minimum = 0.0185714 (at node 148)
	maximum = 0.0467143 (at node 70)
Accepted packet rate average = 0.0160007
	minimum = 0.0122857 (at node 67)
	maximum = 0.0197143 (at node 138)
Injected flit rate average = 0.67391
	minimum = 0.332286 (at node 152)
	maximum = 0.842429 (at node 70)
Accepted flit rate average= 0.290726
	minimum = 0.223714 (at node 67)
	maximum = 0.361286 (at node 138)
Injected packet length average = 18.0009
Accepted packet length average = 18.1695
Total in-flight flits = 793608 (784637 measured)
latency change    = 0.365304
throughput change = 0.00884527
Draining all recorded packets ...
Class 0:
Remaining flits: 366709 366710 366711 366712 366713 374400 374401 374402 374403 374404 [...] (810005 flits)
Measured flits: 451926 451927 451928 451929 451930 451931 451932 451933 451934 451935 [...] (780518 flits)
Class 0:
Remaining flits: 420460 420461 423105 423106 423107 426294 426295 426296 426297 426298 [...] (815578 flits)
Measured flits: 452218 452219 452220 452221 452222 452223 452224 452225 452226 452227 [...] (755374 flits)
Class 0:
Remaining flits: 465204 465205 465206 465207 465208 465209 473219 474634 474635 474636 [...] (811571 flits)
Measured flits: 465204 465205 465206 465207 465208 465209 473219 474634 474635 474636 [...] (720054 flits)
Class 0:
Remaining flits: 477267 477268 477269 501588 501589 501590 501591 501592 501593 501594 [...] (810266 flits)
Measured flits: 477267 477268 477269 501588 501589 501590 501591 501592 501593 501594 [...] (682016 flits)
Class 0:
Remaining flits: 501602 501603 501604 501605 504162 504163 504164 504165 504166 504167 [...] (820185 flits)
Measured flits: 501602 501603 501604 501605 504162 504163 504164 504165 504166 504167 [...] (645927 flits)
Class 0:
Remaining flits: 504168 504169 504170 504171 504172 504173 504174 504175 504176 504177 [...] (825117 flits)
Measured flits: 504168 504169 504170 504171 504172 504173 504174 504175 504176 504177 [...] (609299 flits)
Class 0:
Remaining flits: 535330 535331 535332 535333 535334 535335 535336 535337 564678 564679 [...] (827354 flits)
Measured flits: 535330 535331 535332 535333 535334 535335 535336 535337 564678 564679 [...] (572089 flits)
Class 0:
Remaining flits: 578826 578827 578828 578829 578830 578831 578832 578833 578834 578835 [...] (827233 flits)
Measured flits: 578826 578827 578828 578829 578830 578831 578832 578833 578834 578835 [...] (533074 flits)
Class 0:
Remaining flits: 578843 583992 583993 583994 583995 583996 583997 583998 583999 584000 [...] (828273 flits)
Measured flits: 578843 583992 583993 583994 583995 583996 583997 583998 583999 584000 [...] (493935 flits)
Class 0:
Remaining flits: 642032 642033 642034 642035 642036 642037 642038 642039 642040 642041 [...] (824705 flits)
Measured flits: 642032 642033 642034 642035 642036 642037 642038 642039 642040 642041 [...] (453577 flits)
Class 0:
Remaining flits: 668142 668143 668144 668145 668146 668147 668148 668149 668150 668151 [...] (824170 flits)
Measured flits: 668142 668143 668144 668145 668146 668147 668148 668149 668150 668151 [...] (411898 flits)
Class 0:
Remaining flits: 668153 668154 668155 668156 668157 668158 668159 722664 722665 722666 [...] (832233 flits)
Measured flits: 668153 668154 668155 668156 668157 668158 668159 722664 722665 722666 [...] (369657 flits)
Class 0:
Remaining flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (836767 flits)
Measured flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (325134 flits)
Class 0:
Remaining flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (839584 flits)
Measured flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (280313 flits)
Class 0:
Remaining flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (840270 flits)
Measured flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (235689 flits)
Class 0:
Remaining flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (838599 flits)
Measured flits: 722664 722665 722666 722667 722668 722669 722670 722671 722672 722673 [...] (193225 flits)
Class 0:
Remaining flits: 740086 740087 794682 794683 794684 794685 794686 794687 794688 794689 [...] (840850 flits)
Measured flits: 740086 740087 794682 794683 794684 794685 794686 794687 794688 794689 [...] (153340 flits)
Class 0:
Remaining flits: 794696 794697 794698 794699 795888 795889 795890 795891 795892 795893 [...] (841158 flits)
Measured flits: 794696 794697 794698 794699 795888 795889 795890 795891 795892 795893 [...] (117217 flits)
Class 0:
Remaining flits: 795888 795889 795890 795891 795892 795893 795894 795895 795896 795897 [...] (844371 flits)
Measured flits: 795888 795889 795890 795891 795892 795893 795894 795895 795896 795897 [...] (85823 flits)
Class 0:
Remaining flits: 895374 895375 895376 895377 895378 895379 895380 895381 895382 895383 [...] (849933 flits)
Measured flits: 895374 895375 895376 895377 895378 895379 895380 895381 895382 895383 [...] (59596 flits)
Class 0:
Remaining flits: 895388 895389 895390 895391 950922 950923 950924 950925 950926 950927 [...] (848163 flits)
Measured flits: 895388 895389 895390 895391 950922 950923 950924 950925 950926 950927 [...] (38903 flits)
Class 0:
Remaining flits: 950934 950935 950936 950937 950938 950939 998802 998803 998804 998805 [...] (849996 flits)
Measured flits: 950934 950935 950936 950937 950938 950939 998802 998803 998804 998805 [...] (24613 flits)
Class 0:
Remaining flits: 998802 998803 998804 998805 998806 998807 998808 998809 998810 998811 [...] (851281 flits)
Measured flits: 998802 998803 998804 998805 998806 998807 998808 998809 998810 998811 [...] (15435 flits)
Class 0:
Remaining flits: 1052910 1052911 1052912 1052913 1052914 1052915 1052916 1052917 1052918 1052919 [...] (851680 flits)
Measured flits: 1052910 1052911 1052912 1052913 1052914 1052915 1052916 1052917 1052918 1052919 [...] (8942 flits)
Class 0:
Remaining flits: 1052910 1052911 1052912 1052913 1052914 1052915 1052916 1052917 1052918 1052919 [...] (852262 flits)
Measured flits: 1052910 1052911 1052912 1052913 1052914 1052915 1052916 1052917 1052918 1052919 [...] (5061 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (853676 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (2749 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (855767 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (1419 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (855792 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (711 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (856742 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (352 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (851844 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (179 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (849612 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (91 flits)
Class 0:
Remaining flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (854247 flits)
Measured flits: 1125792 1125793 1125794 1125795 1125796 1125797 1125798 1125799 1125800 1125801 [...] (54 flits)
Class 0:
Remaining flits: 1485522 1485523 1485524 1485525 1485526 1485527 1485528 1485529 1485530 1485531 [...] (861235 flits)
Measured flits: 1572912 1572913 1572914 1572915 1572916 1572917 1572918 1572919 1572920 1572921 [...] (18 flits)
Class 0:
Remaining flits: 1501920 1501921 1501922 1501923 1501924 1501925 1501926 1501927 1501928 1501929 [...] (860806 flits)
Measured flits: 1572912 1572913 1572914 1572915 1572916 1572917 1572918 1572919 1572920 1572921 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1501920 1501921 1501922 1501923 1501924 1501925 1501926 1501927 1501928 1501929 [...] (811826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1501920 1501921 1501922 1501923 1501924 1501925 1501926 1501927 1501928 1501929 [...] (762081 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1575306 1575307 1575308 1575309 1575310 1575311 1575312 1575313 1575314 1575315 [...] (711996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1575306 1575307 1575308 1575309 1575310 1575311 1575312 1575313 1575314 1575315 [...] (661525 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1721754 1721755 1721756 1721757 1721758 1721759 1721760 1721761 1721762 1721763 [...] (611600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1733508 1733509 1733510 1733511 1733512 1733513 1733514 1733515 1733516 1733517 [...] (562068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1733508 1733509 1733510 1733511 1733512 1733513 1733514 1733515 1733516 1733517 [...] (513154 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1733508 1733509 1733510 1733511 1733512 1733513 1733514 1733515 1733516 1733517 [...] (464793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1733508 1733509 1733510 1733511 1733512 1733513 1733514 1733515 1733516 1733517 [...] (416916 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002086 2002087 2002088 2002089 2002090 2002091 2002092 2002093 2002094 2002095 [...] (369326 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002086 2002087 2002088 2002089 2002090 2002091 2002092 2002093 2002094 2002095 [...] (321495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228256 2228257 2228258 2228259 2228260 2228261 2228262 2228263 2228264 2228265 [...] (273517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2237166 2237167 2237168 2237169 2237170 2237171 2237172 2237173 2237174 2237175 [...] (225653 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305566 2305567 2305568 2305569 2305570 2305571 2305572 2305573 2305574 2305575 [...] (177681 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305566 2305567 2305568 2305569 2305570 2305571 2305572 2305573 2305574 2305575 [...] (129704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2336958 2336959 2336960 2336961 2336962 2336963 2336964 2336965 2336966 2336967 [...] (82079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2336958 2336959 2336960 2336961 2336962 2336963 2336964 2336965 2336966 2336967 [...] (36584 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2336958 2336959 2336960 2336961 2336962 2336963 2336964 2336965 2336966 2336967 [...] (9361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2742622 2742623 2771478 2771479 2771480 2771481 2771482 2771483 2771484 2771485 [...] (1184 flits)
Measured flits: (0 flits)
Time taken is 64047 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12409.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 34710 (1 samples)
Network latency average = 11241.1 (1 samples)
	minimum = 22 (1 samples)
	maximum = 34645 (1 samples)
Flit latency average = 13346.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 37039 (1 samples)
Fragmentation average = 282.62 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1796 (1 samples)
Injected packet rate average = 0.0374375 (1 samples)
	minimum = 0.0185714 (1 samples)
	maximum = 0.0467143 (1 samples)
Accepted packet rate average = 0.0160007 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0197143 (1 samples)
Injected flit rate average = 0.67391 (1 samples)
	minimum = 0.332286 (1 samples)
	maximum = 0.842429 (1 samples)
Accepted flit rate average = 0.290726 (1 samples)
	minimum = 0.223714 (1 samples)
	maximum = 0.361286 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.1695 (1 samples)
Hops average = 5.06779 (1 samples)
Total run time 130.477
