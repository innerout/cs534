BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.974
	minimum = 22
	maximum = 912
Network latency average = 298.437
	minimum = 22
	maximum = 878
Slowest packet = 858
Flit latency average = 268.45
	minimum = 5
	maximum = 861
Slowest flit = 15911
Fragmentation average = 86.7818
	minimum = 0
	maximum = 707
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0147031
	minimum = 0.007 (at node 162)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.295391
	minimum = 0.162 (at node 185)
	maximum = 0.471 (at node 22)
Injected packet length average = 17.8388
Accepted packet length average = 20.0903
Total in-flight flits = 112269 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 618.498
	minimum = 22
	maximum = 1809
Network latency average = 578.463
	minimum = 22
	maximum = 1710
Slowest packet = 858
Flit latency average = 532.161
	minimum = 5
	maximum = 1693
Slowest flit = 43667
Fragmentation average = 148.985
	minimum = 0
	maximum = 877
Injected packet rate average = 0.0504844
	minimum = 0.042 (at node 59)
	maximum = 0.056 (at node 161)
Accepted packet rate average = 0.0159479
	minimum = 0.0095 (at node 30)
	maximum = 0.023 (at node 152)
Injected flit rate average = 0.904885
	minimum = 0.756 (at node 59)
	maximum = 1 (at node 161)
Accepted flit rate average= 0.306643
	minimum = 0.1745 (at node 153)
	maximum = 0.437 (at node 152)
Injected packet length average = 17.9241
Accepted packet length average = 19.2278
Total in-flight flits = 231197 (0 measured)
latency change    = 0.474576
throughput change = 0.0366961
Class 0:
Packet latency average = 1431.71
	minimum = 24
	maximum = 2630
Network latency average = 1356.31
	minimum = 22
	maximum = 2549
Slowest packet = 4061
Flit latency average = 1311.53
	minimum = 5
	maximum = 2551
Slowest flit = 64560
Fragmentation average = 259.08
	minimum = 0
	maximum = 912
Injected packet rate average = 0.0470573
	minimum = 0.02 (at node 132)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0166615
	minimum = 0.007 (at node 10)
	maximum = 0.028 (at node 160)
Injected flit rate average = 0.847375
	minimum = 0.363 (at node 132)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.31238
	minimum = 0.136 (at node 32)
	maximum = 0.542 (at node 160)
Injected packet length average = 18.0073
Accepted packet length average = 18.7487
Total in-flight flits = 333850 (0 measured)
latency change    = 0.568002
throughput change = 0.0183654
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 266.97
	minimum = 24
	maximum = 1549
Network latency average = 55.7784
	minimum = 22
	maximum = 559
Slowest packet = 28432
Flit latency average = 1878.84
	minimum = 5
	maximum = 3475
Slowest flit = 80325
Fragmentation average = 7.5509
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0463281
	minimum = 0.017 (at node 176)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0168125
	minimum = 0.006 (at node 82)
	maximum = 0.03 (at node 178)
Injected flit rate average = 0.833516
	minimum = 0.318 (at node 176)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.310755
	minimum = 0.173 (at node 110)
	maximum = 0.527 (at node 178)
Injected packet length average = 17.9916
Accepted packet length average = 18.4836
Total in-flight flits = 434295 (147926 measured)
latency change    = 4.36283
throughput change = 0.0052292
Class 0:
Packet latency average = 392.112
	minimum = 24
	maximum = 2345
Network latency average = 107.817
	minimum = 22
	maximum = 1064
Slowest packet = 28432
Flit latency average = 2201.05
	minimum = 5
	maximum = 4431
Slowest flit = 89528
Fragmentation average = 7.3748
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0463776
	minimum = 0.0215 (at node 156)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0166797
	minimum = 0.01 (at node 100)
	maximum = 0.0265 (at node 178)
Injected flit rate average = 0.834422
	minimum = 0.384 (at node 156)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.305083
	minimum = 0.191 (at node 180)
	maximum = 0.467 (at node 178)
Injected packet length average = 17.9919
Accepted packet length average = 18.2907
Total in-flight flits = 537260 (297864 measured)
latency change    = 0.319149
throughput change = 0.0185912
Class 0:
Packet latency average = 544.861
	minimum = 24
	maximum = 2430
Network latency average = 191.258
	minimum = 22
	maximum = 1425
Slowest packet = 28432
Flit latency average = 2542.22
	minimum = 5
	maximum = 5266
Slowest flit = 116433
Fragmentation average = 7.1324
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0464462
	minimum = 0.02 (at node 156)
	maximum = 0.0556667 (at node 7)
Accepted packet rate average = 0.016566
	minimum = 0.0113333 (at node 135)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.836075
	minimum = 0.358667 (at node 156)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.302163
	minimum = 0.219 (at node 180)
	maximum = 0.451333 (at node 129)
Injected packet length average = 18.0009
Accepted packet length average = 18.24
Total in-flight flits = 641376 (449262 measured)
latency change    = 0.280345
throughput change = 0.00966411
Draining remaining packets ...
Class 0:
Remaining flits: 166116 166117 166118 166119 166120 166121 172059 172060 172061 172941 [...] (590367 flits)
Measured flits: 511578 511579 511580 511581 511582 511583 511584 511585 511586 511587 [...] (445301 flits)
Class 0:
Remaining flits: 204191 214544 214545 214546 214547 214548 214549 214550 214551 214552 [...] (540618 flits)
Measured flits: 511578 511579 511580 511581 511582 511583 511584 511585 511586 511587 [...] (440768 flits)
Class 0:
Remaining flits: 241521 241522 241523 264775 264776 264777 264778 264779 266243 266244 [...] (492367 flits)
Measured flits: 511578 511579 511580 511581 511582 511583 511584 511585 511586 511587 [...] (432269 flits)
Class 0:
Remaining flits: 321945 321946 321947 326613 326614 326615 326616 326617 326618 326619 [...] (444456 flits)
Measured flits: 511614 511615 511616 511617 511618 511619 511620 511621 511622 511623 [...] (416226 flits)
Class 0:
Remaining flits: 393159 393160 393161 393162 393163 393164 393165 393166 393167 393168 [...] (397002 flits)
Measured flits: 511614 511615 511616 511617 511618 511619 511620 511621 511622 511623 [...] (389826 flits)
Class 0:
Remaining flits: 425034 425035 425036 425037 425038 425039 425040 425041 425042 425043 [...] (349573 flits)
Measured flits: 511619 511620 511621 511622 511623 511624 511625 511626 511627 511628 [...] (348686 flits)
Class 0:
Remaining flits: 425042 425043 425044 425045 425046 425047 425048 425049 425050 425051 [...] (302638 flits)
Measured flits: 514079 517007 517008 517009 517010 517011 517012 517013 517918 517919 [...] (302492 flits)
Class 0:
Remaining flits: 468864 468865 468866 468867 468868 468869 468870 468871 468872 468873 [...] (255338 flits)
Measured flits: 523494 523495 523496 523497 523498 523499 523500 523501 523502 523503 [...] (255302 flits)
Class 0:
Remaining flits: 523494 523495 523496 523497 523498 523499 523500 523501 523502 523503 [...] (208346 flits)
Measured flits: 523494 523495 523496 523497 523498 523499 523500 523501 523502 523503 [...] (208346 flits)
Class 0:
Remaining flits: 523505 523506 523507 523508 523509 523510 523511 537963 537964 537965 [...] (160993 flits)
Measured flits: 523505 523506 523507 523508 523509 523510 523511 537963 537964 537965 [...] (160993 flits)
Class 0:
Remaining flits: 600943 600944 600945 600946 600947 605070 605071 605072 605073 605074 [...] (114106 flits)
Measured flits: 600943 600944 600945 600946 600947 605070 605071 605072 605073 605074 [...] (114106 flits)
Class 0:
Remaining flits: 648988 648989 649692 649693 649694 649695 649696 649697 649698 649699 [...] (67016 flits)
Measured flits: 648988 648989 649692 649693 649694 649695 649696 649697 649698 649699 [...] (67016 flits)
Class 0:
Remaining flits: 665988 665989 665990 665991 665992 665993 665994 665995 665996 665997 [...] (20899 flits)
Measured flits: 665988 665989 665990 665991 665992 665993 665994 665995 665996 665997 [...] (20899 flits)
Class 0:
Remaining flits: 750883 750884 750885 750886 750887 766197 766198 766199 766200 766201 [...] (1614 flits)
Measured flits: 750883 750884 750885 750886 750887 766197 766198 766199 766200 766201 [...] (1614 flits)
Time taken is 20782 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9809.48 (1 samples)
	minimum = 24 (1 samples)
	maximum = 16050 (1 samples)
Network latency average = 9504.08 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15680 (1 samples)
Flit latency average = 7456.85 (1 samples)
	minimum = 5 (1 samples)
	maximum = 15663 (1 samples)
Fragmentation average = 289.777 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1637 (1 samples)
Injected packet rate average = 0.0464462 (1 samples)
	minimum = 0.02 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.016566 (1 samples)
	minimum = 0.0113333 (1 samples)
	maximum = 0.026 (1 samples)
Injected flit rate average = 0.836075 (1 samples)
	minimum = 0.358667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.302163 (1 samples)
	minimum = 0.219 (1 samples)
	maximum = 0.451333 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.24 (1 samples)
Hops average = 5.17306 (1 samples)
Total run time 30.8235
