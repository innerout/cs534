BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.932
	minimum = 23
	maximum = 947
Network latency average = 305.519
	minimum = 23
	maximum = 927
Slowest packet = 266
Flit latency average = 250.604
	minimum = 6
	maximum = 942
Slowest flit = 4907
Fragmentation average = 151.252
	minimum = 0
	maximum = 857
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0112604
	minimum = 0.004 (at node 174)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.231677
	minimum = 0.088 (at node 174)
	maximum = 0.362 (at node 63)
Injected packet length average = 17.8489
Accepted packet length average = 20.5745
Total in-flight flits = 90806 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 598.589
	minimum = 23
	maximum = 1913
Network latency average = 581.251
	minimum = 23
	maximum = 1897
Slowest packet = 546
Flit latency average = 516.567
	minimum = 6
	maximum = 1887
Slowest flit = 11272
Fragmentation average = 182.012
	minimum = 0
	maximum = 1435
Injected packet rate average = 0.0397656
	minimum = 0.0285 (at node 88)
	maximum = 0.0485 (at node 100)
Accepted packet rate average = 0.0127214
	minimum = 0.0075 (at node 12)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.712951
	minimum = 0.5055 (at node 88)
	maximum = 0.8645 (at node 100)
Accepted flit rate average= 0.243391
	minimum = 0.145 (at node 153)
	maximum = 0.37 (at node 44)
Injected packet length average = 17.9288
Accepted packet length average = 19.1324
Total in-flight flits = 181398 (0 measured)
latency change    = 0.465523
throughput change = 0.0481265
Class 0:
Packet latency average = 1364.12
	minimum = 23
	maximum = 2807
Network latency average = 1342.58
	minimum = 23
	maximum = 2781
Slowest packet = 1149
Flit latency average = 1295.42
	minimum = 6
	maximum = 2859
Slowest flit = 15430
Fragmentation average = 202.416
	minimum = 0
	maximum = 2320
Injected packet rate average = 0.0396615
	minimum = 0.026 (at node 91)
	maximum = 0.054 (at node 36)
Accepted packet rate average = 0.0138438
	minimum = 0.005 (at node 31)
	maximum = 0.025 (at node 70)
Injected flit rate average = 0.713333
	minimum = 0.465 (at node 91)
	maximum = 0.969 (at node 139)
Accepted flit rate average= 0.248245
	minimum = 0.11 (at node 73)
	maximum = 0.448 (at node 70)
Injected packet length average = 17.9856
Accepted packet length average = 17.9319
Total in-flight flits = 270805 (0 measured)
latency change    = 0.56119
throughput change = 0.019554
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 73.1412
	minimum = 23
	maximum = 351
Network latency average = 52.664
	minimum = 23
	maximum = 335
Slowest packet = 23531
Flit latency average = 1805.34
	minimum = 6
	maximum = 3804
Slowest flit = 23564
Fragmentation average = 14.5731
	minimum = 0
	maximum = 60
Injected packet rate average = 0.0406562
	minimum = 0.027 (at node 70)
	maximum = 0.056 (at node 66)
Accepted packet rate average = 0.0138958
	minimum = 0.006 (at node 48)
	maximum = 0.023 (at node 133)
Injected flit rate average = 0.732083
	minimum = 0.478 (at node 70)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.250823
	minimum = 0.129 (at node 22)
	maximum = 0.384 (at node 133)
Injected packet length average = 18.0067
Accepted packet length average = 18.0502
Total in-flight flits = 363155 (129255 measured)
latency change    = 17.6505
throughput change = 0.0102787
Class 0:
Packet latency average = 76.4783
	minimum = 23
	maximum = 1622
Network latency average = 54.9344
	minimum = 23
	maximum = 1622
Slowest packet = 23245
Flit latency average = 2090.83
	minimum = 6
	maximum = 4663
Slowest flit = 37079
Fragmentation average = 14.4102
	minimum = 0
	maximum = 201
Injected packet rate average = 0.0401901
	minimum = 0.0295 (at node 92)
	maximum = 0.051 (at node 117)
Accepted packet rate average = 0.0137708
	minimum = 0.0075 (at node 4)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.723805
	minimum = 0.532 (at node 92)
	maximum = 0.923 (at node 117)
Accepted flit rate average= 0.247677
	minimum = 0.141 (at node 174)
	maximum = 0.35 (at node 16)
Injected packet length average = 18.0095
Accepted packet length average = 17.9856
Total in-flight flits = 453491 (255680 measured)
latency change    = 0.0436337
throughput change = 0.0127014
Class 0:
Packet latency average = 83.6257
	minimum = 23
	maximum = 2939
Network latency average = 62.1707
	minimum = 23
	maximum = 2905
Slowest packet = 23171
Flit latency average = 2366.67
	minimum = 6
	maximum = 5583
Slowest flit = 52662
Fragmentation average = 14.9852
	minimum = 0
	maximum = 272
Injected packet rate average = 0.0401163
	minimum = 0.0286667 (at node 92)
	maximum = 0.0493333 (at node 117)
Accepted packet rate average = 0.0137483
	minimum = 0.00866667 (at node 180)
	maximum = 0.019 (at node 9)
Injected flit rate average = 0.722292
	minimum = 0.514667 (at node 92)
	maximum = 0.886667 (at node 117)
Accepted flit rate average= 0.247347
	minimum = 0.151667 (at node 180)
	maximum = 0.352333 (at node 47)
Injected packet length average = 18.0049
Accepted packet length average = 17.9912
Total in-flight flits = 544259 (382849 measured)
latency change    = 0.0854693
throughput change = 0.0013336
Draining remaining packets ...
Class 0:
Remaining flits: 52145 53046 53047 53048 53049 53050 53051 53052 53053 53054 [...] (507800 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (380852 flits)
Class 0:
Remaining flits: 53055 53056 53057 53058 53059 53060 53061 53062 53063 60894 [...] (472499 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (376766 flits)
Class 0:
Remaining flits: 62123 62124 62125 62126 62127 62128 62129 62130 62131 62132 [...] (437108 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (366313 flits)
Class 0:
Remaining flits: 87541 87542 87543 87544 87545 87546 87547 87548 87549 87550 [...] (401735 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (350184 flits)
Class 0:
Remaining flits: 87549 87550 87551 88065 88066 88067 88068 88069 88070 88071 [...] (366589 flits)
Measured flits: 411930 411931 411932 411933 411934 411935 411936 411937 411938 411939 [...] (329119 flits)
Class 0:
Remaining flits: 95113 95114 95115 95116 95117 95118 95119 95120 95121 95122 [...] (331950 flits)
Measured flits: 411966 411967 411968 411969 411970 411971 411972 411973 411974 411975 [...] (304676 flits)
Class 0:
Remaining flits: 103536 103537 103538 103539 103540 103541 103542 103543 103544 103545 [...] (298252 flits)
Measured flits: 411966 411967 411968 411969 411970 411971 411972 411973 411974 411975 [...] (278246 flits)
Class 0:
Remaining flits: 164898 164899 164900 164901 164902 164903 164904 164905 164906 164907 [...] (264482 flits)
Measured flits: 412002 412003 412004 412005 412006 412007 412008 412009 412010 412011 [...] (250268 flits)
Class 0:
Remaining flits: 166122 166123 166124 166125 166126 166127 166128 166129 166130 166131 [...] (230857 flits)
Measured flits: 412002 412003 412004 412005 412006 412007 412008 412009 412010 412011 [...] (220369 flits)
Class 0:
Remaining flits: 166122 166123 166124 166125 166126 166127 166128 166129 166130 166131 [...] (197750 flits)
Measured flits: 412002 412003 412004 412005 412006 412007 412008 412009 412010 412011 [...] (189925 flits)
Class 0:
Remaining flits: 190343 190344 190345 190346 190347 190348 190349 207864 207865 207866 [...] (164537 flits)
Measured flits: 412002 412003 412004 412005 412006 412007 412008 412009 412010 412011 [...] (159372 flits)
Class 0:
Remaining flits: 223182 223183 223184 223185 223186 223187 223188 223189 223190 223191 [...] (131327 flits)
Measured flits: 412272 412273 412274 412275 412276 412277 412278 412279 412280 412281 [...] (127919 flits)
Class 0:
Remaining flits: 266562 266563 266564 266565 266566 266567 266568 266569 266570 266571 [...] (98262 flits)
Measured flits: 412487 413766 413767 413768 413769 413770 413771 413772 413773 413774 [...] (96302 flits)
Class 0:
Remaining flits: 292068 292069 292070 292071 292072 292073 292074 292075 292076 292077 [...] (65733 flits)
Measured flits: 413766 413767 413768 413769 413770 413771 413772 413773 413774 413775 [...] (64740 flits)
Class 0:
Remaining flits: 329387 329388 329389 329390 329391 329392 329393 329394 329395 329396 [...] (34944 flits)
Measured flits: 413766 413767 413768 413769 413770 413771 413772 413773 413774 413775 [...] (34567 flits)
Class 0:
Remaining flits: 370384 370385 381798 381799 381800 381801 381802 381803 381804 381805 [...] (11556 flits)
Measured flits: 433188 433189 433190 433191 433192 433193 433194 433195 433196 433197 [...] (11518 flits)
Class 0:
Remaining flits: 564767 585736 585737 619290 619291 619292 619293 619294 619295 619296 [...] (620 flits)
Measured flits: 564767 585736 585737 619290 619291 619292 619293 619294 619295 619296 [...] (620 flits)
Time taken is 23279 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10474.8 (1 samples)
	minimum = 23 (1 samples)
	maximum = 19222 (1 samples)
Network latency average = 10452.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 19222 (1 samples)
Flit latency average = 8445.39 (1 samples)
	minimum = 6 (1 samples)
	maximum = 19425 (1 samples)
Fragmentation average = 166.37 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3392 (1 samples)
Injected packet rate average = 0.0401163 (1 samples)
	minimum = 0.0286667 (1 samples)
	maximum = 0.0493333 (1 samples)
Accepted packet rate average = 0.0137483 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.722292 (1 samples)
	minimum = 0.514667 (1 samples)
	maximum = 0.886667 (1 samples)
Accepted flit rate average = 0.247347 (1 samples)
	minimum = 0.151667 (1 samples)
	maximum = 0.352333 (1 samples)
Injected packet size average = 18.0049 (1 samples)
Accepted packet size average = 17.9912 (1 samples)
Hops average = 5.06838 (1 samples)
Total run time 23.477
