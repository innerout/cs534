BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 255.722
	minimum = 23
	maximum = 843
Network latency average = 251.42
	minimum = 23
	maximum = 843
Slowest packet = 407
Flit latency average = 185.999
	minimum = 6
	maximum = 875
Slowest flit = 6999
Fragmentation average = 138.6
	minimum = 0
	maximum = 704
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00968229
	minimum = 0.003 (at node 174)
	maximum = 0.018 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.201333
	minimum = 0.073 (at node 174)
	maximum = 0.339 (at node 48)
Injected packet length average = 17.8234
Accepted packet length average = 20.794
Total in-flight flits = 30950 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 443.844
	minimum = 23
	maximum = 1707
Network latency average = 439.073
	minimum = 23
	maximum = 1699
Slowest packet = 747
Flit latency average = 352.507
	minimum = 6
	maximum = 1840
Slowest flit = 9839
Fragmentation average = 186.236
	minimum = 0
	maximum = 1391
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0109896
	minimum = 0.0065 (at node 135)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.211503
	minimum = 0.131 (at node 135)
	maximum = 0.328 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.2457
Total in-flight flits = 56987 (0 measured)
latency change    = 0.423848
throughput change = 0.0480811
Class 0:
Packet latency average = 916.14
	minimum = 23
	maximum = 2757
Network latency average = 911.081
	minimum = 23
	maximum = 2749
Slowest packet = 255
Flit latency average = 822.596
	minimum = 6
	maximum = 2771
Slowest flit = 14534
Fragmentation average = 232.21
	minimum = 0
	maximum = 1654
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0121823
	minimum = 0.004 (at node 62)
	maximum = 0.022 (at node 55)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.220073
	minimum = 0.08 (at node 62)
	maximum = 0.375 (at node 131)
Injected packet length average = 17.9811
Accepted packet length average = 18.065
Total in-flight flits = 84411 (0 measured)
latency change    = 0.515528
throughput change = 0.0389431
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 191.771
	minimum = 24
	maximum = 930
Network latency average = 186.979
	minimum = 24
	maximum = 930
Slowest packet = 11741
Flit latency average = 1152.94
	minimum = 6
	maximum = 3664
Slowest flit = 22418
Fragmentation average = 61.1939
	minimum = 1
	maximum = 607
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0122969
	minimum = 0.003 (at node 173)
	maximum = 0.022 (at node 46)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.220312
	minimum = 0.067 (at node 173)
	maximum = 0.38 (at node 46)
Injected packet length average = 17.9618
Accepted packet length average = 17.9161
Total in-flight flits = 113733 (62860 measured)
latency change    = 3.77727
throughput change = 0.00108747
Class 0:
Packet latency average = 637.039
	minimum = 24
	maximum = 1905
Network latency average = 632.335
	minimum = 23
	maximum = 1905
Slowest packet = 11590
Flit latency average = 1324.82
	minimum = 6
	maximum = 4482
Slowest flit = 34337
Fragmentation average = 121.834
	minimum = 0
	maximum = 1263
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0123281
	minimum = 0.0065 (at node 89)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.221065
	minimum = 0.105 (at node 98)
	maximum = 0.327 (at node 129)
Injected packet length average = 18.0096
Accepted packet length average = 17.9318
Total in-flight flits = 139634 (110112 measured)
latency change    = 0.698966
throughput change = 0.00340445
Class 0:
Packet latency average = 1011.78
	minimum = 24
	maximum = 2978
Network latency average = 1006.97
	minimum = 23
	maximum = 2968
Slowest packet = 11560
Flit latency average = 1497.83
	minimum = 6
	maximum = 5366
Slowest flit = 32381
Fragmentation average = 145.369
	minimum = 0
	maximum = 1266
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0122309
	minimum = 0.00733333 (at node 113)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.219903
	minimum = 0.126667 (at node 113)
	maximum = 0.316667 (at node 16)
Injected packet length average = 18.0047
Accepted packet length average = 17.9793
Total in-flight flits = 168527 (150871 measured)
latency change    = 0.370381
throughput change = 0.00528564
Class 0:
Packet latency average = 1343.21
	minimum = 23
	maximum = 3966
Network latency average = 1338.3
	minimum = 23
	maximum = 3966
Slowest packet = 11604
Flit latency average = 1676.48
	minimum = 6
	maximum = 6263
Slowest flit = 46097
Fragmentation average = 158.275
	minimum = 0
	maximum = 2732
Injected packet rate average = 0.0203372
	minimum = 0.01475 (at node 5)
	maximum = 0.026 (at node 156)
Accepted packet rate average = 0.0121758
	minimum = 0.00825 (at node 89)
	maximum = 0.017 (at node 90)
Injected flit rate average = 0.366148
	minimum = 0.2655 (at node 5)
	maximum = 0.468 (at node 156)
Accepted flit rate average= 0.218346
	minimum = 0.145 (at node 158)
	maximum = 0.31125 (at node 90)
Injected packet length average = 18.0038
Accepted packet length average = 17.9328
Total in-flight flits = 197863 (187328 measured)
latency change    = 0.246743
throughput change = 0.00712823
Class 0:
Packet latency average = 1625.38
	minimum = 23
	maximum = 4847
Network latency average = 1620.46
	minimum = 23
	maximum = 4847
Slowest packet = 11778
Flit latency average = 1841.32
	minimum = 6
	maximum = 7003
Slowest flit = 67283
Fragmentation average = 168.416
	minimum = 0
	maximum = 2732
Injected packet rate average = 0.0203438
	minimum = 0.0154 (at node 139)
	maximum = 0.0248 (at node 113)
Accepted packet rate average = 0.012125
	minimum = 0.008 (at node 190)
	maximum = 0.0162 (at node 44)
Injected flit rate average = 0.366196
	minimum = 0.2772 (at node 139)
	maximum = 0.4464 (at node 113)
Accepted flit rate average= 0.217508
	minimum = 0.149 (at node 190)
	maximum = 0.2884 (at node 90)
Injected packet length average = 18.0004
Accepted packet length average = 17.9388
Total in-flight flits = 227143 (220345 measured)
latency change    = 0.1736
throughput change = 0.00385282
Class 0:
Packet latency average = 1899.42
	minimum = 23
	maximum = 5880
Network latency average = 1894.54
	minimum = 23
	maximum = 5880
Slowest packet = 11973
Flit latency average = 2025.71
	minimum = 6
	maximum = 7755
Slowest flit = 65843
Fragmentation average = 172.291
	minimum = 0
	maximum = 2732
Injected packet rate average = 0.0203151
	minimum = 0.016 (at node 155)
	maximum = 0.0245 (at node 96)
Accepted packet rate average = 0.0120616
	minimum = 0.00766667 (at node 190)
	maximum = 0.0155 (at node 44)
Injected flit rate average = 0.365752
	minimum = 0.289333 (at node 155)
	maximum = 0.441 (at node 96)
Accepted flit rate average= 0.216054
	minimum = 0.137667 (at node 190)
	maximum = 0.279167 (at node 44)
Injected packet length average = 18.0039
Accepted packet length average = 17.9125
Total in-flight flits = 256771 (252739 measured)
latency change    = 0.144278
throughput change = 0.00673218
Class 0:
Packet latency average = 2145.37
	minimum = 23
	maximum = 6875
Network latency average = 2140.5
	minimum = 23
	maximum = 6875
Slowest packet = 11892
Flit latency average = 2208.93
	minimum = 6
	maximum = 8334
Slowest flit = 114972
Fragmentation average = 174.535
	minimum = 0
	maximum = 2732
Injected packet rate average = 0.0202984
	minimum = 0.0158571 (at node 155)
	maximum = 0.0248571 (at node 165)
Accepted packet rate average = 0.0119792
	minimum = 0.00785714 (at node 190)
	maximum = 0.015 (at node 90)
Injected flit rate average = 0.365336
	minimum = 0.287286 (at node 155)
	maximum = 0.445429 (at node 165)
Accepted flit rate average= 0.214949
	minimum = 0.143857 (at node 190)
	maximum = 0.271286 (at node 90)
Injected packet length average = 17.9983
Accepted packet length average = 17.9435
Total in-flight flits = 286578 (284333 measured)
latency change    = 0.11464
throughput change = 0.0051415
Draining all recorded packets ...
Class 0:
Remaining flits: 102690 102691 102692 102693 102694 102695 102696 102697 102698 102699 [...] (316185 flits)
Measured flits: 208152 208153 208154 208155 208156 208157 208158 208159 208160 208161 [...] (251210 flits)
Class 0:
Remaining flits: 102690 102691 102692 102693 102694 102695 102696 102697 102698 102699 [...] (343785 flits)
Measured flits: 208463 208464 208465 208466 208467 208468 208469 208470 208471 208472 [...] (217720 flits)
Class 0:
Remaining flits: 137088 137089 137090 137091 137092 137093 137094 137095 137096 137097 [...] (373149 flits)
Measured flits: 208467 208468 208469 208470 208471 208472 208473 208474 208475 208476 [...] (184964 flits)
Class 0:
Remaining flits: 163837 163838 163839 163840 163841 163842 163843 163844 163845 163846 [...] (402958 flits)
Measured flits: 210191 210192 210193 210194 210195 210196 210197 210198 210199 210200 [...] (154980 flits)
Class 0:
Remaining flits: 169056 169057 169058 169059 169060 169061 169062 169063 169064 169065 [...] (432354 flits)
Measured flits: 210978 210979 210980 210981 210982 210983 210984 210985 210986 210987 [...] (128274 flits)
Class 0:
Remaining flits: 198288 198289 198290 198291 198292 198293 198294 198295 198296 198297 [...] (464644 flits)
Measured flits: 210978 210979 210980 210981 210982 210983 210984 210985 210986 210987 [...] (105058 flits)
Class 0:
Remaining flits: 228655 228656 228657 228658 228659 228660 228661 228662 228663 228664 [...] (494053 flits)
Measured flits: 228655 228656 228657 228658 228659 228660 228661 228662 228663 228664 [...] (85705 flits)
Class 0:
Remaining flits: 244332 244333 244334 244335 244336 244337 244338 244339 244340 244341 [...] (525603 flits)
Measured flits: 244332 244333 244334 244335 244336 244337 244338 244339 244340 244341 [...] (69533 flits)
Class 0:
Remaining flits: 255350 255351 255352 255353 255354 255355 255356 255357 255358 255359 [...] (555921 flits)
Measured flits: 255350 255351 255352 255353 255354 255355 255356 255357 255358 255359 [...] (55598 flits)
Class 0:
Remaining flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (587039 flits)
Measured flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (44458 flits)
Class 0:
Remaining flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (616857 flits)
Measured flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (34722 flits)
Class 0:
Remaining flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (646697 flits)
Measured flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (27171 flits)
Class 0:
Remaining flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (677110 flits)
Measured flits: 271944 271945 271946 271947 271948 271949 271950 271951 271952 271953 [...] (21150 flits)
Class 0:
Remaining flits: 300168 300169 300170 300171 300172 300173 300174 300175 300176 300177 [...] (706440 flits)
Measured flits: 300168 300169 300170 300171 300172 300173 300174 300175 300176 300177 [...] (16096 flits)
Class 0:
Remaining flits: 379944 379945 379946 379947 379948 379949 379950 379951 379952 379953 [...] (736697 flits)
Measured flits: 379944 379945 379946 379947 379948 379949 379950 379951 379952 379953 [...] (11894 flits)
Class 0:
Remaining flits: 402391 402392 402393 402394 402395 402396 402397 402398 402399 402400 [...] (767324 flits)
Measured flits: 402391 402392 402393 402394 402395 402396 402397 402398 402399 402400 [...] (8970 flits)
Class 0:
Remaining flits: 455022 455023 455024 455025 455026 455027 455028 455029 455030 455031 [...] (798074 flits)
Measured flits: 455022 455023 455024 455025 455026 455027 455028 455029 455030 455031 [...] (6259 flits)
Class 0:
Remaining flits: 455022 455023 455024 455025 455026 455027 455028 455029 455030 455031 [...] (828612 flits)
Measured flits: 455022 455023 455024 455025 455026 455027 455028 455029 455030 455031 [...] (4749 flits)
Class 0:
Remaining flits: 455023 455024 455025 455026 455027 455028 455029 455030 455031 455032 [...] (858724 flits)
Measured flits: 455023 455024 455025 455026 455027 455028 455029 455030 455031 455032 [...] (3630 flits)
Class 0:
Remaining flits: 455027 455028 455029 455030 455031 455032 455033 455034 455035 455036 [...] (889787 flits)
Measured flits: 455027 455028 455029 455030 455031 455032 455033 455034 455035 455036 [...] (2506 flits)
Class 0:
Remaining flits: 457290 457291 457292 457293 457294 457295 457296 457297 457298 457299 [...] (918800 flits)
Measured flits: 457290 457291 457292 457293 457294 457295 457296 457297 457298 457299 [...] (1775 flits)
Class 0:
Remaining flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (948800 flits)
Measured flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (1189 flits)
Class 0:
Remaining flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (977941 flits)
Measured flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (864 flits)
Class 0:
Remaining flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (1008526 flits)
Measured flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (632 flits)
Class 0:
Remaining flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (1039953 flits)
Measured flits: 488592 488593 488594 488595 488596 488597 488598 488599 488600 488601 [...] (429 flits)
Class 0:
Remaining flits: 542374 542375 545040 545041 545042 545043 545044 545045 545046 545047 [...] (1067511 flits)
Measured flits: 542374 542375 545040 545041 545042 545043 545044 545045 545046 545047 [...] (273 flits)
Class 0:
Remaining flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (1095344 flits)
Measured flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (199 flits)
Class 0:
Remaining flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (1124423 flits)
Measured flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (162 flits)
Class 0:
Remaining flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (1153419 flits)
Measured flits: 545040 545041 545042 545043 545044 545045 545046 545047 545048 545049 [...] (148 flits)
Class 0:
Remaining flits: 606942 606943 606944 606945 606946 606947 606948 606949 606950 606951 [...] (1183091 flits)
Measured flits: 606942 606943 606944 606945 606946 606947 606948 606949 606950 606951 [...] (54 flits)
Class 0:
Remaining flits: 653616 653617 653618 653619 653620 653621 653622 653623 653624 653625 [...] (1208418 flits)
Measured flits: 653616 653617 653618 653619 653620 653621 653622 653623 653624 653625 [...] (28 flits)
Class 0:
Remaining flits: 653616 653617 653618 653619 653620 653621 653622 653623 653624 653625 [...] (1236496 flits)
Measured flits: 653616 653617 653618 653619 653620 653621 653622 653623 653624 653625 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 731682 731683 731684 731685 731686 731687 731688 731689 731690 731691 [...] (1228336 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 731682 731683 731684 731685 731686 731687 731688 731689 731690 731691 [...] (1195396 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (1162999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (1130448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (1097560 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (1064299 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (1030416 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 750114 750115 750116 750117 750118 750119 750120 750121 750122 750123 [...] (996455 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (962805 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (929322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (896585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (863292 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914976 914977 914978 914979 914980 914981 914982 914983 914984 914985 [...] (829395 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 914976 914977 914978 914979 914980 914981 914982 914983 914984 914985 [...] (796120 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923562 923563 923564 923565 923566 923567 923568 923569 923570 923571 [...] (762750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923562 923563 923564 923565 923566 923567 923568 923569 923570 923571 [...] (729942 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923579 951354 951355 951356 951357 951358 951359 951360 951361 951362 [...] (696908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951354 951355 951356 951357 951358 951359 951360 951361 951362 951363 [...] (664576 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951354 951355 951356 951357 951358 951359 951360 951361 951362 951363 [...] (632010 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951363 951364 951365 951366 951367 951368 951369 951370 951371 1006920 [...] (599328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1006920 1006921 1006922 1006923 1006924 1006925 1006926 1006927 1006928 1006929 [...] (566228 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1006920 1006921 1006922 1006923 1006924 1006925 1006926 1006927 1006928 1006929 [...] (533600 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1031832 1031833 1031834 1031835 1031836 1031837 1031838 1031839 1031840 1031841 [...] (500606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1031832 1031833 1031834 1031835 1031836 1031837 1031838 1031839 1031840 1031841 [...] (468532 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1031832 1031833 1031834 1031835 1031836 1031837 1031838 1031839 1031840 1031841 [...] (435522 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1135386 1135387 1135388 1135389 1135390 1135391 1135392 1135393 1135394 1135395 [...] (402640 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1135386 1135387 1135388 1135389 1135390 1135391 1135392 1135393 1135394 1135395 [...] (369161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1135386 1135387 1135388 1135389 1135390 1135391 1135392 1135393 1135394 1135395 [...] (335286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1145124 1145125 1145126 1145127 1145128 1145129 1145130 1145131 1145132 1145133 [...] (301692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1145140 1145141 1195614 1195615 1195616 1195617 1195618 1195619 1195620 1195621 [...] (267441 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1226898 1226899 1226900 1226901 1226902 1226903 1226904 1226905 1226906 1226907 [...] (232255 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1226898 1226899 1226900 1226901 1226902 1226903 1226904 1226905 1226906 1226907 [...] (197981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1226898 1226899 1226900 1226901 1226902 1226903 1226904 1226905 1226906 1226907 [...] (164005 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1226898 1226899 1226900 1226901 1226902 1226903 1226904 1226905 1226906 1226907 [...] (131099 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1226898 1226899 1226900 1226901 1226902 1226903 1226904 1226905 1226906 1226907 [...] (99879 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1286100 1286101 1286102 1286103 1286104 1286105 1286106 1286107 1286108 1286109 [...] (71220 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1476198 1476199 1476200 1476201 1476202 1476203 1476204 1476205 1476206 1476207 [...] (44902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1726043 1726044 1726045 1726046 1726047 1726048 1726049 1726050 1726051 1726052 [...] (23798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2034357 2034358 2034359 2096460 2096461 2096462 2096463 2096464 2096465 2096466 [...] (7144 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2299122 2299123 2299124 2299125 2299126 2299127 2299128 2299129 2299130 2299131 [...] (1173 flits)
Measured flits: (0 flits)
Time taken is 83474 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5629.95 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33535 (1 samples)
Network latency average = 5624.97 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33535 (1 samples)
Flit latency average = 17939.1 (1 samples)
	minimum = 6 (1 samples)
	maximum = 61250 (1 samples)
Fragmentation average = 209.066 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5413 (1 samples)
Injected packet rate average = 0.0202984 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0248571 (1 samples)
Accepted packet rate average = 0.0119792 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.365336 (1 samples)
	minimum = 0.287286 (1 samples)
	maximum = 0.445429 (1 samples)
Accepted flit rate average = 0.214949 (1 samples)
	minimum = 0.143857 (1 samples)
	maximum = 0.271286 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 17.9435 (1 samples)
Hops average = 5.07713 (1 samples)
Total run time 89.5777
