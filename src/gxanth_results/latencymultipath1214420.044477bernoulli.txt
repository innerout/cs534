BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 312.902
	minimum = 22
	maximum = 852
Network latency average = 295.906
	minimum = 22
	maximum = 787
Slowest packet = 229
Flit latency average = 273.412
	minimum = 5
	maximum = 836
Slowest flit = 20244
Fragmentation average = 24.484
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0419323
	minimum = 0.025 (at node 82)
	maximum = 0.055 (at node 109)
Accepted packet rate average = 0.0152708
	minimum = 0.007 (at node 115)
	maximum = 0.027 (at node 44)
Injected flit rate average = 0.747563
	minimum = 0.45 (at node 82)
	maximum = 0.982 (at node 109)
Accepted flit rate average= 0.281677
	minimum = 0.126 (at node 115)
	maximum = 0.489 (at node 44)
Injected packet length average = 17.8278
Accepted packet length average = 18.4454
Total in-flight flits = 91052 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 623.408
	minimum = 22
	maximum = 1605
Network latency average = 597.466
	minimum = 22
	maximum = 1527
Slowest packet = 2167
Flit latency average = 574.121
	minimum = 5
	maximum = 1530
Slowest flit = 65263
Fragmentation average = 26.21
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0359219
	minimum = 0.022 (at node 120)
	maximum = 0.048 (at node 149)
Accepted packet rate average = 0.0151302
	minimum = 0.009 (at node 153)
	maximum = 0.0215 (at node 14)
Injected flit rate average = 0.644815
	minimum = 0.396 (at node 120)
	maximum = 0.862 (at node 149)
Accepted flit rate average= 0.275495
	minimum = 0.162 (at node 153)
	maximum = 0.387 (at node 14)
Injected packet length average = 17.9505
Accepted packet length average = 18.2083
Total in-flight flits = 144230 (0 measured)
latency change    = 0.498078
throughput change = 0.0224407
Class 0:
Packet latency average = 1612.06
	minimum = 24
	maximum = 2370
Network latency average = 1536.03
	minimum = 22
	maximum = 2331
Slowest packet = 4236
Flit latency average = 1518.55
	minimum = 5
	maximum = 2322
Slowest flit = 95916
Fragmentation average = 22.2949
	minimum = 0
	maximum = 186
Injected packet rate average = 0.0156823
	minimum = 0.002 (at node 63)
	maximum = 0.037 (at node 93)
Accepted packet rate average = 0.0148177
	minimum = 0.005 (at node 180)
	maximum = 0.026 (at node 151)
Injected flit rate average = 0.282156
	minimum = 0.036 (at node 63)
	maximum = 0.666 (at node 93)
Accepted flit rate average= 0.266203
	minimum = 0.09 (at node 180)
	maximum = 0.459 (at node 151)
Injected packet length average = 17.992
Accepted packet length average = 17.9652
Total in-flight flits = 147659 (0 measured)
latency change    = 0.613285
throughput change = 0.0349044
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1466.7
	minimum = 503
	maximum = 2423
Network latency average = 103.923
	minimum = 22
	maximum = 928
Slowest packet = 16939
Flit latency average = 2043.6
	minimum = 5
	maximum = 3099
Slowest flit = 126557
Fragmentation average = 4.88462
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0159635
	minimum = 0.004 (at node 118)
	maximum = 0.039 (at node 122)
Accepted packet rate average = 0.0151875
	minimum = 0.006 (at node 86)
	maximum = 0.026 (at node 78)
Injected flit rate average = 0.288062
	minimum = 0.072 (at node 118)
	maximum = 0.713 (at node 122)
Accepted flit rate average= 0.27312
	minimum = 0.108 (at node 86)
	maximum = 0.45 (at node 44)
Injected packet length average = 18.045
Accepted packet length average = 17.9832
Total in-flight flits = 150624 (53028 measured)
latency change    = 0.0991076
throughput change = 0.0253247
Class 0:
Packet latency average = 2313
	minimum = 503
	maximum = 4009
Network latency average = 831.079
	minimum = 22
	maximum = 1944
Slowest packet = 16939
Flit latency average = 2253.06
	minimum = 5
	maximum = 3975
Slowest flit = 146600
Fragmentation average = 12.1253
	minimum = 0
	maximum = 79
Injected packet rate average = 0.0154453
	minimum = 0.0055 (at node 88)
	maximum = 0.03 (at node 85)
Accepted packet rate average = 0.0150286
	minimum = 0.008 (at node 5)
	maximum = 0.022 (at node 28)
Injected flit rate average = 0.278518
	minimum = 0.099 (at node 88)
	maximum = 0.54 (at node 85)
Accepted flit rate average= 0.270172
	minimum = 0.1515 (at node 5)
	maximum = 0.4035 (at node 28)
Injected packet length average = 18.0325
Accepted packet length average = 17.9771
Total in-flight flits = 151031 (98026 measured)
latency change    = 0.365888
throughput change = 0.0109113
Class 0:
Packet latency average = 3088.95
	minimum = 503
	maximum = 4793
Network latency average = 1397.99
	minimum = 22
	maximum = 2958
Slowest packet = 16939
Flit latency average = 2423.11
	minimum = 5
	maximum = 4652
Slowest flit = 171810
Fragmentation average = 14.666
	minimum = 0
	maximum = 110
Injected packet rate average = 0.0152031
	minimum = 0.00733333 (at node 88)
	maximum = 0.025 (at node 76)
Accepted packet rate average = 0.0149288
	minimum = 0.0103333 (at node 36)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.273748
	minimum = 0.132 (at node 88)
	maximum = 0.45 (at node 76)
Accepted flit rate average= 0.268465
	minimum = 0.187 (at node 36)
	maximum = 0.365 (at node 128)
Injected packet length average = 18.0061
Accepted packet length average = 17.983
Total in-flight flits = 150865 (131327 measured)
latency change    = 0.251202
throughput change = 0.00635686
Class 0:
Packet latency average = 3714.38
	minimum = 503
	maximum = 5570
Network latency average = 1917.57
	minimum = 22
	maximum = 3974
Slowest packet = 16939
Flit latency average = 2519.11
	minimum = 5
	maximum = 5273
Slowest flit = 228268
Fragmentation average = 16.4333
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0152708
	minimum = 0.008 (at node 155)
	maximum = 0.02375 (at node 76)
Accepted packet rate average = 0.014901
	minimum = 0.0105 (at node 64)
	maximum = 0.01975 (at node 128)
Injected flit rate average = 0.27478
	minimum = 0.144 (at node 155)
	maximum = 0.42725 (at node 76)
Accepted flit rate average= 0.268296
	minimum = 0.1875 (at node 64)
	maximum = 0.3555 (at node 128)
Injected packet length average = 17.9938
Accepted packet length average = 18.0052
Total in-flight flits = 152658 (149869 measured)
latency change    = 0.16838
throughput change = 0.000632529
Class 0:
Packet latency average = 4259.41
	minimum = 503
	maximum = 6492
Network latency average = 2311.79
	minimum = 22
	maximum = 4870
Slowest packet = 16939
Flit latency average = 2588.93
	minimum = 5
	maximum = 5536
Slowest flit = 254625
Fragmentation average = 17.8001
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0151875
	minimum = 0.0076 (at node 88)
	maximum = 0.0232 (at node 76)
Accepted packet rate average = 0.0149271
	minimum = 0.011 (at node 42)
	maximum = 0.0196 (at node 70)
Injected flit rate average = 0.273557
	minimum = 0.1344 (at node 88)
	maximum = 0.4148 (at node 76)
Accepted flit rate average= 0.268521
	minimum = 0.2002 (at node 42)
	maximum = 0.3504 (at node 157)
Injected packet length average = 18.012
Accepted packet length average = 17.9888
Total in-flight flits = 152589 (152470 measured)
latency change    = 0.127959
throughput change = 0.000838894
Class 0:
Packet latency average = 4687.17
	minimum = 503
	maximum = 7101
Network latency average = 2491.77
	minimum = 22
	maximum = 5482
Slowest packet = 16939
Flit latency average = 2633.04
	minimum = 5
	maximum = 5894
Slowest flit = 290547
Fragmentation average = 18.2334
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0151007
	minimum = 0.00933333 (at node 37)
	maximum = 0.0226667 (at node 45)
Accepted packet rate average = 0.0149002
	minimum = 0.0111667 (at node 42)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.271856
	minimum = 0.168833 (at node 37)
	maximum = 0.405333 (at node 45)
Accepted flit rate average= 0.268114
	minimum = 0.203667 (at node 42)
	maximum = 0.342 (at node 157)
Injected packet length average = 18.0029
Accepted packet length average = 17.994
Total in-flight flits = 152190 (152190 measured)
latency change    = 0.0912633
throughput change = 0.00151845
Class 0:
Packet latency average = 5069.95
	minimum = 503
	maximum = 7698
Network latency average = 2599.31
	minimum = 22
	maximum = 6010
Slowest packet = 16939
Flit latency average = 2671.65
	minimum = 5
	maximum = 5989
Slowest flit = 326738
Fragmentation average = 18.8111
	minimum = 0
	maximum = 212
Injected packet rate average = 0.0150692
	minimum = 0.00871429 (at node 37)
	maximum = 0.0214286 (at node 80)
Accepted packet rate average = 0.0148951
	minimum = 0.0114286 (at node 42)
	maximum = 0.0184286 (at node 70)
Injected flit rate average = 0.271233
	minimum = 0.157714 (at node 37)
	maximum = 0.388 (at node 80)
Accepted flit rate average= 0.267949
	minimum = 0.206714 (at node 67)
	maximum = 0.328143 (at node 70)
Injected packet length average = 17.9992
Accepted packet length average = 17.9891
Total in-flight flits = 152018 (152018 measured)
latency change    = 0.0754991
throughput change = 0.000615993
Draining all recorded packets ...
Class 0:
Remaining flits: 415692 415693 415694 415695 415696 415697 415698 415699 415700 415701 [...] (151559 flits)
Measured flits: 415692 415693 415694 415695 415696 415697 415698 415699 415700 415701 [...] (151559 flits)
Class 0:
Remaining flits: 463338 463339 463340 463341 463342 463343 463344 463345 463346 463347 [...] (151434 flits)
Measured flits: 463338 463339 463340 463341 463342 463343 463344 463345 463346 463347 [...] (151434 flits)
Class 0:
Remaining flits: 528462 528463 528464 528465 528466 528467 528468 528469 528470 528471 [...] (150327 flits)
Measured flits: 528462 528463 528464 528465 528466 528467 528468 528469 528470 528471 [...] (150327 flits)
Class 0:
Remaining flits: 573678 573679 573680 573681 573682 573683 573684 573685 573686 573687 [...] (151396 flits)
Measured flits: 573678 573679 573680 573681 573682 573683 573684 573685 573686 573687 [...] (151396 flits)
Class 0:
Remaining flits: 573678 573679 573680 573681 573682 573683 573684 573685 573686 573687 [...] (152378 flits)
Measured flits: 573678 573679 573680 573681 573682 573683 573684 573685 573686 573687 [...] (152378 flits)
Class 0:
Remaining flits: 649602 649603 649604 649605 649606 649607 649608 649609 649610 649611 [...] (151548 flits)
Measured flits: 649602 649603 649604 649605 649606 649607 649608 649609 649610 649611 [...] (151548 flits)
Class 0:
Remaining flits: 668603 668604 668605 668606 668607 668608 668609 679158 679159 679160 [...] (150552 flits)
Measured flits: 668603 668604 668605 668606 668607 668608 668609 679158 679159 679160 [...] (150552 flits)
Class 0:
Remaining flits: 679158 679159 679160 679161 679162 679163 679164 679165 679166 679167 [...] (147365 flits)
Measured flits: 679158 679159 679160 679161 679162 679163 679164 679165 679166 679167 [...] (147365 flits)
Class 0:
Remaining flits: 818298 818299 818300 818301 818302 818303 818304 818305 818306 818307 [...] (148393 flits)
Measured flits: 818298 818299 818300 818301 818302 818303 818304 818305 818306 818307 [...] (148393 flits)
Class 0:
Remaining flits: 828684 828685 828686 828687 828688 828689 828690 828691 828692 828693 [...] (147225 flits)
Measured flits: 828684 828685 828686 828687 828688 828689 828690 828691 828692 828693 [...] (147225 flits)
Class 0:
Remaining flits: 878328 878329 878330 878331 878332 878333 878334 878335 878336 878337 [...] (147290 flits)
Measured flits: 878328 878329 878330 878331 878332 878333 878334 878335 878336 878337 [...] (147272 flits)
Class 0:
Remaining flits: 927954 927955 927956 927957 927958 927959 927960 927961 927962 927963 [...] (145620 flits)
Measured flits: 927954 927955 927956 927957 927958 927959 927960 927961 927962 927963 [...] (144702 flits)
Class 0:
Remaining flits: 975078 975079 975080 975081 975082 975083 975084 975085 975086 975087 [...] (143354 flits)
Measured flits: 975078 975079 975080 975081 975082 975083 975084 975085 975086 975087 [...] (139754 flits)
Class 0:
Remaining flits: 1019052 1019053 1019054 1019055 1019056 1019057 1019058 1019059 1019060 1019061 [...] (141841 flits)
Measured flits: 1019052 1019053 1019054 1019055 1019056 1019057 1019058 1019059 1019060 1019061 [...] (132661 flits)
Class 0:
Remaining flits: 1083762 1083763 1083764 1083765 1083766 1083767 1083768 1083769 1083770 1083771 [...] (142072 flits)
Measured flits: 1083762 1083763 1083764 1083765 1083766 1083767 1083768 1083769 1083770 1083771 [...] (125733 flits)
Class 0:
Remaining flits: 1115658 1115659 1115660 1115661 1115662 1115663 1115664 1115665 1115666 1115667 [...] (142914 flits)
Measured flits: 1115658 1115659 1115660 1115661 1115662 1115663 1115664 1115665 1115666 1115667 [...] (115728 flits)
Class 0:
Remaining flits: 1151298 1151299 1151300 1151301 1151302 1151303 1151304 1151305 1151306 1151307 [...] (141122 flits)
Measured flits: 1151298 1151299 1151300 1151301 1151302 1151303 1151304 1151305 1151306 1151307 [...] (102579 flits)
Class 0:
Remaining flits: 1168380 1168381 1168382 1168383 1168384 1168385 1168386 1168387 1168388 1168389 [...] (141781 flits)
Measured flits: 1168380 1168381 1168382 1168383 1168384 1168385 1168386 1168387 1168388 1168389 [...] (87202 flits)
Class 0:
Remaining flits: 1251774 1251775 1251776 1251777 1251778 1251779 1251780 1251781 1251782 1251783 [...] (139284 flits)
Measured flits: 1251774 1251775 1251776 1251777 1251778 1251779 1251780 1251781 1251782 1251783 [...] (67863 flits)
Class 0:
Remaining flits: 1348992 1348993 1348994 1348995 1348996 1348997 1348998 1348999 1349000 1349001 [...] (140625 flits)
Measured flits: 1348992 1348993 1348994 1348995 1348996 1348997 1348998 1348999 1349000 1349001 [...] (47432 flits)
Class 0:
Remaining flits: 1377369 1377370 1377371 1377372 1377373 1377374 1377375 1377376 1377377 1380402 [...] (140714 flits)
Measured flits: 1377369 1377370 1377371 1377372 1377373 1377374 1377375 1377376 1377377 1401768 [...] (29101 flits)
Class 0:
Remaining flits: 1401768 1401769 1401770 1401771 1401772 1401773 1401774 1401775 1401776 1401777 [...] (142672 flits)
Measured flits: 1401768 1401769 1401770 1401771 1401772 1401773 1401774 1401775 1401776 1401777 [...] (16537 flits)
Class 0:
Remaining flits: 1406844 1406845 1406846 1406847 1406848 1406849 1406850 1406851 1406852 1406853 [...] (144032 flits)
Measured flits: 1423656 1423657 1423658 1423659 1423660 1423661 1423662 1423663 1423664 1423665 [...] (7838 flits)
Class 0:
Remaining flits: 1424070 1424071 1424072 1424073 1424074 1424075 1424076 1424077 1424078 1424079 [...] (139819 flits)
Measured flits: 1449702 1449703 1449704 1449705 1449706 1449707 1449708 1449709 1449710 1449711 [...] (3776 flits)
Class 0:
Remaining flits: 1457676 1457677 1457678 1457679 1457680 1457681 1457682 1457683 1457684 1457685 [...] (141351 flits)
Measured flits: 1644048 1644049 1644050 1644051 1644052 1644053 1644054 1644055 1644056 1644057 [...] (1565 flits)
Class 0:
Remaining flits: 1630170 1630171 1630172 1630173 1630174 1630175 1630176 1630177 1630178 1630179 [...] (139503 flits)
Measured flits: 1704006 1704007 1704008 1704009 1704010 1704011 1704012 1704013 1704014 1704015 [...] (432 flits)
Class 0:
Remaining flits: 1630185 1630186 1630187 1647270 1647271 1647272 1647273 1647274 1647275 1647276 [...] (139971 flits)
Measured flits: 1730268 1730269 1730270 1730271 1730272 1730273 1730274 1730275 1730276 1730277 [...] (72 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1815210 1815211 1815212 1815213 1815214 1815215 1815216 1815217 1815218 1815219 [...] (91350 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1841238 1841239 1841240 1841241 1841242 1841243 1841244 1841245 1841246 1841247 [...] (43673 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1890054 1890055 1890056 1890057 1890058 1890059 1890060 1890061 1890062 1890063 [...] (5846 flits)
Measured flits: (0 flits)
Time taken is 41762 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12062.9 (1 samples)
	minimum = 503 (1 samples)
	maximum = 28042 (1 samples)
Network latency average = 2866.5 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8911 (1 samples)
Flit latency average = 2801.42 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9936 (1 samples)
Fragmentation average = 20.4999 (1 samples)
	minimum = 0 (1 samples)
	maximum = 212 (1 samples)
Injected packet rate average = 0.0150692 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.0214286 (1 samples)
Accepted packet rate average = 0.0148951 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0184286 (1 samples)
Injected flit rate average = 0.271233 (1 samples)
	minimum = 0.157714 (1 samples)
	maximum = 0.388 (1 samples)
Accepted flit rate average = 0.267949 (1 samples)
	minimum = 0.206714 (1 samples)
	maximum = 0.328143 (1 samples)
Injected packet size average = 17.9992 (1 samples)
Accepted packet size average = 17.9891 (1 samples)
Hops average = 5.05681 (1 samples)
Total run time 44.5234
