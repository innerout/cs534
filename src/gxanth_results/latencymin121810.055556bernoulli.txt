BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 397.632
	minimum = 23
	maximum = 942
Network latency average = 301.523
	minimum = 23
	maximum = 925
Slowest packet = 402
Flit latency average = 257.664
	minimum = 6
	maximum = 936
Slowest flit = 7906
Fragmentation average = 51.4247
	minimum = 0
	maximum = 133
Injected packet rate average = 0.0142344
	minimum = 0.006 (at node 94)
	maximum = 0.028 (at node 6)
Accepted packet rate average = 0.00923437
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 132)
Injected flit rate average = 0.251995
	minimum = 0.108 (at node 94)
	maximum = 0.504 (at node 6)
Accepted flit rate average= 0.171974
	minimum = 0.054 (at node 174)
	maximum = 0.319 (at node 132)
Injected packet length average = 17.7033
Accepted packet length average = 18.6232
Total in-flight flits = 17957 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 806.996
	minimum = 23
	maximum = 1875
Network latency average = 412.588
	minimum = 23
	maximum = 1739
Slowest packet = 688
Flit latency average = 359.067
	minimum = 6
	maximum = 1879
Slowest flit = 15160
Fragmentation average = 55.4261
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0115885
	minimum = 0.0045 (at node 93)
	maximum = 0.02 (at node 73)
Accepted packet rate average = 0.00908854
	minimum = 0.004 (at node 174)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.206443
	minimum = 0.081 (at node 93)
	maximum = 0.36 (at node 73)
Accepted flit rate average= 0.166781
	minimum = 0.075 (at node 174)
	maximum = 0.279 (at node 22)
Injected packet length average = 17.8144
Accepted packet length average = 18.3507
Total in-flight flits = 17838 (0 measured)
latency change    = 0.507269
throughput change = 0.0311348
Class 0:
Packet latency average = 2053.85
	minimum = 1311
	maximum = 2746
Network latency average = 552.591
	minimum = 27
	maximum = 2592
Slowest packet = 842
Flit latency average = 491.038
	minimum = 6
	maximum = 2546
Slowest flit = 28796
Fragmentation average = 57.6837
	minimum = 0
	maximum = 127
Injected packet rate average = 0.00869792
	minimum = 0 (at node 20)
	maximum = 0.026 (at node 5)
Accepted packet rate average = 0.00877604
	minimum = 0.002 (at node 153)
	maximum = 0.017 (at node 86)
Injected flit rate average = 0.157719
	minimum = 0 (at node 61)
	maximum = 0.468 (at node 5)
Accepted flit rate average= 0.158943
	minimum = 0.036 (at node 153)
	maximum = 0.303 (at node 86)
Injected packet length average = 18.1329
Accepted packet length average = 18.111
Total in-flight flits = 17525 (0 measured)
latency change    = 0.607081
throughput change = 0.0493168
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 3042.05
	minimum = 2166
	maximum = 3575
Network latency average = 341.378
	minimum = 27
	maximum = 969
Slowest packet = 6274
Flit latency average = 497.722
	minimum = 6
	maximum = 2550
Slowest flit = 35430
Fragmentation average = 51.992
	minimum = 0
	maximum = 123
Injected packet rate average = 0.00892188
	minimum = 0 (at node 60)
	maximum = 0.018 (at node 27)
Accepted packet rate average = 0.00894792
	minimum = 0 (at node 184)
	maximum = 0.021 (at node 187)
Injected flit rate average = 0.159943
	minimum = 0 (at node 60)
	maximum = 0.329 (at node 83)
Accepted flit rate average= 0.159661
	minimum = 0 (at node 184)
	maximum = 0.338 (at node 187)
Injected packet length average = 17.927
Accepted packet length average = 17.8434
Total in-flight flits = 17632 (16246 measured)
latency change    = 0.324847
throughput change = 0.00450171
Class 0:
Packet latency average = 3508.41
	minimum = 2166
	maximum = 4531
Network latency average = 452.437
	minimum = 23
	maximum = 1608
Slowest packet = 6274
Flit latency average = 493.55
	minimum = 6
	maximum = 2550
Slowest flit = 35430
Fragmentation average = 56.9903
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00894792
	minimum = 0.002 (at node 57)
	maximum = 0.0175 (at node 145)
Accepted packet rate average = 0.00889063
	minimum = 0.0035 (at node 190)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.160789
	minimum = 0.036 (at node 57)
	maximum = 0.316 (at node 145)
Accepted flit rate average= 0.160151
	minimum = 0.0565 (at node 190)
	maximum = 0.3305 (at node 16)
Injected packet length average = 17.9694
Accepted packet length average = 18.0135
Total in-flight flits = 17839 (17779 measured)
latency change    = 0.132928
throughput change = 0.00305701
Class 0:
Packet latency average = 3967.87
	minimum = 2166
	maximum = 5505
Network latency average = 495.517
	minimum = 23
	maximum = 2254
Slowest packet = 6274
Flit latency average = 491.915
	minimum = 6
	maximum = 2550
Slowest flit = 35430
Fragmentation average = 58.8733
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00900174
	minimum = 0.003 (at node 125)
	maximum = 0.015 (at node 30)
Accepted packet rate average = 0.00900521
	minimum = 0.00533333 (at node 86)
	maximum = 0.0156667 (at node 16)
Injected flit rate average = 0.161819
	minimum = 0.052 (at node 125)
	maximum = 0.267333 (at node 30)
Accepted flit rate average= 0.161589
	minimum = 0.095 (at node 134)
	maximum = 0.280333 (at node 16)
Injected packet length average = 17.9765
Accepted packet length average = 17.9439
Total in-flight flits = 17870 (17870 measured)
latency change    = 0.115793
throughput change = 0.00889605
Class 0:
Packet latency average = 4406.67
	minimum = 2166
	maximum = 6305
Network latency average = 512.215
	minimum = 23
	maximum = 2693
Slowest packet = 6274
Flit latency average = 490.407
	minimum = 6
	maximum = 2676
Slowest flit = 146987
Fragmentation average = 58.3726
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00902083
	minimum = 0.00375 (at node 15)
	maximum = 0.01525 (at node 191)
Accepted packet rate average = 0.00905469
	minimum = 0.0055 (at node 4)
	maximum = 0.01425 (at node 129)
Injected flit rate average = 0.162322
	minimum = 0.0675 (at node 15)
	maximum = 0.275 (at node 191)
Accepted flit rate average= 0.162603
	minimum = 0.0965 (at node 36)
	maximum = 0.25575 (at node 129)
Injected packet length average = 17.9941
Accepted packet length average = 17.9579
Total in-flight flits = 17494 (17494 measured)
latency change    = 0.0995782
throughput change = 0.00623804
Class 0:
Packet latency average = 4820
	minimum = 2166
	maximum = 7214
Network latency average = 517.432
	minimum = 23
	maximum = 2693
Slowest packet = 6274
Flit latency average = 487.152
	minimum = 6
	maximum = 2676
Slowest flit = 146987
Fragmentation average = 58.1368
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00900312
	minimum = 0.0036 (at node 85)
	maximum = 0.0158 (at node 191)
Accepted packet rate average = 0.00901771
	minimum = 0.006 (at node 163)
	maximum = 0.0134 (at node 16)
Injected flit rate average = 0.161903
	minimum = 0.068 (at node 85)
	maximum = 0.2814 (at node 191)
Accepted flit rate average= 0.162084
	minimum = 0.107 (at node 163)
	maximum = 0.2402 (at node 16)
Injected packet length average = 17.983
Accepted packet length average = 17.974
Total in-flight flits = 17426 (17426 measured)
latency change    = 0.0857525
throughput change = 0.00319889
Class 0:
Packet latency average = 5239.98
	minimum = 2166
	maximum = 8249
Network latency average = 524.003
	minimum = 23
	maximum = 2693
Slowest packet = 6274
Flit latency average = 488.363
	minimum = 6
	maximum = 2676
Slowest flit = 146987
Fragmentation average = 57.6153
	minimum = 0
	maximum = 141
Injected packet rate average = 0.00900434
	minimum = 0.0045 (at node 85)
	maximum = 0.0146667 (at node 191)
Accepted packet rate average = 0.00900781
	minimum = 0.00633333 (at node 36)
	maximum = 0.0126667 (at node 128)
Injected flit rate average = 0.161962
	minimum = 0.081 (at node 85)
	maximum = 0.264333 (at node 191)
Accepted flit rate average= 0.162003
	minimum = 0.114 (at node 86)
	maximum = 0.229333 (at node 128)
Injected packet length average = 17.9871
Accepted packet length average = 17.9848
Total in-flight flits = 17683 (17683 measured)
latency change    = 0.0801492
throughput change = 0.000499389
Class 0:
Packet latency average = 5649.04
	minimum = 2166
	maximum = 9104
Network latency average = 527.33
	minimum = 23
	maximum = 2839
Slowest packet = 6274
Flit latency average = 487.627
	minimum = 6
	maximum = 2746
Slowest flit = 212184
Fragmentation average = 57.8086
	minimum = 0
	maximum = 142
Injected packet rate average = 0.00895685
	minimum = 0.005 (at node 85)
	maximum = 0.0134286 (at node 191)
Accepted packet rate average = 0.00896652
	minimum = 0.00628571 (at node 36)
	maximum = 0.0122857 (at node 129)
Injected flit rate average = 0.161143
	minimum = 0.09 (at node 99)
	maximum = 0.239857 (at node 191)
Accepted flit rate average= 0.161296
	minimum = 0.113857 (at node 36)
	maximum = 0.220714 (at node 128)
Injected packet length average = 17.991
Accepted packet length average = 17.9887
Total in-flight flits = 17409 (17409 measured)
latency change    = 0.0724117
throughput change = 0.00438536
Draining all recorded packets ...
Class 0:
Remaining flits: 257771 257772 257773 257774 257775 257776 257777 287694 287695 287696 [...] (17609 flits)
Measured flits: 257771 257772 257773 257774 257775 257776 257777 287694 287695 287696 [...] (17609 flits)
Class 0:
Remaining flits: 320238 320239 320240 320241 320242 320243 320244 320245 320246 320247 [...] (17713 flits)
Measured flits: 320238 320239 320240 320241 320242 320243 320244 320245 320246 320247 [...] (17713 flits)
Class 0:
Remaining flits: 338724 338725 338726 338727 338728 338729 338730 338731 338732 338733 [...] (17562 flits)
Measured flits: 338724 338725 338726 338727 338728 338729 338730 338731 338732 338733 [...] (17562 flits)
Class 0:
Remaining flits: 359405 378990 378991 378992 378993 378994 378995 378996 378997 378998 [...] (17659 flits)
Measured flits: 359405 378990 378991 378992 378993 378994 378995 378996 378997 378998 [...] (17659 flits)
Class 0:
Remaining flits: 414918 414919 414920 414921 414922 414923 414924 414925 414926 414927 [...] (17622 flits)
Measured flits: 414918 414919 414920 414921 414922 414923 414924 414925 414926 414927 [...] (17622 flits)
Class 0:
Remaining flits: 442422 442423 442424 442425 442426 442427 442428 442429 442430 442431 [...] (18112 flits)
Measured flits: 442422 442423 442424 442425 442426 442427 442428 442429 442430 442431 [...] (18112 flits)
Class 0:
Remaining flits: 464349 464350 464351 464352 464353 464354 464355 464356 464357 464358 [...] (17762 flits)
Measured flits: 464349 464350 464351 464352 464353 464354 464355 464356 464357 464358 [...] (17762 flits)
Class 0:
Remaining flits: 481168 481169 481170 481171 481172 481173 481174 481175 496062 496063 [...] (17681 flits)
Measured flits: 481168 481169 481170 481171 481172 481173 481174 481175 496062 496063 [...] (17681 flits)
Class 0:
Remaining flits: 534538 534539 534540 534541 534542 534543 534544 534545 535386 535387 [...] (17892 flits)
Measured flits: 534538 534539 534540 534541 534542 534543 534544 534545 535386 535387 [...] (17892 flits)
Class 0:
Remaining flits: 556488 556489 556490 556491 556492 556493 556494 556495 556496 556497 [...] (17660 flits)
Measured flits: 556488 556489 556490 556491 556492 556493 556494 556495 556496 556497 [...] (17660 flits)
Class 0:
Remaining flits: 593028 593029 593030 593031 593032 593033 593034 593035 593036 593037 [...] (17968 flits)
Measured flits: 593028 593029 593030 593031 593032 593033 593034 593035 593036 593037 [...] (17968 flits)
Class 0:
Remaining flits: 624132 624133 624134 624135 624136 624137 624138 624139 624140 624141 [...] (17547 flits)
Measured flits: 624132 624133 624134 624135 624136 624137 624138 624139 624140 624141 [...] (17547 flits)
Class 0:
Remaining flits: 631026 631027 631028 631029 631030 631031 631032 631033 631034 631035 [...] (17816 flits)
Measured flits: 631026 631027 631028 631029 631030 631031 631032 631033 631034 631035 [...] (17816 flits)
Class 0:
Remaining flits: 694674 694675 694676 694677 694678 694679 694680 694681 694682 694683 [...] (17818 flits)
Measured flits: 694674 694675 694676 694677 694678 694679 694680 694681 694682 694683 [...] (17818 flits)
Class 0:
Remaining flits: 718722 718723 718724 718725 718726 718727 718728 718729 718730 718731 [...] (17789 flits)
Measured flits: 718722 718723 718724 718725 718726 718727 718728 718729 718730 718731 [...] (17789 flits)
Class 0:
Remaining flits: 719874 719875 719876 719877 719878 719879 719880 719881 719882 719883 [...] (17859 flits)
Measured flits: 719874 719875 719876 719877 719878 719879 719880 719881 719882 719883 [...] (17859 flits)
Class 0:
Remaining flits: 769176 769177 769178 769179 769180 769181 769182 769183 769184 769185 [...] (17841 flits)
Measured flits: 769176 769177 769178 769179 769180 769181 769182 769183 769184 769185 [...] (17841 flits)
Class 0:
Remaining flits: 814212 814213 814214 814215 814216 814217 814218 814219 814220 814221 [...] (18020 flits)
Measured flits: 814212 814213 814214 814215 814216 814217 814218 814219 814220 814221 [...] (18020 flits)
Class 0:
Remaining flits: 859428 859429 859430 859431 859432 859433 859434 859435 859436 859437 [...] (17695 flits)
Measured flits: 859428 859429 859430 859431 859432 859433 859434 859435 859436 859437 [...] (17695 flits)
Class 0:
Remaining flits: 873486 873487 873488 873489 873490 873491 873492 873493 873494 873495 [...] (17693 flits)
Measured flits: 873486 873487 873488 873489 873490 873491 873492 873493 873494 873495 [...] (17693 flits)
Class 0:
Remaining flits: 904632 904633 904634 904635 904636 904637 904638 904639 904640 904641 [...] (17578 flits)
Measured flits: 904632 904633 904634 904635 904636 904637 904638 904639 904640 904641 [...] (17578 flits)
Class 0:
Remaining flits: 937728 937729 937730 937731 937732 937733 937734 937735 937736 937737 [...] (17472 flits)
Measured flits: 937728 937729 937730 937731 937732 937733 937734 937735 937736 937737 [...] (17472 flits)
Class 0:
Remaining flits: 979596 979597 979598 979599 979600 979601 979602 979603 979604 979605 [...] (17943 flits)
Measured flits: 979596 979597 979598 979599 979600 979601 979602 979603 979604 979605 [...] (17943 flits)
Class 0:
Remaining flits: 995760 995761 995762 995763 995764 995765 995766 995767 995768 995769 [...] (17562 flits)
Measured flits: 995760 995761 995762 995763 995764 995765 995766 995767 995768 995769 [...] (17562 flits)
Class 0:
Remaining flits: 999594 999595 999596 999597 999598 999599 999600 999601 999602 999603 [...] (17958 flits)
Measured flits: 999594 999595 999596 999597 999598 999599 999600 999601 999602 999603 [...] (17958 flits)
Class 0:
Remaining flits: 1052292 1052293 1052294 1052295 1052296 1052297 1065474 1065475 1065476 1065477 [...] (17780 flits)
Measured flits: 1052292 1052293 1052294 1052295 1052296 1052297 1065474 1065475 1065476 1065477 [...] (17780 flits)
Class 0:
Remaining flits: 1095345 1095346 1095347 1095348 1095349 1095350 1095351 1095352 1095353 1095678 [...] (17609 flits)
Measured flits: 1095345 1095346 1095347 1095348 1095349 1095350 1095351 1095352 1095353 1095678 [...] (17609 flits)
Class 0:
Remaining flits: 1121201 1122354 1122355 1122356 1122357 1122358 1122359 1122360 1122361 1122362 [...] (17512 flits)
Measured flits: 1121201 1122354 1122355 1122356 1122357 1122358 1122359 1122360 1122361 1122362 [...] (17512 flits)
Class 0:
Remaining flits: 1146744 1146745 1146746 1146747 1146748 1146749 1146750 1146751 1146752 1146753 [...] (17701 flits)
Measured flits: 1146744 1146745 1146746 1146747 1146748 1146749 1146750 1146751 1146752 1146753 [...] (17701 flits)
Class 0:
Remaining flits: 1185408 1185409 1185410 1185411 1185412 1185413 1185414 1185415 1185416 1185417 [...] (17827 flits)
Measured flits: 1185408 1185409 1185410 1185411 1185412 1185413 1185414 1185415 1185416 1185417 [...] (17827 flits)
Class 0:
Remaining flits: 1220886 1220887 1220888 1220889 1220890 1220891 1220892 1220893 1220894 1220895 [...] (17446 flits)
Measured flits: 1220886 1220887 1220888 1220889 1220890 1220891 1220892 1220893 1220894 1220895 [...] (17446 flits)
Class 0:
Remaining flits: 1231704 1231705 1231706 1231707 1231708 1231709 1231710 1231711 1231712 1231713 [...] (18090 flits)
Measured flits: 1231704 1231705 1231706 1231707 1231708 1231709 1231710 1231711 1231712 1231713 [...] (18090 flits)
Class 0:
Remaining flits: 1282752 1282753 1282754 1282755 1282756 1282757 1282758 1282759 1282760 1282761 [...] (17702 flits)
Measured flits: 1282752 1282753 1282754 1282755 1282756 1282757 1282758 1282759 1282760 1282761 [...] (17702 flits)
Class 0:
Remaining flits: 1308615 1308616 1308617 1315998 1315999 1316000 1316001 1316002 1316003 1316004 [...] (17539 flits)
Measured flits: 1308615 1308616 1308617 1315998 1315999 1316000 1316001 1316002 1316003 1316004 [...] (17539 flits)
Class 0:
Remaining flits: 1349100 1349101 1349102 1349103 1349104 1349105 1349106 1349107 1349108 1349109 [...] (17847 flits)
Measured flits: 1349100 1349101 1349102 1349103 1349104 1349105 1349106 1349107 1349108 1349109 [...] (17847 flits)
Class 0:
Remaining flits: 1349856 1349857 1349858 1349859 1349860 1349861 1349862 1349863 1349864 1349865 [...] (17379 flits)
Measured flits: 1349856 1349857 1349858 1349859 1349860 1349861 1349862 1349863 1349864 1349865 [...] (17379 flits)
Class 0:
Remaining flits: 1410030 1410031 1410032 1410033 1410034 1410035 1410036 1410037 1410038 1410039 [...] (17254 flits)
Measured flits: 1410030 1410031 1410032 1410033 1410034 1410035 1410036 1410037 1410038 1410039 [...] (17254 flits)
Class 0:
Remaining flits: 1443204 1443205 1443206 1443207 1443208 1443209 1443210 1443211 1443212 1443213 [...] (17729 flits)
Measured flits: 1443204 1443205 1443206 1443207 1443208 1443209 1443210 1443211 1443212 1443213 [...] (17729 flits)
Class 0:
Remaining flits: 1473876 1473877 1473878 1473879 1473880 1473881 1473882 1473883 1473884 1473885 [...] (17518 flits)
Measured flits: 1473876 1473877 1473878 1473879 1473880 1473881 1473882 1473883 1473884 1473885 [...] (17518 flits)
Class 0:
Remaining flits: 1477079 1491768 1491769 1491770 1491771 1491772 1491773 1491774 1491775 1491776 [...] (17922 flits)
Measured flits: 1477079 1491768 1491769 1491770 1491771 1491772 1491773 1491774 1491775 1491776 [...] (17904 flits)
Class 0:
Remaining flits: 1534446 1534447 1534448 1534449 1534450 1534451 1534452 1534453 1534454 1534455 [...] (17528 flits)
Measured flits: 1534446 1534447 1534448 1534449 1534450 1534451 1534452 1534453 1534454 1534455 [...] (17399 flits)
Class 0:
Remaining flits: 1558566 1558567 1558568 1558569 1558570 1558571 1558572 1558573 1558574 1558575 [...] (17845 flits)
Measured flits: 1558566 1558567 1558568 1558569 1558570 1558571 1558572 1558573 1558574 1558575 [...] (17628 flits)
Class 0:
Remaining flits: 1568160 1568161 1568162 1568163 1568164 1568165 1568166 1568167 1568168 1568169 [...] (17721 flits)
Measured flits: 1568160 1568161 1568162 1568163 1568164 1568165 1568166 1568167 1568168 1568169 [...] (17241 flits)
Class 0:
Remaining flits: 1609452 1609453 1609454 1609455 1609456 1609457 1609458 1609459 1609460 1609461 [...] (17565 flits)
Measured flits: 1609452 1609453 1609454 1609455 1609456 1609457 1609458 1609459 1609460 1609461 [...] (16941 flits)
Class 0:
Remaining flits: 1652562 1652563 1652564 1652565 1652566 1652567 1652568 1652569 1652570 1652571 [...] (17803 flits)
Measured flits: 1652562 1652563 1652564 1652565 1652566 1652567 1652568 1652569 1652570 1652571 [...] (16797 flits)
Class 0:
Remaining flits: 1671354 1671355 1671356 1671357 1671358 1671359 1671360 1671361 1671362 1671363 [...] (18343 flits)
Measured flits: 1671354 1671355 1671356 1671357 1671358 1671359 1671360 1671361 1671362 1671363 [...] (16429 flits)
Class 0:
Remaining flits: 1677492 1677493 1677494 1677495 1677496 1677497 1677498 1677499 1677500 1677501 [...] (17397 flits)
Measured flits: 1677492 1677493 1677494 1677495 1677496 1677497 1677498 1677499 1677500 1677501 [...] (14912 flits)
Class 0:
Remaining flits: 1742400 1742401 1742402 1742403 1742404 1742405 1742406 1742407 1742408 1742409 [...] (17981 flits)
Measured flits: 1742400 1742401 1742402 1742403 1742404 1742405 1742406 1742407 1742408 1742409 [...] (14439 flits)
Class 0:
Remaining flits: 1772946 1772947 1772948 1772949 1772950 1772951 1772952 1772953 1772954 1772955 [...] (17753 flits)
Measured flits: 1772946 1772947 1772948 1772949 1772950 1772951 1772952 1772953 1772954 1772955 [...] (13223 flits)
Class 0:
Remaining flits: 1804176 1804177 1804178 1804179 1804180 1804181 1804182 1804183 1804184 1804185 [...] (17585 flits)
Measured flits: 1821168 1821169 1821170 1821171 1821172 1821173 1821174 1821175 1821176 1821177 [...] (11694 flits)
Class 0:
Remaining flits: 1828656 1828657 1828658 1828659 1828660 1828661 1828662 1828663 1828664 1828665 [...] (17320 flits)
Measured flits: 1845342 1845343 1845344 1845345 1845346 1845347 1845348 1845349 1845350 1845351 [...] (10678 flits)
Class 0:
Remaining flits: 1859334 1859335 1859336 1859337 1859338 1859339 1859340 1859341 1859342 1859343 [...] (17576 flits)
Measured flits: 1859334 1859335 1859336 1859337 1859338 1859339 1859340 1859341 1859342 1859343 [...] (8871 flits)
Class 0:
Remaining flits: 1880514 1880515 1880516 1880517 1880518 1880519 1880520 1880521 1880522 1880523 [...] (17919 flits)
Measured flits: 1880514 1880515 1880516 1880517 1880518 1880519 1880520 1880521 1880522 1880523 [...] (7546 flits)
Class 0:
Remaining flits: 1922562 1922563 1922564 1922565 1922566 1922567 1922568 1922569 1922570 1922571 [...] (17919 flits)
Measured flits: 1948338 1948339 1948340 1948341 1948342 1948343 1948344 1948345 1948346 1948347 [...] (6052 flits)
Class 0:
Remaining flits: 1957158 1957159 1957160 1957161 1957162 1957163 1957164 1957165 1957166 1957167 [...] (17921 flits)
Measured flits: 1957158 1957159 1957160 1957161 1957162 1957163 1957164 1957165 1957166 1957167 [...] (4528 flits)
Class 0:
Remaining flits: 2004580 2004581 2004582 2004583 2004584 2004585 2004586 2004587 2006261 2007396 [...] (17942 flits)
Measured flits: 2011860 2011861 2011862 2011863 2011864 2011865 2011866 2011867 2011868 2011869 [...] (3604 flits)
Class 0:
Remaining flits: 2030904 2030905 2030906 2030907 2030908 2030909 2030910 2030911 2030912 2030913 [...] (17336 flits)
Measured flits: 2030904 2030905 2030906 2030907 2030908 2030909 2030910 2030911 2030912 2030913 [...] (2610 flits)
Class 0:
Remaining flits: 2042460 2042461 2042462 2042463 2042464 2042465 2042466 2042467 2042468 2042469 [...] (17650 flits)
Measured flits: 2049318 2049319 2049320 2049321 2049322 2049323 2049324 2049325 2049326 2049327 [...] (2244 flits)
Class 0:
Remaining flits: 2062656 2062657 2062658 2062659 2062660 2062661 2062662 2062663 2062664 2062665 [...] (17382 flits)
Measured flits: 2087622 2087623 2087624 2087625 2087626 2087627 2087628 2087629 2087630 2087631 [...] (1432 flits)
Class 0:
Remaining flits: 2105020 2105021 2105022 2105023 2105024 2105025 2105026 2105027 2118924 2118925 [...] (18200 flits)
Measured flits: 2127222 2127223 2127224 2127225 2127226 2127227 2127228 2127229 2127230 2127231 [...] (841 flits)
Class 0:
Remaining flits: 2153888 2153889 2153890 2153891 2153892 2153893 2153894 2153895 2153896 2153897 [...] (17799 flits)
Measured flits: 2167956 2167957 2167958 2167959 2167960 2167961 2167962 2167963 2167964 2167965 [...] (645 flits)
Class 0:
Remaining flits: 2175462 2175463 2175464 2175465 2175466 2175467 2175468 2175469 2175470 2175471 [...] (17564 flits)
Measured flits: 2198133 2198134 2198135 2198136 2198137 2198138 2198139 2198140 2198141 2208743 [...] (429 flits)
Class 0:
Remaining flits: 2199330 2199331 2199332 2199333 2199334 2199335 2199336 2199337 2199338 2199339 [...] (17386 flits)
Measured flits: 2246256 2246257 2246258 2246259 2246260 2246261 2246262 2246263 2246264 2246265 [...] (319 flits)
Class 0:
Remaining flits: 2226204 2226205 2226206 2226207 2226208 2226209 2226210 2226211 2226212 2226213 [...] (17444 flits)
Measured flits: 2275308 2275309 2275310 2275311 2275312 2275313 2275314 2275315 2275316 2275317 [...] (144 flits)
Class 0:
Remaining flits: 2261160 2261161 2261162 2261163 2261164 2261165 2261166 2261167 2261168 2261169 [...] (17801 flits)
Measured flits: 2307096 2307097 2307098 2307099 2307100 2307101 2307102 2307103 2307104 2307105 [...] (90 flits)
Class 0:
Remaining flits: 2298218 2298219 2298220 2298221 2298295 2298296 2298297 2298298 2298299 2298300 [...] (17729 flits)
Measured flits: 2321154 2321155 2321156 2321157 2321158 2321159 2321160 2321161 2321162 2321163 [...] (108 flits)
Class 0:
Remaining flits: 2326040 2326041 2326042 2326043 2326044 2326045 2326046 2326047 2326048 2326049 [...] (17885 flits)
Measured flits: 2379807 2379808 2379809 2379810 2379811 2379812 2379813 2379814 2379815 2380349 [...] (16 flits)
Draining remaining packets ...
Time taken is 77926 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 27822.5 (1 samples)
	minimum = 2166 (1 samples)
	maximum = 67078 (1 samples)
Network latency average = 551.47 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3482 (1 samples)
Flit latency average = 488.967 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3402 (1 samples)
Fragmentation average = 58.5229 (1 samples)
	minimum = 0 (1 samples)
	maximum = 181 (1 samples)
Injected packet rate average = 0.00895685 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0134286 (1 samples)
Accepted packet rate average = 0.00896652 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.161143 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.239857 (1 samples)
Accepted flit rate average = 0.161296 (1 samples)
	minimum = 0.113857 (1 samples)
	maximum = 0.220714 (1 samples)
Injected packet size average = 17.991 (1 samples)
Accepted packet size average = 17.9887 (1 samples)
Hops average = 5.07118 (1 samples)
Total run time 53.1424
