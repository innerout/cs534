BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 357.727
	minimum = 22
	maximum = 986
Network latency average = 246.72
	minimum = 22
	maximum = 805
Slowest packet = 46
Flit latency average = 211.032
	minimum = 5
	maximum = 874
Slowest flit = 9645
Fragmentation average = 66.9383
	minimum = 0
	maximum = 541
Injected packet rate average = 0.0279375
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.0135
	minimum = 0.007 (at node 23)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.497656
	minimum = 0 (at node 17)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.259464
	minimum = 0.135 (at node 23)
	maximum = 0.422 (at node 44)
Injected packet length average = 17.8132
Accepted packet length average = 19.2195
Total in-flight flits = 46789 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 642.145
	minimum = 22
	maximum = 1945
Network latency average = 465.155
	minimum = 22
	maximum = 1686
Slowest packet = 46
Flit latency average = 420.737
	minimum = 5
	maximum = 1669
Slowest flit = 20033
Fragmentation average = 89.9476
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0262578
	minimum = 0.0035 (at node 52)
	maximum = 0.0535 (at node 53)
Accepted packet rate average = 0.0143125
	minimum = 0.008 (at node 30)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.470083
	minimum = 0.063 (at node 52)
	maximum = 0.96 (at node 53)
Accepted flit rate average= 0.267435
	minimum = 0.1545 (at node 83)
	maximum = 0.3815 (at node 22)
Injected packet length average = 17.9026
Accepted packet length average = 18.6854
Total in-flight flits = 79195 (0 measured)
latency change    = 0.442919
throughput change = 0.0298067
Class 0:
Packet latency average = 1446.05
	minimum = 25
	maximum = 2903
Network latency average = 1101.55
	minimum = 22
	maximum = 2270
Slowest packet = 2851
Flit latency average = 1051.21
	minimum = 5
	maximum = 2463
Slowest flit = 45913
Fragmentation average = 119.006
	minimum = 0
	maximum = 748
Injected packet rate average = 0.0186771
	minimum = 0 (at node 73)
	maximum = 0.042 (at node 27)
Accepted packet rate average = 0.0150417
	minimum = 0.005 (at node 4)
	maximum = 0.031 (at node 34)
Injected flit rate average = 0.336432
	minimum = 0 (at node 73)
	maximum = 0.765 (at node 27)
Accepted flit rate average= 0.271286
	minimum = 0.097 (at node 4)
	maximum = 0.562 (at node 34)
Injected packet length average = 18.0131
Accepted packet length average = 18.0357
Total in-flight flits = 92016 (0 measured)
latency change    = 0.555932
throughput change = 0.0141974
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1208.15
	minimum = 33
	maximum = 3567
Network latency average = 103.85
	minimum = 22
	maximum = 995
Slowest packet = 13722
Flit latency average = 1397.49
	minimum = 5
	maximum = 2972
Slowest flit = 97271
Fragmentation average = 10.8854
	minimum = 0
	maximum = 243
Injected packet rate average = 0.0171563
	minimum = 0 (at node 57)
	maximum = 0.034 (at node 74)
Accepted packet rate average = 0.0153021
	minimum = 0.006 (at node 105)
	maximum = 0.026 (at node 63)
Injected flit rate average = 0.307729
	minimum = 0 (at node 57)
	maximum = 0.613 (at node 74)
Accepted flit rate average= 0.274849
	minimum = 0.09 (at node 171)
	maximum = 0.444 (at node 63)
Injected packet length average = 17.9369
Accepted packet length average = 17.9615
Total in-flight flits = 98321 (53296 measured)
latency change    = 0.196911
throughput change = 0.0129617
Class 0:
Packet latency average = 1937.79
	minimum = 28
	maximum = 4543
Network latency average = 727.046
	minimum = 22
	maximum = 1983
Slowest packet = 13722
Flit latency average = 1526.93
	minimum = 5
	maximum = 3760
Slowest flit = 116736
Fragmentation average = 56.8476
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0166042
	minimum = 0.006 (at node 180)
	maximum = 0.028 (at node 74)
Accepted packet rate average = 0.0151667
	minimum = 0.0095 (at node 36)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.298125
	minimum = 0.114 (at node 180)
	maximum = 0.503 (at node 74)
Accepted flit rate average= 0.272555
	minimum = 0.1655 (at node 132)
	maximum = 0.376 (at node 165)
Injected packet length average = 17.9548
Accepted packet length average = 17.9706
Total in-flight flits = 101979 (91205 measured)
latency change    = 0.37653
throughput change = 0.00841765
Class 0:
Packet latency average = 2638.9
	minimum = 28
	maximum = 5624
Network latency average = 1319.32
	minimum = 22
	maximum = 2958
Slowest packet = 13722
Flit latency average = 1614.76
	minimum = 5
	maximum = 4429
Slowest flit = 133973
Fragmentation average = 87.562
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0162604
	minimum = 0.005 (at node 180)
	maximum = 0.0256667 (at node 179)
Accepted packet rate average = 0.0151181
	minimum = 0.00966667 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.292429
	minimum = 0.094 (at node 180)
	maximum = 0.458 (at node 179)
Accepted flit rate average= 0.271542
	minimum = 0.183667 (at node 36)
	maximum = 0.359333 (at node 165)
Injected packet length average = 17.9841
Accepted packet length average = 17.9614
Total in-flight flits = 104142 (103375 measured)
latency change    = 0.265682
throughput change = 0.00373063
Class 0:
Packet latency average = 3111.33
	minimum = 28
	maximum = 6517
Network latency average = 1603.46
	minimum = 22
	maximum = 3764
Slowest packet = 13722
Flit latency average = 1685.69
	minimum = 5
	maximum = 4429
Slowest flit = 133973
Fragmentation average = 98.1347
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0160208
	minimum = 0.00725 (at node 180)
	maximum = 0.02275 (at node 79)
Accepted packet rate average = 0.0150143
	minimum = 0.01025 (at node 64)
	maximum = 0.01925 (at node 128)
Injected flit rate average = 0.288177
	minimum = 0.1335 (at node 180)
	maximum = 0.406 (at node 79)
Accepted flit rate average= 0.270007
	minimum = 0.1845 (at node 64)
	maximum = 0.34475 (at node 165)
Injected packet length average = 17.9876
Accepted packet length average = 17.9833
Total in-flight flits = 106015 (106014 measured)
latency change    = 0.151843
throughput change = 0.00568563
Class 0:
Packet latency average = 3470.74
	minimum = 28
	maximum = 7362
Network latency average = 1743.83
	minimum = 22
	maximum = 4316
Slowest packet = 13722
Flit latency average = 1739.98
	minimum = 5
	maximum = 4429
Slowest flit = 133973
Fragmentation average = 103.386
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0157937
	minimum = 0.0084 (at node 180)
	maximum = 0.021 (at node 29)
Accepted packet rate average = 0.0149979
	minimum = 0.0112 (at node 64)
	maximum = 0.0188 (at node 118)
Injected flit rate average = 0.284041
	minimum = 0.1534 (at node 180)
	maximum = 0.375 (at node 29)
Accepted flit rate average= 0.269755
	minimum = 0.2038 (at node 64)
	maximum = 0.3402 (at node 118)
Injected packet length average = 17.9844
Accepted packet length average = 17.9862
Total in-flight flits = 105715 (105715 measured)
latency change    = 0.103554
throughput change = 0.000931593
Class 0:
Packet latency average = 3801.78
	minimum = 28
	maximum = 7989
Network latency average = 1820.32
	minimum = 22
	maximum = 4833
Slowest packet = 13722
Flit latency average = 1779.59
	minimum = 5
	maximum = 4786
Slowest flit = 286660
Fragmentation average = 106.528
	minimum = 0
	maximum = 690
Injected packet rate average = 0.015671
	minimum = 0.00833333 (at node 180)
	maximum = 0.0208333 (at node 79)
Accepted packet rate average = 0.0149861
	minimum = 0.0118333 (at node 79)
	maximum = 0.0183333 (at node 90)
Injected flit rate average = 0.282019
	minimum = 0.152 (at node 180)
	maximum = 0.372667 (at node 79)
Accepted flit rate average= 0.269839
	minimum = 0.210667 (at node 171)
	maximum = 0.329167 (at node 138)
Injected packet length average = 17.9962
Accepted packet length average = 18.006
Total in-flight flits = 106169 (106169 measured)
latency change    = 0.087075
throughput change = 0.000312043
Class 0:
Packet latency average = 4123.99
	minimum = 28
	maximum = 9058
Network latency average = 1876.77
	minimum = 22
	maximum = 5656
Slowest packet = 13722
Flit latency average = 1812.69
	minimum = 5
	maximum = 5639
Slowest flit = 286595
Fragmentation average = 108.666
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0155432
	minimum = 0.00814286 (at node 180)
	maximum = 0.0207143 (at node 79)
Accepted packet rate average = 0.0150156
	minimum = 0.012 (at node 79)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.279699
	minimum = 0.148286 (at node 180)
	maximum = 0.371429 (at node 79)
Accepted flit rate average= 0.269932
	minimum = 0.210571 (at node 171)
	maximum = 0.340143 (at node 70)
Injected packet length average = 17.995
Accepted packet length average = 17.9768
Total in-flight flits = 105337 (105337 measured)
latency change    = 0.0781305
throughput change = 0.000344093
Draining all recorded packets ...
Class 0:
Remaining flits: 438336 438337 438338 438339 438340 438341 438342 438343 438344 438345 [...] (105109 flits)
Measured flits: 438336 438337 438338 438339 438340 438341 438342 438343 438344 438345 [...] (105109 flits)
Class 0:
Remaining flits: 444870 444871 444872 444873 444874 444875 444876 444877 444878 444879 [...] (105298 flits)
Measured flits: 444870 444871 444872 444873 444874 444875 444876 444877 444878 444879 [...] (105064 flits)
Class 0:
Remaining flits: 508608 508609 508610 508611 508612 508613 508614 508615 508616 508617 [...] (104986 flits)
Measured flits: 508608 508609 508610 508611 508612 508613 508614 508615 508616 508617 [...] (103605 flits)
Class 0:
Remaining flits: 538776 538777 538778 538779 538780 538781 538782 538783 538784 538785 [...] (103843 flits)
Measured flits: 538776 538777 538778 538779 538780 538781 538782 538783 538784 538785 [...] (101343 flits)
Class 0:
Remaining flits: 574290 574291 574292 574293 574294 574295 574296 574297 574298 574299 [...] (105664 flits)
Measured flits: 574290 574291 574292 574293 574294 574295 574296 574297 574298 574299 [...] (100260 flits)
Class 0:
Remaining flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (104340 flits)
Measured flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (93745 flits)
Class 0:
Remaining flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (106310 flits)
Measured flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (89058 flits)
Class 0:
Remaining flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (104903 flits)
Measured flits: 638118 638119 638120 638121 638122 638123 638124 638125 638126 638127 [...] (82746 flits)
Class 0:
Remaining flits: 638133 638134 638135 761922 761923 761924 761925 761926 761927 761928 [...] (104339 flits)
Measured flits: 638133 638134 638135 761922 761923 761924 761925 761926 761927 761928 [...] (78606 flits)
Class 0:
Remaining flits: 761922 761923 761924 761925 761926 761927 761928 761929 761930 761931 [...] (104040 flits)
Measured flits: 761922 761923 761924 761925 761926 761927 761928 761929 761930 761931 [...] (72522 flits)
Class 0:
Remaining flits: 869130 869131 869132 869133 869134 869135 869136 869137 869138 869139 [...] (103271 flits)
Measured flits: 869130 869131 869132 869133 869134 869135 869136 869137 869138 869139 [...] (64344 flits)
Class 0:
Remaining flits: 869130 869131 869132 869133 869134 869135 869136 869137 869138 869139 [...] (104337 flits)
Measured flits: 869130 869131 869132 869133 869134 869135 869136 869137 869138 869139 [...] (56900 flits)
Class 0:
Remaining flits: 1002744 1002745 1002746 1002747 1002748 1002749 1002750 1002751 1002752 1002753 [...] (104534 flits)
Measured flits: 1002744 1002745 1002746 1002747 1002748 1002749 1002750 1002751 1002752 1002753 [...] (48499 flits)
Class 0:
Remaining flits: 1002744 1002745 1002746 1002747 1002748 1002749 1002750 1002751 1002752 1002753 [...] (104263 flits)
Measured flits: 1002744 1002745 1002746 1002747 1002748 1002749 1002750 1002751 1002752 1002753 [...] (40651 flits)
Class 0:
Remaining flits: 1016496 1016497 1016498 1016499 1016500 1016501 1016502 1016503 1016504 1016505 [...] (104116 flits)
Measured flits: 1016496 1016497 1016498 1016499 1016500 1016501 1016502 1016503 1016504 1016505 [...] (32663 flits)
Class 0:
Remaining flits: 1016505 1016506 1016507 1016508 1016509 1016510 1016511 1016512 1016513 1102734 [...] (103762 flits)
Measured flits: 1016505 1016506 1016507 1016508 1016509 1016510 1016511 1016512 1016513 1102734 [...] (27303 flits)
Class 0:
Remaining flits: 1152000 1152001 1152002 1152003 1152004 1152005 1152006 1152007 1152008 1152009 [...] (103508 flits)
Measured flits: 1152000 1152001 1152002 1152003 1152004 1152005 1152006 1152007 1152008 1152009 [...] (21577 flits)
Class 0:
Remaining flits: 1225224 1225225 1225226 1225227 1225228 1225229 1225230 1225231 1225232 1225233 [...] (102070 flits)
Measured flits: 1225224 1225225 1225226 1225227 1225228 1225229 1225230 1225231 1225232 1225233 [...] (17475 flits)
Class 0:
Remaining flits: 1226607 1226608 1226609 1244268 1244269 1244270 1244271 1244272 1244273 1244274 [...] (103757 flits)
Measured flits: 1278144 1278145 1278146 1278147 1278148 1278149 1278150 1278151 1278152 1278153 [...] (14393 flits)
Class 0:
Remaining flits: 1259082 1259083 1259084 1259085 1259086 1259087 1259088 1259089 1259090 1259091 [...] (103295 flits)
Measured flits: 1278148 1278149 1278150 1278151 1278152 1278153 1278154 1278155 1278156 1278157 [...] (11969 flits)
Class 0:
Remaining flits: 1326539 1326540 1326541 1326542 1326543 1326544 1326545 1355976 1355977 1355978 [...] (104280 flits)
Measured flits: 1362210 1362211 1362212 1362213 1362214 1362215 1362216 1362217 1362218 1362219 [...] (9539 flits)
Class 0:
Remaining flits: 1422198 1422199 1422200 1422201 1422202 1422203 1422204 1422205 1422206 1422207 [...] (104482 flits)
Measured flits: 1425236 1425237 1425238 1425239 1516769 1552483 1552484 1552485 1552486 1552487 [...] (7726 flits)
Class 0:
Remaining flits: 1470816 1470817 1470818 1470819 1470820 1470821 1470822 1470823 1470824 1470825 [...] (103863 flits)
Measured flits: 1566382 1566383 1566384 1566385 1566386 1566387 1566388 1566389 1566390 1566391 [...] (6393 flits)
Class 0:
Remaining flits: 1547747 1557540 1557541 1557542 1557543 1557544 1557545 1557546 1557547 1557548 [...] (103269 flits)
Measured flits: 1608930 1608931 1608932 1608933 1608934 1608935 1608936 1608937 1608938 1608939 [...] (5035 flits)
Class 0:
Remaining flits: 1583154 1583155 1583156 1583157 1583158 1583159 1583160 1583161 1583162 1583163 [...] (103030 flits)
Measured flits: 1638568 1638569 1638570 1638571 1638572 1638573 1638574 1638575 1710792 1710793 [...] (5322 flits)
Class 0:
Remaining flits: 1583159 1583160 1583161 1583162 1583163 1583164 1583165 1583166 1583167 1583168 [...] (100726 flits)
Measured flits: 1713978 1713979 1713980 1713981 1713982 1713983 1713984 1713985 1713986 1713987 [...] (4429 flits)
Class 0:
Remaining flits: 1685988 1685989 1685990 1685991 1685992 1685993 1685994 1685995 1685996 1685997 [...] (103233 flits)
Measured flits: 1713978 1713979 1713980 1713981 1713982 1713983 1713984 1713985 1713986 1713987 [...] (4179 flits)
Class 0:
Remaining flits: 1685992 1685993 1685994 1685995 1685996 1685997 1685998 1685999 1686000 1686001 [...] (103180 flits)
Measured flits: 1755303 1755304 1755305 1802574 1802575 1802576 1802577 1802578 1802579 1802580 [...] (3404 flits)
Class 0:
Remaining flits: 1787364 1787365 1787366 1787367 1787368 1787369 1787370 1787371 1787372 1787373 [...] (100757 flits)
Measured flits: 1884947 1884948 1884949 1884950 1884951 1884952 1884953 1884954 1884955 1884956 [...] (2214 flits)
Class 0:
Remaining flits: 1823814 1823815 1823816 1823817 1823818 1823819 1823820 1823821 1823822 1823823 [...] (101881 flits)
Measured flits: 1899234 1899235 1899236 1899237 1899238 1899239 1899240 1899241 1899242 1899243 [...] (1464 flits)
Class 0:
Remaining flits: 1823814 1823815 1823816 1823817 1823818 1823819 1823820 1823821 1823822 1823823 [...] (100465 flits)
Measured flits: 1899234 1899235 1899236 1899237 1899238 1899239 1899240 1899241 1899242 1899243 [...] (1587 flits)
Class 0:
Remaining flits: 1850904 1850905 1850906 1850907 1850908 1850909 1850910 1850911 1850912 1850913 [...] (99790 flits)
Measured flits: 1899234 1899235 1899236 1899237 1899238 1899239 1899240 1899241 1899242 1899243 [...] (1284 flits)
Class 0:
Remaining flits: 1875474 1875475 1875476 1875477 1875478 1875479 1875480 1875481 1875482 1875483 [...] (100445 flits)
Measured flits: 1899234 1899235 1899236 1899237 1899238 1899239 1899240 1899241 1899242 1899243 [...] (1142 flits)
Class 0:
Remaining flits: 1875474 1875475 1875476 1875477 1875478 1875479 1875480 1875481 1875482 1875483 [...] (101704 flits)
Measured flits: 2204280 2204281 2204282 2204283 2204284 2204285 2204286 2204287 2204288 2204289 [...] (1093 flits)
Class 0:
Remaining flits: 1875474 1875475 1875476 1875477 1875478 1875479 1875480 1875481 1875482 1875483 [...] (97945 flits)
Measured flits: 2267910 2267911 2267912 2267913 2267914 2267915 2267916 2267917 2267918 2267919 [...] (638 flits)
Class 0:
Remaining flits: 1960368 1960369 1960370 1960371 1960372 1960373 1960374 1960375 1960376 1960377 [...] (97650 flits)
Measured flits: 2328912 2328913 2328914 2328915 2328916 2328917 2328918 2328919 2328920 2328921 [...] (264 flits)
Class 0:
Remaining flits: 2190888 2190889 2190890 2190891 2190892 2190893 2190894 2190895 2190896 2190897 [...] (96805 flits)
Measured flits: 2404548 2404549 2404550 2404551 2404552 2404553 2404554 2404555 2404556 2404557 [...] (210 flits)
Class 0:
Remaining flits: 2190905 2220102 2220103 2220104 2220105 2220106 2220107 2220108 2220109 2220110 [...] (100431 flits)
Measured flits: 2479590 2479591 2479592 2479593 2479594 2479595 2479596 2479597 2479598 2479599 [...] (370 flits)
Class 0:
Remaining flits: 2264112 2264113 2264114 2264115 2264116 2264117 2264118 2264119 2264120 2264121 [...] (100241 flits)
Measured flits: 2492730 2492731 2492732 2492733 2492734 2492735 2492736 2492737 2492738 2492739 [...] (182 flits)
Class 0:
Remaining flits: 2285211 2285212 2285213 2285214 2285215 2285216 2285217 2285218 2285219 2285220 [...] (100067 flits)
Measured flits: 2575512 2575513 2575514 2575515 2575516 2575517 2575518 2575519 2575520 2575521 [...] (55 flits)
Class 0:
Remaining flits: 2321084 2321085 2321086 2321087 2321088 2321089 2321090 2321091 2321092 2321093 [...] (99417 flits)
Measured flits: 2575512 2575513 2575514 2575515 2575516 2575517 2575518 2575519 2575520 2575521 [...] (36 flits)
Class 0:
Remaining flits: 2361042 2361043 2361044 2361045 2361046 2361047 2361048 2361049 2361050 2361051 [...] (98236 flits)
Measured flits: 2575512 2575513 2575514 2575515 2575516 2575517 2575518 2575519 2575520 2575521 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2431476 2431477 2431478 2431479 2431480 2431481 2431482 2431483 2431484 2431485 [...] (51212 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2431489 2431490 2431491 2431492 2431493 2485134 2485135 2485136 2485137 2485138 [...] (15155 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2485134 2485135 2485136 2485137 2485138 2485139 2485140 2485141 2485142 2485143 [...] (1215 flits)
Measured flits: (0 flits)
Time taken is 56030 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9817.29 (1 samples)
	minimum = 28 (1 samples)
	maximum = 42437 (1 samples)
Network latency average = 2025.75 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8689 (1 samples)
Flit latency average = 1944.49 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11248 (1 samples)
Fragmentation average = 129.599 (1 samples)
	minimum = 0 (1 samples)
	maximum = 690 (1 samples)
Injected packet rate average = 0.0155432 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0207143 (1 samples)
Accepted packet rate average = 0.0150156 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.279699 (1 samples)
	minimum = 0.148286 (1 samples)
	maximum = 0.371429 (1 samples)
Accepted flit rate average = 0.269932 (1 samples)
	minimum = 0.210571 (1 samples)
	maximum = 0.340143 (1 samples)
Injected packet size average = 17.995 (1 samples)
Accepted packet size average = 17.9768 (1 samples)
Hops average = 5.05002 (1 samples)
Total run time 111.52
