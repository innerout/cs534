BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 381.702
	minimum = 27
	maximum = 986
Network latency average = 288.55
	minimum = 23
	maximum = 935
Slowest packet = 166
Flit latency average = 214.153
	minimum = 6
	maximum = 927
Slowest flit = 4739
Fragmentation average = 151.54
	minimum = 0
	maximum = 792
Injected packet rate average = 0.0191198
	minimum = 0.001 (at node 159)
	maximum = 0.039 (at node 25)
Accepted packet rate average = 0.00958333
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.337995
	minimum = 0.018 (at node 159)
	maximum = 0.695 (at node 74)
Accepted flit rate average= 0.197245
	minimum = 0.083 (at node 62)
	maximum = 0.341 (at node 131)
Injected packet length average = 17.6777
Accepted packet length average = 20.5821
Total in-flight flits = 28261 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 655.172
	minimum = 27
	maximum = 1975
Network latency average = 462.316
	minimum = 23
	maximum = 1799
Slowest packet = 127
Flit latency average = 372.5
	minimum = 6
	maximum = 1860
Slowest flit = 11236
Fragmentation average = 179.647
	minimum = 0
	maximum = 1718
Injected packet rate average = 0.0156432
	minimum = 0.004 (at node 79)
	maximum = 0.0315 (at node 25)
Accepted packet rate average = 0.0103724
	minimum = 0.0055 (at node 62)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.278471
	minimum = 0.072 (at node 79)
	maximum = 0.559 (at node 25)
Accepted flit rate average= 0.198508
	minimum = 0.105 (at node 62)
	maximum = 0.304 (at node 152)
Injected packet length average = 17.8014
Accepted packet length average = 19.1381
Total in-flight flits = 32169 (0 measured)
latency change    = 0.417403
throughput change = 0.00636257
Class 0:
Packet latency average = 1521.54
	minimum = 29
	maximum = 2953
Network latency average = 861.096
	minimum = 24
	maximum = 2834
Slowest packet = 385
Flit latency average = 728.542
	minimum = 6
	maximum = 2817
Slowest flit = 11249
Fragmentation average = 219.405
	minimum = 0
	maximum = 2317
Injected packet rate average = 0.0120938
	minimum = 0 (at node 26)
	maximum = 0.032 (at node 61)
Accepted packet rate average = 0.0110365
	minimum = 0.004 (at node 143)
	maximum = 0.021 (at node 56)
Injected flit rate average = 0.216292
	minimum = 0 (at node 42)
	maximum = 0.578 (at node 61)
Accepted flit rate average= 0.194661
	minimum = 0.073 (at node 143)
	maximum = 0.357 (at node 56)
Injected packet length average = 17.8846
Accepted packet length average = 17.638
Total in-flight flits = 36680 (0 measured)
latency change    = 0.569401
throughput change = 0.0197592
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1917.85
	minimum = 45
	maximum = 3876
Network latency average = 363.887
	minimum = 26
	maximum = 951
Slowest packet = 8371
Flit latency average = 840.626
	minimum = 6
	maximum = 3484
Slowest flit = 35009
Fragmentation average = 124.68
	minimum = 1
	maximum = 433
Injected packet rate average = 0.0102604
	minimum = 0 (at node 4)
	maximum = 0.033 (at node 138)
Accepted packet rate average = 0.0108021
	minimum = 0.004 (at node 112)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.184958
	minimum = 0 (at node 4)
	maximum = 0.595 (at node 138)
Accepted flit rate average= 0.196339
	minimum = 0.072 (at node 134)
	maximum = 0.389 (at node 16)
Injected packet length average = 18.0264
Accepted packet length average = 18.176
Total in-flight flits = 34479 (22949 measured)
latency change    = 0.206644
throughput change = 0.00854179
Class 0:
Packet latency average = 2374.23
	minimum = 45
	maximum = 4807
Network latency average = 577.341
	minimum = 26
	maximum = 1957
Slowest packet = 8371
Flit latency average = 856.156
	minimum = 6
	maximum = 4589
Slowest flit = 27413
Fragmentation average = 153.238
	minimum = 0
	maximum = 1358
Injected packet rate average = 0.0105755
	minimum = 0 (at node 18)
	maximum = 0.026 (at node 54)
Accepted packet rate average = 0.0107578
	minimum = 0.004 (at node 134)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.190667
	minimum = 0 (at node 18)
	maximum = 0.4695 (at node 54)
Accepted flit rate average= 0.194286
	minimum = 0.0875 (at node 36)
	maximum = 0.349 (at node 16)
Injected packet length average = 18.0291
Accepted packet length average = 18.06
Total in-flight flits = 35244 (31667 measured)
latency change    = 0.192222
throughput change = 0.0105622
Class 0:
Packet latency average = 2811.11
	minimum = 44
	maximum = 5696
Network latency average = 700.097
	minimum = 25
	maximum = 2975
Slowest packet = 8371
Flit latency average = 863.762
	minimum = 6
	maximum = 5238
Slowest flit = 23164
Fragmentation average = 170.494
	minimum = 0
	maximum = 1976
Injected packet rate average = 0.0106701
	minimum = 0 (at node 18)
	maximum = 0.0243333 (at node 138)
Accepted packet rate average = 0.0106979
	minimum = 0.00566667 (at node 36)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.19246
	minimum = 0.005 (at node 18)
	maximum = 0.442 (at node 138)
Accepted flit rate average= 0.193184
	minimum = 0.112 (at node 36)
	maximum = 0.311667 (at node 129)
Injected packet length average = 18.0373
Accepted packet length average = 18.0581
Total in-flight flits = 36142 (35089 measured)
latency change    = 0.155412
throughput change = 0.00570663
Draining remaining packets ...
Class 0:
Remaining flits: 105769 105770 105771 105772 105773 105774 105775 105776 105777 105778 [...] (5853 flits)
Measured flits: 153504 153505 153506 153507 153508 153509 153510 153511 153512 153513 [...] (5744 flits)
Time taken is 7560 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3271.91 (1 samples)
	minimum = 43 (1 samples)
	maximum = 7013 (1 samples)
Network latency average = 947.219 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4213 (1 samples)
Flit latency average = 988.747 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6383 (1 samples)
Fragmentation average = 171.643 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1976 (1 samples)
Injected packet rate average = 0.0106701 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0106979 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.19246 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.442 (1 samples)
Accepted flit rate average = 0.193184 (1 samples)
	minimum = 0.112 (1 samples)
	maximum = 0.311667 (1 samples)
Injected packet size average = 18.0373 (1 samples)
Accepted packet size average = 18.0581 (1 samples)
Hops average = 5.13427 (1 samples)
Total run time 7.18847
