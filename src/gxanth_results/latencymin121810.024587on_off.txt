BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.096
	minimum = 23
	maximum = 980
Network latency average = 226.609
	minimum = 23
	maximum = 895
Slowest packet = 167
Flit latency average = 183.369
	minimum = 6
	maximum = 935
Slowest flit = 5086
Fragmentation average = 50.8121
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0131094
	minimum = 0 (at node 101)
	maximum = 0.029 (at node 117)
Accepted packet rate average = 0.00895313
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.232271
	minimum = 0 (at node 101)
	maximum = 0.522 (at node 117)
Accepted flit rate average= 0.167177
	minimum = 0.054 (at node 41)
	maximum = 0.323 (at node 49)
Injected packet length average = 17.7179
Accepted packet length average = 18.6725
Total in-flight flits = 14396 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 594.471
	minimum = 23
	maximum = 1967
Network latency average = 325.665
	minimum = 23
	maximum = 1842
Slowest packet = 167
Flit latency average = 276.527
	minimum = 6
	maximum = 1825
Slowest flit = 11177
Fragmentation average = 52.92
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0114687
	minimum = 0.0015 (at node 60)
	maximum = 0.023 (at node 49)
Accepted packet rate average = 0.00911458
	minimum = 0.0045 (at node 135)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.204289
	minimum = 0.0205 (at node 60)
	maximum = 0.414 (at node 49)
Accepted flit rate average= 0.167005
	minimum = 0.082 (at node 135)
	maximum = 0.27 (at node 22)
Injected packet length average = 17.8127
Accepted packet length average = 18.3229
Total in-flight flits = 16528 (0 measured)
latency change    = 0.44977
throughput change = 0.00102916
Class 0:
Packet latency average = 1413.12
	minimum = 35
	maximum = 2921
Network latency average = 506.166
	minimum = 23
	maximum = 1951
Slowest packet = 3164
Flit latency average = 444.206
	minimum = 6
	maximum = 2062
Slowest flit = 43884
Fragmentation average = 60.2531
	minimum = 0
	maximum = 133
Injected packet rate average = 0.00907292
	minimum = 0 (at node 9)
	maximum = 0.02 (at node 75)
Accepted packet rate average = 0.00895313
	minimum = 0.001 (at node 121)
	maximum = 0.017 (at node 14)
Injected flit rate average = 0.163932
	minimum = 0 (at node 56)
	maximum = 0.353 (at node 75)
Accepted flit rate average= 0.161583
	minimum = 0.018 (at node 121)
	maximum = 0.317 (at node 187)
Injected packet length average = 18.0683
Accepted packet length average = 18.0477
Total in-flight flits = 17328 (0 measured)
latency change    = 0.57932
throughput change = 0.0335547
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2058.77
	minimum = 40
	maximum = 3854
Network latency average = 340.34
	minimum = 23
	maximum = 940
Slowest packet = 6268
Flit latency average = 483.569
	minimum = 6
	maximum = 2696
Slowest flit = 35477
Fragmentation average = 56.7717
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00885938
	minimum = 0 (at node 2)
	maximum = 0.019 (at node 66)
Accepted packet rate average = 0.00889063
	minimum = 0.003 (at node 22)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.159932
	minimum = 0 (at node 2)
	maximum = 0.359 (at node 66)
Accepted flit rate average= 0.159552
	minimum = 0.054 (at node 117)
	maximum = 0.303 (at node 16)
Injected packet length average = 18.0523
Accepted packet length average = 17.9461
Total in-flight flits = 17492 (15895 measured)
latency change    = 0.313609
throughput change = 0.012731
Class 0:
Packet latency average = 2464.81
	minimum = 40
	maximum = 4594
Network latency average = 451.913
	minimum = 23
	maximum = 1566
Slowest packet = 6268
Flit latency average = 490.737
	minimum = 6
	maximum = 2696
Slowest flit = 35477
Fragmentation average = 58.3655
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00881771
	minimum = 0.0025 (at node 47)
	maximum = 0.017 (at node 62)
Accepted packet rate average = 0.00877344
	minimum = 0.0045 (at node 150)
	maximum = 0.0165 (at node 34)
Injected flit rate average = 0.158773
	minimum = 0.045 (at node 47)
	maximum = 0.3145 (at node 62)
Accepted flit rate average= 0.157539
	minimum = 0.073 (at node 150)
	maximum = 0.2975 (at node 34)
Injected packet length average = 18.0062
Accepted packet length average = 17.9564
Total in-flight flits = 18069 (18069 measured)
latency change    = 0.164734
throughput change = 0.0127779
Class 0:
Packet latency average = 2800.52
	minimum = 40
	maximum = 5800
Network latency average = 495.313
	minimum = 23
	maximum = 2062
Slowest packet = 6268
Flit latency average = 489.976
	minimum = 6
	maximum = 2696
Slowest flit = 35477
Fragmentation average = 59.133
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00885069
	minimum = 0.00366667 (at node 8)
	maximum = 0.0156667 (at node 62)
Accepted packet rate average = 0.00879688
	minimum = 0.00533333 (at node 17)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.159403
	minimum = 0.062 (at node 8)
	maximum = 0.287667 (at node 62)
Accepted flit rate average= 0.158715
	minimum = 0.091 (at node 17)
	maximum = 0.263 (at node 16)
Injected packet length average = 18.0102
Accepted packet length average = 18.0422
Total in-flight flits = 17672 (17672 measured)
latency change    = 0.119874
throughput change = 0.00741085
Class 0:
Packet latency average = 3144.13
	minimum = 40
	maximum = 6746
Network latency average = 512.234
	minimum = 23
	maximum = 2584
Slowest packet = 6268
Flit latency average = 489.398
	minimum = 6
	maximum = 2696
Slowest flit = 35477
Fragmentation average = 59.6583
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00882031
	minimum = 0.00375 (at node 90)
	maximum = 0.01375 (at node 142)
Accepted packet rate average = 0.00882161
	minimum = 0.00525 (at node 4)
	maximum = 0.01375 (at node 16)
Injected flit rate average = 0.158819
	minimum = 0.0675 (at node 90)
	maximum = 0.247 (at node 142)
Accepted flit rate average= 0.158586
	minimum = 0.0945 (at node 4)
	maximum = 0.24675 (at node 16)
Injected packet length average = 18.0061
Accepted packet length average = 17.977
Total in-flight flits = 17736 (17736 measured)
latency change    = 0.109288
throughput change = 0.000815585
Class 0:
Packet latency average = 3465.21
	minimum = 40
	maximum = 7165
Network latency average = 518.874
	minimum = 23
	maximum = 2709
Slowest packet = 6268
Flit latency average = 487.251
	minimum = 6
	maximum = 2696
Slowest flit = 35477
Fragmentation average = 58.5387
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00883333
	minimum = 0.003 (at node 90)
	maximum = 0.0144 (at node 149)
Accepted packet rate average = 0.00883646
	minimum = 0.0054 (at node 4)
	maximum = 0.0134 (at node 129)
Injected flit rate average = 0.159003
	minimum = 0.054 (at node 90)
	maximum = 0.2616 (at node 149)
Accepted flit rate average= 0.158945
	minimum = 0.099 (at node 4)
	maximum = 0.2412 (at node 129)
Injected packet length average = 18.0004
Accepted packet length average = 17.9874
Total in-flight flits = 17507 (17507 measured)
latency change    = 0.0926588
throughput change = 0.00225773
Class 0:
Packet latency average = 3779.32
	minimum = 40
	maximum = 8152
Network latency average = 525.361
	minimum = 23
	maximum = 2981
Slowest packet = 6268
Flit latency average = 488.87
	minimum = 6
	maximum = 2960
Slowest flit = 176525
Fragmentation average = 58.2492
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00888194
	minimum = 0.00433333 (at node 90)
	maximum = 0.0146667 (at node 83)
Accepted packet rate average = 0.00886545
	minimum = 0.006 (at node 36)
	maximum = 0.0123333 (at node 128)
Injected flit rate average = 0.159928
	minimum = 0.078 (at node 90)
	maximum = 0.264 (at node 83)
Accepted flit rate average= 0.159558
	minimum = 0.1075 (at node 36)
	maximum = 0.224167 (at node 128)
Injected packet length average = 18.006
Accepted packet length average = 17.9977
Total in-flight flits = 17729 (17729 measured)
latency change    = 0.0831116
throughput change = 0.00384417
Class 0:
Packet latency average = 4104.32
	minimum = 40
	maximum = 8706
Network latency average = 530.28
	minimum = 23
	maximum = 2981
Slowest packet = 6268
Flit latency average = 489.491
	minimum = 6
	maximum = 2960
Slowest flit = 176525
Fragmentation average = 58.104
	minimum = 0
	maximum = 160
Injected packet rate average = 0.00892485
	minimum = 0.00542857 (at node 63)
	maximum = 0.0135714 (at node 83)
Accepted packet rate average = 0.00888988
	minimum = 0.00628571 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.160506
	minimum = 0.0957143 (at node 90)
	maximum = 0.244286 (at node 83)
Accepted flit rate average= 0.160049
	minimum = 0.112714 (at node 36)
	maximum = 0.216 (at node 128)
Injected packet length average = 17.9842
Accepted packet length average = 18.0035
Total in-flight flits = 18114 (18114 measured)
latency change    = 0.0791863
throughput change = 0.00306748
Draining all recorded packets ...
Class 0:
Remaining flits: 283356 283357 283358 283359 283360 283361 283362 283363 283364 283365 [...] (17771 flits)
Measured flits: 283356 283357 283358 283359 283360 283361 283362 283363 283364 283365 [...] (17771 flits)
Class 0:
Remaining flits: 316548 316549 316550 316551 316552 316553 316554 316555 316556 316557 [...] (17937 flits)
Measured flits: 316548 316549 316550 316551 316552 316553 316554 316555 316556 316557 [...] (17937 flits)
Class 0:
Remaining flits: 351738 351739 351740 351741 351742 351743 351744 351745 351746 351747 [...] (17563 flits)
Measured flits: 351738 351739 351740 351741 351742 351743 351744 351745 351746 351747 [...] (17563 flits)
Class 0:
Remaining flits: 363942 363943 363944 363945 363946 363947 363948 363949 363950 363951 [...] (17748 flits)
Measured flits: 363942 363943 363944 363945 363946 363947 363948 363949 363950 363951 [...] (17640 flits)
Class 0:
Remaining flits: 413010 413011 413012 413013 413014 413015 413016 413017 413018 413019 [...] (17949 flits)
Measured flits: 413010 413011 413012 413013 413014 413015 413016 413017 413018 413019 [...] (17555 flits)
Class 0:
Remaining flits: 447264 447265 447266 447267 447268 447269 447270 447271 447272 447273 [...] (17677 flits)
Measured flits: 447264 447265 447266 447267 447268 447269 447270 447271 447272 447273 [...] (17249 flits)
Class 0:
Remaining flits: 476406 476407 476408 476409 476410 476411 476412 476413 476414 476415 [...] (17995 flits)
Measured flits: 476406 476407 476408 476409 476410 476411 476412 476413 476414 476415 [...] (17118 flits)
Class 0:
Remaining flits: 508567 508568 508569 508570 508571 516222 516223 516224 516225 516226 [...] (17791 flits)
Measured flits: 508567 508568 508569 508570 508571 516222 516223 516224 516225 516226 [...] (16730 flits)
Class 0:
Remaining flits: 516222 516223 516224 516225 516226 516227 516228 516229 516230 516231 [...] (17552 flits)
Measured flits: 516222 516223 516224 516225 516226 516227 516228 516229 516230 516231 [...] (16022 flits)
Class 0:
Remaining flits: 536007 536008 536009 536010 536011 536012 536013 536014 536015 536016 [...] (17648 flits)
Measured flits: 536007 536008 536009 536010 536011 536012 536013 536014 536015 536016 [...] (15761 flits)
Class 0:
Remaining flits: 600336 600337 600338 600339 600340 600341 600342 600343 600344 600345 [...] (17768 flits)
Measured flits: 600336 600337 600338 600339 600340 600341 600342 600343 600344 600345 [...] (15024 flits)
Class 0:
Remaining flits: 634072 634073 634074 634075 634076 634077 634078 634079 634080 634081 [...] (17802 flits)
Measured flits: 634072 634073 634074 634075 634076 634077 634078 634079 634080 634081 [...] (14118 flits)
Class 0:
Remaining flits: 666810 666811 666812 666813 666814 666815 666816 666817 666818 666819 [...] (17755 flits)
Measured flits: 666810 666811 666812 666813 666814 666815 666816 666817 666818 666819 [...] (13062 flits)
Class 0:
Remaining flits: 677427 677428 677429 682083 682084 682085 682086 682087 682088 682089 [...] (17996 flits)
Measured flits: 677427 677428 677429 682083 682084 682085 682086 682087 682088 682089 [...] (12441 flits)
Class 0:
Remaining flits: 692010 692011 692012 692013 692014 692015 692016 692017 692018 692019 [...] (17569 flits)
Measured flits: 692010 692011 692012 692013 692014 692015 692016 692017 692018 692019 [...] (10728 flits)
Class 0:
Remaining flits: 749898 749899 749900 749901 749902 749903 749904 749905 749906 749907 [...] (17473 flits)
Measured flits: 760968 760969 760970 760971 760972 760973 760974 760975 760976 760977 [...] (9529 flits)
Class 0:
Remaining flits: 771822 771823 771824 771825 771826 771827 771828 771829 771830 771831 [...] (17652 flits)
Measured flits: 771822 771823 771824 771825 771826 771827 771828 771829 771830 771831 [...] (8017 flits)
Class 0:
Remaining flits: 805482 805483 805484 805485 805486 805487 805488 805489 805490 805491 [...] (17648 flits)
Measured flits: 824022 824023 824024 824025 824026 824027 824028 824029 824030 824031 [...] (6964 flits)
Class 0:
Remaining flits: 847530 847531 847532 847533 847534 847535 847536 847537 847538 847539 [...] (17758 flits)
Measured flits: 847530 847531 847532 847533 847534 847535 847536 847537 847538 847539 [...] (6595 flits)
Class 0:
Remaining flits: 870768 870769 870770 870771 870772 870773 870774 870775 870776 870777 [...] (17555 flits)
Measured flits: 870768 870769 870770 870771 870772 870773 870774 870775 870776 870777 [...] (5604 flits)
Class 0:
Remaining flits: 904968 904969 904970 904971 904972 904973 904974 904975 904976 904977 [...] (17707 flits)
Measured flits: 929876 929877 929878 929879 929898 929899 929900 929901 929902 929903 [...] (4991 flits)
Class 0:
Remaining flits: 945090 945091 945092 945093 945094 945095 945096 945097 945098 945099 [...] (17741 flits)
Measured flits: 949122 949123 949124 949125 949126 949127 949128 949129 949130 949131 [...] (4549 flits)
Class 0:
Remaining flits: 974232 974233 974234 974235 974236 974237 974238 974239 974240 974241 [...] (17518 flits)
Measured flits: 994176 994177 994178 994179 994180 994181 994182 994183 994184 994185 [...] (3621 flits)
Class 0:
Remaining flits: 992646 992647 992648 992649 992650 992651 992652 992653 992654 992655 [...] (18082 flits)
Measured flits: 1024254 1024255 1024256 1024257 1024258 1024259 1024260 1024261 1024262 1024263 [...] (3012 flits)
Class 0:
Remaining flits: 1022202 1022203 1022204 1022205 1022206 1022207 1022208 1022209 1022210 1022211 [...] (17623 flits)
Measured flits: 1032325 1032326 1032327 1032328 1032329 1032330 1032331 1032332 1032333 1032334 [...] (2273 flits)
Class 0:
Remaining flits: 1052190 1052191 1052192 1052193 1052194 1052195 1052196 1052197 1052198 1052199 [...] (17431 flits)
Measured flits: 1078722 1078723 1078724 1078725 1078726 1078727 1078728 1078729 1078730 1078731 [...] (1984 flits)
Class 0:
Remaining flits: 1055898 1055899 1055900 1055901 1055902 1055903 1055904 1055905 1055906 1055907 [...] (17309 flits)
Measured flits: 1100691 1100692 1100693 1100694 1100695 1100696 1100697 1100698 1100699 1108062 [...] (1432 flits)
Class 0:
Remaining flits: 1113588 1113589 1113590 1113591 1113592 1113593 1113594 1113595 1113596 1113597 [...] (17466 flits)
Measured flits: 1123599 1123600 1123601 1123602 1123603 1123604 1123605 1123606 1123607 1123608 [...] (1200 flits)
Class 0:
Remaining flits: 1148274 1148275 1148276 1148277 1148278 1148279 1148280 1148281 1148282 1148283 [...] (17766 flits)
Measured flits: 1151550 1151551 1151552 1151553 1151554 1151555 1151556 1151557 1151558 1151559 [...] (824 flits)
Class 0:
Remaining flits: 1180620 1180621 1180622 1180623 1180624 1180625 1180626 1180627 1180628 1180629 [...] (17990 flits)
Measured flits: 1192122 1192123 1192124 1192125 1192126 1192127 1192128 1192129 1192130 1192131 [...] (835 flits)
Class 0:
Remaining flits: 1199191 1199192 1199193 1199194 1199195 1199682 1199683 1199684 1199685 1199686 [...] (17856 flits)
Measured flits: 1260522 1260523 1260524 1260525 1260526 1260527 1260528 1260529 1260530 1260531 [...] (568 flits)
Class 0:
Remaining flits: 1232964 1232965 1232966 1232967 1232968 1232969 1232970 1232971 1232972 1232973 [...] (17547 flits)
Measured flits: 1273759 1273760 1273761 1273762 1273763 1273764 1273765 1273766 1273767 1273768 [...] (504 flits)
Class 0:
Remaining flits: 1256976 1256977 1256978 1256979 1256980 1256981 1256982 1256983 1256984 1256985 [...] (17830 flits)
Measured flits: 1325556 1325557 1325558 1325559 1325560 1325561 1325562 1325563 1325564 1325565 [...] (343 flits)
Class 0:
Remaining flits: 1321326 1321327 1321328 1321329 1321330 1321331 1321332 1321333 1321334 1321335 [...] (17862 flits)
Measured flits: 1350432 1350433 1350434 1350435 1350436 1350437 1350438 1350439 1350440 1350441 [...] (312 flits)
Class 0:
Remaining flits: 1329714 1329715 1329716 1329717 1329718 1329719 1329720 1329721 1329722 1329723 [...] (17730 flits)
Measured flits: 1376781 1376782 1376783 1377630 1377631 1377632 1377633 1377634 1377635 1377636 [...] (111 flits)
Class 0:
Remaining flits: 1353477 1353478 1353479 1353480 1353481 1353482 1353483 1353484 1353485 1353486 [...] (17292 flits)
Measured flits: 1416278 1416279 1416280 1416281 1416282 1416283 1416284 1416285 1416286 1416287 [...] (106 flits)
Draining remaining packets ...
Time taken is 47727 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10772.2 (1 samples)
	minimum = 40 (1 samples)
	maximum = 36832 (1 samples)
Network latency average = 553.596 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3468 (1 samples)
Flit latency average = 488.927 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3440 (1 samples)
Fragmentation average = 57.6275 (1 samples)
	minimum = 0 (1 samples)
	maximum = 160 (1 samples)
Injected packet rate average = 0.00892485 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.0135714 (1 samples)
Accepted packet rate average = 0.00888988 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.160506 (1 samples)
	minimum = 0.0957143 (1 samples)
	maximum = 0.244286 (1 samples)
Accepted flit rate average = 0.160049 (1 samples)
	minimum = 0.112714 (1 samples)
	maximum = 0.216 (1 samples)
Injected packet size average = 17.9842 (1 samples)
Accepted packet size average = 18.0035 (1 samples)
Hops average = 5.07955 (1 samples)
Total run time 31.9743
