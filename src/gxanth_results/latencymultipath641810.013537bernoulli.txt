BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 188.709
	minimum = 23
	maximum = 790
Network latency average = 186.375
	minimum = 23
	maximum = 790
Slowest packet = 103
Flit latency average = 116.701
	minimum = 6
	maximum = 782
Slowest flit = 8924
Fragmentation average = 116.753
	minimum = 0
	maximum = 703
Injected packet rate average = 0.0136042
	minimum = 0.003 (at node 87)
	maximum = 0.023 (at node 25)
Accepted packet rate average = 0.00895313
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.24225
	minimum = 0.054 (at node 87)
	maximum = 0.4 (at node 25)
Accepted flit rate average= 0.181021
	minimum = 0.051 (at node 174)
	maximum = 0.315 (at node 48)
Injected packet length average = 17.807
Accepted packet length average = 20.2187
Total in-flight flits = 12260 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 273.714
	minimum = 23
	maximum = 1619
Network latency average = 270.69
	minimum = 23
	maximum = 1619
Slowest packet = 419
Flit latency average = 186.594
	minimum = 6
	maximum = 1669
Slowest flit = 14395
Fragmentation average = 156.869
	minimum = 0
	maximum = 1463
Injected packet rate average = 0.0132604
	minimum = 0.0065 (at node 3)
	maximum = 0.0205 (at node 47)
Accepted packet rate average = 0.0100313
	minimum = 0.0055 (at node 62)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.237792
	minimum = 0.117 (at node 3)
	maximum = 0.369 (at node 47)
Accepted flit rate average= 0.192927
	minimum = 0.108 (at node 30)
	maximum = 0.302 (at node 22)
Injected packet length average = 17.9324
Accepted packet length average = 19.2326
Total in-flight flits = 17626 (0 measured)
latency change    = 0.31056
throughput change = 0.0617137
Class 0:
Packet latency average = 480.113
	minimum = 25
	maximum = 2442
Network latency average = 464.29
	minimum = 25
	maximum = 2416
Slowest packet = 1007
Flit latency average = 351.782
	minimum = 6
	maximum = 2573
Slowest flit = 10413
Fragmentation average = 227.268
	minimum = 0
	maximum = 2151
Injected packet rate average = 0.0129115
	minimum = 0 (at node 56)
	maximum = 0.034 (at node 45)
Accepted packet rate average = 0.0115104
	minimum = 0.005 (at node 104)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.232099
	minimum = 0 (at node 56)
	maximum = 0.615 (at node 45)
Accepted flit rate average= 0.210823
	minimum = 0.075 (at node 184)
	maximum = 0.379 (at node 16)
Injected packet length average = 17.9762
Accepted packet length average = 18.3158
Total in-flight flits = 21824 (0 measured)
latency change    = 0.429897
throughput change = 0.0848856
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 320.289
	minimum = 25
	maximum = 1787
Network latency average = 278.389
	minimum = 25
	maximum = 923
Slowest packet = 7583
Flit latency average = 421.034
	minimum = 6
	maximum = 3415
Slowest flit = 22211
Fragmentation average = 161.574
	minimum = 0
	maximum = 832
Injected packet rate average = 0.0128177
	minimum = 0 (at node 9)
	maximum = 0.026 (at node 127)
Accepted packet rate average = 0.0115469
	minimum = 0.001 (at node 134)
	maximum = 0.022 (at node 33)
Injected flit rate average = 0.229557
	minimum = 0 (at node 9)
	maximum = 0.469 (at node 127)
Accepted flit rate average= 0.209063
	minimum = 0.048 (at node 134)
	maximum = 0.411 (at node 33)
Injected packet length average = 17.9094
Accepted packet length average = 18.1055
Total in-flight flits = 25964 (18426 measured)
latency change    = 0.498997
throughput change = 0.00842053
Class 0:
Packet latency average = 442.841
	minimum = 23
	maximum = 2571
Network latency average = 379.127
	minimum = 23
	maximum = 1831
Slowest packet = 7583
Flit latency average = 458.53
	minimum = 6
	maximum = 4375
Slowest flit = 10421
Fragmentation average = 188.483
	minimum = 0
	maximum = 1684
Injected packet rate average = 0.0124089
	minimum = 0 (at node 9)
	maximum = 0.0255 (at node 56)
Accepted packet rate average = 0.011625
	minimum = 0.0055 (at node 48)
	maximum = 0.02 (at node 78)
Injected flit rate average = 0.22243
	minimum = 0 (at node 9)
	maximum = 0.46 (at node 56)
Accepted flit rate average= 0.210161
	minimum = 0.0965 (at node 162)
	maximum = 0.355 (at node 78)
Injected packet length average = 17.9251
Accepted packet length average = 18.0784
Total in-flight flits = 26874 (22676 measured)
latency change    = 0.276739
throughput change = 0.00522911
Class 0:
Packet latency average = 547.343
	minimum = 23
	maximum = 4291
Network latency average = 449.497
	minimum = 23
	maximum = 2962
Slowest packet = 7583
Flit latency average = 487.21
	minimum = 6
	maximum = 5145
Slowest flit = 38980
Fragmentation average = 205.085
	minimum = 0
	maximum = 2364
Injected packet rate average = 0.0123785
	minimum = 0 (at node 140)
	maximum = 0.0206667 (at node 56)
Accepted packet rate average = 0.0115764
	minimum = 0.00733333 (at node 48)
	maximum = 0.017 (at node 129)
Injected flit rate average = 0.222123
	minimum = 0.000666667 (at node 108)
	maximum = 0.371 (at node 56)
Accepted flit rate average= 0.209257
	minimum = 0.130667 (at node 31)
	maximum = 0.312 (at node 129)
Injected packet length average = 17.9443
Accepted packet length average = 18.0762
Total in-flight flits = 29578 (26935 measured)
latency change    = 0.190927
throughput change = 0.0043225
Class 0:
Packet latency average = 639.425
	minimum = 23
	maximum = 4985
Network latency average = 500.934
	minimum = 23
	maximum = 3807
Slowest packet = 7583
Flit latency average = 513.097
	minimum = 6
	maximum = 5915
Slowest flit = 47414
Fragmentation average = 216.878
	minimum = 0
	maximum = 2701
Injected packet rate average = 0.0123216
	minimum = 0.00025 (at node 108)
	maximum = 0.01825 (at node 56)
Accepted packet rate average = 0.0115365
	minimum = 0.00775 (at node 36)
	maximum = 0.01625 (at node 128)
Injected flit rate average = 0.221292
	minimum = 0.00225 (at node 108)
	maximum = 0.32875 (at node 56)
Accepted flit rate average= 0.208724
	minimum = 0.1395 (at node 36)
	maximum = 0.29375 (at node 128)
Injected packet length average = 17.9596
Accepted packet length average = 18.0926
Total in-flight flits = 31912 (30172 measured)
latency change    = 0.144008
throughput change = 0.00255355
Class 0:
Packet latency average = 726.272
	minimum = 23
	maximum = 5236
Network latency average = 555.517
	minimum = 23
	maximum = 4915
Slowest packet = 7583
Flit latency average = 542.305
	minimum = 6
	maximum = 7318
Slowest flit = 11303
Fragmentation average = 227.959
	minimum = 0
	maximum = 3032
Injected packet rate average = 0.0121917
	minimum = 0.0028 (at node 11)
	maximum = 0.017 (at node 1)
Accepted packet rate average = 0.0115187
	minimum = 0.0076 (at node 64)
	maximum = 0.0154 (at node 128)
Injected flit rate average = 0.218947
	minimum = 0.052 (at node 11)
	maximum = 0.3036 (at node 1)
Accepted flit rate average= 0.208068
	minimum = 0.1434 (at node 64)
	maximum = 0.2754 (at node 128)
Injected packet length average = 17.9587
Accepted packet length average = 18.0634
Total in-flight flits = 32859 (31678 measured)
latency change    = 0.119579
throughput change = 0.00315402
Class 0:
Packet latency average = 824.538
	minimum = 23
	maximum = 5671
Network latency average = 606.655
	minimum = 23
	maximum = 5671
Slowest packet = 7616
Flit latency average = 573.99
	minimum = 6
	maximum = 8499
Slowest flit = 22386
Fragmentation average = 235.724
	minimum = 0
	maximum = 4029
Injected packet rate average = 0.0120226
	minimum = 0.00366667 (at node 140)
	maximum = 0.0168333 (at node 6)
Accepted packet rate average = 0.0115234
	minimum = 0.0075 (at node 64)
	maximum = 0.0155 (at node 128)
Injected flit rate average = 0.215939
	minimum = 0.0688333 (at node 140)
	maximum = 0.303 (at node 6)
Accepted flit rate average= 0.207751
	minimum = 0.140833 (at node 64)
	maximum = 0.2785 (at node 128)
Injected packet length average = 17.9612
Accepted packet length average = 18.0285
Total in-flight flits = 31885 (31050 measured)
latency change    = 0.119176
throughput change = 0.0015251
Class 0:
Packet latency average = 902.39
	minimum = 23
	maximum = 6634
Network latency average = 644.319
	minimum = 23
	maximum = 6533
Slowest packet = 7811
Flit latency average = 593.178
	minimum = 6
	maximum = 8519
Slowest flit = 22388
Fragmentation average = 240.289
	minimum = 0
	maximum = 5070
Injected packet rate average = 0.0120365
	minimum = 0.00314286 (at node 140)
	maximum = 0.0171429 (at node 95)
Accepted packet rate average = 0.0115149
	minimum = 0.00842857 (at node 64)
	maximum = 0.0147143 (at node 128)
Injected flit rate average = 0.216304
	minimum = 0.059 (at node 140)
	maximum = 0.308571 (at node 95)
Accepted flit rate average= 0.207484
	minimum = 0.154857 (at node 89)
	maximum = 0.268 (at node 128)
Injected packet length average = 17.9707
Accepted packet length average = 18.0188
Total in-flight flits = 34259 (33566 measured)
latency change    = 0.0862731
throughput change = 0.0012844
Draining all recorded packets ...
Class 0:
Remaining flits: 22389 22390 22391 24552 24553 24554 24555 24556 24557 24558 [...] (34287 flits)
Measured flits: 137394 137395 137396 137397 137398 137399 137400 137401 137402 137403 [...] (20597 flits)
Class 0:
Remaining flits: 24552 24553 24554 24555 24556 24557 24558 24559 24560 24561 [...] (37478 flits)
Measured flits: 137394 137395 137396 137397 137398 137399 137400 137401 137402 137403 [...] (15858 flits)
Class 0:
Remaining flits: 28566 28567 28568 28569 28570 28571 28572 28573 28574 28575 [...] (36865 flits)
Measured flits: 137394 137395 137396 137397 137398 137399 137400 137401 137402 137403 [...] (11719 flits)
Class 0:
Remaining flits: 28566 28567 28568 28569 28570 28571 28572 28573 28574 28575 [...] (37850 flits)
Measured flits: 151524 151525 151526 151527 151528 151529 151530 151531 151532 151533 [...] (8707 flits)
Class 0:
Remaining flits: 28577 28578 28579 28580 28581 28582 28583 68688 68689 68690 [...] (36199 flits)
Measured flits: 151539 151540 151541 156528 156529 156530 156531 156532 156533 156534 [...] (5864 flits)
Class 0:
Remaining flits: 74106 74107 74108 74109 74110 74111 74112 74113 74114 74115 [...] (39209 flits)
Measured flits: 156528 156529 156530 156531 156532 156533 156534 156535 156536 156537 [...] (4560 flits)
Class 0:
Remaining flits: 74106 74107 74108 74109 74110 74111 74112 74113 74114 74115 [...] (39901 flits)
Measured flits: 156528 156529 156530 156531 156532 156533 156534 156535 156536 156537 [...] (3503 flits)
Class 0:
Remaining flits: 74106 74107 74108 74109 74110 74111 74112 74113 74114 74115 [...] (41503 flits)
Measured flits: 156543 156544 156545 186570 186571 186572 186573 186574 186575 186576 [...] (2565 flits)
Class 0:
Remaining flits: 74112 74113 74114 74115 74116 74117 74118 74119 74120 74121 [...] (45083 flits)
Measured flits: 186570 186571 186572 186573 186574 186575 186576 186577 186578 186579 [...] (1753 flits)
Class 0:
Remaining flits: 79391 79392 79393 79394 79395 79396 79397 120450 120451 120452 [...] (43557 flits)
Measured flits: 186570 186571 186572 186573 186574 186575 186576 186577 186578 186579 [...] (1444 flits)
Class 0:
Remaining flits: 186580 186581 186582 186583 186584 186585 186586 186587 187974 187975 [...] (43687 flits)
Measured flits: 186580 186581 186582 186583 186584 186585 186586 186587 187974 187975 [...] (913 flits)
Class 0:
Remaining flits: 186580 186581 186582 186583 186584 186585 186586 186587 229374 229375 [...] (47144 flits)
Measured flits: 186580 186581 186582 186583 186584 186585 186586 186587 229374 229375 [...] (754 flits)
Class 0:
Remaining flits: 186583 186584 186585 186586 186587 229374 229375 229376 229377 229378 [...] (49507 flits)
Measured flits: 186583 186584 186585 186586 186587 229374 229375 229376 229377 229378 [...] (609 flits)
Class 0:
Remaining flits: 317381 317382 317383 317384 317385 317386 317387 317388 317389 317390 [...] (51014 flits)
Measured flits: 317381 317382 317383 317384 317385 317386 317387 317388 317389 317390 [...] (688 flits)
Class 0:
Remaining flits: 317388 317389 317390 317391 317392 317393 320561 413028 413029 413030 [...] (52014 flits)
Measured flits: 317388 317389 317390 317391 317392 317393 320561 413028 413029 413030 [...] (394 flits)
Class 0:
Remaining flits: 317388 317389 317390 317391 317392 317393 320561 477792 477793 477794 [...] (50916 flits)
Measured flits: 317388 317389 317390 317391 317392 317393 320561 576670 576671 576672 [...] (302 flits)
Class 0:
Remaining flits: 477792 477793 477794 477795 477796 477797 477798 477799 477800 477801 [...] (50637 flits)
Measured flits: 576670 576671 576672 576673 576674 576675 576676 576677 576678 576679 [...] (222 flits)
Class 0:
Remaining flits: 477792 477793 477794 477795 477796 477797 477798 477799 477800 477801 [...] (53756 flits)
Measured flits: 579134 579135 579136 579137 579138 579139 579140 579141 579142 579143 [...] (189 flits)
Class 0:
Remaining flits: 477792 477793 477794 477795 477796 477797 477798 477799 477800 477801 [...] (55025 flits)
Measured flits: 581274 581275 581276 581277 581278 581279 581280 581281 581282 581283 [...] (144 flits)
Class 0:
Remaining flits: 477792 477793 477794 477795 477796 477797 477798 477799 477800 477801 [...] (54703 flits)
Measured flits: 581274 581275 581276 581277 581278 581279 581280 581281 581282 581283 [...] (136 flits)
Class 0:
Remaining flits: 493349 493350 493351 493352 493353 493354 493355 493356 493357 493358 [...] (56126 flits)
Measured flits: 581274 581275 581276 581277 581278 581279 581280 581281 581282 581283 [...] (111 flits)
Class 0:
Remaining flits: 581287 581288 581289 581290 581291 612075 612076 612077 612078 612079 [...] (57450 flits)
Measured flits: 581287 581288 581289 581290 581291 807768 807769 807770 807771 807772 [...] (55 flits)
Class 0:
Remaining flits: 688085 689616 689617 689618 689619 689620 689621 689622 689623 689624 [...] (56978 flits)
Measured flits: 978373 978374 978375 978376 978377 978378 978379 978380 978381 978382 [...] (17 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 689623 689624 689625 689626 689627 689628 689629 689630 689631 689632 [...] (28961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 763452 763453 763454 763455 763456 763457 763458 763459 763460 763461 [...] (13550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 763463 763464 763465 763466 763467 763468 763469 778874 778875 778876 [...] (2945 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1184922 1184923 1184924 1184925 1184926 1184927 1184928 1184929 1184930 1184931 [...] (113 flits)
Measured flits: (0 flits)
Time taken is 37481 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1742.15 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24079 (1 samples)
Network latency average = 986.493 (1 samples)
	minimum = 23 (1 samples)
	maximum = 19484 (1 samples)
Flit latency average = 1092.79 (1 samples)
	minimum = 6 (1 samples)
	maximum = 19643 (1 samples)
Fragmentation average = 290.948 (1 samples)
	minimum = 0 (1 samples)
	maximum = 13042 (1 samples)
Injected packet rate average = 0.0120365 (1 samples)
	minimum = 0.00314286 (1 samples)
	maximum = 0.0171429 (1 samples)
Accepted packet rate average = 0.0115149 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0147143 (1 samples)
Injected flit rate average = 0.216304 (1 samples)
	minimum = 0.059 (1 samples)
	maximum = 0.308571 (1 samples)
Accepted flit rate average = 0.207484 (1 samples)
	minimum = 0.154857 (1 samples)
	maximum = 0.268 (1 samples)
Injected packet size average = 17.9707 (1 samples)
Accepted packet size average = 18.0188 (1 samples)
Hops average = 5.06454 (1 samples)
Total run time 48.7164
