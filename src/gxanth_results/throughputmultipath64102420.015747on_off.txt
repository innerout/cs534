BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.689
	minimum = 22
	maximum = 826
Network latency average = 151.455
	minimum = 22
	maximum = 701
Slowest packet = 63
Flit latency average = 123.98
	minimum = 5
	maximum = 684
Slowest flit = 14921
Fragmentation average = 25.1858
	minimum = 0
	maximum = 364
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117708
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.217625
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.4885
Total in-flight flits = 12810 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 303.73
	minimum = 22
	maximum = 1596
Network latency average = 233.233
	minimum = 22
	maximum = 1015
Slowest packet = 63
Flit latency average = 204.348
	minimum = 5
	maximum = 1036
Slowest flit = 50668
Fragmentation average = 29.9886
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125807
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.229474
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 70)
Injected packet length average = 17.9137
Accepted packet length average = 18.2401
Total in-flight flits = 19522 (0 measured)
latency change    = 0.29645
throughput change = 0.0516353
Class 0:
Packet latency average = 500.152
	minimum = 22
	maximum = 1673
Network latency average = 424.222
	minimum = 22
	maximum = 1443
Slowest packet = 2814
Flit latency average = 389.048
	minimum = 5
	maximum = 1426
Slowest flit = 81377
Fragmentation average = 35.683
	minimum = 0
	maximum = 375
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140156
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.253318
	minimum = 0.114 (at node 4)
	maximum = 0.419 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.074
Total in-flight flits = 26865 (0 measured)
latency change    = 0.392724
throughput change = 0.0941259
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.704
	minimum = 22
	maximum = 1178
Network latency average = 315.034
	minimum = 22
	maximum = 943
Slowest packet = 9091
Flit latency average = 507.216
	minimum = 5
	maximum = 1677
Slowest flit = 105947
Fragmentation average = 25.5262
	minimum = 0
	maximum = 204
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141927
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.254302
	minimum = 0.09 (at node 36)
	maximum = 0.516 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9178
Total in-flight flits = 29951 (27989 measured)
latency change    = 0.286716
throughput change = 0.00387089
Class 0:
Packet latency average = 580.149
	minimum = 22
	maximum = 2030
Network latency average = 501.16
	minimum = 22
	maximum = 1746
Slowest packet = 9091
Flit latency average = 550.839
	minimum = 5
	maximum = 2151
Slowest flit = 153984
Fragmentation average = 34.5995
	minimum = 0
	maximum = 399
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0141979
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.255698
	minimum = 0.153 (at node 36)
	maximum = 0.375 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0095
Total in-flight flits = 32807 (32773 measured)
latency change    = 0.329993
throughput change = 0.00545892
Class 0:
Packet latency average = 652.175
	minimum = 22
	maximum = 2341
Network latency average = 573.491
	minimum = 22
	maximum = 2015
Slowest packet = 9091
Flit latency average = 580.825
	minimum = 5
	maximum = 2186
Slowest flit = 153989
Fragmentation average = 34.5559
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143368
	minimum = 0.00833333 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257733
	minimum = 0.155667 (at node 36)
	maximum = 0.359667 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.977
Total in-flight flits = 34237 (34237 measured)
latency change    = 0.110439
throughput change = 0.0078947
Draining remaining packets ...
Class 0:
Remaining flits: 271350 271351 271352 271353 271354 271355 271356 271357 271358 271359 [...] (2734 flits)
Measured flits: 271350 271351 271352 271353 271354 271355 271356 271357 271358 271359 [...] (2734 flits)
Time taken is 7634 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 744.603 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2387 (1 samples)
Network latency average = 662.936 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2062 (1 samples)
Flit latency average = 648.054 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2186 (1 samples)
Fragmentation average = 35.5358 (1 samples)
	minimum = 0 (1 samples)
	maximum = 449 (1 samples)
Injected packet rate average = 0.0150295 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0143368 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.270549 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.581667 (1 samples)
Accepted flit rate average = 0.257733 (1 samples)
	minimum = 0.155667 (1 samples)
	maximum = 0.359667 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 17.977 (1 samples)
Hops average = 5.05776 (1 samples)
Total run time 5.90543
