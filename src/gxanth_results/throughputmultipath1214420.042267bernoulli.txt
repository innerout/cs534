BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.093
	minimum = 22
	maximum = 825
Network latency average = 298.576
	minimum = 22
	maximum = 788
Slowest packet = 236
Flit latency average = 275.304
	minimum = 5
	maximum = 787
Slowest flit = 27864
Fragmentation average = 24.4448
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0400573
	minimum = 0.026 (at node 17)
	maximum = 0.055 (at node 102)
Accepted packet rate average = 0.0150104
	minimum = 0.007 (at node 150)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.714479
	minimum = 0.454 (at node 33)
	maximum = 0.988 (at node 102)
Accepted flit rate average= 0.277047
	minimum = 0.133 (at node 150)
	maximum = 0.442 (at node 48)
Injected packet length average = 17.8364
Accepted packet length average = 18.457
Total in-flight flits = 85425 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.832
	minimum = 22
	maximum = 1578
Network latency average = 590.582
	minimum = 22
	maximum = 1485
Slowest packet = 2139
Flit latency average = 566.67
	minimum = 5
	maximum = 1506
Slowest flit = 66838
Fragmentation average = 25.8089
	minimum = 0
	maximum = 115
Injected packet rate average = 0.0356901
	minimum = 0.0195 (at node 112)
	maximum = 0.0465 (at node 41)
Accepted packet rate average = 0.015125
	minimum = 0.008 (at node 79)
	maximum = 0.0225 (at node 78)
Injected flit rate average = 0.64057
	minimum = 0.351 (at node 112)
	maximum = 0.8365 (at node 41)
Accepted flit rate average= 0.275167
	minimum = 0.156 (at node 79)
	maximum = 0.408 (at node 78)
Injected packet length average = 17.9481
Accepted packet length average = 18.1928
Total in-flight flits = 142790 (0 measured)
latency change    = 0.487514
throughput change = 0.00683298
Class 0:
Packet latency average = 1582.29
	minimum = 22
	maximum = 2394
Network latency average = 1516.36
	minimum = 22
	maximum = 2365
Slowest packet = 4781
Flit latency average = 1499.28
	minimum = 5
	maximum = 2348
Slowest flit = 86067
Fragmentation average = 22.3273
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0161667
	minimum = 0.001 (at node 168)
	maximum = 0.044 (at node 47)
Accepted packet rate average = 0.0146406
	minimum = 0.005 (at node 31)
	maximum = 0.025 (at node 24)
Injected flit rate average = 0.291953
	minimum = 0.018 (at node 168)
	maximum = 0.776 (at node 47)
Accepted flit rate average= 0.263276
	minimum = 0.089 (at node 31)
	maximum = 0.448 (at node 34)
Injected packet length average = 18.059
Accepted packet length average = 17.9826
Total in-flight flits = 148635 (0 measured)
latency change    = 0.61143
throughput change = 0.0451641
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1424.28
	minimum = 400
	maximum = 2498
Network latency average = 73.8043
	minimum = 22
	maximum = 898
Slowest packet = 16964
Flit latency average = 2005.7
	minimum = 5
	maximum = 3174
Slowest flit = 114285
Fragmentation average = 4.76087
	minimum = 0
	maximum = 15
Injected packet rate average = 0.0157031
	minimum = 0.001 (at node 21)
	maximum = 0.037 (at node 183)
Accepted packet rate average = 0.0148802
	minimum = 0.007 (at node 15)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.28176
	minimum = 0.018 (at node 77)
	maximum = 0.665 (at node 183)
Accepted flit rate average= 0.267865
	minimum = 0.126 (at node 117)
	maximum = 0.466 (at node 16)
Injected packet length average = 17.943
Accepted packet length average = 18.0014
Total in-flight flits = 151187 (52283 measured)
latency change    = 0.11094
throughput change = 0.0171301
Class 0:
Packet latency average = 2233.88
	minimum = 400
	maximum = 3442
Network latency average = 765.459
	minimum = 22
	maximum = 1988
Slowest packet = 16964
Flit latency average = 2222.5
	minimum = 5
	maximum = 3957
Slowest flit = 143091
Fragmentation average = 8.86079
	minimum = 0
	maximum = 47
Injected packet rate average = 0.0153307
	minimum = 0.004 (at node 77)
	maximum = 0.0285 (at node 116)
Accepted packet rate average = 0.0148984
	minimum = 0.008 (at node 36)
	maximum = 0.0215 (at node 66)
Injected flit rate average = 0.2755
	minimum = 0.072 (at node 77)
	maximum = 0.505 (at node 116)
Accepted flit rate average= 0.268091
	minimum = 0.144 (at node 36)
	maximum = 0.382 (at node 66)
Injected packet length average = 17.9704
Accepted packet length average = 17.9946
Total in-flight flits = 151456 (97763 measured)
latency change    = 0.362418
throughput change = 0.000845095
Class 0:
Packet latency average = 3020.69
	minimum = 400
	maximum = 4538
Network latency average = 1402.98
	minimum = 22
	maximum = 2982
Slowest packet = 16964
Flit latency average = 2389.02
	minimum = 5
	maximum = 4717
Slowest flit = 167903
Fragmentation average = 13.3094
	minimum = 0
	maximum = 261
Injected packet rate average = 0.0154184
	minimum = 0.00633333 (at node 77)
	maximum = 0.027 (at node 169)
Accepted packet rate average = 0.014842
	minimum = 0.01 (at node 64)
	maximum = 0.02 (at node 138)
Injected flit rate average = 0.27742
	minimum = 0.114 (at node 77)
	maximum = 0.483 (at node 169)
Accepted flit rate average= 0.267156
	minimum = 0.18 (at node 64)
	maximum = 0.357333 (at node 138)
Injected packet length average = 17.9928
Accepted packet length average = 18
Total in-flight flits = 154485 (133631 measured)
latency change    = 0.260474
throughput change = 0.00349943
Draining remaining packets ...
Class 0:
Remaining flits: 223362 223363 223364 223365 223366 223367 223368 223369 223370 223371 [...] (104384 flits)
Measured flits: 304848 304849 304850 304851 304852 304853 304854 304855 304856 304857 [...] (100991 flits)
Class 0:
Remaining flits: 286794 286795 286796 286797 286798 286799 286800 286801 286802 286803 [...] (56095 flits)
Measured flits: 307980 307981 307982 307983 307984 307985 307986 307987 307988 307989 [...] (55951 flits)
Class 0:
Remaining flits: 299016 299017 299018 299019 299020 299021 299022 299023 299024 299025 [...] (9699 flits)
Measured flits: 314460 314461 314462 314463 314464 314465 314466 314467 314468 314469 [...] (9681 flits)
Class 0:
Remaining flits: 442518 442519 442520 442521 442522 442523 442524 442525 442526 442527 [...] (12 flits)
Measured flits: 442518 442519 442520 442521 442522 442523 442524 442525 442526 442527 [...] (12 flits)
Time taken is 10023 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4659.64 (1 samples)
	minimum = 400 (1 samples)
	maximum = 7303 (1 samples)
Network latency average = 2798.7 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5911 (1 samples)
Flit latency average = 2780.86 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6660 (1 samples)
Fragmentation average = 17.6895 (1 samples)
	minimum = 0 (1 samples)
	maximum = 261 (1 samples)
Injected packet rate average = 0.0154184 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.027 (1 samples)
Accepted packet rate average = 0.014842 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.27742 (1 samples)
	minimum = 0.114 (1 samples)
	maximum = 0.483 (1 samples)
Accepted flit rate average = 0.267156 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.357333 (1 samples)
Injected packet size average = 17.9928 (1 samples)
Accepted packet size average = 18 (1 samples)
Hops average = 5.07302 (1 samples)
Total run time 8.60967
