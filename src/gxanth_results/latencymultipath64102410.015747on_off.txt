BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 280.868
	minimum = 23
	maximum = 966
Network latency average = 228.184
	minimum = 23
	maximum = 916
Slowest packet = 270
Flit latency average = 155.962
	minimum = 6
	maximum = 908
Slowest flit = 6228
Fragmentation average = 134.976
	minimum = 0
	maximum = 714
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.00896354
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.182984
	minimum = 0.036 (at node 174)
	maximum = 0.327 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 20.4143
Total in-flight flits = 19461 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 417.233
	minimum = 23
	maximum = 1808
Network latency average = 354.109
	minimum = 23
	maximum = 1764
Slowest packet = 210
Flit latency average = 263.734
	minimum = 6
	maximum = 1787
Slowest flit = 11867
Fragmentation average = 180.303
	minimum = 0
	maximum = 1604
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0100339
	minimum = 0.0045 (at node 153)
	maximum = 0.0155 (at node 71)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.195284
	minimum = 0.098 (at node 153)
	maximum = 0.29 (at node 71)
Injected packet length average = 17.9137
Accepted packet length average = 19.4625
Total in-flight flits = 32651 (0 measured)
latency change    = 0.326833
throughput change = 0.0629826
Class 0:
Packet latency average = 721.58
	minimum = 25
	maximum = 2739
Network latency average = 647.472
	minimum = 25
	maximum = 2661
Slowest packet = 288
Flit latency average = 533.811
	minimum = 6
	maximum = 2806
Slowest flit = 11094
Fragmentation average = 267.961
	minimum = 1
	maximum = 2158
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0119063
	minimum = 0.004 (at node 138)
	maximum = 0.021 (at node 23)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.220427
	minimum = 0.056 (at node 138)
	maximum = 0.385 (at node 99)
Injected packet length average = 18.0167
Accepted packet length average = 18.5136
Total in-flight flits = 46309 (0 measured)
latency change    = 0.421779
throughput change = 0.114066
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.39
	minimum = 27
	maximum = 1174
Network latency average = 323.461
	minimum = 23
	maximum = 968
Slowest packet = 9091
Flit latency average = 716.195
	minimum = 6
	maximum = 3802
Slowest flit = 11082
Fragmentation average = 171.867
	minimum = 0
	maximum = 741
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0122448
	minimum = 0.004 (at node 51)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.222422
	minimum = 0.085 (at node 166)
	maximum = 0.393 (at node 103)
Injected packet length average = 17.9896
Accepted packet length average = 18.1646
Total in-flight flits = 55516 (33669 measured)
latency change    = 0.857875
throughput change = 0.0089685
Class 0:
Packet latency average = 605.307
	minimum = 27
	maximum = 2151
Network latency average = 540.037
	minimum = 23
	maximum = 1989
Slowest packet = 9091
Flit latency average = 818.275
	minimum = 6
	maximum = 4894
Slowest flit = 4365
Fragmentation average = 218.941
	minimum = 0
	maximum = 1368
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0122135
	minimum = 0.0055 (at node 79)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.222693
	minimum = 0.1115 (at node 23)
	maximum = 0.3525 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.2333
Total in-flight flits = 64925 (52772 measured)
latency change    = 0.358358
throughput change = 0.00121618
Class 0:
Packet latency average = 788.473
	minimum = 27
	maximum = 3060
Network latency average = 716.717
	minimum = 23
	maximum = 2792
Slowest packet = 9091
Flit latency average = 900.67
	minimum = 6
	maximum = 5472
Slowest flit = 28605
Fragmentation average = 243.212
	minimum = 0
	maximum = 1917
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0123021
	minimum = 0.007 (at node 79)
	maximum = 0.0173333 (at node 129)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.223538
	minimum = 0.138 (at node 79)
	maximum = 0.325333 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 18.1708
Total in-flight flits = 73377 (65672 measured)
latency change    = 0.232305
throughput change = 0.00378229
Class 0:
Packet latency average = 940.567
	minimum = 27
	maximum = 4022
Network latency average = 866.96
	minimum = 23
	maximum = 3889
Slowest packet = 9373
Flit latency average = 991.75
	minimum = 6
	maximum = 6718
Slowest flit = 15107
Fragmentation average = 257.877
	minimum = 0
	maximum = 2625
Injected packet rate average = 0.0150117
	minimum = 0.0055 (at node 35)
	maximum = 0.03025 (at node 158)
Accepted packet rate average = 0.0123333
	minimum = 0.008 (at node 36)
	maximum = 0.018 (at node 129)
Injected flit rate average = 0.27023
	minimum = 0.099 (at node 35)
	maximum = 0.54075 (at node 158)
Accepted flit rate average= 0.22385
	minimum = 0.15075 (at node 146)
	maximum = 0.3365 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 18.15
Total in-flight flits = 81914 (76783 measured)
latency change    = 0.161704
throughput change = 0.00139408
Class 0:
Packet latency average = 1061.53
	minimum = 27
	maximum = 5114
Network latency average = 985.914
	minimum = 23
	maximum = 4903
Slowest packet = 9122
Flit latency average = 1064.4
	minimum = 6
	maximum = 7705
Slowest flit = 13535
Fragmentation average = 271.205
	minimum = 0
	maximum = 3340
Injected packet rate average = 0.0151635
	minimum = 0.005 (at node 68)
	maximum = 0.0292 (at node 96)
Accepted packet rate average = 0.0123219
	minimum = 0.0082 (at node 79)
	maximum = 0.0174 (at node 129)
Injected flit rate average = 0.272958
	minimum = 0.09 (at node 68)
	maximum = 0.5256 (at node 96)
Accepted flit rate average= 0.223729
	minimum = 0.1556 (at node 79)
	maximum = 0.3152 (at node 129)
Injected packet length average = 18.001
Accepted packet length average = 18.1571
Total in-flight flits = 93555 (89942 measured)
latency change    = 0.113952
throughput change = 0.000541252
Class 0:
Packet latency average = 1182.45
	minimum = 27
	maximum = 5901
Network latency average = 1106.07
	minimum = 23
	maximum = 5695
Slowest packet = 9122
Flit latency average = 1146.8
	minimum = 6
	maximum = 8521
Slowest flit = 19079
Fragmentation average = 285.135
	minimum = 0
	maximum = 4318
Injected packet rate average = 0.0151467
	minimum = 0.0055 (at node 68)
	maximum = 0.0271667 (at node 96)
Accepted packet rate average = 0.012349
	minimum = 0.00916667 (at node 42)
	maximum = 0.0168333 (at node 129)
Injected flit rate average = 0.272705
	minimum = 0.099 (at node 68)
	maximum = 0.489 (at node 96)
Accepted flit rate average= 0.223334
	minimum = 0.170833 (at node 135)
	maximum = 0.299 (at node 129)
Injected packet length average = 18.0042
Accepted packet length average = 18.0853
Total in-flight flits = 103110 (100529 measured)
latency change    = 0.102266
throughput change = 0.00176849
Class 0:
Packet latency average = 1287.07
	minimum = 27
	maximum = 6806
Network latency average = 1210.62
	minimum = 23
	maximum = 6669
Slowest packet = 9122
Flit latency average = 1226.68
	minimum = 6
	maximum = 9687
Slowest flit = 17364
Fragmentation average = 291.634
	minimum = 0
	maximum = 5222
Injected packet rate average = 0.0152582
	minimum = 0.007 (at node 167)
	maximum = 0.0258571 (at node 2)
Accepted packet rate average = 0.0123147
	minimum = 0.00928571 (at node 79)
	maximum = 0.016 (at node 181)
Injected flit rate average = 0.274633
	minimum = 0.126 (at node 167)
	maximum = 0.466286 (at node 2)
Accepted flit rate average= 0.222807
	minimum = 0.171429 (at node 171)
	maximum = 0.289857 (at node 181)
Injected packet length average = 17.9991
Accepted packet length average = 18.0927
Total in-flight flits = 115983 (114209 measured)
latency change    = 0.0812835
throughput change = 0.00236821
Draining all recorded packets ...
Class 0:
Remaining flits: 5731 5732 5733 5734 5735 5736 5737 5738 5739 5740 [...] (128049 flits)
Measured flits: 163998 163999 164000 164001 164002 164003 164004 164005 164006 164007 [...] (85833 flits)
Class 0:
Remaining flits: 5731 5732 5733 5734 5735 5736 5737 5738 5739 5740 [...] (138489 flits)
Measured flits: 165909 165910 165911 165912 165913 165914 165915 165916 165917 165918 [...] (62866 flits)
Class 0:
Remaining flits: 11304 11305 11306 11307 11308 11309 11310 11311 11312 11313 [...] (149801 flits)
Measured flits: 165912 165913 165914 165915 165916 165917 165918 165919 165920 165921 [...] (47243 flits)
Class 0:
Remaining flits: 43542 43543 43544 43545 43546 43547 43548 43549 43550 43551 [...] (163608 flits)
Measured flits: 165912 165913 165914 165915 165916 165917 165918 165919 165920 165921 [...] (35668 flits)
Class 0:
Remaining flits: 43549 43550 43551 43552 43553 43554 43555 43556 43557 43558 [...] (180827 flits)
Measured flits: 165912 165913 165914 165915 165916 165917 165918 165919 165920 165921 [...] (27306 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (195024 flits)
Measured flits: 165912 165913 165914 165915 165916 165917 165918 165919 165920 165921 [...] (21168 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (207827 flits)
Measured flits: 177642 177643 177644 177645 177646 177647 177648 177649 177650 177651 [...] (16533 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (221276 flits)
Measured flits: 178003 178004 178005 178006 178007 178008 178009 178010 178011 178012 [...] (12682 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (236698 flits)
Measured flits: 183420 183421 183422 183423 183424 183425 183426 183427 183428 183429 [...] (9467 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (251454 flits)
Measured flits: 183420 183421 183422 183423 183424 183425 183426 183427 183428 183429 [...] (7378 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (264427 flits)
Measured flits: 183420 183421 183422 183423 183424 183425 183426 183427 183428 183429 [...] (6199 flits)
Class 0:
Remaining flits: 54540 54541 54542 54543 54544 54545 54546 54547 54548 54549 [...] (276105 flits)
Measured flits: 185166 185167 185168 185169 185170 185171 185172 185173 185174 185175 [...] (4972 flits)
Class 0:
Remaining flits: 125338 125339 125340 125341 125342 125343 125344 125345 125346 125347 [...] (293266 flits)
Measured flits: 185179 185180 185181 185182 185183 188383 188384 188385 188386 188387 [...] (4097 flits)
Class 0:
Remaining flits: 137879 188383 188384 188385 188386 188387 205254 205255 205256 205257 [...] (314461 flits)
Measured flits: 188383 188384 188385 188386 188387 205254 205255 205256 205257 205258 [...] (3655 flits)
Class 0:
Remaining flits: 137879 188383 188384 188385 188386 188387 215486 215487 215488 215489 [...] (330847 flits)
Measured flits: 188383 188384 188385 188386 188387 215486 215487 215488 215489 215490 [...] (3097 flits)
Class 0:
Remaining flits: 137879 246690 246691 246692 246693 246694 246695 246696 246697 246698 [...] (351869 flits)
Measured flits: 246690 246691 246692 246693 246694 246695 246696 246697 246698 246699 [...] (2499 flits)
Class 0:
Remaining flits: 137879 246690 246691 246692 246693 246694 246695 246696 246697 246698 [...] (368082 flits)
Measured flits: 246690 246691 246692 246693 246694 246695 246696 246697 246698 246699 [...] (1998 flits)
Class 0:
Remaining flits: 246706 246707 249228 249229 249230 249231 249232 249233 249234 249235 [...] (388780 flits)
Measured flits: 246706 246707 249228 249229 249230 249231 249232 249233 249234 249235 [...] (1522 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (408361 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (1214 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (426478 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (996 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (446593 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (880 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (465450 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (662 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (480677 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (616 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (498539 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (564 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (516574 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (332 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (533849 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (270 flits)
Class 0:
Remaining flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (549709 flits)
Measured flits: 249228 249229 249230 249231 249232 249233 249234 249235 249236 249237 [...] (211 flits)
Class 0:
Remaining flits: 249243 249244 249245 403722 403723 403724 403725 403726 403727 403728 [...] (566949 flits)
Measured flits: 249243 249244 249245 403722 403723 403724 403725 403726 403727 403728 [...] (129 flits)
Class 0:
Remaining flits: 475758 475759 475760 475761 475762 475763 475764 475765 475766 475767 [...] (588172 flits)
Measured flits: 475758 475759 475760 475761 475762 475763 475764 475765 475766 475767 [...] (54 flits)
Class 0:
Remaining flits: 485424 485425 485426 485427 485428 485429 485430 485431 485432 485433 [...] (609832 flits)
Measured flits: 485424 485425 485426 485427 485428 485429 485430 485431 485432 485433 [...] (36 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (625809 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (646857 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (667093 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (683896 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (701843 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (717287 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Class 0:
Remaining flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (732652 flits)
Measured flits: 532332 532333 532334 532335 532336 532337 532338 532339 532340 532341 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (704472 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (672826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (641886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (610665 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (578500 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (546817 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (515443 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (483235 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (450308 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 609660 609661 609662 609663 609664 609665 609666 609667 609668 609669 [...] (417671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 737406 737407 737408 737409 737410 737411 737412 737413 737414 737415 [...] (386898 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 737406 737407 737408 737409 737410 737411 737412 737413 737414 737415 [...] (355485 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 815526 815527 815528 815529 815530 815531 815532 815533 815534 815535 [...] (324088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 815543 824616 824617 824618 824619 824620 824621 824622 824623 824624 [...] (291975 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824616 824617 824618 824619 824620 824621 824622 824623 824624 824625 [...] (261180 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824616 824617 824618 824619 824620 824621 824622 824623 824624 824625 [...] (230336 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824616 824617 824618 824619 824620 824621 824622 824623 824624 824625 [...] (199483 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824616 824617 824618 824619 824620 824621 824622 824623 824624 824625 [...] (167553 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 824631 824632 824633 875466 875467 875468 875469 875470 875471 875472 [...] (136499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 875466 875467 875468 875469 875470 875471 875472 875473 875474 875475 [...] (107244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 875466 875467 875468 875469 875470 875471 875472 875473 875474 875475 [...] (80191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 988362 988363 988364 988365 988366 988367 988368 988369 988370 988371 [...] (55977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 988362 988363 988364 988365 988366 988367 988368 988369 988370 988371 [...] (35446 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1129035 1129036 1129037 1129038 1129039 1129040 1129041 1129042 1129043 1129044 [...] (19964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1313838 1313839 1313840 1313841 1313842 1313843 1313844 1313845 1313846 1313847 [...] (10132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1346652 1346653 1346654 1346655 1346656 1346657 1346658 1346659 1346660 1346661 [...] (4078 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1348075 1348076 1348077 1348078 1348079 1348080 1348081 1348082 1348083 1348084 [...] (879 flits)
Measured flits: (0 flits)
Time taken is 75216 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2668 (1 samples)
	minimum = 27 (1 samples)
	maximum = 37494 (1 samples)
Network latency average = 2587.58 (1 samples)
	minimum = 23 (1 samples)
	maximum = 37324 (1 samples)
Flit latency average = 10019.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 52814 (1 samples)
Fragmentation average = 357.049 (1 samples)
	minimum = 0 (1 samples)
	maximum = 15006 (1 samples)
Injected packet rate average = 0.0152582 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0258571 (1 samples)
Accepted packet rate average = 0.0123147 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.274633 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.466286 (1 samples)
Accepted flit rate average = 0.222807 (1 samples)
	minimum = 0.171429 (1 samples)
	maximum = 0.289857 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0927 (1 samples)
Hops average = 5.06962 (1 samples)
Total run time 102.978
