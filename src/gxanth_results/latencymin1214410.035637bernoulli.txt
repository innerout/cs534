BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.501
	minimum = 23
	maximum = 905
Network latency average = 314.468
	minimum = 23
	maximum = 905
Slowest packet = 471
Flit latency average = 282.629
	minimum = 6
	maximum = 906
Slowest flit = 8721
Fragmentation average = 54.6271
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0325104
	minimum = 0.02 (at node 9)
	maximum = 0.049 (at node 42)
Accepted packet rate average = 0.010099
	minimum = 0.004 (at node 174)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.580078
	minimum = 0.36 (at node 9)
	maximum = 0.875 (at node 42)
Accepted flit rate average= 0.189448
	minimum = 0.086 (at node 174)
	maximum = 0.341 (at node 152)
Injected packet length average = 17.8428
Accepted packet length average = 18.7592
Total in-flight flits = 76144 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 656.231
	minimum = 23
	maximum = 1835
Network latency average = 629.004
	minimum = 23
	maximum = 1788
Slowest packet = 1208
Flit latency average = 593.936
	minimum = 6
	maximum = 1858
Slowest flit = 14050
Fragmentation average = 61.7327
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0280469
	minimum = 0.013 (at node 48)
	maximum = 0.0355 (at node 46)
Accepted packet rate average = 0.0100156
	minimum = 0.0055 (at node 83)
	maximum = 0.015 (at node 131)
Injected flit rate average = 0.502096
	minimum = 0.234 (at node 48)
	maximum = 0.6305 (at node 46)
Accepted flit rate average= 0.184229
	minimum = 0.099 (at node 83)
	maximum = 0.2755 (at node 131)
Injected packet length average = 17.902
Accepted packet length average = 18.3942
Total in-flight flits = 124088 (0 measured)
latency change    = 0.502461
throughput change = 0.0283275
Class 0:
Packet latency average = 1733.44
	minimum = 32
	maximum = 2802
Network latency average = 1620.1
	minimum = 27
	maximum = 2721
Slowest packet = 1532
Flit latency average = 1587.71
	minimum = 6
	maximum = 2751
Slowest flit = 28352
Fragmentation average = 71.4091
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0107292
	minimum = 0 (at node 128)
	maximum = 0.039 (at node 38)
Accepted packet rate average = 0.00945833
	minimum = 0.003 (at node 100)
	maximum = 0.019 (at node 51)
Injected flit rate average = 0.196005
	minimum = 0 (at node 128)
	maximum = 0.702 (at node 38)
Accepted flit rate average= 0.169859
	minimum = 0.059 (at node 4)
	maximum = 0.328 (at node 51)
Injected packet length average = 18.2684
Accepted packet length average = 17.9587
Total in-flight flits = 129905 (0 measured)
latency change    = 0.621428
throughput change = 0.0845982
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1836.91
	minimum = 47
	maximum = 3114
Network latency average = 186.231
	minimum = 27
	maximum = 985
Slowest packet = 12991
Flit latency average = 2237.74
	minimum = 6
	maximum = 3560
Slowest flit = 47177
Fragmentation average = 26.6923
	minimum = 0
	maximum = 119
Injected packet rate average = 0.00788021
	minimum = 0 (at node 29)
	maximum = 0.027 (at node 16)
Accepted packet rate average = 0.009
	minimum = 0.003 (at node 98)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.141578
	minimum = 0 (at node 29)
	maximum = 0.486 (at node 16)
Accepted flit rate average= 0.161109
	minimum = 0.066 (at node 71)
	maximum = 0.299 (at node 34)
Injected packet length average = 17.9663
Accepted packet length average = 17.901
Total in-flight flits = 126188 (26026 measured)
latency change    = 0.0563277
throughput change = 0.0543109
Class 0:
Packet latency average = 2612.75
	minimum = 47
	maximum = 4060
Network latency average = 551.906
	minimum = 23
	maximum = 1911
Slowest packet = 12991
Flit latency average = 2518.28
	minimum = 6
	maximum = 4514
Slowest flit = 41272
Fragmentation average = 42.1915
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00828125
	minimum = 0.0005 (at node 42)
	maximum = 0.0235 (at node 121)
Accepted packet rate average = 0.00896094
	minimum = 0.004 (at node 71)
	maximum = 0.014 (at node 160)
Injected flit rate average = 0.148898
	minimum = 0.009 (at node 84)
	maximum = 0.4205 (at node 121)
Accepted flit rate average= 0.161466
	minimum = 0.069 (at node 71)
	maximum = 0.25 (at node 160)
Injected packet length average = 17.9802
Accepted packet length average = 18.0189
Total in-flight flits = 125052 (52710 measured)
latency change    = 0.296946
throughput change = 0.00220957
Class 0:
Packet latency average = 3285.16
	minimum = 47
	maximum = 5045
Network latency average = 992.69
	minimum = 23
	maximum = 2966
Slowest packet = 12991
Flit latency average = 2753.97
	minimum = 6
	maximum = 5353
Slowest flit = 70577
Fragmentation average = 55.5488
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00824653
	minimum = 0.000666667 (at node 26)
	maximum = 0.0223333 (at node 121)
Accepted packet rate average = 0.0089375
	minimum = 0.00466667 (at node 36)
	maximum = 0.0136667 (at node 16)
Injected flit rate average = 0.148372
	minimum = 0.012 (at node 26)
	maximum = 0.400667 (at node 121)
Accepted flit rate average= 0.160925
	minimum = 0.0793333 (at node 36)
	maximum = 0.251667 (at node 16)
Injected packet length average = 17.992
Accepted packet length average = 18.0056
Total in-flight flits = 122712 (74410 measured)
latency change    = 0.20468
throughput change = 0.00336056
Class 0:
Packet latency average = 3888.25
	minimum = 47
	maximum = 6046
Network latency average = 1517.33
	minimum = 23
	maximum = 3938
Slowest packet = 12991
Flit latency average = 2981.02
	minimum = 6
	maximum = 6323
Slowest flit = 73259
Fragmentation average = 59.8292
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00835938
	minimum = 0.0015 (at node 28)
	maximum = 0.01775 (at node 121)
Accepted packet rate average = 0.00896354
	minimum = 0.0045 (at node 36)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.150434
	minimum = 0.027 (at node 28)
	maximum = 0.321 (at node 121)
Accepted flit rate average= 0.161355
	minimum = 0.0775 (at node 36)
	maximum = 0.2545 (at node 16)
Injected packet length average = 17.9958
Accepted packet length average = 18.0013
Total in-flight flits = 121508 (93342 measured)
latency change    = 0.155105
throughput change = 0.00266568
Class 0:
Packet latency average = 4513.31
	minimum = 47
	maximum = 6776
Network latency average = 1989.75
	minimum = 23
	maximum = 4900
Slowest packet = 12991
Flit latency average = 3133.74
	minimum = 6
	maximum = 7254
Slowest flit = 71351
Fragmentation average = 64.15
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00843854
	minimum = 0.0016 (at node 28)
	maximum = 0.0178 (at node 121)
Accepted packet rate average = 0.00897604
	minimum = 0.0044 (at node 36)
	maximum = 0.0126 (at node 83)
Injected flit rate average = 0.151793
	minimum = 0.0288 (at node 28)
	maximum = 0.3216 (at node 121)
Accepted flit rate average= 0.161497
	minimum = 0.0764 (at node 36)
	maximum = 0.2294 (at node 83)
Injected packet length average = 17.988
Accepted packet length average = 17.992
Total in-flight flits = 120686 (105930 measured)
latency change    = 0.138494
throughput change = 0.000875597
Class 0:
Packet latency average = 5074.88
	minimum = 47
	maximum = 7655
Network latency average = 2348.93
	minimum = 23
	maximum = 5900
Slowest packet = 12991
Flit latency average = 3236.76
	minimum = 6
	maximum = 7818
Slowest flit = 87137
Fragmentation average = 65.9436
	minimum = 0
	maximum = 182
Injected packet rate average = 0.00851649
	minimum = 0.002 (at node 42)
	maximum = 0.0171667 (at node 121)
Accepted packet rate average = 0.0089375
	minimum = 0.00466667 (at node 36)
	maximum = 0.0123333 (at node 129)
Injected flit rate average = 0.153182
	minimum = 0.0376667 (at node 42)
	maximum = 0.31 (at node 121)
Accepted flit rate average= 0.16092
	minimum = 0.0843333 (at node 36)
	maximum = 0.2215 (at node 129)
Injected packet length average = 17.9865
Accepted packet length average = 18.0051
Total in-flight flits = 120997 (113932 measured)
latency change    = 0.110656
throughput change = 0.00358399
Class 0:
Packet latency average = 5635.4
	minimum = 47
	maximum = 8432
Network latency average = 2668.9
	minimum = 23
	maximum = 6837
Slowest packet = 12991
Flit latency average = 3316.23
	minimum = 6
	maximum = 8763
Slowest flit = 81503
Fragmentation average = 66.8333
	minimum = 0
	maximum = 182
Injected packet rate average = 0.0086503
	minimum = 0.002 (at node 42)
	maximum = 0.0164286 (at node 121)
Accepted packet rate average = 0.00899107
	minimum = 0.00542857 (at node 36)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.155496
	minimum = 0.0374286 (at node 42)
	maximum = 0.296571 (at node 121)
Accepted flit rate average= 0.161728
	minimum = 0.0965714 (at node 36)
	maximum = 0.218 (at node 128)
Injected packet length average = 17.9758
Accepted packet length average = 17.9876
Total in-flight flits = 121487 (118411 measured)
latency change    = 0.0994645
throughput change = 0.00499321
Draining all recorded packets ...
Class 0:
Remaining flits: 97344 97345 97346 97347 97348 97349 97350 97351 97352 97353 [...] (123300 flits)
Measured flits: 233862 233863 233864 233865 233866 233867 233868 233869 233870 233871 [...] (121778 flits)
Class 0:
Remaining flits: 97344 97345 97346 97347 97348 97349 97350 97351 97352 97353 [...] (120760 flits)
Measured flits: 233910 233911 233912 233913 233914 233915 233916 233917 233918 233919 [...] (120165 flits)
Class 0:
Remaining flits: 176508 176509 176510 176511 176512 176513 176514 176515 176516 176517 [...] (118639 flits)
Measured flits: 233910 233911 233912 233913 233914 233915 233916 233917 233918 233919 [...] (118441 flits)
Class 0:
Remaining flits: 184392 184393 184394 184395 184396 184397 184398 184399 184400 184401 [...] (117924 flits)
Measured flits: 233918 233919 233920 233921 233922 233923 233924 233925 233926 233927 [...] (117870 flits)
Class 0:
Remaining flits: 275958 275959 275960 275961 275962 275963 275964 275965 275966 275967 [...] (118606 flits)
Measured flits: 275958 275959 275960 275961 275962 275963 275964 275965 275966 275967 [...] (118606 flits)
Class 0:
Remaining flits: 279378 279379 279380 279381 279382 279383 279384 279385 279386 279387 [...] (117845 flits)
Measured flits: 279378 279379 279380 279381 279382 279383 279384 279385 279386 279387 [...] (117845 flits)
Class 0:
Remaining flits: 308250 308251 308252 308253 308254 308255 308256 308257 308258 308259 [...] (118065 flits)
Measured flits: 308250 308251 308252 308253 308254 308255 308256 308257 308258 308259 [...] (118065 flits)
Class 0:
Remaining flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (118993 flits)
Measured flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (118993 flits)
Class 0:
Remaining flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (118842 flits)
Measured flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (118842 flits)
Class 0:
Remaining flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (119975 flits)
Measured flits: 321570 321571 321572 321573 321574 321575 321576 321577 321578 321579 [...] (119975 flits)
Class 0:
Remaining flits: 335196 335197 335198 335199 335200 335201 335202 335203 335204 335205 [...] (119251 flits)
Measured flits: 335196 335197 335198 335199 335200 335201 335202 335203 335204 335205 [...] (119179 flits)
Class 0:
Remaining flits: 335196 335197 335198 335199 335200 335201 335202 335203 335204 335205 [...] (118257 flits)
Measured flits: 335196 335197 335198 335199 335200 335201 335202 335203 335204 335205 [...] (118005 flits)
Class 0:
Remaining flits: 337446 337447 337448 337449 337450 337451 337452 337453 337454 337455 [...] (117685 flits)
Measured flits: 337446 337447 337448 337449 337450 337451 337452 337453 337454 337455 [...] (117325 flits)
Class 0:
Remaining flits: 397890 397891 397892 397893 397894 397895 397896 397897 397898 397899 [...] (117486 flits)
Measured flits: 397890 397891 397892 397893 397894 397895 397896 397897 397898 397899 [...] (116838 flits)
Class 0:
Remaining flits: 397893 397894 397895 397896 397897 397898 397899 397900 397901 397902 [...] (118807 flits)
Measured flits: 397893 397894 397895 397896 397897 397898 397899 397900 397901 397902 [...] (117655 flits)
Class 0:
Remaining flits: 535428 535429 535430 535431 535432 535433 535434 535435 535436 535437 [...] (118663 flits)
Measured flits: 535428 535429 535430 535431 535432 535433 535434 535435 535436 535437 [...] (116799 flits)
Class 0:
Remaining flits: 564534 564535 564536 564537 564538 564539 564540 564541 564542 564543 [...] (120009 flits)
Measured flits: 564534 564535 564536 564537 564538 564539 564540 564541 564542 564543 [...] (117597 flits)
Class 0:
Remaining flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (118561 flits)
Measured flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (114479 flits)
Class 0:
Remaining flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (119060 flits)
Measured flits: 569394 569395 569396 569397 569398 569399 569400 569401 569402 569403 [...] (113426 flits)
Class 0:
Remaining flits: 675954 675955 675956 675957 675958 675959 675960 675961 675962 675963 [...] (120008 flits)
Measured flits: 675954 675955 675956 675957 675958 675959 675960 675961 675962 675963 [...] (113173 flits)
Class 0:
Remaining flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (116062 flits)
Measured flits: 679068 679069 679070 679071 679072 679073 679074 679075 679076 679077 [...] (107004 flits)
Class 0:
Remaining flits: 764208 764209 764210 764211 764212 764213 764214 764215 764216 764217 [...] (116898 flits)
Measured flits: 764208 764209 764210 764211 764212 764213 764214 764215 764216 764217 [...] (103853 flits)
Class 0:
Remaining flits: 794844 794845 794846 794847 794848 794849 794850 794851 794852 794853 [...] (117754 flits)
Measured flits: 794844 794845 794846 794847 794848 794849 794850 794851 794852 794853 [...] (100089 flits)
Class 0:
Remaining flits: 816876 816877 816878 816879 816880 816881 816882 816883 816884 816885 [...] (118813 flits)
Measured flits: 816876 816877 816878 816879 816880 816881 816882 816883 816884 816885 [...] (95719 flits)
Class 0:
Remaining flits: 816876 816877 816878 816879 816880 816881 816882 816883 816884 816885 [...] (119120 flits)
Measured flits: 816876 816877 816878 816879 816880 816881 816882 816883 816884 816885 [...] (88385 flits)
Class 0:
Remaining flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (120307 flits)
Measured flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (81524 flits)
Class 0:
Remaining flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (119138 flits)
Measured flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (71621 flits)
Class 0:
Remaining flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (119042 flits)
Measured flits: 893340 893341 893342 893343 893344 893345 893346 893347 893348 893349 [...] (62093 flits)
Class 0:
Remaining flits: 994824 994825 994826 994827 994828 994829 994830 994831 994832 994833 [...] (119605 flits)
Measured flits: 1007028 1007029 1007030 1007031 1007032 1007033 1007034 1007035 1007036 1007037 [...] (52210 flits)
Class 0:
Remaining flits: 1007034 1007035 1007036 1007037 1007038 1007039 1007040 1007041 1007042 1007043 [...] (118264 flits)
Measured flits: 1007034 1007035 1007036 1007037 1007038 1007039 1007040 1007041 1007042 1007043 [...] (43989 flits)
Class 0:
Remaining flits: 1041030 1041031 1041032 1041033 1041034 1041035 1041036 1041037 1041038 1041039 [...] (118170 flits)
Measured flits: 1041030 1041031 1041032 1041033 1041034 1041035 1041036 1041037 1041038 1041039 [...] (35351 flits)
Class 0:
Remaining flits: 1041030 1041031 1041032 1041033 1041034 1041035 1041036 1041037 1041038 1041039 [...] (117832 flits)
Measured flits: 1041030 1041031 1041032 1041033 1041034 1041035 1041036 1041037 1041038 1041039 [...] (27818 flits)
Class 0:
Remaining flits: 1053126 1053127 1053128 1053129 1053130 1053131 1053132 1053133 1053134 1053135 [...] (121076 flits)
Measured flits: 1053126 1053127 1053128 1053129 1053130 1053131 1053132 1053133 1053134 1053135 [...] (22643 flits)
Class 0:
Remaining flits: 1053126 1053127 1053128 1053129 1053130 1053131 1053132 1053133 1053134 1053135 [...] (121337 flits)
Measured flits: 1053126 1053127 1053128 1053129 1053130 1053131 1053132 1053133 1053134 1053135 [...] (17078 flits)
Class 0:
Remaining flits: 1054134 1054135 1054136 1054137 1054138 1054139 1054140 1054141 1054142 1054143 [...] (121502 flits)
Measured flits: 1054134 1054135 1054136 1054137 1054138 1054139 1054140 1054141 1054142 1054143 [...] (13030 flits)
Class 0:
Remaining flits: 1054710 1054711 1054712 1054713 1054714 1054715 1054716 1054717 1054718 1054719 [...] (119493 flits)
Measured flits: 1054710 1054711 1054712 1054713 1054714 1054715 1054716 1054717 1054718 1054719 [...] (9404 flits)
Class 0:
Remaining flits: 1134540 1134541 1134542 1134543 1134544 1134545 1134546 1134547 1134548 1134549 [...] (117834 flits)
Measured flits: 1134540 1134541 1134542 1134543 1134544 1134545 1134546 1134547 1134548 1134549 [...] (6255 flits)
Class 0:
Remaining flits: 1134540 1134541 1134542 1134543 1134544 1134545 1134546 1134547 1134548 1134549 [...] (118665 flits)
Measured flits: 1134540 1134541 1134542 1134543 1134544 1134545 1134546 1134547 1134548 1134549 [...] (4192 flits)
Class 0:
Remaining flits: 1293444 1293445 1293446 1293447 1293448 1293449 1293450 1293451 1293452 1293453 [...] (117252 flits)
Measured flits: 1293444 1293445 1293446 1293447 1293448 1293449 1293450 1293451 1293452 1293453 [...] (2822 flits)
Class 0:
Remaining flits: 1293444 1293445 1293446 1293447 1293448 1293449 1293450 1293451 1293452 1293453 [...] (117253 flits)
Measured flits: 1293444 1293445 1293446 1293447 1293448 1293449 1293450 1293451 1293452 1293453 [...] (1606 flits)
Class 0:
Remaining flits: 1332900 1332901 1332902 1332903 1332904 1332905 1332906 1332907 1332908 1332909 [...] (118510 flits)
Measured flits: 1340568 1340569 1340570 1340571 1340572 1340573 1340574 1340575 1340576 1340577 [...] (846 flits)
Class 0:
Remaining flits: 1332900 1332901 1332902 1332903 1332904 1332905 1332906 1332907 1332908 1332909 [...] (117304 flits)
Measured flits: 1342458 1342459 1342460 1342461 1342462 1342463 1342464 1342465 1342466 1342467 [...] (422 flits)
Class 0:
Remaining flits: 1353186 1353187 1353188 1353189 1353190 1353191 1353192 1353193 1353194 1353195 [...] (118594 flits)
Measured flits: 1472184 1472185 1472186 1472187 1472188 1472189 1472190 1472191 1472192 1472193 [...] (162 flits)
Class 0:
Remaining flits: 1355868 1355869 1355870 1355871 1355872 1355873 1355874 1355875 1355876 1355877 [...] (119372 flits)
Measured flits: 1616994 1616995 1616996 1616997 1616998 1616999 1617000 1617001 1617002 1617003 [...] (90 flits)
Class 0:
Remaining flits: 1424070 1424071 1424072 1424073 1424074 1424075 1424076 1424077 1424078 1424079 [...] (118570 flits)
Measured flits: 1616994 1616995 1616996 1616997 1616998 1616999 1617000 1617001 1617002 1617003 [...] (45 flits)
Class 0:
Remaining flits: 1451700 1451701 1451702 1451703 1451704 1451705 1451706 1451707 1451708 1451709 [...] (118205 flits)
Measured flits: 1621674 1621675 1621676 1621677 1621678 1621679 1621680 1621681 1621682 1621683 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1552356 1552357 1552358 1552359 1552360 1552361 1552362 1552363 1552364 1552365 [...] (90209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1606032 1606033 1606034 1606035 1606036 1606037 1606038 1606039 1606040 1606041 [...] (61534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1626156 1626157 1626158 1626159 1626160 1626161 1626162 1626163 1626164 1626165 [...] (32914 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1646478 1646479 1646480 1646481 1646482 1646483 1646484 1646485 1646486 1646487 [...] (9009 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1774179 1774180 1774181 1774182 1774183 1774184 1774185 1774186 1774187 1776680 [...] (341 flits)
Measured flits: (0 flits)
Time taken is 62011 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 18040.4 (1 samples)
	minimum = 47 (1 samples)
	maximum = 46876 (1 samples)
Network latency average = 3852.49 (1 samples)
	minimum = 23 (1 samples)
	maximum = 17040 (1 samples)
Flit latency average = 3789.19 (1 samples)
	minimum = 6 (1 samples)
	maximum = 17023 (1 samples)
Fragmentation average = 71.1308 (1 samples)
	minimum = 0 (1 samples)
	maximum = 182 (1 samples)
Injected packet rate average = 0.0086503 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.0164286 (1 samples)
Accepted packet rate average = 0.00899107 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.155496 (1 samples)
	minimum = 0.0374286 (1 samples)
	maximum = 0.296571 (1 samples)
Accepted flit rate average = 0.161728 (1 samples)
	minimum = 0.0965714 (1 samples)
	maximum = 0.218 (1 samples)
Injected packet size average = 17.9758 (1 samples)
Accepted packet size average = 17.9876 (1 samples)
Hops average = 5.05756 (1 samples)
Total run time 46.9277
