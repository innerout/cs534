BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 231.904
	minimum = 23
	maximum = 927
Network latency average = 185.321
	minimum = 23
	maximum = 771
Slowest packet = 72
Flit latency average = 146.143
	minimum = 6
	maximum = 827
Slowest flit = 8496
Fragmentation average = 49.0695
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0119688
	minimum = 0 (at node 8)
	maximum = 0.04 (at node 98)
Accepted packet rate average = 0.00832292
	minimum = 0.003 (at node 23)
	maximum = 0.016 (at node 152)
Injected flit rate average = 0.21349
	minimum = 0 (at node 8)
	maximum = 0.708 (at node 98)
Accepted flit rate average= 0.156927
	minimum = 0.054 (at node 174)
	maximum = 0.288 (at node 152)
Injected packet length average = 17.8372
Accepted packet length average = 18.8548
Total in-flight flits = 11234 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 330.857
	minimum = 23
	maximum = 1678
Network latency average = 278.556
	minimum = 23
	maximum = 1568
Slowest packet = 672
Flit latency average = 233.817
	minimum = 6
	maximum = 1551
Slowest flit = 12059
Fragmentation average = 58.4653
	minimum = 0
	maximum = 131
Injected packet rate average = 0.011638
	minimum = 0 (at node 104)
	maximum = 0.029 (at node 11)
Accepted packet rate average = 0.00888802
	minimum = 0.0045 (at node 25)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.208784
	minimum = 0 (at node 104)
	maximum = 0.522 (at node 11)
Accepted flit rate average= 0.163477
	minimum = 0.081 (at node 25)
	maximum = 0.261 (at node 22)
Injected packet length average = 17.9398
Accepted packet length average = 18.3929
Total in-flight flits = 17667 (0 measured)
latency change    = 0.299081
throughput change = 0.0400637
Class 0:
Packet latency average = 570.365
	minimum = 23
	maximum = 2401
Network latency average = 512.617
	minimum = 23
	maximum = 2157
Slowest packet = 783
Flit latency average = 465.845
	minimum = 6
	maximum = 2218
Slowest flit = 30256
Fragmentation average = 68.446
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0117917
	minimum = 0 (at node 1)
	maximum = 0.044 (at node 113)
Accepted packet rate average = 0.00926042
	minimum = 0.003 (at node 91)
	maximum = 0.018 (at node 113)
Injected flit rate average = 0.211458
	minimum = 0 (at node 1)
	maximum = 0.792 (at node 113)
Accepted flit rate average= 0.167922
	minimum = 0.062 (at node 91)
	maximum = 0.307 (at node 113)
Injected packet length average = 17.9329
Accepted packet length average = 18.1333
Total in-flight flits = 26196 (0 measured)
latency change    = 0.419921
throughput change = 0.0264725
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 349.27
	minimum = 23
	maximum = 1186
Network latency average = 302.529
	minimum = 23
	maximum = 942
Slowest packet = 6762
Flit latency average = 652.346
	minimum = 6
	maximum = 3028
Slowest flit = 35063
Fragmentation average = 59.2076
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0118385
	minimum = 0 (at node 18)
	maximum = 0.04 (at node 10)
Accepted packet rate average = 0.0093125
	minimum = 0.004 (at node 4)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.213234
	minimum = 0 (at node 18)
	maximum = 0.737 (at node 143)
Accepted flit rate average= 0.167464
	minimum = 0.066 (at node 95)
	maximum = 0.362 (at node 16)
Injected packet length average = 18.0119
Accepted packet length average = 17.9827
Total in-flight flits = 34993 (28009 measured)
latency change    = 0.633022
throughput change = 0.00273691
Class 0:
Packet latency average = 621.78
	minimum = 23
	maximum = 2523
Network latency average = 568.02
	minimum = 23
	maximum = 1906
Slowest packet = 6742
Flit latency average = 748.439
	minimum = 6
	maximum = 3416
Slowest flit = 63264
Fragmentation average = 67.5538
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0116406
	minimum = 0.0015 (at node 64)
	maximum = 0.033 (at node 154)
Accepted packet rate average = 0.00919531
	minimum = 0.005 (at node 36)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.209589
	minimum = 0.027 (at node 64)
	maximum = 0.587 (at node 154)
Accepted flit rate average= 0.165268
	minimum = 0.083 (at node 36)
	maximum = 0.2645 (at node 16)
Injected packet length average = 18.0049
Accepted packet length average = 17.9731
Total in-flight flits = 43283 (41414 measured)
latency change    = 0.438275
throughput change = 0.0132833
Class 0:
Packet latency average = 834.781
	minimum = 23
	maximum = 2867
Network latency average = 780.644
	minimum = 23
	maximum = 2812
Slowest packet = 6742
Flit latency average = 862.714
	minimum = 6
	maximum = 3882
Slowest flit = 84758
Fragmentation average = 69.0326
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0110608
	minimum = 0.002 (at node 89)
	maximum = 0.0256667 (at node 46)
Accepted packet rate average = 0.00913368
	minimum = 0.00533333 (at node 36)
	maximum = 0.0136667 (at node 120)
Injected flit rate average = 0.1993
	minimum = 0.036 (at node 89)
	maximum = 0.462 (at node 46)
Accepted flit rate average= 0.164115
	minimum = 0.0913333 (at node 36)
	maximum = 0.245333 (at node 120)
Injected packet length average = 18.0187
Accepted packet length average = 17.9681
Total in-flight flits = 46452 (46155 measured)
latency change    = 0.255158
throughput change = 0.00702951
Draining remaining packets ...
Class 0:
Remaining flits: 99720 99721 99722 99723 99724 99725 99726 99727 99728 99729 [...] (18860 flits)
Measured flits: 123084 123085 123086 123087 123088 123089 123090 123091 123092 123093 [...] (18763 flits)
Class 0:
Remaining flits: 165835 165836 165837 165838 165839 165840 165841 165842 165843 165844 [...] (1433 flits)
Measured flits: 165835 165836 165837 165838 165839 165840 165841 165842 165843 165844 [...] (1433 flits)
Time taken is 8519 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1305.06 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4883 (1 samples)
Network latency average = 1235.11 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4718 (1 samples)
Flit latency average = 1190.78 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5031 (1 samples)
Fragmentation average = 67.8353 (1 samples)
	minimum = 0 (1 samples)
	maximum = 155 (1 samples)
Injected packet rate average = 0.0110608 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.0256667 (1 samples)
Accepted packet rate average = 0.00913368 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0136667 (1 samples)
Injected flit rate average = 0.1993 (1 samples)
	minimum = 0.036 (1 samples)
	maximum = 0.462 (1 samples)
Accepted flit rate average = 0.164115 (1 samples)
	minimum = 0.0913333 (1 samples)
	maximum = 0.245333 (1 samples)
Injected packet size average = 18.0187 (1 samples)
Accepted packet size average = 17.9681 (1 samples)
Hops average = 5.09754 (1 samples)
Total run time 5.74884
