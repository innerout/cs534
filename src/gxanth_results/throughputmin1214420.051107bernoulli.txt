BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 335.546
	minimum = 22
	maximum = 857
Network latency average = 310.178
	minimum = 22
	maximum = 789
Slowest packet = 433
Flit latency average = 287.475
	minimum = 5
	maximum = 809
Slowest flit = 25914
Fragmentation average = 26.4043
	minimum = 0
	maximum = 98
Injected packet rate average = 0.0451406
	minimum = 0.031 (at node 88)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.015125
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.805219
	minimum = 0.541 (at node 88)
	maximum = 0.995 (at node 15)
Accepted flit rate average= 0.279406
	minimum = 0.118 (at node 174)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.838
Accepted packet length average = 18.4731
Total in-flight flits = 102630 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 668.481
	minimum = 22
	maximum = 1682
Network latency average = 628.885
	minimum = 22
	maximum = 1630
Slowest packet = 433
Flit latency average = 604.9
	minimum = 5
	maximum = 1595
Slowest flit = 53927
Fragmentation average = 27.0564
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0365313
	minimum = 0.0205 (at node 88)
	maximum = 0.051 (at node 171)
Accepted packet rate average = 0.0151094
	minimum = 0.009 (at node 30)
	maximum = 0.021 (at node 173)
Injected flit rate average = 0.656068
	minimum = 0.369 (at node 88)
	maximum = 0.918 (at node 171)
Accepted flit rate average= 0.274956
	minimum = 0.167 (at node 30)
	maximum = 0.378 (at node 173)
Injected packet length average = 17.9591
Accepted packet length average = 18.1977
Total in-flight flits = 149027 (0 measured)
latency change    = 0.498046
throughput change = 0.0161863
Class 0:
Packet latency average = 1721.31
	minimum = 182
	maximum = 2618
Network latency average = 1597.7
	minimum = 22
	maximum = 2502
Slowest packet = 4010
Flit latency average = 1580.12
	minimum = 5
	maximum = 2485
Slowest flit = 69424
Fragmentation average = 21.7261
	minimum = 0
	maximum = 219
Injected packet rate average = 0.0148438
	minimum = 0.002 (at node 7)
	maximum = 0.034 (at node 173)
Accepted packet rate average = 0.0148333
	minimum = 0.007 (at node 126)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.267141
	minimum = 0.036 (at node 7)
	maximum = 0.616 (at node 173)
Accepted flit rate average= 0.266021
	minimum = 0.126 (at node 132)
	maximum = 0.486 (at node 16)
Injected packet length average = 17.9968
Accepted packet length average = 17.934
Total in-flight flits = 149431 (0 measured)
latency change    = 0.611643
throughput change = 0.0335872
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1760.89
	minimum = 853
	maximum = 2443
Network latency average = 45.0517
	minimum = 22
	maximum = 538
Slowest packet = 17028
Flit latency average = 2113.77
	minimum = 5
	maximum = 3209
Slowest flit = 123267
Fragmentation average = 5.12069
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0151094
	minimum = 0.002 (at node 62)
	maximum = 0.036 (at node 86)
Accepted packet rate average = 0.0146927
	minimum = 0.007 (at node 85)
	maximum = 0.025 (at node 181)
Injected flit rate average = 0.272448
	minimum = 0.036 (at node 62)
	maximum = 0.646 (at node 86)
Accepted flit rate average= 0.263604
	minimum = 0.143 (at node 85)
	maximum = 0.436 (at node 181)
Injected packet length average = 18.0317
Accepted packet length average = 17.9412
Total in-flight flits = 151235 (50307 measured)
latency change    = 0.0224779
throughput change = 0.00916779
Class 0:
Packet latency average = 2600.05
	minimum = 853
	maximum = 3782
Network latency average = 740.308
	minimum = 22
	maximum = 1911
Slowest packet = 17028
Flit latency average = 2329.98
	minimum = 5
	maximum = 4182
Slowest flit = 113523
Fragmentation average = 11.2293
	minimum = 0
	maximum = 93
Injected packet rate average = 0.014901
	minimum = 0.0045 (at node 153)
	maximum = 0.0275 (at node 111)
Accepted packet rate average = 0.0147682
	minimum = 0.0085 (at node 36)
	maximum = 0.0215 (at node 11)
Injected flit rate average = 0.268349
	minimum = 0.081 (at node 153)
	maximum = 0.495 (at node 111)
Accepted flit rate average= 0.265404
	minimum = 0.153 (at node 36)
	maximum = 0.3955 (at node 56)
Injected packet length average = 18.0087
Accepted packet length average = 17.9713
Total in-flight flits = 150530 (94383 measured)
latency change    = 0.322749
throughput change = 0.00678016
Class 0:
Packet latency average = 3328.47
	minimum = 853
	maximum = 4774
Network latency average = 1370.95
	minimum = 22
	maximum = 2985
Slowest packet = 17028
Flit latency average = 2485.46
	minimum = 5
	maximum = 4808
Slowest flit = 181241
Fragmentation average = 14.7395
	minimum = 0
	maximum = 104
Injected packet rate average = 0.0150469
	minimum = 0.007 (at node 110)
	maximum = 0.025 (at node 21)
Accepted packet rate average = 0.0148125
	minimum = 0.00933333 (at node 161)
	maximum = 0.02 (at node 11)
Injected flit rate average = 0.270859
	minimum = 0.126 (at node 110)
	maximum = 0.45 (at node 86)
Accepted flit rate average= 0.266604
	minimum = 0.172 (at node 161)
	maximum = 0.357333 (at node 11)
Injected packet length average = 18.001
Accepted packet length average = 17.9986
Total in-flight flits = 151909 (128643 measured)
latency change    = 0.218846
throughput change = 0.00450301
Draining remaining packets ...
Class 0:
Remaining flits: 224964 224965 224966 224967 224968 224969 224970 224971 224972 224973 [...] (101781 flits)
Measured flits: 306108 306109 306110 306111 306112 306113 306114 306115 306116 306117 [...] (97832 flits)
Class 0:
Remaining flits: 273744 273745 273746 273747 273748 273749 273750 273751 273752 273753 [...] (53340 flits)
Measured flits: 306288 306289 306290 306291 306292 306293 306294 306295 306296 306297 [...] (53219 flits)
Class 0:
Remaining flits: 329148 329149 329150 329151 329152 329153 329154 329155 329156 329157 [...] (8201 flits)
Measured flits: 329148 329149 329150 329151 329152 329153 329154 329155 329156 329157 [...] (8201 flits)
Time taken is 9957 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5087.24 (1 samples)
	minimum = 853 (1 samples)
	maximum = 7647 (1 samples)
Network latency average = 2755.71 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5779 (1 samples)
Flit latency average = 2835.4 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6282 (1 samples)
Fragmentation average = 18.3774 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0150469 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.025 (1 samples)
Accepted packet rate average = 0.0148125 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.270859 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.45 (1 samples)
Accepted flit rate average = 0.266604 (1 samples)
	minimum = 0.172 (1 samples)
	maximum = 0.357333 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 17.9986 (1 samples)
Hops average = 5.06218 (1 samples)
Total run time 13.3817
