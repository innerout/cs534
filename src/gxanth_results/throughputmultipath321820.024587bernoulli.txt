BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 241.266
	minimum = 22
	maximum = 746
Network latency average = 233.363
	minimum = 22
	maximum = 736
Slowest packet = 744
Flit latency average = 199.4
	minimum = 5
	maximum = 757
Slowest flit = 18618
Fragmentation average = 47.304
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0240521
	minimum = 0.012 (at node 54)
	maximum = 0.036 (at node 149)
Accepted packet rate average = 0.0138281
	minimum = 0.006 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.42876
	minimum = 0.216 (at node 54)
	maximum = 0.648 (at node 149)
Accepted flit rate average= 0.258177
	minimum = 0.132 (at node 174)
	maximum = 0.464 (at node 44)
Injected packet length average = 17.8263
Accepted packet length average = 18.6704
Total in-flight flits = 33716 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 430.812
	minimum = 22
	maximum = 1487
Network latency average = 411.941
	minimum = 22
	maximum = 1471
Slowest packet = 1929
Flit latency average = 372.965
	minimum = 5
	maximum = 1454
Slowest flit = 38825
Fragmentation average = 53.3212
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0215026
	minimum = 0.0125 (at node 48)
	maximum = 0.028 (at node 25)
Accepted packet rate average = 0.0144479
	minimum = 0.0085 (at node 30)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.384768
	minimum = 0.225 (at node 48)
	maximum = 0.504 (at node 25)
Accepted flit rate average= 0.264924
	minimum = 0.158 (at node 30)
	maximum = 0.405 (at node 152)
Injected packet length average = 17.894
Accepted packet length average = 18.3365
Total in-flight flits = 47795 (0 measured)
latency change    = 0.439975
throughput change = 0.0254691
Class 0:
Packet latency average = 992.972
	minimum = 22
	maximum = 2171
Network latency average = 846.42
	minimum = 22
	maximum = 2165
Slowest packet = 3492
Flit latency average = 800.006
	minimum = 5
	maximum = 2218
Slowest flit = 66044
Fragmentation average = 61.9523
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0160208
	minimum = 0.004 (at node 130)
	maximum = 0.03 (at node 181)
Accepted packet rate average = 0.0148385
	minimum = 0.006 (at node 40)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.287214
	minimum = 0.069 (at node 130)
	maximum = 0.54 (at node 181)
Accepted flit rate average= 0.267531
	minimum = 0.096 (at node 40)
	maximum = 0.466 (at node 120)
Injected packet length average = 17.9275
Accepted packet length average = 18.0295
Total in-flight flits = 52013 (0 measured)
latency change    = 0.566139
throughput change = 0.0097438
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1149.77
	minimum = 25
	maximum = 2189
Network latency average = 370.008
	minimum = 22
	maximum = 976
Slowest packet = 11416
Flit latency average = 922.349
	minimum = 5
	maximum = 2754
Slowest flit = 85445
Fragmentation average = 32.3996
	minimum = 0
	maximum = 209
Injected packet rate average = 0.0150417
	minimum = 0.005 (at node 22)
	maximum = 0.029 (at node 93)
Accepted packet rate average = 0.0147604
	minimum = 0.006 (at node 88)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.270432
	minimum = 0.091 (at node 95)
	maximum = 0.514 (at node 93)
Accepted flit rate average= 0.267557
	minimum = 0.095 (at node 88)
	maximum = 0.452 (at node 83)
Injected packet length average = 17.9789
Accepted packet length average = 18.1267
Total in-flight flits = 52500 (41700 measured)
latency change    = 0.13637
throughput change = 9.73312e-05
Class 0:
Packet latency average = 1596.1
	minimum = 25
	maximum = 3098
Network latency average = 768.175
	minimum = 22
	maximum = 1980
Slowest packet = 11416
Flit latency average = 943.15
	minimum = 5
	maximum = 2926
Slowest flit = 99593
Fragmentation average = 57.9373
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0150234
	minimum = 0.0055 (at node 151)
	maximum = 0.0225 (at node 135)
Accepted packet rate average = 0.0148021
	minimum = 0.008 (at node 36)
	maximum = 0.022 (at node 66)
Injected flit rate average = 0.270143
	minimum = 0.1035 (at node 151)
	maximum = 0.4115 (at node 135)
Accepted flit rate average= 0.267578
	minimum = 0.1455 (at node 36)
	maximum = 0.4035 (at node 129)
Injected packet length average = 17.9815
Accepted packet length average = 18.0771
Total in-flight flits = 52961 (52332 measured)
latency change    = 0.27964
throughput change = 7.78589e-05
Class 0:
Packet latency average = 1872.97
	minimum = 25
	maximum = 3789
Network latency average = 899.344
	minimum = 22
	maximum = 2789
Slowest packet = 11416
Flit latency average = 953.921
	minimum = 5
	maximum = 3430
Slowest flit = 173836
Fragmentation average = 66.4605
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0149236
	minimum = 0.00366667 (at node 151)
	maximum = 0.022 (at node 117)
Accepted packet rate average = 0.0148351
	minimum = 0.00933333 (at node 89)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.268688
	minimum = 0.069 (at node 151)
	maximum = 0.392667 (at node 117)
Accepted flit rate average= 0.26759
	minimum = 0.171667 (at node 89)
	maximum = 0.379667 (at node 128)
Injected packet length average = 18.0042
Accepted packet length average = 18.0377
Total in-flight flits = 52447 (52386 measured)
latency change    = 0.147822
throughput change = 4.54156e-05
Draining remaining packets ...
Class 0:
Remaining flits: 235818 235819 235820 235821 235822 235823 235824 235825 235826 235827 [...] (7410 flits)
Measured flits: 235818 235819 235820 235821 235822 235823 235824 235825 235826 235827 [...] (7410 flits)
Time taken is 7945 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2166.61 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5100 (1 samples)
Network latency average = 1032.31 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4214 (1 samples)
Flit latency average = 1019.66 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4173 (1 samples)
Fragmentation average = 69.559 (1 samples)
	minimum = 0 (1 samples)
	maximum = 310 (1 samples)
Injected packet rate average = 0.0149236 (1 samples)
	minimum = 0.00366667 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0148351 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.268688 (1 samples)
	minimum = 0.069 (1 samples)
	maximum = 0.392667 (1 samples)
Accepted flit rate average = 0.26759 (1 samples)
	minimum = 0.171667 (1 samples)
	maximum = 0.379667 (1 samples)
Injected packet size average = 18.0042 (1 samples)
Accepted packet size average = 18.0377 (1 samples)
Hops average = 5.10108 (1 samples)
Total run time 9.05758
