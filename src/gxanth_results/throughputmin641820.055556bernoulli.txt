BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 354.958
	minimum = 22
	maximum = 904
Network latency average = 324.658
	minimum = 22
	maximum = 850
Slowest packet = 674
Flit latency average = 282.653
	minimum = 5
	maximum = 840
Slowest flit = 20837
Fragmentation average = 141.318
	minimum = 0
	maximum = 605
Injected packet rate average = 0.0402865
	minimum = 0.022 (at node 0)
	maximum = 0.052 (at node 81)
Accepted packet rate average = 0.013526
	minimum = 0.006 (at node 30)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.718714
	minimum = 0.393 (at node 0)
	maximum = 0.933 (at node 81)
Accepted flit rate average= 0.286453
	minimum = 0.139 (at node 116)
	maximum = 0.499 (at node 44)
Injected packet length average = 17.8401
Accepted packet length average = 21.1779
Total in-flight flits = 84303 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 739.901
	minimum = 22
	maximum = 1828
Network latency average = 666.602
	minimum = 22
	maximum = 1757
Slowest packet = 1430
Flit latency average = 586.41
	minimum = 5
	maximum = 1747
Slowest flit = 36427
Fragmentation average = 216.724
	minimum = 0
	maximum = 860
Injected packet rate average = 0.0281276
	minimum = 0.015 (at node 0)
	maximum = 0.0365 (at node 119)
Accepted packet rate average = 0.0143099
	minimum = 0.0085 (at node 62)
	maximum = 0.0205 (at node 44)
Injected flit rate average = 0.503221
	minimum = 0.265 (at node 0)
	maximum = 0.655 (at node 119)
Accepted flit rate average= 0.276906
	minimum = 0.1655 (at node 153)
	maximum = 0.383 (at node 44)
Injected packet length average = 17.8907
Accepted packet length average = 19.3507
Total in-flight flits = 88356 (0 measured)
latency change    = 0.520263
throughput change = 0.0344769
Class 0:
Packet latency average = 1812.35
	minimum = 649
	maximum = 2596
Network latency average = 1515.89
	minimum = 22
	maximum = 2451
Slowest packet = 2858
Flit latency average = 1429.24
	minimum = 5
	maximum = 2456
Slowest flit = 81408
Fragmentation average = 233.041
	minimum = 0
	maximum = 843
Injected packet rate average = 0.0162656
	minimum = 0.004 (at node 152)
	maximum = 0.031 (at node 107)
Accepted packet rate average = 0.0154688
	minimum = 0.007 (at node 1)
	maximum = 0.025 (at node 78)
Injected flit rate average = 0.291984
	minimum = 0.082 (at node 152)
	maximum = 0.559 (at node 187)
Accepted flit rate average= 0.270542
	minimum = 0.085 (at node 1)
	maximum = 0.455 (at node 34)
Injected packet length average = 17.951
Accepted packet length average = 17.4896
Total in-flight flits = 92608 (0 measured)
latency change    = 0.591744
throughput change = 0.0235253
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2075.44
	minimum = 1179
	maximum = 3206
Network latency average = 84.7229
	minimum = 23
	maximum = 984
Slowest packet = 13979
Flit latency average = 1659.87
	minimum = 5
	maximum = 3419
Slowest flit = 82439
Fragmentation average = 8.19048
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0155313
	minimum = 0.002 (at node 72)
	maximum = 0.033 (at node 189)
Accepted packet rate average = 0.0149115
	minimum = 0.005 (at node 185)
	maximum = 0.024 (at node 120)
Injected flit rate average = 0.280734
	minimum = 0.036 (at node 72)
	maximum = 0.591 (at node 189)
Accepted flit rate average= 0.268474
	minimum = 0.086 (at node 185)
	maximum = 0.421 (at node 66)
Injected packet length average = 18.0755
Accepted packet length average = 18.0045
Total in-flight flits = 94827 (49481 measured)
latency change    = 0.126764
throughput change = 0.00770171
Class 0:
Packet latency average = 2941.65
	minimum = 1179
	maximum = 4256
Network latency average = 772.665
	minimum = 22
	maximum = 1982
Slowest packet = 13979
Flit latency average = 1700.58
	minimum = 5
	maximum = 3781
Slowest flit = 153599
Fragmentation average = 74.848
	minimum = 0
	maximum = 686
Injected packet rate average = 0.0153438
	minimum = 0.0045 (at node 0)
	maximum = 0.024 (at node 126)
Accepted packet rate average = 0.0150469
	minimum = 0.009 (at node 4)
	maximum = 0.0215 (at node 66)
Injected flit rate average = 0.276596
	minimum = 0.0795 (at node 0)
	maximum = 0.4345 (at node 126)
Accepted flit rate average= 0.268474
	minimum = 0.167 (at node 4)
	maximum = 0.378 (at node 66)
Injected packet length average = 18.0266
Accepted packet length average = 17.8425
Total in-flight flits = 95750 (86096 measured)
latency change    = 0.294465
throughput change = 0
Class 0:
Packet latency average = 3667.28
	minimum = 1179
	maximum = 5120
Network latency average = 1383.33
	minimum = 22
	maximum = 2984
Slowest packet = 13979
Flit latency average = 1720.94
	minimum = 5
	maximum = 4329
Slowest flit = 178560
Fragmentation average = 119.907
	minimum = 0
	maximum = 686
Injected packet rate average = 0.0157049
	minimum = 0.00766667 (at node 4)
	maximum = 0.0236667 (at node 89)
Accepted packet rate average = 0.0150938
	minimum = 0.011 (at node 2)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.283148
	minimum = 0.14 (at node 184)
	maximum = 0.426667 (at node 89)
Accepted flit rate average= 0.269599
	minimum = 0.194 (at node 139)
	maximum = 0.378 (at node 177)
Injected packet length average = 18.0293
Accepted packet length average = 17.8616
Total in-flight flits = 100471 (100169 measured)
latency change    = 0.197865
throughput change = 0.00417286
Draining remaining packets ...
Class 0:
Remaining flits: 261156 261157 261158 261159 261160 261161 263513 263514 263515 263516 [...] (53005 flits)
Measured flits: 261156 261157 261158 261159 261160 261161 263513 263514 263515 263516 [...] (53005 flits)
Class 0:
Remaining flits: 289098 289099 289100 289101 289102 289103 289104 289105 289106 289107 [...] (7210 flits)
Measured flits: 289098 289099 289100 289101 289102 289103 289104 289105 289106 289107 [...] (7210 flits)
Time taken is 8682 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4632.19 (1 samples)
	minimum = 1179 (1 samples)
	maximum = 7231 (1 samples)
Network latency average = 1901.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4566 (1 samples)
Flit latency average = 1866.91 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4509 (1 samples)
Fragmentation average = 134.059 (1 samples)
	minimum = 0 (1 samples)
	maximum = 713 (1 samples)
Injected packet rate average = 0.0157049 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0236667 (1 samples)
Accepted packet rate average = 0.0150938 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.283148 (1 samples)
	minimum = 0.14 (1 samples)
	maximum = 0.426667 (1 samples)
Accepted flit rate average = 0.269599 (1 samples)
	minimum = 0.194 (1 samples)
	maximum = 0.378 (1 samples)
Injected packet size average = 18.0293 (1 samples)
Accepted packet size average = 17.8616 (1 samples)
Hops average = 5.14276 (1 samples)
Total run time 13.467
