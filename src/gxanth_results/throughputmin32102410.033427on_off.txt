BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 373.311
	minimum = 23
	maximum = 997
Network latency average = 276.423
	minimum = 23
	maximum = 980
Slowest packet = 156
Flit latency average = 210.352
	minimum = 6
	maximum = 963
Slowest flit = 2897
Fragmentation average = 144.41
	minimum = 0
	maximum = 817
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0102344
	minimum = 0.004 (at node 41)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.211141
	minimum = 0.078 (at node 170)
	maximum = 0.346 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 20.6305
Total in-flight flits = 59901 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 639.128
	minimum = 23
	maximum = 1993
Network latency average = 498.19
	minimum = 23
	maximum = 1891
Slowest packet = 156
Flit latency average = 427.668
	minimum = 6
	maximum = 1874
Slowest flit = 9665
Fragmentation average = 177.581
	minimum = 0
	maximum = 1395
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.011724
	minimum = 0.0065 (at node 116)
	maximum = 0.0165 (at node 48)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.225469
	minimum = 0.133 (at node 83)
	maximum = 0.312 (at node 133)
Injected packet length average = 17.9205
Accepted packet length average = 19.2315
Total in-flight flits = 124164 (0 measured)
latency change    = 0.415905
throughput change = 0.0635482
Class 0:
Packet latency average = 1348.78
	minimum = 28
	maximum = 2919
Network latency average = 1135.95
	minimum = 23
	maximum = 2750
Slowest packet = 1620
Flit latency average = 1072.91
	minimum = 6
	maximum = 2823
Slowest flit = 13508
Fragmentation average = 213.817
	minimum = 0
	maximum = 2450
Injected packet rate average = 0.031875
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.013375
	minimum = 0.003 (at node 94)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.573807
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.239245
	minimum = 0.057 (at node 164)
	maximum = 0.43 (at node 131)
Injected packet length average = 18.0018
Accepted packet length average = 17.8875
Total in-flight flits = 188389 (0 measured)
latency change    = 0.526144
throughput change = 0.0575814
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 398.865
	minimum = 37
	maximum = 1491
Network latency average = 94.195
	minimum = 23
	maximum = 959
Slowest packet = 17887
Flit latency average = 1536.21
	minimum = 6
	maximum = 3665
Slowest flit = 27249
Fragmentation average = 26.8494
	minimum = 0
	maximum = 401
Injected packet rate average = 0.0326719
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.013151
	minimum = 0.004 (at node 40)
	maximum = 0.023 (at node 124)
Injected flit rate average = 0.587896
	minimum = 0 (at node 5)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.236661
	minimum = 0.076 (at node 184)
	maximum = 0.401 (at node 9)
Injected packet length average = 17.9939
Accepted packet length average = 17.9956
Total in-flight flits = 255864 (103140 measured)
latency change    = 2.38155
throughput change = 0.0109157
Class 0:
Packet latency average = 542.334
	minimum = 29
	maximum = 2290
Network latency average = 273.517
	minimum = 23
	maximum = 1967
Slowest packet = 17887
Flit latency average = 1767.18
	minimum = 6
	maximum = 4668
Slowest flit = 28837
Fragmentation average = 52.6293
	minimum = 0
	maximum = 824
Injected packet rate average = 0.032362
	minimum = 0.004 (at node 123)
	maximum = 0.0555 (at node 0)
Accepted packet rate average = 0.0131641
	minimum = 0.006 (at node 184)
	maximum = 0.0195 (at node 147)
Injected flit rate average = 0.582534
	minimum = 0.0715 (at node 123)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.236078
	minimum = 0.114 (at node 184)
	maximum = 0.3545 (at node 147)
Injected packet length average = 18.0006
Accepted packet length average = 17.9335
Total in-flight flits = 321421 (200206 measured)
latency change    = 0.26454
throughput change = 0.00247093
Class 0:
Packet latency average = 793.92
	minimum = 25
	maximum = 3428
Network latency average = 545
	minimum = 23
	maximum = 2972
Slowest packet = 17887
Flit latency average = 2016.68
	minimum = 6
	maximum = 5493
Slowest flit = 38256
Fragmentation average = 74.1069
	minimum = 0
	maximum = 1062
Injected packet rate average = 0.0321302
	minimum = 0.00866667 (at node 95)
	maximum = 0.0556667 (at node 25)
Accepted packet rate average = 0.013092
	minimum = 0.008 (at node 64)
	maximum = 0.0186667 (at node 27)
Injected flit rate average = 0.578356
	minimum = 0.156 (at node 95)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.234755
	minimum = 0.139333 (at node 64)
	maximum = 0.330667 (at node 27)
Injected packet length average = 18.0004
Accepted packet length average = 17.9312
Total in-flight flits = 386296 (292201 measured)
latency change    = 0.31689
throughput change = 0.0056353
Draining remaining packets ...
Class 0:
Remaining flits: 40338 40339 40340 40341 40342 40343 40344 40345 40346 40347 [...] (350886 flits)
Measured flits: 320922 320923 320924 320925 320926 320927 320928 320929 320930 320931 [...] (278689 flits)
Class 0:
Remaining flits: 43278 43279 43280 43281 43282 43283 43284 43285 43286 43287 [...] (316060 flits)
Measured flits: 320922 320923 320924 320925 320926 320927 320928 320929 320930 320931 [...] (261788 flits)
Class 0:
Remaining flits: 60120 60121 60122 60123 60124 60125 60126 60127 60128 60129 [...] (282037 flits)
Measured flits: 320938 320939 320958 320959 320960 320961 320962 320963 320964 320965 [...] (241937 flits)
Class 0:
Remaining flits: 60120 60121 60122 60123 60124 60125 60126 60127 60128 60129 [...] (247959 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (219057 flits)
Class 0:
Remaining flits: 60120 60121 60122 60123 60124 60125 60126 60127 60128 60129 [...] (213810 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (192324 flits)
Class 0:
Remaining flits: 60120 60121 60122 60123 60124 60125 60126 60127 60128 60129 [...] (180417 flits)
Measured flits: 321048 321049 321050 321051 321052 321053 321054 321055 321056 321057 [...] (164622 flits)
Class 0:
Remaining flits: 99061 99062 99063 99064 99065 99066 99067 99068 99069 99070 [...] (148024 flits)
Measured flits: 321048 321049 321050 321051 321052 321053 321054 321055 321056 321057 [...] (136647 flits)
Class 0:
Remaining flits: 99061 99062 99063 99064 99065 99066 99067 99068 99069 99070 [...] (115725 flits)
Measured flits: 321048 321049 321050 321051 321052 321053 321054 321055 321056 321057 [...] (108102 flits)
Class 0:
Remaining flits: 116725 116726 116727 116728 116729 134707 134708 134709 134710 134711 [...] (84636 flits)
Measured flits: 321174 321175 321176 321177 321178 321179 321180 321181 321182 321183 [...] (79468 flits)
Class 0:
Remaining flits: 136296 136297 136298 136299 136300 136301 136302 136303 136304 136305 [...] (55521 flits)
Measured flits: 321678 321679 321680 321681 321682 321683 321684 321685 321686 321687 [...] (52502 flits)
Class 0:
Remaining flits: 136309 136310 136311 136312 136313 137736 137737 137738 137739 137740 [...] (30387 flits)
Measured flits: 321678 321679 321680 321681 321682 321683 321684 321685 321686 321687 [...] (28906 flits)
Class 0:
Remaining flits: 152669 152670 152671 152672 152673 152674 152675 181792 181793 181794 [...] (11389 flits)
Measured flits: 321682 321683 321684 321685 321686 321687 321688 321689 321690 321691 [...] (10883 flits)
Class 0:
Remaining flits: 231111 231112 231113 231114 231115 231116 231117 231118 231119 268488 [...] (2066 flits)
Measured flits: 322560 322561 322562 322563 322564 322565 322566 322567 322568 322569 [...] (1949 flits)
Class 0:
Remaining flits: 483933 483934 483935 483936 483937 483938 483939 483940 483941 483942 [...] (33 flits)
Measured flits: 483933 483934 483935 483936 483937 483938 483939 483940 483941 483942 [...] (33 flits)
Time taken is 20036 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7520.88 (1 samples)
	minimum = 25 (1 samples)
	maximum = 17297 (1 samples)
Network latency average = 7223.23 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16761 (1 samples)
Flit latency average = 6287.62 (1 samples)
	minimum = 6 (1 samples)
	maximum = 17121 (1 samples)
Fragmentation average = 173.936 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4310 (1 samples)
Injected packet rate average = 0.0321302 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.013092 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.578356 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.234755 (1 samples)
	minimum = 0.139333 (1 samples)
	maximum = 0.330667 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 17.9312 (1 samples)
Hops average = 5.06452 (1 samples)
Total run time 18.6814
