BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 347.704
	minimum = 22
	maximum = 928
Network latency average = 320.223
	minimum = 22
	maximum = 859
Slowest packet = 406
Flit latency average = 277.423
	minimum = 5
	maximum = 858
Slowest flit = 16540
Fragmentation average = 126.485
	minimum = 0
	maximum = 593
Injected packet rate average = 0.0394688
	minimum = 0.021 (at node 0)
	maximum = 0.054 (at node 90)
Accepted packet rate average = 0.0138229
	minimum = 0.002 (at node 174)
	maximum = 0.025 (at node 131)
Injected flit rate average = 0.704307
	minimum = 0.372 (at node 0)
	maximum = 0.957 (at node 90)
Accepted flit rate average= 0.284021
	minimum = 0.085 (at node 174)
	maximum = 0.501 (at node 131)
Injected packet length average = 17.8447
Accepted packet length average = 20.5471
Total in-flight flits = 82106 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 703.35
	minimum = 22
	maximum = 1752
Network latency average = 639.774
	minimum = 22
	maximum = 1725
Slowest packet = 2244
Flit latency average = 569.223
	minimum = 5
	maximum = 1739
Slowest flit = 36710
Fragmentation average = 183.002
	minimum = 0
	maximum = 823
Injected packet rate average = 0.0288516
	minimum = 0.0125 (at node 148)
	maximum = 0.038 (at node 139)
Accepted packet rate average = 0.0144245
	minimum = 0.008 (at node 96)
	maximum = 0.0215 (at node 44)
Injected flit rate average = 0.515956
	minimum = 0.225 (at node 148)
	maximum = 0.682 (at node 139)
Accepted flit rate average= 0.276062
	minimum = 0.1565 (at node 96)
	maximum = 0.3965 (at node 44)
Injected packet length average = 17.8831
Accepted packet length average = 19.1385
Total in-flight flits = 93756 (0 measured)
latency change    = 0.505646
throughput change = 0.028828
Class 0:
Packet latency average = 1750.84
	minimum = 387
	maximum = 2576
Network latency average = 1484.44
	minimum = 22
	maximum = 2476
Slowest packet = 2721
Flit latency average = 1408.5
	minimum = 5
	maximum = 2611
Slowest flit = 56661
Fragmentation average = 204.631
	minimum = 0
	maximum = 818
Injected packet rate average = 0.0148281
	minimum = 0.001 (at node 96)
	maximum = 0.03 (at node 97)
Accepted packet rate average = 0.015
	minimum = 0.004 (at node 149)
	maximum = 0.027 (at node 152)
Injected flit rate average = 0.267719
	minimum = 0.026 (at node 96)
	maximum = 0.532 (at node 97)
Accepted flit rate average= 0.267682
	minimum = 0.114 (at node 149)
	maximum = 0.465 (at node 182)
Injected packet length average = 18.0548
Accepted packet length average = 17.8455
Total in-flight flits = 93625 (0 measured)
latency change    = 0.598279
throughput change = 0.0313065
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1969.42
	minimum = 1164
	maximum = 3196
Network latency average = 103.965
	minimum = 22
	maximum = 909
Slowest packet = 13946
Flit latency average = 1687.1
	minimum = 5
	maximum = 3295
Slowest flit = 103834
Fragmentation average = 8.29921
	minimum = 0
	maximum = 91
Injected packet rate average = 0.0156563
	minimum = 0.002 (at node 124)
	maximum = 0.04 (at node 73)
Accepted packet rate average = 0.0150677
	minimum = 0.006 (at node 1)
	maximum = 0.025 (at node 123)
Injected flit rate average = 0.282089
	minimum = 0.036 (at node 124)
	maximum = 0.724 (at node 73)
Accepted flit rate average= 0.267823
	minimum = 0.094 (at node 36)
	maximum = 0.442 (at node 123)
Injected packet length average = 18.0176
Accepted packet length average = 17.7746
Total in-flight flits = 96347 (49393 measured)
latency change    = 0.110987
throughput change = 0.000525067
Class 0:
Packet latency average = 2760.55
	minimum = 1164
	maximum = 4093
Network latency average = 693.538
	minimum = 22
	maximum = 1944
Slowest packet = 13946
Flit latency average = 1730.81
	minimum = 5
	maximum = 4114
Slowest flit = 64529
Fragmentation average = 63.4767
	minimum = 0
	maximum = 643
Injected packet rate average = 0.0160495
	minimum = 0.0065 (at node 92)
	maximum = 0.0285 (at node 73)
Accepted packet rate average = 0.0151302
	minimum = 0.009 (at node 36)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.288888
	minimum = 0.1245 (at node 152)
	maximum = 0.512 (at node 73)
Accepted flit rate average= 0.26976
	minimum = 0.1665 (at node 36)
	maximum = 0.3815 (at node 0)
Injected packet length average = 17.9998
Accepted packet length average = 17.8293
Total in-flight flits = 101079 (90154 measured)
latency change    = 0.286583
throughput change = 0.0071823
Class 0:
Packet latency average = 3502.78
	minimum = 1164
	maximum = 4962
Network latency average = 1356.72
	minimum = 22
	maximum = 2928
Slowest packet = 13946
Flit latency average = 1763.69
	minimum = 5
	maximum = 4517
Slowest flit = 163403
Fragmentation average = 112.068
	minimum = 0
	maximum = 714
Injected packet rate average = 0.0156701
	minimum = 0.00666667 (at node 124)
	maximum = 0.025 (at node 73)
Accepted packet rate average = 0.0150781
	minimum = 0.00866667 (at node 35)
	maximum = 0.0213333 (at node 184)
Injected flit rate average = 0.282172
	minimum = 0.12 (at node 124)
	maximum = 0.449333 (at node 73)
Accepted flit rate average= 0.269328
	minimum = 0.151 (at node 35)
	maximum = 0.378667 (at node 128)
Injected packet length average = 18.007
Accepted packet length average = 17.8622
Total in-flight flits = 101122 (100457 measured)
latency change    = 0.211897
throughput change = 0.00160507
Class 0:
Packet latency average = 3999.33
	minimum = 1164
	maximum = 5914
Network latency average = 1634.59
	minimum = 22
	maximum = 3722
Slowest packet = 13946
Flit latency average = 1790.05
	minimum = 5
	maximum = 4517
Slowest flit = 163403
Fragmentation average = 123.999
	minimum = 0
	maximum = 780
Injected packet rate average = 0.0154844
	minimum = 0.00825 (at node 67)
	maximum = 0.022 (at node 65)
Accepted packet rate average = 0.0150599
	minimum = 0.01075 (at node 42)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.278799
	minimum = 0.145 (at node 152)
	maximum = 0.39775 (at node 65)
Accepted flit rate average= 0.269293
	minimum = 0.19825 (at node 64)
	maximum = 0.36575 (at node 128)
Injected packet length average = 18.0052
Accepted packet length average = 17.8815
Total in-flight flits = 100936 (100865 measured)
latency change    = 0.12416
throughput change = 0.00013055
Class 0:
Packet latency average = 4404.02
	minimum = 1164
	maximum = 6831
Network latency average = 1750.27
	minimum = 22
	maximum = 4610
Slowest packet = 13946
Flit latency average = 1810.62
	minimum = 5
	maximum = 5331
Slowest flit = 227496
Fragmentation average = 126.948
	minimum = 0
	maximum = 780
Injected packet rate average = 0.0155458
	minimum = 0.0078 (at node 60)
	maximum = 0.021 (at node 179)
Accepted packet rate average = 0.0150469
	minimum = 0.011 (at node 86)
	maximum = 0.0188 (at node 128)
Injected flit rate average = 0.279874
	minimum = 0.1392 (at node 60)
	maximum = 0.378 (at node 179)
Accepted flit rate average= 0.269283
	minimum = 0.1956 (at node 86)
	maximum = 0.3372 (at node 128)
Injected packet length average = 18.0031
Accepted packet length average = 17.8963
Total in-flight flits = 103961 (103961 measured)
latency change    = 0.0918896
throughput change = 3.57817e-05
Class 0:
Packet latency average = 4785.7
	minimum = 1164
	maximum = 7475
Network latency average = 1816.94
	minimum = 22
	maximum = 4742
Slowest packet = 13946
Flit latency average = 1829.51
	minimum = 5
	maximum = 5331
Slowest flit = 227496
Fragmentation average = 127.64
	minimum = 0
	maximum = 780
Injected packet rate average = 0.0153741
	minimum = 0.008 (at node 60)
	maximum = 0.02 (at node 126)
Accepted packet rate average = 0.015033
	minimum = 0.0115 (at node 86)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.276871
	minimum = 0.144 (at node 60)
	maximum = 0.359167 (at node 126)
Accepted flit rate average= 0.269044
	minimum = 0.208167 (at node 86)
	maximum = 0.3405 (at node 103)
Injected packet length average = 18.0089
Accepted packet length average = 17.8969
Total in-flight flits = 102862 (102862 measured)
latency change    = 0.0797553
throughput change = 0.000888562
Class 0:
Packet latency average = 5150.27
	minimum = 1164
	maximum = 8364
Network latency average = 1862.58
	minimum = 22
	maximum = 5025
Slowest packet = 13946
Flit latency average = 1846.99
	minimum = 5
	maximum = 5331
Slowest flit = 227496
Fragmentation average = 127.913
	minimum = 0
	maximum = 780
Injected packet rate average = 0.0153735
	minimum = 0.00857143 (at node 60)
	maximum = 0.0198571 (at node 130)
Accepted packet rate average = 0.0150097
	minimum = 0.0117143 (at node 80)
	maximum = 0.0182857 (at node 70)
Injected flit rate average = 0.276742
	minimum = 0.153 (at node 60)
	maximum = 0.358429 (at node 130)
Accepted flit rate average= 0.269182
	minimum = 0.208429 (at node 80)
	maximum = 0.327857 (at node 70)
Injected packet length average = 18.0012
Accepted packet length average = 17.9339
Total in-flight flits = 103959 (103959 measured)
latency change    = 0.0707859
throughput change = 0.000509978
Draining all recorded packets ...
Class 0:
Remaining flits: 421938 421939 421940 421941 421942 421943 421944 421945 421946 421947 [...] (104288 flits)
Measured flits: 421938 421939 421940 421941 421942 421943 421944 421945 421946 421947 [...] (104288 flits)
Class 0:
Remaining flits: 421953 421954 421955 468144 468145 468146 468147 468148 468149 468150 [...] (103595 flits)
Measured flits: 421953 421954 421955 468144 468145 468146 468147 468148 468149 468150 [...] (103595 flits)
Class 0:
Remaining flits: 514878 514879 514880 514881 514882 514883 514884 514885 514886 514887 [...] (104560 flits)
Measured flits: 514878 514879 514880 514881 514882 514883 514884 514885 514886 514887 [...] (104560 flits)
Class 0:
Remaining flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (104995 flits)
Measured flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (104995 flits)
Class 0:
Remaining flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (106454 flits)
Measured flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (106454 flits)
Class 0:
Remaining flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (105253 flits)
Measured flits: 548586 548587 548588 548589 548590 548591 548592 548593 548594 548595 [...] (105253 flits)
Class 0:
Remaining flits: 631962 631963 631964 631965 631966 631967 631968 631969 631970 631971 [...] (107590 flits)
Measured flits: 631962 631963 631964 631965 631966 631967 631968 631969 631970 631971 [...] (107590 flits)
Class 0:
Remaining flits: 679305 679306 679307 679308 679309 679310 679311 679312 679313 679314 [...] (105943 flits)
Measured flits: 679305 679306 679307 679308 679309 679310 679311 679312 679313 679314 [...] (105943 flits)
Class 0:
Remaining flits: 760714 760715 821871 821872 821873 821874 821875 821876 821877 821878 [...] (104346 flits)
Measured flits: 760714 760715 821871 821872 821873 821874 821875 821876 821877 821878 [...] (104346 flits)
Class 0:
Remaining flits: 833256 833257 833258 833259 833260 833261 833262 833263 833264 833265 [...] (103589 flits)
Measured flits: 833256 833257 833258 833259 833260 833261 833262 833263 833264 833265 [...] (103589 flits)
Class 0:
Remaining flits: 898110 898111 898112 898113 898114 898115 898116 898117 898118 898119 [...] (104042 flits)
Measured flits: 898110 898111 898112 898113 898114 898115 898116 898117 898118 898119 [...] (104042 flits)
Class 0:
Remaining flits: 960318 960319 960320 960321 960322 960323 960324 960325 960326 960327 [...] (105753 flits)
Measured flits: 960318 960319 960320 960321 960322 960323 960324 960325 960326 960327 [...] (105753 flits)
Class 0:
Remaining flits: 960318 960319 960320 960321 960322 960323 960324 960325 960326 960327 [...] (104270 flits)
Measured flits: 960318 960319 960320 960321 960322 960323 960324 960325 960326 960327 [...] (104270 flits)
Class 0:
Remaining flits: 1006957 1006958 1006959 1006960 1006961 1006962 1006963 1006964 1006965 1006966 [...] (104010 flits)
Measured flits: 1006957 1006958 1006959 1006960 1006961 1006962 1006963 1006964 1006965 1006966 [...] (104010 flits)
Class 0:
Remaining flits: 1092119 1092120 1092121 1092122 1092123 1092124 1092125 1092126 1092127 1092128 [...] (105687 flits)
Measured flits: 1092119 1092120 1092121 1092122 1092123 1092124 1092125 1092126 1092127 1092128 [...] (105687 flits)
Class 0:
Remaining flits: 1131534 1131535 1131536 1131537 1131538 1131539 1131540 1131541 1131542 1131543 [...] (105603 flits)
Measured flits: 1131534 1131535 1131536 1131537 1131538 1131539 1131540 1131541 1131542 1131543 [...] (105279 flits)
Class 0:
Remaining flits: 1141038 1141039 1141040 1141041 1141042 1141043 1141044 1141045 1141046 1141047 [...] (103228 flits)
Measured flits: 1141038 1141039 1141040 1141041 1141042 1141043 1141044 1141045 1141046 1141047 [...] (102510 flits)
Class 0:
Remaining flits: 1141038 1141039 1141040 1141041 1141042 1141043 1141044 1141045 1141046 1141047 [...] (102338 flits)
Measured flits: 1141038 1141039 1141040 1141041 1141042 1141043 1141044 1141045 1141046 1141047 [...] (98932 flits)
Class 0:
Remaining flits: 1265898 1265899 1265900 1265901 1265902 1265903 1278396 1278397 1278398 1278399 [...] (103332 flits)
Measured flits: 1265898 1265899 1265900 1265901 1265902 1265903 1278396 1278397 1278398 1278399 [...] (89404 flits)
Class 0:
Remaining flits: 1308877 1308878 1308879 1308880 1308881 1308882 1308883 1308884 1308885 1308886 [...] (104151 flits)
Measured flits: 1308877 1308878 1308879 1308880 1308881 1308882 1308883 1308884 1308885 1308886 [...] (75258 flits)
Class 0:
Remaining flits: 1337087 1337088 1337089 1337090 1337091 1337092 1337093 1371528 1371529 1371530 [...] (103978 flits)
Measured flits: 1337087 1337088 1337089 1337090 1337091 1337092 1337093 1371528 1371529 1371530 [...] (56836 flits)
Class 0:
Remaining flits: 1396289 1396290 1396291 1396292 1396293 1396294 1396295 1446479 1454652 1454653 [...] (103697 flits)
Measured flits: 1396289 1396290 1396291 1396292 1396293 1396294 1396295 1446479 1454652 1454653 [...] (38816 flits)
Class 0:
Remaining flits: 1466602 1466603 1466838 1466839 1466840 1466841 1466842 1466843 1466844 1466845 [...] (103268 flits)
Measured flits: 1466602 1466603 1469908 1469909 1469910 1469911 1469912 1469913 1469914 1469915 [...] (24001 flits)
Class 0:
Remaining flits: 1483344 1483345 1483346 1483347 1483348 1483349 1483350 1483351 1483352 1483353 [...] (102438 flits)
Measured flits: 1483344 1483345 1483346 1483347 1483348 1483349 1483350 1483351 1483352 1483353 [...] (17750 flits)
Class 0:
Remaining flits: 1483344 1483345 1483346 1483347 1483348 1483349 1483350 1483351 1483352 1483353 [...] (102313 flits)
Measured flits: 1483344 1483345 1483346 1483347 1483348 1483349 1483350 1483351 1483352 1483353 [...] (15594 flits)
Class 0:
Remaining flits: 1570086 1570087 1570088 1570089 1570090 1570091 1570092 1570093 1570094 1570095 [...] (100467 flits)
Measured flits: 1570086 1570087 1570088 1570089 1570090 1570091 1570092 1570093 1570094 1570095 [...] (13769 flits)
Class 0:
Remaining flits: 1583668 1583669 1583670 1583671 1583672 1583673 1583674 1583675 1628676 1628677 [...] (102909 flits)
Measured flits: 1683846 1683847 1683848 1683849 1683850 1683851 1683852 1683853 1683854 1683855 [...] (12884 flits)
Class 0:
Remaining flits: 1628676 1628677 1628678 1628679 1628680 1628681 1628682 1628683 1628684 1628685 [...] (103083 flits)
Measured flits: 1683846 1683847 1683848 1683849 1683850 1683851 1683852 1683853 1683854 1683855 [...] (13354 flits)
Class 0:
Remaining flits: 1628676 1628677 1628678 1628679 1628680 1628681 1628682 1628683 1628684 1628685 [...] (102784 flits)
Measured flits: 1683846 1683847 1683848 1683849 1683850 1683851 1683852 1683853 1683854 1683855 [...] (12523 flits)
Class 0:
Remaining flits: 1683846 1683847 1683848 1683849 1683850 1683851 1683852 1683853 1683854 1683855 [...] (102208 flits)
Measured flits: 1683846 1683847 1683848 1683849 1683850 1683851 1683852 1683853 1683854 1683855 [...] (12994 flits)
Class 0:
Remaining flits: 1774962 1774963 1774964 1774965 1774966 1774967 1774968 1774969 1774970 1774971 [...] (100570 flits)
Measured flits: 1791540 1791541 1791542 1791543 1791544 1791545 1791546 1791547 1791548 1791549 [...] (10213 flits)
Class 0:
Remaining flits: 1774962 1774963 1774964 1774965 1774966 1774967 1774968 1774969 1774970 1774971 [...] (100592 flits)
Measured flits: 1791540 1791541 1791542 1791543 1791544 1791545 1791546 1791547 1791548 1791549 [...] (8933 flits)
Class 0:
Remaining flits: 1774962 1774963 1774964 1774965 1774966 1774967 1774968 1774969 1774970 1774971 [...] (100117 flits)
Measured flits: 1791540 1791541 1791542 1791543 1791544 1791545 1791546 1791547 1791548 1791549 [...] (8186 flits)
Class 0:
Remaining flits: 1774962 1774963 1774964 1774965 1774966 1774967 1774968 1774969 1774970 1774971 [...] (99459 flits)
Measured flits: 1791540 1791541 1791542 1791543 1791544 1791545 1791546 1791547 1791548 1791549 [...] (6574 flits)
Class 0:
Remaining flits: 1791556 1791557 1968003 1968004 1968005 1968006 1968007 1968008 1968009 1968010 [...] (98585 flits)
Measured flits: 1791556 1791557 2103894 2103895 2103896 2103897 2103898 2103899 2103900 2103901 [...] (4508 flits)
Class 0:
Remaining flits: 2067570 2067571 2067572 2067573 2067574 2067575 2067576 2067577 2067578 2067579 [...] (97516 flits)
Measured flits: 2119788 2119789 2119790 2119791 2119792 2119793 2119794 2119795 2119796 2119797 [...] (3112 flits)
Class 0:
Remaining flits: 2067570 2067571 2067572 2067573 2067574 2067575 2067576 2067577 2067578 2067579 [...] (98573 flits)
Measured flits: 2221164 2221165 2221166 2221167 2221168 2221169 2221170 2221171 2221172 2221173 [...] (1926 flits)
Class 0:
Remaining flits: 2168118 2168119 2168120 2168121 2168122 2168123 2168124 2168125 2168126 2168127 [...] (96404 flits)
Measured flits: 2233746 2233747 2233748 2233749 2233750 2233751 2233752 2233753 2233754 2233755 [...] (1534 flits)
Class 0:
Remaining flits: 2168118 2168119 2168120 2168121 2168122 2168123 2168124 2168125 2168126 2168127 [...] (98250 flits)
Measured flits: 2273875 2273876 2273877 2273878 2273879 2273880 2273881 2273882 2273883 2273884 [...] (1174 flits)
Class 0:
Remaining flits: 2168118 2168119 2168120 2168121 2168122 2168123 2168124 2168125 2168126 2168127 [...] (98421 flits)
Measured flits: 2464416 2464417 2464418 2464419 2464420 2464421 2464422 2464423 2464424 2464425 [...] (728 flits)
Class 0:
Remaining flits: 2168118 2168119 2168120 2168121 2168122 2168123 2168124 2168125 2168126 2168127 [...] (100351 flits)
Measured flits: 2464416 2464417 2464418 2464419 2464420 2464421 2464422 2464423 2464424 2464425 [...] (330 flits)
Class 0:
Remaining flits: 2168119 2168120 2168121 2168122 2168123 2168124 2168125 2168126 2168127 2168128 [...] (100925 flits)
Measured flits: 2523186 2523187 2523188 2523189 2523190 2523191 2523192 2523193 2523194 2523195 [...] (486 flits)
Class 0:
Remaining flits: 2322360 2322361 2322362 2322363 2322364 2322365 2322366 2322367 2322368 2322369 [...] (100539 flits)
Measured flits: 2643228 2643229 2643230 2643231 2643232 2643233 2643234 2643235 2643236 2643237 [...] (310 flits)
Class 0:
Remaining flits: 2322360 2322361 2322362 2322363 2322364 2322365 2322366 2322367 2322368 2322369 [...] (98459 flits)
Measured flits: 2788470 2788471 2788472 2788473 2788474 2788475 2788476 2788477 2788478 2788479 [...] (102 flits)
Class 0:
Remaining flits: 2322360 2322361 2322362 2322363 2322364 2322365 2322366 2322367 2322368 2322369 [...] (98945 flits)
Measured flits: 2788470 2788471 2788472 2788473 2788474 2788475 2788476 2788477 2788478 2788479 [...] (90 flits)
Class 0:
Remaining flits: 2525814 2525815 2525816 2525817 2525818 2525819 2525820 2525821 2525822 2525823 [...] (99076 flits)
Measured flits: 2788470 2788471 2788472 2788473 2788474 2788475 2788476 2788477 2788478 2788479 [...] (73 flits)
Class 0:
Remaining flits: 2583000 2583001 2583002 2583003 2583004 2583005 2583006 2583007 2583008 2583009 [...] (98869 flits)
Measured flits: 2792718 2792719 2792720 2792721 2792722 2792723 2792724 2792725 2792726 2792727 [...] (91 flits)
Class 0:
Remaining flits: 2649240 2649241 2649242 2649243 2649244 2649245 2649246 2649247 2649248 2649249 [...] (96737 flits)
Measured flits: 2845812 2845813 2845814 2845815 2845816 2845817 3030192 3030193 3030194 3030195 [...] (231 flits)
Class 0:
Remaining flits: 2649240 2649241 2649242 2649243 2649244 2649245 2649246 2649247 2649248 2649249 [...] (94585 flits)
Measured flits: 3030192 3030193 3030194 3030195 3030196 3030197 3030198 3030199 3030200 3030201 [...] (320 flits)
Class 0:
Remaining flits: 2649240 2649241 2649242 2649243 2649244 2649245 2649246 2649247 2649248 2649249 [...] (96450 flits)
Measured flits: 3030192 3030193 3030194 3030195 3030196 3030197 3030198 3030199 3030200 3030201 [...] (319 flits)
Class 0:
Remaining flits: 2789046 2789047 2789048 2789049 2789050 2789051 2789052 2789053 2789054 2789055 [...] (99106 flits)
Measured flits: 3030192 3030193 3030194 3030195 3030196 3030197 3030198 3030199 3030200 3030201 [...] (163 flits)
Class 0:
Remaining flits: 2789046 2789047 2789048 2789049 2789050 2789051 2789052 2789053 2789054 2789055 [...] (97305 flits)
Measured flits: 3030193 3030194 3030195 3030196 3030197 3030198 3030199 3030200 3030201 3030202 [...] (175 flits)
Class 0:
Remaining flits: 2789046 2789047 2789048 2789049 2789050 2789051 2789052 2789053 2789054 2789055 [...] (97527 flits)
Measured flits: 3290670 3290671 3290672 3290673 3290674 3290675 3290676 3290677 3290678 3290679 [...] (18 flits)
Class 0:
Remaining flits: 2869560 2869561 2869562 2869563 2869564 2869565 2869566 2869567 2869568 2869569 [...] (100418 flits)
Measured flits: 3290670 3290671 3290672 3290673 3290674 3290675 3290676 3290677 3290678 3290679 [...] (18 flits)
Class 0:
Remaining flits: 2869560 2869561 2869562 2869563 2869564 2869565 2869566 2869567 2869568 2869569 [...] (99773 flits)
Measured flits: 3290670 3290671 3290672 3290673 3290674 3290675 3290676 3290677 3290678 3290679 [...] (18 flits)
Class 0:
Remaining flits: 2869566 2869567 2869568 2869569 2869570 2869571 2869572 2869573 2869574 2869575 [...] (98828 flits)
Measured flits: 3290670 3290671 3290672 3290673 3290674 3290675 3290676 3290677 3290678 3290679 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 3122082 3122083 3122084 3122085 3122086 3122087 3122088 3122089 3122090 3122091 [...] (51670 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3122082 3122083 3122084 3122085 3122086 3122087 3122088 3122089 3122090 3122091 [...] (13910 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3245483 3245484 3245485 3245486 3245487 3245488 3245489 3267900 3267901 3267902 [...] (2046 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3274375 3274376 3274377 3274378 3274379 3372948 3372949 3372950 3372951 3372952 [...] (237 flits)
Measured flits: (0 flits)
Time taken is 70509 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14362.2 (1 samples)
	minimum = 1164 (1 samples)
	maximum = 56276 (1 samples)
Network latency average = 2034.41 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12453 (1 samples)
Flit latency average = 1926.66 (1 samples)
	minimum = 5 (1 samples)
	maximum = 12973 (1 samples)
Fragmentation average = 143.004 (1 samples)
	minimum = 0 (1 samples)
	maximum = 780 (1 samples)
Injected packet rate average = 0.0153735 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0198571 (1 samples)
Accepted packet rate average = 0.0150097 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.276742 (1 samples)
	minimum = 0.153 (1 samples)
	maximum = 0.358429 (1 samples)
Accepted flit rate average = 0.269182 (1 samples)
	minimum = 0.208429 (1 samples)
	maximum = 0.327857 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 17.9339 (1 samples)
Hops average = 5.06194 (1 samples)
Total run time 146.374
