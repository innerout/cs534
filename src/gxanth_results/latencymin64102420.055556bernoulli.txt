BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.04
	minimum = 22
	maximum = 920
Network latency average = 296.167
	minimum = 22
	maximum = 874
Slowest packet = 820
Flit latency average = 264.938
	minimum = 5
	maximum = 860
Slowest flit = 17673
Fragmentation average = 86.7665
	minimum = 0
	maximum = 707
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.015276
	minimum = 0.006 (at node 108)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.304339
	minimum = 0.167 (at node 164)
	maximum = 0.465 (at node 48)
Injected packet length average = 17.8419
Accepted packet length average = 19.9226
Total in-flight flits = 114835 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.233
	minimum = 22
	maximum = 1776
Network latency average = 566.479
	minimum = 22
	maximum = 1686
Slowest packet = 981
Flit latency average = 524.029
	minimum = 5
	maximum = 1732
Slowest flit = 38986
Fragmentation average = 142.257
	minimum = 0
	maximum = 852
Injected packet rate average = 0.0517865
	minimum = 0.0425 (at node 37)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0163047
	minimum = 0.009 (at node 96)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.928016
	minimum = 0.76 (at node 37)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.313544
	minimum = 0.1725 (at node 96)
	maximum = 0.4715 (at node 152)
Injected packet length average = 17.92
Accepted packet length average = 19.2303
Total in-flight flits = 237547 (0 measured)
latency change    = 0.469192
throughput change = 0.0293602
Class 0:
Packet latency average = 1381.23
	minimum = 24
	maximum = 2616
Network latency average = 1291.24
	minimum = 23
	maximum = 2511
Slowest packet = 2474
Flit latency average = 1249.69
	minimum = 5
	maximum = 2582
Slowest flit = 67753
Fragmentation average = 243.141
	minimum = 0
	maximum = 891
Injected packet rate average = 0.0535417
	minimum = 0.039 (at node 140)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0176875
	minimum = 0.009 (at node 117)
	maximum = 0.029 (at node 182)
Injected flit rate average = 0.964151
	minimum = 0.707 (at node 140)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.327823
	minimum = 0.177 (at node 190)
	maximum = 0.549 (at node 182)
Injected packet length average = 18.0075
Accepted packet length average = 18.5342
Total in-flight flits = 359645 (0 measured)
latency change    = 0.555299
throughput change = 0.043556
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 213.248
	minimum = 24
	maximum = 660
Network latency average = 38.5985
	minimum = 22
	maximum = 255
Slowest packet = 30203
Flit latency average = 1793.39
	minimum = 5
	maximum = 3455
Slowest flit = 87083
Fragmentation average = 6.72871
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0539115
	minimum = 0.039 (at node 162)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0173802
	minimum = 0.008 (at node 46)
	maximum = 0.03 (at node 19)
Injected flit rate average = 0.969901
	minimum = 0.691 (at node 162)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.322198
	minimum = 0.17 (at node 80)
	maximum = 0.531 (at node 3)
Injected packet length average = 17.9906
Accepted packet length average = 18.5382
Total in-flight flits = 484101 (171319 measured)
latency change    = 5.47708
throughput change = 0.0174582
Class 0:
Packet latency average = 233.054
	minimum = 24
	maximum = 761
Network latency average = 41.3914
	minimum = 22
	maximum = 557
Slowest packet = 30203
Flit latency average = 2059.12
	minimum = 5
	maximum = 4405
Slowest flit = 93704
Fragmentation average = 6.74403
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0541901
	minimum = 0.045 (at node 73)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0175859
	minimum = 0.0115 (at node 46)
	maximum = 0.025 (at node 3)
Injected flit rate average = 0.975346
	minimum = 0.808 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.323216
	minimum = 0.215 (at node 26)
	maximum = 0.4645 (at node 3)
Injected packet length average = 17.9986
Accepted packet length average = 18.3792
Total in-flight flits = 610092 (344230 measured)
latency change    = 0.084985
throughput change = 0.0031503
Class 0:
Packet latency average = 249.018
	minimum = 24
	maximum = 933
Network latency average = 42.4263
	minimum = 22
	maximum = 557
Slowest packet = 30203
Flit latency average = 2329.56
	minimum = 5
	maximum = 5387
Slowest flit = 96803
Fragmentation average = 6.85647
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0543368
	minimum = 0.045 (at node 73)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0176649
	minimum = 0.0126667 (at node 49)
	maximum = 0.025 (at node 188)
Injected flit rate average = 0.977953
	minimum = 0.811 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.323389
	minimum = 0.236 (at node 36)
	maximum = 0.441667 (at node 188)
Injected packet length average = 17.998
Accepted packet length average = 18.3068
Total in-flight flits = 736737 (517505 measured)
latency change    = 0.0641071
throughput change = 0.000534165
Class 0:
Packet latency average = 264.131
	minimum = 24
	maximum = 3887
Network latency average = 44.8129
	minimum = 22
	maximum = 3811
Slowest packet = 30481
Flit latency average = 2623.62
	minimum = 5
	maximum = 6152
Slowest flit = 141931
Fragmentation average = 7.31744
	minimum = 0
	maximum = 734
Injected packet rate average = 0.0543451
	minimum = 0.04725 (at node 73)
	maximum = 0.05575 (at node 13)
Accepted packet rate average = 0.0177669
	minimum = 0.013 (at node 88)
	maximum = 0.02275 (at node 90)
Injected flit rate average = 0.97815
	minimum = 0.85125 (at node 73)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.32375
	minimum = 0.2385 (at node 31)
	maximum = 0.40175 (at node 138)
Injected packet length average = 17.9989
Accepted packet length average = 18.2221
Total in-flight flits = 862271 (690208 measured)
latency change    = 0.0572158
throughput change = 0.0011154
Class 0:
Packet latency average = 303.181
	minimum = 24
	maximum = 5069
Network latency average = 72.625
	minimum = 22
	maximum = 4924
Slowest packet = 31409
Flit latency average = 2914.56
	minimum = 5
	maximum = 7128
Slowest flit = 145203
Fragmentation average = 9.81867
	minimum = 0
	maximum = 734
Injected packet rate average = 0.0544469
	minimum = 0.048 (at node 99)
	maximum = 0.0556 (at node 1)
Accepted packet rate average = 0.0178927
	minimum = 0.0138 (at node 36)
	maximum = 0.0222 (at node 90)
Injected flit rate average = 0.979891
	minimum = 0.8664 (at node 99)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.324904
	minimum = 0.2474 (at node 36)
	maximum = 0.3992 (at node 54)
Injected packet length average = 17.9972
Accepted packet length average = 18.1585
Total in-flight flits = 988579 (863096 measured)
latency change    = 0.128803
throughput change = 0.00355233
Class 0:
Packet latency average = 487.979
	minimum = 24
	maximum = 6329
Network latency average = 249.853
	minimum = 22
	maximum = 5930
Slowest packet = 31409
Flit latency average = 3198.25
	minimum = 5
	maximum = 7941
Slowest flit = 180739
Fragmentation average = 22.7348
	minimum = 0
	maximum = 834
Injected packet rate average = 0.0545035
	minimum = 0.049 (at node 99)
	maximum = 0.0556667 (at node 4)
Accepted packet rate average = 0.0179679
	minimum = 0.0146667 (at node 7)
	maximum = 0.0221667 (at node 68)
Injected flit rate average = 0.981003
	minimum = 0.8845 (at node 99)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.325998
	minimum = 0.263 (at node 18)
	maximum = 0.407333 (at node 68)
Injected packet length average = 17.9989
Accepted packet length average = 18.1434
Total in-flight flits = 1114279 (1031940 measured)
latency change    = 0.3787
throughput change = 0.00335614
Class 0:
Packet latency average = 934.488
	minimum = 23
	maximum = 7437
Network latency average = 692.337
	minimum = 22
	maximum = 6978
Slowest packet = 31409
Flit latency average = 3486.82
	minimum = 5
	maximum = 8838
Slowest flit = 200027
Fragmentation average = 49.6991
	minimum = 0
	maximum = 834
Injected packet rate average = 0.0545476
	minimum = 0.0492857 (at node 99)
	maximum = 0.0555714 (at node 1)
Accepted packet rate average = 0.018
	minimum = 0.0148571 (at node 67)
	maximum = 0.022 (at node 68)
Injected flit rate average = 0.981708
	minimum = 0.886857 (at node 99)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.325945
	minimum = 0.265429 (at node 67)
	maximum = 0.398286 (at node 68)
Injected packet length average = 17.9973
Accepted packet length average = 18.1081
Total in-flight flits = 1241191 (1194941 measured)
latency change    = 0.477812
throughput change = 0.000163596
Draining all recorded packets ...
Class 0:
Remaining flits: 258241 258242 258243 258244 258245 279306 279307 279308 279309 279310 [...] (1366846 flits)
Measured flits: 543027 543028 543029 543030 543031 543032 543033 543034 543035 543036 [...] (1230153 flits)
Class 0:
Remaining flits: 321782 321783 321784 321785 329296 329297 329298 329299 329300 329301 [...] (1492700 flits)
Measured flits: 543146 543147 543148 543149 543168 543169 543170 543171 543172 543173 [...] (1197388 flits)
Class 0:
Remaining flits: 332709 332710 332711 386310 386311 386312 386313 386314 386315 442534 [...] (1616351 flits)
Measured flits: 543171 543172 543173 543174 543175 543176 543177 543178 543179 543180 [...] (1155564 flits)
Class 0:
Remaining flits: 473666 473667 473668 473669 480417 480418 480419 490513 490514 490515 [...] (1734733 flits)
Measured flits: 544676 544677 544678 544679 544734 544735 544736 544737 544738 544739 [...] (1110377 flits)
Class 0:
Remaining flits: 519058 519059 519060 519061 519062 519063 519064 519065 521058 521059 [...] (1842983 flits)
Measured flits: 545410 545411 545412 545413 545414 545415 545416 545417 546749 547614 [...] (1064094 flits)
Class 0:
Remaining flits: 553248 553249 553250 553251 553252 553253 553254 553255 553256 553257 [...] (1947846 flits)
Measured flits: 553248 553249 553250 553251 553252 553253 553254 553255 553256 553257 [...] (1016873 flits)
Class 0:
Remaining flits: 553971 553972 553973 553974 553975 553976 553977 553978 553979 553980 [...] (2051955 flits)
Measured flits: 553971 553972 553973 553974 553975 553976 553977 553978 553979 553980 [...] (969621 flits)
Class 0:
Remaining flits: 617881 617882 617883 617884 617885 624454 624455 627098 627099 627100 [...] (2154252 flits)
Measured flits: 617881 617882 617883 617884 617885 624454 624455 627098 627099 627100 [...] (922425 flits)
Class 0:
Remaining flits: 652608 652609 652610 652611 652612 652613 652614 652615 652616 652617 [...] (2256138 flits)
Measured flits: 652608 652609 652610 652611 652612 652613 652614 652615 652616 652617 [...] (875543 flits)
Class 0:
Remaining flits: 708220 708221 708222 708223 708224 708225 708226 708227 709663 709664 [...] (2358367 flits)
Measured flits: 708220 708221 708222 708223 708224 708225 708226 708227 709663 709664 [...] (828585 flits)
Class 0:
Remaining flits: 783504 783505 783506 783507 783508 783509 783510 783511 783512 783513 [...] (2462242 flits)
Measured flits: 783504 783505 783506 783507 783508 783509 783510 783511 783512 783513 [...] (781329 flits)
Class 0:
Remaining flits: 785963 785964 785965 785966 785967 785968 785969 812790 812791 812792 [...] (2566976 flits)
Measured flits: 785963 785964 785965 785966 785967 785968 785969 812790 812791 812792 [...] (733819 flits)
Class 0:
Remaining flits: 840175 840176 840177 840178 840179 840180 840181 840182 840183 840184 [...] (2672401 flits)
Measured flits: 840175 840176 840177 840178 840179 840180 840181 840182 840183 840184 [...] (686295 flits)
Class 0:
Remaining flits: 894577 894578 894579 894580 894581 902635 902636 902637 902638 902639 [...] (2781000 flits)
Measured flits: 894577 894578 894579 894580 894581 902635 902636 902637 902638 902639 [...] (639110 flits)
Class 0:
Remaining flits: 916170 916171 916172 916173 916174 916175 916176 916177 916178 916179 [...] (2887530 flits)
Measured flits: 916170 916171 916172 916173 916174 916175 916176 916177 916178 916179 [...] (591748 flits)
Class 0:
Remaining flits: 974725 974726 974727 974728 974729 974730 974731 974732 974733 974734 [...] (2993931 flits)
Measured flits: 974725 974726 974727 974728 974729 974730 974731 974732 974733 974734 [...] (544342 flits)
Class 0:
Remaining flits: 982327 982328 982329 982330 982331 987510 987511 987512 987513 987514 [...] (3100123 flits)
Measured flits: 982327 982328 982329 982330 982331 987510 987511 987512 987513 987514 [...] (496822 flits)
Class 0:
Remaining flits: 1041982 1041983 1046672 1046673 1046674 1046675 1046676 1046677 1046678 1046679 [...] (3208121 flits)
Measured flits: 1041982 1041983 1046672 1046673 1046674 1046675 1046676 1046677 1046678 1046679 [...] (449268 flits)
Class 0:
Remaining flits: 1073165 1073166 1073167 1073168 1073169 1073170 1073171 1073172 1073173 1073174 [...] (3316237 flits)
Measured flits: 1073165 1073166 1073167 1073168 1073169 1073170 1073171 1073172 1073173 1073174 [...] (402543 flits)
Class 0:
Remaining flits: 1125203 1125204 1125205 1125206 1125207 1125208 1125209 1125210 1125211 1125212 [...] (3422986 flits)
Measured flits: 1125203 1125204 1125205 1125206 1125207 1125208 1125209 1125210 1125211 1125212 [...] (356356 flits)
Class 0:
Remaining flits: 1146808 1146809 1146810 1146811 1146812 1146813 1146814 1146815 1183673 1183674 [...] (3531288 flits)
Measured flits: 1146808 1146809 1146810 1146811 1146812 1146813 1146814 1146815 1183673 1183674 [...] (311904 flits)
Class 0:
Remaining flits: 1204932 1204933 1204934 1204935 1204936 1204937 1241244 1241245 1241246 1241247 [...] (3637158 flits)
Measured flits: 1204932 1204933 1204934 1204935 1204936 1204937 1241244 1241245 1241246 1241247 [...] (270690 flits)
Class 0:
Remaining flits: 1241244 1241245 1241246 1241247 1241248 1241249 1241250 1241251 1241252 1241253 [...] (3744961 flits)
Measured flits: 1241244 1241245 1241246 1241247 1241248 1241249 1241250 1241251 1241252 1241253 [...] (232631 flits)
Class 0:
Remaining flits: 1310198 1310199 1310200 1310201 1312377 1312378 1312379 1317078 1317079 1317080 [...] (3853337 flits)
Measured flits: 1310198 1310199 1310200 1310201 1312377 1312378 1312379 1317078 1317079 1317080 [...] (198275 flits)
Class 0:
Remaining flits: 1337310 1337311 1337312 1337313 1337314 1337315 1337316 1337317 1337318 1337319 [...] (3961014 flits)
Measured flits: 1337310 1337311 1337312 1337313 1337314 1337315 1337316 1337317 1337318 1337319 [...] (167321 flits)
Class 0:
Remaining flits: 1363426 1363427 1374174 1374175 1374176 1374177 1374178 1374179 1374180 1374181 [...] (4069127 flits)
Measured flits: 1363426 1363427 1374174 1374175 1374176 1374177 1374178 1374179 1374180 1374181 [...] (139736 flits)
Class 0:
Remaining flits: 1374176 1374177 1374178 1374179 1374180 1374181 1374182 1374183 1374184 1374185 [...] (4177614 flits)
Measured flits: 1374176 1374177 1374178 1374179 1374180 1374181 1374182 1374183 1374184 1374185 [...] (114832 flits)
Class 0:
Remaining flits: 1466096 1466097 1466098 1466099 1469239 1469240 1469241 1469242 1469243 1469244 [...] (4286811 flits)
Measured flits: 1466096 1466097 1466098 1466099 1469239 1469240 1469241 1469242 1469243 1469244 [...] (92550 flits)
Class 0:
Remaining flits: 1513584 1513585 1513586 1513587 1513588 1513589 1513590 1513591 1513592 1513593 [...] (4396294 flits)
Measured flits: 1513584 1513585 1513586 1513587 1513588 1513589 1513590 1513591 1513592 1513593 [...] (72632 flits)
Class 0:
Remaining flits: 1518317 1518372 1518373 1518374 1518375 1518376 1518377 1518378 1518379 1518380 [...] (4506327 flits)
Measured flits: 1518317 1518372 1518373 1518374 1518375 1518376 1518377 1518378 1518379 1518380 [...] (54981 flits)
Class 0:
Remaining flits: 1548831 1548832 1548833 1548834 1548835 1548836 1548837 1548838 1548839 1548840 [...] (4616389 flits)
Measured flits: 1548831 1548832 1548833 1548834 1548835 1548836 1548837 1548838 1548839 1548840 [...] (40719 flits)
Class 0:
Remaining flits: 1606950 1606951 1606952 1606953 1606954 1606955 1606956 1606957 1606958 1606959 [...] (4725943 flits)
Measured flits: 1606950 1606951 1606952 1606953 1606954 1606955 1606956 1606957 1606958 1606959 [...] (28975 flits)
Class 0:
Remaining flits: 1656893 1656894 1656895 1656896 1656897 1656898 1656899 1658164 1658165 1658166 [...] (4834619 flits)
Measured flits: 1656893 1656894 1656895 1656896 1656897 1656898 1656899 1658164 1658165 1658166 [...] (20200 flits)
Class 0:
Remaining flits: 1675145 1675146 1675147 1675148 1675149 1675150 1675151 1686742 1686743 1688688 [...] (4945816 flits)
Measured flits: 1675145 1675146 1675147 1675148 1675149 1675150 1675151 1686742 1686743 1688688 [...] (13139 flits)
Class 0:
Remaining flits: 1699269 1699270 1699271 1753866 1753867 1753868 1753869 1753870 1753871 1753872 [...] (5051242 flits)
Measured flits: 1699269 1699270 1699271 1753866 1753867 1753868 1753869 1753870 1753871 1753872 [...] (7472 flits)
Class 0:
Remaining flits: 1753877 1753878 1753879 1753880 1753881 1753882 1753883 1765224 1765225 1765226 [...] (5161058 flits)
Measured flits: 1753877 1753878 1753879 1753880 1753881 1753882 1753883 1765224 1765225 1765226 [...] (3893 flits)
Class 0:
Remaining flits: 1765239 1765240 1765241 1783687 1783688 1783689 1783690 1783691 1791490 1791491 [...] (5264244 flits)
Measured flits: 1765239 1765240 1765241 1783687 1783688 1783689 1783690 1783691 1791490 1791491 [...] (1754 flits)
Class 0:
Remaining flits: 1814242 1814243 1814244 1814245 1814246 1814247 1814248 1814249 1814250 1814251 [...] (5359062 flits)
Measured flits: 1814242 1814243 1814244 1814245 1814246 1814247 1814248 1814249 1814250 1814251 [...] (761 flits)
Class 0:
Remaining flits: 1872868 1872869 1872870 1872871 1872872 1872873 1872874 1872875 1872876 1872877 [...] (5427325 flits)
Measured flits: 1872868 1872869 1872870 1872871 1872872 1872873 1872874 1872875 1872876 1872877 [...] (167 flits)
Class 0:
Remaining flits: 1933920 1933921 1933922 1933923 1933924 1933925 1933926 1933927 1933928 1933929 [...] (5483757 flits)
Measured flits: 1995300 1995301 1995302 1995303 1995304 1995305 1995306 1995307 1995308 1995309 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2002108 2002109 2002110 2002111 2002112 2002113 2002114 2002115 2002116 2002117 [...] (5467498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2041938 2041939 2041940 2041941 2041942 2041943 2041944 2041945 2041946 2041947 [...] (5415961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2041951 2041952 2041953 2041954 2041955 2052054 2052055 2052056 2052057 2052058 [...] (5364493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2133870 2133871 2133872 2133873 2133874 2133875 2133876 2133877 2133878 2133879 [...] (5313808 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154141 2154142 2154143 2154144 2154145 2154146 2154147 2154148 2154149 2200896 [...] (5262885 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2211030 2211031 2211032 2211033 2211034 2211035 2211036 2211037 2211038 2211039 [...] (5212209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2224596 2224597 2224598 2224599 2224600 2224601 2228573 2228574 2228575 2228576 [...] (5161415 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289870 2289871 2289872 2289873 2289874 2289875 2289876 2289877 2289878 2289879 [...] (5110843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289887 2316816 2316817 2316818 2316819 2316820 2316821 2316822 2316823 2316824 [...] (5060136 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2364292 2364293 2364294 2364295 2364296 2364297 2364298 2364299 2375676 2375677 [...] (5009169 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2439198 2439199 2439200 2439201 2439202 2439203 2439204 2439205 2439206 2439207 [...] (4958835 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2439214 2439215 2481585 2481586 2481587 2482009 2482010 2482011 2482012 2482013 [...] (4907833 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2524860 2524861 2524862 2524863 2524864 2524865 2524866 2524867 2524868 2524869 [...] (4857332 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2584511 2627051 2627052 2627053 2627054 2627055 2627056 2627057 2627058 2627059 [...] (4806756 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2651597 2667240 2667241 2667242 2667243 2667244 2667245 2667246 2667247 2667248 [...] (4755466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2733516 2733517 2733518 2733519 2733520 2733521 2733522 2733523 2733524 2733525 [...] (4704501 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2733524 2733525 2733526 2733527 2733528 2733529 2733530 2733531 2733532 2733533 [...] (4653235 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2778746 2778747 2778748 2778749 2843205 2843206 2843207 2849505 2849506 2849507 [...] (4601981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2900685 2900686 2900687 2900688 2900689 2900690 2900691 2900692 2900693 2900694 [...] (4551053 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2941704 2941705 2941706 2941707 2941708 2941709 2941710 2941711 2941712 2941713 [...] (4499808 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3037446 3037447 3037448 3037449 3037450 3037451 3037452 3037453 3037454 3037455 [...] (4448731 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3037446 3037447 3037448 3037449 3037450 3037451 3037452 3037453 3037454 3037455 [...] (4397870 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3184173 3184174 3184175 3184176 3184177 3184178 3184179 3184180 3184181 3221295 [...] (4347239 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3293334 3293335 3293336 3293337 3293338 3293339 3293340 3293341 3293342 3293343 [...] (4296514 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3341072 3341073 3341074 3341075 3341076 3341077 3341078 3341079 3341080 3341081 [...] (4245417 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3443057 3448158 3448159 3448160 3448161 3448162 3448163 3448164 3448165 3448166 [...] (4194635 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3531420 3531421 3531422 3531423 3531424 3531425 3531426 3531427 3531428 3531429 [...] (4142931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3531429 3531430 3531431 3531432 3531433 3531434 3531435 3531436 3531437 3536494 [...] (4092125 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3615783 3615784 3615785 3640332 3640333 3640334 3640335 3640336 3640337 3641553 [...] (4041073 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3640336 3640337 3648564 3648565 3648566 3648567 3648568 3648569 3648570 3648571 [...] (3989993 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3666024 3666025 3666026 3666027 3666028 3666029 3666030 3666031 3666032 3666033 [...] (3938710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3713569 3713570 3713571 3713572 3713573 3713574 3713575 3713576 3713577 3713578 [...] (3888160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3773278 3773279 3773280 3773281 3773282 3773283 3773284 3773285 3774114 3774115 [...] (3837713 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3774114 3774115 3774116 3774117 3774118 3774119 3774120 3774121 3774122 3774123 [...] (3787317 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3774129 3774130 3774131 3778788 3778789 3778790 3778791 3778792 3778793 3870360 [...] (3737591 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3874565 3874566 3874567 3874568 3874569 3874570 3874571 3874761 3874762 3874763 [...] (3688428 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3879522 3879523 3879524 3879525 3879526 3879527 3879528 3879529 3879530 3879531 [...] (3639260 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3935608 3935609 3947183 3953644 3953645 3954672 3954673 3954674 3954675 3954676 [...] (3590507 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3955644 3955645 3955646 3955647 3955648 3955649 3955650 3955651 3955652 3955653 [...] (3541761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3968674 3968675 3969522 3969523 3969524 3969525 3969526 3969527 3969528 3969529 [...] (3493737 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3969538 3969539 3972780 3972781 3972782 3972783 3972784 3972785 3972786 3972787 [...] (3445447 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3972780 3972781 3972782 3972783 3972784 3972785 3972786 3972787 3972788 3972789 [...] (3397241 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3972791 3972792 3972793 3972794 3972795 3972796 3972797 4066669 4066670 4066671 [...] (3349119 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4105872 4105873 4105874 4105875 4105876 4105877 4105878 4105879 4105880 4105881 [...] (3301276 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4128712 4128713 4130208 4130209 4130210 4130211 4130212 4130213 4130214 4130215 [...] (3253688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4145884 4145885 4155239 4155240 4155241 4155242 4155243 4155244 4155245 4161456 [...] (3205676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4161456 4161457 4161458 4161459 4161460 4161461 4161462 4161463 4161464 4161465 [...] (3158450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4161456 4161457 4161458 4161459 4161460 4161461 4161462 4161463 4161464 4161465 [...] (3111277 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4176414 4176415 4176416 4176417 4176418 4176419 4176420 4176421 4176422 4176423 [...] (3063770 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4176414 4176415 4176416 4176417 4176418 4176419 4176420 4176421 4176422 4176423 [...] (3016037 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4176428 4176429 4176430 4176431 4284048 4284049 4284050 4284051 4284052 4284053 [...] (2968562 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4355188 4355189 4371694 4371695 4371912 4371913 4371914 4371915 4371916 4371917 [...] (2921158 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4383496 4383497 4383498 4383499 4383500 4383501 4383502 4383503 4412664 4412665 [...] (2872889 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4420008 4420009 4420010 4420011 4420012 4420013 4420014 4420015 4420016 4420017 [...] (2825108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4426709 4426710 4426711 4426712 4426713 4426714 4426715 4426716 4426717 4426718 [...] (2777338 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4518805 4518806 4518807 4518808 4518809 4521042 4521043 4521044 4521045 4521046 [...] (2729739 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4528204 4528205 4544368 4544369 4550542 4550543 4577994 4577995 4577996 4577997 [...] (2681802 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4550542 4550543 4578010 4578011 4592754 4592755 4592756 4592757 4592758 4592759 [...] (2634264 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4578010 4578011 4617036 4617037 4617038 4617039 4617040 4617041 4617042 4617043 [...] (2586499 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4578010 4578011 4617036 4617037 4617038 4617039 4617040 4617041 4617042 4617043 [...] (2538432 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4666426 4666427 4675374 4675375 4675376 4675377 4675378 4675379 4675380 4675381 [...] (2490442 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4679442 4679443 4679444 4679445 4679446 4679447 4679448 4679449 4679450 4679451 [...] (2442880 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4679442 4679443 4679444 4679445 4679446 4679447 4679448 4679449 4679450 4679451 [...] (2394754 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4679442 4679443 4679444 4679445 4679446 4679447 4679448 4679449 4679450 4679451 [...] (2346726 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4824666 4824667 4824668 4824669 4824670 4824671 4824672 4824673 4824674 4824675 [...] (2299012 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4824666 4824667 4824668 4824669 4824670 4824671 4824672 4824673 4824674 4824675 [...] (2250820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4862898 4862899 4862900 4862901 4862902 4862903 4862904 4862905 4862906 4862907 [...] (2202937 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4862898 4862899 4862900 4862901 4862902 4862903 4862904 4862905 4862906 4862907 [...] (2154877 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4862898 4862899 4862900 4862901 4862902 4862903 4862904 4862905 4862906 4862907 [...] (2107052 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4862898 4862899 4862900 4862901 4862902 4862903 4862904 4862905 4862906 4862907 [...] (2059087 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4862911 4862912 4862913 4862914 4862915 4883596 4883597 5046858 5046859 5046860 [...] (2011467 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5046858 5046859 5046860 5046861 5046862 5046863 5046864 5046865 5046866 5046867 [...] (1963445 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5046858 5046859 5046860 5046861 5046862 5046863 5046864 5046865 5046866 5046867 [...] (1915587 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5085432 5085433 5085434 5085435 5085436 5085437 5085438 5085439 5085440 5085441 [...] (1867839 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5085432 5085433 5085434 5085435 5085436 5085437 5085438 5085439 5085440 5085441 [...] (1819890 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5085432 5085433 5085434 5085435 5085436 5085437 5085438 5085439 5085440 5085441 [...] (1772246 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5085432 5085433 5085434 5085435 5085436 5085437 5085438 5085439 5085440 5085441 [...] (1724684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5085448 5085449 5091336 5091337 5091338 5091339 5091340 5091341 5091342 5091343 [...] (1676621 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5091336 5091337 5091338 5091339 5091340 5091341 5091342 5091343 5091344 5091345 [...] (1629239 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5091336 5091337 5091338 5091339 5091340 5091341 5091342 5091343 5091344 5091345 [...] (1581496 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5150700 5150701 5150702 5150703 5150704 5150705 5150706 5150707 5150708 5150709 [...] (1534050 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5150700 5150701 5150702 5150703 5150704 5150705 5150706 5150707 5150708 5150709 [...] (1486362 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5150700 5150701 5150702 5150703 5150704 5150705 5150706 5150707 5150708 5150709 [...] (1438622 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5394186 5394187 5394188 5394189 5394190 5394191 5394192 5394193 5394194 5394195 [...] (1390965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5394186 5394187 5394188 5394189 5394190 5394191 5394192 5394193 5394194 5394195 [...] (1342916 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5394200 5394201 5394202 5394203 5429574 5429575 5429576 5429577 5429578 5429579 [...] (1295189 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5429580 5429581 5429582 5429583 5429584 5429585 5429586 5429587 5429588 5429589 [...] (1247182 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5634034 5634035 5638896 5638897 5638898 5638899 5638900 5638901 5638902 5638903 [...] (1199542 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5638896 5638897 5638898 5638899 5638900 5638901 5638902 5638903 5638904 5638905 [...] (1151944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5638896 5638897 5638898 5638899 5638900 5638901 5638902 5638903 5638904 5638905 [...] (1104268 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5834376 5834377 5834378 5834379 5834380 5834381 5834382 5834383 5834384 5834385 [...] (1056681 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5834376 5834377 5834378 5834379 5834380 5834381 5834382 5834383 5834384 5834385 [...] (1009550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5834376 5834377 5834378 5834379 5834380 5834381 5834382 5834383 5834384 5834385 [...] (961751 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5834376 5834377 5834378 5834379 5834380 5834381 5834382 5834383 5834384 5834385 [...] (913764 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5834376 5834377 5834378 5834379 5834380 5834381 5834382 5834383 5834384 5834385 [...] (866810 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6012072 6012073 6012074 6012075 6012076 6012077 6012078 6012079 6012080 6012081 [...] (819203 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6012072 6012073 6012074 6012075 6012076 6012077 6012078 6012079 6012080 6012081 [...] (771646 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6012086 6012087 6012088 6012089 6158322 6158323 6158324 6158325 6158326 6158327 [...] (723663 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6158322 6158323 6158324 6158325 6158326 6158327 6158328 6158329 6158330 6158331 [...] (675850 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6158322 6158323 6158324 6158325 6158326 6158327 6158328 6158329 6158330 6158331 [...] (628232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6158322 6158323 6158324 6158325 6158326 6158327 6158328 6158329 6158330 6158331 [...] (580361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6414030 6414031 6414032 6414033 6414034 6414035 6414036 6414037 6414038 6414039 [...] (532604 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6441084 6441085 6441086 6441087 6441088 6441089 6441090 6441091 6441092 6441093 [...] (484501 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6441084 6441085 6441086 6441087 6441088 6441089 6441090 6441091 6441092 6441093 [...] (436693 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6441084 6441085 6441086 6441087 6441088 6441089 6441090 6441091 6441092 6441093 [...] (388877 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6441088 6441089 6441090 6441091 6441092 6441093 6441094 6441095 6441096 6441097 [...] (341196 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6441100 6441101 6457788 6457789 6457790 6457791 6457792 6457793 6457794 6457795 [...] (293405 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6457788 6457789 6457790 6457791 6457792 6457793 6457794 6457795 6457796 6457797 [...] (245901 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6587442 6587443 6587444 6587445 6587446 6587447 6587448 6587449 6587450 6587451 [...] (198017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6587442 6587443 6587444 6587445 6587446 6587447 6587448 6587449 6587450 6587451 [...] (150524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6587442 6587443 6587444 6587445 6587446 6587447 6587448 6587449 6587450 6587451 [...] (104642 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6695118 6695119 6695120 6695121 6695122 6695123 6695124 6695125 6695126 6695127 [...] (62213 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 6695118 6695119 6695120 6695121 6695122 6695123 6695124 6695125 6695126 6695127 [...] (30125 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 7196614 7196615 7211448 7211449 7211450 7211451 7211452 7211453 7211454 7211455 [...] (10381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 7504326 7504327 7504328 7504329 7504330 7504331 7504332 7504333 7504334 7504335 [...] (1776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 8100216 8100217 8100218 8100219 8100220 8100221 8100222 8100223 8100224 8100225 [...] (351 flits)
Measured flits: (0 flits)
Time taken is 167324 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16874.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 41037 (1 samples)
Network latency average = 16609.8 (1 samples)
	minimum = 22 (1 samples)
	maximum = 40203 (1 samples)
Flit latency average = 54683.3 (1 samples)
	minimum = 5 (1 samples)
	maximum = 125668 (1 samples)
Fragmentation average = 435.178 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1114 (1 samples)
Injected packet rate average = 0.0545476 (1 samples)
	minimum = 0.0492857 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.018 (1 samples)
	minimum = 0.0148571 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.981708 (1 samples)
	minimum = 0.886857 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.325945 (1 samples)
	minimum = 0.265429 (1 samples)
	maximum = 0.398286 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 18.1081 (1 samples)
Hops average = 5.07574 (1 samples)
Total run time 316.471
