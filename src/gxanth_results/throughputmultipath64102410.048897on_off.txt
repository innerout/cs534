BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 379.58
	minimum = 23
	maximum = 990
Network latency average = 275.714
	minimum = 23
	maximum = 924
Slowest packet = 46
Flit latency average = 204.827
	minimum = 6
	maximum = 960
Slowest flit = 3220
Fragmentation average = 159.377
	minimum = 0
	maximum = 809
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0103229
	minimum = 0.004 (at node 25)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.218099
	minimum = 0.101 (at node 25)
	maximum = 0.378 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 21.1276
Total in-flight flits = 60959 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 633.995
	minimum = 23
	maximum = 1976
Network latency average = 480.376
	minimum = 23
	maximum = 1908
Slowest packet = 46
Flit latency average = 391.179
	minimum = 6
	maximum = 1891
Slowest flit = 6569
Fragmentation average = 223.635
	minimum = 0
	maximum = 1582
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0117891
	minimum = 0.007 (at node 170)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.233935
	minimum = 0.1495 (at node 153)
	maximum = 0.351 (at node 34)
Injected packet length average = 17.9125
Accepted packet length average = 19.8434
Total in-flight flits = 126943 (0 measured)
latency change    = 0.401288
throughput change = 0.0676938
Class 0:
Packet latency average = 1263.87
	minimum = 31
	maximum = 2975
Network latency average = 1006.55
	minimum = 23
	maximum = 2784
Slowest packet = 160
Flit latency average = 918.952
	minimum = 6
	maximum = 2862
Slowest flit = 7435
Fragmentation average = 318.635
	minimum = 0
	maximum = 2642
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0141823
	minimum = 0.006 (at node 125)
	maximum = 0.029 (at node 68)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.261193
	minimum = 0.121 (at node 125)
	maximum = 0.512 (at node 68)
Injected packet length average = 17.9975
Accepted packet length average = 18.4168
Total in-flight flits = 198960 (0 measured)
latency change    = 0.49837
throughput change = 0.104359
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 490.089
	minimum = 27
	maximum = 2085
Network latency average = 107.764
	minimum = 25
	maximum = 923
Slowest packet = 18852
Flit latency average = 1351.3
	minimum = 6
	maximum = 3892
Slowest flit = 8853
Fragmentation average = 51.1973
	minimum = 0
	maximum = 615
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0144479
	minimum = 0.007 (at node 48)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.263844
	minimum = 0.117 (at node 48)
	maximum = 0.447 (at node 103)
Injected packet length average = 18.0134
Accepted packet length average = 18.2617
Total in-flight flits = 277686 (116234 measured)
latency change    = 1.57886
throughput change = 0.0100478
Class 0:
Packet latency average = 692.233
	minimum = 27
	maximum = 3196
Network latency average = 336.666
	minimum = 25
	maximum = 1917
Slowest packet = 18852
Flit latency average = 1596.96
	minimum = 6
	maximum = 4780
Slowest flit = 18214
Fragmentation average = 117.401
	minimum = 0
	maximum = 1542
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0144896
	minimum = 0.0075 (at node 10)
	maximum = 0.023 (at node 103)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.263128
	minimum = 0.1355 (at node 48)
	maximum = 0.414 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 18.1598
Total in-flight flits = 352709 (222634 measured)
latency change    = 0.292017
throughput change = 0.00272167
Class 0:
Packet latency average = 967.59
	minimum = 27
	maximum = 4172
Network latency average = 622.947
	minimum = 25
	maximum = 2953
Slowest packet = 18852
Flit latency average = 1843.77
	minimum = 6
	maximum = 5721
Slowest flit = 24843
Fragmentation average = 156.965
	minimum = 0
	maximum = 1741
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0145122
	minimum = 0.00933333 (at node 79)
	maximum = 0.0213333 (at node 103)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.262165
	minimum = 0.184 (at node 79)
	maximum = 0.380667 (at node 103)
Injected packet length average = 18.0004
Accepted packet length average = 18.0652
Total in-flight flits = 429049 (325016 measured)
latency change    = 0.28458
throughput change = 0.00367202
Draining remaining packets ...
Class 0:
Remaining flits: 21474 21475 21476 21477 21478 21479 21480 21481 21482 21483 [...] (389907 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (308070 flits)
Class 0:
Remaining flits: 21474 21475 21476 21477 21478 21479 21480 21481 21482 21483 [...] (351485 flits)
Measured flits: 338958 338959 338960 338961 338962 338963 338964 338965 338966 338967 [...] (287758 flits)
Class 0:
Remaining flits: 21483 21484 21485 21486 21487 21488 21489 21490 21491 21623 [...] (313370 flits)
Measured flits: 338976 338977 338978 338979 338980 338981 338982 338983 338984 338985 [...] (264791 flits)
Class 0:
Remaining flits: 21623 21624 21625 21626 21627 21628 21629 21630 21631 21632 [...] (275860 flits)
Measured flits: 338976 338977 338978 338979 338980 338981 338982 338983 338984 338985 [...] (238685 flits)
Class 0:
Remaining flits: 21623 21624 21625 21626 21627 21628 21629 21630 21631 21632 [...] (239317 flits)
Measured flits: 338976 338977 338978 338979 338980 338981 338982 338983 338984 338985 [...] (210417 flits)
Class 0:
Remaining flits: 21623 21624 21625 21626 21627 21628 21629 21630 21631 21632 [...] (203575 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (181128 flits)
Class 0:
Remaining flits: 21783 21784 21785 21786 21787 21788 21789 21790 21791 21792 [...] (169082 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (151214 flits)
Class 0:
Remaining flits: 43704 43705 43706 43707 43708 43709 43710 43711 43712 43713 [...] (135906 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (122086 flits)
Class 0:
Remaining flits: 43704 43705 43706 43707 43708 43709 43710 43711 43712 43713 [...] (104365 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (93899 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (75978 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (68280 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (49641 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (44201 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (27126 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (24136 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (9586 flits)
Measured flits: 339030 339031 339032 339033 339034 339035 339036 339037 339038 339039 [...] (8330 flits)
Class 0:
Remaining flits: 81036 81037 81038 81039 81040 81041 81042 81043 81044 81045 [...] (2447 flits)
Measured flits: 344682 344683 344684 344685 344686 344687 344688 344689 344690 344691 [...] (1993 flits)
Class 0:
Remaining flits: 167939 178110 178111 178112 178113 178114 178115 178116 178117 178118 [...] (888 flits)
Measured flits: 344682 344683 344684 344685 344686 344687 344688 344689 344690 344691 [...] (707 flits)
Time taken is 21785 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7581.93 (1 samples)
	minimum = 27 (1 samples)
	maximum = 19016 (1 samples)
Network latency average = 7159.41 (1 samples)
	minimum = 25 (1 samples)
	maximum = 18575 (1 samples)
Flit latency average = 6383.39 (1 samples)
	minimum = 6 (1 samples)
	maximum = 19829 (1 samples)
Fragmentation average = 288.253 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7531 (1 samples)
Injected packet rate average = 0.0367569 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0145122 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.661641 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.262165 (1 samples)
	minimum = 0.184 (1 samples)
	maximum = 0.380667 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 18.0652 (1 samples)
Hops average = 5.07855 (1 samples)
Total run time 28.388
