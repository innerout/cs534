BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.018
	minimum = 25
	maximum = 984
Network latency average = 262.03
	minimum = 25
	maximum = 968
Slowest packet = 144
Flit latency average = 189.781
	minimum = 6
	maximum = 951
Slowest flit = 2609
Fragmentation average = 152.68
	minimum = 0
	maximum = 765
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00945833
	minimum = 0.004 (at node 41)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.199948
	minimum = 0.095 (at node 3)
	maximum = 0.35 (at node 70)
Injected packet length average = 17.8416
Accepted packet length average = 21.1399
Total in-flight flits = 41278 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.291
	minimum = 25
	maximum = 1919
Network latency average = 450.232
	minimum = 24
	maximum = 1799
Slowest packet = 167
Flit latency average = 356.61
	minimum = 6
	maximum = 1925
Slowest flit = 5092
Fragmentation average = 217.861
	minimum = 0
	maximum = 1468
Injected packet rate average = 0.0231823
	minimum = 0.002 (at node 118)
	maximum = 0.056 (at node 133)
Accepted packet rate average = 0.0108385
	minimum = 0.006 (at node 153)
	maximum = 0.0165 (at node 98)
Injected flit rate average = 0.415701
	minimum = 0.036 (at node 118)
	maximum = 1 (at node 133)
Accepted flit rate average= 0.214633
	minimum = 0.116 (at node 164)
	maximum = 0.3155 (at node 78)
Injected packet length average = 17.9318
Accepted packet length average = 19.8027
Total in-flight flits = 77817 (0 measured)
latency change    = 0.389077
throughput change = 0.0684187
Class 0:
Packet latency average = 1102.15
	minimum = 29
	maximum = 2894
Network latency average = 959.213
	minimum = 23
	maximum = 2757
Slowest packet = 282
Flit latency average = 860.32
	minimum = 6
	maximum = 2956
Slowest flit = 3227
Fragmentation average = 323.964
	minimum = 0
	maximum = 2198
Injected packet rate average = 0.0240729
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 25)
Accepted packet rate average = 0.012849
	minimum = 0.005 (at node 147)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.432667
	minimum = 0 (at node 15)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.237667
	minimum = 0.082 (at node 147)
	maximum = 0.404 (at node 99)
Injected packet length average = 17.9732
Accepted packet length average = 18.497
Total in-flight flits = 115381 (0 measured)
latency change    = 0.497992
throughput change = 0.0969166
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 375.058
	minimum = 28
	maximum = 2015
Network latency average = 215.872
	minimum = 27
	maximum = 984
Slowest packet = 13563
Flit latency average = 1200.26
	minimum = 6
	maximum = 3797
Slowest flit = 10997
Fragmentation average = 106.352
	minimum = 0
	maximum = 629
Injected packet rate average = 0.0246563
	minimum = 0 (at node 68)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0133698
	minimum = 0.003 (at node 175)
	maximum = 0.023 (at node 37)
Injected flit rate average = 0.443568
	minimum = 0 (at node 68)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.242891
	minimum = 0.098 (at node 175)
	maximum = 0.411 (at node 37)
Injected packet length average = 17.9901
Accepted packet length average = 18.1671
Total in-flight flits = 153958 (73294 measured)
latency change    = 1.93862
throughput change = 0.0215075
Class 0:
Packet latency average = 714.818
	minimum = 28
	maximum = 2785
Network latency average = 548.864
	minimum = 27
	maximum = 1961
Slowest packet = 13563
Flit latency average = 1405.75
	minimum = 6
	maximum = 4709
Slowest flit = 19901
Fragmentation average = 192.814
	minimum = 0
	maximum = 1564
Injected packet rate average = 0.0238932
	minimum = 0.002 (at node 72)
	maximum = 0.0555 (at node 19)
Accepted packet rate average = 0.0132448
	minimum = 0.0075 (at node 149)
	maximum = 0.02 (at node 123)
Injected flit rate average = 0.430354
	minimum = 0.036 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.24106
	minimum = 0.1485 (at node 98)
	maximum = 0.351 (at node 151)
Injected packet length average = 18.0116
Accepted packet length average = 18.2004
Total in-flight flits = 187964 (131299 measured)
latency change    = 0.475309
throughput change = 0.0075945
Class 0:
Packet latency average = 1059.09
	minimum = 28
	maximum = 3552
Network latency average = 888.558
	minimum = 27
	maximum = 2992
Slowest packet = 13563
Flit latency average = 1617.3
	minimum = 6
	maximum = 5792
Slowest flit = 15069
Fragmentation average = 237.288
	minimum = 0
	maximum = 1704
Injected packet rate average = 0.023401
	minimum = 0.00666667 (at node 185)
	maximum = 0.0556667 (at node 65)
Accepted packet rate average = 0.0131823
	minimum = 0.008 (at node 135)
	maximum = 0.0193333 (at node 123)
Injected flit rate average = 0.421082
	minimum = 0.119333 (at node 185)
	maximum = 1 (at node 65)
Accepted flit rate average= 0.238299
	minimum = 0.144 (at node 135)
	maximum = 0.326667 (at node 87)
Injected packet length average = 17.9941
Accepted packet length average = 18.0772
Total in-flight flits = 220743 (181591 measured)
latency change    = 0.325062
throughput change = 0.0115875
Draining remaining packets ...
Class 0:
Remaining flits: 30708 30709 30710 30711 30712 30713 30714 30715 30716 30717 [...] (181381 flits)
Measured flits: 243466 243467 243504 243505 243506 243507 243508 243509 243510 243511 [...] (155114 flits)
Class 0:
Remaining flits: 30724 30725 34740 34741 34742 34743 34744 34745 34746 34747 [...] (142621 flits)
Measured flits: 243467 243504 243505 243506 243507 243508 243509 243510 243511 243512 [...] (125089 flits)
Class 0:
Remaining flits: 30724 30725 34740 34741 34742 34743 34744 34745 34746 34747 [...] (105336 flits)
Measured flits: 243504 243505 243506 243507 243508 243509 243510 243511 243512 243513 [...] (93070 flits)
Class 0:
Remaining flits: 30724 30725 34740 34741 34742 34743 34744 34745 34746 34747 [...] (70939 flits)
Measured flits: 243504 243505 243506 243507 243508 243509 243510 243511 243512 243513 [...] (62432 flits)
Class 0:
Remaining flits: 30724 30725 42966 42967 42968 42969 42970 42971 42972 42973 [...] (41309 flits)
Measured flits: 243504 243505 243506 243507 243508 243509 243510 243511 243512 243513 [...] (36048 flits)
Class 0:
Remaining flits: 42966 42967 42968 42969 42970 42971 42972 42973 42974 42975 [...] (17741 flits)
Measured flits: 243684 243685 243686 243687 243688 243689 243690 243691 243692 243693 [...] (15529 flits)
Class 0:
Remaining flits: 96936 96937 96938 96939 96940 96941 96942 96943 96944 96945 [...] (4713 flits)
Measured flits: 243696 243697 243698 243699 243700 243701 247284 247285 247286 247287 [...] (4035 flits)
Class 0:
Remaining flits: 163674 163675 163676 163677 163678 163679 163680 163681 163682 163683 [...] (452 flits)
Measured flits: 263622 263623 263624 263625 263626 263627 272934 272935 272936 272937 [...] (423 flits)
Time taken is 14348 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4017.89 (1 samples)
	minimum = 28 (1 samples)
	maximum = 11180 (1 samples)
Network latency average = 3788.19 (1 samples)
	minimum = 27 (1 samples)
	maximum = 10927 (1 samples)
Flit latency average = 3593.08 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12492 (1 samples)
Fragmentation average = 305.501 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6876 (1 samples)
Injected packet rate average = 0.023401 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0131823 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.421082 (1 samples)
	minimum = 0.119333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.238299 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.326667 (1 samples)
Injected packet size average = 17.9941 (1 samples)
Accepted packet size average = 18.0772 (1 samples)
Hops average = 5.11076 (1 samples)
Total run time 18.3069
