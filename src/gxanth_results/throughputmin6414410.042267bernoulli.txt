BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.007
	minimum = 27
	maximum = 920
Network latency average = 307.891
	minimum = 23
	maximum = 871
Slowest packet = 252
Flit latency average = 236.843
	minimum = 6
	maximum = 953
Slowest flit = 4232
Fragmentation average = 185.775
	minimum = 0
	maximum = 737
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0112031
	minimum = 0.004 (at node 101)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.240089
	minimum = 0.083 (at node 150)
	maximum = 0.407 (at node 48)
Injected packet length average = 17.8523
Accepted packet length average = 21.4305
Total in-flight flits = 95275 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 576.548
	minimum = 26
	maximum = 1878
Network latency average = 556.539
	minimum = 23
	maximum = 1869
Slowest packet = 697
Flit latency average = 469.36
	minimum = 6
	maximum = 1983
Slowest flit = 1372
Fragmentation average = 250.323
	minimum = 0
	maximum = 1462
Injected packet rate average = 0.0410885
	minimum = 0.029 (at node 184)
	maximum = 0.051 (at node 146)
Accepted packet rate average = 0.0125156
	minimum = 0.0075 (at node 12)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.736323
	minimum = 0.5165 (at node 184)
	maximum = 0.91 (at node 146)
Accepted flit rate average= 0.249424
	minimum = 0.1595 (at node 135)
	maximum = 0.4145 (at node 152)
Injected packet length average = 17.9204
Accepted packet length average = 19.929
Total in-flight flits = 188225 (0 measured)
latency change    = 0.438022
throughput change = 0.0374299
Class 0:
Packet latency average = 1335.27
	minimum = 28
	maximum = 2900
Network latency average = 1304.55
	minimum = 28
	maximum = 2900
Slowest packet = 639
Flit latency average = 1235.71
	minimum = 6
	maximum = 2887
Slowest flit = 12459
Fragmentation average = 358.273
	minimum = 2
	maximum = 2757
Injected packet rate average = 0.0355469
	minimum = 0.005 (at node 136)
	maximum = 0.055 (at node 29)
Accepted packet rate average = 0.0138594
	minimum = 0.006 (at node 81)
	maximum = 0.023 (at node 0)
Injected flit rate average = 0.639062
	minimum = 0.09 (at node 136)
	maximum = 0.997 (at node 29)
Accepted flit rate average= 0.252568
	minimum = 0.107 (at node 162)
	maximum = 0.417 (at node 0)
Injected packet length average = 17.978
Accepted packet length average = 18.2236
Total in-flight flits = 262582 (0 measured)
latency change    = 0.568215
throughput change = 0.0124451
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 327.041
	minimum = 29
	maximum = 2081
Network latency average = 144.692
	minimum = 29
	maximum = 808
Slowest packet = 22622
Flit latency average = 1928.13
	minimum = 6
	maximum = 3870
Slowest flit = 13558
Fragmentation average = 19.085
	minimum = 2
	maximum = 506
Injected packet rate average = 0.0351771
	minimum = 0.002 (at node 56)
	maximum = 0.056 (at node 81)
Accepted packet rate average = 0.0131667
	minimum = 0.005 (at node 153)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.633479
	minimum = 0.034 (at node 56)
	maximum = 1 (at node 81)
Accepted flit rate average= 0.239453
	minimum = 0.091 (at node 163)
	maximum = 0.415 (at node 129)
Injected packet length average = 18.0083
Accepted packet length average = 18.1863
Total in-flight flits = 338179 (115372 measured)
latency change    = 3.08287
throughput change = 0.0547689
Class 0:
Packet latency average = 575.124
	minimum = 29
	maximum = 2999
Network latency average = 310.311
	minimum = 29
	maximum = 1897
Slowest packet = 22622
Flit latency average = 2316.78
	minimum = 6
	maximum = 4816
Slowest flit = 13787
Fragmentation average = 26.4003
	minimum = 2
	maximum = 774
Injected packet rate average = 0.0353047
	minimum = 0.006 (at node 56)
	maximum = 0.0515 (at node 81)
Accepted packet rate average = 0.0130495
	minimum = 0.005 (at node 163)
	maximum = 0.0195 (at node 173)
Injected flit rate average = 0.635734
	minimum = 0.1095 (at node 56)
	maximum = 0.9235 (at node 81)
Accepted flit rate average= 0.23607
	minimum = 0.127 (at node 163)
	maximum = 0.339 (at node 159)
Injected packet length average = 18.0071
Accepted packet length average = 18.0904
Total in-flight flits = 415975 (232125 measured)
latency change    = 0.431355
throughput change = 0.0143297
Class 0:
Packet latency average = 917.076
	minimum = 29
	maximum = 4134
Network latency average = 534.814
	minimum = 28
	maximum = 2928
Slowest packet = 22622
Flit latency average = 2663.96
	minimum = 6
	maximum = 5816
Slowest flit = 22256
Fragmentation average = 44.3043
	minimum = 2
	maximum = 988
Injected packet rate average = 0.0344392
	minimum = 0.00933333 (at node 56)
	maximum = 0.0493333 (at node 151)
Accepted packet rate average = 0.0129965
	minimum = 0.007 (at node 64)
	maximum = 0.018 (at node 165)
Injected flit rate average = 0.620184
	minimum = 0.167333 (at node 56)
	maximum = 0.888 (at node 151)
Accepted flit rate average= 0.23467
	minimum = 0.13 (at node 64)
	maximum = 0.343 (at node 165)
Injected packet length average = 18.0081
Accepted packet length average = 18.0564
Total in-flight flits = 484550 (338027 measured)
latency change    = 0.372873
throughput change = 0.00596656
Class 0:
Packet latency average = 1500.48
	minimum = 29
	maximum = 4922
Network latency average = 949.371
	minimum = 28
	maximum = 3917
Slowest packet = 22622
Flit latency average = 2996.48
	minimum = 6
	maximum = 6878
Slowest flit = 10277
Fragmentation average = 83.7515
	minimum = 2
	maximum = 1493
Injected packet rate average = 0.0319036
	minimum = 0.011 (at node 56)
	maximum = 0.04575 (at node 103)
Accepted packet rate average = 0.0129206
	minimum = 0.00875 (at node 64)
	maximum = 0.01775 (at node 68)
Injected flit rate average = 0.574482
	minimum = 0.19675 (at node 56)
	maximum = 0.824 (at node 103)
Accepted flit rate average= 0.23244
	minimum = 0.157 (at node 36)
	maximum = 0.333 (at node 165)
Injected packet length average = 18.0068
Accepted packet length average = 17.9899
Total in-flight flits = 525212 (412844 measured)
latency change    = 0.388813
throughput change = 0.00959402
Draining remaining packets ...
Class 0:
Remaining flits: 7722 7723 7724 7725 7726 7727 7728 7729 7730 7731 [...] (484679 flits)
Measured flits: 406864 406865 406866 406867 406868 406869 406870 406871 406890 406891 [...] (402336 flits)
Class 0:
Remaining flits: 7722 7723 7724 7725 7726 7727 7728 7729 7730 7731 [...] (444676 flits)
Measured flits: 406908 406909 406910 406911 406912 406913 406914 406915 406916 406917 [...] (384170 flits)
Class 0:
Remaining flits: 7722 7723 7724 7725 7726 7727 7728 7729 7730 7731 [...] (404869 flits)
Measured flits: 406926 406927 406928 406929 406930 406931 406932 406933 406934 406935 [...] (359065 flits)
Class 0:
Remaining flits: 7735 7736 7737 7738 7739 41583 41584 41585 41586 41587 [...] (365613 flits)
Measured flits: 406926 406927 406928 406929 406930 406931 406932 406933 406934 406935 [...] (330093 flits)
Class 0:
Remaining flits: 44864 44865 44866 44867 44868 44869 44870 44871 44872 44873 [...] (327597 flits)
Measured flits: 406926 406927 406928 406929 406930 406931 406932 406933 406934 406935 [...] (298953 flits)
Class 0:
Remaining flits: 45468 45469 45470 45471 45472 45473 45474 45475 45476 45477 [...] (291228 flits)
Measured flits: 406926 406927 406928 406929 406930 406931 406932 406933 406934 406935 [...] (267773 flits)
Class 0:
Remaining flits: 45468 45469 45470 45471 45472 45473 45474 45475 45476 45477 [...] (256273 flits)
Measured flits: 406926 406927 406928 406929 406930 406931 406932 406933 406934 406935 [...] (237593 flits)
Class 0:
Remaining flits: 45468 45469 45470 45471 45472 45473 45474 45475 45476 45477 [...] (222024 flits)
Measured flits: 406980 406981 406982 406983 406984 406985 406986 406987 406988 406989 [...] (206727 flits)
Class 0:
Remaining flits: 45468 45469 45470 45471 45472 45473 45474 45475 45476 45477 [...] (188553 flits)
Measured flits: 407288 407289 407290 407291 407292 407293 407294 407295 407296 407297 [...] (176290 flits)
Class 0:
Remaining flits: 45468 45469 45470 45471 45472 45473 45474 45475 45476 45477 [...] (156056 flits)
Measured flits: 407288 407289 407290 407291 407292 407293 407294 407295 407296 407297 [...] (146423 flits)
Class 0:
Remaining flits: 60498 60499 60500 60501 60502 60503 60504 60505 60506 60507 [...] (124008 flits)
Measured flits: 407340 407341 407342 407343 407344 407345 407346 407347 407348 407349 [...] (116553 flits)
Class 0:
Remaining flits: 60498 60499 60500 60501 60502 60503 60504 60505 60506 60507 [...] (93194 flits)
Measured flits: 407340 407341 407342 407343 407344 407345 407346 407347 407348 407349 [...] (87880 flits)
Class 0:
Remaining flits: 60498 60499 60500 60501 60502 60503 60504 60505 60506 60507 [...] (63189 flits)
Measured flits: 407340 407341 407342 407343 407344 407345 407346 407347 407348 407349 [...] (59618 flits)
Class 0:
Remaining flits: 92203 92204 92205 92206 92207 92208 92209 92210 92211 92212 [...] (34792 flits)
Measured flits: 407340 407341 407342 407343 407344 407345 407346 407347 407348 407349 [...] (32643 flits)
Class 0:
Remaining flits: 124452 124453 124454 124455 124456 124457 124458 124459 124460 124461 [...] (12903 flits)
Measured flits: 407808 407809 407810 407811 407812 407813 407814 407815 407816 407817 [...] (12172 flits)
Class 0:
Remaining flits: 303696 303697 303698 303699 303700 303701 303702 303703 303704 303705 [...] (2909 flits)
Measured flits: 407808 407809 407810 407811 407812 407813 407814 407815 407816 407817 [...] (2838 flits)
Time taken is 23955 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9980.77 (1 samples)
	minimum = 29 (1 samples)
	maximum = 20830 (1 samples)
Network latency average = 9704.26 (1 samples)
	minimum = 28 (1 samples)
	maximum = 20339 (1 samples)
Flit latency average = 8057.87 (1 samples)
	minimum = 6 (1 samples)
	maximum = 22012 (1 samples)
Fragmentation average = 249.723 (1 samples)
	minimum = 0 (1 samples)
	maximum = 9270 (1 samples)
Injected packet rate average = 0.0319036 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.04575 (1 samples)
Accepted packet rate average = 0.0129206 (1 samples)
	minimum = 0.00875 (1 samples)
	maximum = 0.01775 (1 samples)
Injected flit rate average = 0.574482 (1 samples)
	minimum = 0.19675 (1 samples)
	maximum = 0.824 (1 samples)
Accepted flit rate average = 0.23244 (1 samples)
	minimum = 0.157 (1 samples)
	maximum = 0.333 (1 samples)
Injected packet size average = 18.0068 (1 samples)
Accepted packet size average = 17.9899 (1 samples)
Hops average = 5.20185 (1 samples)
Total run time 39.2126
