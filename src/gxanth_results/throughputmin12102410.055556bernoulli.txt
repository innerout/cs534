BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.438
	minimum = 23
	maximum = 920
Network latency average = 297.739
	minimum = 23
	maximum = 899
Slowest packet = 178
Flit latency average = 267.624
	minimum = 6
	maximum = 951
Slowest flit = 4635
Fragmentation average = 51.1379
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0123125
	minimum = 0.005 (at node 93)
	maximum = 0.019 (at node 27)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.229901
	minimum = 0.09 (at node 93)
	maximum = 0.342 (at node 27)
Injected packet length average = 17.8419
Accepted packet length average = 18.6722
Total in-flight flits = 129127 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 606.184
	minimum = 23
	maximum = 1835
Network latency average = 561.41
	minimum = 23
	maximum = 1811
Slowest packet = 672
Flit latency average = 534.582
	minimum = 6
	maximum = 1858
Slowest flit = 18088
Fragmentation average = 53.9553
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0503724
	minimum = 0.0415 (at node 159)
	maximum = 0.056 (at node 77)
Accepted packet rate average = 0.0123568
	minimum = 0.007 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.902578
	minimum = 0.7425 (at node 159)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.226945
	minimum = 0.1325 (at node 116)
	maximum = 0.356 (at node 22)
Injected packet length average = 17.9181
Accepted packet length average = 18.3661
Total in-flight flits = 261063 (0 measured)
latency change    = 0.461487
throughput change = 0.013024
Class 0:
Packet latency average = 1767.76
	minimum = 32
	maximum = 2886
Network latency average = 1681.48
	minimum = 23
	maximum = 2841
Slowest packet = 815
Flit latency average = 1663.53
	minimum = 6
	maximum = 2824
Slowest flit = 14687
Fragmentation average = 66.4277
	minimum = 0
	maximum = 172
Injected packet rate average = 0.0439688
	minimum = 0.011 (at node 44)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0104115
	minimum = 0.004 (at node 21)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.791667
	minimum = 0.205 (at node 44)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.18587
	minimum = 0.062 (at node 91)
	maximum = 0.325 (at node 114)
Injected packet length average = 18.0052
Accepted packet length average = 17.8524
Total in-flight flits = 377332 (0 measured)
latency change    = 0.657089
throughput change = 0.220991
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 582.363
	minimum = 29
	maximum = 2129
Network latency average = 40.9441
	minimum = 23
	maximum = 162
Slowest packet = 27927
Flit latency average = 2409.55
	minimum = 6
	maximum = 3815
Slowest flit = 25269
Fragmentation average = 9.43575
	minimum = 0
	maximum = 41
Injected packet rate average = 0.044375
	minimum = 0.012 (at node 56)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0102969
	minimum = 0.003 (at node 31)
	maximum = 0.019 (at node 119)
Injected flit rate average = 0.798781
	minimum = 0.216 (at node 72)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.186182
	minimum = 0.049 (at node 49)
	maximum = 0.346 (at node 187)
Injected packet length average = 18.0007
Accepted packet length average = 18.0814
Total in-flight flits = 494909 (150040 measured)
latency change    = 2.03549
throughput change = 0.00167846
Class 0:
Packet latency average = 651.979
	minimum = 29
	maximum = 2629
Network latency average = 45.7307
	minimum = 23
	maximum = 1761
Slowest packet = 27927
Flit latency average = 2762.53
	minimum = 6
	maximum = 4770
Slowest flit = 34403
Fragmentation average = 9.77067
	minimum = 0
	maximum = 52
Injected packet rate average = 0.0443802
	minimum = 0.0135 (at node 0)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0103516
	minimum = 0.005 (at node 7)
	maximum = 0.017 (at node 187)
Injected flit rate average = 0.798664
	minimum = 0.2365 (at node 120)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.185924
	minimum = 0.09 (at node 105)
	maximum = 0.3025 (at node 187)
Injected packet length average = 17.996
Accepted packet length average = 17.961
Total in-flight flits = 612675 (299958 measured)
latency change    = 0.106776
throughput change = 0.00138665
Class 0:
Packet latency average = 893.217
	minimum = 29
	maximum = 3474
Network latency average = 116.6
	minimum = 23
	maximum = 2876
Slowest packet = 27927
Flit latency average = 3149.13
	minimum = 6
	maximum = 5712
Slowest flit = 44897
Fragmentation average = 9.88869
	minimum = 0
	maximum = 52
Injected packet rate average = 0.0434549
	minimum = 0.0136667 (at node 0)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0103038
	minimum = 0.006 (at node 132)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.782733
	minimum = 0.245667 (at node 120)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.185918
	minimum = 0.111333 (at node 132)
	maximum = 0.273 (at node 151)
Injected packet length average = 18.0125
Accepted packet length average = 18.0436
Total in-flight flits = 720783 (440470 measured)
latency change    = 0.270078
throughput change = 3.26831e-05
Draining remaining packets ...
Class 0:
Remaining flits: 91926 91927 91928 91929 91930 91931 91932 91933 91934 91935 [...] (688774 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (438541 flits)
Class 0:
Remaining flits: 111735 111736 111737 111738 111739 111740 111741 111742 111743 117000 [...] (656407 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (435669 flits)
Class 0:
Remaining flits: 117000 117001 117002 117003 117004 117005 117006 117007 117008 117009 [...] (624473 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (432095 flits)
Class 0:
Remaining flits: 173353 173354 173355 173356 173357 173664 173665 173666 173667 173668 [...] (592368 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (427944 flits)
Class 0:
Remaining flits: 174528 174529 174530 174531 174532 174533 174534 174535 174536 174537 [...] (559321 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (421421 flits)
Class 0:
Remaining flits: 200178 200179 200180 200181 200182 200183 200184 200185 200186 200187 [...] (526985 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (414221 flits)
Class 0:
Remaining flits: 207753 207754 207755 225186 225187 225188 225189 225190 225191 225192 [...] (494213 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (404493 flits)
Class 0:
Remaining flits: 242514 242515 242516 242517 242518 242519 242520 242521 242522 242523 [...] (461562 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (392538 flits)
Class 0:
Remaining flits: 264150 264151 264152 264153 264154 264155 264156 264157 264158 264159 [...] (429095 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (378472 flits)
Class 0:
Remaining flits: 293796 293797 293798 293799 293800 293801 293802 293803 293804 293805 [...] (398462 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (363362 flits)
Class 0:
Remaining flits: 293796 293797 293798 293799 293800 293801 293802 293803 293804 293805 [...] (368924 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (345310 flits)
Class 0:
Remaining flits: 293796 293797 293798 293799 293800 293801 293802 293803 293804 293805 [...] (339987 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (325214 flits)
Class 0:
Remaining flits: 321912 321913 321914 321915 321916 321917 321918 321919 321920 321921 [...] (310671 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (301823 flits)
Class 0:
Remaining flits: 321912 321913 321914 321915 321916 321917 321918 321919 321920 321921 [...] (281043 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (275926 flits)
Class 0:
Remaining flits: 347111 373626 373627 373628 373629 373630 373631 373632 373633 373634 [...] (252047 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (249397 flits)
Class 0:
Remaining flits: 415950 415951 415952 415953 415954 415955 415956 415957 415958 415959 [...] (222631 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (221394 flits)
Class 0:
Remaining flits: 432824 432825 432826 432827 440838 440839 440840 440841 440842 440843 [...] (193834 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (193163 flits)
Class 0:
Remaining flits: 454122 454123 454124 454125 454126 454127 454128 454129 454130 454131 [...] (164506 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (164205 flits)
Class 0:
Remaining flits: 479085 479086 479087 484524 484525 484526 484527 484528 484529 484530 [...] (135132 flits)
Measured flits: 500166 500167 500168 500169 500170 500171 500172 500173 500174 500175 [...] (135071 flits)
Class 0:
Remaining flits: 485463 485464 485465 485466 485467 485468 485469 485470 485471 485472 [...] (105565 flits)
Measured flits: 505332 505333 505334 505335 505336 505337 505338 505339 505340 505341 [...] (105550 flits)
Class 0:
Remaining flits: 522522 522523 522524 522525 522526 522527 522528 522529 522530 522531 [...] (75806 flits)
Measured flits: 522522 522523 522524 522525 522526 522527 522528 522529 522530 522531 [...] (75806 flits)
Class 0:
Remaining flits: 552024 552025 552026 552027 552028 552029 552030 552031 552032 552033 [...] (46431 flits)
Measured flits: 552024 552025 552026 552027 552028 552029 552030 552031 552032 552033 [...] (46431 flits)
Class 0:
Remaining flits: 585108 585109 585110 585111 585112 585113 585114 585115 585116 585117 [...] (18798 flits)
Measured flits: 585108 585109 585110 585111 585112 585113 585114 585115 585116 585117 [...] (18798 flits)
Class 0:
Remaining flits: 689724 689725 689726 689727 689728 689729 689730 689731 689732 689733 [...] (4045 flits)
Measured flits: 689724 689725 689726 689727 689728 689729 689730 689731 689732 689733 [...] (4045 flits)
Class 0:
Remaining flits: 942115 942116 942117 942118 942119 (5 flits)
Measured flits: 942115 942116 942117 942118 942119 (5 flits)
Time taken is 31007 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16991.3 (1 samples)
	minimum = 29 (1 samples)
	maximum = 26530 (1 samples)
Network latency average = 16613.8 (1 samples)
	minimum = 23 (1 samples)
	maximum = 26055 (1 samples)
Flit latency average = 12603.4 (1 samples)
	minimum = 6 (1 samples)
	maximum = 26038 (1 samples)
Fragmentation average = 66.9835 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.0434549 (1 samples)
	minimum = 0.0136667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0103038 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.782733 (1 samples)
	minimum = 0.245667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.185918 (1 samples)
	minimum = 0.111333 (1 samples)
	maximum = 0.273 (1 samples)
Injected packet size average = 18.0125 (1 samples)
Accepted packet size average = 18.0436 (1 samples)
Hops average = 5.22589 (1 samples)
Total run time 33.9691
