BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 359.861
	minimum = 22
	maximum = 985
Network latency average = 238.082
	minimum = 22
	maximum = 780
Slowest packet = 46
Flit latency average = 203.689
	minimum = 5
	maximum = 837
Slowest flit = 12687
Fragmentation average = 51.2024
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0250937
	minimum = 0 (at node 66)
	maximum = 0.052 (at node 190)
Accepted packet rate average = 0.0137917
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 48)
Injected flit rate average = 0.446703
	minimum = 0 (at node 66)
	maximum = 0.934 (at node 190)
Accepted flit rate average= 0.258797
	minimum = 0.095 (at node 174)
	maximum = 0.404 (at node 103)
Injected packet length average = 17.8014
Accepted packet length average = 18.7647
Total in-flight flits = 37431 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 631.664
	minimum = 22
	maximum = 1936
Network latency average = 431.157
	minimum = 22
	maximum = 1603
Slowest packet = 46
Flit latency average = 390.95
	minimum = 5
	maximum = 1607
Slowest flit = 36183
Fragmentation average = 55.6056
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0213047
	minimum = 0.002 (at node 92)
	maximum = 0.039 (at node 95)
Accepted packet rate average = 0.0144219
	minimum = 0.008 (at node 116)
	maximum = 0.0215 (at node 22)
Injected flit rate average = 0.380349
	minimum = 0.036 (at node 92)
	maximum = 0.702 (at node 95)
Accepted flit rate average= 0.264544
	minimum = 0.144 (at node 116)
	maximum = 0.387 (at node 22)
Injected packet length average = 17.8528
Accepted packet length average = 18.3433
Total in-flight flits = 46519 (0 measured)
latency change    = 0.430296
throughput change = 0.0217256
Class 0:
Packet latency average = 1412.04
	minimum = 25
	maximum = 2825
Network latency average = 856.727
	minimum = 22
	maximum = 2274
Slowest packet = 5352
Flit latency average = 809.403
	minimum = 5
	maximum = 2257
Slowest flit = 43073
Fragmentation average = 59.0088
	minimum = 0
	maximum = 263
Injected packet rate average = 0.0151927
	minimum = 0 (at node 3)
	maximum = 0.039 (at node 78)
Accepted packet rate average = 0.014724
	minimum = 0.006 (at node 4)
	maximum = 0.025 (at node 181)
Injected flit rate average = 0.275417
	minimum = 0 (at node 3)
	maximum = 0.711 (at node 78)
Accepted flit rate average= 0.266505
	minimum = 0.102 (at node 20)
	maximum = 0.443 (at node 181)
Injected packet length average = 18.1282
Accepted packet length average = 18.1001
Total in-flight flits = 48180 (0 measured)
latency change    = 0.552658
throughput change = 0.00735797
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1863.45
	minimum = 33
	maximum = 3661
Network latency average = 392.302
	minimum = 22
	maximum = 983
Slowest packet = 11225
Flit latency average = 871.334
	minimum = 5
	maximum = 2922
Slowest flit = 86490
Fragmentation average = 37.6031
	minimum = 0
	maximum = 268
Injected packet rate average = 0.0158646
	minimum = 0 (at node 3)
	maximum = 0.029 (at node 74)
Accepted packet rate average = 0.0149219
	minimum = 0.005 (at node 86)
	maximum = 0.024 (at node 13)
Injected flit rate average = 0.284276
	minimum = 0 (at node 3)
	maximum = 0.524 (at node 74)
Accepted flit rate average= 0.269307
	minimum = 0.092 (at node 86)
	maximum = 0.418 (at node 90)
Injected packet length average = 17.9189
Accepted packet length average = 18.0478
Total in-flight flits = 51031 (41891 measured)
latency change    = 0.242244
throughput change = 0.0104048
Class 0:
Packet latency average = 2379.68
	minimum = 32
	maximum = 4623
Network latency average = 749.28
	minimum = 22
	maximum = 1855
Slowest packet = 11225
Flit latency average = 903.565
	minimum = 5
	maximum = 3282
Slowest flit = 129311
Fragmentation average = 57.1171
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0155859
	minimum = 0.0015 (at node 67)
	maximum = 0.0245 (at node 65)
Accepted packet rate average = 0.0149818
	minimum = 0.0095 (at node 1)
	maximum = 0.022 (at node 165)
Injected flit rate average = 0.279719
	minimum = 0.027 (at node 67)
	maximum = 0.441 (at node 107)
Accepted flit rate average= 0.26994
	minimum = 0.171 (at node 1)
	maximum = 0.3915 (at node 165)
Injected packet length average = 17.9469
Accepted packet length average = 18.0179
Total in-flight flits = 51893 (51351 measured)
latency change    = 0.216934
throughput change = 0.00234427
Class 0:
Packet latency average = 2737.07
	minimum = 27
	maximum = 5585
Network latency average = 874.241
	minimum = 22
	maximum = 2794
Slowest packet = 11225
Flit latency average = 922.389
	minimum = 5
	maximum = 3434
Slowest flit = 133469
Fragmentation average = 65.4344
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0152743
	minimum = 0.00266667 (at node 67)
	maximum = 0.026 (at node 107)
Accepted packet rate average = 0.014849
	minimum = 0.00966667 (at node 36)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.274401
	minimum = 0.048 (at node 67)
	maximum = 0.466333 (at node 107)
Accepted flit rate average= 0.268042
	minimum = 0.188 (at node 36)
	maximum = 0.366667 (at node 129)
Injected packet length average = 17.9649
Accepted packet length average = 18.0512
Total in-flight flits = 51864 (51828 measured)
latency change    = 0.130572
throughput change = 0.00708262
Draining remaining packets ...
Class 0:
Remaining flits: 244944 244945 244946 244947 244948 244949 244950 244951 244952 244953 [...] (6182 flits)
Measured flits: 244944 244945 244946 244947 244948 244949 244950 244951 244952 244953 [...] (6182 flits)
Time taken is 7731 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3111.44 (1 samples)
	minimum = 27 (1 samples)
	maximum = 6720 (1 samples)
Network latency average = 998.091 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3818 (1 samples)
Flit latency average = 984.372 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3891 (1 samples)
Fragmentation average = 69.2639 (1 samples)
	minimum = 0 (1 samples)
	maximum = 340 (1 samples)
Injected packet rate average = 0.0152743 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.026 (1 samples)
Accepted packet rate average = 0.014849 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.274401 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.466333 (1 samples)
Accepted flit rate average = 0.268042 (1 samples)
	minimum = 0.188 (1 samples)
	maximum = 0.366667 (1 samples)
Injected packet size average = 17.9649 (1 samples)
Accepted packet size average = 18.0512 (1 samples)
Hops average = 5.10829 (1 samples)
Total run time 8.94094
