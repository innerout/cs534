BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 379.367
	minimum = 22
	maximum = 928
Network latency average = 342.239
	minimum = 22
	maximum = 838
Slowest packet = 220
Flit latency average = 295.298
	minimum = 5
	maximum = 841
Slowest flit = 22920
Fragmentation average = 95.3867
	minimum = 0
	maximum = 346
Injected packet rate average = 0.0291406
	minimum = 0.016 (at node 108)
	maximum = 0.041 (at node 174)
Accepted packet rate average = 0.0139688
	minimum = 0.003 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.51787
	minimum = 0.286 (at node 108)
	maximum = 0.727 (at node 174)
Accepted flit rate average= 0.26801
	minimum = 0.084 (at node 174)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.7714
Accepted packet length average = 19.1864
Total in-flight flits = 49810 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 745.615
	minimum = 22
	maximum = 1737
Network latency average = 601.349
	minimum = 22
	maximum = 1737
Slowest packet = 2222
Flit latency average = 542.611
	minimum = 5
	maximum = 1772
Slowest flit = 32827
Fragmentation average = 91.7867
	minimum = 0
	maximum = 361
Injected packet rate average = 0.0220833
	minimum = 0.0125 (at node 92)
	maximum = 0.0315 (at node 191)
Accepted packet rate average = 0.0143828
	minimum = 0.008 (at node 153)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.394674
	minimum = 0.2235 (at node 92)
	maximum = 0.56 (at node 191)
Accepted flit rate average= 0.26488
	minimum = 0.144 (at node 153)
	maximum = 0.3985 (at node 152)
Injected packet length average = 17.8721
Accepted packet length average = 18.4164
Total in-flight flits = 51736 (0 measured)
latency change    = 0.491203
throughput change = 0.0118174
Class 0:
Packet latency average = 1815.74
	minimum = 918
	maximum = 2718
Network latency average = 989.029
	minimum = 22
	maximum = 2631
Slowest packet = 3074
Flit latency average = 923.934
	minimum = 5
	maximum = 2599
Slowest flit = 55349
Fragmentation average = 71.8064
	minimum = 0
	maximum = 327
Injected packet rate average = 0.0151771
	minimum = 0.006 (at node 16)
	maximum = 0.03 (at node 2)
Accepted packet rate average = 0.0150365
	minimum = 0.006 (at node 113)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.272844
	minimum = 0.104 (at node 122)
	maximum = 0.541 (at node 2)
Accepted flit rate average= 0.27062
	minimum = 0.11 (at node 36)
	maximum = 0.465 (at node 120)
Injected packet length average = 17.9774
Accepted packet length average = 17.9976
Total in-flight flits = 52427 (0 measured)
latency change    = 0.589361
throughput change = 0.021209
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2573.89
	minimum = 1520
	maximum = 3400
Network latency average = 363.391
	minimum = 22
	maximum = 970
Slowest packet = 11506
Flit latency average = 961.672
	minimum = 5
	maximum = 2971
Slowest flit = 59236
Fragmentation average = 33.1907
	minimum = 0
	maximum = 200
Injected packet rate average = 0.0146979
	minimum = 0.001 (at node 109)
	maximum = 0.03 (at node 122)
Accepted packet rate average = 0.0149688
	minimum = 0.005 (at node 1)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.265521
	minimum = 0.012 (at node 109)
	maximum = 0.533 (at node 122)
Accepted flit rate average= 0.267937
	minimum = 0.095 (at node 1)
	maximum = 0.432 (at node 99)
Injected packet length average = 18.0652
Accepted packet length average = 17.8998
Total in-flight flits = 51797 (40835 measured)
latency change    = 0.294553
throughput change = 0.0100109
Class 0:
Packet latency average = 3147.21
	minimum = 1520
	maximum = 4203
Network latency average = 759.719
	minimum = 22
	maximum = 1952
Slowest packet = 11506
Flit latency average = 968.207
	minimum = 5
	maximum = 3732
Slowest flit = 69029
Fragmentation average = 58.2796
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0148906
	minimum = 0.006 (at node 123)
	maximum = 0.0235 (at node 42)
Accepted packet rate average = 0.0147734
	minimum = 0.01 (at node 2)
	maximum = 0.021 (at node 66)
Injected flit rate average = 0.267799
	minimum = 0.1105 (at node 123)
	maximum = 0.4275 (at node 42)
Accepted flit rate average= 0.266172
	minimum = 0.1715 (at node 5)
	maximum = 0.3675 (at node 178)
Injected packet length average = 17.9844
Accepted packet length average = 18.0169
Total in-flight flits = 52889 (52260 measured)
latency change    = 0.182169
throughput change = 0.0066334
Class 0:
Packet latency average = 3593.76
	minimum = 1520
	maximum = 5059
Network latency average = 892.354
	minimum = 22
	maximum = 2871
Slowest packet = 11506
Flit latency average = 968.669
	minimum = 5
	maximum = 3732
Slowest flit = 69029
Fragmentation average = 66.81
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0148733
	minimum = 0.009 (at node 4)
	maximum = 0.0236667 (at node 122)
Accepted packet rate average = 0.0148438
	minimum = 0.011 (at node 36)
	maximum = 0.0193333 (at node 165)
Injected flit rate average = 0.267906
	minimum = 0.164 (at node 40)
	maximum = 0.424333 (at node 122)
Accepted flit rate average= 0.267347
	minimum = 0.196667 (at node 64)
	maximum = 0.343333 (at node 165)
Injected packet length average = 18.0126
Accepted packet length average = 18.0108
Total in-flight flits = 52641 (52633 measured)
latency change    = 0.124257
throughput change = 0.00439633
Class 0:
Packet latency average = 3988.17
	minimum = 1520
	maximum = 5905
Network latency average = 940.533
	minimum = 22
	maximum = 3296
Slowest packet = 11506
Flit latency average = 967.875
	minimum = 5
	maximum = 3732
Slowest flit = 69029
Fragmentation average = 69.651
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0147852
	minimum = 0.00925 (at node 120)
	maximum = 0.02025 (at node 42)
Accepted packet rate average = 0.0148763
	minimum = 0.01025 (at node 2)
	maximum = 0.0195 (at node 118)
Injected flit rate average = 0.266262
	minimum = 0.1705 (at node 120)
	maximum = 0.36525 (at node 42)
Accepted flit rate average= 0.267923
	minimum = 0.19025 (at node 2)
	maximum = 0.3505 (at node 118)
Injected packet length average = 18.0087
Accepted packet length average = 18.0101
Total in-flight flits = 50854 (50854 measured)
latency change    = 0.0988936
throughput change = 0.0021497
Class 0:
Packet latency average = 4369.78
	minimum = 1520
	maximum = 6823
Network latency average = 965.923
	minimum = 22
	maximum = 3991
Slowest packet = 11506
Flit latency average = 967.312
	minimum = 5
	maximum = 3974
Slowest flit = 256823
Fragmentation average = 72.447
	minimum = 0
	maximum = 332
Injected packet rate average = 0.01485
	minimum = 0.0096 (at node 128)
	maximum = 0.0202 (at node 131)
Accepted packet rate average = 0.0148906
	minimum = 0.0114 (at node 42)
	maximum = 0.0186 (at node 95)
Injected flit rate average = 0.267273
	minimum = 0.1728 (at node 128)
	maximum = 0.3644 (at node 131)
Accepted flit rate average= 0.268205
	minimum = 0.2046 (at node 42)
	maximum = 0.3364 (at node 95)
Injected packet length average = 17.9982
Accepted packet length average = 18.0117
Total in-flight flits = 51432 (51432 measured)
latency change    = 0.0873293
throughput change = 0.00105155
Class 0:
Packet latency average = 4739.76
	minimum = 1520
	maximum = 7406
Network latency average = 980.981
	minimum = 22
	maximum = 3991
Slowest packet = 11506
Flit latency average = 967.304
	minimum = 5
	maximum = 4161
Slowest flit = 286291
Fragmentation average = 74.3817
	minimum = 0
	maximum = 337
Injected packet rate average = 0.0148438
	minimum = 0.00983333 (at node 128)
	maximum = 0.0201667 (at node 121)
Accepted packet rate average = 0.0148828
	minimum = 0.0115 (at node 79)
	maximum = 0.0188333 (at node 138)
Injected flit rate average = 0.267276
	minimum = 0.175833 (at node 128)
	maximum = 0.362667 (at node 121)
Accepted flit rate average= 0.268286
	minimum = 0.209667 (at node 42)
	maximum = 0.344 (at node 138)
Injected packet length average = 18.006
Accepted packet length average = 18.0266
Total in-flight flits = 51089 (51089 measured)
latency change    = 0.0780598
throughput change = 0.000302848
Class 0:
Packet latency average = 5117.37
	minimum = 1520
	maximum = 8353
Network latency average = 987.145
	minimum = 22
	maximum = 4422
Slowest packet = 11506
Flit latency average = 963.471
	minimum = 5
	maximum = 5170
Slowest flit = 299163
Fragmentation average = 76.0742
	minimum = 0
	maximum = 343
Injected packet rate average = 0.0149501
	minimum = 0.00985714 (at node 56)
	maximum = 0.0194286 (at node 42)
Accepted packet rate average = 0.0149345
	minimum = 0.0117143 (at node 80)
	maximum = 0.0184286 (at node 138)
Injected flit rate average = 0.269115
	minimum = 0.177714 (at node 56)
	maximum = 0.349 (at node 42)
Accepted flit rate average= 0.268951
	minimum = 0.212143 (at node 80)
	maximum = 0.331714 (at node 138)
Injected packet length average = 18.0008
Accepted packet length average = 18.0087
Total in-flight flits = 52361 (52361 measured)
latency change    = 0.0737895
throughput change = 0.00247047
Draining all recorded packets ...
Class 0:
Remaining flits: 384036 384037 384038 384039 384040 384041 384042 384043 384044 384045 [...] (51065 flits)
Measured flits: 384036 384037 384038 384039 384040 384041 384042 384043 384044 384045 [...] (51065 flits)
Class 0:
Remaining flits: 448668 448669 448670 448671 448672 448673 448674 448675 448676 448677 [...] (50591 flits)
Measured flits: 448668 448669 448670 448671 448672 448673 448674 448675 448676 448677 [...] (50591 flits)
Class 0:
Remaining flits: 467370 467371 467372 467373 467374 467375 467376 467377 467378 467379 [...] (51841 flits)
Measured flits: 467370 467371 467372 467373 467374 467375 467376 467377 467378 467379 [...] (51841 flits)
Class 0:
Remaining flits: 554635 554636 554637 554638 554639 554640 554641 554642 554643 554644 [...] (51758 flits)
Measured flits: 554635 554636 554637 554638 554639 554640 554641 554642 554643 554644 [...] (51758 flits)
Class 0:
Remaining flits: 600228 600229 600230 600231 600232 600233 600234 600235 600236 600237 [...] (50379 flits)
Measured flits: 600228 600229 600230 600231 600232 600233 600234 600235 600236 600237 [...] (50379 flits)
Class 0:
Remaining flits: 649920 649921 649922 649923 649924 649925 676854 676855 676856 676857 [...] (49907 flits)
Measured flits: 649920 649921 649922 649923 649924 649925 676854 676855 676856 676857 [...] (49907 flits)
Class 0:
Remaining flits: 693756 693757 693758 693759 693760 693761 693762 693763 693764 693765 [...] (49439 flits)
Measured flits: 693756 693757 693758 693759 693760 693761 693762 693763 693764 693765 [...] (49439 flits)
Class 0:
Remaining flits: 727614 727615 727616 727617 727618 727619 727620 727621 727622 727623 [...] (49668 flits)
Measured flits: 727614 727615 727616 727617 727618 727619 727620 727621 727622 727623 [...] (49668 flits)
Class 0:
Remaining flits: 727614 727615 727616 727617 727618 727619 727620 727621 727622 727623 [...] (50383 flits)
Measured flits: 727614 727615 727616 727617 727618 727619 727620 727621 727622 727623 [...] (50383 flits)
Class 0:
Remaining flits: 848844 848845 848846 848847 848848 848849 848850 848851 848852 848853 [...] (50675 flits)
Measured flits: 848844 848845 848846 848847 848848 848849 848850 848851 848852 848853 [...] (50675 flits)
Class 0:
Remaining flits: 869346 869347 869348 869349 869350 869351 869352 869353 869354 869355 [...] (49610 flits)
Measured flits: 869346 869347 869348 869349 869350 869351 869352 869353 869354 869355 [...] (49610 flits)
Class 0:
Remaining flits: 955674 955675 955676 955677 955678 955679 955680 955681 955682 955683 [...] (49862 flits)
Measured flits: 955674 955675 955676 955677 955678 955679 955680 955681 955682 955683 [...] (49862 flits)
Class 0:
Remaining flits: 994212 994213 994214 994215 994216 994217 994218 994219 994220 994221 [...] (50147 flits)
Measured flits: 994212 994213 994214 994215 994216 994217 994218 994219 994220 994221 [...] (50147 flits)
Class 0:
Remaining flits: 1075390 1075391 1076238 1076239 1076240 1076241 1076242 1076243 1076244 1076245 [...] (49127 flits)
Measured flits: 1075390 1075391 1076238 1076239 1076240 1076241 1076242 1076243 1076244 1076245 [...] (49127 flits)
Class 0:
Remaining flits: 1104822 1104823 1104824 1104825 1104826 1104827 1104828 1104829 1104830 1104831 [...] (49764 flits)
Measured flits: 1104822 1104823 1104824 1104825 1104826 1104827 1104828 1104829 1104830 1104831 [...] (49764 flits)
Class 0:
Remaining flits: 1142172 1142173 1142174 1142175 1142176 1142177 1142178 1142179 1142180 1142181 [...] (49928 flits)
Measured flits: 1142172 1142173 1142174 1142175 1142176 1142177 1142178 1142179 1142180 1142181 [...] (49928 flits)
Class 0:
Remaining flits: 1190320 1190321 1212567 1212568 1212569 1219140 1219141 1219142 1219143 1219144 [...] (49176 flits)
Measured flits: 1190320 1190321 1212567 1212568 1212569 1219140 1219141 1219142 1219143 1219144 [...] (49176 flits)
Class 0:
Remaining flits: 1219148 1219149 1219150 1219151 1219152 1219153 1219154 1219155 1219156 1219157 [...] (49034 flits)
Measured flits: 1219148 1219149 1219150 1219151 1219152 1219153 1219154 1219155 1219156 1219157 [...] (49034 flits)
Class 0:
Remaining flits: 1307850 1307851 1307852 1307853 1307854 1307855 1307856 1307857 1307858 1307859 [...] (49999 flits)
Measured flits: 1307850 1307851 1307852 1307853 1307854 1307855 1307856 1307857 1307858 1307859 [...] (49999 flits)
Class 0:
Remaining flits: 1338426 1338427 1338428 1338429 1338430 1338431 1338432 1338433 1338434 1338435 [...] (48833 flits)
Measured flits: 1338426 1338427 1338428 1338429 1338430 1338431 1338432 1338433 1338434 1338435 [...] (48599 flits)
Class 0:
Remaining flits: 1338429 1338430 1338431 1338432 1338433 1338434 1338435 1338436 1338437 1338438 [...] (49494 flits)
Measured flits: 1338429 1338430 1338431 1338432 1338433 1338434 1338435 1338436 1338437 1338438 [...] (47920 flits)
Class 0:
Remaining flits: 1390158 1390159 1390160 1390161 1390162 1390163 1390164 1390165 1390166 1390167 [...] (49211 flits)
Measured flits: 1390158 1390159 1390160 1390161 1390162 1390163 1390164 1390165 1390166 1390167 [...] (45041 flits)
Class 0:
Remaining flits: 1453104 1453105 1453106 1453107 1453108 1453109 1453110 1453111 1453112 1453113 [...] (50648 flits)
Measured flits: 1453104 1453105 1453106 1453107 1453108 1453109 1453110 1453111 1453112 1453113 [...] (42958 flits)
Class 0:
Remaining flits: 1585170 1585171 1585172 1585173 1585174 1585175 1585176 1585177 1585178 1585179 [...] (49052 flits)
Measured flits: 1585170 1585171 1585172 1585173 1585174 1585175 1585176 1585177 1585178 1585179 [...] (36428 flits)
Class 0:
Remaining flits: 1639224 1639225 1639226 1639227 1639228 1639229 1639230 1639231 1639232 1639233 [...] (49644 flits)
Measured flits: 1639224 1639225 1639226 1639227 1639228 1639229 1639230 1639231 1639232 1639233 [...] (30731 flits)
Class 0:
Remaining flits: 1658811 1658812 1658813 1658814 1658815 1658816 1658817 1658818 1658819 1658820 [...] (50630 flits)
Measured flits: 1658811 1658812 1658813 1658814 1658815 1658816 1658817 1658818 1658819 1658820 [...] (23717 flits)
Class 0:
Remaining flits: 1738896 1738897 1738898 1738899 1738900 1738901 1738902 1738903 1738904 1738905 [...] (50370 flits)
Measured flits: 1738896 1738897 1738898 1738899 1738900 1738901 1738902 1738903 1738904 1738905 [...] (18061 flits)
Class 0:
Remaining flits: 1794875 1794876 1794877 1794878 1794879 1794880 1794881 1794882 1794883 1794884 [...] (50978 flits)
Measured flits: 1794875 1794876 1794877 1794878 1794879 1794880 1794881 1794882 1794883 1794884 [...] (13090 flits)
Class 0:
Remaining flits: 1830150 1830151 1830152 1830153 1830154 1830155 1830156 1830157 1830158 1830159 [...] (49676 flits)
Measured flits: 1859954 1859955 1859956 1859957 1861362 1861363 1861364 1861365 1861366 1861367 [...] (8607 flits)
Class 0:
Remaining flits: 1863630 1863631 1863632 1863633 1863634 1863635 1863636 1863637 1863638 1863639 [...] (48559 flits)
Measured flits: 1863630 1863631 1863632 1863633 1863634 1863635 1863636 1863637 1863638 1863639 [...] (6261 flits)
Class 0:
Remaining flits: 1922652 1922653 1922654 1922655 1922656 1922657 1922658 1922659 1922660 1922661 [...] (50196 flits)
Measured flits: 1922652 1922653 1922654 1922655 1922656 1922657 1922658 1922659 1922660 1922661 [...] (5640 flits)
Class 0:
Remaining flits: 1975806 1975807 1975808 1975809 1975810 1975811 1975812 1975813 1975814 1975815 [...] (50148 flits)
Measured flits: 1975806 1975807 1975808 1975809 1975810 1975811 1975812 1975813 1975814 1975815 [...] (5242 flits)
Class 0:
Remaining flits: 1996470 1996471 1996472 1996473 1996474 1996475 1996476 1996477 1996478 1996479 [...] (49882 flits)
Measured flits: 2077846 2077847 2087712 2087713 2087714 2087715 2087716 2087717 2087718 2087719 [...] (4201 flits)
Class 0:
Remaining flits: 2080260 2080261 2080262 2080263 2080264 2080265 2080266 2080267 2080268 2080269 [...] (48833 flits)
Measured flits: 2158145 2163063 2163064 2163065 2163066 2163067 2163068 2163069 2163070 2163071 [...] (3318 flits)
Class 0:
Remaining flits: 2080260 2080261 2080262 2080263 2080264 2080265 2080266 2080267 2080268 2080269 [...] (49418 flits)
Measured flits: 2179584 2179585 2179586 2179587 2179588 2179589 2179590 2179591 2179592 2179593 [...] (2431 flits)
Class 0:
Remaining flits: 2087262 2087263 2087264 2087265 2087266 2087267 2087268 2087269 2087270 2087271 [...] (49168 flits)
Measured flits: 2179584 2179585 2179586 2179587 2179588 2179589 2179590 2179591 2179592 2179593 [...] (1912 flits)
Class 0:
Remaining flits: 2087276 2087277 2087278 2087279 2222046 2222047 2222048 2222049 2222050 2222051 [...] (50900 flits)
Measured flits: 2287368 2287369 2287370 2287371 2287372 2287373 2287374 2287375 2287376 2287377 [...] (1293 flits)
Class 0:
Remaining flits: 2222046 2222047 2222048 2222049 2222050 2222051 2222052 2222053 2222054 2222055 [...] (49716 flits)
Measured flits: 2328210 2328211 2328212 2328213 2328214 2328215 2328216 2328217 2328218 2328219 [...] (674 flits)
Class 0:
Remaining flits: 2233368 2233369 2233370 2233371 2233372 2233373 2233374 2233375 2233376 2233377 [...] (49613 flits)
Measured flits: 2406366 2406367 2406368 2406369 2406370 2406371 2406372 2406373 2406374 2406375 [...] (623 flits)
Class 0:
Remaining flits: 2344661 2364994 2364995 2364996 2364997 2364998 2364999 2365000 2365001 2387916 [...] (49325 flits)
Measured flits: 2510046 2510047 2510048 2510049 2510050 2510051 2510052 2510053 2510054 2510055 [...] (250 flits)
Class 0:
Remaining flits: 2438613 2438614 2438615 2438616 2438617 2438618 2438619 2438620 2438621 2447568 [...] (48945 flits)
Measured flits: 2510046 2510047 2510048 2510049 2510050 2510051 2510052 2510053 2510054 2510055 [...] (162 flits)
Class 0:
Remaining flits: 2485616 2485617 2485618 2485619 2487510 2487511 2487512 2487513 2487514 2487515 [...] (49274 flits)
Measured flits: 2587662 2587663 2587664 2587665 2587666 2587667 2587668 2587669 2587670 2587671 [...] (216 flits)
Class 0:
Remaining flits: 2529774 2529775 2529776 2529777 2529778 2529779 2529780 2529781 2529782 2529783 [...] (49112 flits)
Measured flits: 2728332 2728333 2728334 2728335 2728336 2728337 2728338 2728339 2728340 2728341 [...] (123 flits)
Class 0:
Remaining flits: 2534795 2534940 2534941 2534942 2534943 2534944 2534945 2534946 2534947 2534948 [...] (49522 flits)
Measured flits: 2728332 2728333 2728334 2728335 2728336 2728337 2728338 2728339 2728340 2728341 [...] (108 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2692118 2692119 2692120 2692121 2692122 2692123 2692124 2692125 2692126 2692127 [...] (8204 flits)
Measured flits: (0 flits)
Time taken is 56961 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15484.7 (1 samples)
	minimum = 1520 (1 samples)
	maximum = 45034 (1 samples)
Network latency average = 995.142 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6541 (1 samples)
Flit latency average = 940.918 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7353 (1 samples)
Fragmentation average = 79.2092 (1 samples)
	minimum = 0 (1 samples)
	maximum = 432 (1 samples)
Injected packet rate average = 0.0149501 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0194286 (1 samples)
Accepted packet rate average = 0.0149345 (1 samples)
	minimum = 0.0117143 (1 samples)
	maximum = 0.0184286 (1 samples)
Injected flit rate average = 0.269115 (1 samples)
	minimum = 0.177714 (1 samples)
	maximum = 0.349 (1 samples)
Accepted flit rate average = 0.268951 (1 samples)
	minimum = 0.212143 (1 samples)
	maximum = 0.331714 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 18.0087 (1 samples)
Hops average = 5.06308 (1 samples)
Total run time 82.94
