BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.795
	minimum = 22
	maximum = 984
Network latency average = 241.94
	minimum = 22
	maximum = 858
Slowest packet = 90
Flit latency average = 211.992
	minimum = 5
	maximum = 841
Slowest flit = 12239
Fragmentation average = 48.9307
	minimum = 0
	maximum = 390
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135365
	minimum = 0.005 (at node 41)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.256443
	minimum = 0.09 (at node 41)
	maximum = 0.467 (at node 76)
Injected packet length average = 17.8246
Accepted packet length average = 18.9446
Total in-flight flits = 51725 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 622.344
	minimum = 22
	maximum = 1948
Network latency average = 457.682
	minimum = 22
	maximum = 1499
Slowest packet = 90
Flit latency average = 425.245
	minimum = 5
	maximum = 1482
Slowest flit = 43811
Fragmentation average = 65.2258
	minimum = 0
	maximum = 448
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0146589
	minimum = 0.0085 (at node 62)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.561727
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 28)
Accepted flit rate average= 0.272747
	minimum = 0.162 (at node 62)
	maximum = 0.4005 (at node 152)
Injected packet length average = 17.9111
Accepted packet length average = 18.6063
Total in-flight flits = 112039 (0 measured)
latency change    = 0.431513
throughput change = 0.0597794
Class 0:
Packet latency average = 1346.82
	minimum = 25
	maximum = 2795
Network latency average = 1084.68
	minimum = 22
	maximum = 2232
Slowest packet = 4435
Flit latency average = 1052.78
	minimum = 5
	maximum = 2310
Slowest flit = 63894
Fragmentation average = 100.912
	minimum = 0
	maximum = 486
Injected packet rate average = 0.0354427
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0162031
	minimum = 0.009 (at node 49)
	maximum = 0.029 (at node 34)
Injected flit rate average = 0.638953
	minimum = 0 (at node 14)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.293479
	minimum = 0.143 (at node 49)
	maximum = 0.534 (at node 182)
Injected packet length average = 18.0278
Accepted packet length average = 18.1125
Total in-flight flits = 178181 (0 measured)
latency change    = 0.537916
throughput change = 0.0706414
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 389.578
	minimum = 23
	maximum = 2317
Network latency average = 36.9319
	minimum = 22
	maximum = 379
Slowest packet = 18901
Flit latency average = 1486.56
	minimum = 5
	maximum = 3043
Slowest flit = 94352
Fragmentation average = 6.01571
	minimum = 0
	maximum = 34
Injected packet rate average = 0.03525
	minimum = 0 (at node 42)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0164635
	minimum = 0.006 (at node 105)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.633786
	minimum = 0 (at node 42)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.297547
	minimum = 0.108 (at node 105)
	maximum = 0.509 (at node 123)
Injected packet length average = 17.9798
Accepted packet length average = 18.0731
Total in-flight flits = 242876 (111425 measured)
latency change    = 2.45713
throughput change = 0.0136708
Class 0:
Packet latency average = 482.876
	minimum = 23
	maximum = 2379
Network latency average = 133.958
	minimum = 22
	maximum = 1891
Slowest packet = 18901
Flit latency average = 1722.21
	minimum = 5
	maximum = 3735
Slowest flit = 110213
Fragmentation average = 9.03164
	minimum = 0
	maximum = 175
Injected packet rate average = 0.0349375
	minimum = 0.002 (at node 86)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0164505
	minimum = 0.0085 (at node 105)
	maximum = 0.0235 (at node 95)
Injected flit rate average = 0.62856
	minimum = 0.036 (at node 86)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.296784
	minimum = 0.167 (at node 36)
	maximum = 0.445 (at node 138)
Injected packet length average = 17.991
Accepted packet length average = 18.041
Total in-flight flits = 305704 (219631 measured)
latency change    = 0.193214
throughput change = 0.00257096
Class 0:
Packet latency average = 878.22
	minimum = 23
	maximum = 3620
Network latency average = 543.575
	minimum = 22
	maximum = 2977
Slowest packet = 18901
Flit latency average = 1962.2
	minimum = 5
	maximum = 4724
Slowest flit = 129436
Fragmentation average = 27.9046
	minimum = 0
	maximum = 385
Injected packet rate average = 0.0351771
	minimum = 0.00766667 (at node 60)
	maximum = 0.0556667 (at node 0)
Accepted packet rate average = 0.0164531
	minimum = 0.012 (at node 57)
	maximum = 0.023 (at node 95)
Injected flit rate average = 0.632896
	minimum = 0.138 (at node 60)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.297451
	minimum = 0.219667 (at node 36)
	maximum = 0.423 (at node 95)
Injected packet length average = 17.9917
Accepted packet length average = 18.0787
Total in-flight flits = 371565 (324155 measured)
latency change    = 0.450165
throughput change = 0.00224418
Draining remaining packets ...
Class 0:
Remaining flits: 202521 202522 202523 202524 202525 202526 202527 202528 202529 202530 [...] (323873 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (302729 flits)
Class 0:
Remaining flits: 231211 231212 231213 231214 231215 231216 231217 231218 231219 231220 [...] (276924 flits)
Measured flits: 339408 339409 339410 339411 339412 339413 339414 339415 339416 339417 [...] (269140 flits)
Class 0:
Remaining flits: 253008 253009 253010 253011 253012 253013 253014 253015 253016 253017 [...] (229853 flits)
Measured flits: 339642 339643 339644 339645 339646 339647 339648 339649 339650 339651 [...] (227610 flits)
Class 0:
Remaining flits: 295618 295619 295620 295621 295622 295623 295624 295625 295626 295627 [...] (182812 flits)
Measured flits: 339930 339931 339932 339933 339934 339935 339936 339937 339938 339939 [...] (182491 flits)
Class 0:
Remaining flits: 347058 347059 347060 347061 347062 347063 347064 347065 347066 347067 [...] (136086 flits)
Measured flits: 347058 347059 347060 347061 347062 347063 347064 347065 347066 347067 [...] (136086 flits)
Class 0:
Remaining flits: 387666 387667 387668 387669 387670 387671 387672 387673 387674 387675 [...] (90264 flits)
Measured flits: 387666 387667 387668 387669 387670 387671 387672 387673 387674 387675 [...] (90264 flits)
Class 0:
Remaining flits: 420896 420897 420898 420899 420900 420901 420902 420903 420904 420905 [...] (49492 flits)
Measured flits: 420896 420897 420898 420899 420900 420901 420902 420903 420904 420905 [...] (49492 flits)
Class 0:
Remaining flits: 482478 482479 482480 482481 482482 482483 482484 482485 482486 482487 [...] (20279 flits)
Measured flits: 482478 482479 482480 482481 482482 482483 482484 482485 482486 482487 [...] (20279 flits)
Class 0:
Remaining flits: 541406 541407 541408 541409 541410 541411 541412 541413 541414 541415 [...] (7251 flits)
Measured flits: 541406 541407 541408 541409 541410 541411 541412 541413 541414 541415 [...] (7251 flits)
Class 0:
Remaining flits: 613517 613518 613519 613520 613521 613522 613523 613524 613525 613526 [...] (1294 flits)
Measured flits: 613517 613518 613519 613520 613521 613522 613523 613524 613525 613526 [...] (1294 flits)
Time taken is 16592 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5767.24 (1 samples)
	minimum = 23 (1 samples)
	maximum = 12005 (1 samples)
Network latency average = 5405.53 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11026 (1 samples)
Flit latency average = 4538.83 (1 samples)
	minimum = 5 (1 samples)
	maximum = 11009 (1 samples)
Fragmentation average = 137.128 (1 samples)
	minimum = 0 (1 samples)
	maximum = 482 (1 samples)
Injected packet rate average = 0.0351771 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0164531 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.023 (1 samples)
Injected flit rate average = 0.632896 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.297451 (1 samples)
	minimum = 0.219667 (1 samples)
	maximum = 0.423 (1 samples)
Injected packet size average = 17.9917 (1 samples)
Accepted packet size average = 18.0787 (1 samples)
Hops average = 5.07507 (1 samples)
Total run time 13.8059
