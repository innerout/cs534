BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.246
	minimum = 27
	maximum = 968
Network latency average = 187.514
	minimum = 23
	maximum = 923
Slowest packet = 122
Flit latency average = 122.198
	minimum = 6
	maximum = 906
Slowest flit = 4085
Fragmentation average = 106.927
	minimum = 0
	maximum = 768
Injected packet rate average = 0.0117604
	minimum = 0 (at node 60)
	maximum = 0.034 (at node 46)
Accepted packet rate average = 0.0081875
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 15)
Injected flit rate average = 0.209729
	minimum = 0 (at node 60)
	maximum = 0.601 (at node 46)
Accepted flit rate average= 0.162167
	minimum = 0.039 (at node 174)
	maximum = 0.293 (at node 152)
Injected packet length average = 17.8335
Accepted packet length average = 19.8066
Total in-flight flits = 9526 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 320.538
	minimum = 27
	maximum = 1674
Network latency average = 261.864
	minimum = 23
	maximum = 1625
Slowest packet = 691
Flit latency average = 181.506
	minimum = 6
	maximum = 1638
Slowest flit = 14388
Fragmentation average = 134.64
	minimum = 0
	maximum = 1590
Injected packet rate average = 0.0108151
	minimum = 0 (at node 177)
	maximum = 0.027 (at node 46)
Accepted packet rate average = 0.00891406
	minimum = 0.005 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.193539
	minimum = 0 (at node 177)
	maximum = 0.4835 (at node 46)
Accepted flit rate average= 0.16868
	minimum = 0.099 (at node 30)
	maximum = 0.306 (at node 22)
Injected packet length average = 17.8953
Accepted packet length average = 18.9229
Total in-flight flits = 10017 (0 measured)
latency change    = 0.284809
throughput change = 0.0386118
Class 0:
Packet latency average = 503.594
	minimum = 23
	maximum = 2578
Network latency average = 390.009
	minimum = 23
	maximum = 2504
Slowest packet = 1011
Flit latency average = 292.028
	minimum = 6
	maximum = 2487
Slowest flit = 18215
Fragmentation average = 167.664
	minimum = 0
	maximum = 2215
Injected packet rate average = 0.0112448
	minimum = 0 (at node 19)
	maximum = 0.033 (at node 41)
Accepted packet rate average = 0.00995833
	minimum = 0.004 (at node 22)
	maximum = 0.02 (at node 63)
Injected flit rate average = 0.201391
	minimum = 0 (at node 19)
	maximum = 0.607 (at node 41)
Accepted flit rate average= 0.181016
	minimum = 0.074 (at node 121)
	maximum = 0.352 (at node 63)
Injected packet length average = 17.9097
Accepted packet length average = 18.1773
Total in-flight flits = 14106 (0 measured)
latency change    = 0.363499
throughput change = 0.0681485
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 478.617
	minimum = 25
	maximum = 2315
Network latency average = 296.209
	minimum = 23
	maximum = 972
Slowest packet = 6321
Flit latency average = 359.576
	minimum = 6
	maximum = 2889
Slowest flit = 44039
Fragmentation average = 141.532
	minimum = 0
	maximum = 558
Injected packet rate average = 0.0107604
	minimum = 0 (at node 9)
	maximum = 0.029 (at node 18)
Accepted packet rate average = 0.0103021
	minimum = 0.002 (at node 190)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.193297
	minimum = 0.005 (at node 60)
	maximum = 0.516 (at node 18)
Accepted flit rate average= 0.187177
	minimum = 0.049 (at node 163)
	maximum = 0.378 (at node 16)
Injected packet length average = 17.9637
Accepted packet length average = 18.1689
Total in-flight flits = 15338 (13562 measured)
latency change    = 0.0521864
throughput change = 0.0329178
Class 0:
Packet latency average = 617.78
	minimum = 25
	maximum = 3315
Network latency average = 396.526
	minimum = 23
	maximum = 1872
Slowest packet = 6321
Flit latency average = 386.075
	minimum = 6
	maximum = 3896
Slowest flit = 32867
Fragmentation average = 164.645
	minimum = 0
	maximum = 1727
Injected packet rate average = 0.0107891
	minimum = 0.0005 (at node 2)
	maximum = 0.024 (at node 85)
Accepted packet rate average = 0.0102865
	minimum = 0.004 (at node 190)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.194362
	minimum = 0.009 (at node 2)
	maximum = 0.432 (at node 85)
Accepted flit rate average= 0.185943
	minimum = 0.078 (at node 1)
	maximum = 0.3475 (at node 16)
Injected packet length average = 18.0147
Accepted packet length average = 18.0765
Total in-flight flits = 17278 (16972 measured)
latency change    = 0.225264
throughput change = 0.00663847
Class 0:
Packet latency average = 694.593
	minimum = 25
	maximum = 3814
Network latency average = 451.278
	minimum = 23
	maximum = 2782
Slowest packet = 6321
Flit latency average = 406.261
	minimum = 6
	maximum = 4462
Slowest flit = 58813
Fragmentation average = 172.253
	minimum = 0
	maximum = 1727
Injected packet rate average = 0.0107205
	minimum = 0.003 (at node 87)
	maximum = 0.022 (at node 166)
Accepted packet rate average = 0.0103038
	minimum = 0.005 (at node 36)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.19299
	minimum = 0.051 (at node 87)
	maximum = 0.393667 (at node 166)
Accepted flit rate average= 0.186312
	minimum = 0.0956667 (at node 36)
	maximum = 0.285333 (at node 16)
Injected packet length average = 18.0019
Accepted packet length average = 18.0819
Total in-flight flits = 17994 (17934 measured)
latency change    = 0.110586
throughput change = 0.00198479
Class 0:
Packet latency average = 766.358
	minimum = 25
	maximum = 4348
Network latency average = 493.928
	minimum = 23
	maximum = 3735
Slowest packet = 6321
Flit latency average = 423.902
	minimum = 6
	maximum = 4554
Slowest flit = 58823
Fragmentation average = 178.705
	minimum = 0
	maximum = 2534
Injected packet rate average = 0.0107031
	minimum = 0.004 (at node 4)
	maximum = 0.0185 (at node 106)
Accepted packet rate average = 0.0103529
	minimum = 0.0065 (at node 36)
	maximum = 0.01525 (at node 16)
Injected flit rate average = 0.192633
	minimum = 0.072 (at node 4)
	maximum = 0.33325 (at node 106)
Accepted flit rate average= 0.187016
	minimum = 0.1155 (at node 36)
	maximum = 0.26925 (at node 16)
Injected packet length average = 17.9978
Accepted packet length average = 18.0641
Total in-flight flits = 18528 (18512 measured)
latency change    = 0.0936447
throughput change = 0.00375971
Class 0:
Packet latency average = 818.247
	minimum = 23
	maximum = 4348
Network latency average = 516.539
	minimum = 23
	maximum = 3978
Slowest packet = 6321
Flit latency average = 433.117
	minimum = 6
	maximum = 4705
Slowest flit = 113055
Fragmentation average = 182.986
	minimum = 0
	maximum = 2835
Injected packet rate average = 0.0105771
	minimum = 0.0038 (at node 14)
	maximum = 0.0176 (at node 6)
Accepted packet rate average = 0.0103615
	minimum = 0.0062 (at node 36)
	maximum = 0.014 (at node 129)
Injected flit rate average = 0.190364
	minimum = 0.0684 (at node 14)
	maximum = 0.319 (at node 6)
Accepted flit rate average= 0.186717
	minimum = 0.1142 (at node 36)
	maximum = 0.2512 (at node 128)
Injected packet length average = 17.9977
Accepted packet length average = 18.0203
Total in-flight flits = 17684 (17682 measured)
latency change    = 0.0634149
throughput change = 0.00160113
Class 0:
Packet latency average = 856.585
	minimum = 23
	maximum = 5290
Network latency average = 527.804
	minimum = 23
	maximum = 4113
Slowest packet = 6321
Flit latency average = 438.18
	minimum = 6
	maximum = 5484
Slowest flit = 113057
Fragmentation average = 183.17
	minimum = 0
	maximum = 2835
Injected packet rate average = 0.010612
	minimum = 0.00416667 (at node 123)
	maximum = 0.0181667 (at node 190)
Accepted packet rate average = 0.0103585
	minimum = 0.00716667 (at node 36)
	maximum = 0.0133333 (at node 128)
Injected flit rate average = 0.190991
	minimum = 0.0761667 (at node 123)
	maximum = 0.327833 (at node 190)
Accepted flit rate average= 0.186885
	minimum = 0.1355 (at node 36)
	maximum = 0.239167 (at node 128)
Injected packet length average = 17.9977
Accepted packet length average = 18.0416
Total in-flight flits = 19045 (19045 measured)
latency change    = 0.0447562
throughput change = 0.000898319
Class 0:
Packet latency average = 884.51
	minimum = 23
	maximum = 5290
Network latency average = 541.014
	minimum = 23
	maximum = 4292
Slowest packet = 6321
Flit latency average = 445.893
	minimum = 6
	maximum = 5484
Slowest flit = 113057
Fragmentation average = 184.617
	minimum = 0
	maximum = 2835
Injected packet rate average = 0.0106176
	minimum = 0.00557143 (at node 30)
	maximum = 0.017 (at node 151)
Accepted packet rate average = 0.0103557
	minimum = 0.00771429 (at node 36)
	maximum = 0.0132857 (at node 129)
Injected flit rate average = 0.191033
	minimum = 0.100286 (at node 30)
	maximum = 0.304143 (at node 151)
Accepted flit rate average= 0.186788
	minimum = 0.141429 (at node 86)
	maximum = 0.236714 (at node 128)
Injected packet length average = 17.9922
Accepted packet length average = 18.0373
Total in-flight flits = 20103 (20103 measured)
latency change    = 0.0315708
throughput change = 0.000517176
Draining all recorded packets ...
Class 0:
Remaining flits: 172130 172131 172132 172133 228794 228795 228796 228797 247059 247060 [...] (20024 flits)
Measured flits: 172130 172131 172132 172133 228794 228795 228796 228797 247059 247060 [...] (9111 flits)
Class 0:
Remaining flits: 266866 266867 270770 270771 270772 270773 275130 275131 275132 275133 [...] (20589 flits)
Measured flits: 266866 266867 270770 270771 270772 270773 275130 275131 275132 275133 [...] (4804 flits)
Class 0:
Remaining flits: 266866 266867 275144 275145 275146 275147 295866 295867 295868 295869 [...] (21084 flits)
Measured flits: 266866 266867 275144 275145 275146 275147 295866 295867 295868 295869 [...] (2171 flits)
Class 0:
Remaining flits: 266867 325223 333414 333415 333416 333417 333418 333419 333420 333421 [...] (20047 flits)
Measured flits: 266867 325223 333414 333415 333416 333417 333418 333419 333420 333421 [...] (860 flits)
Class 0:
Remaining flits: 335736 335737 335738 335739 335740 335741 335742 335743 335744 335745 [...] (21166 flits)
Measured flits: 335736 335737 335738 335739 335740 335741 335742 335743 335744 335745 [...] (478 flits)
Class 0:
Remaining flits: 394236 394237 394238 394239 394240 394241 394242 394243 394244 394245 [...] (20753 flits)
Measured flits: 466506 466507 466508 466509 466510 466511 466512 466513 466514 466515 [...] (201 flits)
Class 0:
Remaining flits: 429192 429193 429194 429195 429196 429197 429198 429199 429200 429201 [...] (21131 flits)
Measured flits: 466506 466507 466508 466509 466510 466511 466512 466513 466514 466515 [...] (34 flits)
Class 0:
Remaining flits: 462043 462044 462045 462046 462047 462048 462049 462050 462051 462052 [...] (21884 flits)
Measured flits: 466506 466507 466508 466509 466510 466511 466512 466513 466514 466515 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 497286 497287 497288 497289 497290 497291 497292 497293 497294 497295 [...] (716 flits)
Measured flits: (0 flits)
Time taken is 19651 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1154.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8868 (1 samples)
Network latency average = 620.12 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6841 (1 samples)
Flit latency average = 518.612 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6824 (1 samples)
Fragmentation average = 195.563 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5149 (1 samples)
Injected packet rate average = 0.0106176 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.017 (1 samples)
Accepted packet rate average = 0.0103557 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0132857 (1 samples)
Injected flit rate average = 0.191033 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.304143 (1 samples)
Accepted flit rate average = 0.186788 (1 samples)
	minimum = 0.141429 (1 samples)
	maximum = 0.236714 (1 samples)
Injected packet size average = 17.9922 (1 samples)
Accepted packet size average = 18.0373 (1 samples)
Hops average = 5.06405 (1 samples)
Total run time 16.3561
