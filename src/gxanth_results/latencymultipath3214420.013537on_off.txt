BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 204.785
	minimum = 22
	maximum = 766
Network latency average = 149.581
	minimum = 22
	maximum = 554
Slowest packet = 63
Flit latency average = 120.383
	minimum = 5
	maximum = 537
Slowest flit = 22967
Fragmentation average = 24.2644
	minimum = 0
	maximum = 160
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.0114844
	minimum = 0.004 (at node 41)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.210755
	minimum = 0.076 (at node 41)
	maximum = 0.376 (at node 44)
Injected packet length average = 17.8695
Accepted packet length average = 18.3515
Total in-flight flits = 7685 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 237.33
	minimum = 22
	maximum = 1332
Network latency average = 178.095
	minimum = 22
	maximum = 1128
Slowest packet = 63
Flit latency average = 148.818
	minimum = 5
	maximum = 1111
Slowest flit = 41993
Fragmentation average = 26.1802
	minimum = 0
	maximum = 273
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.0116198
	minimum = 0.0065 (at node 79)
	maximum = 0.0175 (at node 166)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.212057
	minimum = 0.119 (at node 135)
	maximum = 0.3185 (at node 166)
Injected packet length average = 17.9095
Accepted packet length average = 18.2497
Total in-flight flits = 9650 (0 measured)
latency change    = 0.137128
throughput change = 0.00614024
Class 0:
Packet latency average = 320.521
	minimum = 25
	maximum = 1415
Network latency average = 257.251
	minimum = 22
	maximum = 1198
Slowest packet = 3008
Flit latency average = 226.182
	minimum = 5
	maximum = 1204
Slowest flit = 81651
Fragmentation average = 28.0078
	minimum = 0
	maximum = 275
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0126406
	minimum = 0.006 (at node 184)
	maximum = 0.022 (at node 24)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.226406
	minimum = 0.098 (at node 184)
	maximum = 0.403 (at node 34)
Injected packet length average = 18.0569
Accepted packet length average = 17.911
Total in-flight flits = 11108 (0 measured)
latency change    = 0.25955
throughput change = 0.063377
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 276.362
	minimum = 22
	maximum = 974
Network latency average = 214.287
	minimum = 22
	maximum = 791
Slowest packet = 7572
Flit latency average = 240.737
	minimum = 5
	maximum = 1289
Slowest flit = 81662
Fragmentation average = 26.9461
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0128125
	minimum = 0.004 (at node 68)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.232026
	minimum = 0.078 (at node 68)
	maximum = 0.432 (at node 16)
Injected packet length average = 17.981
Accepted packet length average = 18.1093
Total in-flight flits = 12927 (12223 measured)
latency change    = 0.159788
throughput change = 0.0242205
Class 0:
Packet latency average = 329.88
	minimum = 22
	maximum = 2121
Network latency average = 263.619
	minimum = 22
	maximum = 1952
Slowest packet = 7573
Flit latency average = 264.409
	minimum = 5
	maximum = 1990
Slowest flit = 136067
Fragmentation average = 29.5259
	minimum = 0
	maximum = 363
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.0129583
	minimum = 0.006 (at node 4)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.234201
	minimum = 0.1005 (at node 4)
	maximum = 0.3545 (at node 182)
Injected packet length average = 17.9645
Accepted packet length average = 18.0734
Total in-flight flits = 15531 (15531 measured)
latency change    = 0.162236
throughput change = 0.00928469
Class 0:
Packet latency average = 369.228
	minimum = 22
	maximum = 2455
Network latency average = 301.36
	minimum = 22
	maximum = 2195
Slowest packet = 7573
Flit latency average = 287.457
	minimum = 5
	maximum = 2178
Slowest flit = 142829
Fragmentation average = 29.4601
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0129601
	minimum = 0.008 (at node 86)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.233094
	minimum = 0.144 (at node 86)
	maximum = 0.338667 (at node 128)
Injected packet length average = 17.9913
Accepted packet length average = 17.9855
Total in-flight flits = 13376 (13376 measured)
latency change    = 0.106569
throughput change = 0.00474818
Class 0:
Packet latency average = 380.058
	minimum = 22
	maximum = 2455
Network latency average = 312.823
	minimum = 22
	maximum = 2195
Slowest packet = 7573
Flit latency average = 293.737
	minimum = 5
	maximum = 2178
Slowest flit = 142829
Fragmentation average = 29.2799
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0133529
	minimum = 0.00175 (at node 78)
	maximum = 0.02675 (at node 97)
Accepted packet rate average = 0.0128945
	minimum = 0.009 (at node 36)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.24025
	minimum = 0.0315 (at node 78)
	maximum = 0.48225 (at node 97)
Accepted flit rate average= 0.232491
	minimum = 0.16375 (at node 36)
	maximum = 0.31525 (at node 128)
Injected packet length average = 17.9924
Accepted packet length average = 18.0302
Total in-flight flits = 17145 (17145 measured)
latency change    = 0.028496
throughput change = 0.00259307
Class 0:
Packet latency average = 390.077
	minimum = 22
	maximum = 2455
Network latency average = 323.164
	minimum = 22
	maximum = 2195
Slowest packet = 7573
Flit latency average = 301.019
	minimum = 5
	maximum = 2178
Slowest flit = 142829
Fragmentation average = 30.3681
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0133448
	minimum = 0.004 (at node 78)
	maximum = 0.0256 (at node 109)
Accepted packet rate average = 0.0128927
	minimum = 0.0088 (at node 162)
	maximum = 0.0172 (at node 128)
Injected flit rate average = 0.240096
	minimum = 0.0712 (at node 119)
	maximum = 0.4608 (at node 109)
Accepted flit rate average= 0.23266
	minimum = 0.1584 (at node 162)
	maximum = 0.3096 (at node 128)
Injected packet length average = 17.9917
Accepted packet length average = 18.0459
Total in-flight flits = 18352 (18352 measured)
latency change    = 0.025684
throughput change = 0.000728664
Class 0:
Packet latency average = 407.159
	minimum = 22
	maximum = 2455
Network latency average = 340.528
	minimum = 22
	maximum = 2195
Slowest packet = 7573
Flit latency average = 314.875
	minimum = 5
	maximum = 2178
Slowest flit = 142829
Fragmentation average = 31.6562
	minimum = 0
	maximum = 363
Injected packet rate average = 0.013237
	minimum = 0.00566667 (at node 80)
	maximum = 0.0251667 (at node 28)
Accepted packet rate average = 0.0129271
	minimum = 0.009 (at node 86)
	maximum = 0.017 (at node 128)
Injected flit rate average = 0.238133
	minimum = 0.102 (at node 80)
	maximum = 0.450333 (at node 28)
Accepted flit rate average= 0.233017
	minimum = 0.162333 (at node 86)
	maximum = 0.307167 (at node 128)
Injected packet length average = 17.99
Accepted packet length average = 18.0255
Total in-flight flits = 17154 (17154 measured)
latency change    = 0.0419543
throughput change = 0.00153184
Draining all recorded packets ...
Class 0:
Remaining flits: 382896 382897 382898 382899 382900 382901 382902 382903 382904 382905 [...] (22490 flits)
Measured flits: 382896 382897 382898 382899 382900 382901 382902 382903 382904 382905 [...] (1413 flits)
Class 0:
Remaining flits: 404850 404851 404852 404853 404854 404855 411462 411463 411464 411465 [...] (24515 flits)
Measured flits: 404850 404851 404852 404853 404854 404855 (6 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 483511 483512 483513 483514 483515 483599 483600 483601 483602 483603 [...] (2192 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 508764 508765 508766 508767 508768 508769 (6 flits)
Measured flits: (0 flits)
Time taken is 13056 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 436.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2455 (1 samples)
Network latency average = 368.313 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2195 (1 samples)
Flit latency average = 374.843 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2335 (1 samples)
Fragmentation average = 32.6348 (1 samples)
	minimum = 0 (1 samples)
	maximum = 363 (1 samples)
Injected packet rate average = 0.013237 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0251667 (1 samples)
Accepted packet rate average = 0.0129271 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.238133 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.450333 (1 samples)
Accepted flit rate average = 0.233017 (1 samples)
	minimum = 0.162333 (1 samples)
	maximum = 0.307167 (1 samples)
Injected packet size average = 17.99 (1 samples)
Accepted packet size average = 18.0255 (1 samples)
Hops average = 5.07657 (1 samples)
Total run time 7.83814
