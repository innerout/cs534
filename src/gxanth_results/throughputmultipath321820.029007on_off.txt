BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 313.872
	minimum = 22
	maximum = 973
Network latency average = 217.057
	minimum = 22
	maximum = 841
Slowest packet = 21
Flit latency average = 184.361
	minimum = 5
	maximum = 842
Slowest flit = 11733
Fragmentation average = 44.5213
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0223229
	minimum = 0 (at node 122)
	maximum = 0.052 (at node 11)
Accepted packet rate average = 0.0132187
	minimum = 0.006 (at node 150)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.397432
	minimum = 0 (at node 122)
	maximum = 0.933 (at node 11)
Accepted flit rate average= 0.247984
	minimum = 0.108 (at node 150)
	maximum = 0.416 (at node 44)
Injected packet length average = 17.8038
Accepted packet length average = 18.76
Total in-flight flits = 29679 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 532.686
	minimum = 22
	maximum = 1928
Network latency average = 380.781
	minimum = 22
	maximum = 1550
Slowest packet = 21
Flit latency average = 342.3
	minimum = 5
	maximum = 1533
Slowest flit = 20735
Fragmentation average = 52.5321
	minimum = 0
	maximum = 367
Injected packet rate average = 0.0202682
	minimum = 0.003 (at node 2)
	maximum = 0.0415 (at node 171)
Accepted packet rate average = 0.0140026
	minimum = 0.008 (at node 10)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.362677
	minimum = 0.054 (at node 2)
	maximum = 0.747 (at node 171)
Accepted flit rate average= 0.257315
	minimum = 0.144 (at node 10)
	maximum = 0.396 (at node 152)
Injected packet length average = 17.8939
Accepted packet length average = 18.3762
Total in-flight flits = 41699 (0 measured)
latency change    = 0.410775
throughput change = 0.0362619
Class 0:
Packet latency average = 1152.43
	minimum = 28
	maximum = 2874
Network latency average = 757.477
	minimum = 22
	maximum = 2176
Slowest packet = 1888
Flit latency average = 708.613
	minimum = 5
	maximum = 2159
Slowest flit = 60731
Fragmentation average = 64.1424
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0160417
	minimum = 0 (at node 22)
	maximum = 0.036 (at node 0)
Accepted packet rate average = 0.0149219
	minimum = 0.008 (at node 49)
	maximum = 0.025 (at node 166)
Injected flit rate average = 0.287823
	minimum = 0 (at node 22)
	maximum = 0.641 (at node 0)
Accepted flit rate average= 0.270427
	minimum = 0.141 (at node 146)
	maximum = 0.445 (at node 34)
Injected packet length average = 17.9422
Accepted packet length average = 18.1229
Total in-flight flits = 45577 (0 measured)
latency change    = 0.537771
throughput change = 0.0484862
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1221.27
	minimum = 28
	maximum = 3670
Network latency average = 385.518
	minimum = 22
	maximum = 980
Slowest packet = 10909
Flit latency average = 828.684
	minimum = 5
	maximum = 2563
Slowest flit = 79793
Fragmentation average = 36.2132
	minimum = 0
	maximum = 207
Injected packet rate average = 0.0160677
	minimum = 0 (at node 105)
	maximum = 0.032 (at node 103)
Accepted packet rate average = 0.0149635
	minimum = 0.006 (at node 1)
	maximum = 0.025 (at node 129)
Injected flit rate average = 0.288729
	minimum = 0 (at node 105)
	maximum = 0.561 (at node 103)
Accepted flit rate average= 0.268771
	minimum = 0.091 (at node 1)
	maximum = 0.458 (at node 129)
Injected packet length average = 17.9695
Accepted packet length average = 17.9617
Total in-flight flits = 49413 (42259 measured)
latency change    = 0.0563718
throughput change = 0.00616231
Class 0:
Packet latency average = 1730.43
	minimum = 25
	maximum = 4722
Network latency average = 728.539
	minimum = 22
	maximum = 1908
Slowest packet = 10909
Flit latency average = 853.594
	minimum = 5
	maximum = 3072
Slowest flit = 86633
Fragmentation average = 61.3136
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0154948
	minimum = 0.0005 (at node 186)
	maximum = 0.0275 (at node 187)
Accepted packet rate average = 0.0148333
	minimum = 0.009 (at node 79)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.278773
	minimum = 0.0145 (at node 186)
	maximum = 0.493 (at node 187)
Accepted flit rate average= 0.26819
	minimum = 0.1655 (at node 4)
	maximum = 0.408 (at node 128)
Injected packet length average = 17.9914
Accepted packet length average = 18.0802
Total in-flight flits = 49710 (49311 measured)
latency change    = 0.294239
throughput change = 0.00216536
Class 0:
Packet latency average = 2077.49
	minimum = 25
	maximum = 5587
Network latency average = 852.449
	minimum = 22
	maximum = 2737
Slowest packet = 10909
Flit latency average = 878.823
	minimum = 5
	maximum = 3072
Slowest flit = 86633
Fragmentation average = 67.378
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0154149
	minimum = 0.00233333 (at node 186)
	maximum = 0.025 (at node 170)
Accepted packet rate average = 0.0148663
	minimum = 0.01 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.2772
	minimum = 0.0456667 (at node 186)
	maximum = 0.455333 (at node 170)
Accepted flit rate average= 0.267753
	minimum = 0.176 (at node 36)
	maximum = 0.376333 (at node 129)
Injected packet length average = 17.9825
Accepted packet length average = 18.0107
Total in-flight flits = 51191 (51173 measured)
latency change    = 0.167054
throughput change = 0.00163072
Draining remaining packets ...
Class 0:
Remaining flits: 254408 254409 254410 254411 264582 264583 264584 264585 264586 264587 [...] (5945 flits)
Measured flits: 254408 254409 254410 254411 264582 264583 264584 264585 264586 264587 [...] (5945 flits)
Time taken is 7669 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2388.4 (1 samples)
	minimum = 25 (1 samples)
	maximum = 6294 (1 samples)
Network latency average = 968.034 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3224 (1 samples)
Flit latency average = 942.064 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3909 (1 samples)
Fragmentation average = 70.791 (1 samples)
	minimum = 0 (1 samples)
	maximum = 316 (1 samples)
Injected packet rate average = 0.0154149 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.025 (1 samples)
Accepted packet rate average = 0.0148663 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.2772 (1 samples)
	minimum = 0.0456667 (1 samples)
	maximum = 0.455333 (1 samples)
Accepted flit rate average = 0.267753 (1 samples)
	minimum = 0.176 (1 samples)
	maximum = 0.376333 (1 samples)
Injected packet size average = 17.9825 (1 samples)
Accepted packet size average = 18.0107 (1 samples)
Hops average = 5.10732 (1 samples)
Total run time 8.56611
