BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 372.099
	minimum = 23
	maximum = 955
Network latency average = 345.175
	minimum = 23
	maximum = 951
Slowest packet = 311
Flit latency average = 313.755
	minimum = 6
	maximum = 952
Slowest flit = 5048
Fragmentation average = 57.1117
	minimum = 0
	maximum = 157
Injected packet rate average = 0.0415104
	minimum = 0.021 (at node 16)
	maximum = 0.052 (at node 9)
Accepted packet rate average = 0.010401
	minimum = 0.005 (at node 54)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.740057
	minimum = 0.377 (at node 16)
	maximum = 0.935 (at node 119)
Accepted flit rate average= 0.194734
	minimum = 0.09 (at node 64)
	maximum = 0.348 (at node 44)
Injected packet length average = 17.8282
Accepted packet length average = 18.7226
Total in-flight flits = 106539 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 759.941
	minimum = 23
	maximum = 1900
Network latency average = 691.555
	minimum = 23
	maximum = 1854
Slowest packet = 672
Flit latency average = 656.701
	minimum = 6
	maximum = 1861
Slowest flit = 19281
Fragmentation average = 63.439
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0284948
	minimum = 0.0165 (at node 108)
	maximum = 0.0365 (at node 61)
Accepted packet rate average = 0.0100729
	minimum = 0.006 (at node 96)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.510141
	minimum = 0.2955 (at node 108)
	maximum = 0.653 (at node 61)
Accepted flit rate average= 0.185411
	minimum = 0.108 (at node 96)
	maximum = 0.297 (at node 22)
Injected packet length average = 17.9029
Accepted packet length average = 18.4069
Total in-flight flits = 127036 (0 measured)
latency change    = 0.510358
throughput change = 0.0502823
Class 0:
Packet latency average = 2006.33
	minimum = 405
	maximum = 2833
Network latency average = 1752.72
	minimum = 28
	maximum = 2762
Slowest packet = 1548
Flit latency average = 1722.51
	minimum = 6
	maximum = 2758
Slowest flit = 37874
Fragmentation average = 72.4141
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00995833
	minimum = 0 (at node 3)
	maximum = 0.028 (at node 43)
Accepted packet rate average = 0.00936979
	minimum = 0.003 (at node 4)
	maximum = 0.017 (at node 107)
Injected flit rate average = 0.181583
	minimum = 0 (at node 3)
	maximum = 0.506 (at node 43)
Accepted flit rate average= 0.167036
	minimum = 0.054 (at node 153)
	maximum = 0.309 (at node 107)
Injected packet length average = 18.2343
Accepted packet length average = 17.8271
Total in-flight flits = 130407 (0 measured)
latency change    = 0.621228
throughput change = 0.110006
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2341.41
	minimum = 1485
	maximum = 3237
Network latency average = 163.926
	minimum = 23
	maximum = 887
Slowest packet = 12998
Flit latency average = 2441.63
	minimum = 6
	maximum = 3620
Slowest flit = 56951
Fragmentation average = 23.6852
	minimum = 0
	maximum = 100
Injected packet rate average = 0.00780729
	minimum = 0 (at node 20)
	maximum = 0.039 (at node 27)
Accepted packet rate average = 0.00890104
	minimum = 0.003 (at node 81)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.141245
	minimum = 0 (at node 20)
	maximum = 0.699 (at node 27)
Accepted flit rate average= 0.161036
	minimum = 0.064 (at node 32)
	maximum = 0.312 (at node 56)
Injected packet length average = 18.0914
Accepted packet length average = 18.0919
Total in-flight flits = 126632 (26161 measured)
latency change    = 0.14311
throughput change = 0.0372586
Class 0:
Packet latency average = 3144.43
	minimum = 1485
	maximum = 4216
Network latency average = 570.122
	minimum = 23
	maximum = 1846
Slowest packet = 12998
Flit latency average = 2714.56
	minimum = 6
	maximum = 4608
Slowest flit = 46367
Fragmentation average = 43.558
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00786979
	minimum = 0.0005 (at node 21)
	maximum = 0.0235 (at node 27)
Accepted packet rate average = 0.00885417
	minimum = 0.0045 (at node 36)
	maximum = 0.016 (at node 56)
Injected flit rate average = 0.14163
	minimum = 0.003 (at node 140)
	maximum = 0.423 (at node 27)
Accepted flit rate average= 0.159552
	minimum = 0.081 (at node 36)
	maximum = 0.291 (at node 56)
Injected packet length average = 17.9967
Accepted packet length average = 18.02
Total in-flight flits = 123481 (50943 measured)
latency change    = 0.255378
throughput change = 0.00930339
Class 0:
Packet latency average = 3960.06
	minimum = 1485
	maximum = 5120
Network latency average = 1063.57
	minimum = 23
	maximum = 2870
Slowest packet = 12998
Flit latency average = 2905.79
	minimum = 6
	maximum = 5468
Slowest flit = 80697
Fragmentation average = 55.6083
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00826389
	minimum = 0.000333333 (at node 106)
	maximum = 0.0233333 (at node 20)
Accepted packet rate average = 0.00887326
	minimum = 0.00533333 (at node 4)
	maximum = 0.013 (at node 56)
Injected flit rate average = 0.148637
	minimum = 0.006 (at node 106)
	maximum = 0.415333 (at node 20)
Accepted flit rate average= 0.160097
	minimum = 0.0936667 (at node 4)
	maximum = 0.233667 (at node 51)
Injected packet length average = 17.9863
Accepted packet length average = 18.0427
Total in-flight flits = 123673 (74332 measured)
latency change    = 0.205965
throughput change = 0.00340505
Class 0:
Packet latency average = 4591.72
	minimum = 1485
	maximum = 6060
Network latency average = 1545.44
	minimum = 23
	maximum = 3945
Slowest packet = 12998
Flit latency average = 3074.78
	minimum = 6
	maximum = 6551
Slowest flit = 67387
Fragmentation average = 59.2744
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00852344
	minimum = 0.00075 (at node 106)
	maximum = 0.02 (at node 185)
Accepted packet rate average = 0.009
	minimum = 0.0055 (at node 36)
	maximum = 0.0125 (at node 119)
Injected flit rate average = 0.153579
	minimum = 0.0135 (at node 106)
	maximum = 0.3565 (at node 185)
Accepted flit rate average= 0.162326
	minimum = 0.10325 (at node 36)
	maximum = 0.221 (at node 119)
Injected packet length average = 18.0185
Accepted packet length average = 18.0362
Total in-flight flits = 123623 (93288 measured)
latency change    = 0.137565
throughput change = 0.0137273
Class 0:
Packet latency average = 5193.93
	minimum = 1485
	maximum = 7102
Network latency average = 1984.69
	minimum = 23
	maximum = 4974
Slowest packet = 12998
Flit latency average = 3232.6
	minimum = 6
	maximum = 7387
Slowest flit = 88397
Fragmentation average = 62.9518
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00862917
	minimum = 0.0022 (at node 175)
	maximum = 0.0194 (at node 20)
Accepted packet rate average = 0.00902917
	minimum = 0.0062 (at node 36)
	maximum = 0.0126 (at node 16)
Injected flit rate average = 0.155307
	minimum = 0.0396 (at node 175)
	maximum = 0.3458 (at node 20)
Accepted flit rate average= 0.162659
	minimum = 0.1116 (at node 36)
	maximum = 0.2268 (at node 16)
Injected packet length average = 17.9979
Accepted packet length average = 18.0149
Total in-flight flits = 123294 (106708 measured)
latency change    = 0.115945
throughput change = 0.00205247
Class 0:
Packet latency average = 5794.75
	minimum = 1485
	maximum = 7941
Network latency average = 2362.62
	minimum = 23
	maximum = 5851
Slowest packet = 12998
Flit latency average = 3346.23
	minimum = 6
	maximum = 8388
Slowest flit = 90986
Fragmentation average = 65.9835
	minimum = 0
	maximum = 151
Injected packet rate average = 0.00871354
	minimum = 0.00233333 (at node 1)
	maximum = 0.0185 (at node 20)
Accepted packet rate average = 0.00903559
	minimum = 0.006 (at node 36)
	maximum = 0.0126667 (at node 177)
Injected flit rate average = 0.156748
	minimum = 0.042 (at node 1)
	maximum = 0.330167 (at node 20)
Accepted flit rate average= 0.162907
	minimum = 0.1105 (at node 36)
	maximum = 0.230833 (at node 177)
Injected packet length average = 17.989
Accepted packet length average = 18.0295
Total in-flight flits = 123026 (114596 measured)
latency change    = 0.103683
throughput change = 0.00152076
Class 0:
Packet latency average = 6358.26
	minimum = 1485
	maximum = 8980
Network latency average = 2682.45
	minimum = 23
	maximum = 6919
Slowest packet = 12998
Flit latency average = 3439.01
	minimum = 6
	maximum = 9090
Slowest flit = 127854
Fragmentation average = 67.9481
	minimum = 0
	maximum = 158
Injected packet rate average = 0.00870015
	minimum = 0.00257143 (at node 1)
	maximum = 0.0178571 (at node 20)
Accepted packet rate average = 0.009
	minimum = 0.00642857 (at node 2)
	maximum = 0.0122857 (at node 128)
Injected flit rate average = 0.156648
	minimum = 0.0452857 (at node 1)
	maximum = 0.319 (at node 20)
Accepted flit rate average= 0.162296
	minimum = 0.115286 (at node 2)
	maximum = 0.221286 (at node 128)
Injected packet length average = 18.0052
Accepted packet length average = 18.0329
Total in-flight flits = 122647 (118752 measured)
latency change    = 0.088627
throughput change = 0.00376464
Draining all recorded packets ...
Class 0:
Remaining flits: 107028 107029 107030 107031 107032 107033 107034 107035 107036 107037 [...] (123757 flits)
Measured flits: 234090 234091 234092 234093 234094 234095 234096 234097 234098 234099 [...] (122304 flits)
Class 0:
Remaining flits: 136080 136081 136082 136083 136084 136085 136086 136087 136088 136089 [...] (122078 flits)
Measured flits: 234108 234109 234110 234111 234112 234113 234114 234115 234116 234117 [...] (121477 flits)
Class 0:
Remaining flits: 193698 193699 193700 193701 193702 193703 193704 193705 193706 193707 [...] (121803 flits)
Measured flits: 239436 239437 239438 239439 239440 239441 239442 239443 239444 239445 [...] (121666 flits)
Class 0:
Remaining flits: 274140 274141 274142 274143 274144 274145 274146 274147 274148 274149 [...] (123704 flits)
Measured flits: 274140 274141 274142 274143 274144 274145 274146 274147 274148 274149 [...] (123704 flits)
Class 0:
Remaining flits: 286236 286237 286238 286239 286240 286241 286242 286243 286244 286245 [...] (122632 flits)
Measured flits: 286236 286237 286238 286239 286240 286241 286242 286243 286244 286245 [...] (122632 flits)
Class 0:
Remaining flits: 306774 306775 306776 306777 306778 306779 306780 306781 306782 306783 [...] (124016 flits)
Measured flits: 306774 306775 306776 306777 306778 306779 306780 306781 306782 306783 [...] (124016 flits)
Class 0:
Remaining flits: 344952 344953 344954 344955 344956 344957 344958 344959 344960 344961 [...] (120629 flits)
Measured flits: 344952 344953 344954 344955 344956 344957 344958 344959 344960 344961 [...] (120629 flits)
Class 0:
Remaining flits: 362826 362827 362828 362829 362830 362831 362832 362833 362834 362835 [...] (119506 flits)
Measured flits: 362826 362827 362828 362829 362830 362831 362832 362833 362834 362835 [...] (119506 flits)
Class 0:
Remaining flits: 368278 368279 394056 394057 394058 394059 394060 394061 394062 394063 [...] (120375 flits)
Measured flits: 368278 368279 394056 394057 394058 394059 394060 394061 394062 394063 [...] (120375 flits)
Class 0:
Remaining flits: 394056 394057 394058 394059 394060 394061 394062 394063 394064 394065 [...] (120134 flits)
Measured flits: 394056 394057 394058 394059 394060 394061 394062 394063 394064 394065 [...] (120134 flits)
Class 0:
Remaining flits: 422730 422731 422732 422733 422734 422735 422736 422737 422738 422739 [...] (118337 flits)
Measured flits: 422730 422731 422732 422733 422734 422735 422736 422737 422738 422739 [...] (118337 flits)
Class 0:
Remaining flits: 437256 437257 437258 437259 437260 437261 437262 437263 437264 437265 [...] (119452 flits)
Measured flits: 437256 437257 437258 437259 437260 437261 437262 437263 437264 437265 [...] (119452 flits)
Class 0:
Remaining flits: 437256 437257 437258 437259 437260 437261 437262 437263 437264 437265 [...] (120905 flits)
Measured flits: 437256 437257 437258 437259 437260 437261 437262 437263 437264 437265 [...] (120905 flits)
Class 0:
Remaining flits: 439849 439850 439851 439852 439853 439854 439855 439856 439857 439858 [...] (123084 flits)
Measured flits: 439849 439850 439851 439852 439853 439854 439855 439856 439857 439858 [...] (123084 flits)
Class 0:
Remaining flits: 565938 565939 565940 565941 565942 565943 565944 565945 565946 565947 [...] (122440 flits)
Measured flits: 565938 565939 565940 565941 565942 565943 565944 565945 565946 565947 [...] (122440 flits)
Class 0:
Remaining flits: 612974 612975 612976 612977 612978 612979 612980 612981 612982 612983 [...] (120153 flits)
Measured flits: 612974 612975 612976 612977 612978 612979 612980 612981 612982 612983 [...] (120153 flits)
Class 0:
Remaining flits: 659034 659035 659036 659037 659038 659039 659040 659041 659042 659043 [...] (121191 flits)
Measured flits: 659034 659035 659036 659037 659038 659039 659040 659041 659042 659043 [...] (121191 flits)
Class 0:
Remaining flits: 688122 688123 688124 688125 688126 688127 688128 688129 688130 688131 [...] (121427 flits)
Measured flits: 688122 688123 688124 688125 688126 688127 688128 688129 688130 688131 [...] (121427 flits)
Class 0:
Remaining flits: 690804 690805 690806 690807 690808 690809 690810 690811 690812 690813 [...] (120600 flits)
Measured flits: 690804 690805 690806 690807 690808 690809 690810 690811 690812 690813 [...] (120600 flits)
Class 0:
Remaining flits: 751788 751789 751790 751791 751792 751793 751794 751795 751796 751797 [...] (119151 flits)
Measured flits: 751788 751789 751790 751791 751792 751793 751794 751795 751796 751797 [...] (119151 flits)
Class 0:
Remaining flits: 761238 761239 761240 761241 761242 761243 761244 761245 761246 761247 [...] (118498 flits)
Measured flits: 761238 761239 761240 761241 761242 761243 761244 761245 761246 761247 [...] (118498 flits)
Class 0:
Remaining flits: 787716 787717 787718 787719 787720 787721 787722 787723 787724 787725 [...] (120425 flits)
Measured flits: 787716 787717 787718 787719 787720 787721 787722 787723 787724 787725 [...] (120425 flits)
Class 0:
Remaining flits: 790020 790021 790022 790023 790024 790025 790026 790027 790028 790029 [...] (120550 flits)
Measured flits: 790020 790021 790022 790023 790024 790025 790026 790027 790028 790029 [...] (120550 flits)
Class 0:
Remaining flits: 830700 830701 830702 830703 830704 830705 830706 830707 830708 830709 [...] (117687 flits)
Measured flits: 830700 830701 830702 830703 830704 830705 830706 830707 830708 830709 [...] (117687 flits)
Class 0:
Remaining flits: 837918 837919 837920 837921 837922 837923 837924 837925 837926 837927 [...] (116220 flits)
Measured flits: 837918 837919 837920 837921 837922 837923 837924 837925 837926 837927 [...] (116220 flits)
Class 0:
Remaining flits: 863916 863917 863918 863919 863920 863921 863922 863923 863924 863925 [...] (116724 flits)
Measured flits: 863916 863917 863918 863919 863920 863921 863922 863923 863924 863925 [...] (116724 flits)
Class 0:
Remaining flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (118884 flits)
Measured flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (118884 flits)
Class 0:
Remaining flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (118784 flits)
Measured flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (118784 flits)
Class 0:
Remaining flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (120171 flits)
Measured flits: 940590 940591 940592 940593 940594 940595 940596 940597 940598 940599 [...] (120171 flits)
Class 0:
Remaining flits: 964530 964531 964532 964533 964534 964535 964536 964537 964538 964539 [...] (118088 flits)
Measured flits: 964530 964531 964532 964533 964534 964535 964536 964537 964538 964539 [...] (118088 flits)
Class 0:
Remaining flits: 964530 964531 964532 964533 964534 964535 964536 964537 964538 964539 [...] (118906 flits)
Measured flits: 964530 964531 964532 964533 964534 964535 964536 964537 964538 964539 [...] (118906 flits)
Class 0:
Remaining flits: 1039680 1039681 1039682 1039683 1039684 1039685 1039686 1039687 1039688 1039689 [...] (119535 flits)
Measured flits: 1039680 1039681 1039682 1039683 1039684 1039685 1039686 1039687 1039688 1039689 [...] (119535 flits)
Class 0:
Remaining flits: 1097010 1097011 1097012 1097013 1097014 1097015 1097016 1097017 1097018 1097019 [...] (119692 flits)
Measured flits: 1097010 1097011 1097012 1097013 1097014 1097015 1097016 1097017 1097018 1097019 [...] (119692 flits)
Class 0:
Remaining flits: 1160280 1160281 1160282 1160283 1160284 1160285 1160286 1160287 1160288 1160289 [...] (118425 flits)
Measured flits: 1160280 1160281 1160282 1160283 1160284 1160285 1160286 1160287 1160288 1160289 [...] (118425 flits)
Class 0:
Remaining flits: 1160280 1160281 1160282 1160283 1160284 1160285 1160286 1160287 1160288 1160289 [...] (120113 flits)
Measured flits: 1160280 1160281 1160282 1160283 1160284 1160285 1160286 1160287 1160288 1160289 [...] (120077 flits)
Class 0:
Remaining flits: 1234800 1234801 1234802 1234803 1234804 1234805 1234806 1234807 1234808 1234809 [...] (120860 flits)
Measured flits: 1234800 1234801 1234802 1234803 1234804 1234805 1234806 1234807 1234808 1234809 [...] (120212 flits)
Class 0:
Remaining flits: 1234800 1234801 1234802 1234803 1234804 1234805 1234806 1234807 1234808 1234809 [...] (120345 flits)
Measured flits: 1234800 1234801 1234802 1234803 1234804 1234805 1234806 1234807 1234808 1234809 [...] (118725 flits)
Class 0:
Remaining flits: 1249369 1249370 1249371 1249372 1249373 1249374 1249375 1249376 1249377 1249378 [...] (119314 flits)
Measured flits: 1249369 1249370 1249371 1249372 1249373 1249374 1249375 1249376 1249377 1249378 [...] (116236 flits)
Class 0:
Remaining flits: 1287180 1287181 1287182 1287183 1287184 1287185 1287186 1287187 1287188 1287189 [...] (119381 flits)
Measured flits: 1287180 1287181 1287182 1287183 1287184 1287185 1287186 1287187 1287188 1287189 [...] (114219 flits)
Class 0:
Remaining flits: 1287180 1287181 1287182 1287183 1287184 1287185 1287186 1287187 1287188 1287189 [...] (119393 flits)
Measured flits: 1287180 1287181 1287182 1287183 1287184 1287185 1287186 1287187 1287188 1287189 [...] (112253 flits)
Class 0:
Remaining flits: 1341992 1341993 1341994 1341995 1341996 1341997 1341998 1341999 1342000 1342001 [...] (120955 flits)
Measured flits: 1341992 1341993 1341994 1341995 1341996 1341997 1341998 1341999 1342000 1342001 [...] (111283 flits)
Class 0:
Remaining flits: 1393542 1393543 1393544 1393545 1393546 1393547 1393548 1393549 1393550 1393551 [...] (121630 flits)
Measured flits: 1393542 1393543 1393544 1393545 1393546 1393547 1393548 1393549 1393550 1393551 [...] (107350 flits)
Class 0:
Remaining flits: 1420560 1420561 1420562 1420563 1420564 1420565 1420566 1420567 1420568 1420569 [...] (122669 flits)
Measured flits: 1420560 1420561 1420562 1420563 1420564 1420565 1420566 1420567 1420568 1420569 [...] (104092 flits)
Class 0:
Remaining flits: 1455840 1455841 1455842 1455843 1455844 1455845 1455846 1455847 1455848 1455849 [...] (121726 flits)
Measured flits: 1455840 1455841 1455842 1455843 1455844 1455845 1455846 1455847 1455848 1455849 [...] (97850 flits)
Class 0:
Remaining flits: 1483775 1484784 1484785 1484786 1484787 1484788 1484789 1484790 1484791 1484792 [...] (121776 flits)
Measured flits: 1483775 1484784 1484785 1484786 1484787 1484788 1484789 1484790 1484791 1484792 [...] (91672 flits)
Class 0:
Remaining flits: 1520712 1520713 1520714 1520715 1520716 1520717 1520718 1520719 1520720 1520721 [...] (121764 flits)
Measured flits: 1520712 1520713 1520714 1520715 1520716 1520717 1520718 1520719 1520720 1520721 [...] (85111 flits)
Class 0:
Remaining flits: 1584486 1584487 1584488 1584489 1584490 1584491 1584492 1584493 1584494 1584495 [...] (122701 flits)
Measured flits: 1584486 1584487 1584488 1584489 1584490 1584491 1584492 1584493 1584494 1584495 [...] (78728 flits)
Class 0:
Remaining flits: 1584486 1584487 1584488 1584489 1584490 1584491 1584492 1584493 1584494 1584495 [...] (120306 flits)
Measured flits: 1584486 1584487 1584488 1584489 1584490 1584491 1584492 1584493 1584494 1584495 [...] (72144 flits)
Class 0:
Remaining flits: 1617930 1617931 1617932 1617933 1617934 1617935 1617936 1617937 1617938 1617939 [...] (119116 flits)
Measured flits: 1617930 1617931 1617932 1617933 1617934 1617935 1617936 1617937 1617938 1617939 [...] (65838 flits)
Class 0:
Remaining flits: 1623240 1623241 1623242 1623243 1623244 1623245 1623246 1623247 1623248 1623249 [...] (120096 flits)
Measured flits: 1623240 1623241 1623242 1623243 1623244 1623245 1623246 1623247 1623248 1623249 [...] (59467 flits)
Class 0:
Remaining flits: 1670508 1670509 1670510 1670511 1670512 1670513 1670514 1670515 1670516 1670517 [...] (120369 flits)
Measured flits: 1670508 1670509 1670510 1670511 1670512 1670513 1670514 1670515 1670516 1670517 [...] (52944 flits)
Class 0:
Remaining flits: 1670508 1670509 1670510 1670511 1670512 1670513 1670514 1670515 1670516 1670517 [...] (119311 flits)
Measured flits: 1670508 1670509 1670510 1670511 1670512 1670513 1670514 1670515 1670516 1670517 [...] (46471 flits)
Class 0:
Remaining flits: 1711872 1711873 1711874 1711875 1711876 1711877 1711878 1711879 1711880 1711881 [...] (119298 flits)
Measured flits: 1711872 1711873 1711874 1711875 1711876 1711877 1711878 1711879 1711880 1711881 [...] (39364 flits)
Class 0:
Remaining flits: 1714482 1714483 1714484 1714485 1714486 1714487 1714488 1714489 1714490 1714491 [...] (119699 flits)
Measured flits: 1714482 1714483 1714484 1714485 1714486 1714487 1714488 1714489 1714490 1714491 [...] (34082 flits)
Class 0:
Remaining flits: 1799172 1799173 1799174 1799175 1799176 1799177 1799178 1799179 1799180 1799181 [...] (119714 flits)
Measured flits: 1799172 1799173 1799174 1799175 1799176 1799177 1799178 1799179 1799180 1799181 [...] (28507 flits)
Class 0:
Remaining flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (119402 flits)
Measured flits: 1811916 1811917 1811918 1811919 1811920 1811921 1811922 1811923 1811924 1811925 [...] (22384 flits)
Class 0:
Remaining flits: 1850184 1850185 1850186 1850187 1850188 1850189 1850190 1850191 1850192 1850193 [...] (120927 flits)
Measured flits: 1850184 1850185 1850186 1850187 1850188 1850189 1850190 1850191 1850192 1850193 [...] (17182 flits)
Class 0:
Remaining flits: 1912086 1912087 1912088 1912089 1912090 1912091 1912092 1912093 1912094 1912095 [...] (120769 flits)
Measured flits: 1931256 1931257 1931258 1931259 1931260 1931261 1931262 1931263 1931264 1931265 [...] (12872 flits)
Class 0:
Remaining flits: 1964088 1964089 1964090 1964091 1964092 1964093 1964094 1964095 1964096 1964097 [...] (118880 flits)
Measured flits: 2000106 2000107 2000108 2000109 2000110 2000111 2000112 2000113 2000114 2000115 [...] (9245 flits)
Class 0:
Remaining flits: 1998414 1998415 1998416 1998417 1998418 1998419 1998420 1998421 1998422 1998423 [...] (118765 flits)
Measured flits: 2007522 2007523 2007524 2007525 2007526 2007527 2007528 2007529 2007530 2007531 [...] (6652 flits)
Class 0:
Remaining flits: 2021490 2021491 2021492 2021493 2021494 2021495 2021496 2021497 2021498 2021499 [...] (116332 flits)
Measured flits: 2022588 2022589 2022590 2022591 2022592 2022593 2022594 2022595 2022596 2022597 [...] (4436 flits)
Class 0:
Remaining flits: 2022588 2022589 2022590 2022591 2022592 2022593 2022594 2022595 2022596 2022597 [...] (119210 flits)
Measured flits: 2022588 2022589 2022590 2022591 2022592 2022593 2022594 2022595 2022596 2022597 [...] (2996 flits)
Class 0:
Remaining flits: 2046078 2046079 2046080 2046081 2046082 2046083 2046084 2046085 2046086 2046087 [...] (119897 flits)
Measured flits: 2114460 2114461 2114462 2114463 2114464 2114465 2114466 2114467 2114468 2114469 [...] (2067 flits)
Class 0:
Remaining flits: 2104218 2104219 2104220 2104221 2104222 2104223 2104224 2104225 2104226 2104227 [...] (117940 flits)
Measured flits: 2128212 2128213 2128214 2128215 2128216 2128217 2128218 2128219 2128220 2128221 [...] (1449 flits)
Class 0:
Remaining flits: 2104218 2104219 2104220 2104221 2104222 2104223 2104224 2104225 2104226 2104227 [...] (116025 flits)
Measured flits: 2199438 2199439 2199440 2199441 2199442 2199443 2199444 2199445 2199446 2199447 [...] (741 flits)
Class 0:
Remaining flits: 2143296 2143297 2143298 2143299 2143300 2143301 2143302 2143303 2143304 2143305 [...] (116338 flits)
Measured flits: 2204766 2204767 2204768 2204769 2204770 2204771 2204772 2204773 2204774 2204775 [...] (262 flits)
Class 0:
Remaining flits: 2195784 2195785 2195786 2195787 2195788 2195789 2195790 2195791 2195792 2195793 [...] (115915 flits)
Measured flits: 2204766 2204767 2204768 2204769 2204770 2204771 2204772 2204773 2204774 2204775 [...] (126 flits)
Class 0:
Remaining flits: 2205738 2205739 2205740 2205741 2205742 2205743 2205744 2205745 2205746 2205747 [...] (115591 flits)
Measured flits: 2205738 2205739 2205740 2205741 2205742 2205743 2205744 2205745 2205746 2205747 [...] (59 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2206908 2206909 2206910 2206911 2206912 2206913 2206914 2206915 2206916 2206917 [...] (86671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2250450 2250451 2250452 2250453 2250454 2250455 2250456 2250457 2250458 2250459 [...] (57330 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2307942 2307943 2307944 2307945 2307946 2307947 2307948 2307949 2307950 2307951 [...] (29108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2334978 2334979 2334980 2334981 2334982 2334983 2334984 2334985 2334986 2334987 [...] (7236 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2426328 2426329 2426330 2426331 2426332 2426333 2426334 2426335 2426336 2426337 [...] (425 flits)
Measured flits: (0 flits)
Time taken is 83816 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 29211.7 (1 samples)
	minimum = 1485 (1 samples)
	maximum = 68702 (1 samples)
Network latency average = 3837.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14249 (1 samples)
Flit latency average = 3799.03 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14230 (1 samples)
Fragmentation average = 70.8942 (1 samples)
	minimum = 0 (1 samples)
	maximum = 172 (1 samples)
Injected packet rate average = 0.00870015 (1 samples)
	minimum = 0.00257143 (1 samples)
	maximum = 0.0178571 (1 samples)
Accepted packet rate average = 0.009 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.156648 (1 samples)
	minimum = 0.0452857 (1 samples)
	maximum = 0.319 (1 samples)
Accepted flit rate average = 0.162296 (1 samples)
	minimum = 0.115286 (1 samples)
	maximum = 0.221286 (1 samples)
Injected packet size average = 18.0052 (1 samples)
Accepted packet size average = 18.0329 (1 samples)
Hops average = 5.06325 (1 samples)
Total run time 64.4907
