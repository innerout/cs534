BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.647
	minimum = 23
	maximum = 905
Network latency average = 292.103
	minimum = 23
	maximum = 886
Slowest packet = 362
Flit latency average = 261.84
	minimum = 6
	maximum = 916
Slowest flit = 8713
Fragmentation average = 50.8657
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.0108594
	minimum = 0.004 (at node 64)
	maximum = 0.019 (at node 34)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.203531
	minimum = 0.074 (at node 115)
	maximum = 0.357 (at node 34)
Injected packet length average = 17.8324
Accepted packet length average = 18.7424
Total in-flight flits = 82692 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 571.127
	minimum = 23
	maximum = 1889
Network latency average = 558.175
	minimum = 23
	maximum = 1873
Slowest packet = 623
Flit latency average = 527.412
	minimum = 6
	maximum = 1866
Slowest flit = 14042
Fragmentation average = 55.3699
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0354453
	minimum = 0.0235 (at node 120)
	maximum = 0.0455 (at node 40)
Accepted packet rate average = 0.0113151
	minimum = 0.005 (at node 164)
	maximum = 0.0165 (at node 103)
Injected flit rate average = 0.635529
	minimum = 0.422 (at node 120)
	maximum = 0.819 (at node 40)
Accepted flit rate average= 0.20776
	minimum = 0.09 (at node 164)
	maximum = 0.297 (at node 103)
Injected packet length average = 17.9298
Accepted packet length average = 18.3613
Total in-flight flits = 165218 (0 measured)
latency change    = 0.470088
throughput change = 0.020356
Class 0:
Packet latency average = 1405.73
	minimum = 23
	maximum = 2660
Network latency average = 1388.52
	minimum = 23
	maximum = 2652
Slowest packet = 1580
Flit latency average = 1365.21
	minimum = 6
	maximum = 2715
Slowest flit = 30921
Fragmentation average = 64.7963
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0344792
	minimum = 0.017 (at node 44)
	maximum = 0.05 (at node 126)
Accepted packet rate average = 0.0109427
	minimum = 0.003 (at node 96)
	maximum = 0.021 (at node 179)
Injected flit rate average = 0.620255
	minimum = 0.301 (at node 44)
	maximum = 0.91 (at node 126)
Accepted flit rate average= 0.19725
	minimum = 0.055 (at node 96)
	maximum = 0.364 (at node 179)
Injected packet length average = 17.9893
Accepted packet length average = 18.0257
Total in-flight flits = 246542 (0 measured)
latency change    = 0.593714
throughput change = 0.0532847
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 196.617
	minimum = 23
	maximum = 957
Network latency average = 140.751
	minimum = 23
	maximum = 931
Slowest packet = 20585
Flit latency average = 2041.9
	minimum = 6
	maximum = 3644
Slowest flit = 41905
Fragmentation average = 10.5171
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0319844
	minimum = 0.013 (at node 4)
	maximum = 0.05 (at node 166)
Accepted packet rate average = 0.0105156
	minimum = 0.002 (at node 38)
	maximum = 0.019 (at node 188)
Injected flit rate average = 0.574724
	minimum = 0.224 (at node 4)
	maximum = 0.901 (at node 166)
Accepted flit rate average= 0.189635
	minimum = 0.038 (at node 38)
	maximum = 0.332 (at node 188)
Injected packet length average = 17.9689
Accepted packet length average = 18.0337
Total in-flight flits = 320634 (104670 measured)
latency change    = 6.14957
throughput change = 0.0401538
Class 0:
Packet latency average = 378.822
	minimum = 23
	maximum = 1680
Network latency average = 260.218
	minimum = 23
	maximum = 1639
Slowest packet = 21375
Flit latency average = 2406.61
	minimum = 6
	maximum = 4444
Slowest flit = 53027
Fragmentation average = 10.3948
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0313047
	minimum = 0.0135 (at node 68)
	maximum = 0.046 (at node 166)
Accepted packet rate average = 0.010362
	minimum = 0.0045 (at node 38)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.563169
	minimum = 0.2425 (at node 68)
	maximum = 0.8295 (at node 166)
Accepted flit rate average= 0.186201
	minimum = 0.085 (at node 38)
	maximum = 0.2835 (at node 16)
Injected packet length average = 17.9899
Accepted packet length average = 17.9696
Total in-flight flits = 391437 (205227 measured)
latency change    = 0.480978
throughput change = 0.0184473
Class 0:
Packet latency average = 567.345
	minimum = 23
	maximum = 2885
Network latency average = 399.8
	minimum = 23
	maximum = 2683
Slowest packet = 20396
Flit latency average = 2753.69
	minimum = 6
	maximum = 5396
Slowest flit = 72632
Fragmentation average = 10.5328
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0308854
	minimum = 0.0136667 (at node 32)
	maximum = 0.0453333 (at node 173)
Accepted packet rate average = 0.0102361
	minimum = 0.00633333 (at node 45)
	maximum = 0.0146667 (at node 102)
Injected flit rate average = 0.55575
	minimum = 0.243667 (at node 100)
	maximum = 0.816 (at node 173)
Accepted flit rate average= 0.184219
	minimum = 0.114 (at node 100)
	maximum = 0.270333 (at node 102)
Injected packet length average = 17.9939
Accepted packet length average = 17.9969
Total in-flight flits = 460634 (303950 measured)
latency change    = 0.33229
throughput change = 0.0107577
Class 0:
Packet latency average = 833.398
	minimum = 23
	maximum = 3953
Network latency average = 607.918
	minimum = 23
	maximum = 3948
Slowest packet = 20359
Flit latency average = 3100.13
	minimum = 6
	maximum = 6278
Slowest flit = 76679
Fragmentation average = 11.3728
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0308359
	minimum = 0.014 (at node 32)
	maximum = 0.04475 (at node 173)
Accepted packet rate average = 0.0102083
	minimum = 0.00575 (at node 49)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.554863
	minimum = 0.255 (at node 32)
	maximum = 0.80325 (at node 173)
Accepted flit rate average= 0.183719
	minimum = 0.106 (at node 49)
	maximum = 0.252 (at node 16)
Injected packet length average = 17.994
Accepted packet length average = 17.9969
Total in-flight flits = 531722 (404445 measured)
latency change    = 0.319239
throughput change = 0.00272155
Class 0:
Packet latency average = 1195.04
	minimum = 23
	maximum = 5104
Network latency average = 900.086
	minimum = 23
	maximum = 4991
Slowest packet = 20272
Flit latency average = 3428.46
	minimum = 6
	maximum = 6978
Slowest flit = 122806
Fragmentation average = 14.4618
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0309427
	minimum = 0.0142 (at node 32)
	maximum = 0.0436 (at node 173)
Accepted packet rate average = 0.0101563
	minimum = 0.0062 (at node 49)
	maximum = 0.0132 (at node 157)
Injected flit rate average = 0.556619
	minimum = 0.2524 (at node 100)
	maximum = 0.7848 (at node 173)
Accepted flit rate average= 0.182604
	minimum = 0.1116 (at node 49)
	maximum = 0.2442 (at node 157)
Injected packet length average = 17.9887
Accepted packet length average = 17.9795
Total in-flight flits = 605950 (506209 measured)
latency change    = 0.302618
throughput change = 0.00610382
Class 0:
Packet latency average = 1739.54
	minimum = 23
	maximum = 6658
Network latency average = 1432.67
	minimum = 23
	maximum = 5946
Slowest packet = 20337
Flit latency average = 3766.29
	minimum = 6
	maximum = 7817
Slowest flit = 142504
Fragmentation average = 20.4924
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0308507
	minimum = 0.0141667 (at node 32)
	maximum = 0.0416667 (at node 27)
Accepted packet rate average = 0.0100938
	minimum = 0.00716667 (at node 36)
	maximum = 0.0126667 (at node 152)
Injected flit rate average = 0.555158
	minimum = 0.256667 (at node 32)
	maximum = 0.75 (at node 27)
Accepted flit rate average= 0.181634
	minimum = 0.127167 (at node 36)
	maximum = 0.228167 (at node 152)
Injected packet length average = 17.995
Accepted packet length average = 17.9947
Total in-flight flits = 677020 (602522 measured)
latency change    = 0.313014
throughput change = 0.0053431
Class 0:
Packet latency average = 2495.19
	minimum = 23
	maximum = 7686
Network latency average = 2180.82
	minimum = 23
	maximum = 6971
Slowest packet = 20317
Flit latency average = 4101.86
	minimum = 6
	maximum = 8772
Slowest flit = 136061
Fragmentation average = 28.1601
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0306763
	minimum = 0.014 (at node 32)
	maximum = 0.0412857 (at node 27)
Accepted packet rate average = 0.0100878
	minimum = 0.007 (at node 36)
	maximum = 0.0124286 (at node 120)
Injected flit rate average = 0.552062
	minimum = 0.253286 (at node 32)
	maximum = 0.743143 (at node 27)
Accepted flit rate average= 0.181392
	minimum = 0.123714 (at node 36)
	maximum = 0.221857 (at node 138)
Injected packet length average = 17.9963
Accepted packet length average = 17.9813
Total in-flight flits = 744855 (692215 measured)
latency change    = 0.302846
throughput change = 0.00133174
Draining all recorded packets ...
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (799247 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (693887 flits)
Class 0:
Remaining flits: 178056 178057 178058 178059 178060 178061 178062 178063 178064 178065 [...] (837076 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (688537 flits)
Class 0:
Remaining flits: 186048 186049 186050 186051 186052 186053 186054 186055 186056 186057 [...] (857549 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (677112 flits)
Class 0:
Remaining flits: 209322 209323 209324 209325 209326 209327 209328 209329 209330 209331 [...] (876247 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (663118 flits)
Class 0:
Remaining flits: 225194 225195 225196 225197 253004 253005 253006 253007 256410 256411 [...] (893717 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (645900 flits)
Class 0:
Remaining flits: 288198 288199 288200 288201 288202 288203 288204 288205 288206 288207 [...] (911211 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (626971 flits)
Class 0:
Remaining flits: 288198 288199 288200 288201 288202 288203 288204 288205 288206 288207 [...] (927660 flits)
Measured flits: 364212 364213 364214 364215 364216 364217 364218 364219 364220 364221 [...] (606776 flits)
Class 0:
Remaining flits: 297576 297577 297578 297579 297580 297581 297582 297583 297584 297585 [...] (943614 flits)
Measured flits: 364302 364303 364304 364305 364306 364307 364308 364309 364310 364311 [...] (585500 flits)
Class 0:
Remaining flits: 313056 313057 313058 313059 313060 313061 313062 313063 313064 313065 [...] (957827 flits)
Measured flits: 367794 367795 367796 367797 367798 367799 367800 367801 367802 367803 [...] (561514 flits)
Class 0:
Remaining flits: 313056 313057 313058 313059 313060 313061 313062 313063 313064 313065 [...] (969900 flits)
Measured flits: 373086 373087 373088 373089 373090 373091 373092 373093 373094 373095 [...] (536016 flits)
Class 0:
Remaining flits: 349164 349165 349166 349167 349168 349169 349170 349171 349172 349173 [...] (975256 flits)
Measured flits: 373086 373087 373088 373089 373090 373091 373092 373093 373094 373095 [...] (508728 flits)
Class 0:
Remaining flits: 375934 375935 375936 375937 375938 375939 375940 375941 375942 375943 [...] (976789 flits)
Measured flits: 375934 375935 375936 375937 375938 375939 375940 375941 375942 375943 [...] (479948 flits)
Class 0:
Remaining flits: 413406 413407 413408 413409 413410 413411 413412 413413 413414 413415 [...] (977995 flits)
Measured flits: 413406 413407 413408 413409 413410 413411 413412 413413 413414 413415 [...] (450683 flits)
Class 0:
Remaining flits: 457632 457633 457634 457635 457636 457637 457638 457639 457640 457641 [...] (979390 flits)
Measured flits: 457632 457633 457634 457635 457636 457637 457638 457639 457640 457641 [...] (421732 flits)
Class 0:
Remaining flits: 484002 484003 484004 484005 484006 484007 484008 484009 484010 484011 [...] (978528 flits)
Measured flits: 484002 484003 484004 484005 484006 484007 484008 484009 484010 484011 [...] (392703 flits)
Class 0:
Remaining flits: 484002 484003 484004 484005 484006 484007 484008 484009 484010 484011 [...] (973566 flits)
Measured flits: 484002 484003 484004 484005 484006 484007 484008 484009 484010 484011 [...] (363454 flits)
Class 0:
Remaining flits: 508878 508879 508880 508881 508882 508883 508884 508885 508886 508887 [...] (968884 flits)
Measured flits: 508878 508879 508880 508881 508882 508883 508884 508885 508886 508887 [...] (334708 flits)
Class 0:
Remaining flits: 516870 516871 516872 516873 516874 516875 516876 516877 516878 516879 [...] (968250 flits)
Measured flits: 516870 516871 516872 516873 516874 516875 516876 516877 516878 516879 [...] (305106 flits)
Class 0:
Remaining flits: 516870 516871 516872 516873 516874 516875 516876 516877 516878 516879 [...] (965800 flits)
Measured flits: 516870 516871 516872 516873 516874 516875 516876 516877 516878 516879 [...] (275434 flits)
Class 0:
Remaining flits: 516881 516882 516883 516884 516885 516886 516887 537264 537265 537266 [...] (959706 flits)
Measured flits: 516881 516882 516883 516884 516885 516886 516887 537264 537265 537266 [...] (247185 flits)
Class 0:
Remaining flits: 537264 537265 537266 537267 537268 537269 537270 537271 537272 537273 [...] (957620 flits)
Measured flits: 537264 537265 537266 537267 537268 537269 537270 537271 537272 537273 [...] (218839 flits)
Class 0:
Remaining flits: 587988 587989 587990 587991 587992 587993 587994 587995 587996 587997 [...] (955937 flits)
Measured flits: 587988 587989 587990 587991 587992 587993 587994 587995 587996 587997 [...] (191692 flits)
Class 0:
Remaining flits: 614042 614043 614044 614045 614046 614047 614048 614049 614050 614051 [...] (948971 flits)
Measured flits: 614042 614043 614044 614045 614046 614047 614048 614049 614050 614051 [...] (165874 flits)
Class 0:
Remaining flits: 620014 620015 620016 620017 620018 620019 620020 620021 620022 620023 [...] (945784 flits)
Measured flits: 620014 620015 620016 620017 620018 620019 620020 620021 620022 620023 [...] (141460 flits)
Class 0:
Remaining flits: 659232 659233 659234 659235 659236 659237 659238 659239 659240 659241 [...] (941977 flits)
Measured flits: 659232 659233 659234 659235 659236 659237 659238 659239 659240 659241 [...] (118842 flits)
Class 0:
Remaining flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (939033 flits)
Measured flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (97778 flits)
Class 0:
Remaining flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (933823 flits)
Measured flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (79291 flits)
Class 0:
Remaining flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (928369 flits)
Measured flits: 713718 713719 713720 713721 713722 713723 713724 713725 713726 713727 [...] (62720 flits)
Class 0:
Remaining flits: 731646 731647 731648 731649 731650 731651 731652 731653 731654 731655 [...] (925110 flits)
Measured flits: 731646 731647 731648 731649 731650 731651 731652 731653 731654 731655 [...] (49064 flits)
Class 0:
Remaining flits: 742500 742501 742502 742503 742504 742505 742506 742507 742508 742509 [...] (924180 flits)
Measured flits: 742500 742501 742502 742503 742504 742505 742506 742507 742508 742509 [...] (37665 flits)
Class 0:
Remaining flits: 742503 742504 742505 742506 742507 742508 742509 742510 742511 742512 [...] (924295 flits)
Measured flits: 742503 742504 742505 742506 742507 742508 742509 742510 742511 742512 [...] (27806 flits)
Class 0:
Remaining flits: 788832 788833 788834 788835 788836 788837 788838 788839 788840 788841 [...] (921431 flits)
Measured flits: 788832 788833 788834 788835 788836 788837 788838 788839 788840 788841 [...] (20272 flits)
Class 0:
Remaining flits: 833004 833005 833006 833007 833008 833009 833010 833011 833012 833013 [...] (919315 flits)
Measured flits: 833004 833005 833006 833007 833008 833009 833010 833011 833012 833013 [...] (14423 flits)
Class 0:
Remaining flits: 833014 833015 833016 833017 833018 833019 833020 833021 870192 870193 [...] (918435 flits)
Measured flits: 833014 833015 833016 833017 833018 833019 833020 833021 870192 870193 [...] (10169 flits)
Class 0:
Remaining flits: 882090 882091 882092 882093 882094 882095 882096 882097 882098 882099 [...] (916500 flits)
Measured flits: 882090 882091 882092 882093 882094 882095 882096 882097 882098 882099 [...] (6947 flits)
Class 0:
Remaining flits: 923346 923347 923348 923349 923350 923351 923352 923353 923354 923355 [...] (914166 flits)
Measured flits: 923346 923347 923348 923349 923350 923351 923352 923353 923354 923355 [...] (4450 flits)
Class 0:
Remaining flits: 923346 923347 923348 923349 923350 923351 923352 923353 923354 923355 [...] (910735 flits)
Measured flits: 923346 923347 923348 923349 923350 923351 923352 923353 923354 923355 [...] (3115 flits)
Class 0:
Remaining flits: 952614 952615 952616 952617 952618 952619 952620 952621 952622 952623 [...] (910383 flits)
Measured flits: 952614 952615 952616 952617 952618 952619 952620 952621 952622 952623 [...] (1982 flits)
Class 0:
Remaining flits: 952614 952615 952616 952617 952618 952619 952620 952621 952622 952623 [...] (911303 flits)
Measured flits: 952614 952615 952616 952617 952618 952619 952620 952621 952622 952623 [...] (1365 flits)
Class 0:
Remaining flits: 1048302 1048303 1048304 1048305 1048306 1048307 1048308 1048309 1048310 1048311 [...] (912422 flits)
Measured flits: 1048302 1048303 1048304 1048305 1048306 1048307 1048308 1048309 1048310 1048311 [...] (709 flits)
Class 0:
Remaining flits: 1074420 1074421 1074422 1074423 1074424 1074425 1074426 1074427 1074428 1074429 [...] (912022 flits)
Measured flits: 1074420 1074421 1074422 1074423 1074424 1074425 1074426 1074427 1074428 1074429 [...] (389 flits)
Class 0:
Remaining flits: 1080378 1080379 1080380 1080381 1080382 1080383 1080384 1080385 1080386 1080387 [...] (909644 flits)
Measured flits: 1080378 1080379 1080380 1080381 1080382 1080383 1080384 1080385 1080386 1080387 [...] (195 flits)
Class 0:
Remaining flits: 1080378 1080379 1080380 1080381 1080382 1080383 1080384 1080385 1080386 1080387 [...] (904767 flits)
Measured flits: 1080378 1080379 1080380 1080381 1080382 1080383 1080384 1080385 1080386 1080387 [...] (163 flits)
Class 0:
Remaining flits: 1107666 1107667 1107668 1107669 1107670 1107671 1107672 1107673 1107674 1107675 [...] (902235 flits)
Measured flits: 1107666 1107667 1107668 1107669 1107670 1107671 1107672 1107673 1107674 1107675 [...] (72 flits)
Class 0:
Remaining flits: 1107666 1107667 1107668 1107669 1107670 1107671 1107672 1107673 1107674 1107675 [...] (899780 flits)
Measured flits: 1107666 1107667 1107668 1107669 1107670 1107671 1107672 1107673 1107674 1107675 [...] (23 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1148129 1205892 1205893 1205894 1205895 1205896 1205897 1205898 1205899 1205900 [...] (867613 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1205892 1205893 1205894 1205895 1205896 1205897 1205898 1205899 1205900 1205901 [...] (836055 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1212552 1212553 1212554 1212555 1212556 1212557 1212558 1212559 1212560 1212561 [...] (805296 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1212552 1212553 1212554 1212555 1212556 1212557 1212558 1212559 1212560 1212561 [...] (774896 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1227348 1227349 1227350 1227351 1227352 1227353 1227354 1227355 1227356 1227357 [...] (743887 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1315674 1315675 1315676 1315677 1315678 1315679 1315680 1315681 1315682 1315683 [...] (712525 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1315674 1315675 1315676 1315677 1315678 1315679 1315680 1315681 1315682 1315683 [...] (681687 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1315674 1315675 1315676 1315677 1315678 1315679 1315680 1315681 1315682 1315683 [...] (651119 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1385496 1385497 1385498 1385499 1385500 1385501 1385502 1385503 1385504 1385505 [...] (620010 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1420542 1420543 1420544 1420545 1420546 1420547 1420548 1420549 1420550 1420551 [...] (588050 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1439838 1439839 1439840 1439841 1439842 1439843 1439844 1439845 1439846 1439847 [...] (557359 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1439838 1439839 1439840 1439841 1439842 1439843 1439844 1439845 1439846 1439847 [...] (525938 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1499940 1499941 1499942 1499943 1499944 1499945 1499946 1499947 1499948 1499949 [...] (494669 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1503792 1503793 1503794 1503795 1503796 1503797 1503798 1503799 1503800 1503801 [...] (464301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1548918 1548919 1548920 1548921 1548922 1548923 1548924 1548925 1548926 1548927 [...] (433862 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1548918 1548919 1548920 1548921 1548922 1548923 1548924 1548925 1548926 1548927 [...] (403720 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559934 1559935 1559936 1559937 1559938 1559939 1559940 1559941 1559942 1559943 [...] (374068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559934 1559935 1559936 1559937 1559938 1559939 1559940 1559941 1559942 1559943 [...] (344131 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1632222 1632223 1632224 1632225 1632226 1632227 1632228 1632229 1632230 1632231 [...] (314066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651698 1651699 1651700 1651701 1651702 1651703 1651704 1651705 1651706 1651707 [...] (284129 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651698 1651699 1651700 1651701 1651702 1651703 1651704 1651705 1651706 1651707 [...] (254422 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1691954 1691955 1691956 1691957 1691958 1691959 1691960 1691961 1691962 1691963 [...] (224777 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702710 1702711 1702712 1702713 1702714 1702715 1702716 1702717 1702718 1702719 [...] (195380 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1717146 1717147 1717148 1717149 1717150 1717151 1717152 1717153 1717154 1717155 [...] (166504 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1833714 1833715 1833716 1833717 1833718 1833719 1833720 1833721 1833722 1833723 [...] (138248 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1843398 1843399 1843400 1843401 1843402 1843403 1843404 1843405 1843406 1843407 [...] (109776 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1843398 1843399 1843400 1843401 1843402 1843403 1843404 1843405 1843406 1843407 [...] (82764 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1843398 1843399 1843400 1843401 1843402 1843403 1843404 1843405 1843406 1843407 [...] (56524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1848258 1848259 1848260 1848261 1848262 1848263 1848264 1848265 1848266 1848267 [...] (32777 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1866622 1866623 1866624 1866625 1866626 1866627 1866628 1866629 1866630 1866631 [...] (12292 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2436905 2436906 2436907 2436908 2436909 2436910 2436911 2480130 2480131 2480132 [...] (1752 flits)
Measured flits: (0 flits)
Time taken is 87410 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 17515.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 45474 (1 samples)
Network latency average = 16313.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 45406 (1 samples)
Flit latency average = 22558.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 59808 (1 samples)
Fragmentation average = 71.3956 (1 samples)
	minimum = 0 (1 samples)
	maximum = 178 (1 samples)
Injected packet rate average = 0.0306763 (1 samples)
	minimum = 0.014 (1 samples)
	maximum = 0.0412857 (1 samples)
Accepted packet rate average = 0.0100878 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0124286 (1 samples)
Injected flit rate average = 0.552062 (1 samples)
	minimum = 0.253286 (1 samples)
	maximum = 0.743143 (1 samples)
Accepted flit rate average = 0.181392 (1 samples)
	minimum = 0.123714 (1 samples)
	maximum = 0.221857 (1 samples)
Injected packet size average = 17.9963 (1 samples)
Accepted packet size average = 17.9813 (1 samples)
Hops average = 5.06825 (1 samples)
Total run time 68.3288
