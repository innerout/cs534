BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.548
	minimum = 22
	maximum = 982
Network latency average = 235.255
	minimum = 22
	maximum = 849
Slowest packet = 46
Flit latency average = 205.837
	minimum = 5
	maximum = 877
Slowest flit = 9662
Fragmentation average = 47.4145
	minimum = 0
	maximum = 308
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139479
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.26375
	minimum = 0.118 (at node 150)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8285
Accepted packet length average = 18.9096
Total in-flight flits = 52194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 615.81
	minimum = 22
	maximum = 1951
Network latency average = 446.831
	minimum = 22
	maximum = 1652
Slowest packet = 46
Flit latency average = 415.099
	minimum = 5
	maximum = 1740
Slowest flit = 22884
Fragmentation average = 68.4541
	minimum = 0
	maximum = 401
Injected packet rate average = 0.0313932
	minimum = 0.002 (at node 160)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0147969
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.562247
	minimum = 0.036 (at node 160)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.275737
	minimum = 0.1575 (at node 116)
	maximum = 0.398 (at node 63)
Injected packet length average = 17.9098
Accepted packet length average = 18.6348
Total in-flight flits = 111107 (0 measured)
latency change    = 0.429129
throughput change = 0.0434725
Class 0:
Packet latency average = 1371.3
	minimum = 25
	maximum = 2853
Network latency average = 1104.38
	minimum = 22
	maximum = 2431
Slowest packet = 2780
Flit latency average = 1066.52
	minimum = 5
	maximum = 2417
Slowest flit = 54500
Fragmentation average = 106.562
	minimum = 0
	maximum = 443
Injected packet rate average = 0.0344115
	minimum = 0 (at node 8)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.0162552
	minimum = 0.008 (at node 52)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.619802
	minimum = 0 (at node 8)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.291917
	minimum = 0.142 (at node 36)
	maximum = 0.487 (at node 92)
Injected packet length average = 18.0115
Accepted packet length average = 17.9583
Total in-flight flits = 174021 (0 measured)
latency change    = 0.55093
throughput change = 0.0554257
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 323.924
	minimum = 26
	maximum = 1525
Network latency average = 45.2695
	minimum = 22
	maximum = 792
Slowest packet = 18695
Flit latency average = 1543.12
	minimum = 5
	maximum = 3071
Slowest flit = 92241
Fragmentation average = 6.50334
	minimum = 0
	maximum = 38
Injected packet rate average = 0.0331667
	minimum = 0.001 (at node 7)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0159063
	minimum = 0.003 (at node 32)
	maximum = 0.027 (at node 33)
Injected flit rate average = 0.596375
	minimum = 0.001 (at node 7)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.289292
	minimum = 0.095 (at node 32)
	maximum = 0.526 (at node 123)
Injected packet length average = 17.9812
Accepted packet length average = 18.1873
Total in-flight flits = 233155 (106551 measured)
latency change    = 3.2334
throughput change = 0.00907389
Class 0:
Packet latency average = 516.797
	minimum = 26
	maximum = 3039
Network latency average = 193.613
	minimum = 22
	maximum = 1980
Slowest packet = 18695
Flit latency average = 1804.61
	minimum = 5
	maximum = 3756
Slowest flit = 115163
Fragmentation average = 9.50223
	minimum = 0
	maximum = 173
Injected packet rate average = 0.0320677
	minimum = 0.0065 (at node 136)
	maximum = 0.0555 (at node 11)
Accepted packet rate average = 0.015888
	minimum = 0.0095 (at node 71)
	maximum = 0.0275 (at node 123)
Injected flit rate average = 0.577284
	minimum = 0.117 (at node 136)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.286393
	minimum = 0.1685 (at node 150)
	maximum = 0.501 (at node 123)
Injected packet length average = 18.002
Accepted packet length average = 18.0257
Total in-flight flits = 285950 (205570 measured)
latency change    = 0.373208
throughput change = 0.0101205
Class 0:
Packet latency average = 1172.14
	minimum = 24
	maximum = 4076
Network latency average = 862.715
	minimum = 22
	maximum = 2960
Slowest packet = 18695
Flit latency average = 2066.06
	minimum = 5
	maximum = 4585
Slowest flit = 126791
Fragmentation average = 27.0655
	minimum = 0
	maximum = 505
Injected packet rate average = 0.0302361
	minimum = 0.00866667 (at node 136)
	maximum = 0.0536667 (at node 75)
Accepted packet rate average = 0.0156927
	minimum = 0.0103333 (at node 71)
	maximum = 0.023 (at node 123)
Injected flit rate average = 0.544292
	minimum = 0.151667 (at node 136)
	maximum = 0.962667 (at node 75)
Accepted flit rate average= 0.282377
	minimum = 0.19 (at node 71)
	maximum = 0.414667 (at node 123)
Injected packet length average = 18.0014
Accepted packet length average = 17.9941
Total in-flight flits = 325238 (281671 measured)
latency change    = 0.559099
throughput change = 0.0142239
Draining remaining packets ...
Class 0:
Remaining flits: 155358 155359 155360 155361 155362 155363 155364 155365 155366 155367 [...] (276263 flits)
Measured flits: 335952 335953 335954 335955 335956 335957 335958 335959 335960 335961 [...] (252623 flits)
Class 0:
Remaining flits: 175085 180864 180865 180866 180867 180868 180869 180870 180871 180872 [...] (227353 flits)
Measured flits: 335952 335953 335954 335955 335956 335957 335958 335959 335960 335961 [...] (215169 flits)
Class 0:
Remaining flits: 183280 183281 183282 183283 183284 183285 183286 183287 183288 183289 [...] (178827 flits)
Measured flits: 336024 336025 336026 336027 336028 336029 336030 336031 336032 336033 [...] (172836 flits)
Class 0:
Remaining flits: 223610 223611 223612 223613 239128 239129 240746 240747 240748 240749 [...] (131110 flits)
Measured flits: 336029 336030 336031 336032 336033 336034 336035 336036 336037 336038 [...] (128597 flits)
Class 0:
Remaining flits: 257094 257095 257096 257097 257098 257099 257100 257101 257102 257103 [...] (85226 flits)
Measured flits: 336582 336583 336584 336585 336586 336587 336588 336589 336590 336591 [...] (84409 flits)
Class 0:
Remaining flits: 309708 309709 309710 309711 309712 309713 309714 309715 309716 309717 [...] (43758 flits)
Measured flits: 336582 336583 336584 336585 336586 336587 336588 336589 336590 336591 [...] (43523 flits)
Class 0:
Remaining flits: 326826 326827 326828 326829 326830 326831 326832 326833 326834 326835 [...] (11768 flits)
Measured flits: 359046 359047 359048 359049 359050 359051 359052 359053 359054 359055 [...] (11750 flits)
Class 0:
Remaining flits: 501894 501895 501896 501897 501898 501899 501900 501901 501902 501903 [...] (234 flits)
Measured flits: 501894 501895 501896 501897 501898 501899 501900 501901 501902 501903 [...] (234 flits)
Time taken is 14261 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5249.37 (1 samples)
	minimum = 24 (1 samples)
	maximum = 11097 (1 samples)
Network latency average = 4834.78 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10606 (1 samples)
Flit latency average = 4147.22 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10589 (1 samples)
Fragmentation average = 58.1005 (1 samples)
	minimum = 0 (1 samples)
	maximum = 629 (1 samples)
Injected packet rate average = 0.0302361 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0536667 (1 samples)
Accepted packet rate average = 0.0156927 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.023 (1 samples)
Injected flit rate average = 0.544292 (1 samples)
	minimum = 0.151667 (1 samples)
	maximum = 0.962667 (1 samples)
Accepted flit rate average = 0.282377 (1 samples)
	minimum = 0.19 (1 samples)
	maximum = 0.414667 (1 samples)
Injected packet size average = 18.0014 (1 samples)
Accepted packet size average = 17.9941 (1 samples)
Hops average = 5.14974 (1 samples)
Total run time 14.2202
