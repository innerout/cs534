BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.855
	minimum = 23
	maximum = 943
Network latency average = 312.632
	minimum = 23
	maximum = 918
Slowest packet = 303
Flit latency average = 238.344
	minimum = 6
	maximum = 931
Slowest flit = 5674
Fragmentation average = 190.305
	minimum = 0
	maximum = 828
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0109687
	minimum = 0.003 (at node 112)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.235573
	minimum = 0.085 (at node 112)
	maximum = 0.371 (at node 22)
Injected packet length average = 17.8489
Accepted packet length average = 21.4767
Total in-flight flits = 90058 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 576.52
	minimum = 23
	maximum = 1904
Network latency average = 558.468
	minimum = 23
	maximum = 1884
Slowest packet = 600
Flit latency average = 470.952
	minimum = 6
	maximum = 1947
Slowest flit = 4901
Fragmentation average = 252.397
	minimum = 0
	maximum = 1679
Injected packet rate average = 0.0394036
	minimum = 0.0295 (at node 88)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0123151
	minimum = 0.0065 (at node 164)
	maximum = 0.0215 (at node 44)
Injected flit rate average = 0.706089
	minimum = 0.531 (at node 88)
	maximum = 0.93 (at node 62)
Accepted flit rate average= 0.247096
	minimum = 0.1455 (at node 30)
	maximum = 0.4125 (at node 44)
Injected packet length average = 17.9194
Accepted packet length average = 20.0645
Total in-flight flits = 177473 (0 measured)
latency change    = 0.433055
throughput change = 0.0466354
Class 0:
Packet latency average = 1324.81
	minimum = 27
	maximum = 2823
Network latency average = 1299.61
	minimum = 27
	maximum = 2782
Slowest packet = 883
Flit latency average = 1228.38
	minimum = 6
	maximum = 2861
Slowest flit = 14398
Fragmentation average = 380.362
	minimum = 0
	maximum = 2029
Injected packet rate average = 0.0345312
	minimum = 0.006 (at node 52)
	maximum = 0.052 (at node 22)
Accepted packet rate average = 0.0136823
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 14)
Injected flit rate average = 0.621214
	minimum = 0.12 (at node 52)
	maximum = 0.934 (at node 22)
Accepted flit rate average= 0.249443
	minimum = 0.121 (at node 4)
	maximum = 0.414 (at node 14)
Injected packet length average = 17.9899
Accepted packet length average = 18.2311
Total in-flight flits = 248920 (0 measured)
latency change    = 0.564828
throughput change = 0.00940639
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 245.39
	minimum = 30
	maximum = 1507
Network latency average = 107.743
	minimum = 29
	maximum = 913
Slowest packet = 21770
Flit latency average = 1893.39
	minimum = 6
	maximum = 3844
Slowest flit = 18074
Fragmentation average = 15.1994
	minimum = 3
	maximum = 181
Injected packet rate average = 0.0335417
	minimum = 0.005 (at node 180)
	maximum = 0.053 (at node 77)
Accepted packet rate average = 0.0133906
	minimum = 0.005 (at node 18)
	maximum = 0.025 (at node 115)
Injected flit rate average = 0.604411
	minimum = 0.098 (at node 180)
	maximum = 0.952 (at node 155)
Accepted flit rate average= 0.240708
	minimum = 0.116 (at node 46)
	maximum = 0.445 (at node 115)
Injected packet length average = 18.0197
Accepted packet length average = 17.9759
Total in-flight flits = 318624 (109841 measured)
latency change    = 4.3988
throughput change = 0.0362861
Class 0:
Packet latency average = 476.122
	minimum = 30
	maximum = 2516
Network latency average = 264.372
	minimum = 29
	maximum = 1863
Slowest packet = 21770
Flit latency average = 2241.39
	minimum = 6
	maximum = 4853
Slowest flit = 15525
Fragmentation average = 25.0453
	minimum = 1
	maximum = 560
Injected packet rate average = 0.0335781
	minimum = 0.0095 (at node 180)
	maximum = 0.052 (at node 77)
Accepted packet rate average = 0.0131458
	minimum = 0.0055 (at node 64)
	maximum = 0.0195 (at node 115)
Injected flit rate average = 0.60501
	minimum = 0.1745 (at node 180)
	maximum = 0.9365 (at node 77)
Accepted flit rate average= 0.236932
	minimum = 0.124 (at node 148)
	maximum = 0.357 (at node 115)
Injected packet length average = 18.018
Accepted packet length average = 18.0234
Total in-flight flits = 390030 (219920 measured)
latency change    = 0.484608
throughput change = 0.0159372
Class 0:
Packet latency average = 822.119
	minimum = 30
	maximum = 3865
Network latency average = 513.439
	minimum = 29
	maximum = 2940
Slowest packet = 21770
Flit latency average = 2585.46
	minimum = 6
	maximum = 5721
Slowest flit = 17783
Fragmentation average = 49.0756
	minimum = 1
	maximum = 1139
Injected packet rate average = 0.0333264
	minimum = 0.011 (at node 32)
	maximum = 0.049 (at node 155)
Accepted packet rate average = 0.0130799
	minimum = 0.007 (at node 64)
	maximum = 0.018 (at node 99)
Injected flit rate average = 0.59978
	minimum = 0.199667 (at node 108)
	maximum = 0.881667 (at node 155)
Accepted flit rate average= 0.235491
	minimum = 0.13 (at node 64)
	maximum = 0.317333 (at node 147)
Injected packet length average = 17.9971
Accepted packet length average = 18.0041
Total in-flight flits = 458823 (325610 measured)
latency change    = 0.42086
throughput change = 0.006119
Class 0:
Packet latency average = 1402.96
	minimum = 30
	maximum = 4902
Network latency average = 951.833
	minimum = 28
	maximum = 3956
Slowest packet = 21770
Flit latency average = 2924.95
	minimum = 6
	maximum = 6591
Slowest flit = 8513
Fragmentation average = 98.2941
	minimum = 1
	maximum = 2394
Injected packet rate average = 0.0319883
	minimum = 0.01075 (at node 32)
	maximum = 0.0435 (at node 25)
Accepted packet rate average = 0.0129414
	minimum = 0.00825 (at node 64)
	maximum = 0.0175 (at node 3)
Injected flit rate average = 0.575884
	minimum = 0.19625 (at node 32)
	maximum = 0.7805 (at node 115)
Accepted flit rate average= 0.233012
	minimum = 0.14375 (at node 36)
	maximum = 0.31975 (at node 3)
Injected packet length average = 18.003
Accepted packet length average = 18.0051
Total in-flight flits = 512335 (412889 measured)
latency change    = 0.41401
throughput change = 0.0106415
Class 0:
Packet latency average = 2124.36
	minimum = 30
	maximum = 5933
Network latency average = 1501.24
	minimum = 28
	maximum = 4948
Slowest packet = 21770
Flit latency average = 3233.73
	minimum = 6
	maximum = 7811
Slowest flit = 12446
Fragmentation average = 149.369
	minimum = 1
	maximum = 2681
Injected packet rate average = 0.0297198
	minimum = 0.0116 (at node 32)
	maximum = 0.0398 (at node 30)
Accepted packet rate average = 0.012875
	minimum = 0.0082 (at node 4)
	maximum = 0.0164 (at node 3)
Injected flit rate average = 0.534857
	minimum = 0.2106 (at node 32)
	maximum = 0.7156 (at node 103)
Accepted flit rate average= 0.230972
	minimum = 0.152 (at node 36)
	maximum = 0.292 (at node 157)
Injected packet length average = 17.9967
Accepted packet length average = 17.9396
Total in-flight flits = 540907 (470733 measured)
latency change    = 0.339585
throughput change = 0.00883157
Class 0:
Packet latency average = 3026.77
	minimum = 30
	maximum = 6798
Network latency average = 2318.88
	minimum = 28
	maximum = 5949
Slowest packet = 21770
Flit latency average = 3516.33
	minimum = 6
	maximum = 8662
Slowest flit = 41430
Fragmentation average = 198.206
	minimum = 1
	maximum = 2681
Injected packet rate average = 0.0272995
	minimum = 0.0123333 (at node 108)
	maximum = 0.0363333 (at node 103)
Accepted packet rate average = 0.0127292
	minimum = 0.00916667 (at node 4)
	maximum = 0.0161667 (at node 157)
Injected flit rate average = 0.491489
	minimum = 0.221667 (at node 108)
	maximum = 0.653 (at node 126)
Accepted flit rate average= 0.228656
	minimum = 0.160333 (at node 7)
	maximum = 0.295667 (at node 157)
Injected packet length average = 18.0036
Accepted packet length average = 17.9632
Total in-flight flits = 551752 (500812 measured)
latency change    = 0.298142
throughput change = 0.0101271
Class 0:
Packet latency average = 3884.9
	minimum = 30
	maximum = 7714
Network latency average = 3148.5
	minimum = 28
	maximum = 6977
Slowest packet = 21770
Flit latency average = 3792.27
	minimum = 6
	maximum = 9758
Slowest flit = 12450
Fragmentation average = 239.404
	minimum = 1
	maximum = 5319
Injected packet rate average = 0.025532
	minimum = 0.0121429 (at node 16)
	maximum = 0.0332857 (at node 103)
Accepted packet rate average = 0.0126577
	minimum = 0.00914286 (at node 190)
	maximum = 0.0158571 (at node 157)
Injected flit rate average = 0.459687
	minimum = 0.220429 (at node 16)
	maximum = 0.600143 (at node 117)
Accepted flit rate average= 0.226579
	minimum = 0.165857 (at node 190)
	maximum = 0.285571 (at node 157)
Injected packet length average = 18.0043
Accepted packet length average = 17.9004
Total in-flight flits = 562212 (523503 measured)
latency change    = 0.220889
throughput change = 0.00916847
Draining all recorded packets ...
Class 0:
Remaining flits: 37839 37840 37841 37842 37843 37844 37845 37846 37847 37848 [...] (574980 flits)
Measured flits: 391733 391752 391753 391754 391755 391756 391757 391758 391759 391760 [...] (543969 flits)
Class 0:
Remaining flits: 37850 37851 37852 37853 44604 44605 44606 44607 44608 44609 [...] (589061 flits)
Measured flits: 391752 391753 391754 391755 391756 391757 391758 391759 391760 391761 [...] (563463 flits)
Class 0:
Remaining flits: 44604 44605 44606 44607 44608 44609 44610 44611 44612 44613 [...] (598622 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (576027 flits)
Class 0:
Remaining flits: 44604 44605 44606 44607 44608 44609 44610 44611 44612 44613 [...] (611561 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (585580 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (625817 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (586515 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (641515 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (575152 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (656838 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (554295 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (674198 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (534553 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (690274 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (511721 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (703966 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (486553 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (718417 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (462558 flits)
Class 0:
Remaining flits: 45108 45109 45110 45111 45112 45113 45114 45115 45116 45117 [...] (729615 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (438669 flits)
Class 0:
Remaining flits: 101232 101233 101234 101235 101236 101237 101238 101239 101240 101241 [...] (734597 flits)
Measured flits: 391770 391771 391772 391773 391774 391775 391776 391777 391778 391779 [...] (417115 flits)
Class 0:
Remaining flits: 101232 101233 101234 101235 101236 101237 101238 101239 101240 101241 [...] (737281 flits)
Measured flits: 392819 392820 392821 392822 392823 392824 392825 392826 392827 392828 [...] (394090 flits)
Class 0:
Remaining flits: 101232 101233 101234 101235 101236 101237 101238 101239 101240 101241 [...] (736270 flits)
Measured flits: 392819 392820 392821 392822 392823 392824 392825 392826 392827 392828 [...] (370128 flits)
Class 0:
Remaining flits: 154368 154369 154370 154371 154372 154373 154374 154375 154376 154377 [...] (736390 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (344633 flits)
Class 0:
Remaining flits: 154368 154369 154370 154371 154372 154373 154374 154375 154376 154377 [...] (734761 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (318669 flits)
Class 0:
Remaining flits: 204210 204211 204212 204213 204214 204215 204216 204217 204218 204219 [...] (735333 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (296084 flits)
Class 0:
Remaining flits: 204210 204211 204212 204213 204214 204215 204216 204217 204218 204219 [...] (732264 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (274144 flits)
Class 0:
Remaining flits: 219556 219557 219558 219559 219560 219561 219562 219563 239796 239797 [...] (730532 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (251009 flits)
Class 0:
Remaining flits: 219556 219557 219558 219559 219560 219561 219562 219563 239796 239797 [...] (727118 flits)
Measured flits: 395622 395623 395624 395625 395626 395627 395628 395629 395630 395631 [...] (228599 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (724216 flits)
Measured flits: 395633 395634 395635 395636 395637 395638 395639 397044 397045 397046 [...] (207436 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (723645 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (187511 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (726024 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (170614 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (721547 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (153348 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (719660 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (138884 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (715027 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (125239 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (710824 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (111705 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (703910 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (99024 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (699493 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (87308 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (697152 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (76846 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (702543 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (67911 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (705071 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (60305 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (706716 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (53142 flits)
Class 0:
Remaining flits: 239796 239797 239798 239799 239800 239801 239802 239803 239804 239805 [...] (710361 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (47461 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (713140 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (41037 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (712938 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (35995 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (717766 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (32145 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (720968 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (28170 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (713982 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (25025 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (709535 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (22091 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (706439 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (19445 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (701924 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (16915 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (700923 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (14575 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (697895 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (12790 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (697153 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (10900 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (697489 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (9379 flits)
Class 0:
Remaining flits: 256860 256861 256862 256863 256864 256865 256866 256867 256868 256869 [...] (693351 flits)
Measured flits: 397044 397045 397046 397047 397048 397049 397050 397051 397052 397053 [...] (8337 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (687161 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (7320 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (683577 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (6326 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (680557 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (5485 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (681646 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (4766 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (685083 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (4530 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (689790 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (3986 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (689708 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (3473 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (691288 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (3040 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (691780 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (2721 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (690169 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (2423 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (687833 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (2120 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (690272 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (1893 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (688149 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (1689 flits)
Class 0:
Remaining flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (687880 flits)
Measured flits: 565020 565021 565022 565023 565024 565025 565026 565027 565028 565029 [...] (1410 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (687858 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (1137 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (685124 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (970 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (682707 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (826 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (688735 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (677 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (691212 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (576 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (688199 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (504 flits)
Class 0:
Remaining flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (689801 flits)
Measured flits: 881244 881245 881246 881247 881248 881249 881250 881251 881252 881253 [...] (419 flits)
Class 0:
Remaining flits: 881247 881248 881249 881250 881251 881252 881253 881254 881255 881256 [...] (686570 flits)
Measured flits: 881247 881248 881249 881250 881251 881252 881253 881254 881255 881256 [...] (342 flits)
Class 0:
Remaining flits: 951579 951580 951581 951582 951583 951584 951585 951586 951587 996606 [...] (684650 flits)
Measured flits: 951579 951580 951581 951582 951583 951584 951585 951586 951587 996606 [...] (277 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (680522 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (244 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (676427 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (172 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (672295 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (155 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (669898 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (91 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (677143 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (90 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (677322 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (64 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (677420 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (54 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (672555 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (54 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (667480 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (54 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (667674 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (54 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (666239 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (36 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (663894 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (36 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (661660 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (18 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (662511 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (18 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (659234 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (18 flits)
Class 0:
Remaining flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (658227 flits)
Measured flits: 996606 996607 996608 996609 996610 996611 996612 996613 996614 996615 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1484297 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 [...] (625386 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (591678 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (557747 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (524430 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (492186 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (459186 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (427029 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (394138 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (362059 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (330925 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (301270 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (271526 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1505880 1505881 1505882 1505883 1505884 1505885 1505886 1505887 1505888 1505889 [...] (242088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1508598 1508599 1508600 1508601 1508602 1508603 1508604 1508605 1508606 1508607 [...] (213550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1508598 1508599 1508600 1508601 1508602 1508603 1508604 1508605 1508606 1508607 [...] (185725 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (159221 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (133864 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (109062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (84356 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (61516 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (41232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1664784 1664785 1664786 1664787 1664788 1664789 1664790 1664791 1664792 1664793 [...] (25408 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2355534 2355535 2355536 2355537 2355538 2355539 2355540 2355541 2355542 2355543 [...] (14736 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2446110 2446111 2446112 2446113 2446114 2446115 2446116 2446117 2446118 2446119 [...] (7781 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2465640 2465641 2465642 2465643 2465644 2465645 2465646 2465647 2465648 2465649 [...] (3061 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3341556 3341557 3341558 3341559 3341560 3341561 3341562 3341563 3341564 3341565 [...] (1156 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3583993 3583994 3583995 3583996 3583997 4064322 4064323 4064324 4064325 4064326 [...] (18 flits)
Measured flits: (0 flits)
Time taken is 124123 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 17205.6 (1 samples)
	minimum = 30 (1 samples)
	maximum = 89718 (1 samples)
Network latency average = 14021.4 (1 samples)
	minimum = 25 (1 samples)
	maximum = 87354 (1 samples)
Flit latency average = 17564.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 97192 (1 samples)
Fragmentation average = 246.386 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14929 (1 samples)
Injected packet rate average = 0.025532 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0332857 (1 samples)
Accepted packet rate average = 0.0126577 (1 samples)
	minimum = 0.00914286 (1 samples)
	maximum = 0.0158571 (1 samples)
Injected flit rate average = 0.459687 (1 samples)
	minimum = 0.220429 (1 samples)
	maximum = 0.600143 (1 samples)
Accepted flit rate average = 0.226579 (1 samples)
	minimum = 0.165857 (1 samples)
	maximum = 0.285571 (1 samples)
Injected packet size average = 18.0043 (1 samples)
Accepted packet size average = 17.9004 (1 samples)
Hops average = 5.05575 (1 samples)
Total run time 260.632
