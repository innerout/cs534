BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 339.891
	minimum = 22
	maximum = 973
Network latency average = 229.354
	minimum = 22
	maximum = 735
Slowest packet = 82
Flit latency average = 207.081
	minimum = 5
	maximum = 719
Slowest flit = 23408
Fragmentation average = 18.8167
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0139479
	minimum = 0.006 (at node 41)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.257375
	minimum = 0.109 (at node 41)
	maximum = 0.431 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.4526
Total in-flight flits = 51024 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 601.865
	minimum = 22
	maximum = 1928
Network latency average = 442.397
	minimum = 22
	maximum = 1455
Slowest packet = 82
Flit latency average = 420.185
	minimum = 5
	maximum = 1494
Slowest flit = 45702
Fragmentation average = 20.3051
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0150208
	minimum = 0.008 (at node 83)
	maximum = 0.0215 (at node 22)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.273284
	minimum = 0.146 (at node 83)
	maximum = 0.387 (at node 22)
Injected packet length average = 17.9205
Accepted packet length average = 18.1937
Total in-flight flits = 105803 (0 measured)
latency change    = 0.43527
throughput change = 0.0582137
Class 0:
Packet latency average = 1326.73
	minimum = 22
	maximum = 2771
Network latency average = 1077.81
	minimum = 22
	maximum = 2140
Slowest packet = 4570
Flit latency average = 1057.89
	minimum = 5
	maximum = 2199
Slowest flit = 75526
Fragmentation average = 22.7147
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0320521
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0161719
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.576651
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.29176
	minimum = 0.126 (at node 61)
	maximum = 0.522 (at node 16)
Injected packet length average = 17.9911
Accepted packet length average = 18.0412
Total in-flight flits = 160557 (0 measured)
latency change    = 0.546356
throughput change = 0.0633279
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 369.968
	minimum = 22
	maximum = 1200
Network latency average = 41.483
	minimum = 22
	maximum = 430
Slowest packet = 17864
Flit latency average = 1495.25
	minimum = 5
	maximum = 2862
Slowest flit = 116571
Fragmentation average = 5.20943
	minimum = 0
	maximum = 33
Injected packet rate average = 0.0354375
	minimum = 0 (at node 55)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.016401
	minimum = 0.007 (at node 36)
	maximum = 0.028 (at node 151)
Injected flit rate average = 0.637641
	minimum = 0 (at node 55)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.294964
	minimum = 0.139 (at node 36)
	maximum = 0.517 (at node 151)
Injected packet length average = 17.9934
Accepted packet length average = 17.9844
Total in-flight flits = 226396 (112796 measured)
latency change    = 2.58608
throughput change = 0.0108594
Class 0:
Packet latency average = 439.819
	minimum = 22
	maximum = 2526
Network latency average = 109.101
	minimum = 22
	maximum = 1971
Slowest packet = 18181
Flit latency average = 1708.09
	minimum = 5
	maximum = 3550
Slowest flit = 146825
Fragmentation average = 5.58504
	minimum = 0
	maximum = 65
Injected packet rate average = 0.0344141
	minimum = 0.005 (at node 17)
	maximum = 0.056 (at node 187)
Accepted packet rate average = 0.0164141
	minimum = 0.01 (at node 4)
	maximum = 0.0235 (at node 128)
Injected flit rate average = 0.619701
	minimum = 0.094 (at node 17)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.295521
	minimum = 0.176 (at node 4)
	maximum = 0.423 (at node 128)
Injected packet length average = 18.0072
Accepted packet length average = 18.0041
Total in-flight flits = 284947 (217508 measured)
latency change    = 0.158818
throughput change = 0.00188579
Class 0:
Packet latency average = 1016.02
	minimum = 22
	maximum = 3600
Network latency average = 714.311
	minimum = 22
	maximum = 2982
Slowest packet = 18181
Flit latency average = 1931.71
	minimum = 5
	maximum = 4293
Slowest flit = 178412
Fragmentation average = 10.3551
	minimum = 0
	maximum = 82
Injected packet rate average = 0.0337708
	minimum = 0.0113333 (at node 20)
	maximum = 0.0556667 (at node 6)
Accepted packet rate average = 0.0163906
	minimum = 0.0106667 (at node 52)
	maximum = 0.022 (at node 95)
Injected flit rate average = 0.608189
	minimum = 0.204 (at node 20)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.295036
	minimum = 0.193667 (at node 52)
	maximum = 0.393333 (at node 95)
Injected packet length average = 18.0093
Accepted packet length average = 18.0003
Total in-flight flits = 340752 (309268 measured)
latency change    = 0.567116
throughput change = 0.00164175
Class 0:
Packet latency average = 1783.63
	minimum = 22
	maximum = 4861
Network latency average = 1512.65
	minimum = 22
	maximum = 3982
Slowest packet = 18181
Flit latency average = 2160.38
	minimum = 5
	maximum = 5023
Slowest flit = 201113
Fragmentation average = 15.3641
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0330586
	minimum = 0.01325 (at node 153)
	maximum = 0.0555 (at node 142)
Accepted packet rate average = 0.0163737
	minimum = 0.0115 (at node 86)
	maximum = 0.0215 (at node 95)
Injected flit rate average = 0.595061
	minimum = 0.241 (at node 153)
	maximum = 1 (at node 142)
Accepted flit rate average= 0.294525
	minimum = 0.21075 (at node 86)
	maximum = 0.3895 (at node 150)
Injected packet length average = 18.0002
Accepted packet length average = 17.9877
Total in-flight flits = 391364 (378643 measured)
latency change    = 0.430364
throughput change = 0.00173744
Class 0:
Packet latency average = 2395.02
	minimum = 22
	maximum = 6013
Network latency average = 2118.58
	minimum = 22
	maximum = 4942
Slowest packet = 18181
Flit latency average = 2393.61
	minimum = 5
	maximum = 5726
Slowest flit = 235169
Fragmentation average = 18.4827
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0328229
	minimum = 0.0124 (at node 155)
	maximum = 0.0556 (at node 142)
Accepted packet rate average = 0.0163448
	minimum = 0.012 (at node 80)
	maximum = 0.0216 (at node 103)
Injected flit rate average = 0.590814
	minimum = 0.226 (at node 155)
	maximum = 1 (at node 142)
Accepted flit rate average= 0.294252
	minimum = 0.2154 (at node 80)
	maximum = 0.3874 (at node 103)
Injected packet length average = 18
Accepted packet length average = 18.0028
Total in-flight flits = 445255 (440794 measured)
latency change    = 0.255276
throughput change = 0.000926608
Class 0:
Packet latency average = 2879.08
	minimum = 22
	maximum = 6963
Network latency average = 2588.04
	minimum = 22
	maximum = 5932
Slowest packet = 18181
Flit latency average = 2625.35
	minimum = 5
	maximum = 6529
Slowest flit = 260913
Fragmentation average = 19.9181
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0329861
	minimum = 0.0126667 (at node 64)
	maximum = 0.0541667 (at node 157)
Accepted packet rate average = 0.0163724
	minimum = 0.0121667 (at node 171)
	maximum = 0.0208333 (at node 103)
Injected flit rate average = 0.593795
	minimum = 0.2295 (at node 64)
	maximum = 0.977 (at node 157)
Accepted flit rate average= 0.294629
	minimum = 0.218 (at node 171)
	maximum = 0.376833 (at node 103)
Injected packet length average = 18.0014
Accepted packet length average = 17.9955
Total in-flight flits = 505144 (504007 measured)
latency change    = 0.168129
throughput change = 0.00128045
Class 0:
Packet latency average = 3274.96
	minimum = 22
	maximum = 8170
Network latency average = 2971.69
	minimum = 22
	maximum = 6883
Slowest packet = 18181
Flit latency average = 2854.96
	minimum = 5
	maximum = 7238
Slowest flit = 286217
Fragmentation average = 21.0724
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0331607
	minimum = 0.0128571 (at node 64)
	maximum = 0.0508571 (at node 157)
Accepted packet rate average = 0.0163943
	minimum = 0.0128571 (at node 67)
	maximum = 0.0207143 (at node 103)
Injected flit rate average = 0.596783
	minimum = 0.232571 (at node 64)
	maximum = 0.914857 (at node 157)
Accepted flit rate average= 0.295053
	minimum = 0.231429 (at node 67)
	maximum = 0.372286 (at node 103)
Injected packet length average = 17.9967
Accepted packet length average = 17.9972
Total in-flight flits = 566230 (566068 measured)
latency change    = 0.120881
throughput change = 0.00143529
Draining all recorded packets ...
Class 0:
Remaining flits: 338238 338239 338240 338241 338242 338243 338244 338245 338246 338247 [...] (629313 flits)
Measured flits: 338238 338239 338240 338241 338242 338243 338244 338245 338246 338247 [...] (559303 flits)
Class 0:
Remaining flits: 358344 358345 358346 358347 358348 358349 358350 358351 358352 358353 [...] (684648 flits)
Measured flits: 358344 358345 358346 358347 358348 358349 358350 358351 358352 358353 [...] (516559 flits)
Class 0:
Remaining flits: 396720 396721 396722 396723 396724 396725 396726 396727 396728 396729 [...] (737758 flits)
Measured flits: 396720 396721 396722 396723 396724 396725 396726 396727 396728 396729 [...] (470497 flits)
Class 0:
Remaining flits: 430272 430273 430274 430275 430276 430277 430278 430279 430280 430281 [...] (789691 flits)
Measured flits: 430272 430273 430274 430275 430276 430277 430278 430279 430280 430281 [...] (423614 flits)
Class 0:
Remaining flits: 464544 464545 464546 464547 464548 464549 464550 464551 464552 464553 [...] (835547 flits)
Measured flits: 464544 464545 464546 464547 464548 464549 464550 464551 464552 464553 [...] (377384 flits)
Class 0:
Remaining flits: 505548 505549 505550 505551 505552 505553 505554 505555 505556 505557 [...] (873557 flits)
Measured flits: 505548 505549 505550 505551 505552 505553 505554 505555 505556 505557 [...] (330000 flits)
Class 0:
Remaining flits: 552078 552079 552080 552081 552082 552083 552084 552085 552086 552087 [...] (911610 flits)
Measured flits: 552078 552079 552080 552081 552082 552083 552084 552085 552086 552087 [...] (282993 flits)
Class 0:
Remaining flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (939881 flits)
Measured flits: 561708 561709 561710 561711 561712 561713 561714 561715 561716 561717 [...] (236616 flits)
Class 0:
Remaining flits: 634068 634069 634070 634071 634072 634073 634074 634075 634076 634077 [...] (966501 flits)
Measured flits: 634068 634069 634070 634071 634072 634073 634074 634075 634076 634077 [...] (192064 flits)
Class 0:
Remaining flits: 668695 668696 668697 668698 668699 672300 672301 672302 672303 672304 [...] (993385 flits)
Measured flits: 668695 668696 668697 668698 668699 672300 672301 672302 672303 672304 [...] (152209 flits)
Class 0:
Remaining flits: 700578 700579 700580 700581 700582 700583 700584 700585 700586 700587 [...] (1012121 flits)
Measured flits: 700578 700579 700580 700581 700582 700583 700584 700585 700586 700587 [...] (117451 flits)
Class 0:
Remaining flits: 716490 716491 716492 716493 716494 716495 716496 716497 716498 716499 [...] (1028501 flits)
Measured flits: 716490 716491 716492 716493 716494 716495 716496 716497 716498 716499 [...] (88294 flits)
Class 0:
Remaining flits: 716490 716491 716492 716493 716494 716495 716496 716497 716498 716499 [...] (1043247 flits)
Measured flits: 716490 716491 716492 716493 716494 716495 716496 716497 716498 716499 [...] (64793 flits)
Class 0:
Remaining flits: 805302 805303 805304 805305 805306 805307 805308 805309 805310 805311 [...] (1052564 flits)
Measured flits: 805302 805303 805304 805305 805306 805307 805308 805309 805310 805311 [...] (46958 flits)
Class 0:
Remaining flits: 839286 839287 839288 839289 839290 839291 839292 839293 839294 839295 [...] (1057503 flits)
Measured flits: 839286 839287 839288 839289 839290 839291 839292 839293 839294 839295 [...] (31446 flits)
Class 0:
Remaining flits: 875916 875917 875918 875919 875920 875921 875922 875923 875924 875925 [...] (1065811 flits)
Measured flits: 875916 875917 875918 875919 875920 875921 875922 875923 875924 875925 [...] (19896 flits)
Class 0:
Remaining flits: 893898 893899 893900 893901 893902 893903 893904 893905 893906 893907 [...] (1067948 flits)
Measured flits: 893898 893899 893900 893901 893902 893903 893904 893905 893906 893907 [...] (12137 flits)
Class 0:
Remaining flits: 906264 906265 906266 906267 906268 906269 906270 906271 906272 906273 [...] (1069492 flits)
Measured flits: 906264 906265 906266 906267 906268 906269 906270 906271 906272 906273 [...] (7346 flits)
Class 0:
Remaining flits: 959218 959219 965934 965935 965936 965937 965938 965939 965940 965941 [...] (1072753 flits)
Measured flits: 959218 959219 965934 965935 965936 965937 965938 965939 965940 965941 [...] (4482 flits)
Class 0:
Remaining flits: 1018764 1018765 1018766 1018767 1018768 1018769 1018770 1018771 1018772 1018773 [...] (1074950 flits)
Measured flits: 1018764 1018765 1018766 1018767 1018768 1018769 1018770 1018771 1018772 1018773 [...] (2360 flits)
Class 0:
Remaining flits: 1036656 1036657 1036658 1036659 1036660 1036661 1036662 1036663 1036664 1036665 [...] (1078116 flits)
Measured flits: 1036656 1036657 1036658 1036659 1036660 1036661 1036662 1036663 1036664 1036665 [...] (1420 flits)
Class 0:
Remaining flits: 1076868 1076869 1076870 1076871 1076872 1076873 1076874 1076875 1076876 1076877 [...] (1077937 flits)
Measured flits: 1076868 1076869 1076870 1076871 1076872 1076873 1076874 1076875 1076876 1076877 [...] (936 flits)
Class 0:
Remaining flits: 1090448 1090449 1090450 1090451 1090452 1090453 1090454 1090455 1090456 1090457 [...] (1079363 flits)
Measured flits: 1090448 1090449 1090450 1090451 1090452 1090453 1090454 1090455 1090456 1090457 [...] (640 flits)
Class 0:
Remaining flits: 1136610 1136611 1136612 1136613 1136614 1136615 1136616 1136617 1136618 1136619 [...] (1075583 flits)
Measured flits: 1136610 1136611 1136612 1136613 1136614 1136615 1136616 1136617 1136618 1136619 [...] (360 flits)
Class 0:
Remaining flits: 1160352 1160353 1160354 1160355 1160356 1160357 1160358 1160359 1160360 1160361 [...] (1077411 flits)
Measured flits: 1160352 1160353 1160354 1160355 1160356 1160357 1160358 1160359 1160360 1160361 [...] (90 flits)
Class 0:
Remaining flits: 1179054 1179055 1179056 1179057 1179058 1179059 1179060 1179061 1179062 1179063 [...] (1078187 flits)
Measured flits: 1179054 1179055 1179056 1179057 1179058 1179059 1179060 1179061 1179062 1179063 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1289790 1289791 1289792 1289793 1289794 1289795 1289796 1289797 1289798 1289799 [...] (1027460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1453896 1453897 1453898 1453899 1453900 1453901 1453902 1453903 1453904 1453905 [...] (976470 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1470345 1470346 1470347 1478718 1478719 1478720 1478721 1478722 1478723 1478724 [...] (926126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1501560 1501561 1501562 1501563 1501564 1501565 1501566 1501567 1501568 1501569 [...] (875554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1518462 1518463 1518464 1518465 1518466 1518467 1518468 1518469 1518470 1518471 [...] (825503 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1562022 1562023 1562024 1562025 1562026 1562027 1562028 1562029 1562030 1562031 [...] (774574 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702332 1702333 1702334 1702335 1702336 1702337 1702338 1702339 1702340 1702341 [...] (724327 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1731114 1731115 1731116 1731117 1731118 1731119 1731120 1731121 1731122 1731123 [...] (673658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1745442 1745443 1745444 1745445 1745446 1745447 1745448 1745449 1745450 1745451 [...] (624266 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1831896 1831897 1831898 1831899 1831900 1831901 1831902 1831903 1831904 1831905 [...] (575674 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836648 1836649 1836650 1836651 1836652 1836653 1836654 1836655 1836656 1836657 [...] (527221 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1922112 1922113 1922114 1922115 1922116 1922117 1922118 1922119 1922120 1922121 [...] (479261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1940958 1940959 1940960 1940961 1940962 1940963 1940964 1940965 1940966 1940967 [...] (431655 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1997010 1997011 1997012 1997013 1997014 1997015 1997016 1997017 1997018 1997019 [...] (383768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2070666 2070667 2070668 2070669 2070670 2070671 2070672 2070673 2070674 2070675 [...] (336205 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2115072 2115073 2115074 2115075 2115076 2115077 2115078 2115079 2115080 2115081 [...] (287958 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2153520 2153521 2153522 2153523 2153524 2153525 2153526 2153527 2153528 2153529 [...] (240359 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305170 2305171 2305172 2305173 2305174 2305175 2305176 2305177 2305178 2305179 [...] (192279 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2411622 2411623 2411624 2411625 2411626 2411627 2411628 2411629 2411630 2411631 [...] (144261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2517408 2517409 2517410 2517411 2517412 2517413 2517414 2517415 2517416 2517417 [...] (96528 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2561146 2561147 2579220 2579221 2579222 2579223 2579224 2579225 2579226 2579227 [...] (49500 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2647026 2647027 2647028 2647029 2647030 2647031 2647032 2647033 2647034 2647035 [...] (11759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2840302 2840303 2840304 2840305 2840306 2840307 2840308 2840309 2882142 2882143 [...] (644 flits)
Measured flits: (0 flits)
Time taken is 59475 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7990.46 (1 samples)
	minimum = 22 (1 samples)
	maximum = 26079 (1 samples)
Network latency average = 7595.04 (1 samples)
	minimum = 22 (1 samples)
	maximum = 25579 (1 samples)
Flit latency average = 13956.1 (1 samples)
	minimum = 5 (1 samples)
	maximum = 32258 (1 samples)
Fragmentation average = 26.6146 (1 samples)
	minimum = 0 (1 samples)
	maximum = 853 (1 samples)
Injected packet rate average = 0.0331607 (1 samples)
	minimum = 0.0128571 (1 samples)
	maximum = 0.0508571 (1 samples)
Accepted packet rate average = 0.0163943 (1 samples)
	minimum = 0.0128571 (1 samples)
	maximum = 0.0207143 (1 samples)
Injected flit rate average = 0.596783 (1 samples)
	minimum = 0.232571 (1 samples)
	maximum = 0.914857 (1 samples)
Accepted flit rate average = 0.295053 (1 samples)
	minimum = 0.231429 (1 samples)
	maximum = 0.372286 (1 samples)
Injected packet size average = 17.9967 (1 samples)
Accepted packet size average = 17.9972 (1 samples)
Hops average = 5.07151 (1 samples)
Total run time 56.2473
