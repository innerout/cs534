BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.11
	minimum = 22
	maximum = 629
Network latency average = 210.833
	minimum = 22
	maximum = 614
Slowest packet = 1506
Flit latency average = 186.366
	minimum = 5
	maximum = 600
Slowest flit = 29770
Fragmentation average = 19.3576
	minimum = 0
	maximum = 89
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.01375
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.252078
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.333
Total in-flight flits = 28947 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 382.004
	minimum = 22
	maximum = 1252
Network latency average = 376.484
	minimum = 22
	maximum = 1212
Slowest packet = 3296
Flit latency average = 352.701
	minimum = 5
	maximum = 1195
Slowest flit = 59345
Fragmentation average = 19.9075
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143932
	minimum = 0.0075 (at node 116)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.261813
	minimum = 0.142 (at node 116)
	maximum = 0.367 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.19
Total in-flight flits = 53220 (0 measured)
latency change    = 0.434273
throughput change = 0.0371807
Class 0:
Packet latency average = 850.701
	minimum = 22
	maximum = 1574
Network latency average = 844.288
	minimum = 22
	maximum = 1574
Slowest packet = 5949
Flit latency average = 822.576
	minimum = 5
	maximum = 1557
Slowest flit = 107099
Fragmentation average = 19.3144
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0219167
	minimum = 0.01 (at node 84)
	maximum = 0.033 (at node 0)
Accepted packet rate average = 0.015026
	minimum = 0.006 (at node 184)
	maximum = 0.03 (at node 34)
Injected flit rate average = 0.394427
	minimum = 0.18 (at node 84)
	maximum = 0.608 (at node 0)
Accepted flit rate average= 0.270573
	minimum = 0.121 (at node 184)
	maximum = 0.54 (at node 34)
Injected packet length average = 17.9967
Accepted packet length average = 18.0069
Total in-flight flits = 77212 (0 measured)
latency change    = 0.550954
throughput change = 0.0323773
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 116.639
	minimum = 22
	maximum = 925
Network latency average = 78.5158
	minimum = 22
	maximum = 925
Slowest packet = 12986
Flit latency average = 1107.67
	minimum = 5
	maximum = 1978
Slowest flit = 142685
Fragmentation average = 5.59177
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0218437
	minimum = 0.007 (at node 184)
	maximum = 0.036 (at node 49)
Accepted packet rate average = 0.0152344
	minimum = 0.006 (at node 36)
	maximum = 0.024 (at node 40)
Injected flit rate average = 0.393542
	minimum = 0.126 (at node 184)
	maximum = 0.648 (at node 49)
Accepted flit rate average= 0.27374
	minimum = 0.1 (at node 36)
	maximum = 0.455 (at node 78)
Injected packet length average = 18.0162
Accepted packet length average = 17.9685
Total in-flight flits = 100362 (69908 measured)
latency change    = 6.29343
throughput change = 0.0115682
Class 0:
Packet latency average = 999.846
	minimum = 22
	maximum = 1997
Network latency average = 970.17
	minimum = 22
	maximum = 1966
Slowest packet = 12854
Flit latency average = 1260.79
	minimum = 5
	maximum = 2564
Slowest flit = 180737
Fragmentation average = 15.3244
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0208177
	minimum = 0.006 (at node 120)
	maximum = 0.0325 (at node 23)
Accepted packet rate average = 0.015224
	minimum = 0.009 (at node 84)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.374753
	minimum = 0.1165 (at node 120)
	maximum = 0.592 (at node 23)
Accepted flit rate average= 0.27413
	minimum = 0.162 (at node 84)
	maximum = 0.414 (at node 129)
Injected packet length average = 18.0016
Accepted packet length average = 18.0065
Total in-flight flits = 116360 (113371 measured)
latency change    = 0.883343
throughput change = 0.00142496
Class 0:
Packet latency average = 1459.22
	minimum = 22
	maximum = 3101
Network latency average = 1413.05
	minimum = 22
	maximum = 2936
Slowest packet = 12812
Flit latency average = 1407.49
	minimum = 5
	maximum = 3184
Slowest flit = 210833
Fragmentation average = 17.4342
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0202361
	minimum = 0.00966667 (at node 180)
	maximum = 0.03 (at node 155)
Accepted packet rate average = 0.015191
	minimum = 0.00966667 (at node 84)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.364188
	minimum = 0.169 (at node 180)
	maximum = 0.54 (at node 155)
Accepted flit rate average= 0.272979
	minimum = 0.175 (at node 84)
	maximum = 0.38 (at node 129)
Injected packet length average = 17.9969
Accepted packet length average = 17.9698
Total in-flight flits = 130558 (130396 measured)
latency change    = 0.314809
throughput change = 0.00421659
Class 0:
Packet latency average = 1707.69
	minimum = 22
	maximum = 3784
Network latency average = 1634.89
	minimum = 22
	maximum = 3770
Slowest packet = 13682
Flit latency average = 1544.76
	minimum = 5
	maximum = 3792
Slowest flit = 244896
Fragmentation average = 17.9247
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0195404
	minimum = 0.0105 (at node 56)
	maximum = 0.02575 (at node 71)
Accepted packet rate average = 0.0151237
	minimum = 0.011 (at node 84)
	maximum = 0.02 (at node 95)
Injected flit rate average = 0.351733
	minimum = 0.19325 (at node 56)
	maximum = 0.46575 (at node 71)
Accepted flit rate average= 0.272156
	minimum = 0.201 (at node 84)
	maximum = 0.35775 (at node 129)
Injected packet length average = 18.0003
Accepted packet length average = 17.9954
Total in-flight flits = 139420 (139420 measured)
latency change    = 0.145498
throughput change = 0.00302369
Class 0:
Packet latency average = 1907.37
	minimum = 22
	maximum = 4359
Network latency average = 1805.08
	minimum = 22
	maximum = 4326
Slowest packet = 13834
Flit latency average = 1676.9
	minimum = 5
	maximum = 4309
Slowest flit = 249017
Fragmentation average = 18.1952
	minimum = 0
	maximum = 191
Injected packet rate average = 0.0189
	minimum = 0.0114 (at node 69)
	maximum = 0.0258 (at node 71)
Accepted packet rate average = 0.0150667
	minimum = 0.0114 (at node 104)
	maximum = 0.019 (at node 103)
Injected flit rate average = 0.340217
	minimum = 0.203 (at node 69)
	maximum = 0.4652 (at node 71)
Accepted flit rate average= 0.271177
	minimum = 0.2066 (at node 104)
	maximum = 0.3424 (at node 103)
Injected packet length average = 18.0009
Accepted packet length average = 17.9985
Total in-flight flits = 145202 (145202 measured)
latency change    = 0.104689
throughput change = 0.0036108
Class 0:
Packet latency average = 2082.67
	minimum = 22
	maximum = 5142
Network latency average = 1934.11
	minimum = 22
	maximum = 5142
Slowest packet = 15902
Flit latency average = 1790.47
	minimum = 5
	maximum = 5125
Slowest flit = 286252
Fragmentation average = 18.4238
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0183481
	minimum = 0.0116667 (at node 56)
	maximum = 0.0241667 (at node 119)
Accepted packet rate average = 0.0150616
	minimum = 0.0118333 (at node 64)
	maximum = 0.0186667 (at node 103)
Injected flit rate average = 0.330282
	minimum = 0.212833 (at node 56)
	maximum = 0.434667 (at node 119)
Accepted flit rate average= 0.271088
	minimum = 0.212 (at node 64)
	maximum = 0.339333 (at node 103)
Injected packet length average = 18.0009
Accepted packet length average = 17.9986
Total in-flight flits = 147149 (147149 measured)
latency change    = 0.0841722
throughput change = 0.000329818
Class 0:
Packet latency average = 2267.54
	minimum = 22
	maximum = 5772
Network latency average = 2062.23
	minimum = 22
	maximum = 5396
Slowest packet = 16126
Flit latency average = 1907.5
	minimum = 5
	maximum = 5359
Slowest flit = 328247
Fragmentation average = 18.691
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0178542
	minimum = 0.0108571 (at node 96)
	maximum = 0.0244286 (at node 119)
Accepted packet rate average = 0.0150193
	minimum = 0.0121429 (at node 79)
	maximum = 0.0185714 (at node 181)
Injected flit rate average = 0.321478
	minimum = 0.195429 (at node 96)
	maximum = 0.438857 (at node 119)
Accepted flit rate average= 0.270212
	minimum = 0.218571 (at node 79)
	maximum = 0.336571 (at node 181)
Injected packet length average = 18.0058
Accepted packet length average = 17.9909
Total in-flight flits = 147973 (147973 measured)
latency change    = 0.0815303
throughput change = 0.00324049
Draining all recorded packets ...
Class 0:
Remaining flits: 405486 405487 405488 405489 405490 405491 405492 405493 405494 405495 [...] (145792 flits)
Measured flits: 405486 405487 405488 405489 405490 405491 405492 405493 405494 405495 [...] (136630 flits)
Class 0:
Remaining flits: 420552 420553 420554 420555 420556 420557 420558 420559 420560 420561 [...] (147627 flits)
Measured flits: 420552 420553 420554 420555 420556 420557 420558 420559 420560 420561 [...] (119073 flits)
Class 0:
Remaining flits: 517608 517609 517610 517611 517612 517613 517614 517615 517616 517617 [...] (147536 flits)
Measured flits: 517608 517609 517610 517611 517612 517613 517614 517615 517616 517617 [...] (93721 flits)
Class 0:
Remaining flits: 540762 540763 540764 540765 540766 540767 540768 540769 540770 540771 [...] (145833 flits)
Measured flits: 540762 540763 540764 540765 540766 540767 540768 540769 540770 540771 [...] (63296 flits)
Class 0:
Remaining flits: 563148 563149 563150 563151 563152 563153 563154 563155 563156 563157 [...] (144971 flits)
Measured flits: 563148 563149 563150 563151 563152 563153 563154 563155 563156 563157 [...] (37176 flits)
Class 0:
Remaining flits: 638478 638479 638480 638481 638482 638483 638484 638485 638486 638487 [...] (142951 flits)
Measured flits: 638478 638479 638480 638481 638482 638483 638484 638485 638486 638487 [...] (19482 flits)
Class 0:
Remaining flits: 651672 651673 651674 651675 651676 651677 651678 651679 651680 651681 [...] (142650 flits)
Measured flits: 651672 651673 651674 651675 651676 651677 651678 651679 651680 651681 [...] (8596 flits)
Class 0:
Remaining flits: 702162 702163 702164 702165 702166 702167 702168 702169 702170 702171 [...] (143322 flits)
Measured flits: 733146 733147 733148 733149 733150 733151 733152 733153 733154 733155 [...] (2567 flits)
Class 0:
Remaining flits: 702162 702163 702164 702165 702166 702167 702168 702169 702170 702171 [...] (144555 flits)
Measured flits: 762498 762499 762500 762501 762502 762503 762504 762505 762506 762507 [...] (841 flits)
Class 0:
Remaining flits: 826524 826525 826526 826527 826528 826529 826530 826531 826532 826533 [...] (140177 flits)
Measured flits: 838872 838873 838874 838875 838876 838877 838878 838879 838880 838881 [...] (194 flits)
Class 0:
Remaining flits: 868572 868573 868574 868575 868576 868577 868578 868579 868580 868581 [...] (140790 flits)
Measured flits: 877842 877843 877844 877845 877846 877847 877848 877849 877850 877851 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 921420 921421 921422 921423 921424 921425 921426 921427 921428 921429 [...] (91809 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 951030 951031 951032 951033 951034 951035 951036 951037 951038 951039 [...] (45684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1023138 1023139 1023140 1023141 1023142 1023143 1023144 1023145 1023146 1023147 [...] (8194 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1069902 1069903 1069904 1069905 1069906 1069907 1069908 1069909 1069910 1069911 [...] (701 flits)
Measured flits: (0 flits)
Time taken is 25525 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3437.62 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11811 (1 samples)
Network latency average = 2525.73 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7487 (1 samples)
Flit latency average = 2533.63 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8226 (1 samples)
Fragmentation average = 19.6716 (1 samples)
	minimum = 0 (1 samples)
	maximum = 249 (1 samples)
Injected packet rate average = 0.0178542 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.0244286 (1 samples)
Accepted packet rate average = 0.0150193 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.321478 (1 samples)
	minimum = 0.195429 (1 samples)
	maximum = 0.438857 (1 samples)
Accepted flit rate average = 0.270212 (1 samples)
	minimum = 0.218571 (1 samples)
	maximum = 0.336571 (1 samples)
Injected packet size average = 18.0058 (1 samples)
Accepted packet size average = 17.9909 (1 samples)
Hops average = 5.06781 (1 samples)
Total run time 23.9954
