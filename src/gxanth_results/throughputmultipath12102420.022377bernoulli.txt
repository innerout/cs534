BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.11
	minimum = 22
	maximum = 629
Network latency average = 210.833
	minimum = 22
	maximum = 614
Slowest packet = 1506
Flit latency average = 186.365
	minimum = 5
	maximum = 600
Slowest flit = 29770
Fragmentation average = 19.3576
	minimum = 0
	maximum = 89
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.01375
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.252073
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.3326
Total in-flight flits = 28948 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 382.052
	minimum = 22
	maximum = 1254
Network latency average = 376.532
	minimum = 22
	maximum = 1214
Slowest packet = 3296
Flit latency average = 352.62
	minimum = 5
	maximum = 1197
Slowest flit = 59345
Fragmentation average = 19.8197
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.014401
	minimum = 0.0075 (at node 116)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.261776
	minimum = 0.142 (at node 116)
	maximum = 0.369 (at node 152)
Injected packet length average = 17.9257
Accepted packet length average = 18.1776
Total in-flight flits = 53234 (0 measured)
latency change    = 0.434345
throughput change = 0.0370665
Class 0:
Packet latency average = 840.537
	minimum = 22
	maximum = 1548
Network latency average = 834.268
	minimum = 22
	maximum = 1530
Slowest packet = 6144
Flit latency average = 812.301
	minimum = 5
	maximum = 1513
Slowest flit = 110609
Fragmentation average = 19.2416
	minimum = 0
	maximum = 99
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0152188
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.273552
	minimum = 0.126 (at node 61)
	maximum = 0.526 (at node 16)
Injected packet length average = 18.0068
Accepted packet length average = 17.9747
Total in-flight flits = 77626 (0 measured)
latency change    = 0.545466
throughput change = 0.0430486
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 44.3077
	minimum = 22
	maximum = 122
Network latency average = 39.0171
	minimum = 22
	maximum = 122
Slowest packet = 14473
Flit latency average = 1106.84
	minimum = 5
	maximum = 1810
Slowest flit = 167619
Fragmentation average = 4.54416
	minimum = 0
	maximum = 23
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0153229
	minimum = 0.006 (at node 1)
	maximum = 0.025 (at node 59)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.276729
	minimum = 0.108 (at node 1)
	maximum = 0.446 (at node 59)
Injected packet length average = 17.9469
Accepted packet length average = 18.0598
Total in-flight flits = 103838 (72953 measured)
latency change    = 17.9704
throughput change = 0.0114808
Class 0:
Packet latency average = 907.822
	minimum = 22
	maximum = 2023
Network latency average = 902.394
	minimum = 22
	maximum = 1982
Slowest packet = 12837
Flit latency average = 1260.31
	minimum = 5
	maximum = 2201
Slowest flit = 212885
Fragmentation average = 13.6158
	minimum = 0
	maximum = 92
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.0153594
	minimum = 0.009 (at node 36)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.276768
	minimum = 0.162 (at node 86)
	maximum = 0.387 (at node 128)
Injected packet length average = 17.9983
Accepted packet length average = 18.0195
Total in-flight flits = 126741 (126404 measured)
latency change    = 0.951193
throughput change = 0.000141138
Class 0:
Packet latency average = 1444.58
	minimum = 22
	maximum = 2632
Network latency average = 1438.93
	minimum = 22
	maximum = 2632
Slowest packet = 14381
Flit latency average = 1409.41
	minimum = 5
	maximum = 2667
Slowest flit = 255829
Fragmentation average = 17.3643
	minimum = 0
	maximum = 94
Injected packet rate average = 0.0225382
	minimum = 0.0146667 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0154045
	minimum = 0.01 (at node 36)
	maximum = 0.0206667 (at node 103)
Injected flit rate average = 0.405694
	minimum = 0.264 (at node 171)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.277495
	minimum = 0.175333 (at node 36)
	maximum = 0.372333 (at node 103)
Injected packet length average = 18.0003
Accepted packet length average = 18.0139
Total in-flight flits = 151465 (151465 measured)
latency change    = 0.371566
throughput change = 0.00261829
Draining remaining packets ...
Class 0:
Remaining flits: 296892 296893 296894 296895 296896 296897 296898 296899 296900 296901 [...] (104100 flits)
Measured flits: 296892 296893 296894 296895 296896 296897 296898 296899 296900 296901 [...] (104100 flits)
Class 0:
Remaining flits: 338567 338568 338569 338570 338571 338572 338573 338574 338575 338576 [...] (57921 flits)
Measured flits: 338567 338568 338569 338570 338571 338572 338573 338574 338575 338576 [...] (57921 flits)
Class 0:
Remaining flits: 400653 400654 400655 400656 400657 400658 400659 400660 400661 403146 [...] (13142 flits)
Measured flits: 400653 400654 400655 400656 400657 400658 400659 400660 400661 403146 [...] (13142 flits)
Time taken is 9773 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2274.77 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3989 (1 samples)
Network latency average = 2269.03 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3989 (1 samples)
Flit latency average = 2031.06 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3972 (1 samples)
Fragmentation average = 19.8898 (1 samples)
	minimum = 0 (1 samples)
	maximum = 99 (1 samples)
Injected packet rate average = 0.0225382 (1 samples)
	minimum = 0.0146667 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0154045 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.405694 (1 samples)
	minimum = 0.264 (1 samples)
	maximum = 0.541333 (1 samples)
Accepted flit rate average = 0.277495 (1 samples)
	minimum = 0.175333 (1 samples)
	maximum = 0.372333 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0139 (1 samples)
Hops average = 5.08088 (1 samples)
Total run time 6.09994
