BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 86.5578
	minimum = 23
	maximum = 279
Network latency average = 70.8073
	minimum = 23
	maximum = 235
Slowest packet = 13
Flit latency average = 39.3667
	minimum = 6
	maximum = 218
Slowest flit = 2357
Fragmentation average = 27.9208
	minimum = 0
	maximum = 163
Injected packet rate average = 0.00510938
	minimum = 0 (at node 5)
	maximum = 0.02 (at node 161)
Accepted packet rate average = 0.00486458
	minimum = 0.001 (at node 41)
	maximum = 0.011 (at node 140)
Injected flit rate average = 0.090974
	minimum = 0 (at node 5)
	maximum = 0.36 (at node 161)
Accepted flit rate average= 0.0885208
	minimum = 0.018 (at node 41)
	maximum = 0.198 (at node 140)
Injected packet length average = 17.8053
Accepted packet length average = 18.197
Total in-flight flits = 662 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 83.3603
	minimum = 23
	maximum = 335
Network latency average = 68.1003
	minimum = 23
	maximum = 290
Slowest packet = 13
Flit latency average = 37.904
	minimum = 6
	maximum = 273
Slowest flit = 20212
Fragmentation average = 25.4743
	minimum = 0
	maximum = 256
Injected packet rate average = 0.00473437
	minimum = 0 (at node 37)
	maximum = 0.014 (at node 161)
Accepted packet rate average = 0.00456771
	minimum = 0.0005 (at node 41)
	maximum = 0.008 (at node 48)
Injected flit rate average = 0.0848724
	minimum = 0 (at node 37)
	maximum = 0.251 (at node 161)
Accepted flit rate average= 0.0830859
	minimum = 0.009 (at node 41)
	maximum = 0.146 (at node 70)
Injected packet length average = 17.9268
Accepted packet length average = 18.1899
Total in-flight flits = 819 (0 measured)
latency change    = 0.0383575
throughput change = 0.0654129
Class 0:
Packet latency average = 88.1501
	minimum = 27
	maximum = 446
Network latency average = 71.9211
	minimum = 23
	maximum = 425
Slowest packet = 2079
Flit latency average = 40.4141
	minimum = 6
	maximum = 408
Slowest flit = 37439
Fragmentation average = 28.828
	minimum = 0
	maximum = 285
Injected packet rate average = 0.00475521
	minimum = 0 (at node 1)
	maximum = 0.025 (at node 22)
Accepted packet rate average = 0.00475521
	minimum = 0 (at node 17)
	maximum = 0.014 (at node 173)
Injected flit rate average = 0.0857448
	minimum = 0 (at node 1)
	maximum = 0.467 (at node 22)
Accepted flit rate average= 0.0862656
	minimum = 0 (at node 17)
	maximum = 0.241 (at node 173)
Injected packet length average = 18.0318
Accepted packet length average = 18.1413
Total in-flight flits = 690 (0 measured)
latency change    = 0.0543362
throughput change = 0.0368593
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 85.5242
	minimum = 23
	maximum = 300
Network latency average = 67.6116
	minimum = 23
	maximum = 295
Slowest packet = 3435
Flit latency average = 39.0066
	minimum = 6
	maximum = 278
Slowest flit = 61847
Fragmentation average = 24.6009
	minimum = 0
	maximum = 195
Injected packet rate average = 0.00471875
	minimum = 0 (at node 0)
	maximum = 0.016 (at node 115)
Accepted packet rate average = 0.00474479
	minimum = 0 (at node 18)
	maximum = 0.011 (at node 22)
Injected flit rate average = 0.084651
	minimum = 0 (at node 0)
	maximum = 0.272 (at node 115)
Accepted flit rate average= 0.0846823
	minimum = 0 (at node 18)
	maximum = 0.202 (at node 22)
Injected packet length average = 17.9393
Accepted packet length average = 17.8474
Total in-flight flits = 739 (739 measured)
latency change    = 0.030703
throughput change = 0.0186973
Class 0:
Packet latency average = 83.6319
	minimum = 23
	maximum = 349
Network latency average = 68.1835
	minimum = 23
	maximum = 339
Slowest packet = 3519
Flit latency average = 38.809
	minimum = 6
	maximum = 322
Slowest flit = 63359
Fragmentation average = 25.0265
	minimum = 0
	maximum = 227
Injected packet rate average = 0.00454948
	minimum = 0 (at node 58)
	maximum = 0.012 (at node 115)
Accepted packet rate average = 0.00458073
	minimum = 0.001 (at node 24)
	maximum = 0.0085 (at node 166)
Injected flit rate average = 0.0819323
	minimum = 0 (at node 58)
	maximum = 0.216 (at node 115)
Accepted flit rate average= 0.0820547
	minimum = 0.018 (at node 24)
	maximum = 0.153 (at node 166)
Injected packet length average = 18.0092
Accepted packet length average = 17.913
Total in-flight flits = 627 (627 measured)
latency change    = 0.0226271
throughput change = 0.0320226
Class 0:
Packet latency average = 82.1935
	minimum = 23
	maximum = 349
Network latency average = 66.3681
	minimum = 23
	maximum = 339
Slowest packet = 3519
Flit latency average = 37.7643
	minimum = 6
	maximum = 322
Slowest flit = 63359
Fragmentation average = 23.5871
	minimum = 0
	maximum = 227
Injected packet rate average = 0.00444097
	minimum = 0.000333333 (at node 119)
	maximum = 0.00933333 (at node 173)
Accepted packet rate average = 0.00441667
	minimum = 0.00133333 (at node 153)
	maximum = 0.007 (at node 172)
Injected flit rate average = 0.0799531
	minimum = 0.006 (at node 119)
	maximum = 0.168 (at node 173)
Accepted flit rate average= 0.0796528
	minimum = 0.024 (at node 153)
	maximum = 0.129667 (at node 174)
Injected packet length average = 18.0035
Accepted packet length average = 18.0346
Total in-flight flits = 854 (854 measured)
latency change    = 0.0174991
throughput change = 0.0301548
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6375 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 84.3298 (1 samples)
	minimum = 23 (1 samples)
	maximum = 349 (1 samples)
Network latency average = 67.9186 (1 samples)
	minimum = 23 (1 samples)
	maximum = 339 (1 samples)
Flit latency average = 38.8323 (1 samples)
	minimum = 6 (1 samples)
	maximum = 322 (1 samples)
Fragmentation average = 24.764 (1 samples)
	minimum = 0 (1 samples)
	maximum = 227 (1 samples)
Injected packet rate average = 0.00444097 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00933333 (1 samples)
Accepted packet rate average = 0.00441667 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.007 (1 samples)
Injected flit rate average = 0.0799531 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.168 (1 samples)
Accepted flit rate average = 0.0796528 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.129667 (1 samples)
Injected packet size average = 18.0035 (1 samples)
Accepted packet size average = 18.0346 (1 samples)
Hops average = 5.05194 (1 samples)
Total run time 2.15882
