BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 284.928
	minimum = 27
	maximum = 953
Network latency average = 232.25
	minimum = 23
	maximum = 912
Slowest packet = 77
Flit latency average = 162.881
	minimum = 6
	maximum = 958
Slowest flit = 2828
Fragmentation average = 124.56
	minimum = 0
	maximum = 864
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00879167
	minimum = 0.003 (at node 174)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.176979
	minimum = 0.06 (at node 174)
	maximum = 0.315 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.1303
Total in-flight flits = 14170 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 391.176
	minimum = 27
	maximum = 1932
Network latency average = 334.314
	minimum = 23
	maximum = 1792
Slowest packet = 355
Flit latency average = 248.184
	minimum = 6
	maximum = 1889
Slowest flit = 6820
Fragmentation average = 158.049
	minimum = 0
	maximum = 1500
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00958594
	minimum = 0.004 (at node 135)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.183714
	minimum = 0.0885 (at node 135)
	maximum = 0.284 (at node 22)
Injected packet length average = 17.9095
Accepted packet length average = 19.1649
Total in-flight flits = 20534 (0 measured)
latency change    = 0.271611
throughput change = 0.0366569
Class 0:
Packet latency average = 613.024
	minimum = 27
	maximum = 2838
Network latency average = 551.267
	minimum = 25
	maximum = 2582
Slowest packet = 1018
Flit latency average = 452.635
	minimum = 6
	maximum = 2606
Slowest flit = 20589
Fragmentation average = 213.119
	minimum = 0
	maximum = 2037
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0107708
	minimum = 0.004 (at node 146)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.196094
	minimum = 0.052 (at node 146)
	maximum = 0.358 (at node 24)
Injected packet length average = 18.0569
Accepted packet length average = 18.206
Total in-flight flits = 27812 (0 measured)
latency change    = 0.361891
throughput change = 0.0631341
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 389.212
	minimum = 25
	maximum = 1103
Network latency average = 333.214
	minimum = 23
	maximum = 978
Slowest packet = 7597
Flit latency average = 588.045
	minimum = 6
	maximum = 3647
Slowest flit = 19658
Fragmentation average = 144.915
	minimum = 0
	maximum = 644
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0112656
	minimum = 0.004 (at node 138)
	maximum = 0.021 (at node 34)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.202672
	minimum = 0.06 (at node 138)
	maximum = 0.387 (at node 34)
Injected packet length average = 17.981
Accepted packet length average = 17.9903
Total in-flight flits = 35267 (26690 measured)
latency change    = 0.575041
throughput change = 0.032457
Class 0:
Packet latency average = 571.309
	minimum = 25
	maximum = 2033
Network latency average = 511.779
	minimum = 23
	maximum = 1946
Slowest packet = 7597
Flit latency average = 650.711
	minimum = 6
	maximum = 4685
Slowest flit = 17900
Fragmentation average = 168.384
	minimum = 0
	maximum = 1370
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.011263
	minimum = 0.0045 (at node 4)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.204536
	minimum = 0.089 (at node 4)
	maximum = 0.3305 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.16
Total in-flight flits = 43626 (39955 measured)
latency change    = 0.318737
throughput change = 0.00911614
Class 0:
Packet latency average = 714.543
	minimum = 25
	maximum = 2953
Network latency average = 651.446
	minimum = 23
	maximum = 2936
Slowest packet = 7667
Flit latency average = 718.813
	minimum = 6
	maximum = 5136
Slowest flit = 41029
Fragmentation average = 187.954
	minimum = 0
	maximum = 2065
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0113229
	minimum = 0.00766667 (at node 17)
	maximum = 0.0156667 (at node 0)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.204193
	minimum = 0.137333 (at node 36)
	maximum = 0.274667 (at node 44)
Injected packet length average = 17.9913
Accepted packet length average = 18.0336
Total in-flight flits = 46727 (45028 measured)
latency change    = 0.200456
throughput change = 0.00168346
Class 0:
Packet latency average = 825.745
	minimum = 23
	maximum = 3938
Network latency average = 761.537
	minimum = 23
	maximum = 3784
Slowest packet = 7633
Flit latency average = 780.177
	minimum = 6
	maximum = 5726
Slowest flit = 47501
Fragmentation average = 197.407
	minimum = 0
	maximum = 2155
Injected packet rate average = 0.0133529
	minimum = 0.00175 (at node 78)
	maximum = 0.02675 (at node 97)
Accepted packet rate average = 0.0113464
	minimum = 0.00725 (at node 36)
	maximum = 0.01525 (at node 0)
Injected flit rate average = 0.24025
	minimum = 0.0315 (at node 78)
	maximum = 0.48225 (at node 97)
Accepted flit rate average= 0.204478
	minimum = 0.1295 (at node 36)
	maximum = 0.26775 (at node 0)
Injected packet length average = 17.9924
Accepted packet length average = 18.0215
Total in-flight flits = 55363 (54432 measured)
latency change    = 0.134668
throughput change = 0.00139456
Class 0:
Packet latency average = 926.093
	minimum = 23
	maximum = 4856
Network latency average = 861.025
	minimum = 23
	maximum = 4807
Slowest packet = 8054
Flit latency average = 847.239
	minimum = 6
	maximum = 6767
Slowest flit = 51998
Fragmentation average = 203.591
	minimum = 0
	maximum = 3408
Injected packet rate average = 0.0133448
	minimum = 0.004 (at node 78)
	maximum = 0.0256 (at node 109)
Accepted packet rate average = 0.011324
	minimum = 0.0074 (at node 135)
	maximum = 0.0146 (at node 129)
Injected flit rate average = 0.240096
	minimum = 0.0712 (at node 119)
	maximum = 0.4608 (at node 109)
Accepted flit rate average= 0.204101
	minimum = 0.1384 (at node 135)
	maximum = 0.2724 (at node 129)
Injected packet length average = 17.9917
Accepted packet length average = 18.0238
Total in-flight flits = 62473 (61956 measured)
latency change    = 0.108357
throughput change = 0.00184626
Class 0:
Packet latency average = 1011.7
	minimum = 23
	maximum = 5976
Network latency average = 946.787
	minimum = 23
	maximum = 5809
Slowest packet = 7603
Flit latency average = 908.503
	minimum = 6
	maximum = 7010
Slowest flit = 52001
Fragmentation average = 207.009
	minimum = 0
	maximum = 3408
Injected packet rate average = 0.013237
	minimum = 0.00566667 (at node 80)
	maximum = 0.0251667 (at node 28)
Accepted packet rate average = 0.0113082
	minimum = 0.00833333 (at node 86)
	maximum = 0.0143333 (at node 95)
Injected flit rate average = 0.238133
	minimum = 0.102 (at node 80)
	maximum = 0.450333 (at node 28)
Accepted flit rate average= 0.203511
	minimum = 0.1525 (at node 139)
	maximum = 0.254667 (at node 95)
Injected packet length average = 17.99
Accepted packet length average = 17.9969
Total in-flight flits = 67849 (67482 measured)
latency change    = 0.0846143
throughput change = 0.00289791
Class 0:
Packet latency average = 1091.06
	minimum = 23
	maximum = 6581
Network latency average = 1025.24
	minimum = 23
	maximum = 6547
Slowest packet = 7694
Flit latency average = 970.368
	minimum = 6
	maximum = 7882
Slowest flit = 70379
Fragmentation average = 209.559
	minimum = 0
	maximum = 3408
Injected packet rate average = 0.0134874
	minimum = 0.00642857 (at node 162)
	maximum = 0.025 (at node 28)
Accepted packet rate average = 0.011308
	minimum = 0.00871429 (at node 64)
	maximum = 0.014 (at node 103)
Injected flit rate average = 0.24273
	minimum = 0.113857 (at node 162)
	maximum = 0.45 (at node 28)
Accepted flit rate average= 0.203622
	minimum = 0.157286 (at node 135)
	maximum = 0.249857 (at node 103)
Injected packet length average = 17.9969
Accepted packet length average = 18.0068
Total in-flight flits = 80430 (80174 measured)
latency change    = 0.0727349
throughput change = 0.000543846
Draining all recorded packets ...
Class 0:
Remaining flits: 120924 120925 120926 120927 120928 120929 120930 120931 120932 120933 [...] (89152 flits)
Measured flits: 137700 137701 137702 137703 137704 137705 137706 137707 137708 137709 [...] (52362 flits)
Class 0:
Remaining flits: 120933 120934 120935 120936 120937 120938 120939 120940 120941 123696 [...] (96526 flits)
Measured flits: 142362 142363 142364 142365 142366 142367 142368 142369 142370 142371 [...] (33938 flits)
Class 0:
Remaining flits: 123696 123697 123698 123699 123700 123701 123702 123703 123704 123705 [...] (102696 flits)
Measured flits: 164340 164341 164342 164343 164344 164345 164346 164347 164348 164349 [...] (21671 flits)
Class 0:
Remaining flits: 135316 135317 135318 135319 135320 135321 135322 135323 164340 164341 [...] (111850 flits)
Measured flits: 164340 164341 164342 164343 164344 164345 164346 164347 164348 164349 [...] (14053 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (117236 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (9532 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (122078 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (6490 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (129146 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (4508 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (136910 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (3138 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (143709 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (2329 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (154809 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (1537 flits)
Class 0:
Remaining flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (160841 flits)
Measured flits: 165114 165115 165116 165117 165118 165119 165120 165121 165122 165123 [...] (1230 flits)
Class 0:
Remaining flits: 225378 225379 225380 225381 225382 225383 225384 225385 225386 225387 [...] (169776 flits)
Measured flits: 225378 225379 225380 225381 225382 225383 225384 225385 225386 225387 [...] (727 flits)
Class 0:
Remaining flits: 356922 356923 356924 356925 356926 356927 356928 356929 356930 356931 [...] (179598 flits)
Measured flits: 356922 356923 356924 356925 356926 356927 356928 356929 356930 356931 [...] (412 flits)
Class 0:
Remaining flits: 406234 406235 406236 406237 406238 406239 406240 406241 409385 409386 [...] (189800 flits)
Measured flits: 406234 406235 406236 406237 406238 406239 406240 406241 409385 409386 [...] (183 flits)
Class 0:
Remaining flits: 419489 421434 421435 421436 421437 421438 421439 421440 421441 421442 [...] (201055 flits)
Measured flits: 419489 421434 421435 421436 421437 421438 421439 421440 421441 421442 [...] (81 flits)
Class 0:
Remaining flits: 421434 421435 421436 421437 421438 421439 421440 421441 421442 421443 [...] (212882 flits)
Measured flits: 421434 421435 421436 421437 421438 421439 421440 421441 421442 421443 [...] (54 flits)
Class 0:
Remaining flits: 421436 421437 421438 421439 421440 421441 421442 421443 421444 421445 [...] (217746 flits)
Measured flits: 421436 421437 421438 421439 421440 421441 421442 421443 421444 421445 [...] (27 flits)
Class 0:
Remaining flits: 421446 421447 421448 421449 421450 421451 471618 471619 471620 471621 [...] (227161 flits)
Measured flits: 421446 421447 421448 421449 421450 421451 (6 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 471618 471619 471620 471621 471622 471623 471624 471625 471626 471627 [...] (194079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 471618 471619 471620 471621 471622 471623 471624 471625 471626 471627 [...] (160498 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 540774 540775 540776 540777 540778 540779 540780 540781 540782 540783 [...] (127878 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 540774 540775 540776 540777 540778 540779 540780 540781 540782 540783 [...] (95798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 644998 644999 645000 645001 645002 645003 645004 645005 645006 645007 [...] (65862 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 644999 645000 645001 645002 645003 645004 645005 645006 645007 645008 [...] (40190 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 644999 645000 645001 645002 645003 645004 645005 645006 645007 645008 [...] (20200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 645010 645011 715824 715825 715826 715827 715828 715829 715830 715831 [...] (6890 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1002258 1002259 1002260 1002261 1002262 1002263 1002264 1002265 1002266 1002267 [...] (622 flits)
Measured flits: (0 flits)
Time taken is 37473 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1806.13 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18911 (1 samples)
Network latency average = 1736.77 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18877 (1 samples)
Flit latency average = 3269.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 22154 (1 samples)
Fragmentation average = 229.113 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4342 (1 samples)
Injected packet rate average = 0.0134874 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.025 (1 samples)
Accepted packet rate average = 0.011308 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.24273 (1 samples)
	minimum = 0.113857 (1 samples)
	maximum = 0.45 (1 samples)
Accepted flit rate average = 0.203622 (1 samples)
	minimum = 0.157286 (1 samples)
	maximum = 0.249857 (1 samples)
Injected packet size average = 17.9969 (1 samples)
Accepted packet size average = 18.0068 (1 samples)
Hops average = 5.07913 (1 samples)
Total run time 32.671
