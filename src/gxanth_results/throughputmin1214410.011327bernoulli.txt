BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 153.322
	minimum = 23
	maximum = 643
Network latency average = 151.355
	minimum = 23
	maximum = 643
Slowest packet = 564
Flit latency average = 112.071
	minimum = 6
	maximum = 688
Slowest flit = 11392
Fragmentation average = 50.1465
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0112448
	minimum = 0.004 (at node 12)
	maximum = 0.021 (at node 162)
Accepted packet rate average = 0.00825
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.200438
	minimum = 0.072 (at node 12)
	maximum = 0.378 (at node 162)
Accepted flit rate average= 0.15662
	minimum = 0.036 (at node 41)
	maximum = 0.302 (at node 91)
Injected packet length average = 17.8249
Accepted packet length average = 18.9842
Total in-flight flits = 8791 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 235.149
	minimum = 23
	maximum = 1486
Network latency average = 233.152
	minimum = 23
	maximum = 1486
Slowest packet = 1042
Flit latency average = 188.916
	minimum = 6
	maximum = 1469
Slowest flit = 18773
Fragmentation average = 57.722
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0110964
	minimum = 0.0055 (at node 40)
	maximum = 0.017 (at node 166)
Accepted packet rate average = 0.00882292
	minimum = 0.004 (at node 174)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.198698
	minimum = 0.0955 (at node 40)
	maximum = 0.306 (at node 166)
Accepted flit rate average= 0.162755
	minimum = 0.086 (at node 174)
	maximum = 0.2755 (at node 22)
Injected packet length average = 17.9066
Accepted packet length average = 18.4469
Total in-flight flits = 14200 (0 measured)
latency change    = 0.347979
throughput change = 0.0376972
Class 0:
Packet latency average = 439.502
	minimum = 25
	maximum = 1838
Network latency average = 437.574
	minimum = 25
	maximum = 1838
Slowest packet = 1154
Flit latency average = 392.361
	minimum = 6
	maximum = 1821
Slowest flit = 20789
Fragmentation average = 66.839
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0112708
	minimum = 0.003 (at node 145)
	maximum = 0.02 (at node 77)
Accepted packet rate average = 0.00947917
	minimum = 0.003 (at node 153)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.203234
	minimum = 0.057 (at node 145)
	maximum = 0.36 (at node 162)
Accepted flit rate average= 0.16999
	minimum = 0.054 (at node 153)
	maximum = 0.333 (at node 63)
Injected packet length average = 18.0319
Accepted packet length average = 17.933
Total in-flight flits = 20532 (0 measured)
latency change    = 0.464965
throughput change = 0.0425578
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 293.51
	minimum = 23
	maximum = 966
Network latency average = 291.279
	minimum = 23
	maximum = 959
Slowest packet = 6484
Flit latency average = 538.616
	minimum = 6
	maximum = 2506
Slowest flit = 42173
Fragmentation average = 60.0801
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0117708
	minimum = 0.003 (at node 69)
	maximum = 0.021 (at node 162)
Accepted packet rate average = 0.0095625
	minimum = 0.004 (at node 38)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.211021
	minimum = 0.054 (at node 69)
	maximum = 0.378 (at node 162)
Accepted flit rate average= 0.17226
	minimum = 0.056 (at node 146)
	maximum = 0.324 (at node 16)
Injected packet length average = 17.9274
Accepted packet length average = 18.0142
Total in-flight flits = 28120 (24066 measured)
latency change    = 0.4974
throughput change = 0.0131826
Class 0:
Packet latency average = 503.998
	minimum = 23
	maximum = 1929
Network latency average = 501.694
	minimum = 23
	maximum = 1929
Slowest packet = 6499
Flit latency average = 616.309
	minimum = 6
	maximum = 3052
Slowest flit = 63071
Fragmentation average = 66.8405
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0113854
	minimum = 0.007 (at node 66)
	maximum = 0.0185 (at node 53)
Accepted packet rate average = 0.00941406
	minimum = 0.0035 (at node 36)
	maximum = 0.0155 (at node 120)
Injected flit rate average = 0.205013
	minimum = 0.126 (at node 66)
	maximum = 0.3355 (at node 131)
Accepted flit rate average= 0.16993
	minimum = 0.063 (at node 36)
	maximum = 0.277 (at node 120)
Injected packet length average = 18.0066
Accepted packet length average = 18.0506
Total in-flight flits = 34011 (33329 measured)
latency change    = 0.417637
throughput change = 0.0137158
Class 0:
Packet latency average = 660.949
	minimum = 23
	maximum = 2850
Network latency average = 657.469
	minimum = 23
	maximum = 2850
Slowest packet = 6735
Flit latency average = 698.584
	minimum = 6
	maximum = 3175
Slowest flit = 108534
Fragmentation average = 68.1242
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0113351
	minimum = 0.00666667 (at node 46)
	maximum = 0.0166667 (at node 60)
Accepted packet rate average = 0.00945833
	minimum = 0.00433333 (at node 36)
	maximum = 0.0143333 (at node 16)
Injected flit rate average = 0.204144
	minimum = 0.12 (at node 46)
	maximum = 0.305 (at node 60)
Accepted flit rate average= 0.170087
	minimum = 0.078 (at node 36)
	maximum = 0.259 (at node 16)
Injected packet length average = 18.01
Accepted packet length average = 17.9827
Total in-flight flits = 40138 (40079 measured)
latency change    = 0.237463
throughput change = 0.000923752
Draining remaining packets ...
Class 0:
Remaining flits: 124326 124327 124328 124329 124330 124331 124332 124333 124334 124335 [...] (11975 flits)
Measured flits: 124326 124327 124328 124329 124330 124331 124332 124333 124334 124335 [...] (11975 flits)
Class 0:
Remaining flits: 142326 142327 142328 142329 142330 142331 142332 142333 142334 142335 [...] (321 flits)
Measured flits: 142326 142327 142328 142329 142330 142331 142332 142333 142334 142335 [...] (321 flits)
Time taken is 8223 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 981.099 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4424 (1 samples)
Network latency average = 974.808 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4424 (1 samples)
Flit latency average = 939.605 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4407 (1 samples)
Fragmentation average = 66.0106 (1 samples)
	minimum = 0 (1 samples)
	maximum = 166 (1 samples)
Injected packet rate average = 0.0113351 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0166667 (1 samples)
Accepted packet rate average = 0.00945833 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0143333 (1 samples)
Injected flit rate average = 0.204144 (1 samples)
	minimum = 0.12 (1 samples)
	maximum = 0.305 (1 samples)
Accepted flit rate average = 0.170087 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.259 (1 samples)
Injected packet size average = 18.01 (1 samples)
Accepted packet size average = 17.9827 (1 samples)
Hops average = 5.09446 (1 samples)
Total run time 5.55376
