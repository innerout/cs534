BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 218.513
	minimum = 22
	maximum = 943
Network latency average = 156.187
	minimum = 22
	maximum = 668
Slowest packet = 63
Flit latency average = 126.517
	minimum = 5
	maximum = 651
Slowest flit = 19241
Fragmentation average = 30.5921
	minimum = 0
	maximum = 278
Injected packet rate average = 0.0150677
	minimum = 0 (at node 21)
	maximum = 0.047 (at node 91)
Accepted packet rate average = 0.0115052
	minimum = 0.002 (at node 174)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.269047
	minimum = 0 (at node 21)
	maximum = 0.846 (at node 91)
Accepted flit rate average= 0.213714
	minimum = 0.046 (at node 174)
	maximum = 0.359 (at node 44)
Injected packet length average = 17.8559
Accepted packet length average = 18.5754
Total in-flight flits = 11059 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 281.524
	minimum = 22
	maximum = 1375
Network latency average = 217.75
	minimum = 22
	maximum = 1158
Slowest packet = 63
Flit latency average = 185.58
	minimum = 5
	maximum = 1141
Slowest flit = 33839
Fragmentation average = 35.0015
	minimum = 0
	maximum = 375
Injected packet rate average = 0.0140313
	minimum = 0.001 (at node 172)
	maximum = 0.036 (at node 185)
Accepted packet rate average = 0.0118359
	minimum = 0.007 (at node 135)
	maximum = 0.019 (at node 166)
Injected flit rate average = 0.251922
	minimum = 0.018 (at node 172)
	maximum = 0.642 (at node 185)
Accepted flit rate average= 0.217016
	minimum = 0.126 (at node 153)
	maximum = 0.342 (at node 166)
Injected packet length average = 17.9543
Accepted packet length average = 18.3353
Total in-flight flits = 13650 (0 measured)
latency change    = 0.223822
throughput change = 0.0152159
Class 0:
Packet latency average = 391.946
	minimum = 25
	maximum = 1760
Network latency average = 323.879
	minimum = 22
	maximum = 1668
Slowest packet = 2848
Flit latency average = 286.06
	minimum = 5
	maximum = 1651
Slowest flit = 64781
Fragmentation average = 42.3186
	minimum = 0
	maximum = 526
Injected packet rate average = 0.0136406
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 171)
Accepted packet rate average = 0.0132396
	minimum = 0.003 (at node 184)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.244469
	minimum = 0 (at node 30)
	maximum = 0.865 (at node 171)
Accepted flit rate average= 0.23788
	minimum = 0.06 (at node 184)
	maximum = 0.428 (at node 16)
Injected packet length average = 17.9221
Accepted packet length average = 17.9673
Total in-flight flits = 15137 (0 measured)
latency change    = 0.281728
throughput change = 0.0877105
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 321.708
	minimum = 25
	maximum = 1156
Network latency average = 244.362
	minimum = 22
	maximum = 927
Slowest packet = 8027
Flit latency average = 315.619
	minimum = 5
	maximum = 1706
Slowest flit = 80801
Fragmentation average = 35.9549
	minimum = 0
	maximum = 268
Injected packet rate average = 0.0138646
	minimum = 0 (at node 1)
	maximum = 0.048 (at node 40)
Accepted packet rate average = 0.0134271
	minimum = 0.005 (at node 113)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.249943
	minimum = 0 (at node 1)
	maximum = 0.871 (at node 40)
Accepted flit rate average= 0.241573
	minimum = 0.09 (at node 113)
	maximum = 0.444 (at node 16)
Injected packet length average = 18.0274
Accepted packet length average = 17.9915
Total in-flight flits = 16689 (16278 measured)
latency change    = 0.218327
throughput change = 0.0152861
Class 0:
Packet latency average = 389.179
	minimum = 24
	maximum = 2016
Network latency average = 315.953
	minimum = 22
	maximum = 1700
Slowest packet = 8027
Flit latency average = 326.743
	minimum = 5
	maximum = 2130
Slowest flit = 136328
Fragmentation average = 39.0317
	minimum = 0
	maximum = 365
Injected packet rate average = 0.0133516
	minimum = 0.001 (at node 143)
	maximum = 0.0405 (at node 40)
Accepted packet rate average = 0.0132526
	minimum = 0.007 (at node 4)
	maximum = 0.02 (at node 78)
Injected flit rate average = 0.240669
	minimum = 0.018 (at node 143)
	maximum = 0.7355 (at node 40)
Accepted flit rate average= 0.238203
	minimum = 0.1245 (at node 4)
	maximum = 0.367 (at node 78)
Injected packet length average = 18.0256
Accepted packet length average = 17.9741
Total in-flight flits = 16007 (16004 measured)
latency change    = 0.173367
throughput change = 0.0141467
Class 0:
Packet latency average = 429.124
	minimum = 23
	maximum = 2584
Network latency average = 347.642
	minimum = 22
	maximum = 2265
Slowest packet = 8788
Flit latency average = 338.825
	minimum = 5
	maximum = 2248
Slowest flit = 170243
Fragmentation average = 39.0221
	minimum = 0
	maximum = 494
Injected packet rate average = 0.0130694
	minimum = 0.00266667 (at node 134)
	maximum = 0.0353333 (at node 40)
Accepted packet rate average = 0.013059
	minimum = 0.00766667 (at node 84)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.235238
	minimum = 0.048 (at node 134)
	maximum = 0.638 (at node 40)
Accepted flit rate average= 0.234618
	minimum = 0.138333 (at node 84)
	maximum = 0.369333 (at node 128)
Injected packet length average = 17.9991
Accepted packet length average = 17.966
Total in-flight flits = 15555 (15555 measured)
latency change    = 0.0930838
throughput change = 0.0152804
Class 0:
Packet latency average = 443.954
	minimum = 23
	maximum = 2584
Network latency average = 358.558
	minimum = 22
	maximum = 2398
Slowest packet = 8788
Flit latency average = 340.635
	minimum = 5
	maximum = 2381
Slowest flit = 214541
Fragmentation average = 40.1161
	minimum = 0
	maximum = 494
Injected packet rate average = 0.0132031
	minimum = 0.00375 (at node 4)
	maximum = 0.032 (at node 40)
Accepted packet rate average = 0.0131276
	minimum = 0.00925 (at node 84)
	maximum = 0.01825 (at node 128)
Injected flit rate average = 0.237737
	minimum = 0.06575 (at node 132)
	maximum = 0.5775 (at node 40)
Accepted flit rate average= 0.236108
	minimum = 0.1665 (at node 84)
	maximum = 0.33225 (at node 128)
Injected packet length average = 18.0061
Accepted packet length average = 17.9856
Total in-flight flits = 16380 (16380 measured)
latency change    = 0.0334046
throughput change = 0.00631074
Class 0:
Packet latency average = 454.577
	minimum = 22
	maximum = 3023
Network latency average = 366.422
	minimum = 22
	maximum = 2627
Slowest packet = 8788
Flit latency average = 343.671
	minimum = 5
	maximum = 2591
Slowest flit = 240955
Fragmentation average = 40.1967
	minimum = 0
	maximum = 494
Injected packet rate average = 0.0132865
	minimum = 0.0036 (at node 121)
	maximum = 0.0272 (at node 14)
Accepted packet rate average = 0.0131312
	minimum = 0.0098 (at node 79)
	maximum = 0.018 (at node 129)
Injected flit rate average = 0.239191
	minimum = 0.0676 (at node 121)
	maximum = 0.4896 (at node 14)
Accepted flit rate average= 0.236198
	minimum = 0.1732 (at node 162)
	maximum = 0.324 (at node 129)
Injected packet length average = 18.0026
Accepted packet length average = 17.9875
Total in-flight flits = 18049 (18049 measured)
latency change    = 0.0233699
throughput change = 0.000380375
Class 0:
Packet latency average = 465.6
	minimum = 22
	maximum = 3191
Network latency average = 373.402
	minimum = 22
	maximum = 2733
Slowest packet = 8788
Flit latency average = 347.694
	minimum = 5
	maximum = 2716
Slowest flit = 289043
Fragmentation average = 41.2579
	minimum = 0
	maximum = 500
Injected packet rate average = 0.0133793
	minimum = 0.00483333 (at node 67)
	maximum = 0.0276667 (at node 14)
Accepted packet rate average = 0.0131562
	minimum = 0.0101667 (at node 89)
	maximum = 0.0168333 (at node 128)
Injected flit rate average = 0.240895
	minimum = 0.087 (at node 67)
	maximum = 0.495833 (at node 14)
Accepted flit rate average= 0.236891
	minimum = 0.182167 (at node 89)
	maximum = 0.305167 (at node 128)
Injected packet length average = 18.005
Accepted packet length average = 18.0059
Total in-flight flits = 19709 (19709 measured)
latency change    = 0.0236742
throughput change = 0.00292417
Draining all recorded packets ...
Class 0:
Remaining flits: 337824 337825 337826 337827 337828 337829 337830 337831 337832 337833 [...] (23690 flits)
Measured flits: 337824 337825 337826 337827 337828 337829 337830 337831 337832 337833 [...] (4504 flits)
Class 0:
Remaining flits: 337824 337825 337826 337827 337828 337829 337830 337831 337832 337833 [...] (27968 flits)
Measured flits: 337824 337825 337826 337827 337828 337829 337830 337831 337832 337833 [...] (680 flits)
Class 0:
Remaining flits: 436518 436519 436520 436521 436522 436523 436524 436525 436526 436527 [...] (31312 flits)
Measured flits: 494478 494479 494480 494481 494482 494483 494484 494485 494486 494487 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 496227 496228 496229 496230 496231 496232 496233 496234 496235 496236 [...] (4544 flits)
Measured flits: (0 flits)
Time taken is 13994 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 526.024 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4037 (1 samples)
Network latency average = 418.912 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3175 (1 samples)
Flit latency average = 433.203 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3158 (1 samples)
Fragmentation average = 43.2497 (1 samples)
	minimum = 0 (1 samples)
	maximum = 579 (1 samples)
Injected packet rate average = 0.0133793 (1 samples)
	minimum = 0.00483333 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0131562 (1 samples)
	minimum = 0.0101667 (1 samples)
	maximum = 0.0168333 (1 samples)
Injected flit rate average = 0.240895 (1 samples)
	minimum = 0.087 (1 samples)
	maximum = 0.495833 (1 samples)
Accepted flit rate average = 0.236891 (1 samples)
	minimum = 0.182167 (1 samples)
	maximum = 0.305167 (1 samples)
Injected packet size average = 18.005 (1 samples)
Accepted packet size average = 18.0059 (1 samples)
Hops average = 5.06289 (1 samples)
Total run time 12.2845
