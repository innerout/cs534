BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 116.441
	minimum = 22
	maximum = 528
Network latency average = 82.372
	minimum = 22
	maximum = 407
Slowest packet = 45
Flit latency average = 60.0174
	minimum = 5
	maximum = 390
Slowest flit = 13805
Fragmentation average = 10.2153
	minimum = 0
	maximum = 74
Injected packet rate average = 0.00939062
	minimum = 0 (at node 26)
	maximum = 0.033 (at node 37)
Accepted packet rate average = 0.00870833
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.167693
	minimum = 0 (at node 26)
	maximum = 0.594 (at node 37)
Accepted flit rate average= 0.158875
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 48)
Injected packet length average = 17.8575
Accepted packet length average = 18.244
Total in-flight flits = 1950 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 122.63
	minimum = 22
	maximum = 655
Network latency average = 86.2535
	minimum = 22
	maximum = 543
Slowest packet = 45
Flit latency average = 63.7258
	minimum = 5
	maximum = 526
Slowest flit = 41107
Fragmentation average = 10.1407
	minimum = 0
	maximum = 74
Injected packet rate average = 0.00889063
	minimum = 0.0005 (at node 184)
	maximum = 0.021 (at node 20)
Accepted packet rate average = 0.00847396
	minimum = 0.0045 (at node 174)
	maximum = 0.0135 (at node 44)
Injected flit rate average = 0.159396
	minimum = 0.009 (at node 184)
	maximum = 0.371 (at node 20)
Accepted flit rate average= 0.15362
	minimum = 0.0845 (at node 174)
	maximum = 0.243 (at node 44)
Injected packet length average = 17.9285
Accepted packet length average = 18.1285
Total in-flight flits = 2462 (0 measured)
latency change    = 0.0504729
throughput change = 0.0342092
Class 0:
Packet latency average = 145.743
	minimum = 25
	maximum = 649
Network latency average = 107.159
	minimum = 22
	maximum = 470
Slowest packet = 2891
Flit latency average = 84.5864
	minimum = 5
	maximum = 491
Slowest flit = 77145
Fragmentation average = 11.3314
	minimum = 0
	maximum = 80
Injected packet rate average = 0.00902604
	minimum = 0 (at node 5)
	maximum = 0.036 (at node 162)
Accepted packet rate average = 0.00886458
	minimum = 0.001 (at node 153)
	maximum = 0.017 (at node 6)
Injected flit rate average = 0.162359
	minimum = 0 (at node 5)
	maximum = 0.648 (at node 162)
Accepted flit rate average= 0.159583
	minimum = 0.018 (at node 153)
	maximum = 0.309 (at node 6)
Injected packet length average = 17.9879
Accepted packet length average = 18.0024
Total in-flight flits = 3016 (0 measured)
latency change    = 0.158587
throughput change = 0.0373695
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 145.196
	minimum = 22
	maximum = 845
Network latency average = 105.093
	minimum = 22
	maximum = 531
Slowest packet = 5155
Flit latency average = 92.0759
	minimum = 5
	maximum = 580
Slowest flit = 78189
Fragmentation average = 10.1361
	minimum = 0
	maximum = 73
Injected packet rate average = 0.00923958
	minimum = 0 (at node 47)
	maximum = 0.032 (at node 74)
Accepted packet rate average = 0.00926042
	minimum = 0.001 (at node 184)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.166469
	minimum = 0 (at node 47)
	maximum = 0.583 (at node 74)
Accepted flit rate average= 0.166687
	minimum = 0.018 (at node 184)
	maximum = 0.342 (at node 16)
Injected packet length average = 18.0169
Accepted packet length average = 18
Total in-flight flits = 2944 (2944 measured)
latency change    = 0.00376922
throughput change = 0.0426197
Class 0:
Packet latency average = 146.359
	minimum = 22
	maximum = 845
Network latency average = 106.366
	minimum = 22
	maximum = 531
Slowest packet = 5155
Flit latency average = 88.0452
	minimum = 5
	maximum = 580
Slowest flit = 78189
Fragmentation average = 10.5329
	minimum = 0
	maximum = 73
Injected packet rate average = 0.00933333
	minimum = 0.001 (at node 49)
	maximum = 0.0235 (at node 37)
Accepted packet rate average = 0.00935677
	minimum = 0.0045 (at node 190)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.167958
	minimum = 0.018 (at node 49)
	maximum = 0.43 (at node 37)
Accepted flit rate average= 0.168719
	minimum = 0.081 (at node 190)
	maximum = 0.333 (at node 16)
Injected packet length average = 17.9955
Accepted packet length average = 18.0317
Total in-flight flits = 2740 (2740 measured)
latency change    = 0.0079458
throughput change = 0.0120393
Class 0:
Packet latency average = 139.644
	minimum = 22
	maximum = 845
Network latency average = 101.295
	minimum = 22
	maximum = 531
Slowest packet = 5155
Flit latency average = 81.7438
	minimum = 5
	maximum = 580
Slowest flit = 78189
Fragmentation average = 10.4385
	minimum = 0
	maximum = 73
Injected packet rate average = 0.00898611
	minimum = 0.001 (at node 49)
	maximum = 0.019 (at node 58)
Accepted packet rate average = 0.00910938
	minimum = 0.00533333 (at node 190)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.16178
	minimum = 0.018 (at node 49)
	maximum = 0.342 (at node 58)
Accepted flit rate average= 0.163783
	minimum = 0.098 (at node 190)
	maximum = 0.294 (at node 16)
Injected packet length average = 18.0033
Accepted packet length average = 17.9796
Total in-flight flits = 1845 (1845 measured)
latency change    = 0.0480875
throughput change = 0.030136
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6769 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 140.915 (1 samples)
	minimum = 22 (1 samples)
	maximum = 845 (1 samples)
Network latency average = 101.712 (1 samples)
	minimum = 22 (1 samples)
	maximum = 531 (1 samples)
Flit latency average = 80.556 (1 samples)
	minimum = 5 (1 samples)
	maximum = 580 (1 samples)
Fragmentation average = 10.4634 (1 samples)
	minimum = 0 (1 samples)
	maximum = 73 (1 samples)
Injected packet rate average = 0.00898611 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.019 (1 samples)
Accepted packet rate average = 0.00910938 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.16178 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.342 (1 samples)
Accepted flit rate average = 0.163783 (1 samples)
	minimum = 0.098 (1 samples)
	maximum = 0.294 (1 samples)
Injected packet size average = 18.0033 (1 samples)
Accepted packet size average = 17.9796 (1 samples)
Hops average = 5.08822 (1 samples)
Total run time 2.57007
