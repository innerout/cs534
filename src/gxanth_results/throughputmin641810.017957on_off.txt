BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.825
	minimum = 23
	maximum = 989
Network latency average = 233.986
	minimum = 23
	maximum = 972
Slowest packet = 104
Flit latency average = 158.536
	minimum = 6
	maximum = 955
Slowest flit = 1889
Fragmentation average = 149.772
	minimum = 0
	maximum = 927
Injected packet rate average = 0.0166927
	minimum = 0 (at node 5)
	maximum = 0.049 (at node 117)
Accepted packet rate average = 0.00892188
	minimum = 0.001 (at node 174)
	maximum = 0.018 (at node 132)
Injected flit rate average = 0.297625
	minimum = 0 (at node 5)
	maximum = 0.871 (at node 117)
Accepted flit rate average= 0.187724
	minimum = 0.047 (at node 174)
	maximum = 0.333 (at node 132)
Injected packet length average = 17.8296
Accepted packet length average = 21.0409
Total in-flight flits = 21647 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.649
	minimum = 23
	maximum = 1831
Network latency average = 375.415
	minimum = 23
	maximum = 1779
Slowest packet = 305
Flit latency average = 273.977
	minimum = 6
	maximum = 1820
Slowest flit = 10686
Fragmentation average = 209.137
	minimum = 0
	maximum = 1607
Injected packet rate average = 0.0159036
	minimum = 0.001 (at node 184)
	maximum = 0.0345 (at node 117)
Accepted packet rate average = 0.0102109
	minimum = 0.006 (at node 81)
	maximum = 0.015 (at node 131)
Injected flit rate average = 0.284445
	minimum = 0.018 (at node 184)
	maximum = 0.6125 (at node 117)
Accepted flit rate average= 0.200604
	minimum = 0.119 (at node 81)
	maximum = 0.292 (at node 98)
Injected packet length average = 17.8855
Accepted packet length average = 19.646
Total in-flight flits = 32912 (0 measured)
latency change    = 0.361184
throughput change = 0.0642071
Class 0:
Packet latency average = 824.871
	minimum = 28
	maximum = 2888
Network latency average = 647.611
	minimum = 27
	maximum = 2632
Slowest packet = 573
Flit latency average = 525.286
	minimum = 6
	maximum = 2842
Slowest flit = 8228
Fragmentation average = 270.07
	minimum = 0
	maximum = 2398
Injected packet rate average = 0.0139271
	minimum = 0 (at node 26)
	maximum = 0.04 (at node 145)
Accepted packet rate average = 0.0121875
	minimum = 0.006 (at node 77)
	maximum = 0.024 (at node 42)
Injected flit rate average = 0.250245
	minimum = 0 (at node 26)
	maximum = 0.713 (at node 145)
Accepted flit rate average= 0.222417
	minimum = 0.083 (at node 98)
	maximum = 0.404 (at node 42)
Injected packet length average = 17.9682
Accepted packet length average = 18.2496
Total in-flight flits = 38484 (0 measured)
latency change    = 0.436702
throughput change = 0.0980704
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 636.098
	minimum = 31
	maximum = 3162
Network latency average = 320.043
	minimum = 25
	maximum = 945
Slowest packet = 8797
Flit latency average = 695.97
	minimum = 6
	maximum = 3851
Slowest flit = 8242
Fragmentation average = 165.136
	minimum = 0
	maximum = 612
Injected packet rate average = 0.0112812
	minimum = 0 (at node 7)
	maximum = 0.04 (at node 85)
Accepted packet rate average = 0.0120625
	minimum = 0.005 (at node 32)
	maximum = 0.022 (at node 37)
Injected flit rate average = 0.201521
	minimum = 0 (at node 7)
	maximum = 0.716 (at node 85)
Accepted flit rate average= 0.215021
	minimum = 0.089 (at node 35)
	maximum = 0.373 (at node 37)
Injected packet length average = 17.8633
Accepted packet length average = 17.8256
Total in-flight flits = 36170 (20174 measured)
latency change    = 0.296768
throughput change = 0.0343959
Class 0:
Packet latency average = 889.849
	minimum = 29
	maximum = 3945
Network latency average = 459.276
	minimum = 25
	maximum = 1855
Slowest packet = 8797
Flit latency average = 725.821
	minimum = 6
	maximum = 4723
Slowest flit = 9588
Fragmentation average = 196.168
	minimum = 0
	maximum = 1463
Injected packet rate average = 0.011776
	minimum = 0 (at node 7)
	maximum = 0.0325 (at node 143)
Accepted packet rate average = 0.0118672
	minimum = 0.006 (at node 36)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.211388
	minimum = 0 (at node 35)
	maximum = 0.5865 (at node 143)
Accepted flit rate average= 0.211922
	minimum = 0.129 (at node 36)
	maximum = 0.323 (at node 129)
Injected packet length average = 17.9507
Accepted packet length average = 17.8578
Total in-flight flits = 38538 (29851 measured)
latency change    = 0.285162
throughput change = 0.0146231
Class 0:
Packet latency average = 1095.71
	minimum = 29
	maximum = 4703
Network latency average = 553.039
	minimum = 25
	maximum = 2939
Slowest packet = 8797
Flit latency average = 732.898
	minimum = 6
	maximum = 5828
Slowest flit = 5561
Fragmentation average = 213.153
	minimum = 0
	maximum = 2557
Injected packet rate average = 0.0120556
	minimum = 0 (at node 83)
	maximum = 0.0296667 (at node 143)
Accepted packet rate average = 0.0117309
	minimum = 0.007 (at node 2)
	maximum = 0.0166667 (at node 128)
Injected flit rate average = 0.216354
	minimum = 0 (at node 140)
	maximum = 0.534 (at node 143)
Accepted flit rate average= 0.209878
	minimum = 0.138667 (at node 146)
	maximum = 0.310333 (at node 128)
Injected packet length average = 17.9464
Accepted packet length average = 17.8911
Total in-flight flits = 42568 (36685 measured)
latency change    = 0.187882
throughput change = 0.00973612
Draining remaining packets ...
Class 0:
Remaining flits: 14310 14311 14312 14313 14314 14315 14316 14317 14318 14319 [...] (10803 flits)
Measured flits: 158508 158509 158510 158511 158512 158513 158514 158515 158516 158517 [...] (8402 flits)
Time taken is 7805 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1529.28 (1 samples)
	minimum = 29 (1 samples)
	maximum = 6395 (1 samples)
Network latency average = 889.342 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4673 (1 samples)
Flit latency average = 1051.9 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7211 (1 samples)
Fragmentation average = 226.06 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4060 (1 samples)
Injected packet rate average = 0.0120556 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0296667 (1 samples)
Accepted packet rate average = 0.0117309 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.216354 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.534 (1 samples)
Accepted flit rate average = 0.209878 (1 samples)
	minimum = 0.138667 (1 samples)
	maximum = 0.310333 (1 samples)
Injected packet size average = 17.9464 (1 samples)
Accepted packet size average = 17.8911 (1 samples)
Hops average = 5.1328 (1 samples)
Total run time 9.25702
