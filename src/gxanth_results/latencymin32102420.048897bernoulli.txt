BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.267
	minimum = 25
	maximum = 933
Network latency average = 304.6
	minimum = 22
	maximum = 871
Slowest packet = 918
Flit latency average = 267.598
	minimum = 5
	maximum = 870
Slowest flit = 17874
Fragmentation average = 85.3913
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0150938
	minimum = 0.002 (at node 174)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.293349
	minimum = 0.074 (at node 174)
	maximum = 0.471 (at node 44)
Injected packet length average = 17.8227
Accepted packet length average = 19.4351
Total in-flight flits = 104201 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.131
	minimum = 25
	maximum = 1804
Network latency average = 572.144
	minimum = 22
	maximum = 1708
Slowest packet = 993
Flit latency average = 528.539
	minimum = 5
	maximum = 1714
Slowest flit = 41943
Fragmentation average = 114.271
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0475937
	minimum = 0.0365 (at node 72)
	maximum = 0.0555 (at node 32)
Accepted packet rate average = 0.0160729
	minimum = 0.0085 (at node 116)
	maximum = 0.024 (at node 70)
Injected flit rate average = 0.852776
	minimum = 0.6485 (at node 72)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.302958
	minimum = 0.1605 (at node 116)
	maximum = 0.436 (at node 70)
Injected packet length average = 17.9178
Accepted packet length average = 18.849
Total in-flight flits = 212632 (0 measured)
latency change    = 0.457386
throughput change = 0.0317185
Class 0:
Packet latency average = 1396.02
	minimum = 24
	maximum = 2588
Network latency average = 1351.01
	minimum = 22
	maximum = 2532
Slowest packet = 3135
Flit latency average = 1316.97
	minimum = 5
	maximum = 2660
Slowest flit = 50370
Fragmentation average = 146.663
	minimum = 0
	maximum = 411
Injected packet rate average = 0.0485
	minimum = 0.034 (at node 2)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0174115
	minimum = 0.008 (at node 45)
	maximum = 0.03 (at node 159)
Injected flit rate average = 0.872458
	minimum = 0.608 (at node 2)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.313427
	minimum = 0.155 (at node 100)
	maximum = 0.561 (at node 78)
Injected packet length average = 17.9888
Accepted packet length average = 18.0012
Total in-flight flits = 320070 (0 measured)
latency change    = 0.567965
throughput change = 0.0334009
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 96.4771
	minimum = 23
	maximum = 476
Network latency average = 40.3935
	minimum = 22
	maximum = 265
Slowest packet = 27623
Flit latency average = 1847.13
	minimum = 5
	maximum = 3282
Slowest flit = 73889
Fragmentation average = 6.36253
	minimum = 0
	maximum = 46
Injected packet rate average = 0.0492031
	minimum = 0.031 (at node 153)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0175625
	minimum = 0.008 (at node 35)
	maximum = 0.031 (at node 44)
Injected flit rate average = 0.886292
	minimum = 0.541 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.317115
	minimum = 0.125 (at node 48)
	maximum = 0.568 (at node 129)
Injected packet length average = 18.0129
Accepted packet length average = 18.0563
Total in-flight flits = 429230 (156497 measured)
latency change    = 13.47
throughput change = 0.0116283
Class 0:
Packet latency average = 99.2061
	minimum = 23
	maximum = 476
Network latency average = 39.9639
	minimum = 22
	maximum = 265
Slowest packet = 27623
Flit latency average = 2130.58
	minimum = 5
	maximum = 4023
Slowest flit = 152724
Fragmentation average = 6.52041
	minimum = 0
	maximum = 58
Injected packet rate average = 0.0489766
	minimum = 0.0365 (at node 153)
	maximum = 0.056 (at node 164)
Accepted packet rate average = 0.0174141
	minimum = 0.01 (at node 48)
	maximum = 0.0275 (at node 129)
Injected flit rate average = 0.882107
	minimum = 0.657 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.313786
	minimum = 0.184 (at node 48)
	maximum = 0.5045 (at node 129)
Injected packet length average = 18.0108
Accepted packet length average = 18.0191
Total in-flight flits = 538102 (311912 measured)
latency change    = 0.0275087
throughput change = 0.0106063
Class 0:
Packet latency average = 101.293
	minimum = 22
	maximum = 476
Network latency average = 40.6722
	minimum = 22
	maximum = 366
Slowest packet = 27623
Flit latency average = 2399.65
	minimum = 5
	maximum = 4877
Slowest flit = 176435
Fragmentation average = 6.51993
	minimum = 0
	maximum = 58
Injected packet rate average = 0.0488854
	minimum = 0.0406667 (at node 152)
	maximum = 0.0556667 (at node 44)
Accepted packet rate average = 0.0174844
	minimum = 0.0123333 (at node 48)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.88021
	minimum = 0.733333 (at node 152)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.314134
	minimum = 0.221667 (at node 132)
	maximum = 0.442333 (at node 129)
Injected packet length average = 18.0056
Accepted packet length average = 17.9665
Total in-flight flits = 645973 (466588 measured)
latency change    = 0.0206056
throughput change = 0.00110533
Class 0:
Packet latency average = 102.679
	minimum = 22
	maximum = 484
Network latency average = 40.867
	minimum = 22
	maximum = 366
Slowest packet = 27623
Flit latency average = 2664.11
	minimum = 5
	maximum = 5603
Slowest flit = 222104
Fragmentation average = 6.35705
	minimum = 0
	maximum = 58
Injected packet rate average = 0.0489036
	minimum = 0.04025 (at node 156)
	maximum = 0.0555 (at node 0)
Accepted packet rate average = 0.017487
	minimum = 0.013 (at node 104)
	maximum = 0.02275 (at node 129)
Injected flit rate average = 0.880551
	minimum = 0.72625 (at node 156)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.314702
	minimum = 0.2355 (at node 141)
	maximum = 0.416 (at node 129)
Injected packet length average = 18.0058
Accepted packet length average = 17.9964
Total in-flight flits = 754423 (621725 measured)
latency change    = 0.0134938
throughput change = 0.00180534
Draining all recorded packets ...
Class 0:
Remaining flits: 261031 261032 261033 261034 261035 270089 273186 273187 273188 273189 [...] (864249 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (631163 flits)
Class 0:
Remaining flits: 292122 292123 292124 292125 292126 292127 292128 292129 292130 292131 [...] (971999 flits)
Measured flits: 496620 496621 496622 496623 496624 496625 496626 496627 496628 496629 [...] (624589 flits)
Class 0:
Remaining flits: 353898 353899 353900 353901 353902 353903 353904 353905 353906 353907 [...] (1079162 flits)
Measured flits: 496698 496699 496700 496701 496702 496703 496704 496705 496706 496707 [...] (605820 flits)
Class 0:
Remaining flits: 353904 353905 353906 353907 353908 353909 353910 353911 353912 353913 [...] (1181769 flits)
Measured flits: 496721 496722 496723 496724 496725 496726 496727 496728 496729 496730 [...] (571768 flits)
Class 0:
Remaining flits: 430290 430291 430292 430293 430294 430295 430296 430297 430298 430299 [...] (1277093 flits)
Measured flits: 496926 496927 496928 496929 496930 496931 496932 496933 496934 496935 [...] (528724 flits)
Class 0:
Remaining flits: 472986 472987 472988 472989 472990 472991 472992 472993 472994 472995 [...] (1374173 flits)
Measured flits: 502596 502597 502598 502599 502600 502601 502602 502603 502604 502605 [...] (482342 flits)
Class 0:
Remaining flits: 478810 478811 478812 478813 478814 478815 478816 478817 529902 529903 [...] (1469051 flits)
Measured flits: 529902 529903 529904 529905 529906 529907 529908 529909 529910 529911 [...] (434889 flits)
Class 0:
Remaining flits: 548838 548839 548840 548841 548842 548843 548844 548845 548846 548847 [...] (1564091 flits)
Measured flits: 548838 548839 548840 548841 548842 548843 548844 548845 548846 548847 [...] (387550 flits)
Class 0:
Remaining flits: 610021 610022 610023 610024 610025 610026 610027 610028 610029 610030 [...] (1658613 flits)
Measured flits: 610021 610022 610023 610024 610025 610026 610027 610028 610029 610030 [...] (339917 flits)
Class 0:
Remaining flits: 631428 631429 631430 631431 631432 631433 631434 631435 631436 631437 [...] (1754083 flits)
Measured flits: 631428 631429 631430 631431 631432 631433 631434 631435 631436 631437 [...] (293178 flits)
Class 0:
Remaining flits: 662364 662365 662366 662367 662368 662369 662370 662371 662372 662373 [...] (1847756 flits)
Measured flits: 662364 662365 662366 662367 662368 662369 662370 662371 662372 662373 [...] (246218 flits)
Class 0:
Remaining flits: 719478 719479 719480 719481 719482 719483 719484 719485 719486 719487 [...] (1939689 flits)
Measured flits: 719478 719479 719480 719481 719482 719483 719484 719485 719486 719487 [...] (199283 flits)
Class 0:
Remaining flits: 756612 756613 756614 756615 756616 756617 756618 756619 756620 756621 [...] (2031798 flits)
Measured flits: 756612 756613 756614 756615 756616 756617 756618 756619 756620 756621 [...] (153723 flits)
Class 0:
Remaining flits: 790304 790305 790306 790307 816407 824283 824284 824285 824286 824287 [...] (2119108 flits)
Measured flits: 790304 790305 790306 790307 816407 824283 824284 824285 824286 824287 [...] (111190 flits)
Class 0:
Remaining flits: 848610 848611 848612 848613 848614 848615 848616 848617 848618 848619 [...] (2200462 flits)
Measured flits: 848610 848611 848612 848613 848614 848615 848616 848617 848618 848619 [...] (73595 flits)
Class 0:
Remaining flits: 901656 901657 901658 901659 901660 901661 901662 901663 901664 901665 [...] (2285034 flits)
Measured flits: 901656 901657 901658 901659 901660 901661 901662 901663 901664 901665 [...] (43453 flits)
Class 0:
Remaining flits: 966528 966529 966530 966531 966532 966533 966534 966535 966536 966537 [...] (2368133 flits)
Measured flits: 966528 966529 966530 966531 966532 966533 966534 966535 966536 966537 [...] (22308 flits)
Class 0:
Remaining flits: 1027479 1027480 1027481 1027482 1027483 1027484 1027485 1027486 1027487 1027488 [...] (2451838 flits)
Measured flits: 1027479 1027480 1027481 1027482 1027483 1027484 1027485 1027486 1027487 1027488 [...] (10057 flits)
Class 0:
Remaining flits: 1047220 1047221 1048482 1048483 1048484 1048485 1048486 1048487 1048488 1048489 [...] (2531348 flits)
Measured flits: 1047220 1047221 1048482 1048483 1048484 1048485 1048486 1048487 1048488 1048489 [...] (3688 flits)
Class 0:
Remaining flits: 1061298 1061299 1061300 1061301 1061302 1061303 1061304 1061305 1061306 1061307 [...] (2607494 flits)
Measured flits: 1061298 1061299 1061300 1061301 1061302 1061303 1061304 1061305 1061306 1061307 [...] (1423 flits)
Class 0:
Remaining flits: 1137078 1137079 1137080 1137081 1137082 1137083 1137084 1137085 1137086 1137087 [...] (2672000 flits)
Measured flits: 1137078 1137079 1137080 1137081 1137082 1137083 1137084 1137085 1137086 1137087 [...] (299 flits)
Class 0:
Remaining flits: 1164798 1164799 1164800 1164801 1164802 1164803 1164804 1164805 1164806 1164807 [...] (2704923 flits)
Measured flits: 1164798 1164799 1164800 1164801 1164802 1164803 1164804 1164805 1164806 1164807 [...] (126 flits)
Class 0:
Remaining flits: 1164798 1164799 1164800 1164801 1164802 1164803 1164804 1164805 1164806 1164807 [...] (2720187 flits)
Measured flits: 1164798 1164799 1164800 1164801 1164802 1164803 1164804 1164805 1164806 1164807 [...] (26 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1258399 1258400 1258401 1258402 1258403 1258404 1258405 1258406 1258407 1258408 [...] (2672085 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1303284 1303285 1303286 1303287 1303288 1303289 1306062 1306063 1306064 1306065 [...] (2621519 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1306314 1306315 1306316 1306317 1306318 1306319 1306320 1306321 1306322 1306323 [...] (2570500 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1386466 1386467 1392678 1392679 1392680 1392681 1392682 1392683 1392684 1392685 [...] (2520294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1478301 1478302 1478303 1486818 1486819 1486820 1486821 1486822 1486823 1486824 [...] (2469768 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1541526 1541527 1541528 1541529 1541530 1541531 1541532 1541533 1541534 1541535 [...] (2418962 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1552014 1552015 1552016 1552017 1552018 1552019 1552020 1552021 1552022 1552023 [...] (2368366 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1566303 1566304 1566305 1571629 1571630 1571631 1571632 1571633 1615194 1615195 [...] (2318143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1615194 1615195 1615196 1615197 1615198 1615199 1615200 1615201 1615202 1615203 [...] (2267482 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1615203 1615204 1615205 1615206 1615207 1615208 1615209 1615210 1615211 1648386 [...] (2216615 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1684976 1684977 1684978 1684979 1721160 1721161 1721162 1721163 1721164 1721165 [...] (2166555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1721160 1721161 1721162 1721163 1721164 1721165 1721166 1721167 1721168 1721169 [...] (2114961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1721160 1721161 1721162 1721163 1721164 1721165 1721166 1721167 1721168 1721169 [...] (2064663 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1831824 1831825 1831826 1831827 1831828 1831829 1831830 1831831 1831832 1831833 [...] (2013846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1856358 1856359 1856360 1856361 1856362 1856363 1856364 1856365 1856366 1856367 [...] (1963450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888110 1888111 1888112 1888113 1888114 1888115 1888116 1888117 1888118 1888119 [...] (1912873 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1965366 1965367 1965368 1965369 1965370 1965371 1965372 1965373 1965374 1965375 [...] (1862115 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1989244 1989245 1989246 1989247 1989248 1989249 1989250 1989251 1989846 1989847 [...] (1811587 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1989846 1989847 1989848 1989849 1989850 1989851 1989852 1989853 1989854 1989855 [...] (1760909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1989846 1989847 1989848 1989849 1989850 1989851 1989852 1989853 1989854 1989855 [...] (1710472 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033820 2033821 2033822 2033823 2033824 2033825 2033826 2033827 2033828 2033829 [...] (1660085 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2094840 2094841 2094842 2094843 2094844 2094845 2094846 2094847 2094848 2094849 [...] (1609804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2158614 2158615 2158616 2158617 2158618 2158619 2158620 2158621 2158622 2158623 [...] (1560479 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2198710 2198711 2198712 2198713 2198714 2198715 2198716 2198717 2209966 2209967 [...] (1511364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2241756 2241757 2241758 2241759 2241760 2241761 2241762 2241763 2241764 2241765 [...] (1463393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2242098 2242099 2242100 2242101 2242102 2242103 2242104 2242105 2242106 2242107 [...] (1414882 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2242112 2242113 2242114 2242115 2288788 2288789 2302596 2302597 2302598 2302599 [...] (1367203 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2304144 2304145 2304146 2304147 2304148 2304149 2304150 2304151 2304152 2304153 [...] (1319594 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2307348 2307349 2307350 2307351 2307352 2307353 2307354 2307355 2307356 2307357 [...] (1271692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2307348 2307349 2307350 2307351 2307352 2307353 2307354 2307355 2307356 2307357 [...] (1223909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2307348 2307349 2307350 2307351 2307352 2307353 2307354 2307355 2307356 2307357 [...] (1175951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2435526 2435527 2435528 2435529 2435530 2435531 2435532 2435533 2435534 2435535 [...] (1128429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2435526 2435527 2435528 2435529 2435530 2435531 2435532 2435533 2435534 2435535 [...] (1080676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2435526 2435527 2435528 2435529 2435530 2435531 2435532 2435533 2435534 2435535 [...] (1032749 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2435526 2435527 2435528 2435529 2435530 2435531 2435532 2435533 2435534 2435535 [...] (984857 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2485242 2485243 2485244 2485245 2485246 2485247 2485248 2485249 2485250 2485251 [...] (937182 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2485258 2485259 2629980 2629981 2629982 2629983 2629984 2629985 2629986 2629987 [...] (889133 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2678724 2678725 2678726 2678727 2678728 2678729 2678730 2678731 2678732 2678733 [...] (841408 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2678724 2678725 2678726 2678727 2678728 2678729 2678730 2678731 2678732 2678733 [...] (793716 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2678724 2678725 2678726 2678727 2678728 2678729 2678730 2678731 2678732 2678733 [...] (746262 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2678724 2678725 2678726 2678727 2678728 2678729 2678730 2678731 2678732 2678733 [...] (698283 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2799252 2799253 2799254 2799255 2799256 2799257 2799258 2799259 2799260 2799261 [...] (650671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2799252 2799253 2799254 2799255 2799256 2799257 2799258 2799259 2799260 2799261 [...] (602758 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2826304 2826305 2885148 2885149 2885150 2885151 2885152 2885153 2885154 2885155 [...] (554994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2885148 2885149 2885150 2885151 2885152 2885153 2885154 2885155 2885156 2885157 [...] (507354 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2925702 2925703 2925704 2925705 2925706 2925707 2925708 2925709 2925710 2925711 [...] (459446 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2925702 2925703 2925704 2925705 2925706 2925707 2925708 2925709 2925710 2925711 [...] (411608 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3117870 3117871 3117872 3117873 3117874 3117875 3117876 3117877 3117878 3117879 [...] (363788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3139182 3139183 3139184 3139185 3139186 3139187 3139188 3139189 3139190 3139191 [...] (316150 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3139182 3139183 3139184 3139185 3139186 3139187 3139188 3139189 3139190 3139191 [...] (268414 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3257226 3257227 3257228 3257229 3257230 3257231 3257232 3257233 3257234 3257235 [...] (220608 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3288436 3288437 3293658 3293659 3293660 3293661 3293662 3293663 3293664 3293665 [...] (172895 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3359142 3359143 3359144 3359145 3359146 3359147 3359148 3359149 3359150 3359151 [...] (125280 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3417300 3417301 3417302 3417303 3417304 3417305 3417306 3417307 3417308 3417309 [...] (78128 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3483234 3483235 3483236 3483237 3483238 3483239 3483240 3483241 3483242 3483243 [...] (35967 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3690972 3690973 3690974 3690975 3690976 3690977 3690978 3690979 3690980 3690981 [...] (10924 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3881988 3881989 3881990 3881991 3881992 3881993 3881994 3881995 3881996 3881997 [...] (1829 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4273139 4273140 4273141 4273142 4273143 4273144 4273145 4406472 4406473 4406474 [...] (43 flits)
Measured flits: (0 flits)
Time taken is 88473 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10816.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23492 (1 samples)
Network latency average = 10752.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 23438 (1 samples)
Flit latency average = 28275 (1 samples)
	minimum = 5 (1 samples)
	maximum = 64097 (1 samples)
Fragmentation average = 175.927 (1 samples)
	minimum = 0 (1 samples)
	maximum = 488 (1 samples)
Injected packet rate average = 0.0489036 (1 samples)
	minimum = 0.04025 (1 samples)
	maximum = 0.0555 (1 samples)
Accepted packet rate average = 0.017487 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.02275 (1 samples)
Injected flit rate average = 0.880551 (1 samples)
	minimum = 0.72625 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.314702 (1 samples)
	minimum = 0.2355 (1 samples)
	maximum = 0.416 (1 samples)
Injected packet size average = 18.0058 (1 samples)
Accepted packet size average = 17.9964 (1 samples)
Hops average = 5.07437 (1 samples)
Total run time 115.418
