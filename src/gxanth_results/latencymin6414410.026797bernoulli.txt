BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.936
	minimum = 24
	maximum = 934
Network latency average = 292.454
	minimum = 23
	maximum = 924
Slowest packet = 342
Flit latency average = 212.59
	minimum = 6
	maximum = 907
Slowest flit = 6173
Fragmentation average = 177.345
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0100938
	minimum = 0.004 (at node 45)
	maximum = 0.019 (at node 91)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.215708
	minimum = 0.103 (at node 185)
	maximum = 0.358 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 21.3705
Total in-flight flits = 51194 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 522.072
	minimum = 24
	maximum = 1915
Network latency average = 514.824
	minimum = 23
	maximum = 1906
Slowest packet = 228
Flit latency average = 416.903
	minimum = 6
	maximum = 1943
Slowest flit = 3944
Fragmentation average = 239.992
	minimum = 0
	maximum = 1570
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0114245
	minimum = 0.0065 (at node 96)
	maximum = 0.018 (at node 67)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.226555
	minimum = 0.122 (at node 96)
	maximum = 0.3305 (at node 44)
Injected packet length average = 17.9276
Accepted packet length average = 19.8306
Total in-flight flits = 97431 (0 measured)
latency change    = 0.427406
throughput change = 0.0478752
Class 0:
Packet latency average = 1081.48
	minimum = 24
	maximum = 2806
Network latency average = 1073.52
	minimum = 24
	maximum = 2806
Slowest packet = 858
Flit latency average = 969.129
	minimum = 6
	maximum = 2818
Slowest flit = 15122
Fragmentation average = 337.908
	minimum = 0
	maximum = 2692
Injected packet rate average = 0.0266667
	minimum = 0.015 (at node 7)
	maximum = 0.039 (at node 24)
Accepted packet rate average = 0.0132656
	minimum = 0.004 (at node 135)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.479432
	minimum = 0.26 (at node 7)
	maximum = 0.692 (at node 24)
Accepted flit rate average= 0.244286
	minimum = 0.092 (at node 135)
	maximum = 0.484 (at node 129)
Injected packet length average = 17.9787
Accepted packet length average = 18.415
Total in-flight flits = 142688 (0 measured)
latency change    = 0.517259
throughput change = 0.072586
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 160.488
	minimum = 25
	maximum = 989
Network latency average = 150.955
	minimum = 25
	maximum = 982
Slowest packet = 15423
Flit latency average = 1387.63
	minimum = 6
	maximum = 3846
Slowest flit = 8420
Fragmentation average = 77.6296
	minimum = 1
	maximum = 661
Injected packet rate average = 0.0259583
	minimum = 0.007 (at node 8)
	maximum = 0.041 (at node 55)
Accepted packet rate average = 0.013151
	minimum = 0.005 (at node 86)
	maximum = 0.023 (at node 68)
Injected flit rate average = 0.466656
	minimum = 0.141 (at node 8)
	maximum = 0.723 (at node 55)
Accepted flit rate average= 0.24226
	minimum = 0.105 (at node 86)
	maximum = 0.401 (at node 119)
Injected packet length average = 17.9771
Accepted packet length average = 18.4214
Total in-flight flits = 185886 (79905 measured)
latency change    = 5.73866
throughput change = 0.00836307
Class 0:
Packet latency average = 494.241
	minimum = 23
	maximum = 1924
Network latency average = 473.187
	minimum = 23
	maximum = 1924
Slowest packet = 15459
Flit latency average = 1640.9
	minimum = 6
	maximum = 4649
Slowest flit = 30005
Fragmentation average = 167.129
	minimum = 0
	maximum = 1257
Injected packet rate average = 0.0251432
	minimum = 0.0115 (at node 8)
	maximum = 0.039 (at node 150)
Accepted packet rate average = 0.0132057
	minimum = 0.0075 (at node 190)
	maximum = 0.021 (at node 103)
Injected flit rate average = 0.452581
	minimum = 0.2115 (at node 64)
	maximum = 0.696 (at node 150)
Accepted flit rate average= 0.240271
	minimum = 0.141 (at node 48)
	maximum = 0.365 (at node 83)
Injected packet length average = 18.0001
Accepted packet length average = 18.1944
Total in-flight flits = 224214 (148517 measured)
latency change    = 0.675284
throughput change = 0.00828059
Class 0:
Packet latency average = 907.262
	minimum = 23
	maximum = 2887
Network latency average = 873.869
	minimum = 23
	maximum = 2887
Slowest packet = 15794
Flit latency average = 1874.25
	minimum = 6
	maximum = 5788
Slowest flit = 16071
Fragmentation average = 219.983
	minimum = 0
	maximum = 2254
Injected packet rate average = 0.0247448
	minimum = 0.0116667 (at node 172)
	maximum = 0.0346667 (at node 150)
Accepted packet rate average = 0.013184
	minimum = 0.00833333 (at node 86)
	maximum = 0.0186667 (at node 119)
Injected flit rate average = 0.445309
	minimum = 0.209667 (at node 172)
	maximum = 0.624 (at node 150)
Accepted flit rate average= 0.238901
	minimum = 0.158333 (at node 64)
	maximum = 0.327333 (at node 119)
Injected packet length average = 17.9961
Accepted packet length average = 18.1205
Total in-flight flits = 261635 (209060 measured)
latency change    = 0.455239
throughput change = 0.00573372
Class 0:
Packet latency average = 1377.87
	minimum = 23
	maximum = 3929
Network latency average = 1327.48
	minimum = 23
	maximum = 3929
Slowest packet = 15575
Flit latency average = 2131.39
	minimum = 6
	maximum = 6680
Slowest flit = 28500
Fragmentation average = 253.626
	minimum = 0
	maximum = 2883
Injected packet rate average = 0.0243672
	minimum = 0.0125 (at node 172)
	maximum = 0.0345 (at node 55)
Accepted packet rate average = 0.0130872
	minimum = 0.0085 (at node 64)
	maximum = 0.01725 (at node 119)
Injected flit rate average = 0.438611
	minimum = 0.225 (at node 172)
	maximum = 0.621 (at node 55)
Accepted flit rate average= 0.236878
	minimum = 0.15575 (at node 64)
	maximum = 0.31 (at node 47)
Injected packet length average = 18.0001
Accepted packet length average = 18.0999
Total in-flight flits = 297618 (262337 measured)
latency change    = 0.341546
throughput change = 0.00854212
Class 0:
Packet latency average = 1881.84
	minimum = 23
	maximum = 4977
Network latency average = 1812.25
	minimum = 23
	maximum = 4942
Slowest packet = 15542
Flit latency average = 2385.21
	minimum = 6
	maximum = 7502
Slowest flit = 43427
Fragmentation average = 278.061
	minimum = 0
	maximum = 3509
Injected packet rate average = 0.0243896
	minimum = 0.0128 (at node 148)
	maximum = 0.0322 (at node 55)
Accepted packet rate average = 0.0129844
	minimum = 0.0094 (at node 64)
	maximum = 0.0176 (at node 47)
Injected flit rate average = 0.438789
	minimum = 0.23 (at node 172)
	maximum = 0.5792 (at node 55)
Accepted flit rate average= 0.234632
	minimum = 0.1592 (at node 104)
	maximum = 0.3192 (at node 47)
Injected packet length average = 17.9908
Accepted packet length average = 18.0704
Total in-flight flits = 338893 (314530 measured)
latency change    = 0.267807
throughput change = 0.00956949
Class 0:
Packet latency average = 2381.43
	minimum = 23
	maximum = 5887
Network latency average = 2281.78
	minimum = 23
	maximum = 5879
Slowest packet = 15406
Flit latency average = 2630.92
	minimum = 6
	maximum = 8369
Slowest flit = 40175
Fragmentation average = 298.91
	minimum = 0
	maximum = 3524
Injected packet rate average = 0.0241979
	minimum = 0.013 (at node 148)
	maximum = 0.0311667 (at node 59)
Accepted packet rate average = 0.0129219
	minimum = 0.0095 (at node 89)
	maximum = 0.017 (at node 47)
Injected flit rate average = 0.435378
	minimum = 0.235667 (at node 172)
	maximum = 0.561 (at node 59)
Accepted flit rate average= 0.233106
	minimum = 0.172333 (at node 89)
	maximum = 0.301667 (at node 47)
Injected packet length average = 17.9924
Accepted packet length average = 18.0396
Total in-flight flits = 375918 (358088 measured)
latency change    = 0.209787
throughput change = 0.00654805
Class 0:
Packet latency average = 2807.83
	minimum = 23
	maximum = 6902
Network latency average = 2680.3
	minimum = 23
	maximum = 6902
Slowest packet = 15500
Flit latency average = 2864.63
	minimum = 6
	maximum = 9494
Slowest flit = 46153
Fragmentation average = 311.801
	minimum = 0
	maximum = 4784
Injected packet rate average = 0.0240662
	minimum = 0.0127143 (at node 172)
	maximum = 0.0305714 (at node 18)
Accepted packet rate average = 0.0128318
	minimum = 0.01 (at node 4)
	maximum = 0.0158571 (at node 47)
Injected flit rate average = 0.433184
	minimum = 0.228714 (at node 172)
	maximum = 0.548429 (at node 18)
Accepted flit rate average= 0.231222
	minimum = 0.173857 (at node 36)
	maximum = 0.282714 (at node 95)
Injected packet length average = 17.9997
Accepted packet length average = 18.0194
Total in-flight flits = 414136 (400618 measured)
latency change    = 0.15186
throughput change = 0.00814879
Draining all recorded packets ...
Class 0:
Remaining flits: 34525 34526 34527 34528 34529 34530 34531 34532 34533 34534 [...] (455351 flits)
Measured flits: 276692 276693 276694 276695 276876 276877 276878 276879 276880 276881 [...] (376855 flits)
Class 0:
Remaining flits: 34525 34526 34527 34528 34529 34530 34531 34532 34533 34534 [...] (495681 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (352704 flits)
Class 0:
Remaining flits: 34525 34526 34527 34528 34529 34530 34531 34532 34533 34534 [...] (533006 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (328798 flits)
Class 0:
Remaining flits: 34525 34526 34527 34528 34529 34530 34531 34532 34533 34534 [...] (562894 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (303568 flits)
Class 0:
Remaining flits: 34537 34538 34539 34540 34541 61520 61521 61522 61523 69408 [...] (590995 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (278784 flits)
Class 0:
Remaining flits: 34537 34538 34539 34540 34541 61520 61521 61522 61523 69408 [...] (614263 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (250767 flits)
Class 0:
Remaining flits: 34537 34538 34539 34540 34541 61520 61521 61522 61523 69408 [...] (635823 flits)
Measured flits: 276876 276877 276878 276879 276880 276881 276882 276883 276884 276885 [...] (223707 flits)
Class 0:
Remaining flits: 61520 61521 61522 61523 79884 79885 79886 79887 79888 79889 [...] (659004 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (198186 flits)
Class 0:
Remaining flits: 61520 61521 61522 61523 79884 79885 79886 79887 79888 79889 [...] (679729 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (175115 flits)
Class 0:
Remaining flits: 61520 61521 61522 61523 101970 101971 101972 101973 101974 101975 [...] (695301 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (155357 flits)
Class 0:
Remaining flits: 61520 61521 61522 61523 101970 101971 101972 101973 101974 101975 [...] (706501 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (137816 flits)
Class 0:
Remaining flits: 61520 61521 61522 61523 101970 101971 101972 101973 101974 101975 [...] (715726 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (122334 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (721212 flits)
Measured flits: 277056 277057 277058 277059 277060 277061 277062 277063 277064 277065 [...] (107649 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (727803 flits)
Measured flits: 281466 281467 281468 281469 281470 281471 281472 281473 281474 281475 [...] (94870 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (733110 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (82602 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (733022 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (71739 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (734234 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (62904 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (730321 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (55644 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (728653 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (47898 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (730520 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (41927 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (731124 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (36404 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (732323 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (31237 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (733636 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (27215 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (731907 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (23450 flits)
Class 0:
Remaining flits: 101970 101971 101972 101973 101974 101975 101976 101977 101978 101979 [...] (728089 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (20269 flits)
Class 0:
Remaining flits: 182814 182815 182816 182817 182818 182819 182820 182821 182822 182823 [...] (725475 flits)
Measured flits: 283698 283699 283700 283701 283702 283703 283704 283705 283706 283707 [...] (17740 flits)
Class 0:
Remaining flits: 241169 241170 241171 241172 241173 241174 241175 241176 241177 241178 [...] (729690 flits)
Measured flits: 283700 283701 283702 283703 283704 283705 283706 283707 283708 283709 [...] (15327 flits)
Class 0:
Remaining flits: 251820 251821 251822 251823 251824 251825 251826 251827 251828 251829 [...] (732273 flits)
Measured flits: 283700 283701 283702 283703 283704 283705 283706 283707 283708 283709 [...] (13264 flits)
Class 0:
Remaining flits: 265140 265141 265142 265143 265144 265145 265146 265147 265148 265149 [...] (732660 flits)
Measured flits: 283700 283701 283702 283703 283704 283705 283706 283707 283708 283709 [...] (11112 flits)
Class 0:
Remaining flits: 265140 265141 265142 265143 265144 265145 265146 265147 265148 265149 [...] (726447 flits)
Measured flits: 283700 283701 283702 283703 283704 283705 283706 283707 283708 283709 [...] (9893 flits)
Class 0:
Remaining flits: 265140 265141 265142 265143 265144 265145 265146 265147 265148 265149 [...] (725184 flits)
Measured flits: 283700 283701 283702 283703 283704 283705 283706 283707 283708 283709 [...] (8619 flits)
Class 0:
Remaining flits: 265146 265147 265148 265149 265150 265151 265152 265153 265154 265155 [...] (726376 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (7218 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (725718 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (6067 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (720561 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (5388 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (718619 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (4702 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (714035 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (4109 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (713718 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (3508 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (710528 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (3043 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (709907 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (2737 flits)
Class 0:
Remaining flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (707242 flits)
Measured flits: 283932 283933 283934 283935 283936 283937 283938 283939 283940 283941 [...] (2466 flits)
Class 0:
Remaining flits: 448038 448039 448040 448041 448042 448043 448044 448045 448046 448047 [...] (705288 flits)
Measured flits: 448038 448039 448040 448041 448042 448043 448044 448045 448046 448047 [...] (2009 flits)
Class 0:
Remaining flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (702184 flits)
Measured flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (1641 flits)
Class 0:
Remaining flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (699362 flits)
Measured flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (1480 flits)
Class 0:
Remaining flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (696562 flits)
Measured flits: 466956 466957 466958 466959 466960 466961 466962 466963 466964 466965 [...] (1316 flits)
Class 0:
Remaining flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (695676 flits)
Measured flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (1112 flits)
Class 0:
Remaining flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (693164 flits)
Measured flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (918 flits)
Class 0:
Remaining flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (690124 flits)
Measured flits: 525528 525529 525530 525531 525532 525533 525534 525535 525536 525537 [...] (792 flits)
Class 0:
Remaining flits: 642854 642855 642856 642857 642858 642859 642860 642861 642862 642863 [...] (687029 flits)
Measured flits: 642854 642855 642856 642857 642858 642859 642860 642861 642862 642863 [...] (682 flits)
Class 0:
Remaining flits: 653778 653779 653780 653781 653782 653783 653784 653785 653786 653787 [...] (684998 flits)
Measured flits: 653778 653779 653780 653781 653782 653783 653784 653785 653786 653787 [...] (609 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (683413 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (536 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (679336 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (449 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (681553 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (324 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (681058 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (293 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (684317 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (252 flits)
Class 0:
Remaining flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (691169 flits)
Measured flits: 672336 672337 672338 672339 672340 672341 672342 672343 672344 672345 [...] (216 flits)
Class 0:
Remaining flits: 769032 769033 769034 769035 769036 769037 769038 769039 769040 769041 [...] (691933 flits)
Measured flits: 769032 769033 769034 769035 769036 769037 769038 769039 769040 769041 [...] (162 flits)
Class 0:
Remaining flits: 769032 769033 769034 769035 769036 769037 769038 769039 769040 769041 [...] (687655 flits)
Measured flits: 769032 769033 769034 769035 769036 769037 769038 769039 769040 769041 [...] (162 flits)
Class 0:
Remaining flits: 769040 769041 769042 769043 769044 769045 769046 769047 769048 769049 [...] (692378 flits)
Measured flits: 769040 769041 769042 769043 769044 769045 769046 769047 769048 769049 [...] (154 flits)
Class 0:
Remaining flits: 771192 771193 771194 771195 771196 771197 771198 771199 771200 771201 [...] (692794 flits)
Measured flits: 771192 771193 771194 771195 771196 771197 771198 771199 771200 771201 [...] (144 flits)
Class 0:
Remaining flits: 814050 814051 814052 814053 814054 814055 814056 814057 814058 814059 [...] (693631 flits)
Measured flits: 814050 814051 814052 814053 814054 814055 814056 814057 814058 814059 [...] (126 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (697451 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (90 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (697893 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (90 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (693480 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (72 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (683496 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (62 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (679179 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (54 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (679304 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (36 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (678204 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (677599 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (680658 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (681712 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (687081 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (690121 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (688801 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (687815 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (683005 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (680390 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (677193 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (677405 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (676113 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (676799 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (677146 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (676964 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (675660 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Class 0:
Remaining flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (675069 flits)
Measured flits: 854190 854191 854192 854193 854194 854195 854196 854197 854198 854199 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1166436 1166437 1166438 1166439 1166440 1166441 1166442 1166443 1166444 1166445 [...] (640889 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1166436 1166437 1166438 1166439 1166440 1166441 1166442 1166443 1166444 1166445 [...] (607356 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1166436 1166437 1166438 1166439 1166440 1166441 1166442 1166443 1166444 1166445 [...] (575108 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1166436 1166437 1166438 1166439 1166440 1166441 1166442 1166443 1166444 1166445 [...] (543298 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1166439 1166440 1166441 1166442 1166443 1166444 1166445 1166446 1166447 1166448 [...] (510658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1166439 1166440 1166441 1166442 1166443 1166444 1166445 1166446 1166447 1166448 [...] (477731 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (445118 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (412004 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (379412 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (347053 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (315844 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (285374 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1337292 1337293 1337294 1337295 1337296 1337297 1337298 1337299 1337300 1337301 [...] (254646 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1536570 1536571 1536572 1536573 1536574 1536575 1536576 1536577 1536578 1536579 [...] (224851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1798506 1798507 1798508 1798509 1798510 1798511 1798512 1798513 1798514 1798515 [...] (196687 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (169395 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (142708 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (115960 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1884366 1884367 1884368 1884369 1884370 1884371 1884372 1884373 1884374 1884375 [...] (92320 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1893096 1893097 1893098 1893099 1893100 1893101 1893102 1893103 1893104 1893105 [...] (69001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1934478 1934479 1934480 1934481 1934482 1934483 1934484 1934485 1934486 1934487 [...] (49105 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2066400 2066401 2066402 2066403 2066404 2066405 2066406 2066407 2066408 2066409 [...] (32353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2201560 2201561 2307978 2307979 2307980 2307981 2307982 2307983 2307984 2307985 [...] (18694 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2486898 2486899 2486900 2486901 2486902 2486903 2486904 2486905 2486906 2486907 [...] (9337 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2657005 2657006 2657007 2657008 2657009 2657010 2657011 2657012 2657013 2657014 [...] (4546 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2771730 2771731 2771732 2771733 2771734 2771735 2771736 2771737 2771738 2771739 [...] (1587 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3227832 3227833 3227834 3227835 3227836 3227837 3227838 3227839 3227840 3227841 [...] (436 flits)
Measured flits: (0 flits)
Time taken is 121698 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9282.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 84327 (1 samples)
Network latency average = 8651.02 (1 samples)
	minimum = 23 (1 samples)
	maximum = 84327 (1 samples)
Flit latency average = 17039.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 91049 (1 samples)
Fragmentation average = 328.122 (1 samples)
	minimum = 0 (1 samples)
	maximum = 10710 (1 samples)
Injected packet rate average = 0.0240662 (1 samples)
	minimum = 0.0127143 (1 samples)
	maximum = 0.0305714 (1 samples)
Accepted packet rate average = 0.0128318 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0158571 (1 samples)
Injected flit rate average = 0.433184 (1 samples)
	minimum = 0.228714 (1 samples)
	maximum = 0.548429 (1 samples)
Accepted flit rate average = 0.231222 (1 samples)
	minimum = 0.173857 (1 samples)
	maximum = 0.282714 (1 samples)
Injected packet size average = 17.9997 (1 samples)
Accepted packet size average = 18.0194 (1 samples)
Hops average = 5.07492 (1 samples)
Total run time 245.399
