BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.428
	minimum = 27
	maximum = 883
Network latency average = 259.111
	minimum = 23
	maximum = 853
Slowest packet = 316
Flit latency average = 218.545
	minimum = 6
	maximum = 863
Slowest flit = 9477
Fragmentation average = 52.3203
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140208
	minimum = 0.004 (at node 4)
	maximum = 0.027 (at node 69)
Accepted packet rate average = 0.00892708
	minimum = 0.003 (at node 150)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.248615
	minimum = 0.072 (at node 4)
	maximum = 0.486 (at node 69)
Accepted flit rate average= 0.167771
	minimum = 0.054 (at node 150)
	maximum = 0.306 (at node 70)
Injected packet length average = 17.7318
Accepted packet length average = 18.7935
Total in-flight flits = 17972 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 596.6
	minimum = 27
	maximum = 1922
Network latency average = 390.344
	minimum = 23
	maximum = 1859
Slowest packet = 580
Flit latency average = 338.2
	minimum = 6
	maximum = 1802
Slowest flit = 10457
Fragmentation average = 55.7701
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0114714
	minimum = 0.005 (at node 52)
	maximum = 0.0215 (at node 130)
Accepted packet rate average = 0.00899219
	minimum = 0.004 (at node 174)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.204776
	minimum = 0.09 (at node 52)
	maximum = 0.3865 (at node 130)
Accepted flit rate average= 0.165276
	minimum = 0.078 (at node 174)
	maximum = 0.28 (at node 22)
Injected packet length average = 17.8511
Accepted packet length average = 18.38
Total in-flight flits = 17678 (0 measured)
latency change    = 0.503138
throughput change = 0.0150947
Class 0:
Packet latency average = 1539.65
	minimum = 520
	maximum = 2636
Network latency average = 551.175
	minimum = 23
	maximum = 2634
Slowest packet = 1293
Flit latency average = 486.964
	minimum = 6
	maximum = 2617
Slowest flit = 23291
Fragmentation average = 59.7129
	minimum = 0
	maximum = 130
Injected packet rate average = 0.00909896
	minimum = 0 (at node 116)
	maximum = 0.023 (at node 173)
Accepted packet rate average = 0.00897917
	minimum = 0.001 (at node 153)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.163219
	minimum = 0 (at node 116)
	maximum = 0.418 (at node 173)
Accepted flit rate average= 0.162328
	minimum = 0.018 (at node 153)
	maximum = 0.288 (at node 113)
Injected packet length average = 17.9382
Accepted packet length average = 18.0783
Total in-flight flits = 17939 (0 measured)
latency change    = 0.612509
throughput change = 0.0181602
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2146.3
	minimum = 967
	maximum = 3163
Network latency average = 320.503
	minimum = 23
	maximum = 951
Slowest packet = 6284
Flit latency average = 497.109
	minimum = 6
	maximum = 2654
Slowest flit = 33335
Fragmentation average = 51.8277
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00869792
	minimum = 0.001 (at node 18)
	maximum = 0.022 (at node 132)
Accepted packet rate average = 0.00882813
	minimum = 0.002 (at node 96)
	maximum = 0.018 (at node 119)
Injected flit rate average = 0.157073
	minimum = 0.003 (at node 162)
	maximum = 0.398 (at node 132)
Accepted flit rate average= 0.157349
	minimum = 0.038 (at node 184)
	maximum = 0.321 (at node 119)
Injected packet length average = 18.0587
Accepted packet length average = 17.8236
Total in-flight flits = 18022 (16629 measured)
latency change    = 0.282649
throughput change = 0.0316441
Class 0:
Packet latency average = 2611.11
	minimum = 967
	maximum = 4030
Network latency average = 449.574
	minimum = 23
	maximum = 1945
Slowest packet = 6284
Flit latency average = 492.166
	minimum = 6
	maximum = 2654
Slowest flit = 33335
Fragmentation average = 54.5571
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00889583
	minimum = 0.0015 (at node 77)
	maximum = 0.0175 (at node 30)
Accepted packet rate average = 0.00899479
	minimum = 0.0045 (at node 180)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.159716
	minimum = 0.0205 (at node 162)
	maximum = 0.318 (at node 132)
Accepted flit rate average= 0.16082
	minimum = 0.076 (at node 180)
	maximum = 0.274 (at node 16)
Injected packet length average = 17.954
Accepted packet length average = 17.8793
Total in-flight flits = 17618 (17529 measured)
latency change    = 0.178014
throughput change = 0.0215853
Class 0:
Packet latency average = 2950.84
	minimum = 967
	maximum = 4748
Network latency average = 485.972
	minimum = 23
	maximum = 2244
Slowest packet = 6284
Flit latency average = 486.959
	minimum = 6
	maximum = 2654
Slowest flit = 33335
Fragmentation average = 55.8706
	minimum = 0
	maximum = 130
Injected packet rate average = 0.00899653
	minimum = 0.00333333 (at node 140)
	maximum = 0.0186667 (at node 51)
Accepted packet rate average = 0.0090191
	minimum = 0.005 (at node 4)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.161925
	minimum = 0.0603333 (at node 142)
	maximum = 0.331667 (at node 51)
Accepted flit rate average= 0.16176
	minimum = 0.09 (at node 4)
	maximum = 0.264 (at node 16)
Injected packet length average = 17.9986
Accepted packet length average = 17.9353
Total in-flight flits = 18131 (18131 measured)
latency change    = 0.115129
throughput change = 0.00581171
Class 0:
Packet latency average = 3272.27
	minimum = 967
	maximum = 5495
Network latency average = 506.957
	minimum = 23
	maximum = 2672
Slowest packet = 6284
Flit latency average = 488.556
	minimum = 6
	maximum = 2654
Slowest flit = 33335
Fragmentation average = 56.32
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00895443
	minimum = 0.003 (at node 140)
	maximum = 0.01925 (at node 51)
Accepted packet rate average = 0.00898307
	minimum = 0.00475 (at node 4)
	maximum = 0.01325 (at node 129)
Injected flit rate average = 0.161297
	minimum = 0.05475 (at node 140)
	maximum = 0.3425 (at node 51)
Accepted flit rate average= 0.161548
	minimum = 0.0855 (at node 4)
	maximum = 0.2365 (at node 16)
Injected packet length average = 18.0131
Accepted packet length average = 17.9836
Total in-flight flits = 17836 (17836 measured)
latency change    = 0.0982289
throughput change = 0.00131379
Class 0:
Packet latency average = 3599.39
	minimum = 967
	maximum = 6359
Network latency average = 515.531
	minimum = 23
	maximum = 2726
Slowest packet = 6284
Flit latency average = 488.151
	minimum = 6
	maximum = 2697
Slowest flit = 177995
Fragmentation average = 56.8845
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00894167
	minimum = 0.003 (at node 140)
	maximum = 0.0164 (at node 51)
Accepted packet rate average = 0.00897708
	minimum = 0.0058 (at node 4)
	maximum = 0.0124 (at node 129)
Injected flit rate average = 0.161051
	minimum = 0.0546 (at node 140)
	maximum = 0.2952 (at node 51)
Accepted flit rate average= 0.161393
	minimum = 0.1062 (at node 4)
	maximum = 0.2222 (at node 16)
Injected packet length average = 18.0113
Accepted packet length average = 17.9783
Total in-flight flits = 17694 (17694 measured)
latency change    = 0.0908813
throughput change = 0.000963295
Class 0:
Packet latency average = 3924.51
	minimum = 967
	maximum = 7012
Network latency average = 519.526
	minimum = 23
	maximum = 2726
Slowest packet = 6284
Flit latency average = 485.745
	minimum = 6
	maximum = 2697
Slowest flit = 177995
Fragmentation average = 56.8419
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00892795
	minimum = 0.00416667 (at node 140)
	maximum = 0.0151667 (at node 51)
Accepted packet rate average = 0.00896615
	minimum = 0.00666667 (at node 5)
	maximum = 0.0126667 (at node 128)
Injected flit rate average = 0.160724
	minimum = 0.0751667 (at node 140)
	maximum = 0.270333 (at node 51)
Accepted flit rate average= 0.161209
	minimum = 0.117667 (at node 36)
	maximum = 0.2295 (at node 128)
Injected packet length average = 18.0023
Accepted packet length average = 17.9798
Total in-flight flits = 17518 (17518 measured)
latency change    = 0.0828431
throughput change = 0.00113832
Class 0:
Packet latency average = 4240.04
	minimum = 967
	maximum = 8200
Network latency average = 526.211
	minimum = 23
	maximum = 3289
Slowest packet = 6284
Flit latency average = 487.915
	minimum = 6
	maximum = 3238
Slowest flit = 220175
Fragmentation average = 56.8732
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00890774
	minimum = 0.00457143 (at node 18)
	maximum = 0.0148571 (at node 51)
Accepted packet rate average = 0.00894866
	minimum = 0.00614286 (at node 36)
	maximum = 0.0125714 (at node 129)
Injected flit rate average = 0.160382
	minimum = 0.0822857 (at node 18)
	maximum = 0.265 (at node 51)
Accepted flit rate average= 0.160786
	minimum = 0.110286 (at node 36)
	maximum = 0.223429 (at node 128)
Injected packet length average = 18.0048
Accepted packet length average = 17.9676
Total in-flight flits = 17447 (17447 measured)
latency change    = 0.0744173
throughput change = 0.00263386
Draining all recorded packets ...
Class 0:
Remaining flits: 250722 250723 250724 250725 250726 250727 250728 250729 250730 250731 [...] (17883 flits)
Measured flits: 250722 250723 250724 250725 250726 250727 250728 250729 250730 250731 [...] (17883 flits)
Class 0:
Remaining flits: 315641 315642 315643 315644 315645 315646 315647 329364 329365 329366 [...] (17947 flits)
Measured flits: 315641 315642 315643 315644 315645 315646 315647 329364 329365 329366 [...] (17947 flits)
Class 0:
Remaining flits: 355680 355681 355682 355683 355684 355685 355686 355687 355688 355689 [...] (17997 flits)
Measured flits: 355680 355681 355682 355683 355684 355685 355686 355687 355688 355689 [...] (17997 flits)
Class 0:
Remaining flits: 389251 389252 389253 389254 389255 389256 389257 389258 389259 389260 [...] (17540 flits)
Measured flits: 389251 389252 389253 389254 389255 389256 389257 389258 389259 389260 [...] (17540 flits)
Class 0:
Remaining flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (17730 flits)
Measured flits: 398268 398269 398270 398271 398272 398273 398274 398275 398276 398277 [...] (17730 flits)
Class 0:
Remaining flits: 400374 400375 400376 400377 400378 400379 400380 400381 400382 400383 [...] (17413 flits)
Measured flits: 400374 400375 400376 400377 400378 400379 400380 400381 400382 400383 [...] (17413 flits)
Class 0:
Remaining flits: 456688 456689 456690 456691 456692 456693 456694 456695 458208 458209 [...] (18078 flits)
Measured flits: 456688 456689 456690 456691 456692 456693 456694 456695 458208 458209 [...] (18078 flits)
Class 0:
Remaining flits: 508374 508375 508376 508377 508378 508379 508380 508381 508382 508383 [...] (17774 flits)
Measured flits: 508374 508375 508376 508377 508378 508379 508380 508381 508382 508383 [...] (17693 flits)
Class 0:
Remaining flits: 526990 526991 526992 526993 526994 526995 526996 526997 526998 526999 [...] (17463 flits)
Measured flits: 526990 526991 526992 526993 526994 526995 526996 526997 526998 526999 [...] (17322 flits)
Class 0:
Remaining flits: 576090 576091 576092 576093 576094 576095 576096 576097 576098 576099 [...] (17764 flits)
Measured flits: 576090 576091 576092 576093 576094 576095 576096 576097 576098 576099 [...] (17582 flits)
Class 0:
Remaining flits: 578394 578395 578396 578397 578398 578399 578400 578401 578402 578403 [...] (18062 flits)
Measured flits: 578394 578395 578396 578397 578398 578399 578400 578401 578402 578403 [...] (17689 flits)
Class 0:
Remaining flits: 622116 622117 622118 622119 622120 622121 622122 622123 622124 622125 [...] (17523 flits)
Measured flits: 622116 622117 622118 622119 622120 622121 622122 622123 622124 622125 [...] (17037 flits)
Class 0:
Remaining flits: 653382 653383 653384 653385 653386 653387 653388 653389 653390 653391 [...] (17696 flits)
Measured flits: 653382 653383 653384 653385 653386 653387 653388 653389 653390 653391 [...] (16550 flits)
Class 0:
Remaining flits: 657697 657698 657699 657700 657701 670284 670285 670286 670287 670288 [...] (17704 flits)
Measured flits: 657697 657698 657699 657700 657701 670284 670285 670286 670287 670288 [...] (15558 flits)
Class 0:
Remaining flits: 699623 700000 700001 705294 705295 705296 705297 705298 705299 705300 [...] (17592 flits)
Measured flits: 699623 700000 700001 705294 705295 705296 705297 705298 705299 705300 [...] (13938 flits)
Class 0:
Remaining flits: 734083 734084 734085 734086 734087 734088 734089 734090 734091 734092 [...] (17648 flits)
Measured flits: 734083 734084 734085 734086 734087 734088 734089 734090 734091 734092 [...] (12542 flits)
Class 0:
Remaining flits: 776297 776298 776299 776300 776301 776302 776303 777996 777997 777998 [...] (17404 flits)
Measured flits: 777996 777997 777998 777999 778000 778001 778002 778003 778004 778005 [...] (9931 flits)
Class 0:
Remaining flits: 803014 803015 824940 824941 824942 824943 824944 824945 824946 824947 [...] (17900 flits)
Measured flits: 803014 803015 824940 824941 824942 824943 824944 824945 824946 824947 [...] (7455 flits)
Class 0:
Remaining flits: 838440 838441 838442 838443 838444 838445 838446 838447 838448 838449 [...] (17771 flits)
Measured flits: 851994 851995 851996 851997 851998 851999 852000 852001 852002 852003 [...] (5463 flits)
Class 0:
Remaining flits: 855378 855379 855380 855381 855382 855383 855384 855385 855386 855387 [...] (17773 flits)
Measured flits: 879102 879103 879104 879105 879106 879107 879108 879109 879110 879111 [...] (3318 flits)
Class 0:
Remaining flits: 873144 873145 873146 873147 873148 873149 873150 873151 873152 873153 [...] (17495 flits)
Measured flits: 895482 895483 895484 895485 895486 895487 895488 895489 895490 895491 [...] (1564 flits)
Class 0:
Remaining flits: 920574 920575 920576 920577 920578 920579 920580 920581 920582 920583 [...] (17884 flits)
Measured flits: 954126 954127 954128 954129 954130 954131 954132 954133 954134 954135 [...] (713 flits)
Class 0:
Remaining flits: 947700 947701 947702 947703 947704 947705 947706 947707 947708 947709 [...] (17498 flits)
Measured flits: 979470 979471 979472 979473 979474 979475 979476 979477 979478 979479 [...] (312 flits)
Class 0:
Remaining flits: 960192 960193 960194 960195 960196 960197 960198 960199 960200 960201 [...] (17848 flits)
Measured flits: 1029812 1029813 1029814 1029815 1036152 1036153 1036154 1036155 1036156 1036157 [...] (124 flits)
Class 0:
Remaining flits: 1038438 1038439 1038440 1038441 1038442 1038443 1038444 1038445 1038446 1038447 [...] (17821 flits)
Measured flits: 1089234 1089235 1089236 1089237 1089238 1089239 1089240 1089241 1089242 1089243 [...] (72 flits)
Class 0:
Remaining flits: 1051290 1051291 1051292 1051293 1051294 1051295 1051296 1051297 1051298 1051299 [...] (17827 flits)
Measured flits: 1118682 1118683 1118684 1118685 1118686 1118687 1118688 1118689 1118690 1118691 [...] (18 flits)
Draining remaining packets ...
Time taken is 36937 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10086.5 (1 samples)
	minimum = 967 (1 samples)
	maximum = 26102 (1 samples)
Network latency average = 550.393 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4226 (1 samples)
Flit latency average = 490.746 (1 samples)
	minimum = 6 (1 samples)
	maximum = 4209 (1 samples)
Fragmentation average = 57.8676 (1 samples)
	minimum = 0 (1 samples)
	maximum = 158 (1 samples)
Injected packet rate average = 0.00890774 (1 samples)
	minimum = 0.00457143 (1 samples)
	maximum = 0.0148571 (1 samples)
Accepted packet rate average = 0.00894866 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.0125714 (1 samples)
Injected flit rate average = 0.160382 (1 samples)
	minimum = 0.0822857 (1 samples)
	maximum = 0.265 (1 samples)
Accepted flit rate average = 0.160786 (1 samples)
	minimum = 0.110286 (1 samples)
	maximum = 0.223429 (1 samples)
Injected packet size average = 18.0048 (1 samples)
Accepted packet size average = 17.9676 (1 samples)
Hops average = 5.06624 (1 samples)
Total run time 27.0725
