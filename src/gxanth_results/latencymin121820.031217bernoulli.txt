BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 282.193
	minimum = 22
	maximum = 862
Network latency average = 228.165
	minimum = 22
	maximum = 815
Slowest packet = 836
Flit latency average = 199.953
	minimum = 5
	maximum = 798
Slowest flit = 15065
Fragmentation average = 21.5325
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0187135
	minimum = 0.008 (at node 80)
	maximum = 0.03 (at node 17)
Accepted packet rate average = 0.0135469
	minimum = 0.004 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.333224
	minimum = 0.144 (at node 80)
	maximum = 0.54 (at node 17)
Accepted flit rate average= 0.249302
	minimum = 0.079 (at node 174)
	maximum = 0.397 (at node 44)
Injected packet length average = 17.8066
Accepted packet length average = 18.4029
Total in-flight flits = 18626 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 548.204
	minimum = 22
	maximum = 1573
Network latency average = 294.001
	minimum = 22
	maximum = 1261
Slowest packet = 836
Flit latency average = 261.334
	minimum = 5
	maximum = 1244
Slowest flit = 27917
Fragmentation average = 21.9427
	minimum = 0
	maximum = 122
Injected packet rate average = 0.0163047
	minimum = 0.008 (at node 12)
	maximum = 0.024 (at node 27)
Accepted packet rate average = 0.0138177
	minimum = 0.008 (at node 83)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.291424
	minimum = 0.137 (at node 12)
	maximum = 0.4305 (at node 27)
Accepted flit rate average= 0.251216
	minimum = 0.147 (at node 83)
	maximum = 0.3465 (at node 22)
Injected packet length average = 17.8737
Accepted packet length average = 18.1807
Total in-flight flits = 18067 (0 measured)
latency change    = 0.485241
throughput change = 0.00761919
Class 0:
Packet latency average = 1355.62
	minimum = 410
	maximum = 2398
Network latency average = 362.275
	minimum = 22
	maximum = 1738
Slowest packet = 5589
Flit latency average = 327.289
	minimum = 5
	maximum = 1721
Slowest flit = 45070
Fragmentation average = 21.5703
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0136667
	minimum = 0.002 (at node 113)
	maximum = 0.026 (at node 176)
Accepted packet rate average = 0.0136615
	minimum = 0.006 (at node 4)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.246536
	minimum = 0.036 (at node 180)
	maximum = 0.469 (at node 176)
Accepted flit rate average= 0.245583
	minimum = 0.106 (at node 54)
	maximum = 0.458 (at node 34)
Injected packet length average = 18.0393
Accepted packet length average = 17.9764
Total in-flight flits = 18345 (0 measured)
latency change    = 0.595606
throughput change = 0.0229365
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1944.43
	minimum = 966
	maximum = 2854
Network latency average = 273.812
	minimum = 22
	maximum = 900
Slowest packet = 9008
Flit latency average = 327.969
	minimum = 5
	maximum = 1710
Slowest flit = 128258
Fragmentation average = 20.8865
	minimum = 0
	maximum = 116
Injected packet rate average = 0.0139948
	minimum = 0.003 (at node 176)
	maximum = 0.031 (at node 178)
Accepted packet rate average = 0.0138958
	minimum = 0.006 (at node 48)
	maximum = 0.026 (at node 129)
Injected flit rate average = 0.25113
	minimum = 0.054 (at node 176)
	maximum = 0.557 (at node 178)
Accepted flit rate average= 0.250516
	minimum = 0.108 (at node 48)
	maximum = 0.455 (at node 129)
Injected packet length average = 17.9445
Accepted packet length average = 18.0281
Total in-flight flits = 18414 (18216 measured)
latency change    = 0.302818
throughput change = 0.0196886
Class 0:
Packet latency average = 2256.21
	minimum = 966
	maximum = 3762
Network latency average = 325.348
	minimum = 22
	maximum = 1562
Slowest packet = 9008
Flit latency average = 325.956
	minimum = 5
	maximum = 1733
Slowest flit = 128267
Fragmentation average = 22.1057
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0140026
	minimum = 0.005 (at node 89)
	maximum = 0.025 (at node 178)
Accepted packet rate average = 0.0139219
	minimum = 0.009 (at node 84)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.251935
	minimum = 0.0895 (at node 176)
	maximum = 0.4575 (at node 178)
Accepted flit rate average= 0.250589
	minimum = 0.162 (at node 84)
	maximum = 0.3915 (at node 129)
Injected packet length average = 17.992
Accepted packet length average = 17.9996
Total in-flight flits = 18833 (18833 measured)
latency change    = 0.13819
throughput change = 0.000290982
Class 0:
Packet latency average = 2544.93
	minimum = 966
	maximum = 4313
Network latency average = 342.931
	minimum = 22
	maximum = 1951
Slowest packet = 9008
Flit latency average = 328.888
	minimum = 5
	maximum = 1934
Slowest flit = 208634
Fragmentation average = 22.3454
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0139288
	minimum = 0.00566667 (at node 53)
	maximum = 0.021 (at node 1)
Accepted packet rate average = 0.0138906
	minimum = 0.009 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.250814
	minimum = 0.102 (at node 53)
	maximum = 0.382 (at node 112)
Accepted flit rate average= 0.250168
	minimum = 0.163333 (at node 84)
	maximum = 0.36 (at node 128)
Injected packet length average = 18.0069
Accepted packet length average = 18.0099
Total in-flight flits = 18698 (18698 measured)
latency change    = 0.11345
throughput change = 0.00167942
Class 0:
Packet latency average = 2833.14
	minimum = 966
	maximum = 5068
Network latency average = 348.075
	minimum = 22
	maximum = 2286
Slowest packet = 9008
Flit latency average = 328.175
	minimum = 5
	maximum = 2269
Slowest flit = 210365
Fragmentation average = 22.3114
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0139323
	minimum = 0.007 (at node 53)
	maximum = 0.0205 (at node 41)
Accepted packet rate average = 0.0139154
	minimum = 0.01 (at node 89)
	maximum = 0.01925 (at node 129)
Injected flit rate average = 0.250818
	minimum = 0.126 (at node 53)
	maximum = 0.366 (at node 41)
Accepted flit rate average= 0.250514
	minimum = 0.18225 (at node 89)
	maximum = 0.3405 (at node 129)
Injected packet length average = 18.0026
Accepted packet length average = 18.0027
Total in-flight flits = 18550 (18550 measured)
latency change    = 0.101726
throughput change = 0.00138084
Class 0:
Packet latency average = 3110.58
	minimum = 966
	maximum = 5864
Network latency average = 352.966
	minimum = 22
	maximum = 2286
Slowest packet = 9008
Flit latency average = 329.33
	minimum = 5
	maximum = 2269
Slowest flit = 210365
Fragmentation average = 22.4556
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0139677
	minimum = 0.0078 (at node 92)
	maximum = 0.0206 (at node 162)
Accepted packet rate average = 0.0139427
	minimum = 0.0104 (at node 89)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.25132
	minimum = 0.1404 (at node 92)
	maximum = 0.3674 (at node 162)
Accepted flit rate average= 0.251157
	minimum = 0.1854 (at node 89)
	maximum = 0.3346 (at node 128)
Injected packet length average = 17.9929
Accepted packet length average = 18.0135
Total in-flight flits = 18488 (18488 measured)
latency change    = 0.0891915
throughput change = 0.00256002
Class 0:
Packet latency average = 3390.31
	minimum = 966
	maximum = 6577
Network latency average = 353.84
	minimum = 22
	maximum = 2286
Slowest packet = 9008
Flit latency average = 328.035
	minimum = 5
	maximum = 2269
Slowest flit = 210365
Fragmentation average = 22.4914
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0139583
	minimum = 0.00766667 (at node 92)
	maximum = 0.0218333 (at node 162)
Accepted packet rate average = 0.0139618
	minimum = 0.0111667 (at node 42)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.251303
	minimum = 0.138 (at node 92)
	maximum = 0.3905 (at node 162)
Accepted flit rate average= 0.25145
	minimum = 0.201 (at node 42)
	maximum = 0.33 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 18.0098
Total in-flight flits = 18187 (18187 measured)
latency change    = 0.0825093
throughput change = 0.0011627
Class 0:
Packet latency average = 3669.16
	minimum = 966
	maximum = 7363
Network latency average = 355.752
	minimum = 22
	maximum = 2286
Slowest packet = 9008
Flit latency average = 328.359
	minimum = 5
	maximum = 2269
Slowest flit = 210365
Fragmentation average = 22.605
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0139903
	minimum = 0.00828571 (at node 92)
	maximum = 0.0214286 (at node 162)
Accepted packet rate average = 0.0139568
	minimum = 0.0108571 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.251757
	minimum = 0.149143 (at node 92)
	maximum = 0.385429 (at node 162)
Accepted flit rate average= 0.251336
	minimum = 0.197143 (at node 79)
	maximum = 0.323857 (at node 138)
Injected packet length average = 17.9951
Accepted packet length average = 18.008
Total in-flight flits = 18842 (18842 measured)
latency change    = 0.075998
throughput change = 0.000453924
Draining all recorded packets ...
Class 0:
Remaining flits: 475618 475619 475620 475621 475622 475623 475624 475625 475626 475627 [...] (18491 flits)
Measured flits: 475618 475619 475620 475621 475622 475623 475624 475625 475626 475627 [...] (18491 flits)
Class 0:
Remaining flits: 529916 529917 529918 529919 530856 530857 530858 530859 530860 530861 [...] (17945 flits)
Measured flits: 529916 529917 529918 529919 530856 530857 530858 530859 530860 530861 [...] (17945 flits)
Class 0:
Remaining flits: 587952 587953 587954 587955 587956 587957 587958 587959 587960 587961 [...] (18827 flits)
Measured flits: 587952 587953 587954 587955 587956 587957 587958 587959 587960 587961 [...] (18827 flits)
Class 0:
Remaining flits: 631900 631901 631902 631903 631904 631905 631906 631907 636755 636756 [...] (18321 flits)
Measured flits: 631900 631901 631902 631903 631904 631905 631906 631907 636755 636756 [...] (18321 flits)
Class 0:
Remaining flits: 648893 648894 648895 648896 648897 648898 648899 649764 649765 649766 [...] (18516 flits)
Measured flits: 648893 648894 648895 648896 648897 648898 648899 649764 649765 649766 [...] (18516 flits)
Class 0:
Remaining flits: 719190 719191 719192 719193 719194 719195 719196 719197 719198 719199 [...] (18116 flits)
Measured flits: 719190 719191 719192 719193 719194 719195 719196 719197 719198 719199 [...] (18116 flits)
Class 0:
Remaining flits: 767196 767197 767198 767199 767200 767201 767202 767203 767204 767205 [...] (18063 flits)
Measured flits: 767196 767197 767198 767199 767200 767201 767202 767203 767204 767205 [...] (18026 flits)
Class 0:
Remaining flits: 802512 802513 802514 802515 802516 802517 802518 802519 802520 802521 [...] (17467 flits)
Measured flits: 802512 802513 802514 802515 802516 802517 802518 802519 802520 802521 [...] (17326 flits)
Class 0:
Remaining flits: 866394 866395 866396 866397 866398 866399 866400 866401 866402 866403 [...] (18166 flits)
Measured flits: 866394 866395 866396 866397 866398 866399 866400 866401 866402 866403 [...] (17125 flits)
Class 0:
Remaining flits: 919440 919441 919442 919443 919444 919445 919446 919447 919448 919449 [...] (18599 flits)
Measured flits: 919440 919441 919442 919443 919444 919445 919446 919447 919448 919449 [...] (15773 flits)
Class 0:
Remaining flits: 977259 977260 977261 977262 977263 977264 977265 977266 977267 977268 [...] (19001 flits)
Measured flits: 977259 977260 977261 977262 977263 977264 977265 977266 977267 977268 [...] (13705 flits)
Class 0:
Remaining flits: 1015894 1015895 1015896 1015897 1015898 1015899 1015900 1015901 1017126 1017127 [...] (18467 flits)
Measured flits: 1015894 1015895 1015896 1015897 1015898 1015899 1015900 1015901 1017126 1017127 [...] (10255 flits)
Class 0:
Remaining flits: 1038942 1038943 1038944 1038945 1038946 1038947 1038948 1038949 1038950 1038951 [...] (18542 flits)
Measured flits: 1050912 1050913 1050914 1050915 1050916 1050917 1050918 1050919 1050920 1050921 [...] (7290 flits)
Class 0:
Remaining flits: 1092618 1092619 1092620 1092621 1092622 1092623 1092624 1092625 1092626 1092627 [...] (18013 flits)
Measured flits: 1092618 1092619 1092620 1092621 1092622 1092623 1092624 1092625 1092626 1092627 [...] (4183 flits)
Class 0:
Remaining flits: 1141137 1141138 1141139 1141140 1141141 1141142 1141143 1141144 1141145 1147122 [...] (18058 flits)
Measured flits: 1147122 1147123 1147124 1147125 1147126 1147127 1147128 1147129 1147130 1147131 [...] (2008 flits)
Class 0:
Remaining flits: 1191251 1191252 1191253 1191254 1191255 1191256 1191257 1193382 1193383 1193384 [...] (18409 flits)
Measured flits: 1191251 1191252 1191253 1191254 1191255 1191256 1191257 1193580 1193581 1193582 [...] (871 flits)
Class 0:
Remaining flits: 1232388 1232389 1232390 1232391 1232392 1232393 1232394 1232395 1232396 1232397 [...] (18529 flits)
Measured flits: 1284606 1284607 1284608 1284609 1284610 1284611 1284612 1284613 1284614 1284615 [...] (179 flits)
Draining remaining packets ...
Time taken is 28475 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7294.01 (1 samples)
	minimum = 966 (1 samples)
	maximum = 17610 (1 samples)
Network latency average = 363.882 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2286 (1 samples)
Flit latency average = 330.543 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2269 (1 samples)
Fragmentation average = 22.7967 (1 samples)
	minimum = 0 (1 samples)
	maximum = 136 (1 samples)
Injected packet rate average = 0.0139903 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0214286 (1 samples)
Accepted packet rate average = 0.0139568 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.251757 (1 samples)
	minimum = 0.149143 (1 samples)
	maximum = 0.385429 (1 samples)
Accepted flit rate average = 0.251336 (1 samples)
	minimum = 0.197143 (1 samples)
	maximum = 0.323857 (1 samples)
Injected packet size average = 17.9951 (1 samples)
Accepted packet size average = 18.008 (1 samples)
Hops average = 5.0667 (1 samples)
Total run time 24.9241
