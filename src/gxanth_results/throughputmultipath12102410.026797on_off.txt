BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 337.625
	minimum = 23
	maximum = 976
Network latency average = 261.591
	minimum = 23
	maximum = 869
Slowest packet = 85
Flit latency average = 226.002
	minimum = 6
	maximum = 916
Slowest flit = 6145
Fragmentation average = 55.4494
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.00972396
	minimum = 0.004 (at node 64)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.183167
	minimum = 0.074 (at node 150)
	maximum = 0.324 (at node 152)
Injected packet length average = 17.8353
Accepted packet length average = 18.8366
Total in-flight flits = 48532 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.733
	minimum = 23
	maximum = 1973
Network latency average = 476.758
	minimum = 23
	maximum = 1721
Slowest packet = 85
Flit latency average = 440.778
	minimum = 6
	maximum = 1778
Slowest flit = 18131
Fragmentation average = 61.3608
	minimum = 0
	maximum = 179
Injected packet rate average = 0.0241354
	minimum = 0 (at node 30)
	maximum = 0.054 (at node 91)
Accepted packet rate average = 0.0101849
	minimum = 0.005 (at node 150)
	maximum = 0.016 (at node 140)
Injected flit rate average = 0.432633
	minimum = 0 (at node 30)
	maximum = 0.972 (at node 91)
Accepted flit rate average= 0.187688
	minimum = 0.09 (at node 150)
	maximum = 0.2905 (at node 140)
Injected packet length average = 17.9252
Accepted packet length average = 18.428
Total in-flight flits = 94752 (0 measured)
latency change    = 0.415604
throughput change = 0.024087
Class 0:
Packet latency average = 1279.13
	minimum = 30
	maximum = 2928
Network latency average = 1132.07
	minimum = 27
	maximum = 2632
Slowest packet = 1928
Flit latency average = 1100.72
	minimum = 6
	maximum = 2615
Slowest flit = 26675
Fragmentation average = 67.154
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0254219
	minimum = 0 (at node 59)
	maximum = 0.056 (at node 63)
Accepted packet rate average = 0.0104844
	minimum = 0.004 (at node 27)
	maximum = 0.022 (at node 37)
Injected flit rate average = 0.456974
	minimum = 0 (at node 59)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.187318
	minimum = 0.064 (at node 94)
	maximum = 0.396 (at node 37)
Injected packet length average = 17.9756
Accepted packet length average = 17.8664
Total in-flight flits = 146645 (0 measured)
latency change    = 0.548339
throughput change = 0.00197414
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 290.597
	minimum = 27
	maximum = 1671
Network latency average = 110.449
	minimum = 25
	maximum = 962
Slowest packet = 14152
Flit latency average = 1563.23
	minimum = 6
	maximum = 3630
Slowest flit = 28295
Fragmentation average = 18.6075
	minimum = 0
	maximum = 128
Injected packet rate average = 0.0261354
	minimum = 0 (at node 4)
	maximum = 0.056 (at node 69)
Accepted packet rate average = 0.0105052
	minimum = 0.003 (at node 79)
	maximum = 0.019 (at node 106)
Injected flit rate average = 0.47037
	minimum = 0 (at node 4)
	maximum = 1 (at node 10)
Accepted flit rate average= 0.190349
	minimum = 0.046 (at node 84)
	maximum = 0.334 (at node 106)
Injected packet length average = 17.9974
Accepted packet length average = 18.1195
Total in-flight flits = 200422 (83454 measured)
latency change    = 3.40173
throughput change = 0.0159247
Class 0:
Packet latency average = 459.563
	minimum = 26
	maximum = 2430
Network latency average = 275.037
	minimum = 25
	maximum = 1973
Slowest packet = 14152
Flit latency average = 1821.73
	minimum = 6
	maximum = 4457
Slowest flit = 30994
Fragmentation average = 24.858
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0255208
	minimum = 0.001 (at node 180)
	maximum = 0.0555 (at node 10)
Accepted packet rate average = 0.0103672
	minimum = 0.004 (at node 84)
	maximum = 0.0165 (at node 34)
Injected flit rate average = 0.459354
	minimum = 0.0185 (at node 180)
	maximum = 1 (at node 10)
Accepted flit rate average= 0.186411
	minimum = 0.0675 (at node 84)
	maximum = 0.295 (at node 34)
Injected packet length average = 17.9992
Accepted packet length average = 17.9809
Total in-flight flits = 251499 (160927 measured)
latency change    = 0.367668
throughput change = 0.0211226
Class 0:
Packet latency average = 852.347
	minimum = 26
	maximum = 3266
Network latency average = 671.23
	minimum = 25
	maximum = 2898
Slowest packet = 14152
Flit latency average = 2098.62
	minimum = 6
	maximum = 5463
Slowest flit = 41273
Fragmentation average = 33.8048
	minimum = 0
	maximum = 163
Injected packet rate average = 0.0253559
	minimum = 0.00466667 (at node 13)
	maximum = 0.0543333 (at node 10)
Accepted packet rate average = 0.0104063
	minimum = 0.006 (at node 96)
	maximum = 0.0153333 (at node 46)
Injected flit rate average = 0.456316
	minimum = 0.084 (at node 13)
	maximum = 0.980667 (at node 10)
Accepted flit rate average= 0.187623
	minimum = 0.112 (at node 49)
	maximum = 0.278333 (at node 46)
Injected packet length average = 17.9964
Accepted packet length average = 18.0299
Total in-flight flits = 301500 (234386 measured)
latency change    = 0.460826
throughput change = 0.00645872
Draining remaining packets ...
Class 0:
Remaining flits: 57492 57493 57494 57495 57496 57497 57498 57499 57500 57501 [...] (270708 flits)
Measured flits: 254682 254683 254684 254685 254686 254687 254688 254689 254690 254691 [...] (222760 flits)
Class 0:
Remaining flits: 72936 72937 72938 72939 72940 72941 72942 72943 72944 72945 [...] (240048 flits)
Measured flits: 254682 254683 254684 254685 254686 254687 254688 254689 254690 254691 [...] (207174 flits)
Class 0:
Remaining flits: 92340 92341 92342 92343 92344 92345 92346 92347 92348 92349 [...] (209340 flits)
Measured flits: 254682 254683 254684 254685 254686 254687 254688 254689 254690 254691 [...] (187028 flits)
Class 0:
Remaining flits: 105804 105805 105806 105807 105808 105809 105810 105811 105812 105813 [...] (178914 flits)
Measured flits: 254700 254701 254702 254703 254704 254705 254706 254707 254708 254709 [...] (164344 flits)
Class 0:
Remaining flits: 117738 117739 117740 117741 117742 117743 117744 117745 117746 117747 [...] (150510 flits)
Measured flits: 254718 254719 254720 254721 254722 254723 254724 254725 254726 254727 [...] (141355 flits)
Class 0:
Remaining flits: 122490 122491 122492 122493 122494 122495 122496 122497 122498 122499 [...] (122687 flits)
Measured flits: 254754 254755 254756 254757 254758 254759 254760 254761 254762 254763 [...] (117288 flits)
Class 0:
Remaining flits: 122490 122491 122492 122493 122494 122495 122496 122497 122498 122499 [...] (94285 flits)
Measured flits: 254754 254755 254756 254757 254758 254759 254760 254761 254762 254763 [...] (91571 flits)
Class 0:
Remaining flits: 139230 139231 139232 139233 139234 139235 139236 139237 139238 139239 [...] (67131 flits)
Measured flits: 254754 254755 254756 254757 254758 254759 254760 254761 254762 254763 [...] (65836 flits)
Class 0:
Remaining flits: 156560 156561 156562 156563 170460 170461 170462 170463 170464 170465 [...] (42121 flits)
Measured flits: 258642 258643 258644 258645 258646 258647 258648 258649 258650 258651 [...] (41640 flits)
Class 0:
Remaining flits: 232920 232921 232922 232923 232924 232925 232926 232927 232928 232929 [...] (19106 flits)
Measured flits: 260388 260389 260390 260391 260392 260393 260394 260395 260396 260397 [...] (19052 flits)
Class 0:
Remaining flits: 321513 321514 321515 333162 333163 333164 333165 333166 333167 333168 [...] (3871 flits)
Measured flits: 321513 321514 321515 333162 333163 333164 333165 333166 333167 333168 [...] (3871 flits)
Time taken is 17724 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6889.1 (1 samples)
	minimum = 26 (1 samples)
	maximum = 14263 (1 samples)
Network latency average = 6660.44 (1 samples)
	minimum = 25 (1 samples)
	maximum = 13383 (1 samples)
Flit latency average = 5765.24 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13648 (1 samples)
Fragmentation average = 68.9491 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.0253559 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0543333 (1 samples)
Accepted packet rate average = 0.0104063 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.456316 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.980667 (1 samples)
Accepted flit rate average = 0.187623 (1 samples)
	minimum = 0.112 (1 samples)
	maximum = 0.278333 (1 samples)
Injected packet size average = 17.9964 (1 samples)
Accepted packet size average = 18.0299 (1 samples)
Hops average = 5.11207 (1 samples)
Total run time 10.1925
