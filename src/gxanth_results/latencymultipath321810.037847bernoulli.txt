BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 361.274
	minimum = 23
	maximum = 950
Network latency average = 330.309
	minimum = 23
	maximum = 930
Slowest packet = 241
Flit latency average = 259.18
	minimum = 6
	maximum = 939
Slowest flit = 4658
Fragmentation average = 162.663
	minimum = 0
	maximum = 808
Injected packet rate average = 0.0220208
	minimum = 0.007 (at node 144)
	maximum = 0.033 (at node 49)
Accepted packet rate average = 0.0101458
	minimum = 0.003 (at node 127)
	maximum = 0.02 (at node 91)
Injected flit rate average = 0.389031
	minimum = 0.122 (at node 144)
	maximum = 0.591 (at node 49)
Accepted flit rate average= 0.208927
	minimum = 0.078 (at node 174)
	maximum = 0.382 (at node 76)
Injected packet length average = 17.6665
Accepted packet length average = 20.5924
Total in-flight flits = 36242 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 709.515
	minimum = 23
	maximum = 1904
Network latency average = 567.907
	minimum = 23
	maximum = 1884
Slowest packet = 699
Flit latency average = 463.823
	minimum = 6
	maximum = 1867
Slowest flit = 12599
Fragmentation average = 194.521
	minimum = 0
	maximum = 1687
Injected packet rate average = 0.0166276
	minimum = 0.006 (at node 144)
	maximum = 0.027 (at node 147)
Accepted packet rate average = 0.0106667
	minimum = 0.0055 (at node 43)
	maximum = 0.0185 (at node 22)
Injected flit rate average = 0.295758
	minimum = 0.1 (at node 188)
	maximum = 0.486 (at node 147)
Accepted flit rate average= 0.20382
	minimum = 0.099 (at node 43)
	maximum = 0.333 (at node 22)
Injected packet length average = 17.7872
Accepted packet length average = 19.1082
Total in-flight flits = 36951 (0 measured)
latency change    = 0.490816
throughput change = 0.0250553
Class 0:
Packet latency average = 1723.33
	minimum = 339
	maximum = 2740
Network latency average = 962.48
	minimum = 24
	maximum = 2739
Slowest packet = 1147
Flit latency average = 839.739
	minimum = 6
	maximum = 2809
Slowest flit = 19654
Fragmentation average = 213.473
	minimum = 0
	maximum = 2253
Injected packet rate average = 0.0107292
	minimum = 0 (at node 20)
	maximum = 0.031 (at node 154)
Accepted packet rate average = 0.010875
	minimum = 0.004 (at node 22)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.19424
	minimum = 0 (at node 45)
	maximum = 0.55 (at node 154)
Accepted flit rate average= 0.196932
	minimum = 0.066 (at node 190)
	maximum = 0.385 (at node 152)
Injected packet length average = 18.1039
Accepted packet length average = 18.1087
Total in-flight flits = 36382 (0 measured)
latency change    = 0.588289
throughput change = 0.0349766
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2400.15
	minimum = 1137
	maximum = 3455
Network latency average = 374.099
	minimum = 23
	maximum = 948
Slowest packet = 8477
Flit latency average = 897.5
	minimum = 6
	maximum = 3790
Slowest flit = 22278
Fragmentation average = 128.109
	minimum = 0
	maximum = 619
Injected packet rate average = 0.0105625
	minimum = 0 (at node 26)
	maximum = 0.029 (at node 127)
Accepted packet rate average = 0.0111094
	minimum = 0.002 (at node 113)
	maximum = 0.023 (at node 120)
Injected flit rate average = 0.189193
	minimum = 0 (at node 74)
	maximum = 0.524 (at node 127)
Accepted flit rate average= 0.197307
	minimum = 0.049 (at node 113)
	maximum = 0.385 (at node 120)
Injected packet length average = 17.9117
Accepted packet length average = 17.7604
Total in-flight flits = 34949 (23405 measured)
latency change    = 0.28199
throughput change = 0.00190059
Class 0:
Packet latency average = 2904.18
	minimum = 1137
	maximum = 4655
Network latency average = 612.052
	minimum = 23
	maximum = 1990
Slowest packet = 8477
Flit latency average = 913.781
	minimum = 6
	maximum = 4861
Slowest flit = 15052
Fragmentation average = 154.243
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0107708
	minimum = 0 (at node 77)
	maximum = 0.0225 (at node 105)
Accepted packet rate average = 0.0109297
	minimum = 0.006 (at node 1)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.193576
	minimum = 0 (at node 168)
	maximum = 0.4025 (at node 105)
Accepted flit rate average= 0.19524
	minimum = 0.109 (at node 1)
	maximum = 0.328 (at node 129)
Injected packet length average = 17.9722
Accepted packet length average = 17.8632
Total in-flight flits = 35876 (32633 measured)
latency change    = 0.173555
throughput change = 0.0105906
Class 0:
Packet latency average = 3348.3
	minimum = 1137
	maximum = 5533
Network latency average = 735.32
	minimum = 23
	maximum = 2899
Slowest packet = 8477
Flit latency average = 903.428
	minimum = 6
	maximum = 5587
Slowest flit = 15065
Fragmentation average = 170.074
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0108559
	minimum = 0.000333333 (at node 38)
	maximum = 0.0223333 (at node 42)
Accepted packet rate average = 0.0108976
	minimum = 0.007 (at node 32)
	maximum = 0.016 (at node 129)
Injected flit rate average = 0.194934
	minimum = 0.00533333 (at node 38)
	maximum = 0.401333 (at node 42)
Accepted flit rate average= 0.194609
	minimum = 0.123333 (at node 36)
	maximum = 0.288 (at node 129)
Injected packet length average = 17.9565
Accepted packet length average = 17.8581
Total in-flight flits = 36787 (35705 measured)
latency change    = 0.13264
throughput change = 0.00323832
Class 0:
Packet latency average = 3747.13
	minimum = 1137
	maximum = 6094
Network latency average = 813.452
	minimum = 23
	maximum = 3883
Slowest packet = 8477
Flit latency average = 907.371
	minimum = 6
	maximum = 6327
Slowest flit = 25775
Fragmentation average = 178.851
	minimum = 0
	maximum = 3017
Injected packet rate average = 0.0109128
	minimum = 0.0035 (at node 74)
	maximum = 0.02 (at node 105)
Accepted packet rate average = 0.010862
	minimum = 0.00675 (at node 36)
	maximum = 0.015 (at node 128)
Injected flit rate average = 0.196135
	minimum = 0.06275 (at node 74)
	maximum = 0.359 (at node 105)
Accepted flit rate average= 0.19485
	minimum = 0.12225 (at node 36)
	maximum = 0.268 (at node 128)
Injected packet length average = 17.973
Accepted packet length average = 17.9387
Total in-flight flits = 37523 (37328 measured)
latency change    = 0.106436
throughput change = 0.00123626
Class 0:
Packet latency average = 4135.99
	minimum = 1137
	maximum = 6794
Network latency average = 873.609
	minimum = 23
	maximum = 4810
Slowest packet = 8477
Flit latency average = 912.669
	minimum = 6
	maximum = 6832
Slowest flit = 51047
Fragmentation average = 184.914
	minimum = 0
	maximum = 3185
Injected packet rate average = 0.0108198
	minimum = 0.0028 (at node 74)
	maximum = 0.0186 (at node 22)
Accepted packet rate average = 0.0108573
	minimum = 0.0068 (at node 36)
	maximum = 0.0148 (at node 165)
Injected flit rate average = 0.194612
	minimum = 0.0506 (at node 74)
	maximum = 0.3344 (at node 22)
Accepted flit rate average= 0.195033
	minimum = 0.1268 (at node 36)
	maximum = 0.2636 (at node 165)
Injected packet length average = 17.9867
Accepted packet length average = 17.9634
Total in-flight flits = 36062 (36026 measured)
latency change    = 0.0940189
throughput change = 0.000938675
Class 0:
Packet latency average = 4508.18
	minimum = 1137
	maximum = 7889
Network latency average = 919.334
	minimum = 23
	maximum = 5049
Slowest packet = 8477
Flit latency average = 916.558
	minimum = 6
	maximum = 6832
Slowest flit = 51047
Fragmentation average = 190.723
	minimum = 0
	maximum = 4003
Injected packet rate average = 0.0108733
	minimum = 0.00333333 (at node 74)
	maximum = 0.0173333 (at node 22)
Accepted packet rate average = 0.0108811
	minimum = 0.008 (at node 36)
	maximum = 0.0141667 (at node 129)
Injected flit rate average = 0.195445
	minimum = 0.0591667 (at node 74)
	maximum = 0.312 (at node 22)
Accepted flit rate average= 0.195543
	minimum = 0.149167 (at node 36)
	maximum = 0.254667 (at node 129)
Injected packet length average = 17.9749
Accepted packet length average = 17.9709
Total in-flight flits = 36441 (36423 measured)
latency change    = 0.0825588
throughput change = 0.00260404
Class 0:
Packet latency average = 4875.47
	minimum = 1137
	maximum = 8792
Network latency average = 946.471
	minimum = 23
	maximum = 6931
Slowest packet = 8477
Flit latency average = 918.623
	minimum = 6
	maximum = 6832
Slowest flit = 51047
Fragmentation average = 194.93
	minimum = 0
	maximum = 4003
Injected packet rate average = 0.0108854
	minimum = 0.00314286 (at node 88)
	maximum = 0.0178571 (at node 57)
Accepted packet rate average = 0.0108884
	minimum = 0.00857143 (at node 64)
	maximum = 0.014 (at node 129)
Injected flit rate average = 0.195861
	minimum = 0.0547143 (at node 88)
	maximum = 0.321571 (at node 57)
Accepted flit rate average= 0.195858
	minimum = 0.150286 (at node 80)
	maximum = 0.247714 (at node 129)
Injected packet length average = 17.993
Accepted packet length average = 17.9878
Total in-flight flits = 36453 (36453 measured)
latency change    = 0.0753332
throughput change = 0.00161011
Draining all recorded packets ...
Class 0:
Remaining flits: 203529 203530 203531 203532 203533 203534 203535 203536 203537 203538 [...] (37165 flits)
Measured flits: 203529 203530 203531 203532 203533 203534 203535 203536 203537 203538 [...] (37165 flits)
Class 0:
Remaining flits: 229255 229256 229257 229258 229259 229260 229261 229262 229263 229264 [...] (35532 flits)
Measured flits: 229255 229256 229257 229258 229259 229260 229261 229262 229263 229264 [...] (35532 flits)
Class 0:
Remaining flits: 247455 247456 247457 247458 247459 247460 247461 247462 247463 312823 [...] (37846 flits)
Measured flits: 247455 247456 247457 247458 247459 247460 247461 247462 247463 312823 [...] (37846 flits)
Class 0:
Remaining flits: 329740 329741 345600 345601 345602 345603 345604 345605 345606 345607 [...] (37081 flits)
Measured flits: 329740 329741 345600 345601 345602 345603 345604 345605 345606 345607 [...] (37081 flits)
Class 0:
Remaining flits: 363762 363763 363764 363765 363766 363767 363768 363769 363770 363771 [...] (36178 flits)
Measured flits: 363762 363763 363764 363765 363766 363767 363768 363769 363770 363771 [...] (36178 flits)
Class 0:
Remaining flits: 363762 363763 363764 363765 363766 363767 363768 363769 363770 363771 [...] (37093 flits)
Measured flits: 363762 363763 363764 363765 363766 363767 363768 363769 363770 363771 [...] (37093 flits)
Class 0:
Remaining flits: 363771 363772 363773 363774 363775 363776 363777 363778 363779 424566 [...] (34781 flits)
Measured flits: 363771 363772 363773 363774 363775 363776 363777 363778 363779 424566 [...] (34781 flits)
Class 0:
Remaining flits: 468108 468109 468110 468111 468112 468113 468114 468115 468116 468117 [...] (37462 flits)
Measured flits: 468108 468109 468110 468111 468112 468113 468114 468115 468116 468117 [...] (37462 flits)
Class 0:
Remaining flits: 491420 491421 491422 491423 491424 491425 491426 491427 491428 491429 [...] (37057 flits)
Measured flits: 491420 491421 491422 491423 491424 491425 491426 491427 491428 491429 [...] (37057 flits)
Class 0:
Remaining flits: 510457 510458 510459 510460 510461 565542 565543 565544 565545 565546 [...] (37365 flits)
Measured flits: 510457 510458 510459 510460 510461 565542 565543 565544 565545 565546 [...] (37365 flits)
Class 0:
Remaining flits: 579888 579889 579890 579891 579892 579893 579894 579895 579896 579897 [...] (36979 flits)
Measured flits: 579888 579889 579890 579891 579892 579893 579894 579895 579896 579897 [...] (36979 flits)
Class 0:
Remaining flits: 623930 623931 623932 623933 676134 676135 676136 676137 676138 676139 [...] (37155 flits)
Measured flits: 623930 623931 623932 623933 676134 676135 676136 676137 676138 676139 [...] (37155 flits)
Class 0:
Remaining flits: 686847 686848 686849 686850 686851 686852 686853 686854 686855 686856 [...] (37123 flits)
Measured flits: 686847 686848 686849 686850 686851 686852 686853 686854 686855 686856 [...] (37015 flits)
Class 0:
Remaining flits: 687258 687259 687260 687261 687262 687263 687264 687265 687266 687267 [...] (37334 flits)
Measured flits: 687258 687259 687260 687261 687262 687263 687264 687265 687266 687267 [...] (37100 flits)
Class 0:
Remaining flits: 741708 741709 741710 741711 741712 741713 741714 741715 741716 741717 [...] (37907 flits)
Measured flits: 741708 741709 741710 741711 741712 741713 741714 741715 741716 741717 [...] (37479 flits)
Class 0:
Remaining flits: 741708 741709 741710 741711 741712 741713 741714 741715 741716 741717 [...] (36317 flits)
Measured flits: 741708 741709 741710 741711 741712 741713 741714 741715 741716 741717 [...] (35725 flits)
Class 0:
Remaining flits: 812412 812413 812414 812415 812416 812417 812418 812419 812420 812421 [...] (34727 flits)
Measured flits: 812412 812413 812414 812415 812416 812417 812418 812419 812420 812421 [...] (33311 flits)
Class 0:
Remaining flits: 825696 825697 825698 825699 825700 825701 825702 825703 825704 825705 [...] (36459 flits)
Measured flits: 825696 825697 825698 825699 825700 825701 825702 825703 825704 825705 [...] (34579 flits)
Class 0:
Remaining flits: 826902 826903 826904 826905 826906 826907 826908 826909 826910 826911 [...] (35491 flits)
Measured flits: 826902 826903 826904 826905 826906 826907 826908 826909 826910 826911 [...] (30252 flits)
Class 0:
Remaining flits: 891450 891451 891452 891453 891454 891455 891456 891457 891458 891459 [...] (37016 flits)
Measured flits: 891450 891451 891452 891453 891454 891455 891456 891457 891458 891459 [...] (28380 flits)
Class 0:
Remaining flits: 969012 969013 969014 969015 969016 969017 969018 969019 969020 969021 [...] (38249 flits)
Measured flits: 969012 969013 969014 969015 969016 969017 969018 969019 969020 969021 [...] (25997 flits)
Class 0:
Remaining flits: 1011240 1011241 1011242 1011243 1011244 1011245 1011246 1011247 1011248 1011249 [...] (35190 flits)
Measured flits: 1011240 1011241 1011242 1011243 1011244 1011245 1011246 1011247 1011248 1011249 [...] (21346 flits)
Class 0:
Remaining flits: 1011246 1011247 1011248 1011249 1011250 1011251 1011252 1011253 1011254 1011255 [...] (35534 flits)
Measured flits: 1011246 1011247 1011248 1011249 1011250 1011251 1011252 1011253 1011254 1011255 [...] (17178 flits)
Class 0:
Remaining flits: 1057047 1057048 1057049 1080553 1080554 1080555 1080556 1080557 1092978 1092979 [...] (37212 flits)
Measured flits: 1057047 1057048 1057049 1080553 1080554 1080555 1080556 1080557 1098090 1098091 [...] (15507 flits)
Class 0:
Remaining flits: 1092983 1092984 1092985 1092986 1092987 1092988 1092989 1092990 1092991 1092992 [...] (37324 flits)
Measured flits: 1098107 1105435 1105436 1105437 1105438 1105439 1105440 1105441 1105442 1105443 [...] (12803 flits)
Class 0:
Remaining flits: 1109751 1109752 1109753 1117800 1117801 1117802 1117803 1117804 1117805 1117806 [...] (37413 flits)
Measured flits: 1109751 1109752 1109753 1117800 1117801 1117802 1117803 1117804 1117805 1117806 [...] (11395 flits)
Class 0:
Remaining flits: 1126071 1126072 1126073 1126074 1126075 1126076 1126077 1126078 1126079 1151381 [...] (36946 flits)
Measured flits: 1151381 1151382 1151383 1151384 1151385 1151386 1151387 1156282 1156283 1158957 [...] (7964 flits)
Class 0:
Remaining flits: 1126078 1126079 1160949 1160950 1160951 1160952 1160953 1160954 1160955 1160956 [...] (35514 flits)
Measured flits: 1160949 1160950 1160951 1160952 1160953 1160954 1160955 1160956 1160957 1160958 [...] (6019 flits)
Class 0:
Remaining flits: 1126078 1126079 1232205 1232206 1232207 1238652 1238653 1238654 1238655 1238656 [...] (36147 flits)
Measured flits: 1285844 1285845 1285846 1285847 1306134 1306135 1306136 1306137 1306138 1306139 [...] (5568 flits)
Class 0:
Remaining flits: 1238665 1238666 1238667 1238668 1238669 1282388 1282389 1282390 1282391 1287144 [...] (38378 flits)
Measured flits: 1306139 1306140 1306141 1306142 1306143 1306144 1306145 1306146 1306147 1306148 [...] (5385 flits)
Class 0:
Remaining flits: 1287144 1287145 1287146 1287147 1287148 1287149 1287150 1287151 1287152 1287153 [...] (36381 flits)
Measured flits: 1324260 1324261 1324262 1324263 1324264 1324265 1324266 1324267 1324268 1324269 [...] (4465 flits)
Class 0:
Remaining flits: 1308091 1308092 1308093 1308094 1308095 1324260 1324261 1324262 1324263 1324264 [...] (36464 flits)
Measured flits: 1324260 1324261 1324262 1324263 1324264 1324265 1324266 1324267 1324268 1324269 [...] (3824 flits)
Class 0:
Remaining flits: 1332480 1332481 1332482 1332483 1332484 1332485 1352825 1362618 1362619 1362620 [...] (35175 flits)
Measured flits: 1362618 1362619 1362620 1362621 1362622 1362623 1362624 1362625 1362626 1362627 [...] (3037 flits)
Class 0:
Remaining flits: 1437871 1437872 1437873 1437874 1437875 1465002 1465003 1465004 1465005 1465006 [...] (37035 flits)
Measured flits: 1470978 1470979 1470980 1470981 1470982 1470983 1470984 1470985 1470986 1470987 [...] (2631 flits)
Class 0:
Remaining flits: 1472778 1472779 1472780 1472781 1472782 1472783 1472784 1472785 1472786 1472787 [...] (37042 flits)
Measured flits: 1497374 1497375 1497376 1497377 1497378 1497379 1497380 1497381 1497382 1497383 [...] (2319 flits)
Class 0:
Remaining flits: 1472778 1472779 1472780 1472781 1472782 1472783 1472784 1472785 1472786 1472787 [...] (35866 flits)
Measured flits: 1586268 1586269 1586270 1586271 1586272 1586273 1586274 1586275 1586276 1586277 [...] (1652 flits)
Class 0:
Remaining flits: 1542780 1542781 1542782 1542783 1542784 1542785 1542786 1542787 1542788 1542789 [...] (35986 flits)
Measured flits: 1629810 1629811 1629812 1629813 1629814 1629815 1629816 1629817 1629818 1629819 [...] (1222 flits)
Class 0:
Remaining flits: 1556316 1556317 1556318 1556319 1556320 1556321 1556322 1556323 1556324 1556325 [...] (36342 flits)
Measured flits: 1629810 1629811 1629812 1629813 1629814 1629815 1629816 1629817 1629818 1629819 [...] (939 flits)
Class 0:
Remaining flits: 1581136 1581137 1582109 1588860 1588861 1588862 1588863 1588864 1588865 1588866 [...] (35821 flits)
Measured flits: 1629817 1629818 1629819 1629820 1629821 1629822 1629823 1629824 1629825 1629826 [...] (1197 flits)
Class 0:
Remaining flits: 1637208 1637209 1637210 1637211 1637212 1637213 1637214 1637215 1637216 1637217 [...] (37169 flits)
Measured flits: 1703897 1732998 1732999 1733000 1733001 1733002 1733003 1751472 1751473 1751474 [...] (872 flits)
Class 0:
Remaining flits: 1663002 1663003 1663004 1663005 1663006 1663007 1663008 1663009 1663010 1663011 [...] (37386 flits)
Measured flits: 1751472 1751473 1751474 1751475 1751476 1751477 1751478 1751479 1751480 1751481 [...] (713 flits)
Class 0:
Remaining flits: 1667034 1667035 1667036 1667037 1667038 1667039 1667040 1667041 1667042 1667043 [...] (36478 flits)
Measured flits: 1751478 1751479 1751480 1751481 1751482 1751483 1751484 1751485 1751486 1751487 [...] (489 flits)
Class 0:
Remaining flits: 1668957 1668958 1668959 1686527 1710855 1710856 1710857 1710858 1710859 1710860 [...] (35691 flits)
Measured flits: 1851104 1851105 1851106 1851107 1851108 1851109 1851110 1851111 1851112 1851113 [...] (235 flits)
Class 0:
Remaining flits: 1711638 1711639 1711640 1711641 1711642 1711643 1711644 1711645 1711646 1711647 [...] (33963 flits)
Measured flits: 1851117 1851118 1851119 1874860 1874861 1908540 1908541 1908542 1908543 1908544 [...] (149 flits)
Class 0:
Remaining flits: 1732410 1732411 1732412 1732413 1732414 1732415 1732416 1732417 1732418 1732419 [...] (35086 flits)
Measured flits: 1908540 1908541 1908542 1908543 1908544 1908545 1908546 1908547 1908548 1908549 [...] (90 flits)
Class 0:
Remaining flits: 1732410 1732411 1732412 1732413 1732414 1732415 1732416 1732417 1732418 1732419 [...] (34388 flits)
Measured flits: 2005290 2005291 2005292 2005293 2005294 2005295 2005296 2005297 2005298 2005299 [...] (56 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1759806 1759807 1759808 1759809 1759810 1759811 1759812 1759813 1759814 1759815 [...] (6061 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1932552 1932553 1932554 1932555 1932556 1932557 1932558 1932559 1932560 1932561 [...] (533 flits)
Measured flits: (0 flits)
Time taken is 59318 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14629.6 (1 samples)
	minimum = 1137 (1 samples)
	maximum = 47013 (1 samples)
Network latency average = 1063.61 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8472 (1 samples)
Flit latency average = 948.418 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12020 (1 samples)
Fragmentation average = 209.103 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4733 (1 samples)
Injected packet rate average = 0.0108854 (1 samples)
	minimum = 0.00314286 (1 samples)
	maximum = 0.0178571 (1 samples)
Accepted packet rate average = 0.0108884 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.195861 (1 samples)
	minimum = 0.0547143 (1 samples)
	maximum = 0.321571 (1 samples)
Accepted flit rate average = 0.195858 (1 samples)
	minimum = 0.150286 (1 samples)
	maximum = 0.247714 (1 samples)
Injected packet size average = 17.993 (1 samples)
Accepted packet size average = 17.9878 (1 samples)
Hops average = 5.06122 (1 samples)
Total run time 66.3254
