BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 219.762
	minimum = 22
	maximum = 627
Network latency average = 214.413
	minimum = 22
	maximum = 627
Slowest packet = 1460
Flit latency average = 183.063
	minimum = 5
	maximum = 634
Slowest flit = 27119
Fragmentation average = 37.4453
	minimum = 0
	maximum = 319
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.0136615
	minimum = 0.006 (at node 41)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.253161
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.5311
Total in-flight flits = 28739 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 386.233
	minimum = 22
	maximum = 1293
Network latency average = 380.701
	minimum = 22
	maximum = 1249
Slowest packet = 3296
Flit latency average = 349.01
	minimum = 5
	maximum = 1232
Slowest flit = 58031
Fragmentation average = 38.5408
	minimum = 0
	maximum = 319
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143776
	minimum = 0.008 (at node 116)
	maximum = 0.0205 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.262284
	minimum = 0.144 (at node 116)
	maximum = 0.369 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.2425
Total in-flight flits = 53039 (0 measured)
latency change    = 0.431011
throughput change = 0.0347806
Class 0:
Packet latency average = 844.509
	minimum = 22
	maximum = 1552
Network latency average = 838.292
	minimum = 22
	maximum = 1534
Slowest packet = 6144
Flit latency average = 809.149
	minimum = 5
	maximum = 1517
Slowest flit = 110609
Fragmentation average = 38.6936
	minimum = 0
	maximum = 273
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.0151458
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.273625
	minimum = 0.126 (at node 61)
	maximum = 0.537 (at node 16)
Injected packet length average = 18.0068
Accepted packet length average = 18.066
Total in-flight flits = 77417 (0 measured)
latency change    = 0.542653
throughput change = 0.0414478
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 40.7926
	minimum = 22
	maximum = 109
Network latency average = 35.4688
	minimum = 22
	maximum = 73
Slowest packet = 13024
Flit latency average = 1102.99
	minimum = 5
	maximum = 1804
Slowest flit = 159406
Fragmentation average = 6.74716
	minimum = 0
	maximum = 36
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0153229
	minimum = 0.006 (at node 105)
	maximum = 0.025 (at node 59)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.276562
	minimum = 0.114 (at node 1)
	maximum = 0.448 (at node 0)
Injected packet length average = 17.9469
Accepted packet length average = 18.0489
Total in-flight flits = 103661 (72928 measured)
latency change    = 19.7025
throughput change = 0.0106215
Class 0:
Packet latency average = 902.773
	minimum = 22
	maximum = 2022
Network latency average = 897.37
	minimum = 22
	maximum = 1981
Slowest packet = 12837
Flit latency average = 1256.72
	minimum = 5
	maximum = 2215
Slowest flit = 212885
Fragmentation average = 26.5291
	minimum = 0
	maximum = 193
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.0153932
	minimum = 0.0095 (at node 86)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.276792
	minimum = 0.163 (at node 86)
	maximum = 0.3915 (at node 129)
Injected packet length average = 17.9983
Accepted packet length average = 17.9814
Total in-flight flits = 126523 (126247 measured)
latency change    = 0.954814
throughput change = 0.000827939
Class 0:
Packet latency average = 1446.96
	minimum = 22
	maximum = 2663
Network latency average = 1441.28
	minimum = 22
	maximum = 2651
Slowest packet = 14297
Flit latency average = 1405.6
	minimum = 5
	maximum = 2634
Slowest flit = 257363
Fragmentation average = 37.3896
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0225382
	minimum = 0.0146667 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0154219
	minimum = 0.0106667 (at node 36)
	maximum = 0.021 (at node 138)
Injected flit rate average = 0.405694
	minimum = 0.264 (at node 171)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.277656
	minimum = 0.175333 (at node 36)
	maximum = 0.373667 (at node 103)
Injected packet length average = 18.0003
Accepted packet length average = 18.0041
Total in-flight flits = 151163 (151163 measured)
latency change    = 0.37609
throughput change = 0.00311386
Class 0:
Packet latency average = 1678.01
	minimum = 22
	maximum = 3127
Network latency average = 1672.25
	minimum = 22
	maximum = 3127
Slowest packet = 16258
Flit latency average = 1553.01
	minimum = 5
	maximum = 3147
Slowest flit = 296897
Fragmentation average = 39.3322
	minimum = 0
	maximum = 297
Injected packet rate average = 0.022526
	minimum = 0.0165 (at node 191)
	maximum = 0.02825 (at node 121)
Accepted packet rate average = 0.0154505
	minimum = 0.011 (at node 162)
	maximum = 0.02025 (at node 103)
Injected flit rate average = 0.405411
	minimum = 0.29625 (at node 191)
	maximum = 0.5095 (at node 151)
Accepted flit rate average= 0.278303
	minimum = 0.198 (at node 162)
	maximum = 0.3685 (at node 103)
Injected packet length average = 17.9975
Accepted packet length average = 18.0126
Total in-flight flits = 175080 (175080 measured)
latency change    = 0.137691
throughput change = 0.00232529
Class 0:
Packet latency average = 1865.06
	minimum = 22
	maximum = 3611
Network latency average = 1859.31
	minimum = 22
	maximum = 3611
Slowest packet = 18778
Flit latency average = 1702.45
	minimum = 5
	maximum = 3624
Slowest flit = 338565
Fragmentation average = 40.9914
	minimum = 0
	maximum = 313
Injected packet rate average = 0.0225396
	minimum = 0.0168 (at node 171)
	maximum = 0.0282 (at node 121)
Accepted packet rate average = 0.0154
	minimum = 0.0114 (at node 171)
	maximum = 0.0202 (at node 103)
Injected flit rate average = 0.40568
	minimum = 0.3024 (at node 171)
	maximum = 0.5076 (at node 121)
Accepted flit rate average= 0.277321
	minimum = 0.2062 (at node 86)
	maximum = 0.3644 (at node 103)
Injected packet length average = 17.9986
Accepted packet length average = 18.0078
Total in-flight flits = 200673 (200673 measured)
latency change    = 0.100295
throughput change = 0.00354302
Class 0:
Packet latency average = 2035.42
	minimum = 22
	maximum = 3763
Network latency average = 2029.75
	minimum = 22
	maximum = 3763
Slowest packet = 22258
Flit latency average = 1851.84
	minimum = 5
	maximum = 3774
Slowest flit = 403156
Fragmentation average = 41.1141
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0225061
	minimum = 0.0173333 (at node 191)
	maximum = 0.0273333 (at node 127)
Accepted packet rate average = 0.0154097
	minimum = 0.0116667 (at node 42)
	maximum = 0.0195 (at node 138)
Injected flit rate average = 0.405128
	minimum = 0.313333 (at node 191)
	maximum = 0.492 (at node 127)
Accepted flit rate average= 0.277516
	minimum = 0.215167 (at node 42)
	maximum = 0.350833 (at node 103)
Injected packet length average = 18.0008
Accepted packet length average = 18.0091
Total in-flight flits = 224405 (224405 measured)
latency change    = 0.0836943
throughput change = 0.000701912
Class 0:
Packet latency average = 2198.63
	minimum = 22
	maximum = 4259
Network latency average = 2192.9
	minimum = 22
	maximum = 4255
Slowest packet = 24282
Flit latency average = 2001.4
	minimum = 5
	maximum = 4238
Slowest flit = 437093
Fragmentation average = 42.0794
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0224814
	minimum = 0.0172857 (at node 171)
	maximum = 0.0267143 (at node 127)
Accepted packet rate average = 0.0154144
	minimum = 0.012 (at node 67)
	maximum = 0.0195714 (at node 70)
Injected flit rate average = 0.40459
	minimum = 0.311143 (at node 171)
	maximum = 0.480857 (at node 127)
Accepted flit rate average= 0.277503
	minimum = 0.215714 (at node 67)
	maximum = 0.357571 (at node 70)
Injected packet length average = 17.9967
Accepted packet length average = 18.0028
Total in-flight flits = 248323 (248323 measured)
latency change    = 0.0742363
throughput change = 4.55808e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 492604 492605 495774 495775 495776 495777 495778 495779 495780 495781 [...] (272476 flits)
Measured flits: 492604 492605 495774 495775 495776 495777 495778 495779 495780 495781 [...] (201179 flits)
Class 0:
Remaining flits: 534702 534703 534704 534705 534706 534707 544194 544195 544196 544197 [...] (293386 flits)
Measured flits: 534702 534703 534704 534705 534706 534707 544194 544195 544196 544197 [...] (153670 flits)
Class 0:
Remaining flits: 579509 599711 599712 599713 599714 599715 599716 599717 599718 599719 [...] (317009 flits)
Measured flits: 579509 599711 599712 599713 599714 599715 599716 599717 599718 599719 [...] (106183 flits)
Class 0:
Remaining flits: 641106 641107 641108 641109 641110 641111 641112 641113 641114 641115 [...] (340911 flits)
Measured flits: 641106 641107 641108 641109 641110 641111 641112 641113 641114 641115 [...] (58626 flits)
Class 0:
Remaining flits: 690032 690033 690034 690035 690036 690037 690038 690039 690040 690041 [...] (363489 flits)
Measured flits: 690032 690033 690034 690035 690036 690037 690038 690039 690040 690041 [...] (17509 flits)
Class 0:
Remaining flits: 737060 737061 737062 737063 737802 737803 737804 737805 737806 737807 [...] (389413 flits)
Measured flits: 737060 737061 737062 737063 737802 737803 737804 737805 737806 737807 [...] (1316 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 816978 816979 816980 816981 816982 816983 817414 817415 818604 818605 [...] (356234 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 837571 837572 837573 837574 837575 871326 871327 871328 871329 871330 [...] (309325 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 905561 914526 914527 914528 914529 914530 914531 914532 914533 914534 [...] (262346 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 953235 953236 953237 953238 953239 953240 953241 953242 953243 954360 [...] (215344 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1009232 1009233 1009234 1009235 1009236 1009237 1009238 1009239 1009240 1009241 [...] (168246 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1051406 1051407 1051408 1051409 1051410 1051411 1051412 1051413 1051414 1051415 [...] (120822 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1088901 1088902 1088903 1088904 1088905 1088906 1088907 1088908 1088909 1097478 [...] (73791 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1150961 1150962 1150963 1150964 1150965 1150966 1150967 1150968 1150969 1150970 [...] (27704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1199538 1199539 1199540 1199541 1199542 1199543 1199544 1199545 1199546 1199547 [...] (3652 flits)
Measured flits: (0 flits)
Time taken is 26592 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3227.07 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6823 (1 samples)
Network latency average = 3221.24 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6823 (1 samples)
Flit latency average = 4473.04 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10297 (1 samples)
Fragmentation average = 45.6647 (1 samples)
	minimum = 0 (1 samples)
	maximum = 480 (1 samples)
Injected packet rate average = 0.0224814 (1 samples)
	minimum = 0.0172857 (1 samples)
	maximum = 0.0267143 (1 samples)
Accepted packet rate average = 0.0154144 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0195714 (1 samples)
Injected flit rate average = 0.40459 (1 samples)
	minimum = 0.311143 (1 samples)
	maximum = 0.480857 (1 samples)
Accepted flit rate average = 0.277503 (1 samples)
	minimum = 0.215714 (1 samples)
	maximum = 0.357571 (1 samples)
Injected packet size average = 17.9967 (1 samples)
Accepted packet size average = 18.0028 (1 samples)
Hops average = 5.07459 (1 samples)
Total run time 20.6407
