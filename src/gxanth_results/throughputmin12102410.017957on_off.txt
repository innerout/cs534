BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 284.88
	minimum = 27
	maximum = 979
Network latency average = 227.951
	minimum = 25
	maximum = 832
Slowest packet = 95
Flit latency average = 189.283
	minimum = 6
	maximum = 848
Slowest flit = 9846
Fragmentation average = 53.7424
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00930208
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.174526
	minimum = 0.054 (at node 41)
	maximum = 0.33 (at node 76)
Injected packet length average = 17.8264
Accepted packet length average = 18.762
Total in-flight flits = 28501 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 473.757
	minimum = 23
	maximum = 1900
Network latency average = 404.766
	minimum = 23
	maximum = 1773
Slowest packet = 95
Flit latency average = 364.798
	minimum = 6
	maximum = 1756
Slowest flit = 11213
Fragmentation average = 61.8038
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0096224
	minimum = 0.0055 (at node 30)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.177393
	minimum = 0.099 (at node 30)
	maximum = 0.282 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.4355
Total in-flight flits = 51473 (0 measured)
latency change    = 0.398679
throughput change = 0.0161629
Class 0:
Packet latency average = 1019.9
	minimum = 23
	maximum = 2787
Network latency average = 923.304
	minimum = 23
	maximum = 2757
Slowest packet = 759
Flit latency average = 887.598
	minimum = 6
	maximum = 2740
Slowest flit = 13679
Fragmentation average = 66.5887
	minimum = 0
	maximum = 148
Injected packet rate average = 0.018099
	minimum = 0 (at node 142)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0102448
	minimum = 0.002 (at node 2)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.325151
	minimum = 0 (at node 142)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.183807
	minimum = 0.036 (at node 2)
	maximum = 0.374 (at node 16)
Injected packet length average = 17.9652
Accepted packet length average = 17.9415
Total in-flight flits = 78732 (0 measured)
latency change    = 0.535486
throughput change = 0.0348956
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 282.58
	minimum = 27
	maximum = 1081
Network latency average = 188.334
	minimum = 23
	maximum = 906
Slowest packet = 10132
Flit latency average = 1212.29
	minimum = 6
	maximum = 3341
Slowest flit = 36107
Fragmentation average = 32.9537
	minimum = 0
	maximum = 126
Injected packet rate average = 0.018474
	minimum = 0 (at node 6)
	maximum = 0.056 (at node 25)
Accepted packet rate average = 0.0101563
	minimum = 0.003 (at node 132)
	maximum = 0.018 (at node 37)
Injected flit rate average = 0.332818
	minimum = 0 (at node 6)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.183401
	minimum = 0.054 (at node 132)
	maximum = 0.314 (at node 37)
Injected packet length average = 18.0155
Accepted packet length average = 18.0579
Total in-flight flits = 107365 (56043 measured)
latency change    = 2.60923
throughput change = 0.00221509
Class 0:
Packet latency average = 603.456
	minimum = 23
	maximum = 2098
Network latency average = 514.69
	minimum = 23
	maximum = 1962
Slowest packet = 10132
Flit latency average = 1404.07
	minimum = 6
	maximum = 4383
Slowest flit = 35275
Fragmentation average = 46.9294
	minimum = 0
	maximum = 169
Injected packet rate average = 0.0185521
	minimum = 0.0005 (at node 150)
	maximum = 0.0455 (at node 106)
Accepted packet rate average = 0.0101927
	minimum = 0.005 (at node 132)
	maximum = 0.016 (at node 151)
Injected flit rate average = 0.33418
	minimum = 0.009 (at node 150)
	maximum = 0.8205 (at node 106)
Accepted flit rate average= 0.184102
	minimum = 0.0855 (at node 162)
	maximum = 0.281 (at node 151)
Injected packet length average = 18.0131
Accepted packet length average = 18.0621
Total in-flight flits = 136269 (105564 measured)
latency change    = 0.53173
throughput change = 0.00380508
Class 0:
Packet latency average = 1013.84
	minimum = 23
	maximum = 3247
Network latency average = 927.819
	minimum = 23
	maximum = 2983
Slowest packet = 10132
Flit latency average = 1605.79
	minimum = 6
	maximum = 5186
Slowest flit = 42551
Fragmentation average = 55.4829
	minimum = 0
	maximum = 169
Injected packet rate average = 0.0176424
	minimum = 0.00233333 (at node 85)
	maximum = 0.0393333 (at node 106)
Accepted packet rate average = 0.0100903
	minimum = 0.005 (at node 132)
	maximum = 0.0143333 (at node 44)
Injected flit rate average = 0.317964
	minimum = 0.0446667 (at node 85)
	maximum = 0.709 (at node 106)
Accepted flit rate average= 0.181533
	minimum = 0.095 (at node 132)
	maximum = 0.256667 (at node 44)
Injected packet length average = 18.0227
Accepted packet length average = 17.9909
Total in-flight flits = 157085 (139302 measured)
latency change    = 0.404784
throughput change = 0.0141494
Draining remaining packets ...
Class 0:
Remaining flits: 60372 60373 60374 60375 60376 60377 60378 60379 60380 60381 [...] (126837 flits)
Measured flits: 182196 182197 182198 182199 182200 182201 182202 182203 182204 182205 [...] (117349 flits)
Class 0:
Remaining flits: 67734 67735 67736 67737 67738 67739 67740 67741 67742 67743 [...] (97825 flits)
Measured flits: 182196 182197 182198 182199 182200 182201 182202 182203 182204 182205 [...] (93069 flits)
Class 0:
Remaining flits: 72090 72091 72092 72093 72094 72095 72096 72097 72098 72099 [...] (68849 flits)
Measured flits: 182718 182719 182720 182721 182722 182723 182724 182725 182726 182727 [...] (66438 flits)
Class 0:
Remaining flits: 81000 81001 81002 81003 81004 81005 81006 81007 81008 81009 [...] (42380 flits)
Measured flits: 182718 182719 182720 182721 182722 182723 182724 182725 182726 182727 [...] (41302 flits)
Class 0:
Remaining flits: 131842 131843 131844 131845 131846 131847 131848 131849 144234 144235 [...] (21321 flits)
Measured flits: 184086 184087 184088 184089 184090 184091 184092 184093 184094 184095 [...] (20905 flits)
Class 0:
Remaining flits: 169416 169417 169418 169419 169420 169421 169422 169423 169424 169425 [...] (7127 flits)
Measured flits: 189810 189811 189812 189813 189814 189815 189816 189817 189818 189819 [...] (7040 flits)
Class 0:
Remaining flits: 227322 227323 227324 227325 227326 227327 227328 227329 227330 227331 [...] (1142 flits)
Measured flits: 227322 227323 227324 227325 227326 227327 227328 227329 227330 227331 [...] (1142 flits)
Time taken is 13983 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3710.94 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10205 (1 samples)
Network latency average = 3593.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 9889 (1 samples)
Flit latency average = 3304.76 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9872 (1 samples)
Fragmentation average = 71.9307 (1 samples)
	minimum = 0 (1 samples)
	maximum = 174 (1 samples)
Injected packet rate average = 0.0176424 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.0393333 (1 samples)
Accepted packet rate average = 0.0100903 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0143333 (1 samples)
Injected flit rate average = 0.317964 (1 samples)
	minimum = 0.0446667 (1 samples)
	maximum = 0.709 (1 samples)
Accepted flit rate average = 0.181533 (1 samples)
	minimum = 0.095 (1 samples)
	maximum = 0.256667 (1 samples)
Injected packet size average = 18.0227 (1 samples)
Accepted packet size average = 17.9909 (1 samples)
Hops average = 5.0496 (1 samples)
Total run time 10.6109
