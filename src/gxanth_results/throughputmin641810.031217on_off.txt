BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 379.691
	minimum = 27
	maximum = 996
Network latency average = 285.549
	minimum = 23
	maximum = 979
Slowest packet = 64
Flit latency average = 199.517
	minimum = 6
	maximum = 962
Slowest flit = 2825
Fragmentation average = 192.083
	minimum = 0
	maximum = 933
Injected packet rate average = 0.0225937
	minimum = 0 (at node 21)
	maximum = 0.047 (at node 57)
Accepted packet rate average = 0.00951042
	minimum = 0.004 (at node 41)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.402349
	minimum = 0 (at node 21)
	maximum = 0.83 (at node 57)
Accepted flit rate average= 0.206589
	minimum = 0.078 (at node 59)
	maximum = 0.432 (at node 44)
Injected packet length average = 17.808
Accepted packet length average = 21.7223
Total in-flight flits = 38473 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 665.573
	minimum = 27
	maximum = 1953
Network latency average = 493.091
	minimum = 23
	maximum = 1733
Slowest packet = 64
Flit latency average = 379.007
	minimum = 6
	maximum = 1884
Slowest flit = 8450
Fragmentation average = 260.285
	minimum = 0
	maximum = 1512
Injected packet rate average = 0.0200495
	minimum = 0.004 (at node 113)
	maximum = 0.0375 (at node 18)
Accepted packet rate average = 0.0109193
	minimum = 0.006 (at node 59)
	maximum = 0.018 (at node 103)
Injected flit rate average = 0.357708
	minimum = 0.072 (at node 113)
	maximum = 0.675 (at node 18)
Accepted flit rate average= 0.217268
	minimum = 0.1225 (at node 96)
	maximum = 0.3265 (at node 22)
Injected packet length average = 17.8413
Accepted packet length average = 19.8977
Total in-flight flits = 55295 (0 measured)
latency change    = 0.429527
throughput change = 0.0491544
Class 0:
Packet latency average = 1372.37
	minimum = 36
	maximum = 2922
Network latency average = 934.367
	minimum = 26
	maximum = 2702
Slowest packet = 1008
Flit latency average = 806.298
	minimum = 6
	maximum = 2821
Slowest flit = 13621
Fragmentation average = 308.547
	minimum = 2
	maximum = 2637
Injected packet rate average = 0.0121667
	minimum = 0 (at node 4)
	maximum = 0.041 (at node 189)
Accepted packet rate average = 0.012599
	minimum = 0.003 (at node 4)
	maximum = 0.024 (at node 34)
Injected flit rate average = 0.218063
	minimum = 0 (at node 4)
	maximum = 0.746 (at node 189)
Accepted flit rate average= 0.224109
	minimum = 0.063 (at node 4)
	maximum = 0.39 (at node 34)
Injected packet length average = 17.9229
Accepted packet length average = 17.7879
Total in-flight flits = 54404 (0 measured)
latency change    = 0.515021
throughput change = 0.0305259
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1685.71
	minimum = 44
	maximum = 3840
Network latency average = 334.484
	minimum = 27
	maximum = 929
Slowest packet = 10049
Flit latency average = 952.572
	minimum = 6
	maximum = 3818
Slowest flit = 14342
Fragmentation average = 155.933
	minimum = 2
	maximum = 624
Injected packet rate average = 0.0109583
	minimum = 0 (at node 4)
	maximum = 0.047 (at node 121)
Accepted packet rate average = 0.0119948
	minimum = 0.005 (at node 4)
	maximum = 0.026 (at node 119)
Injected flit rate average = 0.196943
	minimum = 0 (at node 4)
	maximum = 0.845 (at node 121)
Accepted flit rate average= 0.212406
	minimum = 0.064 (at node 139)
	maximum = 0.432 (at node 119)
Injected packet length average = 17.972
Accepted packet length average = 17.7082
Total in-flight flits = 51566 (24039 measured)
latency change    = 0.18588
throughput change = 0.0550978
Class 0:
Packet latency average = 2013.08
	minimum = 44
	maximum = 4603
Network latency average = 507.787
	minimum = 27
	maximum = 1903
Slowest packet = 10049
Flit latency average = 1008.67
	minimum = 6
	maximum = 4773
Slowest flit = 18424
Fragmentation average = 181.945
	minimum = 0
	maximum = 1265
Injected packet rate average = 0.0110026
	minimum = 0 (at node 4)
	maximum = 0.0345 (at node 26)
Accepted packet rate average = 0.0116615
	minimum = 0.007 (at node 4)
	maximum = 0.019 (at node 119)
Injected flit rate average = 0.197984
	minimum = 0 (at node 12)
	maximum = 0.621 (at node 26)
Accepted flit rate average= 0.207354
	minimum = 0.1255 (at node 79)
	maximum = 0.321 (at node 119)
Injected packet length average = 17.9943
Accepted packet length average = 17.7812
Total in-flight flits = 50866 (34947 measured)
latency change    = 0.162621
throughput change = 0.0243645
Class 0:
Packet latency average = 2376.89
	minimum = 31
	maximum = 5576
Network latency average = 637.347
	minimum = 27
	maximum = 2813
Slowest packet = 10049
Flit latency average = 1031.37
	minimum = 6
	maximum = 5862
Slowest flit = 3833
Fragmentation average = 200.045
	minimum = 0
	maximum = 2041
Injected packet rate average = 0.0112622
	minimum = 0 (at node 12)
	maximum = 0.0306667 (at node 158)
Accepted packet rate average = 0.0116406
	minimum = 0.00666667 (at node 139)
	maximum = 0.017 (at node 165)
Injected flit rate average = 0.202717
	minimum = 0 (at node 12)
	maximum = 0.554333 (at node 158)
Accepted flit rate average= 0.207694
	minimum = 0.116 (at node 139)
	maximum = 0.297 (at node 136)
Injected packet length average = 17.9998
Accepted packet length average = 17.8422
Total in-flight flits = 51574 (41474 measured)
latency change    = 0.153061
throughput change = 0.00163836
Class 0:
Packet latency average = 2746.44
	minimum = 31
	maximum = 6380
Network latency average = 735.413
	minimum = 27
	maximum = 3862
Slowest packet = 10049
Flit latency average = 1040.85
	minimum = 6
	maximum = 6864
Slowest flit = 9196
Fragmentation average = 212.947
	minimum = 0
	maximum = 3090
Injected packet rate average = 0.011694
	minimum = 0 (at node 34)
	maximum = 0.02925 (at node 9)
Accepted packet rate average = 0.0116081
	minimum = 0.008 (at node 75)
	maximum = 0.01575 (at node 136)
Injected flit rate average = 0.210499
	minimum = 0 (at node 37)
	maximum = 0.52825 (at node 9)
Accepted flit rate average= 0.207432
	minimum = 0.14725 (at node 104)
	maximum = 0.27725 (at node 136)
Injected packet length average = 18.0006
Accepted packet length average = 17.8697
Total in-flight flits = 56916 (49705 measured)
latency change    = 0.134555
throughput change = 0.0012638
Draining remaining packets ...
Class 0:
Remaining flits: 11016 11017 11018 11019 11020 11021 11022 11023 11024 11025 [...] (23263 flits)
Measured flits: 181908 181909 181910 181911 181912 181913 181914 181915 181916 181917 [...] (19659 flits)
Class 0:
Remaining flits: 11022 11023 11024 11025 11026 11027 11028 11029 11030 11031 [...] (2936 flits)
Measured flits: 182873 182874 182875 182876 182877 182878 182879 183996 183997 183998 [...] (2753 flits)
Time taken is 9626 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3418.67 (1 samples)
	minimum = 31 (1 samples)
	maximum = 8821 (1 samples)
Network latency average = 1174.72 (1 samples)
	minimum = 27 (1 samples)
	maximum = 6168 (1 samples)
Flit latency average = 1420.04 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9131 (1 samples)
Fragmentation average = 223.018 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4781 (1 samples)
Injected packet rate average = 0.011694 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.02925 (1 samples)
Accepted packet rate average = 0.0116081 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.01575 (1 samples)
Injected flit rate average = 0.210499 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.52825 (1 samples)
Accepted flit rate average = 0.207432 (1 samples)
	minimum = 0.14725 (1 samples)
	maximum = 0.27725 (1 samples)
Injected packet size average = 18.0006 (1 samples)
Accepted packet size average = 17.8697 (1 samples)
Hops average = 5.1525 (1 samples)
Total run time 12.743
