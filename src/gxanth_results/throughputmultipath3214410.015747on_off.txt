BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 277.354
	minimum = 23
	maximum = 938
Network latency average = 223.37
	minimum = 23
	maximum = 856
Slowest packet = 64
Flit latency average = 158.242
	minimum = 6
	maximum = 963
Slowest flit = 1995
Fragmentation average = 123.383
	minimum = 0
	maximum = 798
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.00892708
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.182083
	minimum = 0.051 (at node 174)
	maximum = 0.326 (at node 152)
Injected packet length average = 17.8661
Accepted packet length average = 20.3967
Total in-flight flits = 19634 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 424.637
	minimum = 23
	maximum = 1942
Network latency average = 361.019
	minimum = 23
	maximum = 1926
Slowest packet = 269
Flit latency average = 277.756
	minimum = 6
	maximum = 1909
Slowest flit = 4859
Fragmentation average = 163.296
	minimum = 0
	maximum = 1589
Injected packet rate average = 0.0153828
	minimum = 0.0015 (at node 140)
	maximum = 0.037 (at node 112)
Accepted packet rate average = 0.00995833
	minimum = 0.005 (at node 30)
	maximum = 0.0155 (at node 166)
Injected flit rate average = 0.275742
	minimum = 0.027 (at node 140)
	maximum = 0.6605 (at node 112)
Accepted flit rate average= 0.191708
	minimum = 0.1 (at node 116)
	maximum = 0.292 (at node 166)
Injected packet length average = 17.9253
Accepted packet length average = 19.251
Total in-flight flits = 32710 (0 measured)
latency change    = 0.346846
throughput change = 0.0502065
Class 0:
Packet latency average = 768.618
	minimum = 24
	maximum = 2686
Network latency average = 686.569
	minimum = 24
	maximum = 2484
Slowest packet = 362
Flit latency average = 596.615
	minimum = 6
	maximum = 2790
Slowest flit = 11085
Fragmentation average = 214.961
	minimum = 0
	maximum = 1614
Injected packet rate average = 0.0164792
	minimum = 0 (at node 21)
	maximum = 0.055 (at node 113)
Accepted packet rate average = 0.0116354
	minimum = 0.004 (at node 54)
	maximum = 0.022 (at node 102)
Injected flit rate average = 0.296578
	minimum = 0 (at node 21)
	maximum = 1 (at node 113)
Accepted flit rate average= 0.212651
	minimum = 0.067 (at node 54)
	maximum = 0.378 (at node 102)
Injected packet length average = 17.9972
Accepted packet length average = 18.2762
Total in-flight flits = 48833 (0 measured)
latency change    = 0.447531
throughput change = 0.0984839
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 354.114
	minimum = 25
	maximum = 1119
Network latency average = 291.537
	minimum = 23
	maximum = 993
Slowest packet = 9078
Flit latency average = 837.052
	minimum = 6
	maximum = 3536
Slowest flit = 15245
Fragmentation average = 123.319
	minimum = 0
	maximum = 655
Injected packet rate average = 0.015401
	minimum = 0 (at node 3)
	maximum = 0.045 (at node 134)
Accepted packet rate average = 0.0118281
	minimum = 0.005 (at node 150)
	maximum = 0.021 (at node 3)
Injected flit rate average = 0.277495
	minimum = 0 (at node 3)
	maximum = 0.8 (at node 134)
Accepted flit rate average= 0.211615
	minimum = 0.095 (at node 138)
	maximum = 0.367 (at node 3)
Injected packet length average = 18.0179
Accepted packet length average = 17.8908
Total in-flight flits = 61429 (39527 measured)
latency change    = 1.17054
throughput change = 0.00489786
Class 0:
Packet latency average = 641.758
	minimum = 25
	maximum = 2627
Network latency average = 570.108
	minimum = 23
	maximum = 1986
Slowest packet = 9078
Flit latency average = 930.973
	minimum = 6
	maximum = 4376
Slowest flit = 32893
Fragmentation average = 157.666
	minimum = 0
	maximum = 1265
Injected packet rate average = 0.0154609
	minimum = 0.002 (at node 91)
	maximum = 0.039 (at node 41)
Accepted packet rate average = 0.0118646
	minimum = 0.006 (at node 132)
	maximum = 0.0185 (at node 99)
Injected flit rate average = 0.27806
	minimum = 0.035 (at node 91)
	maximum = 0.702 (at node 41)
Accepted flit rate average= 0.211844
	minimum = 0.119 (at node 132)
	maximum = 0.332 (at node 114)
Injected packet length average = 17.9847
Accepted packet length average = 17.8551
Total in-flight flits = 74351 (63826 measured)
latency change    = 0.448212
throughput change = 0.00108177
Class 0:
Packet latency average = 873.004
	minimum = 25
	maximum = 3360
Network latency average = 794.347
	minimum = 23
	maximum = 2900
Slowest packet = 9078
Flit latency average = 1039.25
	minimum = 6
	maximum = 5125
Slowest flit = 45880
Fragmentation average = 174.962
	minimum = 0
	maximum = 1654
Injected packet rate average = 0.0155347
	minimum = 0.00333333 (at node 107)
	maximum = 0.0376667 (at node 30)
Accepted packet rate average = 0.0117587
	minimum = 0.00733333 (at node 163)
	maximum = 0.017 (at node 51)
Injected flit rate average = 0.279618
	minimum = 0.06 (at node 107)
	maximum = 0.676333 (at node 30)
Accepted flit rate average= 0.210823
	minimum = 0.135333 (at node 163)
	maximum = 0.293333 (at node 181)
Injected packet length average = 17.9996
Accepted packet length average = 17.9291
Total in-flight flits = 88463 (83190 measured)
latency change    = 0.264885
throughput change = 0.00484214
Draining remaining packets ...
Class 0:
Remaining flits: 28556 28557 28558 28559 28560 28561 28562 28563 28564 28565 [...] (54058 flits)
Measured flits: 163449 163450 163451 163452 163453 163454 163455 163456 163457 163494 [...] (51550 flits)
Class 0:
Remaining flits: 74546 74547 74548 74549 74550 74551 74552 74553 74554 74555 [...] (25009 flits)
Measured flits: 163494 163495 163496 163497 163498 163499 163500 163501 163502 163503 [...] (23877 flits)
Class 0:
Remaining flits: 90090 90091 90092 90093 90094 90095 90096 90097 90098 90099 [...] (6065 flits)
Measured flits: 166770 166771 166772 166773 166774 166775 166776 166777 166778 166779 [...] (5598 flits)
Class 0:
Remaining flits: 151186 151187 151188 151189 151190 151191 151192 151193 151194 151195 [...] (609 flits)
Measured flits: 166770 166771 166772 166773 166774 166775 166776 166777 166778 166779 [...] (590 flits)
Time taken is 10376 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1867.09 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7942 (1 samples)
Network latency average = 1773.67 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7087 (1 samples)
Flit latency average = 1728.34 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8109 (1 samples)
Fragmentation average = 188.058 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2752 (1 samples)
Injected packet rate average = 0.0155347 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.0376667 (1 samples)
Accepted packet rate average = 0.0117587 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.279618 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.676333 (1 samples)
Accepted flit rate average = 0.210823 (1 samples)
	minimum = 0.135333 (1 samples)
	maximum = 0.293333 (1 samples)
Injected packet size average = 17.9996 (1 samples)
Accepted packet size average = 17.9291 (1 samples)
Hops average = 5.08952 (1 samples)
Total run time 8.17063
