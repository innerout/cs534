BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.56
	minimum = 24
	maximum = 884
Network latency average = 164.324
	minimum = 22
	maximum = 747
Slowest packet = 64
Flit latency average = 136.803
	minimum = 5
	maximum = 745
Slowest flit = 14990
Fragmentation average = 28.8495
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.01225
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22788
	minimum = 0.108 (at node 41)
	maximum = 0.442 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6025
Total in-flight flits = 18257 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 356.197
	minimum = 22
	maximum = 1805
Network latency average = 275.229
	minimum = 22
	maximum = 1400
Slowest packet = 64
Flit latency average = 245.516
	minimum = 5
	maximum = 1433
Slowest flit = 33104
Fragmentation average = 35.0839
	minimum = 0
	maximum = 548
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0131875
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241622
	minimum = 0.151 (at node 153)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.3221
Total in-flight flits = 26809 (0 measured)
latency change    = 0.355525
throughput change = 0.0568746
Class 0:
Packet latency average = 629.123
	minimum = 22
	maximum = 2465
Network latency average = 527.134
	minimum = 22
	maximum = 2034
Slowest packet = 1839
Flit latency average = 492.389
	minimum = 5
	maximum = 2115
Slowest flit = 52981
Fragmentation average = 43.81
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145573
	minimum = 0.004 (at node 118)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261604
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9707
Total in-flight flits = 38951 (0 measured)
latency change    = 0.433819
throughput change = 0.0763817
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.578
	minimum = 25
	maximum = 1427
Network latency average = 315.061
	minimum = 22
	maximum = 979
Slowest packet = 10126
Flit latency average = 638.36
	minimum = 5
	maximum = 2645
Slowest flit = 72107
Fragmentation average = 29.4136
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0147917
	minimum = 0.007 (at node 117)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267385
	minimum = 0.13 (at node 117)
	maximum = 0.483 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 18.0768
Total in-flight flits = 49551 (41405 measured)
latency change    = 0.539786
throughput change = 0.0216214
Class 0:
Packet latency average = 671.182
	minimum = 22
	maximum = 2170
Network latency average = 574.334
	minimum = 22
	maximum = 1969
Slowest packet = 10126
Flit latency average = 720.34
	minimum = 5
	maximum = 2645
Slowest flit = 72107
Fragmentation average = 40.679
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0147891
	minimum = 0.009 (at node 110)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.267216
	minimum = 0.169 (at node 110)
	maximum = 0.426 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 18.0685
Total in-flight flits = 55986 (54908 measured)
latency change    = 0.391255
throughput change = 0.00063346
Class 0:
Packet latency average = 835.333
	minimum = 22
	maximum = 3328
Network latency average = 738.41
	minimum = 22
	maximum = 2903
Slowest packet = 10126
Flit latency average = 795.37
	minimum = 5
	maximum = 3235
Slowest flit = 166017
Fragmentation average = 44.278
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0148247
	minimum = 0.0103333 (at node 135)
	maximum = 0.0213333 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267622
	minimum = 0.190333 (at node 135)
	maximum = 0.382667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.0525
Total in-flight flits = 65935 (65861 measured)
latency change    = 0.19651
throughput change = 0.00151476
Class 0:
Packet latency average = 961.556
	minimum = 22
	maximum = 4093
Network latency average = 862.737
	minimum = 22
	maximum = 3702
Slowest packet = 10126
Flit latency average = 870.976
	minimum = 5
	maximum = 3685
Slowest flit = 186839
Fragmentation average = 49.2373
	minimum = 0
	maximum = 694
Injected packet rate average = 0.017625
	minimum = 0.0025 (at node 120)
	maximum = 0.038 (at node 17)
Accepted packet rate average = 0.0148242
	minimum = 0.011 (at node 144)
	maximum = 0.0195 (at node 138)
Injected flit rate average = 0.317267
	minimum = 0.045 (at node 120)
	maximum = 0.683 (at node 17)
Accepted flit rate average= 0.267237
	minimum = 0.2015 (at node 144)
	maximum = 0.35 (at node 118)
Injected packet length average = 18.001
Accepted packet length average = 18.0271
Total in-flight flits = 77361 (77361 measured)
latency change    = 0.13127
throughput change = 0.00143898
Class 0:
Packet latency average = 1076.14
	minimum = 22
	maximum = 4312
Network latency average = 976.304
	minimum = 22
	maximum = 3995
Slowest packet = 10126
Flit latency average = 955.911
	minimum = 5
	maximum = 3993
Slowest flit = 243426
Fragmentation average = 49.6997
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0175719
	minimum = 0.0032 (at node 89)
	maximum = 0.0388 (at node 23)
Accepted packet rate average = 0.01485
	minimum = 0.011 (at node 144)
	maximum = 0.0192 (at node 118)
Injected flit rate average = 0.31629
	minimum = 0.0576 (at node 89)
	maximum = 0.6994 (at node 23)
Accepted flit rate average= 0.267424
	minimum = 0.2036 (at node 86)
	maximum = 0.3456 (at node 118)
Injected packet length average = 17.9998
Accepted packet length average = 18.0083
Total in-flight flits = 85866 (85866 measured)
latency change    = 0.106474
throughput change = 0.000699186
Class 0:
Packet latency average = 1175.94
	minimum = 22
	maximum = 4312
Network latency average = 1074.57
	minimum = 22
	maximum = 4153
Slowest packet = 10126
Flit latency average = 1038.98
	minimum = 5
	maximum = 4136
Slowest flit = 251927
Fragmentation average = 50.3619
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0177665
	minimum = 0.00516667 (at node 89)
	maximum = 0.0361667 (at node 23)
Accepted packet rate average = 0.0148698
	minimum = 0.0116667 (at node 80)
	maximum = 0.019 (at node 118)
Injected flit rate average = 0.319868
	minimum = 0.093 (at node 89)
	maximum = 0.651 (at node 23)
Accepted flit rate average= 0.268035
	minimum = 0.209333 (at node 80)
	maximum = 0.345167 (at node 118)
Injected packet length average = 18.004
Accepted packet length average = 18.0255
Total in-flight flits = 98581 (98581 measured)
latency change    = 0.0848719
throughput change = 0.00227867
Class 0:
Packet latency average = 1258.71
	minimum = 22
	maximum = 4312
Network latency average = 1158.48
	minimum = 22
	maximum = 4153
Slowest packet = 10126
Flit latency average = 1112.67
	minimum = 5
	maximum = 4136
Slowest flit = 251927
Fragmentation average = 51.1598
	minimum = 0
	maximum = 694
Injected packet rate average = 0.017744
	minimum = 0.00557143 (at node 89)
	maximum = 0.0318571 (at node 23)
Accepted packet rate average = 0.0148966
	minimum = 0.012 (at node 64)
	maximum = 0.0187143 (at node 118)
Injected flit rate average = 0.319408
	minimum = 0.100286 (at node 89)
	maximum = 0.575429 (at node 23)
Accepted flit rate average= 0.268509
	minimum = 0.216714 (at node 64)
	maximum = 0.336857 (at node 118)
Injected packet length average = 18.0009
Accepted packet length average = 18.0249
Total in-flight flits = 107339 (107339 measured)
latency change    = 0.0657525
throughput change = 0.00176607
Draining all recorded packets ...
Class 0:
Remaining flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (120201 flits)
Measured flits: 427356 427357 427358 427359 427360 427361 427362 427363 427364 427365 [...] (68104 flits)
Class 0:
Remaining flits: 474278 474279 474280 474281 475660 475661 475662 475663 475664 475665 [...] (129372 flits)
Measured flits: 474278 474279 474280 474281 475660 475661 475662 475663 475664 475665 [...] (31583 flits)
Class 0:
Remaining flits: 488171 488172 488173 488174 488175 488176 488177 500052 500053 500054 [...] (138472 flits)
Measured flits: 488171 488172 488173 488174 488175 488176 488177 500052 500053 500054 [...] (10554 flits)
Class 0:
Remaining flits: 530510 530511 530512 530513 546768 546769 546770 546771 546772 546773 [...] (149605 flits)
Measured flits: 530510 530511 530512 530513 546768 546769 546770 546771 546772 546773 [...] (2085 flits)
Class 0:
Remaining flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (160842 flits)
Measured flits: 592434 592435 592436 592437 592438 592439 592440 592441 592442 592443 [...] (113 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 668596 668597 668598 668599 668600 668601 668602 668603 668604 668605 [...] (117232 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 728622 728623 728624 728625 728626 728627 728628 728629 728630 728631 [...] (72261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 777354 777355 777356 777357 777358 777359 777360 777361 777362 777363 [...] (35597 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 795558 795559 795560 795561 795562 795563 798616 798617 798618 798619 [...] (13766 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 874565 875172 875173 875174 875175 875176 875177 875585 875586 875587 [...] (4401 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 925574 925575 925576 925577 926784 926785 926786 926787 926788 926789 [...] (794 flits)
Measured flits: (0 flits)
Time taken is 21697 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1620 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5744 (1 samples)
Network latency average = 1518.01 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5397 (1 samples)
Flit latency average = 1943.36 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6636 (1 samples)
Fragmentation average = 58.8883 (1 samples)
	minimum = 0 (1 samples)
	maximum = 735 (1 samples)
Injected packet rate average = 0.017744 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0318571 (1 samples)
Accepted packet rate average = 0.0148966 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.319408 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.575429 (1 samples)
Accepted flit rate average = 0.268509 (1 samples)
	minimum = 0.216714 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.0249 (1 samples)
Hops average = 5.0647 (1 samples)
Total run time 17.9347
