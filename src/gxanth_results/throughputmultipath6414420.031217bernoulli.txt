BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 276.353
	minimum = 22
	maximum = 789
Network latency average = 267.582
	minimum = 22
	maximum = 756
Slowest packet = 1205
Flit latency average = 237.329
	minimum = 5
	maximum = 739
Slowest flit = 21707
Fragmentation average = 51.9464
	minimum = 0
	maximum = 440
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.0141823
	minimum = 0.006 (at node 127)
	maximum = 0.023 (at node 131)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.269677
	minimum = 0.122 (at node 174)
	maximum = 0.446 (at node 44)
Injected packet length average = 17.8483
Accepted packet length average = 19.0151
Total in-flight flits = 54674 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 512.016
	minimum = 22
	maximum = 1524
Network latency average = 502.134
	minimum = 22
	maximum = 1456
Slowest packet = 2701
Flit latency average = 464.697
	minimum = 5
	maximum = 1631
Slowest flit = 38144
Fragmentation average = 74.6056
	minimum = 0
	maximum = 742
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0149036
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 22)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.276945
	minimum = 0.15 (at node 153)
	maximum = 0.385 (at node 152)
Injected packet length average = 17.9239
Accepted packet length average = 18.5824
Total in-flight flits = 108231 (0 measured)
latency change    = 0.460266
throughput change = 0.0262443
Class 0:
Packet latency average = 1169.87
	minimum = 22
	maximum = 2353
Network latency average = 1158.93
	minimum = 22
	maximum = 2300
Slowest packet = 3942
Flit latency average = 1122.01
	minimum = 5
	maximum = 2283
Slowest flit = 70973
Fragmentation average = 110.211
	minimum = 0
	maximum = 730
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0158385
	minimum = 0.004 (at node 96)
	maximum = 0.026 (at node 159)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.28913
	minimum = 0.105 (at node 96)
	maximum = 0.471 (at node 16)
Injected packet length average = 18.0012
Accepted packet length average = 18.2549
Total in-flight flits = 159638 (0 measured)
latency change    = 0.56233
throughput change = 0.0421433
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.5839
	minimum = 22
	maximum = 126
Network latency average = 35.2399
	minimum = 22
	maximum = 101
Slowest packet = 17896
Flit latency average = 1571.02
	minimum = 5
	maximum = 3076
Slowest flit = 93851
Fragmentation average = 6.35881
	minimum = 0
	maximum = 67
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0159479
	minimum = 0.005 (at node 134)
	maximum = 0.027 (at node 103)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.290359
	minimum = 0.108 (at node 36)
	maximum = 0.474 (at node 103)
Injected packet length average = 17.9869
Accepted packet length average = 18.2067
Total in-flight flits = 213869 (101413 measured)
latency change    = 24.6641
throughput change = 0.00423326
Class 0:
Packet latency average = 45.2718
	minimum = 22
	maximum = 126
Network latency average = 35.2469
	minimum = 22
	maximum = 101
Slowest packet = 17896
Flit latency average = 1786.67
	minimum = 5
	maximum = 3739
Slowest flit = 131758
Fragmentation average = 6.3444
	minimum = 0
	maximum = 67
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.0159635
	minimum = 0.0095 (at node 86)
	maximum = 0.023 (at node 66)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.289938
	minimum = 0.184 (at node 86)
	maximum = 0.4085 (at node 129)
Injected packet length average = 17.9923
Accepted packet length average = 18.1625
Total in-flight flits = 264950 (199186 measured)
latency change    = 0.00689347
throughput change = 0.00145505
Class 0:
Packet latency average = 239.448
	minimum = 22
	maximum = 2995
Network latency average = 229.319
	minimum = 22
	maximum = 2979
Slowest packet = 17913
Flit latency average = 2000.24
	minimum = 5
	maximum = 4415
Slowest flit = 167805
Fragmentation average = 13.3843
	minimum = 0
	maximum = 322
Injected packet rate average = 0.0313351
	minimum = 0.0233333 (at node 26)
	maximum = 0.0383333 (at node 67)
Accepted packet rate average = 0.0160434
	minimum = 0.0106667 (at node 135)
	maximum = 0.023 (at node 157)
Injected flit rate average = 0.563896
	minimum = 0.421667 (at node 26)
	maximum = 0.691667 (at node 67)
Accepted flit rate average= 0.29045
	minimum = 0.187667 (at node 135)
	maximum = 0.405 (at node 157)
Injected packet length average = 17.9957
Accepted packet length average = 18.104
Total in-flight flits = 317221 (295394 measured)
latency change    = 0.810932
throughput change = 0.00176331
Draining remaining packets ...
Class 0:
Remaining flits: 201659 201660 201661 201662 201663 201664 201665 201666 201667 201668 [...] (269594 flits)
Measured flits: 321498 321499 321500 321501 321502 321503 321504 321505 321506 321507 [...] (265618 flits)
Class 0:
Remaining flits: 253260 253261 253262 253263 253264 253265 253266 253267 253268 253269 [...] (222075 flits)
Measured flits: 321509 321510 321511 321512 321513 321514 321515 321516 321517 321518 [...] (221302 flits)
Class 0:
Remaining flits: 292226 292227 292228 292229 298502 298503 298504 298505 298506 298507 [...] (174901 flits)
Measured flits: 323010 323011 323012 323013 323014 323015 323016 323017 323018 323019 [...] (174769 flits)
Class 0:
Remaining flits: 311262 311263 311264 311265 311266 311267 311268 311269 311270 311271 [...] (128268 flits)
Measured flits: 323010 323011 323012 323013 323014 323015 323016 323017 323018 323019 [...] (128250 flits)
Class 0:
Remaining flits: 323013 323014 323015 323016 323017 323018 323019 323020 323021 323022 [...] (80972 flits)
Measured flits: 323013 323014 323015 323016 323017 323018 323019 323020 323021 323022 [...] (80972 flits)
Class 0:
Remaining flits: 397638 397639 397640 397641 397642 397643 397644 397645 397646 397647 [...] (34353 flits)
Measured flits: 397638 397639 397640 397641 397642 397643 397644 397645 397646 397647 [...] (34353 flits)
Class 0:
Remaining flits: 476509 476510 476511 476512 476513 481155 481156 481157 483516 483517 [...] (2131 flits)
Measured flits: 476509 476510 476511 476512 476513 481155 481156 481157 483516 483517 [...] (2131 flits)
Time taken is 13495 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4734.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8827 (1 samples)
Network latency average = 4723.13 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8827 (1 samples)
Flit latency average = 3942.42 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8810 (1 samples)
Fragmentation average = 177.329 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1429 (1 samples)
Injected packet rate average = 0.0313351 (1 samples)
	minimum = 0.0233333 (1 samples)
	maximum = 0.0383333 (1 samples)
Accepted packet rate average = 0.0160434 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.023 (1 samples)
Injected flit rate average = 0.563896 (1 samples)
	minimum = 0.421667 (1 samples)
	maximum = 0.691667 (1 samples)
Accepted flit rate average = 0.29045 (1 samples)
	minimum = 0.187667 (1 samples)
	maximum = 0.405 (1 samples)
Injected packet size average = 17.9957 (1 samples)
Accepted packet size average = 18.104 (1 samples)
Hops average = 5.07408 (1 samples)
Total run time 14.5413
