BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 311.531
	minimum = 25
	maximum = 888
Network latency average = 295.137
	minimum = 22
	maximum = 830
Slowest packet = 726
Flit latency average = 260.967
	minimum = 5
	maximum = 844
Slowest flit = 18192
Fragmentation average = 76.058
	minimum = 0
	maximum = 383
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0149063
	minimum = 0.007 (at node 135)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.288583
	minimum = 0.151 (at node 135)
	maximum = 0.462 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 19.3599
Total in-flight flits = 85964 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.027
	minimum = 22
	maximum = 1794
Network latency average = 556.867
	minimum = 22
	maximum = 1673
Slowest packet = 2065
Flit latency average = 515.475
	minimum = 5
	maximum = 1698
Slowest flit = 39712
Fragmentation average = 106.317
	minimum = 0
	maximum = 433
Injected packet rate average = 0.0418438
	minimum = 0.032 (at node 50)
	maximum = 0.052 (at node 154)
Accepted packet rate average = 0.0156172
	minimum = 0.008 (at node 135)
	maximum = 0.0235 (at node 177)
Injected flit rate average = 0.74988
	minimum = 0.5705 (at node 50)
	maximum = 0.93 (at node 154)
Accepted flit rate average= 0.294089
	minimum = 0.1685 (at node 135)
	maximum = 0.437 (at node 152)
Injected packet length average = 17.921
Accepted packet length average = 18.8311
Total in-flight flits = 176294 (0 measured)
latency change    = 0.46011
throughput change = 0.0187196
Class 0:
Packet latency average = 1332
	minimum = 23
	maximum = 2623
Network latency average = 1306.22
	minimum = 22
	maximum = 2570
Slowest packet = 3208
Flit latency average = 1265.46
	minimum = 5
	maximum = 2553
Slowest flit = 57761
Fragmentation average = 147.404
	minimum = 0
	maximum = 456
Injected packet rate average = 0.0418542
	minimum = 0.024 (at node 17)
	maximum = 0.056 (at node 48)
Accepted packet rate average = 0.017099
	minimum = 0.007 (at node 153)
	maximum = 0.028 (at node 39)
Injected flit rate average = 0.753583
	minimum = 0.42 (at node 17)
	maximum = 1 (at node 48)
Accepted flit rate average= 0.30788
	minimum = 0.125 (at node 49)
	maximum = 0.498 (at node 187)
Injected packet length average = 18.005
Accepted packet length average = 18.0058
Total in-flight flits = 261829 (0 measured)
latency change    = 0.566798
throughput change = 0.0447956
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 71.1237
	minimum = 25
	maximum = 363
Network latency average = 42.1487
	minimum = 22
	maximum = 332
Slowest packet = 27332
Flit latency average = 1782.94
	minimum = 5
	maximum = 3224
Slowest flit = 73745
Fragmentation average = 6.08247
	minimum = 0
	maximum = 26
Injected packet rate average = 0.0426406
	minimum = 0.028 (at node 181)
	maximum = 0.054 (at node 156)
Accepted packet rate average = 0.0171979
	minimum = 0.007 (at node 74)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.767135
	minimum = 0.5 (at node 181)
	maximum = 0.984 (at node 167)
Accepted flit rate average= 0.310495
	minimum = 0.154 (at node 185)
	maximum = 0.49 (at node 16)
Injected packet length average = 17.9907
Accepted packet length average = 18.0542
Total in-flight flits = 349580 (135018 measured)
latency change    = 17.728
throughput change = 0.0084207
Class 0:
Packet latency average = 70.082
	minimum = 24
	maximum = 363
Network latency average = 39.8983
	minimum = 22
	maximum = 332
Slowest packet = 27332
Flit latency average = 2053.08
	minimum = 5
	maximum = 4095
Slowest flit = 127621
Fragmentation average = 5.79043
	minimum = 0
	maximum = 26
Injected packet rate average = 0.0424661
	minimum = 0.0265 (at node 104)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0169844
	minimum = 0.0105 (at node 43)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.764622
	minimum = 0.479 (at node 104)
	maximum = 0.9545 (at node 118)
Accepted flit rate average= 0.306461
	minimum = 0.194 (at node 86)
	maximum = 0.434 (at node 16)
Injected packet length average = 18.0055
Accepted packet length average = 18.0437
Total in-flight flits = 437674 (269673 measured)
latency change    = 0.0148641
throughput change = 0.0131627
Class 0:
Packet latency average = 69.128
	minimum = 23
	maximum = 363
Network latency average = 40.2397
	minimum = 22
	maximum = 332
Slowest packet = 27332
Flit latency average = 2321.2
	minimum = 5
	maximum = 4659
Slowest flit = 189832
Fragmentation average = 5.91025
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0423819
	minimum = 0.0336667 (at node 104)
	maximum = 0.0506667 (at node 9)
Accepted packet rate average = 0.0169722
	minimum = 0.0123333 (at node 4)
	maximum = 0.0243333 (at node 129)
Injected flit rate average = 0.762951
	minimum = 0.604667 (at node 104)
	maximum = 0.912667 (at node 9)
Accepted flit rate average= 0.306196
	minimum = 0.230333 (at node 5)
	maximum = 0.432 (at node 129)
Injected packet length average = 18.0018
Accepted packet length average = 18.041
Total in-flight flits = 524876 (403970 measured)
latency change    = 0.0138006
throughput change = 0.000864664
Class 0:
Packet latency average = 98.6758
	minimum = 23
	maximum = 3934
Network latency average = 70.5366
	minimum = 22
	maximum = 3878
Slowest packet = 24255
Flit latency average = 2581.15
	minimum = 5
	maximum = 5470
Slowest flit = 187289
Fragmentation average = 7.43774
	minimum = 0
	maximum = 325
Injected packet rate average = 0.0424362
	minimum = 0.03375 (at node 178)
	maximum = 0.0495 (at node 129)
Accepted packet rate average = 0.0170078
	minimum = 0.01225 (at node 79)
	maximum = 0.02175 (at node 138)
Injected flit rate average = 0.763776
	minimum = 0.6065 (at node 178)
	maximum = 0.89325 (at node 129)
Accepted flit rate average= 0.306728
	minimum = 0.22575 (at node 79)
	maximum = 0.392 (at node 138)
Injected packet length average = 17.9982
Accepted packet length average = 18.0345
Total in-flight flits = 612900 (538455 measured)
latency change    = 0.299444
throughput change = 0.00173341
Class 0:
Packet latency average = 538.434
	minimum = 23
	maximum = 5049
Network latency average = 510.436
	minimum = 22
	maximum = 4973
Slowest packet = 24105
Flit latency average = 2837.2
	minimum = 5
	maximum = 6386
Slowest flit = 228162
Fragmentation average = 25.0205
	minimum = 0
	maximum = 401
Injected packet rate average = 0.042474
	minimum = 0.0356 (at node 10)
	maximum = 0.0484 (at node 14)
Accepted packet rate average = 0.0170781
	minimum = 0.0128 (at node 80)
	maximum = 0.0222 (at node 138)
Injected flit rate average = 0.76463
	minimum = 0.6408 (at node 10)
	maximum = 0.8712 (at node 14)
Accepted flit rate average= 0.307377
	minimum = 0.2274 (at node 80)
	maximum = 0.399 (at node 138)
Injected packet length average = 18.0023
Accepted packet length average = 17.9983
Total in-flight flits = 700697 (665628 measured)
latency change    = 0.816735
throughput change = 0.00211212
Class 0:
Packet latency average = 1505.87
	minimum = 23
	maximum = 6002
Network latency average = 1478.6
	minimum = 22
	maximum = 5980
Slowest packet = 24118
Flit latency average = 3099.38
	minimum = 5
	maximum = 6841
Slowest flit = 310923
Fragmentation average = 57.4428
	minimum = 0
	maximum = 431
Injected packet rate average = 0.0424418
	minimum = 0.0361667 (at node 81)
	maximum = 0.0493333 (at node 68)
Accepted packet rate average = 0.0170616
	minimum = 0.0131667 (at node 63)
	maximum = 0.0216667 (at node 103)
Injected flit rate average = 0.763965
	minimum = 0.651 (at node 81)
	maximum = 0.888333 (at node 68)
Accepted flit rate average= 0.307428
	minimum = 0.238167 (at node 80)
	maximum = 0.391667 (at node 103)
Injected packet length average = 18.0003
Accepted packet length average = 18.0187
Total in-flight flits = 787746 (776588 measured)
latency change    = 0.642444
throughput change = 0.000165463
Class 0:
Packet latency average = 2526.94
	minimum = 23
	maximum = 7051
Network latency average = 2499.93
	minimum = 22
	maximum = 6943
Slowest packet = 24155
Flit latency average = 3350.56
	minimum = 5
	maximum = 7613
Slowest flit = 342244
Fragmentation average = 86.764
	minimum = 0
	maximum = 431
Injected packet rate average = 0.0423586
	minimum = 0.037 (at node 187)
	maximum = 0.0495714 (at node 154)
Accepted packet rate average = 0.0171109
	minimum = 0.013 (at node 80)
	maximum = 0.0214286 (at node 103)
Injected flit rate average = 0.762428
	minimum = 0.668 (at node 81)
	maximum = 0.890429 (at node 154)
Accepted flit rate average= 0.308186
	minimum = 0.233857 (at node 80)
	maximum = 0.388286 (at node 103)
Injected packet length average = 17.9994
Accepted packet length average = 18.0111
Total in-flight flits = 872367 (870084 measured)
latency change    = 0.404073
throughput change = 0.00245975
Draining all recorded packets ...
Class 0:
Remaining flits: 375768 375769 375770 375771 375772 375773 375774 375775 375776 375777 [...] (957699 flits)
Measured flits: 435744 435745 435746 435747 435748 435749 435750 435751 435752 435753 [...] (828136 flits)
Class 0:
Remaining flits: 436067 448141 448142 448143 448144 448145 449712 449713 449714 449715 [...] (1038955 flits)
Measured flits: 436067 448141 448142 448143 448144 448145 449712 449713 449714 449715 [...] (780969 flits)
Class 0:
Remaining flits: 476172 476173 476174 476175 476176 476177 476178 476179 476180 476181 [...] (1125985 flits)
Measured flits: 476172 476173 476174 476175 476176 476177 476178 476179 476180 476181 [...] (733143 flits)
Class 0:
Remaining flits: 481446 481447 481448 481449 481450 481451 481452 481453 481454 481455 [...] (1212430 flits)
Measured flits: 481446 481447 481448 481449 481450 481451 481452 481453 481454 481455 [...] (685832 flits)
Class 0:
Remaining flits: 552819 552820 552821 552822 552823 552824 552825 552826 552827 552828 [...] (1297009 flits)
Measured flits: 552819 552820 552821 552822 552823 552824 552825 552826 552827 552828 [...] (638349 flits)
Class 0:
Remaining flits: 584131 584132 584133 584134 584135 615366 615367 615368 615369 615370 [...] (1382077 flits)
Measured flits: 584131 584132 584133 584134 584135 615366 615367 615368 615369 615370 [...] (591197 flits)
Class 0:
Remaining flits: 644428 644429 644430 644431 644432 644433 644434 644435 667404 667405 [...] (1461178 flits)
Measured flits: 644428 644429 644430 644431 644432 644433 644434 644435 667404 667405 [...] (543662 flits)
Class 0:
Remaining flits: 692026 692027 712404 712405 712406 712407 712408 712409 712410 712411 [...] (1538679 flits)
Measured flits: 692026 692027 712404 712405 712406 712407 712408 712409 712410 712411 [...] (496397 flits)
Class 0:
Remaining flits: 740538 740539 740540 740541 740542 740543 740544 740545 740546 740547 [...] (1615255 flits)
Measured flits: 740538 740539 740540 740541 740542 740543 740544 740545 740546 740547 [...] (448659 flits)
Class 0:
Remaining flits: 793926 793927 793928 793929 793930 793931 793932 793933 793934 793935 [...] (1693612 flits)
Measured flits: 793926 793927 793928 793929 793930 793931 793932 793933 793934 793935 [...] (401655 flits)
Class 0:
Remaining flits: 816431 816432 816433 816434 816435 816436 816437 816438 816439 816440 [...] (1761893 flits)
Measured flits: 816431 816432 816433 816434 816435 816436 816437 816438 816439 816440 [...] (354555 flits)
Class 0:
Remaining flits: 892409 892410 892411 892412 892413 892414 892415 892416 892417 892418 [...] (1827434 flits)
Measured flits: 892409 892410 892411 892412 892413 892414 892415 892416 892417 892418 [...] (306947 flits)
Class 0:
Remaining flits: 895948 895949 928296 928297 928298 928299 928300 928301 928302 928303 [...] (1893417 flits)
Measured flits: 895948 895949 928296 928297 928298 928299 928300 928301 928302 928303 [...] (259091 flits)
Class 0:
Remaining flits: 963538 963539 978444 978445 978446 978447 978448 978449 978450 978451 [...] (1959686 flits)
Measured flits: 963538 963539 978444 978445 978446 978447 978448 978449 978450 978451 [...] (212212 flits)
Class 0:
Remaining flits: 1015470 1015471 1015472 1015473 1015474 1015475 1015476 1015477 1015478 1015479 [...] (2027456 flits)
Measured flits: 1015470 1015471 1015472 1015473 1015474 1015475 1015476 1015477 1015478 1015479 [...] (166714 flits)
Class 0:
Remaining flits: 1043262 1043263 1043264 1043265 1043266 1043267 1043268 1043269 1043270 1043271 [...] (2095222 flits)
Measured flits: 1043262 1043263 1043264 1043265 1043266 1043267 1043268 1043269 1043270 1043271 [...] (123658 flits)
Class 0:
Remaining flits: 1088172 1088173 1088174 1088175 1088176 1088177 1088178 1088179 1088180 1088181 [...] (2161015 flits)
Measured flits: 1088172 1088173 1088174 1088175 1088176 1088177 1088178 1088179 1088180 1088181 [...] (84293 flits)
Class 0:
Remaining flits: 1114200 1114201 1114202 1114203 1114204 1114205 1114206 1114207 1114208 1114209 [...] (2230277 flits)
Measured flits: 1114200 1114201 1114202 1114203 1114204 1114205 1114206 1114207 1114208 1114209 [...] (52288 flits)
Class 0:
Remaining flits: 1138820 1138821 1138822 1138823 1181178 1181179 1181180 1181181 1181182 1181183 [...] (2298258 flits)
Measured flits: 1138820 1138821 1138822 1138823 1181178 1181179 1181180 1181181 1181182 1181183 [...] (28942 flits)
Class 0:
Remaining flits: 1213245 1213246 1213247 1213248 1213249 1213250 1213251 1213252 1213253 1215198 [...] (2362821 flits)
Measured flits: 1213245 1213246 1213247 1213248 1213249 1213250 1213251 1213252 1213253 1215198 [...] (14359 flits)
Class 0:
Remaining flits: 1244698 1244699 1291248 1291249 1291250 1291251 1291252 1291253 1291254 1291255 [...] (2426288 flits)
Measured flits: 1244698 1244699 1291248 1291249 1291250 1291251 1291252 1291253 1291254 1291255 [...] (6508 flits)
Class 0:
Remaining flits: 1308870 1308871 1308872 1308873 1308874 1308875 1308876 1308877 1308878 1308879 [...] (2493055 flits)
Measured flits: 1308870 1308871 1308872 1308873 1308874 1308875 1308876 1308877 1308878 1308879 [...] (2605 flits)
Class 0:
Remaining flits: 1367167 1367168 1367169 1367170 1367171 1375691 1375692 1375693 1375694 1375695 [...] (2559174 flits)
Measured flits: 1367167 1367168 1367169 1367170 1367171 1375691 1375692 1375693 1375694 1375695 [...] (836 flits)
Class 0:
Remaining flits: 1413180 1413181 1413182 1413183 1413184 1413185 1413186 1413187 1413188 1413189 [...] (2614682 flits)
Measured flits: 1413180 1413181 1413182 1413183 1413184 1413185 1413186 1413187 1413188 1413189 [...] (326 flits)
Class 0:
Remaining flits: 1447865 1456112 1456113 1456114 1456115 1456116 1456117 1456118 1456119 1456120 [...] (2654953 flits)
Measured flits: 1447865 1456112 1456113 1456114 1456115 1456116 1456117 1456118 1456119 1456120 [...] (46 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1518930 1518931 1518932 1518933 1518934 1518935 1518936 1518937 1518938 1518939 [...] (2623393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1530450 1530451 1530452 1530453 1530454 1530455 1530456 1530457 1530458 1530459 [...] (2573341 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1580884 1580885 1605508 1605509 1608118 1608119 1612116 1612117 1612118 1612119 [...] (2522729 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1638807 1638808 1638809 1658124 1658125 1658126 1658127 1658128 1658129 1658130 [...] (2472578 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1703628 1703629 1703630 1703631 1703632 1703633 1703634 1703635 1703636 1703637 [...] (2422517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1727388 1727389 1727390 1727391 1727392 1727393 1727394 1727395 1727396 1727397 [...] (2371966 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1736662 1736663 1736664 1736665 1736666 1736667 1736668 1736669 1736670 1736671 [...] (2321664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1807704 1807705 1807706 1807707 1807708 1807709 1807710 1807711 1807712 1807713 [...] (2271039 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1807720 1807721 1813608 1813609 1813610 1813611 1813612 1813613 1813614 1813615 [...] (2220540 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1852866 1852867 1852868 1852869 1852870 1852871 1852872 1852873 1852874 1852875 [...] (2169676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1914397 1914398 1914399 1914400 1914401 1914402 1914403 1914404 1914405 1914406 [...] (2118735 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1929006 1929007 1929008 1929009 1929010 1929011 1929012 1929013 1929014 1929015 [...] (2068347 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1929006 1929007 1929008 1929009 1929010 1929011 1929012 1929013 1929014 1929015 [...] (2017972 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1929015 1929016 1929017 1929018 1929019 1929020 1929021 1929022 1929023 1971054 [...] (1966843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1971066 1971067 1971068 1971069 1971070 1971071 1994220 1994221 1994222 1994223 [...] (1915469 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1994220 1994221 1994222 1994223 1994224 1994225 1994226 1994227 1994228 1994229 [...] (1864780 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031912 2031913 2031914 2031915 2031916 2031917 2031918 2031919 2031920 2031921 [...] (1813513 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031912 2031913 2031914 2031915 2031916 2031917 2031918 2031919 2031920 2031921 [...] (1763061 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031912 2031913 2031914 2031915 2031916 2031917 2031918 2031919 2031920 2031921 [...] (1712684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122359 2122360 2122361 2141568 2141569 2141570 2141571 2141572 2141573 2141574 [...] (1662546 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2176848 2176849 2176850 2176851 2176852 2176853 2176854 2176855 2176856 2176857 [...] (1612804 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2176848 2176849 2176850 2176851 2176852 2176853 2176854 2176855 2176856 2176857 [...] (1563819 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2208762 2208763 2208764 2208765 2208766 2208767 2208768 2208769 2208770 2208771 [...] (1514845 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2230686 2230687 2230688 2230689 2230690 2230691 2230692 2230693 2230694 2230695 [...] (1466661 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2319102 2319103 2319104 2319105 2319106 2319107 2319108 2319109 2319110 2319111 [...] (1418796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2319102 2319103 2319104 2319105 2319106 2319107 2319108 2319109 2319110 2319111 [...] (1370757 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2394756 2394757 2394758 2394759 2394760 2394761 2394762 2394763 2394764 2394765 [...] (1322965 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2476170 2476171 2476172 2476173 2476174 2476175 2476176 2476177 2476178 2476179 [...] (1275551 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2509272 2509273 2509274 2509275 2509276 2509277 2509278 2509279 2509280 2509281 [...] (1227847 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2509281 2509282 2509283 2509284 2509285 2509286 2509287 2509288 2509289 2523852 [...] (1180324 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2523860 2523861 2523862 2523863 2523864 2523865 2523866 2523867 2523868 2523869 [...] (1132399 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2615796 2615797 2615798 2615799 2615800 2615801 2615802 2615803 2615804 2615805 [...] (1084713 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2638800 2638801 2638802 2638803 2638804 2638805 2638806 2638807 2638808 2638809 [...] (1037149 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2750688 2750689 2750690 2750691 2750692 2750693 2750694 2750695 2750696 2750697 [...] (989548 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2832570 2832571 2832572 2832573 2832574 2832575 2832576 2832577 2832578 2832579 [...] (942163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2880450 2880451 2880452 2880453 2880454 2880455 2880456 2880457 2880458 2880459 [...] (894388 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2885184 2885185 2885186 2885187 2885188 2885189 2885190 2885191 2885192 2885193 [...] (846717 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2885184 2885185 2885186 2885187 2885188 2885189 2885190 2885191 2885192 2885193 [...] (799286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2987404 2987405 3023100 3023101 3023102 3023103 3023104 3023105 3023106 3023107 [...] (751363 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3027366 3027367 3027368 3027369 3027370 3027371 3027372 3027373 3027374 3027375 [...] (703276 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3027366 3027367 3027368 3027369 3027370 3027371 3027372 3027373 3027374 3027375 [...] (655406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3093444 3093445 3093446 3093447 3093448 3093449 3093450 3093451 3093452 3093453 [...] (607564 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3196764 3196765 3196766 3196767 3196768 3196769 3196770 3196771 3196772 3196773 [...] (559749 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3290544 3290545 3290546 3290547 3290548 3290549 3290550 3290551 3290552 3290553 [...] (512142 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3364901 3375360 3375361 3375362 3375363 3375364 3375365 3375366 3375367 3375368 [...] (464709 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3417458 3417459 3417460 3417461 3434778 3434779 3434780 3434781 3434782 3434783 [...] (417197 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3477137 3477138 3477139 3477140 3477141 3477142 3477143 3477144 3477145 3477146 [...] (369213 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3543406 3543407 3554514 3554515 3554516 3554517 3554518 3554519 3554520 3554521 [...] (321191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3554514 3554515 3554516 3554517 3554518 3554519 3554520 3554521 3554522 3554523 [...] (273589 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3573576 3573577 3573578 3573579 3573580 3573581 3573582 3573583 3573584 3573585 [...] (226435 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3573576 3573577 3573578 3573579 3573580 3573581 3573582 3573583 3573584 3573585 [...] (179042 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3619548 3619549 3619550 3619551 3619552 3619553 3619554 3619555 3619556 3619557 [...] (131789 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3652128 3652129 3652130 3652131 3652132 3652133 3652134 3652135 3652136 3652137 [...] (84912 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3747618 3747619 3747620 3747621 3747622 3747623 3747624 3747625 3747626 3747627 [...] (41977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3747634 3747635 3987234 3987235 3987236 3987237 3987238 3987239 3987240 3987241 [...] (14116 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4025574 4025575 4025576 4025577 4025578 4025579 4025580 4025581 4025582 4025583 [...] (4040 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4172778 4172779 4172780 4172781 4172782 4172783 4172784 4172785 4172786 4172787 [...] (1158 flits)
Measured flits: (0 flits)
Time taken is 93622 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11227.9 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25764 (1 samples)
Network latency average = 11199.7 (1 samples)
	minimum = 22 (1 samples)
	maximum = 25632 (1 samples)
Flit latency average = 27487 (1 samples)
	minimum = 5 (1 samples)
	maximum = 63895 (1 samples)
Fragmentation average = 174.191 (1 samples)
	minimum = 0 (1 samples)
	maximum = 515 (1 samples)
Injected packet rate average = 0.0423586 (1 samples)
	minimum = 0.037 (1 samples)
	maximum = 0.0495714 (1 samples)
Accepted packet rate average = 0.0171109 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.0214286 (1 samples)
Injected flit rate average = 0.762428 (1 samples)
	minimum = 0.668 (1 samples)
	maximum = 0.890429 (1 samples)
Accepted flit rate average = 0.308186 (1 samples)
	minimum = 0.233857 (1 samples)
	maximum = 0.388286 (1 samples)
Injected packet size average = 17.9994 (1 samples)
Accepted packet size average = 18.0111 (1 samples)
Hops average = 5.07007 (1 samples)
Total run time 123.057
