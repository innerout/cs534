BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.275
	minimum = 23
	maximum = 932
Network latency average = 313.169
	minimum = 23
	maximum = 904
Slowest packet = 532
Flit latency average = 282.874
	minimum = 6
	maximum = 930
Slowest flit = 6329
Fragmentation average = 55.4092
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0340156
	minimum = 0.02 (at node 60)
	maximum = 0.049 (at node 10)
Accepted packet rate average = 0.0102708
	minimum = 0.003 (at node 64)
	maximum = 0.019 (at node 165)
Injected flit rate average = 0.606365
	minimum = 0.354 (at node 144)
	maximum = 0.877 (at node 10)
Accepted flit rate average= 0.193167
	minimum = 0.054 (at node 64)
	maximum = 0.342 (at node 165)
Injected packet length average = 17.8261
Accepted packet length average = 18.8073
Total in-flight flits = 80650 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 668.713
	minimum = 23
	maximum = 1804
Network latency average = 635.591
	minimum = 23
	maximum = 1788
Slowest packet = 975
Flit latency average = 601.046
	minimum = 6
	maximum = 1815
Slowest flit = 20330
Fragmentation average = 61.4907
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0285026
	minimum = 0.016 (at node 132)
	maximum = 0.037 (at node 82)
Accepted packet rate average = 0.0101797
	minimum = 0.0055 (at node 164)
	maximum = 0.0155 (at node 78)
Injected flit rate average = 0.509732
	minimum = 0.288 (at node 132)
	maximum = 0.666 (at node 82)
Accepted flit rate average= 0.18737
	minimum = 0.1075 (at node 164)
	maximum = 0.2875 (at node 78)
Injected packet length average = 17.8837
Accepted packet length average = 18.4062
Total in-flight flits = 126194 (0 measured)
latency change    = 0.510589
throughput change = 0.0309382
Class 0:
Packet latency average = 1770.84
	minimum = 44
	maximum = 2668
Network latency average = 1632.62
	minimum = 23
	maximum = 2653
Slowest packet = 2166
Flit latency average = 1607.2
	minimum = 6
	maximum = 2787
Slowest flit = 24863
Fragmentation average = 69.9834
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0109687
	minimum = 0 (at node 37)
	maximum = 0.036 (at node 49)
Accepted packet rate average = 0.00938542
	minimum = 0.003 (at node 40)
	maximum = 0.017 (at node 33)
Injected flit rate average = 0.200984
	minimum = 0 (at node 37)
	maximum = 0.65 (at node 49)
Accepted flit rate average= 0.168724
	minimum = 0.044 (at node 153)
	maximum = 0.295 (at node 33)
Injected packet length average = 18.3234
Accepted packet length average = 17.9772
Total in-flight flits = 132733 (0 measured)
latency change    = 0.622375
throughput change = 0.110511
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1949.85
	minimum = 980
	maximum = 2926
Network latency average = 152.854
	minimum = 23
	maximum = 833
Slowest packet = 13201
Flit latency average = 2292.43
	minimum = 6
	maximum = 3503
Slowest flit = 54341
Fragmentation average = 23.9792
	minimum = 0
	maximum = 118
Injected packet rate average = 0.00856771
	minimum = 0 (at node 62)
	maximum = 0.024 (at node 132)
Accepted packet rate average = 0.0093125
	minimum = 0.003 (at node 54)
	maximum = 0.017 (at node 89)
Injected flit rate average = 0.153307
	minimum = 0 (at node 62)
	maximum = 0.432 (at node 132)
Accepted flit rate average= 0.166833
	minimum = 0.054 (at node 54)
	maximum = 0.302 (at node 89)
Injected packet length average = 17.8936
Accepted packet length average = 17.915
Total in-flight flits = 130203 (28610 measured)
latency change    = 0.0918086
throughput change = 0.0113324
Class 0:
Packet latency average = 2661.03
	minimum = 980
	maximum = 3973
Network latency average = 626.99
	minimum = 23
	maximum = 1884
Slowest packet = 13201
Flit latency average = 2576.76
	minimum = 6
	maximum = 4435
Slowest flit = 66450
Fragmentation average = 44.2798
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0084974
	minimum = 0.0005 (at node 0)
	maximum = 0.0235 (at node 118)
Accepted packet rate average = 0.00924219
	minimum = 0.004 (at node 31)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.152432
	minimum = 0.009 (at node 0)
	maximum = 0.423 (at node 118)
Accepted flit rate average= 0.165513
	minimum = 0.0675 (at node 31)
	maximum = 0.29 (at node 63)
Injected packet length average = 17.9387
Accepted packet length average = 17.9084
Total in-flight flits = 127640 (54812 measured)
latency change    = 0.267256
throughput change = 0.00797709
Class 0:
Packet latency average = 3364.9
	minimum = 980
	maximum = 5002
Network latency average = 1117.26
	minimum = 23
	maximum = 2913
Slowest packet = 13201
Flit latency average = 2818.94
	minimum = 6
	maximum = 5249
Slowest flit = 86849
Fragmentation average = 52.4242
	minimum = 0
	maximum = 136
Injected packet rate average = 0.00852083
	minimum = 0.001 (at node 136)
	maximum = 0.0196667 (at node 118)
Accepted packet rate average = 0.00921181
	minimum = 0.00533333 (at node 4)
	maximum = 0.014 (at node 177)
Injected flit rate average = 0.153373
	minimum = 0.018 (at node 136)
	maximum = 0.354 (at node 118)
Accepted flit rate average= 0.165681
	minimum = 0.101667 (at node 4)
	maximum = 0.248 (at node 177)
Injected packet length average = 17.9998
Accepted packet length average = 17.9857
Total in-flight flits = 125843 (78209 measured)
latency change    = 0.209181
throughput change = 0.00101119
Draining remaining packets ...
Class 0:
Remaining flits: 91422 91423 91424 91425 91426 91427 91428 91429 91430 91431 [...] (95735 flits)
Measured flits: 237078 237079 237080 237081 237082 237083 237084 237085 237086 237087 [...] (67837 flits)
Class 0:
Remaining flits: 91422 91423 91424 91425 91426 91427 91428 91429 91430 91431 [...] (65701 flits)
Measured flits: 237078 237079 237080 237081 237082 237083 237084 237085 237086 237087 [...] (52551 flits)
Class 0:
Remaining flits: 97884 97885 97886 97887 97888 97889 97890 97891 97892 97893 [...] (36259 flits)
Measured flits: 237096 237097 237098 237099 237100 237101 237102 237103 237104 237105 [...] (31608 flits)
Class 0:
Remaining flits: 107759 107760 107761 107762 107763 107764 107765 173250 173251 173252 [...] (9424 flits)
Measured flits: 237420 237421 237422 237423 237424 237425 237426 237427 237428 237429 [...] (8561 flits)
Time taken is 10905 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5984.15 (1 samples)
	minimum = 980 (1 samples)
	maximum = 8928 (1 samples)
Network latency average = 3607.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7412 (1 samples)
Flit latency average = 3734.29 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9102 (1 samples)
Fragmentation average = 62.3527 (1 samples)
	minimum = 0 (1 samples)
	maximum = 169 (1 samples)
Injected packet rate average = 0.00852083 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.0196667 (1 samples)
Accepted packet rate average = 0.00921181 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.153373 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.354 (1 samples)
Accepted flit rate average = 0.165681 (1 samples)
	minimum = 0.101667 (1 samples)
	maximum = 0.248 (1 samples)
Injected packet size average = 17.9998 (1 samples)
Accepted packet size average = 17.9857 (1 samples)
Hops average = 5.08579 (1 samples)
Total run time 12.09
