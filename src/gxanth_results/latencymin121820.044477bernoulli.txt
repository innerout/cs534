BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 344.127
	minimum = 22
	maximum = 850
Network latency average = 252.27
	minimum = 22
	maximum = 834
Slowest packet = 578
Flit latency average = 221.873
	minimum = 5
	maximum = 817
Slowest flit = 18053
Fragmentation average = 23.0617
	minimum = 0
	maximum = 129
Injected packet rate average = 0.019125
	minimum = 0.009 (at node 156)
	maximum = 0.036 (at node 102)
Accepted packet rate average = 0.0137552
	minimum = 0.007 (at node 25)
	maximum = 0.022 (at node 22)
Injected flit rate average = 0.339656
	minimum = 0.162 (at node 156)
	maximum = 0.635 (at node 102)
Accepted flit rate average= 0.25362
	minimum = 0.126 (at node 25)
	maximum = 0.41 (at node 22)
Injected packet length average = 17.7598
Accepted packet length average = 18.4381
Total in-flight flits = 19129 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 675.042
	minimum = 22
	maximum = 1696
Network latency average = 309.67
	minimum = 22
	maximum = 1310
Slowest packet = 578
Flit latency average = 275.843
	minimum = 5
	maximum = 1293
Slowest flit = 51406
Fragmentation average = 23.1957
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0164896
	minimum = 0.009 (at node 176)
	maximum = 0.024 (at node 54)
Accepted packet rate average = 0.013974
	minimum = 0.0075 (at node 153)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.294831
	minimum = 0.162 (at node 176)
	maximum = 0.427 (at node 54)
Accepted flit rate average= 0.253859
	minimum = 0.142 (at node 153)
	maximum = 0.3695 (at node 152)
Injected packet length average = 17.8798
Accepted packet length average = 18.1666
Total in-flight flits = 18456 (0 measured)
latency change    = 0.490213
throughput change = 0.000943764
Class 0:
Packet latency average = 1684.65
	minimum = 786
	maximum = 2481
Network latency average = 364.281
	minimum = 22
	maximum = 1435
Slowest packet = 5658
Flit latency average = 326.775
	minimum = 5
	maximum = 1412
Slowest flit = 91926
Fragmentation average = 22.7599
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0140104
	minimum = 0.003 (at node 17)
	maximum = 0.032 (at node 166)
Accepted packet rate average = 0.0139063
	minimum = 0.006 (at node 4)
	maximum = 0.026 (at node 34)
Injected flit rate average = 0.251958
	minimum = 0.058 (at node 17)
	maximum = 0.576 (at node 166)
Accepted flit rate average= 0.250615
	minimum = 0.097 (at node 163)
	maximum = 0.483 (at node 34)
Injected packet length average = 17.9836
Accepted packet length average = 18.0217
Total in-flight flits = 18686 (0 measured)
latency change    = 0.599299
throughput change = 0.0129473
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2466.06
	minimum = 1484
	maximum = 3305
Network latency average = 283.314
	minimum = 22
	maximum = 937
Slowest packet = 9133
Flit latency average = 322.438
	minimum = 5
	maximum = 1518
Slowest flit = 97991
Fragmentation average = 21.9678
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0145156
	minimum = 0.003 (at node 129)
	maximum = 0.028 (at node 142)
Accepted packet rate average = 0.0143906
	minimum = 0.007 (at node 72)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.261573
	minimum = 0.054 (at node 129)
	maximum = 0.509 (at node 142)
Accepted flit rate average= 0.259354
	minimum = 0.137 (at node 72)
	maximum = 0.486 (at node 19)
Injected packet length average = 18.0201
Accepted packet length average = 18.0224
Total in-flight flits = 18948 (18883 measured)
latency change    = 0.316864
throughput change = 0.0336975
Class 0:
Packet latency average = 2825.78
	minimum = 1484
	maximum = 4156
Network latency average = 325.613
	minimum = 22
	maximum = 1283
Slowest packet = 9133
Flit latency average = 319.924
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.0992
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0142813
	minimum = 0.0055 (at node 98)
	maximum = 0.025 (at node 3)
Accepted packet rate average = 0.0142891
	minimum = 0.0085 (at node 5)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.256935
	minimum = 0.1045 (at node 129)
	maximum = 0.457 (at node 3)
Accepted flit rate average= 0.257466
	minimum = 0.161 (at node 5)
	maximum = 0.3985 (at node 129)
Injected packet length average = 17.9911
Accepted packet length average = 18.0184
Total in-flight flits = 18459 (18459 measured)
latency change    = 0.1273
throughput change = 0.00733308
Class 0:
Packet latency average = 3173.81
	minimum = 1484
	maximum = 4879
Network latency average = 342.93
	minimum = 22
	maximum = 1642
Slowest packet = 9133
Flit latency average = 324.87
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.288
	minimum = 0
	maximum = 123
Injected packet rate average = 0.0141736
	minimum = 0.00666667 (at node 98)
	maximum = 0.0223333 (at node 66)
Accepted packet rate average = 0.014217
	minimum = 0.00966667 (at node 36)
	maximum = 0.0203333 (at node 138)
Injected flit rate average = 0.255234
	minimum = 0.125667 (at node 98)
	maximum = 0.406333 (at node 66)
Accepted flit rate average= 0.255854
	minimum = 0.169667 (at node 36)
	maximum = 0.366 (at node 138)
Injected packet length average = 18.0077
Accepted packet length average = 17.9963
Total in-flight flits = 18338 (18338 measured)
latency change    = 0.109657
throughput change = 0.00630038
Class 0:
Packet latency average = 3516.9
	minimum = 1484
	maximum = 5574
Network latency average = 346.827
	minimum = 22
	maximum = 1642
Slowest packet = 9133
Flit latency average = 323.844
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.5104
	minimum = 0
	maximum = 136
Injected packet rate average = 0.0141172
	minimum = 0.0075 (at node 98)
	maximum = 0.022 (at node 3)
Accepted packet rate average = 0.0141602
	minimum = 0.01 (at node 89)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.254267
	minimum = 0.13925 (at node 98)
	maximum = 0.39925 (at node 3)
Accepted flit rate average= 0.254944
	minimum = 0.18 (at node 89)
	maximum = 0.34775 (at node 129)
Injected packet length average = 18.0112
Accepted packet length average = 18.0043
Total in-flight flits = 18135 (18135 measured)
latency change    = 0.0975555
throughput change = 0.00357002
Class 0:
Packet latency average = 3853.81
	minimum = 1484
	maximum = 6332
Network latency average = 349.375
	minimum = 22
	maximum = 1743
Slowest packet = 9133
Flit latency average = 323.659
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.3728
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0141031
	minimum = 0.009 (at node 64)
	maximum = 0.0218 (at node 3)
Accepted packet rate average = 0.0140781
	minimum = 0.0106 (at node 64)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.254016
	minimum = 0.162 (at node 64)
	maximum = 0.3952 (at node 3)
Accepted flit rate average= 0.253644
	minimum = 0.1924 (at node 64)
	maximum = 0.3344 (at node 128)
Injected packet length average = 18.0113
Accepted packet length average = 18.0169
Total in-flight flits = 18962 (18962 measured)
latency change    = 0.0874232
throughput change = 0.00512633
Class 0:
Packet latency average = 4195.83
	minimum = 1484
	maximum = 7101
Network latency average = 353.091
	minimum = 22
	maximum = 1773
Slowest packet = 9133
Flit latency average = 325.66
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.5019
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0140451
	minimum = 0.00933333 (at node 186)
	maximum = 0.0211667 (at node 3)
Accepted packet rate average = 0.0140712
	minimum = 0.011 (at node 80)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.252892
	minimum = 0.168 (at node 186)
	maximum = 0.383333 (at node 3)
Accepted flit rate average= 0.253227
	minimum = 0.197667 (at node 80)
	maximum = 0.325333 (at node 128)
Injected packet length average = 18.0057
Accepted packet length average = 17.9961
Total in-flight flits = 18353 (18353 measured)
latency change    = 0.081514
throughput change = 0.00164749
Class 0:
Packet latency average = 4528.66
	minimum = 1484
	maximum = 7932
Network latency average = 354.551
	minimum = 22
	maximum = 1773
Slowest packet = 9133
Flit latency average = 325.811
	minimum = 5
	maximum = 1863
Slowest flit = 154043
Fragmentation average = 22.4995
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0139762
	minimum = 0.00971429 (at node 186)
	maximum = 0.0205714 (at node 3)
Accepted packet rate average = 0.0139993
	minimum = 0.0111429 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.251706
	minimum = 0.172429 (at node 186)
	maximum = 0.372286 (at node 3)
Accepted flit rate average= 0.252048
	minimum = 0.200571 (at node 79)
	maximum = 0.324 (at node 138)
Injected packet length average = 18.0096
Accepted packet length average = 18.0044
Total in-flight flits = 18208 (18208 measured)
latency change    = 0.0734938
throughput change = 0.00467746
Draining all recorded packets ...
Class 0:
Remaining flits: 441792 441793 441794 441795 441796 441797 441798 441799 441800 441801 [...] (18224 flits)
Measured flits: 441792 441793 441794 441795 441796 441797 441798 441799 441800 441801 [...] (18224 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (18604 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (18604 flits)
Class 0:
Remaining flits: 570618 570619 570620 570621 570622 570623 570624 570625 570626 570627 [...] (18825 flits)
Measured flits: 570618 570619 570620 570621 570622 570623 570624 570625 570626 570627 [...] (18825 flits)
Class 0:
Remaining flits: 614122 614123 627076 627077 627078 627079 627080 627081 627082 627083 [...] (18768 flits)
Measured flits: 614122 614123 627076 627077 627078 627079 627080 627081 627082 627083 [...] (18768 flits)
Class 0:
Remaining flits: 681331 681332 681333 681334 681335 682794 682795 682796 682797 682798 [...] (18357 flits)
Measured flits: 681331 681332 681333 681334 681335 682794 682795 682796 682797 682798 [...] (18357 flits)
Class 0:
Remaining flits: 714440 714441 714442 714443 714444 714445 714446 714447 714448 714449 [...] (18292 flits)
Measured flits: 714440 714441 714442 714443 714444 714445 714446 714447 714448 714449 [...] (18292 flits)
Class 0:
Remaining flits: 784314 784315 784316 784317 784318 784319 784320 784321 784322 784323 [...] (18706 flits)
Measured flits: 784314 784315 784316 784317 784318 784319 784320 784321 784322 784323 [...] (18706 flits)
Class 0:
Remaining flits: 824904 824905 824906 824907 824908 824909 824910 824911 824912 824913 [...] (18262 flits)
Measured flits: 824904 824905 824906 824907 824908 824909 824910 824911 824912 824913 [...] (18262 flits)
Class 0:
Remaining flits: 876600 876601 876602 876603 876604 876605 876606 876607 876608 876609 [...] (18551 flits)
Measured flits: 876600 876601 876602 876603 876604 876605 876606 876607 876608 876609 [...] (18551 flits)
Class 0:
Remaining flits: 922858 922859 923948 923949 923950 923951 923952 923953 923954 923955 [...] (18515 flits)
Measured flits: 922858 922859 923948 923949 923950 923951 923952 923953 923954 923955 [...] (18515 flits)
Class 0:
Remaining flits: 952231 952232 952233 952234 952235 974196 974197 974198 974199 974200 [...] (18471 flits)
Measured flits: 952231 952232 952233 952234 952235 974196 974197 974198 974199 974200 [...] (18471 flits)
Class 0:
Remaining flits: 1007748 1007749 1007750 1007751 1007752 1007753 1007754 1007755 1007756 1007757 [...] (18536 flits)
Measured flits: 1007748 1007749 1007750 1007751 1007752 1007753 1007754 1007755 1007756 1007757 [...] (18536 flits)
Class 0:
Remaining flits: 1061158 1061159 1061160 1061161 1061162 1061163 1061164 1061165 1061166 1061167 [...] (18467 flits)
Measured flits: 1061158 1061159 1061160 1061161 1061162 1061163 1061164 1061165 1061166 1061167 [...] (18467 flits)
Class 0:
Remaining flits: 1112292 1112293 1112294 1112295 1112296 1112297 1112298 1112299 1112300 1112301 [...] (18925 flits)
Measured flits: 1112292 1112293 1112294 1112295 1112296 1112297 1112298 1112299 1112300 1112301 [...] (18925 flits)
Class 0:
Remaining flits: 1159506 1159507 1159508 1159509 1159510 1159511 1159512 1159513 1159514 1159515 [...] (18720 flits)
Measured flits: 1159506 1159507 1159508 1159509 1159510 1159511 1159512 1159513 1159514 1159515 [...] (18720 flits)
Class 0:
Remaining flits: 1210158 1210159 1210160 1210161 1210162 1210163 1210164 1210165 1210166 1210167 [...] (18084 flits)
Measured flits: 1210158 1210159 1210160 1210161 1210162 1210163 1210164 1210165 1210166 1210167 [...] (18020 flits)
Class 0:
Remaining flits: 1250694 1250695 1250696 1250697 1250698 1250699 1250700 1250701 1250702 1250703 [...] (18580 flits)
Measured flits: 1250694 1250695 1250696 1250697 1250698 1250699 1250700 1250701 1250702 1250703 [...] (18032 flits)
Class 0:
Remaining flits: 1295108 1295109 1295110 1295111 1295112 1295113 1295114 1295115 1295116 1295117 [...] (18740 flits)
Measured flits: 1295108 1295109 1295110 1295111 1295112 1295113 1295114 1295115 1295116 1295117 [...] (17471 flits)
Class 0:
Remaining flits: 1341666 1341667 1341668 1341669 1341670 1341671 1341672 1341673 1341674 1341675 [...] (18976 flits)
Measured flits: 1341666 1341667 1341668 1341669 1341670 1341671 1341672 1341673 1341674 1341675 [...] (16458 flits)
Class 0:
Remaining flits: 1387103 1387104 1387105 1387106 1387107 1387108 1387109 1387110 1387111 1387112 [...] (18682 flits)
Measured flits: 1387103 1387104 1387105 1387106 1387107 1387108 1387109 1387110 1387111 1387112 [...] (13922 flits)
Class 0:
Remaining flits: 1452870 1452871 1452872 1452873 1452874 1452875 1452876 1452877 1452878 1452879 [...] (18567 flits)
Measured flits: 1452870 1452871 1452872 1452873 1452874 1452875 1452876 1452877 1452878 1452879 [...] (11652 flits)
Class 0:
Remaining flits: 1474254 1474255 1474256 1474257 1474258 1474259 1474260 1474261 1474262 1474263 [...] (18697 flits)
Measured flits: 1474254 1474255 1474256 1474257 1474258 1474259 1474260 1474261 1474262 1474263 [...] (8669 flits)
Class 0:
Remaining flits: 1553720 1553721 1553722 1553723 1556784 1556785 1556786 1556787 1556788 1556789 [...] (18629 flits)
Measured flits: 1556784 1556785 1556786 1556787 1556788 1556789 1556790 1556791 1556792 1556793 [...] (5636 flits)
Class 0:
Remaining flits: 1602414 1602415 1602416 1602417 1602418 1602419 1602420 1602421 1602422 1602423 [...] (18572 flits)
Measured flits: 1613718 1613719 1613720 1613721 1613722 1613723 1613724 1613725 1613726 1613727 [...] (2720 flits)
Class 0:
Remaining flits: 1640916 1640917 1640918 1640919 1640920 1640921 1640922 1640923 1640924 1640925 [...] (18356 flits)
Measured flits: 1657674 1657675 1657676 1657677 1657678 1657679 1657680 1657681 1657682 1657683 [...] (1245 flits)
Class 0:
Remaining flits: 1663737 1663738 1663739 1664154 1664155 1664156 1664157 1664158 1664159 1664160 [...] (18222 flits)
Measured flits: 1702584 1702585 1702586 1702587 1702588 1702589 1702590 1702591 1702592 1702593 [...] (860 flits)
Class 0:
Remaining flits: 1746632 1746633 1746634 1746635 1746636 1746637 1746638 1746639 1746640 1746641 [...] (18695 flits)
Measured flits: 1775880 1775881 1775882 1775883 1775884 1775885 1775886 1775887 1775888 1775889 [...] (506 flits)
Class 0:
Remaining flits: 1797042 1797043 1797044 1797045 1797046 1797047 1798019 1800504 1800505 1800506 [...] (17971 flits)
Measured flits: 1811700 1811701 1811702 1811703 1811704 1811705 1811706 1811707 1811708 1811709 [...] (234 flits)
Class 0:
Remaining flits: 1826277 1826278 1826279 1829394 1829395 1829396 1829397 1829398 1829399 1829400 [...] (18381 flits)
Measured flits: 1854054 1854055 1854056 1854057 1854058 1854059 1854060 1854061 1854062 1854063 [...] (172 flits)
Class 0:
Remaining flits: 1872648 1872649 1872650 1872651 1872652 1872653 1872654 1872655 1872656 1872657 [...] (17975 flits)
Measured flits: 1902582 1902583 1902584 1902585 1902586 1902587 1902588 1902589 1902590 1902591 [...] (43 flits)
Draining remaining packets ...
Time taken is 41095 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12233.7 (1 samples)
	minimum = 1484 (1 samples)
	maximum = 30497 (1 samples)
Network latency average = 364.603 (1 samples)
	minimum = 22 (1 samples)
	maximum = 1828 (1 samples)
Flit latency average = 329.518 (1 samples)
	minimum = 5 (1 samples)
	maximum = 1867 (1 samples)
Fragmentation average = 22.8791 (1 samples)
	minimum = 0 (1 samples)
	maximum = 153 (1 samples)
Injected packet rate average = 0.0139762 (1 samples)
	minimum = 0.00971429 (1 samples)
	maximum = 0.0205714 (1 samples)
Accepted packet rate average = 0.0139993 (1 samples)
	minimum = 0.0111429 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.251706 (1 samples)
	minimum = 0.172429 (1 samples)
	maximum = 0.372286 (1 samples)
Accepted flit rate average = 0.252048 (1 samples)
	minimum = 0.200571 (1 samples)
	maximum = 0.324 (1 samples)
Injected packet size average = 18.0096 (1 samples)
Accepted packet size average = 18.0044 (1 samples)
Hops average = 5.0675 (1 samples)
Total run time 36.4213
