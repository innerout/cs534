BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 326.674
	minimum = 22
	maximum = 845
Network latency average = 306.289
	minimum = 22
	maximum = 823
Slowest packet = 270
Flit latency average = 283.466
	minimum = 5
	maximum = 806
Slowest flit = 23399
Fragmentation average = 25.9045
	minimum = 0
	maximum = 97
Injected packet rate average = 0.0427552
	minimum = 0.028 (at node 19)
	maximum = 0.056 (at node 46)
Accepted packet rate average = 0.0150521
	minimum = 0.006 (at node 64)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.762771
	minimum = 0.504 (at node 19)
	maximum = 0.998 (at node 46)
Accepted flit rate average= 0.277813
	minimum = 0.108 (at node 64)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.8404
Accepted packet length average = 18.4567
Total in-flight flits = 94656 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 646.448
	minimum = 22
	maximum = 1729
Network latency average = 616.015
	minimum = 22
	maximum = 1576
Slowest packet = 270
Flit latency average = 591.945
	minimum = 5
	maximum = 1559
Slowest flit = 57455
Fragmentation average = 27.0584
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0362526
	minimum = 0.02 (at node 160)
	maximum = 0.0475 (at node 35)
Accepted packet rate average = 0.0151276
	minimum = 0.009 (at node 10)
	maximum = 0.021 (at node 177)
Injected flit rate average = 0.650872
	minimum = 0.36 (at node 160)
	maximum = 0.855 (at node 35)
Accepted flit rate average= 0.275331
	minimum = 0.162 (at node 83)
	maximum = 0.378 (at node 177)
Injected packet length average = 17.9538
Accepted packet length average = 18.2006
Total in-flight flits = 146885 (0 measured)
latency change    = 0.494663
throughput change = 0.00901378
Class 0:
Packet latency average = 1644.18
	minimum = 31
	maximum = 2629
Network latency average = 1550.32
	minimum = 22
	maximum = 2496
Slowest packet = 3225
Flit latency average = 1533.21
	minimum = 5
	maximum = 2479
Slowest flit = 74069
Fragmentation average = 22.1317
	minimum = 0
	maximum = 181
Injected packet rate average = 0.0157656
	minimum = 0.004 (at node 68)
	maximum = 0.04 (at node 186)
Accepted packet rate average = 0.0149063
	minimum = 0.006 (at node 105)
	maximum = 0.027 (at node 166)
Injected flit rate average = 0.283958
	minimum = 0.072 (at node 68)
	maximum = 0.72 (at node 186)
Accepted flit rate average= 0.267328
	minimum = 0.126 (at node 117)
	maximum = 0.486 (at node 166)
Injected packet length average = 18.0112
Accepted packet length average = 17.934
Total in-flight flits = 150296 (0 measured)
latency change    = 0.606826
throughput change = 0.0299355
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1536.34
	minimum = 712
	maximum = 2876
Network latency average = 74.5761
	minimum = 22
	maximum = 871
Slowest packet = 17114
Flit latency average = 2104.19
	minimum = 5
	maximum = 3171
Slowest flit = 118998
Fragmentation average = 4.75
	minimum = 0
	maximum = 17
Injected packet rate average = 0.0145313
	minimum = 0.001 (at node 174)
	maximum = 0.041 (at node 5)
Accepted packet rate average = 0.0147969
	minimum = 0.004 (at node 134)
	maximum = 0.027 (at node 83)
Injected flit rate average = 0.261266
	minimum = 0.018 (at node 174)
	maximum = 0.723 (at node 5)
Accepted flit rate average= 0.265943
	minimum = 0.072 (at node 134)
	maximum = 0.475 (at node 83)
Injected packet length average = 17.9796
Accepted packet length average = 17.9729
Total in-flight flits = 149383 (48478 measured)
latency change    = 0.0701942
throughput change = 0.00520946
Class 0:
Packet latency average = 2393.34
	minimum = 712
	maximum = 4156
Network latency average = 666.847
	minimum = 22
	maximum = 1923
Slowest packet = 17114
Flit latency average = 2301.65
	minimum = 5
	maximum = 3965
Slowest flit = 151461
Fragmentation average = 8.88191
	minimum = 0
	maximum = 68
Injected packet rate average = 0.0150495
	minimum = 0.0035 (at node 68)
	maximum = 0.028 (at node 185)
Accepted packet rate average = 0.0149063
	minimum = 0.006 (at node 36)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.271112
	minimum = 0.063 (at node 68)
	maximum = 0.4965 (at node 185)
Accepted flit rate average= 0.268143
	minimum = 0.114 (at node 36)
	maximum = 0.387 (at node 129)
Injected packet length average = 18.0147
Accepted packet length average = 17.9886
Total in-flight flits = 151387 (96755 measured)
latency change    = 0.358078
throughput change = 0.00820651
Class 0:
Packet latency average = 3224.39
	minimum = 712
	maximum = 4870
Network latency average = 1398.8
	minimum = 22
	maximum = 2977
Slowest packet = 17114
Flit latency average = 2468.06
	minimum = 5
	maximum = 4890
Slowest flit = 161670
Fragmentation average = 13.8985
	minimum = 0
	maximum = 108
Injected packet rate average = 0.0150278
	minimum = 0.00566667 (at node 68)
	maximum = 0.0253333 (at node 106)
Accepted packet rate average = 0.0148333
	minimum = 0.0103333 (at node 36)
	maximum = 0.02 (at node 83)
Injected flit rate average = 0.270554
	minimum = 0.102 (at node 68)
	maximum = 0.456 (at node 131)
Accepted flit rate average= 0.267016
	minimum = 0.186 (at node 36)
	maximum = 0.356333 (at node 83)
Injected packet length average = 18.0036
Accepted packet length average = 18.0011
Total in-flight flits = 152213 (131638 measured)
latency change    = 0.257738
throughput change = 0.00422299
Class 0:
Packet latency average = 3854.54
	minimum = 712
	maximum = 5640
Network latency average = 1947.22
	minimum = 22
	maximum = 3975
Slowest packet = 17114
Flit latency average = 2560.46
	minimum = 5
	maximum = 5094
Slowest flit = 238025
Fragmentation average = 16.6493
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0149023
	minimum = 0.0065 (at node 68)
	maximum = 0.025 (at node 97)
Accepted packet rate average = 0.0148945
	minimum = 0.01 (at node 64)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.268411
	minimum = 0.117 (at node 68)
	maximum = 0.44675 (at node 97)
Accepted flit rate average= 0.267958
	minimum = 0.18325 (at node 64)
	maximum = 0.3595 (at node 128)
Injected packet length average = 18.0114
Accepted packet length average = 17.9904
Total in-flight flits = 150676 (147384 measured)
latency change    = 0.163484
throughput change = 0.00351812
Class 0:
Packet latency average = 4397.92
	minimum = 712
	maximum = 6409
Network latency average = 2316.68
	minimum = 22
	maximum = 4928
Slowest packet = 17114
Flit latency average = 2617.57
	minimum = 5
	maximum = 5852
Slowest flit = 235368
Fragmentation average = 17.6196
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0148656
	minimum = 0.007 (at node 68)
	maximum = 0.0246 (at node 70)
Accepted packet rate average = 0.0148698
	minimum = 0.0114 (at node 64)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.267627
	minimum = 0.1254 (at node 68)
	maximum = 0.4416 (at node 70)
Accepted flit rate average= 0.267595
	minimum = 0.2072 (at node 64)
	maximum = 0.3604 (at node 128)
Injected packet length average = 18.0031
Accepted packet length average = 17.9959
Total in-flight flits = 150301 (149983 measured)
latency change    = 0.123554
throughput change = 0.00135855
Class 0:
Packet latency average = 4836.04
	minimum = 712
	maximum = 7263
Network latency average = 2490.56
	minimum = 22
	maximum = 5867
Slowest packet = 17114
Flit latency average = 2655.28
	minimum = 5
	maximum = 6604
Slowest flit = 266542
Fragmentation average = 18.1944
	minimum = 0
	maximum = 187
Injected packet rate average = 0.0148446
	minimum = 0.00666667 (at node 68)
	maximum = 0.0226667 (at node 70)
Accepted packet rate average = 0.0148672
	minimum = 0.0115 (at node 64)
	maximum = 0.0195 (at node 157)
Injected flit rate average = 0.267295
	minimum = 0.119167 (at node 68)
	maximum = 0.406667 (at node 70)
Accepted flit rate average= 0.267693
	minimum = 0.207 (at node 64)
	maximum = 0.351 (at node 157)
Injected packet length average = 18.0062
Accepted packet length average = 18.0056
Total in-flight flits = 149804 (149804 measured)
latency change    = 0.0905949
throughput change = 0.00036578
Class 0:
Packet latency average = 5226.28
	minimum = 712
	maximum = 7954
Network latency average = 2591.7
	minimum = 22
	maximum = 6382
Slowest packet = 17114
Flit latency average = 2685.41
	minimum = 5
	maximum = 6604
Slowest flit = 266542
Fragmentation average = 18.7054
	minimum = 0
	maximum = 213
Injected packet rate average = 0.0149234
	minimum = 0.00728571 (at node 129)
	maximum = 0.0228571 (at node 70)
Accepted packet rate average = 0.0149122
	minimum = 0.0115714 (at node 18)
	maximum = 0.0184286 (at node 157)
Injected flit rate average = 0.26854
	minimum = 0.131143 (at node 129)
	maximum = 0.411429 (at node 70)
Accepted flit rate average= 0.268388
	minimum = 0.209857 (at node 18)
	maximum = 0.331714 (at node 157)
Injected packet length average = 17.9946
Accepted packet length average = 17.9979
Total in-flight flits = 150303 (150303 measured)
latency change    = 0.0746689
throughput change = 0.00258932
Draining all recorded packets ...
Class 0:
Remaining flits: 399024 399025 399026 399027 399028 399029 399030 399031 399032 399033 [...] (152037 flits)
Measured flits: 399024 399025 399026 399027 399028 399029 399030 399031 399032 399033 [...] (152037 flits)
Class 0:
Remaining flits: 434483 444274 444275 456912 456913 456914 456915 456916 456917 456918 [...] (149836 flits)
Measured flits: 434483 444274 444275 456912 456913 456914 456915 456916 456917 456918 [...] (149836 flits)
Class 0:
Remaining flits: 511254 511255 511256 511257 511258 511259 511260 511261 511262 511263 [...] (147825 flits)
Measured flits: 511254 511255 511256 511257 511258 511259 511260 511261 511262 511263 [...] (147825 flits)
Class 0:
Remaining flits: 561834 561835 561836 561837 561838 561839 561840 561841 561842 561843 [...] (150225 flits)
Measured flits: 561834 561835 561836 561837 561838 561839 561840 561841 561842 561843 [...] (150225 flits)
Class 0:
Remaining flits: 622314 622315 622316 622317 622318 622319 622320 622321 622322 622323 [...] (147018 flits)
Measured flits: 622314 622315 622316 622317 622318 622319 622320 622321 622322 622323 [...] (147018 flits)
Class 0:
Remaining flits: 635508 635509 635510 635511 635512 635513 635514 635515 635516 635517 [...] (150437 flits)
Measured flits: 635508 635509 635510 635511 635512 635513 635514 635515 635516 635517 [...] (150437 flits)
Class 0:
Remaining flits: 707346 707347 707348 707349 707350 707351 707352 707353 707354 707355 [...] (150509 flits)
Measured flits: 707346 707347 707348 707349 707350 707351 707352 707353 707354 707355 [...] (150509 flits)
Class 0:
Remaining flits: 748674 748675 748676 748677 748678 748679 748680 748681 748682 748683 [...] (149116 flits)
Measured flits: 748674 748675 748676 748677 748678 748679 748680 748681 748682 748683 [...] (149116 flits)
Class 0:
Remaining flits: 806418 806419 806420 806421 806422 806423 806424 806425 806426 806427 [...] (149051 flits)
Measured flits: 806418 806419 806420 806421 806422 806423 806424 806425 806426 806427 [...] (149051 flits)
Class 0:
Remaining flits: 815454 815455 815456 815457 815458 815459 815460 815461 815462 815463 [...] (144191 flits)
Measured flits: 815454 815455 815456 815457 815458 815459 815460 815461 815462 815463 [...] (144191 flits)
Class 0:
Remaining flits: 870804 870805 870806 870807 870808 870809 870810 870811 870812 870813 [...] (144298 flits)
Measured flits: 870804 870805 870806 870807 870808 870809 870810 870811 870812 870813 [...] (144244 flits)
Class 0:
Remaining flits: 920088 920089 920090 920091 920092 920093 920094 920095 920096 920097 [...] (145053 flits)
Measured flits: 920088 920089 920090 920091 920092 920093 920094 920095 920096 920097 [...] (144459 flits)
Class 0:
Remaining flits: 963396 963397 963398 963399 963400 963401 963402 963403 963404 963405 [...] (141873 flits)
Measured flits: 963396 963397 963398 963399 963400 963401 963402 963403 963404 963405 [...] (139533 flits)
Class 0:
Remaining flits: 977274 977275 977276 977277 977278 977279 977280 977281 977282 977283 [...] (142469 flits)
Measured flits: 977274 977275 977276 977277 977278 977279 977280 977281 977282 977283 [...] (137595 flits)
Class 0:
Remaining flits: 1036836 1036837 1036838 1036839 1036840 1036841 1036842 1036843 1036844 1036845 [...] (142994 flits)
Measured flits: 1036836 1036837 1036838 1036839 1036840 1036841 1036842 1036843 1036844 1036845 [...] (134256 flits)
Class 0:
Remaining flits: 1073520 1073521 1073522 1073523 1073524 1073525 1073526 1073527 1073528 1073529 [...] (144801 flits)
Measured flits: 1073520 1073521 1073522 1073523 1073524 1073525 1073526 1073527 1073528 1073529 [...] (129409 flits)
Class 0:
Remaining flits: 1146757 1146758 1146759 1146760 1146761 1149570 1149571 1149572 1149573 1149574 [...] (143503 flits)
Measured flits: 1146757 1146758 1146759 1146760 1146761 1149570 1149571 1149572 1149573 1149574 [...] (116473 flits)
Class 0:
Remaining flits: 1167120 1167121 1167122 1167123 1167124 1167125 1167126 1167127 1167128 1167129 [...] (144376 flits)
Measured flits: 1167120 1167121 1167122 1167123 1167124 1167125 1167126 1167127 1167128 1167129 [...] (104615 flits)
Class 0:
Remaining flits: 1309865 1309866 1309867 1309868 1309869 1309870 1309871 1309872 1309873 1309874 [...] (143760 flits)
Measured flits: 1309865 1309866 1309867 1309868 1309869 1309870 1309871 1309872 1309873 1309874 [...] (89362 flits)
Class 0:
Remaining flits: 1343592 1343593 1343594 1343595 1343596 1343597 1343598 1343599 1343600 1343601 [...] (141324 flits)
Measured flits: 1349478 1349479 1349480 1349481 1349482 1349483 1349484 1349485 1349486 1349487 [...] (70490 flits)
Class 0:
Remaining flits: 1360944 1360945 1360946 1360947 1360948 1360949 1360950 1360951 1360952 1360953 [...] (140446 flits)
Measured flits: 1360944 1360945 1360946 1360947 1360948 1360949 1360950 1360951 1360952 1360953 [...] (52020 flits)
Class 0:
Remaining flits: 1403226 1403227 1403228 1403229 1403230 1403231 1403232 1403233 1403234 1403235 [...] (142395 flits)
Measured flits: 1403226 1403227 1403228 1403229 1403230 1403231 1403232 1403233 1403234 1403235 [...] (38258 flits)
Class 0:
Remaining flits: 1422206 1422207 1422208 1422209 1422210 1422211 1422212 1422213 1422214 1422215 [...] (144721 flits)
Measured flits: 1422206 1422207 1422208 1422209 1422210 1422211 1422212 1422213 1422214 1422215 [...] (24990 flits)
Class 0:
Remaining flits: 1505754 1505755 1505756 1505757 1505758 1505759 1505760 1505761 1505762 1505763 [...] (143296 flits)
Measured flits: 1505754 1505755 1505756 1505757 1505758 1505759 1505760 1505761 1505762 1505763 [...] (15889 flits)
Class 0:
Remaining flits: 1558008 1558009 1558010 1558011 1558012 1558013 1558014 1558015 1558016 1558017 [...] (142713 flits)
Measured flits: 1620342 1620343 1620344 1620345 1620346 1620347 1620348 1620349 1620350 1620351 [...] (9617 flits)
Class 0:
Remaining flits: 1594347 1594348 1594349 1595538 1595539 1595540 1595541 1595542 1595543 1595544 [...] (144956 flits)
Measured flits: 1641996 1641997 1641998 1641999 1642000 1642001 1642002 1642003 1642004 1642005 [...] (6430 flits)
Class 0:
Remaining flits: 1662336 1662337 1662338 1662339 1662340 1662341 1662342 1662343 1662344 1662345 [...] (145093 flits)
Measured flits: 1726794 1726795 1726796 1726797 1726798 1726799 1726800 1726801 1726802 1726803 [...] (3393 flits)
Class 0:
Remaining flits: 1758474 1758475 1758476 1758477 1758478 1758479 1758480 1758481 1758482 1758483 [...] (144438 flits)
Measured flits: 1758474 1758475 1758476 1758477 1758478 1758479 1758480 1758481 1758482 1758483 [...] (1421 flits)
Class 0:
Remaining flits: 1758474 1758475 1758476 1758477 1758478 1758479 1758480 1758481 1758482 1758483 [...] (143406 flits)
Measured flits: 1758474 1758475 1758476 1758477 1758478 1758479 1758480 1758481 1758482 1758483 [...] (439 flits)
Class 0:
Remaining flits: 1848060 1848061 1848062 1848063 1848064 1848065 1848066 1848067 1848068 1848069 [...] (138883 flits)
Measured flits: 1968939 1968940 1968941 1968942 1968943 1968944 1968945 1968946 1968947 1971000 [...] (99 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1937250 1937251 1937252 1937253 1937254 1937255 1937256 1937257 1937258 1937259 [...] (88260 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2014830 2014831 2014832 2014833 2014834 2014835 2014836 2014837 2014838 2014839 [...] (40465 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2020338 2020339 2020340 2020341 2020342 2020343 2020344 2020345 2020346 2020347 [...] (5535 flits)
Measured flits: (0 flits)
Time taken is 44554 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 12888.4 (1 samples)
	minimum = 712 (1 samples)
	maximum = 30705 (1 samples)
Network latency average = 2827.89 (1 samples)
	minimum = 22 (1 samples)
	maximum = 8780 (1 samples)
Flit latency average = 2787.36 (1 samples)
	minimum = 5 (1 samples)
	maximum = 8763 (1 samples)
Fragmentation average = 20.5978 (1 samples)
	minimum = 0 (1 samples)
	maximum = 234 (1 samples)
Injected packet rate average = 0.0149234 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0228571 (1 samples)
Accepted packet rate average = 0.0149122 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0184286 (1 samples)
Injected flit rate average = 0.26854 (1 samples)
	minimum = 0.131143 (1 samples)
	maximum = 0.411429 (1 samples)
Accepted flit rate average = 0.268388 (1 samples)
	minimum = 0.209857 (1 samples)
	maximum = 0.331714 (1 samples)
Injected packet size average = 17.9946 (1 samples)
Accepted packet size average = 17.9979 (1 samples)
Hops average = 5.04952 (1 samples)
Total run time 44.4359
