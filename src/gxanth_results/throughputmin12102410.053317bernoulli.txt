BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.955
	minimum = 23
	maximum = 969
Network latency average = 302.006
	minimum = 23
	maximum = 925
Slowest packet = 362
Flit latency average = 271.497
	minimum = 6
	maximum = 908
Slowest flit = 8297
Fragmentation average = 49.0617
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0488958
	minimum = 0.036 (at node 30)
	maximum = 0.056 (at node 79)
Accepted packet rate average = 0.0119792
	minimum = 0.005 (at node 185)
	maximum = 0.021 (at node 91)
Injected flit rate average = 0.872245
	minimum = 0.634 (at node 30)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.224318
	minimum = 0.103 (at node 185)
	maximum = 0.378 (at node 91)
Injected packet length average = 17.8388
Accepted packet length average = 18.7257
Total in-flight flits = 125915 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 614.142
	minimum = 23
	maximum = 1866
Network latency average = 576.609
	minimum = 23
	maximum = 1837
Slowest packet = 649
Flit latency average = 547.046
	minimum = 6
	maximum = 1926
Slowest flit = 7429
Fragmentation average = 54.0215
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0494922
	minimum = 0.0395 (at node 70)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0122161
	minimum = 0.0075 (at node 30)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.886747
	minimum = 0.709 (at node 70)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.223859
	minimum = 0.135 (at node 30)
	maximum = 0.344 (at node 22)
Injected packet length average = 17.9169
Accepted packet length average = 18.3249
Total in-flight flits = 256128 (0 measured)
latency change    = 0.465995
throughput change = 0.00204742
Class 0:
Packet latency average = 1740.31
	minimum = 27
	maximum = 2773
Network latency average = 1669.01
	minimum = 25
	maximum = 2746
Slowest packet = 1547
Flit latency average = 1645.92
	minimum = 6
	maximum = 2760
Slowest flit = 36470
Fragmentation average = 65.5216
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0428073
	minimum = 0.012 (at node 96)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0103646
	minimum = 0.003 (at node 15)
	maximum = 0.018 (at node 14)
Injected flit rate average = 0.770531
	minimum = 0.219 (at node 88)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.185984
	minimum = 0.054 (at node 15)
	maximum = 0.337 (at node 47)
Injected packet length average = 18
Accepted packet length average = 17.9442
Total in-flight flits = 368415 (0 measured)
latency change    = 0.647107
throughput change = 0.203646
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 478.799
	minimum = 32
	maximum = 1892
Network latency average = 40.3766
	minimum = 23
	maximum = 134
Slowest packet = 27275
Flit latency average = 2425.95
	minimum = 6
	maximum = 3732
Slowest flit = 30869
Fragmentation average = 8.12987
	minimum = 0
	maximum = 21
Injected packet rate average = 0.043375
	minimum = 0.011 (at node 116)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0102604
	minimum = 0.003 (at node 104)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.781427
	minimum = 0.198 (at node 116)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.185229
	minimum = 0.059 (at node 155)
	maximum = 0.437 (at node 16)
Injected packet length average = 18.0156
Accepted packet length average = 18.0528
Total in-flight flits = 482701 (147058 measured)
latency change    = 2.63474
throughput change = 0.00407716
Class 0:
Packet latency average = 639.44
	minimum = 28
	maximum = 2678
Network latency average = 59.5868
	minimum = 23
	maximum = 1966
Slowest packet = 27275
Flit latency average = 2771.18
	minimum = 6
	maximum = 4609
Slowest flit = 59039
Fragmentation average = 9.07784
	minimum = 0
	maximum = 31
Injected packet rate average = 0.0434583
	minimum = 0.013 (at node 12)
	maximum = 0.056 (at node 21)
Accepted packet rate average = 0.010138
	minimum = 0.005 (at node 100)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.782172
	minimum = 0.2335 (at node 12)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.183135
	minimum = 0.082 (at node 100)
	maximum = 0.3525 (at node 16)
Injected packet length average = 17.9982
Accepted packet length average = 18.0642
Total in-flight flits = 598457 (294289 measured)
latency change    = 0.251222
throughput change = 0.0114328
Class 0:
Packet latency average = 911.371
	minimum = 26
	maximum = 3309
Network latency average = 224.11
	minimum = 23
	maximum = 2924
Slowest packet = 27275
Flit latency average = 3128.7
	minimum = 6
	maximum = 5579
Slowest flit = 60839
Fragmentation average = 9.0789
	minimum = 0
	maximum = 39
Injected packet rate average = 0.0428941
	minimum = 0.0133333 (at node 156)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0100382
	minimum = 0.006 (at node 18)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.772542
	minimum = 0.241333 (at node 156)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.181043
	minimum = 0.104 (at node 18)
	maximum = 0.288 (at node 16)
Injected packet length average = 18.0104
Accepted packet length average = 18.0355
Total in-flight flits = 708860 (434850 measured)
latency change    = 0.298375
throughput change = 0.0115553
Draining remaining packets ...
Class 0:
Remaining flits: 76950 76951 76952 76953 76954 76955 76956 76957 76958 76959 [...] (677416 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (432868 flits)
Class 0:
Remaining flits: 112122 112123 112124 112125 112126 112127 112128 112129 112130 112131 [...] (644373 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (430191 flits)
Class 0:
Remaining flits: 114192 114193 114194 114195 114196 114197 114198 114199 114200 114201 [...] (612663 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (426883 flits)
Class 0:
Remaining flits: 132426 132427 132428 132429 132430 132431 132432 132433 132434 132435 [...] (580336 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (422688 flits)
Class 0:
Remaining flits: 145260 145261 145262 145263 145264 145265 145266 145267 145268 145269 [...] (547071 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (416709 flits)
Class 0:
Remaining flits: 182433 182434 182435 182436 182437 182438 182439 182440 182441 182442 [...] (514026 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (408819 flits)
Class 0:
Remaining flits: 215298 215299 215300 215301 215302 215303 215304 215305 215306 215307 [...] (481351 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (398749 flits)
Class 0:
Remaining flits: 230976 230977 230978 230979 230980 230981 230982 230983 230984 230985 [...] (447892 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (385744 flits)
Class 0:
Remaining flits: 282942 282943 282944 282945 282946 282947 282948 282949 282950 282951 [...] (416259 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (371837 flits)
Class 0:
Remaining flits: 286146 286147 286148 286149 286150 286151 286152 286153 286154 286155 [...] (385585 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (355884 flits)
Class 0:
Remaining flits: 321355 321356 321357 321358 321359 321360 321361 321362 321363 321364 [...] (355625 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (336840 flits)
Class 0:
Remaining flits: 324288 324289 324290 324291 324292 324293 324294 324295 324296 324297 [...] (325388 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (313764 flits)
Class 0:
Remaining flits: 324288 324289 324290 324291 324292 324293 324294 324295 324296 324297 [...] (295743 flits)
Measured flits: 490086 490087 490088 490089 490090 490091 490092 490093 490094 490095 [...] (288754 flits)
Class 0:
Remaining flits: 371340 371341 371342 371343 371344 371345 371346 371347 371348 371349 [...] (265997 flits)
Measured flits: 490122 490123 490124 490125 490126 490127 490128 490129 490130 490131 [...] (262242 flits)
Class 0:
Remaining flits: 378522 378523 378524 378525 378526 378527 378528 378529 378530 378531 [...] (236816 flits)
Measured flits: 490824 490825 490826 490827 490828 490829 490830 490831 490832 490833 [...] (234786 flits)
Class 0:
Remaining flits: 408881 408882 408883 408884 408885 408886 408887 415026 415027 415028 [...] (208123 flits)
Measured flits: 490824 490825 490826 490827 490828 490829 490830 490831 490832 490833 [...] (207249 flits)
Class 0:
Remaining flits: 427248 427249 427250 427251 427252 427253 427254 427255 427256 427257 [...] (178792 flits)
Measured flits: 490824 490825 490826 490827 490828 490829 490830 490831 490832 490833 [...] (178354 flits)
Class 0:
Remaining flits: 427248 427249 427250 427251 427252 427253 427254 427255 427256 427257 [...] (149720 flits)
Measured flits: 493830 493831 493832 493833 493834 493835 493836 493837 493838 493839 [...] (149577 flits)
Class 0:
Remaining flits: 482238 482239 482240 482241 482242 482243 482244 482245 482246 482247 [...] (120227 flits)
Measured flits: 498366 498367 498368 498369 498370 498371 498372 498373 498374 498375 [...] (120155 flits)
Class 0:
Remaining flits: 482238 482239 482240 482241 482242 482243 482244 482245 482246 482247 [...] (90831 flits)
Measured flits: 510137 514296 514297 514298 514299 514300 514301 514302 514303 514304 [...] (90813 flits)
Class 0:
Remaining flits: 523728 523729 523730 523731 523732 523733 523734 523735 523736 523737 [...] (61820 flits)
Measured flits: 523728 523729 523730 523731 523732 523733 523734 523735 523736 523737 [...] (61820 flits)
Class 0:
Remaining flits: 572508 572509 572510 572511 572512 572513 572514 572515 572516 572517 [...] (33621 flits)
Measured flits: 572508 572509 572510 572511 572512 572513 572514 572515 572516 572517 [...] (33621 flits)
Class 0:
Remaining flits: 631692 631693 631694 631695 631696 631697 631698 631699 631700 631701 [...] (10005 flits)
Measured flits: 631692 631693 631694 631695 631696 631697 631698 631699 631700 631701 [...] (10005 flits)
Class 0:
Remaining flits: 874668 874669 874670 874671 874672 874673 906966 906967 906968 906969 [...] (74 flits)
Measured flits: 874668 874669 874670 874671 874672 874673 906966 906967 906968 906969 [...] (74 flits)
Time taken is 30070 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16578.6 (1 samples)
	minimum = 26 (1 samples)
	maximum = 25403 (1 samples)
Network latency average = 16271.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 25272 (1 samples)
Flit latency average = 12378.5 (1 samples)
	minimum = 6 (1 samples)
	maximum = 25255 (1 samples)
Fragmentation average = 66.9538 (1 samples)
	minimum = 0 (1 samples)
	maximum = 169 (1 samples)
Injected packet rate average = 0.0428941 (1 samples)
	minimum = 0.0133333 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0100382 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.772542 (1 samples)
	minimum = 0.241333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.181043 (1 samples)
	minimum = 0.104 (1 samples)
	maximum = 0.288 (1 samples)
Injected packet size average = 18.0104 (1 samples)
Accepted packet size average = 18.0355 (1 samples)
Hops average = 5.22981 (1 samples)
Total run time 34.298
