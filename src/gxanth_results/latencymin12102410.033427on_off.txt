BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 358.237
	minimum = 27
	maximum = 984
Network latency average = 266.551
	minimum = 27
	maximum = 883
Slowest packet = 67
Flit latency average = 233.19
	minimum = 6
	maximum = 901
Slowest flit = 8324
Fragmentation average = 52.539
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0102813
	minimum = 0.004 (at node 28)
	maximum = 0.018 (at node 140)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.193745
	minimum = 0.073 (at node 178)
	maximum = 0.33 (at node 140)
Injected packet length average = 17.8118
Accepted packet length average = 18.8445
Total in-flight flits = 63241 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 624.99
	minimum = 23
	maximum = 1947
Network latency average = 493.343
	minimum = 23
	maximum = 1779
Slowest packet = 67
Flit latency average = 458.729
	minimum = 6
	maximum = 1857
Slowest flit = 11211
Fragmentation average = 57.3408
	minimum = 0
	maximum = 158
Injected packet rate average = 0.0305885
	minimum = 0.003 (at node 92)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0108646
	minimum = 0.0065 (at node 45)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.54838
	minimum = 0.054 (at node 92)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.199961
	minimum = 0.117 (at node 83)
	maximum = 0.286 (at node 89)
Injected packet length average = 17.9276
Accepted packet length average = 18.4048
Total in-flight flits = 134643 (0 measured)
latency change    = 0.426811
throughput change = 0.0310868
Class 0:
Packet latency average = 1413.24
	minimum = 25
	maximum = 2954
Network latency average = 1212.97
	minimum = 23
	maximum = 2716
Slowest packet = 1272
Flit latency average = 1189.97
	minimum = 6
	maximum = 2722
Slowest flit = 24071
Fragmentation average = 62.1275
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0302552
	minimum = 0.001 (at node 42)
	maximum = 0.056 (at node 24)
Accepted packet rate average = 0.0107031
	minimum = 0.003 (at node 190)
	maximum = 0.018 (at node 50)
Injected flit rate average = 0.543885
	minimum = 0.002 (at node 42)
	maximum = 1 (at node 11)
Accepted flit rate average= 0.191281
	minimum = 0.044 (at node 190)
	maximum = 0.307 (at node 34)
Injected packet length average = 17.9766
Accepted packet length average = 17.8715
Total in-flight flits = 202497 (0 measured)
latency change    = 0.557761
throughput change = 0.0453766
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 416.379
	minimum = 32
	maximum = 2591
Network latency average = 97.493
	minimum = 23
	maximum = 877
Slowest packet = 17568
Flit latency average = 1771.4
	minimum = 6
	maximum = 3517
Slowest flit = 41916
Fragmentation average = 14.5738
	minimum = 0
	maximum = 119
Injected packet rate average = 0.0306042
	minimum = 0 (at node 135)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.010401
	minimum = 0.002 (at node 132)
	maximum = 0.02 (at node 87)
Injected flit rate average = 0.550193
	minimum = 0 (at node 135)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.188453
	minimum = 0.034 (at node 132)
	maximum = 0.36 (at node 87)
Injected packet length average = 17.9777
Accepted packet length average = 18.1187
Total in-flight flits = 272064 (99218 measured)
latency change    = 2.39412
throughput change = 0.015007
Class 0:
Packet latency average = 549.875
	minimum = 27
	maximum = 3407
Network latency average = 235.061
	minimum = 23
	maximum = 1961
Slowest packet = 17568
Flit latency average = 2090.84
	minimum = 6
	maximum = 4329
Slowest flit = 62518
Fragmentation average = 17.787
	minimum = 0
	maximum = 124
Injected packet rate average = 0.0297057
	minimum = 0.0005 (at node 135)
	maximum = 0.056 (at node 11)
Accepted packet rate average = 0.0105208
	minimum = 0.0055 (at node 22)
	maximum = 0.0165 (at node 137)
Injected flit rate average = 0.534484
	minimum = 0.009 (at node 135)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.189596
	minimum = 0.0905 (at node 22)
	maximum = 0.294 (at node 137)
Injected packet length average = 17.9926
Accepted packet length average = 18.021
Total in-flight flits = 335000 (191327 measured)
latency change    = 0.242776
throughput change = 0.00602981
Class 0:
Packet latency average = 804.001
	minimum = 27
	maximum = 3883
Network latency average = 489.591
	minimum = 23
	maximum = 2940
Slowest packet = 17568
Flit latency average = 2401.55
	minimum = 6
	maximum = 5389
Slowest flit = 54533
Fragmentation average = 22.5666
	minimum = 0
	maximum = 136
Injected packet rate average = 0.0293003
	minimum = 0.004 (at node 135)
	maximum = 0.0556667 (at node 51)
Accepted packet rate average = 0.0104392
	minimum = 0.00566667 (at node 4)
	maximum = 0.0153333 (at node 147)
Injected flit rate average = 0.527472
	minimum = 0.0696667 (at node 135)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.188069
	minimum = 0.101667 (at node 4)
	maximum = 0.279667 (at node 147)
Injected packet length average = 18.0023
Accepted packet length average = 18.0156
Total in-flight flits = 397973 (281136 measured)
latency change    = 0.316076
throughput change = 0.00811886
Class 0:
Packet latency average = 1202.55
	minimum = 27
	maximum = 4706
Network latency average = 883.224
	minimum = 23
	maximum = 3966
Slowest packet = 17568
Flit latency average = 2725.98
	minimum = 6
	maximum = 6156
Slowest flit = 76355
Fragmentation average = 28.6866
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0289375
	minimum = 0.0085 (at node 84)
	maximum = 0.055 (at node 186)
Accepted packet rate average = 0.010306
	minimum = 0.0065 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.52066
	minimum = 0.153 (at node 84)
	maximum = 0.987 (at node 186)
Accepted flit rate average= 0.185547
	minimum = 0.11775 (at node 64)
	maximum = 0.255 (at node 147)
Injected packet length average = 17.9926
Accepted packet length average = 18.0038
Total in-flight flits = 460011 (367378 measured)
latency change    = 0.331421
throughput change = 0.0135953
Class 0:
Packet latency average = 1779.23
	minimum = 27
	maximum = 6527
Network latency average = 1419.05
	minimum = 23
	maximum = 4939
Slowest packet = 17568
Flit latency average = 3045.07
	minimum = 6
	maximum = 7052
Slowest flit = 82961
Fragmentation average = 35.4206
	minimum = 0
	maximum = 176
Injected packet rate average = 0.0289823
	minimum = 0.0098 (at node 8)
	maximum = 0.052 (at node 186)
Accepted packet rate average = 0.0102615
	minimum = 0.0072 (at node 4)
	maximum = 0.0134 (at node 147)
Injected flit rate average = 0.52155
	minimum = 0.1764 (at node 8)
	maximum = 0.9366 (at node 186)
Accepted flit rate average= 0.184689
	minimum = 0.1294 (at node 132)
	maximum = 0.2466 (at node 182)
Injected packet length average = 17.9955
Accepted packet length average = 17.9983
Total in-flight flits = 526028 (454706 measured)
latency change    = 0.324116
throughput change = 0.00464746
Class 0:
Packet latency average = 2339.64
	minimum = 27
	maximum = 7716
Network latency average = 1969.47
	minimum = 23
	maximum = 5917
Slowest packet = 17568
Flit latency average = 3370.61
	minimum = 6
	maximum = 7857
Slowest flit = 116789
Fragmentation average = 41.4153
	minimum = 0
	maximum = 176
Injected packet rate average = 0.0285425
	minimum = 0.00933333 (at node 84)
	maximum = 0.0485 (at node 50)
Accepted packet rate average = 0.0101892
	minimum = 0.0065 (at node 149)
	maximum = 0.0131667 (at node 142)
Injected flit rate average = 0.513765
	minimum = 0.168 (at node 84)
	maximum = 0.8705 (at node 50)
Accepted flit rate average= 0.183484
	minimum = 0.120167 (at node 149)
	maximum = 0.2345 (at node 142)
Injected packet length average = 18
Accepted packet length average = 18.0076
Total in-flight flits = 583036 (530246 measured)
latency change    = 0.239529
throughput change = 0.00656754
Class 0:
Packet latency average = 2974.95
	minimum = 23
	maximum = 8634
Network latency average = 2592.85
	minimum = 23
	maximum = 6975
Slowest packet = 17568
Flit latency average = 3695.85
	minimum = 6
	maximum = 8777
Slowest flit = 124789
Fragmentation average = 45.9931
	minimum = 0
	maximum = 176
Injected packet rate average = 0.0281473
	minimum = 0.0132857 (at node 135)
	maximum = 0.0465714 (at node 17)
Accepted packet rate average = 0.0101756
	minimum = 0.007 (at node 52)
	maximum = 0.0132857 (at node 90)
Injected flit rate average = 0.506639
	minimum = 0.238143 (at node 135)
	maximum = 0.839143 (at node 17)
Accepted flit rate average= 0.183117
	minimum = 0.127143 (at node 52)
	maximum = 0.238714 (at node 90)
Injected packet length average = 17.9996
Accepted packet length average = 17.9957
Total in-flight flits = 637328 (599605 measured)
latency change    = 0.213553
throughput change = 0.0020025
Draining all recorded packets ...
Class 0:
Remaining flits: 125622 125623 125624 125625 125626 125627 125628 125629 125630 125631 [...] (686436 flits)
Measured flits: 316044 316045 316046 316047 316048 316049 316050 316051 316052 316053 [...] (614733 flits)
Class 0:
Remaining flits: 141066 141067 141068 141069 141070 141071 141072 141073 141074 141075 [...] (730472 flits)
Measured flits: 316062 316063 316064 316065 316066 316067 316068 316069 316070 316071 [...] (608938 flits)
Class 0:
Remaining flits: 152514 152515 152516 152517 152518 152519 152520 152521 152522 152523 [...] (773915 flits)
Measured flits: 316062 316063 316064 316065 316066 316067 316068 316069 316070 316071 [...] (599062 flits)
Class 0:
Remaining flits: 166896 166897 166898 166899 166900 166901 166902 166903 166904 166905 [...] (811458 flits)
Measured flits: 316098 316099 316100 316101 316102 316103 316104 316105 316106 316107 [...] (585135 flits)
Class 0:
Remaining flits: 166896 166897 166898 166899 166900 166901 166902 166903 166904 166905 [...] (844913 flits)
Measured flits: 316098 316099 316100 316101 316102 316103 316104 316105 316106 316107 [...] (567294 flits)
Class 0:
Remaining flits: 181926 181927 181928 181929 181930 181931 181932 181933 181934 181935 [...] (873667 flits)
Measured flits: 316404 316405 316406 316407 316408 316409 316410 316411 316412 316413 [...] (547821 flits)
Class 0:
Remaining flits: 209484 209485 209486 209487 209488 209489 209490 209491 209492 209493 [...] (899536 flits)
Measured flits: 316404 316405 316406 316407 316408 316409 316410 316411 316412 316413 [...] (526075 flits)
Class 0:
Remaining flits: 209484 209485 209486 209487 209488 209489 209490 209491 209492 209493 [...] (923436 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (501640 flits)
Class 0:
Remaining flits: 246780 246781 246782 246783 246784 246785 246786 246787 246788 246789 [...] (939616 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (475606 flits)
Class 0:
Remaining flits: 261468 261469 261470 261471 261472 261473 261474 261475 261476 261477 [...] (952479 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (450994 flits)
Class 0:
Remaining flits: 282852 282853 282854 282855 282856 282857 282858 282859 282860 282861 [...] (964193 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (425052 flits)
Class 0:
Remaining flits: 285624 285625 285626 285627 285628 285629 285630 285631 285632 285633 [...] (974792 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (399367 flits)
Class 0:
Remaining flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (976758 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (374007 flits)
Class 0:
Remaining flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (975757 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (348308 flits)
Class 0:
Remaining flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (972408 flits)
Measured flits: 317358 317359 317360 317361 317362 317363 317364 317365 317366 317367 [...] (323145 flits)
Class 0:
Remaining flits: 379026 379027 379028 379029 379030 379031 379032 379033 379034 379035 [...] (964878 flits)
Measured flits: 379026 379027 379028 379029 379030 379031 379032 379033 379034 379035 [...] (297633 flits)
Class 0:
Remaining flits: 379026 379027 379028 379029 379030 379031 379032 379033 379034 379035 [...] (962574 flits)
Measured flits: 379026 379027 379028 379029 379030 379031 379032 379033 379034 379035 [...] (272551 flits)
Class 0:
Remaining flits: 404820 404821 404822 404823 404824 404825 404826 404827 404828 404829 [...] (965264 flits)
Measured flits: 404820 404821 404822 404823 404824 404825 404826 404827 404828 404829 [...] (248510 flits)
Class 0:
Remaining flits: 414846 414847 414848 414849 414850 414851 414852 414853 414854 414855 [...] (965147 flits)
Measured flits: 414846 414847 414848 414849 414850 414851 414852 414853 414854 414855 [...] (225152 flits)
Class 0:
Remaining flits: 438624 438625 438626 438627 438628 438629 438630 438631 438632 438633 [...] (962977 flits)
Measured flits: 438624 438625 438626 438627 438628 438629 438630 438631 438632 438633 [...] (203935 flits)
Class 0:
Remaining flits: 456174 456175 456176 456177 456178 456179 456180 456181 456182 456183 [...] (961631 flits)
Measured flits: 456174 456175 456176 456177 456178 456179 456180 456181 456182 456183 [...] (183960 flits)
Class 0:
Remaining flits: 500868 500869 500870 500871 500872 500873 500874 500875 500876 500877 [...] (960585 flits)
Measured flits: 500868 500869 500870 500871 500872 500873 500874 500875 500876 500877 [...] (163403 flits)
Class 0:
Remaining flits: 500868 500869 500870 500871 500872 500873 500874 500875 500876 500877 [...] (956155 flits)
Measured flits: 500868 500869 500870 500871 500872 500873 500874 500875 500876 500877 [...] (144423 flits)
Class 0:
Remaining flits: 504054 504055 504056 504057 504058 504059 504060 504061 504062 504063 [...] (953806 flits)
Measured flits: 504054 504055 504056 504057 504058 504059 504060 504061 504062 504063 [...] (126900 flits)
Class 0:
Remaining flits: 504054 504055 504056 504057 504058 504059 504060 504061 504062 504063 [...] (949817 flits)
Measured flits: 504054 504055 504056 504057 504058 504059 504060 504061 504062 504063 [...] (110562 flits)
Class 0:
Remaining flits: 532584 532585 532586 532587 532588 532589 532590 532591 532592 532593 [...] (947790 flits)
Measured flits: 532584 532585 532586 532587 532588 532589 532590 532591 532592 532593 [...] (96500 flits)
Class 0:
Remaining flits: 532584 532585 532586 532587 532588 532589 532590 532591 532592 532593 [...] (943462 flits)
Measured flits: 532584 532585 532586 532587 532588 532589 532590 532591 532592 532593 [...] (83579 flits)
Class 0:
Remaining flits: 549360 549361 549362 549363 549364 549365 549366 549367 549368 549369 [...] (941891 flits)
Measured flits: 549360 549361 549362 549363 549364 549365 549366 549367 549368 549369 [...] (71694 flits)
Class 0:
Remaining flits: 565632 565633 565634 565635 565636 565637 565638 565639 565640 565641 [...] (940489 flits)
Measured flits: 565632 565633 565634 565635 565636 565637 565638 565639 565640 565641 [...] (61783 flits)
Class 0:
Remaining flits: 629460 629461 629462 629463 629464 629465 629466 629467 629468 629469 [...] (937276 flits)
Measured flits: 629460 629461 629462 629463 629464 629465 629466 629467 629468 629469 [...] (52654 flits)
Class 0:
Remaining flits: 629460 629461 629462 629463 629464 629465 629466 629467 629468 629469 [...] (930446 flits)
Measured flits: 629460 629461 629462 629463 629464 629465 629466 629467 629468 629469 [...] (44864 flits)
Class 0:
Remaining flits: 647370 647371 647372 647373 647374 647375 647376 647377 647378 647379 [...] (926805 flits)
Measured flits: 647370 647371 647372 647373 647374 647375 647376 647377 647378 647379 [...] (37429 flits)
Class 0:
Remaining flits: 680328 680329 680330 680331 680332 680333 680334 680335 680336 680337 [...] (923328 flits)
Measured flits: 680328 680329 680330 680331 680332 680333 680334 680335 680336 680337 [...] (31471 flits)
Class 0:
Remaining flits: 680328 680329 680330 680331 680332 680333 680334 680335 680336 680337 [...] (919385 flits)
Measured flits: 680328 680329 680330 680331 680332 680333 680334 680335 680336 680337 [...] (26776 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (918427 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (22014 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (916245 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (17792 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (912915 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (14323 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (910710 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (11699 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (908564 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (9364 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (907155 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (7591 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (904986 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (6235 flits)
Class 0:
Remaining flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (902085 flits)
Measured flits: 704232 704233 704234 704235 704236 704237 704238 704239 704240 704241 [...] (4818 flits)
Class 0:
Remaining flits: 829368 829369 829370 829371 829372 829373 829374 829375 829376 829377 [...] (900303 flits)
Measured flits: 829368 829369 829370 829371 829372 829373 829374 829375 829376 829377 [...] (3662 flits)
Class 0:
Remaining flits: 829368 829369 829370 829371 829372 829373 829374 829375 829376 829377 [...] (892667 flits)
Measured flits: 829368 829369 829370 829371 829372 829373 829374 829375 829376 829377 [...] (2645 flits)
Class 0:
Remaining flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (889193 flits)
Measured flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (2178 flits)
Class 0:
Remaining flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (888576 flits)
Measured flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (1764 flits)
Class 0:
Remaining flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (888788 flits)
Measured flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (1338 flits)
Class 0:
Remaining flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (886269 flits)
Measured flits: 948816 948817 948818 948819 948820 948821 948822 948823 948824 948825 [...] (866 flits)
Class 0:
Remaining flits: 1066356 1066357 1066358 1066359 1066360 1066361 1066362 1066363 1066364 1066365 [...] (884279 flits)
Measured flits: 1120140 1120141 1120142 1120143 1120144 1120145 1120146 1120147 1120148 1120149 [...] (568 flits)
Class 0:
Remaining flits: 1066356 1066357 1066358 1066359 1066360 1066361 1066362 1066363 1066364 1066365 [...] (880535 flits)
Measured flits: 1146060 1146061 1146062 1146063 1146064 1146065 1146066 1146067 1146068 1146069 [...] (432 flits)
Class 0:
Remaining flits: 1127790 1127791 1127792 1127793 1127794 1127795 1127796 1127797 1127798 1127799 [...] (877844 flits)
Measured flits: 1146060 1146061 1146062 1146063 1146064 1146065 1146066 1146067 1146068 1146069 [...] (342 flits)
Class 0:
Remaining flits: 1147248 1147249 1147250 1147251 1147252 1147253 1147254 1147255 1147256 1147257 [...] (875514 flits)
Measured flits: 1175166 1175167 1175168 1175169 1175170 1175171 1175172 1175173 1175174 1175175 [...] (200 flits)
Class 0:
Remaining flits: 1175166 1175167 1175168 1175169 1175170 1175171 1175172 1175173 1175174 1175175 [...] (873226 flits)
Measured flits: 1175166 1175167 1175168 1175169 1175170 1175171 1175172 1175173 1175174 1175175 [...] (162 flits)
Class 0:
Remaining flits: 1277424 1277425 1277426 1277427 1277428 1277429 1277430 1277431 1277432 1277433 [...] (870257 flits)
Measured flits: 1333998 1333999 1334000 1334001 1334002 1334003 1334004 1334005 1334006 1334007 [...] (90 flits)
Class 0:
Remaining flits: 1277424 1277425 1277426 1277427 1277428 1277429 1277430 1277431 1277432 1277433 [...] (869882 flits)
Measured flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (54 flits)
Class 0:
Remaining flits: 1315836 1315837 1315838 1315839 1315840 1315841 1315842 1315843 1315844 1315845 [...] (867070 flits)
Measured flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (54 flits)
Class 0:
Remaining flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (863744 flits)
Measured flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (36 flits)
Class 0:
Remaining flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (859716 flits)
Measured flits: 1365012 1365013 1365014 1365015 1365016 1365017 1365018 1365019 1365020 1365021 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1401372 1401373 1401374 1401375 1401376 1401377 1401378 1401379 1401380 1401381 [...] (828427 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1418238 1418239 1418240 1418241 1418242 1418243 1418244 1418245 1418246 1418247 [...] (796613 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1438542 1438543 1438544 1438545 1438546 1438547 1438548 1438549 1438550 1438551 [...] (765184 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1438542 1438543 1438544 1438545 1438546 1438547 1438548 1438549 1438550 1438551 [...] (733525 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1488420 1488421 1488422 1488423 1488424 1488425 1488426 1488427 1488428 1488429 [...] (702553 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1633410 1633411 1633412 1633413 1633414 1633415 1633416 1633417 1633418 1633419 [...] (671081 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1633410 1633411 1633412 1633413 1633414 1633415 1633416 1633417 1633418 1633419 [...] (640819 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702080 1702081 1702082 1702083 1702084 1702085 1702086 1702087 1702088 1702089 [...] (609783 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1724508 1724509 1724510 1724511 1724512 1724513 1724514 1724515 1724516 1724517 [...] (578587 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1744578 1744579 1744580 1744581 1744582 1744583 1744584 1744585 1744586 1744587 [...] (547301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1749294 1749295 1749296 1749297 1749298 1749299 1749300 1749301 1749302 1749303 [...] (516763 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1774152 1774153 1774154 1774155 1774156 1774157 1774158 1774159 1774160 1774161 [...] (485963 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805454 1805455 1805456 1805457 1805458 1805459 1805460 1805461 1805462 1805463 [...] (454902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1805454 1805455 1805456 1805457 1805458 1805459 1805460 1805461 1805462 1805463 [...] (424128 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1893492 1893493 1893494 1893495 1893496 1893497 1893498 1893499 1893500 1893501 [...] (393313 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1911420 1911421 1911422 1911423 1911424 1911425 1911426 1911427 1911428 1911429 [...] (363220 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1973358 1973359 1973360 1973361 1973362 1973363 1973364 1973365 1973366 1973367 [...] (332513 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1984644 1984645 1984646 1984647 1984648 1984649 1984650 1984651 1984652 1984653 [...] (302398 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002050 2002051 2002052 2002053 2002054 2002055 2002056 2002057 2002058 2002059 [...] (273610 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002050 2002051 2002052 2002053 2002054 2002055 2002056 2002057 2002058 2002059 [...] (244719 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002050 2002051 2002052 2002053 2002054 2002055 2002056 2002057 2002058 2002059 [...] (215508 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002050 2002051 2002052 2002053 2002054 2002055 2002056 2002057 2002058 2002059 [...] (185730 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2002050 2002051 2002052 2002053 2002054 2002055 2002056 2002057 2002058 2002059 [...] (156795 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084886 2084887 2084888 2084889 2084890 2084891 2084892 2084893 2084894 2084895 [...] (128931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084886 2084887 2084888 2084889 2084890 2084891 2084892 2084893 2084894 2084895 [...] (101168 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2093742 2093743 2093744 2093745 2093746 2093747 2093748 2093749 2093750 2093751 [...] (73286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2229660 2229661 2229662 2229663 2229664 2229665 2229666 2229667 2229668 2229669 [...] (46873 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2306826 2306827 2306828 2306829 2306830 2306831 2306832 2306833 2306834 2306835 [...] (24712 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2502594 2502595 2502596 2502597 2502598 2502599 2502600 2502601 2502602 2502603 [...] (8023 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2683890 2683891 2683892 2683893 2683894 2683895 2683896 2683897 2683898 2683899 [...] (719 flits)
Measured flits: (0 flits)
Time taken is 99299 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16579.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 61343 (1 samples)
Network latency average = 14969.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 53872 (1 samples)
Flit latency average = 22765.1 (1 samples)
	minimum = 6 (1 samples)
	maximum = 61556 (1 samples)
Fragmentation average = 72.1966 (1 samples)
	minimum = 0 (1 samples)
	maximum = 184 (1 samples)
Injected packet rate average = 0.0281473 (1 samples)
	minimum = 0.0132857 (1 samples)
	maximum = 0.0465714 (1 samples)
Accepted packet rate average = 0.0101756 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0132857 (1 samples)
Injected flit rate average = 0.506639 (1 samples)
	minimum = 0.238143 (1 samples)
	maximum = 0.839143 (1 samples)
Accepted flit rate average = 0.183117 (1 samples)
	minimum = 0.127143 (1 samples)
	maximum = 0.238714 (1 samples)
Injected packet size average = 17.9996 (1 samples)
Accepted packet size average = 17.9957 (1 samples)
Hops average = 5.04873 (1 samples)
Total run time 78.5576
