BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 368.494
	minimum = 27
	maximum = 977
Network latency average = 281.567
	minimum = 26
	maximum = 907
Slowest packet = 57
Flit latency average = 205.185
	minimum = 6
	maximum = 916
Slowest flit = 6416
Fragmentation average = 151.193
	minimum = 0
	maximum = 814
Injected packet rate average = 0.0191406
	minimum = 0 (at node 92)
	maximum = 0.044 (at node 74)
Accepted packet rate average = 0.00951042
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.338307
	minimum = 0 (at node 92)
	maximum = 0.777 (at node 74)
Accepted flit rate average= 0.19487
	minimum = 0.065 (at node 41)
	maximum = 0.349 (at node 70)
Injected packet length average = 17.6748
Accepted packet length average = 20.4901
Total in-flight flits = 28789 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 647.928
	minimum = 27
	maximum = 1946
Network latency average = 464.161
	minimum = 26
	maximum = 1816
Slowest packet = 314
Flit latency average = 368.686
	minimum = 6
	maximum = 1875
Slowest flit = 9571
Fragmentation average = 183.323
	minimum = 0
	maximum = 1687
Injected packet rate average = 0.0159922
	minimum = 0.004 (at node 104)
	maximum = 0.0285 (at node 74)
Accepted packet rate average = 0.0103984
	minimum = 0.005 (at node 10)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.284354
	minimum = 0.071 (at node 104)
	maximum = 0.5125 (at node 74)
Accepted flit rate average= 0.198792
	minimum = 0.103 (at node 153)
	maximum = 0.2975 (at node 22)
Injected packet length average = 17.7808
Accepted packet length average = 19.1175
Total in-flight flits = 34472 (0 measured)
latency change    = 0.431274
throughput change = 0.0197286
Class 0:
Packet latency average = 1506.46
	minimum = 50
	maximum = 2913
Network latency average = 863.253
	minimum = 27
	maximum = 2714
Slowest packet = 4928
Flit latency average = 739.347
	minimum = 6
	maximum = 2765
Slowest flit = 17360
Fragmentation average = 217.933
	minimum = 0
	maximum = 2098
Injected packet rate average = 0.0107187
	minimum = 0 (at node 1)
	maximum = 0.029 (at node 5)
Accepted packet rate average = 0.010875
	minimum = 0.003 (at node 66)
	maximum = 0.02 (at node 34)
Injected flit rate average = 0.193094
	minimum = 0 (at node 76)
	maximum = 0.521 (at node 73)
Accepted flit rate average= 0.195161
	minimum = 0.077 (at node 32)
	maximum = 0.363 (at node 34)
Injected packet length average = 18.0146
Accepted packet length average = 17.9459
Total in-flight flits = 33973 (0 measured)
latency change    = 0.569901
throughput change = 0.0186011
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2117.11
	minimum = 65
	maximum = 3825
Network latency average = 362.88
	minimum = 25
	maximum = 986
Slowest packet = 8215
Flit latency average = 829.531
	minimum = 6
	maximum = 3478
Slowest flit = 36702
Fragmentation average = 134.011
	minimum = 0
	maximum = 593
Injected packet rate average = 0.0115573
	minimum = 0 (at node 16)
	maximum = 0.029 (at node 86)
Accepted packet rate average = 0.0109792
	minimum = 0.003 (at node 31)
	maximum = 0.021 (at node 159)
Injected flit rate average = 0.207438
	minimum = 0 (at node 55)
	maximum = 0.514 (at node 86)
Accepted flit rate average= 0.195526
	minimum = 0.062 (at node 31)
	maximum = 0.359 (at node 159)
Injected packet length average = 17.9486
Accepted packet length average = 17.8088
Total in-flight flits = 36572 (26840 measured)
latency change    = 0.288433
throughput change = 0.00186463
Class 0:
Packet latency average = 2561.76
	minimum = 65
	maximum = 4860
Network latency average = 615.393
	minimum = 23
	maximum = 1933
Slowest packet = 8215
Flit latency average = 850.468
	minimum = 6
	maximum = 4604
Slowest flit = 21420
Fragmentation average = 161.709
	minimum = 0
	maximum = 1577
Injected packet rate average = 0.0112292
	minimum = 0 (at node 127)
	maximum = 0.025 (at node 126)
Accepted packet rate average = 0.0108385
	minimum = 0.0055 (at node 146)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.202398
	minimum = 0 (at node 163)
	maximum = 0.4535 (at node 126)
Accepted flit rate average= 0.195427
	minimum = 0.1065 (at node 31)
	maximum = 0.317 (at node 16)
Injected packet length average = 18.0244
Accepted packet length average = 18.0308
Total in-flight flits = 36815 (33880 measured)
latency change    = 0.173573
throughput change = 0.00050637
Class 0:
Packet latency average = 2946.3
	minimum = 44
	maximum = 5689
Network latency average = 748.809
	minimum = 23
	maximum = 2902
Slowest packet = 8215
Flit latency average = 870.758
	minimum = 6
	maximum = 5159
Slowest flit = 21436
Fragmentation average = 176.24
	minimum = 0
	maximum = 1970
Injected packet rate average = 0.0109236
	minimum = 0 (at node 127)
	maximum = 0.0203333 (at node 54)
Accepted packet rate average = 0.0107309
	minimum = 0.005 (at node 4)
	maximum = 0.0166667 (at node 129)
Injected flit rate average = 0.1965
	minimum = 0.003 (at node 127)
	maximum = 0.360667 (at node 54)
Accepted flit rate average= 0.193865
	minimum = 0.101333 (at node 79)
	maximum = 0.320333 (at node 129)
Injected packet length average = 17.9886
Accepted packet length average = 18.066
Total in-flight flits = 35707 (34821 measured)
latency change    = 0.130517
throughput change = 0.00805975
Class 0:
Packet latency average = 3355.45
	minimum = 44
	maximum = 6424
Network latency average = 830.485
	minimum = 23
	maximum = 3858
Slowest packet = 8215
Flit latency average = 878.969
	minimum = 6
	maximum = 6100
Slowest flit = 35186
Fragmentation average = 186.094
	minimum = 0
	maximum = 2566
Injected packet rate average = 0.0109727
	minimum = 0.00225 (at node 148)
	maximum = 0.01775 (at node 83)
Accepted packet rate average = 0.0107904
	minimum = 0.00675 (at node 79)
	maximum = 0.015 (at node 129)
Injected flit rate average = 0.197551
	minimum = 0.0365 (at node 148)
	maximum = 0.3235 (at node 83)
Accepted flit rate average= 0.194543
	minimum = 0.1275 (at node 79)
	maximum = 0.28075 (at node 129)
Injected packet length average = 18.0039
Accepted packet length average = 18.0293
Total in-flight flits = 36394 (36077 measured)
latency change    = 0.121937
throughput change = 0.00348707
Class 0:
Packet latency average = 3728.89
	minimum = 44
	maximum = 7264
Network latency average = 883.649
	minimum = 23
	maximum = 4435
Slowest packet = 8215
Flit latency average = 888.174
	minimum = 6
	maximum = 6742
Slowest flit = 35189
Fragmentation average = 192.171
	minimum = 0
	maximum = 2566
Injected packet rate average = 0.0109281
	minimum = 0.0018 (at node 148)
	maximum = 0.018 (at node 143)
Accepted packet rate average = 0.0108
	minimum = 0.0074 (at node 79)
	maximum = 0.0152 (at node 129)
Injected flit rate average = 0.196655
	minimum = 0.0308 (at node 148)
	maximum = 0.3238 (at node 143)
Accepted flit rate average= 0.194751
	minimum = 0.1406 (at node 162)
	maximum = 0.2744 (at node 129)
Injected packet length average = 17.9953
Accepted packet length average = 18.0325
Total in-flight flits = 35976 (35860 measured)
latency change    = 0.100146
throughput change = 0.0010684
Class 0:
Packet latency average = 4108.91
	minimum = 33
	maximum = 8401
Network latency average = 918.227
	minimum = 23
	maximum = 5756
Slowest packet = 8215
Flit latency average = 890.609
	minimum = 6
	maximum = 6742
Slowest flit = 35189
Fragmentation average = 197.417
	minimum = 0
	maximum = 3158
Injected packet rate average = 0.0110191
	minimum = 0.00366667 (at node 148)
	maximum = 0.0168333 (at node 54)
Accepted packet rate average = 0.0108628
	minimum = 0.008 (at node 79)
	maximum = 0.0141667 (at node 128)
Injected flit rate average = 0.198378
	minimum = 0.0655 (at node 148)
	maximum = 0.301333 (at node 54)
Accepted flit rate average= 0.195484
	minimum = 0.1465 (at node 162)
	maximum = 0.260833 (at node 128)
Injected packet length average = 18.0031
Accepted packet length average = 17.9956
Total in-flight flits = 37466 (37412 measured)
latency change    = 0.0924868
throughput change = 0.00374694
Class 0:
Packet latency average = 4460.04
	minimum = 33
	maximum = 9308
Network latency average = 944.452
	minimum = 23
	maximum = 5894
Slowest packet = 8215
Flit latency average = 898.771
	minimum = 6
	maximum = 7490
Slowest flit = 123431
Fragmentation average = 198.21
	minimum = 0
	maximum = 3158
Injected packet rate average = 0.0108222
	minimum = 0.00414286 (at node 148)
	maximum = 0.017 (at node 54)
Accepted packet rate average = 0.0108296
	minimum = 0.00785714 (at node 64)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.194937
	minimum = 0.0744286 (at node 148)
	maximum = 0.303571 (at node 54)
Accepted flit rate average= 0.195048
	minimum = 0.145286 (at node 86)
	maximum = 0.255857 (at node 128)
Injected packet length average = 18.0128
Accepted packet length average = 18.0106
Total in-flight flits = 33981 (33969 measured)
latency change    = 0.0787278
throughput change = 0.00223478
Draining all recorded packets ...
Class 0:
Remaining flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (34976 flits)
Measured flits: 181764 181765 181766 181767 181768 181769 181770 181771 181772 181773 [...] (34976 flits)
Class 0:
Remaining flits: 201492 201493 201494 201495 201496 201497 201498 201499 201500 201501 [...] (37482 flits)
Measured flits: 201492 201493 201494 201495 201496 201497 201498 201499 201500 201501 [...] (37482 flits)
Class 0:
Remaining flits: 201492 201493 201494 201495 201496 201497 201498 201499 201500 201501 [...] (36058 flits)
Measured flits: 201492 201493 201494 201495 201496 201497 201498 201499 201500 201501 [...] (36058 flits)
Class 0:
Remaining flits: 306717 306718 306719 317322 317323 317324 317325 317326 317327 317328 [...] (35656 flits)
Measured flits: 306717 306718 306719 317322 317323 317324 317325 317326 317327 317328 [...] (35568 flits)
Class 0:
Remaining flits: 317322 317323 317324 317325 317326 317327 317328 317329 317330 317331 [...] (35858 flits)
Measured flits: 317322 317323 317324 317325 317326 317327 317328 317329 317330 317331 [...] (35365 flits)
Class 0:
Remaining flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (38340 flits)
Measured flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (37426 flits)
Class 0:
Remaining flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (34569 flits)
Measured flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (33751 flits)
Class 0:
Remaining flits: 420948 420949 420950 420951 420952 420953 420954 420955 420956 420957 [...] (35238 flits)
Measured flits: 420948 420949 420950 420951 420952 420953 420954 420955 420956 420957 [...] (33622 flits)
Class 0:
Remaining flits: 420948 420949 420950 420951 420952 420953 420954 420955 420956 420957 [...] (36920 flits)
Measured flits: 420948 420949 420950 420951 420952 420953 420954 420955 420956 420957 [...] (34591 flits)
Class 0:
Remaining flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (35603 flits)
Measured flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (32179 flits)
Class 0:
Remaining flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (37213 flits)
Measured flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (33014 flits)
Class 0:
Remaining flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (36315 flits)
Measured flits: 551376 551377 551378 551379 551380 551381 551382 551383 551384 551385 [...] (30523 flits)
Class 0:
Remaining flits: 551381 551382 551383 551384 551385 551386 551387 551388 551389 551390 [...] (36294 flits)
Measured flits: 551381 551382 551383 551384 551385 551386 551387 551388 551389 551390 [...] (29388 flits)
Class 0:
Remaining flits: 641545 641546 641547 641548 641549 641550 641551 641552 641553 641554 [...] (37657 flits)
Measured flits: 641545 641546 641547 641548 641549 641550 641551 641552 641553 641554 [...] (28970 flits)
Class 0:
Remaining flits: 665712 665713 665714 665715 665716 665717 665718 665719 665720 665721 [...] (36907 flits)
Measured flits: 665712 665713 665714 665715 665716 665717 665718 665719 665720 665721 [...] (26170 flits)
Class 0:
Remaining flits: 665712 665713 665714 665715 665716 665717 665718 665719 665720 665721 [...] (37822 flits)
Measured flits: 665712 665713 665714 665715 665716 665717 665718 665719 665720 665721 [...] (24459 flits)
Class 0:
Remaining flits: 669816 669817 669818 669819 669820 669821 669822 669823 669824 669825 [...] (35387 flits)
Measured flits: 669816 669817 669818 669819 669820 669821 669822 669823 669824 669825 [...] (21654 flits)
Class 0:
Remaining flits: 766224 766225 766226 766227 766228 766229 766230 766231 766232 766233 [...] (35659 flits)
Measured flits: 766224 766225 766226 766227 766228 766229 766230 766231 766232 766233 [...] (19881 flits)
Class 0:
Remaining flits: 798282 798283 798284 798285 798286 798287 798288 798289 798290 798291 [...] (37413 flits)
Measured flits: 798282 798283 798284 798285 798286 798287 798288 798289 798290 798291 [...] (19089 flits)
Class 0:
Remaining flits: 798282 798283 798284 798285 798286 798287 798288 798289 798290 798291 [...] (36315 flits)
Measured flits: 798282 798283 798284 798285 798286 798287 798288 798289 798290 798291 [...] (16704 flits)
Class 0:
Remaining flits: 903546 903547 903548 903549 903550 903551 903552 903553 903554 903555 [...] (35951 flits)
Measured flits: 934092 934093 934094 934095 934096 934097 934098 934099 934100 934101 [...] (14413 flits)
Class 0:
Remaining flits: 903546 903547 903548 903549 903550 903551 903552 903553 903554 903555 [...] (36296 flits)
Measured flits: 1025444 1025445 1025446 1025447 1025448 1025449 1025450 1025451 1025452 1025453 [...] (13941 flits)
Class 0:
Remaining flits: 1007857 1007858 1007859 1007860 1007861 1007862 1007863 1007864 1007865 1007866 [...] (35946 flits)
Measured flits: 1048152 1048153 1048154 1048155 1048156 1048157 1054710 1054711 1054712 1054713 [...] (11316 flits)
Class 0:
Remaining flits: 1031548 1031549 1031550 1031551 1031552 1031553 1031554 1031555 1031556 1031557 [...] (35143 flits)
Measured flits: 1062918 1062919 1062920 1062921 1062922 1062923 1062924 1062925 1062926 1062927 [...] (10108 flits)
Class 0:
Remaining flits: 1031548 1031549 1031550 1031551 1031552 1031553 1031554 1031555 1031556 1031557 [...] (37969 flits)
Measured flits: 1062918 1062919 1062920 1062921 1062922 1062923 1062924 1062925 1062926 1062927 [...] (9197 flits)
Class 0:
Remaining flits: 1073754 1073755 1073756 1073757 1073758 1073759 1073760 1073761 1073762 1073763 [...] (37150 flits)
Measured flits: 1073754 1073755 1073756 1073757 1073758 1073759 1073760 1073761 1073762 1073763 [...] (7878 flits)
Class 0:
Remaining flits: 1073766 1073767 1073768 1073769 1073770 1073771 1106077 1106078 1106079 1106080 [...] (36337 flits)
Measured flits: 1073766 1073767 1073768 1073769 1073770 1073771 1165068 1165069 1165070 1165071 [...] (6194 flits)
Class 0:
Remaining flits: 1238113 1238114 1238115 1238116 1238117 1238118 1238119 1238120 1238121 1238122 [...] (35967 flits)
Measured flits: 1252026 1252027 1252028 1252029 1252030 1252031 1252032 1252033 1252034 1252035 [...] (5563 flits)
Class 0:
Remaining flits: 1242702 1242703 1242704 1242705 1242706 1242707 1242708 1242709 1242710 1242711 [...] (36568 flits)
Measured flits: 1265616 1265617 1265618 1265619 1265620 1265621 1265622 1265623 1265624 1265625 [...] (5287 flits)
Class 0:
Remaining flits: 1242717 1242718 1242719 1265631 1265632 1265633 1275498 1275499 1275500 1275501 [...] (37278 flits)
Measured flits: 1265631 1265632 1265633 1292742 1292743 1292744 1292745 1292746 1292747 1292748 [...] (4693 flits)
Class 0:
Remaining flits: 1292742 1292743 1292744 1292745 1292746 1292747 1292748 1292749 1292750 1292751 [...] (36276 flits)
Measured flits: 1292742 1292743 1292744 1292745 1292746 1292747 1292748 1292749 1292750 1292751 [...] (4163 flits)
Class 0:
Remaining flits: 1349281 1349282 1349283 1349284 1349285 1349286 1349287 1349288 1349289 1349290 [...] (36327 flits)
Measured flits: 1420352 1420353 1420354 1420355 1420356 1420357 1420358 1420359 1420360 1420361 [...] (3573 flits)
Class 0:
Remaining flits: 1349296 1349297 1377793 1377794 1377795 1377796 1377797 1377798 1377799 1377800 [...] (37339 flits)
Measured flits: 1422702 1422703 1422704 1422705 1422706 1422707 1422708 1422709 1422710 1422711 [...] (2936 flits)
Class 0:
Remaining flits: 1397981 1397982 1397983 1397984 1397985 1397986 1397987 1409777 1411956 1411957 [...] (37310 flits)
Measured flits: 1422702 1422703 1422704 1422705 1422706 1422707 1422708 1422709 1422710 1422711 [...] (2694 flits)
Class 0:
Remaining flits: 1411956 1411957 1411958 1411959 1411960 1411961 1411962 1411963 1411964 1411965 [...] (37428 flits)
Measured flits: 1502893 1502894 1502895 1502896 1502897 1502898 1502899 1502900 1502901 1502902 [...] (2514 flits)
Class 0:
Remaining flits: 1411956 1411957 1411958 1411959 1411960 1411961 1411962 1411963 1411964 1411965 [...] (35624 flits)
Measured flits: 1570904 1570905 1570906 1570907 1570908 1570909 1570910 1570911 1570912 1570913 [...] (2473 flits)
Class 0:
Remaining flits: 1411964 1411965 1411966 1411967 1411968 1411969 1411970 1411971 1411972 1411973 [...] (37321 flits)
Measured flits: 1574597 1574598 1574599 1574600 1574601 1574602 1574603 1592892 1592893 1592894 [...] (1912 flits)
Class 0:
Remaining flits: 1411964 1411965 1411966 1411967 1411968 1411969 1411970 1411971 1411972 1411973 [...] (36840 flits)
Measured flits: 1592892 1592893 1592894 1592895 1592896 1592897 1592898 1592899 1592900 1592901 [...] (1850 flits)
Class 0:
Remaining flits: 1498518 1498519 1498520 1498521 1498522 1498523 1498524 1498525 1498526 1498527 [...] (36035 flits)
Measured flits: 1592892 1592893 1592894 1592895 1592896 1592897 1592898 1592899 1592900 1592901 [...] (1699 flits)
Class 0:
Remaining flits: 1498518 1498519 1498520 1498521 1498522 1498523 1498524 1498525 1498526 1498527 [...] (36831 flits)
Measured flits: 1592901 1592902 1592903 1592904 1592905 1592906 1592907 1592908 1592909 1690480 [...] (1352 flits)
Class 0:
Remaining flits: 1498518 1498519 1498520 1498521 1498522 1498523 1498524 1498525 1498526 1498527 [...] (37537 flits)
Measured flits: 1690480 1690481 1690482 1690483 1690484 1690485 1690486 1690487 1709532 1709533 [...] (1345 flits)
Class 0:
Remaining flits: 1664154 1664155 1664156 1664157 1664158 1664159 1664160 1664161 1664162 1664163 [...] (36945 flits)
Measured flits: 1827753 1827754 1827755 1831626 1831627 1831628 1831629 1831630 1831631 1831632 [...] (1191 flits)
Class 0:
Remaining flits: 1759896 1759897 1759898 1759899 1759900 1759901 1759902 1759903 1759904 1759905 [...] (35881 flits)
Measured flits: 1852776 1852777 1852778 1852779 1852780 1852781 1852782 1852783 1852784 1852785 [...] (990 flits)
Class 0:
Remaining flits: 1759896 1759897 1759898 1759899 1759900 1759901 1759902 1759903 1759904 1759905 [...] (36692 flits)
Measured flits: 1932863 1932864 1932865 1932866 1932867 1932868 1932869 1932870 1932871 1932872 [...] (786 flits)
Class 0:
Remaining flits: 1759896 1759897 1759898 1759899 1759900 1759901 1759902 1759903 1759904 1759905 [...] (38370 flits)
Measured flits: 1937887 1937888 1937889 1937890 1937891 1937892 1937893 1937894 1937895 1937896 [...] (946 flits)
Class 0:
Remaining flits: 1759896 1759897 1759898 1759899 1759900 1759901 1759902 1759903 1759904 1759905 [...] (37317 flits)
Measured flits: 2009610 2009611 2009612 2009613 2009614 2009615 2009616 2009617 2009618 2009619 [...] (831 flits)
Class 0:
Remaining flits: 1882494 1882495 1882496 1882497 1882498 1882499 1882500 1882501 1882502 1882503 [...] (36729 flits)
Measured flits: 2047518 2047519 2047520 2047521 2047522 2047523 2047524 2047525 2047526 2047527 [...] (749 flits)
Class 0:
Remaining flits: 1959102 1959103 1959104 1959105 1959106 1959107 1959108 1959109 1959110 1959111 [...] (36523 flits)
Measured flits: 2047518 2047519 2047520 2047521 2047522 2047523 2047524 2047525 2047526 2047527 [...] (585 flits)
Class 0:
Remaining flits: 1989504 1989505 1989506 1989507 1989508 1989509 1989510 1989511 1989512 1989513 [...] (37180 flits)
Measured flits: 2047518 2047519 2047520 2047521 2047522 2047523 2047524 2047525 2047526 2047527 [...] (690 flits)
Class 0:
Remaining flits: 1989513 1989514 1989515 1989516 1989517 1989518 1989519 1989520 1989521 2019834 [...] (37064 flits)
Measured flits: 2097810 2097811 2097812 2097813 2097814 2097815 2097816 2097817 2097818 2097819 [...] (530 flits)
Class 0:
Remaining flits: 2022210 2022211 2022212 2022213 2022214 2022215 2022216 2022217 2022218 2022219 [...] (37623 flits)
Measured flits: 2138760 2138761 2138762 2138763 2138764 2138765 2138766 2138767 2138768 2138769 [...] (352 flits)
Class 0:
Remaining flits: 2037023 2039400 2039401 2039402 2039403 2039404 2039405 2039406 2039407 2039408 [...] (37555 flits)
Measured flits: 2201701 2201702 2201703 2201704 2201705 2268324 2268325 2268326 2268327 2268328 [...] (175 flits)
Class 0:
Remaining flits: 2039400 2039401 2039402 2039403 2039404 2039405 2039406 2039407 2039408 2039409 [...] (37707 flits)
Measured flits: 2291076 2291077 2291078 2291079 2291080 2291081 2291082 2291083 2291084 2291085 [...] (108 flits)
Class 0:
Remaining flits: 2164734 2164735 2164736 2164737 2164738 2164739 2164740 2164741 2164742 2164743 [...] (37781 flits)
Measured flits: 2291076 2291077 2291078 2291079 2291080 2291081 2291082 2291083 2291084 2291085 [...] (54 flits)
Class 0:
Remaining flits: 2164734 2164735 2164736 2164737 2164738 2164739 2164740 2164741 2164742 2164743 [...] (37787 flits)
Measured flits: 2291076 2291077 2291078 2291079 2291080 2291081 2291082 2291083 2291084 2291085 [...] (54 flits)
Class 0:
Remaining flits: 2188584 2188585 2188586 2188587 2188588 2188589 2188590 2188591 2188592 2188593 [...] (37853 flits)
Measured flits: 2291076 2291077 2291078 2291079 2291080 2291081 2291082 2291083 2291084 2291085 [...] (170 flits)
Class 0:
Remaining flits: 2188584 2188585 2188586 2188587 2188588 2188589 2188590 2188591 2188592 2188593 [...] (35417 flits)
Measured flits: 2291076 2291077 2291078 2291079 2291080 2291081 2291082 2291083 2291084 2291085 [...] (137 flits)
Class 0:
Remaining flits: 2207754 2207755 2207756 2207757 2207758 2207759 2207760 2207761 2207762 2207763 [...] (35440 flits)
Measured flits: 2452068 2452069 2452070 2452071 2452072 2452073 2452074 2452075 2452076 2452077 [...] (105 flits)
Class 0:
Remaining flits: 2298798 2298799 2298800 2298801 2298802 2298803 2298804 2298805 2298806 2298807 [...] (37297 flits)
Measured flits: 2501748 2501749 2501750 2501751 2501752 2501753 2501754 2501755 2501756 2501757 [...] (36 flits)
Class 0:
Remaining flits: 2348838 2348839 2348840 2348841 2348842 2348843 2348844 2348845 2348846 2348847 [...] (37335 flits)
Measured flits: 2501748 2501749 2501750 2501751 2501752 2501753 2501754 2501755 2501756 2501757 [...] (36 flits)
Class 0:
Remaining flits: 2378052 2378053 2378054 2378055 2378056 2378057 2378058 2378059 2378060 2378061 [...] (38377 flits)
Measured flits: 2501759 2501760 2501761 2501762 2501763 2501764 2501765 2539368 2539369 2539370 [...] (25 flits)
Class 0:
Remaining flits: 2378052 2378053 2378054 2378055 2378056 2378057 2378058 2378059 2378060 2378061 [...] (38555 flits)
Measured flits: 2539368 2539369 2539370 2539371 2539372 2539373 2539374 2539375 2539376 2539377 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2378065 2378066 2378067 2378068 2378069 2463408 2463409 2463410 2463411 2463412 [...] (7606 flits)
Measured flits: (0 flits)
Time taken is 74241 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 13907.4 (1 samples)
	minimum = 33 (1 samples)
	maximum = 62662 (1 samples)
Network latency average = 1052.54 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10450 (1 samples)
Flit latency average = 953.263 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12259 (1 samples)
Fragmentation average = 207.725 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5127 (1 samples)
Injected packet rate average = 0.0108222 (1 samples)
	minimum = 0.00414286 (1 samples)
	maximum = 0.017 (1 samples)
Accepted packet rate average = 0.0108296 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.194937 (1 samples)
	minimum = 0.0744286 (1 samples)
	maximum = 0.303571 (1 samples)
Accepted flit rate average = 0.195048 (1 samples)
	minimum = 0.145286 (1 samples)
	maximum = 0.255857 (1 samples)
Injected packet size average = 18.0128 (1 samples)
Accepted packet size average = 18.0106 (1 samples)
Hops average = 5.04013 (1 samples)
Total run time 79.692
