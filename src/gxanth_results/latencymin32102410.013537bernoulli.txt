BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 189.089
	minimum = 23
	maximum = 911
Network latency average = 186.794
	minimum = 23
	maximum = 889
Slowest packet = 191
Flit latency average = 121.347
	minimum = 6
	maximum = 872
Slowest flit = 3455
Fragmentation average = 113.611
	minimum = 0
	maximum = 730
Injected packet rate average = 0.0136094
	minimum = 0.006 (at node 87)
	maximum = 0.022 (at node 123)
Accepted packet rate average = 0.00896875
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 49)
Injected flit rate average = 0.242297
	minimum = 0.1 (at node 87)
	maximum = 0.396 (at node 123)
Accepted flit rate average= 0.181
	minimum = 0.061 (at node 174)
	maximum = 0.308 (at node 132)
Injected packet length average = 17.8037
Accepted packet length average = 20.1812
Total in-flight flits = 12282 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 292.645
	minimum = 23
	maximum = 1717
Network latency average = 290.168
	minimum = 23
	maximum = 1717
Slowest packet = 582
Flit latency average = 208.074
	minimum = 6
	maximum = 1700
Slowest flit = 10493
Fragmentation average = 152.179
	minimum = 0
	maximum = 1362
Injected packet rate average = 0.0133906
	minimum = 0.0075 (at node 161)
	maximum = 0.019 (at node 77)
Accepted packet rate average = 0.00991406
	minimum = 0.005 (at node 62)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.239911
	minimum = 0.135 (at node 161)
	maximum = 0.342 (at node 77)
Accepted flit rate average= 0.190432
	minimum = 0.1135 (at node 79)
	maximum = 0.3015 (at node 166)
Injected packet length average = 17.9164
Accepted packet length average = 19.2083
Total in-flight flits = 19430 (0 measured)
latency change    = 0.35386
throughput change = 0.0495309
Class 0:
Packet latency average = 523.857
	minimum = 23
	maximum = 2603
Network latency average = 520.994
	minimum = 23
	maximum = 2589
Slowest packet = 1011
Flit latency average = 415.111
	minimum = 6
	maximum = 2572
Slowest flit = 18215
Fragmentation average = 208.157
	minimum = 0
	maximum = 2177
Injected packet rate average = 0.0134844
	minimum = 0.004 (at node 136)
	maximum = 0.025 (at node 191)
Accepted packet rate average = 0.0112917
	minimum = 0.003 (at node 184)
	maximum = 0.019 (at node 68)
Injected flit rate average = 0.242443
	minimum = 0.064 (at node 136)
	maximum = 0.457 (at node 191)
Accepted flit rate average= 0.204641
	minimum = 0.058 (at node 184)
	maximum = 0.33 (at node 187)
Injected packet length average = 17.9795
Accepted packet length average = 18.1232
Total in-flight flits = 26741 (0 measured)
latency change    = 0.441365
throughput change = 0.0694307
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 325.03
	minimum = 25
	maximum = 943
Network latency average = 322.417
	minimum = 25
	maximum = 943
Slowest packet = 7812
Flit latency average = 546.6
	minimum = 6
	maximum = 3455
Slowest flit = 24753
Fragmentation average = 145.033
	minimum = 0
	maximum = 804
Injected packet rate average = 0.0140469
	minimum = 0.004 (at node 150)
	maximum = 0.025 (at node 84)
Accepted packet rate average = 0.0115469
	minimum = 0.003 (at node 180)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.252427
	minimum = 0.072 (at node 150)
	maximum = 0.45 (at node 84)
Accepted flit rate average= 0.20924
	minimum = 0.068 (at node 74)
	maximum = 0.401 (at node 16)
Injected packet length average = 17.9703
Accepted packet length average = 18.1209
Total in-flight flits = 35113 (28140 measured)
latency change    = 0.611719
throughput change = 0.0219794
Class 0:
Packet latency average = 508.797
	minimum = 24
	maximum = 1878
Network latency average = 506.102
	minimum = 24
	maximum = 1869
Slowest packet = 7836
Flit latency average = 614.37
	minimum = 6
	maximum = 4250
Slowest flit = 34462
Fragmentation average = 170.522
	minimum = 0
	maximum = 1501
Injected packet rate average = 0.0136901
	minimum = 0.0085 (at node 10)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.0116042
	minimum = 0.0055 (at node 17)
	maximum = 0.02 (at node 137)
Injected flit rate average = 0.246563
	minimum = 0.153 (at node 10)
	maximum = 0.392 (at node 126)
Accepted flit rate average= 0.209063
	minimum = 0.1035 (at node 17)
	maximum = 0.351 (at node 137)
Injected packet length average = 18.0103
Accepted packet length average = 18.0162
Total in-flight flits = 41087 (38661 measured)
latency change    = 0.36118
throughput change = 0.000847035
Class 0:
Packet latency average = 647.72
	minimum = 24
	maximum = 2962
Network latency average = 645.083
	minimum = 23
	maximum = 2945
Slowest packet = 7732
Flit latency average = 685.957
	minimum = 6
	maximum = 4633
Slowest flit = 63480
Fragmentation average = 186.16
	minimum = 0
	maximum = 2236
Injected packet rate average = 0.013658
	minimum = 0.00866667 (at node 18)
	maximum = 0.0196667 (at node 116)
Accepted packet rate average = 0.0114913
	minimum = 0.00766667 (at node 5)
	maximum = 0.016 (at node 8)
Injected flit rate average = 0.245995
	minimum = 0.156 (at node 18)
	maximum = 0.354 (at node 116)
Accepted flit rate average= 0.207064
	minimum = 0.143 (at node 5)
	maximum = 0.295333 (at node 59)
Injected packet length average = 18.0111
Accepted packet length average = 18.0192
Total in-flight flits = 49078 (48039 measured)
latency change    = 0.21448
throughput change = 0.00965045
Class 0:
Packet latency average = 763.262
	minimum = 23
	maximum = 3737
Network latency average = 760.597
	minimum = 23
	maximum = 3737
Slowest packet = 8158
Flit latency average = 757.299
	minimum = 6
	maximum = 5187
Slowest flit = 51371
Fragmentation average = 192.554
	minimum = 0
	maximum = 2236
Injected packet rate average = 0.0136523
	minimum = 0.0085 (at node 172)
	maximum = 0.01775 (at node 126)
Accepted packet rate average = 0.0114922
	minimum = 0.0075 (at node 36)
	maximum = 0.01475 (at node 59)
Injected flit rate average = 0.245932
	minimum = 0.15675 (at node 172)
	maximum = 0.3195 (at node 126)
Accepted flit rate average= 0.207046
	minimum = 0.13475 (at node 36)
	maximum = 0.27025 (at node 66)
Injected packet length average = 18.0139
Accepted packet length average = 18.0162
Total in-flight flits = 56460 (56029 measured)
latency change    = 0.15138
throughput change = 9.01405e-05
Class 0:
Packet latency average = 866.451
	minimum = 23
	maximum = 4836
Network latency average = 863.766
	minimum = 23
	maximum = 4836
Slowest packet = 7791
Flit latency average = 829.379
	minimum = 6
	maximum = 6008
Slowest flit = 82925
Fragmentation average = 198.185
	minimum = 0
	maximum = 2623
Injected packet rate average = 0.013574
	minimum = 0.0106 (at node 163)
	maximum = 0.0178 (at node 116)
Accepted packet rate average = 0.0115292
	minimum = 0.0082 (at node 2)
	maximum = 0.0144 (at node 59)
Injected flit rate average = 0.244522
	minimum = 0.191 (at node 163)
	maximum = 0.3178 (at node 116)
Accepted flit rate average= 0.207482
	minimum = 0.1522 (at node 162)
	maximum = 0.2614 (at node 90)
Injected packet length average = 18.014
Accepted packet length average = 17.9963
Total in-flight flits = 62116 (61970 measured)
latency change    = 0.119094
throughput change = 0.00210485
Class 0:
Packet latency average = 960.169
	minimum = 23
	maximum = 5527
Network latency average = 957.471
	minimum = 23
	maximum = 5527
Slowest packet = 8828
Flit latency average = 899.607
	minimum = 6
	maximum = 6987
Slowest flit = 82565
Fragmentation average = 201.257
	minimum = 0
	maximum = 2623
Injected packet rate average = 0.013599
	minimum = 0.0103333 (at node 163)
	maximum = 0.0173333 (at node 165)
Accepted packet rate average = 0.0115417
	minimum = 0.00883333 (at node 5)
	maximum = 0.0146667 (at node 157)
Injected flit rate average = 0.244839
	minimum = 0.186167 (at node 163)
	maximum = 0.311 (at node 165)
Accepted flit rate average= 0.207316
	minimum = 0.164 (at node 64)
	maximum = 0.262667 (at node 157)
Injected packet length average = 18.0043
Accepted packet length average = 17.9624
Total in-flight flits = 69901 (69862 measured)
latency change    = 0.0976056
throughput change = 0.000802251
Class 0:
Packet latency average = 1041.77
	minimum = 23
	maximum = 6205
Network latency average = 1039.07
	minimum = 23
	maximum = 6205
Slowest packet = 9260
Flit latency average = 965.879
	minimum = 6
	maximum = 7459
Slowest flit = 114371
Fragmentation average = 201.082
	minimum = 0
	maximum = 3251
Injected packet rate average = 0.0135863
	minimum = 0.0104286 (at node 163)
	maximum = 0.0168571 (at node 165)
Accepted packet rate average = 0.011503
	minimum = 0.00885714 (at node 144)
	maximum = 0.0142857 (at node 103)
Injected flit rate average = 0.244568
	minimum = 0.187 (at node 163)
	maximum = 0.303429 (at node 165)
Accepted flit rate average= 0.206478
	minimum = 0.160571 (at node 64)
	maximum = 0.257714 (at node 103)
Injected packet length average = 18.001
Accepted packet length average = 17.95
Total in-flight flits = 77914 (77914 measured)
latency change    = 0.0783264
throughput change = 0.00405635
Draining all recorded packets ...
Class 0:
Remaining flits: 140256 140257 140258 140259 140260 140261 140262 140263 140264 140265 [...] (86502 flits)
Measured flits: 140256 140257 140258 140259 140260 140261 140262 140263 140264 140265 [...] (48654 flits)
Class 0:
Remaining flits: 173160 173161 173162 173163 173164 173165 173166 173167 173168 173169 [...] (93219 flits)
Measured flits: 173160 173161 173162 173163 173164 173165 173166 173167 173168 173169 [...] (30492 flits)
Class 0:
Remaining flits: 173175 173176 173177 178056 178057 178058 178059 178060 178061 178062 [...] (100160 flits)
Measured flits: 173175 173176 173177 178056 178057 178058 178059 178060 178061 178062 [...] (18285 flits)
Class 0:
Remaining flits: 178073 215427 215428 215429 215430 215431 215432 215433 215434 215435 [...] (107994 flits)
Measured flits: 178073 215427 215428 215429 215430 215431 215432 215433 215434 215435 [...] (10684 flits)
Class 0:
Remaining flits: 243666 243667 243668 243669 243670 243671 243672 243673 243674 243675 [...] (114699 flits)
Measured flits: 243666 243667 243668 243669 243670 243671 243672 243673 243674 243675 [...] (6165 flits)
Class 0:
Remaining flits: 243674 243675 243676 243677 243678 243679 243680 243681 243682 243683 [...] (124898 flits)
Measured flits: 243674 243675 243676 243677 243678 243679 243680 243681 243682 243683 [...] (3570 flits)
Class 0:
Remaining flits: 254055 254056 254057 254058 254059 254060 254061 254062 254063 254064 [...] (131804 flits)
Measured flits: 254055 254056 254057 254058 254059 254060 254061 254062 254063 254064 [...] (1917 flits)
Class 0:
Remaining flits: 282765 282766 282767 282768 282769 282770 282771 282772 282773 282774 [...] (140153 flits)
Measured flits: 282765 282766 282767 282768 282769 282770 282771 282772 282773 282774 [...] (960 flits)
Class 0:
Remaining flits: 306738 306739 306740 306741 306742 306743 306744 306745 306746 306747 [...] (148916 flits)
Measured flits: 306738 306739 306740 306741 306742 306743 306744 306745 306746 306747 [...] (342 flits)
Class 0:
Remaining flits: 306741 306742 306743 306744 306745 306746 306747 306748 306749 306750 [...] (157830 flits)
Measured flits: 306741 306742 306743 306744 306745 306746 306747 306748 306749 306750 [...] (179 flits)
Class 0:
Remaining flits: 331848 331849 331850 331851 331852 331853 331854 331855 331856 331857 [...] (165831 flits)
Measured flits: 331848 331849 331850 331851 331852 331853 331854 331855 331856 331857 [...] (94 flits)
Class 0:
Remaining flits: 358305 358306 358307 382428 382429 382430 382431 382432 382433 382434 [...] (174572 flits)
Measured flits: 358305 358306 358307 382428 382429 382430 382431 382432 382433 382434 [...] (55 flits)
Class 0:
Remaining flits: 465157 465158 465159 465160 465161 465162 465163 465164 465165 465166 [...] (183681 flits)
Measured flits: 465157 465158 465159 465160 465161 465162 465163 465164 465165 465166 [...] (17 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 542268 542269 542270 542271 542272 542273 542274 542275 542276 542277 [...] (154989 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 574574 574575 574576 574577 576828 576829 576830 576831 576832 576833 [...] (121958 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 606060 606061 606062 606063 606064 606065 606066 606067 606068 606069 [...] (88367 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 606978 606979 606980 606981 606982 606983 606984 606985 606986 606987 [...] (55902 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 606978 606979 606980 606981 606982 606983 606984 606985 606986 606987 [...] (28107 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 647509 647510 647511 647512 647513 666648 666649 666650 666651 666652 [...] (8592 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 902147 902148 902149 902150 902151 902152 902153 902154 902155 902156 [...] (834 flits)
Measured flits: (0 flits)
Time taken is 31106 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1648.74 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14763 (1 samples)
Network latency average = 1645.98 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14763 (1 samples)
Flit latency average = 2727.63 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16295 (1 samples)
Fragmentation average = 215.884 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3483 (1 samples)
Injected packet rate average = 0.0135863 (1 samples)
	minimum = 0.0104286 (1 samples)
	maximum = 0.0168571 (1 samples)
Accepted packet rate average = 0.011503 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0142857 (1 samples)
Injected flit rate average = 0.244568 (1 samples)
	minimum = 0.187 (1 samples)
	maximum = 0.303429 (1 samples)
Accepted flit rate average = 0.206478 (1 samples)
	minimum = 0.160571 (1 samples)
	maximum = 0.257714 (1 samples)
Injected packet size average = 18.001 (1 samples)
Accepted packet size average = 17.95 (1 samples)
Hops average = 5.05853 (1 samples)
Total run time 26.526
