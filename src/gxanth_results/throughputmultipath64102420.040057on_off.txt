BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.847
	minimum = 22
	maximum = 981
Network latency average = 236.79
	minimum = 22
	maximum = 820
Slowest packet = 46
Flit latency average = 205.513
	minimum = 5
	maximum = 803
Slowest flit = 8027
Fragmentation average = 47.9443
	minimum = 0
	maximum = 614
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0139427
	minimum = 0.005 (at node 150)
	maximum = 0.024 (at node 22)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.263448
	minimum = 0.118 (at node 150)
	maximum = 0.432 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 18.895
Total in-flight flits = 52252 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 615.723
	minimum = 22
	maximum = 1946
Network latency average = 448.502
	minimum = 22
	maximum = 1723
Slowest packet = 46
Flit latency average = 416.053
	minimum = 5
	maximum = 1713
Slowest flit = 25334
Fragmentation average = 73.1419
	minimum = 0
	maximum = 750
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 10)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0147578
	minimum = 0.0085 (at node 30)
	maximum = 0.0215 (at node 63)
Injected flit rate average = 0.561771
	minimum = 0.022 (at node 10)
	maximum = 1 (at node 38)
Accepted flit rate average= 0.276404
	minimum = 0.1655 (at node 164)
	maximum = 0.393 (at node 63)
Injected packet length average = 17.9125
Accepted packet length average = 18.7293
Total in-flight flits = 110635 (0 measured)
latency change    = 0.428563
throughput change = 0.0468725
Class 0:
Packet latency average = 1349.24
	minimum = 27
	maximum = 2824
Network latency average = 1070.27
	minimum = 22
	maximum = 2425
Slowest packet = 3663
Flit latency average = 1031.52
	minimum = 5
	maximum = 2514
Slowest flit = 43806
Fragmentation average = 134.255
	minimum = 0
	maximum = 923
Injected packet rate average = 0.035349
	minimum = 0 (at node 32)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0163438
	minimum = 0.007 (at node 190)
	maximum = 0.035 (at node 16)
Injected flit rate average = 0.636193
	minimum = 0 (at node 32)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.298167
	minimum = 0.12 (at node 113)
	maximum = 0.634 (at node 16)
Injected packet length average = 17.9975
Accepted packet length average = 18.2435
Total in-flight flits = 175553 (0 measured)
latency change    = 0.543653
throughput change = 0.0729894
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 443.186
	minimum = 26
	maximum = 2070
Network latency average = 35.5084
	minimum = 22
	maximum = 75
Slowest packet = 18852
Flit latency average = 1451.73
	minimum = 5
	maximum = 3301
Slowest flit = 65730
Fragmentation average = 6.97993
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0374375
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0164219
	minimum = 0.006 (at node 5)
	maximum = 0.026 (at node 9)
Injected flit rate average = 0.674375
	minimum = 0 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.300307
	minimum = 0.128 (at node 48)
	maximum = 0.465 (at node 46)
Injected packet length average = 18.0134
Accepted packet length average = 18.287
Total in-flight flits = 247278 (118528 measured)
latency change    = 2.04442
throughput change = 0.00712812
Class 0:
Packet latency average = 503.759
	minimum = 26
	maximum = 2596
Network latency average = 93.6119
	minimum = 22
	maximum = 1986
Slowest packet = 18852
Flit latency average = 1684.76
	minimum = 5
	maximum = 4249
Slowest flit = 72837
Fragmentation average = 10.482
	minimum = 0
	maximum = 373
Injected packet rate average = 0.036862
	minimum = 0.0075 (at node 20)
	maximum = 0.0555 (at node 2)
Accepted packet rate average = 0.0164167
	minimum = 0.0085 (at node 48)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.663685
	minimum = 0.135 (at node 20)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.29994
	minimum = 0.159 (at node 48)
	maximum = 0.4595 (at node 103)
Injected packet length average = 18.0046
Accepted packet length average = 18.2705
Total in-flight flits = 315166 (232313 measured)
latency change    = 0.120243
throughput change = 0.0012242
Class 0:
Packet latency average = 894.306
	minimum = 25
	maximum = 3594
Network latency average = 513.567
	minimum = 22
	maximum = 2966
Slowest packet = 18852
Flit latency average = 1914.25
	minimum = 5
	maximum = 5153
Slowest flit = 76841
Fragmentation average = 40.7993
	minimum = 0
	maximum = 659
Injected packet rate average = 0.0367569
	minimum = 0.009 (at node 14)
	maximum = 0.0556667 (at node 2)
Accepted packet rate average = 0.0163194
	minimum = 0.011 (at node 48)
	maximum = 0.023 (at node 103)
Injected flit rate average = 0.661641
	minimum = 0.162 (at node 14)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.29984
	minimum = 0.202667 (at node 48)
	maximum = 0.413 (at node 103)
Injected packet length average = 18.0004
Accepted packet length average = 18.3732
Total in-flight flits = 383941 (338441 measured)
latency change    = 0.436704
throughput change = 0.000332932
Draining remaining packets ...
Class 0:
Remaining flits: 80956 80957 80958 80959 80960 80961 80962 80963 106378 106379 [...] (336355 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (312425 flits)
Class 0:
Remaining flits: 112750 112751 144833 144834 144835 144836 144837 144838 144839 144840 [...] (288710 flits)
Measured flits: 338940 338941 338942 338943 338944 338945 338946 338947 338948 338949 [...] (276135 flits)
Class 0:
Remaining flits: 174734 174735 174736 174737 174738 174739 174740 174741 174742 174743 [...] (240915 flits)
Measured flits: 339120 339121 339122 339123 339124 339125 339126 339127 339128 339129 [...] (235196 flits)
Class 0:
Remaining flits: 195237 195238 195239 195240 195241 195242 195243 195244 195245 216696 [...] (193564 flits)
Measured flits: 339120 339121 339122 339123 339124 339125 339126 339127 339128 339129 [...] (191438 flits)
Class 0:
Remaining flits: 256664 256665 256666 256667 256668 256669 256670 256671 256672 256673 [...] (146604 flits)
Measured flits: 339127 339128 339129 339130 339131 339132 339133 339134 339135 339136 [...] (146049 flits)
Class 0:
Remaining flits: 303866 303867 303868 303869 303870 303871 303872 303873 303874 303875 [...] (100490 flits)
Measured flits: 341820 341821 341822 341823 341824 341825 341826 341827 341828 341829 [...] (100402 flits)
Class 0:
Remaining flits: 353731 353732 353733 353734 353735 368118 368119 368120 368121 368122 [...] (57724 flits)
Measured flits: 353731 353732 353733 353734 353735 368118 368119 368120 368121 368122 [...] (57724 flits)
Class 0:
Remaining flits: 388663 388664 388665 388666 388667 388668 388669 388670 388671 388672 [...] (26537 flits)
Measured flits: 388663 388664 388665 388666 388667 388668 388669 388670 388671 388672 [...] (26537 flits)
Class 0:
Remaining flits: 404028 404029 404030 404031 404032 404033 404034 404035 404036 404037 [...] (8867 flits)
Measured flits: 404028 404029 404030 404031 404032 404033 404034 404035 404036 404037 [...] (8867 flits)
Class 0:
Remaining flits: 445578 445579 445580 445581 445582 445583 445584 445585 445586 445587 [...] (2746 flits)
Measured flits: 445578 445579 445580 445581 445582 445583 445584 445585 445586 445587 [...] (2746 flits)
Class 0:
Remaining flits: 463692 463693 463694 463695 463696 463697 487296 487297 487298 487299 [...] (1335 flits)
Measured flits: 463692 463693 463694 463695 463696 463697 487296 487297 487298 487299 [...] (1335 flits)
Class 0:
Remaining flits: 585999 586000 586001 586002 586003 586004 586005 586006 586007 588258 [...] (337 flits)
Measured flits: 585999 586000 586001 586002 586003 586004 586005 586006 586007 588258 [...] (337 flits)
Time taken is 18357 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5953.48 (1 samples)
	minimum = 25 (1 samples)
	maximum = 14189 (1 samples)
Network latency average = 5530.96 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13294 (1 samples)
Flit latency average = 4623.35 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13277 (1 samples)
Fragmentation average = 291.382 (1 samples)
	minimum = 0 (1 samples)
	maximum = 951 (1 samples)
Injected packet rate average = 0.0367569 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0163194 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.023 (1 samples)
Injected flit rate average = 0.661641 (1 samples)
	minimum = 0.162 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.29984 (1 samples)
	minimum = 0.202667 (1 samples)
	maximum = 0.413 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 18.3732 (1 samples)
Hops average = 5.07855 (1 samples)
Total run time 17.7781
