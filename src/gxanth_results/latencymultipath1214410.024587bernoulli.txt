BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 277.553
	minimum = 26
	maximum = 914
Network latency average = 271.431
	minimum = 23
	maximum = 909
Slowest packet = 228
Flit latency average = 237.319
	minimum = 6
	maximum = 917
Slowest flit = 5858
Fragmentation average = 56.8067
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0240885
	minimum = 0.012 (at node 164)
	maximum = 0.04 (at node 30)
Accepted packet rate average = 0.00980729
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 91)
Injected flit rate average = 0.430167
	minimum = 0.213 (at node 164)
	maximum = 0.72 (at node 30)
Accepted flit rate average= 0.184901
	minimum = 0.054 (at node 41)
	maximum = 0.324 (at node 91)
Injected packet length average = 17.8577
Accepted packet length average = 18.8534
Total in-flight flits = 47839 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 533.721
	minimum = 26
	maximum = 1782
Network latency average = 523.403
	minimum = 23
	maximum = 1782
Slowest packet = 755
Flit latency average = 488.268
	minimum = 6
	maximum = 1813
Slowest flit = 13642
Fragmentation average = 61.394
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0232031
	minimum = 0.0135 (at node 36)
	maximum = 0.034 (at node 30)
Accepted packet rate average = 0.00991406
	minimum = 0.0045 (at node 30)
	maximum = 0.015 (at node 152)
Injected flit rate average = 0.415471
	minimum = 0.243 (at node 36)
	maximum = 0.6075 (at node 30)
Accepted flit rate average= 0.182542
	minimum = 0.081 (at node 30)
	maximum = 0.2755 (at node 152)
Injected packet length average = 17.9058
Accepted packet length average = 18.4124
Total in-flight flits = 90608 (0 measured)
latency change    = 0.479967
throughput change = 0.0129251
Class 0:
Packet latency average = 1371.97
	minimum = 23
	maximum = 2496
Network latency average = 1331.17
	minimum = 23
	maximum = 2496
Slowest packet = 2317
Flit latency average = 1301.52
	minimum = 6
	maximum = 2492
Slowest flit = 41522
Fragmentation average = 66.1726
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0180833
	minimum = 0 (at node 140)
	maximum = 0.032 (at node 25)
Accepted packet rate average = 0.00977604
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.32675
	minimum = 0 (at node 140)
	maximum = 0.576 (at node 25)
Accepted flit rate average= 0.175562
	minimum = 0.054 (at node 40)
	maximum = 0.331 (at node 152)
Injected packet length average = 18.0691
Accepted packet length average = 17.9584
Total in-flight flits = 120512 (0 measured)
latency change    = 0.610983
throughput change = 0.0397532
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 836.493
	minimum = 23
	maximum = 2563
Network latency average = 180.88
	minimum = 23
	maximum = 884
Slowest packet = 12469
Flit latency average = 1890.13
	minimum = 6
	maximum = 3371
Slowest flit = 50867
Fragmentation average = 24.3067
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0111719
	minimum = 0 (at node 28)
	maximum = 0.03 (at node 85)
Accepted packet rate average = 0.00920833
	minimum = 0.003 (at node 65)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.200958
	minimum = 0 (at node 28)
	maximum = 0.529 (at node 85)
Accepted flit rate average= 0.165089
	minimum = 0.043 (at node 96)
	maximum = 0.329 (at node 16)
Injected packet length average = 17.9879
Accepted packet length average = 17.9282
Total in-flight flits = 127983 (37775 measured)
latency change    = 0.640149
throughput change = 0.0634445
Class 0:
Packet latency average = 1555.01
	minimum = 23
	maximum = 3190
Network latency average = 681.918
	minimum = 23
	maximum = 1952
Slowest packet = 12469
Flit latency average = 2164.93
	minimum = 6
	maximum = 4076
Slowest flit = 70254
Fragmentation average = 41.1765
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00976823
	minimum = 0.0005 (at node 64)
	maximum = 0.0225 (at node 61)
Accepted packet rate average = 0.00920573
	minimum = 0.0045 (at node 32)
	maximum = 0.016 (at node 182)
Injected flit rate average = 0.176435
	minimum = 0.009 (at node 64)
	maximum = 0.4 (at node 61)
Accepted flit rate average= 0.165628
	minimum = 0.0755 (at node 96)
	maximum = 0.272 (at node 182)
Injected packet length average = 18.0621
Accepted packet length average = 17.9918
Total in-flight flits = 125545 (63869 measured)
latency change    = 0.462066
throughput change = 0.00325467
Class 0:
Packet latency average = 2266.65
	minimum = 23
	maximum = 4266
Network latency average = 1200.97
	minimum = 23
	maximum = 2940
Slowest packet = 12469
Flit latency average = 2395.12
	minimum = 6
	maximum = 4882
Slowest flit = 82133
Fragmentation average = 53.745
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00927257
	minimum = 0.00166667 (at node 53)
	maximum = 0.0193333 (at node 27)
Accepted packet rate average = 0.00918229
	minimum = 0.00533333 (at node 96)
	maximum = 0.0146667 (at node 182)
Injected flit rate average = 0.166986
	minimum = 0.0263333 (at node 66)
	maximum = 0.348 (at node 27)
Accepted flit rate average= 0.165365
	minimum = 0.098 (at node 96)
	maximum = 0.256333 (at node 99)
Injected packet length average = 18.0086
Accepted packet length average = 18.0091
Total in-flight flits = 122174 (83818 measured)
latency change    = 0.313962
throughput change = 0.00159055
Class 0:
Packet latency average = 2858.67
	minimum = 23
	maximum = 5440
Network latency average = 1768.3
	minimum = 23
	maximum = 3951
Slowest packet = 12469
Flit latency average = 2607.13
	minimum = 6
	maximum = 5785
Slowest flit = 99799
Fragmentation average = 60.9779
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00914062
	minimum = 0.002 (at node 43)
	maximum = 0.018 (at node 58)
Accepted packet rate average = 0.00908464
	minimum = 0.00625 (at node 36)
	maximum = 0.01325 (at node 182)
Injected flit rate average = 0.16441
	minimum = 0.036 (at node 43)
	maximum = 0.3225 (at node 58)
Accepted flit rate average= 0.163382
	minimum = 0.1125 (at node 36)
	maximum = 0.23725 (at node 99)
Injected packet length average = 17.9868
Accepted packet length average = 17.9844
Total in-flight flits = 122079 (100172 measured)
latency change    = 0.207095
throughput change = 0.0121377
Class 0:
Packet latency average = 3431.87
	minimum = 23
	maximum = 6485
Network latency average = 2236.96
	minimum = 23
	maximum = 4930
Slowest packet = 12469
Flit latency average = 2792.78
	minimum = 6
	maximum = 6765
Slowest flit = 102282
Fragmentation average = 65.92
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00918125
	minimum = 0.0024 (at node 163)
	maximum = 0.021 (at node 170)
Accepted packet rate average = 0.00903437
	minimum = 0.0056 (at node 2)
	maximum = 0.0124 (at node 182)
Injected flit rate average = 0.16517
	minimum = 0.041 (at node 163)
	maximum = 0.3764 (at node 170)
Accepted flit rate average= 0.162429
	minimum = 0.1012 (at node 2)
	maximum = 0.2204 (at node 182)
Injected packet length average = 17.9899
Accepted packet length average = 17.979
Total in-flight flits = 123898 (112141 measured)
latency change    = 0.167023
throughput change = 0.00586313
Class 0:
Packet latency average = 3951.37
	minimum = 23
	maximum = 7354
Network latency average = 2577.31
	minimum = 23
	maximum = 5992
Slowest packet = 12469
Flit latency average = 2941.75
	minimum = 6
	maximum = 7219
Slowest flit = 142890
Fragmentation average = 68.3271
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00911024
	minimum = 0.00316667 (at node 163)
	maximum = 0.0203333 (at node 170)
Accepted packet rate average = 0.00903038
	minimum = 0.00583333 (at node 36)
	maximum = 0.0121667 (at node 177)
Injected flit rate average = 0.164038
	minimum = 0.057 (at node 163)
	maximum = 0.3635 (at node 170)
Accepted flit rate average= 0.162464
	minimum = 0.105 (at node 36)
	maximum = 0.215 (at node 177)
Injected packet length average = 18.0059
Accepted packet length average = 17.9908
Total in-flight flits = 123200 (117362 measured)
latency change    = 0.131472
throughput change = 0.000211586
Class 0:
Packet latency average = 4417.05
	minimum = 23
	maximum = 8029
Network latency average = 2813.76
	minimum = 23
	maximum = 6852
Slowest packet = 12469
Flit latency average = 3056.29
	minimum = 6
	maximum = 8279
Slowest flit = 130067
Fragmentation average = 70.023
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00896354
	minimum = 0.00285714 (at node 11)
	maximum = 0.019 (at node 170)
Accepted packet rate average = 0.00901042
	minimum = 0.00571429 (at node 36)
	maximum = 0.0114286 (at node 128)
Injected flit rate average = 0.16138
	minimum = 0.0514286 (at node 11)
	maximum = 0.342 (at node 170)
Accepted flit rate average= 0.16215
	minimum = 0.102857 (at node 36)
	maximum = 0.205286 (at node 128)
Injected packet length average = 18.0041
Accepted packet length average = 17.9959
Total in-flight flits = 120256 (117737 measured)
latency change    = 0.105428
throughput change = 0.00193181
Draining all recorded packets ...
Class 0:
Remaining flits: 151957 151958 151959 151960 151961 151962 151963 151964 151965 151966 [...] (121955 flits)
Measured flits: 224352 224353 224354 224355 224356 224357 224358 224359 224360 224361 [...] (120840 flits)
Class 0:
Remaining flits: 162194 162195 162196 162197 168444 168445 168446 168447 168448 168449 [...] (122961 flits)
Measured flits: 227016 227017 227018 227019 227020 227021 227022 227023 227024 227025 [...] (122607 flits)
Class 0:
Remaining flits: 182900 182901 182902 182903 182904 182905 182906 182907 182908 182909 [...] (122460 flits)
Measured flits: 228672 228673 228674 228675 228676 228677 228678 228679 228680 228681 [...] (122335 flits)
Class 0:
Remaining flits: 228672 228673 228674 228675 228676 228677 228678 228679 228680 228681 [...] (120555 flits)
Measured flits: 228672 228673 228674 228675 228676 228677 228678 228679 228680 228681 [...] (120357 flits)
Class 0:
Remaining flits: 231102 231103 231104 231105 231106 231107 231108 231109 231110 231111 [...] (119722 flits)
Measured flits: 231102 231103 231104 231105 231106 231107 231108 231109 231110 231111 [...] (119362 flits)
Class 0:
Remaining flits: 244674 244675 244676 244677 244678 244679 244680 244681 244682 244683 [...] (120153 flits)
Measured flits: 244674 244675 244676 244677 244678 244679 244680 244681 244682 244683 [...] (119289 flits)
Class 0:
Remaining flits: 244674 244675 244676 244677 244678 244679 244680 244681 244682 244683 [...] (119463 flits)
Measured flits: 244674 244675 244676 244677 244678 244679 244680 244681 244682 244683 [...] (117608 flits)
Class 0:
Remaining flits: 352836 352837 352838 352839 352840 352841 352842 352843 352844 352845 [...] (121198 flits)
Measured flits: 352836 352837 352838 352839 352840 352841 352842 352843 352844 352845 [...] (117202 flits)
Class 0:
Remaining flits: 375012 375013 375014 375015 375016 375017 375018 375019 375020 375021 [...] (122464 flits)
Measured flits: 375012 375013 375014 375015 375016 375017 375018 375019 375020 375021 [...] (115583 flits)
Class 0:
Remaining flits: 381546 381547 381548 381549 381550 381551 381552 381553 381554 381555 [...] (122263 flits)
Measured flits: 381546 381547 381548 381549 381550 381551 381552 381553 381554 381555 [...] (110356 flits)
Class 0:
Remaining flits: 384858 384859 384860 384861 384862 384863 384864 384865 384866 384867 [...] (121314 flits)
Measured flits: 384858 384859 384860 384861 384862 384863 384864 384865 384866 384867 [...] (102491 flits)
Class 0:
Remaining flits: 398340 398341 398342 398343 398344 398345 398346 398347 398348 398349 [...] (120368 flits)
Measured flits: 398340 398341 398342 398343 398344 398345 398346 398347 398348 398349 [...] (92890 flits)
Class 0:
Remaining flits: 398340 398341 398342 398343 398344 398345 398346 398347 398348 398349 [...] (118932 flits)
Measured flits: 398340 398341 398342 398343 398344 398345 398346 398347 398348 398349 [...] (81925 flits)
Class 0:
Remaining flits: 485622 485623 485624 485625 485626 485627 485628 485629 485630 485631 [...] (119443 flits)
Measured flits: 485622 485623 485624 485625 485626 485627 485628 485629 485630 485631 [...] (72215 flits)
Class 0:
Remaining flits: 485622 485623 485624 485625 485626 485627 485628 485629 485630 485631 [...] (118277 flits)
Measured flits: 485622 485623 485624 485625 485626 485627 485628 485629 485630 485631 [...] (61843 flits)
Class 0:
Remaining flits: 567900 567901 567902 567903 567904 567905 567906 567907 567908 567909 [...] (119804 flits)
Measured flits: 567900 567901 567902 567903 567904 567905 567906 567907 567908 567909 [...] (50374 flits)
Class 0:
Remaining flits: 594036 594037 594038 594039 594040 594041 594042 594043 594044 594045 [...] (118252 flits)
Measured flits: 594036 594037 594038 594039 594040 594041 594042 594043 594044 594045 [...] (40639 flits)
Class 0:
Remaining flits: 596124 596125 596126 596127 596128 596129 596130 596131 596132 596133 [...] (119116 flits)
Measured flits: 596124 596125 596126 596127 596128 596129 596130 596131 596132 596133 [...] (32073 flits)
Class 0:
Remaining flits: 678377 678378 678379 678380 678381 678382 678383 680778 680779 680780 [...] (120261 flits)
Measured flits: 678377 678378 678379 678380 678381 678382 678383 680778 680779 680780 [...] (24991 flits)
Class 0:
Remaining flits: 680778 680779 680780 680781 680782 680783 680784 680785 680786 680787 [...] (119300 flits)
Measured flits: 680778 680779 680780 680781 680782 680783 680784 680785 680786 680787 [...] (18701 flits)
Class 0:
Remaining flits: 705024 705025 705026 705027 705028 705029 705030 705031 705032 705033 [...] (118671 flits)
Measured flits: 708365 708366 708367 708368 708369 708370 708371 735210 735211 735212 [...] (13938 flits)
Class 0:
Remaining flits: 726156 726157 726158 726159 726160 726161 726162 726163 726164 726165 [...] (118359 flits)
Measured flits: 775242 775243 775244 775245 775246 775247 775248 775249 775250 775251 [...] (10152 flits)
Class 0:
Remaining flits: 775242 775243 775244 775245 775246 775247 775248 775249 775250 775251 [...] (119572 flits)
Measured flits: 775242 775243 775244 775245 775246 775247 775248 775249 775250 775251 [...] (7458 flits)
Class 0:
Remaining flits: 797040 797041 797042 797043 797044 797045 797046 797047 797048 797049 [...] (118109 flits)
Measured flits: 797040 797041 797042 797043 797044 797045 797046 797047 797048 797049 [...] (5591 flits)
Class 0:
Remaining flits: 834408 834409 834410 834411 834412 834413 834414 834415 834416 834417 [...] (118816 flits)
Measured flits: 834408 834409 834410 834411 834412 834413 834414 834415 834416 834417 [...] (3702 flits)
Class 0:
Remaining flits: 834408 834409 834410 834411 834412 834413 834414 834415 834416 834417 [...] (120451 flits)
Measured flits: 834408 834409 834410 834411 834412 834413 834414 834415 834416 834417 [...] (2403 flits)
Class 0:
Remaining flits: 892152 892153 892154 892155 892156 892157 892158 892159 892160 892161 [...] (120716 flits)
Measured flits: 892152 892153 892154 892155 892156 892157 892158 892159 892160 892161 [...] (1533 flits)
Class 0:
Remaining flits: 979650 979651 979652 979653 979654 979655 979656 979657 979658 979659 [...] (118744 flits)
Measured flits: 979650 979651 979652 979653 979654 979655 979656 979657 979658 979659 [...] (595 flits)
Class 0:
Remaining flits: 1001718 1001719 1001720 1001721 1001722 1001723 1001724 1001725 1001726 1001727 [...] (119637 flits)
Measured flits: 1029798 1029799 1029800 1029801 1029802 1029803 1029804 1029805 1029806 1029807 [...] (72 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1009297 1009298 1009299 1009300 1009301 1009302 1009303 1009304 1009305 1009306 [...] (87863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1011348 1011349 1011350 1011351 1011352 1011353 1011354 1011355 1011356 1011357 [...] (58501 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1082682 1082683 1082684 1082685 1082686 1082687 1082688 1082689 1082690 1082691 [...] (30881 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1157364 1157365 1157366 1157367 1157368 1157369 1157370 1157371 1157372 1157373 [...] (8109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1246248 1246249 1246250 1246251 1246252 1246253 1246254 1246255 1246256 1246257 [...] (867 flits)
Measured flits: (0 flits)
Time taken is 45172 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11087.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 30037 (1 samples)
Network latency average = 3865.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15446 (1 samples)
Flit latency average = 3747.79 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15429 (1 samples)
Fragmentation average = 71.7207 (1 samples)
	minimum = 0 (1 samples)
	maximum = 175 (1 samples)
Injected packet rate average = 0.00896354 (1 samples)
	minimum = 0.00285714 (1 samples)
	maximum = 0.019 (1 samples)
Accepted packet rate average = 0.00901042 (1 samples)
	minimum = 0.00571429 (1 samples)
	maximum = 0.0114286 (1 samples)
Injected flit rate average = 0.16138 (1 samples)
	minimum = 0.0514286 (1 samples)
	maximum = 0.342 (1 samples)
Accepted flit rate average = 0.16215 (1 samples)
	minimum = 0.102857 (1 samples)
	maximum = 0.205286 (1 samples)
Injected packet size average = 18.0041 (1 samples)
Accepted packet size average = 17.9959 (1 samples)
Hops average = 5.05122 (1 samples)
Total run time 35.7816
