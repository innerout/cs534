BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 376.585
	minimum = 23
	maximum = 990
Network latency average = 273.653
	minimum = 23
	maximum = 922
Slowest packet = 46
Flit latency average = 203.963
	minimum = 6
	maximum = 958
Slowest flit = 3264
Fragmentation average = 159.95
	minimum = 0
	maximum = 867
Injected packet rate average = 0.0297552
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0102292
	minimum = 0.004 (at node 25)
	maximum = 0.017 (at node 154)
Injected flit rate average = 0.53049
	minimum = 0 (at node 10)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.218182
	minimum = 0.101 (at node 79)
	maximum = 0.362 (at node 22)
Injected packet length average = 17.8285
Accepted packet length average = 21.3294
Total in-flight flits = 60943 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 633.904
	minimum = 23
	maximum = 1951
Network latency average = 478.571
	minimum = 23
	maximum = 1788
Slowest packet = 160
Flit latency average = 391.507
	minimum = 6
	maximum = 1882
Slowest flit = 10121
Fragmentation average = 226.599
	minimum = 0
	maximum = 1538
Injected packet rate average = 0.0313932
	minimum = 0.0015 (at node 191)
	maximum = 0.056 (at node 87)
Accepted packet rate average = 0.0116901
	minimum = 0.007 (at node 81)
	maximum = 0.0185 (at node 34)
Injected flit rate average = 0.562104
	minimum = 0.027 (at node 191)
	maximum = 1 (at node 87)
Accepted flit rate average= 0.232758
	minimum = 0.136 (at node 153)
	maximum = 0.3715 (at node 63)
Injected packet length average = 17.9053
Accepted packet length average = 19.9107
Total in-flight flits = 127611 (0 measured)
latency change    = 0.405928
throughput change = 0.062621
Class 0:
Packet latency average = 1353.22
	minimum = 28
	maximum = 2960
Network latency average = 1106.1
	minimum = 26
	maximum = 2862
Slowest packet = 413
Flit latency average = 1021.77
	minimum = 6
	maximum = 2869
Slowest flit = 9718
Fragmentation average = 341.96
	minimum = 1
	maximum = 2124
Injected packet rate average = 0.0321146
	minimum = 0 (at node 178)
	maximum = 0.056 (at node 26)
Accepted packet rate average = 0.0134271
	minimum = 0.005 (at node 8)
	maximum = 0.028 (at node 129)
Injected flit rate average = 0.578635
	minimum = 0 (at node 178)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.247047
	minimum = 0.119 (at node 36)
	maximum = 0.479 (at node 129)
Injected packet length average = 18.0178
Accepted packet length average = 18.3991
Total in-flight flits = 191166 (0 measured)
latency change    = 0.531558
throughput change = 0.0578395
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 510.522
	minimum = 29
	maximum = 2330
Network latency average = 143.233
	minimum = 25
	maximum = 912
Slowest packet = 18307
Flit latency average = 1504.11
	minimum = 6
	maximum = 3806
Slowest flit = 15256
Fragmentation average = 58.0628
	minimum = 1
	maximum = 717
Injected packet rate average = 0.0315521
	minimum = 0 (at node 70)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0134375
	minimum = 0.005 (at node 132)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.567609
	minimum = 0 (at node 70)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.243714
	minimum = 0.087 (at node 149)
	maximum = 0.464 (at node 19)
Injected packet length average = 17.9896
Accepted packet length average = 18.1368
Total in-flight flits = 253417 (100082 measured)
latency change    = 1.65065
throughput change = 0.0136773
Class 0:
Packet latency average = 763.194
	minimum = 29
	maximum = 3614
Network latency average = 397.817
	minimum = 25
	maximum = 1990
Slowest packet = 18307
Flit latency average = 1790.6
	minimum = 6
	maximum = 4788
Slowest flit = 10596
Fragmentation average = 121.844
	minimum = 0
	maximum = 1352
Injected packet rate average = 0.0311484
	minimum = 0.0065 (at node 131)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0133073
	minimum = 0.0055 (at node 149)
	maximum = 0.022 (at node 19)
Injected flit rate average = 0.560477
	minimum = 0.117 (at node 131)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.241789
	minimum = 0.095 (at node 149)
	maximum = 0.3825 (at node 3)
Injected packet length average = 17.9937
Accepted packet length average = 18.1697
Total in-flight flits = 313635 (192952 measured)
latency change    = 0.331071
throughput change = 0.00795933
Class 0:
Packet latency average = 1148.08
	minimum = 29
	maximum = 4708
Network latency average = 753.839
	minimum = 25
	maximum = 2914
Slowest packet = 18307
Flit latency average = 2087.26
	minimum = 6
	maximum = 5751
Slowest flit = 21623
Fragmentation average = 174.783
	minimum = 0
	maximum = 2313
Injected packet rate average = 0.0305486
	minimum = 0.01 (at node 38)
	maximum = 0.0556667 (at node 58)
Accepted packet rate average = 0.0132726
	minimum = 0.008 (at node 171)
	maximum = 0.0186667 (at node 44)
Injected flit rate average = 0.549672
	minimum = 0.184667 (at node 38)
	maximum = 1 (at node 58)
Accepted flit rate average= 0.239842
	minimum = 0.148333 (at node 171)
	maximum = 0.348 (at node 50)
Injected packet length average = 17.9934
Accepted packet length average = 18.0705
Total in-flight flits = 369745 (277538 measured)
latency change    = 0.335241
throughput change = 0.00811805
Draining remaining packets ...
Class 0:
Remaining flits: 14490 14491 14492 14493 14494 14495 14496 14497 14498 14499 [...] (328460 flits)
Measured flits: 327996 327997 327998 327999 328000 328001 328002 328003 328004 328005 [...] (259862 flits)
Class 0:
Remaining flits: 14491 14492 14493 14494 14495 14496 14497 14498 14499 14500 [...] (288455 flits)
Measured flits: 327996 327997 327998 327999 328000 328001 328002 328003 328004 328005 [...] (239723 flits)
Class 0:
Remaining flits: 14507 19511 24750 24751 24752 24753 24754 24755 24756 24757 [...] (249601 flits)
Measured flits: 328028 328029 328030 328031 328032 328033 328034 328035 328036 328037 [...] (213624 flits)
Class 0:
Remaining flits: 14507 19511 24750 24751 24752 24753 24754 24755 24756 24757 [...] (211015 flits)
Measured flits: 328032 328033 328034 328035 328036 328037 328038 328039 328040 328041 [...] (183949 flits)
Class 0:
Remaining flits: 14507 24750 24751 24752 24753 24754 24755 24756 24757 24758 [...] (174111 flits)
Measured flits: 328032 328033 328034 328035 328036 328037 328038 328039 328040 328041 [...] (153503 flits)
Class 0:
Remaining flits: 24750 24751 24752 24753 24754 24755 24756 24757 24758 24759 [...] (138536 flits)
Measured flits: 328230 328231 328232 328233 328234 328235 328236 328237 328238 328239 [...] (122718 flits)
Class 0:
Remaining flits: 24750 24751 24752 24753 24754 24755 24756 24757 24758 24759 [...] (105839 flits)
Measured flits: 328230 328231 328232 328233 328234 328235 328236 328237 328238 328239 [...] (94064 flits)
Class 0:
Remaining flits: 35982 35983 35984 35985 35986 35987 35988 35989 35990 35991 [...] (75632 flits)
Measured flits: 328230 328231 328232 328233 328234 328235 328236 328237 328238 328239 [...] (67361 flits)
Class 0:
Remaining flits: 35982 35983 35984 35985 35986 35987 35988 35989 35990 35991 [...] (48848 flits)
Measured flits: 328230 328231 328232 328233 328234 328235 328236 328237 328238 328239 [...] (43440 flits)
Class 0:
Remaining flits: 35982 35983 35984 35985 35986 35987 35988 35989 35990 35991 [...] (26166 flits)
Measured flits: 328230 328231 328232 328233 328234 328235 328236 328237 328238 328239 [...] (23075 flits)
Class 0:
Remaining flits: 35982 35983 35984 35985 35986 35987 35988 35989 35990 35991 [...] (10588 flits)
Measured flits: 328500 328501 328502 328503 328504 328505 328506 328507 328508 328509 [...] (9281 flits)
Class 0:
Remaining flits: 82458 82459 82460 82461 82462 82463 82464 82465 82466 82467 [...] (4248 flits)
Measured flits: 328500 328501 328502 328503 328504 328505 328506 328507 328508 328509 [...] (3751 flits)
Class 0:
Remaining flits: 94230 94231 94232 94233 94234 94235 94236 94237 94238 94239 [...] (925 flits)
Measured flits: 329454 329455 329456 329457 329458 329459 329460 329461 329462 329463 [...] (847 flits)
Time taken is 19558 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6938.94 (1 samples)
	minimum = 29 (1 samples)
	maximum = 16915 (1 samples)
Network latency average = 6423.43 (1 samples)
	minimum = 25 (1 samples)
	maximum = 16442 (1 samples)
Flit latency average = 5683.05 (1 samples)
	minimum = 6 (1 samples)
	maximum = 18318 (1 samples)
Fragmentation average = 285.554 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8684 (1 samples)
Injected packet rate average = 0.0305486 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0132726 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.549672 (1 samples)
	minimum = 0.184667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.239842 (1 samples)
	minimum = 0.148333 (1 samples)
	maximum = 0.348 (1 samples)
Injected packet size average = 17.9934 (1 samples)
Accepted packet size average = 18.0705 (1 samples)
Hops average = 5.18612 (1 samples)
Total run time 27.3863
