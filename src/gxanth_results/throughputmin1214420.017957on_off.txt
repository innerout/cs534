BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 228.216
	minimum = 24
	maximum = 889
Network latency average = 163.047
	minimum = 22
	maximum = 717
Slowest packet = 21
Flit latency average = 140.004
	minimum = 5
	maximum = 728
Slowest flit = 16573
Fragmentation average = 15.683
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0123229
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22688
	minimum = 0.108 (at node 41)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.4112
Total in-flight flits = 18449 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 352.27
	minimum = 24
	maximum = 1822
Network latency average = 269.94
	minimum = 22
	maximum = 1304
Slowest packet = 21
Flit latency average = 245.882
	minimum = 5
	maximum = 1287
Slowest flit = 39797
Fragmentation average = 16.9025
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0180104
	minimum = 0.0005 (at node 169)
	maximum = 0.0445 (at node 41)
Accepted packet rate average = 0.0132682
	minimum = 0.0075 (at node 62)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.322594
	minimum = 0.009 (at node 169)
	maximum = 0.799 (at node 41)
Accepted flit rate average= 0.241273
	minimum = 0.135 (at node 62)
	maximum = 0.351 (at node 22)
Injected packet length average = 17.9115
Accepted packet length average = 18.1843
Total in-flight flits = 31893 (0 measured)
latency change    = 0.352155
throughput change = 0.0596553
Class 0:
Packet latency average = 697.443
	minimum = 22
	maximum = 2409
Network latency average = 581.847
	minimum = 22
	maximum = 1831
Slowest packet = 2410
Flit latency average = 559.838
	minimum = 5
	maximum = 1830
Slowest flit = 71892
Fragmentation average = 18.4315
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0179271
	minimum = 0 (at node 54)
	maximum = 0.055 (at node 27)
Accepted packet rate average = 0.0143646
	minimum = 0.007 (at node 31)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.323219
	minimum = 0 (at node 54)
	maximum = 1 (at node 27)
Accepted flit rate average= 0.258396
	minimum = 0.126 (at node 190)
	maximum = 0.464 (at node 34)
Injected packet length average = 18.0296
Accepted packet length average = 17.9884
Total in-flight flits = 44273 (0 measured)
latency change    = 0.494912
throughput change = 0.0662642
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 421.05
	minimum = 22
	maximum = 1132
Network latency average = 337.367
	minimum = 22
	maximum = 962
Slowest packet = 10390
Flit latency average = 750.182
	minimum = 5
	maximum = 2641
Slowest flit = 83009
Fragmentation average = 15.1531
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0175104
	minimum = 0 (at node 9)
	maximum = 0.052 (at node 130)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 17)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.315078
	minimum = 0 (at node 9)
	maximum = 0.943 (at node 130)
Accepted flit rate average= 0.264448
	minimum = 0.108 (at node 17)
	maximum = 0.481 (at node 178)
Injected packet length average = 17.9938
Accepted packet length average = 18.0114
Total in-flight flits = 53961 (43619 measured)
latency change    = 0.65644
throughput change = 0.0228857
Class 0:
Packet latency average = 749.67
	minimum = 22
	maximum = 2621
Network latency average = 654.106
	minimum = 22
	maximum = 1940
Slowest packet = 10390
Flit latency average = 830.472
	minimum = 5
	maximum = 3099
Slowest flit = 84311
Fragmentation average = 16.4543
	minimum = 0
	maximum = 103
Injected packet rate average = 0.016875
	minimum = 0 (at node 12)
	maximum = 0.0445 (at node 55)
Accepted packet rate average = 0.0146745
	minimum = 0.009 (at node 71)
	maximum = 0.022 (at node 157)
Injected flit rate average = 0.303773
	minimum = 0 (at node 12)
	maximum = 0.8005 (at node 55)
Accepted flit rate average= 0.26456
	minimum = 0.167 (at node 71)
	maximum = 0.396 (at node 157)
Injected packet length average = 18.0014
Accepted packet length average = 18.0286
Total in-flight flits = 59394 (57393 measured)
latency change    = 0.438353
throughput change = 0.000423266
Class 0:
Packet latency average = 942.864
	minimum = 22
	maximum = 3467
Network latency average = 831.855
	minimum = 22
	maximum = 2926
Slowest packet = 10390
Flit latency average = 913.465
	minimum = 5
	maximum = 3264
Slowest flit = 159083
Fragmentation average = 17.434
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0173403
	minimum = 0.00266667 (at node 82)
	maximum = 0.0416667 (at node 110)
Accepted packet rate average = 0.0147101
	minimum = 0.00966667 (at node 36)
	maximum = 0.0213333 (at node 157)
Injected flit rate average = 0.312161
	minimum = 0.048 (at node 82)
	maximum = 0.75 (at node 110)
Accepted flit rate average= 0.264505
	minimum = 0.173667 (at node 36)
	maximum = 0.384 (at node 157)
Injected packet length average = 18.0021
Accepted packet length average = 17.9812
Total in-flight flits = 71738 (71582 measured)
latency change    = 0.204901
throughput change = 0.000206754
Draining remaining packets ...
Class 0:
Remaining flits: 204336 204337 204338 204339 204340 204341 204342 204343 204344 204345 [...] (25834 flits)
Measured flits: 204336 204337 204338 204339 204340 204341 204342 204343 204344 204345 [...] (25834 flits)
Class 0:
Remaining flits: 264852 264853 264854 264855 264856 264857 264858 264859 264860 264861 [...] (1938 flits)
Measured flits: 264852 264853 264854 264855 264856 264857 264858 264859 264860 264861 [...] (1938 flits)
Time taken is 8733 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1287.79 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5043 (1 samples)
Network latency average = 1139.55 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4021 (1 samples)
Flit latency average = 1126.64 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4004 (1 samples)
Fragmentation average = 16.9846 (1 samples)
	minimum = 0 (1 samples)
	maximum = 160 (1 samples)
Injected packet rate average = 0.0173403 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.0416667 (1 samples)
Accepted packet rate average = 0.0147101 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.312161 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.75 (1 samples)
Accepted flit rate average = 0.264505 (1 samples)
	minimum = 0.173667 (1 samples)
	maximum = 0.384 (1 samples)
Injected packet size average = 18.0021 (1 samples)
Accepted packet size average = 17.9812 (1 samples)
Hops average = 5.09359 (1 samples)
Total run time 8.2388
