BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 208.926
	minimum = 23
	maximum = 836
Network latency average = 153.944
	minimum = 22
	maximum = 645
Slowest packet = 63
Flit latency average = 123.544
	minimum = 5
	maximum = 642
Slowest flit = 20139
Fragmentation average = 29.6836
	minimum = 0
	maximum = 181
Injected packet rate average = 0.0149635
	minimum = 0 (at node 18)
	maximum = 0.049 (at node 37)
Accepted packet rate average = 0.0118177
	minimum = 0.004 (at node 115)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.267573
	minimum = 0 (at node 18)
	maximum = 0.879 (at node 37)
Accepted flit rate average= 0.218026
	minimum = 0.084 (at node 174)
	maximum = 0.363 (at node 70)
Injected packet length average = 17.8817
Accepted packet length average = 18.4491
Total in-flight flits = 9853 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 262.284
	minimum = 22
	maximum = 1414
Network latency average = 197.619
	minimum = 22
	maximum = 1190
Slowest packet = 1294
Flit latency average = 165.913
	minimum = 5
	maximum = 1173
Slowest flit = 23308
Fragmentation average = 31.4704
	minimum = 0
	maximum = 227
Injected packet rate average = 0.0136562
	minimum = 0.001 (at node 10)
	maximum = 0.034 (at node 37)
Accepted packet rate average = 0.011974
	minimum = 0.0065 (at node 116)
	maximum = 0.0185 (at node 166)
Injected flit rate average = 0.245003
	minimum = 0.018 (at node 10)
	maximum = 0.612 (at node 37)
Accepted flit rate average= 0.218852
	minimum = 0.117 (at node 116)
	maximum = 0.3415 (at node 166)
Injected packet length average = 17.9407
Accepted packet length average = 18.2773
Total in-flight flits = 10389 (0 measured)
latency change    = 0.203435
throughput change = 0.00377206
Class 0:
Packet latency average = 348.583
	minimum = 25
	maximum = 1839
Network latency average = 267.818
	minimum = 22
	maximum = 1411
Slowest packet = 3107
Flit latency average = 230.782
	minimum = 5
	maximum = 1381
Slowest flit = 70018
Fragmentation average = 35.9281
	minimum = 0
	maximum = 238
Injected packet rate average = 0.0134323
	minimum = 0 (at node 81)
	maximum = 0.041 (at node 185)
Accepted packet rate average = 0.0128281
	minimum = 0.005 (at node 180)
	maximum = 0.022 (at node 56)
Injected flit rate average = 0.241167
	minimum = 0 (at node 81)
	maximum = 0.738 (at node 185)
Accepted flit rate average= 0.230187
	minimum = 0.095 (at node 180)
	maximum = 0.399 (at node 34)
Injected packet length average = 17.9542
Accepted packet length average = 17.944
Total in-flight flits = 12633 (0 measured)
latency change    = 0.247569
throughput change = 0.0492465
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 320.329
	minimum = 22
	maximum = 2155
Network latency average = 225.525
	minimum = 22
	maximum = 944
Slowest packet = 7864
Flit latency average = 267.353
	minimum = 5
	maximum = 2203
Slowest flit = 85233
Fragmentation average = 30.3415
	minimum = 0
	maximum = 211
Injected packet rate average = 0.0139688
	minimum = 0 (at node 70)
	maximum = 0.042 (at node 161)
Accepted packet rate average = 0.0132448
	minimum = 0.005 (at node 17)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.251344
	minimum = 0 (at node 70)
	maximum = 0.758 (at node 161)
Accepted flit rate average= 0.239417
	minimum = 0.087 (at node 113)
	maximum = 0.461 (at node 16)
Injected packet length average = 17.9933
Accepted packet length average = 18.0763
Total in-flight flits = 14995 (14788 measured)
latency change    = 0.088203
throughput change = 0.0385486
Class 0:
Packet latency average = 400.631
	minimum = 22
	maximum = 2545
Network latency average = 289.889
	minimum = 22
	maximum = 1737
Slowest packet = 7864
Flit latency average = 286.368
	minimum = 5
	maximum = 2561
Slowest flit = 95831
Fragmentation average = 34.8979
	minimum = 0
	maximum = 231
Injected packet rate average = 0.0137057
	minimum = 0.0025 (at node 87)
	maximum = 0.032 (at node 180)
Accepted packet rate average = 0.0132135
	minimum = 0.0065 (at node 4)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.246367
	minimum = 0.045 (at node 87)
	maximum = 0.5765 (at node 180)
Accepted flit rate average= 0.238331
	minimum = 0.1205 (at node 113)
	maximum = 0.411 (at node 129)
Injected packet length average = 17.9755
Accepted packet length average = 18.0369
Total in-flight flits = 15902 (15902 measured)
latency change    = 0.20044
throughput change = 0.00455643
Class 0:
Packet latency average = 440.262
	minimum = 22
	maximum = 3153
Network latency average = 322.356
	minimum = 22
	maximum = 2274
Slowest packet = 7864
Flit latency average = 303.582
	minimum = 5
	maximum = 2561
Slowest flit = 95831
Fragmentation average = 36.1496
	minimum = 0
	maximum = 238
Injected packet rate average = 0.0135521
	minimum = 0.00233333 (at node 84)
	maximum = 0.0303333 (at node 141)
Accepted packet rate average = 0.0133264
	minimum = 0.00866667 (at node 86)
	maximum = 0.0196667 (at node 128)
Injected flit rate average = 0.243887
	minimum = 0.042 (at node 84)
	maximum = 0.543 (at node 141)
Accepted flit rate average= 0.239748
	minimum = 0.153333 (at node 86)
	maximum = 0.354667 (at node 128)
Injected packet length average = 17.9963
Accepted packet length average = 17.9905
Total in-flight flits = 15118 (15118 measured)
latency change    = 0.0900161
throughput change = 0.0059126
Class 0:
Packet latency average = 461.184
	minimum = 22
	maximum = 3570
Network latency average = 332.378
	minimum = 22
	maximum = 2415
Slowest packet = 7864
Flit latency average = 308.25
	minimum = 5
	maximum = 2561
Slowest flit = 95831
Fragmentation average = 36.7599
	minimum = 0
	maximum = 252
Injected packet rate average = 0.013526
	minimum = 0.00425 (at node 99)
	maximum = 0.03125 (at node 141)
Accepted packet rate average = 0.0132422
	minimum = 0.00925 (at node 64)
	maximum = 0.01875 (at node 128)
Injected flit rate average = 0.243395
	minimum = 0.0765 (at node 99)
	maximum = 0.5625 (at node 141)
Accepted flit rate average= 0.238523
	minimum = 0.1665 (at node 64)
	maximum = 0.3445 (at node 128)
Injected packet length average = 17.9945
Accepted packet length average = 18.0124
Total in-flight flits = 16629 (16629 measured)
latency change    = 0.0453659
throughput change = 0.00513504
Class 0:
Packet latency average = 480.987
	minimum = 22
	maximum = 3883
Network latency average = 341.188
	minimum = 22
	maximum = 2836
Slowest packet = 7864
Flit latency average = 313.456
	minimum = 5
	maximum = 2753
Slowest flit = 211302
Fragmentation average = 36.8815
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0134833
	minimum = 0.0046 (at node 99)
	maximum = 0.0282 (at node 90)
Accepted packet rate average = 0.0132458
	minimum = 0.0094 (at node 162)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.242573
	minimum = 0.081 (at node 99)
	maximum = 0.509 (at node 90)
Accepted flit rate average= 0.238844
	minimum = 0.1692 (at node 162)
	maximum = 0.324 (at node 128)
Injected packet length average = 17.9906
Accepted packet length average = 18.0316
Total in-flight flits = 16407 (16407 measured)
latency change    = 0.0411711
throughput change = 0.0013411
Class 0:
Packet latency average = 490.317
	minimum = 22
	maximum = 3883
Network latency average = 346.66
	minimum = 22
	maximum = 2836
Slowest packet = 7864
Flit latency average = 317.16
	minimum = 5
	maximum = 2753
Slowest flit = 211302
Fragmentation average = 37.578
	minimum = 0
	maximum = 270
Injected packet rate average = 0.0135451
	minimum = 0.0065 (at node 109)
	maximum = 0.0271667 (at node 90)
Accepted packet rate average = 0.0133012
	minimum = 0.0103333 (at node 64)
	maximum = 0.0178333 (at node 128)
Injected flit rate average = 0.243872
	minimum = 0.117 (at node 109)
	maximum = 0.490667 (at node 90)
Accepted flit rate average= 0.239648
	minimum = 0.186667 (at node 80)
	maximum = 0.321 (at node 128)
Injected packet length average = 18.0044
Accepted packet length average = 18.017
Total in-flight flits = 17485 (17485 measured)
latency change    = 0.0190297
throughput change = 0.00335417
Draining all recorded packets ...
Class 0:
Remaining flits: 381834 381835 381836 381837 381838 381839 381840 381841 381842 381843 [...] (19062 flits)
Measured flits: 381834 381835 381836 381837 381838 381839 381840 381841 381842 381843 [...] (2427 flits)
Class 0:
Remaining flits: 422136 422137 422138 422139 422140 422141 422142 422143 422144 422145 [...] (20046 flits)
Measured flits: 422136 422137 422138 422139 422140 422141 422142 422143 422144 422145 [...] (489 flits)
Class 0:
Remaining flits: 456782 456783 456784 456785 479736 479737 479738 479739 479740 479741 [...] (18379 flits)
Measured flits: 510480 510481 510482 510483 510484 510485 510486 510487 510488 510489 [...] (18 flits)
Draining remaining packets ...
Time taken is 13114 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 531.69 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3883 (1 samples)
Network latency average = 372.426 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2836 (1 samples)
Flit latency average = 349.193 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2753 (1 samples)
Fragmentation average = 38.7955 (1 samples)
	minimum = 0 (1 samples)
	maximum = 282 (1 samples)
Injected packet rate average = 0.0135451 (1 samples)
	minimum = 0.0065 (1 samples)
	maximum = 0.0271667 (1 samples)
Accepted packet rate average = 0.0133012 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0178333 (1 samples)
Injected flit rate average = 0.243872 (1 samples)
	minimum = 0.117 (1 samples)
	maximum = 0.490667 (1 samples)
Accepted flit rate average = 0.239648 (1 samples)
	minimum = 0.186667 (1 samples)
	maximum = 0.321 (1 samples)
Injected packet size average = 18.0044 (1 samples)
Accepted packet size average = 18.017 (1 samples)
Hops average = 5.07383 (1 samples)
Total run time 10.1892
