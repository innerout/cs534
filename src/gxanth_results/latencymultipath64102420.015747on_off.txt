BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.689
	minimum = 22
	maximum = 826
Network latency average = 151.455
	minimum = 22
	maximum = 701
Slowest packet = 63
Flit latency average = 123.98
	minimum = 5
	maximum = 684
Slowest flit = 14921
Fragmentation average = 25.1858
	minimum = 0
	maximum = 364
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117708
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.217625
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.4885
Total in-flight flits = 12810 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 303.73
	minimum = 22
	maximum = 1596
Network latency average = 233.233
	minimum = 22
	maximum = 1015
Slowest packet = 63
Flit latency average = 204.348
	minimum = 5
	maximum = 1036
Slowest flit = 50668
Fragmentation average = 29.9886
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125807
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.229474
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 70)
Injected packet length average = 17.9137
Accepted packet length average = 18.2401
Total in-flight flits = 19522 (0 measured)
latency change    = 0.29645
throughput change = 0.0516353
Class 0:
Packet latency average = 500.152
	minimum = 22
	maximum = 1673
Network latency average = 424.222
	minimum = 22
	maximum = 1443
Slowest packet = 2814
Flit latency average = 389.048
	minimum = 5
	maximum = 1426
Slowest flit = 81377
Fragmentation average = 35.683
	minimum = 0
	maximum = 375
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140156
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.253318
	minimum = 0.114 (at node 4)
	maximum = 0.419 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.074
Total in-flight flits = 26865 (0 measured)
latency change    = 0.392724
throughput change = 0.0941259
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.704
	minimum = 22
	maximum = 1178
Network latency average = 315.034
	minimum = 22
	maximum = 943
Slowest packet = 9091
Flit latency average = 507.216
	minimum = 5
	maximum = 1677
Slowest flit = 105947
Fragmentation average = 25.5262
	minimum = 0
	maximum = 204
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141927
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.254302
	minimum = 0.09 (at node 36)
	maximum = 0.516 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9178
Total in-flight flits = 29951 (27989 measured)
latency change    = 0.286716
throughput change = 0.00387089
Class 0:
Packet latency average = 580.149
	minimum = 22
	maximum = 2030
Network latency average = 501.16
	minimum = 22
	maximum = 1746
Slowest packet = 9091
Flit latency average = 550.839
	minimum = 5
	maximum = 2151
Slowest flit = 153984
Fragmentation average = 34.5995
	minimum = 0
	maximum = 399
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0141979
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.255698
	minimum = 0.153 (at node 36)
	maximum = 0.375 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0095
Total in-flight flits = 32807 (32773 measured)
latency change    = 0.329993
throughput change = 0.00545892
Class 0:
Packet latency average = 652.175
	minimum = 22
	maximum = 2341
Network latency average = 573.491
	minimum = 22
	maximum = 2015
Slowest packet = 9091
Flit latency average = 580.825
	minimum = 5
	maximum = 2186
Slowest flit = 153989
Fragmentation average = 34.5559
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143368
	minimum = 0.00833333 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257733
	minimum = 0.155667 (at node 36)
	maximum = 0.359667 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.977
Total in-flight flits = 34237 (34237 measured)
latency change    = 0.110439
throughput change = 0.0078947
Class 0:
Packet latency average = 691.431
	minimum = 22
	maximum = 2437
Network latency average = 610.912
	minimum = 22
	maximum = 2188
Slowest packet = 9091
Flit latency average = 602.142
	minimum = 5
	maximum = 2186
Slowest flit = 153989
Fragmentation average = 36.8193
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0150117
	minimum = 0.0055 (at node 35)
	maximum = 0.03025 (at node 158)
Accepted packet rate average = 0.0143294
	minimum = 0.0095 (at node 36)
	maximum = 0.01925 (at node 128)
Injected flit rate average = 0.27023
	minimum = 0.099 (at node 35)
	maximum = 0.54075 (at node 158)
Accepted flit rate average= 0.258016
	minimum = 0.175 (at node 36)
	maximum = 0.34575 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 18.006
Total in-flight flits = 36231 (36231 measured)
latency change    = 0.0567743
throughput change = 0.00109678
Class 0:
Packet latency average = 718.637
	minimum = 22
	maximum = 2437
Network latency average = 638.86
	minimum = 22
	maximum = 2319
Slowest packet = 9091
Flit latency average = 621.697
	minimum = 5
	maximum = 2302
Slowest flit = 271367
Fragmentation average = 37.0741
	minimum = 0
	maximum = 519
Injected packet rate average = 0.0151635
	minimum = 0.005 (at node 68)
	maximum = 0.0292 (at node 96)
Accepted packet rate average = 0.014349
	minimum = 0.0106 (at node 86)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.272958
	minimum = 0.09 (at node 68)
	maximum = 0.5256 (at node 96)
Accepted flit rate average= 0.258321
	minimum = 0.1908 (at node 86)
	maximum = 0.3296 (at node 128)
Injected packet length average = 18.001
Accepted packet length average = 18.0028
Total in-flight flits = 40903 (40903 measured)
latency change    = 0.0378588
throughput change = 0.00118151
Class 0:
Packet latency average = 746.407
	minimum = 22
	maximum = 2437
Network latency average = 667.43
	minimum = 22
	maximum = 2319
Slowest packet = 9091
Flit latency average = 644.734
	minimum = 5
	maximum = 2302
Slowest flit = 271367
Fragmentation average = 38.0589
	minimum = 0
	maximum = 519
Injected packet rate average = 0.0151467
	minimum = 0.0055 (at node 68)
	maximum = 0.0271667 (at node 96)
Accepted packet rate average = 0.0143837
	minimum = 0.0113333 (at node 149)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.272705
	minimum = 0.099 (at node 68)
	maximum = 0.489 (at node 96)
Accepted flit rate average= 0.258937
	minimum = 0.207833 (at node 149)
	maximum = 0.323833 (at node 128)
Injected packet length average = 18.0042
Accepted packet length average = 18.0022
Total in-flight flits = 42651 (42651 measured)
latency change    = 0.0372038
throughput change = 0.00238153
Class 0:
Packet latency average = 773.974
	minimum = 22
	maximum = 2800
Network latency average = 694.753
	minimum = 22
	maximum = 2720
Slowest packet = 20150
Flit latency average = 667.146
	minimum = 5
	maximum = 2703
Slowest flit = 365774
Fragmentation average = 38.5407
	minimum = 0
	maximum = 548
Injected packet rate average = 0.0152582
	minimum = 0.007 (at node 167)
	maximum = 0.0258571 (at node 2)
Accepted packet rate average = 0.0144204
	minimum = 0.0112857 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.274633
	minimum = 0.126 (at node 167)
	maximum = 0.466286 (at node 2)
Accepted flit rate average= 0.259613
	minimum = 0.205571 (at node 79)
	maximum = 0.324 (at node 138)
Injected packet length average = 17.9991
Accepted packet length average = 18.0032
Total in-flight flits = 47071 (47071 measured)
latency change    = 0.0356181
throughput change = 0.00260232
Draining all recorded packets ...
Class 0:
Remaining flits: 420070 420071 420072 420073 420074 420075 420076 420077 420078 420079 [...] (49857 flits)
Measured flits: 420070 420071 420072 420073 420074 420075 420076 420077 420078 420079 [...] (12591 flits)
Class 0:
Remaining flits: 505574 505575 505576 505577 505578 505579 505580 505581 505582 505583 [...] (51609 flits)
Measured flits: 505574 505575 505576 505577 505578 505579 505580 505581 505582 505583 [...] (1598 flits)
Class 0:
Remaining flits: 519927 519928 519929 520916 520917 520918 520919 522028 522029 522030 [...] (52616 flits)
Measured flits: 519927 519928 519929 520916 520917 520918 520919 522028 522029 522030 [...] (323 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 597456 597457 597458 597459 597460 597461 597462 597463 597464 597465 [...] (16011 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 663730 663731 664376 664377 664378 664379 664751 664752 664753 664754 [...] (2839 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 712350 712351 712352 712353 712354 712355 712356 712357 712358 712359 [...] (630 flits)
Measured flits: (0 flits)
Time taken is 17487 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 857.959 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3900 (1 samples)
Network latency average = 777.538 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3741 (1 samples)
Flit latency average = 833.648 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3956 (1 samples)
Fragmentation average = 41.4199 (1 samples)
	minimum = 0 (1 samples)
	maximum = 728 (1 samples)
Injected packet rate average = 0.0152582 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0258571 (1 samples)
Accepted packet rate average = 0.0144204 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.274633 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.466286 (1 samples)
Accepted flit rate average = 0.259613 (1 samples)
	minimum = 0.205571 (1 samples)
	maximum = 0.324 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0032 (1 samples)
Hops average = 5.06962 (1 samples)
Total run time 14.0497
