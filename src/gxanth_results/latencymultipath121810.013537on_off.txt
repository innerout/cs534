BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 255.574
	minimum = 23
	maximum = 953
Network latency average = 189.12
	minimum = 23
	maximum = 894
Slowest packet = 206
Flit latency average = 149.291
	minimum = 6
	maximum = 921
Slowest flit = 5206
Fragmentation average = 44.2003
	minimum = 0
	maximum = 110
Injected packet rate average = 0.0113333
	minimum = 0 (at node 42)
	maximum = 0.028 (at node 57)
Accepted packet rate average = 0.00827083
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 140)
Injected flit rate average = 0.201802
	minimum = 0 (at node 42)
	maximum = 0.504 (at node 57)
Accepted flit rate average= 0.155005
	minimum = 0.036 (at node 41)
	maximum = 0.295 (at node 140)
Injected packet length average = 17.8061
Accepted packet length average = 18.7412
Total in-flight flits = 10127 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 414.377
	minimum = 23
	maximum = 1941
Network latency average = 271.215
	minimum = 23
	maximum = 1588
Slowest packet = 342
Flit latency average = 224.679
	minimum = 6
	maximum = 1571
Slowest flit = 6173
Fragmentation average = 50.7118
	minimum = 0
	maximum = 181
Injected packet rate average = 0.0104687
	minimum = 0.0005 (at node 115)
	maximum = 0.023 (at node 147)
Accepted packet rate average = 0.00864844
	minimum = 0.004 (at node 164)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.187049
	minimum = 0.009 (at node 115)
	maximum = 0.413 (at node 147)
Accepted flit rate average= 0.158495
	minimum = 0.077 (at node 164)
	maximum = 0.2715 (at node 22)
Injected packet length average = 17.8674
Accepted packet length average = 18.3264
Total in-flight flits = 12542 (0 measured)
latency change    = 0.383234
throughput change = 0.022017
Class 0:
Packet latency average = 867.096
	minimum = 27
	maximum = 2549
Network latency average = 407.837
	minimum = 23
	maximum = 1840
Slowest packet = 3341
Flit latency average = 353.111
	minimum = 6
	maximum = 1806
Slowest flit = 21023
Fragmentation average = 54.4528
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00944792
	minimum = 0 (at node 20)
	maximum = 0.025 (at node 76)
Accepted packet rate average = 0.00883333
	minimum = 0.001 (at node 121)
	maximum = 0.016 (at node 6)
Injected flit rate average = 0.169464
	minimum = 0 (at node 20)
	maximum = 0.437 (at node 76)
Accepted flit rate average= 0.160703
	minimum = 0.018 (at node 121)
	maximum = 0.268 (at node 6)
Injected packet length average = 17.9366
Accepted packet length average = 18.1928
Total in-flight flits = 14645 (0 measured)
latency change    = 0.522109
throughput change = 0.0137417
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1084.24
	minimum = 23
	maximum = 3477
Network latency average = 304.401
	minimum = 23
	maximum = 953
Slowest packet = 5916
Flit latency average = 410.243
	minimum = 6
	maximum = 1895
Slowest flit = 64439
Fragmentation average = 54.4797
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00932812
	minimum = 0.001 (at node 0)
	maximum = 0.023 (at node 82)
Accepted packet rate average = 0.00897396
	minimum = 0.002 (at node 40)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.167672
	minimum = 0.013 (at node 85)
	maximum = 0.421 (at node 82)
Accepted flit rate average= 0.16125
	minimum = 0.021 (at node 40)
	maximum = 0.314 (at node 114)
Injected packet length average = 17.9749
Accepted packet length average = 17.9687
Total in-flight flits = 15941 (15129 measured)
latency change    = 0.200272
throughput change = 0.00339147
Class 0:
Packet latency average = 1349.25
	minimum = 23
	maximum = 4430
Network latency average = 405.259
	minimum = 23
	maximum = 1853
Slowest packet = 5916
Flit latency average = 416.816
	minimum = 6
	maximum = 2102
Slowest flit = 97253
Fragmentation average = 55.2116
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0093151
	minimum = 0.0015 (at node 89)
	maximum = 0.0185 (at node 43)
Accepted packet rate average = 0.00907552
	minimum = 0.004 (at node 190)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.167208
	minimum = 0.027 (at node 89)
	maximum = 0.333 (at node 43)
Accepted flit rate average= 0.163172
	minimum = 0.075 (at node 190)
	maximum = 0.2705 (at node 34)
Injected packet length average = 17.9502
Accepted packet length average = 17.9793
Total in-flight flits = 16373 (16337 measured)
latency change    = 0.196417
throughput change = 0.0117782
Class 0:
Packet latency average = 1550.99
	minimum = 23
	maximum = 4922
Network latency average = 448.675
	minimum = 23
	maximum = 2139
Slowest packet = 5916
Flit latency average = 429.572
	minimum = 6
	maximum = 2282
Slowest flit = 100835
Fragmentation average = 56.1901
	minimum = 0
	maximum = 132
Injected packet rate average = 0.00922049
	minimum = 0.00166667 (at node 89)
	maximum = 0.0166667 (at node 133)
Accepted packet rate average = 0.00908333
	minimum = 0.00533333 (at node 4)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.165724
	minimum = 0.03 (at node 89)
	maximum = 0.3 (at node 133)
Accepted flit rate average= 0.162948
	minimum = 0.096 (at node 4)
	maximum = 0.286667 (at node 16)
Injected packet length average = 17.9735
Accepted packet length average = 17.9392
Total in-flight flits = 16547 (16547 measured)
latency change    = 0.130068
throughput change = 0.00137442
Class 0:
Packet latency average = 1753.64
	minimum = 23
	maximum = 5483
Network latency average = 466.894
	minimum = 23
	maximum = 2211
Slowest packet = 5916
Flit latency average = 434.329
	minimum = 6
	maximum = 2282
Slowest flit = 100835
Fragmentation average = 55.9972
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0091263
	minimum = 0.0025 (at node 89)
	maximum = 0.01525 (at node 138)
Accepted packet rate average = 0.00902344
	minimum = 0.00475 (at node 4)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.164188
	minimum = 0.045 (at node 89)
	maximum = 0.2745 (at node 138)
Accepted flit rate average= 0.161975
	minimum = 0.0855 (at node 4)
	maximum = 0.244 (at node 16)
Injected packet length average = 17.9906
Accepted packet length average = 17.9505
Total in-flight flits = 16932 (16932 measured)
latency change    = 0.11556
throughput change = 0.00600497
Class 0:
Packet latency average = 1928.05
	minimum = 23
	maximum = 6357
Network latency average = 479.706
	minimum = 23
	maximum = 2425
Slowest packet = 5916
Flit latency average = 441.341
	minimum = 6
	maximum = 2390
Slowest flit = 168011
Fragmentation average = 56.3032
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00909271
	minimum = 0.004 (at node 44)
	maximum = 0.0148 (at node 21)
Accepted packet rate average = 0.00899479
	minimum = 0.006 (at node 4)
	maximum = 0.0134 (at node 16)
Injected flit rate average = 0.163645
	minimum = 0.072 (at node 89)
	maximum = 0.2646 (at node 21)
Accepted flit rate average= 0.16163
	minimum = 0.1102 (at node 4)
	maximum = 0.2386 (at node 16)
Injected packet length average = 17.9974
Accepted packet length average = 17.9693
Total in-flight flits = 16908 (16908 measured)
latency change    = 0.0904609
throughput change = 0.00213482
Class 0:
Packet latency average = 2096.41
	minimum = 23
	maximum = 7135
Network latency average = 491.326
	minimum = 23
	maximum = 2773
Slowest packet = 5916
Flit latency average = 447.442
	minimum = 6
	maximum = 2756
Slowest flit = 189647
Fragmentation average = 56.8182
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00905729
	minimum = 0.00483333 (at node 44)
	maximum = 0.0143333 (at node 21)
Accepted packet rate average = 0.00895052
	minimum = 0.00633333 (at node 4)
	maximum = 0.0126667 (at node 128)
Injected flit rate average = 0.162802
	minimum = 0.0873333 (at node 89)
	maximum = 0.258 (at node 21)
Accepted flit rate average= 0.161026
	minimum = 0.114167 (at node 4)
	maximum = 0.228 (at node 128)
Injected packet length average = 17.9747
Accepted packet length average = 17.9907
Total in-flight flits = 17171 (17171 measured)
latency change    = 0.0803074
throughput change = 0.00375198
Class 0:
Packet latency average = 2265.96
	minimum = 23
	maximum = 7858
Network latency average = 496.122
	minimum = 23
	maximum = 2773
Slowest packet = 5916
Flit latency average = 449.307
	minimum = 6
	maximum = 2756
Slowest flit = 189647
Fragmentation average = 56.8501
	minimum = 0
	maximum = 150
Injected packet rate average = 0.00907589
	minimum = 0.00528571 (at node 44)
	maximum = 0.0135714 (at node 21)
Accepted packet rate average = 0.00898884
	minimum = 0.00642857 (at node 36)
	maximum = 0.012 (at node 128)
Injected flit rate average = 0.163248
	minimum = 0.0967143 (at node 44)
	maximum = 0.243857 (at node 21)
Accepted flit rate average= 0.161601
	minimum = 0.115429 (at node 36)
	maximum = 0.216429 (at node 128)
Injected packet length average = 17.987
Accepted packet length average = 17.978
Total in-flight flits = 17341 (17341 measured)
latency change    = 0.0748241
throughput change = 0.00355906
Draining all recorded packets ...
Class 0:
Remaining flits: 292349 292350 292351 292352 292353 292354 292355 297144 297145 297146 [...] (17344 flits)
Measured flits: 292349 292350 292351 292352 292353 292354 292355 297144 297145 297146 [...] (15461 flits)
Class 0:
Remaining flits: 331344 331345 331346 331347 331348 331349 331350 331351 331352 331353 [...] (17467 flits)
Measured flits: 331344 331345 331346 331347 331348 331349 331350 331351 331352 331353 [...] (13768 flits)
Class 0:
Remaining flits: 357534 357535 357536 357537 357538 357539 357540 357541 357542 357543 [...] (17370 flits)
Measured flits: 357534 357535 357536 357537 357538 357539 357540 357541 357542 357543 [...] (11765 flits)
Class 0:
Remaining flits: 360882 360883 360884 360885 360886 360887 360888 360889 360890 360891 [...] (17463 flits)
Measured flits: 360882 360883 360884 360885 360886 360887 360888 360889 360890 360891 [...] (10337 flits)
Class 0:
Remaining flits: 367164 367165 367166 367167 367168 367169 367170 367171 367172 367173 [...] (17285 flits)
Measured flits: 367164 367165 367166 367167 367168 367169 367170 367171 367172 367173 [...] (7704 flits)
Class 0:
Remaining flits: 415985 415986 415987 415988 415989 415990 415991 415992 415993 415994 [...] (17632 flits)
Measured flits: 415985 415986 415987 415988 415989 415990 415991 415992 415993 415994 [...] (6691 flits)
Class 0:
Remaining flits: 426150 426151 426152 426153 426154 426155 426156 426157 426158 426159 [...] (17385 flits)
Measured flits: 426150 426151 426152 426153 426154 426155 426156 426157 426158 426159 [...] (5002 flits)
Class 0:
Remaining flits: 496656 496657 496658 496659 496660 496661 496662 496663 496664 496665 [...] (17480 flits)
Measured flits: 496656 496657 496658 496659 496660 496661 496662 496663 496664 496665 [...] (4023 flits)
Class 0:
Remaining flits: 500292 500293 500294 500295 500296 500297 500298 500299 500300 500301 [...] (17515 flits)
Measured flits: 545148 545149 545150 545151 545152 545153 545154 545155 545156 545157 [...] (2725 flits)
Class 0:
Remaining flits: 532458 532459 532460 532461 532462 532463 532464 532465 532466 532467 [...] (17573 flits)
Measured flits: 581490 581491 581492 581493 581494 581495 581496 581497 581498 581499 [...] (1637 flits)
Class 0:
Remaining flits: 540651 540652 540653 540654 540655 540656 540657 540658 540659 540660 [...] (17280 flits)
Measured flits: 621055 621056 621057 621058 621059 621060 621061 621062 621063 621064 [...] (1169 flits)
Class 0:
Remaining flits: 625563 625564 625565 625566 625567 625568 625569 625570 625571 627696 [...] (17392 flits)
Measured flits: 664740 664741 664742 664743 664744 664745 664746 664747 664748 664749 [...] (778 flits)
Class 0:
Remaining flits: 662219 664596 664597 664598 664599 664600 664601 664602 664603 664604 [...] (17764 flits)
Measured flits: 673594 673595 701406 701407 701408 701409 701410 701411 701412 701413 [...] (498 flits)
Class 0:
Remaining flits: 696618 696619 696620 696621 696622 696623 696624 696625 696626 696627 [...] (17699 flits)
Measured flits: 712818 712819 712820 712821 712822 712823 712824 712825 712826 712827 [...] (486 flits)
Class 0:
Remaining flits: 707490 707491 707492 707493 707494 707495 707496 707497 707498 707499 [...] (17785 flits)
Measured flits: 750264 750265 750266 750267 750268 750269 750270 750271 750272 750273 [...] (275 flits)
Class 0:
Remaining flits: 729756 729757 729758 729759 729760 729761 729762 729763 729764 729765 [...] (17628 flits)
Measured flits: 770142 770143 770144 770145 770146 770147 780210 780211 780212 780213 [...] (198 flits)
Class 0:
Remaining flits: 773856 773857 773858 773859 773860 773861 773862 773863 773864 773865 [...] (17164 flits)
Measured flits: 838434 838435 838436 838437 838438 838439 840267 840268 840269 840270 [...] (121 flits)
Class 0:
Remaining flits: 798084 798085 798086 798087 798088 798089 798090 798091 798092 798093 [...] (17773 flits)
Measured flits: 862542 862543 862544 862545 862546 862547 862548 862549 862550 862551 [...] (18 flits)
Draining remaining packets ...
Time taken is 29639 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3988.19 (1 samples)
	minimum = 23 (1 samples)
	maximum = 18969 (1 samples)
Network latency average = 527.174 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3972 (1 samples)
Flit latency average = 477.234 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3898 (1 samples)
Fragmentation average = 57.1505 (1 samples)
	minimum = 0 (1 samples)
	maximum = 158 (1 samples)
Injected packet rate average = 0.00907589 (1 samples)
	minimum = 0.00528571 (1 samples)
	maximum = 0.0135714 (1 samples)
Accepted packet rate average = 0.00898884 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.012 (1 samples)
Injected flit rate average = 0.163248 (1 samples)
	minimum = 0.0967143 (1 samples)
	maximum = 0.243857 (1 samples)
Accepted flit rate average = 0.161601 (1 samples)
	minimum = 0.115429 (1 samples)
	maximum = 0.216429 (1 samples)
Injected packet size average = 17.987 (1 samples)
Accepted packet size average = 17.978 (1 samples)
Hops average = 5.06984 (1 samples)
Total run time 20.7517
