BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 292.11
	minimum = 24
	maximum = 941
Network latency average = 279.547
	minimum = 24
	maximum = 941
Slowest packet = 145
Flit latency average = 208.635
	minimum = 6
	maximum = 929
Slowest flit = 4671
Fragmentation average = 149.774
	minimum = 0
	maximum = 867
Injected packet rate average = 0.0204271
	minimum = 0.007 (at node 160)
	maximum = 0.03 (at node 59)
Accepted packet rate average = 0.00993229
	minimum = 0.004 (at node 25)
	maximum = 0.018 (at node 2)
Injected flit rate average = 0.362068
	minimum = 0.113 (at node 160)
	maximum = 0.536 (at node 59)
Accepted flit rate average= 0.204266
	minimum = 0.084 (at node 93)
	maximum = 0.382 (at node 70)
Injected packet length average = 17.7249
Accepted packet length average = 20.5658
Total in-flight flits = 31575 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 548.954
	minimum = 24
	maximum = 1860
Network latency average = 481.299
	minimum = 24
	maximum = 1860
Slowest packet = 395
Flit latency average = 389.691
	minimum = 6
	maximum = 1921
Slowest flit = 4729
Fragmentation average = 183.96
	minimum = 0
	maximum = 1583
Injected packet rate average = 0.0163906
	minimum = 0.0035 (at node 160)
	maximum = 0.025 (at node 77)
Accepted packet rate average = 0.0105677
	minimum = 0.005 (at node 30)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.291638
	minimum = 0.0585 (at node 160)
	maximum = 0.4485 (at node 77)
Accepted flit rate average= 0.202177
	minimum = 0.102 (at node 30)
	maximum = 0.299 (at node 173)
Injected packet length average = 17.793
Accepted packet length average = 19.1316
Total in-flight flits = 35998 (0 measured)
latency change    = 0.46788
throughput change = 0.0103303
Class 0:
Packet latency average = 1296.43
	minimum = 30
	maximum = 2652
Network latency average = 911.962
	minimum = 25
	maximum = 2628
Slowest packet = 203
Flit latency average = 794.239
	minimum = 6
	maximum = 2750
Slowest flit = 18556
Fragmentation average = 210.901
	minimum = 0
	maximum = 2462
Injected packet rate average = 0.0110573
	minimum = 0 (at node 13)
	maximum = 0.027 (at node 74)
Accepted packet rate average = 0.0107969
	minimum = 0.004 (at node 45)
	maximum = 0.022 (at node 99)
Injected flit rate average = 0.198807
	minimum = 0 (at node 13)
	maximum = 0.498 (at node 107)
Accepted flit rate average= 0.193156
	minimum = 0.088 (at node 153)
	maximum = 0.382 (at node 99)
Injected packet length average = 17.9797
Accepted packet length average = 17.89
Total in-flight flits = 37162 (0 measured)
latency change    = 0.576565
throughput change = 0.0467023
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1752.45
	minimum = 94
	maximum = 3262
Network latency average = 354.556
	minimum = 25
	maximum = 952
Slowest packet = 8443
Flit latency average = 878.158
	minimum = 6
	maximum = 3737
Slowest flit = 18557
Fragmentation average = 127.741
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0105104
	minimum = 0 (at node 5)
	maximum = 0.032 (at node 82)
Accepted packet rate average = 0.0109635
	minimum = 0.003 (at node 132)
	maximum = 0.022 (at node 78)
Injected flit rate average = 0.189359
	minimum = 0 (at node 50)
	maximum = 0.576 (at node 82)
Accepted flit rate average= 0.196661
	minimum = 0.074 (at node 132)
	maximum = 0.411 (at node 78)
Injected packet length average = 18.0164
Accepted packet length average = 17.9378
Total in-flight flits = 35637 (24045 measured)
latency change    = 0.260221
throughput change = 0.0178236
Class 0:
Packet latency average = 2138.93
	minimum = 94
	maximum = 4145
Network latency average = 584.573
	minimum = 25
	maximum = 1968
Slowest packet = 8443
Flit latency average = 893.889
	minimum = 6
	maximum = 4610
Slowest flit = 31198
Fragmentation average = 154.012
	minimum = 0
	maximum = 1419
Injected packet rate average = 0.0105833
	minimum = 0 (at node 111)
	maximum = 0.0285 (at node 65)
Accepted packet rate average = 0.0107786
	minimum = 0.005 (at node 132)
	maximum = 0.0175 (at node 78)
Injected flit rate average = 0.190859
	minimum = 0 (at node 111)
	maximum = 0.5095 (at node 65)
Accepted flit rate average= 0.194865
	minimum = 0.103 (at node 132)
	maximum = 0.3095 (at node 78)
Injected packet length average = 18.034
Accepted packet length average = 18.0788
Total in-flight flits = 35540 (32176 measured)
latency change    = 0.180688
throughput change = 0.00922115
Class 0:
Packet latency average = 2537.9
	minimum = 94
	maximum = 5311
Network latency average = 717.751
	minimum = 25
	maximum = 2979
Slowest packet = 8443
Flit latency average = 892.805
	minimum = 6
	maximum = 5107
Slowest flit = 41903
Fragmentation average = 168.001
	minimum = 0
	maximum = 1696
Injected packet rate average = 0.010717
	minimum = 0.000666667 (at node 187)
	maximum = 0.0233333 (at node 65)
Accepted packet rate average = 0.0108264
	minimum = 0.00633333 (at node 4)
	maximum = 0.0153333 (at node 0)
Injected flit rate average = 0.192972
	minimum = 0.0133333 (at node 187)
	maximum = 0.416667 (at node 65)
Accepted flit rate average= 0.194271
	minimum = 0.115333 (at node 4)
	maximum = 0.28 (at node 129)
Injected packet length average = 18.0062
Accepted packet length average = 17.9442
Total in-flight flits = 36412 (35359 measured)
latency change    = 0.157203
throughput change = 0.0030563
Class 0:
Packet latency average = 2857.99
	minimum = 94
	maximum = 5773
Network latency average = 800.22
	minimum = 24
	maximum = 3965
Slowest packet = 8443
Flit latency average = 896.265
	minimum = 6
	maximum = 6358
Slowest flit = 36215
Fragmentation average = 176.245
	minimum = 0
	maximum = 2371
Injected packet rate average = 0.010681
	minimum = 0.00275 (at node 35)
	maximum = 0.021 (at node 129)
Accepted packet rate average = 0.0107656
	minimum = 0.007 (at node 84)
	maximum = 0.01475 (at node 129)
Injected flit rate average = 0.192374
	minimum = 0.04975 (at node 35)
	maximum = 0.3795 (at node 129)
Accepted flit rate average= 0.193167
	minimum = 0.122 (at node 84)
	maximum = 0.2625 (at node 129)
Injected packet length average = 18.0108
Accepted packet length average = 17.9429
Total in-flight flits = 36590 (36234 measured)
latency change    = 0.112
throughput change = 0.00571613
Class 0:
Packet latency average = 3166.27
	minimum = 94
	maximum = 7488
Network latency average = 865.247
	minimum = 24
	maximum = 4723
Slowest packet = 8443
Flit latency average = 907.228
	minimum = 6
	maximum = 6358
Slowest flit = 36215
Fragmentation average = 179.769
	minimum = 0
	maximum = 2659
Injected packet rate average = 0.0106885
	minimum = 0.0028 (at node 52)
	maximum = 0.0198 (at node 129)
Accepted packet rate average = 0.0106875
	minimum = 0.0074 (at node 135)
	maximum = 0.0148 (at node 128)
Injected flit rate average = 0.192491
	minimum = 0.0498 (at node 52)
	maximum = 0.3552 (at node 129)
Accepted flit rate average= 0.192328
	minimum = 0.133 (at node 135)
	maximum = 0.2674 (at node 128)
Injected packet length average = 18.0091
Accepted packet length average = 17.9956
Total in-flight flits = 37333 (37191 measured)
latency change    = 0.0973632
throughput change = 0.00435995
Class 0:
Packet latency average = 3476.46
	minimum = 94
	maximum = 7488
Network latency average = 905.501
	minimum = 24
	maximum = 5700
Slowest packet = 8443
Flit latency average = 910.665
	minimum = 6
	maximum = 7603
Slowest flit = 83249
Fragmentation average = 184.373
	minimum = 0
	maximum = 3132
Injected packet rate average = 0.010724
	minimum = 0.00416667 (at node 40)
	maximum = 0.0183333 (at node 129)
Accepted packet rate average = 0.010697
	minimum = 0.00783333 (at node 79)
	maximum = 0.0143333 (at node 129)
Injected flit rate average = 0.193079
	minimum = 0.0765 (at node 40)
	maximum = 0.329333 (at node 129)
Accepted flit rate average= 0.191991
	minimum = 0.1435 (at node 79)
	maximum = 0.257333 (at node 129)
Injected packet length average = 18.0045
Accepted packet length average = 17.9481
Total in-flight flits = 38540 (38493 measured)
latency change    = 0.0892258
throughput change = 0.00175427
Class 0:
Packet latency average = 3766.65
	minimum = 94
	maximum = 8635
Network latency average = 938.874
	minimum = 24
	maximum = 6393
Slowest packet = 8443
Flit latency average = 922.209
	minimum = 6
	maximum = 8002
Slowest flit = 91817
Fragmentation average = 185.1
	minimum = 0
	maximum = 3228
Injected packet rate average = 0.0106347
	minimum = 0.00371429 (at node 40)
	maximum = 0.0175714 (at node 65)
Accepted packet rate average = 0.0106332
	minimum = 0.00771429 (at node 64)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.191578
	minimum = 0.0658571 (at node 40)
	maximum = 0.316286 (at node 65)
Accepted flit rate average= 0.191216
	minimum = 0.139429 (at node 64)
	maximum = 0.251286 (at node 128)
Injected packet length average = 18.0145
Accepted packet length average = 17.9829
Total in-flight flits = 37766 (37761 measured)
latency change    = 0.0770412
throughput change = 0.00405587
Draining all recorded packets ...
Class 0:
Remaining flits: 178182 178183 178184 178185 178186 178187 178188 178189 178190 178191 [...] (37615 flits)
Measured flits: 178182 178183 178184 178185 178186 178187 178188 178189 178190 178191 [...] (37615 flits)
Class 0:
Remaining flits: 178199 199437 199438 199439 236628 236629 236630 236631 236632 236633 [...] (36940 flits)
Measured flits: 178199 199437 199438 199439 236628 236629 236630 236631 236632 236633 [...] (36940 flits)
Class 0:
Remaining flits: 236628 236629 236630 236631 236632 236633 236634 236635 236636 236637 [...] (36009 flits)
Measured flits: 236628 236629 236630 236631 236632 236633 236634 236635 236636 236637 [...] (36009 flits)
Class 0:
Remaining flits: 246382 246383 301660 301661 303024 303025 303026 303027 303028 303029 [...] (36923 flits)
Measured flits: 246382 246383 301660 301661 303024 303025 303026 303027 303028 303029 [...] (36923 flits)
Class 0:
Remaining flits: 342576 342577 342578 342579 342580 342581 342582 342583 342584 342585 [...] (34743 flits)
Measured flits: 342576 342577 342578 342579 342580 342581 342582 342583 342584 342585 [...] (34743 flits)
Class 0:
Remaining flits: 396340 396341 399825 399826 399827 399828 399829 399830 399831 399832 [...] (35712 flits)
Measured flits: 396340 396341 399825 399826 399827 399828 399829 399830 399831 399832 [...] (35181 flits)
Class 0:
Remaining flits: 408492 408493 408494 408495 408496 408497 408498 408499 408500 408501 [...] (35449 flits)
Measured flits: 408492 408493 408494 408495 408496 408497 408498 408499 408500 408501 [...] (33770 flits)
Class 0:
Remaining flits: 417312 417313 417314 417315 417316 417317 417318 417319 417320 417321 [...] (36240 flits)
Measured flits: 417312 417313 417314 417315 417316 417317 417318 417319 417320 417321 [...] (33012 flits)
Class 0:
Remaining flits: 417315 417316 417317 417318 417319 417320 417321 417322 417323 417324 [...] (37332 flits)
Measured flits: 417315 417316 417317 417318 417319 417320 417321 417322 417323 417324 [...] (31392 flits)
Class 0:
Remaining flits: 493667 511199 543833 552078 552079 552080 552081 552082 552083 552084 [...] (35427 flits)
Measured flits: 493667 511199 543833 552078 552079 552080 552081 552082 552083 552084 [...] (26679 flits)
Class 0:
Remaining flits: 552078 552079 552080 552081 552082 552083 552084 552085 552086 552087 [...] (35911 flits)
Measured flits: 552078 552079 552080 552081 552082 552083 552084 552085 552086 552087 [...] (22499 flits)
Class 0:
Remaining flits: 552094 552095 582642 582643 582644 582645 582646 582647 582648 582649 [...] (36860 flits)
Measured flits: 552094 552095 582642 582643 582644 582645 582646 582647 582648 582649 [...] (19135 flits)
Class 0:
Remaining flits: 582642 582643 582644 582645 582646 582647 582648 582649 582650 582651 [...] (35480 flits)
Measured flits: 582642 582643 582644 582645 582646 582647 582648 582649 582650 582651 [...] (14396 flits)
Class 0:
Remaining flits: 582643 582644 582645 582646 582647 582648 582649 582650 582651 582652 [...] (37649 flits)
Measured flits: 582643 582644 582645 582646 582647 582648 582649 582650 582651 582652 [...] (10978 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (37890 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (8619 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (36043 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (6054 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (37089 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (4987 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (37182 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (3382 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (37622 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (2709 flits)
Class 0:
Remaining flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (37038 flits)
Measured flits: 689868 689869 689870 689871 689872 689873 689874 689875 689876 689877 [...] (1661 flits)
Class 0:
Remaining flits: 689869 689870 689871 689872 689873 689874 689875 689876 689877 689878 [...] (37215 flits)
Measured flits: 689869 689870 689871 689872 689873 689874 689875 689876 689877 689878 [...] (1359 flits)
Class 0:
Remaining flits: 836838 836839 836840 836841 836842 836843 836844 836845 836846 836847 [...] (37242 flits)
Measured flits: 973171 973172 973173 973174 973175 973176 973177 973178 973179 973180 [...] (843 flits)
Class 0:
Remaining flits: 836838 836839 836840 836841 836842 836843 836844 836845 836846 836847 [...] (35849 flits)
Measured flits: 975888 975889 975890 975891 975892 975893 975894 975895 975896 975897 [...] (642 flits)
Class 0:
Remaining flits: 925974 925975 925976 925977 925978 925979 925980 925981 925982 925983 [...] (37456 flits)
Measured flits: 1012626 1012627 1012628 1012629 1012630 1012631 1012632 1012633 1012634 1012635 [...] (699 flits)
Class 0:
Remaining flits: 925974 925975 925976 925977 925978 925979 925980 925981 925982 925983 [...] (35921 flits)
Measured flits: 1012626 1012627 1012628 1012629 1012630 1012631 1012632 1012633 1012634 1012635 [...] (408 flits)
Class 0:
Remaining flits: 925974 925975 925976 925977 925978 925979 925980 925981 925982 925983 [...] (36777 flits)
Measured flits: 1012626 1012627 1012628 1012629 1012630 1012631 1012632 1012633 1012634 1012635 [...] (296 flits)
Class 0:
Remaining flits: 925974 925975 925976 925977 925978 925979 925980 925981 925982 925983 [...] (35124 flits)
Measured flits: 1012637 1012638 1012639 1012640 1012641 1012642 1012643 1247168 1247169 1247170 [...] (135 flits)
Class 0:
Remaining flits: 1067048 1067049 1067050 1067051 1067052 1067053 1067054 1067055 1067056 1067057 [...] (36183 flits)
Measured flits: 1369343 1369344 1369345 1369346 1369347 1369348 1369349 1383606 1383607 1383608 [...] (79 flits)
Class 0:
Remaining flits: 1072944 1072945 1072946 1072947 1072948 1072949 1072950 1072951 1072952 1072953 [...] (35479 flits)
Measured flits: 1383623 1389798 1389799 1389800 1389801 1389802 1389803 1389804 1389805 1389806 [...] (55 flits)
Class 0:
Remaining flits: 1072944 1072945 1072946 1072947 1072948 1072949 1072950 1072951 1072952 1072953 [...] (36166 flits)
Measured flits: 1474470 1474471 1474472 1474473 1474474 1474475 1474476 1474477 1474478 1474479 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1072944 1072945 1072946 1072947 1072948 1072949 1072950 1072951 1072952 1072953 [...] (6704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1456633 1456634 1456635 1456636 1456637 1456638 1456639 1456640 1456641 1456642 [...] (107 flits)
Measured flits: (0 flits)
Time taken is 42815 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8223.73 (1 samples)
	minimum = 94 (1 samples)
	maximum = 30893 (1 samples)
Network latency average = 1078.61 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13554 (1 samples)
Flit latency average = 966.525 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13973 (1 samples)
Fragmentation average = 204.035 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5509 (1 samples)
Injected packet rate average = 0.0106347 (1 samples)
	minimum = 0.00371429 (1 samples)
	maximum = 0.0175714 (1 samples)
Accepted packet rate average = 0.0106332 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.191578 (1 samples)
	minimum = 0.0658571 (1 samples)
	maximum = 0.316286 (1 samples)
Accepted flit rate average = 0.191216 (1 samples)
	minimum = 0.139429 (1 samples)
	maximum = 0.251286 (1 samples)
Injected packet size average = 18.0145 (1 samples)
Accepted packet size average = 17.9829 (1 samples)
Hops average = 5.05397 (1 samples)
Total run time 45.2403
