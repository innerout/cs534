BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 224.575
	minimum = 27
	maximum = 928
Network latency average = 180.389
	minimum = 23
	maximum = 801
Slowest packet = 72
Flit latency average = 141.175
	minimum = 6
	maximum = 820
Slowest flit = 8507
Fragmentation average = 49.7856
	minimum = 0
	maximum = 137
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00826042
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 165)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.155359
	minimum = 0.036 (at node 174)
	maximum = 0.288 (at node 165)
Injected packet length average = 17.8411
Accepted packet length average = 18.8077
Total in-flight flits = 11301 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 323.163
	minimum = 27
	maximum = 1566
Network latency average = 274.966
	minimum = 23
	maximum = 1347
Slowest packet = 672
Flit latency average = 231.708
	minimum = 6
	maximum = 1379
Slowest flit = 25141
Fragmentation average = 57.9249
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00869792
	minimum = 0.0045 (at node 115)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.160044
	minimum = 0.083 (at node 115)
	maximum = 0.252 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 18.4003
Total in-flight flits = 18139 (0 measured)
latency change    = 0.305072
throughput change = 0.0292725
Class 0:
Packet latency average = 566.768
	minimum = 23
	maximum = 2207
Network latency average = 518.34
	minimum = 23
	maximum = 1988
Slowest packet = 1396
Flit latency average = 471.834
	minimum = 6
	maximum = 2027
Slowest flit = 39799
Fragmentation average = 68.4002
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.00931771
	minimum = 0.003 (at node 18)
	maximum = 0.018 (at node 6)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.168052
	minimum = 0.054 (at node 18)
	maximum = 0.306 (at node 6)
Injected packet length average = 17.9936
Accepted packet length average = 18.0358
Total in-flight flits = 24969 (0 measured)
latency change    = 0.429815
throughput change = 0.0476508
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 360.079
	minimum = 23
	maximum = 1097
Network latency average = 313.008
	minimum = 23
	maximum = 956
Slowest packet = 6604
Flit latency average = 617.594
	minimum = 6
	maximum = 2919
Slowest flit = 41723
Fragmentation average = 62.1057
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.00957292
	minimum = 0.002 (at node 4)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.171859
	minimum = 0.048 (at node 163)
	maximum = 0.391 (at node 16)
Injected packet length average = 18.0247
Accepted packet length average = 17.9527
Total in-flight flits = 32724 (25708 measured)
latency change    = 0.57401
throughput change = 0.0221535
Class 0:
Packet latency average = 575.336
	minimum = 23
	maximum = 2097
Network latency average = 528.667
	minimum = 23
	maximum = 1931
Slowest packet = 6596
Flit latency average = 703.448
	minimum = 6
	maximum = 3425
Slowest flit = 62290
Fragmentation average = 65.9004
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.00948698
	minimum = 0.004 (at node 17)
	maximum = 0.0165 (at node 120)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.170836
	minimum = 0.07 (at node 17)
	maximum = 0.2935 (at node 120)
Injected packet length average = 17.9755
Accepted packet length average = 18.0074
Total in-flight flits = 39486 (37610 measured)
latency change    = 0.374142
throughput change = 0.00599076
Class 0:
Packet latency average = 750.278
	minimum = 23
	maximum = 2984
Network latency average = 701.122
	minimum = 23
	maximum = 2849
Slowest packet = 6596
Flit latency average = 784.25
	minimum = 6
	maximum = 4205
Slowest flit = 66185
Fragmentation average = 66.6152
	minimum = 0
	maximum = 156
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.00950347
	minimum = 0.00466667 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.171156
	minimum = 0.084 (at node 4)
	maximum = 0.248333 (at node 16)
Injected packet length average = 18.0002
Accepted packet length average = 18.0099
Total in-flight flits = 43293 (42556 measured)
latency change    = 0.23317
throughput change = 0.00187146
Class 0:
Packet latency average = 879.894
	minimum = 23
	maximum = 3937
Network latency average = 829.611
	minimum = 23
	maximum = 3757
Slowest packet = 6731
Flit latency average = 865.205
	minimum = 6
	maximum = 4750
Slowest flit = 88276
Fragmentation average = 67.3707
	minimum = 0
	maximum = 156
Injected packet rate average = 0.0112526
	minimum = 0.003 (at node 120)
	maximum = 0.028 (at node 81)
Accepted packet rate average = 0.00947135
	minimum = 0.005 (at node 4)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.202583
	minimum = 0.054 (at node 120)
	maximum = 0.504 (at node 81)
Accepted flit rate average= 0.170447
	minimum = 0.092 (at node 4)
	maximum = 0.25825 (at node 129)
Injected packet length average = 18.0032
Accepted packet length average = 17.996
Total in-flight flits = 49622 (49303 measured)
latency change    = 0.147308
throughput change = 0.00416339
Class 0:
Packet latency average = 982.129
	minimum = 23
	maximum = 4664
Network latency average = 930.867
	minimum = 23
	maximum = 4386
Slowest packet = 6996
Flit latency average = 939.352
	minimum = 6
	maximum = 6114
Slowest flit = 73835
Fragmentation average = 68.0234
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0112927
	minimum = 0.005 (at node 91)
	maximum = 0.028 (at node 153)
Accepted packet rate average = 0.00947187
	minimum = 0.006 (at node 4)
	maximum = 0.0138 (at node 16)
Injected flit rate average = 0.203272
	minimum = 0.09 (at node 91)
	maximum = 0.506 (at node 153)
Accepted flit rate average= 0.170356
	minimum = 0.108 (at node 4)
	maximum = 0.2454 (at node 16)
Injected packet length average = 18.0003
Accepted packet length average = 17.9855
Total in-flight flits = 56565 (56433 measured)
latency change    = 0.104096
throughput change = 0.000530445
Class 0:
Packet latency average = 1074.04
	minimum = 23
	maximum = 5665
Network latency average = 1022.36
	minimum = 23
	maximum = 5622
Slowest packet = 7304
Flit latency average = 1009.79
	minimum = 6
	maximum = 6732
Slowest flit = 88576
Fragmentation average = 68.4588
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0112187
	minimum = 0.00466667 (at node 29)
	maximum = 0.0271667 (at node 153)
Accepted packet rate average = 0.00942014
	minimum = 0.0055 (at node 36)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.201935
	minimum = 0.084 (at node 29)
	maximum = 0.490167 (at node 153)
Accepted flit rate average= 0.16947
	minimum = 0.101333 (at node 36)
	maximum = 0.233167 (at node 129)
Injected packet length average = 17.9998
Accepted packet length average = 17.9902
Total in-flight flits = 62371 (62316 measured)
latency change    = 0.0855743
throughput change = 0.00522666
Class 0:
Packet latency average = 1162.56
	minimum = 23
	maximum = 6684
Network latency average = 1111.02
	minimum = 23
	maximum = 6623
Slowest packet = 6699
Flit latency average = 1084.98
	minimum = 6
	maximum = 7073
Slowest flit = 98639
Fragmentation average = 68.7187
	minimum = 0
	maximum = 165
Injected packet rate average = 0.0114658
	minimum = 0.00414286 (at node 29)
	maximum = 0.0238571 (at node 153)
Accepted packet rate average = 0.00945461
	minimum = 0.00614286 (at node 36)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.206394
	minimum = 0.0745714 (at node 29)
	maximum = 0.430857 (at node 153)
Accepted flit rate average= 0.170263
	minimum = 0.111286 (at node 36)
	maximum = 0.232571 (at node 129)
Injected packet length average = 18.0008
Accepted packet length average = 18.0084
Total in-flight flits = 73516 (73498 measured)
latency change    = 0.0761401
throughput change = 0.00465259
Draining all recorded packets ...
Class 0:
Remaining flits: 114322 114323 114324 114325 114326 114327 114328 114329 114330 114331 [...] (80159 flits)
Measured flits: 155592 155593 155594 155595 155596 155597 155598 155599 155600 155601 [...] (48598 flits)
Class 0:
Remaining flits: 178372 178373 178374 178375 178376 178377 178378 178379 193140 193141 [...] (86604 flits)
Measured flits: 178372 178373 178374 178375 178376 178377 178378 178379 193140 193141 [...] (29769 flits)
Class 0:
Remaining flits: 219564 219565 219566 219567 219568 219569 219570 219571 219572 219573 [...] (90852 flits)
Measured flits: 219564 219565 219566 219567 219568 219569 219570 219571 219572 219573 [...] (17720 flits)
Class 0:
Remaining flits: 224982 224983 224984 224985 224986 224987 224988 224989 224990 224991 [...] (100405 flits)
Measured flits: 224982 224983 224984 224985 224986 224987 224988 224989 224990 224991 [...] (9767 flits)
Class 0:
Remaining flits: 234774 234775 234776 234777 234778 234779 234780 234781 234782 234783 [...] (106798 flits)
Measured flits: 234774 234775 234776 234777 234778 234779 234780 234781 234782 234783 [...] (4773 flits)
Class 0:
Remaining flits: 235818 235819 235820 235821 235822 235823 235824 235825 235826 235827 [...] (109063 flits)
Measured flits: 235818 235819 235820 235821 235822 235823 235824 235825 235826 235827 [...] (2049 flits)
Class 0:
Remaining flits: 252247 252248 252249 252250 252251 253188 253189 253190 253191 253192 [...] (115911 flits)
Measured flits: 252247 252248 252249 252250 252251 253188 253189 253190 253191 253192 [...] (899 flits)
Class 0:
Remaining flits: 329976 329977 329978 329979 329980 329981 329982 329983 329984 329985 [...] (124187 flits)
Measured flits: 329976 329977 329978 329979 329980 329981 329982 329983 329984 329985 [...] (332 flits)
Class 0:
Remaining flits: 356670 356671 356672 356673 356674 356675 356676 356677 356678 356679 [...] (131214 flits)
Measured flits: 356670 356671 356672 356673 356674 356675 356676 356677 356678 356679 [...] (57 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 419382 419383 419384 419385 419386 419387 419388 419389 419390 419391 [...] (103832 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 421920 421921 421922 421923 421924 421925 421926 421927 421928 421929 [...] (75676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 433432 433433 433434 433435 433436 433437 433438 433439 466920 466921 [...] (48126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 501583 501584 501585 501586 501587 522018 522019 522020 522021 522022 [...] (23786 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 535193 538092 538093 538094 538095 538096 538097 538098 538099 538100 [...] (7093 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 651726 651727 651728 651729 651730 651731 651732 651733 651734 651735 [...] (1187 flits)
Measured flits: (0 flits)
Time taken is 25912 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1754.69 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10735 (1 samples)
Network latency average = 1701.12 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10681 (1 samples)
Flit latency average = 2415.79 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11382 (1 samples)
Fragmentation average = 70.8259 (1 samples)
	minimum = 0 (1 samples)
	maximum = 171 (1 samples)
Injected packet rate average = 0.0114658 (1 samples)
	minimum = 0.00414286 (1 samples)
	maximum = 0.0238571 (1 samples)
Accepted packet rate average = 0.00945461 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.013 (1 samples)
Injected flit rate average = 0.206394 (1 samples)
	minimum = 0.0745714 (1 samples)
	maximum = 0.430857 (1 samples)
Accepted flit rate average = 0.170263 (1 samples)
	minimum = 0.111286 (1 samples)
	maximum = 0.232571 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 18.0084 (1 samples)
Hops average = 5.07555 (1 samples)
Total run time 13.5888
