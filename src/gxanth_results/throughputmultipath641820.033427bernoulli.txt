BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 281.232
	minimum = 22
	maximum = 843
Network latency average = 270.326
	minimum = 22
	maximum = 806
Slowest packet = 1103
Flit latency average = 236.86
	minimum = 5
	maximum = 798
Slowest flit = 19114
Fragmentation average = 67.4652
	minimum = 0
	maximum = 496
Injected packet rate average = 0.031724
	minimum = 0.018 (at node 146)
	maximum = 0.047 (at node 73)
Accepted packet rate average = 0.0141979
	minimum = 0.006 (at node 81)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.565542
	minimum = 0.323 (at node 146)
	maximum = 0.841 (at node 118)
Accepted flit rate average= 0.273297
	minimum = 0.118 (at node 81)
	maximum = 0.458 (at node 44)
Injected packet length average = 17.827
Accepted packet length average = 19.2491
Total in-flight flits = 57273 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 536.856
	minimum = 22
	maximum = 1600
Network latency average = 515.812
	minimum = 22
	maximum = 1557
Slowest packet = 2315
Flit latency average = 469.895
	minimum = 5
	maximum = 1550
Slowest flit = 45502
Fragmentation average = 90.7879
	minimum = 0
	maximum = 583
Injected packet rate average = 0.0289479
	minimum = 0.016 (at node 84)
	maximum = 0.0395 (at node 19)
Accepted packet rate average = 0.0148802
	minimum = 0.008 (at node 153)
	maximum = 0.0205 (at node 152)
Injected flit rate average = 0.518062
	minimum = 0.288 (at node 84)
	maximum = 0.7065 (at node 19)
Accepted flit rate average= 0.276664
	minimum = 0.1645 (at node 153)
	maximum = 0.3805 (at node 152)
Injected packet length average = 17.8964
Accepted packet length average = 18.5928
Total in-flight flits = 94281 (0 measured)
latency change    = 0.476149
throughput change = 0.0121707
Class 0:
Packet latency average = 1342.17
	minimum = 22
	maximum = 2413
Network latency average = 1245.55
	minimum = 22
	maximum = 2363
Slowest packet = 3885
Flit latency average = 1197.92
	minimum = 5
	maximum = 2346
Slowest flit = 71099
Fragmentation average = 119.407
	minimum = 0
	maximum = 737
Injected packet rate average = 0.0172031
	minimum = 0.002 (at node 72)
	maximum = 0.038 (at node 134)
Accepted packet rate average = 0.0150313
	minimum = 0.006 (at node 21)
	maximum = 0.025 (at node 56)
Injected flit rate average = 0.310401
	minimum = 0.03 (at node 72)
	maximum = 0.673 (at node 134)
Accepted flit rate average= 0.271823
	minimum = 0.123 (at node 21)
	maximum = 0.475 (at node 16)
Injected packet length average = 18.0433
Accepted packet length average = 18.0839
Total in-flight flits = 101797 (0 measured)
latency change    = 0.600009
throughput change = 0.0178099
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1017.56
	minimum = 40
	maximum = 2596
Network latency average = 90.5551
	minimum = 22
	maximum = 932
Slowest packet = 14467
Flit latency average = 1607.64
	minimum = 5
	maximum = 3090
Slowest flit = 65553
Fragmentation average = 8.26432
	minimum = 0
	maximum = 79
Injected packet rate average = 0.0151875
	minimum = 0 (at node 57)
	maximum = 0.031 (at node 47)
Accepted packet rate average = 0.0150313
	minimum = 0.006 (at node 71)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.272562
	minimum = 0 (at node 57)
	maximum = 0.557 (at node 47)
Accepted flit rate average= 0.269536
	minimum = 0.111 (at node 74)
	maximum = 0.493 (at node 83)
Injected packet length average = 17.9465
Accepted packet length average = 17.9317
Total in-flight flits = 102372 (48205 measured)
latency change    = 0.319014
throughput change = 0.00848293
Class 0:
Packet latency average = 1894.71
	minimum = 40
	maximum = 3595
Network latency average = 690.017
	minimum = 22
	maximum = 1913
Slowest packet = 14467
Flit latency average = 1711.72
	minimum = 5
	maximum = 3805
Slowest flit = 121985
Fragmentation average = 52.3322
	minimum = 0
	maximum = 729
Injected packet rate average = 0.0151745
	minimum = 0.005 (at node 64)
	maximum = 0.0255 (at node 47)
Accepted packet rate average = 0.0149844
	minimum = 0.009 (at node 141)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.272802
	minimum = 0.093 (at node 163)
	maximum = 0.4585 (at node 47)
Accepted flit rate average= 0.269432
	minimum = 0.173 (at node 86)
	maximum = 0.4335 (at node 129)
Injected packet length average = 17.9777
Accepted packet length average = 17.9809
Total in-flight flits = 103257 (87223 measured)
latency change    = 0.462948
throughput change = 0.000386615
Class 0:
Packet latency average = 2593.31
	minimum = 40
	maximum = 4464
Network latency average = 1356.71
	minimum = 22
	maximum = 2935
Slowest packet = 14467
Flit latency average = 1788.47
	minimum = 5
	maximum = 4124
Slowest flit = 148517
Fragmentation average = 86.2039
	minimum = 0
	maximum = 729
Injected packet rate average = 0.0149618
	minimum = 0.008 (at node 40)
	maximum = 0.0246667 (at node 47)
Accepted packet rate average = 0.014901
	minimum = 0.01 (at node 135)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.269236
	minimum = 0.144667 (at node 40)
	maximum = 0.44 (at node 47)
Accepted flit rate average= 0.26834
	minimum = 0.182 (at node 135)
	maximum = 0.377667 (at node 128)
Injected packet length average = 17.9949
Accepted packet length average = 18.0082
Total in-flight flits = 102321 (100708 measured)
latency change    = 0.269386
throughput change = 0.00406951
Draining remaining packets ...
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (54804 flits)
Measured flits: 260568 260569 260570 260571 260572 260573 260574 260575 260576 260577 [...] (54732 flits)
Class 0:
Remaining flits: 304190 304191 304192 304193 304194 304195 304196 304197 304198 304199 [...] (8360 flits)
Measured flits: 304190 304191 304192 304193 304194 304195 304196 304197 304198 304199 [...] (8360 flits)
Time taken is 8764 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3481.49 (1 samples)
	minimum = 40 (1 samples)
	maximum = 5940 (1 samples)
Network latency average = 1994.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4363 (1 samples)
Flit latency average = 1970.74 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5087 (1 samples)
Fragmentation average = 102.163 (1 samples)
	minimum = 0 (1 samples)
	maximum = 729 (1 samples)
Injected packet rate average = 0.0149618 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0246667 (1 samples)
Accepted packet rate average = 0.014901 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.269236 (1 samples)
	minimum = 0.144667 (1 samples)
	maximum = 0.44 (1 samples)
Accepted flit rate average = 0.26834 (1 samples)
	minimum = 0.182 (1 samples)
	maximum = 0.377667 (1 samples)
Injected packet size average = 17.9949 (1 samples)
Accepted packet size average = 18.0082 (1 samples)
Hops average = 5.13208 (1 samples)
Total run time 13.7671
