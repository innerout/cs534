BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.953
	minimum = 22
	maximum = 983
Network latency average = 187.341
	minimum = 22
	maximum = 722
Slowest packet = 45
Flit latency average = 160.169
	minimum = 5
	maximum = 705
Slowest flit = 16397
Fragmentation average = 18.3957
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0174688
	minimum = 0 (at node 170)
	maximum = 0.038 (at node 38)
Accepted packet rate average = 0.0131094
	minimum = 0.006 (at node 41)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.310901
	minimum = 0 (at node 170)
	maximum = 0.668 (at node 38)
Accepted flit rate average= 0.240901
	minimum = 0.113 (at node 41)
	maximum = 0.383 (at node 44)
Injected packet length average = 17.7976
Accepted packet length average = 18.3762
Total in-flight flits = 15343 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 583.803
	minimum = 22
	maximum = 1933
Network latency average = 251.433
	minimum = 22
	maximum = 1169
Slowest packet = 45
Flit latency average = 220.725
	minimum = 5
	maximum = 1152
Slowest flit = 27629
Fragmentation average = 20.5697
	minimum = 0
	maximum = 109
Injected packet rate average = 0.0159245
	minimum = 0.0025 (at node 36)
	maximum = 0.028 (at node 162)
Accepted packet rate average = 0.0135755
	minimum = 0.0075 (at node 153)
	maximum = 0.021 (at node 166)
Injected flit rate average = 0.285052
	minimum = 0.045 (at node 36)
	maximum = 0.496 (at node 162)
Accepted flit rate average= 0.24687
	minimum = 0.135 (at node 153)
	maximum = 0.378 (at node 166)
Injected packet length average = 17.9002
Accepted packet length average = 18.1849
Total in-flight flits = 17108 (0 measured)
latency change    = 0.443386
throughput change = 0.0241777
Class 0:
Packet latency average = 1419.89
	minimum = 25
	maximum = 2867
Network latency average = 351.083
	minimum = 22
	maximum = 1531
Slowest packet = 5599
Flit latency average = 316.859
	minimum = 5
	maximum = 1514
Slowest flit = 81000
Fragmentation average = 22.3381
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0140781
	minimum = 0.001 (at node 33)
	maximum = 0.029 (at node 110)
Accepted packet rate average = 0.013724
	minimum = 0.005 (at node 4)
	maximum = 0.023 (at node 142)
Injected flit rate average = 0.252083
	minimum = 0.018 (at node 49)
	maximum = 0.522 (at node 110)
Accepted flit rate average= 0.247104
	minimum = 0.09 (at node 4)
	maximum = 0.417 (at node 142)
Injected packet length average = 17.906
Accepted packet length average = 18.0053
Total in-flight flits = 18246 (0 measured)
latency change    = 0.58884
throughput change = 0.000948487
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2056.29
	minimum = 25
	maximum = 3795
Network latency average = 291.254
	minimum = 22
	maximum = 924
Slowest packet = 8938
Flit latency average = 318.811
	minimum = 5
	maximum = 1456
Slowest flit = 105460
Fragmentation average = 22.2656
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0143125
	minimum = 0.004 (at node 149)
	maximum = 0.026 (at node 141)
Accepted packet rate average = 0.0143594
	minimum = 0.006 (at node 53)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.258615
	minimum = 0.072 (at node 149)
	maximum = 0.477 (at node 141)
Accepted flit rate average= 0.258562
	minimum = 0.112 (at node 53)
	maximum = 0.474 (at node 129)
Injected packet length average = 18.0691
Accepted packet length average = 18.0065
Total in-flight flits = 18048 (17971 measured)
latency change    = 0.309488
throughput change = 0.0443155
Class 0:
Packet latency average = 2327.6
	minimum = 25
	maximum = 4854
Network latency average = 325.078
	minimum = 22
	maximum = 1512
Slowest packet = 8938
Flit latency average = 316.245
	minimum = 5
	maximum = 1495
Slowest flit = 177947
Fragmentation average = 22.0307
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138958
	minimum = 0.005 (at node 81)
	maximum = 0.0235 (at node 97)
Accepted packet rate average = 0.0139818
	minimum = 0.0085 (at node 36)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.250466
	minimum = 0.087 (at node 81)
	maximum = 0.418 (at node 97)
Accepted flit rate average= 0.251445
	minimum = 0.153 (at node 36)
	maximum = 0.425 (at node 129)
Injected packet length average = 18.0246
Accepted packet length average = 17.9838
Total in-flight flits = 17919 (17919 measured)
latency change    = 0.116564
throughput change = 0.0283051
Class 0:
Packet latency average = 2641.76
	minimum = 25
	maximum = 5549
Network latency average = 339.657
	minimum = 22
	maximum = 1947
Slowest packet = 8938
Flit latency average = 320.419
	minimum = 5
	maximum = 1930
Slowest flit = 178720
Fragmentation average = 21.9628
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0138628
	minimum = 0.00666667 (at node 56)
	maximum = 0.0243333 (at node 26)
Accepted packet rate average = 0.0138611
	minimum = 0.00933333 (at node 36)
	maximum = 0.02 (at node 129)
Injected flit rate average = 0.249778
	minimum = 0.12 (at node 56)
	maximum = 0.439333 (at node 26)
Accepted flit rate average= 0.249484
	minimum = 0.168 (at node 36)
	maximum = 0.356333 (at node 129)
Injected packet length average = 18.0178
Accepted packet length average = 17.9989
Total in-flight flits = 18327 (18327 measured)
latency change    = 0.118921
throughput change = 0.00785996
Class 0:
Packet latency average = 2950.53
	minimum = 25
	maximum = 6397
Network latency average = 346.283
	minimum = 22
	maximum = 1947
Slowest packet = 8938
Flit latency average = 322.539
	minimum = 5
	maximum = 1930
Slowest flit = 178720
Fragmentation average = 21.8955
	minimum = 0
	maximum = 137
Injected packet rate average = 0.013724
	minimum = 0.0055 (at node 81)
	maximum = 0.0205 (at node 26)
Accepted packet rate average = 0.0137552
	minimum = 0.00925 (at node 36)
	maximum = 0.0195 (at node 129)
Injected flit rate average = 0.247189
	minimum = 0.10075 (at node 81)
	maximum = 0.371 (at node 26)
Accepted flit rate average= 0.247483
	minimum = 0.17225 (at node 104)
	maximum = 0.351 (at node 129)
Injected packet length average = 18.0115
Accepted packet length average = 17.992
Total in-flight flits = 18187 (18187 measured)
latency change    = 0.104648
throughput change = 0.00808662
Class 0:
Packet latency average = 3275.11
	minimum = 25
	maximum = 7117
Network latency average = 351.368
	minimum = 22
	maximum = 2148
Slowest packet = 8938
Flit latency average = 324.963
	minimum = 5
	maximum = 2101
Slowest flit = 286230
Fragmentation average = 21.617
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0136688
	minimum = 0.0068 (at node 53)
	maximum = 0.0196 (at node 26)
Accepted packet rate average = 0.0137146
	minimum = 0.0104 (at node 104)
	maximum = 0.0178 (at node 128)
Injected flit rate average = 0.24622
	minimum = 0.1238 (at node 53)
	maximum = 0.3536 (at node 26)
Accepted flit rate average= 0.246743
	minimum = 0.1846 (at node 104)
	maximum = 0.3222 (at node 128)
Injected packet length average = 18.0133
Accepted packet length average = 17.9913
Total in-flight flits = 17785 (17785 measured)
latency change    = 0.0991039
throughput change = 0.00300055
Class 0:
Packet latency average = 3581.44
	minimum = 25
	maximum = 7967
Network latency average = 354.132
	minimum = 22
	maximum = 2275
Slowest packet = 8938
Flit latency average = 326.296
	minimum = 5
	maximum = 2236
Slowest flit = 312012
Fragmentation average = 21.6399
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0137153
	minimum = 0.00716667 (at node 53)
	maximum = 0.0193333 (at node 130)
Accepted packet rate average = 0.0136918
	minimum = 0.011 (at node 42)
	maximum = 0.0171667 (at node 128)
Injected flit rate average = 0.246875
	minimum = 0.130167 (at node 53)
	maximum = 0.3505 (at node 130)
Accepted flit rate average= 0.246521
	minimum = 0.197167 (at node 42)
	maximum = 0.3095 (at node 128)
Injected packet length average = 18
Accepted packet length average = 18.0049
Total in-flight flits = 18600 (18600 measured)
latency change    = 0.0855344
throughput change = 0.000900025
Class 0:
Packet latency average = 3888.58
	minimum = 25
	maximum = 8815
Network latency average = 357.082
	minimum = 22
	maximum = 2275
Slowest packet = 8938
Flit latency average = 327.752
	minimum = 5
	maximum = 2236
Slowest flit = 312012
Fragmentation average = 21.8247
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0136964
	minimum = 0.00728571 (at node 53)
	maximum = 0.0195714 (at node 130)
Accepted packet rate average = 0.013689
	minimum = 0.0112857 (at node 79)
	maximum = 0.0175714 (at node 138)
Injected flit rate average = 0.246559
	minimum = 0.132143 (at node 53)
	maximum = 0.354 (at node 130)
Accepted flit rate average= 0.246403
	minimum = 0.202 (at node 161)
	maximum = 0.316286 (at node 138)
Injected packet length average = 18.0017
Accepted packet length average = 18.0001
Total in-flight flits = 18352 (18352 measured)
latency change    = 0.0789835
throughput change = 0.000477102
Draining all recorded packets ...
Class 0:
Remaining flits: 474984 474985 474986 474987 474988 474989 474990 474991 474992 474993 [...] (18639 flits)
Measured flits: 474984 474985 474986 474987 474988 474989 474990 474991 474992 474993 [...] (18639 flits)
Class 0:
Remaining flits: 540900 540901 540902 540903 540904 540905 540906 540907 540908 540909 [...] (18228 flits)
Measured flits: 540900 540901 540902 540903 540904 540905 540906 540907 540908 540909 [...] (18128 flits)
Class 0:
Remaining flits: 581371 581372 581373 581374 581375 581376 581377 581378 581379 581380 [...] (17687 flits)
Measured flits: 581371 581372 581373 581374 581375 581376 581377 581378 581379 581380 [...] (17525 flits)
Class 0:
Remaining flits: 609786 609787 609788 609789 609790 609791 609792 609793 609794 609795 [...] (18444 flits)
Measured flits: 609786 609787 609788 609789 609790 609791 609792 609793 609794 609795 [...] (18191 flits)
Class 0:
Remaining flits: 652140 652141 652142 652143 652144 652145 652146 652147 652148 652149 [...] (18155 flits)
Measured flits: 652140 652141 652142 652143 652144 652145 652146 652147 652148 652149 [...] (17659 flits)
Class 0:
Remaining flits: 717714 717715 717716 717717 717718 717719 717720 717721 717722 717723 [...] (18495 flits)
Measured flits: 717714 717715 717716 717717 717718 717719 717720 717721 717722 717723 [...] (17928 flits)
Class 0:
Remaining flits: 760392 760393 760394 760395 760396 760397 760398 760399 760400 760401 [...] (18826 flits)
Measured flits: 760392 760393 760394 760395 760396 760397 760398 760399 760400 760401 [...] (18002 flits)
Class 0:
Remaining flits: 824850 824851 824852 824853 824854 824855 824856 824857 824858 824859 [...] (18269 flits)
Measured flits: 824940 824941 824942 824943 824944 824945 824946 824947 824948 824949 [...] (16575 flits)
Class 0:
Remaining flits: 867778 867779 872838 872839 872840 872841 872842 872843 872844 872845 [...] (18504 flits)
Measured flits: 867778 867779 872838 872839 872840 872841 872842 872843 872844 872845 [...] (16183 flits)
Class 0:
Remaining flits: 898236 898237 898238 898239 898240 898241 898242 898243 898244 898245 [...] (18424 flits)
Measured flits: 898236 898237 898238 898239 898240 898241 898242 898243 898244 898245 [...] (15212 flits)
Class 0:
Remaining flits: 920628 920629 920630 920631 920632 920633 920634 920635 920636 920637 [...] (18769 flits)
Measured flits: 920628 920629 920630 920631 920632 920633 920634 920635 920636 920637 [...] (13821 flits)
Class 0:
Remaining flits: 1002368 1002369 1002370 1002371 1002372 1002373 1002374 1002375 1002376 1002377 [...] (18054 flits)
Measured flits: 1002368 1002369 1002370 1002371 1002372 1002373 1002374 1002375 1002376 1002377 [...] (12047 flits)
Class 0:
Remaining flits: 1008843 1008844 1008845 1023858 1023859 1023860 1023861 1023862 1023863 1023864 [...] (18359 flits)
Measured flits: 1008843 1008844 1008845 1025009 1032138 1032139 1032140 1032141 1032142 1032143 [...] (11877 flits)
Class 0:
Remaining flits: 1088528 1088529 1088530 1088531 1089305 1090080 1090081 1090082 1090083 1090084 [...] (18583 flits)
Measured flits: 1088528 1088529 1088530 1088531 1089305 1090080 1090081 1090082 1090083 1090084 [...] (10759 flits)
Class 0:
Remaining flits: 1142423 1143414 1143415 1143416 1143417 1143418 1143419 1143420 1143421 1143422 [...] (18487 flits)
Measured flits: 1155024 1155025 1155026 1155027 1155028 1155029 1155030 1155031 1155032 1155033 [...] (9300 flits)
Class 0:
Remaining flits: 1198584 1198585 1198586 1198587 1198588 1198589 1198590 1198591 1198592 1198593 [...] (18498 flits)
Measured flits: 1198584 1198585 1198586 1198587 1198588 1198589 1198590 1198591 1198592 1198593 [...] (8445 flits)
Class 0:
Remaining flits: 1218420 1218421 1218422 1218423 1218424 1218425 1218426 1218427 1218428 1218429 [...] (18238 flits)
Measured flits: 1229886 1229887 1229888 1229889 1229890 1229891 1229892 1229893 1229894 1229895 [...] (6628 flits)
Class 0:
Remaining flits: 1297926 1297927 1297928 1297929 1297930 1297931 1297932 1297933 1297934 1297935 [...] (18279 flits)
Measured flits: 1303200 1303201 1303202 1303203 1303204 1303205 1303206 1303207 1303208 1303209 [...] (5974 flits)
Class 0:
Remaining flits: 1339573 1339574 1339575 1339576 1339577 1340496 1340497 1340498 1340499 1340500 [...] (17958 flits)
Measured flits: 1349352 1349353 1349354 1349355 1349356 1349357 1349358 1349359 1349360 1349361 [...] (4465 flits)
Class 0:
Remaining flits: 1377487 1377488 1377489 1377490 1377491 1377492 1377493 1377494 1377495 1377496 [...] (18810 flits)
Measured flits: 1411686 1411687 1411688 1411689 1411690 1411691 1411692 1411693 1411694 1411695 [...] (3247 flits)
Class 0:
Remaining flits: 1405818 1405819 1405820 1405821 1405822 1405823 1405824 1405825 1405826 1405827 [...] (18530 flits)
Measured flits: 1455210 1455211 1455212 1455213 1455214 1455215 1455216 1455217 1455218 1455219 [...] (2528 flits)
Class 0:
Remaining flits: 1488726 1488727 1488728 1488729 1488730 1488731 1488732 1488733 1488734 1488735 [...] (18438 flits)
Measured flits: 1488726 1488727 1488728 1488729 1488730 1488731 1488732 1488733 1488734 1488735 [...] (1855 flits)
Class 0:
Remaining flits: 1527876 1527877 1527878 1527879 1527880 1527881 1527882 1527883 1527884 1527885 [...] (18198 flits)
Measured flits: 1527876 1527877 1527878 1527879 1527880 1527881 1527882 1527883 1527884 1527885 [...] (1414 flits)
Class 0:
Remaining flits: 1558206 1558207 1558208 1558209 1558210 1558211 1558212 1558213 1558214 1558215 [...] (18194 flits)
Measured flits: 1609402 1609403 1609404 1609405 1609406 1609407 1609408 1609409 1609410 1609411 [...] (1183 flits)
Class 0:
Remaining flits: 1618542 1618543 1618544 1618545 1618546 1618547 1618548 1618549 1618550 1618551 [...] (18581 flits)
Measured flits: 1660932 1660933 1660934 1660935 1660936 1660937 1660938 1660939 1660940 1660941 [...] (966 flits)
Class 0:
Remaining flits: 1638036 1638037 1638038 1638039 1638040 1638041 1638042 1638043 1638044 1638045 [...] (18451 flits)
Measured flits: 1684224 1684225 1684226 1684227 1684228 1684229 1684230 1684231 1684232 1684233 [...] (625 flits)
Class 0:
Remaining flits: 1690614 1690615 1690616 1690617 1690618 1690619 1690620 1690621 1690622 1690623 [...] (18501 flits)
Measured flits: 1769868 1769869 1769870 1769871 1769872 1769873 1769874 1769875 1769876 1769877 [...] (370 flits)
Class 0:
Remaining flits: 1784594 1784595 1784596 1784597 1784598 1784599 1784600 1784601 1784602 1784603 [...] (18456 flits)
Measured flits: 1814076 1814077 1814078 1814079 1814080 1814081 1814082 1814083 1814084 1814085 [...] (144 flits)
Class 0:
Remaining flits: 1803312 1803313 1803314 1803315 1803316 1803317 1803318 1803319 1803320 1803321 [...] (18544 flits)
Measured flits: 1844547 1844548 1844549 1846170 1846171 1846172 1846173 1846174 1846175 1846176 [...] (201 flits)
Class 0:
Remaining flits: 1848489 1848490 1848491 1863234 1863235 1863236 1863237 1863238 1863239 1863240 [...] (18166 flits)
Measured flits: 1921932 1921933 1921934 1921935 1921936 1921937 1921938 1921939 1921940 1921941 [...] (90 flits)
Class 0:
Remaining flits: 1906812 1906813 1906814 1906815 1906816 1906817 1906818 1906819 1906820 1906821 [...] (17984 flits)
Measured flits: 1950732 1950733 1950734 1950735 1950736 1950737 1950738 1950739 1950740 1950741 [...] (108 flits)
Class 0:
Remaining flits: 1960902 1960903 1960904 1960905 1960906 1960907 1960908 1960909 1960910 1960911 [...] (18378 flits)
Measured flits: 2009286 2009287 2009288 2009289 2009290 2009291 2009292 2009293 2009294 2009295 [...] (90 flits)
Class 0:
Remaining flits: 2009358 2009359 2009360 2009361 2009362 2009363 2009364 2009365 2009366 2009367 [...] (18429 flits)
Measured flits: 2057202 2057203 2057204 2057205 2057206 2057207 2057208 2057209 2057210 2057211 [...] (108 flits)
Draining remaining packets ...
Time taken is 44508 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9562.36 (1 samples)
	minimum = 25 (1 samples)
	maximum = 34015 (1 samples)
Network latency average = 361.679 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2356 (1 samples)
Flit latency average = 329.271 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2339 (1 samples)
Fragmentation average = 22.5798 (1 samples)
	minimum = 0 (1 samples)
	maximum = 154 (1 samples)
Injected packet rate average = 0.0136964 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0195714 (1 samples)
Accepted packet rate average = 0.013689 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.0175714 (1 samples)
Injected flit rate average = 0.246559 (1 samples)
	minimum = 0.132143 (1 samples)
	maximum = 0.354 (1 samples)
Accepted flit rate average = 0.246403 (1 samples)
	minimum = 0.202 (1 samples)
	maximum = 0.316286 (1 samples)
Injected packet size average = 18.0017 (1 samples)
Accepted packet size average = 18.0001 (1 samples)
Hops average = 5.06514 (1 samples)
Total run time 39.1874
