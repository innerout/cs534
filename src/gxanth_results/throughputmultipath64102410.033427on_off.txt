BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 374.684
	minimum = 23
	maximum = 984
Network latency average = 276.205
	minimum = 23
	maximum = 957
Slowest packet = 68
Flit latency average = 203.689
	minimum = 6
	maximum = 951
Slowest flit = 3295
Fragmentation average = 163.111
	minimum = 0
	maximum = 816
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0100625
	minimum = 0.004 (at node 23)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.214271
	minimum = 0.089 (at node 68)
	maximum = 0.364 (at node 152)
Injected packet length average = 17.8118
Accepted packet length average = 21.294
Total in-flight flits = 59300 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 627.7
	minimum = 23
	maximum = 1954
Network latency average = 482.662
	minimum = 23
	maximum = 1757
Slowest packet = 183
Flit latency average = 392.91
	minimum = 6
	maximum = 1934
Slowest flit = 2842
Fragmentation average = 225.529
	minimum = 0
	maximum = 1436
Injected packet rate average = 0.0304896
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0117083
	minimum = 0.0065 (at node 135)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.546388
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.232789
	minimum = 0.143 (at node 96)
	maximum = 0.341 (at node 152)
Injected packet length average = 17.9205
Accepted packet length average = 19.8823
Total in-flight flits = 121353 (0 measured)
latency change    = 0.403084
throughput change = 0.0795494
Class 0:
Packet latency average = 1247.15
	minimum = 29
	maximum = 2975
Network latency average = 1027.44
	minimum = 25
	maximum = 2781
Slowest packet = 439
Flit latency average = 938.324
	minimum = 6
	maximum = 2878
Slowest flit = 8131
Fragmentation average = 324.033
	minimum = 0
	maximum = 2210
Injected packet rate average = 0.031875
	minimum = 0 (at node 15)
	maximum = 0.056 (at node 40)
Accepted packet rate average = 0.0138385
	minimum = 0.006 (at node 61)
	maximum = 0.024 (at node 14)
Injected flit rate average = 0.573807
	minimum = 0.014 (at node 15)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.253292
	minimum = 0.099 (at node 167)
	maximum = 0.407 (at node 16)
Injected packet length average = 18.0018
Accepted packet length average = 18.3033
Total in-flight flits = 182881 (0 measured)
latency change    = 0.496692
throughput change = 0.0809446
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 411.986
	minimum = 37
	maximum = 1486
Network latency average = 130.454
	minimum = 23
	maximum = 931
Slowest packet = 17887
Flit latency average = 1365.5
	minimum = 6
	maximum = 3836
Slowest flit = 11985
Fragmentation average = 65.058
	minimum = 0
	maximum = 816
Injected packet rate average = 0.0326719
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0139427
	minimum = 0.005 (at node 126)
	maximum = 0.025 (at node 66)
Injected flit rate average = 0.587896
	minimum = 0 (at node 5)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.253823
	minimum = 0.114 (at node 184)
	maximum = 0.395 (at node 103)
Injected packet length average = 17.9939
Accepted packet length average = 18.2047
Total in-flight flits = 247061 (100884 measured)
latency change    = 2.02716
throughput change = 0.00209299
Class 0:
Packet latency average = 643.172
	minimum = 27
	maximum = 2358
Network latency average = 395.883
	minimum = 23
	maximum = 1956
Slowest packet = 17887
Flit latency average = 1587.31
	minimum = 6
	maximum = 4784
Slowest flit = 16242
Fragmentation average = 139.051
	minimum = 0
	maximum = 1442
Injected packet rate average = 0.032362
	minimum = 0.004 (at node 123)
	maximum = 0.0555 (at node 0)
Accepted packet rate average = 0.0139557
	minimum = 0.008 (at node 126)
	maximum = 0.021 (at node 30)
Injected flit rate average = 0.582534
	minimum = 0.0715 (at node 123)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.254047
	minimum = 0.149 (at node 71)
	maximum = 0.378 (at node 172)
Injected packet length average = 18.0006
Accepted packet length average = 18.2038
Total in-flight flits = 309013 (192448 measured)
latency change    = 0.359446
throughput change = 0.000881563
Class 0:
Packet latency average = 928.753
	minimum = 27
	maximum = 3657
Network latency average = 691.259
	minimum = 23
	maximum = 2975
Slowest packet = 17887
Flit latency average = 1833.81
	minimum = 6
	maximum = 5628
Slowest flit = 21722
Fragmentation average = 178.867
	minimum = 0
	maximum = 2271
Injected packet rate average = 0.0321302
	minimum = 0.00866667 (at node 95)
	maximum = 0.0556667 (at node 25)
Accepted packet rate average = 0.0139583
	minimum = 0.00933333 (at node 77)
	maximum = 0.0206667 (at node 123)
Injected flit rate average = 0.578356
	minimum = 0.156 (at node 95)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.253731
	minimum = 0.172333 (at node 77)
	maximum = 0.356 (at node 137)
Injected packet length average = 18.0004
Accepted packet length average = 18.1777
Total in-flight flits = 369858 (277831 measured)
latency change    = 0.307488
throughput change = 0.0012453
Draining remaining packets ...
Class 0:
Remaining flits: 20502 20503 20504 20505 20506 20507 20508 20509 20510 20511 [...] (330876 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (260219 flits)
Class 0:
Remaining flits: 29271 29272 29273 29274 29275 29276 29277 29278 29279 29280 [...] (292175 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (238984 flits)
Class 0:
Remaining flits: 29271 29272 29273 29274 29275 29276 29277 29278 29279 29280 [...] (253978 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (214518 flits)
Class 0:
Remaining flits: 29271 29272 29273 29274 29275 29276 29277 29278 29279 29280 [...] (216581 flits)
Measured flits: 320958 320959 320960 320961 320962 320963 320964 320965 320966 320967 [...] (186401 flits)
Class 0:
Remaining flits: 32119 32120 32121 32122 32123 32124 32125 32126 32127 32128 [...] (180230 flits)
Measured flits: 320994 320995 320996 320997 320998 320999 321000 321001 321002 321003 [...] (157169 flits)
Class 0:
Remaining flits: 32119 32120 32121 32122 32123 32124 32125 32126 32127 32128 [...] (145044 flits)
Measured flits: 321138 321139 321140 321141 321142 321143 321144 321145 321146 321147 [...] (127400 flits)
Class 0:
Remaining flits: 41706 41707 41708 41709 41710 41711 41712 41713 41714 41715 [...] (112632 flits)
Measured flits: 321174 321175 321176 321177 321178 321179 321180 321181 321182 321183 [...] (99051 flits)
Class 0:
Remaining flits: 41718 41719 41720 41721 41722 41723 43866 43867 43868 43869 [...] (81995 flits)
Measured flits: 321181 321182 321183 321184 321185 321186 321187 321188 321189 321190 [...] (72018 flits)
Class 0:
Remaining flits: 54289 54290 54291 54292 54293 54294 54295 54296 54297 54298 [...] (52655 flits)
Measured flits: 321678 321679 321680 321681 321682 321683 321684 321685 321686 321687 [...] (45834 flits)
Class 0:
Remaining flits: 56484 56485 56486 56487 56488 56489 56490 56491 56492 56493 [...] (27945 flits)
Measured flits: 321678 321679 321680 321681 321682 321683 321684 321685 321686 321687 [...] (24251 flits)
Class 0:
Remaining flits: 101783 101784 101785 101786 101787 101788 101789 108846 108847 108848 [...] (10200 flits)
Measured flits: 321732 321733 321734 321735 321736 321737 321738 321739 321740 321741 [...] (8665 flits)
Class 0:
Remaining flits: 217116 217117 217118 217119 217120 217121 217122 217123 217124 217125 [...] (2515 flits)
Measured flits: 322740 322741 322742 322743 322744 322745 322746 322747 322748 322749 [...] (2178 flits)
Class 0:
Remaining flits: 294408 294409 294410 294411 294412 294413 294414 294415 294416 294417 [...] (310 flits)
Measured flits: 356656 356657 356658 356659 356660 356661 356662 356663 356664 356665 [...] (270 flits)
Time taken is 19247 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6482.42 (1 samples)
	minimum = 27 (1 samples)
	maximum = 16626 (1 samples)
Network latency average = 6184.78 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15815 (1 samples)
Flit latency average = 5631.65 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16494 (1 samples)
Fragmentation average = 291.797 (1 samples)
	minimum = 0 (1 samples)
	maximum = 8365 (1 samples)
Injected packet rate average = 0.0321302 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0139583 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.578356 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.253731 (1 samples)
	minimum = 0.172333 (1 samples)
	maximum = 0.356 (1 samples)
Injected packet size average = 18.0004 (1 samples)
Accepted packet size average = 18.1777 (1 samples)
Hops average = 5.06452 (1 samples)
Total run time 25.2155
