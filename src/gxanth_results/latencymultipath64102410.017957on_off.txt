BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.998
	minimum = 27
	maximum = 965
Network latency average = 239.995
	minimum = 23
	maximum = 939
Slowest packet = 226
Flit latency average = 165.491
	minimum = 6
	maximum = 947
Slowest flit = 2553
Fragmentation average = 140.32
	minimum = 0
	maximum = 894
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00920833
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.189839
	minimum = 0.09 (at node 120)
	maximum = 0.344 (at node 76)
Injected packet length average = 17.8264
Accepted packet length average = 20.616
Total in-flight flits = 25561 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 453.447
	minimum = 24
	maximum = 1894
Network latency average = 382.223
	minimum = 23
	maximum = 1824
Slowest packet = 340
Flit latency average = 292.051
	minimum = 6
	maximum = 1807
Slowest flit = 7343
Fragmentation average = 191.42
	minimum = 0
	maximum = 1370
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0103724
	minimum = 0.005 (at node 30)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.202909
	minimum = 0.101 (at node 30)
	maximum = 0.3165 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 19.5624
Total in-flight flits = 41675 (0 measured)
latency change    = 0.34061
throughput change = 0.0644147
Class 0:
Packet latency average = 836.554
	minimum = 27
	maximum = 2833
Network latency average = 742.083
	minimum = 23
	maximum = 2586
Slowest packet = 722
Flit latency average = 630.223
	minimum = 6
	maximum = 2778
Slowest flit = 12810
Fragmentation average = 272.584
	minimum = 0
	maximum = 2193
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0123333
	minimum = 0.004 (at node 180)
	maximum = 0.02 (at node 51)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.227422
	minimum = 0.078 (at node 180)
	maximum = 0.364 (at node 50)
Injected packet length average = 17.9876
Accepted packet length average = 18.4396
Total in-flight flits = 60380 (0 measured)
latency change    = 0.457959
throughput change = 0.107787
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 391.363
	minimum = 27
	maximum = 1462
Network latency average = 303.224
	minimum = 23
	maximum = 991
Slowest packet = 10126
Flit latency average = 833.872
	minimum = 6
	maximum = 3749
Slowest flit = 14242
Fragmentation average = 159.497
	minimum = 0
	maximum = 658
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0126823
	minimum = 0.005 (at node 36)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.231026
	minimum = 0.085 (at node 36)
	maximum = 0.393 (at node 123)
Injected packet length average = 18.0183
Accepted packet length average = 18.2164
Total in-flight flits = 77961 (44917 measured)
latency change    = 1.13754
throughput change = 0.0156007
Class 0:
Packet latency average = 640.514
	minimum = 26
	maximum = 2509
Network latency average = 555.982
	minimum = 23
	maximum = 1959
Slowest packet = 10126
Flit latency average = 962.334
	minimum = 6
	maximum = 4727
Slowest flit = 16248
Fragmentation average = 212.759
	minimum = 0
	maximum = 1748
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.012724
	minimum = 0.0055 (at node 64)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.231565
	minimum = 0.1025 (at node 64)
	maximum = 0.3375 (at node 78)
Injected packet length average = 18.0075
Accepted packet length average = 18.1991
Total in-flight flits = 91105 (71535 measured)
latency change    = 0.388986
throughput change = 0.00232791
Class 0:
Packet latency average = 850.682
	minimum = 25
	maximum = 3170
Network latency average = 763.723
	minimum = 23
	maximum = 2962
Slowest packet = 10126
Flit latency average = 1078.34
	minimum = 6
	maximum = 5816
Slowest flit = 11200
Fragmentation average = 243.874
	minimum = 0
	maximum = 2061
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0127361
	minimum = 0.00766667 (at node 36)
	maximum = 0.0176667 (at node 6)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.232361
	minimum = 0.134333 (at node 36)
	maximum = 0.316333 (at node 11)
Injected packet length average = 18.014
Accepted packet length average = 18.2443
Total in-flight flits = 107674 (95626 measured)
latency change    = 0.247058
throughput change = 0.00342573
Class 0:
Packet latency average = 1053.42
	minimum = 25
	maximum = 4374
Network latency average = 962.903
	minimum = 23
	maximum = 3957
Slowest packet = 10126
Flit latency average = 1202.05
	minimum = 6
	maximum = 6689
Slowest flit = 15587
Fragmentation average = 261.261
	minimum = 0
	maximum = 2860
Injected packet rate average = 0.017625
	minimum = 0.0025 (at node 120)
	maximum = 0.038 (at node 17)
Accepted packet rate average = 0.0127083
	minimum = 0.008 (at node 36)
	maximum = 0.01675 (at node 129)
Injected flit rate average = 0.317267
	minimum = 0.045 (at node 120)
	maximum = 0.683 (at node 17)
Accepted flit rate average= 0.231257
	minimum = 0.15025 (at node 36)
	maximum = 0.3015 (at node 11)
Injected packet length average = 18.001
Accepted packet length average = 18.1972
Total in-flight flits = 126423 (118807 measured)
latency change    = 0.192459
throughput change = 0.00477652
Class 0:
Packet latency average = 1255.57
	minimum = 25
	maximum = 5072
Network latency average = 1164.32
	minimum = 23
	maximum = 4893
Slowest packet = 10126
Flit latency average = 1328.51
	minimum = 6
	maximum = 7324
Slowest flit = 31355
Fragmentation average = 278.663
	minimum = 0
	maximum = 3220
Injected packet rate average = 0.0175719
	minimum = 0.0032 (at node 89)
	maximum = 0.0388 (at node 23)
Accepted packet rate average = 0.0127188
	minimum = 0.0086 (at node 64)
	maximum = 0.0162 (at node 123)
Injected flit rate average = 0.31629
	minimum = 0.0576 (at node 89)
	maximum = 0.6994 (at node 23)
Accepted flit rate average= 0.230734
	minimum = 0.1624 (at node 64)
	maximum = 0.2896 (at node 123)
Injected packet length average = 17.9998
Accepted packet length average = 18.1413
Total in-flight flits = 142517 (137560 measured)
latency change    = 0.161
throughput change = 0.00226293
Class 0:
Packet latency average = 1430.35
	minimum = 25
	maximum = 6205
Network latency average = 1337.21
	minimum = 23
	maximum = 5933
Slowest packet = 10126
Flit latency average = 1440.32
	minimum = 6
	maximum = 8726
Slowest flit = 15365
Fragmentation average = 291.253
	minimum = 0
	maximum = 4531
Injected packet rate average = 0.0177665
	minimum = 0.00516667 (at node 89)
	maximum = 0.0361667 (at node 23)
Accepted packet rate average = 0.0127396
	minimum = 0.0095 (at node 42)
	maximum = 0.0161667 (at node 157)
Injected flit rate average = 0.319868
	minimum = 0.093 (at node 89)
	maximum = 0.651 (at node 23)
Accepted flit rate average= 0.230805
	minimum = 0.167667 (at node 42)
	maximum = 0.298667 (at node 70)
Injected packet length average = 18.004
Accepted packet length average = 18.1171
Total in-flight flits = 162899 (159499 measured)
latency change    = 0.122191
throughput change = 0.000304641
Class 0:
Packet latency average = 1597.48
	minimum = 25
	maximum = 6999
Network latency average = 1503.18
	minimum = 23
	maximum = 6862
Slowest packet = 10274
Flit latency average = 1558.03
	minimum = 6
	maximum = 9453
Slowest flit = 15749
Fragmentation average = 303.672
	minimum = 0
	maximum = 6016
Injected packet rate average = 0.017744
	minimum = 0.00557143 (at node 89)
	maximum = 0.0318571 (at node 23)
Accepted packet rate average = 0.0127135
	minimum = 0.01 (at node 42)
	maximum = 0.0161429 (at node 138)
Injected flit rate average = 0.319408
	minimum = 0.100286 (at node 89)
	maximum = 0.575429 (at node 23)
Accepted flit rate average= 0.230002
	minimum = 0.18 (at node 42)
	maximum = 0.294429 (at node 138)
Injected packet length average = 18.0009
Accepted packet length average = 18.0911
Total in-flight flits = 180521 (178065 measured)
latency change    = 0.104625
throughput change = 0.0034889
Draining all recorded packets ...
Class 0:
Remaining flits: 39564 39565 39566 39567 39568 39569 39570 39571 39572 39573 [...] (202504 flits)
Measured flits: 183078 183079 183080 183081 183082 183083 183084 183085 183086 183087 [...] (149720 flits)
Class 0:
Remaining flits: 39564 39565 39566 39567 39568 39569 39570 39571 39572 39573 [...] (221576 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (120919 flits)
Class 0:
Remaining flits: 39564 39565 39566 39567 39568 39569 39570 39571 39572 39573 [...] (240346 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (97494 flits)
Class 0:
Remaining flits: 39564 39565 39566 39567 39568 39569 39570 39571 39572 39573 [...] (262420 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (78198 flits)
Class 0:
Remaining flits: 39564 39565 39566 39567 39568 39569 39570 39571 39572 39573 [...] (285469 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (63829 flits)
Class 0:
Remaining flits: 39575 39576 39577 39578 39579 39580 39581 47979 47980 47981 [...] (307093 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (51719 flits)
Class 0:
Remaining flits: 39578 39579 39580 39581 103230 103231 103232 103233 103234 103235 [...] (328366 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (41663 flits)
Class 0:
Remaining flits: 103230 103231 103232 103233 103234 103235 103236 103237 103238 103239 [...] (352662 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (33751 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (372688 flits)
Measured flits: 184356 184357 184358 184359 184360 184361 184362 184363 184364 184365 [...] (28222 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (395121 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (23131 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (415610 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (18709 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (437260 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (14987 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (462808 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (12105 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (487597 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (10013 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (515790 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (8342 flits)
Class 0:
Remaining flits: 103237 103238 103239 103240 103241 103242 103243 103244 103245 103246 [...] (540570 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (6905 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (562413 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (5669 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (590045 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (4777 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (614724 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (4085 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (638415 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (3529 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (663646 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (2809 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (691105 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (2186 flits)
Class 0:
Remaining flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (719911 flits)
Measured flits: 185742 185743 185744 185745 185746 185747 185748 185749 185750 185751 [...] (1829 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (744521 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (1427 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (771357 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (1247 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (796511 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (1079 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (824219 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (967 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (851037 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (935 flits)
Class 0:
Remaining flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (874015 flits)
Measured flits: 282168 282169 282170 282171 282172 282173 282174 282175 282176 282177 [...] (774 flits)
Class 0:
Remaining flits: 282172 282173 282174 282175 282176 282177 282178 282179 282180 282181 [...] (899728 flits)
Measured flits: 282172 282173 282174 282175 282176 282177 282178 282179 282180 282181 [...] (686 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (927539 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (591 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (954818 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (573 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (983715 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (530 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1006271 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (414 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1029594 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (369 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1049585 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (360 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1069480 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (342 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1091681 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (266 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1115300 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (252 flits)
Class 0:
Remaining flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (1135155 flits)
Measured flits: 283014 283015 283016 283017 283018 283019 283020 283021 283022 283023 [...] (216 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1160010 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (108 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1183134 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (90 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1206520 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (90 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1232130 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (82 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1256579 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (38 flits)
Class 0:
Remaining flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (1276031 flits)
Measured flits: 508140 508141 508142 508143 508144 508145 508146 508147 508148 508149 [...] (19 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 635040 635041 635042 635043 635044 635045 635046 635047 635048 635049 [...] (1259406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 635040 635041 635042 635043 635044 635045 635046 635047 635048 635049 [...] (1227126 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1194757 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1162210 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1129429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1096659 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1064681 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (1032163 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (999438 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 636768 636769 636770 636771 636772 636773 636774 636775 636776 636777 [...] (967062 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (934493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (902282 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (870217 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (838296 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (806747 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (775417 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (743520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (710794 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (679608 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (648092 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (615697 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (584501 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (552132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 784332 784333 784334 784335 784336 784337 784338 784339 784340 784341 [...] (519807 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (487381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (455995 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (424783 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (393017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (360787 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 903150 903151 903152 903153 903154 903155 903156 903157 903158 903159 [...] (328782 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (296937 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (265750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (234476 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (203474 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (172885 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (143170 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1127412 1127413 1127414 1127415 1127416 1127417 1127418 1127419 1127420 1127421 [...] (115654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270448 1270449 1270450 1270451 1270452 1270453 1270454 1270455 1270456 1270457 [...] (90801 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284804 1284805 1284806 1284807 1284808 1284809 1284810 1284811 1284812 1284813 [...] (68270 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284804 1284805 1284806 1284807 1284808 1284809 1284810 1284811 1284812 1284813 [...] (49076 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1414404 1414405 1414406 1414407 1414408 1414409 1414410 1414411 1414412 1414413 [...] (32761 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1414404 1414405 1414406 1414407 1414408 1414409 1414410 1414411 1414412 1414413 [...] (20222 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1759374 1759375 1759376 1759377 1759378 1759379 1759380 1759381 1759382 1759383 [...] (11666 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1759374 1759375 1759376 1759377 1759378 1759379 1759380 1759381 1759382 1759383 [...] (6145 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1759374 1759375 1759376 1759377 1759378 1759379 1759380 1759381 1759382 1759383 [...] (3069 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1759374 1759375 1759376 1759377 1759378 1759379 1759380 1759381 1759382 1759383 [...] (730 flits)
Measured flits: (0 flits)
Time taken is 103542 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3975.01 (1 samples)
	minimum = 25 (1 samples)
	maximum = 48754 (1 samples)
Network latency average = 3873.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 48572 (1 samples)
Flit latency average = 18070.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 76346 (1 samples)
Fragmentation average = 379.098 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14278 (1 samples)
Injected packet rate average = 0.017744 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0318571 (1 samples)
Accepted packet rate average = 0.0127135 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0161429 (1 samples)
Injected flit rate average = 0.319408 (1 samples)
	minimum = 0.100286 (1 samples)
	maximum = 0.575429 (1 samples)
Accepted flit rate average = 0.230002 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.294429 (1 samples)
Injected packet size average = 18.0009 (1 samples)
Accepted packet size average = 18.0911 (1 samples)
Hops average = 5.0647 (1 samples)
Total run time 197.24
