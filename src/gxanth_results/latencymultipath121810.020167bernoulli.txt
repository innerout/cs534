BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 259.716
	minimum = 23
	maximum = 871
Network latency average = 232.607
	minimum = 23
	maximum = 838
Slowest packet = 407
Flit latency average = 192.73
	minimum = 6
	maximum = 851
Slowest flit = 8920
Fragmentation average = 49.7112
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0136875
	minimum = 0.005 (at node 0)
	maximum = 0.026 (at node 146)
Accepted packet rate average = 0.009
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 140)
Injected flit rate average = 0.242922
	minimum = 0.09 (at node 0)
	maximum = 0.453 (at node 146)
Accepted flit rate average= 0.167964
	minimum = 0.05 (at node 41)
	maximum = 0.307 (at node 140)
Injected packet length average = 17.7477
Accepted packet length average = 18.6626
Total in-flight flits = 16765 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 512.391
	minimum = 23
	maximum = 1648
Network latency average = 357.423
	minimum = 23
	maximum = 1648
Slowest packet = 1164
Flit latency average = 308.248
	minimum = 6
	maximum = 1631
Slowest flit = 20969
Fragmentation average = 54.0087
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0113906
	minimum = 0.0045 (at node 99)
	maximum = 0.018 (at node 175)
Accepted packet rate average = 0.00894271
	minimum = 0.0045 (at node 164)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.203104
	minimum = 0.081 (at node 99)
	maximum = 0.317 (at node 175)
Accepted flit rate average= 0.163865
	minimum = 0.0835 (at node 174)
	maximum = 0.261 (at node 22)
Injected packet length average = 17.8308
Accepted packet length average = 18.3238
Total in-flight flits = 17680 (0 measured)
latency change    = 0.49313
throughput change = 0.0250143
Class 0:
Packet latency average = 1319.14
	minimum = 250
	maximum = 2534
Network latency average = 550.602
	minimum = 23
	maximum = 2448
Slowest packet = 1282
Flit latency average = 485.78
	minimum = 6
	maximum = 2431
Slowest flit = 23093
Fragmentation average = 58.5352
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00872396
	minimum = 0 (at node 32)
	maximum = 0.02 (at node 40)
Accepted packet rate average = 0.00880729
	minimum = 0.001 (at node 153)
	maximum = 0.018 (at node 14)
Injected flit rate average = 0.156276
	minimum = 0 (at node 32)
	maximum = 0.376 (at node 40)
Accepted flit rate average= 0.157891
	minimum = 0.001 (at node 153)
	maximum = 0.309 (at node 14)
Injected packet length average = 17.9134
Accepted packet length average = 17.9273
Total in-flight flits = 17497 (0 measured)
latency change    = 0.611572
throughput change = 0.0378361
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1903.7
	minimum = 885
	maximum = 3303
Network latency average = 333.651
	minimum = 25
	maximum = 962
Slowest packet = 6157
Flit latency average = 489.922
	minimum = 6
	maximum = 3130
Slowest flit = 26657
Fragmentation average = 52.1965
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00910938
	minimum = 0 (at node 24)
	maximum = 0.023 (at node 32)
Accepted packet rate average = 0.00896875
	minimum = 0.001 (at node 96)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.164917
	minimum = 0 (at node 24)
	maximum = 0.411 (at node 32)
Accepted flit rate average= 0.162786
	minimum = 0.033 (at node 96)
	maximum = 0.305 (at node 34)
Injected packet length average = 18.1041
Accepted packet length average = 18.1504
Total in-flight flits = 17922 (16197 measured)
latency change    = 0.307066
throughput change = 0.0300752
Class 0:
Packet latency average = 2231.21
	minimum = 885
	maximum = 4082
Network latency average = 443.357
	minimum = 23
	maximum = 1860
Slowest packet = 6157
Flit latency average = 488.951
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 54.6421
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0089349
	minimum = 0.001 (at node 111)
	maximum = 0.0185 (at node 46)
Accepted packet rate average = 0.00890365
	minimum = 0.004 (at node 96)
	maximum = 0.0155 (at node 34)
Injected flit rate average = 0.161164
	minimum = 0.018 (at node 111)
	maximum = 0.333 (at node 46)
Accepted flit rate average= 0.160568
	minimum = 0.07 (at node 113)
	maximum = 0.279 (at node 34)
Injected packet length average = 18.0376
Accepted packet length average = 18.0339
Total in-flight flits = 17705 (17536 measured)
latency change    = 0.146784
throughput change = 0.0138182
Class 0:
Packet latency average = 2547
	minimum = 885
	maximum = 4918
Network latency average = 488.742
	minimum = 23
	maximum = 2667
Slowest packet = 6157
Flit latency average = 493.329
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 56.9143
	minimum = 0
	maximum = 131
Injected packet rate average = 0.00903819
	minimum = 0.00233333 (at node 141)
	maximum = 0.0156667 (at node 46)
Accepted packet rate average = 0.0090191
	minimum = 0.00466667 (at node 113)
	maximum = 0.015 (at node 16)
Injected flit rate average = 0.163122
	minimum = 0.042 (at node 141)
	maximum = 0.284 (at node 168)
Accepted flit rate average= 0.162835
	minimum = 0.084 (at node 113)
	maximum = 0.274 (at node 16)
Injected packet length average = 18.048
Accepted packet length average = 18.0545
Total in-flight flits = 17502 (17502 measured)
latency change    = 0.123988
throughput change = 0.0139243
Class 0:
Packet latency average = 2830.3
	minimum = 885
	maximum = 5626
Network latency average = 504.984
	minimum = 23
	maximum = 2667
Slowest packet = 6157
Flit latency average = 490.419
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 57.4888
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00900911
	minimum = 0.00375 (at node 37)
	maximum = 0.01625 (at node 129)
Accepted packet rate average = 0.00898307
	minimum = 0.0045 (at node 4)
	maximum = 0.01275 (at node 16)
Injected flit rate average = 0.162501
	minimum = 0.0705 (at node 51)
	maximum = 0.29275 (at node 129)
Accepted flit rate average= 0.162243
	minimum = 0.084 (at node 4)
	maximum = 0.23325 (at node 78)
Injected packet length average = 18.0374
Accepted packet length average = 18.061
Total in-flight flits = 17454 (17454 measured)
latency change    = 0.100093
throughput change = 0.00364625
Class 0:
Packet latency average = 3111.57
	minimum = 885
	maximum = 6120
Network latency average = 512.937
	minimum = 23
	maximum = 2667
Slowest packet = 6157
Flit latency average = 487.511
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 57.7819
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00898438
	minimum = 0.0038 (at node 90)
	maximum = 0.0158 (at node 129)
Accepted packet rate average = 0.00898229
	minimum = 0.0058 (at node 36)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.161952
	minimum = 0.0682 (at node 90)
	maximum = 0.2854 (at node 129)
Accepted flit rate average= 0.161825
	minimum = 0.1064 (at node 36)
	maximum = 0.2312 (at node 128)
Injected packet length average = 18.026
Accepted packet length average = 18.016
Total in-flight flits = 17575 (17575 measured)
latency change    = 0.0903942
throughput change = 0.00258606
Class 0:
Packet latency average = 3398
	minimum = 885
	maximum = 6694
Network latency average = 518.495
	minimum = 23
	maximum = 2667
Slowest packet = 6157
Flit latency average = 486.645
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 57.8735
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0090217
	minimum = 0.00416667 (at node 141)
	maximum = 0.0156667 (at node 129)
Accepted packet rate average = 0.0090026
	minimum = 0.00616667 (at node 36)
	maximum = 0.0133333 (at node 128)
Injected flit rate average = 0.162511
	minimum = 0.0741667 (at node 141)
	maximum = 0.282833 (at node 129)
Accepted flit rate average= 0.162222
	minimum = 0.109833 (at node 36)
	maximum = 0.2415 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 18.0195
Total in-flight flits = 17763 (17763 measured)
latency change    = 0.0842954
throughput change = 0.00244863
Class 0:
Packet latency average = 3679.14
	minimum = 885
	maximum = 7292
Network latency average = 523.743
	minimum = 23
	maximum = 2667
Slowest packet = 6157
Flit latency average = 486.959
	minimum = 6
	maximum = 3259
Slowest flit = 57150
Fragmentation average = 57.9869
	minimum = 0
	maximum = 159
Injected packet rate average = 0.00901711
	minimum = 0.00442857 (at node 141)
	maximum = 0.014 (at node 129)
Accepted packet rate average = 0.00900298
	minimum = 0.00642857 (at node 36)
	maximum = 0.0122857 (at node 128)
Injected flit rate average = 0.162487
	minimum = 0.0792857 (at node 141)
	maximum = 0.252571 (at node 129)
Accepted flit rate average= 0.162321
	minimum = 0.114714 (at node 36)
	maximum = 0.222714 (at node 128)
Injected packet length average = 18.0198
Accepted packet length average = 18.0297
Total in-flight flits = 17660 (17660 measured)
latency change    = 0.0764143
throughput change = 0.000606591
Draining all recorded packets ...
Class 0:
Remaining flits: 287661 287662 287663 287664 287665 287666 287667 287668 287669 287670 [...] (17657 flits)
Measured flits: 287661 287662 287663 287664 287665 287666 287667 287668 287669 287670 [...] (17657 flits)
Class 0:
Remaining flits: 292634 292635 292636 292637 292638 292639 292640 292641 292642 292643 [...] (17700 flits)
Measured flits: 292634 292635 292636 292637 292638 292639 292640 292641 292642 292643 [...] (17700 flits)
Class 0:
Remaining flits: 338832 338833 338834 338835 338836 338837 338838 338839 338840 338841 [...] (17684 flits)
Measured flits: 338832 338833 338834 338835 338836 338837 338838 338839 338840 338841 [...] (17684 flits)
Class 0:
Remaining flits: 346517 375363 375364 375365 375366 375367 375368 375369 375370 375371 [...] (17627 flits)
Measured flits: 346517 375363 375364 375365 375366 375367 375368 375369 375370 375371 [...] (17627 flits)
Class 0:
Remaining flits: 413514 413515 413516 413517 413518 413519 413520 413521 413522 413523 [...] (17769 flits)
Measured flits: 413514 413515 413516 413517 413518 413519 413520 413521 413522 413523 [...] (17769 flits)
Class 0:
Remaining flits: 423432 423433 423434 423435 423436 423437 423438 423439 423440 423441 [...] (18298 flits)
Measured flits: 423432 423433 423434 423435 423436 423437 423438 423439 423440 423441 [...] (18190 flits)
Class 0:
Remaining flits: 479556 479557 479558 479559 479560 479561 479562 479563 479564 479565 [...] (17746 flits)
Measured flits: 479556 479557 479558 479559 479560 479561 479562 479563 479564 479565 [...] (17389 flits)
Class 0:
Remaining flits: 515034 515035 515036 515037 515038 515039 515040 515041 515042 515043 [...] (17677 flits)
Measured flits: 515034 515035 515036 515037 515038 515039 515040 515041 515042 515043 [...] (16627 flits)
Class 0:
Remaining flits: 528696 528697 528698 528699 528700 528701 528702 528703 528704 528705 [...] (17500 flits)
Measured flits: 528696 528697 528698 528699 528700 528701 528702 528703 528704 528705 [...] (15573 flits)
Class 0:
Remaining flits: 547614 547615 547616 547617 547618 547619 547620 547621 547622 547623 [...] (17830 flits)
Measured flits: 547614 547615 547616 547617 547618 547619 547620 547621 547622 547623 [...] (14410 flits)
Class 0:
Remaining flits: 593244 593245 593246 593247 593248 593249 593250 593251 593252 593253 [...] (17741 flits)
Measured flits: 593244 593245 593246 593247 593248 593249 593250 593251 593252 593253 [...] (12201 flits)
Class 0:
Remaining flits: 613188 613189 613190 613191 613192 613193 613194 613195 613196 613197 [...] (18028 flits)
Measured flits: 613188 613189 613190 613191 613192 613193 613194 613195 613196 613197 [...] (9628 flits)
Class 0:
Remaining flits: 662382 662383 662384 662385 662386 662387 662388 662389 662390 662391 [...] (17753 flits)
Measured flits: 662382 662383 662384 662385 662386 662387 662388 662389 662390 662391 [...] (7457 flits)
Class 0:
Remaining flits: 672185 672186 672187 672188 672189 672190 672191 680310 680311 680312 [...] (17806 flits)
Measured flits: 672185 672186 672187 672188 672189 672190 672191 680310 680311 680312 [...] (5589 flits)
Class 0:
Remaining flits: 717840 717841 717842 717843 717844 717845 717846 717847 717848 717849 [...] (17613 flits)
Measured flits: 722961 722962 722963 722964 722965 722966 722967 722968 722969 737946 [...] (3451 flits)
Class 0:
Remaining flits: 747774 747775 747776 747777 747778 747779 747780 747781 747782 747783 [...] (17679 flits)
Measured flits: 759636 759637 759638 759639 759640 759641 759642 759643 759644 759645 [...] (1743 flits)
Class 0:
Remaining flits: 768204 768205 768206 768207 768208 768209 768210 768211 768212 768213 [...] (17754 flits)
Measured flits: 788994 788995 788996 788997 788998 788999 789000 789001 789002 789003 [...] (721 flits)
Class 0:
Remaining flits: 815040 815041 815042 815043 815044 815045 815046 815047 815048 815049 [...] (17895 flits)
Measured flits: 819486 819487 819488 819489 819490 819491 819492 819493 819494 819495 [...] (432 flits)
Class 0:
Remaining flits: 846072 846073 846074 846075 846076 846077 846078 846079 846080 846081 [...] (17752 flits)
Measured flits: 863352 863353 863354 863355 863356 863357 863358 863359 863360 863361 [...] (228 flits)
Class 0:
Remaining flits: 863352 863353 863354 863355 863356 863357 863358 863359 863360 863361 [...] (17764 flits)
Measured flits: 863352 863353 863354 863355 863356 863357 863358 863359 863360 863361 [...] (180 flits)
Class 0:
Remaining flits: 887562 887563 887564 887565 887566 887567 887568 887569 887570 887571 [...] (17844 flits)
Measured flits: 907794 907795 907796 907797 907798 907799 907800 907801 907802 907803 [...] (72 flits)
Draining remaining packets ...
Time taken is 32075 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7526.7 (1 samples)
	minimum = 885 (1 samples)
	maximum = 21318 (1 samples)
Network latency average = 549.223 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3425 (1 samples)
Flit latency average = 491.171 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3379 (1 samples)
Fragmentation average = 58.4651 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.00901711 (1 samples)
	minimum = 0.00442857 (1 samples)
	maximum = 0.014 (1 samples)
Accepted packet rate average = 0.00900298 (1 samples)
	minimum = 0.00642857 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.162487 (1 samples)
	minimum = 0.0792857 (1 samples)
	maximum = 0.252571 (1 samples)
Accepted flit rate average = 0.162321 (1 samples)
	minimum = 0.114714 (1 samples)
	maximum = 0.222714 (1 samples)
Injected packet size average = 18.0198 (1 samples)
Accepted packet size average = 18.0297 (1 samples)
Hops average = 5.06889 (1 samples)
Total run time 23.3329
