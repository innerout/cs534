BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 236.764
	minimum = 27
	maximum = 907
Network latency average = 192.804
	minimum = 23
	maximum = 848
Slowest packet = 265
Flit latency average = 124.889
	minimum = 6
	maximum = 861
Slowest flit = 7028
Fragmentation average = 110.394
	minimum = 0
	maximum = 678
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00819271
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 70)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.163031
	minimum = 0.036 (at node 174)
	maximum = 0.318 (at node 70)
Injected packet length average = 17.8411
Accepted packet length average = 19.8996
Total in-flight flits = 9828 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 315.892
	minimum = 27
	maximum = 1736
Network latency average = 267.434
	minimum = 23
	maximum = 1622
Slowest packet = 472
Flit latency average = 186.107
	minimum = 6
	maximum = 1605
Slowest flit = 12113
Fragmentation average = 143.4
	minimum = 0
	maximum = 1219
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00893229
	minimum = 0.005 (at node 23)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.171867
	minimum = 0.098 (at node 79)
	maximum = 0.291 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 19.2411
Total in-flight flits = 13599 (0 measured)
latency change    = 0.250491
throughput change = 0.0514114
Class 0:
Packet latency average = 461.676
	minimum = 23
	maximum = 2382
Network latency average = 414.588
	minimum = 23
	maximum = 2335
Slowest packet = 852
Flit latency average = 310.928
	minimum = 6
	maximum = 2693
Slowest flit = 10544
Fragmentation average = 200.358
	minimum = 0
	maximum = 2229
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0106615
	minimum = 0.004 (at node 117)
	maximum = 0.018 (at node 6)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.192875
	minimum = 0.079 (at node 116)
	maximum = 0.343 (at node 16)
Injected packet length average = 17.9936
Accepted packet length average = 18.0909
Total in-flight flits = 15663 (0 measured)
latency change    = 0.31577
throughput change = 0.108919
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 337.67
	minimum = 23
	maximum = 1107
Network latency average = 291.285
	minimum = 23
	maximum = 996
Slowest packet = 6606
Flit latency average = 360.877
	minimum = 6
	maximum = 3361
Slowest flit = 24389
Fragmentation average = 169.193
	minimum = 0
	maximum = 824
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.0107344
	minimum = 0.003 (at node 163)
	maximum = 0.022 (at node 120)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.195885
	minimum = 0.071 (at node 163)
	maximum = 0.414 (at node 159)
Injected packet length average = 18.0247
Accepted packet length average = 18.2484
Total in-flight flits = 18805 (15057 measured)
latency change    = 0.367237
throughput change = 0.0153683
Class 0:
Packet latency average = 433.174
	minimum = 23
	maximum = 1908
Network latency average = 384
	minimum = 23
	maximum = 1797
Slowest packet = 6622
Flit latency average = 398.966
	minimum = 6
	maximum = 4734
Slowest flit = 11607
Fragmentation average = 193.515
	minimum = 0
	maximum = 1414
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.0108125
	minimum = 0.005 (at node 17)
	maximum = 0.0185 (at node 120)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.195789
	minimum = 0.093 (at node 17)
	maximum = 0.323 (at node 120)
Injected packet length average = 17.9755
Accepted packet length average = 18.1077
Total in-flight flits = 20598 (19039 measured)
latency change    = 0.220474
throughput change = 0.000492133
Class 0:
Packet latency average = 494.834
	minimum = 23
	maximum = 2767
Network latency average = 444.126
	minimum = 23
	maximum = 2713
Slowest packet = 6713
Flit latency average = 423.185
	minimum = 6
	maximum = 5278
Slowest flit = 14795
Fragmentation average = 206.567
	minimum = 0
	maximum = 1870
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0108229
	minimum = 0.006 (at node 4)
	maximum = 0.0156667 (at node 99)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.195484
	minimum = 0.109333 (at node 4)
	maximum = 0.268667 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 18.0621
Total in-flight flits = 19974 (19045 measured)
latency change    = 0.124608
throughput change = 0.00155863
Class 0:
Packet latency average = 536.924
	minimum = 23
	maximum = 3884
Network latency average = 485.309
	minimum = 23
	maximum = 3838
Slowest packet = 6632
Flit latency average = 440.007
	minimum = 6
	maximum = 5559
Slowest flit = 22139
Fragmentation average = 217.345
	minimum = 0
	maximum = 2506
Injected packet rate average = 0.0112526
	minimum = 0.003 (at node 120)
	maximum = 0.028 (at node 81)
Accepted packet rate average = 0.0108112
	minimum = 0.007 (at node 163)
	maximum = 0.01525 (at node 129)
Injected flit rate average = 0.202583
	minimum = 0.054 (at node 120)
	maximum = 0.504 (at node 81)
Accepted flit rate average= 0.195453
	minimum = 0.1305 (at node 84)
	maximum = 0.2755 (at node 129)
Injected packet length average = 18.0032
Accepted packet length average = 18.0788
Total in-flight flits = 21111 (20512 measured)
latency change    = 0.07839
throughput change = 0.000159885
Class 0:
Packet latency average = 567.28
	minimum = 23
	maximum = 4500
Network latency average = 515.538
	minimum = 23
	maximum = 4451
Slowest packet = 6812
Flit latency average = 453.641
	minimum = 6
	maximum = 6919
Slowest flit = 43369
Fragmentation average = 225.185
	minimum = 0
	maximum = 3320
Injected packet rate average = 0.0112927
	minimum = 0.005 (at node 91)
	maximum = 0.028 (at node 153)
Accepted packet rate average = 0.0108094
	minimum = 0.0074 (at node 79)
	maximum = 0.0154 (at node 129)
Injected flit rate average = 0.203272
	minimum = 0.09 (at node 91)
	maximum = 0.506 (at node 153)
Accepted flit rate average= 0.195727
	minimum = 0.1348 (at node 79)
	maximum = 0.2672 (at node 129)
Injected packet length average = 18.0003
Accepted packet length average = 18.1072
Total in-flight flits = 22903 (22526 measured)
latency change    = 0.0535114
throughput change = 0.0013997
Class 0:
Packet latency average = 589.737
	minimum = 23
	maximum = 5593
Network latency average = 538.261
	minimum = 23
	maximum = 5533
Slowest packet = 6817
Flit latency average = 467.279
	minimum = 6
	maximum = 7613
Slowest flit = 45263
Fragmentation average = 230.433
	minimum = 0
	maximum = 3661
Injected packet rate average = 0.0112187
	minimum = 0.00466667 (at node 29)
	maximum = 0.0271667 (at node 153)
Accepted packet rate average = 0.0108759
	minimum = 0.00766667 (at node 79)
	maximum = 0.0145 (at node 128)
Injected flit rate average = 0.201935
	minimum = 0.084 (at node 29)
	maximum = 0.490167 (at node 153)
Accepted flit rate average= 0.19661
	minimum = 0.139167 (at node 79)
	maximum = 0.265667 (at node 128)
Injected packet length average = 17.9998
Accepted packet length average = 18.0777
Total in-flight flits = 21800 (21584 measured)
latency change    = 0.0380801
throughput change = 0.00449193
Class 0:
Packet latency average = 607.739
	minimum = 23
	maximum = 6373
Network latency average = 556.335
	minimum = 23
	maximum = 6287
Slowest packet = 6964
Flit latency average = 474.659
	minimum = 6
	maximum = 7613
Slowest flit = 45263
Fragmentation average = 235.076
	minimum = 0
	maximum = 4205
Injected packet rate average = 0.0114658
	minimum = 0.00414286 (at node 29)
	maximum = 0.0238571 (at node 153)
Accepted packet rate average = 0.0109137
	minimum = 0.00785714 (at node 79)
	maximum = 0.0144286 (at node 128)
Injected flit rate average = 0.206394
	minimum = 0.0745714 (at node 29)
	maximum = 0.430857 (at node 153)
Accepted flit rate average= 0.197753
	minimum = 0.147714 (at node 79)
	maximum = 0.264 (at node 128)
Injected packet length average = 18.0008
Accepted packet length average = 18.1197
Total in-flight flits = 27263 (27080 measured)
latency change    = 0.0296209
throughput change = 0.00577859
Draining all recorded packets ...
Class 0:
Remaining flits: 18888 18889 18890 18891 18892 18893 18894 18895 18896 18897 [...] (27645 flits)
Measured flits: 118764 118765 118766 118767 118768 118769 118770 118771 118772 118773 [...] (10863 flits)
Class 0:
Remaining flits: 18888 18889 18890 18891 18892 18893 18894 18895 18896 18897 [...] (27524 flits)
Measured flits: 118764 118765 118766 118767 118768 118769 118770 118771 118772 118773 [...] (5802 flits)
Class 0:
Remaining flits: 78632 78633 78634 78635 78636 78637 78638 78639 78640 78641 [...] (25762 flits)
Measured flits: 118764 118765 118766 118767 118768 118769 118770 118771 118772 118773 [...] (3753 flits)
Class 0:
Remaining flits: 78640 78641 100944 100945 100946 100947 100948 100949 100950 100951 [...] (29330 flits)
Measured flits: 118774 118775 118776 118777 118778 118779 118780 118781 124518 124519 [...] (2626 flits)
Class 0:
Remaining flits: 100944 100945 100946 100947 100948 100949 100950 100951 100952 100953 [...] (30274 flits)
Measured flits: 124521 124522 124523 127043 166582 166583 166584 166585 166586 166587 [...] (1850 flits)
Class 0:
Remaining flits: 100944 100945 100946 100947 100948 100949 100950 100951 100952 100953 [...] (27352 flits)
Measured flits: 127043 166584 166585 166586 166587 166588 166589 192260 192261 192262 [...] (1424 flits)
Class 0:
Remaining flits: 100944 100945 100946 100947 100948 100949 100950 100951 100952 100953 [...] (28176 flits)
Measured flits: 192260 192261 192262 192263 192264 192265 192266 192267 192268 192269 [...] (902 flits)
Class 0:
Remaining flits: 100944 100945 100946 100947 100948 100949 100950 100951 100952 100953 [...] (29924 flits)
Measured flits: 192260 192261 192262 192263 192264 192265 192266 192267 192268 192269 [...] (703 flits)
Class 0:
Remaining flits: 192260 192261 192262 192263 192264 192265 192266 192267 192268 192269 [...] (30682 flits)
Measured flits: 192260 192261 192262 192263 192264 192265 192266 192267 192268 192269 [...] (617 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (32138 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (524 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (29060 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (415 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (30917 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (349 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (31811 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (291 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (30459 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (263 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (31726 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (209 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (35189 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (172 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (34525 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (172 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (35162 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (157 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (32231 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (155 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (33796 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (100 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (34255 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (56 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (36228 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (52 flits)
Class 0:
Remaining flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (37393 flits)
Measured flits: 194024 194025 194026 194027 194028 194029 194030 194031 194032 194033 [...] (52 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 286650 286651 [...] (39774 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 286650 286651 [...] (44 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 286650 286651 [...] (40893 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 286650 286651 [...] (44 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (38745 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (26 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (38801 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (26 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (39773 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (26 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (39510 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (26 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (39057 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391914 391915 [...] (26 flits)
Class 0:
Remaining flits: 194032 194033 194034 194035 194036 194037 194038 194039 391918 391919 [...] (39899 flits)
Measured flits: 194032 194033 194034 194035 194036 194037 194038 194039 391918 391919 [...] (22 flits)
Class 0:
Remaining flits: 391918 391919 391920 391921 391922 391923 391924 391925 391926 391927 [...] (40577 flits)
Measured flits: 391918 391919 391920 391921 391922 391923 391924 391925 391926 391927 [...] (14 flits)
Class 0:
Remaining flits: 391918 391919 391920 391921 391922 391923 391924 391925 391926 391927 [...] (42436 flits)
Measured flits: 391918 391919 391920 391921 391922 391923 391924 391925 391926 391927 [...] (14 flits)
Class 0:
Remaining flits: 391925 391926 391927 391928 391929 391930 391931 464221 464222 464223 [...] (42278 flits)
Measured flits: 391925 391926 391927 391928 391929 391930 391931 (7 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 502542 502543 502544 502545 502546 502547 502548 502549 502550 502551 [...] (15443 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 502542 502543 502544 502545 502546 502547 502548 502549 502550 502551 [...] (4968 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1382559 1382560 1382561 1400988 1400989 1400990 1400991 1400992 1400993 1405386 [...] (1247 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1735262 1735263 1735264 1735265 1735266 1735267 1735268 1735269 1735270 1735271 [...] (76 flits)
Measured flits: (0 flits)
Time taken is 48874 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 841.634 (1 samples)
	minimum = 23 (1 samples)
	maximum = 36684 (1 samples)
Network latency average = 788.064 (1 samples)
	minimum = 23 (1 samples)
	maximum = 36513 (1 samples)
Flit latency average = 816.227 (1 samples)
	minimum = 6 (1 samples)
	maximum = 36496 (1 samples)
Fragmentation average = 286.046 (1 samples)
	minimum = 0 (1 samples)
	maximum = 30787 (1 samples)
Injected packet rate average = 0.0114658 (1 samples)
	minimum = 0.00414286 (1 samples)
	maximum = 0.0238571 (1 samples)
Accepted packet rate average = 0.0109137 (1 samples)
	minimum = 0.00785714 (1 samples)
	maximum = 0.0144286 (1 samples)
Injected flit rate average = 0.206394 (1 samples)
	minimum = 0.0745714 (1 samples)
	maximum = 0.430857 (1 samples)
Accepted flit rate average = 0.197753 (1 samples)
	minimum = 0.147714 (1 samples)
	maximum = 0.264 (1 samples)
Injected packet size average = 18.0008 (1 samples)
Accepted packet size average = 18.1197 (1 samples)
Hops average = 5.07555 (1 samples)
Total run time 51.5412
