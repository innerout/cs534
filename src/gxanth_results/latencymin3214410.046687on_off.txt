BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 374.889
	minimum = 23
	maximum = 981
Network latency average = 273.31
	minimum = 23
	maximum = 959
Slowest packet = 46
Flit latency average = 210.039
	minimum = 6
	maximum = 950
Slowest flit = 3987
Fragmentation average = 142.889
	minimum = 0
	maximum = 885
Injected packet rate average = 0.0297969
	minimum = 0.001 (at node 182)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0104219
	minimum = 0.003 (at node 41)
	maximum = 0.021 (at node 88)
Injected flit rate average = 0.531026
	minimum = 0.018 (at node 182)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.21401
	minimum = 0.085 (at node 172)
	maximum = 0.402 (at node 88)
Injected packet length average = 17.8215
Accepted packet length average = 20.5347
Total in-flight flits = 61888 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 644.675
	minimum = 23
	maximum = 1987
Network latency average = 505.209
	minimum = 23
	maximum = 1838
Slowest packet = 46
Flit latency average = 431.123
	minimum = 6
	maximum = 1971
Slowest flit = 2885
Fragmentation average = 181.075
	minimum = 0
	maximum = 1619
Injected packet rate average = 0.0307995
	minimum = 0.006 (at node 12)
	maximum = 0.056 (at node 118)
Accepted packet rate average = 0.0116615
	minimum = 0.007 (at node 30)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.55144
	minimum = 0.1025 (at node 52)
	maximum = 1 (at node 118)
Accepted flit rate average= 0.224086
	minimum = 0.137 (at node 138)
	maximum = 0.321 (at node 152)
Injected packet length average = 17.9042
Accepted packet length average = 19.2159
Total in-flight flits = 126873 (0 measured)
latency change    = 0.418484
throughput change = 0.0449628
Class 0:
Packet latency average = 1497.67
	minimum = 29
	maximum = 2966
Network latency average = 1272.08
	minimum = 27
	maximum = 2729
Slowest packet = 160
Flit latency average = 1213.52
	minimum = 6
	maximum = 2712
Slowest flit = 10655
Fragmentation average = 224.165
	minimum = 0
	maximum = 2135
Injected packet rate average = 0.029776
	minimum = 0 (at node 34)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0125052
	minimum = 0.004 (at node 20)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.536062
	minimum = 0 (at node 34)
	maximum = 1 (at node 5)
Accepted flit rate average= 0.222214
	minimum = 0.042 (at node 20)
	maximum = 0.403 (at node 16)
Injected packet length average = 18.0031
Accepted packet length average = 17.7697
Total in-flight flits = 187168 (0 measured)
latency change    = 0.569548
throughput change = 0.00842611
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 592.03
	minimum = 23
	maximum = 3151
Network latency average = 173.043
	minimum = 23
	maximum = 940
Slowest packet = 17657
Flit latency average = 1783.65
	minimum = 6
	maximum = 3733
Slowest flit = 22479
Fragmentation average = 43.2974
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0270833
	minimum = 0 (at node 19)
	maximum = 0.056 (at node 53)
Accepted packet rate average = 0.0120313
	minimum = 0.004 (at node 57)
	maximum = 0.022 (at node 14)
Injected flit rate average = 0.487443
	minimum = 0 (at node 19)
	maximum = 1 (at node 42)
Accepted flit rate average= 0.217177
	minimum = 0.061 (at node 118)
	maximum = 0.388 (at node 75)
Injected packet length average = 17.9979
Accepted packet length average = 18.0511
Total in-flight flits = 239052 (89185 measured)
latency change    = 1.52972
throughput change = 0.0231906
Class 0:
Packet latency average = 953.157
	minimum = 23
	maximum = 3557
Network latency average = 480.035
	minimum = 23
	maximum = 1868
Slowest packet = 17657
Flit latency average = 2053.09
	minimum = 6
	maximum = 4737
Slowest flit = 23032
Fragmentation average = 69.1987
	minimum = 0
	maximum = 592
Injected packet rate average = 0.0248229
	minimum = 0 (at node 146)
	maximum = 0.0555 (at node 173)
Accepted packet rate average = 0.0119844
	minimum = 0.005 (at node 113)
	maximum = 0.0195 (at node 181)
Injected flit rate average = 0.446852
	minimum = 0 (at node 146)
	maximum = 1 (at node 173)
Accepted flit rate average= 0.215802
	minimum = 0.0825 (at node 113)
	maximum = 0.346 (at node 181)
Injected packet length average = 18.0016
Accepted packet length average = 18.007
Total in-flight flits = 275876 (159610 measured)
latency change    = 0.378875
throughput change = 0.00637158
Class 0:
Packet latency average = 1612.53
	minimum = 23
	maximum = 5181
Network latency average = 1014.89
	minimum = 23
	maximum = 2950
Slowest packet = 17657
Flit latency average = 2327.47
	minimum = 6
	maximum = 5540
Slowest flit = 42535
Fragmentation average = 105.951
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.0231944
	minimum = 0.007 (at node 146)
	maximum = 0.0446667 (at node 119)
Accepted packet rate average = 0.0119167
	minimum = 0.007 (at node 84)
	maximum = 0.0176667 (at node 181)
Injected flit rate average = 0.417472
	minimum = 0.126 (at node 146)
	maximum = 0.803333 (at node 119)
Accepted flit rate average= 0.214618
	minimum = 0.126667 (at node 84)
	maximum = 0.312667 (at node 181)
Injected packet length average = 17.9988
Accepted packet length average = 18.0099
Total in-flight flits = 304136 (215957 measured)
latency change    = 0.408907
throughput change = 0.00551691
Class 0:
Packet latency average = 2252.66
	minimum = 23
	maximum = 6175
Network latency average = 1560.77
	minimum = 23
	maximum = 3891
Slowest packet = 17657
Flit latency average = 2595.46
	minimum = 6
	maximum = 6434
Slowest flit = 23111
Fragmentation average = 127.965
	minimum = 0
	maximum = 1517
Injected packet rate average = 0.0220521
	minimum = 0.00625 (at node 132)
	maximum = 0.038 (at node 91)
Accepted packet rate average = 0.0118698
	minimum = 0.00675 (at node 84)
	maximum = 0.01825 (at node 19)
Injected flit rate average = 0.396651
	minimum = 0.11325 (at node 132)
	maximum = 0.684 (at node 91)
Accepted flit rate average= 0.214036
	minimum = 0.12775 (at node 84)
	maximum = 0.3315 (at node 19)
Injected packet length average = 17.987
Accepted packet length average = 18.032
Total in-flight flits = 328050 (262126 measured)
latency change    = 0.284164
throughput change = 0.00271728
Class 0:
Packet latency average = 2854.57
	minimum = 23
	maximum = 7228
Network latency average = 2069.29
	minimum = 23
	maximum = 4926
Slowest packet = 17657
Flit latency average = 2857.36
	minimum = 6
	maximum = 7215
Slowest flit = 76152
Fragmentation average = 141.609
	minimum = 0
	maximum = 1525
Injected packet rate average = 0.0209417
	minimum = 0.0094 (at node 132)
	maximum = 0.0344 (at node 170)
Accepted packet rate average = 0.0118375
	minimum = 0.008 (at node 84)
	maximum = 0.017 (at node 19)
Injected flit rate average = 0.377157
	minimum = 0.1704 (at node 132)
	maximum = 0.6192 (at node 170)
Accepted flit rate average= 0.212279
	minimum = 0.1522 (at node 144)
	maximum = 0.3012 (at node 19)
Injected packet length average = 18.0099
Accepted packet length average = 17.9328
Total in-flight flits = 346134 (297092 measured)
latency change    = 0.210859
throughput change = 0.00827821
Class 0:
Packet latency average = 3375.73
	minimum = 23
	maximum = 8056
Network latency average = 2519.9
	minimum = 23
	maximum = 5910
Slowest packet = 17657
Flit latency average = 3126.94
	minimum = 6
	maximum = 8312
Slowest flit = 64884
Fragmentation average = 150.319
	minimum = 0
	maximum = 2669
Injected packet rate average = 0.0196128
	minimum = 0.00783333 (at node 132)
	maximum = 0.0306667 (at node 91)
Accepted packet rate average = 0.011678
	minimum = 0.008 (at node 144)
	maximum = 0.0151667 (at node 19)
Injected flit rate average = 0.353209
	minimum = 0.142 (at node 132)
	maximum = 0.551 (at node 91)
Accepted flit rate average= 0.209907
	minimum = 0.1435 (at node 144)
	maximum = 0.277167 (at node 19)
Injected packet length average = 18.0091
Accepted packet length average = 17.9747
Total in-flight flits = 353127 (317373 measured)
latency change    = 0.154384
throughput change = 0.0113005
Class 0:
Packet latency average = 3906.53
	minimum = 23
	maximum = 9199
Network latency average = 2984.68
	minimum = 23
	maximum = 6992
Slowest packet = 17657
Flit latency average = 3399.99
	minimum = 6
	maximum = 9324
Slowest flit = 56861
Fragmentation average = 156.923
	minimum = 0
	maximum = 2669
Injected packet rate average = 0.0185737
	minimum = 0.00728571 (at node 132)
	maximum = 0.0298571 (at node 91)
Accepted packet rate average = 0.0115796
	minimum = 0.00842857 (at node 144)
	maximum = 0.0158571 (at node 19)
Injected flit rate average = 0.334476
	minimum = 0.132 (at node 132)
	maximum = 0.536857 (at node 91)
Accepted flit rate average= 0.207741
	minimum = 0.150143 (at node 144)
	maximum = 0.285571 (at node 19)
Injected packet length average = 18.0081
Accepted packet length average = 17.9402
Total in-flight flits = 358558 (332823 measured)
latency change    = 0.135874
throughput change = 0.0104267
Draining all recorded packets ...
Class 0:
Remaining flits: 86940 86941 86942 86943 86944 86945 86946 86947 86948 86949 [...] (359661 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (339855 flits)
Class 0:
Remaining flits: 86940 86941 86942 86943 86944 86945 86946 86947 86948 86949 [...] (356969 flits)
Measured flits: 315900 315901 315902 315903 315904 315905 315906 315907 315908 315909 [...] (340627 flits)
Class 0:
Remaining flits: 86957 103482 103483 103484 103485 103486 103487 103488 103489 103490 [...] (353032 flits)
Measured flits: 315990 315991 315992 315993 315994 315995 315996 315997 315998 315999 [...] (339255 flits)
Class 0:
Remaining flits: 103497 103498 103499 127038 127039 127040 127041 127042 127043 155268 [...] (350634 flits)
Measured flits: 315990 315991 315992 315993 315994 315995 315996 315997 315998 315999 [...] (336850 flits)
Class 0:
Remaining flits: 155268 155269 155270 155271 155272 155273 155274 155275 155276 155277 [...] (347998 flits)
Measured flits: 316469 316470 316471 316472 316473 316474 316475 316494 316495 316496 [...] (332502 flits)
Class 0:
Remaining flits: 170895 170896 170897 170898 170899 170900 170901 170902 170903 170904 [...] (346281 flits)
Measured flits: 316926 316927 316928 316929 316930 316931 316932 316933 316934 316935 [...] (326032 flits)
Class 0:
Remaining flits: 175458 175459 175460 175461 175462 175463 184884 184885 184886 184887 [...] (344505 flits)
Measured flits: 317880 317881 317882 317883 317884 317885 317886 317887 317888 317889 [...] (318154 flits)
Class 0:
Remaining flits: 195156 195157 195158 195159 195160 195161 195162 195163 195164 195165 [...] (344868 flits)
Measured flits: 319464 319465 319466 319467 319468 319469 319470 319471 319472 319473 [...] (311632 flits)
Class 0:
Remaining flits: 195156 195157 195158 195159 195160 195161 195162 195163 195164 195165 [...] (344414 flits)
Measured flits: 319464 319465 319466 319467 319468 319469 319470 319471 319472 319473 [...] (300227 flits)
Class 0:
Remaining flits: 208852 208853 210024 210025 210026 210027 210028 210029 210030 210031 [...] (340593 flits)
Measured flits: 319464 319465 319466 319467 319468 319469 319470 319471 319472 319473 [...] (286408 flits)
Class 0:
Remaining flits: 210024 210025 210026 210027 210028 210029 210030 210031 210032 210033 [...] (339903 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (273818 flits)
Class 0:
Remaining flits: 210024 210025 210026 210027 210028 210029 210030 210031 210032 210033 [...] (339624 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (262389 flits)
Class 0:
Remaining flits: 210024 210025 210026 210027 210028 210029 210030 210031 210032 210033 [...] (336938 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (245296 flits)
Class 0:
Remaining flits: 210024 210025 210026 210027 210028 210029 210030 210031 210032 210033 [...] (336817 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (230341 flits)
Class 0:
Remaining flits: 210024 210025 210026 210027 210028 210029 210030 210031 210032 210033 [...] (337336 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (216576 flits)
Class 0:
Remaining flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (336077 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (203364 flits)
Class 0:
Remaining flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (334246 flits)
Measured flits: 322002 322003 322004 322005 322006 322007 322008 322009 322010 322011 [...] (189653 flits)
Class 0:
Remaining flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (336091 flits)
Measured flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (175348 flits)
Class 0:
Remaining flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (336728 flits)
Measured flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (160984 flits)
Class 0:
Remaining flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (334382 flits)
Measured flits: 344304 344305 344306 344307 344308 344309 344310 344311 344312 344313 [...] (147563 flits)
Class 0:
Remaining flits: 344318 344319 344320 344321 412704 412705 412706 412707 412708 412709 [...] (333788 flits)
Measured flits: 344318 344319 344320 344321 412704 412705 412706 412707 412708 412709 [...] (132381 flits)
Class 0:
Remaining flits: 412704 412705 412706 412707 412708 412709 412710 412711 412712 412713 [...] (331236 flits)
Measured flits: 412704 412705 412706 412707 412708 412709 412710 412711 412712 412713 [...] (118984 flits)
Class 0:
Remaining flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (329917 flits)
Measured flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (106911 flits)
Class 0:
Remaining flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (328766 flits)
Measured flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (95812 flits)
Class 0:
Remaining flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (327123 flits)
Measured flits: 418590 418591 418592 418593 418594 418595 418596 418597 418598 418599 [...] (85852 flits)
Class 0:
Remaining flits: 510858 510859 510860 510861 510862 510863 510864 510865 510866 510867 [...] (328610 flits)
Measured flits: 510858 510859 510860 510861 510862 510863 510864 510865 510866 510867 [...] (75731 flits)
Class 0:
Remaining flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (328292 flits)
Measured flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (65676 flits)
Class 0:
Remaining flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (329662 flits)
Measured flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (57730 flits)
Class 0:
Remaining flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (324824 flits)
Measured flits: 531810 531811 531812 531813 531814 531815 531816 531817 531818 531819 [...] (49690 flits)
Class 0:
Remaining flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (326250 flits)
Measured flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (42676 flits)
Class 0:
Remaining flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (323301 flits)
Measured flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (36599 flits)
Class 0:
Remaining flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (321001 flits)
Measured flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (31591 flits)
Class 0:
Remaining flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (321597 flits)
Measured flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (27479 flits)
Class 0:
Remaining flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (321706 flits)
Measured flits: 645462 645463 645464 645465 645466 645467 645468 645469 645470 645471 [...] (24245 flits)
Class 0:
Remaining flits: 793854 793855 793856 793857 793858 793859 793860 793861 793862 793863 [...] (319795 flits)
Measured flits: 793854 793855 793856 793857 793858 793859 793860 793861 793862 793863 [...] (22001 flits)
Class 0:
Remaining flits: 795618 795619 795620 795621 795622 795623 795624 795625 795626 795627 [...] (315509 flits)
Measured flits: 795618 795619 795620 795621 795622 795623 795624 795625 795626 795627 [...] (19269 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (316105 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (16568 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (317315 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (14288 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (316889 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (12249 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (316817 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (10522 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (318568 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (8401 flits)
Class 0:
Remaining flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (321084 flits)
Measured flits: 944118 944119 944120 944121 944122 944123 944124 944125 944126 944127 [...] (7202 flits)
Class 0:
Remaining flits: 1018818 1018819 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 [...] (320305 flits)
Measured flits: 1018818 1018819 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 [...] (6254 flits)
Class 0:
Remaining flits: 1018818 1018819 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 [...] (317860 flits)
Measured flits: 1018818 1018819 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 [...] (5384 flits)
Class 0:
Remaining flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (317081 flits)
Measured flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (4528 flits)
Class 0:
Remaining flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (316066 flits)
Measured flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (3799 flits)
Class 0:
Remaining flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (313999 flits)
Measured flits: 1018820 1018821 1018822 1018823 1018824 1018825 1018826 1018827 1018828 1018829 [...] (3294 flits)
Class 0:
Remaining flits: 1291860 1291861 1291862 1291863 1291864 1291865 1291866 1291867 1291868 1291869 [...] (316058 flits)
Measured flits: 1356930 1356931 1356932 1356933 1356934 1356935 1356936 1356937 1356938 1356939 [...] (2521 flits)
Class 0:
Remaining flits: 1291860 1291861 1291862 1291863 1291864 1291865 1291866 1291867 1291868 1291869 [...] (316186 flits)
Measured flits: 1356930 1356931 1356932 1356933 1356934 1356935 1356936 1356937 1356938 1356939 [...] (1989 flits)
Class 0:
Remaining flits: 1291860 1291861 1291862 1291863 1291864 1291865 1291866 1291867 1291868 1291869 [...] (314036 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (1904 flits)
Class 0:
Remaining flits: 1291863 1291864 1291865 1291866 1291867 1291868 1291869 1291870 1291871 1291872 [...] (313344 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (1698 flits)
Class 0:
Remaining flits: 1291863 1291864 1291865 1291866 1291867 1291868 1291869 1291870 1291871 1291872 [...] (312205 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (1483 flits)
Class 0:
Remaining flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (309456 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (1177 flits)
Class 0:
Remaining flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (308233 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (916 flits)
Class 0:
Remaining flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (310213 flits)
Measured flits: 1437768 1437769 1437770 1437771 1437772 1437773 1437774 1437775 1437776 1437777 [...] (773 flits)
Class 0:
Remaining flits: 1464372 1464373 1464374 1464375 1464376 1464377 1464378 1464379 1464380 1464381 [...] (309528 flits)
Measured flits: 1708182 1708183 1708184 1708185 1708186 1708187 1708188 1708189 1708190 1708191 [...] (619 flits)
Class 0:
Remaining flits: 1612692 1612693 1612694 1612695 1612696 1612697 1612698 1612699 1612700 1612701 [...] (310483 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (447 flits)
Class 0:
Remaining flits: 1612706 1612707 1612708 1612709 1700190 1700191 1700192 1700193 1700194 1700195 [...] (310854 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (307 flits)
Class 0:
Remaining flits: 1718028 1718029 1718030 1718031 1718032 1718033 1718034 1718035 1718036 1718037 [...] (312113 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (248 flits)
Class 0:
Remaining flits: 1734876 1734877 1734878 1734879 1734880 1734881 1734882 1734883 1734884 1734885 [...] (308777 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (193 flits)
Class 0:
Remaining flits: 1734876 1734877 1734878 1734879 1734880 1734881 1734882 1734883 1734884 1734885 [...] (306379 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (129 flits)
Class 0:
Remaining flits: 1740366 1740367 1740368 1740369 1740370 1740371 1740372 1740373 1740374 1740375 [...] (302985 flits)
Measured flits: 1800180 1800181 1800182 1800183 1800184 1800185 1800186 1800187 1800188 1800189 [...] (98 flits)
Class 0:
Remaining flits: 1740366 1740367 1740368 1740369 1740370 1740371 1740372 1740373 1740374 1740375 [...] (305496 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (54 flits)
Class 0:
Remaining flits: 1740366 1740367 1740368 1740369 1740370 1740371 1740372 1740373 1740374 1740375 [...] (306618 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (54 flits)
Class 0:
Remaining flits: 1740366 1740367 1740368 1740369 1740370 1740371 1740372 1740373 1740374 1740375 [...] (307010 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (40 flits)
Class 0:
Remaining flits: 1860750 1860751 1860752 1860753 1860754 1860755 1860756 1860757 1860758 1860759 [...] (306922 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (18 flits)
Class 0:
Remaining flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (305278 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (18 flits)
Class 0:
Remaining flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (302717 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (18 flits)
Class 0:
Remaining flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (303543 flits)
Measured flits: 1926234 1926235 1926236 1926237 1926238 1926239 1926240 1926241 1926242 1926243 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1998396 1998397 1998398 1998399 1998400 1998401 1998402 1998403 1998404 1998405 [...] (269102 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998396 1998397 1998398 1998399 1998400 1998401 1998402 1998403 1998404 1998405 [...] (235024 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998396 1998397 1998398 1998399 1998400 1998401 1998402 1998403 1998404 1998405 [...] (202615 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998396 1998397 1998398 1998399 1998400 1998401 1998402 1998403 1998404 1998405 [...] (171202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998396 1998397 1998398 1998399 1998400 1998401 1998402 1998403 1998404 1998405 [...] (141263 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1998412 1998413 2044763 2171934 2171935 2171936 2171937 2171938 2171939 2171940 [...] (113010 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2171934 2171935 2171936 2171937 2171938 2171939 2171940 2171941 2171942 2171943 [...] (85236 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2171934 2171935 2171936 2171937 2171938 2171939 2171940 2171941 2171942 2171943 [...] (60664 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2171940 2171941 2171942 2171943 2171944 2171945 2171946 2171947 2171948 2171949 [...] (41747 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2229732 2229733 2229734 2229735 2229736 2229737 2229738 2229739 2229740 2229741 [...] (26606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2253330 2253331 2253332 2253333 2253334 2253335 2253336 2253337 2253338 2253339 [...] (15656 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2398842 2398843 2398844 2398845 2398846 2398847 2398848 2398849 2398850 2398851 [...] (7577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2452034 2452035 2452036 2452037 2452038 2452039 2452040 2452041 2452042 2452043 [...] (3979 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2549484 2549485 2549486 2549487 2549488 2549489 2549490 2549491 2549492 2549493 [...] (1483 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2612826 2612827 2612828 2612829 2612830 2612831 2612832 2612833 2612834 2612835 [...] (186 flits)
Measured flits: (0 flits)
Time taken is 94404 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16079.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 70055 (1 samples)
Network latency average = 8510.58 (1 samples)
	minimum = 23 (1 samples)
	maximum = 40231 (1 samples)
Flit latency average = 8425.7 (1 samples)
	minimum = 6 (1 samples)
	maximum = 40879 (1 samples)
Fragmentation average = 206.003 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5689 (1 samples)
Injected packet rate average = 0.0185737 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0298571 (1 samples)
Accepted packet rate average = 0.0115796 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0158571 (1 samples)
Injected flit rate average = 0.334476 (1 samples)
	minimum = 0.132 (1 samples)
	maximum = 0.536857 (1 samples)
Accepted flit rate average = 0.207741 (1 samples)
	minimum = 0.150143 (1 samples)
	maximum = 0.285571 (1 samples)
Injected packet size average = 18.0081 (1 samples)
Accepted packet size average = 17.9402 (1 samples)
Hops average = 5.06078 (1 samples)
Total run time 119.273
