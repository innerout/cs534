BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.886
	minimum = 24
	maximum = 955
Network latency average = 233.455
	minimum = 22
	maximum = 778
Slowest packet = 57
Flit latency average = 203.711
	minimum = 5
	maximum = 788
Slowest flit = 17104
Fragmentation average = 43.6166
	minimum = 0
	maximum = 413
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.013776
	minimum = 0.007 (at node 23)
	maximum = 0.024 (at node 76)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.259057
	minimum = 0.126 (at node 28)
	maximum = 0.432 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.8049
Total in-flight flits = 50701 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 604.237
	minimum = 22
	maximum = 1934
Network latency average = 446.717
	minimum = 22
	maximum = 1568
Slowest packet = 57
Flit latency average = 415.232
	minimum = 5
	maximum = 1664
Slowest flit = 28487
Fragmentation average = 62.9013
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0305781
	minimum = 0.001 (at node 5)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.014776
	minimum = 0.0085 (at node 83)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.547818
	minimum = 0.018 (at node 5)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.27463
	minimum = 0.1595 (at node 83)
	maximum = 0.417 (at node 152)
Injected packet length average = 17.9153
Accepted packet length average = 18.5862
Total in-flight flits = 105898 (0 measured)
latency change    = 0.430876
throughput change = 0.056705
Class 0:
Packet latency average = 1330.32
	minimum = 28
	maximum = 2814
Network latency average = 1087.67
	minimum = 22
	maximum = 2362
Slowest packet = 3341
Flit latency average = 1052.97
	minimum = 5
	maximum = 2361
Slowest flit = 57642
Fragmentation average = 102.768
	minimum = 0
	maximum = 417
Injected packet rate average = 0.0312187
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0160625
	minimum = 0.005 (at node 61)
	maximum = 0.032 (at node 51)
Injected flit rate average = 0.562953
	minimum = 0 (at node 0)
	maximum = 1 (at node 32)
Accepted flit rate average= 0.29037
	minimum = 0.111 (at node 61)
	maximum = 0.547 (at node 51)
Injected packet length average = 18.0325
Accepted packet length average = 18.0775
Total in-flight flits = 158075 (0 measured)
latency change    = 0.545795
throughput change = 0.0542053
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 341.625
	minimum = 27
	maximum = 2312
Network latency average = 46.2949
	minimum = 22
	maximum = 620
Slowest packet = 17747
Flit latency average = 1525.61
	minimum = 5
	maximum = 2994
Slowest flit = 93041
Fragmentation average = 6.4612
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0320469
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0159375
	minimum = 0.006 (at node 18)
	maximum = 0.026 (at node 91)
Injected flit rate average = 0.575927
	minimum = 0 (at node 100)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.288026
	minimum = 0.16 (at node 64)
	maximum = 0.468 (at node 178)
Injected packet length average = 17.9714
Accepted packet length average = 18.0722
Total in-flight flits = 213564 (102572 measured)
latency change    = 2.89409
throughput change = 0.00813729
Class 0:
Packet latency average = 530.796
	minimum = 27
	maximum = 3126
Network latency average = 236.332
	minimum = 22
	maximum = 1865
Slowest packet = 17747
Flit latency average = 1767.48
	minimum = 5
	maximum = 3818
Slowest flit = 121212
Fragmentation average = 15.3291
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0309714
	minimum = 0.003 (at node 124)
	maximum = 0.0555 (at node 73)
Accepted packet rate average = 0.0157734
	minimum = 0.009 (at node 64)
	maximum = 0.0225 (at node 24)
Injected flit rate average = 0.557
	minimum = 0.054 (at node 124)
	maximum = 1 (at node 73)
Accepted flit rate average= 0.284333
	minimum = 0.1595 (at node 64)
	maximum = 0.3935 (at node 24)
Injected packet length average = 17.9844
Accepted packet length average = 18.0261
Total in-flight flits = 263127 (196834 measured)
latency change    = 0.35639
throughput change = 0.0129873
Class 0:
Packet latency average = 1146.05
	minimum = 22
	maximum = 3797
Network latency average = 868.623
	minimum = 22
	maximum = 2968
Slowest packet = 17747
Flit latency average = 2014.41
	minimum = 5
	maximum = 4520
Slowest flit = 150255
Fragmentation average = 34.8537
	minimum = 0
	maximum = 483
Injected packet rate average = 0.0291354
	minimum = 0.005 (at node 124)
	maximum = 0.052 (at node 73)
Accepted packet rate average = 0.0157118
	minimum = 0.0103333 (at node 104)
	maximum = 0.0216667 (at node 136)
Injected flit rate average = 0.524274
	minimum = 0.09 (at node 124)
	maximum = 0.940333 (at node 73)
Accepted flit rate average= 0.281693
	minimum = 0.195333 (at node 132)
	maximum = 0.393667 (at node 136)
Injected packet length average = 17.9944
Accepted packet length average = 17.9287
Total in-flight flits = 298238 (267703 measured)
latency change    = 0.536847
throughput change = 0.00937413
Class 0:
Packet latency average = 2051.85
	minimum = 22
	maximum = 5934
Network latency average = 1768.02
	minimum = 22
	maximum = 3985
Slowest packet = 17747
Flit latency average = 2261.5
	minimum = 5
	maximum = 5214
Slowest flit = 178127
Fragmentation average = 52.0958
	minimum = 0
	maximum = 562
Injected packet rate average = 0.0278724
	minimum = 0.00925 (at node 124)
	maximum = 0.0465 (at node 73)
Accepted packet rate average = 0.0156484
	minimum = 0.012 (at node 104)
	maximum = 0.02025 (at node 61)
Injected flit rate average = 0.501539
	minimum = 0.1665 (at node 124)
	maximum = 0.8395 (at node 73)
Accepted flit rate average= 0.280087
	minimum = 0.21225 (at node 132)
	maximum = 0.36775 (at node 95)
Injected packet length average = 17.9941
Accepted packet length average = 17.8987
Total in-flight flits = 328672 (315571 measured)
latency change    = 0.441456
throughput change = 0.00573203
Class 0:
Packet latency average = 2682.31
	minimum = 22
	maximum = 6763
Network latency average = 2372.97
	minimum = 22
	maximum = 4989
Slowest packet = 17747
Flit latency average = 2500.44
	minimum = 5
	maximum = 6368
Slowest flit = 171594
Fragmentation average = 60.7915
	minimum = 0
	maximum = 615
Injected packet rate average = 0.0268823
	minimum = 0.0086 (at node 124)
	maximum = 0.0416 (at node 186)
Accepted packet rate average = 0.0155563
	minimum = 0.0122 (at node 18)
	maximum = 0.0206 (at node 177)
Injected flit rate average = 0.483748
	minimum = 0.1548 (at node 124)
	maximum = 0.747 (at node 186)
Accepted flit rate average= 0.278942
	minimum = 0.2182 (at node 52)
	maximum = 0.3626 (at node 177)
Injected packet length average = 17.995
Accepted packet length average = 17.9312
Total in-flight flits = 355519 (349778 measured)
latency change    = 0.235044
throughput change = 0.00410685
Class 0:
Packet latency average = 3161.62
	minimum = 22
	maximum = 7367
Network latency average = 2835.9
	minimum = 22
	maximum = 5995
Slowest packet = 17747
Flit latency average = 2746.94
	minimum = 5
	maximum = 7177
Slowest flit = 189866
Fragmentation average = 64.3585
	minimum = 0
	maximum = 659
Injected packet rate average = 0.0255156
	minimum = 0.0095 (at node 68)
	maximum = 0.0378333 (at node 126)
Accepted packet rate average = 0.0154965
	minimum = 0.012 (at node 52)
	maximum = 0.02 (at node 70)
Injected flit rate average = 0.459188
	minimum = 0.173333 (at node 68)
	maximum = 0.681833 (at node 126)
Accepted flit rate average= 0.278035
	minimum = 0.217667 (at node 161)
	maximum = 0.360333 (at node 70)
Injected packet length average = 17.9963
Accepted packet length average = 17.9417
Total in-flight flits = 367627 (365300 measured)
latency change    = 0.151604
throughput change = 0.00326198
Class 0:
Packet latency average = 3567.89
	minimum = 22
	maximum = 8420
Network latency average = 3209.98
	minimum = 22
	maximum = 6959
Slowest packet = 17747
Flit latency average = 2982.86
	minimum = 5
	maximum = 7777
Slowest flit = 232195
Fragmentation average = 65.4311
	minimum = 0
	maximum = 659
Injected packet rate average = 0.0245595
	minimum = 0.0102857 (at node 156)
	maximum = 0.0354286 (at node 126)
Accepted packet rate average = 0.0154494
	minimum = 0.0124286 (at node 18)
	maximum = 0.0191429 (at node 70)
Injected flit rate average = 0.441967
	minimum = 0.183286 (at node 156)
	maximum = 0.639143 (at node 126)
Accepted flit rate average= 0.277013
	minimum = 0.221571 (at node 52)
	maximum = 0.342429 (at node 70)
Injected packet length average = 17.9957
Accepted packet length average = 17.9303
Total in-flight flits = 381030 (380178 measured)
latency change    = 0.113867
throughput change = 0.00368963
Draining all recorded packets ...
Class 0:
Remaining flits: 250316 250317 250318 250319 250320 250321 250322 250323 250324 250325 [...] (388528 flits)
Measured flits: 319572 319573 319574 319575 319576 319577 319578 319579 319580 319581 [...] (370369 flits)
Class 0:
Remaining flits: 271134 271135 271136 271137 271138 271139 271140 271141 271142 271143 [...] (397955 flits)
Measured flits: 320526 320527 320528 320529 320530 320531 320532 320533 320534 320535 [...] (356492 flits)
Class 0:
Remaining flits: 294784 294785 297469 297470 297471 297472 297473 297474 297475 297476 [...] (403546 flits)
Measured flits: 320724 320725 320726 320727 320728 320729 320730 320731 320732 320733 [...] (338347 flits)
Class 0:
Remaining flits: 343296 343297 343298 343299 343300 343301 343302 343303 343304 343305 [...] (408968 flits)
Measured flits: 343296 343297 343298 343299 343300 343301 343302 343303 343304 343305 [...] (315983 flits)
Class 0:
Remaining flits: 358866 358867 358868 358869 358870 358871 358872 358873 358874 358875 [...] (412547 flits)
Measured flits: 358866 358867 358868 358869 358870 358871 358872 358873 358874 358875 [...] (291546 flits)
Class 0:
Remaining flits: 401652 401653 401654 401655 401656 401657 401658 401659 401660 401661 [...] (414889 flits)
Measured flits: 401652 401653 401654 401655 401656 401657 401658 401659 401660 401661 [...] (262952 flits)
Class 0:
Remaining flits: 475560 475561 475562 475563 475564 475565 475566 475567 475568 475569 [...] (413147 flits)
Measured flits: 475560 475561 475562 475563 475564 475565 475566 475567 475568 475569 [...] (232957 flits)
Class 0:
Remaining flits: 475560 475561 475562 475563 475564 475565 475566 475567 475568 475569 [...] (415092 flits)
Measured flits: 475560 475561 475562 475563 475564 475565 475566 475567 475568 475569 [...] (205470 flits)
Class 0:
Remaining flits: 529848 529849 529850 529851 529852 529853 529854 529855 529856 529857 [...] (416656 flits)
Measured flits: 529848 529849 529850 529851 529852 529853 529854 529855 529856 529857 [...] (178373 flits)
Class 0:
Remaining flits: 529848 529849 529850 529851 529852 529853 529854 529855 529856 529857 [...] (417529 flits)
Measured flits: 529848 529849 529850 529851 529852 529853 529854 529855 529856 529857 [...] (152838 flits)
Class 0:
Remaining flits: 648982 648983 648984 648985 648986 648987 648988 648989 663858 663859 [...] (418733 flits)
Measured flits: 648982 648983 648984 648985 648986 648987 648988 648989 663858 663859 [...] (127235 flits)
Class 0:
Remaining flits: 663858 663859 663860 663861 663862 663863 663864 663865 663866 663867 [...] (419535 flits)
Measured flits: 663858 663859 663860 663861 663862 663863 663864 663865 663866 663867 [...] (103113 flits)
Class 0:
Remaining flits: 663858 663859 663860 663861 663862 663863 663864 663865 663866 663867 [...] (421955 flits)
Measured flits: 663858 663859 663860 663861 663862 663863 663864 663865 663866 663867 [...] (82987 flits)
Class 0:
Remaining flits: 726642 726643 726644 726645 726646 726647 726648 726649 726650 726651 [...] (420703 flits)
Measured flits: 726642 726643 726644 726645 726646 726647 726648 726649 726650 726651 [...] (64483 flits)
Class 0:
Remaining flits: 758358 758359 758360 758361 758362 758363 758364 758365 758366 758367 [...] (422199 flits)
Measured flits: 758358 758359 758360 758361 758362 758363 758364 758365 758366 758367 [...] (49142 flits)
Class 0:
Remaining flits: 943020 943021 943022 943023 943024 943025 943026 943027 943028 943029 [...] (421985 flits)
Measured flits: 943020 943021 943022 943023 943024 943025 943026 943027 943028 943029 [...] (37094 flits)
Class 0:
Remaining flits: 946026 946027 946028 946029 946030 946031 946032 946033 946034 946035 [...] (421335 flits)
Measured flits: 946026 946027 946028 946029 946030 946031 946032 946033 946034 946035 [...] (26848 flits)
Class 0:
Remaining flits: 1092847 1092848 1092849 1092850 1092851 1097907 1097908 1097909 1101006 1101007 [...] (423302 flits)
Measured flits: 1097907 1097908 1097909 1101006 1101007 1101008 1101009 1101010 1101011 1101012 [...] (19029 flits)
Class 0:
Remaining flits: 1101006 1101007 1101008 1101009 1101010 1101011 1101012 1101013 1101014 1101015 [...] (420557 flits)
Measured flits: 1101006 1101007 1101008 1101009 1101010 1101011 1101012 1101013 1101014 1101015 [...] (14098 flits)
Class 0:
Remaining flits: 1101006 1101007 1101008 1101009 1101010 1101011 1101012 1101013 1101014 1101015 [...] (418497 flits)
Measured flits: 1101006 1101007 1101008 1101009 1101010 1101011 1101012 1101013 1101014 1101015 [...] (9840 flits)
Class 0:
Remaining flits: 1101009 1101010 1101011 1101012 1101013 1101014 1101015 1101016 1101017 1101018 [...] (419635 flits)
Measured flits: 1101009 1101010 1101011 1101012 1101013 1101014 1101015 1101016 1101017 1101018 [...] (7368 flits)
Class 0:
Remaining flits: 1264338 1264339 1264340 1264341 1264342 1264343 1264344 1264345 1264346 1264347 [...] (421528 flits)
Measured flits: 1313820 1313821 1313822 1313823 1313824 1313825 1313826 1313827 1313828 1313829 [...] (5484 flits)
Class 0:
Remaining flits: 1313820 1313821 1313822 1313823 1313824 1313825 1313826 1313827 1313828 1313829 [...] (420084 flits)
Measured flits: 1313820 1313821 1313822 1313823 1313824 1313825 1313826 1313827 1313828 1313829 [...] (3928 flits)
Class 0:
Remaining flits: 1360242 1360243 1360244 1360245 1360246 1360247 1360248 1360249 1360250 1360251 [...] (422016 flits)
Measured flits: 1461330 1461331 1461332 1461333 1461334 1461335 1461336 1461337 1461338 1461339 [...] (3006 flits)
Class 0:
Remaining flits: 1424574 1424575 1424576 1424577 1424578 1424579 1424580 1424581 1424582 1424583 [...] (423957 flits)
Measured flits: 1461330 1461331 1461332 1461333 1461334 1461335 1461336 1461337 1461338 1461339 [...] (2244 flits)
Class 0:
Remaining flits: 1440324 1440325 1440326 1440327 1440328 1440329 1440330 1440331 1440332 1440333 [...] (425571 flits)
Measured flits: 1461330 1461331 1461332 1461333 1461334 1461335 1461336 1461337 1461338 1461339 [...] (1854 flits)
Class 0:
Remaining flits: 1479312 1479313 1479314 1479315 1479316 1479317 1479318 1479319 1479320 1479321 [...] (425820 flits)
Measured flits: 1480824 1480825 1480826 1480827 1480828 1480829 1480830 1480831 1480832 1480833 [...] (1200 flits)
Class 0:
Remaining flits: 1479312 1479313 1479314 1479315 1479316 1479317 1479318 1479319 1479320 1479321 [...] (424397 flits)
Measured flits: 1586862 1586863 1586864 1586865 1586866 1586867 1586868 1586869 1586870 1586871 [...] (771 flits)
Class 0:
Remaining flits: 1479312 1479313 1479314 1479315 1479316 1479317 1479318 1479319 1479320 1479321 [...] (424111 flits)
Measured flits: 1879668 1879669 1879670 1879671 1879672 1879673 1879674 1879675 1879676 1879677 [...] (432 flits)
Class 0:
Remaining flits: 1622016 1622017 1622018 1622019 1622020 1622021 1622022 1622023 1622024 1622025 [...] (428872 flits)
Measured flits: 1906794 1906795 1906796 1906797 1906798 1906799 1906800 1906801 1906802 1906803 [...] (287 flits)
Class 0:
Remaining flits: 1689516 1689517 1689518 1689519 1689520 1689521 1689522 1689523 1689524 1689525 [...] (427213 flits)
Measured flits: 1971000 1971001 1971002 1971003 1971004 1971005 1971006 1971007 1971008 1971009 [...] (162 flits)
Class 0:
Remaining flits: 1747818 1747819 1747820 1747821 1747822 1747823 1747824 1747825 1747826 1747827 [...] (424878 flits)
Measured flits: 2025306 2025307 2025308 2025309 2025310 2025311 2025312 2025313 2025314 2025315 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1775826 1775827 1775828 1775829 1775830 1775831 1775832 1775833 1775834 1775835 [...] (372364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836468 1836469 1836470 1836471 1836472 1836473 1836474 1836475 1836476 1836477 [...] (322331 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1877076 1877077 1877078 1877079 1877080 1877081 1877082 1877083 1877084 1877085 [...] (271935 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1933239 1933240 1933241 1933242 1933243 1933244 1933245 1933246 1933247 1933248 [...] (222697 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1989576 1989577 1989578 1989579 1989580 1989581 1989582 1989583 1989584 1989585 [...] (174375 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2086776 2086777 2086778 2086779 2086780 2086781 2086782 2086783 2086784 2086785 [...] (126596 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2199942 2199943 2199944 2199945 2199946 2199947 2199948 2199949 2199950 2199951 [...] (79038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2234466 2234467 2234468 2234469 2234470 2234471 2234472 2234473 2234474 2234475 [...] (32433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2273094 2273095 2273096 2273097 2273098 2273099 2273100 2273101 2273102 2273103 [...] (4857 flits)
Measured flits: (0 flits)
Time taken is 52537 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8493.46 (1 samples)
	minimum = 22 (1 samples)
	maximum = 32802 (1 samples)
Network latency average = 6234.54 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17957 (1 samples)
Flit latency average = 7082.17 (1 samples)
	minimum = 5 (1 samples)
	maximum = 19086 (1 samples)
Fragmentation average = 55.9362 (1 samples)
	minimum = 0 (1 samples)
	maximum = 784 (1 samples)
Injected packet rate average = 0.0245595 (1 samples)
	minimum = 0.0102857 (1 samples)
	maximum = 0.0354286 (1 samples)
Accepted packet rate average = 0.0154494 (1 samples)
	minimum = 0.0124286 (1 samples)
	maximum = 0.0191429 (1 samples)
Injected flit rate average = 0.441967 (1 samples)
	minimum = 0.183286 (1 samples)
	maximum = 0.639143 (1 samples)
Accepted flit rate average = 0.277013 (1 samples)
	minimum = 0.221571 (1 samples)
	maximum = 0.342429 (1 samples)
Injected packet size average = 17.9957 (1 samples)
Accepted packet size average = 17.9303 (1 samples)
Hops average = 5.07409 (1 samples)
Total run time 73.9423
