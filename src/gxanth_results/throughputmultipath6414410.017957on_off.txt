BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.931
	minimum = 27
	maximum = 994
Network latency average = 240.357
	minimum = 23
	maximum = 977
Slowest packet = 102
Flit latency average = 165.425
	minimum = 6
	maximum = 960
Slowest flit = 1853
Fragmentation average = 140.521
	minimum = 0
	maximum = 894
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00924479
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.189865
	minimum = 0.09 (at node 120)
	maximum = 0.344 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 20.5375
Total in-flight flits = 25556 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 455.379
	minimum = 23
	maximum = 1925
Network latency average = 383.91
	minimum = 23
	maximum = 1879
Slowest packet = 407
Flit latency average = 293.026
	minimum = 6
	maximum = 1862
Slowest flit = 7343
Fragmentation average = 192.459
	minimum = 0
	maximum = 1365
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.010349
	minimum = 0.0055 (at node 30)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.202935
	minimum = 0.107 (at node 30)
	maximum = 0.316 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 19.6092
Total in-flight flits = 41665 (0 measured)
latency change    = 0.343556
throughput change = 0.0644064
Class 0:
Packet latency average = 829.868
	minimum = 27
	maximum = 2803
Network latency average = 737
	minimum = 23
	maximum = 2786
Slowest packet = 711
Flit latency average = 627.349
	minimum = 6
	maximum = 2769
Slowest flit = 11159
Fragmentation average = 277.55
	minimum = 0
	maximum = 2116
Injected packet rate average = 0.0175885
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 186)
Accepted packet rate average = 0.0121563
	minimum = 0.003 (at node 163)
	maximum = 0.024 (at node 56)
Injected flit rate average = 0.316135
	minimum = 0 (at node 112)
	maximum = 1 (at node 120)
Accepted flit rate average= 0.225349
	minimum = 0.059 (at node 180)
	maximum = 0.42 (at node 56)
Injected packet length average = 17.9739
Accepted packet length average = 18.5377
Total in-flight flits = 59184 (0 measured)
latency change    = 0.451263
throughput change = 0.0994638
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 373.537
	minimum = 29
	maximum = 1200
Network latency average = 305.134
	minimum = 23
	maximum = 977
Slowest packet = 10042
Flit latency average = 822.69
	minimum = 6
	maximum = 3728
Slowest flit = 16248
Fragmentation average = 160.678
	minimum = 0
	maximum = 612
Injected packet rate average = 0.0173438
	minimum = 0.001 (at node 141)
	maximum = 0.051 (at node 142)
Accepted packet rate average = 0.0126875
	minimum = 0.002 (at node 2)
	maximum = 0.023 (at node 168)
Injected flit rate average = 0.312417
	minimum = 0.018 (at node 141)
	maximum = 0.916 (at node 142)
Accepted flit rate average= 0.231589
	minimum = 0.066 (at node 2)
	maximum = 0.386 (at node 90)
Injected packet length average = 18.0132
Accepted packet length average = 18.2533
Total in-flight flits = 74659 (42311 measured)
latency change    = 1.22165
throughput change = 0.0269425
Class 0:
Packet latency average = 623.421
	minimum = 26
	maximum = 2067
Network latency average = 542.244
	minimum = 23
	maximum = 1936
Slowest packet = 10042
Flit latency average = 951.268
	minimum = 6
	maximum = 4795
Slowest flit = 11204
Fragmentation average = 218.459
	minimum = 0
	maximum = 1470
Injected packet rate average = 0.0174818
	minimum = 0.001 (at node 157)
	maximum = 0.043 (at node 142)
Accepted packet rate average = 0.0128828
	minimum = 0.0055 (at node 2)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.315052
	minimum = 0.018 (at node 157)
	maximum = 0.774 (at node 142)
Accepted flit rate average= 0.233815
	minimum = 0.1 (at node 36)
	maximum = 0.348 (at node 128)
Injected packet length average = 18.0217
Accepted packet length average = 18.1494
Total in-flight flits = 90233 (71205 measured)
latency change    = 0.400827
throughput change = 0.00952275
Class 0:
Packet latency average = 875.582
	minimum = 26
	maximum = 3268
Network latency average = 785.466
	minimum = 23
	maximum = 2866
Slowest packet = 10042
Flit latency average = 1085.14
	minimum = 6
	maximum = 5694
Slowest flit = 16955
Fragmentation average = 250.884
	minimum = 0
	maximum = 2115
Injected packet rate average = 0.0168889
	minimum = 0.00333333 (at node 53)
	maximum = 0.0346667 (at node 1)
Accepted packet rate average = 0.0127378
	minimum = 0.00733333 (at node 2)
	maximum = 0.0186667 (at node 157)
Injected flit rate average = 0.30421
	minimum = 0.06 (at node 95)
	maximum = 0.622667 (at node 1)
Accepted flit rate average= 0.230752
	minimum = 0.136 (at node 64)
	maximum = 0.329333 (at node 157)
Injected packet length average = 18.0124
Accepted packet length average = 18.1154
Total in-flight flits = 101375 (89483 measured)
latency change    = 0.287993
throughput change = 0.0132756
Draining remaining packets ...
Class 0:
Remaining flits: 9314 9315 9316 9317 9318 9319 9320 9321 9322 9323 [...] (63161 flits)
Measured flits: 180721 180722 180723 180724 180725 180726 180727 180728 180729 180730 [...] (55662 flits)
Class 0:
Remaining flits: 9314 9315 9316 9317 9318 9319 9320 9321 9322 9323 [...] (30116 flits)
Measured flits: 181098 181099 181100 181101 181102 181103 181104 181105 181106 181107 [...] (25927 flits)
Class 0:
Remaining flits: 43783 43784 43785 43786 43787 43788 43789 43790 43791 43792 [...] (9103 flits)
Measured flits: 181098 181099 181100 181101 181102 181103 181104 181105 181106 181107 [...] (7515 flits)
Class 0:
Remaining flits: 136404 136405 136406 136407 136408 136409 136410 136411 136412 136413 [...] (351 flits)
Measured flits: 184716 184717 184718 184719 184720 184721 184722 184723 184724 184725 [...] (224 flits)
Time taken is 10164 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1928.59 (1 samples)
	minimum = 26 (1 samples)
	maximum = 7155 (1 samples)
Network latency average = 1826.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7079 (1 samples)
Flit latency average = 1885.75 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8517 (1 samples)
Fragmentation average = 291.112 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5456 (1 samples)
Injected packet rate average = 0.0168889 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.0346667 (1 samples)
Accepted packet rate average = 0.0127378 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.30421 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.622667 (1 samples)
Accepted flit rate average = 0.230752 (1 samples)
	minimum = 0.136 (1 samples)
	maximum = 0.329333 (1 samples)
Injected packet size average = 18.0124 (1 samples)
Accepted packet size average = 18.1154 (1 samples)
Hops average = 5.06579 (1 samples)
Total run time 11.4243
