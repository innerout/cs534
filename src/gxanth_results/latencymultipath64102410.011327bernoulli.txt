BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 158.714
	minimum = 23
	maximum = 827
Network latency average = 156.72
	minimum = 23
	maximum = 827
Slowest packet = 157
Flit latency average = 94.4336
	minimum = 6
	maximum = 951
Slowest flit = 1246
Fragmentation average = 96.7458
	minimum = 0
	maximum = 691
Injected packet rate average = 0.0112448
	minimum = 0.004 (at node 12)
	maximum = 0.021 (at node 162)
Accepted packet rate average = 0.00842188
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.200438
	minimum = 0.072 (at node 12)
	maximum = 0.378 (at node 162)
Accepted flit rate average= 0.166568
	minimum = 0.036 (at node 41)
	maximum = 0.304 (at node 91)
Injected packet length average = 17.8249
Accepted packet length average = 19.778
Total in-flight flits = 6881 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 223.51
	minimum = 23
	maximum = 1603
Network latency average = 221.509
	minimum = 23
	maximum = 1603
Slowest packet = 284
Flit latency average = 143.303
	minimum = 6
	maximum = 1586
Slowest flit = 5129
Fragmentation average = 130.833
	minimum = 0
	maximum = 1254
Injected packet rate average = 0.0110964
	minimum = 0.0055 (at node 40)
	maximum = 0.017 (at node 166)
Accepted packet rate average = 0.00920052
	minimum = 0.0045 (at node 174)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.198698
	minimum = 0.0955 (at node 40)
	maximum = 0.306 (at node 166)
Accepted flit rate average= 0.175036
	minimum = 0.0925 (at node 174)
	maximum = 0.298 (at node 22)
Injected packet length average = 17.9066
Accepted packet length average = 19.0246
Total in-flight flits = 9484 (0 measured)
latency change    = 0.289901
throughput change = 0.0483828
Class 0:
Packet latency average = 335.399
	minimum = 23
	maximum = 2183
Network latency average = 333.436
	minimum = 23
	maximum = 2179
Slowest packet = 1153
Flit latency average = 233.236
	minimum = 6
	maximum = 2520
Slowest flit = 11399
Fragmentation average = 176.203
	minimum = 0
	maximum = 1594
Injected packet rate average = 0.0112969
	minimum = 0.002 (at node 100)
	maximum = 0.022 (at node 144)
Accepted packet rate average = 0.0106094
	minimum = 0.003 (at node 153)
	maximum = 0.019 (at node 63)
Injected flit rate average = 0.203667
	minimum = 0.036 (at node 100)
	maximum = 0.386 (at node 144)
Accepted flit rate average= 0.193276
	minimum = 0.074 (at node 121)
	maximum = 0.356 (at node 78)
Injected packet length average = 18.0286
Accepted packet length average = 18.2175
Total in-flight flits = 11417 (0 measured)
latency change    = 0.3336
throughput change = 0.0943706
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 264.082
	minimum = 23
	maximum = 949
Network latency average = 262.052
	minimum = 23
	maximum = 949
Slowest packet = 6470
Flit latency average = 265.943
	minimum = 6
	maximum = 2953
Slowest flit = 29230
Fragmentation average = 156.136
	minimum = 0
	maximum = 628
Injected packet rate average = 0.0117604
	minimum = 0.002 (at node 104)
	maximum = 0.02 (at node 78)
Accepted packet rate average = 0.0107812
	minimum = 0.002 (at node 163)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.210964
	minimum = 0.036 (at node 104)
	maximum = 0.36 (at node 117)
Accepted flit rate average= 0.196333
	minimum = 0.036 (at node 163)
	maximum = 0.362 (at node 34)
Injected packet length average = 17.9384
Accepted packet length average = 18.2106
Total in-flight flits = 14365 (12063 measured)
latency change    = 0.270056
throughput change = 0.0155719
Class 0:
Packet latency average = 320.546
	minimum = 23
	maximum = 1776
Network latency average = 318.447
	minimum = 23
	maximum = 1776
Slowest packet = 6537
Flit latency average = 291.148
	minimum = 6
	maximum = 4632
Slowest flit = 11411
Fragmentation average = 173.175
	minimum = 0
	maximum = 1322
Injected packet rate average = 0.0114141
	minimum = 0.0065 (at node 18)
	maximum = 0.017 (at node 117)
Accepted packet rate average = 0.0109297
	minimum = 0.0055 (at node 163)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.20537
	minimum = 0.117 (at node 18)
	maximum = 0.3115 (at node 172)
Accepted flit rate average= 0.197422
	minimum = 0.1055 (at node 163)
	maximum = 0.3445 (at node 16)
Injected packet length average = 17.9927
Accepted packet length average = 18.0629
Total in-flight flits = 14501 (13408 measured)
latency change    = 0.176148
throughput change = 0.00551378
Class 0:
Packet latency average = 353.775
	minimum = 23
	maximum = 2856
Network latency average = 351.682
	minimum = 23
	maximum = 2856
Slowest packet = 6657
Flit latency average = 306.631
	minimum = 6
	maximum = 4758
Slowest flit = 42353
Fragmentation average = 183.567
	minimum = 0
	maximum = 2422
Injected packet rate average = 0.0113872
	minimum = 0.007 (at node 104)
	maximum = 0.0163333 (at node 172)
Accepted packet rate average = 0.0109687
	minimum = 0.00566667 (at node 4)
	maximum = 0.0166667 (at node 16)
Injected flit rate average = 0.205071
	minimum = 0.126 (at node 104)
	maximum = 0.297667 (at node 172)
Accepted flit rate average= 0.198161
	minimum = 0.0983333 (at node 4)
	maximum = 0.304 (at node 16)
Injected packet length average = 18.009
Accepted packet length average = 18.066
Total in-flight flits = 15338 (14749 measured)
latency change    = 0.0939277
throughput change = 0.00373223
Class 0:
Packet latency average = 380.583
	minimum = 23
	maximum = 3727
Network latency average = 378.488
	minimum = 23
	maximum = 3727
Slowest packet = 6627
Flit latency average = 316.34
	minimum = 6
	maximum = 4758
Slowest flit = 42353
Fragmentation average = 191.882
	minimum = 0
	maximum = 3592
Injected packet rate average = 0.0113724
	minimum = 0.0075 (at node 104)
	maximum = 0.0155 (at node 142)
Accepted packet rate average = 0.0109635
	minimum = 0.0075 (at node 163)
	maximum = 0.0155 (at node 128)
Injected flit rate average = 0.204727
	minimum = 0.135 (at node 104)
	maximum = 0.28225 (at node 142)
Accepted flit rate average= 0.19819
	minimum = 0.13475 (at node 163)
	maximum = 0.28975 (at node 128)
Injected packet length average = 18.0021
Accepted packet length average = 18.0772
Total in-flight flits = 16419 (16036 measured)
latency change    = 0.0704386
throughput change = 0.000144537
Class 0:
Packet latency average = 405.117
	minimum = 23
	maximum = 4847
Network latency average = 403.033
	minimum = 23
	maximum = 4847
Slowest packet = 6532
Flit latency average = 330.671
	minimum = 6
	maximum = 7022
Slowest flit = 34235
Fragmentation average = 198.564
	minimum = 0
	maximum = 4051
Injected packet rate average = 0.0113156
	minimum = 0.008 (at node 2)
	maximum = 0.0148 (at node 117)
Accepted packet rate average = 0.0110115
	minimum = 0.0074 (at node 36)
	maximum = 0.0154 (at node 128)
Injected flit rate average = 0.203767
	minimum = 0.144 (at node 2)
	maximum = 0.269 (at node 142)
Accepted flit rate average= 0.198797
	minimum = 0.1312 (at node 36)
	maximum = 0.2804 (at node 128)
Injected packet length average = 18.0075
Accepted packet length average = 18.0536
Total in-flight flits = 16106 (15849 measured)
latency change    = 0.0605606
throughput change = 0.00305222
Class 0:
Packet latency average = 422.143
	minimum = 23
	maximum = 5368
Network latency average = 420.032
	minimum = 23
	maximum = 5368
Slowest packet = 7418
Flit latency average = 342.144
	minimum = 6
	maximum = 7022
Slowest flit = 34235
Fragmentation average = 200.795
	minimum = 0
	maximum = 4051
Injected packet rate average = 0.0113533
	minimum = 0.008 (at node 7)
	maximum = 0.0148333 (at node 181)
Accepted packet rate average = 0.0110278
	minimum = 0.008 (at node 64)
	maximum = 0.0148333 (at node 128)
Injected flit rate average = 0.204398
	minimum = 0.144 (at node 7)
	maximum = 0.267 (at node 181)
Accepted flit rate average= 0.199429
	minimum = 0.1455 (at node 64)
	maximum = 0.271833 (at node 128)
Injected packet length average = 18.0034
Accepted packet length average = 18.0842
Total in-flight flits = 17097 (16921 measured)
latency change    = 0.0403326
throughput change = 0.00316877
Class 0:
Packet latency average = 439.848
	minimum = 23
	maximum = 6602
Network latency average = 437.739
	minimum = 23
	maximum = 6602
Slowest packet = 7175
Flit latency average = 351.933
	minimum = 6
	maximum = 7734
Slowest flit = 80927
Fragmentation average = 205.569
	minimum = 0
	maximum = 4051
Injected packet rate average = 0.0113393
	minimum = 0.00871429 (at node 7)
	maximum = 0.0144286 (at node 0)
Accepted packet rate average = 0.0110856
	minimum = 0.00814286 (at node 64)
	maximum = 0.0145714 (at node 128)
Injected flit rate average = 0.204016
	minimum = 0.155 (at node 7)
	maximum = 0.259714 (at node 0)
Accepted flit rate average= 0.200065
	minimum = 0.148143 (at node 64)
	maximum = 0.264143 (at node 128)
Injected packet length average = 17.9919
Accepted packet length average = 18.0474
Total in-flight flits = 16849 (16749 measured)
latency change    = 0.0402536
throughput change = 0.00318224
Draining all recorded packets ...
Class 0:
Remaining flits: 52231 52232 52233 52234 52235 58306 58307 58308 58309 58310 [...] (17570 flits)
Measured flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (4750 flits)
Class 0:
Remaining flits: 74305 74306 74307 74308 74309 74310 74311 74312 74313 74314 [...] (17179 flits)
Measured flits: 123264 123265 123266 123267 123268 123269 123270 123271 123272 123273 [...] (2563 flits)
Class 0:
Remaining flits: 74305 74306 74307 74308 74309 74310 74311 74312 74313 74314 [...] (16984 flits)
Measured flits: 150962 150963 150964 150965 158852 158853 158854 158855 158856 158857 [...] (1464 flits)
Class 0:
Remaining flits: 74305 74306 74307 74308 74309 74310 74311 74312 74313 74314 [...] (17796 flits)
Measured flits: 150962 150963 150964 150965 171978 171979 171980 171981 171982 171983 [...] (1012 flits)
Class 0:
Remaining flits: 74305 74306 74307 74308 74309 74310 74311 74312 74313 74314 [...] (18262 flits)
Measured flits: 171978 171979 171980 171981 171982 171983 171984 171985 171986 171987 [...] (623 flits)
Class 0:
Remaining flits: 74309 74310 74311 74312 74313 74314 74315 74316 74317 74318 [...] (20525 flits)
Measured flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (434 flits)
Class 0:
Remaining flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (19359 flits)
Measured flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (257 flits)
Class 0:
Remaining flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (19342 flits)
Measured flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (217 flits)
Class 0:
Remaining flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (19782 flits)
Measured flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (171 flits)
Class 0:
Remaining flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (20308 flits)
Measured flits: 183960 183961 183962 183963 183964 183965 183966 183967 183968 183969 [...] (116 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (20909 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (95 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (21108 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (85 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (21660 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247630 [...] (61 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247633 [...] (21125 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 247633 [...] (45 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 308140 [...] (20420 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 308140 [...] (12 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 394375 [...] (20306 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 (9 flits)
Class 0:
Remaining flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 394375 [...] (20373 flits)
Measured flits: 232767 232768 232769 232770 232771 232772 232773 232774 232775 (9 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 745155 745156 745157 745158 745159 745160 745161 745162 745163 872820 [...] (240 flits)
Measured flits: (0 flits)
Time taken is 29103 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 555.758 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21975 (1 samples)
Network latency average = 553.625 (1 samples)
	minimum = 23 (1 samples)
	maximum = 21975 (1 samples)
Flit latency average = 468.098 (1 samples)
	minimum = 6 (1 samples)
	maximum = 21958 (1 samples)
Fragmentation average = 237.408 (1 samples)
	minimum = 0 (1 samples)
	maximum = 17283 (1 samples)
Injected packet rate average = 0.0113393 (1 samples)
	minimum = 0.00871429 (1 samples)
	maximum = 0.0144286 (1 samples)
Accepted packet rate average = 0.0110856 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0145714 (1 samples)
Injected flit rate average = 0.204016 (1 samples)
	minimum = 0.155 (1 samples)
	maximum = 0.259714 (1 samples)
Accepted flit rate average = 0.200065 (1 samples)
	minimum = 0.148143 (1 samples)
	maximum = 0.264143 (1 samples)
Injected packet size average = 17.9919 (1 samples)
Accepted packet size average = 18.0474 (1 samples)
Hops average = 5.05614 (1 samples)
Total run time 28.7501
