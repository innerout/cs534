BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 252.088
	minimum = 22
	maximum = 772
Network latency average = 245.175
	minimum = 22
	maximum = 752
Slowest packet = 1143
Flit latency average = 212.146
	minimum = 5
	maximum = 763
Slowest flit = 19670
Fragmentation average = 53.9706
	minimum = 0
	maximum = 393
Injected packet rate average = 0.026651
	minimum = 0.011 (at node 190)
	maximum = 0.038 (at node 82)
Accepted packet rate average = 0.0137969
	minimum = 0.007 (at node 115)
	maximum = 0.023 (at node 131)
Injected flit rate average = 0.475339
	minimum = 0.198 (at node 190)
	maximum = 0.684 (at node 82)
Accepted flit rate average= 0.261896
	minimum = 0.126 (at node 115)
	maximum = 0.428 (at node 44)
Injected packet length average = 17.8356
Accepted packet length average = 18.9823
Total in-flight flits = 41876 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 462.042
	minimum = 22
	maximum = 1533
Network latency average = 451.803
	minimum = 22
	maximum = 1519
Slowest packet = 2410
Flit latency average = 411.496
	minimum = 5
	maximum = 1592
Slowest flit = 34657
Fragmentation average = 70.6695
	minimum = 0
	maximum = 566
Injected packet rate average = 0.0257604
	minimum = 0.016 (at node 24)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0145599
	minimum = 0.0085 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.461453
	minimum = 0.288 (at node 24)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.269698
	minimum = 0.153 (at node 116)
	maximum = 0.3825 (at node 152)
Injected packet length average = 17.9133
Accepted packet length average = 18.5233
Total in-flight flits = 74834 (0 measured)
latency change    = 0.454405
throughput change = 0.028929
Class 0:
Packet latency average = 1065.28
	minimum = 23
	maximum = 2154
Network latency average = 1030.81
	minimum = 22
	maximum = 2147
Slowest packet = 4190
Flit latency average = 985.456
	minimum = 5
	maximum = 2159
Slowest flit = 74612
Fragmentation average = 96.5111
	minimum = 0
	maximum = 588
Injected packet rate average = 0.0213385
	minimum = 0.001 (at node 148)
	maximum = 0.035 (at node 38)
Accepted packet rate average = 0.015276
	minimum = 0.005 (at node 17)
	maximum = 0.029 (at node 34)
Injected flit rate average = 0.383417
	minimum = 0.011 (at node 148)
	maximum = 0.644 (at node 49)
Accepted flit rate average= 0.278297
	minimum = 0.099 (at node 17)
	maximum = 0.52 (at node 34)
Injected packet length average = 17.9683
Accepted packet length average = 18.2179
Total in-flight flits = 95345 (0 measured)
latency change    = 0.566273
throughput change = 0.0308985
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 460.557
	minimum = 22
	maximum = 2177
Network latency average = 74.2764
	minimum = 22
	maximum = 973
Slowest packet = 14097
Flit latency average = 1374.96
	minimum = 5
	maximum = 2835
Slowest flit = 105353
Fragmentation average = 7.42276
	minimum = 0
	maximum = 64
Injected packet rate average = 0.0171042
	minimum = 0.002 (at node 166)
	maximum = 0.031 (at node 149)
Accepted packet rate average = 0.0151354
	minimum = 0.006 (at node 36)
	maximum = 0.026 (at node 128)
Injected flit rate average = 0.30663
	minimum = 0.04 (at node 0)
	maximum = 0.557 (at node 149)
Accepted flit rate average= 0.270172
	minimum = 0.103 (at node 36)
	maximum = 0.468 (at node 123)
Injected packet length average = 17.9272
Accepted packet length average = 17.8503
Total in-flight flits = 102584 (54522 measured)
latency change    = 1.31303
throughput change = 0.0300734
Class 0:
Packet latency average = 1266.99
	minimum = 22
	maximum = 3167
Network latency average = 737.444
	minimum = 22
	maximum = 1983
Slowest packet = 14097
Flit latency average = 1509.48
	minimum = 5
	maximum = 3175
Slowest flit = 132623
Fragmentation average = 47.976
	minimum = 0
	maximum = 419
Injected packet rate average = 0.0163021
	minimum = 0.0025 (at node 48)
	maximum = 0.025 (at node 118)
Accepted packet rate average = 0.0150365
	minimum = 0.0085 (at node 36)
	maximum = 0.023 (at node 128)
Injected flit rate average = 0.293206
	minimum = 0.0535 (at node 48)
	maximum = 0.45 (at node 118)
Accepted flit rate average= 0.270107
	minimum = 0.162 (at node 36)
	maximum = 0.4195 (at node 128)
Injected packet length average = 17.9858
Accepted packet length average = 17.9635
Total in-flight flits = 104556 (92502 measured)
latency change    = 0.636496
throughput change = 0.000241031
Class 0:
Packet latency average = 1941.38
	minimum = 22
	maximum = 3871
Network latency average = 1377.52
	minimum = 22
	maximum = 2971
Slowest packet = 14097
Flit latency average = 1621
	minimum = 5
	maximum = 4100
Slowest flit = 146213
Fragmentation average = 78.7359
	minimum = 0
	maximum = 661
Injected packet rate average = 0.0158785
	minimum = 0.00433333 (at node 48)
	maximum = 0.0246667 (at node 2)
Accepted packet rate average = 0.0150226
	minimum = 0.01 (at node 36)
	maximum = 0.0226667 (at node 128)
Injected flit rate average = 0.285436
	minimum = 0.0836667 (at node 48)
	maximum = 0.438333 (at node 2)
Accepted flit rate average= 0.269703
	minimum = 0.186667 (at node 36)
	maximum = 0.407333 (at node 128)
Injected packet length average = 17.9763
Accepted packet length average = 17.9532
Total in-flight flits = 104912 (103802 measured)
latency change    = 0.347376
throughput change = 0.00149663
Draining remaining packets ...
Class 0:
Remaining flits: 246562 246563 249696 249697 249698 249699 249700 249701 249702 249703 [...] (57572 flits)
Measured flits: 253970 253971 253972 253973 253974 253975 253976 253977 253978 253979 [...] (57552 flits)
Class 0:
Remaining flits: 267336 267337 267338 267339 267340 267341 267342 267343 267344 267345 [...] (11251 flits)
Measured flits: 267336 267337 267338 267339 267340 267341 267342 267343 267344 267345 [...] (11251 flits)
Time taken is 8832 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2743.61 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5437 (1 samples)
Network latency average = 1992.54 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4991 (1 samples)
Flit latency average = 1879.83 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4974 (1 samples)
Fragmentation average = 96.0356 (1 samples)
	minimum = 0 (1 samples)
	maximum = 661 (1 samples)
Injected packet rate average = 0.0158785 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0246667 (1 samples)
Accepted packet rate average = 0.0150226 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.285436 (1 samples)
	minimum = 0.0836667 (1 samples)
	maximum = 0.438333 (1 samples)
Accepted flit rate average = 0.269703 (1 samples)
	minimum = 0.186667 (1 samples)
	maximum = 0.407333 (1 samples)
Injected packet size average = 17.9763 (1 samples)
Accepted packet size average = 17.9532 (1 samples)
Hops average = 5.13087 (1 samples)
Total run time 13.1422
