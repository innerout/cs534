BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 300.081
	minimum = 22
	maximum = 850
Network latency average = 285.699
	minimum = 22
	maximum = 793
Slowest packet = 666
Flit latency average = 249.841
	minimum = 5
	maximum = 839
Slowest flit = 15817
Fragmentation average = 84.5238
	minimum = 0
	maximum = 639
Injected packet rate average = 0.0351354
	minimum = 0.024 (at node 4)
	maximum = 0.049 (at node 7)
Accepted packet rate average = 0.0142292
	minimum = 0.006 (at node 96)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.627089
	minimum = 0.424 (at node 120)
	maximum = 0.87 (at node 7)
Accepted flit rate average= 0.278573
	minimum = 0.114 (at node 115)
	maximum = 0.473 (at node 76)
Injected packet length average = 17.8478
Accepted packet length average = 19.5776
Total in-flight flits = 68140 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 588.729
	minimum = 22
	maximum = 1655
Network latency average = 559.417
	minimum = 22
	maximum = 1625
Slowest packet = 2437
Flit latency average = 509.97
	minimum = 5
	maximum = 1658
Slowest flit = 41893
Fragmentation average = 112.969
	minimum = 0
	maximum = 823
Injected packet rate average = 0.0293073
	minimum = 0.018 (at node 4)
	maximum = 0.0395 (at node 7)
Accepted packet rate average = 0.0147552
	minimum = 0.0095 (at node 96)
	maximum = 0.021 (at node 14)
Injected flit rate average = 0.523966
	minimum = 0.3185 (at node 4)
	maximum = 0.703 (at node 7)
Accepted flit rate average= 0.277018
	minimum = 0.1795 (at node 153)
	maximum = 0.393 (at node 44)
Injected packet length average = 17.8784
Accepted packet length average = 18.7743
Total in-flight flits = 96467 (0 measured)
latency change    = 0.49029
throughput change = 0.00561222
Class 0:
Packet latency average = 1497.83
	minimum = 35
	maximum = 2339
Network latency average = 1364.59
	minimum = 22
	maximum = 2273
Slowest packet = 4377
Flit latency average = 1310.43
	minimum = 5
	maximum = 2307
Slowest flit = 85042
Fragmentation average = 143.121
	minimum = 0
	maximum = 804
Injected packet rate average = 0.0159635
	minimum = 0.004 (at node 136)
	maximum = 0.033 (at node 145)
Accepted packet rate average = 0.0149844
	minimum = 0.005 (at node 184)
	maximum = 0.026 (at node 182)
Injected flit rate average = 0.288125
	minimum = 0.072 (at node 136)
	maximum = 0.599 (at node 145)
Accepted flit rate average= 0.268641
	minimum = 0.101 (at node 184)
	maximum = 0.471 (at node 182)
Injected packet length average = 18.0489
Accepted packet length average = 17.9281
Total in-flight flits = 100382 (0 measured)
latency change    = 0.606946
throughput change = 0.0311852
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1330.51
	minimum = 259
	maximum = 2620
Network latency average = 88.1707
	minimum = 22
	maximum = 987
Slowest packet = 14375
Flit latency average = 1663.28
	minimum = 5
	maximum = 3071
Slowest flit = 93761
Fragmentation average = 9.26016
	minimum = 0
	maximum = 180
Injected packet rate average = 0.0156563
	minimum = 0.001 (at node 51)
	maximum = 0.031 (at node 135)
Accepted packet rate average = 0.0149063
	minimum = 0.005 (at node 1)
	maximum = 0.027 (at node 128)
Injected flit rate average = 0.281688
	minimum = 0.003 (at node 51)
	maximum = 0.562 (at node 135)
Accepted flit rate average= 0.269187
	minimum = 0.114 (at node 1)
	maximum = 0.512 (at node 128)
Injected packet length average = 17.992
Accepted packet length average = 18.0587
Total in-flight flits = 102680 (49429 measured)
latency change    = 0.125759
throughput change = 0.00203158
Class 0:
Packet latency average = 2154.43
	minimum = 259
	maximum = 3794
Network latency average = 670.013
	minimum = 22
	maximum = 1988
Slowest packet = 14375
Flit latency average = 1749.19
	minimum = 5
	maximum = 3711
Slowest flit = 148693
Fragmentation average = 53.2966
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0154583
	minimum = 0.0055 (at node 48)
	maximum = 0.025 (at node 35)
Accepted packet rate average = 0.0150495
	minimum = 0.0095 (at node 146)
	maximum = 0.022 (at node 181)
Injected flit rate average = 0.278242
	minimum = 0.0995 (at node 48)
	maximum = 0.4535 (at node 35)
Accepted flit rate average= 0.269714
	minimum = 0.16 (at node 146)
	maximum = 0.399 (at node 128)
Injected packet length average = 17.9995
Accepted packet length average = 17.9218
Total in-flight flits = 103822 (88640 measured)
latency change    = 0.382433
throughput change = 0.00195037
Class 0:
Packet latency average = 2901.29
	minimum = 259
	maximum = 4489
Network latency average = 1332.25
	minimum = 22
	maximum = 2960
Slowest packet = 14375
Flit latency average = 1800.23
	minimum = 5
	maximum = 4248
Slowest flit = 153467
Fragmentation average = 91.2238
	minimum = 0
	maximum = 806
Injected packet rate average = 0.0153229
	minimum = 0.00533333 (at node 48)
	maximum = 0.023 (at node 143)
Accepted packet rate average = 0.0150156
	minimum = 0.0103333 (at node 139)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.275938
	minimum = 0.0966667 (at node 48)
	maximum = 0.409 (at node 143)
Accepted flit rate average= 0.270092
	minimum = 0.187 (at node 146)
	maximum = 0.378 (at node 128)
Injected packet length average = 18.0082
Accepted packet length average = 17.9874
Total in-flight flits = 103785 (102383 measured)
latency change    = 0.257421
throughput change = 0.00140127
Draining remaining packets ...
Class 0:
Remaining flits: 239922 239923 239924 239925 239926 239927 239928 239929 239930 239931 [...] (56271 flits)
Measured flits: 258570 258571 258572 258573 258574 258575 258576 258577 258578 258579 [...] (56199 flits)
Class 0:
Remaining flits: 299569 299570 299571 299572 299573 300317 300318 300319 300320 300321 [...] (9772 flits)
Measured flits: 299569 299570 299571 299572 299573 300317 300318 300319 300320 300321 [...] (9772 flits)
Time taken is 8923 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3847.73 (1 samples)
	minimum = 259 (1 samples)
	maximum = 6674 (1 samples)
Network latency average = 1987.31 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4536 (1 samples)
Flit latency average = 1976.14 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4789 (1 samples)
Fragmentation average = 109.518 (1 samples)
	minimum = 0 (1 samples)
	maximum = 806 (1 samples)
Injected packet rate average = 0.0153229 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.023 (1 samples)
Accepted packet rate average = 0.0150156 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.275938 (1 samples)
	minimum = 0.0966667 (1 samples)
	maximum = 0.409 (1 samples)
Accepted flit rate average = 0.270092 (1 samples)
	minimum = 0.187 (1 samples)
	maximum = 0.378 (1 samples)
Injected packet size average = 18.0082 (1 samples)
Accepted packet size average = 17.9874 (1 samples)
Hops average = 5.12172 (1 samples)
Total run time 13.7629
