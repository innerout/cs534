BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 307.257
	minimum = 25
	maximum = 850
Network latency average = 291.346
	minimum = 22
	maximum = 813
Slowest packet = 771
Flit latency average = 261.226
	minimum = 5
	maximum = 846
Slowest flit = 13084
Fragmentation average = 74.9082
	minimum = 0
	maximum = 651
Injected packet rate average = 0.0409062
	minimum = 0.028 (at node 184)
	maximum = 0.054 (at node 86)
Accepted packet rate average = 0.0145781
	minimum = 0.007 (at node 150)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.730271
	minimum = 0.503 (at node 184)
	maximum = 0.963 (at node 124)
Accepted flit rate average= 0.287161
	minimum = 0.151 (at node 135)
	maximum = 0.456 (at node 44)
Injected packet length average = 17.8523
Accepted packet length average = 19.6981
Total in-flight flits = 86237 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.767
	minimum = 23
	maximum = 1719
Network latency average = 557.834
	minimum = 22
	maximum = 1649
Slowest packet = 2040
Flit latency average = 515.311
	minimum = 5
	maximum = 1632
Slowest flit = 44927
Fragmentation average = 123.785
	minimum = 0
	maximum = 889
Injected packet rate average = 0.0417969
	minimum = 0.031 (at node 50)
	maximum = 0.052 (at node 154)
Accepted packet rate average = 0.0153984
	minimum = 0.0075 (at node 135)
	maximum = 0.025 (at node 152)
Injected flit rate average = 0.74924
	minimum = 0.556 (at node 50)
	maximum = 0.9345 (at node 154)
Accepted flit rate average= 0.294229
	minimum = 0.159 (at node 135)
	maximum = 0.4655 (at node 152)
Injected packet length average = 17.9257
Accepted packet length average = 19.1077
Total in-flight flits = 175916 (0 measured)
latency change    = 0.468198
throughput change = 0.0240211
Class 0:
Packet latency average = 1344.67
	minimum = 24
	maximum = 2568
Network latency average = 1319.09
	minimum = 22
	maximum = 2479
Slowest packet = 2938
Flit latency average = 1273.09
	minimum = 5
	maximum = 2490
Slowest flit = 67911
Fragmentation average = 229.598
	minimum = 0
	maximum = 870
Injected packet rate average = 0.0414219
	minimum = 0.024 (at node 124)
	maximum = 0.055 (at node 58)
Accepted packet rate average = 0.0162969
	minimum = 0.008 (at node 52)
	maximum = 0.027 (at node 63)
Injected flit rate average = 0.745422
	minimum = 0.428 (at node 124)
	maximum = 1 (at node 58)
Accepted flit rate average= 0.301552
	minimum = 0.155 (at node 118)
	maximum = 0.474 (at node 120)
Injected packet length average = 17.9959
Accepted packet length average = 18.5037
Total in-flight flits = 261172 (0 measured)
latency change    = 0.570329
throughput change = 0.0242841
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 87.6268
	minimum = 23
	maximum = 782
Network latency average = 38.4593
	minimum = 23
	maximum = 347
Slowest packet = 24028
Flit latency average = 1778.12
	minimum = 5
	maximum = 3378
Slowest flit = 82256
Fragmentation average = 6.32057
	minimum = 0
	maximum = 30
Injected packet rate average = 0.0401354
	minimum = 0.016 (at node 0)
	maximum = 0.055 (at node 161)
Accepted packet rate average = 0.0165938
	minimum = 0.009 (at node 53)
	maximum = 0.03 (at node 178)
Injected flit rate average = 0.722635
	minimum = 0.3 (at node 0)
	maximum = 0.984 (at node 161)
Accepted flit rate average= 0.304979
	minimum = 0.163 (at node 53)
	maximum = 0.581 (at node 178)
Injected packet length average = 18.0049
Accepted packet length average = 18.3792
Total in-flight flits = 341342 (127375 measured)
latency change    = 14.3455
throughput change = 0.0112371
Class 0:
Packet latency average = 117.417
	minimum = 23
	maximum = 1317
Network latency average = 40.2885
	minimum = 22
	maximum = 443
Slowest packet = 24028
Flit latency average = 2045.4
	minimum = 5
	maximum = 4291
Slowest flit = 90305
Fragmentation average = 6.48947
	minimum = 0
	maximum = 32
Injected packet rate average = 0.0394036
	minimum = 0.0215 (at node 0)
	maximum = 0.0555 (at node 161)
Accepted packet rate average = 0.0165729
	minimum = 0.0105 (at node 82)
	maximum = 0.025 (at node 178)
Injected flit rate average = 0.709271
	minimum = 0.3915 (at node 0)
	maximum = 0.992 (at node 161)
Accepted flit rate average= 0.304331
	minimum = 0.1905 (at node 141)
	maximum = 0.4575 (at node 178)
Injected packet length average = 18.0001
Accepted packet length average = 18.3631
Total in-flight flits = 416667 (250053 measured)
latency change    = 0.25371
throughput change = 0.0021307
Class 0:
Packet latency average = 154.068
	minimum = 23
	maximum = 1573
Network latency average = 44.6537
	minimum = 22
	maximum = 839
Slowest packet = 24028
Flit latency average = 2317.12
	minimum = 5
	maximum = 5104
Slowest flit = 124016
Fragmentation average = 6.57127
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0393785
	minimum = 0.0243333 (at node 56)
	maximum = 0.05 (at node 29)
Accepted packet rate average = 0.0164427
	minimum = 0.012 (at node 80)
	maximum = 0.022 (at node 129)
Injected flit rate average = 0.708637
	minimum = 0.435 (at node 56)
	maximum = 0.905333 (at node 29)
Accepted flit rate average= 0.301769
	minimum = 0.213 (at node 80)
	maximum = 0.399333 (at node 178)
Injected packet length average = 17.9955
Accepted packet length average = 18.3528
Total in-flight flits = 495647 (375858 measured)
latency change    = 0.237891
throughput change = 0.00848872
Draining remaining packets ...
Class 0:
Remaining flits: 157014 157015 157016 157017 157018 157019 157020 157021 157022 157023 [...] (447349 flits)
Measured flits: 432054 432055 432056 432057 432058 432059 432060 432061 432062 432063 [...] (371461 flits)
Class 0:
Remaining flits: 189569 189570 189571 189572 189573 189574 189575 203309 206199 206200 [...] (399847 flits)
Measured flits: 432054 432055 432056 432057 432058 432059 432060 432061 432062 432063 [...] (357580 flits)
Class 0:
Remaining flits: 239058 239059 239060 239061 239062 239063 239064 239065 239066 239067 [...] (352130 flits)
Measured flits: 432154 432155 432156 432157 432158 432159 432160 432161 432180 432181 [...] (333391 flits)
Class 0:
Remaining flits: 285533 304704 304705 304706 304707 304708 304709 304710 304711 304712 [...] (304734 flits)
Measured flits: 432183 432184 432185 432186 432187 432188 432189 432190 432191 432192 [...] (299590 flits)
Class 0:
Remaining flits: 334878 334879 334880 334881 334882 334883 334884 334885 334886 334887 [...] (257663 flits)
Measured flits: 432610 432611 434069 434165 434166 434167 434168 434169 434170 434171 [...] (256918 flits)
Class 0:
Remaining flits: 384238 384239 384240 384241 384242 384243 384244 384245 421632 421633 [...] (210430 flits)
Measured flits: 436788 436789 436790 436791 436792 436793 436794 436795 436796 436797 [...] (210404 flits)
Class 0:
Remaining flits: 456206 456207 456208 456209 456641 458622 458623 458624 458625 458626 [...] (163237 flits)
Measured flits: 456206 456207 456208 456209 456641 458622 458623 458624 458625 458626 [...] (163237 flits)
Class 0:
Remaining flits: 460404 460405 460406 460407 460408 460409 460410 460411 460412 460413 [...] (115438 flits)
Measured flits: 460404 460405 460406 460407 460408 460409 460410 460411 460412 460413 [...] (115438 flits)
Class 0:
Remaining flits: 472263 472264 472265 486774 486775 486776 486777 486778 486779 486780 [...] (68614 flits)
Measured flits: 472263 472264 472265 486774 486775 486776 486777 486778 486779 486780 [...] (68614 flits)
Class 0:
Remaining flits: 541116 541117 541118 541119 541120 541121 541122 541123 541124 541125 [...] (23880 flits)
Measured flits: 541116 541117 541118 541119 541120 541121 541122 541123 541124 541125 [...] (23880 flits)
Class 0:
Remaining flits: 580929 580930 580931 590937 590938 590939 637442 637443 637444 637445 [...] (2749 flits)
Measured flits: 580929 580930 580931 590937 590938 590939 637442 637443 637444 637445 [...] (2749 flits)
Time taken is 17835 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7531.34 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13152 (1 samples)
Network latency average = 7424.26 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13062 (1 samples)
Flit latency average = 5945.83 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13045 (1 samples)
Fragmentation average = 329.064 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1635 (1 samples)
Injected packet rate average = 0.0393785 (1 samples)
	minimum = 0.0243333 (1 samples)
	maximum = 0.05 (1 samples)
Accepted packet rate average = 0.0164427 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.022 (1 samples)
Injected flit rate average = 0.708637 (1 samples)
	minimum = 0.435 (1 samples)
	maximum = 0.905333 (1 samples)
Accepted flit rate average = 0.301769 (1 samples)
	minimum = 0.213 (1 samples)
	maximum = 0.399333 (1 samples)
Injected packet size average = 17.9955 (1 samples)
Accepted packet size average = 18.3528 (1 samples)
Hops average = 5.12035 (1 samples)
Total run time 23.2348
