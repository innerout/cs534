BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 288.297
	minimum = 27
	maximum = 963
Network latency average = 236.398
	minimum = 24
	maximum = 860
Slowest packet = 135
Flit latency average = 160.233
	minimum = 6
	maximum = 931
Slowest flit = 4868
Fragmentation average = 133.418
	minimum = 0
	maximum = 697
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00872396
	minimum = 0.003 (at node 12)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.177109
	minimum = 0.058 (at node 93)
	maximum = 0.312 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.3015
Total in-flight flits = 14145 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 387.274
	minimum = 27
	maximum = 1984
Network latency average = 330.372
	minimum = 23
	maximum = 1868
Slowest packet = 466
Flit latency average = 237.678
	minimum = 6
	maximum = 1851
Slowest flit = 8369
Fragmentation average = 169.17
	minimum = 0
	maximum = 1427
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00958594
	minimum = 0.005 (at node 96)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.184854
	minimum = 0.093 (at node 96)
	maximum = 0.2905 (at node 152)
Injected packet length average = 17.9095
Accepted packet length average = 19.2839
Total in-flight flits = 20096 (0 measured)
latency change    = 0.255572
throughput change = 0.0418968
Class 0:
Packet latency average = 580.503
	minimum = 26
	maximum = 2693
Network latency average = 520.021
	minimum = 23
	maximum = 2611
Slowest packet = 828
Flit latency average = 403.311
	minimum = 6
	maximum = 2911
Slowest flit = 5979
Fragmentation average = 238.395
	minimum = 0
	maximum = 2414
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0109583
	minimum = 0.005 (at node 62)
	maximum = 0.021 (at node 131)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.201667
	minimum = 0.094 (at node 178)
	maximum = 0.334 (at node 131)
Injected packet length average = 18.0569
Accepted packet length average = 18.403
Total in-flight flits = 26304 (0 measured)
latency change    = 0.332865
throughput change = 0.0833678
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 369.26
	minimum = 27
	maximum = 1092
Network latency average = 313.294
	minimum = 23
	maximum = 940
Slowest packet = 7572
Flit latency average = 513.123
	minimum = 6
	maximum = 3769
Slowest flit = 2843
Fragmentation average = 175.409
	minimum = 0
	maximum = 716
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0115833
	minimum = 0.003 (at node 113)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.210615
	minimum = 0.069 (at node 153)
	maximum = 0.412 (at node 16)
Injected packet length average = 17.981
Accepted packet length average = 18.1826
Total in-flight flits = 32234 (22819 measured)
latency change    = 0.57207
throughput change = 0.0424848
Class 0:
Packet latency average = 502.063
	minimum = 25
	maximum = 2119
Network latency average = 440.166
	minimum = 23
	maximum = 1906
Slowest packet = 7572
Flit latency average = 555.518
	minimum = 6
	maximum = 4627
Slowest flit = 19886
Fragmentation average = 204.287
	minimum = 0
	maximum = 1264
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.0116849
	minimum = 0.0065 (at node 153)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.213247
	minimum = 0.118 (at node 4)
	maximum = 0.386 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.2498
Total in-flight flits = 38773 (33750 measured)
latency change    = 0.264515
throughput change = 0.0123463
Class 0:
Packet latency average = 616.655
	minimum = 25
	maximum = 2996
Network latency average = 553.46
	minimum = 23
	maximum = 2837
Slowest packet = 7591
Flit latency average = 610.484
	minimum = 6
	maximum = 5354
Slowest flit = 13571
Fragmentation average = 226.146
	minimum = 0
	maximum = 2555
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0117969
	minimum = 0.00733333 (at node 64)
	maximum = 0.0166667 (at node 16)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.21417
	minimum = 0.142667 (at node 64)
	maximum = 0.297 (at node 16)
Injected packet length average = 17.9913
Accepted packet length average = 18.1548
Total in-flight flits = 39472 (36410 measured)
latency change    = 0.185827
throughput change = 0.00430846
Draining remaining packets ...
Class 0:
Remaining flits: 29358 29359 29360 29361 29362 29363 29364 29365 29366 29367 [...] (10019 flits)
Measured flits: 136062 136063 136064 136065 136066 136067 136068 136069 136070 136071 [...] (8479 flits)
Class 0:
Remaining flits: 102042 102043 102044 102045 102046 102047 102048 102049 102050 102051 [...] (227 flits)
Measured flits: 170820 170821 170822 170823 170824 170825 170826 170827 170828 170829 [...] (155 flits)
Time taken is 8227 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 946.057 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4972 (1 samples)
Network latency average = 877.615 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4716 (1 samples)
Flit latency average = 903.091 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7020 (1 samples)
Fragmentation average = 239.459 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2821 (1 samples)
Injected packet rate average = 0.0131684 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.0117969 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.236917 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.517 (1 samples)
Accepted flit rate average = 0.21417 (1 samples)
	minimum = 0.142667 (1 samples)
	maximum = 0.297 (1 samples)
Injected packet size average = 17.9913 (1 samples)
Accepted packet size average = 18.1548 (1 samples)
Hops average = 5.07673 (1 samples)
Total run time 7.61377
