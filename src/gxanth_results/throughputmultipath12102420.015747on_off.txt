BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.08
	minimum = 22
	maximum = 873
Network latency average = 150.635
	minimum = 22
	maximum = 680
Slowest packet = 70
Flit latency average = 127.143
	minimum = 5
	maximum = 663
Slowest flit = 16937
Fragmentation average = 14.6005
	minimum = 0
	maximum = 76
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117865
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.216839
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.3973
Total in-flight flits = 12961 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 301.839
	minimum = 22
	maximum = 1568
Network latency average = 230.915
	minimum = 22
	maximum = 978
Slowest packet = 70
Flit latency average = 207.373
	minimum = 5
	maximum = 961
Slowest flit = 50885
Fragmentation average = 15.7645
	minimum = 0
	maximum = 94
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125964
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 71)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.22881
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 71)
Injected packet length average = 17.9137
Accepted packet length average = 18.1648
Total in-flight flits = 19777 (0 measured)
latency change    = 0.294063
throughput change = 0.0523201
Class 0:
Packet latency average = 498.49
	minimum = 22
	maximum = 1645
Network latency average = 423.111
	minimum = 22
	maximum = 1342
Slowest packet = 3189
Flit latency average = 398.029
	minimum = 5
	maximum = 1325
Slowest flit = 86705
Fragmentation average = 17.3863
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140365
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.252953
	minimum = 0.119 (at node 4)
	maximum = 0.414 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.0212
Total in-flight flits = 27190 (0 measured)
latency change    = 0.394493
throughput change = 0.0954455
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 389.487
	minimum = 22
	maximum = 1192
Network latency average = 316.367
	minimum = 22
	maximum = 965
Slowest packet = 9091
Flit latency average = 511.805
	minimum = 5
	maximum = 1664
Slowest flit = 105937
Fragmentation average = 13.9381
	minimum = 0
	maximum = 76
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141406
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.254401
	minimum = 0.074 (at node 48)
	maximum = 0.517 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9908
Total in-flight flits = 30257 (28229 measured)
latency change    = 0.279864
throughput change = 0.00569147
Class 0:
Packet latency average = 579.183
	minimum = 22
	maximum = 2047
Network latency average = 500.177
	minimum = 22
	maximum = 1751
Slowest packet = 9091
Flit latency average = 555.584
	minimum = 5
	maximum = 2121
Slowest flit = 156474
Fragmentation average = 16.7741
	minimum = 0
	maximum = 85
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0142188
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.256141
	minimum = 0.153 (at node 36)
	maximum = 0.378 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0143
Total in-flight flits = 32962 (32933 measured)
latency change    = 0.327524
throughput change = 0.00679152
Class 0:
Packet latency average = 649.248
	minimum = 22
	maximum = 2337
Network latency average = 570.592
	minimum = 22
	maximum = 2027
Slowest packet = 9091
Flit latency average = 585.899
	minimum = 5
	maximum = 2232
Slowest flit = 158256
Fragmentation average = 16.4978
	minimum = 0
	maximum = 85
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143333
	minimum = 0.00833333 (at node 36)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257977
	minimum = 0.153 (at node 36)
	maximum = 0.362 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.9984
Total in-flight flits = 34421 (34421 measured)
latency change    = 0.107917
throughput change = 0.00712002
Draining remaining packets ...
Class 0:
Remaining flits: 271350 271351 271352 271353 271354 271355 271356 271357 271358 271359 [...] (2756 flits)
Measured flits: 271350 271351 271352 271353 271354 271355 271356 271357 271358 271359 [...] (2756 flits)
Time taken is 7659 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 740.13 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2375 (1 samples)
Network latency average = 658.463 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2061 (1 samples)
Flit latency average = 652.625 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2232 (1 samples)
Fragmentation average = 16.0015 (1 samples)
	minimum = 0 (1 samples)
	maximum = 85 (1 samples)
Injected packet rate average = 0.0150295 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0143333 (1 samples)
	minimum = 0.00833333 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.270549 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.581667 (1 samples)
Accepted flit rate average = 0.257977 (1 samples)
	minimum = 0.153 (1 samples)
	maximum = 0.362 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 17.9984 (1 samples)
Hops average = 5.05776 (1 samples)
Total run time 4.1415
