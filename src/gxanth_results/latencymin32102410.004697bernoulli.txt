BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.004697
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 53.7964
	minimum = 23
	maximum = 181
Network latency average = 52.8922
	minimum = 23
	maximum = 181
Slowest packet = 85
Flit latency average = 29.1509
	minimum = 6
	maximum = 164
Slowest flit = 1547
Fragmentation average = 13.1641
	minimum = 0
	maximum = 129
Injected packet rate average = 0.00458333
	minimum = 0 (at node 33)
	maximum = 0.01 (at node 39)
Accepted packet rate average = 0.00434896
	minimum = 0.001 (at node 4)
	maximum = 0.011 (at node 140)
Injected flit rate average = 0.0817552
	minimum = 0 (at node 33)
	maximum = 0.18 (at node 39)
Accepted flit rate average= 0.0793698
	minimum = 0.018 (at node 4)
	maximum = 0.198 (at node 140)
Injected packet length average = 17.8375
Accepted packet length average = 18.2503
Total in-flight flits = 601 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 54.3194
	minimum = 23
	maximum = 213
Network latency average = 53.5676
	minimum = 23
	maximum = 205
Slowest packet = 931
Flit latency average = 29.5471
	minimum = 6
	maximum = 188
Slowest flit = 17351
Fragmentation average = 13.5879
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00447656
	minimum = 0.0005 (at node 82)
	maximum = 0.0085 (at node 132)
Accepted packet rate average = 0.00435417
	minimum = 0.0005 (at node 41)
	maximum = 0.008 (at node 48)
Injected flit rate average = 0.080276
	minimum = 0.009 (at node 82)
	maximum = 0.153 (at node 132)
Accepted flit rate average= 0.0791146
	minimum = 0.009 (at node 41)
	maximum = 0.144 (at node 48)
Injected packet length average = 17.9325
Accepted packet length average = 18.1699
Total in-flight flits = 562 (0 measured)
latency change    = 0.0096277
throughput change = 0.00322581
Class 0:
Packet latency average = 54.0033
	minimum = 23
	maximum = 157
Network latency average = 53.1674
	minimum = 23
	maximum = 157
Slowest packet = 2074
Flit latency average = 29.0824
	minimum = 6
	maximum = 140
Slowest flit = 37349
Fragmentation average = 13.4694
	minimum = 0
	maximum = 118
Injected packet rate average = 0.00477604
	minimum = 0 (at node 40)
	maximum = 0.013 (at node 121)
Accepted packet rate average = 0.00476042
	minimum = 0 (at node 17)
	maximum = 0.011 (at node 168)
Injected flit rate average = 0.0858698
	minimum = 0 (at node 40)
	maximum = 0.234 (at node 121)
Accepted flit rate average= 0.0856094
	minimum = 0 (at node 17)
	maximum = 0.198 (at node 168)
Injected packet length average = 17.9793
Accepted packet length average = 17.9836
Total in-flight flits = 631 (0 measured)
latency change    = 0.00585327
throughput change = 0.0758654
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 56.746
	minimum = 23
	maximum = 170
Network latency average = 56.0057
	minimum = 23
	maximum = 170
Slowest packet = 3335
Flit latency average = 31.0005
	minimum = 6
	maximum = 153
Slowest flit = 60047
Fragmentation average = 15.7132
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00483854
	minimum = 0.001 (at node 0)
	maximum = 0.011 (at node 128)
Accepted packet rate average = 0.00485417
	minimum = 0 (at node 18)
	maximum = 0.013 (at node 22)
Injected flit rate average = 0.087026
	minimum = 0.018 (at node 0)
	maximum = 0.198 (at node 128)
Accepted flit rate average= 0.0871094
	minimum = 0 (at node 18)
	maximum = 0.234 (at node 22)
Injected packet length average = 17.986
Accepted packet length average = 17.9453
Total in-flight flits = 628 (628 measured)
latency change    = 0.0483338
throughput change = 0.0172197
Class 0:
Packet latency average = 55.3871
	minimum = 23
	maximum = 170
Network latency average = 54.6358
	minimum = 23
	maximum = 170
Slowest packet = 3335
Flit latency average = 30.1668
	minimum = 6
	maximum = 153
Slowest flit = 60047
Fragmentation average = 14.5317
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00467187
	minimum = 0.001 (at node 0)
	maximum = 0.01 (at node 128)
Accepted packet rate average = 0.0046849
	minimum = 0.001 (at node 24)
	maximum = 0.0085 (at node 22)
Injected flit rate average = 0.0840313
	minimum = 0.0095 (at node 0)
	maximum = 0.18 (at node 128)
Accepted flit rate average= 0.0842266
	minimum = 0.018 (at node 24)
	maximum = 0.153 (at node 22)
Injected packet length average = 17.9866
Accepted packet length average = 17.9783
Total in-flight flits = 580 (580 measured)
latency change    = 0.0245356
throughput change = 0.0342269
Class 0:
Packet latency average = 54.6588
	minimum = 23
	maximum = 170
Network latency average = 53.9817
	minimum = 23
	maximum = 170
Slowest packet = 3335
Flit latency average = 29.8147
	minimum = 6
	maximum = 153
Slowest flit = 60047
Fragmentation average = 13.9308
	minimum = 0
	maximum = 122
Injected packet rate average = 0.00464583
	minimum = 0.000666667 (at node 0)
	maximum = 0.008 (at node 128)
Accepted packet rate average = 0.00465104
	minimum = 0.00133333 (at node 153)
	maximum = 0.00733333 (at node 97)
Injected flit rate average = 0.0837483
	minimum = 0.012 (at node 0)
	maximum = 0.144 (at node 128)
Accepted flit rate average= 0.0837413
	minimum = 0.024 (at node 153)
	maximum = 0.132 (at node 97)
Injected packet length average = 18.0265
Accepted packet length average = 18.0049
Total in-flight flits = 564 (564 measured)
latency change    = 0.013324
throughput change = 0.00579455
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6180 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 54.9836 (1 samples)
	minimum = 23 (1 samples)
	maximum = 198 (1 samples)
Network latency average = 54.2749 (1 samples)
	minimum = 23 (1 samples)
	maximum = 198 (1 samples)
Flit latency average = 30.0606 (1 samples)
	minimum = 6 (1 samples)
	maximum = 181 (1 samples)
Fragmentation average = 14.2036 (1 samples)
	minimum = 0 (1 samples)
	maximum = 145 (1 samples)
Injected packet rate average = 0.00464583 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.008 (1 samples)
Accepted packet rate average = 0.00465104 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.00733333 (1 samples)
Injected flit rate average = 0.0837483 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.144 (1 samples)
Accepted flit rate average = 0.0837413 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.132 (1 samples)
Injected packet size average = 18.0265 (1 samples)
Accepted packet size average = 18.0049 (1 samples)
Hops average = 5.09189 (1 samples)
Total run time 1.70984
