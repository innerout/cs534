BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 294.504
	minimum = 22
	maximum = 828
Network latency average = 282.596
	minimum = 22
	maximum = 806
Slowest packet = 1061
Flit latency average = 258.82
	minimum = 5
	maximum = 789
Slowest flit = 19115
Fragmentation average = 23.8867
	minimum = 0
	maximum = 96
Injected packet rate average = 0.0352344
	minimum = 0.018 (at node 99)
	maximum = 0.052 (at node 160)
Accepted packet rate average = 0.0146615
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 103)
Injected flit rate average = 0.628312
	minimum = 0.324 (at node 99)
	maximum = 0.936 (at node 160)
Accepted flit rate average= 0.270411
	minimum = 0.09 (at node 174)
	maximum = 0.432 (at node 103)
Injected packet length average = 17.8324
Accepted packet length average = 18.4437
Total in-flight flits = 69851 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 542.324
	minimum = 22
	maximum = 1523
Network latency average = 528.328
	minimum = 22
	maximum = 1509
Slowest packet = 2486
Flit latency average = 504.57
	minimum = 5
	maximum = 1492
Slowest flit = 54971
Fragmentation average = 24.6064
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0354844
	minimum = 0.0215 (at node 120)
	maximum = 0.0485 (at node 160)
Accepted packet rate average = 0.0155964
	minimum = 0.009 (at node 153)
	maximum = 0.021 (at node 67)
Injected flit rate average = 0.636125
	minimum = 0.387 (at node 120)
	maximum = 0.8695 (at node 160)
Accepted flit rate average= 0.283951
	minimum = 0.162 (at node 153)
	maximum = 0.3815 (at node 152)
Injected packet length average = 17.9269
Accepted packet length average = 18.2062
Total in-flight flits = 136231 (0 measured)
latency change    = 0.456958
throughput change = 0.0476811
Class 0:
Packet latency average = 1263.56
	minimum = 22
	maximum = 2225
Network latency average = 1248.91
	minimum = 22
	maximum = 2210
Slowest packet = 5312
Flit latency average = 1232
	minimum = 5
	maximum = 2193
Slowest flit = 96227
Fragmentation average = 26.6314
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0353229
	minimum = 0.02 (at node 116)
	maximum = 0.051 (at node 86)
Accepted packet rate average = 0.0161094
	minimum = 0.007 (at node 52)
	maximum = 0.029 (at node 159)
Injected flit rate average = 0.635583
	minimum = 0.36 (at node 190)
	maximum = 0.927 (at node 86)
Accepted flit rate average= 0.291245
	minimum = 0.126 (at node 52)
	maximum = 0.513 (at node 159)
Injected packet length average = 17.9935
Accepted packet length average = 18.0792
Total in-flight flits = 202388 (0 measured)
latency change    = 0.570797
throughput change = 0.0250452
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 53.5144
	minimum = 22
	maximum = 174
Network latency average = 38.9904
	minimum = 22
	maximum = 136
Slowest packet = 21177
Flit latency average = 1714.32
	minimum = 5
	maximum = 2859
Slowest flit = 135557
Fragmentation average = 4.66027
	minimum = 0
	maximum = 27
Injected packet rate average = 0.0362344
	minimum = 0.019 (at node 168)
	maximum = 0.052 (at node 30)
Accepted packet rate average = 0.0163438
	minimum = 0.007 (at node 36)
	maximum = 0.028 (at node 19)
Injected flit rate average = 0.652
	minimum = 0.342 (at node 168)
	maximum = 0.936 (at node 30)
Accepted flit rate average= 0.293953
	minimum = 0.137 (at node 36)
	maximum = 0.53 (at node 129)
Injected packet length average = 17.994
Accepted packet length average = 17.9857
Total in-flight flits = 271175 (115753 measured)
latency change    = 22.6116
throughput change = 0.00921349
Class 0:
Packet latency average = 55.1086
	minimum = 22
	maximum = 205
Network latency average = 41.015
	minimum = 22
	maximum = 181
Slowest packet = 21177
Flit latency average = 1933.5
	minimum = 5
	maximum = 3655
Slowest flit = 147329
Fragmentation average = 4.78641
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0358438
	minimum = 0.0265 (at node 168)
	maximum = 0.0495 (at node 187)
Accepted packet rate average = 0.0165104
	minimum = 0.011 (at node 145)
	maximum = 0.027 (at node 129)
Injected flit rate average = 0.645065
	minimum = 0.477 (at node 168)
	maximum = 0.8905 (at node 187)
Accepted flit rate average= 0.296734
	minimum = 0.194 (at node 145)
	maximum = 0.4845 (at node 129)
Injected packet length average = 17.9966
Accepted packet length average = 17.9726
Total in-flight flits = 336194 (227195 measured)
latency change    = 0.0289277
throughput change = 0.00937286
Class 0:
Packet latency average = 55.5367
	minimum = 22
	maximum = 220
Network latency average = 41.1568
	minimum = 22
	maximum = 181
Slowest packet = 21177
Flit latency average = 2172.65
	minimum = 5
	maximum = 4324
Slowest flit = 195767
Fragmentation average = 4.73135
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0357135
	minimum = 0.0286667 (at node 132)
	maximum = 0.0486667 (at node 187)
Accepted packet rate average = 0.0165399
	minimum = 0.011 (at node 36)
	maximum = 0.0256667 (at node 129)
Injected flit rate average = 0.642964
	minimum = 0.517 (at node 132)
	maximum = 0.871 (at node 187)
Accepted flit rate average= 0.297557
	minimum = 0.198 (at node 36)
	maximum = 0.463333 (at node 129)
Injected packet length average = 18.0034
Accepted packet length average = 17.9902
Total in-flight flits = 401273 (339250 measured)
latency change    = 0.00770935
throughput change = 0.00276557
Class 0:
Packet latency average = 518.421
	minimum = 22
	maximum = 3993
Network latency average = 503.946
	minimum = 22
	maximum = 3974
Slowest packet = 20426
Flit latency average = 2409.96
	minimum = 5
	maximum = 4966
Slowest flit = 245321
Fragmentation average = 8.66541
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0357253
	minimum = 0.0275 (at node 28)
	maximum = 0.044 (at node 169)
Accepted packet rate average = 0.0166094
	minimum = 0.01125 (at node 171)
	maximum = 0.02425 (at node 129)
Injected flit rate average = 0.643023
	minimum = 0.4965 (at node 28)
	maximum = 0.792 (at node 169)
Accepted flit rate average= 0.298637
	minimum = 0.2025 (at node 171)
	maximum = 0.44 (at node 129)
Injected packet length average = 17.9991
Accepted packet length average = 17.98
Total in-flight flits = 466919 (445573 measured)
latency change    = 0.892873
throughput change = 0.00361452
Class 0:
Packet latency average = 1697.34
	minimum = 22
	maximum = 5009
Network latency average = 1682.55
	minimum = 22
	maximum = 4959
Slowest packet = 20520
Flit latency average = 2649.13
	minimum = 5
	maximum = 5787
Slowest flit = 269910
Fragmentation average = 15.5863
	minimum = 0
	maximum = 102
Injected packet rate average = 0.035775
	minimum = 0.0294 (at node 73)
	maximum = 0.0432 (at node 169)
Accepted packet rate average = 0.0166417
	minimum = 0.0116 (at node 171)
	maximum = 0.0218 (at node 129)
Injected flit rate average = 0.644011
	minimum = 0.5292 (at node 73)
	maximum = 0.7776 (at node 169)
Accepted flit rate average= 0.299278
	minimum = 0.2104 (at node 171)
	maximum = 0.395 (at node 129)
Injected packet length average = 18.0017
Accepted packet length average = 17.9837
Total in-flight flits = 533273 (530257 measured)
latency change    = 0.694568
throughput change = 0.00214318
Class 0:
Packet latency average = 2625.84
	minimum = 22
	maximum = 6022
Network latency average = 2610.64
	minimum = 22
	maximum = 5959
Slowest packet = 20577
Flit latency average = 2897.7
	minimum = 5
	maximum = 6410
Slowest flit = 295199
Fragmentation average = 19.6902
	minimum = 0
	maximum = 102
Injected packet rate average = 0.035704
	minimum = 0.0288333 (at node 101)
	maximum = 0.042 (at node 61)
Accepted packet rate average = 0.0165946
	minimum = 0.0121667 (at node 67)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.642628
	minimum = 0.516833 (at node 101)
	maximum = 0.756333 (at node 61)
Accepted flit rate average= 0.298513
	minimum = 0.219 (at node 67)
	maximum = 0.374 (at node 129)
Injected packet length average = 17.9988
Accepted packet length average = 17.9885
Total in-flight flits = 598877 (598705 measured)
latency change    = 0.353601
throughput change = 0.00256305
Class 0:
Packet latency average = 3229.4
	minimum = 22
	maximum = 6824
Network latency average = 3213.66
	minimum = 22
	maximum = 6809
Slowest packet = 20741
Flit latency average = 3152.72
	minimum = 5
	maximum = 6908
Slowest flit = 366821
Fragmentation average = 21.6762
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0353542
	minimum = 0.0298571 (at node 8)
	maximum = 0.0422857 (at node 61)
Accepted packet rate average = 0.0164978
	minimum = 0.0118571 (at node 67)
	maximum = 0.0201429 (at node 40)
Injected flit rate average = 0.636342
	minimum = 0.537286 (at node 8)
	maximum = 0.761286 (at node 61)
Accepted flit rate average= 0.296681
	minimum = 0.213429 (at node 67)
	maximum = 0.361143 (at node 40)
Injected packet length average = 17.9991
Accepted packet length average = 17.9831
Total in-flight flits = 658937 (658937 measured)
latency change    = 0.186895
throughput change = 0.00617572
Draining all recorded packets ...
Class 0:
Remaining flits: 459648 459649 459650 459651 459652 459653 459654 459655 459656 459657 [...] (715627 flits)
Measured flits: 459648 459649 459650 459651 459652 459653 459654 459655 459656 459657 [...] (616405 flits)
Class 0:
Remaining flits: 484496 484497 484498 484499 484500 484501 484502 484503 484504 484505 [...] (767020 flits)
Measured flits: 484496 484497 484498 484499 484500 484501 484502 484503 484504 484505 [...] (570781 flits)
Class 0:
Remaining flits: 528195 528196 528197 528198 528199 528200 528201 528202 528203 528204 [...] (820752 flits)
Measured flits: 528195 528196 528197 528198 528199 528200 528201 528202 528203 528204 [...] (524551 flits)
Class 0:
Remaining flits: 581328 581329 581330 581331 581332 581333 581334 581335 581336 581337 [...] (872062 flits)
Measured flits: 581328 581329 581330 581331 581332 581333 581334 581335 581336 581337 [...] (477581 flits)
Class 0:
Remaining flits: 614250 614251 614252 614253 614254 614255 614256 614257 614258 614259 [...] (924903 flits)
Measured flits: 614250 614251 614252 614253 614254 614255 614256 614257 614258 614259 [...] (429816 flits)
Class 0:
Remaining flits: 672966 672967 672968 672969 672970 672971 672972 672973 672974 672975 [...] (972282 flits)
Measured flits: 672966 672967 672968 672969 672970 672971 672972 672973 672974 672975 [...] (382308 flits)
Class 0:
Remaining flits: 726011 730998 730999 731000 731001 731002 731003 731004 731005 731006 [...] (1017792 flits)
Measured flits: 726011 730998 730999 731000 731001 731002 731003 731004 731005 731006 [...] (335099 flits)
Class 0:
Remaining flits: 750186 750187 750188 750189 750190 750191 750192 750193 750194 750195 [...] (1049676 flits)
Measured flits: 750186 750187 750188 750189 750190 750191 750192 750193 750194 750195 [...] (288087 flits)
Class 0:
Remaining flits: 772787 772788 772789 772790 772791 772792 772793 783594 783595 783596 [...] (1070383 flits)
Measured flits: 772787 772788 772789 772790 772791 772792 772793 783594 783595 783596 [...] (241362 flits)
Class 0:
Remaining flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (1076617 flits)
Measured flits: 800532 800533 800534 800535 800536 800537 800538 800539 800540 800541 [...] (194265 flits)
Class 0:
Remaining flits: 822312 822313 822314 822315 822316 822317 822318 822319 822320 822321 [...] (1081279 flits)
Measured flits: 822312 822313 822314 822315 822316 822317 822318 822319 822320 822321 [...] (147214 flits)
Class 0:
Remaining flits: 862524 862525 862526 862527 862528 862529 862530 862531 862532 862533 [...] (1078832 flits)
Measured flits: 862524 862525 862526 862527 862528 862529 862530 862531 862532 862533 [...] (101921 flits)
Class 0:
Remaining flits: 945612 945613 945614 945615 945616 945617 945618 945619 945620 945621 [...] (1081480 flits)
Measured flits: 945612 945613 945614 945615 945616 945617 945618 945619 945620 945621 [...] (63691 flits)
Class 0:
Remaining flits: 945612 945613 945614 945615 945616 945617 945618 945619 945620 945621 [...] (1084384 flits)
Measured flits: 945612 945613 945614 945615 945616 945617 945618 945619 945620 945621 [...] (35614 flits)
Class 0:
Remaining flits: 1037538 1037539 1037540 1037541 1037542 1037543 1037544 1037545 1037546 1037547 [...] (1080182 flits)
Measured flits: 1037538 1037539 1037540 1037541 1037542 1037543 1037544 1037545 1037546 1037547 [...] (17811 flits)
Class 0:
Remaining flits: 1063692 1063693 1063694 1063695 1063696 1063697 1063698 1063699 1063700 1063701 [...] (1078549 flits)
Measured flits: 1063692 1063693 1063694 1063695 1063696 1063697 1063698 1063699 1063700 1063701 [...] (8826 flits)
Class 0:
Remaining flits: 1079568 1079569 1079570 1079571 1079572 1079573 1079574 1079575 1079576 1079577 [...] (1080824 flits)
Measured flits: 1079568 1079569 1079570 1079571 1079572 1079573 1079574 1079575 1079576 1079577 [...] (4056 flits)
Class 0:
Remaining flits: 1130130 1130131 1130132 1130133 1130134 1130135 1130136 1130137 1130138 1130139 [...] (1080597 flits)
Measured flits: 1130130 1130131 1130132 1130133 1130134 1130135 1130136 1130137 1130138 1130139 [...] (872 flits)
Class 0:
Remaining flits: 1175526 1175527 1175528 1175529 1175530 1175531 1175532 1175533 1175534 1175535 [...] (1079599 flits)
Measured flits: 1175526 1175527 1175528 1175529 1175530 1175531 1175532 1175533 1175534 1175535 [...] (162 flits)
Class 0:
Remaining flits: 1210149 1210150 1210151 1210152 1210153 1210154 1210155 1210156 1210157 1214532 [...] (1075030 flits)
Measured flits: 1210149 1210150 1210151 1210152 1210153 1210154 1210155 1210156 1210157 1214532 [...] (27 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1272798 1272799 1272800 1272801 1272802 1272803 1272804 1272805 1272806 1272807 [...] (1025381 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284804 1284805 1284806 1284807 1284808 1284809 1284810 1284811 1284812 1284813 [...] (975432 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1340726 1340727 1340728 1340729 1342620 1342621 1342622 1342623 1342624 1342625 [...] (925384 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1376676 1376677 1376678 1376679 1376680 1376681 1376682 1376683 1376684 1376685 [...] (875307 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1378602 1378603 1378604 1378605 1378606 1378607 1378608 1378609 1378610 1378611 [...] (824461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1441512 1441513 1441514 1441515 1441516 1441517 1441518 1441519 1441520 1441521 [...] (773402 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1466226 1466227 1466228 1466229 1466230 1466231 1466232 1466233 1466234 1466235 [...] (722118 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1547604 1547605 1547606 1547607 1547608 1547609 1547610 1547611 1547612 1547613 [...] (671895 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1549620 1549621 1549622 1549623 1549624 1549625 1549626 1549627 1549628 1549629 [...] (621709 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1571220 1571221 1571222 1571223 1571224 1571225 1571226 1571227 1571228 1571229 [...] (573066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1582827 1582828 1582829 1584594 1584595 1584596 1584597 1584598 1584599 1584600 [...] (525262 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1595556 1595557 1595558 1595559 1595560 1595561 1595562 1595563 1595564 1595565 [...] (477460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1715742 1715743 1715744 1715745 1715746 1715747 1715748 1715749 1715750 1715751 [...] (429719 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1786032 1786033 1786034 1786035 1786036 1786037 1786038 1786039 1786040 1786041 [...] (381924 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1795914 1795915 1795916 1795917 1795918 1795919 1795920 1795921 1795922 1795923 [...] (334268 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1849320 1849321 1849322 1849323 1849324 1849325 1849326 1849327 1849328 1849329 [...] (286561 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1852649 1888956 1888957 1888958 1888959 1888960 1888961 1888962 1888963 1888964 [...] (239043 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1980684 1980685 1980686 1980687 1980688 1980689 1980690 1980691 1980692 1980693 [...] (191322 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2182230 2182231 2182232 2182233 2182234 2182235 2182236 2182237 2182238 2182239 [...] (143468 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2262486 2262487 2262488 2262489 2262490 2262491 2287942 2287943 2294622 2294623 [...] (95951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2296890 2296891 2296892 2296893 2296894 2296895 2296896 2296897 2296898 2296899 [...] (48672 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2376164 2376165 2376166 2376167 2376168 2376169 2376170 2376171 2376172 2376173 [...] (12389 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2459246 2459247 2459248 2459249 2471886 2471887 2471888 2471889 2471890 2471891 [...] (962 flits)
Measured flits: (0 flits)
Time taken is 54073 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8553.18 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20629 (1 samples)
Network latency average = 8517.43 (1 samples)
	minimum = 22 (1 samples)
	maximum = 20629 (1 samples)
Flit latency average = 13911 (1 samples)
	minimum = 5 (1 samples)
	maximum = 31531 (1 samples)
Fragmentation average = 28.2074 (1 samples)
	minimum = 0 (1 samples)
	maximum = 968 (1 samples)
Injected packet rate average = 0.0353542 (1 samples)
	minimum = 0.0298571 (1 samples)
	maximum = 0.0422857 (1 samples)
Accepted packet rate average = 0.0164978 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0201429 (1 samples)
Injected flit rate average = 0.636342 (1 samples)
	minimum = 0.537286 (1 samples)
	maximum = 0.761286 (1 samples)
Accepted flit rate average = 0.296681 (1 samples)
	minimum = 0.213429 (1 samples)
	maximum = 0.361143 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 17.9831 (1 samples)
Hops average = 5.06811 (1 samples)
Total run time 50.9377
