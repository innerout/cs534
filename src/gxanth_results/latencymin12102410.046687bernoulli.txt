BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.196
	minimum = 23
	maximum = 976
Network latency average = 309.589
	minimum = 23
	maximum = 963
Slowest packet = 217
Flit latency average = 279.371
	minimum = 6
	maximum = 946
Slowest flit = 3923
Fragmentation average = 53.2292
	minimum = 0
	maximum = 162
Injected packet rate average = 0.0446667
	minimum = 0.028 (at node 186)
	maximum = 0.055 (at node 35)
Accepted packet rate average = 0.0112917
	minimum = 0.004 (at node 106)
	maximum = 0.021 (at node 175)
Injected flit rate average = 0.797755
	minimum = 0.504 (at node 186)
	maximum = 0.986 (at node 119)
Accepted flit rate average= 0.211672
	minimum = 0.084 (at node 174)
	maximum = 0.378 (at node 175)
Injected packet length average = 17.8602
Accepted packet length average = 18.7458
Total in-flight flits = 113727 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 601.381
	minimum = 23
	maximum = 1904
Network latency average = 577.097
	minimum = 23
	maximum = 1883
Slowest packet = 295
Flit latency average = 545.927
	minimum = 6
	maximum = 1872
Slowest flit = 16579
Fragmentation average = 55.8765
	minimum = 0
	maximum = 164
Injected packet rate average = 0.0453854
	minimum = 0.0345 (at node 10)
	maximum = 0.055 (at node 135)
Accepted packet rate average = 0.0118464
	minimum = 0.006 (at node 164)
	maximum = 0.017 (at node 76)
Injected flit rate average = 0.813201
	minimum = 0.62 (at node 105)
	maximum = 0.9855 (at node 135)
Accepted flit rate average= 0.217417
	minimum = 0.1095 (at node 164)
	maximum = 0.306 (at node 76)
Injected packet length average = 17.9177
Accepted packet length average = 18.353
Total in-flight flits = 230270 (0 measured)
latency change    = 0.455926
throughput change = 0.026423
Class 0:
Packet latency average = 1644.81
	minimum = 27
	maximum = 2817
Network latency average = 1603.44
	minimum = 25
	maximum = 2783
Slowest packet = 1142
Flit latency average = 1583.82
	minimum = 6
	maximum = 2766
Slowest flit = 20573
Fragmentation average = 62.6991
	minimum = 0
	maximum = 163
Injected packet rate average = 0.03925
	minimum = 0.012 (at node 84)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0108177
	minimum = 0.002 (at node 116)
	maximum = 0.02 (at node 97)
Injected flit rate average = 0.707063
	minimum = 0.217 (at node 84)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.193521
	minimum = 0.049 (at node 116)
	maximum = 0.373 (at node 97)
Injected packet length average = 18.0143
Accepted packet length average = 17.8893
Total in-flight flits = 328708 (0 measured)
latency change    = 0.634376
throughput change = 0.123479
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 462.609
	minimum = 25
	maximum = 1692
Network latency average = 51.5385
	minimum = 25
	maximum = 948
Slowest packet = 25060
Flit latency average = 2318.34
	minimum = 6
	maximum = 3694
Slowest flit = 42543
Fragmentation average = 10.3964
	minimum = 0
	maximum = 47
Injected packet rate average = 0.0389583
	minimum = 0.011 (at node 184)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0103802
	minimum = 0.001 (at node 47)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.700156
	minimum = 0.199 (at node 184)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.187219
	minimum = 0.018 (at node 47)
	maximum = 0.331 (at node 152)
Injected packet length average = 17.9719
Accepted packet length average = 18.0361
Total in-flight flits = 427402 (131515 measured)
latency change    = 2.55549
throughput change = 0.0336616
Class 0:
Packet latency average = 724.759
	minimum = 24
	maximum = 2297
Network latency average = 276.968
	minimum = 23
	maximum = 1941
Slowest packet = 25060
Flit latency average = 2670.63
	minimum = 6
	maximum = 4508
Slowest flit = 72075
Fragmentation average = 10.1073
	minimum = 0
	maximum = 48
Injected packet rate average = 0.0390599
	minimum = 0.013 (at node 76)
	maximum = 0.056 (at node 47)
Accepted packet rate average = 0.0102578
	minimum = 0.0045 (at node 17)
	maximum = 0.016 (at node 42)
Injected flit rate average = 0.702505
	minimum = 0.232 (at node 76)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.184667
	minimum = 0.081 (at node 17)
	maximum = 0.28 (at node 42)
Injected packet length average = 17.9853
Accepted packet length average = 18.0025
Total in-flight flits = 527796 (262590 measured)
latency change    = 0.361705
throughput change = 0.0138199
Class 0:
Packet latency average = 1030.41
	minimum = 23
	maximum = 3126
Network latency average = 542.291
	minimum = 23
	maximum = 2887
Slowest packet = 25060
Flit latency average = 3048.59
	minimum = 6
	maximum = 5331
Slowest flit = 98693
Fragmentation average = 10.0675
	minimum = 0
	maximum = 48
Injected packet rate average = 0.0389236
	minimum = 0.013 (at node 76)
	maximum = 0.0556667 (at node 47)
Accepted packet rate average = 0.0101927
	minimum = 0.006 (at node 7)
	maximum = 0.0153333 (at node 129)
Injected flit rate average = 0.700637
	minimum = 0.236667 (at node 40)
	maximum = 1 (at node 39)
Accepted flit rate average= 0.183675
	minimum = 0.108667 (at node 149)
	maximum = 0.281667 (at node 129)
Injected packet length average = 18.0003
Accepted packet length average = 18.0203
Total in-flight flits = 626471 (391450 measured)
latency change    = 0.296629
throughput change = 0.00539713
Class 0:
Packet latency average = 1340.25
	minimum = 23
	maximum = 3809
Network latency average = 805.039
	minimum = 23
	maximum = 3680
Slowest packet = 25141
Flit latency average = 3412.85
	minimum = 6
	maximum = 6321
Slowest flit = 98603
Fragmentation average = 10.2971
	minimum = 0
	maximum = 48
Injected packet rate average = 0.0384062
	minimum = 0.01375 (at node 40)
	maximum = 0.054 (at node 47)
Accepted packet rate average = 0.0101888
	minimum = 0.0055 (at node 17)
	maximum = 0.01575 (at node 129)
Injected flit rate average = 0.691355
	minimum = 0.2465 (at node 40)
	maximum = 0.96825 (at node 47)
Accepted flit rate average= 0.183667
	minimum = 0.09975 (at node 17)
	maximum = 0.28225 (at node 129)
Injected packet length average = 18.0011
Accepted packet length average = 18.0263
Total in-flight flits = 718580 (513469 measured)
latency change    = 0.231184
throughput change = 4.72626e-05
Class 0:
Packet latency average = 1668.63
	minimum = 23
	maximum = 5535
Network latency average = 1053.23
	minimum = 23
	maximum = 4773
Slowest packet = 25141
Flit latency average = 3778.61
	minimum = 6
	maximum = 7270
Slowest flit = 109557
Fragmentation average = 10.9317
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0354875
	minimum = 0.0136 (at node 80)
	maximum = 0.048 (at node 173)
Accepted packet rate average = 0.0101437
	minimum = 0.0066 (at node 17)
	maximum = 0.014 (at node 129)
Injected flit rate average = 0.639108
	minimum = 0.2444 (at node 80)
	maximum = 0.8652 (at node 173)
Accepted flit rate average= 0.182733
	minimum = 0.1188 (at node 17)
	maximum = 0.2512 (at node 129)
Injected packet length average = 18.0094
Accepted packet length average = 18.0144
Total in-flight flits = 766508 (590663 measured)
latency change    = 0.196795
throughput change = 0.00510762
Class 0:
Packet latency average = 2148.74
	minimum = 23
	maximum = 6700
Network latency average = 1431.24
	minimum = 23
	maximum = 5958
Slowest packet = 25141
Flit latency average = 4159.93
	minimum = 6
	maximum = 8017
Slowest flit = 150773
Fragmentation average = 12.8182
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0321771
	minimum = 0.0135 (at node 40)
	maximum = 0.0423333 (at node 105)
Accepted packet rate average = 0.0100668
	minimum = 0.00633333 (at node 17)
	maximum = 0.0131667 (at node 129)
Injected flit rate average = 0.579508
	minimum = 0.2415 (at node 40)
	maximum = 0.7635 (at node 105)
Accepted flit rate average= 0.181356
	minimum = 0.1145 (at node 17)
	maximum = 0.238833 (at node 11)
Injected packet length average = 18.01
Accepted packet length average = 18.0152
Total in-flight flits = 787010 (639591 measured)
latency change    = 0.223439
throughput change = 0.00759518
Class 0:
Packet latency average = 2706.72
	minimum = 23
	maximum = 7919
Network latency average = 1900.08
	minimum = 23
	maximum = 6909
Slowest packet = 25141
Flit latency average = 4538.18
	minimum = 6
	maximum = 8932
Slowest flit = 162319
Fragmentation average = 16.1509
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0296987
	minimum = 0.0137143 (at node 40)
	maximum = 0.0384286 (at node 105)
Accepted packet rate average = 0.0100275
	minimum = 0.00714286 (at node 36)
	maximum = 0.0132857 (at node 11)
Injected flit rate average = 0.53485
	minimum = 0.246143 (at node 40)
	maximum = 0.693 (at node 105)
Accepted flit rate average= 0.180684
	minimum = 0.129857 (at node 36)
	maximum = 0.241571 (at node 11)
Injected packet length average = 18.0092
Accepted packet length average = 18.0188
Total in-flight flits = 804339 (684669 measured)
latency change    = 0.206144
throughput change = 0.00371989
Draining all recorded packets ...
Class 0:
Remaining flits: 172224 172225 172226 172227 172228 172229 172230 172231 172232 172233 [...] (822201 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (728141 flits)
Class 0:
Remaining flits: 178578 178579 178580 178581 178582 178583 178584 178585 178586 178587 [...] (839128 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (768173 flits)
Class 0:
Remaining flits: 202352 202353 202354 202355 213354 213355 213356 213357 213358 213359 [...] (855975 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (804501 flits)
Class 0:
Remaining flits: 217260 217261 217262 217263 217264 217265 217266 217267 217268 217269 [...] (872967 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (835934 flits)
Class 0:
Remaining flits: 239620 239621 239622 239623 239624 239625 239626 239627 239628 239629 [...] (890888 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (859134 flits)
Class 0:
Remaining flits: 276534 276535 276536 276537 276538 276539 276540 276541 276542 276543 [...] (909097 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (872040 flits)
Class 0:
Remaining flits: 276534 276535 276536 276537 276538 276539 276540 276541 276542 276543 [...] (924516 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (870250 flits)
Class 0:
Remaining flits: 276534 276535 276536 276537 276538 276539 276540 276541 276542 276543 [...] (938575 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (860091 flits)
Class 0:
Remaining flits: 342738 342739 342740 342741 342742 342743 342744 342745 342746 342747 [...] (952505 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (843291 flits)
Class 0:
Remaining flits: 358506 358507 358508 358509 358510 358511 358512 358513 358514 358515 [...] (966147 flits)
Measured flits: 449352 449353 449354 449355 449356 449357 449358 449359 449360 449361 [...] (824508 flits)
Class 0:
Remaining flits: 382914 382915 382916 382917 382918 382919 382920 382921 382922 382923 [...] (972050 flits)
Measured flits: 450144 450145 450146 450147 450148 450149 450150 450151 450152 450153 [...] (802714 flits)
Class 0:
Remaining flits: 405234 405235 405236 405237 405238 405239 405240 405241 405242 405243 [...] (976373 flits)
Measured flits: 450144 450145 450146 450147 450148 450149 450150 450151 450152 450153 [...] (780810 flits)
Class 0:
Remaining flits: 420192 420193 420194 420195 420196 420197 420198 420199 420200 420201 [...] (976958 flits)
Measured flits: 450792 450793 450794 450795 450796 450797 450798 450799 450800 450801 [...] (756379 flits)
Class 0:
Remaining flits: 449118 449119 449120 449121 449122 449123 449124 449125 449126 449127 [...] (972609 flits)
Measured flits: 450792 450793 450794 450795 450796 450797 450798 450799 450800 450801 [...] (732866 flits)
Class 0:
Remaining flits: 449118 449119 449120 449121 449122 449123 449124 449125 449126 449127 [...] (967218 flits)
Measured flits: 450900 450901 450902 450903 450904 450905 450906 450907 450908 450909 [...] (706740 flits)
Class 0:
Remaining flits: 460926 460927 460928 460929 460930 460931 460932 460933 460934 460935 [...] (966335 flits)
Measured flits: 460926 460927 460928 460929 460930 460931 460932 460933 460934 460935 [...] (683100 flits)
Class 0:
Remaining flits: 493866 493867 493868 493869 493870 493871 493872 493873 493874 493875 [...] (964033 flits)
Measured flits: 493866 493867 493868 493869 493870 493871 493872 493873 493874 493875 [...] (660157 flits)
Class 0:
Remaining flits: 507114 507115 507116 507117 507118 507119 507120 507121 507122 507123 [...] (961426 flits)
Measured flits: 507114 507115 507116 507117 507118 507119 507120 507121 507122 507123 [...] (634849 flits)
Class 0:
Remaining flits: 534384 534385 534386 534387 534388 534389 534390 534391 534392 534393 [...] (963439 flits)
Measured flits: 534384 534385 534386 534387 534388 534389 534390 534391 534392 534393 [...] (611123 flits)
Class 0:
Remaining flits: 534384 534385 534386 534387 534388 534389 534390 534391 534392 534393 [...] (962271 flits)
Measured flits: 534384 534385 534386 534387 534388 534389 534390 534391 534392 534393 [...] (585065 flits)
Class 0:
Remaining flits: 540882 540883 540884 540885 540886 540887 540888 540889 540890 540891 [...] (960652 flits)
Measured flits: 540882 540883 540884 540885 540886 540887 540888 540889 540890 540891 [...] (557902 flits)
Class 0:
Remaining flits: 585558 585559 585560 585561 585562 585563 585564 585565 585566 585567 [...] (959681 flits)
Measured flits: 585558 585559 585560 585561 585562 585563 585564 585565 585566 585567 [...] (530728 flits)
Class 0:
Remaining flits: 592145 592776 592777 592778 592779 592780 592781 592782 592783 592784 [...] (953772 flits)
Measured flits: 592145 592776 592777 592778 592779 592780 592781 592782 592783 592784 [...] (501678 flits)
Class 0:
Remaining flits: 626256 626257 626258 626259 626260 626261 626262 626263 626264 626265 [...] (949950 flits)
Measured flits: 626256 626257 626258 626259 626260 626261 626262 626263 626264 626265 [...] (473147 flits)
Class 0:
Remaining flits: 675099 675100 675101 675102 675103 675104 675105 675106 675107 679158 [...] (944745 flits)
Measured flits: 675099 675100 675101 675102 675103 675104 675105 675106 675107 679158 [...] (444806 flits)
Class 0:
Remaining flits: 685134 685135 685136 685137 685138 685139 685140 685141 685142 685143 [...] (939204 flits)
Measured flits: 685134 685135 685136 685137 685138 685139 685140 685141 685142 685143 [...] (416047 flits)
Class 0:
Remaining flits: 685134 685135 685136 685137 685138 685139 685140 685141 685142 685143 [...] (936801 flits)
Measured flits: 685134 685135 685136 685137 685138 685139 685140 685141 685142 685143 [...] (387291 flits)
Class 0:
Remaining flits: 720936 720937 720938 720939 720940 720941 720942 720943 720944 720945 [...] (931828 flits)
Measured flits: 720936 720937 720938 720939 720940 720941 720942 720943 720944 720945 [...] (358701 flits)
Class 0:
Remaining flits: 752310 752311 752312 752313 752314 752315 752316 752317 752318 752319 [...] (928067 flits)
Measured flits: 752310 752311 752312 752313 752314 752315 752316 752317 752318 752319 [...] (330462 flits)
Class 0:
Remaining flits: 757067 757068 757069 757070 757071 757072 757073 757074 757075 757076 [...] (923073 flits)
Measured flits: 757067 757068 757069 757070 757071 757072 757073 757074 757075 757076 [...] (302798 flits)
Class 0:
Remaining flits: 820152 820153 820154 820155 820156 820157 820158 820159 820160 820161 [...] (918518 flits)
Measured flits: 820152 820153 820154 820155 820156 820157 820158 820159 820160 820161 [...] (275068 flits)
Class 0:
Remaining flits: 820153 820154 820155 820156 820157 820158 820159 820160 820161 820162 [...] (915060 flits)
Measured flits: 820153 820154 820155 820156 820157 820158 820159 820160 820161 820162 [...] (247581 flits)
Class 0:
Remaining flits: 827244 827245 827246 827247 827248 827249 827250 827251 827252 827253 [...] (913667 flits)
Measured flits: 827244 827245 827246 827247 827248 827249 827250 827251 827252 827253 [...] (221296 flits)
Class 0:
Remaining flits: 834840 834841 834842 834843 834844 834845 834846 834847 834848 834849 [...] (913115 flits)
Measured flits: 834840 834841 834842 834843 834844 834845 834846 834847 834848 834849 [...] (195642 flits)
Class 0:
Remaining flits: 834840 834841 834842 834843 834844 834845 834846 834847 834848 834849 [...] (911383 flits)
Measured flits: 834840 834841 834842 834843 834844 834845 834846 834847 834848 834849 [...] (170925 flits)
Class 0:
Remaining flits: 871812 871813 871814 871815 871816 871817 871818 871819 871820 871821 [...] (910590 flits)
Measured flits: 871812 871813 871814 871815 871816 871817 871818 871819 871820 871821 [...] (148174 flits)
Class 0:
Remaining flits: 884970 884971 884972 884973 884974 884975 884976 884977 884978 884979 [...] (910588 flits)
Measured flits: 884970 884971 884972 884973 884974 884975 884976 884977 884978 884979 [...] (127089 flits)
Class 0:
Remaining flits: 899586 899587 899588 899589 899590 899591 899592 899593 899594 899595 [...] (908119 flits)
Measured flits: 899586 899587 899588 899589 899590 899591 899592 899593 899594 899595 [...] (108383 flits)
Class 0:
Remaining flits: 899586 899587 899588 899589 899590 899591 899592 899593 899594 899595 [...] (904733 flits)
Measured flits: 899586 899587 899588 899589 899590 899591 899592 899593 899594 899595 [...] (91731 flits)
Class 0:
Remaining flits: 939060 939061 939062 939063 939064 939065 939066 939067 939068 939069 [...] (903293 flits)
Measured flits: 939060 939061 939062 939063 939064 939065 939066 939067 939068 939069 [...] (76122 flits)
Class 0:
Remaining flits: 939060 939061 939062 939063 939064 939065 939066 939067 939068 939069 [...] (905302 flits)
Measured flits: 939060 939061 939062 939063 939064 939065 939066 939067 939068 939069 [...] (62004 flits)
Class 0:
Remaining flits: 986058 986059 986060 986061 986062 986063 986064 986065 986066 986067 [...] (904023 flits)
Measured flits: 986058 986059 986060 986061 986062 986063 986064 986065 986066 986067 [...] (50351 flits)
Class 0:
Remaining flits: 1023858 1023859 1023860 1023861 1023862 1023863 1023864 1023865 1023866 1023867 [...] (899982 flits)
Measured flits: 1023858 1023859 1023860 1023861 1023862 1023863 1023864 1023865 1023866 1023867 [...] (40498 flits)
Class 0:
Remaining flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (897241 flits)
Measured flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (31706 flits)
Class 0:
Remaining flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (898970 flits)
Measured flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (24100 flits)
Class 0:
Remaining flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (898219 flits)
Measured flits: 1045134 1045135 1045136 1045137 1045138 1045139 1045140 1045141 1045142 1045143 [...] (17902 flits)
Class 0:
Remaining flits: 1126836 1126837 1126838 1126839 1126840 1126841 1126842 1126843 1126844 1126845 [...] (898679 flits)
Measured flits: 1126836 1126837 1126838 1126839 1126840 1126841 1126842 1126843 1126844 1126845 [...] (13310 flits)
Class 0:
Remaining flits: 1126853 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 [...] (894753 flits)
Measured flits: 1126853 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 [...] (9645 flits)
Class 0:
Remaining flits: 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 1206603 [...] (894490 flits)
Measured flits: 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 1206603 [...] (6862 flits)
Class 0:
Remaining flits: 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 1206603 [...] (892618 flits)
Measured flits: 1206594 1206595 1206596 1206597 1206598 1206599 1206600 1206601 1206602 1206603 [...] (4899 flits)
Class 0:
Remaining flits: 1215450 1215451 1215452 1215453 1215454 1215455 1215456 1215457 1215458 1215459 [...] (890765 flits)
Measured flits: 1215450 1215451 1215452 1215453 1215454 1215455 1215456 1215457 1215458 1215459 [...] (3331 flits)
Class 0:
Remaining flits: 1228698 1228699 1228700 1228701 1228702 1228703 1228704 1228705 1228706 1228707 [...] (887749 flits)
Measured flits: 1228698 1228699 1228700 1228701 1228702 1228703 1228704 1228705 1228706 1228707 [...] (2277 flits)
Class 0:
Remaining flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (885384 flits)
Measured flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (1402 flits)
Class 0:
Remaining flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (881284 flits)
Measured flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (882 flits)
Class 0:
Remaining flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (879013 flits)
Measured flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (561 flits)
Class 0:
Remaining flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (874088 flits)
Measured flits: 1271124 1271125 1271126 1271127 1271128 1271129 1271130 1271131 1271132 1271133 [...] (391 flits)
Class 0:
Remaining flits: 1344330 1344331 1344332 1344333 1344334 1344335 1344336 1344337 1344338 1344339 [...] (869628 flits)
Measured flits: 1344330 1344331 1344332 1344333 1344334 1344335 1344336 1344337 1344338 1344339 [...] (270 flits)
Class 0:
Remaining flits: 1471590 1471591 1471592 1471593 1471594 1471595 1471596 1471597 1471598 1471599 [...] (868026 flits)
Measured flits: 1551600 1551601 1551602 1551603 1551604 1551605 1551606 1551607 1551608 1551609 [...] (162 flits)
Class 0:
Remaining flits: 1477980 1477981 1477982 1477983 1477984 1477985 1477986 1477987 1477988 1477989 [...] (869677 flits)
Measured flits: 1551600 1551601 1551602 1551603 1551604 1551605 1551606 1551607 1551608 1551609 [...] (110 flits)
Class 0:
Remaining flits: 1489266 1489267 1489268 1489269 1489270 1489271 1489272 1489273 1489274 1489275 [...] (868582 flits)
Measured flits: 2285640 2285641 2285642 2285643 2285644 2285645 2285646 2285647 2285648 2285649 [...] (90 flits)
Class 0:
Remaining flits: 1524834 1524835 1524836 1524837 1524838 1524839 1524840 1524841 1524842 1524843 [...] (871323 flits)
Measured flits: 2317140 2317141 2317142 2317143 2317144 2317145 2317146 2317147 2317148 2317149 [...] (72 flits)
Class 0:
Remaining flits: 1524834 1524835 1524836 1524837 1524838 1524839 1524840 1524841 1524842 1524843 [...] (869881 flits)
Measured flits: 2317140 2317141 2317142 2317143 2317144 2317145 2317146 2317147 2317148 2317149 [...] (72 flits)
Class 0:
Remaining flits: 1524834 1524835 1524836 1524837 1524838 1524839 1524840 1524841 1524842 1524843 [...] (871613 flits)
Measured flits: 2357334 2357335 2357336 2357337 2357338 2357339 2357340 2357341 2357342 2357343 [...] (36 flits)
Class 0:
Remaining flits: 1524834 1524835 1524836 1524837 1524838 1524839 1524840 1524841 1524842 1524843 [...] (871077 flits)
Measured flits: 2433798 2433799 2433800 2433801 2433802 2433803 2433804 2433805 2433806 2433807 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1606608 1606609 1606610 1606611 1606612 1606613 1606614 1606615 1606616 1606617 [...] (839134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1651302 1651303 1651304 1651305 1651306 1651307 1651308 1651309 1651310 1651311 [...] (807550 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1691334 1691335 1691336 1691337 1691338 1691339 1691340 1691341 1691342 1691343 [...] (775851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1692756 1692757 1692758 1692759 1692760 1692761 1692762 1692763 1692764 1692765 [...] (744966 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1702233 1702234 1702235 1702236 1702237 1702238 1702239 1702240 1702241 1716228 [...] (714286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1716230 1716231 1716232 1716233 1716234 1716235 1716236 1716237 1716238 1716239 [...] (682591 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1735254 1735255 1735256 1735257 1735258 1735259 1735260 1735261 1735262 1735263 [...] (651491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1735259 1735260 1735261 1735262 1735263 1735264 1735265 1735266 1735267 1735268 [...] (619866 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1771344 1771345 1771346 1771347 1771348 1771349 1771350 1771351 1771352 1771353 [...] (588796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1807488 1807489 1807490 1807491 1807492 1807493 1807494 1807495 1807496 1807497 [...] (557798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845360 1845361 1845362 1845363 1845364 1845365 1845366 1845367 1845368 1845369 [...] (526335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1845360 1845361 1845362 1845363 1845364 1845365 1845366 1845367 1845368 1845369 [...] (494595 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1989144 1989145 1989146 1989147 1989148 1989149 1989150 1989151 1989152 1989153 [...] (463351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2018894 2018895 2018896 2018897 2050902 2050903 2050904 2050905 2050906 2050907 [...] (432206 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2050902 2050903 2050904 2050905 2050906 2050907 2050908 2050909 2050910 2050911 [...] (400991 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2072754 2072755 2072756 2072757 2072758 2072759 2072760 2072761 2072762 2072763 [...] (370155 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2123802 2123803 2123804 2123805 2123806 2123807 2123808 2123809 2123810 2123811 [...] (339610 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2216790 2216791 2216792 2216793 2216794 2216795 2216796 2216797 2216798 2216799 [...] (309415 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2227032 2227033 2227034 2227035 2227036 2227037 2227038 2227039 2227040 2227041 [...] (279516 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2253510 2253511 2253512 2253513 2253514 2253515 2253516 2253517 2253518 2253519 [...] (249836 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2289762 2289763 2289764 2289765 2289766 2289767 2289768 2289769 2289770 2289771 [...] (219830 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2304270 2304271 2304272 2304273 2304274 2304275 2304276 2304277 2304278 2304279 [...] (190778 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2320542 2320543 2320544 2320545 2320546 2320547 2320548 2320549 2320550 2320551 [...] (163419 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2420964 2420965 2420966 2420967 2420968 2420969 2420970 2420971 2420972 2420973 [...] (134654 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2440710 2440711 2440712 2440713 2440714 2440715 2440716 2440717 2440718 2440719 [...] (106160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2453292 2453293 2453294 2453295 2453296 2453297 2453298 2453299 2453300 2453301 [...] (79377 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2468394 2468395 2468396 2468397 2468398 2468399 2468400 2468401 2468402 2468403 [...] (54023 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2468394 2468395 2468396 2468397 2468398 2468399 2468400 2468401 2468402 2468403 [...] (29536 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2524968 2524969 2524970 2524971 2524972 2524973 2524974 2524975 2524976 2524977 [...] (11788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2798460 2798461 2798462 2798463 2798464 2798465 2798466 2798467 2798468 2798469 [...] (2557 flits)
Measured flits: (0 flits)
Time taken is 105427 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 24676.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 64445 (1 samples)
Network latency average = 21195.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 54588 (1 samples)
Flit latency average = 23768.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 61145 (1 samples)
Fragmentation average = 72.0619 (1 samples)
	minimum = 0 (1 samples)
	maximum = 197 (1 samples)
Injected packet rate average = 0.0296987 (1 samples)
	minimum = 0.0137143 (1 samples)
	maximum = 0.0384286 (1 samples)
Accepted packet rate average = 0.0100275 (1 samples)
	minimum = 0.00714286 (1 samples)
	maximum = 0.0132857 (1 samples)
Injected flit rate average = 0.53485 (1 samples)
	minimum = 0.246143 (1 samples)
	maximum = 0.693 (1 samples)
Accepted flit rate average = 0.180684 (1 samples)
	minimum = 0.129857 (1 samples)
	maximum = 0.241571 (1 samples)
Injected packet size average = 18.0092 (1 samples)
Accepted packet size average = 18.0188 (1 samples)
Hops average = 5.05455 (1 samples)
Total run time 85.3115
