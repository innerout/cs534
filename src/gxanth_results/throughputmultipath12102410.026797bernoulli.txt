BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 275.308
	minimum = 23
	maximum = 859
Network latency average = 269.371
	minimum = 23
	maximum = 859
Slowest packet = 597
Flit latency average = 237.985
	minimum = 6
	maximum = 883
Slowest flit = 8347
Fragmentation average = 55.3027
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.00992708
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 121)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.188172
	minimum = 0.056 (at node 41)
	maximum = 0.308 (at node 121)
Injected packet length average = 17.8601
Accepted packet length average = 18.9554
Total in-flight flits = 56481 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 527.277
	minimum = 23
	maximum = 1771
Network latency average = 520.422
	minimum = 23
	maximum = 1754
Slowest packet = 589
Flit latency average = 486.405
	minimum = 6
	maximum = 1801
Slowest flit = 16905
Fragmentation average = 61.6813
	minimum = 0
	maximum = 169
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0102318
	minimum = 0.006 (at node 68)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.188174
	minimum = 0.108 (at node 108)
	maximum = 0.279 (at node 22)
Injected packet length average = 17.9276
Accepted packet length average = 18.3912
Total in-flight flits = 112169 (0 measured)
latency change    = 0.477868
throughput change = 1.38391e-05
Class 0:
Packet latency average = 1267.12
	minimum = 23
	maximum = 2753
Network latency average = 1258.64
	minimum = 23
	maximum = 2753
Slowest packet = 920
Flit latency average = 1231.95
	minimum = 6
	maximum = 2736
Slowest flit = 16560
Fragmentation average = 60.9814
	minimum = 0
	maximum = 166
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0109427
	minimum = 0.003 (at node 10)
	maximum = 0.019 (at node 78)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.196427
	minimum = 0.063 (at node 146)
	maximum = 0.342 (at node 78)
Injected packet length average = 17.9863
Accepted packet length average = 17.9505
Total in-flight flits = 166741 (0 measured)
latency change    = 0.583878
throughput change = 0.0420136
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 67.561
	minimum = 23
	maximum = 247
Network latency average = 58.8831
	minimum = 23
	maximum = 222
Slowest packet = 15486
Flit latency average = 1768.5
	minimum = 6
	maximum = 3548
Slowest flit = 40827
Fragmentation average = 9.44156
	minimum = 0
	maximum = 47
Injected packet rate average = 0.027026
	minimum = 0.013 (at node 159)
	maximum = 0.043 (at node 26)
Accepted packet rate average = 0.0105937
	minimum = 0.004 (at node 184)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.486271
	minimum = 0.238 (at node 159)
	maximum = 0.766 (at node 26)
Accepted flit rate average= 0.190318
	minimum = 0.085 (at node 184)
	maximum = 0.382 (at node 16)
Injected packet length average = 17.9927
Accepted packet length average = 17.9651
Total in-flight flits = 223602 (86383 measured)
latency change    = 17.7552
throughput change = 0.0321009
Class 0:
Packet latency average = 89.9416
	minimum = 23
	maximum = 1965
Network latency average = 77.3985
	minimum = 23
	maximum = 1931
Slowest packet = 15565
Flit latency average = 2040.27
	minimum = 6
	maximum = 4424
Slowest flit = 45053
Fragmentation average = 10.4822
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0263672
	minimum = 0.017 (at node 159)
	maximum = 0.036 (at node 26)
Accepted packet rate average = 0.0106146
	minimum = 0.0055 (at node 190)
	maximum = 0.018 (at node 16)
Injected flit rate average = 0.474875
	minimum = 0.309 (at node 31)
	maximum = 0.6515 (at node 26)
Accepted flit rate average= 0.191721
	minimum = 0.103 (at node 190)
	maximum = 0.321 (at node 16)
Injected packet length average = 18.0101
Accepted packet length average = 18.0621
Total in-flight flits = 275370 (167873 measured)
latency change    = 0.248835
throughput change = 0.00732128
Class 0:
Packet latency average = 242.673
	minimum = 23
	maximum = 2942
Network latency average = 219.393
	minimum = 23
	maximum = 2942
Slowest packet = 15572
Flit latency average = 2321.73
	minimum = 6
	maximum = 5132
Slowest flit = 79357
Fragmentation average = 14.0042
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0260365
	minimum = 0.0186667 (at node 0)
	maximum = 0.0353333 (at node 26)
Accepted packet rate average = 0.0105035
	minimum = 0.00733333 (at node 109)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.468349
	minimum = 0.330667 (at node 0)
	maximum = 0.634 (at node 26)
Accepted flit rate average= 0.189172
	minimum = 0.130667 (at node 153)
	maximum = 0.283667 (at node 16)
Injected packet length average = 17.9882
Accepted packet length average = 18.0104
Total in-flight flits = 327742 (248258 measured)
latency change    = 0.629372
throughput change = 0.0134771
Draining remaining packets ...
Class 0:
Remaining flits: 73530 73531 73532 73533 73534 73535 73536 73537 73538 73539 [...] (296781 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (242325 flits)
Class 0:
Remaining flits: 85662 85663 85664 85665 85666 85667 85668 85669 85670 85671 [...] (267299 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (232681 flits)
Class 0:
Remaining flits: 107748 107749 107750 107751 107752 107753 107754 107755 107756 107757 [...] (237557 flits)
Measured flits: 276732 276733 276734 276735 276736 276737 276738 276739 276740 276741 [...] (217470 flits)
Class 0:
Remaining flits: 107748 107749 107750 107751 107752 107753 107754 107755 107756 107757 [...] (208245 flits)
Measured flits: 276748 276749 276750 276751 276752 276753 276754 276755 276756 276757 [...] (197313 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (179028 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (173224 flits)
Class 0:
Remaining flits: 170226 170227 170228 170229 170230 170231 170232 170233 170234 170235 [...] (149815 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (147129 flits)
Class 0:
Remaining flits: 171666 171667 171668 171669 171670 171671 171672 171673 171674 171675 [...] (120967 flits)
Measured flits: 276750 276751 276752 276753 276754 276755 276756 276757 276758 276759 [...] (119778 flits)
Class 0:
Remaining flits: 195156 195157 195158 195159 195160 195161 195162 195163 195164 195165 [...] (91174 flits)
Measured flits: 277686 277687 277688 277689 277690 277691 277692 277693 277694 277695 [...] (90719 flits)
Class 0:
Remaining flits: 196830 196831 196832 196833 196834 196835 196836 196837 196838 196839 [...] (62216 flits)
Measured flits: 278334 278335 278336 278337 278338 278339 278340 278341 278342 278343 [...] (62018 flits)
Class 0:
Remaining flits: 200016 200017 200018 200019 200020 200021 200022 200023 200024 200025 [...] (33825 flits)
Measured flits: 278334 278335 278336 278337 278338 278339 278340 278341 278342 278343 [...] (33681 flits)
Class 0:
Remaining flits: 216630 216631 216632 216633 216634 216635 216636 216637 216638 216639 [...] (8295 flits)
Measured flits: 302958 302959 302960 302961 302962 302963 302964 302965 302966 302967 [...] (8241 flits)
Class 0:
Remaining flits: 471402 471403 471404 471405 471406 471407 471408 471409 471410 471411 [...] (224 flits)
Measured flits: 471402 471403 471404 471405 471406 471407 471408 471409 471410 471411 [...] (224 flits)
Time taken is 18242 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7571.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14255 (1 samples)
Network latency average = 7550.37 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14255 (1 samples)
Flit latency average = 6247.81 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14832 (1 samples)
Fragmentation average = 66.5763 (1 samples)
	minimum = 0 (1 samples)
	maximum = 164 (1 samples)
Injected packet rate average = 0.0260365 (1 samples)
	minimum = 0.0186667 (1 samples)
	maximum = 0.0353333 (1 samples)
Accepted packet rate average = 0.0105035 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.468349 (1 samples)
	minimum = 0.330667 (1 samples)
	maximum = 0.634 (1 samples)
Accepted flit rate average = 0.189172 (1 samples)
	minimum = 0.130667 (1 samples)
	maximum = 0.283667 (1 samples)
Injected packet size average = 17.9882 (1 samples)
Accepted packet size average = 18.0104 (1 samples)
Hops average = 5.08908 (1 samples)
Total run time 10.8097
