BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 377.29
	minimum = 23
	maximum = 952
Network latency average = 302.494
	minimum = 23
	maximum = 929
Slowest packet = 285
Flit latency average = 254.92
	minimum = 6
	maximum = 925
Slowest flit = 6636
Fragmentation average = 57.2331
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0139844
	minimum = 0.006 (at node 4)
	maximum = 0.03 (at node 138)
Accepted packet rate average = 0.00900521
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.247411
	minimum = 0.108 (at node 4)
	maximum = 0.535 (at node 138)
Accepted flit rate average= 0.167911
	minimum = 0.036 (at node 41)
	maximum = 0.288 (at node 91)
Injected packet length average = 17.692
Accepted packet length average = 18.646
Total in-flight flits = 17819 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 751.074
	minimum = 23
	maximum = 1830
Network latency average = 417.749
	minimum = 23
	maximum = 1710
Slowest packet = 1069
Flit latency average = 362.44
	minimum = 6
	maximum = 1822
Slowest flit = 15293
Fragmentation average = 57.6636
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0113828
	minimum = 0.005 (at node 181)
	maximum = 0.0215 (at node 94)
Accepted packet rate average = 0.00896354
	minimum = 0.0055 (at node 79)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.202849
	minimum = 0.09 (at node 181)
	maximum = 0.3785 (at node 94)
Accepted flit rate average= 0.164237
	minimum = 0.099 (at node 79)
	maximum = 0.27 (at node 44)
Injected packet length average = 17.8206
Accepted packet length average = 18.3228
Total in-flight flits = 17501 (0 measured)
latency change    = 0.497666
throughput change = 0.022373
Class 0:
Packet latency average = 1900.41
	minimum = 1114
	maximum = 2753
Network latency average = 558.296
	minimum = 23
	maximum = 2424
Slowest packet = 906
Flit latency average = 495.196
	minimum = 6
	maximum = 2349
Slowest flit = 22772
Fragmentation average = 58.6548
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00856771
	minimum = 0 (at node 6)
	maximum = 0.027 (at node 10)
Accepted packet rate average = 0.00849479
	minimum = 0 (at node 121)
	maximum = 0.018 (at node 78)
Injected flit rate average = 0.154729
	minimum = 0 (at node 6)
	maximum = 0.485 (at node 10)
Accepted flit rate average= 0.153818
	minimum = 0 (at node 121)
	maximum = 0.307 (at node 78)
Injected packet length average = 18.0596
Accepted packet length average = 18.1073
Total in-flight flits = 17704 (0 measured)
latency change    = 0.604783
throughput change = 0.0677378
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2784.85
	minimum = 1639
	maximum = 3600
Network latency average = 333.703
	minimum = 23
	maximum = 943
Slowest packet = 6148
Flit latency average = 506.375
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 52.1596
	minimum = 0
	maximum = 134
Injected packet rate average = 0.00864583
	minimum = 0.001 (at node 4)
	maximum = 0.022 (at node 25)
Accepted packet rate average = 0.00869792
	minimum = 0.002 (at node 185)
	maximum = 0.017 (at node 34)
Injected flit rate average = 0.155396
	minimum = 0.003 (at node 119)
	maximum = 0.397 (at node 25)
Accepted flit rate average= 0.155615
	minimum = 0.045 (at node 190)
	maximum = 0.292 (at node 34)
Injected packet length average = 17.9735
Accepted packet length average = 17.891
Total in-flight flits = 17598 (15702 measured)
latency change    = 0.317591
throughput change = 0.011547
Class 0:
Packet latency average = 3276.45
	minimum = 1639
	maximum = 4542
Network latency average = 446.284
	minimum = 23
	maximum = 1893
Slowest packet = 6148
Flit latency average = 498.911
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 55.6947
	minimum = 0
	maximum = 161
Injected packet rate average = 0.00884375
	minimum = 0.0025 (at node 58)
	maximum = 0.0165 (at node 23)
Accepted packet rate average = 0.00883073
	minimum = 0.0045 (at node 21)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.158674
	minimum = 0.037 (at node 58)
	maximum = 0.2985 (at node 139)
Accepted flit rate average= 0.158857
	minimum = 0.081 (at node 190)
	maximum = 0.302 (at node 16)
Injected packet length average = 17.942
Accepted packet length average = 17.9891
Total in-flight flits = 17561 (17507 measured)
latency change    = 0.15004
throughput change = 0.0204095
Class 0:
Packet latency average = 3682.45
	minimum = 1639
	maximum = 5404
Network latency average = 490.204
	minimum = 23
	maximum = 2157
Slowest packet = 6148
Flit latency average = 495.062
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 58.3194
	minimum = 0
	maximum = 161
Injected packet rate average = 0.00886458
	minimum = 0.00266667 (at node 153)
	maximum = 0.0156667 (at node 139)
Accepted packet rate average = 0.00886979
	minimum = 0.00466667 (at node 190)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.159389
	minimum = 0.0436667 (at node 153)
	maximum = 0.279 (at node 26)
Accepted flit rate average= 0.159573
	minimum = 0.0846667 (at node 190)
	maximum = 0.282667 (at node 16)
Injected packet length average = 17.9804
Accepted packet length average = 17.9906
Total in-flight flits = 17590 (17590 measured)
latency change    = 0.110251
throughput change = 0.00448789
Class 0:
Packet latency average = 4073.75
	minimum = 1639
	maximum = 6329
Network latency average = 509.469
	minimum = 23
	maximum = 2512
Slowest packet = 6148
Flit latency average = 494.664
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 58.5659
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0088151
	minimum = 0.00375 (at node 79)
	maximum = 0.01575 (at node 139)
Accepted packet rate average = 0.00885156
	minimum = 0.005 (at node 4)
	maximum = 0.01375 (at node 16)
Injected flit rate average = 0.158594
	minimum = 0.0635 (at node 79)
	maximum = 0.28525 (at node 139)
Accepted flit rate average= 0.158988
	minimum = 0.0925 (at node 4)
	maximum = 0.2435 (at node 16)
Injected packet length average = 17.9911
Accepted packet length average = 17.9616
Total in-flight flits = 17515 (17515 measured)
latency change    = 0.096054
throughput change = 0.00367722
Class 0:
Packet latency average = 4474.46
	minimum = 1639
	maximum = 7098
Network latency average = 522.635
	minimum = 23
	maximum = 2898
Slowest packet = 6148
Flit latency average = 496.441
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 58.0461
	minimum = 0
	maximum = 161
Injected packet rate average = 0.00890625
	minimum = 0.004 (at node 117)
	maximum = 0.0142 (at node 139)
Accepted packet rate average = 0.00886354
	minimum = 0.006 (at node 4)
	maximum = 0.0128 (at node 16)
Injected flit rate average = 0.16007
	minimum = 0.0722 (at node 117)
	maximum = 0.2548 (at node 139)
Accepted flit rate average= 0.159522
	minimum = 0.109 (at node 36)
	maximum = 0.2272 (at node 16)
Injected packet length average = 17.9727
Accepted packet length average = 17.9975
Total in-flight flits = 18121 (18121 measured)
latency change    = 0.0895565
throughput change = 0.00334496
Class 0:
Packet latency average = 4869.38
	minimum = 1639
	maximum = 7931
Network latency average = 529.303
	minimum = 23
	maximum = 2898
Slowest packet = 6148
Flit latency average = 496.114
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 58.4038
	minimum = 0
	maximum = 161
Injected packet rate average = 0.00886372
	minimum = 0.00416667 (at node 118)
	maximum = 0.0141667 (at node 84)
Accepted packet rate average = 0.00884809
	minimum = 0.00616667 (at node 36)
	maximum = 0.0123333 (at node 128)
Injected flit rate average = 0.159495
	minimum = 0.0753333 (at node 118)
	maximum = 0.256667 (at node 84)
Accepted flit rate average= 0.159257
	minimum = 0.1125 (at node 36)
	maximum = 0.222667 (at node 128)
Injected packet length average = 17.9941
Accepted packet length average = 17.999
Total in-flight flits = 17876 (17876 measured)
latency change    = 0.0811013
throughput change = 0.00166354
Class 0:
Packet latency average = 5256.42
	minimum = 1639
	maximum = 8759
Network latency average = 531.312
	minimum = 23
	maximum = 2898
Slowest packet = 6148
Flit latency average = 493.824
	minimum = 6
	maximum = 3367
Slowest flit = 27522
Fragmentation average = 58.2128
	minimum = 0
	maximum = 161
Injected packet rate average = 0.00882738
	minimum = 0.00457143 (at node 152)
	maximum = 0.0137143 (at node 84)
Accepted packet rate average = 0.00882813
	minimum = 0.00628571 (at node 36)
	maximum = 0.0114286 (at node 128)
Injected flit rate average = 0.158864
	minimum = 0.0798571 (at node 152)
	maximum = 0.248143 (at node 84)
Accepted flit rate average= 0.158818
	minimum = 0.115 (at node 36)
	maximum = 0.206286 (at node 128)
Injected packet length average = 17.9967
Accepted packet length average = 17.9901
Total in-flight flits = 17912 (17912 measured)
latency change    = 0.073633
throughput change = 0.00276096
Draining all recorded packets ...
Class 0:
Remaining flits: 266616 266617 266618 266619 266620 266621 266622 266623 266624 266625 [...] (17918 flits)
Measured flits: 266616 266617 266618 266619 266620 266621 266622 266623 266624 266625 [...] (17918 flits)
Class 0:
Remaining flits: 319968 319969 319970 319971 319972 319973 319974 319975 319976 319977 [...] (17827 flits)
Measured flits: 319968 319969 319970 319971 319972 319973 319974 319975 319976 319977 [...] (17827 flits)
Class 0:
Remaining flits: 327318 327319 327320 327321 327322 327323 327324 327325 327326 327327 [...] (17597 flits)
Measured flits: 327318 327319 327320 327321 327322 327323 327324 327325 327326 327327 [...] (17597 flits)
Class 0:
Remaining flits: 355248 355249 355250 355251 355252 355253 355254 355255 355256 355257 [...] (17719 flits)
Measured flits: 355248 355249 355250 355251 355252 355253 355254 355255 355256 355257 [...] (17719 flits)
Class 0:
Remaining flits: 387468 387469 387470 387471 387472 387473 387474 387475 387476 387477 [...] (17899 flits)
Measured flits: 387468 387469 387470 387471 387472 387473 387474 387475 387476 387477 [...] (17899 flits)
Class 0:
Remaining flits: 387484 387485 398538 398539 398540 398541 398542 398543 398544 398545 [...] (17795 flits)
Measured flits: 387484 387485 398538 398539 398540 398541 398542 398543 398544 398545 [...] (17795 flits)
Class 0:
Remaining flits: 452916 452917 452918 452919 452920 452921 452922 452923 452924 452925 [...] (17801 flits)
Measured flits: 452916 452917 452918 452919 452920 452921 452922 452923 452924 452925 [...] (17801 flits)
Class 0:
Remaining flits: 489906 489907 489908 489909 489910 489911 489912 489913 489914 489915 [...] (17572 flits)
Measured flits: 489906 489907 489908 489909 489910 489911 489912 489913 489914 489915 [...] (17572 flits)
Class 0:
Remaining flits: 525096 525097 525098 525099 525100 525101 525102 525103 525104 525105 [...] (17680 flits)
Measured flits: 525096 525097 525098 525099 525100 525101 525102 525103 525104 525105 [...] (17680 flits)
Class 0:
Remaining flits: 536922 536923 536924 536925 536926 536927 536928 536929 536930 536931 [...] (17875 flits)
Measured flits: 536922 536923 536924 536925 536926 536927 536928 536929 536930 536931 [...] (17875 flits)
Class 0:
Remaining flits: 580554 580555 580556 580557 580558 580559 580560 580561 580562 580563 [...] (17344 flits)
Measured flits: 580554 580555 580556 580557 580558 580559 580560 580561 580562 580563 [...] (17344 flits)
Class 0:
Remaining flits: 614898 614899 614900 614901 614902 614903 614904 614905 614906 614907 [...] (17237 flits)
Measured flits: 614898 614899 614900 614901 614902 614903 614904 614905 614906 614907 [...] (17237 flits)
Class 0:
Remaining flits: 650700 650701 650702 650703 650704 650705 650706 650707 650708 650709 [...] (18045 flits)
Measured flits: 650700 650701 650702 650703 650704 650705 650706 650707 650708 650709 [...] (18045 flits)
Class 0:
Remaining flits: 693586 693587 693588 693589 693590 693591 693592 693593 693810 693811 [...] (17719 flits)
Measured flits: 693586 693587 693588 693589 693590 693591 693592 693593 693810 693811 [...] (17719 flits)
Class 0:
Remaining flits: 730109 730110 730111 730112 730113 730114 730115 730584 730585 730586 [...] (18011 flits)
Measured flits: 730109 730110 730111 730112 730113 730114 730115 730584 730585 730586 [...] (18011 flits)
Class 0:
Remaining flits: 756054 756055 756056 756057 756058 756059 756060 756061 756062 756063 [...] (17704 flits)
Measured flits: 756054 756055 756056 756057 756058 756059 756060 756061 756062 756063 [...] (17704 flits)
Class 0:
Remaining flits: 787380 787381 787382 787383 787384 787385 787386 787387 787388 787389 [...] (17883 flits)
Measured flits: 787380 787381 787382 787383 787384 787385 787386 787387 787388 787389 [...] (17883 flits)
Class 0:
Remaining flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (17296 flits)
Measured flits: 800370 800371 800372 800373 800374 800375 800376 800377 800378 800379 [...] (17296 flits)
Class 0:
Remaining flits: 831070 831071 831072 831073 831074 831075 831076 831077 838746 838747 [...] (17699 flits)
Measured flits: 831070 831071 831072 831073 831074 831075 831076 831077 838746 838747 [...] (17699 flits)
Class 0:
Remaining flits: 873804 873805 873806 873807 873808 873809 874206 874207 874208 874209 [...] (17684 flits)
Measured flits: 873804 873805 873806 873807 873808 873809 874206 874207 874208 874209 [...] (17684 flits)
Class 0:
Remaining flits: 906444 906445 906446 906447 906448 906449 906450 906451 906452 906453 [...] (17523 flits)
Measured flits: 906444 906445 906446 906447 906448 906449 906450 906451 906452 906453 [...] (17523 flits)
Class 0:
Remaining flits: 940502 940503 940504 940505 940506 940507 940508 940509 940510 940511 [...] (17652 flits)
Measured flits: 940502 940503 940504 940505 940506 940507 940508 940509 940510 940511 [...] (17652 flits)
Class 0:
Remaining flits: 967410 967411 967412 967413 967414 967415 967416 967417 967418 967419 [...] (17951 flits)
Measured flits: 967410 967411 967412 967413 967414 967415 967416 967417 967418 967419 [...] (17951 flits)
Class 0:
Remaining flits: 989190 989191 989192 989193 989194 989195 989196 989197 989198 989199 [...] (17404 flits)
Measured flits: 989190 989191 989192 989193 989194 989195 989196 989197 989198 989199 [...] (17404 flits)
Class 0:
Remaining flits: 1008774 1008775 1008776 1008777 1008778 1008779 1008780 1008781 1008782 1008783 [...] (17726 flits)
Measured flits: 1008774 1008775 1008776 1008777 1008778 1008779 1008780 1008781 1008782 1008783 [...] (17690 flits)
Class 0:
Remaining flits: 1043802 1043803 1043804 1043805 1043806 1043807 1043808 1043809 1043810 1043811 [...] (17691 flits)
Measured flits: 1043802 1043803 1043804 1043805 1043806 1043807 1043808 1043809 1043810 1043811 [...] (17655 flits)
Class 0:
Remaining flits: 1088226 1088227 1088228 1088229 1088230 1088231 1088232 1088233 1088234 1088235 [...] (18127 flits)
Measured flits: 1088226 1088227 1088228 1088229 1088230 1088231 1088232 1088233 1088234 1088235 [...] (17965 flits)
Class 0:
Remaining flits: 1122174 1122175 1122176 1122177 1122178 1122179 1122180 1122181 1122182 1122183 [...] (17825 flits)
Measured flits: 1122174 1122175 1122176 1122177 1122178 1122179 1122180 1122181 1122182 1122183 [...] (17473 flits)
Class 0:
Remaining flits: 1154571 1154572 1154573 1155720 1155721 1155722 1155723 1155724 1155725 1156185 [...] (17473 flits)
Measured flits: 1154571 1154572 1154573 1155720 1155721 1155722 1155723 1155724 1155725 1156185 [...] (16595 flits)
Class 0:
Remaining flits: 1162998 1162999 1163000 1163001 1163002 1163003 1163004 1163005 1163006 1163007 [...] (17734 flits)
Measured flits: 1162998 1162999 1163000 1163001 1163002 1163003 1163004 1163005 1163006 1163007 [...] (16287 flits)
Class 0:
Remaining flits: 1207044 1207045 1207046 1207047 1207048 1207049 1207050 1207051 1207052 1207053 [...] (17850 flits)
Measured flits: 1207044 1207045 1207046 1207047 1207048 1207049 1207050 1207051 1207052 1207053 [...] (14345 flits)
Class 0:
Remaining flits: 1244250 1244251 1244252 1244253 1244254 1244255 1244256 1244257 1244258 1244259 [...] (17789 flits)
Measured flits: 1244250 1244251 1244252 1244253 1244254 1244255 1244256 1244257 1244258 1244259 [...] (13034 flits)
Class 0:
Remaining flits: 1261656 1261657 1261658 1261659 1261660 1261661 1261662 1261663 1261664 1261665 [...] (17910 flits)
Measured flits: 1261656 1261657 1261658 1261659 1261660 1261661 1261662 1261663 1261664 1261665 [...] (11125 flits)
Class 0:
Remaining flits: 1277658 1277659 1277660 1277661 1277662 1277663 1277664 1277665 1277666 1277667 [...] (17693 flits)
Measured flits: 1277658 1277659 1277660 1277661 1277662 1277663 1277664 1277665 1277666 1277667 [...] (9621 flits)
Class 0:
Remaining flits: 1327662 1327663 1327664 1327665 1327666 1327667 1327668 1327669 1327670 1327671 [...] (17807 flits)
Measured flits: 1327662 1327663 1327664 1327665 1327666 1327667 1327668 1327669 1327670 1327671 [...] (7927 flits)
Class 0:
Remaining flits: 1368504 1368505 1368506 1368507 1368508 1368509 1368510 1368511 1368512 1368513 [...] (17950 flits)
Measured flits: 1369170 1369171 1369172 1369173 1369174 1369175 1369176 1369177 1369178 1369179 [...] (6524 flits)
Class 0:
Remaining flits: 1387206 1387207 1387208 1387209 1387210 1387211 1387212 1387213 1387214 1387215 [...] (17381 flits)
Measured flits: 1387206 1387207 1387208 1387209 1387210 1387211 1387212 1387213 1387214 1387215 [...] (4771 flits)
Class 0:
Remaining flits: 1424682 1424683 1424684 1424685 1424686 1424687 1424688 1424689 1424690 1424691 [...] (17973 flits)
Measured flits: 1439162 1439163 1439164 1439165 1439166 1439167 1439168 1439169 1439170 1439171 [...] (3715 flits)
Class 0:
Remaining flits: 1440450 1440451 1440452 1440453 1440454 1440455 1440456 1440457 1440458 1440459 [...] (17627 flits)
Measured flits: 1468185 1468186 1468187 1469952 1469953 1469954 1469955 1469956 1469957 1469958 [...] (2625 flits)
Class 0:
Remaining flits: 1499922 1499923 1499924 1499925 1499926 1499927 1499928 1499929 1499930 1499931 [...] (17646 flits)
Measured flits: 1512252 1512253 1512254 1512255 1512256 1512257 1512258 1512259 1512260 1512261 [...] (1601 flits)
Class 0:
Remaining flits: 1514917 1514918 1514919 1514920 1514921 1514922 1514923 1514924 1514925 1514926 [...] (17729 flits)
Measured flits: 1529586 1529587 1529588 1529589 1529590 1529591 1529592 1529593 1529594 1529595 [...] (1268 flits)
Class 0:
Remaining flits: 1527300 1527301 1527302 1527303 1527304 1527305 1527306 1527307 1527308 1527309 [...] (17459 flits)
Measured flits: 1540116 1540117 1540118 1540119 1540120 1540121 1540122 1540123 1540124 1540125 [...] (840 flits)
Class 0:
Remaining flits: 1593378 1593379 1593380 1593381 1593382 1593383 1593384 1593385 1593386 1593387 [...] (17702 flits)
Measured flits: 1622279 1622280 1622281 1622282 1622283 1622284 1622285 1624749 1624750 1624751 [...] (516 flits)
Class 0:
Remaining flits: 1621014 1621015 1621016 1621017 1621018 1621019 1621020 1621021 1621022 1621023 [...] (17864 flits)
Measured flits: 1651968 1651969 1651970 1651971 1651972 1651973 1651974 1651975 1651976 1651977 [...] (306 flits)
Class 0:
Remaining flits: 1635759 1635760 1635761 1635762 1635763 1635764 1635765 1635766 1635767 1644966 [...] (17405 flits)
Measured flits: 1686437 1690074 1690075 1690076 1690077 1690078 1690079 1690080 1690081 1690082 [...] (163 flits)
Class 0:
Remaining flits: 1664262 1664263 1664264 1664265 1664266 1664267 1664268 1664269 1664270 1664271 [...] (17651 flits)
Measured flits: 1719466 1719467 1724903 1728847 1728848 1728849 1728850 1728851 1728852 1728853 [...] (140 flits)
Class 0:
Remaining flits: 1691154 1691155 1691156 1691157 1691158 1691159 1691160 1691161 1691162 1691163 [...] (17321 flits)
Measured flits: 1765044 1765045 1765046 1765047 1765048 1765049 1765050 1765051 1765052 1765053 [...] (36 flits)
Draining remaining packets ...
Time taken is 58176 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 19145.1 (1 samples)
	minimum = 1639 (1 samples)
	maximum = 47320 (1 samples)
Network latency average = 551.66 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3668 (1 samples)
Flit latency average = 491.724 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3777 (1 samples)
Fragmentation average = 58.4424 (1 samples)
	minimum = 0 (1 samples)
	maximum = 161 (1 samples)
Injected packet rate average = 0.00882738 (1 samples)
	minimum = 0.00457143 (1 samples)
	maximum = 0.0137143 (1 samples)
Accepted packet rate average = 0.00882813 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0114286 (1 samples)
Injected flit rate average = 0.158864 (1 samples)
	minimum = 0.0798571 (1 samples)
	maximum = 0.248143 (1 samples)
Accepted flit rate average = 0.158818 (1 samples)
	minimum = 0.115 (1 samples)
	maximum = 0.206286 (1 samples)
Injected packet size average = 17.9967 (1 samples)
Accepted packet size average = 17.9901 (1 samples)
Hops average = 5.07028 (1 samples)
Total run time 43.3361
