BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.924
	minimum = 23
	maximum = 985
Network latency average = 257.466
	minimum = 23
	maximum = 932
Slowest packet = 46
Flit latency average = 215.839
	minimum = 6
	maximum = 915
Slowest flit = 6029
Fragmentation average = 50.5893
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0135104
	minimum = 0 (at node 42)
	maximum = 0.029 (at node 46)
Accepted packet rate average = 0.00892708
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 48)
Injected flit rate average = 0.239646
	minimum = 0 (at node 42)
	maximum = 0.516 (at node 46)
Accepted flit rate average= 0.166292
	minimum = 0.036 (at node 41)
	maximum = 0.307 (at node 48)
Injected packet length average = 17.7379
Accepted packet length average = 18.6278
Total in-flight flits = 16330 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 713.658
	minimum = 23
	maximum = 1971
Network latency average = 371.419
	minimum = 23
	maximum = 1560
Slowest packet = 46
Flit latency average = 319.564
	minimum = 6
	maximum = 1543
Slowest flit = 9917
Fragmentation average = 55.1404
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0113516
	minimum = 0.003 (at node 168)
	maximum = 0.021 (at node 46)
Accepted packet rate average = 0.00892188
	minimum = 0.005 (at node 25)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.202115
	minimum = 0.0465 (at node 168)
	maximum = 0.378 (at node 46)
Accepted flit rate average= 0.163448
	minimum = 0.09 (at node 25)
	maximum = 0.2785 (at node 22)
Injected packet length average = 17.805
Accepted packet length average = 18.3199
Total in-flight flits = 17372 (0 measured)
latency change    = 0.473243
throughput change = 0.0173985
Class 0:
Packet latency average = 1847.72
	minimum = 23
	maximum = 2927
Network latency average = 516.967
	minimum = 23
	maximum = 2192
Slowest packet = 1870
Flit latency average = 454.123
	minimum = 6
	maximum = 2175
Slowest flit = 18449
Fragmentation average = 56.7927
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00905208
	minimum = 0 (at node 100)
	maximum = 0.026 (at node 62)
Accepted packet rate average = 0.00904688
	minimum = 0.001 (at node 153)
	maximum = 0.017 (at node 113)
Injected flit rate average = 0.163755
	minimum = 0 (at node 100)
	maximum = 0.47 (at node 62)
Accepted flit rate average= 0.163781
	minimum = 0.018 (at node 153)
	maximum = 0.329 (at node 113)
Injected packet length average = 18.0903
Accepted packet length average = 18.1036
Total in-flight flits = 17678 (0 measured)
latency change    = 0.613763
throughput change = 0.00203524
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2705.39
	minimum = 23
	maximum = 3891
Network latency average = 335.714
	minimum = 23
	maximum = 933
Slowest packet = 6262
Flit latency average = 503.297
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 54.2784
	minimum = 0
	maximum = 130
Injected packet rate average = 0.00895313
	minimum = 0 (at node 144)
	maximum = 0.021 (at node 35)
Accepted packet rate average = 0.00893229
	minimum = 0.002 (at node 184)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.16099
	minimum = 0 (at node 144)
	maximum = 0.365 (at node 35)
Accepted flit rate average= 0.160516
	minimum = 0.036 (at node 184)
	maximum = 0.335 (at node 34)
Injected packet length average = 17.9814
Accepted packet length average = 17.9703
Total in-flight flits = 17675 (16440 measured)
latency change    = 0.317022
throughput change = 0.0203446
Class 0:
Packet latency average = 3147.04
	minimum = 23
	maximum = 4823
Network latency average = 451.473
	minimum = 23
	maximum = 1819
Slowest packet = 6262
Flit latency average = 491.134
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 56.0777
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0089349
	minimum = 0.002 (at node 1)
	maximum = 0.017 (at node 35)
Accepted packet rate average = 0.00890365
	minimum = 0.0045 (at node 54)
	maximum = 0.0175 (at node 16)
Injected flit rate average = 0.160734
	minimum = 0.036 (at node 1)
	maximum = 0.3035 (at node 35)
Accepted flit rate average= 0.160062
	minimum = 0.0795 (at node 163)
	maximum = 0.3175 (at node 16)
Injected packet length average = 17.9895
Accepted packet length average = 17.9772
Total in-flight flits = 17648 (17601 measured)
latency change    = 0.140338
throughput change = 0.00283093
Class 0:
Packet latency average = 3564.72
	minimum = 23
	maximum = 5652
Network latency average = 497.306
	minimum = 23
	maximum = 2139
Slowest packet = 6262
Flit latency average = 493.612
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 57.329
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00889757
	minimum = 0.00366667 (at node 17)
	maximum = 0.016 (at node 35)
Accepted packet rate average = 0.00886285
	minimum = 0.005 (at node 190)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.160252
	minimum = 0.064 (at node 51)
	maximum = 0.284667 (at node 35)
Accepted flit rate average= 0.159611
	minimum = 0.0913333 (at node 17)
	maximum = 0.262 (at node 16)
Injected packet length average = 18.0107
Accepted packet length average = 18.009
Total in-flight flits = 17830 (17830 measured)
latency change    = 0.11717
throughput change = 0.00282805
Class 0:
Packet latency average = 3926.61
	minimum = 23
	maximum = 6604
Network latency average = 510.763
	minimum = 23
	maximum = 2402
Slowest packet = 6262
Flit latency average = 490.081
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 57.7657
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00890365
	minimum = 0.00375 (at node 19)
	maximum = 0.014 (at node 35)
Accepted packet rate average = 0.00891276
	minimum = 0.005 (at node 4)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.16026
	minimum = 0.064 (at node 19)
	maximum = 0.25075 (at node 35)
Accepted flit rate average= 0.160246
	minimum = 0.09 (at node 4)
	maximum = 0.24975 (at node 16)
Injected packet length average = 17.9994
Accepted packet length average = 17.9794
Total in-flight flits = 17603 (17603 measured)
latency change    = 0.0921634
throughput change = 0.00396255
Class 0:
Packet latency average = 4308.18
	minimum = 23
	maximum = 7591
Network latency average = 523.051
	minimum = 23
	maximum = 2402
Slowest packet = 6262
Flit latency average = 492.531
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 57.909
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00894167
	minimum = 0.0046 (at node 177)
	maximum = 0.013 (at node 35)
Accepted packet rate average = 0.00891458
	minimum = 0.0054 (at node 4)
	maximum = 0.0124 (at node 16)
Injected flit rate average = 0.160731
	minimum = 0.0828 (at node 177)
	maximum = 0.2368 (at node 97)
Accepted flit rate average= 0.160157
	minimum = 0.098 (at node 4)
	maximum = 0.2248 (at node 16)
Injected packet length average = 17.9755
Accepted packet length average = 17.9658
Total in-flight flits = 18115 (18115 measured)
latency change    = 0.0885696
throughput change = 0.000554468
Class 0:
Packet latency average = 4685.76
	minimum = 23
	maximum = 8261
Network latency average = 527.697
	minimum = 23
	maximum = 2602
Slowest packet = 6262
Flit latency average = 491.358
	minimum = 6
	maximum = 2917
Slowest flit = 30743
Fragmentation average = 58.2654
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00893403
	minimum = 0.00466667 (at node 187)
	maximum = 0.0135 (at node 68)
Accepted packet rate average = 0.00889931
	minimum = 0.006 (at node 36)
	maximum = 0.0123333 (at node 128)
Injected flit rate average = 0.160818
	minimum = 0.0841667 (at node 187)
	maximum = 0.241333 (at node 68)
Accepted flit rate average= 0.160189
	minimum = 0.11 (at node 36)
	maximum = 0.223667 (at node 128)
Injected packet length average = 18.0006
Accepted packet length average = 18.0002
Total in-flight flits = 18198 (18198 measured)
latency change    = 0.0805794
throughput change = 0.000199417
Class 0:
Packet latency average = 5078.83
	minimum = 23
	maximum = 9184
Network latency average = 534.17
	minimum = 23
	maximum = 3142
Slowest packet = 6262
Flit latency average = 493.231
	minimum = 6
	maximum = 3125
Slowest flit = 216989
Fragmentation average = 58.6179
	minimum = 0
	maximum = 155
Injected packet rate average = 0.00892708
	minimum = 0.00557143 (at node 148)
	maximum = 0.0127143 (at node 68)
Accepted packet rate average = 0.00889807
	minimum = 0.00628571 (at node 36)
	maximum = 0.0121429 (at node 128)
Injected flit rate average = 0.160554
	minimum = 0.0978571 (at node 148)
	maximum = 0.228857 (at node 68)
Accepted flit rate average= 0.160112
	minimum = 0.112714 (at node 36)
	maximum = 0.218714 (at node 128)
Injected packet length average = 17.9851
Accepted packet length average = 17.9941
Total in-flight flits = 18055 (18055 measured)
latency change    = 0.0773942
throughput change = 0.000480194
Draining all recorded packets ...
Class 0:
Remaining flits: 293310 293311 293312 293313 293314 293315 293316 293317 293318 293319 [...] (17617 flits)
Measured flits: 293310 293311 293312 293313 293314 293315 293316 293317 293318 293319 [...] (17617 flits)
Class 0:
Remaining flits: 312408 312409 312410 312411 312412 312413 312414 312415 312416 312417 [...] (18321 flits)
Measured flits: 312408 312409 312410 312411 312412 312413 312414 312415 312416 312417 [...] (18321 flits)
Class 0:
Remaining flits: 341820 341821 341822 341823 341824 341825 341826 341827 341828 341829 [...] (17493 flits)
Measured flits: 341820 341821 341822 341823 341824 341825 341826 341827 341828 341829 [...] (17493 flits)
Class 0:
Remaining flits: 383796 383797 383798 383799 383800 383801 383802 383803 383804 383805 [...] (17604 flits)
Measured flits: 383796 383797 383798 383799 383800 383801 383802 383803 383804 383805 [...] (17604 flits)
Class 0:
Remaining flits: 409824 409825 409826 409827 409828 409829 409830 409831 409832 409833 [...] (17691 flits)
Measured flits: 409824 409825 409826 409827 409828 409829 409830 409831 409832 409833 [...] (17691 flits)
Class 0:
Remaining flits: 437637 437638 437639 437640 437641 437642 437643 437644 437645 437646 [...] (17390 flits)
Measured flits: 437637 437638 437639 437640 437641 437642 437643 437644 437645 437646 [...] (17390 flits)
Class 0:
Remaining flits: 462168 462169 462170 462171 462172 462173 462174 462175 462176 462177 [...] (17722 flits)
Measured flits: 462168 462169 462170 462171 462172 462173 462174 462175 462176 462177 [...] (17650 flits)
Class 0:
Remaining flits: 465192 465193 465194 465195 465196 465197 465198 465199 465200 465201 [...] (17656 flits)
Measured flits: 465192 465193 465194 465195 465196 465197 465198 465199 465200 465201 [...] (17453 flits)
Class 0:
Remaining flits: 506466 506467 506468 506469 506470 506471 506472 506473 506474 506475 [...] (18069 flits)
Measured flits: 506466 506467 506468 506469 506470 506471 506472 506473 506474 506475 [...] (17824 flits)
Class 0:
Remaining flits: 548658 548659 548660 548661 548662 548663 548664 548665 548666 548667 [...] (17733 flits)
Measured flits: 548658 548659 548660 548661 548662 548663 548664 548665 548666 548667 [...] (17553 flits)
Class 0:
Remaining flits: 605818 605819 605820 605821 605822 605823 605824 605825 607464 607465 [...] (17602 flits)
Measured flits: 605818 605819 605820 605821 605822 605823 605824 605825 607464 607465 [...] (17404 flits)
Class 0:
Remaining flits: 634104 634105 634106 634107 634108 634109 634110 634111 634112 634113 [...] (18005 flits)
Measured flits: 634104 634105 634106 634107 634108 634109 634110 634111 634112 634113 [...] (17759 flits)
Class 0:
Remaining flits: 654418 654419 654420 654421 654422 654423 654424 654425 667422 667423 [...] (17508 flits)
Measured flits: 654418 654419 654420 654421 654422 654423 654424 654425 667422 667423 [...] (17120 flits)
Class 0:
Remaining flits: 694479 694480 694481 694482 694483 694484 694485 694486 694487 694488 [...] (17784 flits)
Measured flits: 694479 694480 694481 694482 694483 694484 694485 694486 694487 694488 [...] (17216 flits)
Class 0:
Remaining flits: 713106 713107 713108 713109 713110 713111 713112 713113 713114 713115 [...] (17510 flits)
Measured flits: 713106 713107 713108 713109 713110 713111 713112 713113 713114 713115 [...] (16700 flits)
Class 0:
Remaining flits: 714744 714745 714746 714747 714748 714749 714750 714751 714752 714753 [...] (17951 flits)
Measured flits: 714744 714745 714746 714747 714748 714749 714750 714751 714752 714753 [...] (16957 flits)
Class 0:
Remaining flits: 782568 782569 782570 782571 782572 782573 782574 782575 782576 782577 [...] (17572 flits)
Measured flits: 782568 782569 782570 782571 782572 782573 782574 782575 782576 782577 [...] (16387 flits)
Class 0:
Remaining flits: 814878 814879 814880 814881 814882 814883 814884 814885 814886 814887 [...] (17878 flits)
Measured flits: 814878 814879 814880 814881 814882 814883 814884 814885 814886 814887 [...] (16380 flits)
Class 0:
Remaining flits: 834084 834085 834086 834087 834088 834089 834090 834091 834092 834093 [...] (17465 flits)
Measured flits: 834084 834085 834086 834087 834088 834089 834090 834091 834092 834093 [...] (15863 flits)
Class 0:
Remaining flits: 874876 874877 874878 874879 874880 874881 874882 874883 874884 874885 [...] (17853 flits)
Measured flits: 874876 874877 874878 874879 874880 874881 874882 874883 874884 874885 [...] (15880 flits)
Class 0:
Remaining flits: 899820 899821 899822 899823 899824 899825 899826 899827 899828 899829 [...] (17940 flits)
Measured flits: 899820 899821 899822 899823 899824 899825 899826 899827 899828 899829 [...] (15953 flits)
Class 0:
Remaining flits: 959736 959737 959738 959739 959740 959741 960156 960157 960158 960159 [...] (17435 flits)
Measured flits: 959736 959737 959738 959739 959740 959741 960156 960157 960158 960159 [...] (14963 flits)
Class 0:
Remaining flits: 971334 971335 971336 971337 971338 971339 971340 971341 971342 971343 [...] (17793 flits)
Measured flits: 971334 971335 971336 971337 971338 971339 971340 971341 971342 971343 [...] (14689 flits)
Class 0:
Remaining flits: 1009242 1009243 1009244 1009245 1009246 1009247 1009248 1009249 1009250 1009251 [...] (17586 flits)
Measured flits: 1009242 1009243 1009244 1009245 1009246 1009247 1009248 1009249 1009250 1009251 [...] (13991 flits)
Class 0:
Remaining flits: 1015272 1015273 1015274 1015275 1015276 1015277 1015278 1015279 1015280 1015281 [...] (17436 flits)
Measured flits: 1015272 1015273 1015274 1015275 1015276 1015277 1015278 1015279 1015280 1015281 [...] (13087 flits)
Class 0:
Remaining flits: 1053792 1053793 1053794 1053795 1053796 1053797 1053798 1053799 1053800 1053801 [...] (17236 flits)
Measured flits: 1053792 1053793 1053794 1053795 1053796 1053797 1053798 1053799 1053800 1053801 [...] (12198 flits)
Class 0:
Remaining flits: 1094712 1094713 1094714 1094715 1094716 1094717 1094718 1094719 1094720 1094721 [...] (18064 flits)
Measured flits: 1094712 1094713 1094714 1094715 1094716 1094717 1094718 1094719 1094720 1094721 [...] (12120 flits)
Class 0:
Remaining flits: 1134108 1134109 1134110 1134111 1134112 1134113 1134114 1134115 1134116 1134117 [...] (17911 flits)
Measured flits: 1134108 1134109 1134110 1134111 1134112 1134113 1134114 1134115 1134116 1134117 [...] (11265 flits)
Class 0:
Remaining flits: 1147446 1147447 1147448 1147449 1147450 1147451 1147452 1147453 1147454 1147455 [...] (17883 flits)
Measured flits: 1147446 1147447 1147448 1147449 1147450 1147451 1147452 1147453 1147454 1147455 [...] (10055 flits)
Class 0:
Remaining flits: 1182852 1182853 1182854 1182855 1182856 1182857 1182858 1182859 1182860 1182861 [...] (18027 flits)
Measured flits: 1186902 1186903 1186904 1186905 1186906 1186907 1186908 1186909 1186910 1186911 [...] (9811 flits)
Class 0:
Remaining flits: 1222038 1222039 1222040 1222041 1222042 1222043 1222044 1222045 1222046 1222047 [...] (17707 flits)
Measured flits: 1225415 1225416 1225417 1225418 1225419 1225420 1225421 1227445 1227446 1227447 [...] (8787 flits)
Class 0:
Remaining flits: 1248796 1248797 1248798 1248799 1248800 1248801 1248802 1248803 1248954 1248955 [...] (17671 flits)
Measured flits: 1248796 1248797 1248798 1248799 1248800 1248801 1248802 1248803 1249794 1249795 [...] (8038 flits)
Class 0:
Remaining flits: 1264525 1264526 1264527 1264528 1264529 1264530 1264531 1264532 1264533 1264534 [...] (17721 flits)
Measured flits: 1280376 1280377 1280378 1280379 1280380 1280381 1280382 1280383 1280384 1280385 [...] (7549 flits)
Class 0:
Remaining flits: 1305576 1305577 1305578 1305579 1305580 1305581 1305582 1305583 1305584 1305585 [...] (17371 flits)
Measured flits: 1314612 1314613 1314614 1314615 1314616 1314617 1314618 1314619 1314620 1314621 [...] (6865 flits)
Class 0:
Remaining flits: 1345674 1345675 1345676 1345677 1345678 1345679 1350090 1350091 1350092 1350093 [...] (18031 flits)
Measured flits: 1345674 1345675 1345676 1345677 1345678 1345679 1350090 1350091 1350092 1350093 [...] (6312 flits)
Class 0:
Remaining flits: 1357751 1357752 1357753 1357754 1357755 1357756 1357757 1363662 1363663 1363664 [...] (18146 flits)
Measured flits: 1357751 1357752 1357753 1357754 1357755 1357756 1357757 1363662 1363663 1363664 [...] (5775 flits)
Class 0:
Remaining flits: 1380421 1380422 1380423 1380424 1380425 1380426 1380427 1380428 1380429 1380430 [...] (17791 flits)
Measured flits: 1380421 1380422 1380423 1380424 1380425 1380426 1380427 1380428 1380429 1380430 [...] (5157 flits)
Class 0:
Remaining flits: 1431036 1431037 1431038 1431039 1431040 1431041 1431042 1431043 1431044 1431045 [...] (17756 flits)
Measured flits: 1436202 1436203 1436204 1436205 1436206 1436207 1436208 1436209 1436210 1436211 [...] (4678 flits)
Class 0:
Remaining flits: 1469934 1469935 1469936 1469937 1469938 1469939 1469940 1469941 1469942 1469943 [...] (17421 flits)
Measured flits: 1469934 1469935 1469936 1469937 1469938 1469939 1469940 1469941 1469942 1469943 [...] (4198 flits)
Class 0:
Remaining flits: 1494043 1494044 1494045 1494046 1494047 1494048 1494049 1494050 1494051 1494052 [...] (17785 flits)
Measured flits: 1510776 1510777 1510778 1510779 1510780 1510781 1510782 1510783 1510784 1510785 [...] (3736 flits)
Class 0:
Remaining flits: 1533780 1533781 1533782 1533783 1533784 1533785 1533786 1533787 1533788 1533789 [...] (17406 flits)
Measured flits: 1541538 1541539 1541540 1541541 1541542 1541543 1541544 1541545 1541546 1541547 [...] (3137 flits)
Class 0:
Remaining flits: 1545534 1545535 1545536 1545537 1545538 1545539 1545540 1545541 1545542 1545543 [...] (17574 flits)
Measured flits: 1577772 1577773 1577774 1577775 1577776 1577777 1577778 1577779 1577780 1577781 [...] (2757 flits)
Class 0:
Remaining flits: 1570233 1570234 1570235 1570236 1570237 1570238 1570239 1570240 1570241 1570242 [...] (17726 flits)
Measured flits: 1590012 1590013 1590014 1590015 1590016 1590017 1590018 1590019 1590020 1590021 [...] (2336 flits)
Class 0:
Remaining flits: 1623834 1623835 1623836 1623837 1623838 1623839 1623840 1623841 1623842 1623843 [...] (17743 flits)
Measured flits: 1640970 1640971 1640972 1640973 1640974 1640975 1640976 1640977 1640978 1640979 [...] (2448 flits)
Class 0:
Remaining flits: 1643418 1643419 1643420 1643421 1643422 1643423 1643424 1643425 1643426 1643427 [...] (17808 flits)
Measured flits: 1656486 1656487 1656488 1656489 1656490 1656491 1656492 1656493 1656494 1656495 [...] (1954 flits)
Class 0:
Remaining flits: 1687536 1687537 1687538 1687539 1687540 1687541 1687542 1687543 1687544 1687545 [...] (17467 flits)
Measured flits: 1709964 1709965 1709966 1709967 1709968 1709969 1709970 1709971 1709972 1709973 [...] (1596 flits)
Class 0:
Remaining flits: 1701378 1701379 1701380 1701381 1701382 1701383 1701384 1701385 1701386 1701387 [...] (17778 flits)
Measured flits: 1744056 1744057 1744058 1744059 1744060 1744061 1744062 1744063 1744064 1744065 [...] (1055 flits)
Class 0:
Remaining flits: 1750392 1750393 1750394 1750395 1750396 1750397 1750398 1750399 1750400 1750401 [...] (17905 flits)
Measured flits: 1775196 1775197 1775198 1775199 1775200 1775201 1775202 1775203 1775204 1775205 [...] (860 flits)
Class 0:
Remaining flits: 1778040 1778041 1778042 1778043 1778044 1778045 1778046 1778047 1778048 1778049 [...] (17376 flits)
Measured flits: 1788480 1788481 1788482 1788483 1788484 1788485 1788486 1788487 1788488 1788489 [...] (523 flits)
Class 0:
Remaining flits: 1790406 1790407 1790408 1790409 1790410 1790411 1790412 1790413 1790414 1790415 [...] (17469 flits)
Measured flits: 1826460 1826461 1826462 1826463 1826464 1826465 1826466 1826467 1826468 1826469 [...] (564 flits)
Class 0:
Remaining flits: 1840266 1840267 1840268 1840269 1840270 1840271 1840272 1840273 1840274 1840275 [...] (17857 flits)
Measured flits: 1861218 1861219 1861220 1861221 1861222 1861223 1861224 1861225 1861226 1861227 [...] (423 flits)
Class 0:
Remaining flits: 1871442 1871443 1871444 1871445 1871446 1871447 1871448 1871449 1871450 1871451 [...] (17934 flits)
Measured flits: 1904760 1904761 1904762 1904763 1904764 1904765 1904766 1904767 1904768 1904769 [...] (288 flits)
Class 0:
Remaining flits: 1905210 1905211 1905212 1905213 1905214 1905215 1905216 1905217 1905218 1905219 [...] (17830 flits)
Measured flits: 1951012 1951013 1951014 1951015 1951016 1951017 1951018 1951019 1953648 1953649 [...] (234 flits)
Class 0:
Remaining flits: 1923516 1923517 1923518 1923519 1923520 1923521 1923522 1923523 1923524 1923525 [...] (17712 flits)
Measured flits: 1967382 1967383 1967384 1967385 1967386 1967387 1967388 1967389 1967390 1967391 [...] (198 flits)
Class 0:
Remaining flits: 1940116 1940117 1940118 1940119 1940120 1940121 1940122 1940123 1940124 1940125 [...] (17329 flits)
Measured flits: 2005254 2005255 2005256 2005257 2005258 2005259 2005260 2005261 2005262 2005263 [...] (198 flits)
Class 0:
Remaining flits: 1950984 1950985 1950986 1950987 1950988 1950989 1950990 1950991 1950992 1950993 [...] (17590 flits)
Measured flits: 2005812 2005813 2005814 2005815 2005816 2005817 2005818 2005819 2005820 2005821 [...] (144 flits)
Class 0:
Remaining flits: 2012436 2012437 2012438 2012439 2012440 2012441 2012442 2012443 2012444 2012445 [...] (17309 flits)
Measured flits: 2018052 2018053 2018054 2018055 2018056 2018057 2018058 2018059 2018060 2018061 [...] (126 flits)
Class 0:
Remaining flits: 2042532 2042533 2042534 2042535 2042536 2042537 2042538 2042539 2042540 2042541 [...] (17777 flits)
Measured flits: 2101752 2101753 2101754 2101755 2101756 2101757 2101758 2101759 2101760 2101761 [...] (90 flits)
Class 0:
Remaining flits: 2069802 2069803 2069804 2069805 2069806 2069807 2069808 2069809 2069810 2069811 [...] (17830 flits)
Measured flits: 2126604 2126605 2126606 2126607 2126608 2126609 2127204 2127205 2127206 2127207 [...] (96 flits)
Class 0:
Remaining flits: 2094408 2094409 2094410 2094411 2094412 2094413 2094414 2094415 2094416 2094417 [...] (17675 flits)
Measured flits: 2168523 2168524 2168525 2168526 2168527 2168528 2168529 2168530 2168531 2170450 [...] (107 flits)
Class 0:
Remaining flits: 2146662 2146663 2146664 2146665 2146666 2146667 2146668 2146669 2146670 2146671 [...] (17724 flits)
Measured flits: 2187450 2187451 2187452 2187453 2187454 2187455 2187456 2187457 2187458 2187459 [...] (108 flits)
Class 0:
Remaining flits: 2169468 2169469 2169470 2169471 2169472 2169473 2169474 2169475 2169476 2169477 [...] (17806 flits)
Measured flits: 2226366 2226367 2226368 2226369 2226370 2226371 2226372 2226373 2226374 2226375 [...] (90 flits)
Class 0:
Remaining flits: 2200451 2200452 2200453 2200454 2200455 2200456 2200457 2200458 2200459 2200460 [...] (17623 flits)
Measured flits: 2244366 2244367 2244368 2244369 2244370 2244371 2244372 2244373 2244374 2244375 [...] (72 flits)
Draining remaining packets ...
Time taken is 74235 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 18688.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 63552 (1 samples)
Network latency average = 551.459 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3835 (1 samples)
Flit latency average = 489.752 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3774 (1 samples)
Fragmentation average = 58.4637 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.00892708 (1 samples)
	minimum = 0.00557143 (1 samples)
	maximum = 0.0127143 (1 samples)
Accepted packet rate average = 0.00889807 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.160554 (1 samples)
	minimum = 0.0978571 (1 samples)
	maximum = 0.228857 (1 samples)
Accepted flit rate average = 0.160112 (1 samples)
	minimum = 0.112714 (1 samples)
	maximum = 0.218714 (1 samples)
Injected packet size average = 17.9851 (1 samples)
Accepted packet size average = 17.9941 (1 samples)
Hops average = 5.06963 (1 samples)
Total run time 55.0109
