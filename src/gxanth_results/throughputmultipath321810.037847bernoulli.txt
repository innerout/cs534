BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 361.274
	minimum = 23
	maximum = 950
Network latency average = 330.309
	minimum = 23
	maximum = 930
Slowest packet = 241
Flit latency average = 259.18
	minimum = 6
	maximum = 939
Slowest flit = 4658
Fragmentation average = 162.663
	minimum = 0
	maximum = 808
Injected packet rate average = 0.0220208
	minimum = 0.007 (at node 144)
	maximum = 0.033 (at node 49)
Accepted packet rate average = 0.0101458
	minimum = 0.003 (at node 127)
	maximum = 0.02 (at node 91)
Injected flit rate average = 0.389031
	minimum = 0.122 (at node 144)
	maximum = 0.591 (at node 49)
Accepted flit rate average= 0.208927
	minimum = 0.078 (at node 174)
	maximum = 0.382 (at node 76)
Injected packet length average = 17.6665
Accepted packet length average = 20.5924
Total in-flight flits = 36242 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 709.515
	minimum = 23
	maximum = 1904
Network latency average = 567.907
	minimum = 23
	maximum = 1884
Slowest packet = 699
Flit latency average = 463.823
	minimum = 6
	maximum = 1867
Slowest flit = 12599
Fragmentation average = 194.521
	minimum = 0
	maximum = 1687
Injected packet rate average = 0.0166276
	minimum = 0.006 (at node 144)
	maximum = 0.027 (at node 147)
Accepted packet rate average = 0.0106667
	minimum = 0.0055 (at node 43)
	maximum = 0.0185 (at node 22)
Injected flit rate average = 0.295758
	minimum = 0.1 (at node 188)
	maximum = 0.486 (at node 147)
Accepted flit rate average= 0.20382
	minimum = 0.099 (at node 43)
	maximum = 0.333 (at node 22)
Injected packet length average = 17.7872
Accepted packet length average = 19.1082
Total in-flight flits = 36951 (0 measured)
latency change    = 0.490816
throughput change = 0.0250553
Class 0:
Packet latency average = 1723.33
	minimum = 339
	maximum = 2740
Network latency average = 962.48
	minimum = 24
	maximum = 2739
Slowest packet = 1147
Flit latency average = 839.739
	minimum = 6
	maximum = 2809
Slowest flit = 19654
Fragmentation average = 213.473
	minimum = 0
	maximum = 2253
Injected packet rate average = 0.0107292
	minimum = 0 (at node 20)
	maximum = 0.031 (at node 154)
Accepted packet rate average = 0.010875
	minimum = 0.004 (at node 22)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.19424
	minimum = 0 (at node 45)
	maximum = 0.55 (at node 154)
Accepted flit rate average= 0.196932
	minimum = 0.066 (at node 190)
	maximum = 0.385 (at node 152)
Injected packet length average = 18.1039
Accepted packet length average = 18.1087
Total in-flight flits = 36382 (0 measured)
latency change    = 0.588289
throughput change = 0.0349766
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2400.15
	minimum = 1137
	maximum = 3455
Network latency average = 374.099
	minimum = 23
	maximum = 948
Slowest packet = 8477
Flit latency average = 897.5
	minimum = 6
	maximum = 3790
Slowest flit = 22278
Fragmentation average = 128.109
	minimum = 0
	maximum = 619
Injected packet rate average = 0.0105625
	minimum = 0 (at node 26)
	maximum = 0.029 (at node 127)
Accepted packet rate average = 0.0111094
	minimum = 0.002 (at node 113)
	maximum = 0.023 (at node 120)
Injected flit rate average = 0.189193
	minimum = 0 (at node 74)
	maximum = 0.524 (at node 127)
Accepted flit rate average= 0.197307
	minimum = 0.049 (at node 113)
	maximum = 0.385 (at node 120)
Injected packet length average = 17.9117
Accepted packet length average = 17.7604
Total in-flight flits = 34949 (23405 measured)
latency change    = 0.28199
throughput change = 0.00190059
Class 0:
Packet latency average = 2904.18
	minimum = 1137
	maximum = 4655
Network latency average = 612.052
	minimum = 23
	maximum = 1990
Slowest packet = 8477
Flit latency average = 913.781
	minimum = 6
	maximum = 4861
Slowest flit = 15052
Fragmentation average = 154.243
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0107708
	minimum = 0 (at node 77)
	maximum = 0.0225 (at node 105)
Accepted packet rate average = 0.0109297
	minimum = 0.006 (at node 1)
	maximum = 0.0185 (at node 129)
Injected flit rate average = 0.193576
	minimum = 0 (at node 168)
	maximum = 0.4025 (at node 105)
Accepted flit rate average= 0.19524
	minimum = 0.109 (at node 1)
	maximum = 0.328 (at node 129)
Injected packet length average = 17.9722
Accepted packet length average = 17.8632
Total in-flight flits = 35876 (32633 measured)
latency change    = 0.173555
throughput change = 0.0105906
Class 0:
Packet latency average = 3348.3
	minimum = 1137
	maximum = 5533
Network latency average = 735.32
	minimum = 23
	maximum = 2899
Slowest packet = 8477
Flit latency average = 903.428
	minimum = 6
	maximum = 5587
Slowest flit = 15065
Fragmentation average = 170.074
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0108559
	minimum = 0.000333333 (at node 38)
	maximum = 0.0223333 (at node 42)
Accepted packet rate average = 0.0108976
	minimum = 0.007 (at node 32)
	maximum = 0.016 (at node 129)
Injected flit rate average = 0.194934
	minimum = 0.00533333 (at node 38)
	maximum = 0.401333 (at node 42)
Accepted flit rate average= 0.194609
	minimum = 0.123333 (at node 36)
	maximum = 0.288 (at node 129)
Injected packet length average = 17.9565
Accepted packet length average = 17.8581
Total in-flight flits = 36787 (35705 measured)
latency change    = 0.13264
throughput change = 0.00323832
Draining remaining packets ...
Class 0:
Remaining flits: 51047 82133 89154 89155 89156 89157 89158 89159 89160 89161 [...] (6159 flits)
Measured flits: 154835 155721 155722 155723 155724 155725 155726 155727 155728 155729 [...] (6109 flits)
Time taken is 7550 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3826.72 (1 samples)
	minimum = 1137 (1 samples)
	maximum = 6518 (1 samples)
Network latency average = 969.349 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4066 (1 samples)
Flit latency average = 1021.18 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6560 (1 samples)
Fragmentation average = 173.332 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2776 (1 samples)
Injected packet rate average = 0.0108559 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0108976 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.194934 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.401333 (1 samples)
Accepted flit rate average = 0.194609 (1 samples)
	minimum = 0.123333 (1 samples)
	maximum = 0.288 (1 samples)
Injected packet size average = 17.9565 (1 samples)
Accepted packet size average = 17.8581 (1 samples)
Hops average = 5.12432 (1 samples)
Total run time 7.73118
