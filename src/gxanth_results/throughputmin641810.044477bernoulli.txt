BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.044477
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 389.193
	minimum = 27
	maximum = 943
Network latency average = 360.504
	minimum = 23
	maximum = 928
Slowest packet = 457
Flit latency average = 259.434
	minimum = 6
	maximum = 938
Slowest flit = 6213
Fragmentation average = 250.542
	minimum = 0
	maximum = 700
Injected packet rate average = 0.0305521
	minimum = 0.015 (at node 160)
	maximum = 0.041 (at node 133)
Accepted packet rate average = 0.010099
	minimum = 0.003 (at node 174)
	maximum = 0.018 (at node 136)
Injected flit rate average = 0.544255
	minimum = 0.266 (at node 160)
	maximum = 0.736 (at node 133)
Accepted flit rate average= 0.226615
	minimum = 0.082 (at node 174)
	maximum = 0.369 (at node 152)
Injected packet length average = 17.814
Accepted packet length average = 22.4394
Total in-flight flits = 62168 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 752.46
	minimum = 27
	maximum = 1914
Network latency average = 647.071
	minimum = 23
	maximum = 1902
Slowest packet = 335
Flit latency average = 518.397
	minimum = 6
	maximum = 1932
Slowest flit = 5859
Fragmentation average = 313.164
	minimum = 0
	maximum = 1356
Injected packet rate average = 0.0223724
	minimum = 0.009 (at node 184)
	maximum = 0.029 (at node 107)
Accepted packet rate average = 0.0112292
	minimum = 0.0065 (at node 4)
	maximum = 0.018 (at node 22)
Injected flit rate average = 0.399224
	minimum = 0.156 (at node 184)
	maximum = 0.5215 (at node 107)
Accepted flit rate average= 0.225031
	minimum = 0.1475 (at node 96)
	maximum = 0.348 (at node 152)
Injected packet length average = 17.8445
Accepted packet length average = 20.0399
Total in-flight flits = 68334 (0 measured)
latency change    = 0.482772
throughput change = 0.00703606
Class 0:
Packet latency average = 1706.68
	minimum = 149
	maximum = 2926
Network latency average = 1232.05
	minimum = 30
	maximum = 2926
Slowest packet = 503
Flit latency average = 1092.06
	minimum = 6
	maximum = 2909
Slowest flit = 9071
Fragmentation average = 329.99
	minimum = 1
	maximum = 2272
Injected packet rate average = 0.00969792
	minimum = 0 (at node 7)
	maximum = 0.035 (at node 127)
Accepted packet rate average = 0.0123542
	minimum = 0.004 (at node 74)
	maximum = 0.021 (at node 102)
Injected flit rate average = 0.173771
	minimum = 0 (at node 7)
	maximum = 0.626 (at node 127)
Accepted flit rate average= 0.218708
	minimum = 0.084 (at node 74)
	maximum = 0.366 (at node 124)
Injected packet length average = 17.9184
Accepted packet length average = 17.7032
Total in-flight flits = 60002 (0 measured)
latency change    = 0.559108
throughput change = 0.0289103
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2292.63
	minimum = 1010
	maximum = 3364
Network latency average = 347.979
	minimum = 25
	maximum = 942
Slowest packet = 10485
Flit latency average = 1217.69
	minimum = 6
	maximum = 3862
Slowest flit = 6065
Fragmentation average = 145.506
	minimum = 1
	maximum = 608
Injected packet rate average = 0.0100885
	minimum = 0 (at node 2)
	maximum = 0.041 (at node 23)
Accepted packet rate average = 0.0120677
	minimum = 0.004 (at node 36)
	maximum = 0.023 (at node 81)
Injected flit rate average = 0.181646
	minimum = 0 (at node 3)
	maximum = 0.743 (at node 23)
Accepted flit rate average= 0.209615
	minimum = 0.063 (at node 36)
	maximum = 0.386 (at node 81)
Injected packet length average = 18.0052
Accepted packet length average = 17.3699
Total in-flight flits = 54604 (23514 measured)
latency change    = 0.255581
throughput change = 0.0433832
Class 0:
Packet latency average = 2803.14
	minimum = 1010
	maximum = 4256
Network latency average = 524.483
	minimum = 25
	maximum = 1894
Slowest packet = 10485
Flit latency average = 1204.14
	minimum = 6
	maximum = 4821
Slowest flit = 19187
Fragmentation average = 176.269
	minimum = 0
	maximum = 1434
Injected packet rate average = 0.0105339
	minimum = 0 (at node 14)
	maximum = 0.0325 (at node 154)
Accepted packet rate average = 0.0117526
	minimum = 0.0055 (at node 36)
	maximum = 0.019 (at node 81)
Injected flit rate average = 0.189961
	minimum = 0 (at node 14)
	maximum = 0.5875 (at node 154)
Accepted flit rate average= 0.207849
	minimum = 0.107 (at node 36)
	maximum = 0.3205 (at node 81)
Injected packet length average = 18.0334
Accepted packet length average = 17.6854
Total in-flight flits = 52980 (35118 measured)
latency change    = 0.18212
throughput change = 0.00849475
Class 0:
Packet latency average = 3261.08
	minimum = 1010
	maximum = 5262
Network latency average = 676.618
	minimum = 25
	maximum = 2939
Slowest packet = 10485
Flit latency average = 1212.11
	minimum = 6
	maximum = 5931
Slowest flit = 7013
Fragmentation average = 198.157
	minimum = 0
	maximum = 1862
Injected packet rate average = 0.0106962
	minimum = 0 (at node 14)
	maximum = 0.03 (at node 51)
Accepted packet rate average = 0.0116458
	minimum = 0.00633333 (at node 36)
	maximum = 0.0166667 (at node 107)
Injected flit rate average = 0.192557
	minimum = 0 (at node 14)
	maximum = 0.539667 (at node 51)
Accepted flit rate average= 0.206948
	minimum = 0.114333 (at node 36)
	maximum = 0.302 (at node 107)
Injected packet length average = 18.0024
Accepted packet length average = 17.7701
Total in-flight flits = 51662 (40672 measured)
latency change    = 0.140426
throughput change = 0.00435395
Draining remaining packets ...
Class 0:
Remaining flits: 10237 10238 10239 10240 10241 17154 17155 17156 17157 17158 [...] (18425 flits)
Measured flits: 188640 188641 188642 188643 188644 188645 188646 188647 188648 188649 [...] (13726 flits)
Class 0:
Remaining flits: 52236 52237 52238 52239 52240 52241 52242 52243 52244 52245 [...] (589 flits)
Measured flits: 190152 190153 190154 190155 190156 190157 190158 190159 190160 190161 [...] (387 flits)
Time taken is 8336 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3931.41 (1 samples)
	minimum = 1010 (1 samples)
	maximum = 7260 (1 samples)
Network latency average = 1133.03 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5289 (1 samples)
Flit latency average = 1594.98 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7786 (1 samples)
Fragmentation average = 212.866 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3484 (1 samples)
Injected packet rate average = 0.0106962 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0116458 (1 samples)
	minimum = 0.00633333 (1 samples)
	maximum = 0.0166667 (1 samples)
Injected flit rate average = 0.192557 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.539667 (1 samples)
Accepted flit rate average = 0.206948 (1 samples)
	minimum = 0.114333 (1 samples)
	maximum = 0.302 (1 samples)
Injected packet size average = 18.0024 (1 samples)
Accepted packet size average = 17.7701 (1 samples)
Hops average = 5.13233 (1 samples)
Total run time 11.6125
