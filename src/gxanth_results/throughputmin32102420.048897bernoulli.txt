BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 327.267
	minimum = 25
	maximum = 933
Network latency average = 304.6
	minimum = 22
	maximum = 871
Slowest packet = 918
Flit latency average = 267.598
	minimum = 5
	maximum = 870
Slowest flit = 17874
Fragmentation average = 85.3913
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0464479
	minimum = 0.029 (at node 72)
	maximum = 0.056 (at node 39)
Accepted packet rate average = 0.0150938
	minimum = 0.002 (at node 174)
	maximum = 0.026 (at node 44)
Injected flit rate average = 0.827828
	minimum = 0.517 (at node 72)
	maximum = 0.999 (at node 68)
Accepted flit rate average= 0.293349
	minimum = 0.074 (at node 174)
	maximum = 0.471 (at node 44)
Injected packet length average = 17.8227
Accepted packet length average = 19.4351
Total in-flight flits = 104201 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.131
	minimum = 25
	maximum = 1804
Network latency average = 572.144
	minimum = 22
	maximum = 1708
Slowest packet = 993
Flit latency average = 528.539
	minimum = 5
	maximum = 1714
Slowest flit = 41943
Fragmentation average = 114.271
	minimum = 0
	maximum = 511
Injected packet rate average = 0.0475937
	minimum = 0.0365 (at node 72)
	maximum = 0.0555 (at node 32)
Accepted packet rate average = 0.0160729
	minimum = 0.0085 (at node 116)
	maximum = 0.024 (at node 70)
Injected flit rate average = 0.852776
	minimum = 0.6485 (at node 72)
	maximum = 0.996 (at node 39)
Accepted flit rate average= 0.302958
	minimum = 0.1605 (at node 116)
	maximum = 0.436 (at node 70)
Injected packet length average = 17.9178
Accepted packet length average = 18.849
Total in-flight flits = 212632 (0 measured)
latency change    = 0.457386
throughput change = 0.0317185
Class 0:
Packet latency average = 1396.02
	minimum = 24
	maximum = 2588
Network latency average = 1351.01
	minimum = 22
	maximum = 2532
Slowest packet = 3135
Flit latency average = 1316.97
	minimum = 5
	maximum = 2660
Slowest flit = 50370
Fragmentation average = 146.663
	minimum = 0
	maximum = 411
Injected packet rate average = 0.0485
	minimum = 0.034 (at node 2)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0174115
	minimum = 0.008 (at node 45)
	maximum = 0.03 (at node 159)
Injected flit rate average = 0.872458
	minimum = 0.608 (at node 2)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.313427
	minimum = 0.155 (at node 100)
	maximum = 0.561 (at node 78)
Injected packet length average = 17.9888
Accepted packet length average = 18.0012
Total in-flight flits = 320070 (0 measured)
latency change    = 0.567965
throughput change = 0.0334009
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 96.4771
	minimum = 23
	maximum = 476
Network latency average = 40.3935
	minimum = 22
	maximum = 265
Slowest packet = 27623
Flit latency average = 1847.13
	minimum = 5
	maximum = 3282
Slowest flit = 73889
Fragmentation average = 6.36253
	minimum = 0
	maximum = 46
Injected packet rate average = 0.0492031
	minimum = 0.031 (at node 153)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0175625
	minimum = 0.008 (at node 35)
	maximum = 0.031 (at node 44)
Injected flit rate average = 0.886292
	minimum = 0.541 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.317115
	minimum = 0.125 (at node 48)
	maximum = 0.568 (at node 129)
Injected packet length average = 18.0129
Accepted packet length average = 18.0563
Total in-flight flits = 429230 (156497 measured)
latency change    = 13.47
throughput change = 0.0116283
Class 0:
Packet latency average = 99.2061
	minimum = 23
	maximum = 476
Network latency average = 39.9639
	minimum = 22
	maximum = 265
Slowest packet = 27623
Flit latency average = 2130.58
	minimum = 5
	maximum = 4023
Slowest flit = 152724
Fragmentation average = 6.52041
	minimum = 0
	maximum = 58
Injected packet rate average = 0.0489766
	minimum = 0.0365 (at node 153)
	maximum = 0.056 (at node 164)
Accepted packet rate average = 0.0174141
	minimum = 0.01 (at node 48)
	maximum = 0.0275 (at node 129)
Injected flit rate average = 0.882107
	minimum = 0.657 (at node 153)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.313786
	minimum = 0.184 (at node 48)
	maximum = 0.5045 (at node 129)
Injected packet length average = 18.0108
Accepted packet length average = 18.0191
Total in-flight flits = 538102 (311912 measured)
latency change    = 0.0275087
throughput change = 0.0106063
Class 0:
Packet latency average = 101.293
	minimum = 22
	maximum = 476
Network latency average = 40.6722
	minimum = 22
	maximum = 366
Slowest packet = 27623
Flit latency average = 2399.65
	minimum = 5
	maximum = 4877
Slowest flit = 176435
Fragmentation average = 6.51993
	minimum = 0
	maximum = 58
Injected packet rate average = 0.0488854
	minimum = 0.0406667 (at node 152)
	maximum = 0.0556667 (at node 44)
Accepted packet rate average = 0.0174844
	minimum = 0.0123333 (at node 48)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.88021
	minimum = 0.733333 (at node 152)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.314134
	minimum = 0.221667 (at node 132)
	maximum = 0.442333 (at node 129)
Injected packet length average = 18.0056
Accepted packet length average = 17.9665
Total in-flight flits = 645973 (466588 measured)
latency change    = 0.0206056
throughput change = 0.00110533
Draining remaining packets ...
Class 0:
Remaining flits: 190206 190207 190208 190209 190210 190211 190212 190213 190214 190215 [...] (599005 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (466353 flits)
Class 0:
Remaining flits: 261031 261032 261033 261034 261035 270089 273186 273187 273188 273189 [...] (551730 flits)
Measured flits: 496584 496585 496586 496587 496588 496589 496590 496591 496592 496593 [...] (465364 flits)
Class 0:
Remaining flits: 292122 292123 292124 292125 292126 292127 292128 292129 292130 292131 [...] (504693 flits)
Measured flits: 496620 496621 496622 496623 496624 496625 496626 496627 496628 496629 [...] (458778 flits)
Class 0:
Remaining flits: 353898 353899 353900 353901 353902 353903 353904 353905 353906 353907 [...] (457615 flits)
Measured flits: 496698 496699 496700 496701 496702 496703 496704 496705 496706 496707 [...] (439985 flits)
Class 0:
Remaining flits: 353904 353905 353906 353907 353908 353909 353910 353911 353912 353913 [...] (410579 flits)
Measured flits: 496722 496723 496724 496725 496726 496727 496728 496729 496730 496731 [...] (405932 flits)
Class 0:
Remaining flits: 430290 430291 430292 430293 430294 430295 430296 430297 430298 430299 [...] (363477 flits)
Measured flits: 496926 496927 496928 496929 496930 496931 496932 496933 496934 496935 [...] (362886 flits)
Class 0:
Remaining flits: 472986 472987 472988 472989 472990 472991 472992 472993 472994 472995 [...] (316533 flits)
Measured flits: 502596 502597 502598 502599 502600 502601 502602 502603 502604 502605 [...] (316461 flits)
Class 0:
Remaining flits: 478811 478812 478813 478814 478815 478816 478817 529902 529903 529904 [...] (269195 flits)
Measured flits: 529902 529903 529904 529905 529906 529907 529908 529909 529910 529911 [...] (269188 flits)
Class 0:
Remaining flits: 548838 548839 548840 548841 548842 548843 548844 548845 548846 548847 [...] (221802 flits)
Measured flits: 548838 548839 548840 548841 548842 548843 548844 548845 548846 548847 [...] (221802 flits)
Class 0:
Remaining flits: 620828 620829 620830 620831 620832 620833 620834 620835 620836 620837 [...] (174363 flits)
Measured flits: 620828 620829 620830 620831 620832 620833 620834 620835 620836 620837 [...] (174363 flits)
Class 0:
Remaining flits: 658115 658746 658747 658748 658749 658750 658751 658752 658753 658754 [...] (127519 flits)
Measured flits: 658115 658746 658747 658748 658749 658750 658751 658752 658753 658754 [...] (127519 flits)
Class 0:
Remaining flits: 664806 664807 664808 664809 664810 664811 696880 696881 696882 696883 [...] (80493 flits)
Measured flits: 664806 664807 664808 664809 664810 664811 696880 696881 696882 696883 [...] (80493 flits)
Class 0:
Remaining flits: 740066 740067 740068 740069 748710 748711 748712 748713 748714 748715 [...] (35297 flits)
Measured flits: 740066 740067 740068 740069 748710 748711 748712 748713 748714 748715 [...] (35297 flits)
Class 0:
Remaining flits: 816392 816393 816394 816395 816396 816397 816398 816399 816400 816401 [...] (4224 flits)
Measured flits: 816392 816393 816394 816395 816396 816397 816398 816399 816400 816401 [...] (4224 flits)
Class 0:
Remaining flits: 969560 969561 969562 969563 969564 969565 969566 969567 969568 969569 [...] (28 flits)
Measured flits: 969560 969561 969562 969563 969564 969565 969566 969567 969568 969569 [...] (28 flits)
Time taken is 21039 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9554.08 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15761 (1 samples)
Network latency average = 9492.58 (1 samples)
	minimum = 22 (1 samples)
	maximum = 15584 (1 samples)
Flit latency average = 7513.03 (1 samples)
	minimum = 5 (1 samples)
	maximum = 15567 (1 samples)
Fragmentation average = 162.885 (1 samples)
	minimum = 0 (1 samples)
	maximum = 474 (1 samples)
Injected packet rate average = 0.0488854 (1 samples)
	minimum = 0.0406667 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0174844 (1 samples)
	minimum = 0.0123333 (1 samples)
	maximum = 0.024 (1 samples)
Injected flit rate average = 0.88021 (1 samples)
	minimum = 0.733333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.314134 (1 samples)
	minimum = 0.221667 (1 samples)
	maximum = 0.442333 (1 samples)
Injected packet size average = 18.0056 (1 samples)
Accepted packet size average = 17.9665 (1 samples)
Hops average = 5.07373 (1 samples)
Total run time 20.649
