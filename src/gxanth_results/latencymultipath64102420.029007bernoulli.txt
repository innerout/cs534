BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 271.444
	minimum = 22
	maximum = 746
Network latency average = 263.446
	minimum = 22
	maximum = 727
Slowest packet = 952
Flit latency average = 233.25
	minimum = 5
	maximum = 732
Slowest flit = 24240
Fragmentation average = 49.4692
	minimum = 0
	maximum = 535
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0137865
	minimum = 0.005 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.261542
	minimum = 0.095 (at node 174)
	maximum = 0.439 (at node 44)
Injected packet length average = 17.8589
Accepted packet length average = 18.9709
Total in-flight flits = 49432 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 498.978
	minimum = 22
	maximum = 1427
Network latency average = 490.172
	minimum = 22
	maximum = 1397
Slowest packet = 2551
Flit latency average = 455.289
	minimum = 5
	maximum = 1470
Slowest flit = 51410
Fragmentation average = 62.9534
	minimum = 0
	maximum = 674
Injected packet rate average = 0.0287995
	minimum = 0.0195 (at node 64)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0147083
	minimum = 0.0085 (at node 153)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.516167
	minimum = 0.351 (at node 64)
	maximum = 0.675 (at node 191)
Accepted flit rate average= 0.272073
	minimum = 0.16 (at node 153)
	maximum = 0.402 (at node 152)
Injected packet length average = 17.9228
Accepted packet length average = 18.4979
Total in-flight flits = 94586 (0 measured)
latency change    = 0.456001
throughput change = 0.0387075
Class 0:
Packet latency average = 1119.06
	minimum = 23
	maximum = 2051
Network latency average = 1110.27
	minimum = 22
	maximum = 2051
Slowest packet = 5037
Flit latency average = 1076.38
	minimum = 5
	maximum = 2054
Slowest flit = 92185
Fragmentation average = 87.1353
	minimum = 0
	maximum = 667
Injected packet rate average = 0.0287708
	minimum = 0.016 (at node 65)
	maximum = 0.045 (at node 69)
Accepted packet rate average = 0.0158229
	minimum = 0.007 (at node 113)
	maximum = 0.027 (at node 34)
Injected flit rate average = 0.518208
	minimum = 0.288 (at node 65)
	maximum = 0.818 (at node 69)
Accepted flit rate average= 0.287089
	minimum = 0.119 (at node 113)
	maximum = 0.502 (at node 34)
Injected packet length average = 18.0116
Accepted packet length average = 18.1438
Total in-flight flits = 138897 (0 measured)
latency change    = 0.554109
throughput change = 0.0523031
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 45.2352
	minimum = 22
	maximum = 134
Network latency average = 35.3055
	minimum = 22
	maximum = 64
Slowest packet = 16634
Flit latency average = 1479.81
	minimum = 5
	maximum = 2806
Slowest flit = 99989
Fragmentation average = 6.74945
	minimum = 0
	maximum = 39
Injected packet rate average = 0.029625
	minimum = 0.013 (at node 119)
	maximum = 0.045 (at node 150)
Accepted packet rate average = 0.0161719
	minimum = 0.007 (at node 17)
	maximum = 0.027 (at node 78)
Injected flit rate average = 0.532229
	minimum = 0.234 (at node 119)
	maximum = 0.815 (at node 150)
Accepted flit rate average= 0.289458
	minimum = 0.139 (at node 162)
	maximum = 0.46 (at node 16)
Injected packet length average = 17.9655
Accepted packet length average = 17.8989
Total in-flight flits = 185705 (94057 measured)
latency change    = 23.7387
throughput change = 0.00818699
Class 0:
Packet latency average = 48.7194
	minimum = 22
	maximum = 2000
Network latency average = 39.2716
	minimum = 22
	maximum = 1978
Slowest packet = 16654
Flit latency average = 1694.18
	minimum = 5
	maximum = 3467
Slowest flit = 148959
Fragmentation average = 6.9147
	minimum = 0
	maximum = 301
Injected packet rate average = 0.0291536
	minimum = 0.0185 (at node 23)
	maximum = 0.0385 (at node 13)
Accepted packet rate average = 0.0158281
	minimum = 0.009 (at node 36)
	maximum = 0.024 (at node 128)
Injected flit rate average = 0.524307
	minimum = 0.333 (at node 109)
	maximum = 0.689 (at node 22)
Accepted flit rate average= 0.285846
	minimum = 0.1615 (at node 88)
	maximum = 0.44 (at node 128)
Injected packet length average = 17.9843
Accepted packet length average = 18.0594
Total in-flight flits = 230642 (185334 measured)
latency change    = 0.0715167
throughput change = 0.0126361
Class 0:
Packet latency average = 604.422
	minimum = 22
	maximum = 2987
Network latency average = 595.411
	minimum = 22
	maximum = 2986
Slowest packet = 16584
Flit latency average = 1900.03
	minimum = 5
	maximum = 4211
Slowest flit = 176953
Fragmentation average = 22.8302
	minimum = 0
	maximum = 395
Injected packet rate average = 0.0291406
	minimum = 0.0196667 (at node 115)
	maximum = 0.0373333 (at node 136)
Accepted packet rate average = 0.015908
	minimum = 0.00966667 (at node 88)
	maximum = 0.0223333 (at node 128)
Injected flit rate average = 0.524429
	minimum = 0.354 (at node 115)
	maximum = 0.672 (at node 136)
Accepted flit rate average= 0.286918
	minimum = 0.179 (at node 88)
	maximum = 0.403667 (at node 128)
Injected packet length average = 17.9965
Accepted packet length average = 18.0361
Total in-flight flits = 275762 (269151 measured)
latency change    = 0.919395
throughput change = 0.00373642
Class 0:
Packet latency average = 1844.11
	minimum = 22
	maximum = 4009
Network latency average = 1834.94
	minimum = 22
	maximum = 3991
Slowest packet = 16702
Flit latency average = 2108.15
	minimum = 5
	maximum = 5078
Slowest flit = 180467
Fragmentation average = 68.1771
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0291667
	minimum = 0.023 (at node 115)
	maximum = 0.0345 (at node 186)
Accepted packet rate average = 0.0159297
	minimum = 0.011 (at node 36)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.524824
	minimum = 0.4105 (at node 115)
	maximum = 0.62425 (at node 186)
Accepted flit rate average= 0.287535
	minimum = 0.20475 (at node 162)
	maximum = 0.37525 (at node 128)
Injected packet length average = 17.994
Accepted packet length average = 18.0503
Total in-flight flits = 321270 (320852 measured)
latency change    = 0.672242
throughput change = 0.00214497
Class 0:
Packet latency average = 2383.96
	minimum = 22
	maximum = 4942
Network latency average = 2374.42
	minimum = 22
	maximum = 4879
Slowest packet = 17083
Flit latency average = 2313.66
	minimum = 5
	maximum = 5181
Slowest flit = 274265
Fragmentation average = 90.7804
	minimum = 0
	maximum = 820
Injected packet rate average = 0.029251
	minimum = 0.023 (at node 127)
	maximum = 0.035 (at node 71)
Accepted packet rate average = 0.0159385
	minimum = 0.0122 (at node 42)
	maximum = 0.0198 (at node 103)
Injected flit rate average = 0.526481
	minimum = 0.414 (at node 127)
	maximum = 0.63 (at node 71)
Accepted flit rate average= 0.287943
	minimum = 0.2148 (at node 42)
	maximum = 0.3584 (at node 128)
Injected packet length average = 17.9987
Accepted packet length average = 18.0658
Total in-flight flits = 367930 (367924 measured)
latency change    = 0.226452
throughput change = 0.00141539
Class 0:
Packet latency average = 2748.12
	minimum = 22
	maximum = 5731
Network latency average = 2738.61
	minimum = 22
	maximum = 5706
Slowest packet = 18160
Flit latency average = 2523.63
	minimum = 5
	maximum = 5689
Slowest flit = 326897
Fragmentation average = 104.623
	minimum = 0
	maximum = 820
Injected packet rate average = 0.0291953
	minimum = 0.0238333 (at node 127)
	maximum = 0.0341667 (at node 107)
Accepted packet rate average = 0.0159575
	minimum = 0.0126667 (at node 92)
	maximum = 0.0198333 (at node 138)
Injected flit rate average = 0.525501
	minimum = 0.428333 (at node 127)
	maximum = 0.6145 (at node 107)
Accepted flit rate average= 0.288232
	minimum = 0.231833 (at node 42)
	maximum = 0.3565 (at node 138)
Injected packet length average = 17.9995
Accepted packet length average = 18.0625
Total in-flight flits = 412248 (412248 measured)
latency change    = 0.132512
throughput change = 0.00100288
Class 0:
Packet latency average = 3054.15
	minimum = 22
	maximum = 6263
Network latency average = 3044.85
	minimum = 22
	maximum = 6246
Slowest packet = 19215
Flit latency average = 2737.5
	minimum = 5
	maximum = 6229
Slowest flit = 345887
Fragmentation average = 113.22
	minimum = 0
	maximum = 820
Injected packet rate average = 0.0291362
	minimum = 0.0241429 (at node 127)
	maximum = 0.0351429 (at node 107)
Accepted packet rate average = 0.015968
	minimum = 0.0127143 (at node 67)
	maximum = 0.0201429 (at node 138)
Injected flit rate average = 0.524373
	minimum = 0.433143 (at node 127)
	maximum = 0.630571 (at node 107)
Accepted flit rate average= 0.28792
	minimum = 0.228571 (at node 102)
	maximum = 0.359714 (at node 138)
Injected packet length average = 17.9973
Accepted packet length average = 18.0311
Total in-flight flits = 456794 (456794 measured)
latency change    = 0.100201
throughput change = 0.00108149
Draining all recorded packets ...
Class 0:
Remaining flits: 412852 412853 412854 412855 412856 412857 412858 412859 412860 412861 [...] (503224 flits)
Measured flits: 412852 412853 412854 412855 412856 412857 412858 412859 412860 412861 [...] (410860 flits)
Class 0:
Remaining flits: 461368 461369 461370 461371 461372 461373 461374 461375 478353 478354 [...] (546103 flits)
Measured flits: 461368 461369 461370 461371 461372 461373 461374 461375 478353 478354 [...] (363403 flits)
Class 0:
Remaining flits: 503559 503560 503561 503562 503563 503564 503565 503566 503567 516403 [...] (591352 flits)
Measured flits: 503559 503560 503561 503562 503563 503564 503565 503566 503567 516403 [...] (315760 flits)
Class 0:
Remaining flits: 523673 541281 541282 541283 541284 541285 541286 541287 541288 541289 [...] (637602 flits)
Measured flits: 523673 541281 541282 541283 541284 541285 541286 541287 541288 541289 [...] (268298 flits)
Class 0:
Remaining flits: 585719 609642 609643 609644 609645 609646 609647 609648 609649 609650 [...] (682208 flits)
Measured flits: 585719 609642 609643 609644 609645 609646 609647 609648 609649 609650 [...] (221035 flits)
Class 0:
Remaining flits: 673488 673489 673490 673491 673492 673493 673494 673495 673496 673497 [...] (730102 flits)
Measured flits: 673488 673489 673490 673491 673492 673493 673494 673495 673496 673497 [...] (173912 flits)
Class 0:
Remaining flits: 694241 705398 705399 705400 705401 718020 718021 718022 718023 718024 [...] (774183 flits)
Measured flits: 694241 705398 705399 705400 705401 718020 718021 718022 718023 718024 [...] (126641 flits)
Class 0:
Remaining flits: 733821 733822 733823 737764 737765 752382 752383 752384 752385 752386 [...] (818607 flits)
Measured flits: 733821 733822 733823 737764 737765 752382 752383 752384 752385 752386 [...] (79060 flits)
Class 0:
Remaining flits: 786293 830802 830803 830804 830805 830806 830807 835117 835118 835119 [...] (862400 flits)
Measured flits: 786293 830802 830803 830804 830805 830806 830807 835117 835118 835119 [...] (33812 flits)
Class 0:
Remaining flits: 874332 874333 874334 874335 874336 874337 874338 874339 874340 874341 [...] (907591 flits)
Measured flits: 874332 874333 874334 874335 874336 874337 874338 874339 874340 874341 [...] (7100 flits)
Class 0:
Remaining flits: 894294 894295 894296 894297 894298 894299 894300 894301 894302 894303 [...] (951046 flits)
Measured flits: 894294 894295 894296 894297 894298 894299 894300 894301 894302 894303 [...] (721 flits)
Class 0:
Remaining flits: 947979 947980 947981 947982 947983 947984 947985 947986 947987 953098 [...] (995060 flits)
Measured flits: 947979 947980 947981 947982 947983 947984 947985 947986 947987 953098 [...] (46 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1044120 1044121 1044122 1044123 1044124 1044125 1054620 1054621 1054622 1054623 [...] (971116 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1086408 1086409 1086410 1086411 1086412 1086413 1086414 1086415 1086416 1086417 [...] (923563 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1086418 1086419 1086420 1086421 1086422 1086423 1086424 1086425 1111067 1124226 [...] (876217 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1124233 1124234 1124235 1124236 1124237 1124238 1124239 1124240 1124241 1124242 [...] (828708 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1173312 1173313 1173314 1173315 1173316 1173317 1173318 1173319 1173320 1173321 [...] (781143 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270584 1270585 1270586 1270587 1270588 1270589 1270590 1270591 1270592 1270593 [...] (734286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270593 1270594 1270595 1270596 1270597 1270598 1270599 1270600 1270601 1314376 [...] (686750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426211 1431088 1431089 1431763 1431764 1431765 1431766 1431767 1431768 1431769 [...] (639131 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1455480 1455481 1455482 1455483 1455484 1455485 1455486 1455487 1455488 1455489 [...] (591618 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1463300 1463301 1463302 1463303 1463304 1463305 1463306 1463307 1463308 1463309 [...] (544450 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1504161 1504162 1504163 1504164 1504165 1504166 1504167 1504168 1504169 1535025 [...] (496994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1590233 1590234 1590235 1590236 1590237 1590238 1590239 1590240 1590241 1590242 [...] (449538 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1640533 1640534 1640535 1640536 1640537 1642469 1642470 1642471 1642472 1642473 [...] (401972 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1705122 1705123 1705124 1705125 1705126 1705127 1705128 1705129 1705130 1705131 [...] (354774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1705136 1705137 1705138 1705139 1744910 1744911 1744912 1744913 1744914 1744915 [...] (307200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1793583 1793584 1793585 1793586 1793587 1793588 1793589 1793590 1793591 1809502 [...] (259568 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1838817 1838818 1838819 1838820 1838821 1838822 1838823 1838824 1838825 1853298 [...] (211977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1890171 1890172 1890173 1890174 1890175 1890176 1890177 1890178 1890179 1911005 [...] (164449 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1942239 1942240 1942241 1942242 1942243 1942244 1942245 1942246 1942247 1942248 [...] (116792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1963687 1963688 1963689 1963690 1963691 1973196 1973197 1973198 1973199 1973200 [...] (69452 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2037546 2037547 2037548 2037549 2037550 2037551 2037552 2037553 2037554 2037555 [...] (26112 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2104827 2104828 2104829 2108609 2114980 2114981 2122146 2122147 2122148 2122149 [...] (5430 flits)
Measured flits: (0 flits)
Time taken is 45520 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5912.11 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12727 (1 samples)
Network latency average = 5902.64 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12727 (1 samples)
Flit latency average = 10650.8 (1 samples)
	minimum = 5 (1 samples)
	maximum = 24167 (1 samples)
Fragmentation average = 159.244 (1 samples)
	minimum = 0 (1 samples)
	maximum = 863 (1 samples)
Injected packet rate average = 0.0291362 (1 samples)
	minimum = 0.0241429 (1 samples)
	maximum = 0.0351429 (1 samples)
Accepted packet rate average = 0.015968 (1 samples)
	minimum = 0.0127143 (1 samples)
	maximum = 0.0201429 (1 samples)
Injected flit rate average = 0.524373 (1 samples)
	minimum = 0.433143 (1 samples)
	maximum = 0.630571 (1 samples)
Accepted flit rate average = 0.28792 (1 samples)
	minimum = 0.228571 (1 samples)
	maximum = 0.359714 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 18.0311 (1 samples)
Hops average = 5.07298 (1 samples)
Total run time 51.277
