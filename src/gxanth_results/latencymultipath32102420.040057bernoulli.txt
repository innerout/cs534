BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 308.419
	minimum = 22
	maximum = 850
Network latency average = 293.447
	minimum = 22
	maximum = 797
Slowest packet = 1047
Flit latency average = 259.648
	minimum = 5
	maximum = 824
Slowest flit = 20531
Fragmentation average = 73.4197
	minimum = 0
	maximum = 454
Injected packet rate average = 0.0391458
	minimum = 0.026 (at node 68)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0146198
	minimum = 0.005 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.698708
	minimum = 0.452 (at node 68)
	maximum = 0.921 (at node 62)
Accepted flit rate average= 0.281995
	minimum = 0.105 (at node 174)
	maximum = 0.487 (at node 44)
Injected packet length average = 17.8489
Accepted packet length average = 19.2886
Total in-flight flits = 81145 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 564.486
	minimum = 22
	maximum = 1678
Network latency average = 546.644
	minimum = 22
	maximum = 1651
Slowest packet = 2363
Flit latency average = 507.394
	minimum = 5
	maximum = 1701
Slowest flit = 37077
Fragmentation average = 101.037
	minimum = 0
	maximum = 454
Injected packet rate average = 0.0397656
	minimum = 0.0285 (at node 88)
	maximum = 0.0485 (at node 100)
Accepted packet rate average = 0.0155964
	minimum = 0.0085 (at node 153)
	maximum = 0.0225 (at node 63)
Injected flit rate average = 0.712951
	minimum = 0.5055 (at node 88)
	maximum = 0.8645 (at node 100)
Accepted flit rate average= 0.293461
	minimum = 0.172 (at node 116)
	maximum = 0.4115 (at node 63)
Injected packet length average = 17.9288
Accepted packet length average = 18.816
Total in-flight flits = 162171 (0 measured)
latency change    = 0.45363
throughput change = 0.0390721
Class 0:
Packet latency average = 1332.75
	minimum = 22
	maximum = 2477
Network latency average = 1311.3
	minimum = 22
	maximum = 2428
Slowest packet = 3860
Flit latency average = 1265.84
	minimum = 5
	maximum = 2466
Slowest flit = 68307
Fragmentation average = 148.314
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0396615
	minimum = 0.026 (at node 91)
	maximum = 0.054 (at node 36)
Accepted packet rate average = 0.016651
	minimum = 0.008 (at node 42)
	maximum = 0.03 (at node 137)
Injected flit rate average = 0.713333
	minimum = 0.465 (at node 91)
	maximum = 0.969 (at node 139)
Accepted flit rate average= 0.30074
	minimum = 0.163 (at node 17)
	maximum = 0.523 (at node 16)
Injected packet length average = 17.9856
Accepted packet length average = 18.0613
Total in-flight flits = 241499 (0 measured)
latency change    = 0.576449
throughput change = 0.0242025
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 59.6306
	minimum = 23
	maximum = 269
Network latency average = 39.1385
	minimum = 22
	maximum = 269
Slowest packet = 27104
Flit latency average = 1754.5
	minimum = 5
	maximum = 3250
Slowest flit = 98314
Fragmentation average = 6.30096
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0406562
	minimum = 0.027 (at node 70)
	maximum = 0.056 (at node 66)
Accepted packet rate average = 0.0169427
	minimum = 0.008 (at node 4)
	maximum = 0.028 (at node 175)
Injected flit rate average = 0.732083
	minimum = 0.478 (at node 70)
	maximum = 1 (at node 66)
Accepted flit rate average= 0.305016
	minimum = 0.134 (at node 36)
	maximum = 0.505 (at node 90)
Injected packet length average = 18.0067
Accepted packet length average = 18.0028
Total in-flight flits = 323444 (129062 measured)
latency change    = 21.3501
throughput change = 0.0140191
Class 0:
Packet latency average = 60.4205
	minimum = 22
	maximum = 275
Network latency average = 38.8435
	minimum = 22
	maximum = 269
Slowest packet = 27104
Flit latency average = 2025.4
	minimum = 5
	maximum = 3856
Slowest flit = 143279
Fragmentation average = 6.04645
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0401901
	minimum = 0.0295 (at node 92)
	maximum = 0.051 (at node 117)
Accepted packet rate average = 0.0167891
	minimum = 0.01 (at node 36)
	maximum = 0.0245 (at node 90)
Injected flit rate average = 0.723805
	minimum = 0.532 (at node 92)
	maximum = 0.923 (at node 117)
Accepted flit rate average= 0.30262
	minimum = 0.1875 (at node 36)
	maximum = 0.4355 (at node 90)
Injected packet length average = 18.0095
Accepted packet length average = 18.0248
Total in-flight flits = 403087 (255566 measured)
latency change    = 0.0130744
throughput change = 0.00791698
Class 0:
Packet latency average = 60.7436
	minimum = 22
	maximum = 305
Network latency average = 39.0623
	minimum = 22
	maximum = 290
Slowest packet = 42161
Flit latency average = 2284.2
	minimum = 5
	maximum = 4638
Slowest flit = 172529
Fragmentation average = 5.87315
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0401163
	minimum = 0.0286667 (at node 92)
	maximum = 0.0493333 (at node 117)
Accepted packet rate average = 0.0167587
	minimum = 0.0103333 (at node 64)
	maximum = 0.0223333 (at node 66)
Injected flit rate average = 0.722292
	minimum = 0.514667 (at node 92)
	maximum = 0.886667 (at node 117)
Accepted flit rate average= 0.301741
	minimum = 0.188667 (at node 64)
	maximum = 0.391667 (at node 66)
Injected packet length average = 18.0049
Accepted packet length average = 18.0051
Total in-flight flits = 483622 (382779 measured)
latency change    = 0.00531806
throughput change = 0.00291134
Class 0:
Packet latency average = 142.987
	minimum = 22
	maximum = 3995
Network latency average = 120.967
	minimum = 22
	maximum = 3979
Slowest packet = 22950
Flit latency average = 2538.5
	minimum = 5
	maximum = 5413
Slowest flit = 214913
Fragmentation average = 9.7259
	minimum = 0
	maximum = 279
Injected packet rate average = 0.0401615
	minimum = 0.032 (at node 92)
	maximum = 0.0495 (at node 104)
Accepted packet rate average = 0.0167813
	minimum = 0.01075 (at node 64)
	maximum = 0.02275 (at node 128)
Injected flit rate average = 0.722917
	minimum = 0.57475 (at node 92)
	maximum = 0.891 (at node 104)
Accepted flit rate average= 0.302409
	minimum = 0.1935 (at node 64)
	maximum = 0.40825 (at node 128)
Injected packet length average = 18.0003
Accepted packet length average = 18.0206
Total in-flight flits = 564441 (509212 measured)
latency change    = 0.57518
throughput change = 0.00220739
Class 0:
Packet latency average = 838.908
	minimum = 22
	maximum = 5016
Network latency average = 817.543
	minimum = 22
	maximum = 4993
Slowest packet = 22892
Flit latency average = 2785.74
	minimum = 5
	maximum = 6013
Slowest flit = 270246
Fragmentation average = 38.1094
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0402271
	minimum = 0.0334 (at node 80)
	maximum = 0.0476 (at node 104)
Accepted packet rate average = 0.0168406
	minimum = 0.012 (at node 171)
	maximum = 0.0222 (at node 128)
Injected flit rate average = 0.724119
	minimum = 0.6012 (at node 80)
	maximum = 0.8568 (at node 104)
Accepted flit rate average= 0.303383
	minimum = 0.2098 (at node 171)
	maximum = 0.3994 (at node 128)
Injected packet length average = 18.0008
Accepted packet length average = 18.015
Total in-flight flits = 645375 (624176 measured)
latency change    = 0.829556
throughput change = 0.00321204
Class 0:
Packet latency average = 1905.97
	minimum = 22
	maximum = 5997
Network latency average = 1885.05
	minimum = 22
	maximum = 5951
Slowest packet = 22989
Flit latency average = 3035.79
	minimum = 5
	maximum = 6643
Slowest flit = 320539
Fragmentation average = 76.037
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0402257
	minimum = 0.0346667 (at node 92)
	maximum = 0.0475 (at node 12)
Accepted packet rate average = 0.0168646
	minimum = 0.0121667 (at node 171)
	maximum = 0.0213333 (at node 128)
Injected flit rate average = 0.724063
	minimum = 0.622833 (at node 92)
	maximum = 0.855333 (at node 12)
Accepted flit rate average= 0.303907
	minimum = 0.217667 (at node 171)
	maximum = 0.382667 (at node 128)
Injected packet length average = 18
Accepted packet length average = 18.0204
Total in-flight flits = 725518 (720845 measured)
latency change    = 0.559852
throughput change = 0.0017235
Class 0:
Packet latency average = 2845.14
	minimum = 22
	maximum = 7084
Network latency average = 2824.04
	minimum = 22
	maximum = 6970
Slowest packet = 23066
Flit latency average = 3294.01
	minimum = 5
	maximum = 7369
Slowest flit = 358663
Fragmentation average = 101.101
	minimum = 0
	maximum = 437
Injected packet rate average = 0.0401332
	minimum = 0.0347143 (at node 113)
	maximum = 0.0471429 (at node 12)
Accepted packet rate average = 0.0168802
	minimum = 0.0128571 (at node 171)
	maximum = 0.0205714 (at node 40)
Injected flit rate average = 0.722361
	minimum = 0.624286 (at node 113)
	maximum = 0.849143 (at node 12)
Accepted flit rate average= 0.304014
	minimum = 0.227714 (at node 171)
	maximum = 0.368571 (at node 128)
Injected packet length average = 17.9991
Accepted packet length average = 18.0101
Total in-flight flits = 803806 (803274 measured)
latency change    = 0.330096
throughput change = 0.000352019
Draining all recorded packets ...
Class 0:
Remaining flits: 384012 384013 384014 384015 384016 384017 384018 384019 384020 384021 [...] (883149 flits)
Measured flits: 425425 425426 425427 425428 425429 433616 433617 433618 433619 435531 [...] (758993 flits)
Class 0:
Remaining flits: 472752 472753 472754 472755 472756 472757 472758 472759 472760 472761 [...] (958972 flits)
Measured flits: 472752 472753 472754 472755 472756 472757 472758 472759 472760 472761 [...] (711812 flits)
Class 0:
Remaining flits: 497986 497987 500544 500545 500546 500547 500548 500549 500550 500551 [...] (1040127 flits)
Measured flits: 497986 497987 500544 500545 500546 500547 500548 500549 500550 500551 [...] (664751 flits)
Class 0:
Remaining flits: 555552 555553 555554 555555 555556 555557 555558 555559 555560 555561 [...] (1121838 flits)
Measured flits: 555552 555553 555554 555555 555556 555557 555558 555559 555560 555561 [...] (617452 flits)
Class 0:
Remaining flits: 586602 586603 586604 586605 586606 586607 586608 586609 586610 586611 [...] (1200924 flits)
Measured flits: 586602 586603 586604 586605 586606 586607 586608 586609 586610 586611 [...] (570293 flits)
Class 0:
Remaining flits: 611612 611613 611614 611615 611616 611617 611618 611619 611620 611621 [...] (1282025 flits)
Measured flits: 611612 611613 611614 611615 611616 611617 611618 611619 611620 611621 [...] (522835 flits)
Class 0:
Remaining flits: 626966 626967 626968 626969 626970 626971 626972 626973 626974 626975 [...] (1358553 flits)
Measured flits: 626966 626967 626968 626969 626970 626971 626972 626973 626974 626975 [...] (475710 flits)
Class 0:
Remaining flits: 715410 715411 715412 715413 715414 715415 715416 715417 715418 715419 [...] (1434473 flits)
Measured flits: 715410 715411 715412 715413 715414 715415 715416 715417 715418 715419 [...] (428137 flits)
Class 0:
Remaining flits: 760320 760321 760322 760323 760324 760325 760326 760327 760328 760329 [...] (1510716 flits)
Measured flits: 760320 760321 760322 760323 760324 760325 760326 760327 760328 760329 [...] (380769 flits)
Class 0:
Remaining flits: 762811 762812 762813 762814 762815 762816 762817 762818 762819 762820 [...] (1586618 flits)
Measured flits: 762811 762812 762813 762814 762815 762816 762817 762818 762819 762820 [...] (333234 flits)
Class 0:
Remaining flits: 848448 848449 848450 848451 848452 848453 848454 848455 848456 848457 [...] (1649351 flits)
Measured flits: 848448 848449 848450 848451 848452 848453 848454 848455 848456 848457 [...] (285672 flits)
Class 0:
Remaining flits: 884830 884831 884832 884833 884834 884835 884836 884837 884838 884839 [...] (1709466 flits)
Measured flits: 884830 884831 884832 884833 884834 884835 884836 884837 884838 884839 [...] (238331 flits)
Class 0:
Remaining flits: 919080 919081 919082 919083 919084 919085 919086 919087 919088 919089 [...] (1769233 flits)
Measured flits: 919080 919081 919082 919083 919084 919085 919086 919087 919088 919089 [...] (191965 flits)
Class 0:
Remaining flits: 973746 973747 973748 973749 973750 973751 973752 973753 973754 973755 [...] (1830960 flits)
Measured flits: 973746 973747 973748 973749 973750 973751 973752 973753 973754 973755 [...] (147784 flits)
Class 0:
Remaining flits: 1034352 1034353 1034354 1034355 1034356 1034357 1034358 1034359 1034360 1034361 [...] (1890850 flits)
Measured flits: 1034352 1034353 1034354 1034355 1034356 1034357 1034358 1034359 1034360 1034361 [...] (106355 flits)
Class 0:
Remaining flits: 1049454 1049455 1049456 1049457 1049458 1049459 1049460 1049461 1049462 1049463 [...] (1953610 flits)
Measured flits: 1049454 1049455 1049456 1049457 1049458 1049459 1049460 1049461 1049462 1049463 [...] (70346 flits)
Class 0:
Remaining flits: 1085238 1085239 1085240 1085241 1085242 1085243 1085244 1085245 1085246 1085247 [...] (2016623 flits)
Measured flits: 1085238 1085239 1085240 1085241 1085242 1085243 1085244 1085245 1085246 1085247 [...] (42429 flits)
Class 0:
Remaining flits: 1146753 1146754 1146755 1146756 1146757 1146758 1146759 1146760 1146761 1150416 [...] (2078349 flits)
Measured flits: 1146753 1146754 1146755 1146756 1146757 1146758 1146759 1146760 1146761 1150416 [...] (22657 flits)
Class 0:
Remaining flits: 1201068 1201069 1201070 1201071 1201072 1201073 1201074 1201075 1201076 1201077 [...] (2141004 flits)
Measured flits: 1201068 1201069 1201070 1201071 1201072 1201073 1201074 1201075 1201076 1201077 [...] (10148 flits)
Class 0:
Remaining flits: 1201068 1201069 1201070 1201071 1201072 1201073 1201074 1201075 1201076 1201077 [...] (2201422 flits)
Measured flits: 1201068 1201069 1201070 1201071 1201072 1201073 1201074 1201075 1201076 1201077 [...] (4138 flits)
Class 0:
Remaining flits: 1201072 1201073 1201074 1201075 1201076 1201077 1201078 1201079 1201080 1201081 [...] (2261902 flits)
Measured flits: 1201072 1201073 1201074 1201075 1201076 1201077 1201078 1201079 1201080 1201081 [...] (1682 flits)
Class 0:
Remaining flits: 1253088 1253089 1253090 1253091 1253092 1253093 1253094 1253095 1253096 1253097 [...] (2325482 flits)
Measured flits: 1253088 1253089 1253090 1253091 1253092 1253093 1253094 1253095 1253096 1253097 [...] (450 flits)
Class 0:
Remaining flits: 1253103 1253104 1253105 1359486 1359487 1359488 1359489 1359490 1359491 1359492 [...] (2386749 flits)
Measured flits: 1253103 1253104 1253105 1359486 1359487 1359488 1359489 1359490 1359491 1359492 [...] (99 flits)
Class 0:
Remaining flits: 1366915 1366916 1366917 1366918 1366919 1393290 1393291 1393292 1393293 1393294 [...] (2450929 flits)
Measured flits: 1366915 1366916 1366917 1366918 1366919 (5 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1393290 1393291 1393292 1393293 1393294 1393295 1393296 1393297 1393298 1393299 [...] (2403660 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1534456 1534457 1534458 1534459 1534460 1534461 1534462 1534463 1541574 1541575 [...] (2353542 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1577106 1577107 1577108 1577109 1577110 1577111 1577112 1577113 1577114 1577115 [...] (2303385 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1577122 1577123 1579752 1579753 1579754 1579755 1579756 1579757 1579758 1579759 [...] (2253246 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1621744 1621745 1630437 1630438 1630439 1632978 1632979 1632980 1632981 1632982 [...] (2202353 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1659318 1659319 1659320 1659321 1659322 1659323 1659324 1659325 1659326 1659327 [...] (2152097 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1661815 1661816 1661817 1661818 1661819 1661820 1661821 1661822 1661823 1661824 [...] (2101753 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1739268 1739269 1739270 1739271 1739272 1739273 1739274 1739275 1739276 1739277 [...] (2051306 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1739268 1739269 1739270 1739271 1739272 1739273 1739274 1739275 1739276 1739277 [...] (2001068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1739268 1739269 1739270 1739271 1739272 1739273 1739274 1739275 1739276 1739277 [...] (1950619 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1764180 1764181 1764182 1764183 1764184 1764185 1764186 1764187 1764188 1764189 [...] (1900541 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1798866 1798867 1798868 1798869 1798870 1798871 1798872 1798873 1798874 1798875 [...] (1849559 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1798866 1798867 1798868 1798869 1798870 1798871 1798872 1798873 1798874 1798875 [...] (1798710 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1869822 1869823 1869824 1869825 1869826 1869827 1869828 1869829 1869830 1869831 [...] (1748009 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1869822 1869823 1869824 1869825 1869826 1869827 1869828 1869829 1869830 1869831 [...] (1698250 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1887984 1887985 1887986 1887987 1887988 1887989 1887990 1887991 1887992 1887993 [...] (1648134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1941354 1941355 1941356 1941357 1941358 1941359 1941360 1941361 1941362 1941363 [...] (1598491 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2036907 2036908 2036909 2036910 2036911 2036912 2036913 2036914 2036915 2080908 [...] (1549613 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2093958 2093959 2093960 2093961 2093962 2093963 2093964 2093965 2093966 2093967 [...] (1501056 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2153574 2153575 2153576 2153577 2153578 2153579 2153580 2153581 2153582 2153583 [...] (1452996 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2153587 2153588 2153589 2153590 2153591 2253330 2253331 2253332 2253333 2253334 [...] (1405251 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2270790 2270791 2270792 2270793 2270794 2270795 2270796 2270797 2270798 2270799 [...] (1357262 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2305152 2305153 2305154 2305155 2305156 2305157 2305158 2305159 2305160 2305161 [...] (1309286 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2314800 2314801 2314802 2314803 2314804 2314805 2314806 2314807 2314808 2314809 [...] (1261188 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2359170 2359171 2359172 2359173 2359174 2359175 2359176 2359177 2359178 2359179 [...] (1213540 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2370762 2370763 2370764 2370765 2370766 2370767 2370768 2370769 2370770 2370771 [...] (1165720 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2494731 2494732 2494733 2494734 2494735 2494736 2494737 2494738 2494739 2494740 [...] (1118275 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2517084 2517085 2517086 2517087 2517088 2517089 2517090 2517091 2517092 2517093 [...] (1070292 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2565484 2565485 2568060 2568061 2568062 2568063 2568064 2568065 2568066 2568067 [...] (1022460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2568076 2568077 2570774 2570775 2570776 2570777 2585556 2585557 2585558 2585559 [...] (975088 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2585572 2585573 2590146 2590147 2590148 2590149 2590150 2590151 2590152 2590153 [...] (927271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2590146 2590147 2590148 2590149 2590150 2590151 2590152 2590153 2590154 2590155 [...] (879487 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2628072 2628073 2628074 2628075 2628076 2628077 2628078 2628079 2628080 2628081 [...] (831977 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2754036 2754037 2754038 2754039 2754040 2754041 2754042 2754043 2754044 2754045 [...] (784338 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2764746 2764747 2764748 2764749 2764750 2764751 2764752 2764753 2764754 2764755 [...] (736793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2908602 2908603 2908604 2908605 2908606 2908607 2908608 2908609 2908610 2908611 [...] (689095 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2918106 2918107 2918108 2918109 2918110 2918111 2918112 2918113 2918114 2918115 [...] (641083 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2918106 2918107 2918108 2918109 2918110 2918111 2918112 2918113 2918114 2918115 [...] (593204 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2918106 2918107 2918108 2918109 2918110 2918111 2918112 2918113 2918114 2918115 [...] (545237 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2918111 2918112 2918113 2918114 2918115 2918116 2918117 2918118 2918119 2918120 [...] (497460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3044106 3044107 3044108 3044109 3044110 3044111 3044112 3044113 3044114 3044115 [...] (449723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3101148 3101149 3101150 3101151 3101152 3101153 3101154 3101155 3101156 3101157 [...] (401999 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3146778 3146779 3146780 3146781 3146782 3146783 3146784 3146785 3146786 3146787 [...] (354166 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3189762 3189763 3189764 3189765 3189766 3189767 3189768 3189769 3189770 3189771 [...] (306200 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3189762 3189763 3189764 3189765 3189766 3189767 3189768 3189769 3189770 3189771 [...] (258393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3291282 3291283 3291284 3291285 3291286 3291287 3291288 3291289 3291290 3291291 [...] (210379 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3291282 3291283 3291284 3291285 3291286 3291287 3291288 3291289 3291290 3291291 [...] (162855 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3313764 3313765 3313766 3313767 3313768 3313769 3313770 3313771 3313772 3313773 [...] (115238 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3313778 3313779 3313780 3313781 3464550 3464551 3464552 3464553 3464554 3464555 [...] (69429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3504762 3504763 3504764 3504765 3504766 3504767 3504768 3504769 3504770 3504771 [...] (30571 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3556078 3556079 3684308 3684309 3684310 3684311 3687642 3687643 3687644 3687645 [...] (8459 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3777336 3777337 3777338 3777339 3777340 3777341 3777342 3777343 3777344 3777345 [...] (1870 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3777390 3777391 3777392 3777393 3777394 3777395 3777396 3777397 3777398 3777399 [...] (247 flits)
Measured flits: (0 flits)
Time taken is 87326 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10399.9 (1 samples)
	minimum = 22 (1 samples)
	maximum = 24198 (1 samples)
Network latency average = 10377.6 (1 samples)
	minimum = 22 (1 samples)
	maximum = 24160 (1 samples)
Flit latency average = 25087 (1 samples)
	minimum = 5 (1 samples)
	maximum = 58579 (1 samples)
Fragmentation average = 175.529 (1 samples)
	minimum = 0 (1 samples)
	maximum = 612 (1 samples)
Injected packet rate average = 0.0401332 (1 samples)
	minimum = 0.0347143 (1 samples)
	maximum = 0.0471429 (1 samples)
Accepted packet rate average = 0.0168802 (1 samples)
	minimum = 0.0128571 (1 samples)
	maximum = 0.0205714 (1 samples)
Injected flit rate average = 0.722361 (1 samples)
	minimum = 0.624286 (1 samples)
	maximum = 0.849143 (1 samples)
Accepted flit rate average = 0.304014 (1 samples)
	minimum = 0.227714 (1 samples)
	maximum = 0.368571 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0101 (1 samples)
Hops average = 5.07047 (1 samples)
Total run time 111.897
