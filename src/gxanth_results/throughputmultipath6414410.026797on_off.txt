BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 351.091
	minimum = 23
	maximum = 977
Network latency average = 268.121
	minimum = 23
	maximum = 944
Slowest packet = 85
Flit latency average = 194.435
	minimum = 6
	maximum = 960
Slowest flit = 2831
Fragmentation average = 156.482
	minimum = 0
	maximum = 798
Injected packet rate average = 0.0242188
	minimum = 0 (at node 30)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.00986458
	minimum = 0.002 (at node 93)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.431948
	minimum = 0 (at node 30)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.206818
	minimum = 0.072 (at node 150)
	maximum = 0.36 (at node 44)
Injected packet length average = 17.8353
Accepted packet length average = 20.9657
Total in-flight flits = 43991 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 567.172
	minimum = 23
	maximum = 1961
Network latency average = 455.798
	minimum = 23
	maximum = 1802
Slowest packet = 85
Flit latency average = 360.907
	minimum = 6
	maximum = 1941
Slowest flit = 3307
Fragmentation average = 219.747
	minimum = 0
	maximum = 1396
Injected packet rate average = 0.0244427
	minimum = 0.002 (at node 174)
	maximum = 0.055 (at node 91)
Accepted packet rate average = 0.0110885
	minimum = 0.0055 (at node 150)
	maximum = 0.016 (at node 51)
Injected flit rate average = 0.438031
	minimum = 0.036 (at node 174)
	maximum = 0.99 (at node 91)
Accepted flit rate average= 0.219435
	minimum = 0.122 (at node 83)
	maximum = 0.3125 (at node 51)
Injected packet length average = 17.9207
Accepted packet length average = 19.7893
Total in-flight flits = 84685 (0 measured)
latency change    = 0.380979
throughput change = 0.0574985
Class 0:
Packet latency average = 1105.44
	minimum = 29
	maximum = 2960
Network latency average = 952.74
	minimum = 26
	maximum = 2871
Slowest packet = 231
Flit latency average = 854.631
	minimum = 6
	maximum = 2901
Slowest flit = 6733
Fragmentation average = 322.226
	minimum = 0
	maximum = 2403
Injected packet rate average = 0.0250104
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 15)
Accepted packet rate average = 0.0131562
	minimum = 0.005 (at node 87)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.450036
	minimum = 0 (at node 100)
	maximum = 1 (at node 15)
Accepted flit rate average= 0.242297
	minimum = 0.101 (at node 79)
	maximum = 0.392 (at node 44)
Injected packet length average = 17.994
Accepted packet length average = 18.4169
Total in-flight flits = 124600 (0 measured)
latency change    = 0.486928
throughput change = 0.0943552
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 366.44
	minimum = 32
	maximum = 2040
Network latency average = 190.515
	minimum = 23
	maximum = 925
Slowest packet = 14245
Flit latency average = 1262.77
	minimum = 6
	maximum = 3878
Slowest flit = 9116
Fragmentation average = 98.3143
	minimum = 0
	maximum = 825
Injected packet rate average = 0.0251563
	minimum = 0 (at node 121)
	maximum = 0.056 (at node 98)
Accepted packet rate average = 0.0132969
	minimum = 0.005 (at node 146)
	maximum = 0.023 (at node 33)
Injected flit rate average = 0.452573
	minimum = 0 (at node 121)
	maximum = 1 (at node 85)
Accepted flit rate average= 0.242047
	minimum = 0.099 (at node 32)
	maximum = 0.424 (at node 182)
Injected packet length average = 17.9905
Accepted packet length average = 18.2033
Total in-flight flits = 165067 (76011 measured)
latency change    = 2.01671
throughput change = 0.00103286
Class 0:
Packet latency average = 686.578
	minimum = 31
	maximum = 2537
Network latency average = 514.449
	minimum = 23
	maximum = 1958
Slowest packet = 14245
Flit latency average = 1464.59
	minimum = 6
	maximum = 4797
Slowest flit = 15202
Fragmentation average = 187.084
	minimum = 0
	maximum = 1439
Injected packet rate average = 0.025125
	minimum = 0.0045 (at node 53)
	maximum = 0.0555 (at node 101)
Accepted packet rate average = 0.0132422
	minimum = 0.0075 (at node 36)
	maximum = 0.021 (at node 78)
Injected flit rate average = 0.452253
	minimum = 0.081 (at node 53)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.240198
	minimum = 0.141 (at node 36)
	maximum = 0.3535 (at node 182)
Injected packet length average = 18.0001
Accepted packet length average = 18.1388
Total in-flight flits = 206028 (142281 measured)
latency change    = 0.466281
throughput change = 0.00769765
Class 0:
Packet latency average = 1046.13
	minimum = 27
	maximum = 3829
Network latency average = 870.22
	minimum = 23
	maximum = 2991
Slowest packet = 14245
Flit latency average = 1680.98
	minimum = 6
	maximum = 5689
Slowest flit = 19645
Fragmentation average = 233.424
	minimum = 0
	maximum = 2064
Injected packet rate average = 0.0247865
	minimum = 0.00733333 (at node 38)
	maximum = 0.0523333 (at node 102)
Accepted packet rate average = 0.0131458
	minimum = 0.008 (at node 36)
	maximum = 0.0193333 (at node 129)
Injected flit rate average = 0.445943
	minimum = 0.132 (at node 38)
	maximum = 0.945667 (at node 102)
Accepted flit rate average= 0.238387
	minimum = 0.156333 (at node 36)
	maximum = 0.335667 (at node 129)
Injected packet length average = 17.9914
Accepted packet length average = 18.134
Total in-flight flits = 244275 (199114 measured)
latency change    = 0.343695
throughput change = 0.0075959
Draining remaining packets ...
Class 0:
Remaining flits: 3628 3629 3630 3631 3632 3633 3634 3635 11969 13506 [...] (204769 flits)
Measured flits: 255402 255403 255404 255405 255406 255407 255408 255409 255410 255411 [...] (172983 flits)
Class 0:
Remaining flits: 3628 3629 3630 3631 3632 3633 3634 3635 13506 13507 [...] (165596 flits)
Measured flits: 255528 255529 255530 255531 255532 255533 255534 255535 255536 255537 [...] (143067 flits)
Class 0:
Remaining flits: 13508 13509 13510 13511 13512 13513 13514 13515 13516 13517 [...] (127658 flits)
Measured flits: 255528 255529 255530 255531 255532 255533 255534 255535 255536 255537 [...] (111475 flits)
Class 0:
Remaining flits: 21014 21015 21016 21017 21018 21019 21020 21021 21022 21023 [...] (92313 flits)
Measured flits: 255528 255529 255530 255531 255532 255533 255534 255535 255536 255537 [...] (80658 flits)
Class 0:
Remaining flits: 21014 21015 21016 21017 21018 21019 21020 21021 21022 21023 [...] (60075 flits)
Measured flits: 255528 255529 255530 255531 255532 255533 255534 255535 255536 255537 [...] (51988 flits)
Class 0:
Remaining flits: 21014 21015 21016 21017 21018 21019 21020 21021 21022 21023 [...] (33118 flits)
Measured flits: 255870 255871 255872 255873 255874 255875 255876 255877 255878 255879 [...] (28249 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (13416 flits)
Measured flits: 256209 256210 256211 256675 256676 256677 256678 256679 258516 258517 [...] (11437 flits)
Class 0:
Remaining flits: 65286 65287 65288 65289 65290 65291 65292 65293 65294 65295 [...] (3420 flits)
Measured flits: 261414 261415 261416 261417 261418 261419 261420 261421 261422 261423 [...] (2844 flits)
Class 0:
Remaining flits: 430621 430622 430623 430624 430625 430626 430627 430628 430629 430630 [...] (11 flits)
Measured flits: 430621 430622 430623 430624 430625 430626 430627 430628 430629 430630 [...] (11 flits)
Time taken is 15013 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4372.86 (1 samples)
	minimum = 27 (1 samples)
	maximum = 12152 (1 samples)
Network latency average = 4130.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11699 (1 samples)
Flit latency average = 3910.35 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13652 (1 samples)
Fragmentation average = 299.389 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7742 (1 samples)
Injected packet rate average = 0.0247865 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0523333 (1 samples)
Accepted packet rate average = 0.0131458 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0193333 (1 samples)
Injected flit rate average = 0.445943 (1 samples)
	minimum = 0.132 (1 samples)
	maximum = 0.945667 (1 samples)
Accepted flit rate average = 0.238387 (1 samples)
	minimum = 0.156333 (1 samples)
	maximum = 0.335667 (1 samples)
Injected packet size average = 17.9914 (1 samples)
Accepted packet size average = 18.134 (1 samples)
Hops average = 5.13644 (1 samples)
Total run time 19.703
