BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 309.593
	minimum = 23
	maximum = 953
Network latency average = 242.791
	minimum = 23
	maximum = 890
Slowest packet = 3
Flit latency average = 204.593
	minimum = 6
	maximum = 873
Slowest flit = 4913
Fragmentation average = 54.6652
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.00944271
	minimum = 0.002 (at node 93)
	maximum = 0.017 (at node 2)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.177411
	minimum = 0.039 (at node 93)
	maximum = 0.317 (at node 2)
Injected packet length average = 17.8591
Accepted packet length average = 18.7882
Total in-flight flits = 33275 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 508.82
	minimum = 23
	maximum = 1959
Network latency average = 424.325
	minimum = 23
	maximum = 1773
Slowest packet = 3
Flit latency average = 384.05
	minimum = 6
	maximum = 1756
Slowest flit = 14381
Fragmentation average = 60.3128
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.00979948
	minimum = 0.005 (at node 79)
	maximum = 0.0145 (at node 152)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.180424
	minimum = 0.09 (at node 79)
	maximum = 0.266 (at node 152)
Injected packet length average = 17.9339
Accepted packet length average = 18.4116
Total in-flight flits = 65159 (0 measured)
latency change    = 0.391547
throughput change = 0.0166996
Class 0:
Packet latency average = 1076.31
	minimum = 29
	maximum = 2699
Network latency average = 963.018
	minimum = 29
	maximum = 2562
Slowest packet = 2180
Flit latency average = 932.761
	minimum = 6
	maximum = 2545
Slowest flit = 24929
Fragmentation average = 67.0125
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0212604
	minimum = 0 (at node 10)
	maximum = 0.055 (at node 102)
Accepted packet rate average = 0.0103854
	minimum = 0.004 (at node 4)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.382182
	minimum = 0 (at node 10)
	maximum = 1 (at node 102)
Accepted flit rate average= 0.187609
	minimum = 0.072 (at node 31)
	maximum = 0.373 (at node 16)
Injected packet length average = 17.9762
Accepted packet length average = 18.0647
Total in-flight flits = 102614 (0 measured)
latency change    = 0.527257
throughput change = 0.0382971
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 257.484
	minimum = 23
	maximum = 1317
Network latency average = 135.519
	minimum = 23
	maximum = 926
Slowest packet = 11601
Flit latency average = 1368.42
	minimum = 6
	maximum = 3662
Slowest flit = 21347
Fragmentation average = 21.1162
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0215156
	minimum = 0 (at node 34)
	maximum = 0.053 (at node 20)
Accepted packet rate average = 0.0104271
	minimum = 0.002 (at node 126)
	maximum = 0.018 (at node 0)
Injected flit rate average = 0.387453
	minimum = 0 (at node 34)
	maximum = 0.946 (at node 20)
Accepted flit rate average= 0.18738
	minimum = 0.05 (at node 126)
	maximum = 0.325 (at node 172)
Injected packet length average = 18.008
Accepted packet length average = 17.9705
Total in-flight flits = 140995 (67521 measured)
latency change    = 3.18013
throughput change = 0.001223
Class 0:
Packet latency average = 579.2
	minimum = 23
	maximum = 2218
Network latency average = 473.991
	minimum = 23
	maximum = 1985
Slowest packet = 11601
Flit latency average = 1592.88
	minimum = 6
	maximum = 4133
Slowest flit = 56203
Fragmentation average = 37.3361
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0200469
	minimum = 0.002 (at node 34)
	maximum = 0.043 (at node 158)
Accepted packet rate average = 0.0101589
	minimum = 0.005 (at node 170)
	maximum = 0.0165 (at node 44)
Injected flit rate average = 0.360661
	minimum = 0.036 (at node 34)
	maximum = 0.774 (at node 158)
Accepted flit rate average= 0.182727
	minimum = 0.092 (at node 190)
	maximum = 0.2925 (at node 44)
Injected packet length average = 17.9909
Accepted packet length average = 17.9869
Total in-flight flits = 171029 (120626 measured)
latency change    = 0.55545
throughput change = 0.0254678
Class 0:
Packet latency average = 1009.6
	minimum = 23
	maximum = 3346
Network latency average = 903.368
	minimum = 23
	maximum = 2931
Slowest packet = 11601
Flit latency average = 1838.98
	minimum = 6
	maximum = 4868
Slowest flit = 75238
Fragmentation average = 46.0896
	minimum = 0
	maximum = 157
Injected packet rate average = 0.0196059
	minimum = 0.00266667 (at node 33)
	maximum = 0.0396667 (at node 149)
Accepted packet rate average = 0.0101823
	minimum = 0.006 (at node 191)
	maximum = 0.0143333 (at node 44)
Injected flit rate average = 0.35284
	minimum = 0.048 (at node 33)
	maximum = 0.712333 (at node 149)
Accepted flit rate average= 0.183023
	minimum = 0.113333 (at node 191)
	maximum = 0.254333 (at node 29)
Injected packet length average = 17.9966
Accepted packet length average = 17.9746
Total in-flight flits = 200467 (168131 measured)
latency change    = 0.426308
throughput change = 0.00161732
Draining remaining packets ...
Class 0:
Remaining flits: 47052 47053 47054 47055 47056 47057 47058 47059 47060 47061 [...] (170715 flits)
Measured flits: 207918 207919 207920 207921 207922 207923 207924 207925 207926 207927 [...] (150902 flits)
Class 0:
Remaining flits: 69426 69427 69428 69429 69430 69431 69432 69433 69434 69435 [...] (141822 flits)
Measured flits: 207918 207919 207920 207921 207922 207923 207924 207925 207926 207927 [...] (130225 flits)
Class 0:
Remaining flits: 78714 78715 78716 78717 78718 78719 78720 78721 78722 78723 [...] (111943 flits)
Measured flits: 207918 207919 207920 207921 207922 207923 207924 207925 207926 207927 [...] (105748 flits)
Class 0:
Remaining flits: 122508 122509 122510 122511 122512 122513 122514 122515 122516 122517 [...] (83657 flits)
Measured flits: 208188 208189 208190 208191 208192 208193 208194 208195 208196 208197 [...] (80308 flits)
Class 0:
Remaining flits: 122508 122509 122510 122511 122512 122513 122514 122515 122516 122517 [...] (55424 flits)
Measured flits: 208188 208189 208190 208191 208192 208193 208194 208195 208196 208197 [...] (53883 flits)
Class 0:
Remaining flits: 138114 138115 138116 138117 138118 138119 138120 138121 138122 138123 [...] (29350 flits)
Measured flits: 208368 208369 208370 208371 208372 208373 208374 208375 208376 208377 [...] (28785 flits)
Class 0:
Remaining flits: 150120 150121 150122 150123 150124 150125 150126 150127 150128 150129 [...] (8945 flits)
Measured flits: 214854 214855 214856 214857 214858 214859 214860 214861 214862 214863 [...] (8798 flits)
Class 0:
Remaining flits: 246762 246763 246764 246765 246766 246767 246768 246769 246770 246771 [...] (1351 flits)
Measured flits: 246762 246763 246764 246765 246766 246767 246768 246769 246770 246771 [...] (1351 flits)
Time taken is 14839 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4674.38 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10771 (1 samples)
Network latency average = 4549.84 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10594 (1 samples)
Flit latency average = 4088.16 (1 samples)
	minimum = 6 (1 samples)
	maximum = 11230 (1 samples)
Fragmentation average = 68.3801 (1 samples)
	minimum = 0 (1 samples)
	maximum = 181 (1 samples)
Injected packet rate average = 0.0196059 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.0396667 (1 samples)
Accepted packet rate average = 0.0101823 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0143333 (1 samples)
Injected flit rate average = 0.35284 (1 samples)
	minimum = 0.048 (1 samples)
	maximum = 0.712333 (1 samples)
Accepted flit rate average = 0.183023 (1 samples)
	minimum = 0.113333 (1 samples)
	maximum = 0.254333 (1 samples)
Injected packet size average = 17.9966 (1 samples)
Accepted packet size average = 17.9746 (1 samples)
Hops average = 5.08403 (1 samples)
Total run time 7.7475
