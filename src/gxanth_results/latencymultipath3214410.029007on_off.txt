BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 356.441
	minimum = 23
	maximum = 986
Network latency average = 268.855
	minimum = 23
	maximum = 918
Slowest packet = 3
Flit latency average = 203.605
	minimum = 6
	maximum = 948
Slowest flit = 4454
Fragmentation average = 142.979
	minimum = 0
	maximum = 797
Injected packet rate average = 0.0256042
	minimum = 0 (at node 10)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0100833
	minimum = 0.004 (at node 41)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.455682
	minimum = 0 (at node 10)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.206161
	minimum = 0.087 (at node 174)
	maximum = 0.357 (at node 121)
Injected packet length average = 17.7972
Accepted packet length average = 20.4458
Total in-flight flits = 48905 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 596.912
	minimum = 23
	maximum = 1971
Network latency average = 474.569
	minimum = 23
	maximum = 1835
Slowest packet = 98
Flit latency average = 397.79
	minimum = 6
	maximum = 1874
Slowest flit = 9227
Fragmentation average = 178.843
	minimum = 0
	maximum = 1478
Injected packet rate average = 0.0260521
	minimum = 0 (at node 79)
	maximum = 0.056 (at node 129)
Accepted packet rate average = 0.0113281
	minimum = 0.007 (at node 36)
	maximum = 0.0175 (at node 22)
Injected flit rate average = 0.467005
	minimum = 0 (at node 79)
	maximum = 0.9995 (at node 129)
Accepted flit rate average= 0.217294
	minimum = 0.1395 (at node 108)
	maximum = 0.327 (at node 22)
Injected packet length average = 17.9258
Accepted packet length average = 19.1818
Total in-flight flits = 96631 (0 measured)
latency change    = 0.402859
throughput change = 0.0512338
Class 0:
Packet latency average = 1271.02
	minimum = 29
	maximum = 2966
Network latency average = 1091.88
	minimum = 25
	maximum = 2852
Slowest packet = 531
Flit latency average = 1027.45
	minimum = 6
	maximum = 2835
Slowest flit = 9575
Fragmentation average = 221.164
	minimum = 0
	maximum = 1934
Injected packet rate average = 0.0268177
	minimum = 0 (at node 37)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0123698
	minimum = 0.004 (at node 43)
	maximum = 0.024 (at node 120)
Injected flit rate average = 0.481927
	minimum = 0 (at node 37)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.223307
	minimum = 0.073 (at node 135)
	maximum = 0.419 (at node 8)
Injected packet length average = 17.9705
Accepted packet length average = 18.0526
Total in-flight flits = 146474 (0 measured)
latency change    = 0.530368
throughput change = 0.0269271
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 466.68
	minimum = 27
	maximum = 2838
Network latency average = 180.674
	minimum = 23
	maximum = 903
Slowest packet = 15193
Flit latency average = 1529.02
	minimum = 6
	maximum = 3869
Slowest flit = 10477
Fragmentation average = 50.997
	minimum = 0
	maximum = 404
Injected packet rate average = 0.0248177
	minimum = 0 (at node 67)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0121042
	minimum = 0.002 (at node 167)
	maximum = 0.022 (at node 59)
Injected flit rate average = 0.44687
	minimum = 0 (at node 154)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.217958
	minimum = 0.051 (at node 60)
	maximum = 0.411 (at node 165)
Injected packet length average = 18.0061
Accepted packet length average = 18.0069
Total in-flight flits = 190378 (79196 measured)
latency change    = 1.72354
throughput change = 0.0245412
Class 0:
Packet latency average = 821.023
	minimum = 27
	maximum = 3808
Network latency average = 530.052
	minimum = 23
	maximum = 1975
Slowest packet = 15193
Flit latency average = 1777.7
	minimum = 6
	maximum = 4708
Slowest flit = 21516
Fragmentation average = 92.0821
	minimum = 0
	maximum = 949
Injected packet rate average = 0.0238698
	minimum = 0.004 (at node 19)
	maximum = 0.0505 (at node 47)
Accepted packet rate average = 0.0120938
	minimum = 0.006 (at node 167)
	maximum = 0.0185 (at node 103)
Injected flit rate average = 0.429234
	minimum = 0.0725 (at node 19)
	maximum = 0.907 (at node 47)
Accepted flit rate average= 0.217539
	minimum = 0.114 (at node 167)
	maximum = 0.322 (at node 165)
Injected packet length average = 17.9823
Accepted packet length average = 17.9877
Total in-flight flits = 228035 (146788 measured)
latency change    = 0.431588
throughput change = 0.00192734
Class 0:
Packet latency average = 1311.04
	minimum = 27
	maximum = 5132
Network latency average = 983.969
	minimum = 23
	maximum = 2972
Slowest packet = 15193
Flit latency average = 2037.05
	minimum = 6
	maximum = 5462
Slowest flit = 10493
Fragmentation average = 123.813
	minimum = 0
	maximum = 1250
Injected packet rate average = 0.0228681
	minimum = 0.00566667 (at node 178)
	maximum = 0.0413333 (at node 50)
Accepted packet rate average = 0.0120347
	minimum = 0.007 (at node 167)
	maximum = 0.0166667 (at node 103)
Injected flit rate average = 0.411434
	minimum = 0.102 (at node 178)
	maximum = 0.744 (at node 50)
Accepted flit rate average= 0.216116
	minimum = 0.139333 (at node 167)
	maximum = 0.300333 (at node 103)
Injected packet length average = 17.9916
Accepted packet length average = 17.9577
Total in-flight flits = 259195 (201721 measured)
latency change    = 0.373762
throughput change = 0.00658323
Class 0:
Packet latency average = 1858.23
	minimum = 26
	maximum = 5821
Network latency average = 1469.64
	minimum = 23
	maximum = 3957
Slowest packet = 15193
Flit latency average = 2277.26
	minimum = 6
	maximum = 6519
Slowest flit = 37379
Fragmentation average = 140.732
	minimum = 0
	maximum = 1385
Injected packet rate average = 0.021918
	minimum = 0.007 (at node 35)
	maximum = 0.038 (at node 98)
Accepted packet rate average = 0.0119753
	minimum = 0.008 (at node 57)
	maximum = 0.0165 (at node 85)
Injected flit rate average = 0.394383
	minimum = 0.126 (at node 35)
	maximum = 0.68475 (at node 98)
Accepted flit rate average= 0.214645
	minimum = 0.14375 (at node 57)
	maximum = 0.3 (at node 103)
Injected packet length average = 17.9936
Accepted packet length average = 17.924
Total in-flight flits = 284963 (244114 measured)
latency change    = 0.294468
throughput change = 0.00685686
Class 0:
Packet latency average = 2332.12
	minimum = 26
	maximum = 6618
Network latency average = 1891.25
	minimum = 23
	maximum = 4977
Slowest packet = 15193
Flit latency average = 2510.47
	minimum = 6
	maximum = 7178
Slowest flit = 55349
Fragmentation average = 153.291
	minimum = 0
	maximum = 1920
Injected packet rate average = 0.0212531
	minimum = 0.0084 (at node 183)
	maximum = 0.0348 (at node 173)
Accepted packet rate average = 0.0118875
	minimum = 0.0088 (at node 57)
	maximum = 0.016 (at node 103)
Injected flit rate average = 0.38241
	minimum = 0.153 (at node 183)
	maximum = 0.6258 (at node 173)
Accepted flit rate average= 0.213255
	minimum = 0.1566 (at node 57)
	maximum = 0.286 (at node 103)
Injected packet length average = 17.9931
Accepted packet length average = 17.9394
Total in-flight flits = 309489 (280741 measured)
latency change    = 0.203202
throughput change = 0.00651484
Class 0:
Packet latency average = 2792.97
	minimum = 26
	maximum = 7816
Network latency average = 2305.99
	minimum = 23
	maximum = 5875
Slowest packet = 15193
Flit latency average = 2753.22
	minimum = 6
	maximum = 8130
Slowest flit = 73400
Fragmentation average = 160.018
	minimum = 0
	maximum = 2081
Injected packet rate average = 0.0205538
	minimum = 0.0075 (at node 190)
	maximum = 0.0341667 (at node 122)
Accepted packet rate average = 0.0117665
	minimum = 0.0085 (at node 79)
	maximum = 0.0153333 (at node 103)
Injected flit rate average = 0.369923
	minimum = 0.134333 (at node 190)
	maximum = 0.615833 (at node 122)
Accepted flit rate average= 0.211287
	minimum = 0.153833 (at node 79)
	maximum = 0.268 (at node 103)
Injected packet length average = 17.9978
Accepted packet length average = 17.9567
Total in-flight flits = 330193 (309769 measured)
latency change    = 0.165002
throughput change = 0.00931377
Class 0:
Packet latency average = 3239.82
	minimum = 25
	maximum = 8532
Network latency average = 2708.71
	minimum = 23
	maximum = 6949
Slowest packet = 15193
Flit latency average = 2998.72
	minimum = 6
	maximum = 9321
Slowest flit = 26945
Fragmentation average = 165.549
	minimum = 0
	maximum = 2372
Injected packet rate average = 0.0196905
	minimum = 0.00828571 (at node 190)
	maximum = 0.0311429 (at node 122)
Accepted packet rate average = 0.0116734
	minimum = 0.00857143 (at node 57)
	maximum = 0.0147143 (at node 27)
Injected flit rate average = 0.354272
	minimum = 0.149143 (at node 190)
	maximum = 0.559143 (at node 122)
Accepted flit rate average= 0.20913
	minimum = 0.151 (at node 57)
	maximum = 0.260286 (at node 177)
Injected packet length average = 17.992
Accepted packet length average = 17.9152
Total in-flight flits = 342817 (328687 measured)
latency change    = 0.137925
throughput change = 0.0103147
Draining all recorded packets ...
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (347774 flits)
Measured flits: 272808 272809 272810 272811 272812 272813 272814 272815 272816 272817 [...] (326687 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (351616 flits)
Measured flits: 272808 272809 272810 272811 272812 272813 272814 272815 272816 272817 [...] (321332 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (351772 flits)
Measured flits: 272808 272809 272810 272811 272812 272813 272814 272815 272816 272817 [...] (312879 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (350159 flits)
Measured flits: 272808 272809 272810 272811 272812 272813 272814 272815 272816 272817 [...] (300675 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (348186 flits)
Measured flits: 272808 272809 272810 272811 272812 272813 272814 272815 272816 272817 [...] (287934 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (348787 flits)
Measured flits: 272822 272823 272824 272825 273244 273245 273246 273247 273248 273249 [...] (274437 flits)
Class 0:
Remaining flits: 70146 70147 70148 70149 70150 70151 70152 70153 70154 70155 [...] (346186 flits)
Measured flits: 273245 273246 273247 273248 273249 273250 273251 273252 273253 273254 [...] (260111 flits)
Class 0:
Remaining flits: 73967 73968 73969 73970 73971 73972 73973 73974 73975 73976 [...] (344253 flits)
Measured flits: 273582 273583 273584 273585 273586 273587 273588 273589 273590 273591 [...] (242687 flits)
Class 0:
Remaining flits: 211914 211915 211916 211917 211918 211919 211920 211921 211922 211923 [...] (342422 flits)
Measured flits: 277668 277669 277670 277671 277672 277673 277674 277675 277676 277677 [...] (222907 flits)
Class 0:
Remaining flits: 239945 239946 239947 239948 239949 239950 239951 239952 239953 239954 [...] (340097 flits)
Measured flits: 277668 277669 277670 277671 277672 277673 277674 277675 277676 277677 [...] (202124 flits)
Class 0:
Remaining flits: 244854 244855 244856 244857 244858 244859 244860 244861 244862 244863 [...] (337426 flits)
Measured flits: 277668 277669 277670 277671 277672 277673 277674 277675 277676 277677 [...] (180821 flits)
Class 0:
Remaining flits: 252949 252950 252951 252952 252953 263448 263449 263450 263451 263452 [...] (337294 flits)
Measured flits: 277668 277669 277670 277671 277672 277673 277674 277675 277676 277677 [...] (161574 flits)
Class 0:
Remaining flits: 263464 263465 277668 277669 277670 277671 277672 277673 277674 277675 [...] (335180 flits)
Measured flits: 277668 277669 277670 277671 277672 277673 277674 277675 277676 277677 [...] (143934 flits)
Class 0:
Remaining flits: 292050 292051 292052 292053 292054 292055 292056 292057 292058 292059 [...] (335102 flits)
Measured flits: 292050 292051 292052 292053 292054 292055 292056 292057 292058 292059 [...] (126314 flits)
Class 0:
Remaining flits: 300726 300727 300728 300729 300730 300731 300732 300733 300734 300735 [...] (338297 flits)
Measured flits: 300726 300727 300728 300729 300730 300731 300732 300733 300734 300735 [...] (109815 flits)
Class 0:
Remaining flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (335302 flits)
Measured flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (94663 flits)
Class 0:
Remaining flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (333094 flits)
Measured flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (81749 flits)
Class 0:
Remaining flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (335217 flits)
Measured flits: 333558 333559 333560 333561 333562 333563 333564 333565 333566 333567 [...] (69638 flits)
Class 0:
Remaining flits: 415998 415999 416000 416001 416002 416003 416004 416005 416006 416007 [...] (333564 flits)
Measured flits: 415998 415999 416000 416001 416002 416003 416004 416005 416006 416007 [...] (57948 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (331476 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (47965 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (330854 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (39503 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (330183 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (31737 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (328252 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (25266 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (325125 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (20121 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (322803 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (16851 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (323201 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (13414 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (323633 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (10280 flits)
Class 0:
Remaining flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (322841 flits)
Measured flits: 438840 438841 438842 438843 438844 438845 438846 438847 438848 438849 [...] (7480 flits)
Class 0:
Remaining flits: 438853 438854 438855 438856 438857 743328 743329 743330 743331 743332 [...] (323420 flits)
Measured flits: 438853 438854 438855 438856 438857 743328 743329 743330 743331 743332 [...] (5533 flits)
Class 0:
Remaining flits: 817776 817777 817778 817779 817780 817781 817782 817783 817784 817785 [...] (326343 flits)
Measured flits: 817776 817777 817778 817779 817780 817781 817782 817783 817784 817785 [...] (4551 flits)
Class 0:
Remaining flits: 830826 830827 830828 830829 830830 830831 830832 830833 830834 830835 [...] (326483 flits)
Measured flits: 830826 830827 830828 830829 830830 830831 830832 830833 830834 830835 [...] (3496 flits)
Class 0:
Remaining flits: 834822 834823 834824 834825 834826 834827 834828 834829 834830 834831 [...] (325801 flits)
Measured flits: 834822 834823 834824 834825 834826 834827 834828 834829 834830 834831 [...] (2823 flits)
Class 0:
Remaining flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (324680 flits)
Measured flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (2141 flits)
Class 0:
Remaining flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (324404 flits)
Measured flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (1755 flits)
Class 0:
Remaining flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (316636 flits)
Measured flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (1148 flits)
Class 0:
Remaining flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (315753 flits)
Measured flits: 836172 836173 836174 836175 836176 836177 836178 836179 836180 836181 [...] (836 flits)
Class 0:
Remaining flits: 902934 902935 902936 902937 902938 902939 902940 902941 902942 902943 [...] (318917 flits)
Measured flits: 902934 902935 902936 902937 902938 902939 902940 902941 902942 902943 [...] (533 flits)
Class 0:
Remaining flits: 931698 931699 931700 931701 931702 931703 931704 931705 931706 931707 [...] (319490 flits)
Measured flits: 931698 931699 931700 931701 931702 931703 931704 931705 931706 931707 [...] (413 flits)
Class 0:
Remaining flits: 987462 987463 987464 987465 987466 987467 987468 987469 987470 987471 [...] (320130 flits)
Measured flits: 1078251 1078252 1078253 1082484 1082485 1082486 1082487 1082488 1082489 1082490 [...] (179 flits)
Class 0:
Remaining flits: 1082484 1082485 1082486 1082487 1082488 1082489 1082490 1082491 1082492 1082493 [...] (319506 flits)
Measured flits: 1082484 1082485 1082486 1082487 1082488 1082489 1082490 1082491 1082492 1082493 [...] (108 flits)
Class 0:
Remaining flits: 1104516 1104517 1104518 1104519 1104520 1104521 1104522 1104523 1104524 1104525 [...] (318484 flits)
Measured flits: 1276452 1276453 1276454 1276455 1276456 1276457 1276458 1276459 1276460 1276461 [...] (54 flits)
Class 0:
Remaining flits: 1137744 1137745 1137746 1137747 1137748 1137749 1137750 1137751 1137752 1137753 [...] (317657 flits)
Measured flits: 1276452 1276453 1276454 1276455 1276456 1276457 1276458 1276459 1276460 1276461 [...] (54 flits)
Class 0:
Remaining flits: 1144098 1144099 1144100 1144101 1144102 1144103 1144104 1144105 1144106 1144107 [...] (313177 flits)
Measured flits: 1479706 1479707 (2 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1144098 1144099 1144100 1144101 1144102 1144103 1144104 1144105 1144106 1144107 [...] (277699 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1144098 1144099 1144100 1144101 1144102 1144103 1144104 1144105 1144106 1144107 [...] (243469 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1194768 1194769 1194770 1194771 1194772 1194773 1194774 1194775 1194776 1194777 [...] (208979 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270350 1270351 1270352 1270353 1270354 1270355 1270356 1270357 1270358 1270359 [...] (176175 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270350 1270351 1270352 1270353 1270354 1270355 1270356 1270357 1270358 1270359 [...] (143425 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270350 1270351 1270352 1270353 1270354 1270355 1270356 1270357 1270358 1270359 [...] (111274 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1338931 1338932 1338933 1338934 1338935 1338936 1338937 1338938 1338939 1338940 [...] (82327 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1407600 1407601 1407602 1407603 1407604 1407605 1407606 1407607 1407608 1407609 [...] (56554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1407600 1407601 1407602 1407603 1407604 1407605 1407606 1407607 1407608 1407609 [...] (34116 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1407600 1407601 1407602 1407603 1407604 1407605 1407606 1407607 1407608 1407609 [...] (17091 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1672290 1672291 1672292 1672293 1672294 1672295 1672296 1672297 1672298 1672299 [...] (6848 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1836813 1836814 1836815 1836816 1836817 1836818 1836819 1836820 1836821 1836822 [...] (1463 flits)
Measured flits: (0 flits)
Time taken is 66024 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11168.8 (1 samples)
	minimum = 25 (1 samples)
	maximum = 44167 (1 samples)
Network latency average = 7734.69 (1 samples)
	minimum = 23 (1 samples)
	maximum = 34890 (1 samples)
Flit latency average = 8131.28 (1 samples)
	minimum = 6 (1 samples)
	maximum = 35611 (1 samples)
Fragmentation average = 202.34 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5722 (1 samples)
Injected packet rate average = 0.0196905 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0311429 (1 samples)
Accepted packet rate average = 0.0116734 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0147143 (1 samples)
Injected flit rate average = 0.354272 (1 samples)
	minimum = 0.149143 (1 samples)
	maximum = 0.559143 (1 samples)
Accepted flit rate average = 0.20913 (1 samples)
	minimum = 0.151 (1 samples)
	maximum = 0.260286 (1 samples)
Injected packet size average = 17.992 (1 samples)
Accepted packet size average = 17.9152 (1 samples)
Hops average = 5.05852 (1 samples)
Total run time 84.9038
