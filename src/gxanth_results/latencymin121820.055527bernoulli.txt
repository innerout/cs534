BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.253
	minimum = 22
	maximum = 911
Network latency average = 254.937
	minimum = 22
	maximum = 839
Slowest packet = 756
Flit latency average = 224.841
	minimum = 5
	maximum = 858
Slowest flit = 18549
Fragmentation average = 22.4974
	minimum = 0
	maximum = 113
Injected packet rate average = 0.019026
	minimum = 0.009 (at node 24)
	maximum = 0.03 (at node 31)
Accepted packet rate average = 0.0139583
	minimum = 0.003 (at node 174)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.339068
	minimum = 0.162 (at node 24)
	maximum = 0.523 (at node 31)
Accepted flit rate average= 0.255911
	minimum = 0.062 (at node 174)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8212
Accepted packet length average = 18.334
Total in-flight flits = 18671 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 735.924
	minimum = 22
	maximum = 1830
Network latency average = 308.526
	minimum = 22
	maximum = 1471
Slowest packet = 883
Flit latency average = 274.98
	minimum = 5
	maximum = 1454
Slowest flit = 37637
Fragmentation average = 22.2712
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0164688
	minimum = 0.008 (at node 106)
	maximum = 0.0265 (at node 50)
Accepted packet rate average = 0.0139141
	minimum = 0.008 (at node 116)
	maximum = 0.019 (at node 98)
Injected flit rate average = 0.294539
	minimum = 0.144 (at node 106)
	maximum = 0.477 (at node 50)
Accepted flit rate average= 0.253044
	minimum = 0.144 (at node 116)
	maximum = 0.347 (at node 154)
Injected packet length average = 17.8847
Accepted packet length average = 18.1862
Total in-flight flits = 18535 (0 measured)
latency change    = 0.490093
throughput change = 0.0113308
Class 0:
Packet latency average = 1837.85
	minimum = 998
	maximum = 2646
Network latency average = 359.537
	minimum = 22
	maximum = 1617
Slowest packet = 6269
Flit latency average = 324.965
	minimum = 5
	maximum = 1600
Slowest flit = 57761
Fragmentation average = 22.3495
	minimum = 0
	maximum = 109
Injected packet rate average = 0.0139219
	minimum = 0.004 (at node 30)
	maximum = 0.032 (at node 73)
Accepted packet rate average = 0.0139792
	minimum = 0.005 (at node 91)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.25037
	minimum = 0.082 (at node 30)
	maximum = 0.576 (at node 73)
Accepted flit rate average= 0.251521
	minimum = 0.082 (at node 163)
	maximum = 0.468 (at node 16)
Injected packet length average = 17.9839
Accepted packet length average = 17.9925
Total in-flight flits = 18411 (0 measured)
latency change    = 0.599572
throughput change = 0.0060569
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2688.97
	minimum = 1694
	maximum = 3523
Network latency average = 290.033
	minimum = 22
	maximum = 898
Slowest packet = 9165
Flit latency average = 333.496
	minimum = 5
	maximum = 1635
Slowest flit = 91800
Fragmentation average = 20.9586
	minimum = 0
	maximum = 125
Injected packet rate average = 0.0139688
	minimum = 0.001 (at node 44)
	maximum = 0.029 (at node 31)
Accepted packet rate average = 0.014
	minimum = 0.006 (at node 53)
	maximum = 0.025 (at node 19)
Injected flit rate average = 0.251604
	minimum = 0.018 (at node 44)
	maximum = 0.507 (at node 31)
Accepted flit rate average= 0.252161
	minimum = 0.108 (at node 53)
	maximum = 0.448 (at node 19)
Injected packet length average = 18.0119
Accepted packet length average = 18.0115
Total in-flight flits = 18182 (18056 measured)
latency change    = 0.316524
throughput change = 0.00254053
Class 0:
Packet latency average = 3096.16
	minimum = 1694
	maximum = 4236
Network latency average = 330.81
	minimum = 22
	maximum = 1569
Slowest packet = 9165
Flit latency average = 328.27
	minimum = 5
	maximum = 2117
Slowest flit = 158404
Fragmentation average = 21.8654
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0138672
	minimum = 0.002 (at node 46)
	maximum = 0.022 (at node 141)
Accepted packet rate average = 0.0138828
	minimum = 0.0085 (at node 35)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.249969
	minimum = 0.036 (at node 46)
	maximum = 0.392 (at node 141)
Accepted flit rate average= 0.250268
	minimum = 0.149 (at node 35)
	maximum = 0.387 (at node 129)
Injected packet length average = 18.0259
Accepted packet length average = 18.0272
Total in-flight flits = 18284 (18253 measured)
latency change    = 0.131514
throughput change = 0.0075648
Class 0:
Packet latency average = 3474.13
	minimum = 1694
	maximum = 5109
Network latency average = 343.849
	minimum = 22
	maximum = 1663
Slowest packet = 9165
Flit latency average = 328.777
	minimum = 5
	maximum = 2130
Slowest flit = 158417
Fragmentation average = 21.971
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0137951
	minimum = 0.00466667 (at node 46)
	maximum = 0.0223333 (at node 121)
Accepted packet rate average = 0.0137951
	minimum = 0.00933333 (at node 5)
	maximum = 0.0193333 (at node 128)
Injected flit rate average = 0.24841
	minimum = 0.084 (at node 46)
	maximum = 0.398 (at node 121)
Accepted flit rate average= 0.248479
	minimum = 0.165667 (at node 135)
	maximum = 0.348667 (at node 138)
Injected packet length average = 18.007
Accepted packet length average = 18.0121
Total in-flight flits = 18333 (18333 measured)
latency change    = 0.108798
throughput change = 0.00720005
Class 0:
Packet latency average = 3856.75
	minimum = 1694
	maximum = 5965
Network latency average = 352.375
	minimum = 22
	maximum = 2133
Slowest packet = 9165
Flit latency average = 331.063
	minimum = 5
	maximum = 2130
Slowest flit = 158417
Fragmentation average = 22.5291
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0138424
	minimum = 0.006 (at node 46)
	maximum = 0.02025 (at node 121)
Accepted packet rate average = 0.0137995
	minimum = 0.00975 (at node 84)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.24903
	minimum = 0.108 (at node 46)
	maximum = 0.3645 (at node 179)
Accepted flit rate average= 0.248531
	minimum = 0.1755 (at node 84)
	maximum = 0.342 (at node 129)
Injected packet length average = 17.9903
Accepted packet length average = 18.0102
Total in-flight flits = 18753 (18753 measured)
latency change    = 0.099206
throughput change = 0.000209565
Class 0:
Packet latency average = 4245.38
	minimum = 1694
	maximum = 6917
Network latency average = 354.343
	minimum = 22
	maximum = 2133
Slowest packet = 9165
Flit latency average = 329.775
	minimum = 5
	maximum = 2130
Slowest flit = 158417
Fragmentation average = 22.5478
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0138969
	minimum = 0.0084 (at node 46)
	maximum = 0.021 (at node 179)
Accepted packet rate average = 0.0138656
	minimum = 0.0104 (at node 104)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.250122
	minimum = 0.1494 (at node 46)
	maximum = 0.378 (at node 179)
Accepted flit rate average= 0.249715
	minimum = 0.1878 (at node 104)
	maximum = 0.3234 (at node 128)
Injected packet length average = 17.9984
Accepted packet length average = 18.0096
Total in-flight flits = 18715 (18715 measured)
latency change    = 0.0915427
throughput change = 0.00473874
Class 0:
Packet latency average = 4624.15
	minimum = 1694
	maximum = 7541
Network latency average = 356.677
	minimum = 22
	maximum = 2133
Slowest packet = 9165
Flit latency average = 329.958
	minimum = 5
	maximum = 2130
Slowest flit = 158417
Fragmentation average = 22.7177
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0138976
	minimum = 0.009 (at node 46)
	maximum = 0.019 (at node 179)
Accepted packet rate average = 0.0138819
	minimum = 0.0108333 (at node 57)
	maximum = 0.0175 (at node 128)
Injected flit rate average = 0.250215
	minimum = 0.161667 (at node 46)
	maximum = 0.3395 (at node 179)
Accepted flit rate average= 0.250007
	minimum = 0.194333 (at node 42)
	maximum = 0.315 (at node 138)
Injected packet length average = 18.0042
Accepted packet length average = 18.0095
Total in-flight flits = 18619 (18619 measured)
latency change    = 0.0819117
throughput change = 0.00116941
Class 0:
Packet latency average = 4997.88
	minimum = 1694
	maximum = 8246
Network latency average = 357.747
	minimum = 22
	maximum = 2133
Slowest packet = 9165
Flit latency average = 329.646
	minimum = 5
	maximum = 2130
Slowest flit = 158417
Fragmentation average = 22.7034
	minimum = 0
	maximum = 145
Injected packet rate average = 0.0138973
	minimum = 0.00957143 (at node 46)
	maximum = 0.0191429 (at node 179)
Accepted packet rate average = 0.0138824
	minimum = 0.011 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.250124
	minimum = 0.172286 (at node 46)
	maximum = 0.344571 (at node 179)
Accepted flit rate average= 0.249946
	minimum = 0.200429 (at node 79)
	maximum = 0.326714 (at node 138)
Injected packet length average = 17.998
Accepted packet length average = 18.0044
Total in-flight flits = 18562 (18562 measured)
latency change    = 0.0747779
throughput change = 0.000245093
Draining all recorded packets ...
Class 0:
Remaining flits: 479214 479215 479216 479217 479218 479219 479220 479221 479222 479223 [...] (18538 flits)
Measured flits: 479214 479215 479216 479217 479218 479219 479220 479221 479222 479223 [...] (18538 flits)
Class 0:
Remaining flits: 519138 519139 519140 519141 519142 519143 519144 519145 519146 519147 [...] (18435 flits)
Measured flits: 519138 519139 519140 519141 519142 519143 519144 519145 519146 519147 [...] (18435 flits)
Class 0:
Remaining flits: 558137 558138 558139 558140 558141 558142 558143 570942 570943 570944 [...] (18291 flits)
Measured flits: 558137 558138 558139 558140 558141 558142 558143 570942 570943 570944 [...] (18291 flits)
Class 0:
Remaining flits: 582138 582139 582140 582141 582142 582143 582144 582145 582146 582147 [...] (18233 flits)
Measured flits: 582138 582139 582140 582141 582142 582143 582144 582145 582146 582147 [...] (18233 flits)
Class 0:
Remaining flits: 669708 669709 669710 669711 669712 669713 669714 669715 669716 669717 [...] (18639 flits)
Measured flits: 669708 669709 669710 669711 669712 669713 669714 669715 669716 669717 [...] (18639 flits)
Class 0:
Remaining flits: 721458 721459 721460 721461 721462 721463 721464 721465 721466 721467 [...] (18229 flits)
Measured flits: 721458 721459 721460 721461 721462 721463 721464 721465 721466 721467 [...] (18229 flits)
Class 0:
Remaining flits: 766278 766279 766280 766281 766282 766283 766284 766285 766286 766287 [...] (18438 flits)
Measured flits: 766278 766279 766280 766281 766282 766283 766284 766285 766286 766287 [...] (18438 flits)
Class 0:
Remaining flits: 806806 806807 806808 806809 806810 806811 806812 806813 824400 824401 [...] (18377 flits)
Measured flits: 806806 806807 806808 806809 806810 806811 806812 806813 824400 824401 [...] (18377 flits)
Class 0:
Remaining flits: 865764 865765 865766 865767 865768 865769 865770 865771 865772 865773 [...] (17882 flits)
Measured flits: 865764 865765 865766 865767 865768 865769 865770 865771 865772 865773 [...] (17882 flits)
Class 0:
Remaining flits: 895392 895393 895394 895395 895396 895397 895398 895399 895400 895401 [...] (18470 flits)
Measured flits: 895392 895393 895394 895395 895396 895397 895398 895399 895400 895401 [...] (18470 flits)
Class 0:
Remaining flits: 968832 968833 968834 968835 968836 968837 968838 968839 968840 968841 [...] (18558 flits)
Measured flits: 968832 968833 968834 968835 968836 968837 968838 968839 968840 968841 [...] (18558 flits)
Class 0:
Remaining flits: 1017666 1017667 1017668 1017669 1017670 1017671 1017672 1017673 1017674 1017675 [...] (18580 flits)
Measured flits: 1017666 1017667 1017668 1017669 1017670 1017671 1017672 1017673 1017674 1017675 [...] (18580 flits)
Class 0:
Remaining flits: 1052010 1052011 1052012 1052013 1052014 1052015 1052016 1052017 1052018 1052019 [...] (18839 flits)
Measured flits: 1052010 1052011 1052012 1052013 1052014 1052015 1052016 1052017 1052018 1052019 [...] (18839 flits)
Class 0:
Remaining flits: 1097532 1097533 1097534 1097535 1097536 1097537 1097538 1097539 1097540 1097541 [...] (18898 flits)
Measured flits: 1097532 1097533 1097534 1097535 1097536 1097537 1097538 1097539 1097540 1097541 [...] (18898 flits)
Class 0:
Remaining flits: 1166694 1166695 1166696 1166697 1166698 1166699 1166700 1166701 1166702 1166703 [...] (18751 flits)
Measured flits: 1166694 1166695 1166696 1166697 1166698 1166699 1166700 1166701 1166702 1166703 [...] (18751 flits)
Class 0:
Remaining flits: 1216314 1216315 1216316 1216317 1216318 1216319 1216320 1216321 1216322 1216323 [...] (18189 flits)
Measured flits: 1216314 1216315 1216316 1216317 1216318 1216319 1216320 1216321 1216322 1216323 [...] (18189 flits)
Class 0:
Remaining flits: 1251450 1251451 1251452 1251453 1251454 1251455 1251456 1251457 1251458 1251459 [...] (18453 flits)
Measured flits: 1251450 1251451 1251452 1251453 1251454 1251455 1251456 1251457 1251458 1251459 [...] (18453 flits)
Class 0:
Remaining flits: 1285435 1285436 1285437 1285438 1285439 1285440 1285441 1285442 1285443 1285444 [...] (18244 flits)
Measured flits: 1285435 1285436 1285437 1285438 1285439 1285440 1285441 1285442 1285443 1285444 [...] (18244 flits)
Class 0:
Remaining flits: 1359864 1359865 1359866 1359867 1359868 1359869 1359870 1359871 1359872 1359873 [...] (18344 flits)
Measured flits: 1359864 1359865 1359866 1359867 1359868 1359869 1359870 1359871 1359872 1359873 [...] (18344 flits)
Class 0:
Remaining flits: 1389974 1389975 1389976 1389977 1399174 1399175 1402110 1402111 1402112 1402113 [...] (18613 flits)
Measured flits: 1389974 1389975 1389976 1389977 1399174 1399175 1402110 1402111 1402112 1402113 [...] (18613 flits)
Class 0:
Remaining flits: 1449760 1449761 1449762 1449763 1449764 1449765 1449766 1449767 1449768 1449769 [...] (18644 flits)
Measured flits: 1449760 1449761 1449762 1449763 1449764 1449765 1449766 1449767 1449768 1449769 [...] (18644 flits)
Class 0:
Remaining flits: 1499436 1499437 1499438 1499439 1499440 1499441 1499442 1499443 1499444 1499445 [...] (19051 flits)
Measured flits: 1499436 1499437 1499438 1499439 1499440 1499441 1499442 1499443 1499444 1499445 [...] (18997 flits)
Class 0:
Remaining flits: 1527546 1527547 1527548 1527549 1527550 1527551 1528488 1528489 1528490 1528491 [...] (18436 flits)
Measured flits: 1527546 1527547 1527548 1527549 1527550 1527551 1528488 1528489 1528490 1528491 [...] (18256 flits)
Class 0:
Remaining flits: 1593022 1593023 1593024 1593025 1593026 1593027 1593028 1593029 1593030 1593031 [...] (18422 flits)
Measured flits: 1593022 1593023 1593024 1593025 1593026 1593027 1593028 1593029 1593030 1593031 [...] (17997 flits)
Class 0:
Remaining flits: 1634058 1634059 1634060 1634061 1634062 1634063 1634064 1634065 1634066 1634067 [...] (18347 flits)
Measured flits: 1634058 1634059 1634060 1634061 1634062 1634063 1634064 1634065 1634066 1634067 [...] (18041 flits)
Class 0:
Remaining flits: 1638116 1638117 1638118 1638119 1638120 1638121 1638122 1638123 1638124 1638125 [...] (18705 flits)
Measured flits: 1638116 1638117 1638118 1638119 1638120 1638121 1638122 1638123 1638124 1638125 [...] (17780 flits)
Class 0:
Remaining flits: 1723986 1723987 1723988 1723989 1723990 1723991 1723992 1723993 1723994 1723995 [...] (18564 flits)
Measured flits: 1723986 1723987 1723988 1723989 1723990 1723991 1723992 1723993 1723994 1723995 [...] (16579 flits)
Class 0:
Remaining flits: 1788620 1788621 1788622 1788623 1792710 1792711 1792712 1792713 1792714 1792715 [...] (18220 flits)
Measured flits: 1788620 1788621 1788622 1788623 1792710 1792711 1792712 1792713 1792714 1792715 [...] (14006 flits)
Class 0:
Remaining flits: 1821042 1821043 1821044 1821045 1821046 1821047 1821048 1821049 1821050 1821051 [...] (18739 flits)
Measured flits: 1821042 1821043 1821044 1821045 1821046 1821047 1821048 1821049 1821050 1821051 [...] (11921 flits)
Class 0:
Remaining flits: 1874358 1874359 1874360 1874361 1874362 1874363 1874364 1874365 1874366 1874367 [...] (18454 flits)
Measured flits: 1886076 1886077 1886078 1886079 1886080 1886081 1886082 1886083 1886084 1886085 [...] (9344 flits)
Class 0:
Remaining flits: 1880406 1880407 1880408 1880409 1880410 1880411 1880412 1880413 1880414 1880415 [...] (18528 flits)
Measured flits: 1907514 1907515 1907516 1907517 1907518 1907519 1907520 1907521 1907522 1907523 [...] (6087 flits)
Class 0:
Remaining flits: 1955412 1955413 1955414 1955415 1955416 1955417 1955418 1955419 1955420 1955421 [...] (18456 flits)
Measured flits: 1969371 1969372 1969373 1969374 1969375 1969376 1969377 1969378 1969379 1971198 [...] (3421 flits)
Class 0:
Remaining flits: 2014915 2014916 2014917 2014918 2014919 2018844 2018845 2018846 2018847 2018848 [...] (18003 flits)
Measured flits: 2027502 2027503 2027504 2027505 2027506 2027507 2027508 2027509 2027510 2027511 [...] (1824 flits)
Class 0:
Remaining flits: 2066328 2066329 2066330 2066331 2066332 2066333 2066334 2066335 2066336 2066337 [...] (18328 flits)
Measured flits: 2066328 2066329 2066330 2066331 2066332 2066333 2066334 2066335 2066336 2066337 [...] (536 flits)
Class 0:
Remaining flits: 2116638 2116639 2116640 2116641 2116642 2116643 2116644 2116645 2116646 2116647 [...] (18732 flits)
Measured flits: 2154648 2154649 2154650 2154651 2154652 2154653 2161829 2161830 2161831 2161832 [...] (429 flits)
Draining remaining packets ...
Time taken is 46330 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16363.3 (1 samples)
	minimum = 1694 (1 samples)
	maximum = 35687 (1 samples)
Network latency average = 364.528 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2352 (1 samples)
Flit latency average = 329.466 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2310 (1 samples)
Fragmentation average = 22.8489 (1 samples)
	minimum = 0 (1 samples)
	maximum = 156 (1 samples)
Injected packet rate average = 0.0138973 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0191429 (1 samples)
Accepted packet rate average = 0.0138824 (1 samples)
	minimum = 0.011 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.250124 (1 samples)
	minimum = 0.172286 (1 samples)
	maximum = 0.344571 (1 samples)
Accepted flit rate average = 0.249946 (1 samples)
	minimum = 0.200429 (1 samples)
	maximum = 0.326714 (1 samples)
Injected packet size average = 17.998 (1 samples)
Accepted packet size average = 18.0044 (1 samples)
Hops average = 5.0706 (1 samples)
Total run time 41.2894
