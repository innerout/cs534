BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.006907
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 84.7405
	minimum = 22
	maximum = 323
Network latency average = 58.7325
	minimum = 22
	maximum = 263
Slowest packet = 7
Flit latency average = 36.0072
	minimum = 5
	maximum = 246
Slowest flit = 13589
Fragmentation average = 10.8426
	minimum = 0
	maximum = 212
Injected packet rate average = 0.00764583
	minimum = 0 (at node 3)
	maximum = 0.029 (at node 97)
Accepted packet rate average = 0.00714583
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 165)
Injected flit rate average = 0.136563
	minimum = 0 (at node 3)
	maximum = 0.522 (at node 97)
Accepted flit rate average= 0.130552
	minimum = 0.018 (at node 41)
	maximum = 0.27 (at node 165)
Injected packet length average = 17.861
Accepted packet length average = 18.2697
Total in-flight flits = 1358 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 88.2723
	minimum = 22
	maximum = 400
Network latency average = 61.4476
	minimum = 22
	maximum = 324
Slowest packet = 7
Flit latency average = 38.7606
	minimum = 5
	maximum = 307
Slowest flit = 23435
Fragmentation average = 10.5325
	minimum = 0
	maximum = 212
Injected packet rate average = 0.0072474
	minimum = 0.0005 (at node 9)
	maximum = 0.019 (at node 161)
Accepted packet rate average = 0.00705729
	minimum = 0.0035 (at node 25)
	maximum = 0.0125 (at node 44)
Injected flit rate average = 0.129961
	minimum = 0.009 (at node 9)
	maximum = 0.342 (at node 161)
Accepted flit rate average= 0.127826
	minimum = 0.063 (at node 25)
	maximum = 0.231 (at node 44)
Injected packet length average = 17.9321
Accepted packet length average = 18.1125
Total in-flight flits = 1009 (0 measured)
latency change    = 0.0400103
throughput change = 0.0213303
Class 0:
Packet latency average = 90.709
	minimum = 22
	maximum = 399
Network latency average = 63.7468
	minimum = 22
	maximum = 281
Slowest packet = 2637
Flit latency average = 40.9349
	minimum = 5
	maximum = 264
Slowest flit = 54408
Fragmentation average = 11.093
	minimum = 0
	maximum = 105
Injected packet rate average = 0.00692188
	minimum = 0 (at node 9)
	maximum = 0.033 (at node 20)
Accepted packet rate average = 0.00689063
	minimum = 0.001 (at node 23)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.124344
	minimum = 0 (at node 9)
	maximum = 0.586 (at node 20)
Accepted flit rate average= 0.124036
	minimum = 0.018 (at node 23)
	maximum = 0.259 (at node 22)
Injected packet length average = 17.9639
Accepted packet length average = 18.0008
Total in-flight flits = 1116 (0 measured)
latency change    = 0.0268625
throughput change = 0.030548
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 95.2981
	minimum = 22
	maximum = 494
Network latency average = 64.7957
	minimum = 22
	maximum = 248
Slowest packet = 4121
Flit latency average = 41.9163
	minimum = 5
	maximum = 231
Slowest flit = 85967
Fragmentation average = 11.4298
	minimum = 0
	maximum = 94
Injected packet rate average = 0.00702083
	minimum = 0 (at node 47)
	maximum = 0.031 (at node 79)
Accepted packet rate average = 0.00701563
	minimum = 0.001 (at node 121)
	maximum = 0.014 (at node 6)
Injected flit rate average = 0.126255
	minimum = 0 (at node 47)
	maximum = 0.57 (at node 79)
Accepted flit rate average= 0.126521
	minimum = 0.018 (at node 121)
	maximum = 0.242 (at node 6)
Injected packet length average = 17.9829
Accepted packet length average = 18.0341
Total in-flight flits = 1088 (1088 measured)
latency change    = 0.0481553
throughput change = 0.0196361
Class 0:
Packet latency average = 95.3591
	minimum = 22
	maximum = 581
Network latency average = 67.2051
	minimum = 22
	maximum = 438
Slowest packet = 4121
Flit latency average = 44.0471
	minimum = 5
	maximum = 421
Slowest flit = 105817
Fragmentation average = 11.9714
	minimum = 0
	maximum = 107
Injected packet rate average = 0.00723177
	minimum = 0.0005 (at node 143)
	maximum = 0.0265 (at node 79)
Accepted packet rate average = 0.007125
	minimum = 0.003 (at node 184)
	maximum = 0.0125 (at node 56)
Injected flit rate average = 0.130234
	minimum = 0.009 (at node 143)
	maximum = 0.4765 (at node 79)
Accepted flit rate average= 0.128497
	minimum = 0.0475 (at node 184)
	maximum = 0.225 (at node 56)
Injected packet length average = 18.0086
Accepted packet length average = 18.0347
Total in-flight flits = 1759 (1759 measured)
latency change    = 0.000639104
throughput change = 0.0153821
Class 0:
Packet latency average = 93.7236
	minimum = 22
	maximum = 581
Network latency average = 66.03
	minimum = 22
	maximum = 438
Slowest packet = 4121
Flit latency average = 42.997
	minimum = 5
	maximum = 421
Slowest flit = 105817
Fragmentation average = 11.4835
	minimum = 0
	maximum = 107
Injected packet rate average = 0.00697917
	minimum = 0.000666667 (at node 143)
	maximum = 0.0213333 (at node 79)
Accepted packet rate average = 0.00701389
	minimum = 0.00366667 (at node 153)
	maximum = 0.0123333 (at node 34)
Injected flit rate average = 0.125767
	minimum = 0.012 (at node 143)
	maximum = 0.388 (at node 79)
Accepted flit rate average= 0.126035
	minimum = 0.066 (at node 153)
	maximum = 0.222 (at node 34)
Injected packet length average = 18.0204
Accepted packet length average = 17.9693
Total in-flight flits = 880 (880 measured)
latency change    = 0.0174502
throughput change = 0.0195396
Draining remaining packets ...
Time taken is 6121 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 93.753 (1 samples)
	minimum = 22 (1 samples)
	maximum = 581 (1 samples)
Network latency average = 66.1403 (1 samples)
	minimum = 22 (1 samples)
	maximum = 438 (1 samples)
Flit latency average = 43.1256 (1 samples)
	minimum = 5 (1 samples)
	maximum = 421 (1 samples)
Fragmentation average = 11.4637 (1 samples)
	minimum = 0 (1 samples)
	maximum = 107 (1 samples)
Injected packet rate average = 0.00697917 (1 samples)
	minimum = 0.000666667 (1 samples)
	maximum = 0.0213333 (1 samples)
Accepted packet rate average = 0.00701389 (1 samples)
	minimum = 0.00366667 (1 samples)
	maximum = 0.0123333 (1 samples)
Injected flit rate average = 0.125767 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.388 (1 samples)
Accepted flit rate average = 0.126035 (1 samples)
	minimum = 0.066 (1 samples)
	maximum = 0.222 (1 samples)
Injected packet size average = 18.0204 (1 samples)
Accepted packet size average = 17.9693 (1 samples)
Hops average = 5.0699 (1 samples)
Total run time 2.97615
