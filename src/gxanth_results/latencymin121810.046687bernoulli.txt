BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 388.397
	minimum = 27
	maximum = 962
Network latency average = 293.308
	minimum = 27
	maximum = 932
Slowest packet = 143
Flit latency average = 249.408
	minimum = 6
	maximum = 920
Slowest flit = 7699
Fragmentation average = 53.5903
	minimum = 0
	maximum = 151
Injected packet rate average = 0.0142656
	minimum = 0.005 (at node 148)
	maximum = 0.026 (at node 157)
Accepted packet rate average = 0.00922917
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 165)
Injected flit rate average = 0.252073
	minimum = 0.074 (at node 148)
	maximum = 0.453 (at node 157)
Accepted flit rate average= 0.172359
	minimum = 0.054 (at node 41)
	maximum = 0.306 (at node 165)
Injected packet length average = 17.67
Accepted packet length average = 18.6755
Total in-flight flits = 18045 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 774.713
	minimum = 27
	maximum = 1823
Network latency average = 415.519
	minimum = 24
	maximum = 1757
Slowest packet = 1058
Flit latency average = 362.649
	minimum = 6
	maximum = 1854
Slowest flit = 17209
Fragmentation average = 55.7043
	minimum = 0
	maximum = 151
Injected packet rate average = 0.011526
	minimum = 0.0045 (at node 169)
	maximum = 0.0215 (at node 150)
Accepted packet rate average = 0.00905469
	minimum = 0.0055 (at node 30)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.205906
	minimum = 0.081 (at node 169)
	maximum = 0.3795 (at node 150)
Accepted flit rate average= 0.166326
	minimum = 0.099 (at node 30)
	maximum = 0.2715 (at node 22)
Injected packet length average = 17.8644
Accepted packet length average = 18.369
Total in-flight flits = 17815 (0 measured)
latency change    = 0.498657
throughput change = 0.0362774
Class 0:
Packet latency average = 1971.14
	minimum = 1176
	maximum = 2805
Network latency average = 561.031
	minimum = 25
	maximum = 2413
Slowest packet = 633
Flit latency average = 492.571
	minimum = 6
	maximum = 2396
Slowest flit = 11411
Fragmentation average = 61.4039
	minimum = 0
	maximum = 136
Injected packet rate average = 0.00881771
	minimum = 0.001 (at node 12)
	maximum = 0.022 (at node 185)
Accepted packet rate average = 0.00880729
	minimum = 0.001 (at node 10)
	maximum = 0.016 (at node 63)
Injected flit rate average = 0.157969
	minimum = 0.018 (at node 12)
	maximum = 0.39 (at node 185)
Accepted flit rate average= 0.158708
	minimum = 0.019 (at node 10)
	maximum = 0.296 (at node 172)
Injected packet length average = 17.9149
Accepted packet length average = 18.0201
Total in-flight flits = 17781 (0 measured)
latency change    = 0.606973
throughput change = 0.0479949
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2887.47
	minimum = 1860
	maximum = 3716
Network latency average = 334.132
	minimum = 23
	maximum = 934
Slowest packet = 6264
Flit latency average = 471.315
	minimum = 6
	maximum = 2610
Slowest flit = 62376
Fragmentation average = 53.6683
	minimum = 0
	maximum = 140
Injected packet rate average = 0.00925
	minimum = 0 (at node 171)
	maximum = 0.024 (at node 130)
Accepted packet rate average = 0.00925521
	minimum = 0.003 (at node 38)
	maximum = 0.016 (at node 43)
Injected flit rate average = 0.165333
	minimum = 0 (at node 171)
	maximum = 0.424 (at node 130)
Accepted flit rate average= 0.165542
	minimum = 0.045 (at node 96)
	maximum = 0.286 (at node 43)
Injected packet length average = 17.8739
Accepted packet length average = 17.8863
Total in-flight flits = 17623 (16189 measured)
latency change    = 0.317345
throughput change = 0.0412786
Class 0:
Packet latency average = 3352.09
	minimum = 1860
	maximum = 4473
Network latency average = 446.543
	minimum = 23
	maximum = 1826
Slowest packet = 6264
Flit latency average = 480.018
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 55.7152
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00899219
	minimum = 0.002 (at node 181)
	maximum = 0.02 (at node 166)
Accepted packet rate average = 0.00903125
	minimum = 0.005 (at node 22)
	maximum = 0.0165 (at node 16)
Injected flit rate average = 0.162172
	minimum = 0.039 (at node 181)
	maximum = 0.359 (at node 166)
Accepted flit rate average= 0.162078
	minimum = 0.0855 (at node 190)
	maximum = 0.2975 (at node 16)
Injected packet length average = 18.0348
Accepted packet length average = 17.9464
Total in-flight flits = 17589 (17564 measured)
latency change    = 0.138607
throughput change = 0.0213696
Class 0:
Packet latency average = 3788.63
	minimum = 1860
	maximum = 5570
Network latency average = 488.265
	minimum = 23
	maximum = 2370
Slowest packet = 6264
Flit latency average = 481.377
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 57.1938
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00906076
	minimum = 0.00233333 (at node 120)
	maximum = 0.0186667 (at node 166)
Accepted packet rate average = 0.00906076
	minimum = 0.00533333 (at node 17)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.163149
	minimum = 0.042 (at node 120)
	maximum = 0.333333 (at node 166)
Accepted flit rate average= 0.163054
	minimum = 0.0923333 (at node 17)
	maximum = 0.280333 (at node 16)
Injected packet length average = 18.0061
Accepted packet length average = 17.9956
Total in-flight flits = 17696 (17696 measured)
latency change    = 0.115224
throughput change = 0.00598388
Class 0:
Packet latency average = 4201.67
	minimum = 1860
	maximum = 6458
Network latency average = 507.691
	minimum = 23
	maximum = 2584
Slowest packet = 6264
Flit latency average = 483.724
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 58.3917
	minimum = 0
	maximum = 144
Injected packet rate average = 0.0090599
	minimum = 0.0035 (at node 120)
	maximum = 0.01675 (at node 166)
Accepted packet rate average = 0.00903125
	minimum = 0.00525 (at node 4)
	maximum = 0.013 (at node 16)
Injected flit rate average = 0.162924
	minimum = 0.06025 (at node 120)
	maximum = 0.3015 (at node 166)
Accepted flit rate average= 0.16259
	minimum = 0.0915 (at node 4)
	maximum = 0.23425 (at node 16)
Injected packet length average = 17.983
Accepted packet length average = 18.003
Total in-flight flits = 17868 (17868 measured)
latency change    = 0.0983036
throughput change = 0.00285366
Class 0:
Packet latency average = 4605.52
	minimum = 1860
	maximum = 7234
Network latency average = 520.733
	minimum = 23
	maximum = 2584
Slowest packet = 6264
Flit latency average = 487.231
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 59.1472
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00897083
	minimum = 0.0036 (at node 120)
	maximum = 0.0154 (at node 130)
Accepted packet rate average = 0.00898125
	minimum = 0.0058 (at node 4)
	maximum = 0.0126 (at node 129)
Injected flit rate average = 0.161451
	minimum = 0.0648 (at node 120)
	maximum = 0.2758 (at node 130)
Accepted flit rate average= 0.161574
	minimum = 0.103 (at node 4)
	maximum = 0.229 (at node 129)
Injected packet length average = 17.9973
Accepted packet length average = 17.9901
Total in-flight flits = 17596 (17596 measured)
latency change    = 0.087689
throughput change = 0.00628743
Class 0:
Packet latency average = 5022.62
	minimum = 1860
	maximum = 8075
Network latency average = 524.887
	minimum = 23
	maximum = 2584
Slowest packet = 6264
Flit latency average = 485.752
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 58.64
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00897483
	minimum = 0.00416667 (at node 120)
	maximum = 0.016 (at node 32)
Accepted packet rate average = 0.00899045
	minimum = 0.00583333 (at node 36)
	maximum = 0.0123333 (at node 128)
Injected flit rate average = 0.161626
	minimum = 0.074 (at node 120)
	maximum = 0.289 (at node 32)
Accepted flit rate average= 0.161762
	minimum = 0.105 (at node 36)
	maximum = 0.223667 (at node 128)
Injected packet length average = 18.0088
Accepted packet length average = 17.9927
Total in-flight flits = 17641 (17641 measured)
latency change    = 0.0830435
throughput change = 0.0011634
Class 0:
Packet latency average = 5437.56
	minimum = 1860
	maximum = 8889
Network latency average = 528.379
	minimum = 23
	maximum = 2681
Slowest packet = 6264
Flit latency average = 485.548
	minimum = 6
	maximum = 2749
Slowest flit = 69387
Fragmentation average = 58.6182
	minimum = 0
	maximum = 144
Injected packet rate average = 0.00898958
	minimum = 0.00457143 (at node 120)
	maximum = 0.0157143 (at node 32)
Accepted packet rate average = 0.00899851
	minimum = 0.00628571 (at node 36)
	maximum = 0.0121429 (at node 129)
Injected flit rate average = 0.161756
	minimum = 0.0821429 (at node 120)
	maximum = 0.283286 (at node 32)
Accepted flit rate average= 0.16191
	minimum = 0.113429 (at node 36)
	maximum = 0.217857 (at node 129)
Injected packet length average = 17.9937
Accepted packet length average = 17.993
Total in-flight flits = 17524 (17524 measured)
latency change    = 0.0763103
throughput change = 0.000912961
Draining all recorded packets ...
Class 0:
Remaining flits: 281214 281215 281216 281217 281218 281219 281220 281221 281222 281223 [...] (17700 flits)
Measured flits: 281214 281215 281216 281217 281218 281219 281220 281221 281222 281223 [...] (17700 flits)
Class 0:
Remaining flits: 301662 301663 301664 301665 301666 301667 301668 301669 301670 301671 [...] (17709 flits)
Measured flits: 301662 301663 301664 301665 301666 301667 301668 301669 301670 301671 [...] (17709 flits)
Class 0:
Remaining flits: 353914 353915 362376 362377 362378 362379 362380 362381 362382 362383 [...] (17993 flits)
Measured flits: 353914 353915 362376 362377 362378 362379 362380 362381 362382 362383 [...] (17993 flits)
Class 0:
Remaining flits: 374598 374599 374600 374601 374602 374603 374604 374605 374606 374607 [...] (17457 flits)
Measured flits: 374598 374599 374600 374601 374602 374603 374604 374605 374606 374607 [...] (17457 flits)
Class 0:
Remaining flits: 399210 399211 399212 399213 399214 399215 399216 399217 399218 399219 [...] (17774 flits)
Measured flits: 399210 399211 399212 399213 399214 399215 399216 399217 399218 399219 [...] (17774 flits)
Class 0:
Remaining flits: 427201 427202 427203 427204 427205 427206 427207 427208 427209 427210 [...] (17868 flits)
Measured flits: 427201 427202 427203 427204 427205 427206 427207 427208 427209 427210 [...] (17868 flits)
Class 0:
Remaining flits: 460458 460459 460460 460461 460462 460463 460464 460465 460466 460467 [...] (17701 flits)
Measured flits: 460458 460459 460460 460461 460462 460463 460464 460465 460466 460467 [...] (17701 flits)
Class 0:
Remaining flits: 501390 501391 501392 501393 501394 501395 501396 501397 501398 501399 [...] (17248 flits)
Measured flits: 501390 501391 501392 501393 501394 501395 501396 501397 501398 501399 [...] (17248 flits)
Class 0:
Remaining flits: 537498 537499 537500 537501 537502 537503 537504 537505 537506 537507 [...] (17431 flits)
Measured flits: 537498 537499 537500 537501 537502 537503 537504 537505 537506 537507 [...] (17431 flits)
Class 0:
Remaining flits: 545562 545563 545564 545565 545566 545567 545568 545569 545570 545571 [...] (17603 flits)
Measured flits: 545562 545563 545564 545565 545566 545567 545568 545569 545570 545571 [...] (17603 flits)
Class 0:
Remaining flits: 579474 579475 579476 579477 579478 579479 579480 579481 579482 579483 [...] (17690 flits)
Measured flits: 579474 579475 579476 579477 579478 579479 579480 579481 579482 579483 [...] (17690 flits)
Class 0:
Remaining flits: 585968 585969 585970 585971 612108 612109 612110 612111 612112 612113 [...] (18044 flits)
Measured flits: 585968 585969 585970 585971 612108 612109 612110 612111 612112 612113 [...] (18044 flits)
Class 0:
Remaining flits: 662490 662491 662492 662493 662494 662495 662496 662497 662498 662499 [...] (17860 flits)
Measured flits: 662490 662491 662492 662493 662494 662495 662496 662497 662498 662499 [...] (17860 flits)
Class 0:
Remaining flits: 688320 688321 688322 688323 688324 688325 688326 688327 688328 688329 [...] (17523 flits)
Measured flits: 688320 688321 688322 688323 688324 688325 688326 688327 688328 688329 [...] (17523 flits)
Class 0:
Remaining flits: 709488 709489 709490 709491 709492 709493 709494 709495 709496 709497 [...] (17667 flits)
Measured flits: 709488 709489 709490 709491 709492 709493 709494 709495 709496 709497 [...] (17667 flits)
Class 0:
Remaining flits: 713106 713107 713108 713109 713110 713111 713112 713113 713114 713115 [...] (17840 flits)
Measured flits: 713106 713107 713108 713109 713110 713111 713112 713113 713114 713115 [...] (17840 flits)
Class 0:
Remaining flits: 747216 747217 747218 747219 747220 747221 747222 747223 747224 747225 [...] (18026 flits)
Measured flits: 747216 747217 747218 747219 747220 747221 747222 747223 747224 747225 [...] (18026 flits)
Class 0:
Remaining flits: 820838 820839 820840 820841 820842 820843 820844 820845 820846 820847 [...] (17670 flits)
Measured flits: 820838 820839 820840 820841 820842 820843 820844 820845 820846 820847 [...] (17670 flits)
Class 0:
Remaining flits: 830466 830467 830468 830469 830470 830471 830472 830473 830474 830475 [...] (17719 flits)
Measured flits: 830466 830467 830468 830469 830470 830471 830472 830473 830474 830475 [...] (17719 flits)
Class 0:
Remaining flits: 879246 879247 879248 879249 879250 879251 879252 879253 879254 879255 [...] (17920 flits)
Measured flits: 879246 879247 879248 879249 879250 879251 879252 879253 879254 879255 [...] (17920 flits)
Class 0:
Remaining flits: 889254 889255 889256 889257 889258 889259 889260 889261 889262 889263 [...] (17888 flits)
Measured flits: 889254 889255 889256 889257 889258 889259 889260 889261 889262 889263 [...] (17888 flits)
Class 0:
Remaining flits: 916758 916759 916760 916761 916762 916763 916764 916765 916766 916767 [...] (17733 flits)
Measured flits: 916758 916759 916760 916761 916762 916763 916764 916765 916766 916767 [...] (17733 flits)
Class 0:
Remaining flits: 969246 969247 969248 969249 969250 969251 969252 969253 969254 969255 [...] (17290 flits)
Measured flits: 969246 969247 969248 969249 969250 969251 969252 969253 969254 969255 [...] (17290 flits)
Class 0:
Remaining flits: 980550 980551 980552 980553 980554 980555 980556 980557 980558 980559 [...] (17602 flits)
Measured flits: 980550 980551 980552 980553 980554 980555 980556 980557 980558 980559 [...] (17602 flits)
Class 0:
Remaining flits: 1025442 1025443 1025444 1025445 1025446 1025447 1025448 1025449 1025450 1025451 [...] (17731 flits)
Measured flits: 1025442 1025443 1025444 1025445 1025446 1025447 1025448 1025449 1025450 1025451 [...] (17731 flits)
Class 0:
Remaining flits: 1029240 1029241 1029242 1029243 1029244 1029245 1029246 1029247 1029248 1029249 [...] (17664 flits)
Measured flits: 1029240 1029241 1029242 1029243 1029244 1029245 1029246 1029247 1029248 1029249 [...] (17664 flits)
Class 0:
Remaining flits: 1076202 1076203 1076204 1076205 1076206 1076207 1076208 1076209 1076210 1076211 [...] (17701 flits)
Measured flits: 1076202 1076203 1076204 1076205 1076206 1076207 1076208 1076209 1076210 1076211 [...] (17701 flits)
Class 0:
Remaining flits: 1124640 1124641 1124642 1124643 1124644 1124645 1124646 1124647 1124648 1124649 [...] (17954 flits)
Measured flits: 1124640 1124641 1124642 1124643 1124644 1124645 1124646 1124647 1124648 1124649 [...] (17954 flits)
Class 0:
Remaining flits: 1150686 1150687 1150688 1150689 1150690 1150691 1150692 1150693 1150694 1150695 [...] (17822 flits)
Measured flits: 1150686 1150687 1150688 1150689 1150690 1150691 1150692 1150693 1150694 1150695 [...] (17786 flits)
Class 0:
Remaining flits: 1191950 1191951 1191952 1191953 1191954 1191955 1191956 1191957 1191958 1191959 [...] (18038 flits)
Measured flits: 1191950 1191951 1191952 1191953 1191954 1191955 1191956 1191957 1191958 1191959 [...] (17930 flits)
Class 0:
Remaining flits: 1208934 1208935 1208936 1208937 1208938 1208939 1208940 1208941 1208942 1208943 [...] (17624 flits)
Measured flits: 1208934 1208935 1208936 1208937 1208938 1208939 1208940 1208941 1208942 1208943 [...] (17499 flits)
Class 0:
Remaining flits: 1240246 1240247 1240248 1240249 1240250 1240251 1240252 1240253 1245456 1245457 [...] (17911 flits)
Measured flits: 1240246 1240247 1240248 1240249 1240250 1240251 1240252 1240253 1245456 1245457 [...] (17609 flits)
Class 0:
Remaining flits: 1280610 1280611 1280612 1280613 1280614 1280615 1280616 1280617 1280618 1280619 [...] (17361 flits)
Measured flits: 1280610 1280611 1280612 1280613 1280614 1280615 1280616 1280617 1280618 1280619 [...] (16771 flits)
Class 0:
Remaining flits: 1303339 1303340 1303341 1303342 1303343 1304648 1304649 1304650 1304651 1304652 [...] (18166 flits)
Measured flits: 1303339 1303340 1303341 1303342 1303343 1304648 1304649 1304650 1304651 1304652 [...] (17345 flits)
Class 0:
Remaining flits: 1350144 1350145 1350146 1350147 1350148 1350149 1350150 1350151 1350152 1350153 [...] (18217 flits)
Measured flits: 1350144 1350145 1350146 1350147 1350148 1350149 1350150 1350151 1350152 1350153 [...] (16748 flits)
Class 0:
Remaining flits: 1366488 1366489 1366490 1366491 1366492 1366493 1366494 1366495 1366496 1366497 [...] (17938 flits)
Measured flits: 1366488 1366489 1366490 1366491 1366492 1366493 1366494 1366495 1366496 1366497 [...] (15925 flits)
Class 0:
Remaining flits: 1399068 1399069 1399070 1399071 1399072 1399073 1399074 1399075 1399076 1399077 [...] (17628 flits)
Measured flits: 1399068 1399069 1399070 1399071 1399072 1399073 1399074 1399075 1399076 1399077 [...] (14871 flits)
Class 0:
Remaining flits: 1442520 1442521 1442522 1442523 1442524 1442525 1442526 1442527 1442528 1442529 [...] (17931 flits)
Measured flits: 1442520 1442521 1442522 1442523 1442524 1442525 1442526 1442527 1442528 1442529 [...] (14222 flits)
Class 0:
Remaining flits: 1477836 1477837 1477838 1477839 1477840 1477841 1477842 1477843 1477844 1477845 [...] (17978 flits)
Measured flits: 1477836 1477837 1477838 1477839 1477840 1477841 1477842 1477843 1477844 1477845 [...] (12754 flits)
Class 0:
Remaining flits: 1483848 1483849 1483850 1483851 1483852 1483853 1483854 1483855 1483856 1483857 [...] (17625 flits)
Measured flits: 1485630 1485631 1485632 1485633 1485634 1485635 1485636 1485637 1485638 1485639 [...] (10935 flits)
Class 0:
Remaining flits: 1512522 1512523 1512524 1512525 1512526 1512527 1512528 1512529 1512530 1512531 [...] (17121 flits)
Measured flits: 1512522 1512523 1512524 1512525 1512526 1512527 1512528 1512529 1512530 1512531 [...] (9476 flits)
Class 0:
Remaining flits: 1542600 1542601 1542602 1542603 1542604 1542605 1542606 1542607 1542608 1542609 [...] (17321 flits)
Measured flits: 1542600 1542601 1542602 1542603 1542604 1542605 1542606 1542607 1542608 1542609 [...] (8126 flits)
Class 0:
Remaining flits: 1557702 1557703 1557704 1557705 1557706 1557707 1557708 1557709 1557710 1557711 [...] (18076 flits)
Measured flits: 1557702 1557703 1557704 1557705 1557706 1557707 1557708 1557709 1557710 1557711 [...] (6845 flits)
Class 0:
Remaining flits: 1607336 1607337 1607338 1607339 1607340 1607341 1607342 1607343 1607344 1607345 [...] (17789 flits)
Measured flits: 1620702 1620703 1620704 1620705 1620706 1620707 1620708 1620709 1620710 1620711 [...] (5627 flits)
Class 0:
Remaining flits: 1625148 1625149 1625150 1625151 1625152 1625153 1625154 1625155 1625156 1625157 [...] (18108 flits)
Measured flits: 1625148 1625149 1625150 1625151 1625152 1625153 1625154 1625155 1625156 1625157 [...] (5029 flits)
Class 0:
Remaining flits: 1676164 1676165 1676166 1676167 1676168 1676169 1676170 1676171 1676172 1676173 [...] (17514 flits)
Measured flits: 1680390 1680391 1680392 1680393 1680394 1680395 1680396 1680397 1680398 1680399 [...] (3360 flits)
Class 0:
Remaining flits: 1697868 1697869 1697870 1697871 1697872 1697873 1697874 1697875 1697876 1697877 [...] (17502 flits)
Measured flits: 1697868 1697869 1697870 1697871 1697872 1697873 1697874 1697875 1697876 1697877 [...] (2580 flits)
Class 0:
Remaining flits: 1738674 1738675 1738676 1738677 1738678 1738679 1738680 1738681 1738682 1738683 [...] (17651 flits)
Measured flits: 1745676 1745677 1745678 1745679 1745680 1745681 1745682 1745683 1745684 1745685 [...] (1610 flits)
Class 0:
Remaining flits: 1758510 1758511 1758512 1758513 1758514 1758515 1758516 1758517 1758518 1758519 [...] (17562 flits)
Measured flits: 1766142 1766143 1766144 1766145 1766146 1766147 1766148 1766149 1766150 1766151 [...] (1219 flits)
Class 0:
Remaining flits: 1781406 1781407 1781408 1781409 1781410 1781411 1781412 1781413 1781414 1781415 [...] (17965 flits)
Measured flits: 1810656 1810657 1810658 1810659 1810660 1810661 1810662 1810663 1810664 1810665 [...] (1158 flits)
Class 0:
Remaining flits: 1813410 1813411 1813412 1813413 1813414 1813415 1813416 1813417 1813418 1813419 [...] (18086 flits)
Measured flits: 1859033 1859034 1859035 1859036 1859037 1859038 1859039 1866312 1866313 1866314 [...] (716 flits)
Class 0:
Remaining flits: 1848222 1848223 1848224 1848225 1848226 1848227 1848228 1848229 1848230 1848231 [...] (17734 flits)
Measured flits: 1885500 1885501 1885502 1885503 1885504 1885505 1885506 1885507 1885508 1885509 [...] (442 flits)
Class 0:
Remaining flits: 1876482 1876483 1876484 1876485 1876486 1876487 1876488 1876489 1876490 1876491 [...] (17737 flits)
Measured flits: 1947312 1947313 1947314 1947315 1947316 1947317 1947318 1947319 1947320 1947321 [...] (270 flits)
Class 0:
Remaining flits: 1924164 1924165 1924166 1924167 1924168 1924169 1924170 1924171 1924172 1924173 [...] (17912 flits)
Measured flits: 1980435 1980436 1980437 1980438 1980439 1980440 1980441 1980442 1980443 1980444 [...] (314 flits)
Class 0:
Remaining flits: 1966329 1966330 1966331 1966332 1966333 1966334 1966335 1966336 1966337 1966542 [...] (17706 flits)
Measured flits: 1997496 1997497 1997498 1997499 1997500 1997501 1997502 1997503 1997504 1997505 [...] (251 flits)
Class 0:
Remaining flits: 1978904 1978905 1978906 1978907 1978908 1978909 1978910 1978911 1978912 1978913 [...] (17396 flits)
Measured flits: 2027646 2027647 2027648 2027649 2027650 2027651 2027652 2027653 2027654 2027655 [...] (146 flits)
Class 0:
Remaining flits: 2025107 2025738 2025739 2025740 2025741 2025742 2025743 2025744 2025745 2025746 [...] (17610 flits)
Measured flits: 2066508 2066509 2066510 2066511 2066512 2066513 2066514 2066515 2066516 2066517 [...] (142 flits)
Class 0:
Remaining flits: 2038302 2038303 2038304 2038305 2038306 2038307 2038308 2038309 2038310 2038311 [...] (17368 flits)
Measured flits: 2078316 2078317 2078318 2078319 2078320 2078321 2078322 2078323 2078324 2078325 [...] (90 flits)
Class 0:
Remaining flits: 2092158 2092159 2092160 2092161 2092162 2092163 2092164 2092165 2092166 2092167 [...] (18039 flits)
Measured flits: 2130666 2130667 2130668 2130669 2130670 2130671 2130672 2130673 2130674 2130675 [...] (132 flits)
Class 0:
Remaining flits: 2105766 2105767 2105768 2105769 2105770 2105771 2105772 2105773 2105774 2105775 [...] (17610 flits)
Measured flits: 2161854 2161855 2161856 2161857 2161858 2161859 2161860 2161861 2161862 2161863 [...] (72 flits)
Draining remaining packets ...
Time taken is 71385 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 22842.8 (1 samples)
	minimum = 1860 (1 samples)
	maximum = 60608 (1 samples)
Network latency average = 548.328 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4021 (1 samples)
Flit latency average = 488.594 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3964 (1 samples)
Fragmentation average = 58.4083 (1 samples)
	minimum = 0 (1 samples)
	maximum = 159 (1 samples)
Injected packet rate average = 0.00898958 (1 samples)
	minimum = 0.00457143 (1 samples)
	maximum = 0.0157143 (1 samples)
Accepted packet rate average = 0.00899851 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0121429 (1 samples)
Injected flit rate average = 0.161756 (1 samples)
	minimum = 0.0821429 (1 samples)
	maximum = 0.283286 (1 samples)
Accepted flit rate average = 0.16191 (1 samples)
	minimum = 0.113429 (1 samples)
	maximum = 0.217857 (1 samples)
Injected packet size average = 17.9937 (1 samples)
Accepted packet size average = 17.993 (1 samples)
Hops average = 5.065 (1 samples)
Total run time 48.5241
