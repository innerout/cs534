BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.2
	minimum = 22
	maximum = 879
Network latency average = 299.369
	minimum = 22
	maximum = 832
Slowest packet = 237
Flit latency average = 264.628
	minimum = 5
	maximum = 861
Slowest flit = 16357
Fragmentation average = 84.9049
	minimum = 0
	maximum = 447
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0150573
	minimum = 0.007 (at node 115)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.294193
	minimum = 0.141 (at node 150)
	maximum = 0.436 (at node 70)
Injected packet length average = 17.8433
Accepted packet length average = 19.5382
Total in-flight flits = 107351 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 603.013
	minimum = 22
	maximum = 1762
Network latency average = 565.972
	minimum = 22
	maximum = 1697
Slowest packet = 1065
Flit latency average = 525.223
	minimum = 5
	maximum = 1701
Slowest flit = 42908
Fragmentation average = 113.121
	minimum = 0
	maximum = 455
Injected packet rate average = 0.0490573
	minimum = 0.039 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0162995
	minimum = 0.01 (at node 83)
	maximum = 0.0245 (at node 152)
Injected flit rate average = 0.878964
	minimum = 0.698 (at node 43)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.307635
	minimum = 0.183 (at node 83)
	maximum = 0.45 (at node 177)
Injected packet length average = 17.9171
Accepted packet length average = 18.8739
Total in-flight flits = 220952 (0 measured)
latency change    = 0.462368
throughput change = 0.0436969
Class 0:
Packet latency average = 1402.09
	minimum = 23
	maximum = 2619
Network latency average = 1343.67
	minimum = 22
	maximum = 2527
Slowest packet = 3074
Flit latency average = 1306.76
	minimum = 5
	maximum = 2539
Slowest flit = 68845
Fragmentation average = 150.309
	minimum = 0
	maximum = 487
Injected packet rate average = 0.050625
	minimum = 0.036 (at node 119)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0179063
	minimum = 0.008 (at node 20)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.91126
	minimum = 0.651 (at node 119)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.320365
	minimum = 0.156 (at node 20)
	maximum = 0.544 (at node 151)
Injected packet length average = 18.0002
Accepted packet length average = 17.8912
Total in-flight flits = 334402 (0 measured)
latency change    = 0.569919
throughput change = 0.0397334
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 123.699
	minimum = 25
	maximum = 565
Network latency average = 40.6645
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 1859.65
	minimum = 5
	maximum = 3262
Slowest flit = 114371
Fragmentation average = 6.60634
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0511719
	minimum = 0.037 (at node 10)
	maximum = 0.056 (at node 18)
Accepted packet rate average = 0.0176042
	minimum = 0.007 (at node 39)
	maximum = 0.028 (at node 123)
Injected flit rate average = 0.921703
	minimum = 0.68 (at node 10)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.319031
	minimum = 0.158 (at node 180)
	maximum = 0.512 (at node 121)
Injected packet length average = 18.0119
Accepted packet length average = 18.1225
Total in-flight flits = 449998 (163052 measured)
latency change    = 10.3347
throughput change = 0.00417932
Class 0:
Packet latency average = 129.195
	minimum = 23
	maximum = 583
Network latency average = 41.4117
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 2123.26
	minimum = 5
	maximum = 4095
Slowest flit = 144801
Fragmentation average = 6.65756
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0512031
	minimum = 0.0395 (at node 43)
	maximum = 0.056 (at node 32)
Accepted packet rate average = 0.0176771
	minimum = 0.0115 (at node 36)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.921833
	minimum = 0.71 (at node 43)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.319669
	minimum = 0.212 (at node 60)
	maximum = 0.462 (at node 90)
Injected packet length average = 18.0035
Accepted packet length average = 18.0838
Total in-flight flits = 565565 (325470 measured)
latency change    = 0.0425421
throughput change = 0.00199588
Class 0:
Packet latency average = 127.103
	minimum = 23
	maximum = 671
Network latency average = 41.2017
	minimum = 22
	maximum = 283
Slowest packet = 28570
Flit latency average = 2390.3
	minimum = 5
	maximum = 4883
Slowest flit = 180516
Fragmentation average = 6.63235
	minimum = 0
	maximum = 49
Injected packet rate average = 0.0510625
	minimum = 0.041 (at node 43)
	maximum = 0.0556667 (at node 15)
Accepted packet rate average = 0.0177083
	minimum = 0.0116667 (at node 36)
	maximum = 0.026 (at node 128)
Injected flit rate average = 0.918997
	minimum = 0.737333 (at node 43)
	maximum = 1 (at node 4)
Accepted flit rate average= 0.319351
	minimum = 0.213667 (at node 36)
	maximum = 0.461667 (at node 128)
Injected packet length average = 17.9975
Accepted packet length average = 18.0339
Total in-flight flits = 679872 (486463 measured)
latency change    = 0.0164599
throughput change = 0.000997575
Draining remaining packets ...
Class 0:
Remaining flits: 230777 251557 251558 251559 251560 251561 251562 251563 251564 251565 [...] (631938 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (485925 flits)
Class 0:
Remaining flits: 274422 274423 274424 274425 274426 274427 274747 274748 274749 274750 [...] (584540 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (485371 flits)
Class 0:
Remaining flits: 325044 325045 325046 325047 325048 325049 325050 325051 325052 325053 [...] (537043 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (481841 flits)
Class 0:
Remaining flits: 362897 369405 369406 369407 369408 369409 369410 369411 369412 369413 [...] (489685 flits)
Measured flits: 514044 514045 514046 514047 514048 514049 514050 514051 514052 514053 [...] (467542 flits)
Class 0:
Remaining flits: 378918 378919 378920 378921 378922 378923 378924 378925 378926 378927 [...] (442405 flits)
Measured flits: 514048 514049 514050 514051 514052 514053 514054 514055 514056 514057 [...] (436792 flits)
Class 0:
Remaining flits: 427968 427969 427970 427971 427972 427973 427974 427975 427976 427977 [...] (395161 flits)
Measured flits: 514111 514112 514113 514114 514115 514278 514279 514280 514281 514282 [...] (394350 flits)
Class 0:
Remaining flits: 476298 476299 476300 476301 476302 476303 476304 476305 476306 476307 [...] (347515 flits)
Measured flits: 514890 514891 514892 514893 514894 514895 514896 514897 514898 514899 [...] (347428 flits)
Class 0:
Remaining flits: 495029 495030 495031 495032 495033 495034 495035 536616 536617 536618 [...] (300338 flits)
Measured flits: 536616 536617 536618 536619 536620 536621 536622 536623 536624 536625 [...] (300331 flits)
Class 0:
Remaining flits: 564984 564985 564986 564987 564988 564989 564990 564991 564992 564993 [...] (253087 flits)
Measured flits: 564984 564985 564986 564987 564988 564989 564990 564991 564992 564993 [...] (253087 flits)
Class 0:
Remaining flits: 596682 596683 596684 596685 596686 596687 596688 596689 596690 596691 [...] (205436 flits)
Measured flits: 596682 596683 596684 596685 596686 596687 596688 596689 596690 596691 [...] (205436 flits)
Class 0:
Remaining flits: 664397 677286 677287 677288 677289 677290 677291 677292 677293 677294 [...] (158267 flits)
Measured flits: 664397 677286 677287 677288 677289 677290 677291 677292 677293 677294 [...] (158267 flits)
Class 0:
Remaining flits: 698598 698599 698600 698601 698602 698603 698604 698605 698606 698607 [...] (110865 flits)
Measured flits: 698598 698599 698600 698601 698602 698603 698604 698605 698606 698607 [...] (110865 flits)
Class 0:
Remaining flits: 767412 767413 767414 767415 767416 767417 767418 767419 767420 767421 [...] (64190 flits)
Measured flits: 767412 767413 767414 767415 767416 767417 767418 767419 767420 767421 [...] (64190 flits)
Class 0:
Remaining flits: 799372 799373 799374 799375 799376 799377 799378 799379 817847 830352 [...] (20285 flits)
Measured flits: 799372 799373 799374 799375 799376 799377 799378 799379 817847 830352 [...] (20285 flits)
Class 0:
Remaining flits: 897832 897833 897834 897835 897836 897837 897838 897839 929358 929359 [...] (1184 flits)
Measured flits: 897832 897833 897834 897835 897836 897837 897838 897839 929358 929359 [...] (1184 flits)
Time taken is 21502 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9980.25 (1 samples)
	minimum = 23 (1 samples)
	maximum = 16263 (1 samples)
Network latency average = 9890.52 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16007 (1 samples)
Flit latency average = 7805.89 (1 samples)
	minimum = 5 (1 samples)
	maximum = 15990 (1 samples)
Fragmentation average = 161.564 (1 samples)
	minimum = 0 (1 samples)
	maximum = 454 (1 samples)
Injected packet rate average = 0.0510625 (1 samples)
	minimum = 0.041 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0177083 (1 samples)
	minimum = 0.0116667 (1 samples)
	maximum = 0.026 (1 samples)
Injected flit rate average = 0.918997 (1 samples)
	minimum = 0.737333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.319351 (1 samples)
	minimum = 0.213667 (1 samples)
	maximum = 0.461667 (1 samples)
Injected packet size average = 17.9975 (1 samples)
Accepted packet size average = 18.0339 (1 samples)
Hops average = 5.07089 (1 samples)
Total run time 21.4025
