BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.053317
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.306
	minimum = 23
	maximum = 984
Network latency average = 315.751
	minimum = 23
	maximum = 984
Slowest packet = 54
Flit latency average = 261.714
	minimum = 6
	maximum = 967
Slowest flit = 989
Fragmentation average = 152.592
	minimum = 0
	maximum = 946
Injected packet rate average = 0.0478646
	minimum = 0.033 (at node 103)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0119844
	minimum = 0.004 (at node 62)
	maximum = 0.023 (at node 114)
Injected flit rate average = 0.853906
	minimum = 0.594 (at node 103)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.244349
	minimum = 0.096 (at node 62)
	maximum = 0.427 (at node 114)
Injected packet length average = 17.84
Accepted packet length average = 20.389
Total in-flight flits = 118523 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 686.173
	minimum = 23
	maximum = 1896
Network latency average = 646.912
	minimum = 23
	maximum = 1894
Slowest packet = 375
Flit latency average = 581.968
	minimum = 6
	maximum = 1937
Slowest flit = 7565
Fragmentation average = 189.118
	minimum = 0
	maximum = 1845
Injected packet rate average = 0.0455859
	minimum = 0.027 (at node 152)
	maximum = 0.056 (at node 101)
Accepted packet rate average = 0.0123568
	minimum = 0.007 (at node 62)
	maximum = 0.0195 (at node 78)
Injected flit rate average = 0.816292
	minimum = 0.485 (at node 152)
	maximum = 1 (at node 101)
Accepted flit rate average= 0.236292
	minimum = 0.136 (at node 153)
	maximum = 0.369 (at node 78)
Injected packet length average = 17.9067
Accepted packet length average = 19.1224
Total in-flight flits = 224390 (0 measured)
latency change    = 0.499681
throughput change = 0.0340989
Class 0:
Packet latency average = 1804.68
	minimum = 33
	maximum = 2842
Network latency average = 1718.51
	minimum = 28
	maximum = 2822
Slowest packet = 980
Flit latency average = 1675.98
	minimum = 6
	maximum = 2851
Slowest flit = 19622
Fragmentation average = 233.143
	minimum = 0
	maximum = 1823
Injected packet rate average = 0.0328177
	minimum = 0.007 (at node 28)
	maximum = 0.056 (at node 179)
Accepted packet rate average = 0.0121042
	minimum = 0.004 (at node 13)
	maximum = 0.022 (at node 34)
Injected flit rate average = 0.592458
	minimum = 0.13 (at node 28)
	maximum = 1 (at node 179)
Accepted flit rate average= 0.217396
	minimum = 0.072 (at node 81)
	maximum = 0.38 (at node 99)
Injected packet length average = 18.053
Accepted packet length average = 17.9604
Total in-flight flits = 296122 (0 measured)
latency change    = 0.619782
throughput change = 0.086919
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1430.66
	minimum = 32
	maximum = 2537
Network latency average = 41.3333
	minimum = 31
	maximum = 130
Slowest packet = 23905
Flit latency average = 2376.3
	minimum = 6
	maximum = 3794
Slowest flit = 26585
Fragmentation average = 11.0392
	minimum = 4
	maximum = 27
Injected packet rate average = 0.01675
	minimum = 0.001 (at node 69)
	maximum = 0.035 (at node 71)
Accepted packet rate average = 0.011974
	minimum = 0.004 (at node 77)
	maximum = 0.022 (at node 19)
Injected flit rate average = 0.301349
	minimum = 0.024 (at node 69)
	maximum = 0.626 (at node 71)
Accepted flit rate average= 0.215333
	minimum = 0.096 (at node 168)
	maximum = 0.399 (at node 19)
Injected packet length average = 17.991
Accepted packet length average = 17.9835
Total in-flight flits = 312792 (56178 measured)
latency change    = 0.261436
throughput change = 0.00957817
Class 0:
Packet latency average = 1867.28
	minimum = 32
	maximum = 3826
Network latency average = 185.174
	minimum = 28
	maximum = 1958
Slowest packet = 23905
Flit latency average = 2701.05
	minimum = 6
	maximum = 4676
Slowest flit = 47431
Fragmentation average = 26.3632
	minimum = 4
	maximum = 278
Injected packet rate average = 0.0159089
	minimum = 0.0075 (at node 116)
	maximum = 0.026 (at node 71)
Accepted packet rate average = 0.0118359
	minimum = 0.0065 (at node 92)
	maximum = 0.0205 (at node 16)
Injected flit rate average = 0.286211
	minimum = 0.1385 (at node 116)
	maximum = 0.467 (at node 71)
Accepted flit rate average= 0.211789
	minimum = 0.12 (at node 92)
	maximum = 0.3555 (at node 16)
Injected packet length average = 17.9907
Accepted packet length average = 17.8937
Total in-flight flits = 324847 (106266 measured)
latency change    = 0.233828
throughput change = 0.0167349
Class 0:
Packet latency average = 2738.24
	minimum = 32
	maximum = 4619
Network latency average = 784.533
	minimum = 28
	maximum = 2936
Slowest packet = 23905
Flit latency average = 3010.39
	minimum = 6
	maximum = 5683
Slowest flit = 47996
Fragmentation average = 69.7
	minimum = 1
	maximum = 421
Injected packet rate average = 0.0154688
	minimum = 0.00933333 (at node 75)
	maximum = 0.0236667 (at node 71)
Accepted packet rate average = 0.0117569
	minimum = 0.00733333 (at node 86)
	maximum = 0.0186667 (at node 16)
Injected flit rate average = 0.27834
	minimum = 0.169333 (at node 75)
	maximum = 0.422333 (at node 71)
Accepted flit rate average= 0.210085
	minimum = 0.134333 (at node 92)
	maximum = 0.331667 (at node 129)
Injected packet length average = 17.9937
Accepted packet length average = 17.869
Total in-flight flits = 335745 (152107 measured)
latency change    = 0.318074
throughput change = 0.00811097
Draining remaining packets ...
Class 0:
Remaining flits: 44870 44871 44872 44873 45843 45844 45845 51246 51247 51248 [...] (298005 flits)
Measured flits: 428598 428599 428600 428601 428602 428603 428604 428605 428606 428607 [...] (145665 flits)
Class 0:
Remaining flits: 58589 70200 70201 70202 70203 70204 70205 70206 70207 70208 [...] (261314 flits)
Measured flits: 428598 428599 428600 428601 428602 428603 428604 428605 428606 428607 [...] (137057 flits)
Class 0:
Remaining flits: 74016 74017 74018 74019 74020 74021 74022 74023 74024 74025 [...] (225047 flits)
Measured flits: 428598 428599 428600 428601 428602 428603 428604 428605 428606 428607 [...] (127356 flits)
Class 0:
Remaining flits: 100099 100100 100101 100102 100103 100104 100105 100106 100107 100108 [...] (189682 flits)
Measured flits: 428598 428599 428600 428601 428602 428603 428604 428605 428606 428607 [...] (115833 flits)
Class 0:
Remaining flits: 100152 100153 100154 100155 100156 100157 100158 100159 100160 100161 [...] (155352 flits)
Measured flits: 428598 428599 428600 428601 428602 428603 428604 428605 428606 428607 [...] (101299 flits)
Class 0:
Remaining flits: 113436 113437 113438 113439 113440 113441 113442 113443 113444 113445 [...] (121593 flits)
Measured flits: 428634 428635 428636 428637 428638 428639 428640 428641 428642 428643 [...] (84130 flits)
Class 0:
Remaining flits: 113436 113437 113438 113439 113440 113441 113442 113443 113444 113445 [...] (88035 flits)
Measured flits: 428636 428637 428638 428639 428640 428641 428642 428643 428644 428645 [...] (63120 flits)
Class 0:
Remaining flits: 113436 113437 113438 113439 113440 113441 113442 113443 113444 113445 [...] (55953 flits)
Measured flits: 428742 428743 428744 428745 428746 428747 428748 428749 428750 428751 [...] (41233 flits)
Class 0:
Remaining flits: 113446 113447 113448 113449 113450 113451 113452 113453 162414 162415 [...] (26983 flits)
Measured flits: 428742 428743 428744 428745 428746 428747 428748 428749 428750 428751 [...] (20360 flits)
Class 0:
Remaining flits: 162414 162415 162416 162417 162418 162419 162420 162421 162422 162423 [...] (7069 flits)
Measured flits: 429462 429463 429464 429465 429466 429467 429468 429469 429470 429471 [...] (5346 flits)
Class 0:
Remaining flits: 162425 162426 162427 162428 162429 162430 162431 254538 254539 254540 [...] (1299 flits)
Measured flits: 440172 440173 440174 440175 440176 440177 440178 440179 440180 440181 [...] (960 flits)
Class 0:
Remaining flits: 335269 335270 335271 335272 335273 335274 335275 335276 335277 335278 [...] (19 flits)
Measured flits: 492316 492317 (2 flits)
Time taken is 18019 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8943.36 (1 samples)
	minimum = 32 (1 samples)
	maximum = 16247 (1 samples)
Network latency average = 7288.72 (1 samples)
	minimum = 28 (1 samples)
	maximum = 14388 (1 samples)
Flit latency average = 6383.99 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16535 (1 samples)
Fragmentation average = 156.337 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2914 (1 samples)
Injected packet rate average = 0.0154688 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0236667 (1 samples)
Accepted packet rate average = 0.0117569 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.27834 (1 samples)
	minimum = 0.169333 (1 samples)
	maximum = 0.422333 (1 samples)
Accepted flit rate average = 0.210085 (1 samples)
	minimum = 0.134333 (1 samples)
	maximum = 0.331667 (1 samples)
Injected packet size average = 17.9937 (1 samples)
Accepted packet size average = 17.869 (1 samples)
Hops average = 5.08259 (1 samples)
Total run time 18.6469
