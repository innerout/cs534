BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 338.79
	minimum = 22
	maximum = 908
Network latency average = 308.19
	minimum = 22
	maximum = 846
Slowest packet = 83
Flit latency average = 271.341
	minimum = 5
	maximum = 867
Slowest flit = 18774
Fragmentation average = 88.2512
	minimum = 0
	maximum = 422
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0152188
	minimum = 0.005 (at node 174)
	maximum = 0.026 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.296667
	minimum = 0.125 (at node 174)
	maximum = 0.49 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 19.4935
Total in-flight flits = 116146 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 619.885
	minimum = 22
	maximum = 1830
Network latency average = 569.177
	minimum = 22
	maximum = 1715
Slowest packet = 1141
Flit latency average = 526.827
	minimum = 5
	maximum = 1724
Slowest flit = 42147
Fragmentation average = 114.564
	minimum = 0
	maximum = 439
Injected packet rate average = 0.051625
	minimum = 0.04 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0165286
	minimum = 0.01 (at node 116)
	maximum = 0.023 (at node 78)
Injected flit rate average = 0.925266
	minimum = 0.713 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.310945
	minimum = 0.1985 (at node 116)
	maximum = 0.4275 (at node 103)
Injected packet length average = 17.9228
Accepted packet length average = 18.8125
Total in-flight flits = 237429 (0 measured)
latency change    = 0.453463
throughput change = 0.0459201
Class 0:
Packet latency average = 1458.69
	minimum = 24
	maximum = 2739
Network latency average = 1373.73
	minimum = 22
	maximum = 2648
Slowest packet = 2415
Flit latency average = 1339.71
	minimum = 5
	maximum = 2715
Slowest flit = 43461
Fragmentation average = 146.898
	minimum = 0
	maximum = 491
Injected packet rate average = 0.0538177
	minimum = 0.043 (at node 63)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0176094
	minimum = 0.007 (at node 149)
	maximum = 0.027 (at node 3)
Injected flit rate average = 0.968359
	minimum = 0.78 (at node 63)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.317604
	minimum = 0.114 (at node 149)
	maximum = 0.498 (at node 3)
Injected packet length average = 17.9933
Accepted packet length average = 18.0361
Total in-flight flits = 362443 (0 measured)
latency change    = 0.575041
throughput change = 0.0209659
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 225.639
	minimum = 28
	maximum = 679
Network latency average = 41.2127
	minimum = 22
	maximum = 334
Slowest packet = 30185
Flit latency average = 1871.55
	minimum = 5
	maximum = 3351
Slowest flit = 104901
Fragmentation average = 6.44866
	minimum = 0
	maximum = 41
Injected packet rate average = 0.0543333
	minimum = 0.045 (at node 18)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0179531
	minimum = 0.009 (at node 55)
	maximum = 0.031 (at node 0)
Injected flit rate average = 0.978068
	minimum = 0.808 (at node 18)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.323589
	minimum = 0.158 (at node 55)
	maximum = 0.561 (at node 181)
Injected packet length average = 18.0012
Accepted packet length average = 18.0241
Total in-flight flits = 488090 (172948 measured)
latency change    = 5.4647
throughput change = 0.0184938
Class 0:
Packet latency average = 237.179
	minimum = 24
	maximum = 836
Network latency average = 41.6503
	minimum = 22
	maximum = 405
Slowest packet = 30185
Flit latency average = 2142.47
	minimum = 5
	maximum = 4056
Slowest flit = 151091
Fragmentation average = 6.38852
	minimum = 0
	maximum = 41
Injected packet rate average = 0.054349
	minimum = 0.0465 (at node 18)
	maximum = 0.056 (at node 10)
Accepted packet rate average = 0.0180391
	minimum = 0.0105 (at node 86)
	maximum = 0.026 (at node 0)
Injected flit rate average = 0.978453
	minimum = 0.8385 (at node 18)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.32506
	minimum = 0.195 (at node 86)
	maximum = 0.4565 (at node 0)
Injected packet length average = 18.0032
Accepted packet length average = 18.0198
Total in-flight flits = 613280 (345334 measured)
latency change    = 0.0486526
throughput change = 0.00452641
Class 0:
Packet latency average = 247.029
	minimum = 24
	maximum = 896
Network latency average = 41.385
	minimum = 22
	maximum = 405
Slowest packet = 30185
Flit latency average = 2401.73
	minimum = 5
	maximum = 4908
Slowest flit = 184385
Fragmentation average = 6.54039
	minimum = 0
	maximum = 43
Injected packet rate average = 0.0542639
	minimum = 0.047 (at node 162)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0181823
	minimum = 0.0123333 (at node 144)
	maximum = 0.0256667 (at node 129)
Injected flit rate average = 0.976606
	minimum = 0.846333 (at node 162)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.326995
	minimum = 0.216333 (at node 144)
	maximum = 0.449333 (at node 129)
Injected packet length average = 17.9973
Accepted packet length average = 17.9842
Total in-flight flits = 736702 (515879 measured)
latency change    = 0.039875
throughput change = 0.00591721
Class 0:
Packet latency average = 260.944
	minimum = 24
	maximum = 912
Network latency average = 41.2931
	minimum = 22
	maximum = 405
Slowest packet = 30185
Flit latency average = 2679.93
	minimum = 5
	maximum = 5770
Slowest flit = 209241
Fragmentation average = 6.51055
	minimum = 0
	maximum = 43
Injected packet rate average = 0.0542526
	minimum = 0.048 (at node 155)
	maximum = 0.05575 (at node 10)
Accepted packet rate average = 0.0180977
	minimum = 0.01375 (at node 84)
	maximum = 0.0245 (at node 72)
Injected flit rate average = 0.976676
	minimum = 0.86175 (at node 155)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.325799
	minimum = 0.2475 (at node 162)
	maximum = 0.434 (at node 128)
Injected packet length average = 18.0024
Accepted packet length average = 18.0023
Total in-flight flits = 862217 (688387 measured)
latency change    = 0.0533247
throughput change = 0.00366886
Class 0:
Packet latency average = 275.744
	minimum = 22
	maximum = 4975
Network latency average = 43.5899
	minimum = 22
	maximum = 4851
Slowest packet = 30750
Flit latency average = 2947.16
	minimum = 5
	maximum = 6539
Slowest flit = 252752
Fragmentation average = 6.6111
	minimum = 0
	maximum = 262
Injected packet rate average = 0.0543729
	minimum = 0.049 (at node 82)
	maximum = 0.0556 (at node 3)
Accepted packet rate average = 0.0180927
	minimum = 0.014 (at node 144)
	maximum = 0.0232 (at node 72)
Injected flit rate average = 0.978558
	minimum = 0.8806 (at node 82)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.32603
	minimum = 0.2508 (at node 144)
	maximum = 0.4184 (at node 72)
Injected packet length average = 17.9972
Accepted packet length average = 18.02
Total in-flight flits = 989018 (862138 measured)
latency change    = 0.0536739
throughput change = 0.000707693
Class 0:
Packet latency average = 338.475
	minimum = 22
	maximum = 6093
Network latency average = 94.8762
	minimum = 22
	maximum = 5928
Slowest packet = 30314
Flit latency average = 3230.08
	minimum = 5
	maximum = 7051
Slowest flit = 320831
Fragmentation average = 8.40644
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0541432
	minimum = 0.05 (at node 82)
	maximum = 0.0556667 (at node 4)
Accepted packet rate average = 0.0180868
	minimum = 0.0138333 (at node 144)
	maximum = 0.023 (at node 72)
Injected flit rate average = 0.97471
	minimum = 0.898 (at node 158)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.325653
	minimum = 0.250667 (at node 144)
	maximum = 0.41 (at node 72)
Injected packet length average = 18.0024
Accepted packet length average = 18.005
Total in-flight flits = 1110005 (1029466 measured)
latency change    = 0.185335
throughput change = 0.001159
Class 0:
Packet latency average = 687.577
	minimum = 22
	maximum = 7307
Network latency average = 434.738
	minimum = 22
	maximum = 6996
Slowest packet = 30314
Flit latency average = 3508.35
	minimum = 5
	maximum = 7929
Slowest flit = 368086
Fragmentation average = 18.1615
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0535179
	minimum = 0.0462857 (at node 128)
	maximum = 0.0555714 (at node 3)
Accepted packet rate average = 0.0180275
	minimum = 0.0144286 (at node 144)
	maximum = 0.0227143 (at node 181)
Injected flit rate average = 0.963329
	minimum = 0.833714 (at node 128)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.324451
	minimum = 0.258143 (at node 171)
	maximum = 0.406857 (at node 181)
Injected packet length average = 18.0001
Accepted packet length average = 17.9975
Total in-flight flits = 1221085 (1180288 measured)
latency change    = 0.507727
throughput change = 0.00370437
Draining all recorded packets ...
Class 0:
Remaining flits: 407725 407726 407727 407728 407729 407730 407731 407732 407733 407734 [...] (1327966 flits)
Measured flits: 542826 542827 542828 542829 542830 542831 542832 542833 542834 542835 [...] (1223676 flits)
Class 0:
Remaining flits: 468029 468030 468031 468032 468033 468034 468035 471366 471367 471368 [...] (1435361 flits)
Measured flits: 543006 543007 543008 543009 543010 543011 543012 543013 543014 543015 [...] (1199034 flits)
Class 0:
Remaining flits: 478098 478099 478100 478101 478102 478103 478104 478105 478106 478107 [...] (1544363 flits)
Measured flits: 545094 545095 545096 545097 545098 545099 545100 545101 545102 545103 [...] (1157324 flits)
Class 0:
Remaining flits: 546821 556486 556487 556874 556875 556876 556877 556878 556879 556880 [...] (1653200 flits)
Measured flits: 546821 556486 556487 556874 556875 556876 556877 556878 556879 556880 [...] (1110609 flits)
Class 0:
Remaining flits: 583452 583453 583454 583455 583456 583457 583458 583459 583460 583461 [...] (1761491 flits)
Measured flits: 583452 583453 583454 583455 583456 583457 583458 583459 583460 583461 [...] (1063264 flits)
Class 0:
Remaining flits: 620601 620602 620603 642840 642841 642842 642843 642844 642845 642846 [...] (1870107 flits)
Measured flits: 620601 620602 620603 642840 642841 642842 642843 642844 642845 642846 [...] (1016017 flits)
Class 0:
Remaining flits: 690961 690962 690963 690964 690965 710676 710677 710678 710679 710680 [...] (1980607 flits)
Measured flits: 690961 690962 690963 690964 690965 710676 710677 710678 710679 710680 [...] (968758 flits)
Class 0:
Remaining flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (2090049 flits)
Measured flits: 747936 747937 747938 747939 747940 747941 747942 747943 747944 747945 [...] (921384 flits)
Class 0:
Remaining flits: 760176 760177 760178 760179 760180 760181 760182 760183 760184 760185 [...] (2197917 flits)
Measured flits: 760176 760177 760178 760179 760180 760181 760182 760183 760184 760185 [...] (873922 flits)
Class 0:
Remaining flits: 817452 817453 817454 817455 817456 817457 817458 817459 817460 817461 [...] (2305183 flits)
Measured flits: 817452 817453 817454 817455 817456 817457 817458 817459 817460 817461 [...] (826678 flits)
Class 0:
Remaining flits: 844720 844721 858240 858241 858242 858243 858244 858245 858246 858247 [...] (2411947 flits)
Measured flits: 844720 844721 858240 858241 858242 858243 858244 858245 858246 858247 [...] (779480 flits)
Class 0:
Remaining flits: 891972 891973 891974 891975 891976 891977 891978 891979 891980 891981 [...] (2511670 flits)
Measured flits: 891972 891973 891974 891975 891976 891977 891978 891979 891980 891981 [...] (732037 flits)
Class 0:
Remaining flits: 891972 891973 891974 891975 891976 891977 891978 891979 891980 891981 [...] (2609566 flits)
Measured flits: 891972 891973 891974 891975 891976 891977 891978 891979 891980 891981 [...] (684520 flits)
Class 0:
Remaining flits: 925830 925831 925832 925833 925834 925835 925836 925837 925838 925839 [...] (2688109 flits)
Measured flits: 925830 925831 925832 925833 925834 925835 925836 925837 925838 925839 [...] (636883 flits)
Class 0:
Remaining flits: 999921 999922 999923 999924 999925 999926 999927 999928 999929 999930 [...] (2725933 flits)
Measured flits: 999921 999922 999923 999924 999925 999926 999927 999928 999929 999930 [...] (589091 flits)
Class 0:
Remaining flits: 1090530 1090531 1090532 1090533 1090534 1090535 1090536 1090537 1090538 1090539 [...] (2724199 flits)
Measured flits: 1090530 1090531 1090532 1090533 1090534 1090535 1090536 1090537 1090538 1090539 [...] (541115 flits)
Class 0:
Remaining flits: 1090543 1090544 1090545 1090546 1090547 1103994 1103995 1103996 1103997 1103998 [...] (2716698 flits)
Measured flits: 1090543 1090544 1090545 1090546 1090547 1103994 1103995 1103996 1103997 1103998 [...] (493318 flits)
Class 0:
Remaining flits: 1117418 1117419 1117420 1117421 1174698 1174699 1174700 1174701 1174702 1174703 [...] (2705384 flits)
Measured flits: 1117418 1117419 1117420 1117421 1174698 1174699 1174700 1174701 1174702 1174703 [...] (445417 flits)
Class 0:
Remaining flits: 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 1174708 1174709 [...] (2700162 flits)
Measured flits: 1174700 1174701 1174702 1174703 1174704 1174705 1174706 1174707 1174708 1174709 [...] (397623 flits)
Class 0:
Remaining flits: 1235502 1235503 1235504 1235505 1235506 1235507 1235508 1235509 1235510 1235511 [...] (2697826 flits)
Measured flits: 1235502 1235503 1235504 1235505 1235506 1235507 1235508 1235509 1235510 1235511 [...] (350337 flits)
Class 0:
Remaining flits: 1299816 1299817 1299818 1299819 1299820 1299821 1299822 1299823 1299824 1299825 [...] (2694017 flits)
Measured flits: 1299816 1299817 1299818 1299819 1299820 1299821 1299822 1299823 1299824 1299825 [...] (303071 flits)
Class 0:
Remaining flits: 1299832 1299833 1320372 1320373 1320374 1320375 1320376 1320377 1320378 1320379 [...] (2689919 flits)
Measured flits: 1299832 1299833 1320372 1320373 1320374 1320375 1320376 1320377 1320378 1320379 [...] (255930 flits)
Class 0:
Remaining flits: 1330650 1330651 1330652 1330653 1330654 1330655 1330656 1330657 1330658 1330659 [...] (2686339 flits)
Measured flits: 1330650 1330651 1330652 1330653 1330654 1330655 1330656 1330657 1330658 1330659 [...] (209589 flits)
Class 0:
Remaining flits: 1429398 1429399 1429400 1429401 1429402 1429403 1429404 1429405 1429406 1429407 [...] (2679003 flits)
Measured flits: 1429398 1429399 1429400 1429401 1429402 1429403 1429404 1429405 1429406 1429407 [...] (164483 flits)
Class 0:
Remaining flits: 1432746 1432747 1432748 1432749 1432750 1432751 1432752 1432753 1432754 1432755 [...] (2672743 flits)
Measured flits: 1432746 1432747 1432748 1432749 1432750 1432751 1432752 1432753 1432754 1432755 [...] (122224 flits)
Class 0:
Remaining flits: 1496412 1496413 1496414 1496415 1496416 1496417 1496418 1496419 1496420 1496421 [...] (2669380 flits)
Measured flits: 1496412 1496413 1496414 1496415 1496416 1496417 1496418 1496419 1496420 1496421 [...] (84378 flits)
Class 0:
Remaining flits: 1537830 1537831 1537832 1537833 1537834 1537835 1537836 1537837 1537838 1537839 [...] (2668762 flits)
Measured flits: 1537830 1537831 1537832 1537833 1537834 1537835 1537836 1537837 1537838 1537839 [...] (52731 flits)
Class 0:
Remaining flits: 1653156 1653157 1653158 1653159 1653160 1653161 1653162 1653163 1653164 1653165 [...] (2660808 flits)
Measured flits: 1653156 1653157 1653158 1653159 1653160 1653161 1653162 1653163 1653164 1653165 [...] (29839 flits)
Class 0:
Remaining flits: 1685925 1685926 1685927 1685928 1685929 1685930 1685931 1685932 1685933 1711782 [...] (2660468 flits)
Measured flits: 1685925 1685926 1685927 1685928 1685929 1685930 1685931 1685932 1685933 1711782 [...] (16118 flits)
Class 0:
Remaining flits: 1720078 1720079 1741896 1741897 1741898 1741899 1741900 1741901 1741902 1741903 [...] (2654560 flits)
Measured flits: 1720078 1720079 1741896 1741897 1741898 1741899 1741900 1741901 1741902 1741903 [...] (7976 flits)
Class 0:
Remaining flits: 1745010 1745011 1745012 1745013 1745014 1745015 1745016 1745017 1745018 1745019 [...] (2646824 flits)
Measured flits: 1745010 1745011 1745012 1745013 1745014 1745015 1745016 1745017 1745018 1745019 [...] (3939 flits)
Class 0:
Remaining flits: 1810825 1810826 1810827 1810828 1810829 1810830 1810831 1810832 1810833 1810834 [...] (2638799 flits)
Measured flits: 1810825 1810826 1810827 1810828 1810829 1810830 1810831 1810832 1810833 1810834 [...] (2062 flits)
Class 0:
Remaining flits: 1854522 1854523 1854524 1854525 1854526 1854527 1854528 1854529 1854530 1854531 [...] (2632799 flits)
Measured flits: 1854522 1854523 1854524 1854525 1854526 1854527 1854528 1854529 1854530 1854531 [...] (1201 flits)
Class 0:
Remaining flits: 1854522 1854523 1854524 1854525 1854526 1854527 1854528 1854529 1854530 1854531 [...] (2631616 flits)
Measured flits: 1854522 1854523 1854524 1854525 1854526 1854527 1854528 1854529 1854530 1854531 [...] (777 flits)
Class 0:
Remaining flits: 1854526 1854527 1854528 1854529 1854530 1854531 1854532 1854533 1854534 1854535 [...] (2629706 flits)
Measured flits: 1854526 1854527 1854528 1854529 1854530 1854531 1854532 1854533 1854534 1854535 [...] (584 flits)
Class 0:
Remaining flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (2624219 flits)
Measured flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (356 flits)
Class 0:
Remaining flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (2625304 flits)
Measured flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (326 flits)
Class 0:
Remaining flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (2622721 flits)
Measured flits: 1866654 1866655 1866656 1866657 1866658 1866659 1866660 1866661 1866662 1866663 [...] (306 flits)
Class 0:
Remaining flits: 1896507 1896508 1896509 1896510 1896511 1896512 1896513 1896514 1896515 1914624 [...] (2620458 flits)
Measured flits: 1896507 1896508 1896509 1896510 1896511 1896512 1896513 1896514 1896515 1914624 [...] (243 flits)
Class 0:
Remaining flits: 1944360 1944361 1944362 1944363 1944364 1944365 1944366 1944367 1944368 1944369 [...] (2617080 flits)
Measured flits: 1944360 1944361 1944362 1944363 1944364 1944365 1944366 1944367 1944368 1944369 [...] (162 flits)
Class 0:
Remaining flits: 1947420 1947421 1947422 1947423 1947424 1947425 1947426 1947427 1947428 1947429 [...] (2612004 flits)
Measured flits: 1947420 1947421 1947422 1947423 1947424 1947425 1947426 1947427 1947428 1947429 [...] (64 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2048884 2048885 2155086 2155087 2155088 2155089 2155090 2155091 2155092 2155093 [...] (2559753 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2158650 2158651 2158652 2158653 2158654 2158655 2158656 2158657 2158658 2158659 [...] (2508905 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2168746 2168747 2199978 2199979 2199980 2199981 2199982 2199983 2199984 2199985 [...] (2458278 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2204910 2204911 2204912 2204913 2204914 2204915 2204916 2204917 2204918 2204919 [...] (2408129 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2246994 2246995 2246996 2246997 2246998 2246999 2247000 2247001 2247002 2247003 [...] (2357529 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2246994 2246995 2246996 2246997 2246998 2246999 2247000 2247001 2247002 2247003 [...] (2306637 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2303372 2303373 2303374 2303375 2303376 2303377 2303378 2303379 2303380 2303381 [...] (2256083 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2363196 2363197 2363198 2363199 2363200 2363201 2375167 2375168 2375169 2375170 [...] (2205981 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (2155380 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (2105476 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (2055500 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (2005012 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (1954377 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2407644 2407645 2407646 2407647 2407648 2407649 2407650 2407651 2407652 2407653 [...] (1903750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2571066 2571067 2571068 2571069 2571070 2571071 2571072 2571073 2571074 2571075 [...] (1852728 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2589354 2589355 2589356 2589357 2589358 2589359 2589360 2589361 2589362 2589363 [...] (1802719 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2589354 2589355 2589356 2589357 2589358 2589359 2589360 2589361 2589362 2589363 [...] (1751733 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2658042 2658043 2658044 2658045 2658046 2658047 2658048 2658049 2658050 2658051 [...] (1701170 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2658042 2658043 2658044 2658045 2658046 2658047 2658048 2658049 2658050 2658051 [...] (1651380 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2682090 2682091 2682092 2682093 2682094 2682095 2682096 2682097 2682098 2682099 [...] (1601928 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2682090 2682091 2682092 2682093 2682094 2682095 2682096 2682097 2682098 2682099 [...] (1552530 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2729790 2729791 2729792 2729793 2729794 2729795 2729796 2729797 2729798 2729799 [...] (1503675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2729790 2729791 2729792 2729793 2729794 2729795 2729796 2729797 2729798 2729799 [...] (1455161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2729790 2729791 2729792 2729793 2729794 2729795 2729796 2729797 2729798 2729799 [...] (1406909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2729790 2729791 2729792 2729793 2729794 2729795 2729796 2729797 2729798 2729799 [...] (1358974 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2753910 2753911 2753912 2753913 2753914 2753915 2753916 2753917 2753918 2753919 [...] (1311066 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2753910 2753911 2753912 2753913 2753914 2753915 2753916 2753917 2753918 2753919 [...] (1263521 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2932938 2932939 2932940 2932941 2932942 2932943 2932944 2932945 2932946 2932947 [...] (1215684 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018132 3018133 3018134 3018135 3018136 3018137 3018138 3018139 3018140 3018141 [...] (1168037 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018132 3018133 3018134 3018135 3018136 3018137 3018138 3018139 3018140 3018141 [...] (1120304 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018132 3018133 3018134 3018135 3018136 3018137 3018138 3018139 3018140 3018141 [...] (1072554 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3018148 3018149 3265196 3265197 3265198 3265199 3269160 3269161 3269162 3269163 [...] (1024909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3269161 3269162 3269163 3269164 3269165 3269166 3269167 3269168 3269169 3269170 [...] (977132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3297978 3297979 3297980 3297981 3297982 3297983 3297984 3297985 3297986 3297987 [...] (929335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3297978 3297979 3297980 3297981 3297982 3297983 3297984 3297985 3297986 3297987 [...] (881580 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3469140 3469141 3469142 3469143 3469144 3469145 3469146 3469147 3469148 3469149 [...] (834089 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3493962 3493963 3493964 3493965 3493966 3493967 3493968 3493969 3493970 3493971 [...] (786689 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3518316 3518317 3518318 3518319 3518320 3518321 3518322 3518323 3518324 3518325 [...] (739138 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3518316 3518317 3518318 3518319 3518320 3518321 3518322 3518323 3518324 3518325 [...] (691968 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3618972 3618973 3618974 3618975 3618976 3618977 3618978 3618979 3618980 3618981 [...] (644020 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3719826 3719827 3719828 3719829 3719830 3719831 3719832 3719833 3719834 3719835 [...] (596209 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3719826 3719827 3719828 3719829 3719830 3719831 3719832 3719833 3719834 3719835 [...] (548774 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3764016 3764017 3764018 3764019 3764020 3764021 3764022 3764023 3764024 3764025 [...] (500596 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3764016 3764017 3764018 3764019 3764020 3764021 3764022 3764023 3764024 3764025 [...] (452848 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3764016 3764017 3764018 3764019 3764020 3764021 3764022 3764023 3764024 3764025 [...] (404884 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3937320 3937321 3937322 3937323 3937324 3937325 3937326 3937327 3937328 3937329 [...] (357049 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3993840 3993841 3993842 3993843 3993844 3993845 3993846 3993847 3993848 3993849 [...] (309509 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4004046 4004047 4004048 4004049 4004050 4004051 4004052 4004053 4004054 4004055 [...] (261838 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4052592 4052593 4052594 4052595 4052596 4052597 4052598 4052599 4052600 4052601 [...] (214093 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4052592 4052593 4052594 4052595 4052596 4052597 4052598 4052599 4052600 4052601 [...] (166742 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4052592 4052593 4052594 4052595 4052596 4052597 4052598 4052599 4052600 4052601 [...] (119703 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4306500 4306501 4306502 4306503 4306504 4306505 4306506 4306507 4306508 4306509 [...] (73806 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4405356 4405357 4405358 4405359 4405360 4405361 4405362 4405363 4405364 4405365 [...] (35375 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4536954 4536955 4536956 4536957 4536958 4536959 4536960 4536961 4536962 4536963 [...] (10688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4732286 4732287 4732288 4732289 4783464 4783465 4783466 4783467 4783468 4783469 [...] (1687 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4922208 4922209 4922210 4922211 4922212 4922213 4922214 4922215 4922216 4922217 [...] (609 flits)
Measured flits: (0 flits)
Time taken is 108577 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16380.5 (1 samples)
	minimum = 22 (1 samples)
	maximum = 42009 (1 samples)
Network latency average = 16079.4 (1 samples)
	minimum = 22 (1 samples)
	maximum = 40904 (1 samples)
Flit latency average = 33476 (1 samples)
	minimum = 5 (1 samples)
	maximum = 80032 (1 samples)
Fragmentation average = 175.411 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2333 (1 samples)
Injected packet rate average = 0.0535179 (1 samples)
	minimum = 0.0462857 (1 samples)
	maximum = 0.0555714 (1 samples)
Accepted packet rate average = 0.0180275 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.0227143 (1 samples)
Injected flit rate average = 0.963329 (1 samples)
	minimum = 0.833714 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.324451 (1 samples)
	minimum = 0.258143 (1 samples)
	maximum = 0.406857 (1 samples)
Injected packet size average = 18.0001 (1 samples)
Accepted packet size average = 17.9975 (1 samples)
Hops average = 5.06981 (1 samples)
Total run time 153.199
