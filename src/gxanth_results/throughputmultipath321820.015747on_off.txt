BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 226.307
	minimum = 22
	maximum = 948
Network latency average = 155.015
	minimum = 22
	maximum = 801
Slowest packet = 64
Flit latency average = 126.534
	minimum = 5
	maximum = 811
Slowest flit = 10551
Fragmentation average = 30.3222
	minimum = 0
	maximum = 259
Injected packet rate average = 0.0164063
	minimum = 0 (at node 26)
	maximum = 0.052 (at node 186)
Accepted packet rate average = 0.0118021
	minimum = 0.003 (at node 41)
	maximum = 0.021 (at node 132)
Injected flit rate average = 0.292495
	minimum = 0 (at node 26)
	maximum = 0.921 (at node 186)
Accepted flit rate average= 0.219943
	minimum = 0.054 (at node 41)
	maximum = 0.383 (at node 132)
Injected packet length average = 17.8283
Accepted packet length average = 18.6359
Total in-flight flits = 14543 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 330.272
	minimum = 22
	maximum = 1702
Network latency average = 239.487
	minimum = 22
	maximum = 1315
Slowest packet = 64
Flit latency average = 206.128
	minimum = 5
	maximum = 1299
Slowest flit = 38656
Fragmentation average = 36.1789
	minimum = 0
	maximum = 259
Injected packet rate average = 0.015513
	minimum = 0.0005 (at node 10)
	maximum = 0.0415 (at node 178)
Accepted packet rate average = 0.0127396
	minimum = 0.007 (at node 83)
	maximum = 0.019 (at node 166)
Injected flit rate average = 0.277958
	minimum = 0.009 (at node 10)
	maximum = 0.747 (at node 178)
Accepted flit rate average= 0.23338
	minimum = 0.126 (at node 83)
	maximum = 0.346 (at node 166)
Injected packet length average = 17.9177
Accepted packet length average = 18.3193
Total in-flight flits = 17662 (0 measured)
latency change    = 0.314785
throughput change = 0.0575777
Class 0:
Packet latency average = 531.003
	minimum = 25
	maximum = 2378
Network latency average = 399.859
	minimum = 22
	maximum = 1628
Slowest packet = 5024
Flit latency average = 360.897
	minimum = 5
	maximum = 1624
Slowest flit = 73726
Fragmentation average = 46.5616
	minimum = 0
	maximum = 252
Injected packet rate average = 0.0154844
	minimum = 0 (at node 56)
	maximum = 0.047 (at node 123)
Accepted packet rate average = 0.0139583
	minimum = 0.006 (at node 190)
	maximum = 0.028 (at node 187)
Injected flit rate average = 0.278135
	minimum = 0 (at node 56)
	maximum = 0.852 (at node 123)
Accepted flit rate average= 0.252286
	minimum = 0.104 (at node 190)
	maximum = 0.473 (at node 187)
Injected packet length average = 17.9623
Accepted packet length average = 18.0743
Total in-flight flits = 22863 (0 measured)
latency change    = 0.378023
throughput change = 0.0749396
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 444.383
	minimum = 25
	maximum = 3061
Network latency average = 286.475
	minimum = 22
	maximum = 947
Slowest packet = 8974
Flit latency average = 416.886
	minimum = 5
	maximum = 2005
Slowest flit = 89513
Fragmentation average = 35.7349
	minimum = 0
	maximum = 233
Injected packet rate average = 0.014349
	minimum = 0 (at node 74)
	maximum = 0.038 (at node 2)
Accepted packet rate average = 0.0143594
	minimum = 0.006 (at node 36)
	maximum = 0.031 (at node 16)
Injected flit rate average = 0.257542
	minimum = 0 (at node 74)
	maximum = 0.689 (at node 28)
Accepted flit rate average= 0.257578
	minimum = 0.112 (at node 174)
	maximum = 0.545 (at node 16)
Injected packet length average = 17.9485
Accepted packet length average = 17.938
Total in-flight flits = 22980 (21887 measured)
latency change    = 0.194923
throughput change = 0.0205439
Class 0:
Packet latency average = 602.641
	minimum = 25
	maximum = 4004
Network latency average = 401.446
	minimum = 22
	maximum = 1833
Slowest packet = 8974
Flit latency average = 432.126
	minimum = 5
	maximum = 2472
Slowest flit = 129059
Fragmentation average = 42.6296
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0144896
	minimum = 0 (at node 163)
	maximum = 0.032 (at node 2)
Accepted packet rate average = 0.0143411
	minimum = 0.0085 (at node 36)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.260651
	minimum = 0 (at node 163)
	maximum = 0.579 (at node 28)
Accepted flit rate average= 0.257362
	minimum = 0.153 (at node 36)
	maximum = 0.384 (at node 129)
Injected packet length average = 17.9889
Accepted packet length average = 17.9457
Total in-flight flits = 24242 (24188 measured)
latency change    = 0.262608
throughput change = 0.000839851
Class 0:
Packet latency average = 667.823
	minimum = 25
	maximum = 4004
Network latency average = 447.321
	minimum = 22
	maximum = 2582
Slowest packet = 8974
Flit latency average = 446.261
	minimum = 5
	maximum = 2529
Slowest flit = 161375
Fragmentation average = 44.8242
	minimum = 0
	maximum = 256
Injected packet rate average = 0.0144306
	minimum = 0.00233333 (at node 131)
	maximum = 0.0353333 (at node 165)
Accepted packet rate average = 0.0143247
	minimum = 0.01 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.259943
	minimum = 0.042 (at node 131)
	maximum = 0.632333 (at node 165)
Accepted flit rate average= 0.257347
	minimum = 0.182333 (at node 36)
	maximum = 0.360667 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 17.9653
Total in-flight flits = 24283 (24283 measured)
latency change    = 0.0976032
throughput change = 5.73425e-05
Draining remaining packets ...
Class 0:
Remaining flits: 286541 290448 290449 290450 290451 290452 290453 290454 290455 290456 [...] (26 flits)
Measured flits: 286541 290448 290449 290450 290451 290452 290453 290454 290455 290456 [...] (26 flits)
Time taken is 7033 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 737.806 (1 samples)
	minimum = 25 (1 samples)
	maximum = 4004 (1 samples)
Network latency average = 496.672 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2582 (1 samples)
Flit latency average = 481.667 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2529 (1 samples)
Fragmentation average = 44.6055 (1 samples)
	minimum = 0 (1 samples)
	maximum = 285 (1 samples)
Injected packet rate average = 0.0144306 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.0353333 (1 samples)
Accepted packet rate average = 0.0143247 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.259943 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.632333 (1 samples)
Accepted flit rate average = 0.257347 (1 samples)
	minimum = 0.182333 (1 samples)
	maximum = 0.360667 (1 samples)
Injected packet size average = 18.0134 (1 samples)
Accepted packet size average = 17.9653 (1 samples)
Hops average = 5.07421 (1 samples)
Total run time 5.86081
