BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 299.62
	minimum = 27
	maximum = 977
Network latency average = 240.246
	minimum = 25
	maximum = 912
Slowest packet = 21
Flit latency average = 169.943
	minimum = 6
	maximum = 895
Slowest flit = 5741
Fragmentation average = 131.44
	minimum = 0
	maximum = 716
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00923437
	minimum = 0.003 (at node 4)
	maximum = 0.019 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.188208
	minimum = 0.082 (at node 172)
	maximum = 0.371 (at node 76)
Injected packet length average = 17.8264
Accepted packet length average = 20.3813
Total in-flight flits = 25874 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 471.886
	minimum = 23
	maximum = 1883
Network latency average = 400.206
	minimum = 23
	maximum = 1753
Slowest packet = 21
Flit latency average = 317.038
	minimum = 6
	maximum = 1835
Slowest flit = 9490
Fragmentation average = 172.548
	minimum = 0
	maximum = 1489
Injected packet rate average = 0.0173698
	minimum = 0.002 (at node 48)
	maximum = 0.0385 (at node 6)
Accepted packet rate average = 0.0103281
	minimum = 0.0055 (at node 116)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.311279
	minimum = 0.036 (at node 48)
	maximum = 0.693 (at node 147)
Accepted flit rate average= 0.198755
	minimum = 0.1035 (at node 116)
	maximum = 0.308 (at node 152)
Injected packet length average = 17.9207
Accepted packet length average = 19.2441
Total in-flight flits = 43738 (0 measured)
latency change    = 0.365058
throughput change = 0.0530646
Class 0:
Packet latency average = 907.379
	minimum = 28
	maximum = 2799
Network latency average = 816.277
	minimum = 24
	maximum = 2711
Slowest packet = 527
Flit latency average = 720.078
	minimum = 6
	maximum = 2810
Slowest flit = 10976
Fragmentation average = 225.437
	minimum = 0
	maximum = 1906
Injected packet rate average = 0.0179948
	minimum = 0 (at node 83)
	maximum = 0.048 (at node 67)
Accepted packet rate average = 0.0118281
	minimum = 0.004 (at node 2)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.323969
	minimum = 0 (at node 83)
	maximum = 0.865 (at node 67)
Accepted flit rate average= 0.213771
	minimum = 0.083 (at node 2)
	maximum = 0.384 (at node 16)
Injected packet length average = 18.0035
Accepted packet length average = 18.0731
Total in-flight flits = 64884 (0 measured)
latency change    = 0.479946
throughput change = 0.0702417
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 367.629
	minimum = 28
	maximum = 1533
Network latency average = 286.375
	minimum = 23
	maximum = 977
Slowest packet = 10131
Flit latency average = 996.87
	minimum = 6
	maximum = 3736
Slowest flit = 16151
Fragmentation average = 102.9
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0189948
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 113)
Accepted packet rate average = 0.0118958
	minimum = 0.004 (at node 17)
	maximum = 0.024 (at node 81)
Injected flit rate average = 0.341885
	minimum = 0 (at node 72)
	maximum = 1 (at node 113)
Accepted flit rate average= 0.214115
	minimum = 0.079 (at node 17)
	maximum = 0.386 (at node 137)
Injected packet length average = 17.9989
Accepted packet length average = 17.9991
Total in-flight flits = 89420 (54674 measured)
latency change    = 1.46819
throughput change = 0.00160545
Class 0:
Packet latency average = 746.771
	minimum = 28
	maximum = 2580
Network latency average = 651.554
	minimum = 23
	maximum = 1958
Slowest packet = 10131
Flit latency average = 1136.26
	minimum = 6
	maximum = 4323
Slowest flit = 36791
Fragmentation average = 150.392
	minimum = 0
	maximum = 1433
Injected packet rate average = 0.0184427
	minimum = 0.001 (at node 87)
	maximum = 0.0425 (at node 93)
Accepted packet rate average = 0.011974
	minimum = 0.007 (at node 17)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.331404
	minimum = 0.013 (at node 87)
	maximum = 0.7695 (at node 93)
Accepted flit rate average= 0.214987
	minimum = 0.132 (at node 17)
	maximum = 0.314 (at node 87)
Injected packet length average = 17.9694
Accepted packet length average = 17.9545
Total in-flight flits = 109805 (90913 measured)
latency change    = 0.507708
throughput change = 0.0040579
Class 0:
Packet latency average = 1035.47
	minimum = 25
	maximum = 3519
Network latency average = 935.238
	minimum = 23
	maximum = 2961
Slowest packet = 10131
Flit latency average = 1278.76
	minimum = 6
	maximum = 5285
Slowest flit = 42123
Fragmentation average = 169.565
	minimum = 0
	maximum = 1758
Injected packet rate average = 0.017849
	minimum = 0.00466667 (at node 51)
	maximum = 0.0343333 (at node 35)
Accepted packet rate average = 0.0118767
	minimum = 0.007 (at node 105)
	maximum = 0.0163333 (at node 66)
Injected flit rate average = 0.321335
	minimum = 0.084 (at node 51)
	maximum = 0.623 (at node 35)
Accepted flit rate average= 0.213198
	minimum = 0.128 (at node 105)
	maximum = 0.291 (at node 66)
Injected packet length average = 18.003
Accepted packet length average = 17.9509
Total in-flight flits = 127140 (116399 measured)
latency change    = 0.27881
throughput change = 0.00839156
Draining remaining packets ...
Class 0:
Remaining flits: 48258 48259 48260 48261 48262 48263 48264 48265 48266 48267 [...] (91664 flits)
Measured flits: 182663 182715 182716 182717 182736 182737 182738 182739 182740 182741 [...] (85904 flits)
Class 0:
Remaining flits: 59436 59437 59438 59439 59440 59441 59442 59443 59444 59445 [...] (57782 flits)
Measured flits: 182736 182737 182738 182739 182740 182741 182742 182743 182744 182745 [...] (54841 flits)
Class 0:
Remaining flits: 74412 74413 74414 74415 74416 74417 74418 74419 74420 74421 [...] (27163 flits)
Measured flits: 182736 182737 182738 182739 182740 182741 182742 182743 182744 182745 [...] (26037 flits)
Class 0:
Remaining flits: 87480 87481 87482 87483 87484 87485 87486 87487 87488 87489 [...] (5215 flits)
Measured flits: 186390 186391 186392 186393 186394 186395 186396 186397 186398 186399 [...] (5073 flits)
Class 0:
Remaining flits: 172883 172884 172885 172886 172887 172888 172889 249714 249715 249716 [...] (261 flits)
Measured flits: 249714 249715 249716 249717 249718 249719 249720 249721 249722 249723 [...] (254 flits)
Time taken is 11270 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2588.68 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7784 (1 samples)
Network latency average = 2452.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7519 (1 samples)
Flit latency average = 2335.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8955 (1 samples)
Fragmentation average = 185.93 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2363 (1 samples)
Injected packet rate average = 0.017849 (1 samples)
	minimum = 0.00466667 (1 samples)
	maximum = 0.0343333 (1 samples)
Accepted packet rate average = 0.0118767 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.321335 (1 samples)
	minimum = 0.084 (1 samples)
	maximum = 0.623 (1 samples)
Accepted flit rate average = 0.213198 (1 samples)
	minimum = 0.128 (1 samples)
	maximum = 0.291 (1 samples)
Injected packet size average = 18.003 (1 samples)
Accepted packet size average = 17.9509 (1 samples)
Hops average = 5.10271 (1 samples)
Total run time 9.52607
