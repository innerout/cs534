BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 323.812
	minimum = 22
	maximum = 969
Network latency average = 218.713
	minimum = 22
	maximum = 753
Slowest packet = 7
Flit latency average = 190.953
	minimum = 5
	maximum = 742
Slowest flit = 20147
Fragmentation average = 38.5766
	minimum = 0
	maximum = 469
Injected packet rate average = 0.025526
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135052
	minimum = 0.006 (at node 25)
	maximum = 0.023 (at node 70)
Injected flit rate average = 0.454854
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.253708
	minimum = 0.119 (at node 25)
	maximum = 0.424 (at node 70)
Injected packet length average = 17.8192
Accepted packet length average = 18.786
Total in-flight flits = 39506 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 549.923
	minimum = 22
	maximum = 1909
Network latency average = 406.177
	minimum = 22
	maximum = 1505
Slowest packet = 7
Flit latency average = 374.384
	minimum = 5
	maximum = 1488
Slowest flit = 39635
Fragmentation average = 56.799
	minimum = 0
	maximum = 611
Injected packet rate average = 0.0268125
	minimum = 0 (at node 38)
	maximum = 0.056 (at node 20)
Accepted packet rate average = 0.0144583
	minimum = 0.0085 (at node 83)
	maximum = 0.0195 (at node 88)
Injected flit rate average = 0.480391
	minimum = 0 (at node 38)
	maximum = 1 (at node 20)
Accepted flit rate average= 0.268224
	minimum = 0.153 (at node 153)
	maximum = 0.362 (at node 166)
Injected packet length average = 17.9167
Accepted packet length average = 18.5515
Total in-flight flits = 82330 (0 measured)
latency change    = 0.411168
throughput change = 0.0541176
Class 0:
Packet latency average = 1145.34
	minimum = 23
	maximum = 2693
Network latency average = 946.873
	minimum = 22
	maximum = 2149
Slowest packet = 4193
Flit latency average = 909.49
	minimum = 5
	maximum = 2208
Slowest flit = 66425
Fragmentation average = 104.519
	minimum = 0
	maximum = 751
Injected packet rate average = 0.0301198
	minimum = 0 (at node 48)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0158073
	minimum = 0.006 (at node 184)
	maximum = 0.029 (at node 168)
Injected flit rate average = 0.541453
	minimum = 0 (at node 48)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.287932
	minimum = 0.105 (at node 184)
	maximum = 0.537 (at node 168)
Injected packet length average = 17.9767
Accepted packet length average = 18.2152
Total in-flight flits = 131141 (0 measured)
latency change    = 0.519862
throughput change = 0.0684478
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 293.295
	minimum = 22
	maximum = 1415
Network latency average = 61.1586
	minimum = 22
	maximum = 738
Slowest packet = 16088
Flit latency average = 1315.56
	minimum = 5
	maximum = 3175
Slowest flit = 67406
Fragmentation average = 8.62249
	minimum = 0
	maximum = 105
Injected packet rate average = 0.0299219
	minimum = 0.001 (at node 148)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0159323
	minimum = 0.007 (at node 4)
	maximum = 0.027 (at node 165)
Injected flit rate average = 0.539427
	minimum = 0.017 (at node 148)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.289286
	minimum = 0.125 (at node 4)
	maximum = 0.489 (at node 165)
Injected packet length average = 18.0279
Accepted packet length average = 18.1572
Total in-flight flits = 179008 (94345 measured)
latency change    = 2.90509
throughput change = 0.00468106
Class 0:
Packet latency average = 609.33
	minimum = 22
	maximum = 2305
Network latency average = 403.543
	minimum = 22
	maximum = 1967
Slowest packet = 16088
Flit latency average = 1544.6
	minimum = 5
	maximum = 4092
Slowest flit = 72962
Fragmentation average = 31.4533
	minimum = 0
	maximum = 589
Injected packet rate average = 0.0288594
	minimum = 0.004 (at node 94)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0157578
	minimum = 0.0085 (at node 36)
	maximum = 0.024 (at node 165)
Injected flit rate average = 0.519826
	minimum = 0.072 (at node 94)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.28657
	minimum = 0.157 (at node 36)
	maximum = 0.4295 (at node 165)
Injected packet length average = 18.0124
Accepted packet length average = 18.1859
Total in-flight flits = 220574 (176690 measured)
latency change    = 0.51866
throughput change = 0.00947811
Class 0:
Packet latency average = 1266.57
	minimum = 22
	maximum = 3709
Network latency average = 1068.41
	minimum = 22
	maximum = 2980
Slowest packet = 16088
Flit latency average = 1763.15
	minimum = 5
	maximum = 4843
Slowest flit = 96623
Fragmentation average = 65.4036
	minimum = 0
	maximum = 665
Injected packet rate average = 0.0281146
	minimum = 0.005 (at node 111)
	maximum = 0.0556667 (at node 1)
Accepted packet rate average = 0.0157517
	minimum = 0.00933333 (at node 36)
	maximum = 0.0213333 (at node 123)
Injected flit rate average = 0.506476
	minimum = 0.0893333 (at node 111)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.286483
	minimum = 0.169667 (at node 36)
	maximum = 0.392 (at node 129)
Injected packet length average = 18.0147
Accepted packet length average = 18.1874
Total in-flight flits = 257637 (239509 measured)
latency change    = 0.518911
throughput change = 0.000306035
Draining remaining packets ...
Class 0:
Remaining flits: 122744 122745 122746 122747 122748 122749 122750 122751 122752 122753 [...] (211053 flits)
Measured flits: 289426 289427 289428 289429 289430 289431 289432 289433 289434 289435 [...] (203153 flits)
Class 0:
Remaining flits: 156379 156380 156381 156382 156383 171244 171245 171246 171247 171248 [...] (163859 flits)
Measured flits: 289476 289477 289478 289479 289480 289481 289482 289483 289484 289485 [...] (160376 flits)
Class 0:
Remaining flits: 193730 193731 193732 193733 193748 193749 193750 193751 208980 208981 [...] (116666 flits)
Measured flits: 289490 289491 289492 289493 289872 289873 289874 289875 289876 289877 [...] (115313 flits)
Class 0:
Remaining flits: 218516 218517 218518 218519 226605 226606 226607 226608 226609 226610 [...] (70850 flits)
Measured flits: 289872 289873 289874 289875 289876 289877 289878 289879 289880 289881 [...] (70480 flits)
Class 0:
Remaining flits: 269312 269313 269314 269315 284970 284971 284972 284973 284974 284975 [...] (33793 flits)
Measured flits: 291884 291885 291886 291887 300030 300031 300032 300033 300034 300035 [...] (33772 flits)
Class 0:
Remaining flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (15490 flits)
Measured flits: 310032 310033 310034 310035 310036 310037 310038 310039 310040 310041 [...] (15490 flits)
Class 0:
Remaining flits: 310038 310039 310040 310041 310042 310043 310044 310045 310046 310047 [...] (7489 flits)
Measured flits: 310038 310039 310040 310041 310042 310043 310044 310045 310046 310047 [...] (7489 flits)
Class 0:
Remaining flits: 372091 372092 372093 372094 372095 387504 387505 387506 387507 387508 [...] (3450 flits)
Measured flits: 372091 372092 372093 372094 372095 387504 387505 387506 387507 387508 [...] (3450 flits)
Class 0:
Remaining flits: 427727 427728 427729 427730 427731 427732 427733 429336 429337 429338 [...] (1206 flits)
Measured flits: 427727 427728 427729 427730 427731 427732 427733 429336 429337 429338 [...] (1206 flits)
Time taken is 15913 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4167.22 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11964 (1 samples)
Network latency average = 3940.59 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10943 (1 samples)
Flit latency average = 3395.44 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10926 (1 samples)
Fragmentation average = 172.231 (1 samples)
	minimum = 0 (1 samples)
	maximum = 1393 (1 samples)
Injected packet rate average = 0.0281146 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0157517 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.506476 (1 samples)
	minimum = 0.0893333 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.286483 (1 samples)
	minimum = 0.169667 (1 samples)
	maximum = 0.392 (1 samples)
Injected packet size average = 18.0147 (1 samples)
Accepted packet size average = 18.1874 (1 samples)
Hops average = 5.08978 (1 samples)
Total run time 12.8147
