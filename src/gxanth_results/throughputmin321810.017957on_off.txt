BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 305.527
	minimum = 27
	maximum = 969
Network latency average = 239.721
	minimum = 23
	maximum = 816
Slowest packet = 156
Flit latency average = 165.897
	minimum = 6
	maximum = 938
Slowest flit = 2860
Fragmentation average = 132.3
	minimum = 0
	maximum = 686
Injected packet rate average = 0.0150208
	minimum = 0 (at node 9)
	maximum = 0.04 (at node 74)
Accepted packet rate average = 0.00908854
	minimum = 0.002 (at node 41)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.266708
	minimum = 0 (at node 9)
	maximum = 0.716 (at node 74)
Accepted flit rate average= 0.182828
	minimum = 0.036 (at node 174)
	maximum = 0.338 (at node 48)
Injected packet length average = 17.7559
Accepted packet length average = 20.1163
Total in-flight flits = 16827 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.329
	minimum = 23
	maximum = 1914
Network latency average = 355.867
	minimum = 23
	maximum = 1897
Slowest packet = 158
Flit latency average = 268.884
	minimum = 6
	maximum = 1880
Slowest flit = 2861
Fragmentation average = 163.058
	minimum = 0
	maximum = 1742
Injected packet rate average = 0.0135651
	minimum = 0.001 (at node 103)
	maximum = 0.0275 (at node 74)
Accepted packet rate average = 0.00984115
	minimum = 0.005 (at node 30)
	maximum = 0.0155 (at node 71)
Injected flit rate average = 0.241484
	minimum = 0.013 (at node 103)
	maximum = 0.4885 (at node 118)
Accepted flit rate average= 0.187924
	minimum = 0.1035 (at node 30)
	maximum = 0.287 (at node 71)
Injected packet length average = 17.8019
Accepted packet length average = 19.0958
Total in-flight flits = 21671 (0 measured)
latency change    = 0.342003
throughput change = 0.0271192
Class 0:
Packet latency average = 960.545
	minimum = 32
	maximum = 2734
Network latency average = 629.146
	minimum = 28
	maximum = 2544
Slowest packet = 228
Flit latency average = 515.61
	minimum = 6
	maximum = 2810
Slowest flit = 11309
Fragmentation average = 199.65
	minimum = 0
	maximum = 2052
Injected packet rate average = 0.0123229
	minimum = 0 (at node 4)
	maximum = 0.035 (at node 169)
Accepted packet rate average = 0.0105521
	minimum = 0.004 (at node 54)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.221885
	minimum = 0 (at node 4)
	maximum = 0.638 (at node 169)
Accepted flit rate average= 0.193297
	minimum = 0.077 (at node 118)
	maximum = 0.318 (at node 56)
Injected packet length average = 18.0059
Accepted packet length average = 18.3184
Total in-flight flits = 27182 (0 measured)
latency change    = 0.516598
throughput change = 0.0277935
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1063.2
	minimum = 27
	maximum = 3578
Network latency average = 341.816
	minimum = 27
	maximum = 938
Slowest packet = 7598
Flit latency average = 629.899
	minimum = 6
	maximum = 3718
Slowest flit = 15137
Fragmentation average = 137.111
	minimum = 0
	maximum = 604
Injected packet rate average = 0.0114635
	minimum = 0 (at node 5)
	maximum = 0.037 (at node 187)
Accepted packet rate average = 0.0109375
	minimum = 0.003 (at node 4)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.205708
	minimum = 0 (at node 139)
	maximum = 0.668 (at node 187)
Accepted flit rate average= 0.194813
	minimum = 0.063 (at node 4)
	maximum = 0.408 (at node 16)
Injected packet length average = 17.9446
Accepted packet length average = 17.8114
Total in-flight flits = 29504 (23513 measured)
latency change    = 0.0965529
throughput change = 0.00777992
Class 0:
Packet latency average = 1412.5
	minimum = 27
	maximum = 4700
Network latency average = 555.426
	minimum = 26
	maximum = 1925
Slowest packet = 7598
Flit latency average = 671.09
	minimum = 6
	maximum = 4518
Slowest flit = 26785
Fragmentation average = 164.726
	minimum = 0
	maximum = 1277
Injected packet rate average = 0.0114687
	minimum = 0 (at node 68)
	maximum = 0.0235 (at node 142)
Accepted packet rate average = 0.0108437
	minimum = 0.0055 (at node 4)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.20612
	minimum = 0.0045 (at node 68)
	maximum = 0.431 (at node 142)
Accepted flit rate average= 0.194242
	minimum = 0.099 (at node 4)
	maximum = 0.3655 (at node 16)
Injected packet length average = 17.9723
Accepted packet length average = 17.9128
Total in-flight flits = 32009 (30442 measured)
latency change    = 0.247292
throughput change = 0.00293609
Class 0:
Packet latency average = 1706.14
	minimum = 27
	maximum = 5662
Network latency average = 666.641
	minimum = 26
	maximum = 2925
Slowest packet = 7598
Flit latency average = 698.875
	minimum = 6
	maximum = 5139
Slowest flit = 29393
Fragmentation average = 177.73
	minimum = 0
	maximum = 2162
Injected packet rate average = 0.0113507
	minimum = 0.002 (at node 177)
	maximum = 0.022 (at node 61)
Accepted packet rate average = 0.0108056
	minimum = 0.005 (at node 4)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.203915
	minimum = 0.0356667 (at node 177)
	maximum = 0.395333 (at node 61)
Accepted flit rate average= 0.194182
	minimum = 0.103 (at node 4)
	maximum = 0.290667 (at node 16)
Injected packet length average = 17.965
Accepted packet length average = 17.9706
Total in-flight flits = 33107 (32585 measured)
latency change    = 0.172107
throughput change = 0.000308452
Draining remaining packets ...
Class 0:
Remaining flits: 110788 110789 116982 116983 116984 116985 116986 116987 116988 116989 [...] (2959 flits)
Measured flits: 144450 144451 144452 144453 144454 144455 144456 144457 144458 144459 [...] (2927 flits)
Time taken is 7507 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2023.32 (1 samples)
	minimum = 27 (1 samples)
	maximum = 6412 (1 samples)
Network latency average = 866.858 (1 samples)
	minimum = 26 (1 samples)
	maximum = 4284 (1 samples)
Flit latency average = 821.787 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6051 (1 samples)
Fragmentation average = 184.909 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2412 (1 samples)
Injected packet rate average = 0.0113507 (1 samples)
	minimum = 0.002 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0108056 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.203915 (1 samples)
	minimum = 0.0356667 (1 samples)
	maximum = 0.395333 (1 samples)
Accepted flit rate average = 0.194182 (1 samples)
	minimum = 0.103 (1 samples)
	maximum = 0.290667 (1 samples)
Injected packet size average = 17.965 (1 samples)
Accepted packet size average = 17.9706 (1 samples)
Hops average = 5.14412 (1 samples)
Total run time 8.07862
