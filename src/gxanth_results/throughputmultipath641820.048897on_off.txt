BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 357.727
	minimum = 22
	maximum = 986
Network latency average = 246.72
	minimum = 22
	maximum = 805
Slowest packet = 46
Flit latency average = 211.032
	minimum = 5
	maximum = 874
Slowest flit = 9645
Fragmentation average = 66.9383
	minimum = 0
	maximum = 541
Injected packet rate average = 0.0279375
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 14)
Accepted packet rate average = 0.0135
	minimum = 0.007 (at node 23)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.497656
	minimum = 0 (at node 17)
	maximum = 1 (at node 53)
Accepted flit rate average= 0.259464
	minimum = 0.135 (at node 23)
	maximum = 0.422 (at node 44)
Injected packet length average = 17.8132
Accepted packet length average = 19.2195
Total in-flight flits = 46789 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 642.145
	minimum = 22
	maximum = 1945
Network latency average = 465.155
	minimum = 22
	maximum = 1686
Slowest packet = 46
Flit latency average = 420.737
	minimum = 5
	maximum = 1669
Slowest flit = 20033
Fragmentation average = 89.9476
	minimum = 0
	maximum = 666
Injected packet rate average = 0.0262578
	minimum = 0.0035 (at node 52)
	maximum = 0.0535 (at node 53)
Accepted packet rate average = 0.0143125
	minimum = 0.008 (at node 30)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.470083
	minimum = 0.063 (at node 52)
	maximum = 0.96 (at node 53)
Accepted flit rate average= 0.267435
	minimum = 0.1545 (at node 83)
	maximum = 0.3815 (at node 22)
Injected packet length average = 17.9026
Accepted packet length average = 18.6854
Total in-flight flits = 79195 (0 measured)
latency change    = 0.442919
throughput change = 0.0298067
Class 0:
Packet latency average = 1446.05
	minimum = 25
	maximum = 2903
Network latency average = 1101.55
	minimum = 22
	maximum = 2270
Slowest packet = 2851
Flit latency average = 1051.21
	minimum = 5
	maximum = 2463
Slowest flit = 45913
Fragmentation average = 119.006
	minimum = 0
	maximum = 748
Injected packet rate average = 0.0186771
	minimum = 0 (at node 73)
	maximum = 0.042 (at node 27)
Accepted packet rate average = 0.0150417
	minimum = 0.005 (at node 4)
	maximum = 0.031 (at node 34)
Injected flit rate average = 0.336432
	minimum = 0 (at node 73)
	maximum = 0.765 (at node 27)
Accepted flit rate average= 0.271286
	minimum = 0.097 (at node 4)
	maximum = 0.562 (at node 34)
Injected packet length average = 18.0131
Accepted packet length average = 18.0357
Total in-flight flits = 92016 (0 measured)
latency change    = 0.555932
throughput change = 0.0141974
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1208.15
	minimum = 33
	maximum = 3567
Network latency average = 103.85
	minimum = 22
	maximum = 995
Slowest packet = 13722
Flit latency average = 1397.49
	minimum = 5
	maximum = 2972
Slowest flit = 97271
Fragmentation average = 10.8854
	minimum = 0
	maximum = 243
Injected packet rate average = 0.0171563
	minimum = 0 (at node 57)
	maximum = 0.034 (at node 74)
Accepted packet rate average = 0.0153021
	minimum = 0.006 (at node 105)
	maximum = 0.026 (at node 63)
Injected flit rate average = 0.307729
	minimum = 0 (at node 57)
	maximum = 0.613 (at node 74)
Accepted flit rate average= 0.274849
	minimum = 0.09 (at node 171)
	maximum = 0.444 (at node 63)
Injected packet length average = 17.9369
Accepted packet length average = 17.9615
Total in-flight flits = 98321 (53296 measured)
latency change    = 0.196911
throughput change = 0.0129617
Class 0:
Packet latency average = 1937.79
	minimum = 28
	maximum = 4543
Network latency average = 727.046
	minimum = 22
	maximum = 1983
Slowest packet = 13722
Flit latency average = 1526.93
	minimum = 5
	maximum = 3760
Slowest flit = 116736
Fragmentation average = 56.8476
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0166042
	minimum = 0.006 (at node 180)
	maximum = 0.028 (at node 74)
Accepted packet rate average = 0.0151667
	minimum = 0.0095 (at node 36)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.298125
	minimum = 0.114 (at node 180)
	maximum = 0.503 (at node 74)
Accepted flit rate average= 0.272555
	minimum = 0.1655 (at node 132)
	maximum = 0.376 (at node 165)
Injected packet length average = 17.9548
Accepted packet length average = 17.9706
Total in-flight flits = 101979 (91205 measured)
latency change    = 0.37653
throughput change = 0.00841765
Class 0:
Packet latency average = 2638.9
	minimum = 28
	maximum = 5624
Network latency average = 1319.32
	minimum = 22
	maximum = 2958
Slowest packet = 13722
Flit latency average = 1614.76
	minimum = 5
	maximum = 4429
Slowest flit = 133973
Fragmentation average = 87.562
	minimum = 0
	maximum = 690
Injected packet rate average = 0.0162604
	minimum = 0.005 (at node 180)
	maximum = 0.0256667 (at node 179)
Accepted packet rate average = 0.0151181
	minimum = 0.00966667 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.292429
	minimum = 0.094 (at node 180)
	maximum = 0.458 (at node 179)
Accepted flit rate average= 0.271542
	minimum = 0.183667 (at node 36)
	maximum = 0.359333 (at node 165)
Injected packet length average = 17.9841
Accepted packet length average = 17.9614
Total in-flight flits = 104142 (103375 measured)
latency change    = 0.265682
throughput change = 0.00373063
Draining remaining packets ...
Class 0:
Remaining flits: 226133 259615 259616 259617 259618 259619 259620 259621 259622 259623 [...] (56835 flits)
Measured flits: 259615 259616 259617 259618 259619 259620 259621 259622 259623 259624 [...] (56834 flits)
Class 0:
Remaining flits: 286578 286579 286580 286581 286582 286583 286584 286585 286586 286587 [...] (10414 flits)
Measured flits: 286578 286579 286580 286581 286582 286583 286584 286585 286586 286587 [...] (10414 flits)
Time taken is 8866 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3571.07 (1 samples)
	minimum = 28 (1 samples)
	maximum = 7931 (1 samples)
Network latency average = 1928.36 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4774 (1 samples)
Flit latency average = 1851.61 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4757 (1 samples)
Fragmentation average = 105.173 (1 samples)
	minimum = 0 (1 samples)
	maximum = 690 (1 samples)
Injected packet rate average = 0.0162604 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.0256667 (1 samples)
Accepted packet rate average = 0.0151181 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.02 (1 samples)
Injected flit rate average = 0.292429 (1 samples)
	minimum = 0.094 (1 samples)
	maximum = 0.458 (1 samples)
Accepted flit rate average = 0.271542 (1 samples)
	minimum = 0.183667 (1 samples)
	maximum = 0.359333 (1 samples)
Injected packet size average = 17.9841 (1 samples)
Accepted packet size average = 17.9614 (1 samples)
Hops average = 5.11823 (1 samples)
Total run time 13.1063
