BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 229.56
	minimum = 24
	maximum = 884
Network latency average = 164.324
	minimum = 22
	maximum = 747
Slowest packet = 64
Flit latency average = 136.803
	minimum = 5
	maximum = 745
Slowest flit = 14990
Fragmentation average = 28.8495
	minimum = 0
	maximum = 453
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.01225
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22788
	minimum = 0.108 (at node 41)
	maximum = 0.442 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.6025
Total in-flight flits = 18257 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 356.197
	minimum = 22
	maximum = 1805
Network latency average = 275.229
	minimum = 22
	maximum = 1400
Slowest packet = 64
Flit latency average = 245.516
	minimum = 5
	maximum = 1433
Slowest flit = 33104
Fragmentation average = 35.0839
	minimum = 0
	maximum = 548
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0131875
	minimum = 0.008 (at node 83)
	maximum = 0.0195 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.241622
	minimum = 0.151 (at node 153)
	maximum = 0.351 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 18.3221
Total in-flight flits = 26809 (0 measured)
latency change    = 0.355525
throughput change = 0.0568746
Class 0:
Packet latency average = 629.123
	minimum = 22
	maximum = 2465
Network latency average = 527.134
	minimum = 22
	maximum = 2034
Slowest packet = 1839
Flit latency average = 492.389
	minimum = 5
	maximum = 2115
Slowest flit = 52981
Fragmentation average = 43.81
	minimum = 0
	maximum = 533
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0145573
	minimum = 0.004 (at node 118)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.261604
	minimum = 0.072 (at node 118)
	maximum = 0.449 (at node 16)
Injected packet length average = 17.9876
Accepted packet length average = 17.9707
Total in-flight flits = 38951 (0 measured)
latency change    = 0.433819
throughput change = 0.0763817
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 408.578
	minimum = 25
	maximum = 1427
Network latency average = 315.061
	minimum = 22
	maximum = 979
Slowest packet = 10126
Flit latency average = 638.36
	minimum = 5
	maximum = 2645
Slowest flit = 72107
Fragmentation average = 29.4136
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0147917
	minimum = 0.007 (at node 117)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.267385
	minimum = 0.13 (at node 117)
	maximum = 0.483 (at node 19)
Injected packet length average = 18.0183
Accepted packet length average = 18.0768
Total in-flight flits = 49551 (41405 measured)
latency change    = 0.539786
throughput change = 0.0216214
Class 0:
Packet latency average = 671.182
	minimum = 22
	maximum = 2170
Network latency average = 574.334
	minimum = 22
	maximum = 1969
Slowest packet = 10126
Flit latency average = 720.34
	minimum = 5
	maximum = 2645
Slowest flit = 72107
Fragmentation average = 40.679
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.0147891
	minimum = 0.009 (at node 110)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.267216
	minimum = 0.169 (at node 110)
	maximum = 0.426 (at node 129)
Injected packet length average = 18.0075
Accepted packet length average = 18.0685
Total in-flight flits = 55986 (54908 measured)
latency change    = 0.391255
throughput change = 0.00063346
Class 0:
Packet latency average = 835.333
	minimum = 22
	maximum = 3328
Network latency average = 738.41
	minimum = 22
	maximum = 2903
Slowest packet = 10126
Flit latency average = 795.37
	minimum = 5
	maximum = 3235
Slowest flit = 166017
Fragmentation average = 44.278
	minimum = 0
	maximum = 694
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0148247
	minimum = 0.0103333 (at node 135)
	maximum = 0.0213333 (at node 178)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.267622
	minimum = 0.190333 (at node 135)
	maximum = 0.382667 (at node 178)
Injected packet length average = 18.014
Accepted packet length average = 18.0525
Total in-flight flits = 65935 (65861 measured)
latency change    = 0.19651
throughput change = 0.00151476
Draining remaining packets ...
Class 0:
Remaining flits: 207265 207266 207267 207268 207269 208375 208376 208377 208378 208379 [...] (24247 flits)
Measured flits: 207265 207266 207267 207268 207269 208375 208376 208377 208378 208379 [...] (24247 flits)
Class 0:
Remaining flits: 286492 286493 286494 286495 286496 286497 286498 286499 286500 286501 [...] (4467 flits)
Measured flits: 286492 286493 286494 286495 286496 286497 286498 286499 286500 286501 [...] (4467 flits)
Class 0:
Remaining flits: 355873 355874 355875 355876 355877 356230 356231 356232 356233 356234 [...] (409 flits)
Measured flits: 355873 355874 355875 355876 355877 356230 356231 356232 356233 356234 [...] (409 flits)
Time taken is 9240 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1197.64 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4074 (1 samples)
Network latency average = 1093.59 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3790 (1 samples)
Flit latency average = 1053.04 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3773 (1 samples)
Fragmentation average = 48.1198 (1 samples)
	minimum = 0 (1 samples)
	maximum = 694 (1 samples)
Injected packet rate average = 0.0174705 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0148247 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.314714 (1 samples)
	minimum = 0.049 (1 samples)
	maximum = 0.725333 (1 samples)
Accepted flit rate average = 0.267622 (1 samples)
	minimum = 0.190333 (1 samples)
	maximum = 0.382667 (1 samples)
Injected packet size average = 18.014 (1 samples)
Accepted packet size average = 18.0525 (1 samples)
Hops average = 5.07761 (1 samples)
Total run time 7.06358
