BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.002487
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 44.2197
	minimum = 23
	maximum = 80
Network latency average = 44.032
	minimum = 23
	maximum = 80
Slowest packet = 158
Flit latency average = 24.5533
	minimum = 6
	maximum = 63
Slowest flit = 3221
Fragmentation average = 4.73684
	minimum = 0
	maximum = 36
Injected packet rate average = 0.00238542
	minimum = 0 (at node 8)
	maximum = 0.007 (at node 124)
Accepted packet rate average = 0.00227604
	minimum = 0 (at node 11)
	maximum = 0.008 (at node 140)
Injected flit rate average = 0.0425625
	minimum = 0 (at node 8)
	maximum = 0.126 (at node 124)
Accepted flit rate average= 0.0413542
	minimum = 0 (at node 11)
	maximum = 0.144 (at node 140)
Injected packet length average = 17.8428
Accepted packet length average = 18.1693
Total in-flight flits = 304 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 45.0821
	minimum = 23
	maximum = 124
Network latency average = 44.818
	minimum = 23
	maximum = 124
Slowest packet = 511
Flit latency average = 24.9689
	minimum = 6
	maximum = 107
Slowest flit = 9215
Fragmentation average = 5.40844
	minimum = 0
	maximum = 85
Injected packet rate average = 0.00238021
	minimum = 0 (at node 81)
	maximum = 0.0055 (at node 161)
Accepted packet rate average = 0.00234635
	minimum = 0.0005 (at node 35)
	maximum = 0.0055 (at node 140)
Injected flit rate average = 0.0427266
	minimum = 0 (at node 81)
	maximum = 0.099 (at node 161)
Accepted flit rate average= 0.0424453
	minimum = 0.009 (at node 41)
	maximum = 0.099 (at node 140)
Injected packet length average = 17.9508
Accepted packet length average = 18.0899
Total in-flight flits = 153 (0 measured)
latency change    = 0.0191307
throughput change = 0.0257071
Class 0:
Packet latency average = 45.2998
	minimum = 23
	maximum = 96
Network latency average = 44.7736
	minimum = 23
	maximum = 86
Slowest packet = 997
Flit latency average = 24.9348
	minimum = 6
	maximum = 69
Slowest flit = 24083
Fragmentation average = 5.39832
	minimum = 0
	maximum = 46
Injected packet rate average = 0.00256771
	minimum = 0 (at node 0)
	maximum = 0.006 (at node 18)
Accepted packet rate average = 0.00248438
	minimum = 0 (at node 5)
	maximum = 0.007 (at node 22)
Injected flit rate average = 0.0460937
	minimum = 0 (at node 0)
	maximum = 0.108 (at node 18)
Accepted flit rate average= 0.045151
	minimum = 0 (at node 10)
	maximum = 0.126 (at node 22)
Injected packet length average = 17.9513
Accepted packet length average = 18.174
Total in-flight flits = 358 (0 measured)
latency change    = 0.00480487
throughput change = 0.0599262
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 46.5748
	minimum = 23
	maximum = 88
Network latency average = 46.2585
	minimum = 23
	maximum = 85
Slowest packet = 1490
Flit latency average = 25.6312
	minimum = 6
	maximum = 68
Slowest flit = 26837
Fragmentation average = 6.92521
	minimum = 0
	maximum = 45
Injected packet rate average = 0.00255208
	minimum = 0 (at node 13)
	maximum = 0.008 (at node 120)
Accepted packet rate average = 0.00258854
	minimum = 0 (at node 32)
	maximum = 0.008 (at node 168)
Injected flit rate average = 0.045849
	minimum = 0 (at node 13)
	maximum = 0.148 (at node 177)
Accepted flit rate average= 0.0464427
	minimum = 0 (at node 32)
	maximum = 0.126 (at node 45)
Injected packet length average = 17.9653
Accepted packet length average = 17.9416
Total in-flight flits = 261 (261 measured)
latency change    = 0.0273752
throughput change = 0.027812
Class 0:
Packet latency average = 46.2401
	minimum = 23
	maximum = 90
Network latency average = 45.9346
	minimum = 23
	maximum = 90
Slowest packet = 2074
Flit latency average = 25.5177
	minimum = 6
	maximum = 73
Slowest flit = 37349
Fragmentation average = 6.5552
	minimum = 0
	maximum = 47
Injected packet rate average = 0.00248177
	minimum = 0 (at node 134)
	maximum = 0.0065 (at node 120)
Accepted packet rate average = 0.00250521
	minimum = 0 (at node 116)
	maximum = 0.006 (at node 44)
Injected flit rate average = 0.0446901
	minimum = 0 (at node 134)
	maximum = 0.117 (at node 120)
Accepted flit rate average= 0.0449479
	minimum = 0 (at node 116)
	maximum = 0.108 (at node 65)
Injected packet length average = 18.0073
Accepted packet length average = 17.9418
Total in-flight flits = 252 (252 measured)
latency change    = 0.00723832
throughput change = 0.0332561
Class 0:
Packet latency average = 46.2945
	minimum = 23
	maximum = 106
Network latency average = 45.973
	minimum = 23
	maximum = 106
Slowest packet = 2648
Flit latency average = 25.4913
	minimum = 6
	maximum = 89
Slowest flit = 47681
Fragmentation average = 6.57752
	minimum = 0
	maximum = 65
Injected packet rate average = 0.00246875
	minimum = 0.000333333 (at node 134)
	maximum = 0.00566667 (at node 55)
Accepted packet rate average = 0.00249132
	minimum = 0.000333333 (at node 116)
	maximum = 0.006 (at node 44)
Injected flit rate average = 0.0444896
	minimum = 0.006 (at node 134)
	maximum = 0.102 (at node 55)
Accepted flit rate average= 0.0447622
	minimum = 0.006 (at node 116)
	maximum = 0.0973333 (at node 44)
Injected packet length average = 18.0211
Accepted packet length average = 17.9672
Total in-flight flits = 171 (171 measured)
latency change    = 0.00117437
throughput change = 0.00415002
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6082 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 46.2518 (1 samples)
	minimum = 23 (1 samples)
	maximum = 106 (1 samples)
Network latency average = 45.9233 (1 samples)
	minimum = 23 (1 samples)
	maximum = 106 (1 samples)
Flit latency average = 25.4614 (1 samples)
	minimum = 6 (1 samples)
	maximum = 89 (1 samples)
Fragmentation average = 6.53516 (1 samples)
	minimum = 0 (1 samples)
	maximum = 65 (1 samples)
Injected packet rate average = 0.00246875 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.00566667 (1 samples)
Accepted packet rate average = 0.00249132 (1 samples)
	minimum = 0.000333333 (1 samples)
	maximum = 0.006 (1 samples)
Injected flit rate average = 0.0444896 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.102 (1 samples)
Accepted flit rate average = 0.0447622 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0973333 (1 samples)
Injected packet size average = 18.0211 (1 samples)
Accepted packet size average = 17.9672 (1 samples)
Hops average = 5.06751 (1 samples)
Total run time 1.11925
