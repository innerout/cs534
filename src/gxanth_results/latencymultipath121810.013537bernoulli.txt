BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 176.731
	minimum = 23
	maximum = 851
Network latency average = 169.352
	minimum = 23
	maximum = 851
Slowest packet = 342
Flit latency average = 131.751
	minimum = 6
	maximum = 897
Slowest flit = 4234
Fragmentation average = 45.9725
	minimum = 0
	maximum = 124
Injected packet rate average = 0.012276
	minimum = 0.005 (at node 45)
	maximum = 0.022 (at node 113)
Accepted packet rate average = 0.00870833
	minimum = 0.002 (at node 93)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.218151
	minimum = 0.09 (at node 45)
	maximum = 0.382 (at node 113)
Accepted flit rate average= 0.163557
	minimum = 0.036 (at node 93)
	maximum = 0.305 (at node 165)
Injected packet length average = 17.7705
Accepted packet length average = 18.7817
Total in-flight flits = 11743 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 315.479
	minimum = 23
	maximum = 1456
Network latency average = 272.356
	minimum = 23
	maximum = 1430
Slowest packet = 541
Flit latency average = 227.83
	minimum = 6
	maximum = 1407
Slowest flit = 9754
Fragmentation average = 51.4392
	minimum = 0
	maximum = 143
Injected packet rate average = 0.011
	minimum = 0.005 (at node 40)
	maximum = 0.0165 (at node 109)
Accepted packet rate average = 0.00888802
	minimum = 0.0055 (at node 30)
	maximum = 0.014 (at node 22)
Injected flit rate average = 0.196688
	minimum = 0.084 (at node 40)
	maximum = 0.297 (at node 109)
Accepted flit rate average= 0.162964
	minimum = 0.099 (at node 30)
	maximum = 0.266 (at node 22)
Injected packet length average = 17.8807
Accepted packet length average = 18.3352
Total in-flight flits = 14894 (0 measured)
latency change    = 0.4398
throughput change = 0.00364345
Class 0:
Packet latency average = 725.099
	minimum = 23
	maximum = 2257
Network latency average = 452.754
	minimum = 23
	maximum = 1879
Slowest packet = 1352
Flit latency average = 397.197
	minimum = 6
	maximum = 1840
Slowest flit = 36144
Fragmentation average = 58.1094
	minimum = 0
	maximum = 152
Injected packet rate average = 0.00914062
	minimum = 0 (at node 12)
	maximum = 0.022 (at node 59)
Accepted packet rate average = 0.00885417
	minimum = 0.001 (at node 116)
	maximum = 0.016 (at node 71)
Injected flit rate average = 0.163958
	minimum = 0 (at node 12)
	maximum = 0.395 (at node 120)
Accepted flit rate average= 0.159818
	minimum = 0.018 (at node 116)
	maximum = 0.305 (at node 71)
Injected packet length average = 17.9373
Accepted packet length average = 18.05
Total in-flight flits = 16123 (0 measured)
latency change    = 0.564916
throughput change = 0.0196839
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 941.48
	minimum = 31
	maximum = 2678
Network latency average = 319.759
	minimum = 23
	maximum = 907
Slowest packet = 6164
Flit latency average = 454.494
	minimum = 6
	maximum = 2640
Slowest flit = 47951
Fragmentation average = 55.5054
	minimum = 0
	maximum = 139
Injected packet rate average = 0.0090625
	minimum = 0 (at node 188)
	maximum = 0.019 (at node 141)
Accepted packet rate average = 0.00881771
	minimum = 0.001 (at node 184)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.162286
	minimum = 0 (at node 188)
	maximum = 0.328 (at node 141)
Accepted flit rate average= 0.158099
	minimum = 0.002 (at node 184)
	maximum = 0.333 (at node 56)
Injected packet length average = 17.9075
Accepted packet length average = 17.9297
Total in-flight flits = 16926 (15115 measured)
latency change    = 0.229831
throughput change = 0.0108714
Class 0:
Packet latency average = 1237.05
	minimum = 23
	maximum = 3472
Network latency average = 430.795
	minimum = 23
	maximum = 1877
Slowest packet = 6164
Flit latency average = 470.079
	minimum = 6
	maximum = 2817
Slowest flit = 65933
Fragmentation average = 56.502
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00904688
	minimum = 0.0005 (at node 188)
	maximum = 0.018 (at node 141)
Accepted packet rate average = 0.00890365
	minimum = 0.004 (at node 85)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.16288
	minimum = 0.009 (at node 188)
	maximum = 0.319 (at node 141)
Accepted flit rate average= 0.160294
	minimum = 0.065 (at node 163)
	maximum = 0.288 (at node 34)
Injected packet length average = 18.004
Accepted packet length average = 18.0032
Total in-flight flits = 17156 (17069 measured)
latency change    = 0.238931
throughput change = 0.0136955
Class 0:
Packet latency average = 1454.01
	minimum = 23
	maximum = 4226
Network latency average = 467.35
	minimum = 23
	maximum = 2648
Slowest packet = 6164
Flit latency average = 466.978
	minimum = 6
	maximum = 2817
Slowest flit = 65933
Fragmentation average = 56.5781
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00905208
	minimum = 0.00166667 (at node 188)
	maximum = 0.0186667 (at node 141)
Accepted packet rate average = 0.00893576
	minimum = 0.00566667 (at node 17)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.162642
	minimum = 0.03 (at node 188)
	maximum = 0.336 (at node 141)
Accepted flit rate average= 0.160795
	minimum = 0.0996667 (at node 163)
	maximum = 0.264 (at node 16)
Injected packet length average = 17.9674
Accepted packet length average = 17.9946
Total in-flight flits = 17411 (17411 measured)
latency change    = 0.149214
throughput change = 0.00311495
Class 0:
Packet latency average = 1652.73
	minimum = 23
	maximum = 4928
Network latency average = 488.031
	minimum = 23
	maximum = 3155
Slowest packet = 6164
Flit latency average = 470.27
	minimum = 6
	maximum = 3089
Slowest flit = 132588
Fragmentation average = 57.2744
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00900651
	minimum = 0.00325 (at node 188)
	maximum = 0.01525 (at node 141)
Accepted packet rate average = 0.00891927
	minimum = 0.00525 (at node 4)
	maximum = 0.0135 (at node 16)
Injected flit rate average = 0.162091
	minimum = 0.0585 (at node 188)
	maximum = 0.27175 (at node 141)
Accepted flit rate average= 0.160566
	minimum = 0.09125 (at node 4)
	maximum = 0.243 (at node 16)
Injected packet length average = 17.9971
Accepted packet length average = 18.0022
Total in-flight flits = 17422 (17422 measured)
latency change    = 0.120238
throughput change = 0.00142454
Class 0:
Packet latency average = 1836.06
	minimum = 23
	maximum = 5555
Network latency average = 501.857
	minimum = 23
	maximum = 3155
Slowest packet = 6164
Flit latency average = 473.525
	minimum = 6
	maximum = 3143
Slowest flit = 167090
Fragmentation average = 57.2942
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00905104
	minimum = 0.0034 (at node 46)
	maximum = 0.0142 (at node 141)
Accepted packet rate average = 0.00896667
	minimum = 0.0064 (at node 36)
	maximum = 0.0122 (at node 16)
Injected flit rate average = 0.162832
	minimum = 0.0612 (at node 46)
	maximum = 0.2556 (at node 141)
Accepted flit rate average= 0.161403
	minimum = 0.1124 (at node 163)
	maximum = 0.2196 (at node 16)
Injected packet length average = 17.9904
Accepted packet length average = 18.0003
Total in-flight flits = 17722 (17722 measured)
latency change    = 0.0998502
throughput change = 0.00518403
Class 0:
Packet latency average = 2011.97
	minimum = 23
	maximum = 5555
Network latency average = 507.273
	minimum = 23
	maximum = 3176
Slowest packet = 6164
Flit latency average = 472.749
	minimum = 6
	maximum = 3152
Slowest flit = 167093
Fragmentation average = 57.0954
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00905295
	minimum = 0.00433333 (at node 46)
	maximum = 0.0136667 (at node 141)
Accepted packet rate average = 0.00898177
	minimum = 0.00616667 (at node 36)
	maximum = 0.012 (at node 99)
Injected flit rate average = 0.162765
	minimum = 0.0778333 (at node 46)
	maximum = 0.245333 (at node 141)
Accepted flit rate average= 0.161629
	minimum = 0.111 (at node 36)
	maximum = 0.2145 (at node 99)
Injected packet length average = 17.9792
Accepted packet length average = 17.9953
Total in-flight flits = 17720 (17720 measured)
latency change    = 0.0874317
throughput change = 0.00139959
Class 0:
Packet latency average = 2181.12
	minimum = 23
	maximum = 5716
Network latency average = 514.495
	minimum = 23
	maximum = 3176
Slowest packet = 6164
Flit latency average = 475.441
	minimum = 6
	maximum = 3152
Slowest flit = 167093
Fragmentation average = 57.4401
	minimum = 0
	maximum = 139
Injected packet rate average = 0.00906696
	minimum = 0.00471429 (at node 46)
	maximum = 0.0141429 (at node 141)
Accepted packet rate average = 0.00899256
	minimum = 0.00628571 (at node 36)
	maximum = 0.0115714 (at node 128)
Injected flit rate average = 0.163066
	minimum = 0.0825714 (at node 46)
	maximum = 0.254571 (at node 141)
Accepted flit rate average= 0.161844
	minimum = 0.113714 (at node 135)
	maximum = 0.209143 (at node 128)
Injected packet length average = 17.9847
Accepted packet length average = 17.9975
Total in-flight flits = 17989 (17989 measured)
latency change    = 0.0775524
throughput change = 0.00132479
Draining all recorded packets ...
Class 0:
Remaining flits: 272849 272850 272851 272852 272853 272854 272855 272856 272857 272858 [...] (17713 flits)
Measured flits: 272849 272850 272851 272852 272853 272854 272855 272856 272857 272858 [...] (17299 flits)
Class 0:
Remaining flits: 322442 322443 322444 322445 322446 322447 322448 322449 322450 322451 [...] (17300 flits)
Measured flits: 322442 322443 322444 322445 322446 322447 322448 322449 322450 322451 [...] (16204 flits)
Class 0:
Remaining flits: 348645 348646 348647 348648 348649 348650 348651 348652 348653 348654 [...] (17766 flits)
Measured flits: 348645 348646 348647 348648 348649 348650 348651 348652 348653 348654 [...] (14745 flits)
Class 0:
Remaining flits: 365292 365293 365294 365295 365296 365297 365298 365299 365300 365301 [...] (17606 flits)
Measured flits: 365292 365293 365294 365295 365296 365297 365298 365299 365300 365301 [...] (11079 flits)
Class 0:
Remaining flits: 392292 392293 392294 392295 392296 392297 392298 392299 392300 392301 [...] (17557 flits)
Measured flits: 392292 392293 392294 392295 392296 392297 392298 392299 392300 392301 [...] (8185 flits)
Class 0:
Remaining flits: 417834 417835 417836 417837 417838 417839 417840 417841 417842 417843 [...] (18033 flits)
Measured flits: 417834 417835 417836 417837 417838 417839 417840 417841 417842 417843 [...] (5037 flits)
Class 0:
Remaining flits: 476286 476287 476288 476289 476290 476291 476292 476293 476294 476295 [...] (17540 flits)
Measured flits: 482150 482151 482152 482153 482154 482155 482156 482157 482158 482159 [...] (2480 flits)
Class 0:
Remaining flits: 501894 501895 501896 501897 501898 501899 501900 501901 501902 501903 [...] (17620 flits)
Measured flits: 520110 520111 520112 520113 520114 520115 520116 520117 520118 520119 [...] (1146 flits)
Class 0:
Remaining flits: 509130 509131 509132 509133 509134 509135 509136 509137 509138 509139 [...] (17783 flits)
Measured flits: 536238 536239 536240 536241 536242 536243 536244 536245 536246 536247 [...] (702 flits)
Class 0:
Remaining flits: 542590 542591 547053 547054 547055 548928 548929 548930 548931 548932 [...] (17680 flits)
Measured flits: 571446 571447 571448 571449 571450 571451 571452 571453 571454 571455 [...] (486 flits)
Class 0:
Remaining flits: 605758 605759 605760 605761 605762 605763 605764 605765 605766 605767 [...] (18082 flits)
Measured flits: 645624 645625 645626 645627 645628 645629 645630 645631 645632 645633 [...] (72 flits)
Draining remaining packets ...
Time taken is 22293 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3329.16 (1 samples)
	minimum = 23 (1 samples)
	maximum = 11542 (1 samples)
Network latency average = 542.505 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3307 (1 samples)
Flit latency average = 488.589 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3338 (1 samples)
Fragmentation average = 58.0046 (1 samples)
	minimum = 0 (1 samples)
	maximum = 154 (1 samples)
Injected packet rate average = 0.00906696 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0141429 (1 samples)
Accepted packet rate average = 0.00899256 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0115714 (1 samples)
Injected flit rate average = 0.163066 (1 samples)
	minimum = 0.0825714 (1 samples)
	maximum = 0.254571 (1 samples)
Accepted flit rate average = 0.161844 (1 samples)
	minimum = 0.113714 (1 samples)
	maximum = 0.209143 (1 samples)
Injected packet size average = 17.9847 (1 samples)
Accepted packet size average = 17.9975 (1 samples)
Hops average = 5.06474 (1 samples)
Total run time 15.7086
