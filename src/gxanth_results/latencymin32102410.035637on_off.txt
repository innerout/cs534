BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 375.628
	minimum = 27
	maximum = 989
Network latency average = 280.957
	minimum = 23
	maximum = 877
Slowest packet = 21
Flit latency average = 216.461
	minimum = 6
	maximum = 960
Slowest flit = 2534
Fragmentation average = 146.352
	minimum = 0
	maximum = 756
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0101979
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 88)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.209714
	minimum = 0.089 (at node 41)
	maximum = 0.357 (at node 166)
Injected packet length average = 17.8246
Accepted packet length average = 20.5644
Total in-flight flits = 60697 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 644.069
	minimum = 27
	maximum = 1975
Network latency average = 504.588
	minimum = 23
	maximum = 1891
Slowest packet = 166
Flit latency average = 429.128
	minimum = 6
	maximum = 1874
Slowest flit = 7433
Fragmentation average = 182.173
	minimum = 0
	maximum = 1609
Injected packet rate average = 0.031362
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 28)
Accepted packet rate average = 0.0116693
	minimum = 0.007 (at node 24)
	maximum = 0.018 (at node 166)
Injected flit rate average = 0.561727
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 28)
Accepted flit rate average= 0.223971
	minimum = 0.135 (at node 135)
	maximum = 0.3395 (at node 166)
Injected packet length average = 17.9111
Accepted packet length average = 19.1933
Total in-flight flits = 130769 (0 measured)
latency change    = 0.416789
throughput change = 0.0636591
Class 0:
Packet latency average = 1362.37
	minimum = 31
	maximum = 2944
Network latency average = 1136
	minimum = 23
	maximum = 2741
Slowest packet = 2211
Flit latency average = 1087.53
	minimum = 6
	maximum = 2800
Slowest flit = 16206
Fragmentation average = 211.697
	minimum = 0
	maximum = 1684
Injected packet rate average = 0.0354427
	minimum = 0 (at node 14)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0133281
	minimum = 0.004 (at node 75)
	maximum = 0.023 (at node 24)
Injected flit rate average = 0.638953
	minimum = 0 (at node 14)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.240969
	minimum = 0.079 (at node 75)
	maximum = 0.386 (at node 177)
Injected packet length average = 18.0278
Accepted packet length average = 18.0797
Total in-flight flits = 206993 (0 measured)
latency change    = 0.527244
throughput change = 0.0705378
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 420.348
	minimum = 33
	maximum = 2329
Network latency average = 80.8846
	minimum = 25
	maximum = 841
Slowest packet = 18901
Flit latency average = 1519.62
	minimum = 6
	maximum = 3726
Slowest flit = 24787
Fragmentation average = 25.0679
	minimum = 0
	maximum = 349
Injected packet rate average = 0.03525
	minimum = 0 (at node 42)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.0137083
	minimum = 0.003 (at node 52)
	maximum = 0.024 (at node 119)
Injected flit rate average = 0.633786
	minimum = 0 (at node 42)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.244958
	minimum = 0.088 (at node 52)
	maximum = 0.432 (at node 119)
Injected packet length average = 17.9798
Accepted packet length average = 17.8693
Total in-flight flits = 281785 (110890 measured)
latency change    = 2.24106
throughput change = 0.0162868
Class 0:
Packet latency average = 522.122
	minimum = 29
	maximum = 2329
Network latency average = 196.074
	minimum = 23
	maximum = 1955
Slowest packet = 18901
Flit latency average = 1779.15
	minimum = 6
	maximum = 4636
Slowest flit = 33627
Fragmentation average = 40.8225
	minimum = 0
	maximum = 790
Injected packet rate average = 0.0349375
	minimum = 0.002 (at node 86)
	maximum = 0.056 (at node 106)
Accepted packet rate average = 0.0135286
	minimum = 0.0065 (at node 36)
	maximum = 0.02 (at node 156)
Injected flit rate average = 0.62856
	minimum = 0.036 (at node 86)
	maximum = 1 (at node 0)
Accepted flit rate average= 0.242378
	minimum = 0.121 (at node 36)
	maximum = 0.3625 (at node 156)
Injected packet length average = 17.991
Accepted packet length average = 17.9159
Total in-flight flits = 355408 (217545 measured)
latency change    = 0.194924
throughput change = 0.0106476
Class 0:
Packet latency average = 763.924
	minimum = 29
	maximum = 3406
Network latency average = 445.524
	minimum = 23
	maximum = 2982
Slowest packet = 18901
Flit latency average = 2037.27
	minimum = 6
	maximum = 5627
Slowest flit = 25316
Fragmentation average = 59.747
	minimum = 0
	maximum = 1037
Injected packet rate average = 0.0350694
	minimum = 0.00533333 (at node 38)
	maximum = 0.0556667 (at node 9)
Accepted packet rate average = 0.0134271
	minimum = 0.00833333 (at node 36)
	maximum = 0.0186667 (at node 53)
Injected flit rate average = 0.63112
	minimum = 0.0923333 (at node 38)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.241094
	minimum = 0.148333 (at node 36)
	maximum = 0.334667 (at node 53)
Injected packet length average = 17.9963
Accepted packet length average = 17.9558
Total in-flight flits = 431723 (323160 measured)
latency change    = 0.316526
throughput change = 0.00532512
Class 0:
Packet latency average = 1124.89
	minimum = 28
	maximum = 4804
Network latency average = 822.596
	minimum = 23
	maximum = 3973
Slowest packet = 18901
Flit latency average = 2289.14
	minimum = 6
	maximum = 6587
Slowest flit = 25325
Fragmentation average = 83.095
	minimum = 0
	maximum = 1450
Injected packet rate average = 0.034901
	minimum = 0.011 (at node 30)
	maximum = 0.0555 (at node 9)
Accepted packet rate average = 0.013375
	minimum = 0.00825 (at node 52)
	maximum = 0.0175 (at node 107)
Injected flit rate average = 0.627934
	minimum = 0.198 (at node 30)
	maximum = 1 (at node 9)
Accepted flit rate average= 0.239605
	minimum = 0.156 (at node 52)
	maximum = 0.31375 (at node 168)
Injected packet length average = 17.9918
Accepted packet length average = 17.9144
Total in-flight flits = 505448 (421379 measured)
latency change    = 0.320892
throughput change = 0.00621138
Class 0:
Packet latency average = 1513.05
	minimum = 25
	maximum = 5758
Network latency average = 1216.14
	minimum = 23
	maximum = 4962
Slowest packet = 18901
Flit latency average = 2554.34
	minimum = 6
	maximum = 7425
Slowest flit = 52107
Fragmentation average = 96.8914
	minimum = 0
	maximum = 1682
Injected packet rate average = 0.0343333
	minimum = 0.0088 (at node 30)
	maximum = 0.054 (at node 52)
Accepted packet rate average = 0.0132823
	minimum = 0.0088 (at node 89)
	maximum = 0.0172 (at node 167)
Injected flit rate average = 0.617837
	minimum = 0.1584 (at node 30)
	maximum = 0.9718 (at node 52)
Accepted flit rate average= 0.238038
	minimum = 0.1622 (at node 89)
	maximum = 0.3072 (at node 103)
Injected packet length average = 17.9953
Accepted packet length average = 17.9214
Total in-flight flits = 571757 (508412 measured)
latency change    = 0.256538
throughput change = 0.00658707
Class 0:
Packet latency average = 1929.39
	minimum = 25
	maximum = 7294
Network latency average = 1633.55
	minimum = 23
	maximum = 5914
Slowest packet = 18901
Flit latency average = 2805.88
	minimum = 6
	maximum = 8362
Slowest flit = 57581
Fragmentation average = 109.365
	minimum = 0
	maximum = 2583
Injected packet rate average = 0.0343342
	minimum = 0.0123333 (at node 30)
	maximum = 0.0536667 (at node 15)
Accepted packet rate average = 0.0132057
	minimum = 0.00933333 (at node 89)
	maximum = 0.0168333 (at node 118)
Injected flit rate average = 0.617925
	minimum = 0.222 (at node 30)
	maximum = 0.9655 (at node 15)
Accepted flit rate average= 0.236893
	minimum = 0.167833 (at node 89)
	maximum = 0.302 (at node 118)
Injected packet length average = 17.9974
Accepted packet length average = 17.9387
Total in-flight flits = 646046 (598911 measured)
latency change    = 0.215791
throughput change = 0.00483032
Class 0:
Packet latency average = 2390.35
	minimum = 25
	maximum = 8163
Network latency average = 2088.35
	minimum = 23
	maximum = 6970
Slowest packet = 18901
Flit latency average = 3057.3
	minimum = 6
	maximum = 9089
Slowest flit = 61343
Fragmentation average = 119.21
	minimum = 0
	maximum = 2583
Injected packet rate average = 0.0343437
	minimum = 0.013 (at node 30)
	maximum = 0.0534286 (at node 15)
Accepted packet rate average = 0.0131801
	minimum = 0.01 (at node 105)
	maximum = 0.0162857 (at node 157)
Injected flit rate average = 0.618076
	minimum = 0.234 (at node 30)
	maximum = 0.959857 (at node 15)
Accepted flit rate average= 0.236361
	minimum = 0.176286 (at node 105)
	maximum = 0.292143 (at node 118)
Injected packet length average = 17.9968
Accepted packet length average = 17.9332
Total in-flight flits = 720168 (685138 measured)
latency change    = 0.192842
throughput change = 0.00225234
Draining all recorded packets ...
Class 0:
Remaining flits: 85968 85969 85970 85971 85972 85973 85974 85975 85976 85977 [...] (793991 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (703286 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (863153 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (682340 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (929558 flits)
Measured flits: 339264 339265 339266 339267 339268 339269 339270 339271 339272 339273 [...] (655850 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (996793 flits)
Measured flits: 339318 339319 339320 339321 339322 339323 339324 339325 339326 339327 [...] (628358 flits)
Class 0:
Remaining flits: 89334 89335 89336 89337 89338 89339 89340 89341 89342 89343 [...] (1060294 flits)
Measured flits: 339624 339625 339626 339627 339628 339629 339630 339631 339632 339633 [...] (599921 flits)
Class 0:
Remaining flits: 142688 142689 142690 142691 142692 142693 142694 142695 142696 142697 [...] (1130719 flits)
Measured flits: 339796 339797 339798 339799 339800 339801 339802 339803 339876 339877 [...] (570255 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (1197138 flits)
Measured flits: 339930 339931 339932 339933 339934 339935 339936 339937 339938 339939 [...] (539976 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (1259436 flits)
Measured flits: 340064 340065 340066 340067 340068 340069 340070 340071 340072 340073 [...] (509278 flits)
Class 0:
Remaining flits: 149040 149041 149042 149043 149044 149045 149046 149047 149048 149049 [...] (1327035 flits)
Measured flits: 340092 340093 340094 340095 340096 340097 340098 340099 340100 340101 [...] (478682 flits)
Class 0:
Remaining flits: 208417 208418 208419 208420 208421 213084 213085 213086 213087 213088 [...] (1392990 flits)
Measured flits: 340092 340093 340094 340095 340096 340097 340098 340099 340100 340101 [...] (447956 flits)
Class 0:
Remaining flits: 208417 208418 208419 208420 208421 229932 229933 229934 229935 229936 [...] (1462251 flits)
Measured flits: 340092 340093 340094 340095 340096 340097 340098 340099 340100 340101 [...] (418271 flits)
Class 0:
Remaining flits: 239994 239995 239996 239997 239998 239999 240000 240001 240002 240003 [...] (1519309 flits)
Measured flits: 342648 342649 342650 342651 342652 342653 342654 342655 342656 342657 [...] (388580 flits)
Class 0:
Remaining flits: 239994 239995 239996 239997 239998 239999 240000 240001 240002 240003 [...] (1578949 flits)
Measured flits: 342648 342649 342650 342651 342652 342653 342654 342655 342656 342657 [...] (359568 flits)
Class 0:
Remaining flits: 239994 239995 239996 239997 239998 239999 240000 240001 240002 240003 [...] (1643223 flits)
Measured flits: 343026 343027 343028 343029 343030 343031 343032 343033 343034 343035 [...] (332142 flits)
Class 0:
Remaining flits: 239994 239995 239996 239997 239998 239999 240000 240001 240002 240003 [...] (1704612 flits)
Measured flits: 343440 343441 343442 343443 343444 343445 343446 343447 343448 343449 [...] (306393 flits)
Class 0:
Remaining flits: 239994 239995 239996 239997 239998 239999 240000 240001 240002 240003 [...] (1769192 flits)
Measured flits: 343440 343441 343442 343443 343444 343445 343446 343447 343448 343449 [...] (281861 flits)
Class 0:
Remaining flits: 296262 296263 296264 296265 296266 296267 296268 296269 296270 296271 [...] (1832180 flits)
Measured flits: 343455 343456 343457 346577 346578 346579 346580 346581 346582 346583 [...] (258705 flits)
Class 0:
Remaining flits: 308778 308779 308780 308781 308782 308783 308784 308785 308786 308787 [...] (1884766 flits)
Measured flits: 368388 368389 368390 368391 368392 368393 368394 368395 368396 368397 [...] (235556 flits)
Class 0:
Remaining flits: 320382 320383 320384 320385 320386 320387 320388 320389 320390 320391 [...] (1936126 flits)
Measured flits: 368388 368389 368390 368391 368392 368393 368394 368395 368396 368397 [...] (214272 flits)
Class 0:
Remaining flits: 320384 320385 320386 320387 320388 320389 320390 320391 320392 320393 [...] (1984549 flits)
Measured flits: 368388 368389 368390 368391 368392 368393 368394 368395 368396 368397 [...] (195233 flits)
Class 0:
Remaining flits: 320384 320385 320386 320387 320388 320389 320390 320391 320392 320393 [...] (2028927 flits)
Measured flits: 402732 402733 402734 402735 402736 402737 402738 402739 402740 402741 [...] (176118 flits)
Class 0:
Remaining flits: 320396 320397 320398 320399 402732 402733 402734 402735 402736 402737 [...] (2069321 flits)
Measured flits: 402732 402733 402734 402735 402736 402737 402738 402739 402740 402741 [...] (159480 flits)
Class 0:
Remaining flits: 402732 402733 402734 402735 402736 402737 402738 402739 402740 402741 [...] (2107581 flits)
Measured flits: 402732 402733 402734 402735 402736 402737 402738 402739 402740 402741 [...] (143282 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2146803 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (128413 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2180584 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (115172 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2206292 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (102543 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2232345 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (91188 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2253833 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (81022 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2270735 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (71662 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2292301 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (62629 flits)
Class 0:
Remaining flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (2308110 flits)
Measured flits: 409554 409555 409556 409557 409558 409559 409560 409561 409562 409563 [...] (54618 flits)
Class 0:
Remaining flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (2324913 flits)
Measured flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (47862 flits)
Class 0:
Remaining flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (2341619 flits)
Measured flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (42179 flits)
Class 0:
Remaining flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (2353742 flits)
Measured flits: 493038 493039 493040 493041 493042 493043 493044 493045 493046 493047 [...] (36565 flits)
Class 0:
Remaining flits: 539519 539520 539521 539522 539523 539524 539525 539526 539527 539528 [...] (2365027 flits)
Measured flits: 539519 539520 539521 539522 539523 539524 539525 539526 539527 539528 [...] (32098 flits)
Class 0:
Remaining flits: 550998 550999 551000 551001 551002 551003 551004 551005 551006 551007 [...] (2379778 flits)
Measured flits: 550998 550999 551000 551001 551002 551003 551004 551005 551006 551007 [...] (27690 flits)
Class 0:
Remaining flits: 551006 551007 551008 551009 551010 551011 551012 551013 551014 551015 [...] (2391237 flits)
Measured flits: 551006 551007 551008 551009 551010 551011 551012 551013 551014 551015 [...] (23179 flits)
Class 0:
Remaining flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (2400258 flits)
Measured flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (19678 flits)
Class 0:
Remaining flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (2409255 flits)
Measured flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (16803 flits)
Class 0:
Remaining flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (2416831 flits)
Measured flits: 622566 622567 622568 622569 622570 622571 622572 622573 622574 622575 [...] (14290 flits)
Class 0:
Remaining flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (2424173 flits)
Measured flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (12372 flits)
Class 0:
Remaining flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (2430067 flits)
Measured flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (10582 flits)
Class 0:
Remaining flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (2435911 flits)
Measured flits: 673092 673093 673094 673095 673096 673097 673098 673099 673100 673101 [...] (8832 flits)
Class 0:
Remaining flits: 738954 738955 738956 738957 738958 738959 738960 738961 738962 738963 [...] (2437157 flits)
Measured flits: 738954 738955 738956 738957 738958 738959 738960 738961 738962 738963 [...] (7302 flits)
Class 0:
Remaining flits: 738954 738955 738956 738957 738958 738959 738960 738961 738962 738963 [...] (2440750 flits)
Measured flits: 738954 738955 738956 738957 738958 738959 738960 738961 738962 738963 [...] (6256 flits)
Class 0:
Remaining flits: 829890 829891 829892 829893 829894 829895 829896 829897 829898 829899 [...] (2446544 flits)
Measured flits: 829890 829891 829892 829893 829894 829895 829896 829897 829898 829899 [...] (5162 flits)
Class 0:
Remaining flits: 829906 829907 849132 849133 849134 849135 849136 849137 849138 849139 [...] (2448326 flits)
Measured flits: 829906 829907 849132 849133 849134 849135 849136 849137 849138 849139 [...] (4285 flits)
Class 0:
Remaining flits: 852376 852377 852378 852379 852380 852381 852382 852383 852384 852385 [...] (2449172 flits)
Measured flits: 852376 852377 852378 852379 852380 852381 852382 852383 852384 852385 [...] (3523 flits)
Class 0:
Remaining flits: 866196 866197 866198 866199 866200 866201 866202 866203 866204 866205 [...] (2450570 flits)
Measured flits: 866196 866197 866198 866199 866200 866201 866202 866203 866204 866205 [...] (2938 flits)
Class 0:
Remaining flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (2449072 flits)
Measured flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (2523 flits)
Class 0:
Remaining flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (2453053 flits)
Measured flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (1968 flits)
Class 0:
Remaining flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (2450461 flits)
Measured flits: 874386 874387 874388 874389 874390 874391 874392 874393 874394 874395 [...] (1508 flits)
Class 0:
Remaining flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (2449507 flits)
Measured flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (1246 flits)
Class 0:
Remaining flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (2448740 flits)
Measured flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (1059 flits)
Class 0:
Remaining flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (2445308 flits)
Measured flits: 955476 955477 955478 955479 955480 955481 955482 955483 955484 955485 [...] (918 flits)
Class 0:
Remaining flits: 992718 992719 992720 992721 992722 992723 992724 992725 992726 992727 [...] (2439711 flits)
Measured flits: 992718 992719 992720 992721 992722 992723 992724 992725 992726 992727 [...] (799 flits)
Class 0:
Remaining flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (2434427 flits)
Measured flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (648 flits)
Class 0:
Remaining flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (2426719 flits)
Measured flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (494 flits)
Class 0:
Remaining flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (2424741 flits)
Measured flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (412 flits)
Class 0:
Remaining flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (2417767 flits)
Measured flits: 1003068 1003069 1003070 1003071 1003072 1003073 1003074 1003075 1003076 1003077 [...] (306 flits)
Class 0:
Remaining flits: 1003080 1003081 1003082 1003083 1003084 1003085 1054962 1054963 1054964 1054965 [...] (2409735 flits)
Measured flits: 1003080 1003081 1003082 1003083 1003084 1003085 1054962 1054963 1054964 1054965 [...] (258 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2400382 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (218 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2394055 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (197 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2390285 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (162 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2384721 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (144 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2376416 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (82 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2369199 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (18 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2363473 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (18 flits)
Class 0:
Remaining flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (2357227 flits)
Measured flits: 1054962 1054963 1054964 1054965 1054966 1054967 1054968 1054969 1054970 1054971 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2321792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2286688 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2251080 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2215873 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2181036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2145742 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321830 1321831 1321832 1321833 1321834 1321835 1321836 1321837 1321838 1321839 [...] (2110216 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1321837 1321838 1321839 1321840 1321841 1321842 1321843 1321844 1321845 1321846 [...] (2074517 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1555974 1555975 1555976 1555977 1555978 1555979 1555980 1555981 1555982 1555983 [...] (2039202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1562922 1562923 1562924 1562925 1562926 1562927 1562928 1562929 1562930 1562931 [...] (2004723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1562922 1562923 1562924 1562925 1562926 1562927 1562928 1562929 1562930 1562931 [...] (1969917 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1562922 1562923 1562924 1562925 1562926 1562927 1562928 1562929 1562930 1562931 [...] (1934301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1898667 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1863625 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1828373 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1792456 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1756800 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1721643 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1685458 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565910 1565911 1565912 1565913 1565914 1565915 1565916 1565917 1565918 1565919 [...] (1649886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1637784 1637785 1637786 1637787 1637788 1637789 1637790 1637791 1637792 1637793 [...] (1613803 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1637784 1637785 1637786 1637787 1637788 1637789 1637790 1637791 1637792 1637793 [...] (1577851 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1637784 1637785 1637786 1637787 1637788 1637789 1637790 1637791 1637792 1637793 [...] (1542631 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1656594 1656595 1656596 1656597 1656598 1656599 1656600 1656601 1656602 1656603 [...] (1507299 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1699022 1699023 1699024 1699025 1699026 1699027 1699028 1699029 1699030 1699031 [...] (1471793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1789389 1789390 1789391 1789392 1789393 1789394 1789395 1789396 1789397 1861272 [...] (1436897 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1861272 1861273 1861274 1861275 1861276 1861277 1861278 1861279 1861280 1861281 [...] (1402565 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1861272 1861273 1861274 1861275 1861276 1861277 1861278 1861279 1861280 1861281 [...] (1368104 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1864530 1864531 1864532 1864533 1864534 1864535 1864536 1864537 1864538 1864539 [...] (1333577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1875438 1875439 1875440 1875441 1875442 1875443 1875444 1875445 1875446 1875447 [...] (1299306 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1875447 1875448 1875449 1875450 1875451 1875452 1875453 1875454 1875455 1920690 [...] (1264537 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1920690 1920691 1920692 1920693 1920694 1920695 1920696 1920697 1920698 1920699 [...] (1230358 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1196294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1161754 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1127210 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1093122 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1060139 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (1026983 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2031426 2031427 2031428 2031429 2031430 2031431 2031432 2031433 2031434 2031435 [...] (993617 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (960024 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (926782 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (893793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (860282 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (827097 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (792947 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2122560 2122561 2122562 2122563 2122564 2122565 2122566 2122567 2122568 2122569 [...] (759140 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2207718 2207719 2207720 2207721 2207722 2207723 2207724 2207725 2207726 2207727 [...] (726078 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2207733 2207734 2207735 2366226 2366227 2366228 2366229 2366230 2366231 2366232 [...] (692407 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (658847 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (625013 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (591109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (557119 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (523008 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (489135 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (454780 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (420406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (385843 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (351159 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (316576 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (282604 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (249398 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (215534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (182792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (151160 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (120555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2366226 2366227 2366228 2366229 2366230 2366231 2366232 2366233 2366234 2366235 [...] (93026 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2471245 2471246 2471247 2471248 2471249 2471250 2471251 2471252 2471253 2471254 [...] (69041 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2471245 2471246 2471247 2471248 2471249 2471250 2471251 2471252 2471253 2471254 [...] (48281 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2876580 2876581 2876582 2876583 2876584 2876585 2876586 2876587 2876588 2876589 [...] (29114 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3470580 3470581 3470582 3470583 3470584 3470585 3470586 3470587 3470588 3470589 [...] (17131 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3470580 3470581 3470582 3470583 3470584 3470585 3470586 3470587 3470588 3470589 [...] (10257 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3470580 3470581 3470582 3470583 3470584 3470585 3470586 3470587 3470588 3470589 [...] (5624 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3781782 3781783 3781784 3781785 3781786 3781787 3781788 3781789 3781790 3781791 [...] (3080 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4015088 4015089 4015090 4015091 4015092 4015093 4015094 4015095 4015096 4015097 [...] (2079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4237308 4237309 4237310 4237311 4237312 4237313 4237314 4237315 4237316 4237317 [...] (1079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 5002521 5002522 5002523 5015016 5015017 5015018 5015019 5015020 5015021 5015022 [...] (75 flits)
Measured flits: (0 flits)
Time taken is 155268 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15288 (1 samples)
	minimum = 25 (1 samples)
	maximum = 71085 (1 samples)
Network latency average = 14784.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 70151 (1 samples)
Flit latency average = 43016.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 125132 (1 samples)
Fragmentation average = 194.064 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4281 (1 samples)
Injected packet rate average = 0.0343437 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.0534286 (1 samples)
Accepted packet rate average = 0.0131801 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0162857 (1 samples)
Injected flit rate average = 0.618076 (1 samples)
	minimum = 0.234 (1 samples)
	maximum = 0.959857 (1 samples)
Accepted flit rate average = 0.236361 (1 samples)
	minimum = 0.176286 (1 samples)
	maximum = 0.292143 (1 samples)
Injected packet size average = 17.9968 (1 samples)
Accepted packet size average = 17.9332 (1 samples)
Hops average = 5.06735 (1 samples)
Total run time 200.032
