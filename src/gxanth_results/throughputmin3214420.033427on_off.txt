BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 343.886
	minimum = 24
	maximum = 955
Network latency average = 233.455
	minimum = 22
	maximum = 778
Slowest packet = 57
Flit latency average = 203.711
	minimum = 5
	maximum = 788
Slowest flit = 17104
Fragmentation average = 43.6166
	minimum = 0
	maximum = 413
Injected packet rate average = 0.0290625
	minimum = 0 (at node 5)
	maximum = 0.056 (at node 0)
Accepted packet rate average = 0.013776
	minimum = 0.007 (at node 23)
	maximum = 0.024 (at node 76)
Injected flit rate average = 0.517656
	minimum = 0 (at node 5)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.259057
	minimum = 0.126 (at node 28)
	maximum = 0.432 (at node 76)
Injected packet length average = 17.8118
Accepted packet length average = 18.8049
Total in-flight flits = 50701 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 604.237
	minimum = 22
	maximum = 1934
Network latency average = 446.717
	minimum = 22
	maximum = 1568
Slowest packet = 57
Flit latency average = 415.232
	minimum = 5
	maximum = 1664
Slowest flit = 28487
Fragmentation average = 62.9013
	minimum = 0
	maximum = 449
Injected packet rate average = 0.0305781
	minimum = 0.001 (at node 5)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.014776
	minimum = 0.0085 (at node 83)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.547818
	minimum = 0.018 (at node 5)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.27463
	minimum = 0.1595 (at node 83)
	maximum = 0.417 (at node 152)
Injected packet length average = 17.9153
Accepted packet length average = 18.5862
Total in-flight flits = 105898 (0 measured)
latency change    = 0.430876
throughput change = 0.056705
Class 0:
Packet latency average = 1330.32
	minimum = 28
	maximum = 2814
Network latency average = 1087.67
	minimum = 22
	maximum = 2362
Slowest packet = 3341
Flit latency average = 1052.97
	minimum = 5
	maximum = 2361
Slowest flit = 57642
Fragmentation average = 102.768
	minimum = 0
	maximum = 417
Injected packet rate average = 0.0312187
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 38)
Accepted packet rate average = 0.0160625
	minimum = 0.005 (at node 61)
	maximum = 0.032 (at node 51)
Injected flit rate average = 0.562953
	minimum = 0 (at node 0)
	maximum = 1 (at node 32)
Accepted flit rate average= 0.29037
	minimum = 0.111 (at node 61)
	maximum = 0.547 (at node 51)
Injected packet length average = 18.0325
Accepted packet length average = 18.0775
Total in-flight flits = 158075 (0 measured)
latency change    = 0.545795
throughput change = 0.0542053
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 341.625
	minimum = 27
	maximum = 2312
Network latency average = 46.2949
	minimum = 22
	maximum = 620
Slowest packet = 17747
Flit latency average = 1525.61
	minimum = 5
	maximum = 2994
Slowest flit = 93041
Fragmentation average = 6.4612
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0320469
	minimum = 0 (at node 100)
	maximum = 0.056 (at node 6)
Accepted packet rate average = 0.0159375
	minimum = 0.006 (at node 18)
	maximum = 0.026 (at node 91)
Injected flit rate average = 0.575927
	minimum = 0 (at node 100)
	maximum = 1 (at node 6)
Accepted flit rate average= 0.288026
	minimum = 0.16 (at node 64)
	maximum = 0.468 (at node 178)
Injected packet length average = 17.9714
Accepted packet length average = 18.0722
Total in-flight flits = 213564 (102572 measured)
latency change    = 2.89409
throughput change = 0.00813729
Class 0:
Packet latency average = 530.796
	minimum = 27
	maximum = 3126
Network latency average = 236.332
	minimum = 22
	maximum = 1865
Slowest packet = 17747
Flit latency average = 1767.48
	minimum = 5
	maximum = 3818
Slowest flit = 121212
Fragmentation average = 15.3291
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0309714
	minimum = 0.003 (at node 124)
	maximum = 0.0555 (at node 73)
Accepted packet rate average = 0.0157734
	minimum = 0.009 (at node 64)
	maximum = 0.0225 (at node 24)
Injected flit rate average = 0.557
	minimum = 0.054 (at node 124)
	maximum = 1 (at node 73)
Accepted flit rate average= 0.284333
	minimum = 0.1595 (at node 64)
	maximum = 0.3935 (at node 24)
Injected packet length average = 17.9844
Accepted packet length average = 18.0261
Total in-flight flits = 263127 (196834 measured)
latency change    = 0.35639
throughput change = 0.0129873
Class 0:
Packet latency average = 1146.05
	minimum = 22
	maximum = 3797
Network latency average = 868.623
	minimum = 22
	maximum = 2968
Slowest packet = 17747
Flit latency average = 2014.41
	minimum = 5
	maximum = 4520
Slowest flit = 150255
Fragmentation average = 34.8537
	minimum = 0
	maximum = 483
Injected packet rate average = 0.0291354
	minimum = 0.005 (at node 124)
	maximum = 0.052 (at node 73)
Accepted packet rate average = 0.0157118
	minimum = 0.0103333 (at node 104)
	maximum = 0.0216667 (at node 136)
Injected flit rate average = 0.524274
	minimum = 0.09 (at node 124)
	maximum = 0.940333 (at node 73)
Accepted flit rate average= 0.281693
	minimum = 0.195333 (at node 132)
	maximum = 0.393667 (at node 136)
Injected packet length average = 17.9944
Accepted packet length average = 17.9287
Total in-flight flits = 298238 (267703 measured)
latency change    = 0.536847
throughput change = 0.00937413
Draining remaining packets ...
Class 0:
Remaining flits: 144108 144109 144110 144111 144112 144113 144114 144115 144116 144117 [...] (248982 flits)
Measured flits: 319284 319285 319286 319287 319288 319289 319290 319291 319292 319293 [...] (235881 flits)
Class 0:
Remaining flits: 164718 164719 164720 164721 164722 164723 164724 164725 164726 164727 [...] (200215 flits)
Measured flits: 319284 319285 319286 319287 319288 319289 319290 319291 319292 319293 [...] (194469 flits)
Class 0:
Remaining flits: 189867 189868 189869 189870 189871 189872 189873 189874 189875 189876 [...] (152018 flits)
Measured flits: 319572 319573 319574 319575 319576 319577 319578 319579 319580 319581 [...] (149708 flits)
Class 0:
Remaining flits: 234180 234181 234182 234183 234184 234185 234186 234187 234188 234189 [...] (104583 flits)
Measured flits: 319572 319573 319574 319575 319576 319577 319578 319579 319580 319581 [...] (103830 flits)
Class 0:
Remaining flits: 258660 258661 258662 258663 258664 258665 258666 258667 258668 258669 [...] (59585 flits)
Measured flits: 319572 319573 319574 319575 319576 319577 319578 319579 319580 319581 [...] (59464 flits)
Class 0:
Remaining flits: 320724 320725 320726 320727 320728 320729 320730 320731 320732 320733 [...] (21959 flits)
Measured flits: 320724 320725 320726 320727 320728 320729 320730 320731 320732 320733 [...] (21959 flits)
Class 0:
Remaining flits: 404010 404011 404012 404013 404014 404015 404016 404017 404018 404019 [...] (2157 flits)
Measured flits: 404010 404011 404012 404013 404014 404015 404016 404017 404018 404019 [...] (2157 flits)
Time taken is 13830 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4812.61 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10450 (1 samples)
Network latency average = 4453.38 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9681 (1 samples)
Flit latency average = 3834.77 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9664 (1 samples)
Fragmentation average = 60.8383 (1 samples)
	minimum = 0 (1 samples)
	maximum = 659 (1 samples)
Injected packet rate average = 0.0291354 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.052 (1 samples)
Accepted packet rate average = 0.0157118 (1 samples)
	minimum = 0.0103333 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.524274 (1 samples)
	minimum = 0.09 (1 samples)
	maximum = 0.940333 (1 samples)
Accepted flit rate average = 0.281693 (1 samples)
	minimum = 0.195333 (1 samples)
	maximum = 0.393667 (1 samples)
Injected packet size average = 17.9944 (1 samples)
Accepted packet size average = 17.9287 (1 samples)
Hops average = 5.1566 (1 samples)
Total run time 12.9309
