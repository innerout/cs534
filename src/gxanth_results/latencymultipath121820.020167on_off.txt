BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 245.703
	minimum = 22
	maximum = 931
Network latency average = 153.158
	minimum = 22
	maximum = 675
Slowest packet = 3
Flit latency average = 127.399
	minimum = 5
	maximum = 658
Slowest flit = 18053
Fragmentation average = 16.4817
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0158021
	minimum = 0 (at node 10)
	maximum = 0.037 (at node 126)
Accepted packet rate average = 0.0122396
	minimum = 0.004 (at node 174)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.281453
	minimum = 0 (at node 10)
	maximum = 0.666 (at node 126)
Accepted flit rate average= 0.224797
	minimum = 0.072 (at node 174)
	maximum = 0.377 (at node 132)
Injected packet length average = 17.8111
Accepted packet length average = 18.3664
Total in-flight flits = 12297 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 414.275
	minimum = 22
	maximum = 1841
Network latency average = 212.467
	minimum = 22
	maximum = 1070
Slowest packet = 3
Flit latency average = 183.476
	minimum = 5
	maximum = 1053
Slowest flit = 33371
Fragmentation average = 18.5562
	minimum = 0
	maximum = 114
Injected packet rate average = 0.0150234
	minimum = 0.0005 (at node 30)
	maximum = 0.0305 (at node 126)
Accepted packet rate average = 0.0129453
	minimum = 0.007 (at node 153)
	maximum = 0.0185 (at node 14)
Injected flit rate average = 0.268797
	minimum = 0.009 (at node 30)
	maximum = 0.549 (at node 126)
Accepted flit rate average= 0.23562
	minimum = 0.1315 (at node 153)
	maximum = 0.34 (at node 22)
Injected packet length average = 17.8918
Accepted packet length average = 18.2012
Total in-flight flits = 14552 (0 measured)
latency change    = 0.406907
throughput change = 0.0459338
Class 0:
Packet latency average = 856.709
	minimum = 35
	maximum = 2526
Network latency average = 302.056
	minimum = 22
	maximum = 1516
Slowest packet = 5233
Flit latency average = 270.64
	minimum = 5
	maximum = 1488
Slowest flit = 71298
Fragmentation average = 21.7289
	minimum = 0
	maximum = 116
Injected packet rate average = 0.0141875
	minimum = 0 (at node 25)
	maximum = 0.029 (at node 117)
Accepted packet rate average = 0.0138698
	minimum = 0.003 (at node 184)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.255505
	minimum = 0 (at node 25)
	maximum = 0.515 (at node 117)
Accepted flit rate average= 0.248792
	minimum = 0.054 (at node 184)
	maximum = 0.471 (at node 34)
Injected packet length average = 18.0092
Accepted packet length average = 17.9377
Total in-flight flits = 16392 (0 measured)
latency change    = 0.516435
throughput change = 0.0529434
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1126.84
	minimum = 22
	maximum = 3297
Network latency average = 253.866
	minimum = 22
	maximum = 966
Slowest packet = 8658
Flit latency average = 289.863
	minimum = 5
	maximum = 1766
Slowest flit = 106434
Fragmentation average = 18.958
	minimum = 0
	maximum = 94
Injected packet rate average = 0.0136146
	minimum = 0 (at node 57)
	maximum = 0.027 (at node 131)
Accepted packet rate average = 0.0135833
	minimum = 0.005 (at node 48)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.244615
	minimum = 0 (at node 57)
	maximum = 0.486 (at node 131)
Accepted flit rate average= 0.244479
	minimum = 0.09 (at node 48)
	maximum = 0.443 (at node 16)
Injected packet length average = 17.9671
Accepted packet length average = 17.9985
Total in-flight flits = 16324 (16047 measured)
latency change    = 0.239726
throughput change = 0.0176395
Class 0:
Packet latency average = 1315.69
	minimum = 22
	maximum = 4113
Network latency average = 302.197
	minimum = 22
	maximum = 1277
Slowest packet = 8658
Flit latency average = 297.554
	minimum = 5
	maximum = 1883
Slowest flit = 112947
Fragmentation average = 20.3184
	minimum = 0
	maximum = 122
Injected packet rate average = 0.0137917
	minimum = 0.0035 (at node 128)
	maximum = 0.024 (at node 37)
Accepted packet rate average = 0.013625
	minimum = 0.0065 (at node 4)
	maximum = 0.0205 (at node 129)
Injected flit rate average = 0.248154
	minimum = 0.063 (at node 143)
	maximum = 0.432 (at node 37)
Accepted flit rate average= 0.24582
	minimum = 0.1205 (at node 4)
	maximum = 0.3775 (at node 129)
Injected packet length average = 17.993
Accepted packet length average = 18.0419
Total in-flight flits = 17217 (17217 measured)
latency change    = 0.143532
throughput change = 0.0054558
Class 0:
Packet latency average = 1492.65
	minimum = 22
	maximum = 4986
Network latency average = 320.327
	minimum = 22
	maximum = 1461
Slowest packet = 8658
Flit latency average = 303.037
	minimum = 5
	maximum = 1883
Slowest flit = 112947
Fragmentation average = 21.4122
	minimum = 0
	maximum = 122
Injected packet rate average = 0.013901
	minimum = 0.00266667 (at node 143)
	maximum = 0.023 (at node 37)
Accepted packet rate average = 0.0137483
	minimum = 0.00866667 (at node 36)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.250002
	minimum = 0.0426667 (at node 143)
	maximum = 0.412333 (at node 37)
Accepted flit rate average= 0.247925
	minimum = 0.156 (at node 36)
	maximum = 0.377667 (at node 128)
Injected packet length average = 17.9844
Accepted packet length average = 18.0332
Total in-flight flits = 17407 (17407 measured)
latency change    = 0.118559
throughput change = 0.0084906
Class 0:
Packet latency average = 1654.32
	minimum = 22
	maximum = 5353
Network latency average = 329.32
	minimum = 22
	maximum = 1461
Slowest packet = 8658
Flit latency average = 306.156
	minimum = 5
	maximum = 1883
Slowest flit = 112947
Fragmentation average = 21.9705
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0139232
	minimum = 0.003 (at node 143)
	maximum = 0.0205 (at node 37)
Accepted packet rate average = 0.0138398
	minimum = 0.0095 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.250358
	minimum = 0.054 (at node 143)
	maximum = 0.369 (at node 37)
Accepted flit rate average= 0.249491
	minimum = 0.171 (at node 36)
	maximum = 0.35625 (at node 128)
Injected packet length average = 17.9814
Accepted packet length average = 18.027
Total in-flight flits = 17113 (17113 measured)
latency change    = 0.0977259
throughput change = 0.00627493
Class 0:
Packet latency average = 1803.99
	minimum = 22
	maximum = 6248
Network latency average = 330.53
	minimum = 22
	maximum = 1461
Slowest packet = 8658
Flit latency average = 305.018
	minimum = 5
	maximum = 1883
Slowest flit = 112947
Fragmentation average = 21.6663
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0139385
	minimum = 0.0052 (at node 143)
	maximum = 0.021 (at node 178)
Accepted packet rate average = 0.013849
	minimum = 0.0102 (at node 162)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.250765
	minimum = 0.0922 (at node 143)
	maximum = 0.376 (at node 178)
Accepted flit rate average= 0.249425
	minimum = 0.1836 (at node 162)
	maximum = 0.3392 (at node 128)
Injected packet length average = 17.9907
Accepted packet length average = 18.0104
Total in-flight flits = 17658 (17658 measured)
latency change    = 0.0829648
throughput change = 0.000264149
Class 0:
Packet latency average = 1953.15
	minimum = 22
	maximum = 6622
Network latency average = 334.446
	minimum = 22
	maximum = 1944
Slowest packet = 8658
Flit latency average = 307.325
	minimum = 5
	maximum = 1927
Slowest flit = 346625
Fragmentation average = 21.7823
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0139019
	minimum = 0.0065 (at node 143)
	maximum = 0.0203333 (at node 178)
Accepted packet rate average = 0.0138359
	minimum = 0.0108333 (at node 42)
	maximum = 0.0188333 (at node 128)
Injected flit rate average = 0.250141
	minimum = 0.117 (at node 143)
	maximum = 0.363667 (at node 178)
Accepted flit rate average= 0.249245
	minimum = 0.1965 (at node 80)
	maximum = 0.3365 (at node 128)
Injected packet length average = 17.9933
Accepted packet length average = 18.0143
Total in-flight flits = 17550 (17550 measured)
latency change    = 0.0763701
throughput change = 0.000723017
Class 0:
Packet latency average = 2103.41
	minimum = 22
	maximum = 7207
Network latency average = 337.6
	minimum = 22
	maximum = 2597
Slowest packet = 8658
Flit latency average = 309.01
	minimum = 5
	maximum = 2580
Slowest flit = 347322
Fragmentation average = 21.8671
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0138981
	minimum = 0.00757143 (at node 128)
	maximum = 0.0197143 (at node 36)
Accepted packet rate average = 0.0138378
	minimum = 0.0108571 (at node 79)
	maximum = 0.0182857 (at node 128)
Injected flit rate average = 0.250071
	minimum = 0.138 (at node 128)
	maximum = 0.354857 (at node 36)
Accepted flit rate average= 0.249227
	minimum = 0.195429 (at node 79)
	maximum = 0.327 (at node 128)
Injected packet length average = 17.9932
Accepted packet length average = 18.0106
Total in-flight flits = 17671 (17671 measured)
latency change    = 0.0714323
throughput change = 7.16501e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 470718 470719 470720 470721 470722 470723 470724 470725 470726 470727 [...] (18023 flits)
Measured flits: 470718 470719 470720 470721 470722 470723 470724 470725 470726 470727 [...] (16609 flits)
Class 0:
Remaining flits: 479322 479323 479324 479325 479326 479327 479328 479329 479330 479331 [...] (18467 flits)
Measured flits: 479322 479323 479324 479325 479326 479327 479328 479329 479330 479331 [...] (15327 flits)
Class 0:
Remaining flits: 564953 564954 564955 564956 564957 564958 564959 564960 564961 564962 [...] (18161 flits)
Measured flits: 564953 564954 564955 564956 564957 564958 564959 564960 564961 564962 [...] (12325 flits)
Class 0:
Remaining flits: 618966 618967 618968 618969 618970 618971 618972 618973 618974 618975 [...] (18201 flits)
Measured flits: 618966 618967 618968 618969 618970 618971 618972 618973 618974 618975 [...] (9783 flits)
Class 0:
Remaining flits: 646271 669402 669403 669404 669405 669406 669407 669408 669409 669410 [...] (18097 flits)
Measured flits: 646271 669402 669403 669404 669405 669406 669407 669408 669409 669410 [...] (7869 flits)
Class 0:
Remaining flits: 692568 692569 692570 692571 692572 692573 692574 692575 692576 692577 [...] (18244 flits)
Measured flits: 710712 710713 710714 710715 710716 710717 710718 710719 710720 710721 [...] (5580 flits)
Class 0:
Remaining flits: 749268 749269 749270 749271 749272 749273 749274 749275 749276 749277 [...] (18489 flits)
Measured flits: 756324 756325 756326 756327 756328 756329 756330 756331 756332 756333 [...] (4336 flits)
Class 0:
Remaining flits: 809532 809533 809534 809535 809536 809537 809538 809539 809540 809541 [...] (17893 flits)
Measured flits: 826794 826795 826796 826797 826798 826799 826800 826801 826802 826803 [...] (2660 flits)
Class 0:
Remaining flits: 858978 858979 858980 858981 858982 858983 858984 858985 858986 858987 [...] (18647 flits)
Measured flits: 880218 880219 880220 880221 880222 880223 880224 880225 880226 880227 [...] (1907 flits)
Class 0:
Remaining flits: 911412 911413 911414 911415 911416 911417 911418 911419 911420 911421 [...] (18481 flits)
Measured flits: 933683 933684 933685 933686 933687 933688 933689 933690 933691 933692 [...] (1534 flits)
Class 0:
Remaining flits: 950688 950689 950690 950691 950692 950693 950694 950695 950696 950697 [...] (19006 flits)
Measured flits: 966276 966277 966278 966279 966280 966281 966282 966283 966284 966285 [...] (1158 flits)
Class 0:
Remaining flits: 1013972 1013973 1013974 1013975 1019039 1019040 1019041 1019042 1019043 1019044 [...] (18106 flits)
Measured flits: 1022382 1022383 1022384 1022385 1022386 1022387 1022388 1022389 1022390 1022391 [...] (708 flits)
Class 0:
Remaining flits: 1039320 1039321 1039322 1039323 1039324 1039325 1039326 1039327 1039328 1039329 [...] (18100 flits)
Measured flits: 1078128 1078129 1078130 1078131 1078132 1078133 1078134 1078135 1078136 1078137 [...] (234 flits)
Class 0:
Remaining flits: 1086165 1086166 1086167 1086168 1086169 1086170 1086171 1086172 1086173 1089738 [...] (17838 flits)
Measured flits: 1139760 1139761 1139762 1139763 1139764 1139765 1139766 1139767 1139768 1139769 [...] (133 flits)
Class 0:
Remaining flits: 1131714 1131715 1131716 1131717 1131718 1131719 1131720 1131721 1131722 1131723 [...] (18277 flits)
Measured flits: 1198260 1198261 1198262 1198263 1198264 1198265 1198266 1198267 1198268 1198269 [...] (162 flits)
Class 0:
Remaining flits: 1199251 1199252 1199253 1199254 1199255 1199256 1199257 1199258 1199259 1199260 [...] (18649 flits)
Measured flits: 1227816 1227817 1227818 1227819 1227820 1227821 1227822 1227823 1227824 1227825 [...] (171 flits)
Class 0:
Remaining flits: 1229346 1229347 1229348 1229349 1229350 1229351 1229352 1229353 1229354 1229355 [...] (18339 flits)
Measured flits: 1289934 1289935 1289936 1289937 1289938 1289939 1289940 1289941 1289942 1289943 [...] (36 flits)
Draining remaining packets ...
Time taken is 27823 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3629.75 (1 samples)
	minimum = 22 (1 samples)
	maximum = 17505 (1 samples)
Network latency average = 349.943 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2644 (1 samples)
Flit latency average = 322.805 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2600 (1 samples)
Fragmentation average = 22.2932 (1 samples)
	minimum = 0 (1 samples)
	maximum = 138 (1 samples)
Injected packet rate average = 0.0138981 (1 samples)
	minimum = 0.00757143 (1 samples)
	maximum = 0.0197143 (1 samples)
Accepted packet rate average = 0.0138378 (1 samples)
	minimum = 0.0108571 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.250071 (1 samples)
	minimum = 0.138 (1 samples)
	maximum = 0.354857 (1 samples)
Accepted flit rate average = 0.249227 (1 samples)
	minimum = 0.195429 (1 samples)
	maximum = 0.327 (1 samples)
Injected packet size average = 17.9932 (1 samples)
Accepted packet size average = 18.0106 (1 samples)
Hops average = 5.06146 (1 samples)
Total run time 25.633
