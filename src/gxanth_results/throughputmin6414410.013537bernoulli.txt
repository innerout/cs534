BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 188.876
	minimum = 23
	maximum = 931
Network latency average = 186.608
	minimum = 23
	maximum = 909
Slowest packet = 191
Flit latency average = 117.504
	minimum = 6
	maximum = 892
Slowest flit = 3455
Fragmentation average = 115.813
	minimum = 0
	maximum = 754
Injected packet rate average = 0.0136094
	minimum = 0.006 (at node 87)
	maximum = 0.022 (at node 123)
Accepted packet rate average = 0.00910417
	minimum = 0.002 (at node 41)
	maximum = 0.016 (at node 91)
Injected flit rate average = 0.242297
	minimum = 0.1 (at node 87)
	maximum = 0.396 (at node 123)
Accepted flit rate average= 0.182781
	minimum = 0.059 (at node 174)
	maximum = 0.326 (at node 132)
Injected packet length average = 17.8037
Accepted packet length average = 20.0767
Total in-flight flits = 11940 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 275.32
	minimum = 23
	maximum = 1758
Network latency average = 272.844
	minimum = 23
	maximum = 1758
Slowest packet = 582
Flit latency average = 187.02
	minimum = 6
	maximum = 1741
Slowest flit = 10493
Fragmentation average = 155.819
	minimum = 0
	maximum = 1310
Injected packet rate average = 0.0133906
	minimum = 0.0075 (at node 161)
	maximum = 0.019 (at node 77)
Accepted packet rate average = 0.0100573
	minimum = 0.0055 (at node 116)
	maximum = 0.0165 (at node 166)
Injected flit rate average = 0.239911
	minimum = 0.135 (at node 161)
	maximum = 0.342 (at node 77)
Accepted flit rate average= 0.19401
	minimum = 0.1065 (at node 116)
	maximum = 0.3095 (at node 166)
Injected packet length average = 17.9164
Accepted packet length average = 19.2905
Total in-flight flits = 18056 (0 measured)
latency change    = 0.313976
throughput change = 0.0578792
Class 0:
Packet latency average = 442.022
	minimum = 26
	maximum = 2536
Network latency average = 439.218
	minimum = 23
	maximum = 2536
Slowest packet = 1030
Flit latency average = 329.714
	minimum = 6
	maximum = 2519
Slowest flit = 18557
Fragmentation average = 218.279
	minimum = 0
	maximum = 2307
Injected packet rate average = 0.0134844
	minimum = 0.004 (at node 136)
	maximum = 0.025 (at node 191)
Accepted packet rate average = 0.0116667
	minimum = 0.002 (at node 184)
	maximum = 0.019 (at node 56)
Injected flit rate average = 0.242443
	minimum = 0.064 (at node 136)
	maximum = 0.457 (at node 191)
Accepted flit rate average= 0.214328
	minimum = 0.043 (at node 184)
	maximum = 0.335 (at node 159)
Injected packet length average = 17.9795
Accepted packet length average = 18.371
Total in-flight flits = 23507 (0 measured)
latency change    = 0.377135
throughput change = 0.0947972
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 294.391
	minimum = 27
	maximum = 965
Network latency average = 291.651
	minimum = 27
	maximum = 965
Slowest packet = 7756
Flit latency average = 402.613
	minimum = 6
	maximum = 3299
Slowest flit = 22391
Fragmentation average = 163.571
	minimum = 0
	maximum = 836
Injected packet rate average = 0.0140469
	minimum = 0.004 (at node 150)
	maximum = 0.025 (at node 84)
Accepted packet rate average = 0.0121667
	minimum = 0.005 (at node 1)
	maximum = 0.022 (at node 120)
Injected flit rate average = 0.252427
	minimum = 0.072 (at node 150)
	maximum = 0.45 (at node 84)
Accepted flit rate average= 0.22162
	minimum = 0.083 (at node 17)
	maximum = 0.377 (at node 129)
Injected packet length average = 17.9703
Accepted packet length average = 18.2153
Total in-flight flits = 29502 (20904 measured)
latency change    = 0.501481
throughput change = 0.0329017
Class 0:
Packet latency average = 382.771
	minimum = 23
	maximum = 1966
Network latency average = 380.111
	minimum = 23
	maximum = 1953
Slowest packet = 7751
Flit latency average = 455.419
	minimum = 6
	maximum = 4290
Slowest flit = 21946
Fragmentation average = 185.426
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0136901
	minimum = 0.0085 (at node 10)
	maximum = 0.022 (at node 126)
Accepted packet rate average = 0.0122969
	minimum = 0.0055 (at node 4)
	maximum = 0.0195 (at node 123)
Injected flit rate average = 0.246563
	minimum = 0.153 (at node 10)
	maximum = 0.392 (at node 126)
Accepted flit rate average= 0.223065
	minimum = 0.111 (at node 4)
	maximum = 0.346 (at node 129)
Injected packet length average = 18.0103
Accepted packet length average = 18.14
Total in-flight flits = 32476 (27616 measured)
latency change    = 0.230897
throughput change = 0.00647933
Class 0:
Packet latency average = 458.443
	minimum = 23
	maximum = 2756
Network latency average = 455.864
	minimum = 23
	maximum = 2756
Slowest packet = 7794
Flit latency average = 489.982
	minimum = 6
	maximum = 5695
Slowest flit = 11286
Fragmentation average = 204.613
	minimum = 0
	maximum = 2217
Injected packet rate average = 0.013658
	minimum = 0.00866667 (at node 18)
	maximum = 0.0196667 (at node 116)
Accepted packet rate average = 0.0122934
	minimum = 0.008 (at node 17)
	maximum = 0.0173333 (at node 123)
Injected flit rate average = 0.245995
	minimum = 0.156 (at node 18)
	maximum = 0.354 (at node 116)
Accepted flit rate average= 0.222833
	minimum = 0.145333 (at node 17)
	maximum = 0.301667 (at node 19)
Injected packet length average = 18.0111
Accepted packet length average = 18.1263
Total in-flight flits = 36761 (33473 measured)
latency change    = 0.165062
throughput change = 0.00104011
Draining remaining packets ...
Class 0:
Remaining flits: 32020 32021 36299 36300 36301 36302 36303 36304 36305 41403 [...] (4645 flits)
Measured flits: 139410 139411 139412 139413 139414 139415 139416 139417 139418 139419 [...] (3972 flits)
Time taken is 7510 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 726.214 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4277 (1 samples)
Network latency average = 723.589 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4277 (1 samples)
Flit latency average = 757.889 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6446 (1 samples)
Fragmentation average = 216.703 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2759 (1 samples)
Injected packet rate average = 0.013658 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0196667 (1 samples)
Accepted packet rate average = 0.0122934 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.245995 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 0.354 (1 samples)
Accepted flit rate average = 0.222833 (1 samples)
	minimum = 0.145333 (1 samples)
	maximum = 0.301667 (1 samples)
Injected packet size average = 18.0111 (1 samples)
Accepted packet size average = 18.1263 (1 samples)
Hops average = 5.05707 (1 samples)
Total run time 7.42469
