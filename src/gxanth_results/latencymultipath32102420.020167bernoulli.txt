BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 198.71
	minimum = 22
	maximum = 610
Network latency average = 194.087
	minimum = 22
	maximum = 590
Slowest packet = 1533
Flit latency average = 163.848
	minimum = 5
	maximum = 573
Slowest flit = 27610
Fragmentation average = 33.8352
	minimum = 0
	maximum = 239
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.013401
	minimum = 0.005 (at node 115)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.248203
	minimum = 0.09 (at node 115)
	maximum = 0.437 (at node 44)
Injected packet length average = 17.8234
Accepted packet length average = 18.5212
Total in-flight flits = 21951 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 334.701
	minimum = 22
	maximum = 1054
Network latency average = 329.834
	minimum = 22
	maximum = 1042
Slowest packet = 3685
Flit latency average = 298.771
	minimum = 5
	maximum = 1097
Slowest flit = 61648
Fragmentation average = 35.6198
	minimum = 0
	maximum = 280
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0141432
	minimum = 0.0075 (at node 153)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.258112
	minimum = 0.138 (at node 153)
	maximum = 0.378 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 18.2499
Total in-flight flits = 39089 (0 measured)
latency change    = 0.406304
throughput change = 0.0383897
Class 0:
Packet latency average = 700.002
	minimum = 22
	maximum = 1522
Network latency average = 695.33
	minimum = 22
	maximum = 1522
Slowest packet = 4974
Flit latency average = 665.078
	minimum = 5
	maximum = 1505
Slowest flit = 89549
Fragmentation average = 36.8482
	minimum = 0
	maximum = 431
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0150938
	minimum = 0.006 (at node 126)
	maximum = 0.027 (at node 16)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.272323
	minimum = 0.108 (at node 126)
	maximum = 0.501 (at node 16)
Injected packet length average = 17.9811
Accepted packet length average = 18.0421
Total in-flight flits = 56481 (0 measured)
latency change    = 0.521858
throughput change = 0.0521841
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 187.517
	minimum = 22
	maximum = 1001
Network latency average = 182.42
	minimum = 22
	maximum = 982
Slowest packet = 11551
Flit latency average = 887.318
	minimum = 5
	maximum = 1700
Slowest flit = 158526
Fragmentation average = 10.4656
	minimum = 0
	maximum = 98
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0153333
	minimum = 0.007 (at node 48)
	maximum = 0.026 (at node 123)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.275885
	minimum = 0.126 (at node 48)
	maximum = 0.463 (at node 129)
Injected packet length average = 17.9618
Accepted packet length average = 17.9925
Total in-flight flits = 75133 (64232 measured)
latency change    = 2.73302
throughput change = 0.012913
Class 0:
Packet latency average = 958.675
	minimum = 22
	maximum = 1929
Network latency average = 953.794
	minimum = 22
	maximum = 1913
Slowest packet = 11595
Flit latency average = 1007.63
	minimum = 5
	maximum = 1930
Slowest flit = 204407
Fragmentation average = 32.3408
	minimum = 0
	maximum = 232
Injected packet rate average = 0.0202708
	minimum = 0.0125 (at node 110)
	maximum = 0.028 (at node 38)
Accepted packet rate average = 0.0153229
	minimum = 0.009 (at node 113)
	maximum = 0.0235 (at node 66)
Injected flit rate average = 0.36507
	minimum = 0.225 (at node 169)
	maximum = 0.504 (at node 91)
Accepted flit rate average= 0.275969
	minimum = 0.1685 (at node 113)
	maximum = 0.409 (at node 66)
Injected packet length average = 18.0096
Accepted packet length average = 18.0102
Total in-flight flits = 90621 (90621 measured)
latency change    = 0.8044
throughput change = 0.000301967
Class 0:
Packet latency average = 1193.7
	minimum = 22
	maximum = 2309
Network latency average = 1188.66
	minimum = 22
	maximum = 2309
Slowest packet = 14056
Flit latency average = 1129.62
	minimum = 5
	maximum = 2292
Slowest flit = 253025
Fragmentation average = 35.1842
	minimum = 0
	maximum = 250
Injected packet rate average = 0.0203299
	minimum = 0.0136667 (at node 147)
	maximum = 0.026 (at node 115)
Accepted packet rate average = 0.0153281
	minimum = 0.01 (at node 135)
	maximum = 0.021 (at node 118)
Injected flit rate average = 0.366033
	minimum = 0.246 (at node 147)
	maximum = 0.468333 (at node 115)
Accepted flit rate average= 0.276113
	minimum = 0.178 (at node 135)
	maximum = 0.378 (at node 118)
Injected packet length average = 18.0047
Accepted packet length average = 18.0135
Total in-flight flits = 108220 (108220 measured)
latency change    = 0.196891
throughput change = 0.000521878
Class 0:
Packet latency average = 1347.32
	minimum = 22
	maximum = 2697
Network latency average = 1342.35
	minimum = 22
	maximum = 2694
Slowest packet = 15118
Flit latency average = 1247.99
	minimum = 5
	maximum = 2677
Slowest flit = 272141
Fragmentation average = 35.9729
	minimum = 0
	maximum = 310
Injected packet rate average = 0.0203372
	minimum = 0.01475 (at node 5)
	maximum = 0.026 (at node 156)
Accepted packet rate average = 0.0153034
	minimum = 0.011 (at node 135)
	maximum = 0.02025 (at node 128)
Injected flit rate average = 0.366148
	minimum = 0.2655 (at node 5)
	maximum = 0.468 (at node 156)
Accepted flit rate average= 0.275546
	minimum = 0.1965 (at node 135)
	maximum = 0.36025 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 18.0055
Total in-flight flits = 126004 (126004 measured)
latency change    = 0.114013
throughput change = 0.00205873
Class 0:
Packet latency average = 1480.53
	minimum = 22
	maximum = 3079
Network latency average = 1475.6
	minimum = 22
	maximum = 3068
Slowest packet = 19087
Flit latency average = 1365.01
	minimum = 5
	maximum = 3051
Slowest flit = 343583
Fragmentation average = 37.2024
	minimum = 0
	maximum = 310
Injected packet rate average = 0.0203438
	minimum = 0.0154 (at node 139)
	maximum = 0.0248 (at node 113)
Accepted packet rate average = 0.0153469
	minimum = 0.012 (at node 42)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.366196
	minimum = 0.2772 (at node 139)
	maximum = 0.4464 (at node 113)
Accepted flit rate average= 0.27611
	minimum = 0.2116 (at node 42)
	maximum = 0.3546 (at node 128)
Injected packet length average = 18.0004
Accepted packet length average = 17.9913
Total in-flight flits = 142955 (142955 measured)
latency change    = 0.0899777
throughput change = 0.00204572
Class 0:
Packet latency average = 1610.54
	minimum = 22
	maximum = 3366
Network latency average = 1605.59
	minimum = 22
	maximum = 3347
Slowest packet = 21300
Flit latency average = 1484.85
	minimum = 5
	maximum = 3337
Slowest flit = 393187
Fragmentation average = 37.1424
	minimum = 0
	maximum = 310
Injected packet rate average = 0.0203151
	minimum = 0.016 (at node 155)
	maximum = 0.0245 (at node 96)
Accepted packet rate average = 0.0153194
	minimum = 0.0123333 (at node 42)
	maximum = 0.0191667 (at node 128)
Injected flit rate average = 0.365752
	minimum = 0.289333 (at node 155)
	maximum = 0.441 (at node 96)
Accepted flit rate average= 0.275749
	minimum = 0.220833 (at node 42)
	maximum = 0.342167 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 17.9999
Total in-flight flits = 160072 (160072 measured)
latency change    = 0.0807234
throughput change = 0.00131019
Class 0:
Packet latency average = 1737.69
	minimum = 22
	maximum = 3755
Network latency average = 1732.71
	minimum = 22
	maximum = 3755
Slowest packet = 23579
Flit latency average = 1605.18
	minimum = 5
	maximum = 3741
Slowest flit = 435108
Fragmentation average = 37.3454
	minimum = 0
	maximum = 310
Injected packet rate average = 0.0202984
	minimum = 0.0158571 (at node 155)
	maximum = 0.0248571 (at node 165)
Accepted packet rate average = 0.0153266
	minimum = 0.012 (at node 42)
	maximum = 0.0194286 (at node 70)
Injected flit rate average = 0.365336
	minimum = 0.287286 (at node 155)
	maximum = 0.445429 (at node 165)
Accepted flit rate average= 0.275936
	minimum = 0.212857 (at node 42)
	maximum = 0.350286 (at node 70)
Injected packet length average = 17.9983
Accepted packet length average = 18.0037
Total in-flight flits = 176681 (176681 measured)
latency change    = 0.0731741
throughput change = 0.000677258
Draining all recorded packets ...
Class 0:
Remaining flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (193605 flits)
Measured flits: 477810 477811 477812 477813 477814 477815 477816 477817 477818 477819 [...] (129917 flits)
Class 0:
Remaining flits: 521910 521911 521912 521913 521914 521915 521916 521917 521918 521919 [...] (208427 flits)
Measured flits: 521910 521911 521912 521913 521914 521915 521916 521917 521918 521919 [...] (82741 flits)
Class 0:
Remaining flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (224945 flits)
Measured flits: 583524 583525 583526 583527 583528 583529 583530 583531 583532 583533 [...] (35966 flits)
Class 0:
Remaining flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (241546 flits)
Measured flits: 640926 640927 640928 640929 640930 640931 640932 640933 640934 640935 [...] (4758 flits)
Class 0:
Remaining flits: 679536 679537 679538 679539 679540 679541 679542 679543 679544 679545 [...] (257339 flits)
Measured flits: 679536 679537 679538 679539 679540 679541 679542 679543 679544 679545 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 731726 731727 731728 731729 731730 731731 731732 731733 731734 731735 [...] (213621 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 780588 780589 780590 780591 780592 780593 780594 780595 780596 780597 [...] (166217 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 825534 825535 825536 825537 825538 825539 825540 825541 825542 825543 [...] (118555 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 893484 893485 893486 893487 893488 893489 893490 893491 893492 893493 [...] (71046 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944123 944124 944125 944126 944127 944128 944129 944130 944131 944132 [...] (25904 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 983626 983627 994140 994141 994142 994143 994144 994145 994146 994147 [...] (2634 flits)
Measured flits: (0 flits)
Time taken is 22051 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2315.93 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5308 (1 samples)
Network latency average = 2310.95 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5308 (1 samples)
Flit latency average = 2975.11 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7215 (1 samples)
Fragmentation average = 39.4258 (1 samples)
	minimum = 0 (1 samples)
	maximum = 357 (1 samples)
Injected packet rate average = 0.0202984 (1 samples)
	minimum = 0.0158571 (1 samples)
	maximum = 0.0248571 (1 samples)
Accepted packet rate average = 0.0153266 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0194286 (1 samples)
Injected flit rate average = 0.365336 (1 samples)
	minimum = 0.287286 (1 samples)
	maximum = 0.445429 (1 samples)
Accepted flit rate average = 0.275936 (1 samples)
	minimum = 0.212857 (1 samples)
	maximum = 0.350286 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 18.0037 (1 samples)
Hops average = 5.07713 (1 samples)
Total run time 17.2869
