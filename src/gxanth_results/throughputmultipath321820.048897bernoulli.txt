BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.048897
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 356.903
	minimum = 24
	maximum = 875
Network latency average = 326.584
	minimum = 22
	maximum = 867
Slowest packet = 1010
Flit latency average = 282.586
	minimum = 5
	maximum = 874
Slowest flit = 15471
Fragmentation average = 83.9846
	minimum = 0
	maximum = 360
Injected packet rate average = 0.0293021
	minimum = 0.014 (at node 8)
	maximum = 0.041 (at node 137)
Accepted packet rate average = 0.0142083
	minimum = 0.005 (at node 135)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.5215
	minimum = 0.252 (at node 8)
	maximum = 0.732 (at node 137)
Accepted flit rate average= 0.269875
	minimum = 0.093 (at node 174)
	maximum = 0.416 (at node 44)
Injected packet length average = 17.7974
Accepted packet length average = 18.9941
Total in-flight flits = 50208 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 703.352
	minimum = 24
	maximum = 1824
Network latency average = 583.113
	minimum = 22
	maximum = 1719
Slowest packet = 1663
Flit latency average = 529.756
	minimum = 5
	maximum = 1780
Slowest flit = 27180
Fragmentation average = 82.6133
	minimum = 0
	maximum = 363
Injected packet rate average = 0.0221042
	minimum = 0.009 (at node 8)
	maximum = 0.0305 (at node 134)
Accepted packet rate average = 0.0145
	minimum = 0.0075 (at node 116)
	maximum = 0.021 (at node 22)
Injected flit rate average = 0.394682
	minimum = 0.162 (at node 8)
	maximum = 0.549 (at node 134)
Accepted flit rate average= 0.266951
	minimum = 0.135 (at node 116)
	maximum = 0.3825 (at node 22)
Injected packet length average = 17.8556
Accepted packet length average = 18.4104
Total in-flight flits = 51067 (0 measured)
latency change    = 0.492568
throughput change = 0.0109551
Class 0:
Packet latency average = 1728.61
	minimum = 883
	maximum = 2748
Network latency average = 996.913
	minimum = 22
	maximum = 2673
Slowest packet = 2493
Flit latency average = 931.298
	minimum = 5
	maximum = 2656
Slowest flit = 44891
Fragmentation average = 71.0042
	minimum = 0
	maximum = 344
Injected packet rate average = 0.015349
	minimum = 0.003 (at node 174)
	maximum = 0.028 (at node 58)
Accepted packet rate average = 0.0148854
	minimum = 0.006 (at node 163)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.276979
	minimum = 0.053 (at node 174)
	maximum = 0.505 (at node 58)
Accepted flit rate average= 0.268521
	minimum = 0.111 (at node 163)
	maximum = 0.48 (at node 16)
Injected packet length average = 18.0455
Accepted packet length average = 18.0392
Total in-flight flits = 52773 (0 measured)
latency change    = 0.593112
throughput change = 0.00584801
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2408.61
	minimum = 1342
	maximum = 3221
Network latency average = 358.911
	minimum = 22
	maximum = 978
Slowest packet = 11540
Flit latency average = 961.654
	minimum = 5
	maximum = 3371
Slowest flit = 78009
Fragmentation average = 34.5467
	minimum = 0
	maximum = 202
Injected packet rate average = 0.0148438
	minimum = 0.006 (at node 171)
	maximum = 0.032 (at node 174)
Accepted packet rate average = 0.0149688
	minimum = 0.006 (at node 139)
	maximum = 0.025 (at node 0)
Injected flit rate average = 0.266458
	minimum = 0.102 (at node 172)
	maximum = 0.573 (at node 174)
Accepted flit rate average= 0.268729
	minimum = 0.126 (at node 71)
	maximum = 0.432 (at node 34)
Injected packet length average = 17.9509
Accepted packet length average = 17.9527
Total in-flight flits = 52297 (41165 measured)
latency change    = 0.282319
throughput change = 0.000775254
Class 0:
Packet latency average = 2988.11
	minimum = 1342
	maximum = 4146
Network latency average = 768.712
	minimum = 22
	maximum = 1872
Slowest packet = 11540
Flit latency average = 970.263
	minimum = 5
	maximum = 3510
Slowest flit = 106036
Fragmentation average = 60.0553
	minimum = 0
	maximum = 293
Injected packet rate average = 0.0147734
	minimum = 0.007 (at node 9)
	maximum = 0.0265 (at node 174)
Accepted packet rate average = 0.0148125
	minimum = 0.0085 (at node 122)
	maximum = 0.0235 (at node 0)
Injected flit rate average = 0.265977
	minimum = 0.129 (at node 9)
	maximum = 0.4725 (at node 174)
Accepted flit rate average= 0.266513
	minimum = 0.1575 (at node 122)
	maximum = 0.4155 (at node 0)
Injected packet length average = 18.0037
Accepted packet length average = 17.9924
Total in-flight flits = 52456 (51987 measured)
latency change    = 0.193935
throughput change = 0.00831534
Class 0:
Packet latency average = 3414.57
	minimum = 1342
	maximum = 5079
Network latency average = 902.652
	minimum = 22
	maximum = 2901
Slowest packet = 11540
Flit latency average = 971.954
	minimum = 5
	maximum = 3654
Slowest flit = 151019
Fragmentation average = 66.4065
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0147483
	minimum = 0.009 (at node 36)
	maximum = 0.0223333 (at node 174)
Accepted packet rate average = 0.0148455
	minimum = 0.01 (at node 36)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.265467
	minimum = 0.159667 (at node 36)
	maximum = 0.397 (at node 174)
Accepted flit rate average= 0.267097
	minimum = 0.179667 (at node 36)
	maximum = 0.371 (at node 138)
Injected packet length average = 17.9999
Accepted packet length average = 17.9918
Total in-flight flits = 51655 (51655 measured)
latency change    = 0.124895
throughput change = 0.00218722
Draining remaining packets ...
Class 0:
Remaining flits: 266526 266527 266528 266529 266530 266531 266532 266533 266534 266535 [...] (6810 flits)
Measured flits: 266526 266527 266528 266529 266530 266531 266532 266533 266534 266535 [...] (6810 flits)
Time taken is 7493 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3836.64 (1 samples)
	minimum = 1342 (1 samples)
	maximum = 6127 (1 samples)
Network latency average = 1023.83 (1 samples)
	minimum = 22 (1 samples)
	maximum = 3837 (1 samples)
Flit latency average = 1023.69 (1 samples)
	minimum = 5 (1 samples)
	maximum = 3820 (1 samples)
Fragmentation average = 69.9169 (1 samples)
	minimum = 0 (1 samples)
	maximum = 307 (1 samples)
Injected packet rate average = 0.0147483 (1 samples)
	minimum = 0.009 (1 samples)
	maximum = 0.0223333 (1 samples)
Accepted packet rate average = 0.0148455 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.265467 (1 samples)
	minimum = 0.159667 (1 samples)
	maximum = 0.397 (1 samples)
Accepted flit rate average = 0.267097 (1 samples)
	minimum = 0.179667 (1 samples)
	maximum = 0.371 (1 samples)
Injected packet size average = 17.9999 (1 samples)
Accepted packet size average = 17.9918 (1 samples)
Hops average = 5.10654 (1 samples)
Total run time 9.41171
