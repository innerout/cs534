BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 313.042
	minimum = 23
	maximum = 931
Network latency average = 300.483
	minimum = 23
	maximum = 931
Slowest packet = 355
Flit latency average = 244.864
	minimum = 6
	maximum = 933
Slowest flit = 4799
Fragmentation average = 150.707
	minimum = 0
	maximum = 733
Injected packet rate average = 0.037276
	minimum = 0.025 (at node 81)
	maximum = 0.051 (at node 151)
Accepted packet rate average = 0.0111875
	minimum = 0.004 (at node 81)
	maximum = 0.02 (at node 88)
Injected flit rate average = 0.665292
	minimum = 0.45 (at node 81)
	maximum = 0.909 (at node 151)
Accepted flit rate average= 0.230281
	minimum = 0.091 (at node 135)
	maximum = 0.369 (at node 48)
Injected packet length average = 17.8477
Accepted packet length average = 20.5838
Total in-flight flits = 84612 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 590.673
	minimum = 23
	maximum = 1912
Network latency average = 575.626
	minimum = 23
	maximum = 1863
Slowest packet = 748
Flit latency average = 508.428
	minimum = 6
	maximum = 1919
Slowest flit = 6458
Fragmentation average = 185.573
	minimum = 0
	maximum = 1405
Injected packet rate average = 0.0376797
	minimum = 0.0285 (at node 81)
	maximum = 0.05 (at node 151)
Accepted packet rate average = 0.0123932
	minimum = 0.0075 (at node 9)
	maximum = 0.02 (at node 136)
Injected flit rate average = 0.675784
	minimum = 0.513 (at node 81)
	maximum = 0.897 (at node 151)
Accepted flit rate average= 0.237555
	minimum = 0.1415 (at node 9)
	maximum = 0.367 (at node 136)
Injected packet length average = 17.935
Accepted packet length average = 19.1681
Total in-flight flits = 169221 (0 measured)
latency change    = 0.470025
throughput change = 0.0306179
Class 0:
Packet latency average = 1354.77
	minimum = 23
	maximum = 2796
Network latency average = 1338.27
	minimum = 23
	maximum = 2744
Slowest packet = 902
Flit latency average = 1291.24
	minimum = 6
	maximum = 2822
Slowest flit = 19681
Fragmentation average = 210.349
	minimum = 0
	maximum = 2254
Injected packet rate average = 0.0375521
	minimum = 0.022 (at node 163)
	maximum = 0.049 (at node 110)
Accepted packet rate average = 0.0135729
	minimum = 0.006 (at node 155)
	maximum = 0.024 (at node 41)
Injected flit rate average = 0.675307
	minimum = 0.397 (at node 163)
	maximum = 0.883 (at node 110)
Accepted flit rate average= 0.243005
	minimum = 0.1 (at node 147)
	maximum = 0.433 (at node 29)
Injected packet length average = 17.9832
Accepted packet length average = 17.9037
Total in-flight flits = 252344 (0 measured)
latency change    = 0.564005
throughput change = 0.0224296
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 75.5771
	minimum = 25
	maximum = 335
Network latency average = 55.5839
	minimum = 23
	maximum = 285
Slowest packet = 22234
Flit latency average = 1802.48
	minimum = 6
	maximum = 3825
Slowest flit = 17817
Fragmentation average = 15.7705
	minimum = 0
	maximum = 71
Injected packet rate average = 0.0384792
	minimum = 0.023 (at node 16)
	maximum = 0.053 (at node 104)
Accepted packet rate average = 0.0137552
	minimum = 0.006 (at node 156)
	maximum = 0.024 (at node 151)
Injected flit rate average = 0.692391
	minimum = 0.414 (at node 77)
	maximum = 0.967 (at node 104)
Accepted flit rate average= 0.247036
	minimum = 0.119 (at node 121)
	maximum = 0.413 (at node 70)
Injected packet length average = 17.9939
Accepted packet length average = 17.9595
Total in-flight flits = 337897 (122295 measured)
latency change    = 16.9257
throughput change = 0.0163184
Class 0:
Packet latency average = 72.6399
	minimum = 23
	maximum = 335
Network latency average = 53.606
	minimum = 23
	maximum = 307
Slowest packet = 22234
Flit latency average = 2052.08
	minimum = 6
	maximum = 4831
Slowest flit = 14705
Fragmentation average = 15.9619
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0380859
	minimum = 0.0255 (at node 176)
	maximum = 0.0485 (at node 38)
Accepted packet rate average = 0.013763
	minimum = 0.008 (at node 4)
	maximum = 0.02 (at node 78)
Injected flit rate average = 0.685562
	minimum = 0.4625 (at node 176)
	maximum = 0.8805 (at node 106)
Accepted flit rate average= 0.247167
	minimum = 0.135 (at node 4)
	maximum = 0.3605 (at node 127)
Injected packet length average = 18.0004
Accepted packet length average = 17.9588
Total in-flight flits = 420682 (241308 measured)
latency change    = 0.0404344
throughput change = 0.000526804
Class 0:
Packet latency average = 93.3421
	minimum = 23
	maximum = 2977
Network latency average = 75.3104
	minimum = 23
	maximum = 2977
Slowest packet = 21702
Flit latency average = 2310.07
	minimum = 6
	maximum = 5482
Slowest flit = 65143
Fragmentation average = 16.9251
	minimum = 0
	maximum = 238
Injected packet rate average = 0.0379688
	minimum = 0.0276667 (at node 95)
	maximum = 0.0466667 (at node 120)
Accepted packet rate average = 0.0137205
	minimum = 0.00833333 (at node 4)
	maximum = 0.0193333 (at node 78)
Injected flit rate average = 0.683458
	minimum = 0.498 (at node 95)
	maximum = 0.844333 (at node 120)
Accepted flit rate average= 0.245467
	minimum = 0.152333 (at node 4)
	maximum = 0.341333 (at node 151)
Injected packet length average = 18.0005
Accepted packet length average = 17.8905
Total in-flight flits = 504615 (360566 measured)
latency change    = 0.221788
throughput change = 0.00692416
Class 0:
Packet latency average = 227.993
	minimum = 23
	maximum = 3967
Network latency average = 209.567
	minimum = 23
	maximum = 3921
Slowest packet = 22211
Flit latency average = 2569.5
	minimum = 6
	maximum = 6655
Slowest flit = 42683
Fragmentation average = 23.9835
	minimum = 0
	maximum = 971
Injected packet rate average = 0.0379714
	minimum = 0.0285 (at node 95)
	maximum = 0.0455 (at node 132)
Accepted packet rate average = 0.0136393
	minimum = 0.00875 (at node 26)
	maximum = 0.01825 (at node 70)
Injected flit rate average = 0.683424
	minimum = 0.50925 (at node 95)
	maximum = 0.81975 (at node 132)
Accepted flit rate average= 0.244393
	minimum = 0.15175 (at node 26)
	maximum = 0.3325 (at node 70)
Injected packet length average = 17.9984
Accepted packet length average = 17.9183
Total in-flight flits = 589566 (478659 measured)
latency change    = 0.590592
throughput change = 0.00439368
Class 0:
Packet latency average = 577.086
	minimum = 23
	maximum = 4980
Network latency average = 558.659
	minimum = 23
	maximum = 4958
Slowest packet = 21870
Flit latency average = 2823.42
	minimum = 6
	maximum = 7655
Slowest flit = 41302
Fragmentation average = 37.9132
	minimum = 0
	maximum = 1292
Injected packet rate average = 0.0380417
	minimum = 0.0296 (at node 95)
	maximum = 0.0454 (at node 172)
Accepted packet rate average = 0.0135698
	minimum = 0.0092 (at node 4)
	maximum = 0.0184 (at node 70)
Injected flit rate average = 0.684782
	minimum = 0.5318 (at node 95)
	maximum = 0.8138 (at node 172)
Accepted flit rate average= 0.243371
	minimum = 0.1642 (at node 26)
	maximum = 0.339 (at node 70)
Injected packet length average = 18.0008
Accepted packet length average = 17.9348
Total in-flight flits = 676068 (593621 measured)
latency change    = 0.604924
throughput change = 0.00420098
Class 0:
Packet latency average = 1136.23
	minimum = 23
	maximum = 5982
Network latency average = 1117.87
	minimum = 23
	maximum = 5907
Slowest packet = 21706
Flit latency average = 3087.76
	minimum = 6
	maximum = 8580
Slowest flit = 41309
Fragmentation average = 56.5068
	minimum = 0
	maximum = 1292
Injected packet rate average = 0.0378845
	minimum = 0.0308333 (at node 191)
	maximum = 0.0445 (at node 79)
Accepted packet rate average = 0.0134757
	minimum = 0.0095 (at node 4)
	maximum = 0.0188333 (at node 70)
Injected flit rate average = 0.681858
	minimum = 0.556333 (at node 191)
	maximum = 0.800167 (at node 79)
Accepted flit rate average= 0.24195
	minimum = 0.168167 (at node 5)
	maximum = 0.335 (at node 70)
Injected packet length average = 17.9983
Accepted packet length average = 17.9545
Total in-flight flits = 759192 (698901 measured)
latency change    = 0.492105
throughput change = 0.00587387
Class 0:
Packet latency average = 1807.02
	minimum = 23
	maximum = 6967
Network latency average = 1786.86
	minimum = 23
	maximum = 6937
Slowest packet = 21701
Flit latency average = 3364.93
	minimum = 6
	maximum = 9231
Slowest flit = 96061
Fragmentation average = 75.2176
	minimum = 0
	maximum = 1425
Injected packet rate average = 0.0373311
	minimum = 0.0301429 (at node 92)
	maximum = 0.0431429 (at node 79)
Accepted packet rate average = 0.0133943
	minimum = 0.00985714 (at node 5)
	maximum = 0.0177143 (at node 70)
Injected flit rate average = 0.671815
	minimum = 0.543714 (at node 92)
	maximum = 0.776571 (at node 79)
Accepted flit rate average= 0.240558
	minimum = 0.178 (at node 5)
	maximum = 0.319286 (at node 70)
Injected packet length average = 17.9961
Accepted packet length average = 17.9597
Total in-flight flits = 832148 (788576 measured)
latency change    = 0.371213
throughput change = 0.00578495
Draining all recorded packets ...
Class 0:
Remaining flits: 86796 86797 86798 86799 86800 86801 86802 86803 86804 86805 [...] (904815 flits)
Measured flits: 390312 390313 390314 390315 390316 390317 390318 390319 390320 390321 [...] (773308 flits)
Class 0:
Remaining flits: 106123 106124 106125 106126 106127 108486 108487 108488 108489 108490 [...] (972476 flits)
Measured flits: 390312 390313 390314 390315 390316 390317 390318 390319 390320 390321 [...] (752236 flits)
Class 0:
Remaining flits: 108486 108487 108488 108489 108490 108491 108492 108493 108494 108495 [...] (1043408 flits)
Measured flits: 390312 390313 390314 390315 390316 390317 390318 390319 390320 390321 [...] (725670 flits)
Class 0:
Remaining flits: 108486 108487 108488 108489 108490 108491 108492 108493 108494 108495 [...] (1113006 flits)
Measured flits: 390456 390457 390458 390459 390460 390461 390462 390463 390464 390465 [...] (696792 flits)
Class 0:
Remaining flits: 108499 108500 108501 108502 108503 123372 123373 123374 123375 123376 [...] (1186720 flits)
Measured flits: 390456 390457 390458 390459 390460 390461 390462 390463 390464 390465 [...] (666362 flits)
Class 0:
Remaining flits: 180355 180356 180357 180358 180359 180558 180559 180560 180561 180562 [...] (1257030 flits)
Measured flits: 390456 390457 390458 390459 390460 390461 390462 390463 390464 390465 [...] (635191 flits)
Class 0:
Remaining flits: 186372 186373 186374 186375 186376 186377 186378 186379 186380 186381 [...] (1330504 flits)
Measured flits: 390582 390583 390584 390585 390586 390587 390588 390589 390590 390591 [...] (602809 flits)
Class 0:
Remaining flits: 186372 186373 186374 186375 186376 186377 186378 186379 186380 186381 [...] (1400077 flits)
Measured flits: 390582 390583 390584 390585 390586 390587 390588 390589 390590 390591 [...] (570528 flits)
Class 0:
Remaining flits: 186383 186384 186385 186386 186387 186388 186389 227178 227179 227180 [...] (1470660 flits)
Measured flits: 390582 390583 390584 390585 390586 390587 390588 390589 390590 390591 [...] (537506 flits)
Class 0:
Remaining flits: 249253 249254 249255 249256 249257 249258 249259 249260 249261 249262 [...] (1542659 flits)
Measured flits: 390582 390583 390584 390585 390586 390587 390588 390589 390590 390591 [...] (504389 flits)
Class 0:
Remaining flits: 260082 260083 260084 260085 260086 260087 260088 260089 260090 260091 [...] (1616540 flits)
Measured flits: 390690 390691 390692 390693 390694 390695 390696 390697 390698 390699 [...] (472066 flits)
Class 0:
Remaining flits: 260082 260083 260084 260085 260086 260087 260088 260089 260090 260091 [...] (1686981 flits)
Measured flits: 390700 390701 390702 390703 390704 390705 390706 390707 396432 396433 [...] (438947 flits)
Class 0:
Remaining flits: 285475 285476 285477 285478 285479 287280 287281 287282 287283 287284 [...] (1757060 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (406394 flits)
Class 0:
Remaining flits: 287280 287281 287282 287283 287284 287285 287286 287287 287288 287289 [...] (1827523 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (374094 flits)
Class 0:
Remaining flits: 287280 287281 287282 287283 287284 287285 287286 287287 287288 287289 [...] (1898137 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (342338 flits)
Class 0:
Remaining flits: 287288 287289 287290 287291 287292 287293 287294 287295 287296 287297 [...] (1970268 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (311050 flits)
Class 0:
Remaining flits: 328302 328303 328304 328305 328306 328307 328308 328309 328310 328311 [...] (2038706 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (280777 flits)
Class 0:
Remaining flits: 356202 356203 356204 356205 356206 356207 356208 356209 356210 356211 [...] (2104600 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (252373 flits)
Class 0:
Remaining flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (2155829 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (225650 flits)
Class 0:
Remaining flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (2190767 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (199764 flits)
Class 0:
Remaining flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (2214871 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (176303 flits)
Class 0:
Remaining flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (2229442 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (155108 flits)
Class 0:
Remaining flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (2241490 flits)
Measured flits: 396432 396433 396434 396435 396436 396437 396438 396439 396440 396441 [...] (136137 flits)
Class 0:
Remaining flits: 439812 439813 439814 439815 439816 439817 439818 439819 439820 439821 [...] (2252056 flits)
Measured flits: 439812 439813 439814 439815 439816 439817 439818 439819 439820 439821 [...] (119014 flits)
Class 0:
Remaining flits: 439824 439825 439826 439827 439828 439829 475920 475921 475922 475923 [...] (2263916 flits)
Measured flits: 439824 439825 439826 439827 439828 439829 475920 475921 475922 475923 [...] (103729 flits)
Class 0:
Remaining flits: 475920 475921 475922 475923 475924 475925 475926 475927 475928 475929 [...] (2273695 flits)
Measured flits: 475920 475921 475922 475923 475924 475925 475926 475927 475928 475929 [...] (88945 flits)
Class 0:
Remaining flits: 475920 475921 475922 475923 475924 475925 475926 475927 475928 475929 [...] (2283825 flits)
Measured flits: 475920 475921 475922 475923 475924 475925 475926 475927 475928 475929 [...] (76798 flits)
Class 0:
Remaining flits: 475934 475935 475936 475937 477072 477073 477074 477075 477076 477077 [...] (2295409 flits)
Measured flits: 475934 475935 475936 475937 477072 477073 477074 477075 477076 477077 [...] (66015 flits)
Class 0:
Remaining flits: 566361 566362 566363 566364 566365 566366 566367 566368 566369 589140 [...] (2305044 flits)
Measured flits: 566361 566362 566363 566364 566365 566366 566367 566368 566369 589140 [...] (56877 flits)
Class 0:
Remaining flits: 611154 611155 611156 611157 611158 611159 611160 611161 611162 611163 [...] (2318234 flits)
Measured flits: 611154 611155 611156 611157 611158 611159 611160 611161 611162 611163 [...] (49001 flits)
Class 0:
Remaining flits: 611154 611155 611156 611157 611158 611159 611160 611161 611162 611163 [...] (2330142 flits)
Measured flits: 611154 611155 611156 611157 611158 611159 611160 611161 611162 611163 [...] (42130 flits)
Class 0:
Remaining flits: 628254 628255 628256 628257 628258 628259 628260 628261 628262 628263 [...] (2343102 flits)
Measured flits: 628254 628255 628256 628257 628258 628259 628260 628261 628262 628263 [...] (36093 flits)
Class 0:
Remaining flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (2351669 flits)
Measured flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (31199 flits)
Class 0:
Remaining flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (2362866 flits)
Measured flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (26650 flits)
Class 0:
Remaining flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (2371695 flits)
Measured flits: 628308 628309 628310 628311 628312 628313 628314 628315 628316 628317 [...] (23041 flits)
Class 0:
Remaining flits: 707868 707869 707870 707871 707872 707873 707874 707875 707876 707877 [...] (2381557 flits)
Measured flits: 707868 707869 707870 707871 707872 707873 707874 707875 707876 707877 [...] (19716 flits)
Class 0:
Remaining flits: 707868 707869 707870 707871 707872 707873 707874 707875 707876 707877 [...] (2393278 flits)
Measured flits: 707868 707869 707870 707871 707872 707873 707874 707875 707876 707877 [...] (17010 flits)
Class 0:
Remaining flits: 717325 717326 717327 717328 717329 717330 717331 717332 717333 717334 [...] (2400836 flits)
Measured flits: 717325 717326 717327 717328 717329 717330 717331 717332 717333 717334 [...] (14877 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2407956 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (12916 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2418350 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (10947 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2423795 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (9285 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2429008 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (7820 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2435838 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (6411 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2438854 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (5521 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2442651 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (4766 flits)
Class 0:
Remaining flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (2446185 flits)
Measured flits: 783378 783379 783380 783381 783382 783383 783384 783385 783386 783387 [...] (3866 flits)
Class 0:
Remaining flits: 783388 783389 783390 783391 783392 783393 783394 783395 922932 922933 [...] (2449351 flits)
Measured flits: 783388 783389 783390 783391 783392 783393 783394 783395 922932 922933 [...] (3183 flits)
Class 0:
Remaining flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2454284 flits)
Measured flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2634 flits)
Class 0:
Remaining flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2455017 flits)
Measured flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2263 flits)
Class 0:
Remaining flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2453741 flits)
Measured flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (1886 flits)
Class 0:
Remaining flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (2449939 flits)
Measured flits: 922932 922933 922934 922935 922936 922937 922938 922939 922940 922941 [...] (1593 flits)
Class 0:
Remaining flits: 975042 975043 975044 975045 975046 975047 975048 975049 975050 975051 [...] (2447261 flits)
Measured flits: 975042 975043 975044 975045 975046 975047 975048 975049 975050 975051 [...] (1287 flits)
Class 0:
Remaining flits: 982044 982045 982046 982047 982048 982049 982050 982051 982052 982053 [...] (2444672 flits)
Measured flits: 982044 982045 982046 982047 982048 982049 982050 982051 982052 982053 [...] (987 flits)
Class 0:
Remaining flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (2441484 flits)
Measured flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (798 flits)
Class 0:
Remaining flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (2442273 flits)
Measured flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (601 flits)
Class 0:
Remaining flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (2441385 flits)
Measured flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (465 flits)
Class 0:
Remaining flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (2441415 flits)
Measured flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (357 flits)
Class 0:
Remaining flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (2435886 flits)
Measured flits: 1046412 1046413 1046414 1046415 1046416 1046417 1046418 1046419 1046420 1046421 [...] (252 flits)
Class 0:
Remaining flits: 1161010 1161011 1161012 1161013 1161014 1161015 1161016 1161017 1228302 1228303 [...] (2430247 flits)
Measured flits: 1161010 1161011 1161012 1161013 1161014 1161015 1161016 1161017 1228302 1228303 [...] (134 flits)
Class 0:
Remaining flits: 1228302 1228303 1228304 1228305 1228306 1228307 1228308 1228309 1228310 1228311 [...] (2424286 flits)
Measured flits: 1228302 1228303 1228304 1228305 1228306 1228307 1228308 1228309 1228310 1228311 [...] (90 flits)
Class 0:
Remaining flits: 1228302 1228303 1228304 1228305 1228306 1228307 1228308 1228309 1228310 1228311 [...] (2418586 flits)
Measured flits: 1228302 1228303 1228304 1228305 1228306 1228307 1228308 1228309 1228310 1228311 [...] (72 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1349099 1367424 1367425 1367426 1367427 1367428 1367429 1367430 1367431 1367432 [...] (2382328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1367424 1367425 1367426 1367427 1367428 1367429 1367430 1367431 1367432 1367433 [...] (2348496 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1367424 1367425 1367426 1367427 1367428 1367429 1367430 1367431 1367432 1367433 [...] (2314570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1367424 1367425 1367426 1367427 1367428 1367429 1367430 1367431 1367432 1367433 [...] (2279803 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1367424 1367425 1367426 1367427 1367428 1367429 1367430 1367431 1367432 1367433 [...] (2244894 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1371852 1371853 1371854 1371855 1371856 1371857 1371858 1371859 1371860 1371861 [...] (2210146 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1371866 1371867 1371868 1371869 1396314 1396315 1396316 1396317 1396318 1396319 [...] (2175029 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424070 1424071 1424072 1424073 1424074 1424075 1424076 1424077 1424078 1424079 [...] (2140175 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424070 1424071 1424072 1424073 1424074 1424075 1424076 1424077 1424078 1424079 [...] (2104123 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1424078 1424079 1424080 1424081 1424082 1424083 1424084 1424085 1424086 1424087 [...] (2069444 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1552644 1552645 1552646 1552647 1552648 1552649 1552650 1552651 1552652 1552653 [...] (2034742 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559196 1559197 1559198 1559199 1559200 1559201 1559202 1559203 1559204 1559205 [...] (1999923 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559196 1559197 1559198 1559199 1559200 1559201 1559202 1559203 1559204 1559205 [...] (1965812 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559196 1559197 1559198 1559199 1559200 1559201 1559202 1559203 1559204 1559205 [...] (1930640 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559196 1559197 1559198 1559199 1559200 1559201 1559202 1559203 1559204 1559205 [...] (1895738 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1559196 1559197 1559198 1559199 1559200 1559201 1559202 1559203 1559204 1559205 [...] (1860708 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1677834 1677835 1677836 1677837 1677838 1677839 1677840 1677841 1677842 1677843 [...] (1825530 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1677834 1677835 1677836 1677837 1677838 1677839 1677840 1677841 1677842 1677843 [...] (1790964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1677834 1677835 1677836 1677837 1677838 1677839 1677840 1677841 1677842 1677843 [...] (1756481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811268 1811269 1811270 1811271 1811272 1811273 1811274 1811275 1811276 1811277 [...] (1722605 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1811268 1811269 1811270 1811271 1811272 1811273 1811274 1811275 1811276 1811277 [...] (1688640 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839222 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 [...] (1654429 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839222 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 [...] (1618836 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839222 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 [...] (1583671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839222 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 [...] (1548907 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839222 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 [...] (1514792 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1839223 1839224 1839225 1839226 1839227 1839228 1839229 1839230 1839231 1839232 [...] (1479338 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033172 2033173 2033174 2033175 2033176 2033177 2033178 2033179 2033180 2033181 [...] (1443840 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033172 2033173 2033174 2033175 2033176 2033177 2033178 2033179 2033180 2033181 [...] (1408956 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033172 2033173 2033174 2033175 2033176 2033177 2033178 2033179 2033180 2033181 [...] (1374819 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033172 2033173 2033174 2033175 2033176 2033177 2033178 2033179 2033180 2033181 [...] (1341101 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2033172 2033173 2033174 2033175 2033176 2033177 2033178 2033179 2033180 2033181 [...] (1306926 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154528 2154529 2154530 2154531 2154532 2154533 2154534 2154535 2154536 2154537 [...] (1272063 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154528 2154529 2154530 2154531 2154532 2154533 2154534 2154535 2154536 2154537 [...] (1237524 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154528 2154529 2154530 2154531 2154532 2154533 2154534 2154535 2154536 2154537 [...] (1203406 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2154528 2154529 2154530 2154531 2154532 2154533 2154534 2154535 2154536 2154537 [...] (1169226 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2186119 2186120 2186121 2186122 2186123 2186124 2186125 2186126 2186127 2186128 [...] (1135394 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228976 2228977 2228978 2228979 2228980 2228981 2228982 2228983 2228984 2228985 [...] (1101690 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228976 2228977 2228978 2228979 2228980 2228981 2228982 2228983 2228984 2228985 [...] (1067828 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228976 2228977 2228978 2228979 2228980 2228981 2228982 2228983 2228984 2228985 [...] (1034512 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228976 2228977 2228978 2228979 2228980 2228981 2228982 2228983 2228984 2228985 [...] (1000931 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2228993 2320704 2320705 2320706 2320707 2320708 2320709 2320710 2320711 2320712 [...] (967207 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2320704 2320705 2320706 2320707 2320708 2320709 2320710 2320711 2320712 2320713 [...] (933179 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2320704 2320705 2320706 2320707 2320708 2320709 2320710 2320711 2320712 2320713 [...] (899270 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2320716 2320717 2320718 2320719 2320720 2320721 2404026 2404027 2404028 2404029 [...] (865655 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2404026 2404027 2404028 2404029 2404030 2404031 2404032 2404033 2404034 2404035 [...] (832294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2404026 2404027 2404028 2404029 2404030 2404031 2404032 2404033 2404034 2404035 [...] (799319 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (765879 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (732283 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (698359 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (664453 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414088 2414089 2414090 2414091 2414092 2414093 2414094 2414095 2414096 2414097 [...] (630729 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2414103 2414104 2414105 2532762 2532763 2532764 2532765 2532766 2532767 2532768 [...] (596277 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2532762 2532763 2532764 2532765 2532766 2532767 2532768 2532769 2532770 2532771 [...] (561860 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555064 2555065 2555066 2555067 2555068 2555069 2555070 2555071 2555072 2555073 [...] (527202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555064 2555065 2555066 2555067 2555068 2555069 2555070 2555071 2555072 2555073 [...] (492208 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555064 2555065 2555066 2555067 2555068 2555069 2555070 2555071 2555072 2555073 [...] (458027 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555064 2555065 2555066 2555067 2555068 2555069 2555070 2555071 2555072 2555073 [...] (423483 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2555064 2555065 2555066 2555067 2555068 2555069 2555070 2555071 2555072 2555073 [...] (388765 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2646180 2646181 2646182 2646183 2646184 2646185 2646186 2646187 2646188 2646189 [...] (354261 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2780154 2780155 2780156 2780157 2780158 2780159 2780160 2780161 2780162 2780163 [...] (319520 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2780154 2780155 2780156 2780157 2780158 2780159 2780160 2780161 2780162 2780163 [...] (284699 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2780154 2780155 2780156 2780157 2780158 2780159 2780160 2780161 2780162 2780163 [...] (251068 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2780154 2780155 2780156 2780157 2780158 2780159 2780160 2780161 2780162 2780163 [...] (218078 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836908 2836909 2836910 2836911 2836912 2836913 2836914 2836915 2836916 2836917 [...] (185810 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836908 2836909 2836910 2836911 2836912 2836913 2836914 2836915 2836916 2836917 [...] (154377 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836908 2836909 2836910 2836911 2836912 2836913 2836914 2836915 2836916 2836917 [...] (125272 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836908 2836909 2836910 2836911 2836912 2836913 2836914 2836915 2836916 2836917 [...] (98298 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2836908 2836909 2836910 2836911 2836912 2836913 2836914 2836915 2836916 2836917 [...] (73223 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3107376 3107377 3107378 3107379 3107380 3107381 3107382 3107383 3107384 3107385 [...] (49775 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3107376 3107377 3107378 3107379 3107380 3107381 3107382 3107383 3107384 3107385 [...] (29704 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3129066 3129067 3129068 3129069 3129070 3129071 3129072 3129073 3129074 3129075 [...] (15717 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3129066 3129067 3129068 3129069 3129070 3129071 3129072 3129073 3129074 3129075 [...] (5976 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 4024273 4024274 4024275 4024276 4024277 4268250 4268251 4268252 4268253 4268254 [...] (712 flits)
Measured flits: (0 flits)
Time taken is 146286 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15785.2 (1 samples)
	minimum = 23 (1 samples)
	maximum = 62304 (1 samples)
Network latency average = 15738.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 62242 (1 samples)
Flit latency average = 43297.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 118136 (1 samples)
Fragmentation average = 190.724 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4414 (1 samples)
Injected packet rate average = 0.0373311 (1 samples)
	minimum = 0.0301429 (1 samples)
	maximum = 0.0431429 (1 samples)
Accepted packet rate average = 0.0133943 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0177143 (1 samples)
Injected flit rate average = 0.671815 (1 samples)
	minimum = 0.543714 (1 samples)
	maximum = 0.776571 (1 samples)
Accepted flit rate average = 0.240558 (1 samples)
	minimum = 0.178 (1 samples)
	maximum = 0.319286 (1 samples)
Injected packet size average = 17.9961 (1 samples)
Accepted packet size average = 17.9597 (1 samples)
Hops average = 5.06811 (1 samples)
Total run time 194.434
