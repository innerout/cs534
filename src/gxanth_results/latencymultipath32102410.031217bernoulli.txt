BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 302.607
	minimum = 23
	maximum = 969
Network latency average = 294.174
	minimum = 23
	maximum = 942
Slowest packet = 191
Flit latency average = 232.145
	minimum = 6
	maximum = 960
Slowest flit = 2812
Fragmentation average = 151.971
	minimum = 0
	maximum = 796
Injected packet rate average = 0.0308021
	minimum = 0.018 (at node 35)
	maximum = 0.049 (at node 95)
Accepted packet rate average = 0.010599
	minimum = 0.004 (at node 174)
	maximum = 0.019 (at node 152)
Injected flit rate average = 0.549766
	minimum = 0.319 (at node 35)
	maximum = 0.873 (at node 95)
Accepted flit rate average= 0.219641
	minimum = 0.083 (at node 174)
	maximum = 0.371 (at node 152)
Injected packet length average = 17.8483
Accepted packet length average = 20.7229
Total in-flight flits = 64281 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 555.415
	minimum = 23
	maximum = 1914
Network latency average = 545.72
	minimum = 23
	maximum = 1880
Slowest packet = 559
Flit latency average = 472.424
	minimum = 6
	maximum = 1863
Slowest flit = 10079
Fragmentation average = 188.529
	minimum = 0
	maximum = 1743
Injected packet rate average = 0.0310443
	minimum = 0.022 (at node 15)
	maximum = 0.042 (at node 135)
Accepted packet rate average = 0.0118385
	minimum = 0.006 (at node 174)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.556435
	minimum = 0.391 (at node 15)
	maximum = 0.7485 (at node 135)
Accepted flit rate average= 0.226917
	minimum = 0.117 (at node 10)
	maximum = 0.346 (at node 22)
Injected packet length average = 17.9239
Accepted packet length average = 19.1676
Total in-flight flits = 127442 (0 measured)
latency change    = 0.45517
throughput change = 0.0320648
Class 0:
Packet latency average = 1260.39
	minimum = 25
	maximum = 2861
Network latency average = 1249.43
	minimum = 23
	maximum = 2861
Slowest packet = 417
Flit latency average = 1189.27
	minimum = 6
	maximum = 2865
Slowest flit = 11492
Fragmentation average = 212.921
	minimum = 0
	maximum = 2466
Injected packet rate average = 0.0309375
	minimum = 0.017 (at node 104)
	maximum = 0.051 (at node 63)
Accepted packet rate average = 0.0132135
	minimum = 0.005 (at node 59)
	maximum = 0.023 (at node 76)
Injected flit rate average = 0.556911
	minimum = 0.298 (at node 104)
	maximum = 0.918 (at node 63)
Accepted flit rate average= 0.237458
	minimum = 0.1 (at node 22)
	maximum = 0.397 (at node 78)
Injected packet length average = 18.0012
Accepted packet length average = 17.9708
Total in-flight flits = 188770 (0 measured)
latency change    = 0.55933
throughput change = 0.0443938
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 61.2022
	minimum = 26
	maximum = 297
Network latency average = 50.95
	minimum = 23
	maximum = 284
Slowest packet = 22133
Flit latency average = 1697.73
	minimum = 6
	maximum = 3671
Slowest flit = 33021
Fragmentation average = 12.937
	minimum = 0
	maximum = 71
Injected packet rate average = 0.0318229
	minimum = 0.018 (at node 49)
	maximum = 0.048 (at node 27)
Accepted packet rate average = 0.0128854
	minimum = 0.003 (at node 86)
	maximum = 0.026 (at node 103)
Injected flit rate average = 0.572396
	minimum = 0.336 (at node 49)
	maximum = 0.864 (at node 27)
Accepted flit rate average= 0.232297
	minimum = 0.071 (at node 86)
	maximum = 0.466 (at node 103)
Injected packet length average = 17.9869
Accepted packet length average = 18.0279
Total in-flight flits = 254149 (101572 measured)
latency change    = 19.5939
throughput change = 0.0222192
Class 0:
Packet latency average = 91.0892
	minimum = 23
	maximum = 1863
Network latency average = 81.0297
	minimum = 23
	maximum = 1863
Slowest packet = 18335
Flit latency average = 1938.56
	minimum = 6
	maximum = 4700
Slowest flit = 22931
Fragmentation average = 16.4728
	minimum = 0
	maximum = 265
Injected packet rate average = 0.0313437
	minimum = 0.0215 (at node 164)
	maximum = 0.04 (at node 67)
Accepted packet rate average = 0.0130104
	minimum = 0.005 (at node 86)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.563945
	minimum = 0.387 (at node 164)
	maximum = 0.7225 (at node 67)
Accepted flit rate average= 0.233813
	minimum = 0.1015 (at node 86)
	maximum = 0.3785 (at node 103)
Injected packet length average = 17.9923
Accepted packet length average = 17.9712
Total in-flight flits = 315634 (198812 measured)
latency change    = 0.328107
throughput change = 0.00648222
Class 0:
Packet latency average = 299.277
	minimum = 23
	maximum = 2981
Network latency average = 289.208
	minimum = 23
	maximum = 2981
Slowest packet = 17961
Flit latency average = 2174.37
	minimum = 6
	maximum = 5566
Slowest flit = 40820
Fragmentation average = 32.7751
	minimum = 0
	maximum = 1063
Injected packet rate average = 0.0313576
	minimum = 0.024 (at node 15)
	maximum = 0.0396667 (at node 67)
Accepted packet rate average = 0.012934
	minimum = 0.00766667 (at node 135)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.564382
	minimum = 0.435667 (at node 15)
	maximum = 0.715667 (at node 67)
Accepted flit rate average= 0.232535
	minimum = 0.137333 (at node 135)
	maximum = 0.348333 (at node 16)
Injected packet length average = 17.9982
Accepted packet length average = 17.9785
Total in-flight flits = 379946 (295323 measured)
latency change    = 0.695636
throughput change = 0.005495
Class 0:
Packet latency average = 776.784
	minimum = 23
	maximum = 3977
Network latency average = 766.51
	minimum = 23
	maximum = 3962
Slowest packet = 17907
Flit latency average = 2421.21
	minimum = 6
	maximum = 6350
Slowest flit = 67408
Fragmentation average = 58.7691
	minimum = 0
	maximum = 1063
Injected packet rate average = 0.0313737
	minimum = 0.024 (at node 15)
	maximum = 0.038 (at node 67)
Accepted packet rate average = 0.0128945
	minimum = 0.00875 (at node 64)
	maximum = 0.01975 (at node 16)
Injected flit rate average = 0.564616
	minimum = 0.4315 (at node 15)
	maximum = 0.68525 (at node 67)
Accepted flit rate average= 0.231539
	minimum = 0.15825 (at node 64)
	maximum = 0.35325 (at node 16)
Injected packet length average = 17.9965
Accepted packet length average = 17.9564
Total in-flight flits = 444658 (385743 measured)
latency change    = 0.614723
throughput change = 0.00430018
Class 0:
Packet latency average = 1393.82
	minimum = 23
	maximum = 4977
Network latency average = 1383.26
	minimum = 23
	maximum = 4964
Slowest packet = 17916
Flit latency average = 2662.41
	minimum = 6
	maximum = 7379
Slowest flit = 57923
Fragmentation average = 84.6359
	minimum = 0
	maximum = 1699
Injected packet rate average = 0.031424
	minimum = 0.0256 (at node 15)
	maximum = 0.0378 (at node 64)
Accepted packet rate average = 0.0128906
	minimum = 0.0094 (at node 31)
	maximum = 0.0184 (at node 16)
Injected flit rate average = 0.565514
	minimum = 0.4608 (at node 97)
	maximum = 0.6776 (at node 64)
Accepted flit rate average= 0.231349
	minimum = 0.1664 (at node 64)
	maximum = 0.3278 (at node 16)
Injected packet length average = 17.9963
Accepted packet length average = 17.9471
Total in-flight flits = 509681 (469083 measured)
latency change    = 0.442693
throughput change = 0.00082172
Class 0:
Packet latency average = 1999.69
	minimum = 23
	maximum = 5957
Network latency average = 1989.14
	minimum = 23
	maximum = 5938
Slowest packet = 17868
Flit latency average = 2910.19
	minimum = 6
	maximum = 8168
Slowest flit = 28924
Fragmentation average = 104.042
	minimum = 0
	maximum = 1749
Injected packet rate average = 0.0313828
	minimum = 0.0255 (at node 97)
	maximum = 0.0365 (at node 64)
Accepted packet rate average = 0.0128568
	minimum = 0.00933333 (at node 64)
	maximum = 0.0173333 (at node 16)
Injected flit rate average = 0.564931
	minimum = 0.459 (at node 97)
	maximum = 0.6545 (at node 64)
Accepted flit rate average= 0.230768
	minimum = 0.169333 (at node 64)
	maximum = 0.306667 (at node 16)
Injected packet length average = 18.0013
Accepted packet length average = 17.9492
Total in-flight flits = 573679 (545771 measured)
latency change    = 0.302983
throughput change = 0.0025165
Class 0:
Packet latency average = 2555.08
	minimum = 23
	maximum = 6929
Network latency average = 2544.46
	minimum = 23
	maximum = 6909
Slowest packet = 17877
Flit latency average = 3160.45
	minimum = 6
	maximum = 9500
Slowest flit = 28925
Fragmentation average = 117.117
	minimum = 0
	maximum = 2683
Injected packet rate average = 0.0312999
	minimum = 0.0252857 (at node 15)
	maximum = 0.0361429 (at node 64)
Accepted packet rate average = 0.012785
	minimum = 0.01 (at node 104)
	maximum = 0.0162857 (at node 16)
Injected flit rate average = 0.563356
	minimum = 0.455143 (at node 97)
	maximum = 0.650571 (at node 64)
Accepted flit rate average= 0.229636
	minimum = 0.181857 (at node 104)
	maximum = 0.290286 (at node 103)
Injected packet length average = 17.9987
Accepted packet length average = 17.9614
Total in-flight flits = 637345 (617142 measured)
latency change    = 0.217366
throughput change = 0.00492984
Draining all recorded packets ...
Class 0:
Remaining flits: 49013 93492 93493 93494 93495 93496 93497 93498 93499 93500 [...] (701727 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (590186 flits)
Class 0:
Remaining flits: 95238 95239 95240 95241 95242 95243 95244 95245 95246 95247 [...] (763290 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (560598 flits)
Class 0:
Remaining flits: 95238 95239 95240 95241 95242 95243 95244 95245 95246 95247 [...] (824876 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (530138 flits)
Class 0:
Remaining flits: 106863 106864 106865 132264 132265 132266 132267 132268 132269 132270 [...] (882275 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (498372 flits)
Class 0:
Remaining flits: 141408 141409 141410 141411 141412 141413 141414 141415 141416 141417 [...] (937710 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (465942 flits)
Class 0:
Remaining flits: 141416 141417 141418 141419 141420 141421 141422 141423 141424 141425 [...] (993456 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (433164 flits)
Class 0:
Remaining flits: 183798 183799 183800 183801 183802 183803 183804 183805 183806 183807 [...] (1046790 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (399567 flits)
Class 0:
Remaining flits: 183798 183799 183800 183801 183802 183803 183804 183805 183806 183807 [...] (1100552 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (366425 flits)
Class 0:
Remaining flits: 183798 183799 183800 183801 183802 183803 183804 183805 183806 183807 [...] (1156147 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (333241 flits)
Class 0:
Remaining flits: 212418 212419 212420 212421 212422 212423 212424 212425 212426 212427 [...] (1211849 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (301072 flits)
Class 0:
Remaining flits: 212418 212419 212420 212421 212422 212423 212424 212425 212426 212427 [...] (1267946 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (269875 flits)
Class 0:
Remaining flits: 248814 248815 248816 248817 248818 248819 248820 248821 248822 248823 [...] (1322206 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (240747 flits)
Class 0:
Remaining flits: 248814 248815 248816 248817 248818 248819 248820 248821 248822 248823 [...] (1377081 flits)
Measured flits: 321552 321553 321554 321555 321556 321557 321558 321559 321560 321561 [...] (212422 flits)
Class 0:
Remaining flits: 307962 307963 307964 307965 307966 307967 307968 307969 307970 307971 [...] (1430640 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (185881 flits)
Class 0:
Remaining flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (1485350 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (160828 flits)
Class 0:
Remaining flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (1539014 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (138328 flits)
Class 0:
Remaining flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (1594223 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (118908 flits)
Class 0:
Remaining flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (1651281 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (101266 flits)
Class 0:
Remaining flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (1706661 flits)
Measured flits: 333972 333973 333974 333975 333976 333977 333978 333979 333980 333981 [...] (85647 flits)
Class 0:
Remaining flits: 358920 358921 358922 358923 358924 358925 358926 358927 358928 358929 [...] (1759800 flits)
Measured flits: 358920 358921 358922 358923 358924 358925 358926 358927 358928 358929 [...] (72784 flits)
Class 0:
Remaining flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (1813499 flits)
Measured flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (61312 flits)
Class 0:
Remaining flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (1866934 flits)
Measured flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (51279 flits)
Class 0:
Remaining flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (1923593 flits)
Measured flits: 406602 406603 406604 406605 406606 406607 406608 406609 406610 406611 [...] (42987 flits)
Class 0:
Remaining flits: 406605 406606 406607 406608 406609 406610 406611 406612 406613 406614 [...] (1978437 flits)
Measured flits: 406605 406606 406607 406608 406609 406610 406611 406612 406613 406614 [...] (35176 flits)
Class 0:
Remaining flits: 580464 580465 580466 580467 580468 580469 580470 580471 580472 580473 [...] (2033490 flits)
Measured flits: 580464 580465 580466 580467 580468 580469 580470 580471 580472 580473 [...] (28940 flits)
Class 0:
Remaining flits: 580476 580477 580478 580479 580480 580481 603018 603019 603020 603021 [...] (2089186 flits)
Measured flits: 580476 580477 580478 580479 580480 580481 603018 603019 603020 603021 [...] (24395 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2144230 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (19733 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2195202 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (16035 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2240655 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (12955 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2279420 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (10110 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2311673 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (7982 flits)
Class 0:
Remaining flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (2339132 flits)
Measured flits: 613692 613693 613694 613695 613696 613697 613698 613699 613700 613701 [...] (6100 flits)
Class 0:
Remaining flits: 645210 645211 645212 645213 645214 645215 645216 645217 645218 645219 [...] (2357552 flits)
Measured flits: 645210 645211 645212 645213 645214 645215 645216 645217 645218 645219 [...] (4648 flits)
Class 0:
Remaining flits: 645210 645211 645212 645213 645214 645215 645216 645217 645218 645219 [...] (2373020 flits)
Measured flits: 645210 645211 645212 645213 645214 645215 645216 645217 645218 645219 [...] (3528 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2385479 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2585 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2399521 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2122 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2408380 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (1713 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2417254 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (1395 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2424382 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (1191 flits)
Class 0:
Remaining flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (2436341 flits)
Measured flits: 694224 694225 694226 694227 694228 694229 694230 694231 694232 694233 [...] (870 flits)
Class 0:
Remaining flits: 814854 814855 814856 814857 814858 814859 859068 859069 859070 859071 [...] (2444429 flits)
Measured flits: 814854 814855 814856 814857 814858 814859 859068 859069 859070 859071 [...] (655 flits)
Class 0:
Remaining flits: 859068 859069 859070 859071 859072 859073 859074 859075 859076 859077 [...] (2451615 flits)
Measured flits: 859068 859069 859070 859071 859072 859073 859074 859075 859076 859077 [...] (353 flits)
Class 0:
Remaining flits: 859068 859069 859070 859071 859072 859073 859074 859075 859076 859077 [...] (2456359 flits)
Measured flits: 859068 859069 859070 859071 859072 859073 859074 859075 859076 859077 [...] (174 flits)
Class 0:
Remaining flits: 956952 956953 956954 956955 956956 956957 956958 956959 956960 956961 [...] (2459946 flits)
Measured flits: 956952 956953 956954 956955 956956 956957 956958 956959 956960 956961 [...] (108 flits)
Class 0:
Remaining flits: 956952 956953 956954 956955 956956 956957 956958 956959 956960 956961 [...] (2462780 flits)
Measured flits: 956952 956953 956954 956955 956956 956957 956958 956959 956960 956961 [...] (90 flits)
Class 0:
Remaining flits: 961693 961694 961695 961696 961697 961698 961699 961700 961701 961702 [...] (2464120 flits)
Measured flits: 961693 961694 961695 961696 961697 961698 961699 961700 961701 961702 [...] (47 flits)
Class 0:
Remaining flits: 1000766 1000767 1000768 1000769 1000770 1000771 1000772 1000773 1000774 1000775 [...] (2468017 flits)
Measured flits: 1000766 1000767 1000768 1000769 1000770 1000771 1000772 1000773 1000774 1000775 [...] (34 flits)
Class 0:
Remaining flits: 1000767 1000768 1000769 1000770 1000771 1000772 1000773 1000774 1000775 1000776 [...] (2467462 flits)
Measured flits: 1000767 1000768 1000769 1000770 1000771 1000772 1000773 1000774 1000775 1000776 [...] (15 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2431477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2395951 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2360438 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2324790 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2290082 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2255545 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2220477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091952 1091953 1091954 1091955 1091956 1091957 1091958 1091959 1091960 1091961 [...] (2186284 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1203374 1203375 1203376 1203377 1203378 1203379 1203380 1203381 1203382 1203383 [...] (2150863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1268406 1268407 1268408 1268409 1268410 1268411 1268412 1268413 1268414 1268415 [...] (2115863 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1342008 1342009 1342010 1342011 1342012 1342013 1342014 1342015 1342016 1342017 [...] (2081750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1342008 1342009 1342010 1342011 1342012 1342013 1342014 1342015 1342016 1342017 [...] (2046909 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1342008 1342009 1342010 1342011 1342012 1342013 1342014 1342015 1342016 1342017 [...] (2012021 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1342008 1342009 1342010 1342011 1342012 1342013 1342014 1342015 1342016 1342017 [...] (1976564 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1373724 1373725 1373726 1373727 1373728 1373729 1373730 1373731 1373732 1373733 [...] (1942271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1491977 1491978 1491979 1491980 1491981 1491982 1491983 1496142 1496143 1496144 [...] (1906927 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1522602 1522603 1522604 1522605 1522606 1522607 1522608 1522609 1522610 1522611 [...] (1871073 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1522602 1522603 1522604 1522605 1522606 1522607 1522608 1522609 1522610 1522611 [...] (1836350 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1522602 1522603 1522604 1522605 1522606 1522607 1522608 1522609 1522610 1522611 [...] (1801403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1558494 1558495 1558496 1558497 1558498 1558499 1558500 1558501 1558502 1558503 [...] (1765949 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1576800 1576801 1576802 1576803 1576804 1576805 1576806 1576807 1576808 1576809 [...] (1730386 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630206 1630207 1630208 1630209 1630210 1630211 1630212 1630213 1630214 1630215 [...] (1694826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630206 1630207 1630208 1630209 1630210 1630211 1630212 1630213 1630214 1630215 [...] (1659962 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630206 1630207 1630208 1630209 1630210 1630211 1630212 1630213 1630214 1630215 [...] (1624115 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1630207 1630208 1630209 1630210 1630211 1630212 1630213 1630214 1630215 1630216 [...] (1588925 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1709856 1709857 1709858 1709859 1709860 1709861 1709862 1709863 1709864 1709865 [...] (1553466 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1709856 1709857 1709858 1709859 1709860 1709861 1709862 1709863 1709864 1709865 [...] (1517219 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1709860 1709861 1709862 1709863 1709864 1709865 1709866 1709867 1709868 1709869 [...] (1482432 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1447751 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1413289 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1378825 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1344585 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1310393 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1765782 1765783 1765784 1765785 1765786 1765787 1765788 1765789 1765790 1765791 [...] (1276014 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1837692 1837693 1837694 1837695 1837696 1837697 1837698 1837699 1837700 1837701 [...] (1241712 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888470 1888471 1888472 1888473 1888474 1888475 1888476 1888477 1888478 1888479 [...] (1207568 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1888478 1888479 1888480 1888481 1888482 1888483 1888484 1888485 1888486 1888487 [...] (1173675 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1996002 1996003 1996004 1996005 1996006 1996007 1996008 1996009 1996010 1996011 [...] (1139911 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2009268 2009269 2009270 2009271 2009272 2009273 2009274 2009275 2009276 2009277 [...] (1106837 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2009272 2009273 2009274 2009275 2009276 2009277 2009278 2009279 2009280 2009281 [...] (1073059 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084616 2084617 2084618 2084619 2084620 2084621 2084622 2084623 2084624 2084625 [...] (1039772 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2084617 2084618 2084619 2084620 2084621 2084622 2084623 2084624 2084625 2084626 [...] (1006294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2172467 2172468 2172469 2172470 2172471 2172472 2172473 2178738 2178739 2178740 [...] (972881 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2178738 2178739 2178740 2178741 2178742 2178743 2178744 2178745 2178746 2178747 [...] (939254 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2178738 2178739 2178740 2178741 2178742 2178743 2178744 2178745 2178746 2178747 [...] (906274 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2227554 2227555 2227556 2227557 2227558 2227559 2227560 2227561 2227562 2227563 [...] (873611 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2227554 2227555 2227556 2227557 2227558 2227559 2227560 2227561 2227562 2227563 [...] (841090 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2314152 2314153 2314154 2314155 2314156 2314157 2314158 2314159 2314160 2314161 [...] (808206 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2314152 2314153 2314154 2314155 2314156 2314157 2314158 2314159 2314160 2314161 [...] (775001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2314152 2314153 2314154 2314155 2314156 2314157 2314158 2314159 2314160 2314161 [...] (741305 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (707120 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (673154 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (639349 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (606098 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (572614 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (539036 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2478024 2478025 2478026 2478027 2478028 2478029 2478030 2478031 2478032 2478033 [...] (504886 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2540358 2540359 2540360 2540361 2540362 2540363 2540364 2540365 2540366 2540367 [...] (470816 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2540358 2540359 2540360 2540361 2540362 2540363 2540364 2540365 2540366 2540367 [...] (436826 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2556486 2556487 2556488 2556489 2556490 2556491 2556492 2556493 2556494 2556495 [...] (402812 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2556492 2556493 2556494 2556495 2556496 2556497 2556498 2556499 2556500 2556501 [...] (368304 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (333375 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (298919 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (264846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (231799 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (199481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (167167 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2603988 2603989 2603990 2603991 2603992 2603993 2603994 2603995 2603996 2603997 [...] (135380 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2740878 2740879 2740880 2740881 2740882 2740883 2740884 2740885 2740886 2740887 [...] (105669 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2787498 2787499 2787500 2787501 2787502 2787503 2787504 2787505 2787506 2787507 [...] (78335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2787498 2787499 2787500 2787501 2787502 2787503 2787504 2787505 2787506 2787507 [...] (52503 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2823033 2823034 2823035 2823036 2823037 2823038 2823039 2823040 2823041 2823042 [...] (30673 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2976480 2976481 2976482 2976483 2976484 2976485 2976486 2976487 2976488 2976489 [...] (14593 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3327969 3327970 3327971 3327972 3327973 3327974 3327975 3327976 3327977 3327978 [...] (5010 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3579747 3579748 3579749 3688200 3688201 3688202 3688203 3688204 3688205 3688206 [...] (915 flits)
Measured flits: (0 flits)
Time taken is 134253 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11998.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 49109 (1 samples)
Network latency average = 11987.3 (1 samples)
	minimum = 23 (1 samples)
	maximum = 49057 (1 samples)
Flit latency average = 38830.4 (1 samples)
	minimum = 6 (1 samples)
	maximum = 103003 (1 samples)
Fragmentation average = 194.443 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4084 (1 samples)
Injected packet rate average = 0.0312999 (1 samples)
	minimum = 0.0252857 (1 samples)
	maximum = 0.0361429 (1 samples)
Accepted packet rate average = 0.012785 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0162857 (1 samples)
Injected flit rate average = 0.563356 (1 samples)
	minimum = 0.455143 (1 samples)
	maximum = 0.650571 (1 samples)
Accepted flit rate average = 0.229636 (1 samples)
	minimum = 0.181857 (1 samples)
	maximum = 0.290286 (1 samples)
Injected packet size average = 17.9987 (1 samples)
Accepted packet size average = 17.9614 (1 samples)
Hops average = 5.07428 (1 samples)
Total run time 172.272
