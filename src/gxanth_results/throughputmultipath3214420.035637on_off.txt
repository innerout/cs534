BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.035637
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 353.795
	minimum = 22
	maximum = 984
Network latency average = 241.94
	minimum = 22
	maximum = 858
Slowest packet = 90
Flit latency average = 211.992
	minimum = 5
	maximum = 841
Slowest flit = 12239
Fragmentation average = 48.9307
	minimum = 0
	maximum = 390
Injected packet rate average = 0.0292135
	minimum = 0 (at node 24)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0135365
	minimum = 0.005 (at node 41)
	maximum = 0.025 (at node 76)
Injected flit rate average = 0.520719
	minimum = 0 (at node 24)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.256443
	minimum = 0.09 (at node 41)
	maximum = 0.467 (at node 76)
Injected packet length average = 17.8246
Accepted packet length average = 18.9446
Total in-flight flits = 51725 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 621.972
	minimum = 22
	maximum = 1948
Network latency average = 457.442
	minimum = 22
	maximum = 1499
Slowest packet = 90
Flit latency average = 424.795
	minimum = 5
	maximum = 1482
Slowest flit = 43811
Fragmentation average = 65.1877
	minimum = 0
	maximum = 448
Injected packet rate average = 0.0313776
	minimum = 0.0015 (at node 22)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.0146797
	minimum = 0.0085 (at node 62)
	maximum = 0.0215 (at node 152)
Injected flit rate average = 0.562112
	minimum = 0.027 (at node 22)
	maximum = 1 (at node 29)
Accepted flit rate average= 0.273086
	minimum = 0.162 (at node 62)
	maximum = 0.4005 (at node 152)
Injected packet length average = 17.9144
Accepted packet length average = 18.603
Total in-flight flits = 112017 (0 measured)
latency change    = 0.431172
throughput change = 0.060945
Class 0:
Packet latency average = 1361.98
	minimum = 26
	maximum = 2818
Network latency average = 1097.95
	minimum = 23
	maximum = 2219
Slowest packet = 5109
Flit latency average = 1066.14
	minimum = 5
	maximum = 2307
Slowest flit = 62797
Fragmentation average = 101.566
	minimum = 0
	maximum = 486
Injected packet rate average = 0.03425
	minimum = 0 (at node 51)
	maximum = 0.056 (at node 9)
Accepted packet rate average = 0.0159948
	minimum = 0.008 (at node 132)
	maximum = 0.029 (at node 34)
Injected flit rate average = 0.616964
	minimum = 0 (at node 51)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.289953
	minimum = 0.139 (at node 132)
	maximum = 0.506 (at node 34)
Injected packet length average = 18.0135
Accepted packet length average = 18.128
Total in-flight flits = 174714 (0 measured)
latency change    = 0.543333
throughput change = 0.0581721
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 413.563
	minimum = 26
	maximum = 2045
Network latency average = 78.3821
	minimum = 22
	maximum = 891
Slowest packet = 18647
Flit latency average = 1548.7
	minimum = 5
	maximum = 2919
Slowest flit = 109015
Fragmentation average = 8.1441
	minimum = 0
	maximum = 169
Injected packet rate average = 0.031849
	minimum = 0 (at node 132)
	maximum = 0.056 (at node 2)
Accepted packet rate average = 0.0158229
	minimum = 0.006 (at node 89)
	maximum = 0.026 (at node 19)
Injected flit rate average = 0.573188
	minimum = 0 (at node 132)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.28662
	minimum = 0.106 (at node 144)
	maximum = 0.479 (at node 123)
Injected packet length average = 17.9971
Accepted packet length average = 18.1142
Total in-flight flits = 229843 (101760 measured)
latency change    = 2.29329
throughput change = 0.0116298
Class 0:
Packet latency average = 555.283
	minimum = 23
	maximum = 2899
Network latency average = 216.596
	minimum = 22
	maximum = 1849
Slowest packet = 18647
Flit latency average = 1832.69
	minimum = 5
	maximum = 3727
Slowest flit = 110213
Fragmentation average = 17.0113
	minimum = 0
	maximum = 291
Injected packet rate average = 0.0316563
	minimum = 0.0035 (at node 171)
	maximum = 0.0555 (at node 25)
Accepted packet rate average = 0.0157474
	minimum = 0.007 (at node 4)
	maximum = 0.0235 (at node 123)
Injected flit rate average = 0.569312
	minimum = 0.063 (at node 171)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.282784
	minimum = 0.1215 (at node 4)
	maximum = 0.4165 (at node 123)
Injected packet length average = 17.9842
Accepted packet length average = 17.9575
Total in-flight flits = 285113 (202821 measured)
latency change    = 0.255221
throughput change = 0.0135649
Class 0:
Packet latency average = 1071.96
	minimum = 23
	maximum = 4684
Network latency average = 745.85
	minimum = 22
	maximum = 2976
Slowest packet = 18647
Flit latency average = 2099.63
	minimum = 5
	maximum = 4535
Slowest flit = 151360
Fragmentation average = 31.3653
	minimum = 0
	maximum = 360
Injected packet rate average = 0.0297517
	minimum = 0.00666667 (at node 93)
	maximum = 0.0513333 (at node 185)
Accepted packet rate average = 0.0156823
	minimum = 0.00933333 (at node 4)
	maximum = 0.0226667 (at node 181)
Injected flit rate average = 0.535257
	minimum = 0.12 (at node 93)
	maximum = 0.921667 (at node 185)
Accepted flit rate average= 0.280722
	minimum = 0.167667 (at node 4)
	maximum = 0.397667 (at node 181)
Injected packet length average = 17.9908
Accepted packet length average = 17.9006
Total in-flight flits = 321916 (279311 measured)
latency change    = 0.481995
throughput change = 0.00734403
Draining remaining packets ...
Class 0:
Remaining flits: 193102 193103 193104 193105 193106 193107 193108 193109 193110 193111 [...] (272815 flits)
Measured flits: 335268 335269 335270 335271 335272 335273 335274 335275 335276 335277 [...] (252733 flits)
Class 0:
Remaining flits: 210020 210021 210022 210023 217566 217567 217568 217569 217570 217571 [...] (223915 flits)
Measured flits: 335286 335287 335288 335289 335290 335291 335292 335293 335294 335295 [...] (214856 flits)
Class 0:
Remaining flits: 217566 217567 217568 217569 217570 217571 217572 217573 217574 217575 [...] (175547 flits)
Measured flits: 335286 335287 335288 335289 335290 335291 335292 335293 335294 335295 [...] (171413 flits)
Class 0:
Remaining flits: 217566 217567 217568 217569 217570 217571 217572 217573 217574 217575 [...] (128221 flits)
Measured flits: 335300 335301 335302 335303 335790 335791 335792 335793 335794 335795 [...] (126425 flits)
Class 0:
Remaining flits: 217566 217567 217568 217569 217570 217571 217572 217573 217574 217575 [...] (81348 flits)
Measured flits: 337122 337123 337124 337125 337126 337127 337128 337129 337130 337131 [...] (80754 flits)
Class 0:
Remaining flits: 291348 291349 291350 291351 291352 291353 291354 291355 291356 291357 [...] (37997 flits)
Measured flits: 337122 337123 337124 337125 337126 337127 337128 337129 337130 337131 [...] (37961 flits)
Class 0:
Remaining flits: 391968 391969 391970 391971 391972 391973 391974 391975 391976 391977 [...] (6918 flits)
Measured flits: 391968 391969 391970 391971 391972 391973 391974 391975 391976 391977 [...] (6918 flits)
Time taken is 13911 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5255.75 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10659 (1 samples)
Network latency average = 4821.53 (1 samples)
	minimum = 22 (1 samples)
	maximum = 9810 (1 samples)
Flit latency average = 4114.2 (1 samples)
	minimum = 5 (1 samples)
	maximum = 9793 (1 samples)
Fragmentation average = 59.6099 (1 samples)
	minimum = 0 (1 samples)
	maximum = 704 (1 samples)
Injected packet rate average = 0.0297517 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0513333 (1 samples)
Accepted packet rate average = 0.0156823 (1 samples)
	minimum = 0.00933333 (1 samples)
	maximum = 0.0226667 (1 samples)
Injected flit rate average = 0.535257 (1 samples)
	minimum = 0.12 (1 samples)
	maximum = 0.921667 (1 samples)
Accepted flit rate average = 0.280722 (1 samples)
	minimum = 0.167667 (1 samples)
	maximum = 0.397667 (1 samples)
Injected packet size average = 17.9908 (1 samples)
Accepted packet size average = 17.9006 (1 samples)
Hops average = 5.14166 (1 samples)
Total run time 13.9785
