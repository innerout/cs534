BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 305.527
	minimum = 27
	maximum = 969
Network latency average = 239.721
	minimum = 23
	maximum = 816
Slowest packet = 156
Flit latency average = 165.897
	minimum = 6
	maximum = 938
Slowest flit = 2860
Fragmentation average = 132.3
	minimum = 0
	maximum = 686
Injected packet rate average = 0.0150208
	minimum = 0 (at node 9)
	maximum = 0.04 (at node 74)
Accepted packet rate average = 0.00908854
	minimum = 0.002 (at node 41)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.266708
	minimum = 0 (at node 9)
	maximum = 0.716 (at node 74)
Accepted flit rate average= 0.182828
	minimum = 0.036 (at node 174)
	maximum = 0.338 (at node 48)
Injected packet length average = 17.7559
Accepted packet length average = 20.1163
Total in-flight flits = 16827 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 464.329
	minimum = 23
	maximum = 1914
Network latency average = 355.867
	minimum = 23
	maximum = 1897
Slowest packet = 158
Flit latency average = 268.884
	minimum = 6
	maximum = 1880
Slowest flit = 2861
Fragmentation average = 163.058
	minimum = 0
	maximum = 1742
Injected packet rate average = 0.0135651
	minimum = 0.001 (at node 103)
	maximum = 0.0275 (at node 74)
Accepted packet rate average = 0.00984115
	minimum = 0.005 (at node 30)
	maximum = 0.0155 (at node 71)
Injected flit rate average = 0.241484
	minimum = 0.013 (at node 103)
	maximum = 0.4885 (at node 118)
Accepted flit rate average= 0.187924
	minimum = 0.1035 (at node 30)
	maximum = 0.287 (at node 71)
Injected packet length average = 17.8019
Accepted packet length average = 19.0958
Total in-flight flits = 21671 (0 measured)
latency change    = 0.342003
throughput change = 0.0271192
Class 0:
Packet latency average = 960.545
	minimum = 32
	maximum = 2734
Network latency average = 629.146
	minimum = 28
	maximum = 2544
Slowest packet = 228
Flit latency average = 515.61
	minimum = 6
	maximum = 2810
Slowest flit = 11309
Fragmentation average = 199.65
	minimum = 0
	maximum = 2052
Injected packet rate average = 0.0123229
	minimum = 0 (at node 4)
	maximum = 0.035 (at node 169)
Accepted packet rate average = 0.0105521
	minimum = 0.004 (at node 54)
	maximum = 0.018 (at node 63)
Injected flit rate average = 0.221885
	minimum = 0 (at node 4)
	maximum = 0.638 (at node 169)
Accepted flit rate average= 0.193297
	minimum = 0.077 (at node 118)
	maximum = 0.318 (at node 56)
Injected packet length average = 18.0059
Accepted packet length average = 18.3184
Total in-flight flits = 27182 (0 measured)
latency change    = 0.516598
throughput change = 0.0277935
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1063.2
	minimum = 27
	maximum = 3578
Network latency average = 341.816
	minimum = 27
	maximum = 938
Slowest packet = 7598
Flit latency average = 629.899
	minimum = 6
	maximum = 3718
Slowest flit = 15137
Fragmentation average = 137.111
	minimum = 0
	maximum = 604
Injected packet rate average = 0.0114635
	minimum = 0 (at node 5)
	maximum = 0.037 (at node 187)
Accepted packet rate average = 0.0109375
	minimum = 0.003 (at node 4)
	maximum = 0.023 (at node 16)
Injected flit rate average = 0.205708
	minimum = 0 (at node 139)
	maximum = 0.668 (at node 187)
Accepted flit rate average= 0.194813
	minimum = 0.063 (at node 4)
	maximum = 0.408 (at node 16)
Injected packet length average = 17.9446
Accepted packet length average = 17.8114
Total in-flight flits = 29504 (23513 measured)
latency change    = 0.0965529
throughput change = 0.00777992
Class 0:
Packet latency average = 1412.5
	minimum = 27
	maximum = 4700
Network latency average = 555.426
	minimum = 26
	maximum = 1925
Slowest packet = 7598
Flit latency average = 671.09
	minimum = 6
	maximum = 4518
Slowest flit = 26785
Fragmentation average = 164.726
	minimum = 0
	maximum = 1277
Injected packet rate average = 0.0114687
	minimum = 0 (at node 68)
	maximum = 0.0235 (at node 142)
Accepted packet rate average = 0.0108437
	minimum = 0.0055 (at node 4)
	maximum = 0.02 (at node 16)
Injected flit rate average = 0.20612
	minimum = 0.0045 (at node 68)
	maximum = 0.431 (at node 142)
Accepted flit rate average= 0.194242
	minimum = 0.099 (at node 4)
	maximum = 0.3655 (at node 16)
Injected packet length average = 17.9723
Accepted packet length average = 17.9128
Total in-flight flits = 32009 (30442 measured)
latency change    = 0.247292
throughput change = 0.00293609
Class 0:
Packet latency average = 1706.14
	minimum = 27
	maximum = 5662
Network latency average = 666.641
	minimum = 26
	maximum = 2925
Slowest packet = 7598
Flit latency average = 698.875
	minimum = 6
	maximum = 5139
Slowest flit = 29393
Fragmentation average = 177.73
	minimum = 0
	maximum = 2162
Injected packet rate average = 0.0113507
	minimum = 0.002 (at node 177)
	maximum = 0.022 (at node 61)
Accepted packet rate average = 0.0108056
	minimum = 0.005 (at node 4)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.203915
	minimum = 0.0356667 (at node 177)
	maximum = 0.395333 (at node 61)
Accepted flit rate average= 0.194182
	minimum = 0.103 (at node 4)
	maximum = 0.290667 (at node 16)
Injected packet length average = 17.965
Accepted packet length average = 17.9706
Total in-flight flits = 33107 (32585 measured)
latency change    = 0.172107
throughput change = 0.000308452
Class 0:
Packet latency average = 1922.24
	minimum = 27
	maximum = 5860
Network latency average = 754.117
	minimum = 25
	maximum = 3693
Slowest packet = 7598
Flit latency average = 731.114
	minimum = 6
	maximum = 5139
Slowest flit = 29393
Fragmentation average = 188.38
	minimum = 0
	maximum = 2582
Injected packet rate average = 0.0113086
	minimum = 0.00325 (at node 24)
	maximum = 0.01825 (at node 185)
Accepted packet rate average = 0.0108255
	minimum = 0.00675 (at node 4)
	maximum = 0.01475 (at node 16)
Injected flit rate average = 0.203249
	minimum = 0.05575 (at node 78)
	maximum = 0.3265 (at node 185)
Accepted flit rate average= 0.194457
	minimum = 0.127 (at node 4)
	maximum = 0.26825 (at node 16)
Injected packet length average = 17.9729
Accepted packet length average = 17.9628
Total in-flight flits = 34421 (34223 measured)
latency change    = 0.112421
throughput change = 0.00141285
Class 0:
Packet latency average = 2142.06
	minimum = 27
	maximum = 6881
Network latency average = 802.666
	minimum = 23
	maximum = 4686
Slowest packet = 7598
Flit latency average = 753.829
	minimum = 6
	maximum = 6275
Slowest flit = 46655
Fragmentation average = 192.226
	minimum = 0
	maximum = 2723
Injected packet rate average = 0.0112302
	minimum = 0.0042 (at node 24)
	maximum = 0.018 (at node 185)
Accepted packet rate average = 0.0108354
	minimum = 0.0076 (at node 36)
	maximum = 0.0146 (at node 129)
Injected flit rate average = 0.201796
	minimum = 0.077 (at node 24)
	maximum = 0.3252 (at node 185)
Accepted flit rate average= 0.194542
	minimum = 0.1338 (at node 36)
	maximum = 0.2626 (at node 129)
Injected packet length average = 17.969
Accepted packet length average = 17.9542
Total in-flight flits = 34732 (34731 measured)
latency change    = 0.102622
throughput change = 0.00043505
Class 0:
Packet latency average = 2362.13
	minimum = 27
	maximum = 7620
Network latency average = 848.941
	minimum = 23
	maximum = 5301
Slowest packet = 7598
Flit latency average = 776.779
	minimum = 6
	maximum = 6275
Slowest flit = 46655
Fragmentation average = 196.773
	minimum = 0
	maximum = 4325
Injected packet rate average = 0.0111345
	minimum = 0.00483333 (at node 180)
	maximum = 0.0176667 (at node 187)
Accepted packet rate average = 0.0107891
	minimum = 0.00783333 (at node 36)
	maximum = 0.014 (at node 128)
Injected flit rate average = 0.200234
	minimum = 0.0851667 (at node 180)
	maximum = 0.318333 (at node 187)
Accepted flit rate average= 0.193628
	minimum = 0.1415 (at node 36)
	maximum = 0.248333 (at node 129)
Injected packet length average = 17.9832
Accepted packet length average = 17.9467
Total in-flight flits = 35404 (35404 measured)
latency change    = 0.0931678
throughput change = 0.00471622
Class 0:
Packet latency average = 2559.04
	minimum = 27
	maximum = 8697
Network latency average = 880.378
	minimum = 23
	maximum = 6143
Slowest packet = 7598
Flit latency average = 794.189
	minimum = 6
	maximum = 6275
Slowest flit = 46655
Fragmentation average = 198.914
	minimum = 0
	maximum = 4325
Injected packet rate average = 0.0110997
	minimum = 0.00471429 (at node 180)
	maximum = 0.0164286 (at node 39)
Accepted packet rate average = 0.0107649
	minimum = 0.00814286 (at node 89)
	maximum = 0.0141429 (at node 128)
Injected flit rate average = 0.199494
	minimum = 0.0832857 (at node 180)
	maximum = 0.295429 (at node 39)
Accepted flit rate average= 0.193163
	minimum = 0.145286 (at node 89)
	maximum = 0.251143 (at node 128)
Injected packet length average = 17.9729
Accepted packet length average = 17.9438
Total in-flight flits = 36365 (36365 measured)
latency change    = 0.0769468
throughput change = 0.00241002
Draining all recorded packets ...
Class 0:
Remaining flits: 237060 237061 237062 237063 237064 237065 237066 237067 237068 237069 [...] (37087 flits)
Measured flits: 237060 237061 237062 237063 237064 237065 237066 237067 237068 237069 [...] (35348 flits)
Class 0:
Remaining flits: 237074 237075 237076 237077 255603 255604 255605 255606 255607 255608 [...] (36098 flits)
Measured flits: 237074 237075 237076 237077 255603 255604 255605 255606 255607 255608 [...] (31997 flits)
Class 0:
Remaining flits: 255615 255616 255617 278442 278443 278444 278445 278446 278447 278448 [...] (35824 flits)
Measured flits: 255615 255616 255617 278442 278443 278444 278445 278446 278447 278448 [...] (29219 flits)
Class 0:
Remaining flits: 278442 278443 278444 278445 278446 278447 278448 278449 278450 278451 [...] (33721 flits)
Measured flits: 278442 278443 278444 278445 278446 278447 278448 278449 278450 278451 [...] (22714 flits)
Class 0:
Remaining flits: 341154 341155 341156 341157 341158 341159 341160 341161 341162 341163 [...] (35343 flits)
Measured flits: 341154 341155 341156 341157 341158 341159 341160 341161 341162 341163 [...] (17903 flits)
Class 0:
Remaining flits: 341167 341168 341169 341170 341171 394974 394975 394976 394977 394978 [...] (35951 flits)
Measured flits: 341167 341168 341169 341170 341171 394974 394975 394976 394977 394978 [...] (16038 flits)
Class 0:
Remaining flits: 430330 430331 430332 430333 430334 430335 430336 430337 430338 430339 [...] (36809 flits)
Measured flits: 430330 430331 430332 430333 430334 430335 430336 430337 430338 430339 [...] (14168 flits)
Class 0:
Remaining flits: 430343 442188 442189 442190 442191 442192 442193 442194 442195 442196 [...] (35648 flits)
Measured flits: 430343 442188 442189 442190 442191 442192 442193 442194 442195 442196 [...] (11068 flits)
Class 0:
Remaining flits: 442188 442189 442190 442191 442192 442193 442194 442195 442196 442197 [...] (35261 flits)
Measured flits: 442188 442189 442190 442191 442192 442193 442194 442195 442196 442197 [...] (8357 flits)
Class 0:
Remaining flits: 456782 456783 456784 456785 559849 559850 559851 559852 559853 562230 [...] (35439 flits)
Measured flits: 456782 456783 456784 456785 559849 559850 559851 559852 559853 562230 [...] (6433 flits)
Class 0:
Remaining flits: 562231 562232 562233 562234 562235 562236 562237 562238 562239 562240 [...] (36912 flits)
Measured flits: 562231 562232 562233 562234 562235 562236 562237 562238 562239 562240 [...] (5792 flits)
Class 0:
Remaining flits: 570438 570439 570440 570441 570442 570443 570444 570445 570446 570447 [...] (35981 flits)
Measured flits: 570438 570439 570440 570441 570442 570443 570444 570445 570446 570447 [...] (4990 flits)
Class 0:
Remaining flits: 572992 572993 573624 573625 573626 573627 573628 573629 573630 573631 [...] (35038 flits)
Measured flits: 572992 572993 573624 573625 573626 573627 573628 573629 573630 573631 [...] (4065 flits)
Class 0:
Remaining flits: 573624 573625 573626 573627 573628 573629 573630 573631 573632 573633 [...] (34604 flits)
Measured flits: 573624 573625 573626 573627 573628 573629 573630 573631 573632 573633 [...] (3295 flits)
Class 0:
Remaining flits: 573633 573634 573635 573636 573637 573638 573639 573640 573641 715764 [...] (36394 flits)
Measured flits: 573633 573634 573635 573636 573637 573638 573639 573640 573641 759974 [...] (2210 flits)
Class 0:
Remaining flits: 757678 757679 757680 757681 757682 757683 757684 757685 757686 757687 [...] (38176 flits)
Measured flits: 810918 810919 810920 810921 810922 810923 810924 810925 810926 810927 [...] (1124 flits)
Class 0:
Remaining flits: 757684 757685 757686 757687 757688 757689 757690 757691 762822 762823 [...] (36035 flits)
Measured flits: 810918 810919 810920 810921 810922 810923 810924 810925 810926 810927 [...] (856 flits)
Class 0:
Remaining flits: 762822 762823 762824 762825 762826 762827 762828 762829 762830 762831 [...] (36711 flits)
Measured flits: 879048 879049 879050 879051 879052 879053 879054 879055 879056 879057 [...] (591 flits)
Class 0:
Remaining flits: 874152 874153 874154 874155 874156 874157 874158 874159 874160 874161 [...] (36432 flits)
Measured flits: 892962 892963 892964 892965 892966 892967 892968 892969 892970 892971 [...] (678 flits)
Class 0:
Remaining flits: 887875 887876 887877 887878 887879 887880 887881 887882 887883 887884 [...] (38079 flits)
Measured flits: 898074 898075 898076 898077 898078 898079 898080 898081 898082 898083 [...] (611 flits)
Class 0:
Remaining flits: 910206 910207 910208 910209 910210 910211 910212 910213 910214 910215 [...] (36952 flits)
Measured flits: 1002258 1002259 1002260 1002261 1002262 1002263 1002264 1002265 1002266 1002267 [...] (357 flits)
Class 0:
Remaining flits: 910217 910218 910219 910220 910221 910222 910223 920646 920647 920648 [...] (36494 flits)
Measured flits: 1002258 1002259 1002260 1002261 1002262 1002263 1002264 1002265 1002266 1002267 [...] (369 flits)
Class 0:
Remaining flits: 910219 910220 910221 910222 910223 920646 920647 920648 920649 920650 [...] (35548 flits)
Measured flits: 1032374 1032375 1032376 1032377 1032378 1032379 1032380 1032381 1032382 1032383 [...] (349 flits)
Class 0:
Remaining flits: 1004130 1004131 1004132 1004133 1004134 1004135 1004136 1004137 1004138 1004139 [...] (36829 flits)
Measured flits: 1191402 1191403 1191404 1191405 1191406 1191407 1191408 1191409 1191410 1191411 [...] (215 flits)
Class 0:
Remaining flits: 1004130 1004131 1004132 1004133 1004134 1004135 1004136 1004137 1004138 1004139 [...] (36140 flits)
Measured flits: 1252797 1252798 1252799 1261746 1261747 1261748 1261749 1261750 1261751 1261752 [...] (116 flits)
Class 0:
Remaining flits: 1004130 1004131 1004132 1004133 1004134 1004135 1004136 1004137 1004138 1004139 [...] (36391 flits)
Measured flits: 1312369 1312370 1312371 1312372 1312373 1312374 1312375 1312376 1312377 1312378 [...] (11 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1004130 1004131 1004132 1004133 1004134 1004135 1004136 1004137 1004138 1004139 [...] (6945 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1098036 1098037 1098038 1098039 1098040 1098041 1098042 1098043 1098044 1098045 [...] (427 flits)
Measured flits: (0 flits)
Time taken is 38735 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5205.89 (1 samples)
	minimum = 27 (1 samples)
	maximum = 26908 (1 samples)
Network latency average = 1026.95 (1 samples)
	minimum = 23 (1 samples)
	maximum = 10509 (1 samples)
Flit latency average = 921.232 (1 samples)
	minimum = 6 (1 samples)
	maximum = 12134 (1 samples)
Fragmentation average = 210.072 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4677 (1 samples)
Injected packet rate average = 0.0110997 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0164286 (1 samples)
Accepted packet rate average = 0.0107649 (1 samples)
	minimum = 0.00814286 (1 samples)
	maximum = 0.0141429 (1 samples)
Injected flit rate average = 0.199494 (1 samples)
	minimum = 0.0832857 (1 samples)
	maximum = 0.295429 (1 samples)
Accepted flit rate average = 0.193163 (1 samples)
	minimum = 0.145286 (1 samples)
	maximum = 0.251143 (1 samples)
Injected packet size average = 17.9729 (1 samples)
Accepted packet size average = 17.9438 (1 samples)
Hops average = 5.05721 (1 samples)
Total run time 41.2678
