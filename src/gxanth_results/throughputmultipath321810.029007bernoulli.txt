BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 315.869
	minimum = 23
	maximum = 967
Network latency average = 298.421
	minimum = 23
	maximum = 967
Slowest packet = 125
Flit latency average = 230.914
	minimum = 6
	maximum = 950
Slowest flit = 2267
Fragmentation average = 157.201
	minimum = 0
	maximum = 873
Injected packet rate average = 0.021401
	minimum = 0.008 (at node 36)
	maximum = 0.032 (at node 114)
Accepted packet rate average = 0.00986458
	minimum = 0.004 (at node 9)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.37799
	minimum = 0.14 (at node 36)
	maximum = 0.559 (at node 114)
Accepted flit rate average= 0.205187
	minimum = 0.085 (at node 153)
	maximum = 0.343 (at node 44)
Injected packet length average = 17.6622
Accepted packet length average = 20.8004
Total in-flight flits = 34710 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 612.779
	minimum = 23
	maximum = 1904
Network latency average = 523.924
	minimum = 23
	maximum = 1879
Slowest packet = 451
Flit latency average = 426.618
	minimum = 6
	maximum = 1898
Slowest flit = 7806
Fragmentation average = 193.09
	minimum = 0
	maximum = 1692
Injected packet rate average = 0.0163932
	minimum = 0.004 (at node 36)
	maximum = 0.0285 (at node 22)
Accepted packet rate average = 0.010513
	minimum = 0.0045 (at node 153)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.291591
	minimum = 0.0715 (at node 36)
	maximum = 0.5085 (at node 22)
Accepted flit rate average= 0.201909
	minimum = 0.0925 (at node 153)
	maximum = 0.3075 (at node 22)
Injected packet length average = 17.7873
Accepted packet length average = 19.2056
Total in-flight flits = 36101 (0 measured)
latency change    = 0.48453
throughput change = 0.0162382
Class 0:
Packet latency average = 1491.1
	minimum = 54
	maximum = 2808
Network latency average = 941.149
	minimum = 27
	maximum = 2808
Slowest packet = 435
Flit latency average = 812.952
	minimum = 6
	maximum = 2885
Slowest flit = 9490
Fragmentation average = 219.274
	minimum = 2
	maximum = 2280
Injected packet rate average = 0.0110208
	minimum = 0 (at node 5)
	maximum = 0.035 (at node 101)
Accepted packet rate average = 0.011
	minimum = 0.003 (at node 40)
	maximum = 0.019 (at node 142)
Injected flit rate average = 0.198417
	minimum = 0 (at node 19)
	maximum = 0.643 (at node 101)
Accepted flit rate average= 0.195109
	minimum = 0.059 (at node 40)
	maximum = 0.332 (at node 142)
Injected packet length average = 18.0038
Accepted packet length average = 17.7372
Total in-flight flits = 36818 (0 measured)
latency change    = 0.589042
throughput change = 0.0348496
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2005.52
	minimum = 588
	maximum = 3227
Network latency average = 355.599
	minimum = 27
	maximum = 972
Slowest packet = 8441
Flit latency average = 904.971
	minimum = 6
	maximum = 3743
Slowest flit = 5777
Fragmentation average = 128.025
	minimum = 0
	maximum = 584
Injected packet rate average = 0.0108958
	minimum = 0 (at node 65)
	maximum = 0.033 (at node 150)
Accepted packet rate average = 0.010849
	minimum = 0.002 (at node 113)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.196406
	minimum = 0 (at node 88)
	maximum = 0.608 (at node 150)
Accepted flit rate average= 0.194495
	minimum = 0.07 (at node 190)
	maximum = 0.38 (at node 16)
Injected packet length average = 18.0258
Accepted packet length average = 17.9275
Total in-flight flits = 37095 (25942 measured)
latency change    = 0.256502
throughput change = 0.0031599
Class 0:
Packet latency average = 2476.48
	minimum = 588
	maximum = 4189
Network latency average = 620.007
	minimum = 27
	maximum = 1950
Slowest packet = 8441
Flit latency average = 913.228
	minimum = 6
	maximum = 4916
Slowest flit = 7005
Fragmentation average = 158.284
	minimum = 0
	maximum = 1221
Injected packet rate average = 0.0107422
	minimum = 0 (at node 77)
	maximum = 0.0235 (at node 181)
Accepted packet rate average = 0.0107865
	minimum = 0.0045 (at node 4)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.193198
	minimum = 0 (at node 88)
	maximum = 0.4235 (at node 181)
Accepted flit rate average= 0.194378
	minimum = 0.087 (at node 4)
	maximum = 0.3265 (at node 16)
Injected packet length average = 17.985
Accepted packet length average = 18.0205
Total in-flight flits = 36355 (33131 measured)
latency change    = 0.190174
throughput change = 0.000602886
Class 0:
Packet latency average = 2880
	minimum = 588
	maximum = 5245
Network latency average = 752.235
	minimum = 23
	maximum = 2854
Slowest packet = 8441
Flit latency average = 915.841
	minimum = 6
	maximum = 5503
Slowest flit = 43360
Fragmentation average = 176.129
	minimum = 0
	maximum = 1610
Injected packet rate average = 0.0109896
	minimum = 0 (at node 88)
	maximum = 0.022 (at node 19)
Accepted packet rate average = 0.0108767
	minimum = 0.00566667 (at node 113)
	maximum = 0.016 (at node 16)
Injected flit rate average = 0.197733
	minimum = 0 (at node 88)
	maximum = 0.395333 (at node 19)
Accepted flit rate average= 0.195998
	minimum = 0.111 (at node 113)
	maximum = 0.283 (at node 16)
Injected packet length average = 17.9927
Accepted packet length average = 18.02
Total in-flight flits = 37827 (36953 measured)
latency change    = 0.140112
throughput change = 0.00826875
Draining remaining packets ...
Class 0:
Remaining flits: 18596 18597 18598 18599 18600 18601 18602 18603 18604 18605 [...] (6223 flits)
Measured flits: 153659 153660 153661 153662 153663 153664 153665 155195 160636 160637 [...] (6033 flits)
Time taken is 7614 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3324.68 (1 samples)
	minimum = 588 (1 samples)
	maximum = 6296 (1 samples)
Network latency average = 985.332 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4075 (1 samples)
Flit latency average = 1025.79 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6863 (1 samples)
Fragmentation average = 178.575 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2672 (1 samples)
Injected packet rate average = 0.0109896 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0108767 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.016 (1 samples)
Injected flit rate average = 0.197733 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.395333 (1 samples)
Accepted flit rate average = 0.195998 (1 samples)
	minimum = 0.111 (1 samples)
	maximum = 0.283 (1 samples)
Injected packet size average = 17.9927 (1 samples)
Accepted packet size average = 18.02 (1 samples)
Hops average = 5.13085 (1 samples)
Total run time 7.71581
