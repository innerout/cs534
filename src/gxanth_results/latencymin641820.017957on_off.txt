BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 227.687
	minimum = 25
	maximum = 884
Network latency average = 161.503
	minimum = 22
	maximum = 719
Slowest packet = 64
Flit latency average = 131.819
	minimum = 5
	maximum = 708
Slowest flit = 16934
Fragmentation average = 32.0008
	minimum = 0
	maximum = 285
Injected packet rate average = 0.0172604
	minimum = 0 (at node 71)
	maximum = 0.052 (at node 62)
Accepted packet rate average = 0.0124115
	minimum = 0.006 (at node 115)
	maximum = 0.021 (at node 131)
Injected flit rate average = 0.308042
	minimum = 0 (at node 71)
	maximum = 0.922 (at node 62)
Accepted flit rate average= 0.230917
	minimum = 0.108 (at node 115)
	maximum = 0.39 (at node 44)
Injected packet length average = 17.8467
Accepted packet length average = 18.6051
Total in-flight flits = 15316 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 333.033
	minimum = 25
	maximum = 1566
Network latency average = 254.937
	minimum = 22
	maximum = 1282
Slowest packet = 64
Flit latency average = 219.414
	minimum = 5
	maximum = 1265
Slowest flit = 42083
Fragmentation average = 43.264
	minimum = 0
	maximum = 490
Injected packet rate average = 0.0171172
	minimum = 0.0025 (at node 2)
	maximum = 0.041 (at node 178)
Accepted packet rate average = 0.0131589
	minimum = 0.006 (at node 116)
	maximum = 0.019 (at node 166)
Injected flit rate average = 0.306859
	minimum = 0.038 (at node 146)
	maximum = 0.731 (at node 178)
Accepted flit rate average= 0.241914
	minimum = 0.116 (at node 116)
	maximum = 0.3485 (at node 177)
Injected packet length average = 17.927
Accepted packet length average = 18.3841
Total in-flight flits = 25509 (0 measured)
latency change    = 0.316325
throughput change = 0.0454599
Class 0:
Packet latency average = 608.035
	minimum = 25
	maximum = 2172
Network latency average = 500.505
	minimum = 22
	maximum = 1770
Slowest packet = 3172
Flit latency average = 461.141
	minimum = 5
	maximum = 1841
Slowest flit = 67047
Fragmentation average = 62.7248
	minimum = 0
	maximum = 687
Injected packet rate average = 0.0171406
	minimum = 0 (at node 23)
	maximum = 0.053 (at node 50)
Accepted packet rate average = 0.0143073
	minimum = 0.006 (at node 184)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.308344
	minimum = 0 (at node 23)
	maximum = 0.967 (at node 50)
Accepted flit rate average= 0.259099
	minimum = 0.121 (at node 184)
	maximum = 0.486 (at node 16)
Injected packet length average = 17.9891
Accepted packet length average = 18.1096
Total in-flight flits = 34928 (0 measured)
latency change    = 0.452279
throughput change = 0.0663256
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 468.45
	minimum = 25
	maximum = 1993
Network latency average = 365.132
	minimum = 22
	maximum = 982
Slowest packet = 9881
Flit latency average = 629.829
	minimum = 5
	maximum = 2149
Slowest flit = 86651
Fragmentation average = 48.0643
	minimum = 0
	maximum = 324
Injected packet rate average = 0.0175052
	minimum = 0 (at node 34)
	maximum = 0.056 (at node 167)
Accepted packet rate average = 0.0147396
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.31425
	minimum = 0 (at node 34)
	maximum = 1 (at node 167)
Accepted flit rate average= 0.266578
	minimum = 0.118 (at node 132)
	maximum = 0.424 (at node 19)
Injected packet length average = 17.9518
Accepted packet length average = 18.0859
Total in-flight flits = 44297 (39878 measured)
latency change    = 0.29797
throughput change = 0.0280562
Class 0:
Packet latency average = 758.379
	minimum = 25
	maximum = 2392
Network latency average = 628.289
	minimum = 22
	maximum = 1855
Slowest packet = 9881
Flit latency average = 696.958
	minimum = 5
	maximum = 2559
Slowest flit = 133685
Fragmentation average = 65.0158
	minimum = 0
	maximum = 421
Injected packet rate average = 0.0166875
	minimum = 0.003 (at node 71)
	maximum = 0.0385 (at node 167)
Accepted packet rate average = 0.0147318
	minimum = 0.008 (at node 4)
	maximum = 0.0215 (at node 129)
Injected flit rate average = 0.30032
	minimum = 0.054 (at node 71)
	maximum = 0.6965 (at node 167)
Accepted flit rate average= 0.266047
	minimum = 0.154 (at node 4)
	maximum = 0.3845 (at node 129)
Injected packet length average = 17.9967
Accepted packet length average = 18.0594
Total in-flight flits = 48290 (48083 measured)
latency change    = 0.382301
throughput change = 0.00199683
Class 0:
Packet latency average = 903.408
	minimum = 23
	maximum = 3384
Network latency average = 750.287
	minimum = 22
	maximum = 2475
Slowest packet = 9881
Flit latency average = 751.309
	minimum = 5
	maximum = 2783
Slowest flit = 137203
Fragmentation average = 71.1703
	minimum = 0
	maximum = 570
Injected packet rate average = 0.0163229
	minimum = 0.00433333 (at node 5)
	maximum = 0.0336667 (at node 106)
Accepted packet rate average = 0.014717
	minimum = 0.01 (at node 64)
	maximum = 0.0196667 (at node 128)
Injected flit rate average = 0.293531
	minimum = 0.078 (at node 5)
	maximum = 0.605333 (at node 106)
Accepted flit rate average= 0.265359
	minimum = 0.177333 (at node 64)
	maximum = 0.356 (at node 128)
Injected packet length average = 17.9828
Accepted packet length average = 18.0308
Total in-flight flits = 51497 (51497 measured)
latency change    = 0.160535
throughput change = 0.00259083
Class 0:
Packet latency average = 1002.2
	minimum = 23
	maximum = 3796
Network latency average = 825.181
	minimum = 22
	maximum = 3018
Slowest packet = 9881
Flit latency average = 797.962
	minimum = 5
	maximum = 3309
Slowest flit = 218739
Fragmentation average = 74.4568
	minimum = 0
	maximum = 570
Injected packet rate average = 0.0163464
	minimum = 0.005 (at node 5)
	maximum = 0.031 (at node 106)
Accepted packet rate average = 0.0147044
	minimum = 0.01 (at node 2)
	maximum = 0.0185 (at node 68)
Injected flit rate average = 0.294095
	minimum = 0.09 (at node 5)
	maximum = 0.558 (at node 106)
Accepted flit rate average= 0.265361
	minimum = 0.1825 (at node 2)
	maximum = 0.33325 (at node 128)
Injected packet length average = 17.9915
Accepted packet length average = 18.0463
Total in-flight flits = 57373 (57373 measured)
latency change    = 0.098572
throughput change = 4.90684e-06
Class 0:
Packet latency average = 1080.74
	minimum = 22
	maximum = 4871
Network latency average = 881.463
	minimum = 22
	maximum = 3642
Slowest packet = 9881
Flit latency average = 838.836
	minimum = 5
	maximum = 3679
Slowest flit = 251502
Fragmentation average = 77.168
	minimum = 0
	maximum = 570
Injected packet rate average = 0.016325
	minimum = 0.0064 (at node 41)
	maximum = 0.0282 (at node 3)
Accepted packet rate average = 0.014701
	minimum = 0.0108 (at node 64)
	maximum = 0.0192 (at node 68)
Injected flit rate average = 0.293618
	minimum = 0.1152 (at node 70)
	maximum = 0.5076 (at node 3)
Accepted flit rate average= 0.265268
	minimum = 0.1944 (at node 42)
	maximum = 0.347 (at node 68)
Injected packet length average = 17.9858
Accepted packet length average = 18.0441
Total in-flight flits = 62619 (62619 measured)
latency change    = 0.0726723
throughput change = 0.000350471
Class 0:
Packet latency average = 1158.81
	minimum = 22
	maximum = 5814
Network latency average = 936.244
	minimum = 22
	maximum = 3764
Slowest packet = 9881
Flit latency average = 884.545
	minimum = 5
	maximum = 3679
Slowest flit = 251502
Fragmentation average = 80.0664
	minimum = 0
	maximum = 570
Injected packet rate average = 0.0164436
	minimum = 0.00666667 (at node 41)
	maximum = 0.0271667 (at node 3)
Accepted packet rate average = 0.0147179
	minimum = 0.0113333 (at node 42)
	maximum = 0.0185 (at node 138)
Injected flit rate average = 0.295871
	minimum = 0.121 (at node 41)
	maximum = 0.486333 (at node 3)
Accepted flit rate average= 0.265829
	minimum = 0.206333 (at node 42)
	maximum = 0.333333 (at node 138)
Injected packet length average = 17.9931
Accepted packet length average = 18.0616
Total in-flight flits = 70045 (70045 measured)
latency change    = 0.0673761
throughput change = 0.00211145
Class 0:
Packet latency average = 1247.78
	minimum = 22
	maximum = 6369
Network latency average = 993.6
	minimum = 22
	maximum = 3775
Slowest packet = 9881
Flit latency average = 934.09
	minimum = 5
	maximum = 3758
Slowest flit = 345347
Fragmentation average = 82.6712
	minimum = 0
	maximum = 570
Injected packet rate average = 0.0164829
	minimum = 0.00628571 (at node 41)
	maximum = 0.0252857 (at node 106)
Accepted packet rate average = 0.014782
	minimum = 0.0115714 (at node 42)
	maximum = 0.0187143 (at node 70)
Injected flit rate average = 0.296509
	minimum = 0.114 (at node 41)
	maximum = 0.454571 (at node 106)
Accepted flit rate average= 0.266856
	minimum = 0.207857 (at node 42)
	maximum = 0.338143 (at node 70)
Injected packet length average = 17.9889
Accepted packet length average = 18.0528
Total in-flight flits = 75262 (75262 measured)
latency change    = 0.071304
throughput change = 0.00384725
Draining all recorded packets ...
Class 0:
Remaining flits: 407844 407845 407846 407847 407848 407849 407850 407851 407852 407853 [...] (81503 flits)
Measured flits: 407844 407845 407846 407847 407848 407849 407850 407851 407852 407853 [...] (51170 flits)
Class 0:
Remaining flits: 468666 468667 468668 468669 468670 468671 468672 468673 468674 468675 [...] (80721 flits)
Measured flits: 468666 468667 468668 468669 468670 468671 468672 468673 468674 468675 [...] (25901 flits)
Class 0:
Remaining flits: 512514 512515 512516 512517 512518 512519 512520 512521 512522 512523 [...] (85859 flits)
Measured flits: 512514 512515 512516 512517 512518 512519 512520 512521 512522 512523 [...] (13310 flits)
Class 0:
Remaining flits: 519630 519631 519632 519633 519634 519635 519636 519637 519638 519639 [...] (84993 flits)
Measured flits: 519630 519631 519632 519633 519634 519635 519636 519637 519638 519639 [...] (6978 flits)
Class 0:
Remaining flits: 570492 570493 570494 570495 570496 570497 570498 570499 570500 570501 [...] (88754 flits)
Measured flits: 570492 570493 570494 570495 570496 570497 570498 570499 570500 570501 [...] (3657 flits)
Class 0:
Remaining flits: 612395 633996 633997 633998 633999 634000 634001 634002 634003 634004 [...] (88843 flits)
Measured flits: 612395 672228 672229 672230 672231 672232 672233 672234 672235 672236 [...] (2446 flits)
Class 0:
Remaining flits: 717372 717373 717374 717375 717376 717377 717378 717379 717380 717381 [...] (89879 flits)
Measured flits: 787680 787681 787682 787683 787684 787685 787686 787687 787688 787689 [...] (1137 flits)
Class 0:
Remaining flits: 761397 761398 761399 769608 769609 769610 769611 769612 769613 769614 [...] (89502 flits)
Measured flits: 916452 916453 916454 916455 916456 916457 916458 916459 916460 916461 [...] (372 flits)
Class 0:
Remaining flits: 770580 770581 770582 770583 770584 770585 770586 770587 770588 770589 [...] (91495 flits)
Measured flits: 936378 936379 936380 936381 936382 936383 936384 936385 936386 936387 [...] (126 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 920870 920871 920872 920873 920874 920875 920876 920877 920878 920879 [...] (45795 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 923010 923011 923012 923013 923014 923015 923016 923017 923018 923019 [...] (6831 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 989802 989803 989804 989805 989806 989807 989808 989809 989810 989811 [...] (216 flits)
Measured flits: (0 flits)
Time taken is 22760 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1758.62 (1 samples)
	minimum = 22 (1 samples)
	maximum = 11633 (1 samples)
Network latency average = 1203.68 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5829 (1 samples)
Flit latency average = 1376.06 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6219 (1 samples)
Fragmentation average = 91.5435 (1 samples)
	minimum = 0 (1 samples)
	maximum = 570 (1 samples)
Injected packet rate average = 0.0164829 (1 samples)
	minimum = 0.00628571 (1 samples)
	maximum = 0.0252857 (1 samples)
Accepted packet rate average = 0.014782 (1 samples)
	minimum = 0.0115714 (1 samples)
	maximum = 0.0187143 (1 samples)
Injected flit rate average = 0.296509 (1 samples)
	minimum = 0.114 (1 samples)
	maximum = 0.454571 (1 samples)
Accepted flit rate average = 0.266856 (1 samples)
	minimum = 0.207857 (1 samples)
	maximum = 0.338143 (1 samples)
Injected packet size average = 17.9889 (1 samples)
Accepted packet size average = 18.0528 (1 samples)
Hops average = 5.06139 (1 samples)
Total run time 32.0342
