BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 298.998
	minimum = 27
	maximum = 965
Network latency average = 239.995
	minimum = 23
	maximum = 939
Slowest packet = 226
Flit latency average = 165.491
	minimum = 6
	maximum = 947
Slowest flit = 2553
Fragmentation average = 140.32
	minimum = 0
	maximum = 894
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.00920833
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 76)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.189839
	minimum = 0.09 (at node 120)
	maximum = 0.344 (at node 76)
Injected packet length average = 17.8264
Accepted packet length average = 20.616
Total in-flight flits = 25561 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 453.447
	minimum = 24
	maximum = 1894
Network latency average = 382.223
	minimum = 23
	maximum = 1824
Slowest packet = 340
Flit latency average = 292.051
	minimum = 6
	maximum = 1807
Slowest flit = 7343
Fragmentation average = 191.42
	minimum = 0
	maximum = 1370
Injected packet rate average = 0.0173021
	minimum = 0.0005 (at node 169)
	maximum = 0.04 (at node 41)
Accepted packet rate average = 0.0103724
	minimum = 0.005 (at node 30)
	maximum = 0.0175 (at node 152)
Injected flit rate average = 0.310135
	minimum = 0.009 (at node 169)
	maximum = 0.72 (at node 41)
Accepted flit rate average= 0.202909
	minimum = 0.101 (at node 30)
	maximum = 0.3165 (at node 152)
Injected packet length average = 17.9247
Accepted packet length average = 19.5624
Total in-flight flits = 41675 (0 measured)
latency change    = 0.34061
throughput change = 0.0644147
Class 0:
Packet latency average = 836.554
	minimum = 27
	maximum = 2833
Network latency average = 742.083
	minimum = 23
	maximum = 2586
Slowest packet = 722
Flit latency average = 630.223
	minimum = 6
	maximum = 2778
Slowest flit = 12810
Fragmentation average = 272.584
	minimum = 0
	maximum = 2193
Injected packet rate average = 0.0180469
	minimum = 0 (at node 167)
	maximum = 0.056 (at node 74)
Accepted packet rate average = 0.0123333
	minimum = 0.004 (at node 180)
	maximum = 0.02 (at node 51)
Injected flit rate average = 0.32462
	minimum = 0 (at node 167)
	maximum = 1 (at node 74)
Accepted flit rate average= 0.227422
	minimum = 0.078 (at node 180)
	maximum = 0.364 (at node 50)
Injected packet length average = 17.9876
Accepted packet length average = 18.4396
Total in-flight flits = 60380 (0 measured)
latency change    = 0.457959
throughput change = 0.107787
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 391.363
	minimum = 27
	maximum = 1462
Network latency average = 303.224
	minimum = 23
	maximum = 991
Slowest packet = 10126
Flit latency average = 833.872
	minimum = 6
	maximum = 3749
Slowest flit = 14242
Fragmentation average = 159.497
	minimum = 0
	maximum = 658
Injected packet rate average = 0.0179219
	minimum = 0 (at node 120)
	maximum = 0.055 (at node 23)
Accepted packet rate average = 0.0126823
	minimum = 0.005 (at node 36)
	maximum = 0.022 (at node 123)
Injected flit rate average = 0.322922
	minimum = 0 (at node 120)
	maximum = 1 (at node 23)
Accepted flit rate average= 0.231026
	minimum = 0.085 (at node 36)
	maximum = 0.393 (at node 123)
Injected packet length average = 18.0183
Accepted packet length average = 18.2164
Total in-flight flits = 77961 (44917 measured)
latency change    = 1.13754
throughput change = 0.0156007
Class 0:
Packet latency average = 640.514
	minimum = 26
	maximum = 2509
Network latency average = 555.982
	minimum = 23
	maximum = 1959
Slowest packet = 10126
Flit latency average = 962.334
	minimum = 6
	maximum = 4727
Slowest flit = 16248
Fragmentation average = 212.759
	minimum = 0
	maximum = 1748
Injected packet rate average = 0.0173099
	minimum = 0.002 (at node 122)
	maximum = 0.0445 (at node 176)
Accepted packet rate average = 0.012724
	minimum = 0.0055 (at node 64)
	maximum = 0.0205 (at node 78)
Injected flit rate average = 0.311708
	minimum = 0.036 (at node 122)
	maximum = 0.801 (at node 176)
Accepted flit rate average= 0.231565
	minimum = 0.1025 (at node 64)
	maximum = 0.3375 (at node 78)
Injected packet length average = 18.0075
Accepted packet length average = 18.1991
Total in-flight flits = 91105 (71535 measured)
latency change    = 0.388986
throughput change = 0.00232791
Class 0:
Packet latency average = 850.682
	minimum = 25
	maximum = 3170
Network latency average = 763.723
	minimum = 23
	maximum = 2962
Slowest packet = 10126
Flit latency average = 1078.34
	minimum = 6
	maximum = 5816
Slowest flit = 11200
Fragmentation average = 243.874
	minimum = 0
	maximum = 2061
Injected packet rate average = 0.0174705
	minimum = 0.003 (at node 120)
	maximum = 0.04 (at node 169)
Accepted packet rate average = 0.0127361
	minimum = 0.00766667 (at node 36)
	maximum = 0.0176667 (at node 6)
Injected flit rate average = 0.314714
	minimum = 0.049 (at node 120)
	maximum = 0.725333 (at node 169)
Accepted flit rate average= 0.232361
	minimum = 0.134333 (at node 36)
	maximum = 0.316333 (at node 11)
Injected packet length average = 18.014
Accepted packet length average = 18.2443
Total in-flight flits = 107674 (95626 measured)
latency change    = 0.247058
throughput change = 0.00342573
Draining remaining packets ...
Class 0:
Remaining flits: 9311 9312 9313 9314 9315 9316 9317 9318 9319 9320 [...] (69797 flits)
Measured flits: 182142 182143 182144 182145 182146 182147 182148 182149 182150 182151 [...] (62351 flits)
Class 0:
Remaining flits: 15354 15355 15356 15357 15358 15359 15360 15361 15362 15363 [...] (36311 flits)
Measured flits: 182664 182665 182666 182667 182668 182669 182670 182671 182672 182673 [...] (32098 flits)
Class 0:
Remaining flits: 15354 15355 15356 15357 15358 15359 15360 15361 15362 15363 [...] (11434 flits)
Measured flits: 182664 182665 182666 182667 182668 182669 182670 182671 182672 182673 [...] (9734 flits)
Class 0:
Remaining flits: 120114 120115 120116 120117 120118 120119 120120 120121 120122 120123 [...] (1842 flits)
Measured flits: 185932 185933 185934 185935 185936 185937 185938 185939 186084 186085 [...] (1558 flits)
Time taken is 10523 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2015.33 (1 samples)
	minimum = 25 (1 samples)
	maximum = 7488 (1 samples)
Network latency average = 1911.29 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7339 (1 samples)
Flit latency average = 1942.52 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8823 (1 samples)
Fragmentation average = 291.593 (1 samples)
	minimum = 0 (1 samples)
	maximum = 6340 (1 samples)
Injected packet rate average = 0.0174705 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.04 (1 samples)
Accepted packet rate average = 0.0127361 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.314714 (1 samples)
	minimum = 0.049 (1 samples)
	maximum = 0.725333 (1 samples)
Accepted flit rate average = 0.232361 (1 samples)
	minimum = 0.134333 (1 samples)
	maximum = 0.316333 (1 samples)
Injected packet size average = 18.014 (1 samples)
Accepted packet size average = 18.2443 (1 samples)
Hops average = 5.07761 (1 samples)
Total run time 11.8656
