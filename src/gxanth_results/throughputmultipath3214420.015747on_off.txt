BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.362
	minimum = 22
	maximum = 844
Network latency average = 151.279
	minimum = 22
	maximum = 696
Slowest packet = 63
Flit latency average = 123.939
	minimum = 5
	maximum = 679
Slowest flit = 14921
Fragmentation average = 25.3605
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117604
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.217516
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.4956
Total in-flight flits = 12831 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 304.892
	minimum = 22
	maximum = 1529
Network latency average = 234.15
	minimum = 22
	maximum = 1034
Slowest packet = 63
Flit latency average = 204.338
	minimum = 5
	maximum = 1017
Slowest flit = 49535
Fragmentation average = 30.5992
	minimum = 0
	maximum = 316
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125859
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.229474
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 70)
Injected packet length average = 17.9137
Accepted packet length average = 18.2326
Total in-flight flits = 19522 (0 measured)
latency change    = 0.300204
throughput change = 0.0521119
Class 0:
Packet latency average = 497.788
	minimum = 22
	maximum = 1657
Network latency average = 422.468
	minimum = 22
	maximum = 1339
Slowest packet = 3307
Flit latency average = 388.476
	minimum = 5
	maximum = 1397
Slowest flit = 85763
Fragmentation average = 36.551
	minimum = 0
	maximum = 299
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140469
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.253979
	minimum = 0.114 (at node 4)
	maximum = 0.418 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.0808
Total in-flight flits = 26738 (0 measured)
latency change    = 0.387505
throughput change = 0.0964851
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.004
	minimum = 22
	maximum = 1180
Network latency average = 314.051
	minimum = 22
	maximum = 953
Slowest packet = 9091
Flit latency average = 505.773
	minimum = 5
	maximum = 1657
Slowest flit = 105947
Fragmentation average = 25.9969
	minimum = 0
	maximum = 221
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141771
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.253937
	minimum = 0.09 (at node 36)
	maximum = 0.516 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9118
Total in-flight flits = 29894 (27963 measured)
latency change    = 0.282945
throughput change = 0.000164082
Class 0:
Packet latency average = 579.957
	minimum = 22
	maximum = 2030
Network latency average = 500.939
	minimum = 22
	maximum = 1739
Slowest packet = 9091
Flit latency average = 549.37
	minimum = 5
	maximum = 2062
Slowest flit = 158273
Fragmentation average = 34.716
	minimum = 0
	maximum = 325
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0141823
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.255414
	minimum = 0.153 (at node 36)
	maximum = 0.375 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0094
Total in-flight flits = 32789 (32756 measured)
latency change    = 0.330978
throughput change = 0.00578105
Class 0:
Packet latency average = 651.82
	minimum = 22
	maximum = 2340
Network latency average = 573.056
	minimum = 22
	maximum = 2054
Slowest packet = 9091
Flit latency average = 580.138
	minimum = 5
	maximum = 2197
Slowest flit = 156485
Fragmentation average = 34.5533
	minimum = 0
	maximum = 325
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143264
	minimum = 0.00866667 (at node 36)
	maximum = 0.0196667 (at node 0)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257604
	minimum = 0.156 (at node 36)
	maximum = 0.355333 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.9811
Total in-flight flits = 34184 (34184 measured)
latency change    = 0.11025
throughput change = 0.00850182
Draining remaining packets ...
Class 0:
Remaining flits: 269435 269436 269437 269438 269439 269440 269441 270144 270145 270146 [...] (2735 flits)
Measured flits: 269435 269436 269437 269438 269439 269440 269441 270144 270145 270146 [...] (2735 flits)
Time taken is 7634 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 743.639 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2514 (1 samples)
Network latency average = 661.971 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2137 (1 samples)
Flit latency average = 647.11 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2197 (1 samples)
Fragmentation average = 34.7586 (1 samples)
	minimum = 0 (1 samples)
	maximum = 325 (1 samples)
Injected packet rate average = 0.0150295 (1 samples)
	minimum = 0.00433333 (1 samples)
	maximum = 0.0323333 (1 samples)
Accepted packet rate average = 0.0143264 (1 samples)
	minimum = 0.00866667 (1 samples)
	maximum = 0.0196667 (1 samples)
Injected flit rate average = 0.270549 (1 samples)
	minimum = 0.078 (1 samples)
	maximum = 0.581667 (1 samples)
Accepted flit rate average = 0.257604 (1 samples)
	minimum = 0.156 (1 samples)
	maximum = 0.355333 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 17.9811 (1 samples)
Hops average = 5.05776 (1 samples)
Total run time 4.9556
