BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 359.861
	minimum = 22
	maximum = 985
Network latency average = 238.082
	minimum = 22
	maximum = 780
Slowest packet = 46
Flit latency average = 203.689
	minimum = 5
	maximum = 837
Slowest flit = 12687
Fragmentation average = 51.2024
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0250937
	minimum = 0 (at node 66)
	maximum = 0.052 (at node 190)
Accepted packet rate average = 0.0137917
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 48)
Injected flit rate average = 0.446703
	minimum = 0 (at node 66)
	maximum = 0.934 (at node 190)
Accepted flit rate average= 0.258797
	minimum = 0.095 (at node 174)
	maximum = 0.404 (at node 103)
Injected packet length average = 17.8014
Accepted packet length average = 18.7647
Total in-flight flits = 37431 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 631.664
	minimum = 22
	maximum = 1936
Network latency average = 431.157
	minimum = 22
	maximum = 1603
Slowest packet = 46
Flit latency average = 390.95
	minimum = 5
	maximum = 1607
Slowest flit = 36183
Fragmentation average = 55.6056
	minimum = 0
	maximum = 376
Injected packet rate average = 0.0213047
	minimum = 0.002 (at node 92)
	maximum = 0.039 (at node 95)
Accepted packet rate average = 0.0144219
	minimum = 0.008 (at node 116)
	maximum = 0.0215 (at node 22)
Injected flit rate average = 0.380349
	minimum = 0.036 (at node 92)
	maximum = 0.702 (at node 95)
Accepted flit rate average= 0.264544
	minimum = 0.144 (at node 116)
	maximum = 0.387 (at node 22)
Injected packet length average = 17.8528
Accepted packet length average = 18.3433
Total in-flight flits = 46519 (0 measured)
latency change    = 0.430296
throughput change = 0.0217256
Class 0:
Packet latency average = 1412.04
	minimum = 25
	maximum = 2825
Network latency average = 856.727
	minimum = 22
	maximum = 2274
Slowest packet = 5352
Flit latency average = 809.403
	minimum = 5
	maximum = 2257
Slowest flit = 43073
Fragmentation average = 59.0088
	minimum = 0
	maximum = 263
Injected packet rate average = 0.0151927
	minimum = 0 (at node 3)
	maximum = 0.039 (at node 78)
Accepted packet rate average = 0.014724
	minimum = 0.006 (at node 4)
	maximum = 0.025 (at node 181)
Injected flit rate average = 0.275417
	minimum = 0 (at node 3)
	maximum = 0.711 (at node 78)
Accepted flit rate average= 0.266505
	minimum = 0.102 (at node 20)
	maximum = 0.443 (at node 181)
Injected packet length average = 18.1282
Accepted packet length average = 18.1001
Total in-flight flits = 48180 (0 measured)
latency change    = 0.552658
throughput change = 0.00735797
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1863.45
	minimum = 33
	maximum = 3661
Network latency average = 392.302
	minimum = 22
	maximum = 983
Slowest packet = 11225
Flit latency average = 871.334
	minimum = 5
	maximum = 2922
Slowest flit = 86490
Fragmentation average = 37.6031
	minimum = 0
	maximum = 268
Injected packet rate average = 0.0158646
	minimum = 0 (at node 3)
	maximum = 0.029 (at node 74)
Accepted packet rate average = 0.0149219
	minimum = 0.005 (at node 86)
	maximum = 0.024 (at node 13)
Injected flit rate average = 0.284276
	minimum = 0 (at node 3)
	maximum = 0.524 (at node 74)
Accepted flit rate average= 0.269307
	minimum = 0.092 (at node 86)
	maximum = 0.418 (at node 90)
Injected packet length average = 17.9189
Accepted packet length average = 18.0478
Total in-flight flits = 51031 (41891 measured)
latency change    = 0.242244
throughput change = 0.0104048
Class 0:
Packet latency average = 2379.68
	minimum = 32
	maximum = 4623
Network latency average = 749.28
	minimum = 22
	maximum = 1855
Slowest packet = 11225
Flit latency average = 903.565
	minimum = 5
	maximum = 3282
Slowest flit = 129311
Fragmentation average = 57.1171
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0155859
	minimum = 0.0015 (at node 67)
	maximum = 0.0245 (at node 65)
Accepted packet rate average = 0.0149818
	minimum = 0.0095 (at node 1)
	maximum = 0.022 (at node 165)
Injected flit rate average = 0.279719
	minimum = 0.027 (at node 67)
	maximum = 0.441 (at node 107)
Accepted flit rate average= 0.26994
	minimum = 0.171 (at node 1)
	maximum = 0.3915 (at node 165)
Injected packet length average = 17.9469
Accepted packet length average = 18.0179
Total in-flight flits = 51893 (51351 measured)
latency change    = 0.216934
throughput change = 0.00234427
Class 0:
Packet latency average = 2737.07
	minimum = 27
	maximum = 5585
Network latency average = 874.241
	minimum = 22
	maximum = 2794
Slowest packet = 11225
Flit latency average = 922.389
	minimum = 5
	maximum = 3434
Slowest flit = 133469
Fragmentation average = 65.4344
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0152743
	minimum = 0.00266667 (at node 67)
	maximum = 0.026 (at node 107)
Accepted packet rate average = 0.014849
	minimum = 0.00966667 (at node 36)
	maximum = 0.0206667 (at node 129)
Injected flit rate average = 0.274401
	minimum = 0.048 (at node 67)
	maximum = 0.466333 (at node 107)
Accepted flit rate average= 0.268042
	minimum = 0.188 (at node 36)
	maximum = 0.366667 (at node 129)
Injected packet length average = 17.9649
Accepted packet length average = 18.0512
Total in-flight flits = 51864 (51828 measured)
latency change    = 0.130572
throughput change = 0.00708262
Class 0:
Packet latency average = 3081.35
	minimum = 27
	maximum = 6204
Network latency average = 931.812
	minimum = 22
	maximum = 3812
Slowest packet = 11225
Flit latency average = 937.109
	minimum = 5
	maximum = 4056
Slowest flit = 189017
Fragmentation average = 69.3553
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0152005
	minimum = 0.00775 (at node 67)
	maximum = 0.02375 (at node 65)
Accepted packet rate average = 0.0148451
	minimum = 0.0105 (at node 86)
	maximum = 0.0205 (at node 103)
Injected flit rate average = 0.273173
	minimum = 0.1395 (at node 67)
	maximum = 0.42725 (at node 65)
Accepted flit rate average= 0.267803
	minimum = 0.1885 (at node 86)
	maximum = 0.36275 (at node 103)
Injected packet length average = 17.9713
Accepted packet length average = 18.0399
Total in-flight flits = 52243 (52243 measured)
latency change    = 0.11173
throughput change = 0.000889762
Class 0:
Packet latency average = 3392.62
	minimum = 27
	maximum = 7046
Network latency average = 956.875
	minimum = 22
	maximum = 3910
Slowest packet = 11225
Flit latency average = 940.968
	minimum = 5
	maximum = 4180
Slowest flit = 244959
Fragmentation average = 72.2229
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0151865
	minimum = 0.0082 (at node 106)
	maximum = 0.022 (at node 65)
Accepted packet rate average = 0.0148698
	minimum = 0.0114 (at node 86)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.273105
	minimum = 0.145 (at node 106)
	maximum = 0.396 (at node 65)
Accepted flit rate average= 0.268341
	minimum = 0.2046 (at node 86)
	maximum = 0.358 (at node 128)
Injected packet length average = 17.9835
Accepted packet length average = 18.046
Total in-flight flits = 52617 (52617 measured)
latency change    = 0.0917508
throughput change = 0.00200208
Class 0:
Packet latency average = 3691.06
	minimum = 27
	maximum = 8224
Network latency average = 975.191
	minimum = 22
	maximum = 4208
Slowest packet = 11225
Flit latency average = 946.59
	minimum = 5
	maximum = 4191
Slowest flit = 244961
Fragmentation average = 74.5958
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0151285
	minimum = 0.0105 (at node 32)
	maximum = 0.02 (at node 65)
Accepted packet rate average = 0.0148767
	minimum = 0.0115 (at node 18)
	maximum = 0.0196667 (at node 138)
Injected flit rate average = 0.272029
	minimum = 0.189 (at node 32)
	maximum = 0.36 (at node 65)
Accepted flit rate average= 0.268097
	minimum = 0.206167 (at node 18)
	maximum = 0.353333 (at node 138)
Injected packet length average = 17.9812
Accepted packet length average = 18.0212
Total in-flight flits = 52622 (52622 measured)
latency change    = 0.0808547
throughput change = 0.00090789
Class 0:
Packet latency average = 3989.96
	minimum = 27
	maximum = 8678
Network latency average = 985.687
	minimum = 22
	maximum = 4208
Slowest packet = 11225
Flit latency average = 948.918
	minimum = 5
	maximum = 4349
Slowest flit = 340807
Fragmentation average = 75.6881
	minimum = 0
	maximum = 340
Injected packet rate average = 0.0150893
	minimum = 0.0104286 (at node 32)
	maximum = 0.0194286 (at node 65)
Accepted packet rate average = 0.0148951
	minimum = 0.0118571 (at node 80)
	maximum = 0.0188571 (at node 103)
Injected flit rate average = 0.271336
	minimum = 0.186714 (at node 32)
	maximum = 0.350286 (at node 107)
Accepted flit rate average= 0.268628
	minimum = 0.211286 (at node 190)
	maximum = 0.335857 (at node 103)
Injected packet length average = 17.982
Accepted packet length average = 18.0347
Total in-flight flits = 51752 (51752 measured)
latency change    = 0.0749111
throughput change = 0.0019758
Draining all recorded packets ...
Class 0:
Remaining flits: 423972 423973 423974 423975 423976 423977 423978 423979 423980 423981 [...] (51800 flits)
Measured flits: 423972 423973 423974 423975 423976 423977 423978 423979 423980 423981 [...] (51488 flits)
Class 0:
Remaining flits: 461394 461395 461396 461397 461398 461399 461400 461401 461402 461403 [...] (52384 flits)
Measured flits: 461394 461395 461396 461397 461398 461399 461400 461401 461402 461403 [...] (51530 flits)
Class 0:
Remaining flits: 490500 490501 490502 490503 490504 490505 490506 490507 490508 490509 [...] (51771 flits)
Measured flits: 490500 490501 490502 490503 490504 490505 490506 490507 490508 490509 [...] (50344 flits)
Class 0:
Remaining flits: 535993 535994 535995 535996 535997 535998 535999 536000 536001 536002 [...] (51041 flits)
Measured flits: 535993 535994 535995 535996 535997 535998 535999 536000 536001 536002 [...] (48972 flits)
Class 0:
Remaining flits: 614124 614125 614126 614127 614128 614129 614130 614131 614132 614133 [...] (51034 flits)
Measured flits: 614124 614125 614126 614127 614128 614129 614130 614131 614132 614133 [...] (48493 flits)
Class 0:
Remaining flits: 655036 655037 685782 685783 685784 685785 685786 685787 685788 685789 [...] (50179 flits)
Measured flits: 655036 655037 685782 685783 685784 685785 685786 685787 685788 685789 [...] (47148 flits)
Class 0:
Remaining flits: 705618 705619 705620 705621 705622 705623 705624 705625 705626 705627 [...] (50379 flits)
Measured flits: 705618 705619 705620 705621 705622 705623 705624 705625 705626 705627 [...] (45615 flits)
Class 0:
Remaining flits: 763524 763525 763526 763527 763528 763529 763530 763531 763532 763533 [...] (50324 flits)
Measured flits: 763524 763525 763526 763527 763528 763529 763530 763531 763532 763533 [...] (43789 flits)
Class 0:
Remaining flits: 838313 840060 840061 840062 840063 840064 840065 840066 840067 840068 [...] (48864 flits)
Measured flits: 838313 840060 840061 840062 840063 840064 840065 840066 840067 840068 [...] (39264 flits)
Class 0:
Remaining flits: 840168 840169 840170 840171 840172 840173 840174 840175 840176 840177 [...] (49505 flits)
Measured flits: 840168 840169 840170 840171 840172 840173 840174 840175 840176 840177 [...] (36919 flits)
Class 0:
Remaining flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (48928 flits)
Measured flits: 860832 860833 860834 860835 860836 860837 860838 860839 860840 860841 [...] (32462 flits)
Class 0:
Remaining flits: 920610 920611 920612 920613 920614 920615 920616 920617 920618 920619 [...] (50156 flits)
Measured flits: 920610 920611 920612 920613 920614 920615 920616 920617 920618 920619 [...] (29703 flits)
Class 0:
Remaining flits: 920610 920611 920612 920613 920614 920615 920616 920617 920618 920619 [...] (49876 flits)
Measured flits: 920610 920611 920612 920613 920614 920615 920616 920617 920618 920619 [...] (25642 flits)
Class 0:
Remaining flits: 1036608 1036609 1036610 1036611 1036612 1036613 1036614 1036615 1036616 1036617 [...] (49191 flits)
Measured flits: 1051398 1051399 1051400 1051401 1051402 1051403 1051404 1051405 1051406 1051407 [...] (23756 flits)
Class 0:
Remaining flits: 1060254 1060255 1060256 1060257 1060258 1060259 1060260 1060261 1060262 1060263 [...] (50958 flits)
Measured flits: 1060254 1060255 1060256 1060257 1060258 1060259 1060260 1060261 1060262 1060263 [...] (21060 flits)
Class 0:
Remaining flits: 1060254 1060255 1060256 1060257 1060258 1060259 1060260 1060261 1060262 1060263 [...] (50573 flits)
Measured flits: 1060254 1060255 1060256 1060257 1060258 1060259 1060260 1060261 1060262 1060263 [...] (16643 flits)
Class 0:
Remaining flits: 1198800 1198801 1198802 1198803 1198804 1198805 1198806 1198807 1198808 1198809 [...] (50370 flits)
Measured flits: 1241568 1241569 1241570 1241571 1241572 1241573 1241574 1241575 1241576 1241577 [...] (13684 flits)
Class 0:
Remaining flits: 1291299 1291300 1291301 1300212 1300213 1300214 1300215 1300216 1300217 1300218 [...] (49961 flits)
Measured flits: 1319454 1319455 1319456 1319457 1319458 1319459 1319460 1319461 1319462 1319463 [...] (10764 flits)
Class 0:
Remaining flits: 1300212 1300213 1300214 1300215 1300216 1300217 1300218 1300219 1300220 1300221 [...] (49811 flits)
Measured flits: 1320408 1320409 1320410 1320411 1320412 1320413 1320414 1320415 1320416 1320417 [...] (9240 flits)
Class 0:
Remaining flits: 1320416 1320417 1320418 1320419 1320420 1320421 1320422 1320423 1320424 1320425 [...] (49799 flits)
Measured flits: 1320416 1320417 1320418 1320419 1320420 1320421 1320422 1320423 1320424 1320425 [...] (6956 flits)
Class 0:
Remaining flits: 1430550 1430551 1430552 1430553 1430554 1430555 1430556 1430557 1430558 1430559 [...] (49184 flits)
Measured flits: 1430550 1430551 1430552 1430553 1430554 1430555 1430556 1430557 1430558 1430559 [...] (5320 flits)
Class 0:
Remaining flits: 1472130 1472131 1472132 1472133 1472134 1472135 1472136 1472137 1472138 1472139 [...] (49693 flits)
Measured flits: 1529817 1529818 1529819 1531710 1531711 1531712 1531713 1531714 1531715 1531716 [...] (4643 flits)
Class 0:
Remaining flits: 1475874 1475875 1475876 1475877 1475878 1475879 1475880 1475881 1475882 1475883 [...] (49506 flits)
Measured flits: 1531710 1531711 1531712 1531713 1531714 1531715 1531716 1531717 1531718 1531719 [...] (3910 flits)
Class 0:
Remaining flits: 1475874 1475875 1475876 1475877 1475878 1475879 1475880 1475881 1475882 1475883 [...] (49007 flits)
Measured flits: 1538694 1538695 1538696 1538697 1538698 1538699 1538700 1538701 1538702 1538703 [...] (2929 flits)
Class 0:
Remaining flits: 1538694 1538695 1538696 1538697 1538698 1538699 1538700 1538701 1538702 1538703 [...] (50785 flits)
Measured flits: 1538694 1538695 1538696 1538697 1538698 1538699 1538700 1538701 1538702 1538703 [...] (2018 flits)
Class 0:
Remaining flits: 1689883 1689884 1689885 1689886 1689887 1689888 1689889 1689890 1689891 1689892 [...] (50973 flits)
Measured flits: 1729098 1729099 1729100 1729101 1729102 1729103 1729104 1729105 1729106 1729107 [...] (919 flits)
Class 0:
Remaining flits: 1693152 1693153 1693154 1693155 1693156 1693157 1693158 1693159 1693160 1693161 [...] (51181 flits)
Measured flits: 1809990 1809991 1809992 1809993 1809994 1809995 1809996 1809997 1809998 1809999 [...] (861 flits)
Class 0:
Remaining flits: 1754802 1754803 1754804 1754805 1754806 1754807 1754808 1754809 1754810 1754811 [...] (49929 flits)
Measured flits: 1938816 1938817 1938818 1938819 1938820 1938821 1938822 1938823 1938824 1938825 [...] (435 flits)
Class 0:
Remaining flits: 1866168 1866169 1866170 1866171 1866172 1866173 1866174 1866175 1866176 1866177 [...] (49386 flits)
Measured flits: 1985022 1985023 1985024 1985025 1985026 1985027 1985028 1985029 1985030 1985031 [...] (162 flits)
Class 0:
Remaining flits: 1866183 1866184 1866185 1891206 1891207 1891208 1891209 1891210 1891211 1891212 [...] (50964 flits)
Measured flits: 2062818 2062819 2062820 2062821 2062822 2062823 2062824 2062825 2062826 2062827 [...] (144 flits)
Class 0:
Remaining flits: 1958959 1958960 1958961 1958962 1958963 1958964 1958965 1958966 1958967 1958968 [...] (49067 flits)
Measured flits: 2094370 2094371 2131884 2131885 2131886 2131887 2131888 2131889 2131890 2131891 [...] (164 flits)
Class 0:
Remaining flits: 1987997 1987998 1987999 1988000 1988001 1988002 1988003 1988004 1988005 1988006 [...] (49214 flits)
Measured flits: 2153466 2153467 2153468 2153469 2153470 2153471 2153472 2153473 2153474 2153475 [...] (144 flits)
Class 0:
Remaining flits: 2025324 2025325 2025326 2025327 2025328 2025329 2025330 2025331 2025332 2025333 [...] (49501 flits)
Measured flits: 2153466 2153467 2153468 2153469 2153470 2153471 2153472 2153473 2153474 2153475 [...] (85 flits)
Class 0:
Remaining flits: 2087190 2087191 2087192 2087193 2087194 2087195 2087196 2087197 2087198 2087199 [...] (49712 flits)
Measured flits: 2153466 2153467 2153468 2153469 2153470 2153471 2153472 2153473 2153474 2153475 [...] (188 flits)
Class 0:
Remaining flits: 2087190 2087191 2087192 2087193 2087194 2087195 2087196 2087197 2087198 2087199 [...] (47982 flits)
Measured flits: 2310284 2310285 2310286 2310287 2310288 2310289 2310290 2310291 2310292 2310293 [...] (178 flits)
Class 0:
Remaining flits: 2164446 2164447 2164448 2164449 2164450 2164451 2164452 2164453 2164454 2164455 [...] (47482 flits)
Measured flits: 2345508 2345509 2345510 2345511 2345512 2345513 2345514 2345515 2345516 2345517 [...] (167 flits)
Class 0:
Remaining flits: 2164446 2164447 2164448 2164449 2164450 2164451 2164452 2164453 2164454 2164455 [...] (48127 flits)
Measured flits: 2345514 2345515 2345516 2345517 2345518 2345519 2345520 2345521 2345522 2345523 [...] (136 flits)
Class 0:
Remaining flits: 2225070 2225071 2225072 2225073 2225074 2225075 2225076 2225077 2225078 2225079 [...] (48948 flits)
Measured flits: 2399274 2399275 2399276 2399277 2399278 2399279 2399280 2399281 2399282 2399283 [...] (108 flits)
Class 0:
Remaining flits: 2321136 2321137 2321138 2321139 2321140 2321141 2321142 2321143 2321144 2321145 [...] (49192 flits)
Measured flits: 2399274 2399275 2399276 2399277 2399278 2399279 2399280 2399281 2399282 2399283 [...] (162 flits)
Class 0:
Remaining flits: 2321136 2321137 2321138 2321139 2321140 2321141 2321142 2321143 2321144 2321145 [...] (48777 flits)
Measured flits: 2565702 2565703 2565704 2565705 2565706 2565707 2565708 2565709 2565710 2565711 [...] (54 flits)
Class 0:
Remaining flits: 2321136 2321137 2321138 2321139 2321140 2321141 2321142 2321143 2321144 2321145 [...] (48595 flits)
Measured flits: 2567538 2567539 2567540 2567541 2567542 2567543 2567544 2567545 2567546 2567547 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2513286 2513287 2513288 2513289 2513290 2513291 2513292 2513293 2513294 2513295 [...] (8571 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2568734 2568735 2568736 2568737 2568738 2568739 2568740 2568741 2568742 2568743 [...] (104 flits)
Measured flits: (0 flits)
Time taken is 54061 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9429.11 (1 samples)
	minimum = 27 (1 samples)
	maximum = 42026 (1 samples)
Network latency average = 1008.97 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7036 (1 samples)
Flit latency average = 939.255 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7273 (1 samples)
Fragmentation average = 80.365 (1 samples)
	minimum = 0 (1 samples)
	maximum = 343 (1 samples)
Injected packet rate average = 0.0150893 (1 samples)
	minimum = 0.0104286 (1 samples)
	maximum = 0.0194286 (1 samples)
Accepted packet rate average = 0.0148951 (1 samples)
	minimum = 0.0118571 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.271336 (1 samples)
	minimum = 0.186714 (1 samples)
	maximum = 0.350286 (1 samples)
Accepted flit rate average = 0.268628 (1 samples)
	minimum = 0.211286 (1 samples)
	maximum = 0.335857 (1 samples)
Injected packet size average = 17.982 (1 samples)
Accepted packet size average = 18.0347 (1 samples)
Hops average = 5.06539 (1 samples)
Total run time 77.7123
