BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 369.688
	minimum = 27
	maximum = 981
Network latency average = 275.277
	minimum = 23
	maximum = 951
Slowest packet = 64
Flit latency average = 210.079
	minimum = 6
	maximum = 951
Slowest flit = 3789
Fragmentation average = 142.311
	minimum = 0
	maximum = 854
Injected packet rate average = 0.027625
	minimum = 0 (at node 72)
	maximum = 0.056 (at node 19)
Accepted packet rate average = 0.0101302
	minimum = 0.004 (at node 79)
	maximum = 0.019 (at node 48)
Injected flit rate average = 0.491703
	minimum = 0 (at node 72)
	maximum = 1 (at node 19)
Accepted flit rate average= 0.206953
	minimum = 0.078 (at node 83)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.7992
Accepted packet length average = 20.4293
Total in-flight flits = 55737 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.064
	minimum = 23
	maximum = 1972
Network latency average = 491.252
	minimum = 23
	maximum = 1823
Slowest packet = 64
Flit latency average = 418.951
	minimum = 6
	maximum = 1885
Slowest flit = 7809
Fragmentation average = 179.566
	minimum = 0
	maximum = 1469
Injected packet rate average = 0.028987
	minimum = 0.001 (at node 138)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0113802
	minimum = 0.0055 (at node 10)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.519185
	minimum = 0.018 (at node 138)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.219055
	minimum = 0.1135 (at node 116)
	maximum = 0.3175 (at node 56)
Injected packet length average = 17.911
Accepted packet length average = 19.2487
Total in-flight flits = 116241 (0 measured)
latency change    = 0.400892
throughput change = 0.0552445
Class 0:
Packet latency average = 1294.11
	minimum = 30
	maximum = 2915
Network latency average = 1097.74
	minimum = 25
	maximum = 2698
Slowest packet = 173
Flit latency average = 1035.71
	minimum = 6
	maximum = 2762
Slowest flit = 19020
Fragmentation average = 213.133
	minimum = 0
	maximum = 2312
Injected packet rate average = 0.0320208
	minimum = 0 (at node 172)
	maximum = 0.056 (at node 4)
Accepted packet rate average = 0.0132344
	minimum = 0.004 (at node 164)
	maximum = 0.025 (at node 11)
Injected flit rate average = 0.577255
	minimum = 0 (at node 172)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.238484
	minimum = 0.1 (at node 164)
	maximum = 0.487 (at node 82)
Injected packet length average = 18.0275
Accepted packet length average = 18.0201
Total in-flight flits = 181116 (0 measured)
latency change    = 0.523176
throughput change = 0.0814715
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 396.361
	minimum = 29
	maximum = 1994
Network latency average = 88.6734
	minimum = 23
	maximum = 905
Slowest packet = 17305
Flit latency average = 1517.62
	minimum = 6
	maximum = 3670
Slowest flit = 27695
Fragmentation average = 29.0081
	minimum = 0
	maximum = 408
Injected packet rate average = 0.0309271
	minimum = 0.002 (at node 27)
	maximum = 0.056 (at node 7)
Accepted packet rate average = 0.0131198
	minimum = 0.004 (at node 126)
	maximum = 0.022 (at node 137)
Injected flit rate average = 0.555724
	minimum = 0.036 (at node 27)
	maximum = 1 (at node 7)
Accepted flit rate average= 0.235146
	minimum = 0.085 (at node 126)
	maximum = 0.396 (at node 137)
Injected packet length average = 17.9688
Accepted packet length average = 17.923
Total in-flight flits = 242852 (97546 measured)
latency change    = 2.26499
throughput change = 0.0141977
Class 0:
Packet latency average = 545.229
	minimum = 29
	maximum = 2237
Network latency average = 275.612
	minimum = 23
	maximum = 1957
Slowest packet = 17305
Flit latency average = 1722.35
	minimum = 6
	maximum = 4867
Slowest flit = 10072
Fragmentation average = 55.674
	minimum = 0
	maximum = 642
Injected packet rate average = 0.0306641
	minimum = 0.0055 (at node 59)
	maximum = 0.056 (at node 24)
Accepted packet rate average = 0.0132083
	minimum = 0.0065 (at node 126)
	maximum = 0.0185 (at node 73)
Injected flit rate average = 0.551911
	minimum = 0.095 (at node 59)
	maximum = 1 (at node 24)
Accepted flit rate average= 0.23612
	minimum = 0.1135 (at node 126)
	maximum = 0.3415 (at node 73)
Injected packet length average = 17.9986
Accepted packet length average = 17.8766
Total in-flight flits = 302396 (188153 measured)
latency change    = 0.273038
throughput change = 0.00412485
Class 0:
Packet latency average = 812.754
	minimum = 25
	maximum = 3572
Network latency average = 569.673
	minimum = 23
	maximum = 2952
Slowest packet = 17305
Flit latency average = 1961.76
	minimum = 6
	maximum = 5566
Slowest flit = 36400
Fragmentation average = 77.8605
	minimum = 0
	maximum = 1181
Injected packet rate average = 0.0303785
	minimum = 0.008 (at node 39)
	maximum = 0.0556667 (at node 24)
Accepted packet rate average = 0.013059
	minimum = 0.00766667 (at node 36)
	maximum = 0.0176667 (at node 6)
Injected flit rate average = 0.546731
	minimum = 0.144 (at node 39)
	maximum = 1 (at node 24)
Accepted flit rate average= 0.234061
	minimum = 0.135 (at node 36)
	maximum = 0.334 (at node 73)
Injected packet length average = 17.9973
Accepted packet length average = 17.9233
Total in-flight flits = 361261 (273217 measured)
latency change    = 0.329159
throughput change = 0.00879698
Draining remaining packets ...
Class 0:
Remaining flits: 14098 14099 14100 14101 14102 14103 14104 14105 14106 14107 [...] (325522 flits)
Measured flits: 311022 311023 311024 311025 311026 311027 311028 311029 311030 311031 [...] (259375 flits)
Class 0:
Remaining flits: 46764 46765 46766 46767 46768 46769 46770 46771 46772 46773 [...] (290475 flits)
Measured flits: 311022 311023 311024 311025 311026 311027 311028 311029 311030 311031 [...] (240283 flits)
Class 0:
Remaining flits: 55999 56000 56001 56002 56003 56004 56005 56006 56007 56008 [...] (255216 flits)
Measured flits: 311022 311023 311024 311025 311026 311027 311028 311029 311030 311031 [...] (218430 flits)
Class 0:
Remaining flits: 62919 62920 62921 62922 62923 62924 62925 62926 62927 70403 [...] (220915 flits)
Measured flits: 311076 311077 311078 311079 311080 311081 311082 311083 311084 311085 [...] (193938 flits)
Class 0:
Remaining flits: 62919 62920 62921 62922 62923 62924 62925 62926 62927 94212 [...] (186904 flits)
Measured flits: 311094 311095 311096 311097 311098 311099 311100 311101 311102 311103 [...] (167093 flits)
Class 0:
Remaining flits: 94284 94285 94286 94287 94288 94289 94290 94291 94292 94293 [...] (153102 flits)
Measured flits: 311094 311095 311096 311097 311098 311099 311100 311101 311102 311103 [...] (138973 flits)
Class 0:
Remaining flits: 94284 94285 94286 94287 94288 94289 94290 94291 94292 94293 [...] (119806 flits)
Measured flits: 311148 311149 311150 311151 311152 311153 311154 311155 311156 311157 [...] (109477 flits)
Class 0:
Remaining flits: 100908 100909 100910 100911 100912 100913 100914 100915 100916 100917 [...] (88373 flits)
Measured flits: 311148 311149 311150 311151 311152 311153 311154 311155 311156 311157 [...] (81324 flits)
Class 0:
Remaining flits: 117792 117793 117794 117795 117796 117797 117798 117799 117800 117801 [...] (58610 flits)
Measured flits: 311148 311149 311150 311151 311152 311153 311154 311155 311156 311157 [...] (54274 flits)
Class 0:
Remaining flits: 120168 120169 120170 120171 120172 120173 120174 120175 120176 120177 [...] (33776 flits)
Measured flits: 311184 311185 311186 311187 311188 311189 311190 311191 311192 311193 [...] (31510 flits)
Class 0:
Remaining flits: 120168 120169 120170 120171 120172 120173 120174 120175 120176 120177 [...] (14317 flits)
Measured flits: 311191 311192 311193 311194 311195 311196 311197 311198 311199 311200 [...] (13288 flits)
Class 0:
Remaining flits: 120168 120169 120170 120171 120172 120173 120174 120175 120176 120177 [...] (2420 flits)
Measured flits: 313362 313363 313364 313365 313366 313367 313368 313369 313370 313371 [...] (2158 flits)
Time taken is 18692 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6963.19 (1 samples)
	minimum = 25 (1 samples)
	maximum = 16179 (1 samples)
Network latency average = 6673.01 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15418 (1 samples)
Flit latency average = 5882.25 (1 samples)
	minimum = 6 (1 samples)
	maximum = 16942 (1 samples)
Fragmentation average = 177.931 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3906 (1 samples)
Injected packet rate average = 0.0303785 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.013059 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.0176667 (1 samples)
Injected flit rate average = 0.546731 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.234061 (1 samples)
	minimum = 0.135 (1 samples)
	maximum = 0.334 (1 samples)
Injected packet size average = 17.9973 (1 samples)
Accepted packet size average = 17.9233 (1 samples)
Hops average = 5.08212 (1 samples)
Total run time 17.5256
