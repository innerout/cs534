BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.031217
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 368.324
	minimum = 27
	maximum = 993
Network latency average = 274.888
	minimum = 23
	maximum = 944
Slowest packet = 64
Flit latency average = 208.784
	minimum = 6
	maximum = 943
Slowest flit = 3789
Fragmentation average = 142.089
	minimum = 0
	maximum = 868
Injected packet rate average = 0.0272969
	minimum = 0 (at node 26)
	maximum = 0.056 (at node 41)
Accepted packet rate average = 0.0101771
	minimum = 0.004 (at node 30)
	maximum = 0.018 (at node 14)
Injected flit rate average = 0.486422
	minimum = 0 (at node 26)
	maximum = 1 (at node 41)
Accepted flit rate average= 0.208552
	minimum = 0.082 (at node 30)
	maximum = 0.358 (at node 48)
Injected packet length average = 17.8197
Accepted packet length average = 20.4923
Total in-flight flits = 54296 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 618.613
	minimum = 23
	maximum = 1977
Network latency average = 495.174
	minimum = 23
	maximum = 1829
Slowest packet = 166
Flit latency average = 422.014
	minimum = 6
	maximum = 1847
Slowest flit = 11113
Fragmentation average = 179.359
	minimum = 0
	maximum = 1569
Injected packet rate average = 0.0275729
	minimum = 0.001 (at node 128)
	maximum = 0.056 (at node 50)
Accepted packet rate average = 0.0113802
	minimum = 0.0055 (at node 178)
	maximum = 0.0165 (at node 97)
Injected flit rate average = 0.49368
	minimum = 0.018 (at node 128)
	maximum = 1 (at node 50)
Accepted flit rate average= 0.218943
	minimum = 0.1045 (at node 178)
	maximum = 0.325 (at node 97)
Injected packet length average = 17.9045
Accepted packet length average = 19.2389
Total in-flight flits = 106510 (0 measured)
latency change    = 0.404597
throughput change = 0.0474582
Class 0:
Packet latency average = 1347.71
	minimum = 28
	maximum = 2949
Network latency average = 1168.57
	minimum = 23
	maximum = 2850
Slowest packet = 1269
Flit latency average = 1097.61
	minimum = 6
	maximum = 2860
Slowest flit = 10772
Fragmentation average = 222.898
	minimum = 0
	maximum = 1954
Injected packet rate average = 0.0271458
	minimum = 0 (at node 39)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0127083
	minimum = 0.005 (at node 169)
	maximum = 0.023 (at node 78)
Injected flit rate average = 0.48876
	minimum = 0 (at node 39)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.224901
	minimum = 0.096 (at node 169)
	maximum = 0.391 (at node 78)
Injected packet length average = 18.005
Accepted packet length average = 17.6971
Total in-flight flits = 157181 (0 measured)
latency change    = 0.54099
throughput change = 0.0264931
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 524.181
	minimum = 31
	maximum = 2359
Network latency average = 146.113
	minimum = 29
	maximum = 963
Slowest packet = 15838
Flit latency average = 1608.15
	minimum = 6
	maximum = 3628
Slowest flit = 30722
Fragmentation average = 38.3014
	minimum = 1
	maximum = 401
Injected packet rate average = 0.0255521
	minimum = 0 (at node 191)
	maximum = 0.056 (at node 34)
Accepted packet rate average = 0.0119271
	minimum = 0.004 (at node 126)
	maximum = 0.022 (at node 114)
Injected flit rate average = 0.460266
	minimum = 0 (at node 191)
	maximum = 1 (at node 25)
Accepted flit rate average= 0.214359
	minimum = 0.088 (at node 109)
	maximum = 0.396 (at node 124)
Injected packet length average = 18.0128
Accepted packet length average = 17.9725
Total in-flight flits = 204314 (82799 measured)
latency change    = 1.57108
throughput change = 0.0491775
Class 0:
Packet latency average = 898.414
	minimum = 27
	maximum = 3965
Network latency average = 525.521
	minimum = 27
	maximum = 1975
Slowest packet = 15838
Flit latency average = 1870.57
	minimum = 6
	maximum = 4588
Slowest flit = 34412
Fragmentation average = 79.7949
	minimum = 0
	maximum = 792
Injected packet rate average = 0.0246536
	minimum = 0.0025 (at node 9)
	maximum = 0.0555 (at node 34)
Accepted packet rate average = 0.0118672
	minimum = 0.0065 (at node 64)
	maximum = 0.0195 (at node 103)
Injected flit rate average = 0.443568
	minimum = 0.045 (at node 9)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.213677
	minimum = 0.1175 (at node 31)
	maximum = 0.3385 (at node 128)
Injected packet length average = 17.992
Accepted packet length average = 18.0057
Total in-flight flits = 245571 (154650 measured)
latency change    = 0.416549
throughput change = 0.0031931
Class 0:
Packet latency average = 1392.87
	minimum = 24
	maximum = 5298
Network latency average = 987.069
	minimum = 24
	maximum = 2935
Slowest packet = 15838
Flit latency average = 2129
	minimum = 6
	maximum = 5694
Slowest flit = 25967
Fragmentation average = 108.541
	minimum = 0
	maximum = 861
Injected packet rate average = 0.0237031
	minimum = 0.006 (at node 56)
	maximum = 0.045 (at node 34)
Accepted packet rate average = 0.0117604
	minimum = 0.00733333 (at node 64)
	maximum = 0.018 (at node 128)
Injected flit rate average = 0.426448
	minimum = 0.108 (at node 56)
	maximum = 0.81 (at node 34)
Accepted flit rate average= 0.212031
	minimum = 0.141333 (at node 190)
	maximum = 0.313 (at node 128)
Injected packet length average = 17.9912
Accepted packet length average = 18.0292
Total in-flight flits = 280877 (214571 measured)
latency change    = 0.35499
throughput change = 0.00776222
Draining remaining packets ...
Class 0:
Remaining flits: 36656 36657 36658 36659 36660 36661 36662 36663 36664 36665 [...] (243575 flits)
Measured flits: 284436 284437 284438 284439 284440 284441 284442 284443 284444 284445 [...] (195536 flits)
Class 0:
Remaining flits: 56268 56269 56270 56271 56272 56273 56274 56275 56276 56277 [...] (206477 flits)
Measured flits: 284706 284707 284708 284709 284710 284711 284712 284713 284714 284715 [...] (172396 flits)
Class 0:
Remaining flits: 62918 62919 62920 62921 62922 62923 62924 62925 62926 62927 [...] (170940 flits)
Measured flits: 284706 284707 284708 284709 284710 284711 284712 284713 284714 284715 [...] (147106 flits)
Class 0:
Remaining flits: 70398 70399 70400 70401 70402 70403 70404 70405 70406 70407 [...] (136836 flits)
Measured flits: 284706 284707 284708 284709 284710 284711 284712 284713 284714 284715 [...] (120415 flits)
Class 0:
Remaining flits: 70398 70399 70400 70401 70402 70403 70404 70405 70406 70407 [...] (103321 flits)
Measured flits: 284742 284743 284744 284745 284746 284747 284748 284749 284750 284751 [...] (92762 flits)
Class 0:
Remaining flits: 70414 70415 76212 76213 76214 76215 76216 76217 76218 76219 [...] (71276 flits)
Measured flits: 284742 284743 284744 284745 284746 284747 284748 284749 284750 284751 [...] (64772 flits)
Class 0:
Remaining flits: 87732 87733 87734 87735 87736 87737 87738 87739 87740 87741 [...] (40329 flits)
Measured flits: 284744 284745 284746 284747 284748 284749 284750 284751 284752 284753 [...] (37070 flits)
Class 0:
Remaining flits: 122890 122891 122892 122893 122894 122895 122896 122897 122898 122899 [...] (14417 flits)
Measured flits: 284886 284887 284888 284889 284890 284891 284892 284893 284894 284895 [...] (13457 flits)
Class 0:
Remaining flits: 188658 188659 188660 188661 188662 188663 188664 188665 188666 188667 [...] (756 flits)
Measured flits: 284896 284897 284898 284899 284900 284901 284902 284903 291150 291151 [...] (718 flits)
Time taken is 15330 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5881.42 (1 samples)
	minimum = 24 (1 samples)
	maximum = 12881 (1 samples)
Network latency average = 5379.24 (1 samples)
	minimum = 24 (1 samples)
	maximum = 12199 (1 samples)
Flit latency average = 4831.12 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13088 (1 samples)
Fragmentation average = 171.118 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2676 (1 samples)
Injected packet rate average = 0.0237031 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.045 (1 samples)
Accepted packet rate average = 0.0117604 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.426448 (1 samples)
	minimum = 0.108 (1 samples)
	maximum = 0.81 (1 samples)
Accepted flit rate average = 0.212031 (1 samples)
	minimum = 0.141333 (1 samples)
	maximum = 0.313 (1 samples)
Injected packet size average = 17.9912 (1 samples)
Accepted packet size average = 18.0292 (1 samples)
Hops average = 5.15282 (1 samples)
Total run time 15.8959
