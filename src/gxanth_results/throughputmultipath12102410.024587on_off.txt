BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 324.852
	minimum = 23
	maximum = 974
Network latency average = 250.692
	minimum = 23
	maximum = 835
Slowest packet = 85
Flit latency average = 213.731
	minimum = 6
	maximum = 855
Slowest flit = 10854
Fragmentation average = 53.954
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00961979
	minimum = 0.003 (at node 174)
	maximum = 0.017 (at node 124)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.181568
	minimum = 0.067 (at node 174)
	maximum = 0.316 (at node 152)
Injected packet length average = 17.8416
Accepted packet length average = 18.8744
Total in-flight flits = 44807 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 555.514
	minimum = 23
	maximum = 1962
Network latency average = 457.037
	minimum = 23
	maximum = 1729
Slowest packet = 85
Flit latency average = 419.812
	minimum = 6
	maximum = 1815
Slowest flit = 13430
Fragmentation average = 59.5055
	minimum = 0
	maximum = 168
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0101172
	minimum = 0.004 (at node 174)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.186086
	minimum = 0.0835 (at node 174)
	maximum = 0.3305 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 18.3931
Total in-flight flits = 90867 (0 measured)
latency change    = 0.415222
throughput change = 0.0242803
Class 0:
Packet latency average = 1222.42
	minimum = 23
	maximum = 2945
Network latency average = 1083.55
	minimum = 23
	maximum = 2680
Slowest packet = 886
Flit latency average = 1055.39
	minimum = 6
	maximum = 2663
Slowest flit = 17153
Fragmentation average = 63.2132
	minimum = 0
	maximum = 174
Injected packet rate average = 0.0254635
	minimum = 0 (at node 165)
	maximum = 0.056 (at node 51)
Accepted packet rate average = 0.010724
	minimum = 0.004 (at node 32)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.458318
	minimum = 0 (at node 165)
	maximum = 1 (at node 51)
Accepted flit rate average= 0.193453
	minimum = 0.073 (at node 32)
	maximum = 0.34 (at node 144)
Injected packet length average = 17.999
Accepted packet length average = 18.0393
Total in-flight flits = 141726 (0 measured)
latency change    = 0.545561
throughput change = 0.0380825
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 265.495
	minimum = 23
	maximum = 1302
Network latency average = 109.215
	minimum = 23
	maximum = 916
Slowest packet = 13922
Flit latency average = 1569.69
	minimum = 6
	maximum = 3723
Slowest flit = 19966
Fragmentation average = 16.6957
	minimum = 0
	maximum = 127
Injected packet rate average = 0.023151
	minimum = 0 (at node 103)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0106406
	minimum = 0.004 (at node 2)
	maximum = 0.02 (at node 81)
Injected flit rate average = 0.417427
	minimum = 0 (at node 103)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.19024
	minimum = 0.072 (at node 117)
	maximum = 0.362 (at node 81)
Injected packet length average = 18.0306
Accepted packet length average = 17.8786
Total in-flight flits = 185210 (73260 measured)
latency change    = 3.6043
throughput change = 0.0168921
Class 0:
Packet latency average = 462.504
	minimum = 23
	maximum = 2364
Network latency average = 323.78
	minimum = 23
	maximum = 1915
Slowest packet = 13922
Flit latency average = 1828.6
	minimum = 6
	maximum = 4453
Slowest flit = 39891
Fragmentation average = 27.0667
	minimum = 0
	maximum = 135
Injected packet rate average = 0.0227812
	minimum = 0.0035 (at node 1)
	maximum = 0.049 (at node 22)
Accepted packet rate average = 0.0103229
	minimum = 0.0045 (at node 144)
	maximum = 0.016 (at node 81)
Injected flit rate average = 0.410362
	minimum = 0.063 (at node 1)
	maximum = 0.887 (at node 22)
Accepted flit rate average= 0.186135
	minimum = 0.0875 (at node 144)
	maximum = 0.2815 (at node 81)
Injected packet length average = 18.0131
Accepted packet length average = 18.0313
Total in-flight flits = 227714 (142006 measured)
latency change    = 0.425962
throughput change = 0.0220494
Class 0:
Packet latency average = 849.24
	minimum = 23
	maximum = 3248
Network latency average = 707.159
	minimum = 23
	maximum = 2954
Slowest packet = 13922
Flit latency average = 2084.04
	minimum = 6
	maximum = 5167
Slowest flit = 64368
Fragmentation average = 37.3515
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0227587
	minimum = 0.003 (at node 101)
	maximum = 0.0503333 (at node 91)
Accepted packet rate average = 0.0103073
	minimum = 0.006 (at node 45)
	maximum = 0.015 (at node 81)
Injected flit rate average = 0.409865
	minimum = 0.054 (at node 101)
	maximum = 0.910333 (at node 91)
Accepted flit rate average= 0.185257
	minimum = 0.106333 (at node 84)
	maximum = 0.269333 (at node 81)
Injected packet length average = 18.0092
Accepted packet length average = 17.9734
Total in-flight flits = 270998 (207438 measured)
latency change    = 0.455391
throughput change = 0.00474191
Draining remaining packets ...
Class 0:
Remaining flits: 49554 49555 49556 49557 49558 49559 49560 49561 49562 49563 [...] (240132 flits)
Measured flits: 250344 250345 250346 250347 250348 250349 250350 250351 250352 250353 [...] (194519 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (210253 flits)
Measured flits: 250344 250345 250346 250347 250348 250349 250350 250351 250352 250353 [...] (178557 flits)
Class 0:
Remaining flits: 54054 54055 54056 54057 54058 54059 54060 54061 54062 54063 [...] (180929 flits)
Measured flits: 250344 250345 250346 250347 250348 250349 250350 250351 250352 250353 [...] (159974 flits)
Class 0:
Remaining flits: 101214 101215 101216 101217 101218 101219 101220 101221 101222 101223 [...] (151260 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (137651 flits)
Class 0:
Remaining flits: 121590 121591 121592 121593 121594 121595 121596 121597 121598 121599 [...] (121806 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (113348 flits)
Class 0:
Remaining flits: 123408 123409 123410 123411 123412 123413 123414 123415 123416 123417 [...] (93658 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (88774 flits)
Class 0:
Remaining flits: 124776 124777 124778 124779 124780 124781 124782 124783 124784 124785 [...] (66304 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (63709 flits)
Class 0:
Remaining flits: 152539 152540 152541 152542 152543 152544 152545 152546 152547 152548 [...] (40524 flits)
Measured flits: 250452 250453 250454 250455 250456 250457 250458 250459 250460 250461 [...] (39415 flits)
Class 0:
Remaining flits: 204984 204985 204986 204987 204988 204989 204990 204991 204992 204993 [...] (18150 flits)
Measured flits: 250830 250831 250832 250833 250834 250835 250836 250837 250838 250839 [...] (17787 flits)
Class 0:
Remaining flits: 245034 245035 245036 245037 245038 245039 245040 245041 245042 245043 [...] (2512 flits)
Measured flits: 253116 253117 253118 253119 253120 253121 253122 253123 253124 253125 [...] (2494 flits)
Time taken is 16879 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6232.58 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13999 (1 samples)
Network latency average = 6055.76 (1 samples)
	minimum = 23 (1 samples)
	maximum = 13270 (1 samples)
Flit latency average = 5320.2 (1 samples)
	minimum = 6 (1 samples)
	maximum = 13467 (1 samples)
Fragmentation average = 68.7331 (1 samples)
	minimum = 0 (1 samples)
	maximum = 173 (1 samples)
Injected packet rate average = 0.0227587 (1 samples)
	minimum = 0.003 (1 samples)
	maximum = 0.0503333 (1 samples)
Accepted packet rate average = 0.0103073 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.409865 (1 samples)
	minimum = 0.054 (1 samples)
	maximum = 0.910333 (1 samples)
Accepted flit rate average = 0.185257 (1 samples)
	minimum = 0.106333 (1 samples)
	maximum = 0.269333 (1 samples)
Injected packet size average = 18.0092 (1 samples)
Accepted packet size average = 17.9734 (1 samples)
Hops average = 5.10107 (1 samples)
Total run time 9.73627
