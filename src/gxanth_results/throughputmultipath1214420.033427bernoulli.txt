BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 278.397
	minimum = 22
	maximum = 741
Network latency average = 268.773
	minimum = 22
	maximum = 721
Slowest packet = 1258
Flit latency average = 246.188
	minimum = 5
	maximum = 735
Slowest flit = 27834
Fragmentation average = 22.7866
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0328958
	minimum = 0.02 (at node 62)
	maximum = 0.048 (at node 157)
Accepted packet rate average = 0.0147917
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.586443
	minimum = 0.355 (at node 62)
	maximum = 0.852 (at node 157)
Accepted flit rate average= 0.27313
	minimum = 0.134 (at node 174)
	maximum = 0.436 (at node 152)
Injected packet length average = 17.8273
Accepted packet length average = 18.4651
Total in-flight flits = 61301 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 529.314
	minimum = 22
	maximum = 1595
Network latency average = 516.583
	minimum = 22
	maximum = 1567
Slowest packet = 2447
Flit latency average = 492.311
	minimum = 5
	maximum = 1557
Slowest flit = 48409
Fragmentation average = 23.9515
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0317422
	minimum = 0.0205 (at node 160)
	maximum = 0.0415 (at node 151)
Accepted packet rate average = 0.0152396
	minimum = 0.009 (at node 83)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.569065
	minimum = 0.369 (at node 160)
	maximum = 0.747 (at node 151)
Accepted flit rate average= 0.277146
	minimum = 0.162 (at node 116)
	maximum = 0.3975 (at node 152)
Injected packet length average = 17.9277
Accepted packet length average = 18.1859
Total in-flight flits = 113554 (0 measured)
latency change    = 0.474041
throughput change = 0.0144892
Class 0:
Packet latency average = 1316.58
	minimum = 22
	maximum = 2317
Network latency average = 1288.37
	minimum = 22
	maximum = 2219
Slowest packet = 4090
Flit latency average = 1270.71
	minimum = 5
	maximum = 2202
Slowest flit = 84761
Fragmentation average = 21.4473
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0231719
	minimum = 0.004 (at node 98)
	maximum = 0.041 (at node 33)
Accepted packet rate average = 0.0149167
	minimum = 0.005 (at node 22)
	maximum = 0.025 (at node 151)
Injected flit rate average = 0.418896
	minimum = 0.073 (at node 98)
	maximum = 0.739 (at node 33)
Accepted flit rate average= 0.267932
	minimum = 0.118 (at node 22)
	maximum = 0.441 (at node 151)
Injected packet length average = 18.0778
Accepted packet length average = 17.9619
Total in-flight flits = 143507 (0 measured)
latency change    = 0.597962
throughput change = 0.0343876
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 822.72
	minimum = 25
	maximum = 1774
Network latency average = 116.976
	minimum = 22
	maximum = 975
Slowest packet = 16827
Flit latency average = 1745.32
	minimum = 5
	maximum = 2867
Slowest flit = 107657
Fragmentation average = 6.112
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0167031
	minimum = 0.002 (at node 36)
	maximum = 0.042 (at node 119)
Accepted packet rate average = 0.0150833
	minimum = 0.006 (at node 79)
	maximum = 0.024 (at node 99)
Injected flit rate average = 0.300073
	minimum = 0.036 (at node 36)
	maximum = 0.743 (at node 119)
Accepted flit rate average= 0.270479
	minimum = 0.108 (at node 79)
	maximum = 0.44 (at node 99)
Injected packet length average = 17.9651
Accepted packet length average = 17.9323
Total in-flight flits = 149409 (55549 measured)
latency change    = 0.600275
throughput change = 0.00941616
Class 0:
Packet latency average = 1559.52
	minimum = 25
	maximum = 3212
Network latency average = 673.482
	minimum = 22
	maximum = 1949
Slowest packet = 16827
Flit latency average = 1958.78
	minimum = 5
	maximum = 3606
Slowest flit = 157212
Fragmentation average = 10.5965
	minimum = 0
	maximum = 85
Injected packet rate average = 0.0157526
	minimum = 0.006 (at node 61)
	maximum = 0.0305 (at node 113)
Accepted packet rate average = 0.0149349
	minimum = 0.009 (at node 79)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.283445
	minimum = 0.1045 (at node 184)
	maximum = 0.546 (at node 113)
Accepted flit rate average= 0.268911
	minimum = 0.1645 (at node 79)
	maximum = 0.405 (at node 129)
Injected packet length average = 17.9936
Accepted packet length average = 18.0056
Total in-flight flits = 149379 (100594 measured)
latency change    = 0.472452
throughput change = 0.00582983
Class 0:
Packet latency average = 2352.05
	minimum = 25
	maximum = 3934
Network latency average = 1410.69
	minimum = 22
	maximum = 2969
Slowest packet = 16827
Flit latency average = 2136.69
	minimum = 5
	maximum = 4423
Slowest flit = 176490
Fragmentation average = 14.7485
	minimum = 0
	maximum = 155
Injected packet rate average = 0.0157031
	minimum = 0.00733333 (at node 111)
	maximum = 0.0296667 (at node 113)
Accepted packet rate average = 0.0149826
	minimum = 0.01 (at node 79)
	maximum = 0.0203333 (at node 165)
Injected flit rate average = 0.282675
	minimum = 0.132 (at node 111)
	maximum = 0.529 (at node 113)
Accepted flit rate average= 0.269486
	minimum = 0.18 (at node 79)
	maximum = 0.369 (at node 165)
Injected packet length average = 18.0012
Accepted packet length average = 17.9866
Total in-flight flits = 151543 (133542 measured)
latency change    = 0.336954
throughput change = 0.0021324
Draining remaining packets ...
Class 0:
Remaining flits: 214050 214051 214052 214053 214054 214055 220140 220141 220142 220143 [...] (101430 flits)
Measured flits: 301392 301393 301394 301395 301396 301397 301398 301399 301400 301401 [...] (97941 flits)
Class 0:
Remaining flits: 225348 225349 225350 225351 225352 225353 225354 225355 225356 225357 [...] (53044 flits)
Measured flits: 305712 305713 305714 305715 305716 305717 305718 305719 305720 305721 [...] (52926 flits)
Class 0:
Remaining flits: 327998 327999 328000 328001 328002 328003 328004 328005 328006 328007 [...] (7576 flits)
Measured flits: 327998 327999 328000 328001 328002 328003 328004 328005 328006 328007 [...] (7576 flits)
Time taken is 9690 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3854.41 (1 samples)
	minimum = 25 (1 samples)
	maximum = 6777 (1 samples)
Network latency average = 2736.94 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5657 (1 samples)
Flit latency average = 2618.29 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5930 (1 samples)
Fragmentation average = 17.8868 (1 samples)
	minimum = 0 (1 samples)
	maximum = 155 (1 samples)
Injected packet rate average = 0.0157031 (1 samples)
	minimum = 0.00733333 (1 samples)
	maximum = 0.0296667 (1 samples)
Accepted packet rate average = 0.0149826 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.282675 (1 samples)
	minimum = 0.132 (1 samples)
	maximum = 0.529 (1 samples)
Accepted flit rate average = 0.269486 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.369 (1 samples)
Injected packet size average = 18.0012 (1 samples)
Accepted packet size average = 17.9866 (1 samples)
Hops average = 5.05678 (1 samples)
Total run time 8.28489
