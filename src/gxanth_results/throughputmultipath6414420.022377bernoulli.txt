BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 219.058
	minimum = 22
	maximum = 671
Network latency average = 213.75
	minimum = 22
	maximum = 671
Slowest packet = 1345
Flit latency average = 183.149
	minimum = 5
	maximum = 654
Slowest flit = 24227
Fragmentation average = 36.9191
	minimum = 0
	maximum = 320
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.0136458
	minimum = 0.006 (at node 41)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.253083
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.5466
Total in-flight flits = 28754 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 386.409
	minimum = 22
	maximum = 1231
Network latency average = 380.886
	minimum = 22
	maximum = 1198
Slowest packet = 3296
Flit latency average = 349.268
	minimum = 5
	maximum = 1229
Slowest flit = 58027
Fragmentation average = 38.0722
	minimum = 0
	maximum = 332
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143854
	minimum = 0.008 (at node 116)
	maximum = 0.0205 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.262242
	minimum = 0.144 (at node 116)
	maximum = 0.369 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.2297
Total in-flight flits = 53055 (0 measured)
latency change    = 0.433094
throughput change = 0.0349252
Class 0:
Packet latency average = 844.585
	minimum = 22
	maximum = 1552
Network latency average = 838.346
	minimum = 22
	maximum = 1534
Slowest packet = 6144
Flit latency average = 809.404
	minimum = 5
	maximum = 1517
Slowest flit = 110609
Fragmentation average = 37.8938
	minimum = 0
	maximum = 279
Injected packet rate average = 0.0222552
	minimum = 0.013 (at node 23)
	maximum = 0.034 (at node 48)
Accepted packet rate average = 0.015151
	minimum = 0.007 (at node 61)
	maximum = 0.029 (at node 16)
Injected flit rate average = 0.400745
	minimum = 0.234 (at node 23)
	maximum = 0.612 (at node 48)
Accepted flit rate average= 0.273578
	minimum = 0.126 (at node 61)
	maximum = 0.538 (at node 16)
Injected packet length average = 18.0068
Accepted packet length average = 18.0567
Total in-flight flits = 77442 (0 measured)
latency change    = 0.542486
throughput change = 0.0414358
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 40.3644
	minimum = 22
	maximum = 110
Network latency average = 35.1102
	minimum = 22
	maximum = 77
Slowest packet = 13024
Flit latency average = 1103.11
	minimum = 5
	maximum = 1961
Slowest flit = 155229
Fragmentation average = 6.50282
	minimum = 0
	maximum = 42
Injected packet rate average = 0.0229583
	minimum = 0.013 (at node 43)
	maximum = 0.033 (at node 59)
Accepted packet rate average = 0.0153281
	minimum = 0.006 (at node 105)
	maximum = 0.025 (at node 59)
Injected flit rate average = 0.412031
	minimum = 0.234 (at node 43)
	maximum = 0.603 (at node 105)
Accepted flit rate average= 0.276297
	minimum = 0.12 (at node 1)
	maximum = 0.446 (at node 0)
Injected packet length average = 17.9469
Accepted packet length average = 18.0255
Total in-flight flits = 103737 (72925 measured)
latency change    = 19.924
throughput change = 0.00983996
Class 0:
Packet latency average = 898.268
	minimum = 22
	maximum = 1993
Network latency average = 892.845
	minimum = 22
	maximum = 1988
Slowest packet = 12877
Flit latency average = 1256.2
	minimum = 5
	maximum = 2327
Slowest flit = 194381
Fragmentation average = 26.2339
	minimum = 0
	maximum = 200
Injected packet rate average = 0.0224818
	minimum = 0.0145 (at node 51)
	maximum = 0.032 (at node 139)
Accepted packet rate average = 0.015362
	minimum = 0.009 (at node 86)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.404633
	minimum = 0.261 (at node 171)
	maximum = 0.57 (at node 139)
Accepted flit rate average= 0.276766
	minimum = 0.1605 (at node 86)
	maximum = 0.39 (at node 129)
Injected packet length average = 17.9983
Accepted packet length average = 18.0163
Total in-flight flits = 126558 (126092 measured)
latency change    = 0.955064
throughput change = 0.00169367
Class 0:
Packet latency average = 1437.54
	minimum = 22
	maximum = 2847
Network latency average = 1431.95
	minimum = 22
	maximum = 2847
Slowest packet = 13339
Flit latency average = 1403.37
	minimum = 5
	maximum = 2830
Slowest flit = 240119
Fragmentation average = 40.0324
	minimum = 0
	maximum = 361
Injected packet rate average = 0.0225382
	minimum = 0.0146667 (at node 171)
	maximum = 0.03 (at node 139)
Accepted packet rate average = 0.0153872
	minimum = 0.0106667 (at node 36)
	maximum = 0.0206667 (at node 103)
Injected flit rate average = 0.405694
	minimum = 0.264 (at node 171)
	maximum = 0.541333 (at node 151)
Accepted flit rate average= 0.277747
	minimum = 0.180333 (at node 36)
	maximum = 0.375333 (at node 103)
Injected packet length average = 18.0003
Accepted packet length average = 18.0505
Total in-flight flits = 151136 (151136 measured)
latency change    = 0.375137
throughput change = 0.00353165
Draining remaining packets ...
Class 0:
Remaining flits: 266830 266831 268350 268351 268352 268353 268354 268355 268356 268357 [...] (103890 flits)
Measured flits: 266830 266831 268350 268351 268352 268353 268354 268355 268356 268357 [...] (103890 flits)
Class 0:
Remaining flits: 319792 319793 319794 319795 319796 319797 319798 319799 319800 319801 [...] (57589 flits)
Measured flits: 319792 319793 319794 319795 319796 319797 319798 319799 319800 319801 [...] (57589 flits)
Class 0:
Remaining flits: 347148 347149 347150 347151 347152 347153 347154 347155 347156 347157 [...] (12605 flits)
Measured flits: 347148 347149 347150 347151 347152 347153 347154 347155 347156 347157 [...] (12605 flits)
Time taken is 9761 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2283.19 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4844 (1 samples)
Network latency average = 2277.45 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4844 (1 samples)
Flit latency average = 2025.85 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4827 (1 samples)
Fragmentation average = 51.1716 (1 samples)
	minimum = 0 (1 samples)
	maximum = 619 (1 samples)
Injected packet rate average = 0.0225382 (1 samples)
	minimum = 0.0146667 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.0153872 (1 samples)
	minimum = 0.0106667 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.405694 (1 samples)
	minimum = 0.264 (1 samples)
	maximum = 0.541333 (1 samples)
Accepted flit rate average = 0.277747 (1 samples)
	minimum = 0.180333 (1 samples)
	maximum = 0.375333 (1 samples)
Injected packet size average = 18.0003 (1 samples)
Accepted packet size average = 18.0505 (1 samples)
Hops average = 5.08088 (1 samples)
Total run time 9.1523
