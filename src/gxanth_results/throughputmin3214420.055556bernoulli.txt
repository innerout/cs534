BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055556
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 331.253
	minimum = 22
	maximum = 897
Network latency average = 301.131
	minimum = 22
	maximum = 862
Slowest packet = 868
Flit latency average = 266.249
	minimum = 5
	maximum = 858
Slowest flit = 19416
Fragmentation average = 86.2735
	minimum = 0
	maximum = 411
Injected packet rate average = 0.0501354
	minimum = 0.036 (at node 20)
	maximum = 0.056 (at node 23)
Accepted packet rate average = 0.0156927
	minimum = 0.008 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.89451
	minimum = 0.648 (at node 20)
	maximum = 1 (at node 77)
Accepted flit rate average= 0.305797
	minimum = 0.161 (at node 41)
	maximum = 0.455 (at node 44)
Injected packet length average = 17.8419
Accepted packet length average = 19.4866
Total in-flight flits = 114555 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 625.081
	minimum = 22
	maximum = 1853
Network latency average = 575.492
	minimum = 22
	maximum = 1729
Slowest packet = 1970
Flit latency average = 531.572
	minimum = 5
	maximum = 1744
Slowest flit = 38975
Fragmentation average = 116.467
	minimum = 0
	maximum = 462
Injected packet rate average = 0.050151
	minimum = 0.041 (at node 100)
	maximum = 0.0555 (at node 30)
Accepted packet rate average = 0.0165
	minimum = 0.009 (at node 79)
	maximum = 0.025 (at node 152)
Injected flit rate average = 0.898495
	minimum = 0.731 (at node 148)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.310375
	minimum = 0.1895 (at node 116)
	maximum = 0.465 (at node 152)
Injected packet length average = 17.9158
Accepted packet length average = 18.8106
Total in-flight flits = 227514 (0 measured)
latency change    = 0.470064
throughput change = 0.0147503
Class 0:
Packet latency average = 1614.15
	minimum = 28
	maximum = 2644
Network latency average = 1520.88
	minimum = 22
	maximum = 2560
Slowest packet = 2508
Flit latency average = 1484.12
	minimum = 5
	maximum = 2694
Slowest flit = 46252
Fragmentation average = 161.923
	minimum = 0
	maximum = 429
Injected packet rate average = 0.046474
	minimum = 0.014 (at node 4)
	maximum = 0.056 (at node 1)
Accepted packet rate average = 0.0162188
	minimum = 0.009 (at node 4)
	maximum = 0.033 (at node 34)
Injected flit rate average = 0.837203
	minimum = 0.249 (at node 28)
	maximum = 1 (at node 1)
Accepted flit rate average= 0.290411
	minimum = 0.152 (at node 77)
	maximum = 0.627 (at node 34)
Injected packet length average = 18.0145
Accepted packet length average = 17.9059
Total in-flight flits = 332441 (0 measured)
latency change    = 0.612749
throughput change = 0.0687423
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 628.165
	minimum = 34
	maximum = 1809
Network latency average = 85.0915
	minimum = 22
	maximum = 946
Slowest packet = 28259
Flit latency average = 2249.85
	minimum = 5
	maximum = 3453
Slowest flit = 90240
Fragmentation average = 5.04878
	minimum = 0
	maximum = 18
Injected packet rate average = 0.0317292
	minimum = 0.002 (at node 124)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0148542
	minimum = 0.006 (at node 86)
	maximum = 0.026 (at node 3)
Injected flit rate average = 0.57288
	minimum = 0.036 (at node 124)
	maximum = 1 (at node 17)
Accepted flit rate average= 0.26875
	minimum = 0.112 (at node 86)
	maximum = 0.491 (at node 129)
Injected packet length average = 18.0553
Accepted packet length average = 18.0926
Total in-flight flits = 390839 (107015 measured)
latency change    = 1.56963
throughput change = 0.0806008
Class 0:
Packet latency average = 1075.94
	minimum = 34
	maximum = 2762
Network latency average = 293.379
	minimum = 22
	maximum = 1920
Slowest packet = 28259
Flit latency average = 2614.01
	minimum = 5
	maximum = 4123
Slowest flit = 146303
Fragmentation average = 6.0347
	minimum = 0
	maximum = 37
Injected packet rate average = 0.024888
	minimum = 0.008 (at node 99)
	maximum = 0.0445 (at node 33)
Accepted packet rate average = 0.0149896
	minimum = 0.0075 (at node 36)
	maximum = 0.0225 (at node 128)
Injected flit rate average = 0.448695
	minimum = 0.1505 (at node 72)
	maximum = 0.801 (at node 33)
Accepted flit rate average= 0.267865
	minimum = 0.1465 (at node 86)
	maximum = 0.41 (at node 128)
Injected packet length average = 18.0286
Accepted packet length average = 17.87
Total in-flight flits = 402165 (166832 measured)
latency change    = 0.41617
throughput change = 0.00330546
Class 0:
Packet latency average = 1576.67
	minimum = 34
	maximum = 3849
Network latency average = 608.013
	minimum = 22
	maximum = 2880
Slowest packet = 28259
Flit latency average = 2943.8
	minimum = 5
	maximum = 4969
Slowest flit = 174720
Fragmentation average = 6.37523
	minimum = 0
	maximum = 37
Injected packet rate average = 0.0219253
	minimum = 0.006 (at node 160)
	maximum = 0.035 (at node 33)
Accepted packet rate average = 0.0150816
	minimum = 0.011 (at node 35)
	maximum = 0.0213333 (at node 59)
Injected flit rate average = 0.395523
	minimum = 0.113667 (at node 160)
	maximum = 0.630667 (at node 33)
Accepted flit rate average= 0.269193
	minimum = 0.185 (at node 126)
	maximum = 0.382333 (at node 128)
Injected packet length average = 18.0395
Accepted packet length average = 17.8491
Total in-flight flits = 405482 (218302 measured)
latency change    = 0.317587
throughput change = 0.00493373
Class 0:
Packet latency average = 2246.67
	minimum = 34
	maximum = 5105
Network latency average = 1151.77
	minimum = 22
	maximum = 3954
Slowest packet = 28259
Flit latency average = 3271.08
	minimum = 5
	maximum = 6025
Slowest flit = 164605
Fragmentation average = 13.2358
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0201914
	minimum = 0.008 (at node 144)
	maximum = 0.031 (at node 161)
Accepted packet rate average = 0.0149727
	minimum = 0.0115 (at node 86)
	maximum = 0.01975 (at node 128)
Injected flit rate average = 0.363832
	minimum = 0.144 (at node 144)
	maximum = 0.5585 (at node 161)
Accepted flit rate average= 0.267785
	minimum = 0.1985 (at node 86)
	maximum = 0.361 (at node 128)
Injected packet length average = 18.0192
Accepted packet length average = 17.8849
Total in-flight flits = 406646 (264818 measured)
latency change    = 0.298221
throughput change = 0.00525627
Draining remaining packets ...
Class 0:
Remaining flits: 197820 197821 197822 197823 197824 197825 197826 197827 197828 197829 [...] (356688 flits)
Measured flits: 507384 507385 507386 507387 507388 507389 507390 507391 507392 507393 [...] (256572 flits)
Class 0:
Remaining flits: 197820 197821 197822 197823 197824 197825 197826 197827 197828 197829 [...] (306396 flits)
Measured flits: 507384 507385 507386 507387 507388 507389 507390 507391 507392 507393 [...] (240781 flits)
Class 0:
Remaining flits: 270774 270775 270776 270777 270778 270779 270780 270781 270782 270783 [...] (256793 flits)
Measured flits: 507384 507385 507386 507387 507388 507389 507390 507391 507392 507393 [...] (218207 flits)
Class 0:
Remaining flits: 310086 310087 310088 310089 310090 310091 310092 310093 310094 310095 [...] (207367 flits)
Measured flits: 507402 507403 507404 507405 507406 507407 507408 507409 507410 507411 [...] (188536 flits)
Class 0:
Remaining flits: 329058 329059 329060 329061 329062 329063 329064 329065 329066 329067 [...] (159755 flits)
Measured flits: 507402 507403 507404 507405 507406 507407 507408 507409 507410 507411 [...] (151961 flits)
Class 0:
Remaining flits: 375912 375913 375914 375915 375916 375917 375918 375919 375920 375921 [...] (112234 flits)
Measured flits: 507402 507403 507404 507405 507406 507407 507408 507409 507410 507411 [...] (109875 flits)
Class 0:
Remaining flits: 447048 447049 447050 447051 447052 447053 447054 447055 447056 447057 [...] (64937 flits)
Measured flits: 507402 507403 507404 507405 507406 507407 507408 507409 507410 507411 [...] (64294 flits)
Class 0:
Remaining flits: 470934 470935 470936 470937 470938 470939 470940 470941 470942 470943 [...] (19007 flits)
Measured flits: 509610 509611 509612 509613 509614 509615 512478 512479 512480 512481 [...] (18867 flits)
Class 0:
Remaining flits: 540504 540505 540506 540507 540508 540509 540510 540511 540512 540513 [...] (837 flits)
Measured flits: 540504 540505 540506 540507 540508 540509 540510 540511 540512 540513 [...] (837 flits)
Time taken is 16602 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 8426.34 (1 samples)
	minimum = 34 (1 samples)
	maximum = 13297 (1 samples)
Network latency average = 7235.44 (1 samples)
	minimum = 22 (1 samples)
	maximum = 13118 (1 samples)
Flit latency average = 5898.39 (1 samples)
	minimum = 5 (1 samples)
	maximum = 13101 (1 samples)
Fragmentation average = 56.3634 (1 samples)
	minimum = 0 (1 samples)
	maximum = 956 (1 samples)
Injected packet rate average = 0.0201914 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0149727 (1 samples)
	minimum = 0.0115 (1 samples)
	maximum = 0.01975 (1 samples)
Injected flit rate average = 0.363832 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.5585 (1 samples)
Accepted flit rate average = 0.267785 (1 samples)
	minimum = 0.1985 (1 samples)
	maximum = 0.361 (1 samples)
Injected packet size average = 18.0192 (1 samples)
Accepted packet size average = 17.8849 (1 samples)
Hops average = 5.12426 (1 samples)
Total run time 19.5312
