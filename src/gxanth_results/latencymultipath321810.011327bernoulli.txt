BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 158.821
	minimum = 23
	maximum = 863
Network latency average = 156.846
	minimum = 23
	maximum = 863
Slowest packet = 69
Flit latency average = 97.3328
	minimum = 6
	maximum = 846
Slowest flit = 1259
Fragmentation average = 94.5068
	minimum = 0
	maximum = 819
Injected packet rate average = 0.0111875
	minimum = 0.002 (at node 119)
	maximum = 0.02 (at node 162)
Accepted packet rate average = 0.00839583
	minimum = 0.001 (at node 41)
	maximum = 0.015 (at node 20)
Injected flit rate average = 0.199344
	minimum = 0.036 (at node 119)
	maximum = 0.36 (at node 162)
Accepted flit rate average= 0.164745
	minimum = 0.036 (at node 174)
	maximum = 0.291 (at node 165)
Injected packet length average = 17.8184
Accepted packet length average = 19.6222
Total in-flight flits = 7051 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 226.53
	minimum = 23
	maximum = 1739
Network latency average = 221.874
	minimum = 23
	maximum = 1735
Slowest packet = 211
Flit latency average = 146.609
	minimum = 6
	maximum = 1718
Slowest flit = 3815
Fragmentation average = 122.004
	minimum = 0
	maximum = 1548
Injected packet rate average = 0.0109948
	minimum = 0.0035 (at node 126)
	maximum = 0.0175 (at node 24)
Accepted packet rate average = 0.0092474
	minimum = 0.0045 (at node 174)
	maximum = 0.0165 (at node 22)
Injected flit rate average = 0.197036
	minimum = 0.063 (at node 126)
	maximum = 0.315 (at node 24)
Accepted flit rate average= 0.174339
	minimum = 0.0875 (at node 174)
	maximum = 0.3035 (at node 22)
Injected packet length average = 17.9209
Accepted packet length average = 18.8527
Total in-flight flits = 9050 (0 measured)
latency change    = 0.298898
throughput change = 0.0550294
Class 0:
Packet latency average = 348.361
	minimum = 23
	maximum = 2415
Network latency average = 335.589
	minimum = 23
	maximum = 2415
Slowest packet = 1012
Flit latency average = 244.413
	minimum = 6
	maximum = 2398
Slowest flit = 18233
Fragmentation average = 161.812
	minimum = 0
	maximum = 1735
Injected packet rate average = 0.0110417
	minimum = 0 (at node 89)
	maximum = 0.022 (at node 183)
Accepted packet rate average = 0.0100938
	minimum = 0.002 (at node 153)
	maximum = 0.018 (at node 119)
Injected flit rate average = 0.198083
	minimum = 0 (at node 96)
	maximum = 0.408 (at node 183)
Accepted flit rate average= 0.184328
	minimum = 0.046 (at node 153)
	maximum = 0.331 (at node 89)
Injected packet length average = 17.9396
Accepted packet length average = 18.2616
Total in-flight flits = 11819 (0 measured)
latency change    = 0.349725
throughput change = 0.0541946
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 293.446
	minimum = 24
	maximum = 1940
Network latency average = 260.901
	minimum = 24
	maximum = 925
Slowest packet = 6385
Flit latency average = 300.792
	minimum = 6
	maximum = 2912
Slowest flit = 18053
Fragmentation average = 137.859
	minimum = 0
	maximum = 805
Injected packet rate average = 0.0110885
	minimum = 0 (at node 128)
	maximum = 0.021 (at node 81)
Accepted packet rate average = 0.0103646
	minimum = 0.003 (at node 163)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.199099
	minimum = 0 (at node 128)
	maximum = 0.379 (at node 153)
Accepted flit rate average= 0.189391
	minimum = 0.036 (at node 163)
	maximum = 0.387 (at node 16)
Injected packet length average = 17.9554
Accepted packet length average = 18.2729
Total in-flight flits = 13796 (12320 measured)
latency change    = 0.187139
throughput change = 0.0267305
Class 0:
Packet latency average = 396.166
	minimum = 24
	maximum = 2719
Network latency average = 347.252
	minimum = 24
	maximum = 1786
Slowest packet = 6385
Flit latency average = 326.101
	minimum = 6
	maximum = 4063
Slowest flit = 34415
Fragmentation average = 158.023
	minimum = 0
	maximum = 1125
Injected packet rate average = 0.011013
	minimum = 0 (at node 128)
	maximum = 0.0175 (at node 99)
Accepted packet rate average = 0.0105234
	minimum = 0.005 (at node 163)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.197948
	minimum = 0 (at node 128)
	maximum = 0.3205 (at node 99)
Accepted flit rate average= 0.191312
	minimum = 0.097 (at node 163)
	maximum = 0.3625 (at node 16)
Injected packet length average = 17.974
Accepted packet length average = 18.1797
Total in-flight flits = 14513 (14227 measured)
latency change    = 0.259286
throughput change = 0.0100457
Class 0:
Packet latency average = 469.668
	minimum = 24
	maximum = 3444
Network latency average = 397.048
	minimum = 24
	maximum = 2727
Slowest packet = 6385
Flit latency average = 343.83
	minimum = 6
	maximum = 4751
Slowest flit = 35309
Fragmentation average = 170.211
	minimum = 0
	maximum = 1777
Injected packet rate average = 0.0110434
	minimum = 0.00366667 (at node 128)
	maximum = 0.0156667 (at node 99)
Accepted packet rate average = 0.0105573
	minimum = 0.00533333 (at node 4)
	maximum = 0.0176667 (at node 16)
Injected flit rate average = 0.198627
	minimum = 0.0693333 (at node 128)
	maximum = 0.285 (at node 99)
Accepted flit rate average= 0.191229
	minimum = 0.0966667 (at node 4)
	maximum = 0.315667 (at node 16)
Injected packet length average = 17.986
Accepted packet length average = 18.1135
Total in-flight flits = 16259 (16206 measured)
latency change    = 0.156498
throughput change = 0.000435777
Class 0:
Packet latency average = 512.703
	minimum = 24
	maximum = 3798
Network latency average = 432.787
	minimum = 24
	maximum = 3798
Slowest packet = 6472
Flit latency average = 361.048
	minimum = 6
	maximum = 4751
Slowest flit = 35309
Fragmentation average = 177.526
	minimum = 0
	maximum = 2823
Injected packet rate average = 0.0110547
	minimum = 0.00275 (at node 128)
	maximum = 0.01575 (at node 125)
Accepted packet rate average = 0.0105951
	minimum = 0.007 (at node 86)
	maximum = 0.01525 (at node 16)
Injected flit rate average = 0.198914
	minimum = 0.052 (at node 128)
	maximum = 0.2835 (at node 125)
Accepted flit rate average= 0.191482
	minimum = 0.1235 (at node 86)
	maximum = 0.2735 (at node 16)
Injected packet length average = 17.9936
Accepted packet length average = 18.0728
Total in-flight flits = 17599 (17594 measured)
latency change    = 0.0839384
throughput change = 0.00131921
Class 0:
Packet latency average = 549.929
	minimum = 24
	maximum = 4692
Network latency average = 461.839
	minimum = 24
	maximum = 4053
Slowest packet = 6756
Flit latency average = 378.327
	minimum = 6
	maximum = 4751
Slowest flit = 35309
Fragmentation average = 181.405
	minimum = 0
	maximum = 2823
Injected packet rate average = 0.0109875
	minimum = 0.0044 (at node 128)
	maximum = 0.0156 (at node 125)
Accepted packet rate average = 0.0106156
	minimum = 0.007 (at node 36)
	maximum = 0.0142 (at node 16)
Injected flit rate average = 0.197605
	minimum = 0.0822 (at node 128)
	maximum = 0.2808 (at node 125)
Accepted flit rate average= 0.191516
	minimum = 0.1272 (at node 36)
	maximum = 0.2568 (at node 16)
Injected packet length average = 17.9845
Accepted packet length average = 18.0409
Total in-flight flits = 17990 (17990 measured)
latency change    = 0.0676914
throughput change = 0.00017677
Class 0:
Packet latency average = 582.985
	minimum = 23
	maximum = 4692
Network latency average = 480.014
	minimum = 23
	maximum = 4053
Slowest packet = 6756
Flit latency average = 389.308
	minimum = 6
	maximum = 4751
Slowest flit = 35309
Fragmentation average = 182.801
	minimum = 0
	maximum = 2823
Injected packet rate average = 0.0110174
	minimum = 0.00616667 (at node 128)
	maximum = 0.015 (at node 125)
Accepted packet rate average = 0.0105668
	minimum = 0.00716667 (at node 36)
	maximum = 0.0135 (at node 128)
Injected flit rate average = 0.198228
	minimum = 0.111333 (at node 128)
	maximum = 0.27 (at node 125)
Accepted flit rate average= 0.190754
	minimum = 0.129167 (at node 36)
	maximum = 0.2485 (at node 128)
Injected packet length average = 17.9924
Accepted packet length average = 18.0522
Total in-flight flits = 20616 (20616 measured)
latency change    = 0.0567019
throughput change = 0.00399092
Class 0:
Packet latency average = 615.958
	minimum = 23
	maximum = 4817
Network latency average = 500.159
	minimum = 23
	maximum = 4814
Slowest packet = 10653
Flit latency average = 405.059
	minimum = 6
	maximum = 4797
Slowest flit = 191771
Fragmentation average = 184.314
	minimum = 0
	maximum = 2823
Injected packet rate average = 0.0109211
	minimum = 0.00728571 (at node 80)
	maximum = 0.0142857 (at node 113)
Accepted packet rate average = 0.010567
	minimum = 0.00771429 (at node 89)
	maximum = 0.0134286 (at node 128)
Injected flit rate average = 0.196412
	minimum = 0.131 (at node 80)
	maximum = 0.259571 (at node 158)
Accepted flit rate average= 0.190517
	minimum = 0.134571 (at node 89)
	maximum = 0.246714 (at node 128)
Injected packet length average = 17.9846
Accepted packet length average = 18.0295
Total in-flight flits = 20058 (20058 measured)
latency change    = 0.0535309
throughput change = 0.00124518
Draining all recorded packets ...
Class 0:
Remaining flits: 229428 229429 229430 229431 229432 229433 229434 229435 229436 229437 [...] (21379 flits)
Measured flits: 229428 229429 229430 229431 229432 229433 229434 229435 229436 229437 [...] (7595 flits)
Class 0:
Remaining flits: 231890 231891 231892 231893 278703 278704 278705 278706 278707 278708 [...] (22442 flits)
Measured flits: 231890 231891 231892 231893 278703 278704 278705 278706 278707 278708 [...] (2862 flits)
Class 0:
Remaining flits: 281700 281701 281702 281703 281704 281705 281706 281707 281708 281709 [...] (22471 flits)
Measured flits: 281700 281701 281702 281703 281704 281705 281706 281707 281708 281709 [...] (903 flits)
Class 0:
Remaining flits: 340270 340271 354168 354169 354170 354171 354172 354173 354174 354175 [...] (22184 flits)
Measured flits: 340270 340271 354168 354169 354170 354171 354172 354173 354174 354175 [...] (303 flits)
Class 0:
Remaining flits: 354168 354169 354170 354171 354172 354173 354174 354175 354176 354177 [...] (23367 flits)
Measured flits: 354168 354169 354170 354171 354172 354173 354174 354175 354176 354177 [...] (112 flits)
Class 0:
Remaining flits: 363092 363093 363094 363095 378342 378343 378344 378345 378346 378347 [...] (23670 flits)
Measured flits: 363092 363093 363094 363095 378342 378343 378344 378345 378346 378347 [...] (22 flits)
Class 0:
Remaining flits: 363094 363095 378343 378344 378345 378346 378347 378348 378349 378350 [...] (25392 flits)
Measured flits: 363094 363095 378343 378344 378345 378346 378347 378348 378349 378350 [...] (19 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 493464 493465 493466 493467 493468 493469 529758 529759 529760 529761 [...] (803 flits)
Measured flits: (0 flits)
Time taken is 18651 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 772.62 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7743 (1 samples)
Network latency average = 585.327 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7558 (1 samples)
Flit latency average = 521.361 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7800 (1 samples)
Fragmentation average = 196.32 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5071 (1 samples)
Injected packet rate average = 0.0109211 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0142857 (1 samples)
Accepted packet rate average = 0.010567 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.0134286 (1 samples)
Injected flit rate average = 0.196412 (1 samples)
	minimum = 0.131 (1 samples)
	maximum = 0.259571 (1 samples)
Accepted flit rate average = 0.190517 (1 samples)
	minimum = 0.134571 (1 samples)
	maximum = 0.246714 (1 samples)
Injected packet size average = 17.9846 (1 samples)
Accepted packet size average = 18.0295 (1 samples)
Hops average = 5.07034 (1 samples)
Total run time 16.1698
