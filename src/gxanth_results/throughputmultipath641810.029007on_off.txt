BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 369.755
	minimum = 23
	maximum = 974
Network latency average = 277.984
	minimum = 23
	maximum = 879
Slowest packet = 3
Flit latency average = 195.373
	minimum = 6
	maximum = 959
Slowest flit = 2230
Fragmentation average = 185.633
	minimum = 0
	maximum = 687
Injected packet rate average = 0.0221875
	minimum = 0.001 (at node 63)
	maximum = 0.055 (at node 142)
Accepted packet rate average = 0.00954167
	minimum = 0.003 (at node 23)
	maximum = 0.016 (at node 70)
Injected flit rate average = 0.394771
	minimum = 0.018 (at node 63)
	maximum = 0.989 (at node 142)
Accepted flit rate average= 0.206953
	minimum = 0.078 (at node 135)
	maximum = 0.355 (at node 70)
Injected packet length average = 17.7925
Accepted packet length average = 21.6894
Total in-flight flits = 36963 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 642.56
	minimum = 23
	maximum = 1966
Network latency average = 479.167
	minimum = 23
	maximum = 1933
Slowest packet = 183
Flit latency average = 367.486
	minimum = 6
	maximum = 1916
Slowest flit = 3311
Fragmentation average = 249.687
	minimum = 0
	maximum = 1648
Injected packet rate average = 0.0195729
	minimum = 0.0025 (at node 63)
	maximum = 0.0385 (at node 142)
Accepted packet rate average = 0.0109792
	minimum = 0.006 (at node 23)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.349328
	minimum = 0.045 (at node 63)
	maximum = 0.689 (at node 142)
Accepted flit rate average= 0.217029
	minimum = 0.116 (at node 23)
	maximum = 0.3165 (at node 63)
Injected packet length average = 17.8475
Accepted packet length average = 19.7673
Total in-flight flits = 52093 (0 measured)
latency change    = 0.424559
throughput change = 0.0464248
Class 0:
Packet latency average = 1333.12
	minimum = 30
	maximum = 2925
Network latency average = 889.618
	minimum = 28
	maximum = 2852
Slowest packet = 1191
Flit latency average = 761.95
	minimum = 6
	maximum = 2835
Slowest flit = 11105
Fragmentation average = 298.597
	minimum = 1
	maximum = 2360
Injected packet rate average = 0.0126875
	minimum = 0 (at node 2)
	maximum = 0.049 (at node 117)
Accepted packet rate average = 0.0123854
	minimum = 0.004 (at node 150)
	maximum = 0.022 (at node 34)
Injected flit rate average = 0.227693
	minimum = 0 (at node 4)
	maximum = 0.877 (at node 117)
Accepted flit rate average= 0.221307
	minimum = 0.068 (at node 150)
	maximum = 0.406 (at node 34)
Injected packet length average = 17.9462
Accepted packet length average = 17.8684
Total in-flight flits = 53450 (0 measured)
latency change    = 0.518002
throughput change = 0.0193335
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1411.71
	minimum = 38
	maximum = 3781
Network latency average = 322.956
	minimum = 27
	maximum = 962
Slowest packet = 9998
Flit latency average = 938.433
	minimum = 6
	maximum = 3801
Slowest flit = 12645
Fragmentation average = 143.744
	minimum = 0
	maximum = 624
Injected packet rate average = 0.0115885
	minimum = 0 (at node 2)
	maximum = 0.039 (at node 137)
Accepted packet rate average = 0.0120781
	minimum = 0.004 (at node 71)
	maximum = 0.022 (at node 9)
Injected flit rate average = 0.207927
	minimum = 0 (at node 2)
	maximum = 0.701 (at node 137)
Accepted flit rate average= 0.216104
	minimum = 0.069 (at node 4)
	maximum = 0.406 (at node 9)
Injected packet length average = 17.9425
Accepted packet length average = 17.8922
Total in-flight flits = 52026 (25894 measured)
latency change    = 0.0556672
throughput change = 0.0240769
Class 0:
Packet latency average = 1870.25
	minimum = 38
	maximum = 4728
Network latency average = 529.097
	minimum = 27
	maximum = 1930
Slowest packet = 9998
Flit latency average = 997.226
	minimum = 6
	maximum = 4702
Slowest flit = 23353
Fragmentation average = 185.496
	minimum = 0
	maximum = 1463
Injected packet rate average = 0.0112708
	minimum = 0 (at node 2)
	maximum = 0.0335 (at node 187)
Accepted packet rate average = 0.0118177
	minimum = 0.005 (at node 20)
	maximum = 0.019 (at node 165)
Injected flit rate average = 0.202391
	minimum = 0 (at node 2)
	maximum = 0.608 (at node 187)
Accepted flit rate average= 0.211992
	minimum = 0.0955 (at node 20)
	maximum = 0.3355 (at node 165)
Injected packet length average = 17.957
Accepted packet length average = 17.9385
Total in-flight flits = 50075 (35499 measured)
latency change    = 0.245179
throughput change = 0.0193968
Class 0:
Packet latency average = 2298.55
	minimum = 38
	maximum = 5480
Network latency average = 659.081
	minimum = 27
	maximum = 2900
Slowest packet = 9998
Flit latency average = 1017.51
	minimum = 6
	maximum = 5815
Slowest flit = 13783
Fragmentation average = 204.22
	minimum = 0
	maximum = 2077
Injected packet rate average = 0.0114583
	minimum = 0 (at node 2)
	maximum = 0.0276667 (at node 65)
Accepted packet rate average = 0.0117257
	minimum = 0.00666667 (at node 20)
	maximum = 0.0186667 (at node 165)
Injected flit rate average = 0.205736
	minimum = 0 (at node 2)
	maximum = 0.496333 (at node 65)
Accepted flit rate average= 0.210715
	minimum = 0.125 (at node 2)
	maximum = 0.331 (at node 165)
Injected packet length average = 17.9552
Accepted packet length average = 17.9704
Total in-flight flits = 50968 (41577 measured)
latency change    = 0.186334
throughput change = 0.00605988
Draining remaining packets ...
Class 0:
Remaining flits: 12330 12331 12332 12333 12334 12335 12336 12337 12338 12339 [...] (17393 flits)
Measured flits: 179352 179353 179354 179355 179356 179357 179358 179359 179360 179361 [...] (13028 flits)
Class 0:
Remaining flits: 12457 12458 12459 12460 12461 12462 12463 12464 12465 12466 [...] (696 flits)
Measured flits: 209267 213800 213801 213802 213803 214864 214865 215568 215569 215570 [...] (599 flits)
Time taken is 8328 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2908.63 (1 samples)
	minimum = 38 (1 samples)
	maximum = 7432 (1 samples)
Network latency average = 1096.27 (1 samples)
	minimum = 27 (1 samples)
	maximum = 4729 (1 samples)
Flit latency average = 1402.33 (1 samples)
	minimum = 6 (1 samples)
	maximum = 8000 (1 samples)
Fragmentation average = 224.558 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3326 (1 samples)
Injected packet rate average = 0.0114583 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0276667 (1 samples)
Accepted packet rate average = 0.0117257 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0186667 (1 samples)
Injected flit rate average = 0.205736 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.496333 (1 samples)
Accepted flit rate average = 0.210715 (1 samples)
	minimum = 0.125 (1 samples)
	maximum = 0.331 (1 samples)
Injected packet size average = 17.9552 (1 samples)
Accepted packet size average = 17.9704 (1 samples)
Hops average = 5.15534 (1 samples)
Total run time 10.7877
