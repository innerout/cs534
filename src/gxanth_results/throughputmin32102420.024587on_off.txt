BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 296.161
	minimum = 23
	maximum = 964
Network latency average = 204.648
	minimum = 22
	maximum = 729
Slowest packet = 85
Flit latency average = 176.589
	minimum = 5
	maximum = 770
Slowest flit = 15870
Fragmentation average = 38.2824
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0230521
	minimum = 0 (at node 82)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.0129479
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.411286
	minimum = 0 (at node 82)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.243104
	minimum = 0.095 (at node 174)
	maximum = 0.404 (at node 44)
Injected packet length average = 17.8416
Accepted packet length average = 18.7755
Total in-flight flits = 32992 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 503.083
	minimum = 23
	maximum = 1901
Network latency average = 382.149
	minimum = 22
	maximum = 1434
Slowest packet = 85
Flit latency average = 349.978
	minimum = 5
	maximum = 1417
Slowest flit = 37295
Fragmentation average = 49.6044
	minimum = 0
	maximum = 368
Injected packet rate average = 0.0234844
	minimum = 0.002 (at node 4)
	maximum = 0.054 (at node 69)
Accepted packet rate average = 0.0140495
	minimum = 0.0085 (at node 81)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.420693
	minimum = 0.036 (at node 4)
	maximum = 0.9655 (at node 69)
Accepted flit rate average= 0.258258
	minimum = 0.1585 (at node 164)
	maximum = 0.405 (at node 152)
Injected packet length average = 17.9137
Accepted packet length average = 18.382
Total in-flight flits = 63153 (0 measured)
latency change    = 0.411309
throughput change = 0.0586764
Class 0:
Packet latency average = 1038.71
	minimum = 25
	maximum = 2440
Network latency average = 884.234
	minimum = 22
	maximum = 2022
Slowest packet = 2921
Flit latency average = 844.616
	minimum = 5
	maximum = 2120
Slowest flit = 67327
Fragmentation average = 71.3735
	minimum = 0
	maximum = 390
Injected packet rate average = 0.0248021
	minimum = 0 (at node 0)
	maximum = 0.056 (at node 80)
Accepted packet rate average = 0.0153802
	minimum = 0.005 (at node 163)
	maximum = 0.028 (at node 3)
Injected flit rate average = 0.4465
	minimum = 0 (at node 0)
	maximum = 1 (at node 60)
Accepted flit rate average= 0.277729
	minimum = 0.095 (at node 163)
	maximum = 0.499 (at node 3)
Injected packet length average = 18.0025
Accepted packet length average = 18.0576
Total in-flight flits = 95545 (0 measured)
latency change    = 0.515667
throughput change = 0.0701091
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 321.629
	minimum = 26
	maximum = 1168
Network latency average = 169.517
	minimum = 22
	maximum = 987
Slowest packet = 13795
Flit latency average = 1167.73
	minimum = 5
	maximum = 2625
Slowest flit = 110131
Fragmentation average = 16.4175
	minimum = 0
	maximum = 209
Injected packet rate average = 0.0253698
	minimum = 0 (at node 40)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0155104
	minimum = 0.006 (at node 189)
	maximum = 0.028 (at node 182)
Injected flit rate average = 0.456693
	minimum = 0 (at node 40)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.281417
	minimum = 0.136 (at node 189)
	maximum = 0.469 (at node 182)
Injected packet length average = 18.0014
Accepted packet length average = 18.1437
Total in-flight flits = 129191 (78035 measured)
latency change    = 2.22954
throughput change = 0.0131033
Class 0:
Packet latency average = 739.812
	minimum = 25
	maximum = 2384
Network latency average = 599.466
	minimum = 22
	maximum = 1964
Slowest packet = 13795
Flit latency average = 1352.23
	minimum = 5
	maximum = 3107
Slowest flit = 151751
Fragmentation average = 35.182
	minimum = 0
	maximum = 345
Injected packet rate average = 0.0246979
	minimum = 0.002 (at node 86)
	maximum = 0.0545 (at node 117)
Accepted packet rate average = 0.0154844
	minimum = 0.0085 (at node 36)
	maximum = 0.024 (at node 95)
Injected flit rate average = 0.444807
	minimum = 0.0405 (at node 86)
	maximum = 0.9805 (at node 117)
Accepted flit rate average= 0.279945
	minimum = 0.1605 (at node 36)
	maximum = 0.421 (at node 95)
Injected packet length average = 18.0099
Accepted packet length average = 18.0792
Total in-flight flits = 158758 (140484 measured)
latency change    = 0.565255
throughput change = 0.00525586
Class 0:
Packet latency average = 1272.91
	minimum = 22
	maximum = 3791
Network latency average = 1128.97
	minimum = 22
	maximum = 2989
Slowest packet = 13795
Flit latency average = 1519.14
	minimum = 5
	maximum = 3760
Slowest flit = 179117
Fragmentation average = 48.9704
	minimum = 0
	maximum = 386
Injected packet rate average = 0.0242535
	minimum = 0.00566667 (at node 171)
	maximum = 0.052 (at node 88)
Accepted packet rate average = 0.0154583
	minimum = 0.01 (at node 36)
	maximum = 0.0213333 (at node 95)
Injected flit rate average = 0.436701
	minimum = 0.102 (at node 171)
	maximum = 0.935 (at node 88)
Accepted flit rate average= 0.279243
	minimum = 0.184333 (at node 36)
	maximum = 0.384 (at node 128)
Injected packet length average = 18.0057
Accepted packet length average = 18.0642
Total in-flight flits = 186161 (182903 measured)
latency change    = 0.418804
throughput change = 0.00251486
Draining remaining packets ...
Class 0:
Remaining flits: 214307 220518 220519 220520 220521 220522 220523 220524 220525 220526 [...] (139166 flits)
Measured flits: 248688 248689 248690 248691 248692 248693 248694 248695 248696 248697 [...] (138823 flits)
Class 0:
Remaining flits: 232482 232483 232484 232485 232486 232487 250718 250719 250720 250721 [...] (92576 flits)
Measured flits: 250718 250719 250720 250721 251988 251989 251990 251991 251992 251993 [...] (92570 flits)
Class 0:
Remaining flits: 276590 276591 276592 276593 276594 276595 276596 276597 276598 276599 [...] (49113 flits)
Measured flits: 276590 276591 276592 276593 276594 276595 276596 276597 276598 276599 [...] (49113 flits)
Class 0:
Remaining flits: 299563 299564 299565 299566 299567 299568 299569 299570 299571 299572 [...] (20776 flits)
Measured flits: 299563 299564 299565 299566 299567 299568 299569 299570 299571 299572 [...] (20776 flits)
Class 0:
Remaining flits: 360480 360481 360482 360483 360484 360485 362052 362053 362054 362055 [...] (6688 flits)
Measured flits: 360480 360481 360482 360483 360484 360485 362052 362053 362054 362055 [...] (6688 flits)
Class 0:
Remaining flits: 389757 389758 389759 389760 389761 389762 389763 389764 389765 389766 [...] (1499 flits)
Measured flits: 389757 389758 389759 389760 389761 389762 389763 389764 389765 389766 [...] (1499 flits)
Time taken is 12817 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3043.96 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7957 (1 samples)
Network latency average = 2875.93 (1 samples)
	minimum = 22 (1 samples)
	maximum = 7584 (1 samples)
Flit latency average = 2557.88 (1 samples)
	minimum = 5 (1 samples)
	maximum = 7567 (1 samples)
Fragmentation average = 82.9714 (1 samples)
	minimum = 0 (1 samples)
	maximum = 450 (1 samples)
Injected packet rate average = 0.0242535 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.052 (1 samples)
Accepted packet rate average = 0.0154583 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0213333 (1 samples)
Injected flit rate average = 0.436701 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.935 (1 samples)
Accepted flit rate average = 0.279243 (1 samples)
	minimum = 0.184333 (1 samples)
	maximum = 0.384 (1 samples)
Injected packet size average = 18.0057 (1 samples)
Accepted packet size average = 18.0642 (1 samples)
Hops average = 5.06256 (1 samples)
Total run time 8.41074
