BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 265.182
	minimum = 22
	maximum = 957
Network latency average = 186.627
	minimum = 22
	maximum = 706
Slowest packet = 72
Flit latency average = 156.164
	minimum = 5
	maximum = 689
Slowest flit = 20123
Fragmentation average = 36.2427
	minimum = 0
	maximum = 277
Injected packet rate average = 0.0194844
	minimum = 0 (at node 96)
	maximum = 0.056 (at node 56)
Accepted packet rate average = 0.0126406
	minimum = 0.005 (at node 174)
	maximum = 0.022 (at node 44)
Injected flit rate average = 0.347974
	minimum = 0 (at node 96)
	maximum = 0.998 (at node 56)
Accepted flit rate average= 0.235604
	minimum = 0.108 (at node 93)
	maximum = 0.406 (at node 44)
Injected packet length average = 17.8591
Accepted packet length average = 18.6386
Total in-flight flits = 22102 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 409.192
	minimum = 22
	maximum = 1692
Network latency average = 310.293
	minimum = 22
	maximum = 1316
Slowest packet = 72
Flit latency average = 276.775
	minimum = 5
	maximum = 1393
Slowest flit = 38289
Fragmentation average = 43.7817
	minimum = 0
	maximum = 401
Injected packet rate average = 0.0194505
	minimum = 0.0005 (at node 179)
	maximum = 0.045 (at node 4)
Accepted packet rate average = 0.0135964
	minimum = 0.0075 (at node 116)
	maximum = 0.019 (at node 22)
Injected flit rate average = 0.348823
	minimum = 0.009 (at node 179)
	maximum = 0.805 (at node 4)
Accepted flit rate average= 0.249255
	minimum = 0.135 (at node 116)
	maximum = 0.3535 (at node 166)
Injected packet length average = 17.9339
Accepted packet length average = 18.3325
Total in-flight flits = 38728 (0 measured)
latency change    = 0.351937
throughput change = 0.0547673
Class 0:
Packet latency average = 793.105
	minimum = 22
	maximum = 2476
Network latency average = 662.192
	minimum = 22
	maximum = 1941
Slowest packet = 3545
Flit latency average = 629.694
	minimum = 5
	maximum = 1963
Slowest flit = 69434
Fragmentation average = 53.3172
	minimum = 0
	maximum = 383
Injected packet rate average = 0.0207448
	minimum = 0 (at node 7)
	maximum = 0.056 (at node 30)
Accepted packet rate average = 0.0148281
	minimum = 0.005 (at node 36)
	maximum = 0.025 (at node 92)
Injected flit rate average = 0.372271
	minimum = 0 (at node 7)
	maximum = 1 (at node 30)
Accepted flit rate average= 0.268203
	minimum = 0.086 (at node 61)
	maximum = 0.458 (at node 92)
Injected packet length average = 17.9453
Accepted packet length average = 18.0875
Total in-flight flits = 58927 (0 measured)
latency change    = 0.484064
throughput change = 0.0706476
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 411.892
	minimum = 25
	maximum = 1280
Network latency average = 310.003
	minimum = 22
	maximum = 958
Slowest packet = 11456
Flit latency average = 884.098
	minimum = 5
	maximum = 2286
Slowest flit = 109835
Fragmentation average = 28.9659
	minimum = 0
	maximum = 288
Injected packet rate average = 0.020276
	minimum = 0 (at node 77)
	maximum = 0.055 (at node 34)
Accepted packet rate average = 0.0152448
	minimum = 0.006 (at node 79)
	maximum = 0.025 (at node 66)
Injected flit rate average = 0.36649
	minimum = 0 (at node 77)
	maximum = 1 (at node 34)
Accepted flit rate average= 0.273594
	minimum = 0.108 (at node 79)
	maximum = 0.459 (at node 66)
Injected packet length average = 18.075
Accepted packet length average = 17.9467
Total in-flight flits = 76471 (57385 measured)
latency change    = 0.925517
throughput change = 0.019703
Class 0:
Packet latency average = 847.088
	minimum = 23
	maximum = 2580
Network latency average = 738.999
	minimum = 22
	maximum = 1962
Slowest packet = 11456
Flit latency average = 1005.34
	minimum = 5
	maximum = 2713
Slowest flit = 151613
Fragmentation average = 38.384
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0198438
	minimum = 0.001 (at node 191)
	maximum = 0.051 (at node 11)
Accepted packet rate average = 0.0152161
	minimum = 0.0085 (at node 89)
	maximum = 0.0225 (at node 0)
Injected flit rate average = 0.357513
	minimum = 0.018 (at node 191)
	maximum = 0.918 (at node 11)
Accepted flit rate average= 0.273719
	minimum = 0.157 (at node 89)
	maximum = 0.4095 (at node 66)
Injected packet length average = 18.0164
Accepted packet length average = 17.9887
Total in-flight flits = 90979 (87257 measured)
latency change    = 0.513755
throughput change = 0.000456673
Class 0:
Packet latency average = 1147.56
	minimum = 23
	maximum = 3180
Network latency average = 1034.39
	minimum = 22
	maximum = 2874
Slowest packet = 11456
Flit latency average = 1129.79
	minimum = 5
	maximum = 3472
Slowest flit = 171734
Fragmentation average = 46.4312
	minimum = 0
	maximum = 393
Injected packet rate average = 0.0199097
	minimum = 0.004 (at node 131)
	maximum = 0.042 (at node 55)
Accepted packet rate average = 0.0152066
	minimum = 0.00966667 (at node 86)
	maximum = 0.0216667 (at node 90)
Injected flit rate average = 0.358792
	minimum = 0.072 (at node 131)
	maximum = 0.756 (at node 55)
Accepted flit rate average= 0.273543
	minimum = 0.173 (at node 86)
	maximum = 0.395667 (at node 90)
Injected packet length average = 18.0209
Accepted packet length average = 17.9885
Total in-flight flits = 107790 (107547 measured)
latency change    = 0.261838
throughput change = 0.000641022
Draining remaining packets ...
Class 0:
Remaining flits: 231246 231247 231248 231249 231250 231251 231252 231253 231254 231255 [...] (61391 flits)
Measured flits: 231246 231247 231248 231249 231250 231251 231252 231253 231254 231255 [...] (61391 flits)
Class 0:
Remaining flits: 277540 277541 282348 282349 282350 282351 282352 282353 282354 282355 [...] (21522 flits)
Measured flits: 277540 277541 282348 282349 282350 282351 282352 282353 282354 282355 [...] (21522 flits)
Class 0:
Remaining flits: 313975 313976 313977 313978 313979 313980 313981 313982 313983 313984 [...] (3732 flits)
Measured flits: 313975 313976 313977 313978 313979 313980 313981 313982 313983 313984 [...] (3732 flits)
Class 0:
Remaining flits: 391024 391025 391026 391027 391028 391029 391030 391031 398776 398777 [...] (433 flits)
Measured flits: 391024 391025 391026 391027 391028 391029 391030 391031 398776 398777 [...] (433 flits)
Time taken is 10282 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1807.45 (1 samples)
	minimum = 23 (1 samples)
	maximum = 5029 (1 samples)
Network latency average = 1685.86 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4792 (1 samples)
Flit latency average = 1575.71 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4775 (1 samples)
Fragmentation average = 54.4893 (1 samples)
	minimum = 0 (1 samples)
	maximum = 393 (1 samples)
Injected packet rate average = 0.0199097 (1 samples)
	minimum = 0.004 (1 samples)
	maximum = 0.042 (1 samples)
Accepted packet rate average = 0.0152066 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0216667 (1 samples)
Injected flit rate average = 0.358792 (1 samples)
	minimum = 0.072 (1 samples)
	maximum = 0.756 (1 samples)
Accepted flit rate average = 0.273543 (1 samples)
	minimum = 0.173 (1 samples)
	maximum = 0.395667 (1 samples)
Injected packet size average = 18.0209 (1 samples)
Accepted packet size average = 17.9885 (1 samples)
Hops average = 5.08912 (1 samples)
Total run time 6.82341
