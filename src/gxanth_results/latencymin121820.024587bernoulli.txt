BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 227.756
	minimum = 22
	maximum = 734
Network latency average = 196.933
	minimum = 22
	maximum = 681
Slowest packet = 715
Flit latency average = 169.779
	minimum = 5
	maximum = 662
Slowest flit = 26218
Fragmentation average = 20.9304
	minimum = 0
	maximum = 106
Injected packet rate average = 0.0186719
	minimum = 0.006 (at node 4)
	maximum = 0.03 (at node 67)
Accepted packet rate average = 0.013625
	minimum = 0.005 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.331786
	minimum = 0.093 (at node 4)
	maximum = 0.524 (at node 67)
Accepted flit rate average= 0.251344
	minimum = 0.106 (at node 174)
	maximum = 0.45 (at node 44)
Injected packet length average = 17.7693
Accepted packet length average = 18.4472
Total in-flight flits = 17676 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 426.972
	minimum = 22
	maximum = 1497
Network latency average = 273.297
	minimum = 22
	maximum = 1371
Slowest packet = 1397
Flit latency average = 241.112
	minimum = 5
	maximum = 1340
Slowest flit = 43613
Fragmentation average = 22.3806
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0163229
	minimum = 0.0065 (at node 4)
	maximum = 0.0255 (at node 67)
Accepted packet rate average = 0.0138776
	minimum = 0.0075 (at node 116)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.291898
	minimum = 0.1165 (at node 4)
	maximum = 0.459 (at node 67)
Accepted flit rate average= 0.252042
	minimum = 0.143 (at node 116)
	maximum = 0.351 (at node 22)
Injected packet length average = 17.8827
Accepted packet length average = 18.1618
Total in-flight flits = 17948 (0 measured)
latency change    = 0.466578
throughput change = 0.00276905
Class 0:
Packet latency average = 1053.23
	minimum = 41
	maximum = 2436
Network latency average = 354.366
	minimum = 22
	maximum = 1418
Slowest packet = 5328
Flit latency average = 318.98
	minimum = 5
	maximum = 1451
Slowest flit = 92165
Fragmentation average = 22.2059
	minimum = 0
	maximum = 113
Injected packet rate average = 0.0138385
	minimum = 0.001 (at node 109)
	maximum = 0.029 (at node 61)
Accepted packet rate average = 0.0136615
	minimum = 0.004 (at node 184)
	maximum = 0.023 (at node 34)
Injected flit rate average = 0.249125
	minimum = 0.018 (at node 109)
	maximum = 0.53 (at node 61)
Accepted flit rate average= 0.246927
	minimum = 0.084 (at node 184)
	maximum = 0.414 (at node 34)
Injected packet length average = 18.0023
Accepted packet length average = 18.0747
Total in-flight flits = 18454 (0 measured)
latency change    = 0.594608
throughput change = 0.0207129
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1501.39
	minimum = 223
	maximum = 2960
Network latency average = 282.093
	minimum = 22
	maximum = 972
Slowest packet = 9043
Flit latency average = 330.023
	minimum = 5
	maximum = 1547
Slowest flit = 110393
Fragmentation average = 20.7776
	minimum = 0
	maximum = 111
Injected packet rate average = 0.0140417
	minimum = 0.004 (at node 50)
	maximum = 0.027 (at node 113)
Accepted packet rate average = 0.0140469
	minimum = 0.006 (at node 93)
	maximum = 0.026 (at node 90)
Injected flit rate average = 0.252547
	minimum = 0.071 (at node 50)
	maximum = 0.492 (at node 113)
Accepted flit rate average= 0.252188
	minimum = 0.108 (at node 93)
	maximum = 0.473 (at node 90)
Injected packet length average = 17.9855
Accepted packet length average = 17.9533
Total in-flight flits = 18544 (18392 measured)
latency change    = 0.298492
throughput change = 0.0208591
Class 0:
Packet latency average = 1753.92
	minimum = 223
	maximum = 3522
Network latency average = 330.598
	minimum = 22
	maximum = 1351
Slowest packet = 9043
Flit latency average = 328.319
	minimum = 5
	maximum = 1547
Slowest flit = 110393
Fragmentation average = 21.5033
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140573
	minimum = 0.0045 (at node 124)
	maximum = 0.0235 (at node 69)
Accepted packet rate average = 0.0140365
	minimum = 0.007 (at node 36)
	maximum = 0.0225 (at node 129)
Injected flit rate average = 0.252807
	minimum = 0.0765 (at node 124)
	maximum = 0.428 (at node 97)
Accepted flit rate average= 0.252669
	minimum = 0.126 (at node 36)
	maximum = 0.4135 (at node 129)
Injected packet length average = 17.9841
Accepted packet length average = 18.0009
Total in-flight flits = 18305 (18305 measured)
latency change    = 0.143982
throughput change = 0.00190673
Class 0:
Packet latency average = 1974.5
	minimum = 223
	maximum = 3945
Network latency average = 342.427
	minimum = 22
	maximum = 1837
Slowest packet = 9043
Flit latency average = 327.498
	minimum = 5
	maximum = 1820
Slowest flit = 208222
Fragmentation average = 21.8349
	minimum = 0
	maximum = 126
Injected packet rate average = 0.0140712
	minimum = 0.00666667 (at node 124)
	maximum = 0.0213333 (at node 69)
Accepted packet rate average = 0.0140486
	minimum = 0.00933333 (at node 36)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.253219
	minimum = 0.12 (at node 124)
	maximum = 0.384 (at node 69)
Accepted flit rate average= 0.252814
	minimum = 0.168 (at node 36)
	maximum = 0.363 (at node 128)
Injected packet length average = 17.9956
Accepted packet length average = 17.9957
Total in-flight flits = 18723 (18723 measured)
latency change    = 0.111714
throughput change = 0.000573406
Class 0:
Packet latency average = 2200.88
	minimum = 223
	maximum = 4406
Network latency average = 347.674
	minimum = 22
	maximum = 2132
Slowest packet = 9043
Flit latency average = 326.786
	minimum = 5
	maximum = 2115
Slowest flit = 210060
Fragmentation average = 22.141
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0140313
	minimum = 0.007 (at node 186)
	maximum = 0.021 (at node 38)
Accepted packet rate average = 0.0140273
	minimum = 0.0095 (at node 2)
	maximum = 0.0205 (at node 128)
Injected flit rate average = 0.252484
	minimum = 0.13025 (at node 186)
	maximum = 0.378 (at node 38)
Accepted flit rate average= 0.252359
	minimum = 0.17275 (at node 2)
	maximum = 0.36525 (at node 128)
Injected packet length average = 17.9944
Accepted packet length average = 17.9905
Total in-flight flits = 18412 (18412 measured)
latency change    = 0.102859
throughput change = 0.00180243
Class 0:
Packet latency average = 2416.22
	minimum = 223
	maximum = 5252
Network latency average = 350.517
	minimum = 22
	maximum = 2132
Slowest packet = 9043
Flit latency average = 326.572
	minimum = 5
	maximum = 2115
Slowest flit = 210060
Fragmentation average = 22.1268
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0139958
	minimum = 0.0086 (at node 40)
	maximum = 0.0196 (at node 38)
Accepted packet rate average = 0.0139948
	minimum = 0.0106 (at node 104)
	maximum = 0.0196 (at node 128)
Injected flit rate average = 0.251675
	minimum = 0.1524 (at node 40)
	maximum = 0.3516 (at node 38)
Accepted flit rate average= 0.251857
	minimum = 0.1926 (at node 104)
	maximum = 0.3518 (at node 128)
Injected packet length average = 17.9821
Accepted packet length average = 17.9965
Total in-flight flits = 18141 (18141 measured)
latency change    = 0.0891245
throughput change = 0.00199352
Class 0:
Packet latency average = 2635.83
	minimum = 223
	maximum = 5793
Network latency average = 352.749
	minimum = 22
	maximum = 2132
Slowest packet = 9043
Flit latency average = 326.58
	minimum = 5
	maximum = 2115
Slowest flit = 210060
Fragmentation average = 22.2043
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0139488
	minimum = 0.0085 (at node 17)
	maximum = 0.019 (at node 38)
Accepted packet rate average = 0.0139714
	minimum = 0.0111667 (at node 42)
	maximum = 0.0185 (at node 128)
Injected flit rate average = 0.251095
	minimum = 0.153 (at node 17)
	maximum = 0.342 (at node 38)
Accepted flit rate average= 0.251358
	minimum = 0.1995 (at node 42)
	maximum = 0.331833 (at node 128)
Injected packet length average = 18.0012
Accepted packet length average = 17.9909
Total in-flight flits = 18114 (18114 measured)
latency change    = 0.0833168
throughput change = 0.00198782
Class 0:
Packet latency average = 2854.23
	minimum = 223
	maximum = 5973
Network latency average = 353.222
	minimum = 22
	maximum = 2132
Slowest packet = 9043
Flit latency average = 325.663
	minimum = 5
	maximum = 2115
Slowest flit = 210060
Fragmentation average = 22.2592
	minimum = 0
	maximum = 141
Injected packet rate average = 0.0139494
	minimum = 0.008 (at node 19)
	maximum = 0.0182857 (at node 11)
Accepted packet rate average = 0.0139658
	minimum = 0.0112857 (at node 63)
	maximum = 0.0182857 (at node 138)
Injected flit rate average = 0.251092
	minimum = 0.144 (at node 19)
	maximum = 0.328571 (at node 11)
Accepted flit rate average= 0.251368
	minimum = 0.199143 (at node 63)
	maximum = 0.329143 (at node 138)
Injected packet length average = 18.0002
Accepted packet length average = 17.9989
Total in-flight flits = 18025 (18025 measured)
latency change    = 0.0765186
throughput change = 4.24265e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 444047 444048 444049 444050 444051 444052 444053 444054 444055 444056 [...] (18376 flits)
Measured flits: 444047 444048 444049 444050 444051 444052 444053 444054 444055 444056 [...] (18376 flits)
Class 0:
Remaining flits: 532296 532297 532298 532299 532300 532301 532302 532303 532304 532305 [...] (18076 flits)
Measured flits: 532296 532297 532298 532299 532300 532301 532302 532303 532304 532305 [...] (18076 flits)
Class 0:
Remaining flits: 565506 565507 565508 565509 565510 565511 565512 565513 565514 565515 [...] (18828 flits)
Measured flits: 565506 565507 565508 565509 565510 565511 565512 565513 565514 565515 [...] (18828 flits)
Class 0:
Remaining flits: 601596 601597 601598 601599 601600 601601 601602 601603 601604 601605 [...] (18213 flits)
Measured flits: 601596 601597 601598 601599 601600 601601 601602 601603 601604 601605 [...] (17997 flits)
Class 0:
Remaining flits: 673452 673453 673454 673455 673456 673457 673458 673459 673460 673461 [...] (18613 flits)
Measured flits: 673452 673453 673454 673455 673456 673457 673458 673459 673460 673461 [...] (17307 flits)
Class 0:
Remaining flits: 723438 723439 723440 723441 723442 723443 723444 723445 723446 723447 [...] (18663 flits)
Measured flits: 723438 723439 723440 723441 723442 723443 723444 723445 723446 723447 [...] (15037 flits)
Class 0:
Remaining flits: 760248 760249 760250 760251 760252 760253 760254 760255 760256 760257 [...] (18492 flits)
Measured flits: 760248 760249 760250 760251 760252 760253 760254 760255 760256 760257 [...] (11046 flits)
Class 0:
Remaining flits: 800712 800713 800714 800715 800716 800717 800718 800719 800720 800721 [...] (18235 flits)
Measured flits: 800712 800713 800714 800715 800716 800717 800718 800719 800720 800721 [...] (7192 flits)
Class 0:
Remaining flits: 822132 822133 822134 822135 822136 822137 822138 822139 822140 822141 [...] (18320 flits)
Measured flits: 822132 822133 822134 822135 822136 822137 822138 822139 822140 822141 [...] (4084 flits)
Class 0:
Remaining flits: 890820 890821 890822 890823 890824 890825 890826 890827 890828 890829 [...] (18003 flits)
Measured flits: 911250 911251 911252 911253 911254 911255 911256 911257 911258 911259 [...] (1495 flits)
Class 0:
Remaining flits: 947232 947233 947234 947235 947236 947237 947238 947239 947240 947241 [...] (18484 flits)
Measured flits: 947232 947233 947234 947235 947236 947237 947238 947239 947240 947241 [...] (665 flits)
Class 0:
Remaining flits: 998442 998443 998444 998445 998446 998447 998448 998449 998450 998451 [...] (18136 flits)
Measured flits: 1037178 1037179 1037180 1037181 1037182 1037183 1037184 1037185 1037186 1037187 [...] (342 flits)
Draining remaining packets ...
Time taken is 23347 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4704.95 (1 samples)
	minimum = 223 (1 samples)
	maximum = 12899 (1 samples)
Network latency average = 361.79 (1 samples)
	minimum = 22 (1 samples)
	maximum = 2411 (1 samples)
Flit latency average = 329.298 (1 samples)
	minimum = 5 (1 samples)
	maximum = 2385 (1 samples)
Fragmentation average = 22.3384 (1 samples)
	minimum = 0 (1 samples)
	maximum = 141 (1 samples)
Injected packet rate average = 0.0139494 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.0182857 (1 samples)
Accepted packet rate average = 0.0139658 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.251092 (1 samples)
	minimum = 0.144 (1 samples)
	maximum = 0.328571 (1 samples)
Accepted flit rate average = 0.251368 (1 samples)
	minimum = 0.199143 (1 samples)
	maximum = 0.329143 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 17.9989 (1 samples)
Hops average = 5.06548 (1 samples)
Total run time 20.5438
