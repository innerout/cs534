BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 288.359
	minimum = 27
	maximum = 965
Network latency average = 236.42
	minimum = 23
	maximum = 859
Slowest packet = 135
Flit latency average = 160.25
	minimum = 6
	maximum = 931
Slowest flit = 4868
Fragmentation average = 133.521
	minimum = 0
	maximum = 697
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00873438
	minimum = 0.003 (at node 12)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.17712
	minimum = 0.058 (at node 93)
	maximum = 0.312 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.2785
Total in-flight flits = 14143 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 387.958
	minimum = 27
	maximum = 1985
Network latency average = 331.129
	minimum = 23
	maximum = 1869
Slowest packet = 321
Flit latency average = 237.524
	minimum = 6
	maximum = 1852
Slowest flit = 8369
Fragmentation average = 168.741
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00957552
	minimum = 0.0045 (at node 96)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.184771
	minimum = 0.095 (at node 96)
	maximum = 0.28 (at node 152)
Injected packet length average = 17.9095
Accepted packet length average = 19.2962
Total in-flight flits = 20128 (0 measured)
latency change    = 0.256725
throughput change = 0.0414083
Class 0:
Packet latency average = 578.98
	minimum = 25
	maximum = 2675
Network latency average = 518.129
	minimum = 23
	maximum = 2581
Slowest packet = 828
Flit latency average = 405.811
	minimum = 6
	maximum = 2921
Slowest flit = 5979
Fragmentation average = 239.469
	minimum = 0
	maximum = 2262
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0109844
	minimum = 0.004 (at node 62)
	maximum = 0.019 (at node 131)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.201604
	minimum = 0.096 (at node 178)
	maximum = 0.313 (at node 177)
Injected packet length average = 18.0569
Accepted packet length average = 18.3537
Total in-flight flits = 26348 (0 measured)
latency change    = 0.329929
throughput change = 0.083497
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 368.719
	minimum = 27
	maximum = 1085
Network latency average = 312.558
	minimum = 23
	maximum = 921
Slowest packet = 7597
Flit latency average = 514.846
	minimum = 6
	maximum = 3835
Slowest flit = 2843
Fragmentation average = 175.358
	minimum = 0
	maximum = 769
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0114635
	minimum = 0.003 (at node 153)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.210349
	minimum = 0.062 (at node 153)
	maximum = 0.467 (at node 16)
Injected packet length average = 17.981
Accepted packet length average = 18.3494
Total in-flight flits = 32329 (22931 measured)
latency change    = 0.570248
throughput change = 0.0415728
Class 0:
Packet latency average = 500.428
	minimum = 25
	maximum = 2129
Network latency average = 438.278
	minimum = 23
	maximum = 1861
Slowest packet = 7597
Flit latency average = 550.097
	minimum = 6
	maximum = 4506
Slowest flit = 6461
Fragmentation average = 203.352
	minimum = 0
	maximum = 1275
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.0116536
	minimum = 0.0065 (at node 134)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.213651
	minimum = 0.123 (at node 134)
	maximum = 0.4055 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.3334
Total in-flight flits = 38662 (33563 measured)
latency change    = 0.263193
throughput change = 0.0154555
Class 0:
Packet latency average = 611.163
	minimum = 25
	maximum = 2939
Network latency average = 547.809
	minimum = 23
	maximum = 2899
Slowest packet = 7583
Flit latency average = 607.679
	minimum = 6
	maximum = 5411
Slowest flit = 13571
Fragmentation average = 225.99
	minimum = 0
	maximum = 2544
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0117656
	minimum = 0.008 (at node 64)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.21438
	minimum = 0.146333 (at node 79)
	maximum = 0.311333 (at node 16)
Injected packet length average = 17.9913
Accepted packet length average = 18.2209
Total in-flight flits = 39395 (36275 measured)
latency change    = 0.181186
throughput change = 0.00340128
Draining remaining packets ...
Class 0:
Remaining flits: 22731 22732 22733 29358 29359 29360 29361 29362 29363 29364 [...] (9998 flits)
Measured flits: 136062 136063 136064 136065 136066 136067 136068 136069 136070 136071 [...] (8443 flits)
Class 0:
Remaining flits: 102042 102043 102044 102045 102046 102047 102048 102049 102050 102051 [...] (234 flits)
Measured flits: 170820 170821 170822 170823 170824 170825 170826 170827 170828 170829 [...] (162 flits)
Time taken is 8235 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 947.164 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5217 (1 samples)
Network latency average = 878.722 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4961 (1 samples)
Flit latency average = 902.711 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7038 (1 samples)
Fragmentation average = 240.451 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3207 (1 samples)
Injected packet rate average = 0.0131684 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.0117656 (1 samples)
	minimum = 0.008 (1 samples)
	maximum = 0.017 (1 samples)
Injected flit rate average = 0.236917 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.517 (1 samples)
Accepted flit rate average = 0.21438 (1 samples)
	minimum = 0.146333 (1 samples)
	maximum = 0.311333 (1 samples)
Injected packet size average = 17.9913 (1 samples)
Accepted packet size average = 18.2209 (1 samples)
Hops average = 5.07673 (1 samples)
Total run time 7.63473
