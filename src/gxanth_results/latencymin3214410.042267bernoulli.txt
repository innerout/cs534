BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.042267
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 318.502
	minimum = 25
	maximum = 943
Network latency average = 302.734
	minimum = 25
	maximum = 943
Slowest packet = 252
Flit latency average = 247.266
	minimum = 6
	maximum = 967
Slowest flit = 2527
Fragmentation average = 149.654
	minimum = 0
	maximum = 790
Injected packet rate average = 0.040875
	minimum = 0.024 (at node 30)
	maximum = 0.055 (at node 51)
Accepted packet rate average = 0.0114687
	minimum = 0.004 (at node 108)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.729615
	minimum = 0.428 (at node 30)
	maximum = 0.98 (at node 51)
Accepted flit rate average= 0.234719
	minimum = 0.086 (at node 108)
	maximum = 0.387 (at node 104)
Injected packet length average = 17.8499
Accepted packet length average = 20.4659
Total in-flight flits = 96198 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 632.418
	minimum = 25
	maximum = 1911
Network latency average = 609.835
	minimum = 25
	maximum = 1911
Slowest packet = 557
Flit latency average = 541.225
	minimum = 6
	maximum = 1894
Slowest flit = 10043
Fragmentation average = 183.036
	minimum = 0
	maximum = 1732
Injected packet rate average = 0.0388724
	minimum = 0.025 (at node 96)
	maximum = 0.0505 (at node 177)
Accepted packet rate average = 0.0123203
	minimum = 0.0055 (at node 108)
	maximum = 0.0185 (at node 106)
Injected flit rate average = 0.69637
	minimum = 0.444 (at node 96)
	maximum = 0.903 (at node 177)
Accepted flit rate average= 0.234557
	minimum = 0.119 (at node 108)
	maximum = 0.359 (at node 177)
Injected packet length average = 17.9142
Accepted packet length average = 19.0383
Total in-flight flits = 178634 (0 measured)
latency change    = 0.496374
throughput change = 0.000688354
Class 0:
Packet latency average = 1649.78
	minimum = 31
	maximum = 2801
Network latency average = 1604.27
	minimum = 28
	maximum = 2780
Slowest packet = 808
Flit latency average = 1559.14
	minimum = 6
	maximum = 2919
Slowest flit = 7821
Fragmentation average = 230.489
	minimum = 3
	maximum = 1914
Injected packet rate average = 0.034875
	minimum = 0.011 (at node 0)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.0119792
	minimum = 0.005 (at node 64)
	maximum = 0.025 (at node 92)
Injected flit rate average = 0.627995
	minimum = 0.201 (at node 0)
	maximum = 1 (at node 137)
Accepted flit rate average= 0.218229
	minimum = 0.077 (at node 66)
	maximum = 0.44 (at node 92)
Injected packet length average = 18.007
Accepted packet length average = 18.2174
Total in-flight flits = 257262 (0 measured)
latency change    = 0.616666
throughput change = 0.074821
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 860.846
	minimum = 31
	maximum = 2250
Network latency average = 50.5846
	minimum = 29
	maximum = 730
Slowest packet = 21705
Flit latency average = 2196.76
	minimum = 6
	maximum = 3817
Slowest flit = 14923
Fragmentation average = 13.1154
	minimum = 2
	maximum = 304
Injected packet rate average = 0.0262865
	minimum = 0.01 (at node 184)
	maximum = 0.048 (at node 169)
Accepted packet rate average = 0.0120677
	minimum = 0.005 (at node 36)
	maximum = 0.021 (at node 75)
Injected flit rate average = 0.472354
	minimum = 0.182 (at node 184)
	maximum = 0.859 (at node 169)
Accepted flit rate average= 0.214573
	minimum = 0.079 (at node 92)
	maximum = 0.396 (at node 137)
Injected packet length average = 17.9695
Accepted packet length average = 17.7808
Total in-flight flits = 306964 (88540 measured)
latency change    = 0.916466
throughput change = 0.0170397
Class 0:
Packet latency average = 1305.33
	minimum = 30
	maximum = 3262
Network latency average = 239.552
	minimum = 29
	maximum = 1933
Slowest packet = 21705
Flit latency average = 2513.8
	minimum = 6
	maximum = 4801
Slowest flit = 24772
Fragmentation average = 25.5878
	minimum = 2
	maximum = 304
Injected packet rate average = 0.0214193
	minimum = 0.006 (at node 184)
	maximum = 0.0365 (at node 109)
Accepted packet rate average = 0.0118932
	minimum = 0.006 (at node 36)
	maximum = 0.02 (at node 0)
Injected flit rate average = 0.385177
	minimum = 0.107 (at node 184)
	maximum = 0.661 (at node 109)
Accepted flit rate average= 0.212458
	minimum = 0.105 (at node 5)
	maximum = 0.3685 (at node 0)
Injected packet length average = 17.9827
Accepted packet length average = 17.8638
Total in-flight flits = 323818 (142933 measured)
latency change    = 0.340514
throughput change = 0.00995293
Class 0:
Packet latency average = 2153.27
	minimum = 30
	maximum = 4389
Network latency average = 895.116
	minimum = 29
	maximum = 2986
Slowest packet = 21705
Flit latency average = 2811.82
	minimum = 6
	maximum = 5585
Slowest flit = 54191
Fragmentation average = 61.5973
	minimum = 1
	maximum = 645
Injected packet rate average = 0.0192083
	minimum = 0.00833333 (at node 51)
	maximum = 0.0293333 (at node 109)
Accepted packet rate average = 0.011783
	minimum = 0.007 (at node 36)
	maximum = 0.0176667 (at node 0)
Injected flit rate average = 0.345408
	minimum = 0.147667 (at node 51)
	maximum = 0.527 (at node 109)
Accepted flit rate average= 0.21091
	minimum = 0.121 (at node 67)
	maximum = 0.316 (at node 0)
Injected packet length average = 17.9822
Accepted packet length average = 17.8995
Total in-flight flits = 335272 (188353 measured)
latency change    = 0.393792
throughput change = 0.00734253
Class 0:
Packet latency average = 2852.24
	minimum = 30
	maximum = 5302
Network latency average = 1453.45
	minimum = 25
	maximum = 3920
Slowest packet = 21705
Flit latency average = 3101.14
	minimum = 6
	maximum = 6678
Slowest flit = 42466
Fragmentation average = 85.0596
	minimum = 1
	maximum = 1357
Injected packet rate average = 0.0178633
	minimum = 0.00825 (at node 106)
	maximum = 0.02675 (at node 141)
Accepted packet rate average = 0.0116628
	minimum = 0.00775 (at node 17)
	maximum = 0.01675 (at node 0)
Injected flit rate average = 0.321513
	minimum = 0.151 (at node 106)
	maximum = 0.481 (at node 141)
Accepted flit rate average= 0.20949
	minimum = 0.142 (at node 67)
	maximum = 0.29925 (at node 0)
Injected packet length average = 17.9985
Accepted packet length average = 17.9623
Total in-flight flits = 343946 (227325 measured)
latency change    = 0.245059
throughput change = 0.00677904
Class 0:
Packet latency average = 3509.14
	minimum = 30
	maximum = 6269
Network latency average = 2036.77
	minimum = 25
	maximum = 4965
Slowest packet = 21705
Flit latency average = 3389.15
	minimum = 6
	maximum = 7582
Slowest flit = 56347
Fragmentation average = 107.906
	minimum = 0
	maximum = 1358
Injected packet rate average = 0.0170656
	minimum = 0.0078 (at node 106)
	maximum = 0.0242 (at node 30)
Accepted packet rate average = 0.0116187
	minimum = 0.0082 (at node 36)
	maximum = 0.0152 (at node 103)
Injected flit rate average = 0.307192
	minimum = 0.1408 (at node 106)
	maximum = 0.4356 (at node 30)
Accepted flit rate average= 0.208061
	minimum = 0.148 (at node 67)
	maximum = 0.276 (at node 103)
Injected packet length average = 18.0006
Accepted packet length average = 17.9074
Total in-flight flits = 353173 (262671 measured)
latency change    = 0.187197
throughput change = 0.00686396
Class 0:
Packet latency average = 4092.78
	minimum = 30
	maximum = 7336
Network latency average = 2588.96
	minimum = 25
	maximum = 5958
Slowest packet = 21705
Flit latency average = 3683.08
	minimum = 6
	maximum = 8384
Slowest flit = 67129
Fragmentation average = 122.82
	minimum = 0
	maximum = 1879
Injected packet rate average = 0.0162977
	minimum = 0.00783333 (at node 88)
	maximum = 0.0235 (at node 141)
Accepted packet rate average = 0.0115286
	minimum = 0.0085 (at node 36)
	maximum = 0.0151667 (at node 103)
Injected flit rate average = 0.293628
	minimum = 0.142 (at node 88)
	maximum = 0.4215 (at node 141)
Accepted flit rate average= 0.206563
	minimum = 0.154833 (at node 36)
	maximum = 0.268 (at node 103)
Injected packet length average = 18.0165
Accepted packet length average = 17.9174
Total in-flight flits = 358475 (290327 measured)
latency change    = 0.142603
throughput change = 0.00725245
Class 0:
Packet latency average = 4690.66
	minimum = 30
	maximum = 8305
Network latency average = 3162.93
	minimum = 25
	maximum = 6898
Slowest packet = 21705
Flit latency average = 3962.61
	minimum = 6
	maximum = 9483
Slowest flit = 29069
Fragmentation average = 134.26
	minimum = 0
	maximum = 2041
Injected packet rate average = 0.0155193
	minimum = 0.00671429 (at node 88)
	maximum = 0.0232857 (at node 59)
Accepted packet rate average = 0.0114241
	minimum = 0.00842857 (at node 36)
	maximum = 0.015 (at node 103)
Injected flit rate average = 0.279498
	minimum = 0.121714 (at node 88)
	maximum = 0.420286 (at node 59)
Accepted flit rate average= 0.204495
	minimum = 0.156286 (at node 36)
	maximum = 0.266286 (at node 103)
Injected packet length average = 18.0096
Accepted packet length average = 17.9003
Total in-flight flits = 359287 (307984 measured)
latency change    = 0.127462
throughput change = 0.0101155
Draining all recorded packets ...
Class 0:
Remaining flits: 70434 70435 70436 70437 70438 70439 70440 70441 70442 70443 [...] (354727 flits)
Measured flits: 389250 389251 389252 389253 389254 389255 389256 389257 389258 389259 [...] (317024 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (351367 flits)
Measured flits: 389250 389251 389252 389253 389254 389255 389256 389257 389258 389259 [...] (324916 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (349166 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (330221 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (346956 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (334411 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (347836 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (339580 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (344972 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (339397 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (341099 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (337202 flits)
Class 0:
Remaining flits: 102780 102781 102782 102783 102784 102785 102786 102787 102788 102789 [...] (336730 flits)
Measured flits: 389286 389287 389288 389289 389290 389291 389292 389293 389294 389295 [...] (333810 flits)
Class 0:
Remaining flits: 102781 102782 102783 102784 102785 102786 102787 102788 102789 102790 [...] (334813 flits)
Measured flits: 389718 389719 389720 389721 389722 389723 389724 389725 389726 389727 [...] (332813 flits)
Class 0:
Remaining flits: 168498 168499 168500 168501 168502 168503 168504 168505 168506 168507 [...] (333233 flits)
Measured flits: 389718 389719 389720 389721 389722 389723 389724 389725 389726 389727 [...] (331890 flits)
Class 0:
Remaining flits: 168515 269658 269659 269660 269661 269662 269663 269664 269665 269666 [...] (331944 flits)
Measured flits: 389718 389719 389720 389721 389722 389723 389724 389725 389726 389727 [...] (331066 flits)
Class 0:
Remaining flits: 269658 269659 269660 269661 269662 269663 269664 269665 269666 269667 [...] (332292 flits)
Measured flits: 390618 390619 390620 390621 390622 390623 390624 390625 390626 390627 [...] (330627 flits)
Class 0:
Remaining flits: 269658 269659 269660 269661 269662 269663 269664 269665 269666 269667 [...] (330651 flits)
Measured flits: 390620 390621 390622 390623 390624 390625 390626 390627 390628 390629 [...] (327367 flits)
Class 0:
Remaining flits: 269658 269659 269660 269661 269662 269663 269664 269665 269666 269667 [...] (330209 flits)
Measured flits: 395010 395011 395012 395013 395014 395015 395016 395017 395018 395019 [...] (323049 flits)
Class 0:
Remaining flits: 269672 269673 269674 269675 271278 271279 271280 271281 271282 271283 [...] (329297 flits)
Measured flits: 395010 395011 395012 395013 395014 395015 395016 395017 395018 395019 [...] (316295 flits)
Class 0:
Remaining flits: 271281 271282 271283 271284 271285 271286 271287 271288 271289 271290 [...] (325957 flits)
Measured flits: 395011 395012 395013 395014 395015 395016 395017 395018 395019 395020 [...] (304088 flits)
Class 0:
Remaining flits: 271281 271282 271283 271284 271285 271286 271287 271288 271289 271290 [...] (324481 flits)
Measured flits: 414036 414037 414038 414039 414040 414041 414042 414043 414044 414045 [...] (289375 flits)
Class 0:
Remaining flits: 271281 271282 271283 271284 271285 271286 271287 271288 271289 271290 [...] (322279 flits)
Measured flits: 414036 414037 414038 414039 414040 414041 414042 414043 414044 414045 [...] (273334 flits)
Class 0:
Remaining flits: 271290 271291 271292 271293 271294 271295 287275 287276 287277 287278 [...] (322235 flits)
Measured flits: 425592 425593 425594 425595 425596 425597 425598 425599 425600 425601 [...] (254138 flits)
Class 0:
Remaining flits: 425592 425593 425594 425595 425596 425597 425598 425599 425600 425601 [...] (323278 flits)
Measured flits: 425592 425593 425594 425595 425596 425597 425598 425599 425600 425601 [...] (237889 flits)
Class 0:
Remaining flits: 425592 425593 425594 425595 425596 425597 425598 425599 425600 425601 [...] (319415 flits)
Measured flits: 425592 425593 425594 425595 425596 425597 425598 425599 425600 425601 [...] (215013 flits)
Class 0:
Remaining flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (320334 flits)
Measured flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (194422 flits)
Class 0:
Remaining flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (318850 flits)
Measured flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (172953 flits)
Class 0:
Remaining flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (318355 flits)
Measured flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (153960 flits)
Class 0:
Remaining flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (317104 flits)
Measured flits: 486144 486145 486146 486147 486148 486149 486150 486151 486152 486153 [...] (135038 flits)
Class 0:
Remaining flits: 486153 486154 486155 486156 486157 486158 486159 486160 486161 540180 [...] (316200 flits)
Measured flits: 486153 486154 486155 486156 486157 486158 486159 486160 486161 540180 [...] (116899 flits)
Class 0:
Remaining flits: 486157 486158 486159 486160 486161 540180 540181 540182 540183 540184 [...] (316834 flits)
Measured flits: 486157 486158 486159 486160 486161 540180 540181 540182 540183 540184 [...] (100218 flits)
Class 0:
Remaining flits: 678672 678673 678674 678675 678676 678677 678678 678679 678680 678681 [...] (317681 flits)
Measured flits: 678672 678673 678674 678675 678676 678677 678678 678679 678680 678681 [...] (85450 flits)
Class 0:
Remaining flits: 678672 678673 678674 678675 678676 678677 678678 678679 678680 678681 [...] (316926 flits)
Measured flits: 678672 678673 678674 678675 678676 678677 678678 678679 678680 678681 [...] (70873 flits)
Class 0:
Remaining flits: 678676 678677 678678 678679 678680 678681 678682 678683 678684 678685 [...] (315030 flits)
Measured flits: 678676 678677 678678 678679 678680 678681 678682 678683 678684 678685 [...] (59625 flits)
Class 0:
Remaining flits: 759438 759439 759440 759441 759442 759443 759444 759445 759446 759447 [...] (316102 flits)
Measured flits: 759438 759439 759440 759441 759442 759443 759444 759445 759446 759447 [...] (50171 flits)
Class 0:
Remaining flits: 788343 788344 788345 803970 803971 803972 803973 803974 803975 803976 [...] (313868 flits)
Measured flits: 788343 788344 788345 803970 803971 803972 803973 803974 803975 803976 [...] (41953 flits)
Class 0:
Remaining flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (315710 flits)
Measured flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (34473 flits)
Class 0:
Remaining flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (313714 flits)
Measured flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (27566 flits)
Class 0:
Remaining flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (309601 flits)
Measured flits: 803970 803971 803972 803973 803974 803975 803976 803977 803978 803979 [...] (22868 flits)
Class 0:
Remaining flits: 803979 803980 803981 803982 803983 803984 803985 803986 803987 829638 [...] (308751 flits)
Measured flits: 803979 803980 803981 803982 803983 803984 803985 803986 803987 829638 [...] (19596 flits)
Class 0:
Remaining flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (310157 flits)
Measured flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (16894 flits)
Class 0:
Remaining flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (310044 flits)
Measured flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (13168 flits)
Class 0:
Remaining flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (310799 flits)
Measured flits: 848088 848089 848090 848091 848092 848093 848094 848095 848096 848097 [...] (10196 flits)
Class 0:
Remaining flits: 1026437 1026438 1026439 1026440 1026441 1026442 1026443 1026444 1026445 1026446 [...] (308701 flits)
Measured flits: 1026437 1026438 1026439 1026440 1026441 1026442 1026443 1026444 1026445 1026446 [...] (8045 flits)
Class 0:
Remaining flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (309174 flits)
Measured flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (6402 flits)
Class 0:
Remaining flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (306286 flits)
Measured flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (5011 flits)
Class 0:
Remaining flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (309142 flits)
Measured flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (3745 flits)
Class 0:
Remaining flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (309859 flits)
Measured flits: 1078524 1078525 1078526 1078527 1078528 1078529 1078530 1078531 1078532 1078533 [...] (3159 flits)
Class 0:
Remaining flits: 1152378 1152379 1152380 1152381 1152382 1152383 1152384 1152385 1152386 1152387 [...] (309001 flits)
Measured flits: 1152378 1152379 1152380 1152381 1152382 1152383 1152384 1152385 1152386 1152387 [...] (2326 flits)
Class 0:
Remaining flits: 1152378 1152379 1152380 1152381 1152382 1152383 1152384 1152385 1152386 1152387 [...] (306466 flits)
Measured flits: 1152378 1152379 1152380 1152381 1152382 1152383 1152384 1152385 1152386 1152387 [...] (1765 flits)
Class 0:
Remaining flits: 1244358 1244359 1244360 1244361 1244362 1244363 1244364 1244365 1244366 1244367 [...] (305386 flits)
Measured flits: 1244358 1244359 1244360 1244361 1244362 1244363 1244364 1244365 1244366 1244367 [...] (1254 flits)
Class 0:
Remaining flits: 1244358 1244359 1244360 1244361 1244362 1244363 1244364 1244365 1244366 1244367 [...] (304758 flits)
Measured flits: 1244358 1244359 1244360 1244361 1244362 1244363 1244364 1244365 1244366 1244367 [...] (945 flits)
Class 0:
Remaining flits: 1321938 1321939 1321940 1321941 1321942 1321943 1321944 1321945 1321946 1321947 [...] (301632 flits)
Measured flits: 1321938 1321939 1321940 1321941 1321942 1321943 1321944 1321945 1321946 1321947 [...] (575 flits)
Class 0:
Remaining flits: 1321938 1321939 1321940 1321941 1321942 1321943 1321944 1321945 1321946 1321947 [...] (302038 flits)
Measured flits: 1321938 1321939 1321940 1321941 1321942 1321943 1321944 1321945 1321946 1321947 [...] (464 flits)
Class 0:
Remaining flits: 1401642 1401643 1401644 1401645 1401646 1401647 1401648 1401649 1401650 1401651 [...] (299870 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (272 flits)
Class 0:
Remaining flits: 1401642 1401643 1401644 1401645 1401646 1401647 1401648 1401649 1401650 1401651 [...] (303551 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (182 flits)
Class 0:
Remaining flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (300429 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (91 flits)
Class 0:
Remaining flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (299709 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (90 flits)
Class 0:
Remaining flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (301066 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (67 flits)
Class 0:
Remaining flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (299129 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (54 flits)
Class 0:
Remaining flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (297239 flits)
Measured flits: 1403838 1403839 1403840 1403841 1403842 1403843 1403844 1403845 1403846 1403847 [...] (24 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1639692 1639693 1639694 1639695 1639696 1639697 1639698 1639699 1639700 1639701 [...] (264033 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1787022 1787023 1787024 1787025 1787026 1787027 1787028 1787029 1787030 1787031 [...] (229921 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1787022 1787023 1787024 1787025 1787026 1787027 1787028 1787029 1787030 1787031 [...] (197335 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1787022 1787023 1787024 1787025 1787026 1787027 1787028 1787029 1787030 1787031 [...] (164161 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1787022 1787023 1787024 1787025 1787026 1787027 1787028 1787029 1787030 1787031 [...] (133788 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1787031 1787032 1787033 1787034 1787035 1787036 1787037 1787038 1787039 1825542 [...] (107320 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825542 1825543 1825544 1825545 1825546 1825547 1825548 1825549 1825550 1825551 [...] (82306 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825542 1825543 1825544 1825545 1825546 1825547 1825548 1825549 1825550 1825551 [...] (58692 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1826766 1826767 1826768 1826769 1826770 1826771 1826772 1826773 1826774 1826775 [...] (39856 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2000182 2000183 2000184 2000185 2000186 2000187 2000188 2000189 2000190 2000191 [...] (23295 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2115792 2115793 2115794 2115795 2115796 2115797 2115798 2115799 2115800 2115801 [...] (12387 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2168172 2168173 2168174 2168175 2168176 2168177 2168178 2168179 2168180 2168181 [...] (4889 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2168172 2168173 2168174 2168175 2168176 2168177 2168178 2168179 2168180 2168181 [...] (1202 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2583327 2583328 2583329 2583330 2583331 2583332 2583333 2583334 2583335 2583336 [...] (168 flits)
Measured flits: (0 flits)
Time taken is 81866 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 18331.2 (1 samples)
	minimum = 30 (1 samples)
	maximum = 58476 (1 samples)
Network latency average = 8855.08 (1 samples)
	minimum = 23 (1 samples)
	maximum = 39402 (1 samples)
Flit latency average = 8414.42 (1 samples)
	minimum = 6 (1 samples)
	maximum = 39264 (1 samples)
Fragmentation average = 208.157 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5916 (1 samples)
Injected packet rate average = 0.0155193 (1 samples)
	minimum = 0.00671429 (1 samples)
	maximum = 0.0232857 (1 samples)
Accepted packet rate average = 0.0114241 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.279498 (1 samples)
	minimum = 0.121714 (1 samples)
	maximum = 0.420286 (1 samples)
Accepted flit rate average = 0.204495 (1 samples)
	minimum = 0.156286 (1 samples)
	maximum = 0.266286 (1 samples)
Injected packet size average = 18.0096 (1 samples)
Accepted packet size average = 17.9003 (1 samples)
Hops average = 5.04361 (1 samples)
Total run time 104.431
