BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 288.359
	minimum = 27
	maximum = 965
Network latency average = 236.42
	minimum = 23
	maximum = 859
Slowest packet = 135
Flit latency average = 160.25
	minimum = 6
	maximum = 931
Slowest flit = 4868
Fragmentation average = 133.521
	minimum = 0
	maximum = 697
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00873438
	minimum = 0.003 (at node 12)
	maximum = 0.016 (at node 44)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.17712
	minimum = 0.058 (at node 93)
	maximum = 0.312 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 20.2785
Total in-flight flits = 14143 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 387.958
	minimum = 27
	maximum = 1985
Network latency average = 331.129
	minimum = 23
	maximum = 1869
Slowest packet = 321
Flit latency average = 237.524
	minimum = 6
	maximum = 1852
Slowest flit = 8369
Fragmentation average = 168.741
	minimum = 0
	maximum = 1428
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00957552
	minimum = 0.0045 (at node 96)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.184771
	minimum = 0.095 (at node 96)
	maximum = 0.28 (at node 152)
Injected packet length average = 17.9095
Accepted packet length average = 19.2962
Total in-flight flits = 20128 (0 measured)
latency change    = 0.256725
throughput change = 0.0414083
Class 0:
Packet latency average = 578.98
	minimum = 25
	maximum = 2675
Network latency average = 518.129
	minimum = 23
	maximum = 2581
Slowest packet = 828
Flit latency average = 405.811
	minimum = 6
	maximum = 2921
Slowest flit = 5979
Fragmentation average = 239.469
	minimum = 0
	maximum = 2262
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.0109844
	minimum = 0.004 (at node 62)
	maximum = 0.019 (at node 131)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.201604
	minimum = 0.096 (at node 178)
	maximum = 0.313 (at node 177)
Injected packet length average = 18.0569
Accepted packet length average = 18.3537
Total in-flight flits = 26348 (0 measured)
latency change    = 0.329929
throughput change = 0.083497
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 368.719
	minimum = 27
	maximum = 1085
Network latency average = 312.558
	minimum = 23
	maximum = 921
Slowest packet = 7597
Flit latency average = 514.846
	minimum = 6
	maximum = 3835
Slowest flit = 2843
Fragmentation average = 175.358
	minimum = 0
	maximum = 769
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.0114635
	minimum = 0.003 (at node 153)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.210349
	minimum = 0.062 (at node 153)
	maximum = 0.467 (at node 16)
Injected packet length average = 17.981
Accepted packet length average = 18.3494
Total in-flight flits = 32329 (22931 measured)
latency change    = 0.570248
throughput change = 0.0415728
Class 0:
Packet latency average = 500.428
	minimum = 25
	maximum = 2129
Network latency average = 438.278
	minimum = 23
	maximum = 1861
Slowest packet = 7597
Flit latency average = 550.097
	minimum = 6
	maximum = 4506
Slowest flit = 6461
Fragmentation average = 203.352
	minimum = 0
	maximum = 1275
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.0116536
	minimum = 0.0065 (at node 134)
	maximum = 0.0215 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.213651
	minimum = 0.123 (at node 134)
	maximum = 0.4055 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 18.3334
Total in-flight flits = 38662 (33563 measured)
latency change    = 0.263193
throughput change = 0.0154555
Class 0:
Packet latency average = 611.163
	minimum = 25
	maximum = 2939
Network latency average = 547.809
	minimum = 23
	maximum = 2899
Slowest packet = 7583
Flit latency average = 607.679
	minimum = 6
	maximum = 5411
Slowest flit = 13571
Fragmentation average = 225.99
	minimum = 0
	maximum = 2544
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.0117656
	minimum = 0.008 (at node 64)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.21438
	minimum = 0.146333 (at node 79)
	maximum = 0.311333 (at node 16)
Injected packet length average = 17.9913
Accepted packet length average = 18.2209
Total in-flight flits = 39395 (36275 measured)
latency change    = 0.181186
throughput change = 0.00340128
Class 0:
Packet latency average = 693.786
	minimum = 23
	maximum = 3865
Network latency average = 629.401
	minimum = 23
	maximum = 3763
Slowest packet = 7694
Flit latency average = 641.223
	minimum = 6
	maximum = 6640
Slowest flit = 19872
Fragmentation average = 241.679
	minimum = 0
	maximum = 3292
Injected packet rate average = 0.0133529
	minimum = 0.00175 (at node 78)
	maximum = 0.02675 (at node 97)
Accepted packet rate average = 0.0118151
	minimum = 0.008 (at node 2)
	maximum = 0.01675 (at node 128)
Injected flit rate average = 0.24025
	minimum = 0.0315 (at node 78)
	maximum = 0.48225 (at node 97)
Accepted flit rate average= 0.214716
	minimum = 0.14925 (at node 36)
	maximum = 0.302 (at node 128)
Injected packet length average = 17.9924
Accepted packet length average = 18.173
Total in-flight flits = 46036 (43720 measured)
latency change    = 0.119091
throughput change = 0.00156457
Class 0:
Packet latency average = 765.21
	minimum = 23
	maximum = 5120
Network latency average = 700.513
	minimum = 23
	maximum = 4835
Slowest packet = 7929
Flit latency average = 688.909
	minimum = 6
	maximum = 7300
Slowest flit = 33695
Fragmentation average = 252.628
	minimum = 0
	maximum = 3292
Injected packet rate average = 0.0133448
	minimum = 0.004 (at node 78)
	maximum = 0.0256 (at node 109)
Accepted packet rate average = 0.0118427
	minimum = 0.0086 (at node 135)
	maximum = 0.0168 (at node 128)
Injected flit rate average = 0.240096
	minimum = 0.0712 (at node 119)
	maximum = 0.4608 (at node 109)
Accepted flit rate average= 0.215004
	minimum = 0.1606 (at node 135)
	maximum = 0.3012 (at node 128)
Injected packet length average = 17.9917
Accepted packet length average = 18.155
Total in-flight flits = 50542 (48916 measured)
latency change    = 0.0933384
throughput change = 0.00133961
Class 0:
Packet latency average = 820.111
	minimum = 23
	maximum = 5724
Network latency average = 755.476
	minimum = 23
	maximum = 5589
Slowest packet = 7610
Flit latency average = 724.976
	minimum = 6
	maximum = 8234
Slowest flit = 32291
Fragmentation average = 260.887
	minimum = 0
	maximum = 4526
Injected packet rate average = 0.0132144
	minimum = 0.00583333 (at node 119)
	maximum = 0.0248333 (at node 28)
Accepted packet rate average = 0.0118576
	minimum = 0.009 (at node 2)
	maximum = 0.0158333 (at node 128)
Injected flit rate average = 0.237734
	minimum = 0.105 (at node 119)
	maximum = 0.446167 (at node 28)
Accepted flit rate average= 0.215127
	minimum = 0.1645 (at node 79)
	maximum = 0.289 (at node 128)
Injected packet length average = 17.9905
Accepted packet length average = 18.1425
Total in-flight flits = 52536 (51352 measured)
latency change    = 0.0669431
throughput change = 0.000569755
Class 0:
Packet latency average = 873.568
	minimum = 23
	maximum = 6924
Network latency average = 809.297
	minimum = 23
	maximum = 6896
Slowest packet = 7576
Flit latency average = 762.986
	minimum = 6
	maximum = 8624
Slowest flit = 63124
Fragmentation average = 269.31
	minimum = 0
	maximum = 5318
Injected packet rate average = 0.0132269
	minimum = 0.00542857 (at node 16)
	maximum = 0.0238571 (at node 133)
Accepted packet rate average = 0.0118854
	minimum = 0.00885714 (at node 79)
	maximum = 0.0162857 (at node 128)
Injected flit rate average = 0.238065
	minimum = 0.0977143 (at node 16)
	maximum = 0.428714 (at node 133)
Accepted flit rate average= 0.215339
	minimum = 0.157714 (at node 79)
	maximum = 0.293857 (at node 128)
Injected packet length average = 17.9985
Accepted packet length average = 18.1179
Total in-flight flits = 56918 (56017 measured)
latency change    = 0.0611941
throughput change = 0.000987045
Draining all recorded packets ...
Class 0:
Remaining flits: 19887 19888 19889 22716 22717 22718 22719 22720 22721 22722 [...] (63304 flits)
Measured flits: 136065 136066 136067 136068 136069 136070 136071 136072 136073 136074 [...] (33084 flits)
Class 0:
Remaining flits: 22716 22717 22718 22719 22720 22721 22722 22723 22724 22725 [...] (70147 flits)
Measured flits: 136065 136066 136067 136068 136069 136070 136071 136072 136073 136074 [...] (21225 flits)
Class 0:
Remaining flits: 35748 35749 35750 35751 35752 35753 35754 35755 35756 35757 [...] (71878 flits)
Measured flits: 136065 136066 136067 136068 136069 136070 136071 136072 136073 136074 [...] (14412 flits)
Class 0:
Remaining flits: 35748 35749 35750 35751 35752 35753 35754 35755 35756 35757 [...] (76364 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (10417 flits)
Class 0:
Remaining flits: 35748 35749 35750 35751 35752 35753 35754 35755 35756 35757 [...] (84040 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (7587 flits)
Class 0:
Remaining flits: 45684 45685 45686 45687 45688 45689 45690 45691 45692 45693 [...] (87672 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (5857 flits)
Class 0:
Remaining flits: 45684 45685 45686 45687 45688 45689 45690 45691 45692 45693 [...] (93807 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (4718 flits)
Class 0:
Remaining flits: 45684 45685 45686 45687 45688 45689 45690 45691 45692 45693 [...] (101802 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (3718 flits)
Class 0:
Remaining flits: 45684 45685 45686 45687 45688 45689 45690 45691 45692 45693 [...] (106558 flits)
Measured flits: 136890 136891 136892 136893 136894 136895 136896 136897 136898 136899 [...] (3021 flits)
Class 0:
Remaining flits: 45688 45689 45690 45691 45692 45693 45694 45695 45696 45697 [...] (109251 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (2505 flits)
Class 0:
Remaining flits: 64518 64519 64520 64521 64522 64523 64524 64525 64526 64527 [...] (114068 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (1870 flits)
Class 0:
Remaining flits: 64518 64519 64520 64521 64522 64523 64524 64525 64526 64527 [...] (121267 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (1400 flits)
Class 0:
Remaining flits: 125095 125096 125097 125098 125099 127755 127756 127757 127758 127759 [...] (131037 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (956 flits)
Class 0:
Remaining flits: 125095 125096 125097 125098 125099 127760 127761 127762 127763 146753 [...] (137934 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (739 flits)
Class 0:
Remaining flits: 127760 127761 127762 127763 146753 150912 150913 150914 150915 150916 [...] (147305 flits)
Measured flits: 146753 150912 150913 150914 150915 150916 150917 150918 150919 150920 [...] (565 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (154946 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (439 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (162885 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (348 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (171361 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (287 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (177827 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (180 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (185520 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (108 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (195728 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (89 flits)
Class 0:
Remaining flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (200777 flits)
Measured flits: 170640 170641 170642 170643 170644 170645 170646 170647 170648 170649 [...] (72 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (209012 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (36 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (218212 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (222988 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (233743 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (242574 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (248628 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (256341 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (263716 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (270984 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Class 0:
Remaining flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (282772 flits)
Measured flits: 384246 384247 384248 384249 384250 384251 384252 384253 384254 384255 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 499491 499492 499493 499494 499495 499496 499497 499498 499499 530766 [...] (251271 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 499491 499492 499493 499494 499495 499496 499497 499498 499499 530766 [...] (218479 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 499491 499492 499493 499494 499495 499496 499497 499498 499499 530766 [...] (186357 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 690338 690339 690340 690341 690342 690343 690344 690345 690346 690347 [...] (155926 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 695844 695845 695846 695847 695848 695849 695850 695851 695852 695853 [...] (127016 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 695844 695845 695846 695847 695848 695849 695850 695851 695852 695853 [...] (99043 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 695844 695845 695846 695847 695848 695849 695850 695851 695852 695853 [...] (73336 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 771120 771121 771122 771123 771124 771125 771126 771127 771128 771129 [...] (51935 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 771123 771124 771125 771126 771127 771128 771129 771130 771131 771132 [...] (34115 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 771125 771126 771127 771128 771129 771130 771131 771132 771133 771134 [...] (20315 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 771125 771126 771127 771128 771129 771130 771131 771132 771133 771134 [...] (10221 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1089756 1089757 1089758 1089759 1089760 1089761 1089762 1089763 1089764 1089765 [...] (3908 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1089760 1089761 1089762 1089763 1089764 1089765 1089766 1089767 1089768 1089769 [...] (1362 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1574636 1574637 1574638 1574639 1620666 1620667 1620668 1620669 1620670 1620671 [...] (364 flits)
Measured flits: (0 flits)
Time taken is 56599 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1512.7 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33867 (1 samples)
Network latency average = 1444.45 (1 samples)
	minimum = 23 (1 samples)
	maximum = 33805 (1 samples)
Flit latency average = 3738.63 (1 samples)
	minimum = 6 (1 samples)
	maximum = 36644 (1 samples)
Fragmentation average = 350.84 (1 samples)
	minimum = 0 (1 samples)
	maximum = 14455 (1 samples)
Injected packet rate average = 0.0132269 (1 samples)
	minimum = 0.00542857 (1 samples)
	maximum = 0.0238571 (1 samples)
Accepted packet rate average = 0.0118854 (1 samples)
	minimum = 0.00885714 (1 samples)
	maximum = 0.0162857 (1 samples)
Injected flit rate average = 0.238065 (1 samples)
	minimum = 0.0977143 (1 samples)
	maximum = 0.428714 (1 samples)
Accepted flit rate average = 0.215339 (1 samples)
	minimum = 0.157714 (1 samples)
	maximum = 0.293857 (1 samples)
Injected packet size average = 17.9985 (1 samples)
Accepted packet size average = 18.1179 (1 samples)
Hops average = 5.08203 (1 samples)
Total run time 70.1212
