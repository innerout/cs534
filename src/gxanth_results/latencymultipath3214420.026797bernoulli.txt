BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 256.546
	minimum = 22
	maximum = 750
Network latency average = 249.992
	minimum = 22
	maximum = 748
Slowest packet = 1143
Flit latency average = 218.7
	minimum = 5
	maximum = 754
Slowest flit = 20446
Fragmentation average = 45.7404
	minimum = 0
	maximum = 347
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0138854
	minimum = 0.007 (at node 64)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.260271
	minimum = 0.126 (at node 174)
	maximum = 0.442 (at node 44)
Injected packet length average = 17.8601
Accepted packet length average = 18.7442
Total in-flight flits = 42638 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 465.116
	minimum = 22
	maximum = 1355
Network latency average = 457.598
	minimum = 22
	maximum = 1327
Slowest packet = 2133
Flit latency average = 425.064
	minimum = 5
	maximum = 1415
Slowest flit = 52476
Fragmentation average = 52.8679
	minimum = 0
	maximum = 394
Injected packet rate average = 0.0266823
	minimum = 0.017 (at node 24)
	maximum = 0.036 (at node 191)
Accepted packet rate average = 0.0145885
	minimum = 0.0085 (at node 30)
	maximum = 0.0205 (at node 14)
Injected flit rate average = 0.478349
	minimum = 0.305 (at node 24)
	maximum = 0.644 (at node 191)
Accepted flit rate average= 0.26881
	minimum = 0.1595 (at node 96)
	maximum = 0.3795 (at node 14)
Injected packet length average = 17.9276
Accepted packet length average = 18.4261
Total in-flight flits = 81205 (0 measured)
latency change    = 0.448425
throughput change = 0.0317662
Class 0:
Packet latency average = 1040.86
	minimum = 22
	maximum = 1892
Network latency average = 1033.06
	minimum = 22
	maximum = 1883
Slowest packet = 5069
Flit latency average = 1003.58
	minimum = 5
	maximum = 1935
Slowest flit = 97053
Fragmentation average = 66.7382
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0267031
	minimum = 0.013 (at node 139)
	maximum = 0.039 (at node 34)
Accepted packet rate average = 0.0155781
	minimum = 0.007 (at node 146)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.480292
	minimum = 0.229 (at node 139)
	maximum = 0.702 (at node 34)
Accepted flit rate average= 0.282307
	minimum = 0.133 (at node 146)
	maximum = 0.546 (at node 16)
Injected packet length average = 17.9863
Accepted packet length average = 18.122
Total in-flight flits = 119288 (0 measured)
latency change    = 0.553145
throughput change = 0.047811
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 43.9674
	minimum = 22
	maximum = 125
Network latency average = 34.8045
	minimum = 22
	maximum = 125
Slowest packet = 16985
Flit latency average = 1397.36
	minimum = 5
	maximum = 2659
Slowest flit = 116837
Fragmentation average = 5.86967
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0271667
	minimum = 0.014 (at node 159)
	maximum = 0.043 (at node 104)
Accepted packet rate average = 0.0159583
	minimum = 0.007 (at node 4)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.487828
	minimum = 0.258 (at node 186)
	maximum = 0.759 (at node 104)
Accepted flit rate average= 0.285854
	minimum = 0.128 (at node 71)
	maximum = 0.514 (at node 123)
Injected packet length average = 17.9569
Accepted packet length average = 17.9125
Total in-flight flits = 158310 (86582 measured)
latency change    = 22.6736
throughput change = 0.012408
Class 0:
Packet latency average = 127.308
	minimum = 22
	maximum = 2006
Network latency average = 119.037
	minimum = 22
	maximum = 1985
Slowest packet = 15403
Flit latency average = 1580.28
	minimum = 5
	maximum = 2942
Slowest flit = 186009
Fragmentation average = 8.65076
	minimum = 0
	maximum = 167
Injected packet rate average = 0.026651
	minimum = 0.0165 (at node 132)
	maximum = 0.038 (at node 104)
Accepted packet rate average = 0.0158802
	minimum = 0.0095 (at node 36)
	maximum = 0.0245 (at node 129)
Injected flit rate average = 0.479745
	minimum = 0.289 (at node 132)
	maximum = 0.682 (at node 104)
Accepted flit rate average= 0.284099
	minimum = 0.1705 (at node 36)
	maximum = 0.435 (at node 129)
Injected packet length average = 18.001
Accepted packet length average = 17.8901
Total in-flight flits = 194442 (168540 measured)
latency change    = 0.654639
throughput change = 0.00617816
Class 0:
Packet latency average = 1255.61
	minimum = 22
	maximum = 2974
Network latency average = 1246.65
	minimum = 22
	maximum = 2974
Slowest packet = 15436
Flit latency average = 1763.76
	minimum = 5
	maximum = 3748
Slowest flit = 206285
Fragmentation average = 34.2889
	minimum = 0
	maximum = 435
Injected packet rate average = 0.0264427
	minimum = 0.019 (at node 100)
	maximum = 0.0346667 (at node 26)
Accepted packet rate average = 0.0159115
	minimum = 0.00933333 (at node 36)
	maximum = 0.0216667 (at node 129)
Injected flit rate average = 0.475937
	minimum = 0.342 (at node 100)
	maximum = 0.625 (at node 26)
Accepted flit rate average= 0.285187
	minimum = 0.169333 (at node 36)
	maximum = 0.396333 (at node 129)
Injected packet length average = 17.9988
Accepted packet length average = 17.9234
Total in-flight flits = 229412 (226293 measured)
latency change    = 0.898609
throughput change = 0.00381693
Class 0:
Packet latency average = 1930.76
	minimum = 22
	maximum = 3869
Network latency average = 1920.28
	minimum = 22
	maximum = 3869
Slowest packet = 15521
Flit latency average = 1959.41
	minimum = 5
	maximum = 4277
Slowest flit = 249250
Fragmentation average = 44.5201
	minimum = 0
	maximum = 457
Injected packet rate average = 0.0260234
	minimum = 0.01675 (at node 100)
	maximum = 0.03375 (at node 81)
Accepted packet rate average = 0.0158125
	minimum = 0.01025 (at node 36)
	maximum = 0.02075 (at node 129)
Injected flit rate average = 0.468271
	minimum = 0.3015 (at node 100)
	maximum = 0.608 (at node 81)
Accepted flit rate average= 0.283652
	minimum = 0.185 (at node 36)
	maximum = 0.374 (at node 129)
Injected packet length average = 17.9942
Accepted packet length average = 17.9385
Total in-flight flits = 261533 (261074 measured)
latency change    = 0.34968
throughput change = 0.0054121
Class 0:
Packet latency average = 2316.62
	minimum = 22
	maximum = 4814
Network latency average = 2303.12
	minimum = 22
	maximum = 4812
Slowest packet = 15468
Flit latency average = 2162.07
	minimum = 5
	maximum = 5052
Slowest flit = 266705
Fragmentation average = 47.7593
	minimum = 0
	maximum = 691
Injected packet rate average = 0.0257667
	minimum = 0.0152 (at node 72)
	maximum = 0.032 (at node 43)
Accepted packet rate average = 0.0156865
	minimum = 0.012 (at node 36)
	maximum = 0.02 (at node 118)
Injected flit rate average = 0.463922
	minimum = 0.2768 (at node 72)
	maximum = 0.576 (at node 43)
Accepted flit rate average= 0.281555
	minimum = 0.2158 (at node 36)
	maximum = 0.3568 (at node 118)
Injected packet length average = 18.0047
Accepted packet length average = 17.9489
Total in-flight flits = 294711 (294603 measured)
latency change    = 0.16656
throughput change = 0.0074484
Class 0:
Packet latency average = 2616.22
	minimum = 22
	maximum = 5859
Network latency average = 2597.47
	minimum = 22
	maximum = 5837
Slowest packet = 16018
Flit latency average = 2362.99
	minimum = 5
	maximum = 6047
Slowest flit = 271182
Fragmentation average = 50.5287
	minimum = 0
	maximum = 721
Injected packet rate average = 0.0253724
	minimum = 0.0155 (at node 68)
	maximum = 0.0318333 (at node 43)
Accepted packet rate average = 0.0156102
	minimum = 0.0121667 (at node 36)
	maximum = 0.0191667 (at node 95)
Injected flit rate average = 0.456688
	minimum = 0.279 (at node 68)
	maximum = 0.5705 (at node 43)
Accepted flit rate average= 0.280155
	minimum = 0.2155 (at node 80)
	maximum = 0.343333 (at node 138)
Injected packet length average = 17.9994
Accepted packet length average = 17.9469
Total in-flight flits = 323085 (323080 measured)
latency change    = 0.114518
throughput change = 0.00499661
Class 0:
Packet latency average = 2874.9
	minimum = 22
	maximum = 6342
Network latency average = 2850.57
	minimum = 22
	maximum = 6342
Slowest packet = 17366
Flit latency average = 2563.08
	minimum = 5
	maximum = 6325
Slowest flit = 312605
Fragmentation average = 50.4426
	minimum = 0
	maximum = 721
Injected packet rate average = 0.0250551
	minimum = 0.0161429 (at node 184)
	maximum = 0.0317143 (at node 49)
Accepted packet rate average = 0.015532
	minimum = 0.012 (at node 80)
	maximum = 0.0192857 (at node 129)
Injected flit rate average = 0.451033
	minimum = 0.292286 (at node 184)
	maximum = 0.571143 (at node 49)
Accepted flit rate average= 0.278799
	minimum = 0.211714 (at node 80)
	maximum = 0.348429 (at node 129)
Injected packet length average = 18.0017
Accepted packet length average = 17.95
Total in-flight flits = 351290 (351290 measured)
latency change    = 0.089978
throughput change = 0.0048647
Draining all recorded packets ...
Class 0:
Remaining flits: 309600 309601 309602 309603 309604 309605 309606 309607 309608 309609 [...] (372840 flits)
Measured flits: 309600 309601 309602 309603 309604 309605 309606 309607 309608 309609 [...] (314030 flits)
Class 0:
Remaining flits: 376596 376597 376598 376599 376600 376601 376602 376603 376604 376605 [...] (390821 flits)
Measured flits: 376596 376597 376598 376599 376600 376601 376602 376603 376604 376605 [...] (277408 flits)
Class 0:
Remaining flits: 425212 425213 428760 428761 428762 428763 428764 428765 428766 428767 [...] (399179 flits)
Measured flits: 425212 425213 428760 428761 428762 428763 428764 428765 428766 428767 [...] (235626 flits)
Class 0:
Remaining flits: 456388 456389 465836 465837 465838 465839 485442 485443 485444 485445 [...] (407566 flits)
Measured flits: 456388 456389 465836 465837 465838 465839 485442 485443 485444 485445 [...] (192105 flits)
Class 0:
Remaining flits: 485442 485443 485444 485445 485446 485447 485448 485449 485450 485451 [...] (413130 flits)
Measured flits: 485442 485443 485444 485445 485446 485447 485448 485449 485450 485451 [...] (147133 flits)
Class 0:
Remaining flits: 494982 494983 494984 494985 494986 494987 494988 494989 494990 494991 [...] (412594 flits)
Measured flits: 494982 494983 494984 494985 494986 494987 494988 494989 494990 494991 [...] (105393 flits)
Class 0:
Remaining flits: 494982 494983 494984 494985 494986 494987 494988 494989 494990 494991 [...] (415272 flits)
Measured flits: 494982 494983 494984 494985 494986 494987 494988 494989 494990 494991 [...] (69617 flits)
Class 0:
Remaining flits: 611478 611479 611480 611481 611482 611483 611484 611485 611486 611487 [...] (415673 flits)
Measured flits: 611478 611479 611480 611481 611482 611483 611484 611485 611486 611487 [...] (40745 flits)
Class 0:
Remaining flits: 621774 621775 621776 621777 621778 621779 621780 621781 621782 621783 [...] (416098 flits)
Measured flits: 621774 621775 621776 621777 621778 621779 621780 621781 621782 621783 [...] (21599 flits)
Class 0:
Remaining flits: 664362 664363 664364 664365 664366 664367 664368 664369 664370 664371 [...] (417954 flits)
Measured flits: 664362 664363 664364 664365 664366 664367 664368 664369 664370 664371 [...] (9633 flits)
Class 0:
Remaining flits: 664362 664363 664364 664365 664366 664367 664368 664369 664370 664371 [...] (418411 flits)
Measured flits: 664362 664363 664364 664365 664366 664367 664368 664369 664370 664371 [...] (3958 flits)
Class 0:
Remaining flits: 715242 715243 715244 715245 715246 715247 716886 716887 716888 716889 [...] (421721 flits)
Measured flits: 715242 715243 715244 715245 715246 715247 716886 716887 716888 716889 [...] (1575 flits)
Class 0:
Remaining flits: 758178 758179 758180 758181 758182 758183 758184 758185 758186 758187 [...] (421037 flits)
Measured flits: 758178 758179 758180 758181 758182 758183 758184 758185 758186 758187 [...] (582 flits)
Class 0:
Remaining flits: 777397 777398 777399 777400 777401 827496 827497 827498 827499 827500 [...] (421995 flits)
Measured flits: 777397 777398 777399 777400 777401 827496 827497 827498 827499 827500 [...] (183 flits)
Class 0:
Remaining flits: 827502 827503 827504 827505 827506 827507 827508 827509 827510 827511 [...] (422225 flits)
Measured flits: 827502 827503 827504 827505 827506 827507 827508 827509 827510 827511 [...] (30 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 984294 984295 984296 984297 984298 984299 984300 984301 984302 984303 [...] (372361 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010646 1010647 1010648 1010649 1010650 1010651 1010652 1010653 1010654 1010655 [...] (322488 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1091577 1091578 1091579 1091580 1091581 1091582 1091583 1091584 1091585 1091586 [...] (272639 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1112476 1112477 1112478 1112479 1112480 1112481 1112482 1112483 1112484 1112485 [...] (224091 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1222452 1222453 1222454 1222455 1222456 1222457 1222458 1222459 1222460 1222461 [...] (176254 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1242108 1242109 1242110 1242111 1242112 1242113 1242114 1242115 1242116 1242117 [...] (128622 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1270656 1270657 1270658 1270659 1270660 1270661 1270662 1270663 1270664 1270665 [...] (81289 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1284480 1284481 1284482 1284483 1284484 1284485 1284486 1284487 1284488 1284489 [...] (33855 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1445382 1445383 1445384 1445385 1445386 1445387 1445388 1445389 1445390 1445391 [...] (6899 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1619512 1619513 1627938 1627939 1627940 1627941 1627942 1627943 1627944 1627945 [...] (241 flits)
Measured flits: (0 flits)
Time taken is 35735 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 5113.14 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16105 (1 samples)
Network latency average = 4857.84 (1 samples)
	minimum = 22 (1 samples)
	maximum = 16079 (1 samples)
Flit latency average = 6195.01 (1 samples)
	minimum = 5 (1 samples)
	maximum = 17242 (1 samples)
Fragmentation average = 50.0537 (1 samples)
	minimum = 0 (1 samples)
	maximum = 743 (1 samples)
Injected packet rate average = 0.0250551 (1 samples)
	minimum = 0.0161429 (1 samples)
	maximum = 0.0317143 (1 samples)
Accepted packet rate average = 0.015532 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0192857 (1 samples)
Injected flit rate average = 0.451033 (1 samples)
	minimum = 0.292286 (1 samples)
	maximum = 0.571143 (1 samples)
Accepted flit rate average = 0.278799 (1 samples)
	minimum = 0.211714 (1 samples)
	maximum = 0.348429 (1 samples)
Injected packet size average = 18.0017 (1 samples)
Accepted packet size average = 17.95 (1 samples)
Hops average = 5.07387 (1 samples)
Total run time 46.4565
