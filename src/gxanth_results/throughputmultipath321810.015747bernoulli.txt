BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 223.835
	minimum = 23
	maximum = 927
Network latency average = 220.554
	minimum = 23
	maximum = 927
Slowest packet = 64
Flit latency average = 150.38
	minimum = 6
	maximum = 951
Slowest flit = 1527
Fragmentation average = 130.734
	minimum = 0
	maximum = 784
Injected packet rate average = 0.015375
	minimum = 0.008 (at node 19)
	maximum = 0.026 (at node 42)
Accepted packet rate average = 0.00936458
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 140)
Injected flit rate average = 0.273953
	minimum = 0.144 (at node 19)
	maximum = 0.468 (at node 89)
Accepted flit rate average= 0.189677
	minimum = 0.052 (at node 174)
	maximum = 0.324 (at node 140)
Injected packet length average = 17.8181
Accepted packet length average = 20.2547
Total in-flight flits = 16736 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 366.435
	minimum = 23
	maximum = 1589
Network latency average = 351.662
	minimum = 23
	maximum = 1589
Slowest packet = 1067
Flit latency average = 260.182
	minimum = 6
	maximum = 1790
Slowest flit = 10130
Fragmentation average = 166.318
	minimum = 0
	maximum = 1404
Injected packet rate average = 0.0141771
	minimum = 0.007 (at node 26)
	maximum = 0.02 (at node 42)
Accepted packet rate average = 0.0102109
	minimum = 0.0055 (at node 30)
	maximum = 0.0175 (at node 22)
Injected flit rate average = 0.252661
	minimum = 0.122 (at node 149)
	maximum = 0.36 (at node 69)
Accepted flit rate average= 0.194883
	minimum = 0.109 (at node 62)
	maximum = 0.3295 (at node 22)
Injected packet length average = 17.8218
Accepted packet length average = 19.0857
Total in-flight flits = 23229 (0 measured)
latency change    = 0.389153
throughput change = 0.0267121
Class 0:
Packet latency average = 732.267
	minimum = 25
	maximum = 2599
Network latency average = 610.143
	minimum = 25
	maximum = 2586
Slowest packet = 725
Flit latency average = 500.456
	minimum = 6
	maximum = 2569
Slowest flit = 13067
Fragmentation average = 206.778
	minimum = 0
	maximum = 2164
Injected packet rate average = 0.0124896
	minimum = 0 (at node 5)
	maximum = 0.029 (at node 39)
Accepted packet rate average = 0.0110469
	minimum = 0.004 (at node 72)
	maximum = 0.021 (at node 34)
Injected flit rate average = 0.225068
	minimum = 0 (at node 5)
	maximum = 0.522 (at node 39)
Accepted flit rate average= 0.201417
	minimum = 0.057 (at node 72)
	maximum = 0.355 (at node 34)
Injected packet length average = 18.0204
Accepted packet length average = 18.2329
Total in-flight flits = 27775 (0 measured)
latency change    = 0.499589
throughput change = 0.0324395
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 752.529
	minimum = 24
	maximum = 3118
Network latency average = 349.109
	minimum = 24
	maximum = 939
Slowest packet = 7875
Flit latency average = 635.83
	minimum = 6
	maximum = 3771
Slowest flit = 6533
Fragmentation average = 147.575
	minimum = 0
	maximum = 795
Injected packet rate average = 0.0121146
	minimum = 0 (at node 95)
	maximum = 0.026 (at node 165)
Accepted packet rate average = 0.0110781
	minimum = 0.004 (at node 4)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.21701
	minimum = 0 (at node 97)
	maximum = 0.464 (at node 165)
Accepted flit rate average= 0.198615
	minimum = 0.088 (at node 26)
	maximum = 0.419 (at node 16)
Injected packet length average = 17.9132
Accepted packet length average = 17.9285
Total in-flight flits = 31599 (24643 measured)
latency change    = 0.0269243
throughput change = 0.0141081
Class 0:
Packet latency average = 1025.31
	minimum = 24
	maximum = 3849
Network latency average = 544.781
	minimum = 24
	maximum = 1889
Slowest packet = 7875
Flit latency average = 678.03
	minimum = 6
	maximum = 3932
Slowest flit = 53657
Fragmentation average = 169.316
	minimum = 0
	maximum = 1379
Injected packet rate average = 0.0118464
	minimum = 0 (at node 179)
	maximum = 0.022 (at node 142)
Accepted packet rate average = 0.0110469
	minimum = 0.0055 (at node 4)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.212745
	minimum = 0 (at node 179)
	maximum = 0.399 (at node 142)
Accepted flit rate average= 0.198492
	minimum = 0.087 (at node 4)
	maximum = 0.335 (at node 16)
Injected packet length average = 17.9587
Accepted packet length average = 17.9682
Total in-flight flits = 33742 (31757 measured)
latency change    = 0.266051
throughput change = 0.000616628
Class 0:
Packet latency average = 1241.76
	minimum = 24
	maximum = 4507
Network latency average = 675.934
	minimum = 24
	maximum = 2870
Slowest packet = 7875
Flit latency average = 719.315
	minimum = 6
	maximum = 4864
Slowest flit = 47393
Fragmentation average = 182.57
	minimum = 0
	maximum = 2161
Injected packet rate average = 0.011401
	minimum = 0.00266667 (at node 131)
	maximum = 0.021 (at node 117)
Accepted packet rate average = 0.0109583
	minimum = 0.00666667 (at node 86)
	maximum = 0.0173333 (at node 16)
Injected flit rate average = 0.204634
	minimum = 0.046 (at node 131)
	maximum = 0.378667 (at node 117)
Accepted flit rate average= 0.197063
	minimum = 0.128667 (at node 132)
	maximum = 0.300667 (at node 16)
Injected packet length average = 17.9487
Accepted packet length average = 17.9829
Total in-flight flits = 32707 (31981 measured)
latency change    = 0.174307
throughput change = 0.007255
Draining remaining packets ...
Class 0:
Remaining flits: 129372 129373 129374 129375 129376 129377 129378 129379 129380 129381 [...] (2341 flits)
Measured flits: 157011 157012 157013 161244 161245 161246 161247 161248 161249 161250 [...] (2317 flits)
Time taken is 7405 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1526.74 (1 samples)
	minimum = 24 (1 samples)
	maximum = 5826 (1 samples)
Network latency average = 869.153 (1 samples)
	minimum = 24 (1 samples)
	maximum = 3870 (1 samples)
Flit latency average = 840.419 (1 samples)
	minimum = 6 (1 samples)
	maximum = 5254 (1 samples)
Fragmentation average = 185.048 (1 samples)
	minimum = 0 (1 samples)
	maximum = 2707 (1 samples)
Injected packet rate average = 0.011401 (1 samples)
	minimum = 0.00266667 (1 samples)
	maximum = 0.021 (1 samples)
Accepted packet rate average = 0.0109583 (1 samples)
	minimum = 0.00666667 (1 samples)
	maximum = 0.0173333 (1 samples)
Injected flit rate average = 0.204634 (1 samples)
	minimum = 0.046 (1 samples)
	maximum = 0.378667 (1 samples)
Accepted flit rate average = 0.197063 (1 samples)
	minimum = 0.128667 (1 samples)
	maximum = 0.300667 (1 samples)
Injected packet size average = 17.9487 (1 samples)
Accepted packet size average = 17.9829 (1 samples)
Hops average = 5.09985 (1 samples)
Total run time 6.76914
