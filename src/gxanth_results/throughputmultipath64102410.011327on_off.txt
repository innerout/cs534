BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.011327
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 236.764
	minimum = 27
	maximum = 907
Network latency average = 192.804
	minimum = 23
	maximum = 848
Slowest packet = 265
Flit latency average = 124.889
	minimum = 6
	maximum = 861
Slowest flit = 7028
Fragmentation average = 110.394
	minimum = 0
	maximum = 678
Injected packet rate average = 0.011901
	minimum = 0 (at node 35)
	maximum = 0.037 (at node 70)
Accepted packet rate average = 0.00819271
	minimum = 0.002 (at node 174)
	maximum = 0.016 (at node 70)
Injected flit rate average = 0.212328
	minimum = 0 (at node 35)
	maximum = 0.666 (at node 88)
Accepted flit rate average= 0.163031
	minimum = 0.036 (at node 174)
	maximum = 0.318 (at node 70)
Injected packet length average = 17.8411
Accepted packet length average = 19.8996
Total in-flight flits = 9828 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 315.892
	minimum = 27
	maximum = 1736
Network latency average = 267.434
	minimum = 23
	maximum = 1622
Slowest packet = 472
Flit latency average = 186.107
	minimum = 6
	maximum = 1605
Slowest flit = 12113
Fragmentation average = 143.4
	minimum = 0
	maximum = 1219
Injected packet rate average = 0.0115156
	minimum = 0.0005 (at node 87)
	maximum = 0.0305 (at node 70)
Accepted packet rate average = 0.00893229
	minimum = 0.005 (at node 23)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.206414
	minimum = 0.009 (at node 87)
	maximum = 0.543 (at node 70)
Accepted flit rate average= 0.171867
	minimum = 0.098 (at node 79)
	maximum = 0.291 (at node 22)
Injected packet length average = 17.9247
Accepted packet length average = 19.2411
Total in-flight flits = 13599 (0 measured)
latency change    = 0.250491
throughput change = 0.0514114
Class 0:
Packet latency average = 461.676
	minimum = 23
	maximum = 2382
Network latency average = 414.588
	minimum = 23
	maximum = 2335
Slowest packet = 852
Flit latency average = 310.928
	minimum = 6
	maximum = 2693
Slowest flit = 10544
Fragmentation average = 200.358
	minimum = 0
	maximum = 2229
Injected packet rate average = 0.0113125
	minimum = 0 (at node 2)
	maximum = 0.031 (at node 97)
Accepted packet rate average = 0.0106615
	minimum = 0.004 (at node 117)
	maximum = 0.018 (at node 6)
Injected flit rate average = 0.203552
	minimum = 0 (at node 2)
	maximum = 0.542 (at node 97)
Accepted flit rate average= 0.192875
	minimum = 0.079 (at node 116)
	maximum = 0.343 (at node 16)
Injected packet length average = 17.9936
Accepted packet length average = 18.0909
Total in-flight flits = 15663 (0 measured)
latency change    = 0.31577
throughput change = 0.108919
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 337.67
	minimum = 23
	maximum = 1107
Network latency average = 291.285
	minimum = 23
	maximum = 996
Slowest packet = 6606
Flit latency average = 360.877
	minimum = 6
	maximum = 3361
Slowest flit = 24389
Fragmentation average = 169.193
	minimum = 0
	maximum = 824
Injected packet rate average = 0.0117917
	minimum = 0 (at node 78)
	maximum = 0.045 (at node 41)
Accepted packet rate average = 0.0107344
	minimum = 0.003 (at node 163)
	maximum = 0.022 (at node 120)
Injected flit rate average = 0.212542
	minimum = 0 (at node 78)
	maximum = 0.821 (at node 41)
Accepted flit rate average= 0.195885
	minimum = 0.071 (at node 163)
	maximum = 0.414 (at node 159)
Injected packet length average = 18.0247
Accepted packet length average = 18.2484
Total in-flight flits = 18805 (15057 measured)
latency change    = 0.367237
throughput change = 0.0153683
Class 0:
Packet latency average = 433.174
	minimum = 23
	maximum = 1908
Network latency average = 384
	minimum = 23
	maximum = 1797
Slowest packet = 6622
Flit latency average = 398.966
	minimum = 6
	maximum = 4734
Slowest flit = 11607
Fragmentation average = 193.515
	minimum = 0
	maximum = 1414
Injected packet rate average = 0.0115911
	minimum = 0 (at node 131)
	maximum = 0.0365 (at node 153)
Accepted packet rate average = 0.0108125
	minimum = 0.005 (at node 17)
	maximum = 0.0185 (at node 120)
Injected flit rate average = 0.208357
	minimum = 0 (at node 131)
	maximum = 0.6575 (at node 153)
Accepted flit rate average= 0.195789
	minimum = 0.093 (at node 17)
	maximum = 0.323 (at node 120)
Injected packet length average = 17.9755
Accepted packet length average = 18.1077
Total in-flight flits = 20598 (19039 measured)
latency change    = 0.220474
throughput change = 0.000492133
Class 0:
Packet latency average = 494.834
	minimum = 23
	maximum = 2767
Network latency average = 444.126
	minimum = 23
	maximum = 2713
Slowest packet = 6713
Flit latency average = 423.185
	minimum = 6
	maximum = 5278
Slowest flit = 14795
Fragmentation average = 206.567
	minimum = 0
	maximum = 1870
Injected packet rate average = 0.011276
	minimum = 0.00333333 (at node 28)
	maximum = 0.031 (at node 153)
Accepted packet rate average = 0.0108229
	minimum = 0.006 (at node 4)
	maximum = 0.0156667 (at node 99)
Injected flit rate average = 0.20297
	minimum = 0.06 (at node 28)
	maximum = 0.561333 (at node 153)
Accepted flit rate average= 0.195484
	minimum = 0.109333 (at node 4)
	maximum = 0.268667 (at node 129)
Injected packet length average = 18.0002
Accepted packet length average = 18.0621
Total in-flight flits = 19974 (19045 measured)
latency change    = 0.124608
throughput change = 0.00155863
Draining remaining packets ...
Class 0:
Remaining flits: 18882 18883 18884 18885 18886 18887 18888 18889 18890 18891 [...] (1758 flits)
Measured flits: 118764 118765 118766 118767 118768 118769 118770 118771 118772 118773 [...] (1639 flits)
Class 0:
Remaining flits: (0 flits)
Measured flits: (0 flits)
Time taken is 8002 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 648.097 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4510 (1 samples)
Network latency average = 593.203 (1 samples)
	minimum = 23 (1 samples)
	maximum = 4510 (1 samples)
Flit latency average = 555.015 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7154 (1 samples)
Fragmentation average = 217.36 (1 samples)
	minimum = 0 (1 samples)
	maximum = 3672 (1 samples)
Injected packet rate average = 0.011276 (1 samples)
	minimum = 0.00333333 (1 samples)
	maximum = 0.031 (1 samples)
Accepted packet rate average = 0.0108229 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0156667 (1 samples)
Injected flit rate average = 0.20297 (1 samples)
	minimum = 0.06 (1 samples)
	maximum = 0.561333 (1 samples)
Accepted flit rate average = 0.195484 (1 samples)
	minimum = 0.109333 (1 samples)
	maximum = 0.268667 (1 samples)
Injected packet size average = 18.0002 (1 samples)
Accepted packet size average = 18.0621 (1 samples)
Hops average = 5.08668 (1 samples)
Total run time 6.17357
