BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 311.584
	minimum = 26
	maximum = 970
Network latency average = 249.948
	minimum = 25
	maximum = 871
Slowest packet = 69
Flit latency average = 212.158
	minimum = 6
	maximum = 857
Slowest flit = 9996
Fragmentation average = 55.3799
	minimum = 0
	maximum = 137
Injected packet rate average = 0.0207865
	minimum = 0 (at node 90)
	maximum = 0.055 (at node 22)
Accepted packet rate average = 0.00932292
	minimum = 0.003 (at node 41)
	maximum = 0.018 (at node 48)
Injected flit rate average = 0.370661
	minimum = 0 (at node 90)
	maximum = 0.979 (at node 22)
Accepted flit rate average= 0.175125
	minimum = 0.064 (at node 41)
	maximum = 0.326 (at node 48)
Injected packet length average = 17.8319
Accepted packet length average = 18.7844
Total in-flight flits = 38232 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 534.201
	minimum = 23
	maximum = 1920
Network latency average = 448.407
	minimum = 23
	maximum = 1788
Slowest packet = 352
Flit latency average = 410.113
	minimum = 6
	maximum = 1771
Slowest flit = 6353
Fragmentation average = 61.7693
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0198516
	minimum = 0.0005 (at node 90)
	maximum = 0.047 (at node 22)
Accepted packet rate average = 0.00963802
	minimum = 0.0055 (at node 66)
	maximum = 0.0145 (at node 14)
Injected flit rate average = 0.355667
	minimum = 0.009 (at node 90)
	maximum = 0.8415 (at node 22)
Accepted flit rate average= 0.177576
	minimum = 0.099 (at node 164)
	maximum = 0.267 (at node 156)
Injected packet length average = 17.9163
Accepted packet length average = 18.4245
Total in-flight flits = 69313 (0 measured)
latency change    = 0.416728
throughput change = 0.0137999
Class 0:
Packet latency average = 1278.39
	minimum = 23
	maximum = 2778
Network latency average = 1125.33
	minimum = 23
	maximum = 2654
Slowest packet = 1988
Flit latency average = 1092.22
	minimum = 6
	maximum = 2637
Slowest flit = 22427
Fragmentation average = 71.185
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0166771
	minimum = 0 (at node 8)
	maximum = 0.047 (at node 23)
Accepted packet rate average = 0.00965625
	minimum = 0.003 (at node 2)
	maximum = 0.016 (at node 23)
Injected flit rate average = 0.30049
	minimum = 0 (at node 8)
	maximum = 0.845 (at node 23)
Accepted flit rate average= 0.172656
	minimum = 0.049 (at node 176)
	maximum = 0.3 (at node 102)
Injected packet length average = 18.0181
Accepted packet length average = 17.8803
Total in-flight flits = 94411 (0 measured)
latency change    = 0.582132
throughput change = 0.0284917
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 553.322
	minimum = 23
	maximum = 2687
Network latency average = 219.842
	minimum = 23
	maximum = 980
Slowest packet = 10894
Flit latency average = 1613.39
	minimum = 6
	maximum = 3479
Slowest flit = 26549
Fragmentation average = 25.655
	minimum = 0
	maximum = 117
Injected packet rate average = 0.0135729
	minimum = 0 (at node 25)
	maximum = 0.041 (at node 177)
Accepted packet rate average = 0.00926042
	minimum = 0.004 (at node 36)
	maximum = 0.018 (at node 66)
Injected flit rate average = 0.244927
	minimum = 0 (at node 25)
	maximum = 0.738 (at node 177)
Accepted flit rate average= 0.1665
	minimum = 0.07 (at node 143)
	maximum = 0.313 (at node 66)
Injected packet length average = 18.0453
Accepted packet length average = 17.9798
Total in-flight flits = 109963 (44338 measured)
latency change    = 1.3104
throughput change = 0.0369745
Class 0:
Packet latency average = 1165.53
	minimum = 23
	maximum = 3460
Network latency average = 703.468
	minimum = 23
	maximum = 1939
Slowest packet = 10894
Flit latency average = 1862.85
	minimum = 6
	maximum = 4236
Slowest flit = 53354
Fragmentation average = 46.4069
	minimum = 0
	maximum = 129
Injected packet rate average = 0.0122266
	minimum = 0 (at node 25)
	maximum = 0.028 (at node 70)
Accepted packet rate average = 0.00926823
	minimum = 0.0045 (at node 86)
	maximum = 0.015 (at node 66)
Injected flit rate average = 0.220271
	minimum = 0 (at node 25)
	maximum = 0.504 (at node 70)
Accepted flit rate average= 0.16724
	minimum = 0.0795 (at node 146)
	maximum = 0.2755 (at node 159)
Injected packet length average = 18.0158
Accepted packet length average = 18.0444
Total in-flight flits = 115565 (74911 measured)
latency change    = 0.525262
throughput change = 0.0044223
Class 0:
Packet latency average = 1780.2
	minimum = 23
	maximum = 4619
Network latency average = 1196.81
	minimum = 23
	maximum = 2961
Slowest packet = 10894
Flit latency average = 2083.32
	minimum = 6
	maximum = 5353
Slowest flit = 43159
Fragmentation average = 56.5861
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0116024
	minimum = 0.00233333 (at node 115)
	maximum = 0.0296667 (at node 26)
Accepted packet rate average = 0.00930035
	minimum = 0.00533333 (at node 36)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.208562
	minimum = 0.042 (at node 115)
	maximum = 0.534 (at node 26)
Accepted flit rate average= 0.167655
	minimum = 0.0986667 (at node 36)
	maximum = 0.258667 (at node 78)
Injected packet length average = 17.9758
Accepted packet length average = 18.0267
Total in-flight flits = 118928 (96697 measured)
latency change    = 0.345282
throughput change = 0.00247491
Draining remaining packets ...
Class 0:
Remaining flits: 58374 58375 58376 58377 58378 58379 58380 58381 58382 58383 [...] (88897 flits)
Measured flits: 195768 195769 195770 195771 195772 195773 195774 195775 195776 195777 [...] (77331 flits)
Class 0:
Remaining flits: 87567 87568 87569 91440 91441 91442 91443 91444 91445 91446 [...] (58399 flits)
Measured flits: 195768 195769 195770 195771 195772 195773 195774 195775 195776 195777 [...] (53411 flits)
Class 0:
Remaining flits: 116910 116911 116912 116913 116914 116915 116916 116917 116918 116919 [...] (29959 flits)
Measured flits: 195768 195769 195770 195771 195772 195773 195774 195775 195776 195777 [...] (28682 flits)
Class 0:
Remaining flits: 160132 160133 160134 160135 160136 160137 160138 160139 160140 160141 [...] (6692 flits)
Measured flits: 196110 196111 196112 196113 196114 196115 196116 196117 196118 196119 [...] (6674 flits)
Time taken is 10973 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3987.83 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8625 (1 samples)
Network latency average = 3155.49 (1 samples)
	minimum = 23 (1 samples)
	maximum = 7233 (1 samples)
Flit latency average = 3041.72 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7711 (1 samples)
Fragmentation average = 64.6886 (1 samples)
	minimum = 0 (1 samples)
	maximum = 151 (1 samples)
Injected packet rate average = 0.0116024 (1 samples)
	minimum = 0.00233333 (1 samples)
	maximum = 0.0296667 (1 samples)
Accepted packet rate average = 0.00930035 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.014 (1 samples)
Injected flit rate average = 0.208562 (1 samples)
	minimum = 0.042 (1 samples)
	maximum = 0.534 (1 samples)
Accepted flit rate average = 0.167655 (1 samples)
	minimum = 0.0986667 (1 samples)
	maximum = 0.258667 (1 samples)
Injected packet size average = 17.9758 (1 samples)
Accepted packet size average = 18.0267 (1 samples)
Hops average = 5.11818 (1 samples)
Total run time 9.91858
