BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 347.182
	minimum = 23
	maximum = 971
Network latency average = 315
	minimum = 23
	maximum = 960
Slowest packet = 179
Flit latency average = 261.651
	minimum = 6
	maximum = 948
Slowest flit = 4909
Fragmentation average = 151.02
	minimum = 0
	maximum = 838
Injected packet rate average = 0.049125
	minimum = 0.038 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0120781
	minimum = 0.004 (at node 41)
	maximum = 0.022 (at node 70)
Injected flit rate average = 0.87576
	minimum = 0.681 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.246047
	minimum = 0.097 (at node 174)
	maximum = 0.415 (at node 78)
Injected packet length average = 17.8272
Accepted packet length average = 20.3713
Total in-flight flits = 122589 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 696.542
	minimum = 23
	maximum = 1907
Network latency average = 647.719
	minimum = 23
	maximum = 1861
Slowest packet = 740
Flit latency average = 581.222
	minimum = 6
	maximum = 1894
Slowest flit = 12813
Fragmentation average = 186.32
	minimum = 0
	maximum = 1505
Injected packet rate average = 0.0465104
	minimum = 0.0275 (at node 84)
	maximum = 0.0555 (at node 14)
Accepted packet rate average = 0.0125026
	minimum = 0.0075 (at node 10)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.833526
	minimum = 0.491 (at node 84)
	maximum = 0.999 (at node 147)
Accepted flit rate average= 0.238268
	minimum = 0.1445 (at node 116)
	maximum = 0.3465 (at node 152)
Injected packet length average = 17.9213
Accepted packet length average = 19.0575
Total in-flight flits = 229985 (0 measured)
latency change    = 0.501564
throughput change = 0.0326466
Class 0:
Packet latency average = 1825
	minimum = 33
	maximum = 2825
Network latency average = 1727.49
	minimum = 30
	maximum = 2804
Slowest packet = 1520
Flit latency average = 1685.24
	minimum = 6
	maximum = 2893
Slowest flit = 13705
Fragmentation average = 237.162
	minimum = 0
	maximum = 1715
Injected packet rate average = 0.0316042
	minimum = 0.007 (at node 104)
	maximum = 0.056 (at node 93)
Accepted packet rate average = 0.0119063
	minimum = 0.005 (at node 100)
	maximum = 0.021 (at node 63)
Injected flit rate average = 0.569328
	minimum = 0.113 (at node 104)
	maximum = 1 (at node 93)
Accepted flit rate average= 0.216427
	minimum = 0.103 (at node 100)
	maximum = 0.368 (at node 99)
Injected packet length average = 18.0143
Accepted packet length average = 18.1776
Total in-flight flits = 297745 (0 measured)
latency change    = 0.618334
throughput change = 0.100917
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1578.86
	minimum = 55
	maximum = 2777
Network latency average = 47.9439
	minimum = 31
	maximum = 214
Slowest packet = 23978
Flit latency average = 2413.4
	minimum = 6
	maximum = 3931
Slowest flit = 7498
Fragmentation average = 12.0187
	minimum = 3
	maximum = 45
Injected packet rate average = 0.0155885
	minimum = 0 (at node 97)
	maximum = 0.03 (at node 47)
Accepted packet rate average = 0.0119063
	minimum = 0.004 (at node 134)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.280818
	minimum = 0.005 (at node 97)
	maximum = 0.536 (at node 47)
Accepted flit rate average= 0.213708
	minimum = 0.083 (at node 86)
	maximum = 0.414 (at node 16)
Injected packet length average = 18.0144
Accepted packet length average = 17.9493
Total in-flight flits = 310641 (51986 measured)
latency change    = 0.155899
throughput change = 0.0127218
Class 0:
Packet latency average = 2025.5
	minimum = 55
	maximum = 3787
Network latency average = 186.004
	minimum = 29
	maximum = 1831
Slowest packet = 23978
Flit latency average = 2720.47
	minimum = 6
	maximum = 4784
Slowest flit = 30884
Fragmentation average = 30.2155
	minimum = 2
	maximum = 394
Injected packet rate average = 0.0152266
	minimum = 0.0055 (at node 69)
	maximum = 0.022 (at node 47)
Accepted packet rate average = 0.0117786
	minimum = 0.006 (at node 180)
	maximum = 0.0195 (at node 16)
Injected flit rate average = 0.274255
	minimum = 0.0985 (at node 69)
	maximum = 0.3955 (at node 47)
Accepted flit rate average= 0.210687
	minimum = 0.112 (at node 180)
	maximum = 0.347 (at node 16)
Injected packet length average = 18.0116
Accepted packet length average = 17.8872
Total in-flight flits = 322249 (101036 measured)
latency change    = 0.220509
throughput change = 0.014338
Class 0:
Packet latency average = 2809.04
	minimum = 55
	maximum = 4738
Network latency average = 775.543
	minimum = 29
	maximum = 2914
Slowest packet = 23978
Flit latency average = 3042.04
	minimum = 6
	maximum = 5685
Slowest flit = 49986
Fragmentation average = 76.5099
	minimum = 2
	maximum = 1052
Injected packet rate average = 0.0150243
	minimum = 0.00633333 (at node 24)
	maximum = 0.0203333 (at node 31)
Accepted packet rate average = 0.0116597
	minimum = 0.008 (at node 86)
	maximum = 0.0173333 (at node 16)
Injected flit rate average = 0.270637
	minimum = 0.112 (at node 24)
	maximum = 0.365333 (at node 181)
Accepted flit rate average= 0.208948
	minimum = 0.137667 (at node 86)
	maximum = 0.317333 (at node 16)
Injected packet length average = 18.0133
Accepted packet length average = 17.9205
Total in-flight flits = 333307 (147259 measured)
latency change    = 0.278935
throughput change = 0.00832544
Class 0:
Packet latency average = 3597.39
	minimum = 55
	maximum = 5648
Network latency average = 1437.92
	minimum = 27
	maximum = 3905
Slowest packet = 23978
Flit latency average = 3346.5
	minimum = 6
	maximum = 6794
Slowest flit = 26666
Fragmentation average = 102.405
	minimum = 2
	maximum = 1052
Injected packet rate average = 0.0149089
	minimum = 0.00725 (at node 0)
	maximum = 0.01925 (at node 66)
Accepted packet rate average = 0.0116172
	minimum = 0.0075 (at node 86)
	maximum = 0.0165 (at node 56)
Injected flit rate average = 0.268559
	minimum = 0.129 (at node 0)
	maximum = 0.34675 (at node 66)
Accepted flit rate average= 0.208275
	minimum = 0.13225 (at node 86)
	maximum = 0.29425 (at node 56)
Injected packet length average = 18.0134
Accepted packet length average = 17.9282
Total in-flight flits = 344376 (189734 measured)
latency change    = 0.219145
throughput change = 0.00323216
Class 0:
Packet latency average = 4264.93
	minimum = 55
	maximum = 6705
Network latency average = 1894.92
	minimum = 26
	maximum = 4896
Slowest packet = 23978
Flit latency average = 3647.4
	minimum = 6
	maximum = 7621
Slowest flit = 59032
Fragmentation average = 111.222
	minimum = 2
	maximum = 1445
Injected packet rate average = 0.0147969
	minimum = 0.0066 (at node 36)
	maximum = 0.0218 (at node 181)
Accepted packet rate average = 0.0116229
	minimum = 0.0078 (at node 4)
	maximum = 0.0152 (at node 56)
Injected flit rate average = 0.266431
	minimum = 0.1212 (at node 36)
	maximum = 0.3932 (at node 181)
Accepted flit rate average= 0.208308
	minimum = 0.1462 (at node 4)
	maximum = 0.2824 (at node 129)
Injected packet length average = 18.0059
Accepted packet length average = 17.9222
Total in-flight flits = 354143 (228686 measured)
latency change    = 0.156519
throughput change = 0.000161269
Class 0:
Packet latency average = 4833.59
	minimum = 55
	maximum = 7840
Network latency average = 2310.74
	minimum = 23
	maximum = 5936
Slowest packet = 23978
Flit latency average = 3943.4
	minimum = 6
	maximum = 8433
Slowest flit = 92452
Fragmentation average = 121.182
	minimum = 0
	maximum = 1445
Injected packet rate average = 0.0144245
	minimum = 0.0055 (at node 36)
	maximum = 0.0203333 (at node 181)
Accepted packet rate average = 0.0115755
	minimum = 0.0085 (at node 64)
	maximum = 0.0146667 (at node 56)
Injected flit rate average = 0.259914
	minimum = 0.101 (at node 36)
	maximum = 0.3655 (at node 181)
Accepted flit rate average= 0.20767
	minimum = 0.151167 (at node 64)
	maximum = 0.265833 (at node 56)
Injected packet length average = 18.019
Accepted packet length average = 17.9405
Total in-flight flits = 358659 (259425 measured)
latency change    = 0.117646
throughput change = 0.00307312
Class 0:
Packet latency average = 5409.21
	minimum = 55
	maximum = 8681
Network latency average = 2804.19
	minimum = 23
	maximum = 6991
Slowest packet = 23978
Flit latency average = 4225.27
	minimum = 6
	maximum = 9332
Slowest flit = 110635
Fragmentation average = 132.328
	minimum = 0
	maximum = 1990
Injected packet rate average = 0.0138921
	minimum = 0.00471429 (at node 36)
	maximum = 0.0192857 (at node 181)
Accepted packet rate average = 0.0114918
	minimum = 0.00857143 (at node 4)
	maximum = 0.0142857 (at node 44)
Injected flit rate average = 0.250364
	minimum = 0.0865714 (at node 36)
	maximum = 0.347714 (at node 181)
Accepted flit rate average= 0.205863
	minimum = 0.155143 (at node 190)
	maximum = 0.257143 (at node 56)
Injected packet length average = 18.022
Accepted packet length average = 17.9139
Total in-flight flits = 358295 (280978 measured)
latency change    = 0.106416
throughput change = 0.00877789
Draining all recorded packets ...
Class 0:
Remaining flits: 76194 76195 76196 76197 76198 76199 76200 76201 76202 76203 [...] (358595 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (299811 flits)
Class 0:
Remaining flits: 76194 76195 76196 76197 76198 76199 76200 76201 76202 76203 [...] (356936 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (313489 flits)
Class 0:
Remaining flits: 135504 135505 135506 135507 135508 135509 135510 135511 135512 135513 [...] (353404 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (322284 flits)
Class 0:
Remaining flits: 147978 147979 147980 147981 147982 147983 147984 147985 147986 147987 [...] (352338 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (330360 flits)
Class 0:
Remaining flits: 147978 147979 147980 147981 147982 147983 147984 147985 147986 147987 [...] (350213 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (334884 flits)
Class 0:
Remaining flits: 176490 176491 176492 176493 176494 176495 176496 176497 176498 176499 [...] (349557 flits)
Measured flits: 430830 430831 430832 430833 430834 430835 430836 430837 430838 430839 [...] (339229 flits)
Class 0:
Remaining flits: 223686 223687 223688 223689 223690 223691 223692 223693 223694 223695 [...] (343526 flits)
Measured flits: 430834 430835 430836 430837 430838 430839 430840 430841 430842 430843 [...] (336440 flits)
Class 0:
Remaining flits: 233478 233479 233480 233481 233482 233483 233484 233485 233486 233487 [...] (340909 flits)
Measured flits: 430848 430849 430850 430851 430852 430853 430854 430855 430856 430857 [...] (336293 flits)
Class 0:
Remaining flits: 240624 240625 240626 240627 240628 240629 240630 240631 240632 240633 [...] (340851 flits)
Measured flits: 430855 430856 430857 430858 430859 430860 430861 430862 430863 430864 [...] (337951 flits)
Class 0:
Remaining flits: 257131 257132 257133 257134 257135 257136 257137 257138 257139 257140 [...] (336619 flits)
Measured flits: 431532 431533 431534 431535 431536 431537 431538 431539 431540 431541 [...] (334702 flits)
Class 0:
Remaining flits: 289746 289747 289748 289749 289750 289751 289752 289753 289754 289755 [...] (335087 flits)
Measured flits: 431532 431533 431534 431535 431536 431537 431538 431539 431540 431541 [...] (333775 flits)
Class 0:
Remaining flits: 289746 289747 289748 289749 289750 289751 289752 289753 289754 289755 [...] (332913 flits)
Measured flits: 431532 431533 431534 431535 431536 431537 431538 431539 431540 431541 [...] (331987 flits)
Class 0:
Remaining flits: 289746 289747 289748 289749 289750 289751 289752 289753 289754 289755 [...] (330682 flits)
Measured flits: 431532 431533 431534 431535 431536 431537 431538 431539 431540 431541 [...] (330125 flits)
Class 0:
Remaining flits: 289746 289747 289748 289749 289750 289751 289752 289753 289754 289755 [...] (327610 flits)
Measured flits: 438318 438319 438320 438321 438322 438323 438324 438325 438326 438327 [...] (327281 flits)
Class 0:
Remaining flits: 289748 289749 289750 289751 289752 289753 289754 289755 289756 289757 [...] (331279 flits)
Measured flits: 438318 438319 438320 438321 438322 438323 438324 438325 438326 438327 [...] (331099 flits)
Class 0:
Remaining flits: 289761 289762 289763 351576 351577 351578 351579 351580 351581 351582 [...] (330375 flits)
Measured flits: 438318 438319 438320 438321 438322 438323 438324 438325 438326 438327 [...] (330257 flits)
Class 0:
Remaining flits: 351576 351577 351578 351579 351580 351581 351582 351583 351584 351585 [...] (330945 flits)
Measured flits: 438318 438319 438320 438321 438322 438323 438324 438325 438326 438327 [...] (330855 flits)
Class 0:
Remaining flits: 351585 351586 351587 351588 351589 351590 351591 351592 351593 425394 [...] (331407 flits)
Measured flits: 438318 438319 438320 438321 438322 438323 438324 438325 438326 438327 [...] (331344 flits)
Class 0:
Remaining flits: 425700 425701 425702 425703 425704 425705 425706 425707 425708 425709 [...] (329420 flits)
Measured flits: 459738 459739 459740 459741 459742 459743 459744 459745 459746 459747 [...] (329402 flits)
Class 0:
Remaining flits: 459738 459739 459740 459741 459742 459743 459744 459745 459746 459747 [...] (325934 flits)
Measured flits: 459738 459739 459740 459741 459742 459743 459744 459745 459746 459747 [...] (325934 flits)
Class 0:
Remaining flits: 459740 459741 459742 459743 459744 459745 459746 459747 459748 459749 [...] (324633 flits)
Measured flits: 459740 459741 459742 459743 459744 459745 459746 459747 459748 459749 [...] (324579 flits)
Class 0:
Remaining flits: 459751 459752 459753 459754 459755 575568 575569 575570 575571 575572 [...] (322023 flits)
Measured flits: 459751 459752 459753 459754 459755 575568 575569 575570 575571 575572 [...] (321447 flits)
Class 0:
Remaining flits: 575568 575569 575570 575571 575572 575573 575574 575575 575576 575577 [...] (317048 flits)
Measured flits: 575568 575569 575570 575571 575572 575573 575574 575575 575576 575577 [...] (314852 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (315125 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (311165 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (312121 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (304885 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (311596 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (298224 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (313834 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (292507 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (314071 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (285919 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (315285 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (275888 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (313236 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (263059 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (314210 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (249516 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (313145 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (233561 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (312443 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (219623 flits)
Class 0:
Remaining flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (312119 flits)
Measured flits: 582030 582031 582032 582033 582034 582035 582036 582037 582038 582039 [...] (205283 flits)
Class 0:
Remaining flits: 622584 622585 622586 622587 622588 622589 622590 622591 622592 622593 [...] (311336 flits)
Measured flits: 622584 622585 622586 622587 622588 622589 622590 622591 622592 622593 [...] (188237 flits)
Class 0:
Remaining flits: 622590 622591 622592 622593 622594 622595 622596 622597 622598 622599 [...] (309108 flits)
Measured flits: 622590 622591 622592 622593 622594 622595 622596 622597 622598 622599 [...] (171141 flits)
Class 0:
Remaining flits: 964494 964495 964496 964497 964498 964499 964500 964501 964502 964503 [...] (310593 flits)
Measured flits: 964494 964495 964496 964497 964498 964499 964500 964501 964502 964503 [...] (155916 flits)
Class 0:
Remaining flits: 964494 964495 964496 964497 964498 964499 964500 964501 964502 964503 [...] (308387 flits)
Measured flits: 964494 964495 964496 964497 964498 964499 964500 964501 964502 964503 [...] (136699 flits)
Class 0:
Remaining flits: 1004382 1004383 1004384 1004385 1004386 1004387 1004388 1004389 1004390 1004391 [...] (309680 flits)
Measured flits: 1004382 1004383 1004384 1004385 1004386 1004387 1004388 1004389 1004390 1004391 [...] (122317 flits)
Class 0:
Remaining flits: 1023966 1023967 1023968 1023969 1023970 1023971 1023972 1023973 1023974 1023975 [...] (312623 flits)
Measured flits: 1023966 1023967 1023968 1023969 1023970 1023971 1023972 1023973 1023974 1023975 [...] (108858 flits)
Class 0:
Remaining flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (311701 flits)
Measured flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (94402 flits)
Class 0:
Remaining flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (312106 flits)
Measured flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (80697 flits)
Class 0:
Remaining flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (312902 flits)
Measured flits: 1028430 1028431 1028432 1028433 1028434 1028435 1028436 1028437 1028438 1028439 [...] (68926 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (312720 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (58881 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (312381 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (50408 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (311329 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (43098 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (312376 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (36542 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (311004 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (30843 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (309580 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (26470 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (310070 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (23036 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (308075 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (20048 flits)
Class 0:
Remaining flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (309058 flits)
Measured flits: 1084428 1084429 1084430 1084431 1084432 1084433 1084434 1084435 1084436 1084437 [...] (17748 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (307664 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (14430 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308075 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (11779 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (307858 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (9647 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (310278 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (9000 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308558 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (7093 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (310416 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (5464 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308642 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (4585 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308219 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (3964 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (309702 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (3220 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (310348 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (2275 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308031 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (1599 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308026 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (1190 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (310432 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (878 flits)
Class 0:
Remaining flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (308006 flits)
Measured flits: 1342872 1342873 1342874 1342875 1342876 1342877 1342878 1342879 1342880 1342881 [...] (682 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (309939 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (503 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (311833 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (396 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (313499 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (302 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (313493 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (162 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (313352 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (119 flits)
Class 0:
Remaining flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (311138 flits)
Measured flits: 1877130 1877131 1877132 1877133 1877134 1877135 1877136 1877137 1877138 1877139 [...] (117 flits)
Class 0:
Remaining flits: 2109546 2109547 2109548 2109549 2109550 2109551 2109552 2109553 2109554 2109555 [...] (312113 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (81 flits)
Class 0:
Remaining flits: 2205934 2205935 2209735 2209736 2209737 2209738 2209739 2209740 2209741 2209742 [...] (310691 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (72 flits)
Class 0:
Remaining flits: 2276208 2276209 2276210 2276211 2276212 2276213 2276214 2276215 2276216 2276217 [...] (311942 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (49 flits)
Class 0:
Remaining flits: 2276208 2276209 2276210 2276211 2276212 2276213 2276214 2276215 2276216 2276217 [...] (309442 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (36 flits)
Class 0:
Remaining flits: 2301624 2301625 2301626 2301627 2301628 2301629 2301630 2301631 2301632 2301633 [...] (309437 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (36 flits)
Class 0:
Remaining flits: 2301624 2301625 2301626 2301627 2301628 2301629 2301630 2301631 2301632 2301633 [...] (312438 flits)
Measured flits: 2360250 2360251 2360252 2360253 2360254 2360255 2360256 2360257 2360258 2360259 [...] (18 flits)
Class 0:
Remaining flits: 2301624 2301625 2301626 2301627 2301628 2301629 2301630 2301631 2301632 2301633 [...] (312597 flits)
Measured flits: 2360262 2360263 2360264 2360265 2360266 2360267 (6 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2301624 2301625 2301626 2301627 2301628 2301629 2301630 2301631 2301632 2301633 [...] (277813 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2301624 2301625 2301626 2301627 2301628 2301629 2301630 2301631 2301632 2301633 [...] (243668 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2333574 2333575 2333576 2333577 2333578 2333579 2333580 2333581 2333582 2333583 [...] (209101 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2333574 2333575 2333576 2333577 2333578 2333579 2333580 2333581 2333582 2333583 [...] (174016 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2341134 2341135 2341136 2341137 2341138 2341139 2341140 2341141 2341142 2341143 [...] (140645 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2341134 2341135 2341136 2341137 2341138 2341139 2341140 2341141 2341142 2341143 [...] (111566 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2341134 2341135 2341136 2341137 2341138 2341139 2341140 2341141 2341142 2341143 [...] (84493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2341150 2341151 2622708 2622709 2622710 2622711 2622712 2622713 2622714 2622715 [...] (59857 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2769084 2769085 2769086 2769087 2769088 2769089 2769090 2769091 2769092 2769093 [...] (38225 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2769084 2769085 2769086 2769087 2769088 2769089 2769090 2769091 2769092 2769093 [...] (19947 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2856114 2856115 2856116 2856117 2856118 2856119 2856120 2856121 2856122 2856123 [...] (8846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2893381 2893382 2893383 2893384 2893385 2893386 2893387 2893388 2893389 2893390 [...] (2031 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 3182706 3182707 3182708 3182709 3182710 3182711 3182712 3182713 3182714 3182715 [...] (317 flits)
Measured flits: (0 flits)
Time taken is 102385 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 25718.1 (1 samples)
	minimum = 55 (1 samples)
	maximum = 79269 (1 samples)
Network latency average = 8936.44 (1 samples)
	minimum = 23 (1 samples)
	maximum = 50489 (1 samples)
Flit latency average = 8484.31 (1 samples)
	minimum = 6 (1 samples)
	maximum = 50465 (1 samples)
Fragmentation average = 202.537 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5396 (1 samples)
Injected packet rate average = 0.0138921 (1 samples)
	minimum = 0.00471429 (1 samples)
	maximum = 0.0192857 (1 samples)
Accepted packet rate average = 0.0114918 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0142857 (1 samples)
Injected flit rate average = 0.250364 (1 samples)
	minimum = 0.0865714 (1 samples)
	maximum = 0.347714 (1 samples)
Accepted flit rate average = 0.205863 (1 samples)
	minimum = 0.155143 (1 samples)
	maximum = 0.257143 (1 samples)
Injected packet size average = 18.022 (1 samples)
Accepted packet size average = 17.9139 (1 samples)
Hops average = 5.0364 (1 samples)
Total run time 137.274
