BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.055527
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 333.636
	minimum = 22
	maximum = 898
Network latency average = 303.545
	minimum = 22
	maximum = 820
Slowest packet = 389
Flit latency average = 280.268
	minimum = 5
	maximum = 803
Slowest flit = 23885
Fragmentation average = 25.1712
	minimum = 0
	maximum = 112
Injected packet rate average = 0.0500885
	minimum = 0.037 (at node 2)
	maximum = 0.056 (at node 13)
Accepted packet rate average = 0.0157604
	minimum = 0.007 (at node 108)
	maximum = 0.027 (at node 70)
Injected flit rate average = 0.892781
	minimum = 0.655 (at node 2)
	maximum = 0.998 (at node 147)
Accepted flit rate average= 0.290464
	minimum = 0.128 (at node 108)
	maximum = 0.503 (at node 70)
Injected packet length average = 17.8241
Accepted packet length average = 18.4299
Total in-flight flits = 117337 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 617.105
	minimum = 22
	maximum = 1719
Network latency average = 567.108
	minimum = 22
	maximum = 1603
Slowest packet = 2361
Flit latency average = 544.39
	minimum = 5
	maximum = 1621
Slowest flit = 59022
Fragmentation average = 25.0804
	minimum = 0
	maximum = 112
Injected packet rate average = 0.051625
	minimum = 0.04 (at node 188)
	maximum = 0.0555 (at node 13)
Accepted packet rate average = 0.0168333
	minimum = 0.0105 (at node 116)
	maximum = 0.0245 (at node 154)
Injected flit rate average = 0.925266
	minimum = 0.713 (at node 188)
	maximum = 0.9985 (at node 50)
Accepted flit rate average= 0.30669
	minimum = 0.1945 (at node 116)
	maximum = 0.441 (at node 154)
Injected packet length average = 17.9228
Accepted packet length average = 18.2192
Total in-flight flits = 239063 (0 measured)
latency change    = 0.459353
throughput change = 0.0529087
Class 0:
Packet latency average = 1469.35
	minimum = 22
	maximum = 2463
Network latency average = 1382.51
	minimum = 22
	maximum = 2377
Slowest packet = 3279
Flit latency average = 1369.29
	minimum = 5
	maximum = 2360
Slowest flit = 101735
Fragmentation average = 25.4482
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0525521
	minimum = 0.034 (at node 24)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0175104
	minimum = 0.008 (at node 40)
	maximum = 0.028 (at node 67)
Injected flit rate average = 0.945495
	minimum = 0.62 (at node 24)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.314771
	minimum = 0.133 (at node 190)
	maximum = 0.498 (at node 67)
Injected packet length average = 17.9916
Accepted packet length average = 17.9762
Total in-flight flits = 360247 (0 measured)
latency change    = 0.580016
throughput change = 0.0256718
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 400.583
	minimum = 25
	maximum = 1070
Network latency average = 174.753
	minimum = 22
	maximum = 721
Slowest packet = 29966
Flit latency average = 2068.73
	minimum = 5
	maximum = 3038
Slowest flit = 165333
Fragmentation average = 5.47233
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0484635
	minimum = 0.026 (at node 60)
	maximum = 0.056 (at node 5)
Accepted packet rate average = 0.0165469
	minimum = 0.005 (at node 48)
	maximum = 0.027 (at node 90)
Injected flit rate average = 0.872229
	minimum = 0.461 (at node 128)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.297802
	minimum = 0.084 (at node 48)
	maximum = 0.487 (at node 90)
Injected packet length average = 17.9976
Accepted packet length average = 17.9975
Total in-flight flits = 470559 (158224 measured)
latency change    = 2.66804
throughput change = 0.05698
Class 0:
Packet latency average = 576.944
	minimum = 25
	maximum = 1634
Network latency average = 293.112
	minimum = 22
	maximum = 1268
Slowest packet = 29966
Flit latency average = 2401.61
	minimum = 5
	maximum = 3889
Slowest flit = 191956
Fragmentation average = 5.16263
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0483333
	minimum = 0.027 (at node 104)
	maximum = 0.056 (at node 29)
Accepted packet rate average = 0.016401
	minimum = 0.01 (at node 36)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.870031
	minimum = 0.491 (at node 176)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.295234
	minimum = 0.1715 (at node 36)
	maximum = 0.444 (at node 90)
Injected packet length average = 18.0006
Accepted packet length average = 18.001
Total in-flight flits = 580957 (315351 measured)
latency change    = 0.305681
throughput change = 0.00869719
Class 0:
Packet latency average = 766.492
	minimum = 25
	maximum = 2252
Network latency average = 406.302
	minimum = 22
	maximum = 1872
Slowest packet = 29966
Flit latency average = 2732.97
	minimum = 5
	maximum = 4603
Slowest flit = 245216
Fragmentation average = 5.29485
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0482639
	minimum = 0.0276667 (at node 16)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0163403
	minimum = 0.0106667 (at node 36)
	maximum = 0.0236667 (at node 128)
Injected flit rate average = 0.868934
	minimum = 0.503 (at node 16)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.293913
	minimum = 0.192 (at node 36)
	maximum = 0.426667 (at node 128)
Injected packet length average = 18.0038
Accepted packet length average = 17.987
Total in-flight flits = 691371 (472747 measured)
latency change    = 0.247294
throughput change = 0.00449514
Class 0:
Packet latency average = 948.666
	minimum = 25
	maximum = 2589
Network latency average = 523.55
	minimum = 22
	maximum = 2207
Slowest packet = 29966
Flit latency average = 3047.36
	minimum = 5
	maximum = 5336
Slowest flit = 291364
Fragmentation average = 5.37422
	minimum = 0
	maximum = 34
Injected packet rate average = 0.0482956
	minimum = 0.028 (at node 16)
	maximum = 0.05575 (at node 29)
Accepted packet rate average = 0.0163594
	minimum = 0.01225 (at node 162)
	maximum = 0.0215 (at node 128)
Injected flit rate average = 0.86951
	minimum = 0.50575 (at node 60)
	maximum = 1 (at node 2)
Accepted flit rate average= 0.294337
	minimum = 0.219 (at node 162)
	maximum = 0.3865 (at node 128)
Injected packet length average = 18.0039
Accepted packet length average = 17.992
Total in-flight flits = 801834 (630006 measured)
latency change    = 0.192031
throughput change = 0.00144068
Draining remaining packets ...
Class 0:
Remaining flits: 331380 331381 331382 331383 331384 331385 331386 331387 331388 331389 [...] (748884 flits)
Measured flits: 538452 538453 538454 538455 538456 538457 538458 538459 538460 538461 [...] (624609 flits)
Class 0:
Remaining flits: 378198 378199 378200 378201 378202 378203 378204 378205 378206 378207 [...] (696758 flits)
Measured flits: 538452 538453 538454 538455 538456 538457 538458 538459 538460 538461 [...] (619742 flits)
Class 0:
Remaining flits: 414792 414793 414794 414795 414796 414797 414798 414799 414800 414801 [...] (645494 flits)
Measured flits: 538452 538453 538454 538455 538456 538457 538458 538459 538460 538461 [...] (613607 flits)
Class 0:
Remaining flits: 450918 450919 450920 450921 450922 450923 450924 450925 450926 450927 [...] (596415 flits)
Measured flits: 538470 538471 538472 538473 538474 538475 538476 538477 538478 538479 [...] (591603 flits)
Class 0:
Remaining flits: 502938 502939 502940 502941 502942 502943 502944 502945 502946 502947 [...] (548148 flits)
Measured flits: 538506 538507 538508 538509 538510 538511 538512 538513 538514 538515 [...] (547981 flits)
Class 0:
Remaining flits: 560196 560197 560198 560199 560200 560201 560202 560203 560204 560205 [...] (500454 flits)
Measured flits: 560196 560197 560198 560199 560200 560201 560202 560203 560204 560205 [...] (500454 flits)
Class 0:
Remaining flits: 599724 599725 599726 599727 599728 599729 599730 599731 599732 599733 [...] (452957 flits)
Measured flits: 599724 599725 599726 599727 599728 599729 599730 599731 599732 599733 [...] (452957 flits)
Class 0:
Remaining flits: 638712 638713 638714 638715 638716 638717 638718 638719 638720 638721 [...] (405926 flits)
Measured flits: 638712 638713 638714 638715 638716 638717 638718 638719 638720 638721 [...] (405926 flits)
Class 0:
Remaining flits: 703764 703765 703766 703767 703768 703769 703770 703771 703772 703773 [...] (358625 flits)
Measured flits: 703764 703765 703766 703767 703768 703769 703770 703771 703772 703773 [...] (358625 flits)
Class 0:
Remaining flits: 730422 730423 730424 730425 730426 730427 730428 730429 730430 730431 [...] (311268 flits)
Measured flits: 730422 730423 730424 730425 730426 730427 730428 730429 730430 730431 [...] (311268 flits)
Class 0:
Remaining flits: 773639 776268 776269 776270 776271 776272 776273 776274 776275 776276 [...] (264034 flits)
Measured flits: 773639 776268 776269 776270 776271 776272 776273 776274 776275 776276 [...] (264034 flits)
Class 0:
Remaining flits: 818802 818803 818804 818805 818806 818807 818808 818809 818810 818811 [...] (217188 flits)
Measured flits: 818802 818803 818804 818805 818806 818807 818808 818809 818810 818811 [...] (217188 flits)
Class 0:
Remaining flits: 853243 853244 853245 853246 853247 853248 853249 853250 853251 853252 [...] (170027 flits)
Measured flits: 853243 853244 853245 853246 853247 853248 853249 853250 853251 853252 [...] (170027 flits)
Class 0:
Remaining flits: 898344 898345 898346 898347 898348 898349 898350 898351 898352 898353 [...] (122900 flits)
Measured flits: 898344 898345 898346 898347 898348 898349 898350 898351 898352 898353 [...] (122900 flits)
Class 0:
Remaining flits: 953172 953173 953174 953175 953176 953177 953178 953179 953180 953181 [...] (75714 flits)
Measured flits: 953172 953173 953174 953175 953176 953177 953178 953179 953180 953181 [...] (75714 flits)
Class 0:
Remaining flits: 989780 989781 989782 989783 998766 998767 998768 998769 998770 998771 [...] (28840 flits)
Measured flits: 989780 989781 989782 989783 998766 998767 998768 998769 998770 998771 [...] (28840 flits)
Class 0:
Remaining flits: 1061729 1085544 1085545 1085546 1085547 1085548 1085549 1085550 1085551 1085552 [...] (2302 flits)
Measured flits: 1061729 1085544 1085545 1085546 1085547 1085548 1085549 1085550 1085551 1085552 [...] (2302 flits)
Time taken is 24508 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11601 (1 samples)
	minimum = 25 (1 samples)
	maximum = 18531 (1 samples)
Network latency average = 11232.2 (1 samples)
	minimum = 22 (1 samples)
	maximum = 18050 (1 samples)
Flit latency average = 8993.27 (1 samples)
	minimum = 5 (1 samples)
	maximum = 18033 (1 samples)
Fragmentation average = 26.7002 (1 samples)
	minimum = 0 (1 samples)
	maximum = 762 (1 samples)
Injected packet rate average = 0.0482956 (1 samples)
	minimum = 0.028 (1 samples)
	maximum = 0.05575 (1 samples)
Accepted packet rate average = 0.0163594 (1 samples)
	minimum = 0.01225 (1 samples)
	maximum = 0.0215 (1 samples)
Injected flit rate average = 0.86951 (1 samples)
	minimum = 0.50575 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.294337 (1 samples)
	minimum = 0.219 (1 samples)
	maximum = 0.3865 (1 samples)
Injected packet size average = 18.0039 (1 samples)
Accepted packet size average = 17.992 (1 samples)
Hops average = 5.16349 (1 samples)
Total run time 24.3239
