BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 318.268
	minimum = 24
	maximum = 860
Network latency average = 271.92
	minimum = 23
	maximum = 845
Slowest packet = 176
Flit latency average = 229.867
	minimum = 6
	maximum = 848
Slowest flit = 11175
Fragmentation average = 51.6946
	minimum = 0
	maximum = 121
Injected packet rate average = 0.0139115
	minimum = 0.007 (at node 41)
	maximum = 0.025 (at node 187)
Accepted packet rate average = 0.00900521
	minimum = 0.003 (at node 41)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.246891
	minimum = 0.111 (at node 160)
	maximum = 0.437 (at node 187)
Accepted flit rate average= 0.168057
	minimum = 0.054 (at node 178)
	maximum = 0.306 (at node 91)
Injected packet length average = 17.7473
Accepted packet length average = 18.6622
Total in-flight flits = 17791 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 633.375
	minimum = 24
	maximum = 1689
Network latency average = 391.547
	minimum = 23
	maximum = 1689
Slowest packet = 893
Flit latency average = 339.57
	minimum = 6
	maximum = 1752
Slowest flit = 19072
Fragmentation average = 55.1513
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0114141
	minimum = 0.0055 (at node 41)
	maximum = 0.02 (at node 123)
Accepted packet rate average = 0.00894792
	minimum = 0.0045 (at node 135)
	maximum = 0.0145 (at node 22)
Injected flit rate average = 0.203901
	minimum = 0.099 (at node 41)
	maximum = 0.3525 (at node 123)
Accepted flit rate average= 0.164326
	minimum = 0.081 (at node 135)
	maximum = 0.266 (at node 22)
Injected packet length average = 17.864
Accepted packet length average = 18.3647
Total in-flight flits = 17809 (0 measured)
latency change    = 0.497505
throughput change = 0.0227096
Class 0:
Packet latency average = 1626.71
	minimum = 626
	maximum = 2549
Network latency average = 558.543
	minimum = 23
	maximum = 2455
Slowest packet = 1360
Flit latency average = 491.469
	minimum = 6
	maximum = 2405
Slowest flit = 34054
Fragmentation average = 60.2732
	minimum = 0
	maximum = 135
Injected packet rate average = 0.00926562
	minimum = 0 (at node 20)
	maximum = 0.022 (at node 87)
Accepted packet rate average = 0.0091875
	minimum = 0.002 (at node 153)
	maximum = 0.017 (at node 6)
Injected flit rate average = 0.165911
	minimum = 0 (at node 20)
	maximum = 0.386 (at node 87)
Accepted flit rate average= 0.164521
	minimum = 0.044 (at node 153)
	maximum = 0.318 (at node 6)
Injected packet length average = 17.9061
Accepted packet length average = 17.907
Total in-flight flits = 18117 (0 measured)
latency change    = 0.61064
throughput change = 0.00118716
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2343.09
	minimum = 1085
	maximum = 3341
Network latency average = 333.456
	minimum = 23
	maximum = 936
Slowest packet = 6274
Flit latency average = 478.404
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 52.4582
	minimum = 0
	maximum = 127
Injected packet rate average = 0.00903646
	minimum = 0 (at node 64)
	maximum = 0.02 (at node 178)
Accepted packet rate average = 0.00903125
	minimum = 0.002 (at node 184)
	maximum = 0.018 (at node 56)
Injected flit rate average = 0.162292
	minimum = 0 (at node 64)
	maximum = 0.36 (at node 178)
Accepted flit rate average= 0.162615
	minimum = 0.036 (at node 184)
	maximum = 0.317 (at node 56)
Injected packet length average = 17.9597
Accepted packet length average = 18.0058
Total in-flight flits = 18035 (16609 measured)
latency change    = 0.305743
throughput change = 0.0117225
Class 0:
Packet latency average = 2757.32
	minimum = 1085
	maximum = 4002
Network latency average = 461.393
	minimum = 23
	maximum = 1777
Slowest packet = 6274
Flit latency average = 488.965
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 55.7264
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00898958
	minimum = 0.001 (at node 64)
	maximum = 0.02 (at node 160)
Accepted packet rate average = 0.00897917
	minimum = 0.0045 (at node 91)
	maximum = 0.0155 (at node 59)
Injected flit rate average = 0.161651
	minimum = 0.018 (at node 64)
	maximum = 0.3575 (at node 160)
Accepted flit rate average= 0.162107
	minimum = 0.0805 (at node 91)
	maximum = 0.2755 (at node 59)
Injected packet length average = 17.982
Accepted packet length average = 18.0537
Total in-flight flits = 17914 (17891 measured)
latency change    = 0.150229
throughput change = 0.00313258
Class 0:
Packet latency average = 3103.75
	minimum = 1085
	maximum = 4780
Network latency average = 494.884
	minimum = 23
	maximum = 2102
Slowest packet = 6274
Flit latency average = 486.005
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 56.6291
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00893403
	minimum = 0.00333333 (at node 64)
	maximum = 0.0173333 (at node 160)
Accepted packet rate average = 0.0089809
	minimum = 0.005 (at node 71)
	maximum = 0.0146667 (at node 16)
Injected flit rate average = 0.160825
	minimum = 0.06 (at node 64)
	maximum = 0.312 (at node 160)
Accepted flit rate average= 0.161646
	minimum = 0.0953333 (at node 71)
	maximum = 0.263333 (at node 16)
Injected packet length average = 18.0014
Accepted packet length average = 17.9988
Total in-flight flits = 17763 (17763 measured)
latency change    = 0.111615
throughput change = 0.00285153
Class 0:
Packet latency average = 3437.87
	minimum = 1085
	maximum = 5595
Network latency average = 511.2
	minimum = 23
	maximum = 2244
Slowest packet = 6274
Flit latency average = 486.606
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 57.2785
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00889063
	minimum = 0.0035 (at node 2)
	maximum = 0.01675 (at node 8)
Accepted packet rate average = 0.0089401
	minimum = 0.00475 (at node 4)
	maximum = 0.01325 (at node 16)
Injected flit rate average = 0.16028
	minimum = 0.063 (at node 2)
	maximum = 0.298 (at node 8)
Accepted flit rate average= 0.160934
	minimum = 0.0855 (at node 4)
	maximum = 0.2415 (at node 129)
Injected packet length average = 18.028
Accepted packet length average = 18.0013
Total in-flight flits = 17514 (17514 measured)
latency change    = 0.0971876
throughput change = 0.00442567
Class 0:
Packet latency average = 3775.93
	minimum = 1085
	maximum = 6436
Network latency average = 519.307
	minimum = 23
	maximum = 2647
Slowest packet = 6274
Flit latency average = 486.299
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 57.7373
	minimum = 0
	maximum = 148
Injected packet rate average = 0.00889479
	minimum = 0.0042 (at node 2)
	maximum = 0.0152 (at node 160)
Accepted packet rate average = 0.00891562
	minimum = 0.0056 (at node 4)
	maximum = 0.0136 (at node 129)
Injected flit rate average = 0.160189
	minimum = 0.073 (at node 132)
	maximum = 0.273 (at node 160)
Accepted flit rate average= 0.160607
	minimum = 0.1034 (at node 4)
	maximum = 0.2444 (at node 129)
Injected packet length average = 18.0093
Accepted packet length average = 18.0141
Total in-flight flits = 17744 (17744 measured)
latency change    = 0.0895314
throughput change = 0.00203168
Class 0:
Packet latency average = 4116.8
	minimum = 1085
	maximum = 7119
Network latency average = 529.276
	minimum = 23
	maximum = 2756
Slowest packet = 6274
Flit latency average = 489.754
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 57.8571
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0089184
	minimum = 0.004 (at node 2)
	maximum = 0.0145 (at node 8)
Accepted packet rate average = 0.00891406
	minimum = 0.006 (at node 36)
	maximum = 0.0121667 (at node 128)
Injected flit rate average = 0.160624
	minimum = 0.0701667 (at node 2)
	maximum = 0.259833 (at node 8)
Accepted flit rate average= 0.160684
	minimum = 0.108 (at node 36)
	maximum = 0.220833 (at node 128)
Injected packet length average = 18.0104
Accepted packet length average = 18.0259
Total in-flight flits = 17923 (17923 measured)
latency change    = 0.0827987
throughput change = 0.000477559
Class 0:
Packet latency average = 4447.21
	minimum = 1085
	maximum = 7973
Network latency average = 532.128
	minimum = 23
	maximum = 3054
Slowest packet = 6274
Flit latency average = 489.182
	minimum = 6
	maximum = 3039
Slowest flit = 26279
Fragmentation average = 58.0631
	minimum = 0
	maximum = 148
Injected packet rate average = 0.0088936
	minimum = 0.00342857 (at node 2)
	maximum = 0.0145714 (at node 8)
Accepted packet rate average = 0.00890997
	minimum = 0.00571429 (at node 36)
	maximum = 0.0122857 (at node 128)
Injected flit rate average = 0.160071
	minimum = 0.0617143 (at node 2)
	maximum = 0.262286 (at node 8)
Accepted flit rate average= 0.160474
	minimum = 0.103857 (at node 36)
	maximum = 0.221143 (at node 128)
Injected packet length average = 17.9985
Accepted packet length average = 18.0106
Total in-flight flits = 17720 (17720 measured)
latency change    = 0.0742967
throughput change = 0.00130906
Draining all recorded packets ...
Class 0:
Remaining flits: 273137 273138 273139 273140 273141 273142 273143 273144 273145 273146 [...] (17820 flits)
Measured flits: 273137 273138 273139 273140 273141 273142 273143 273144 273145 273146 [...] (17820 flits)
Class 0:
Remaining flits: 301577 301578 301579 301580 301581 301582 301583 301584 301585 301586 [...] (17635 flits)
Measured flits: 301577 301578 301579 301580 301581 301582 301583 301584 301585 301586 [...] (17635 flits)
Class 0:
Remaining flits: 348372 348373 348374 348375 348376 348377 348378 348379 348380 348381 [...] (17866 flits)
Measured flits: 348372 348373 348374 348375 348376 348377 348378 348379 348380 348381 [...] (17866 flits)
Class 0:
Remaining flits: 378027 378028 378029 378030 378031 378032 378033 378034 378035 381690 [...] (17694 flits)
Measured flits: 378027 378028 378029 378030 378031 378032 378033 378034 378035 381690 [...] (17694 flits)
Class 0:
Remaining flits: 403254 403255 403256 403257 403258 403259 403260 403261 403262 403263 [...] (17671 flits)
Measured flits: 403254 403255 403256 403257 403258 403259 403260 403261 403262 403263 [...] (17671 flits)
Class 0:
Remaining flits: 404280 404281 404282 404283 404284 404285 404286 404287 404288 404289 [...] (17490 flits)
Measured flits: 404280 404281 404282 404283 404284 404285 404286 404287 404288 404289 [...] (17490 flits)
Class 0:
Remaining flits: 455238 455239 455240 455241 455242 455243 455244 455245 455246 455247 [...] (17882 flits)
Measured flits: 455238 455239 455240 455241 455242 455243 455244 455245 455246 455247 [...] (17882 flits)
Class 0:
Remaining flits: 507456 507457 507458 507459 507460 507461 507462 507463 507464 507465 [...] (17712 flits)
Measured flits: 507456 507457 507458 507459 507460 507461 507462 507463 507464 507465 [...] (17712 flits)
Class 0:
Remaining flits: 535536 535537 535538 535539 535540 535541 535542 535543 535544 535545 [...] (17466 flits)
Measured flits: 535536 535537 535538 535539 535540 535541 535542 535543 535544 535545 [...] (17466 flits)
Class 0:
Remaining flits: 567126 567127 567128 567129 567130 567131 567132 567133 567134 567135 [...] (17808 flits)
Measured flits: 567126 567127 567128 567129 567130 567131 567132 567133 567134 567135 [...] (17808 flits)
Class 0:
Remaining flits: 595674 595675 595676 595677 595678 595679 595680 595681 595682 595683 [...] (17549 flits)
Measured flits: 595674 595675 595676 595677 595678 595679 595680 595681 595682 595683 [...] (17549 flits)
Class 0:
Remaining flits: 633091 633092 633093 633094 633095 636696 636697 636698 636699 636700 [...] (17954 flits)
Measured flits: 633091 633092 633093 633094 633095 636696 636697 636698 636699 636700 [...] (17936 flits)
Class 0:
Remaining flits: 647064 647065 647066 647067 647068 647069 647070 647071 647072 647073 [...] (17863 flits)
Measured flits: 647064 647065 647066 647067 647068 647069 647070 647071 647072 647073 [...] (17646 flits)
Class 0:
Remaining flits: 671940 671941 671942 671943 671944 671945 671946 671947 671948 671949 [...] (17725 flits)
Measured flits: 671940 671941 671942 671943 671944 671945 671946 671947 671948 671949 [...] (17329 flits)
Class 0:
Remaining flits: 737802 737803 737804 737805 737806 737807 737808 737809 737810 737811 [...] (17865 flits)
Measured flits: 737802 737803 737804 737805 737806 737807 737808 737809 737810 737811 [...] (16887 flits)
Class 0:
Remaining flits: 743166 743167 743168 743169 743170 743171 743172 743173 743174 743175 [...] (17649 flits)
Measured flits: 743166 743167 743168 743169 743170 743171 743172 743173 743174 743175 [...] (15784 flits)
Class 0:
Remaining flits: 769266 769267 769268 769269 769270 769271 769272 769273 769274 769275 [...] (17942 flits)
Measured flits: 769266 769267 769268 769269 769270 769271 769272 769273 769274 769275 [...] (14205 flits)
Class 0:
Remaining flits: 805284 805285 805286 805287 805288 805289 805290 805291 805292 805293 [...] (17804 flits)
Measured flits: 805284 805285 805286 805287 805288 805289 805290 805291 805292 805293 [...] (12136 flits)
Class 0:
Remaining flits: 816069 816070 816071 816072 816073 816074 816075 816076 816077 816078 [...] (17676 flits)
Measured flits: 816069 816070 816071 816072 816073 816074 816075 816076 816077 816078 [...] (10155 flits)
Class 0:
Remaining flits: 866618 866619 866620 866621 866622 866623 866624 866625 866626 866627 [...] (17937 flits)
Measured flits: 866618 866619 866620 866621 866622 866623 866624 866625 866626 866627 [...] (8256 flits)
Class 0:
Remaining flits: 881730 881731 881732 881733 881734 881735 881736 881737 881738 881739 [...] (17573 flits)
Measured flits: 894433 894434 894435 894436 894437 895230 895231 895232 895233 895234 [...] (6334 flits)
Class 0:
Remaining flits: 894996 894997 894998 894999 895000 895001 895002 895003 895004 895005 [...] (17988 flits)
Measured flits: 943913 943914 943915 943916 943917 943918 943919 944622 944623 944624 [...] (4868 flits)
Class 0:
Remaining flits: 954720 954721 954722 954723 954724 954725 954726 954727 954728 954729 [...] (17937 flits)
Measured flits: 954720 954721 954722 954723 954724 954725 954726 954727 954728 954729 [...] (3503 flits)
Class 0:
Remaining flits: 996444 996445 996446 996447 996448 996449 996450 996451 996452 996453 [...] (17556 flits)
Measured flits: 1023174 1023175 1023176 1023177 1023178 1023179 1023180 1023181 1023182 1023183 [...] (2156 flits)
Class 0:
Remaining flits: 1033002 1033003 1033004 1033005 1033006 1033007 1033008 1033009 1033010 1033011 [...] (18069 flits)
Measured flits: 1036674 1036675 1036676 1036677 1036678 1036679 1036680 1036681 1036682 1036683 [...] (1367 flits)
Class 0:
Remaining flits: 1062178 1062179 1062198 1062199 1062200 1062201 1062202 1062203 1062204 1062205 [...] (17830 flits)
Measured flits: 1086228 1086229 1086230 1086231 1086232 1086233 1086234 1086235 1086236 1086237 [...] (815 flits)
Class 0:
Remaining flits: 1102284 1102285 1102286 1102287 1102288 1102289 1102290 1102291 1102292 1102293 [...] (17982 flits)
Measured flits: 1131858 1131859 1131860 1131861 1131862 1131863 1131864 1131865 1131866 1131867 [...] (162 flits)
Draining remaining packets ...
Time taken is 38570 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11350.7 (1 samples)
	minimum = 1085 (1 samples)
	maximum = 27688 (1 samples)
Network latency average = 554.964 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3581 (1 samples)
Flit latency average = 491.746 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3818 (1 samples)
Fragmentation average = 58.4529 (1 samples)
	minimum = 0 (1 samples)
	maximum = 158 (1 samples)
Injected packet rate average = 0.0088936 (1 samples)
	minimum = 0.00342857 (1 samples)
	maximum = 0.0145714 (1 samples)
Accepted packet rate average = 0.00890997 (1 samples)
	minimum = 0.00571429 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.160071 (1 samples)
	minimum = 0.0617143 (1 samples)
	maximum = 0.262286 (1 samples)
Accepted flit rate average = 0.160474 (1 samples)
	minimum = 0.103857 (1 samples)
	maximum = 0.221143 (1 samples)
Injected packet size average = 17.9985 (1 samples)
Accepted packet size average = 18.0106 (1 samples)
Hops average = 5.06813 (1 samples)
Total run time 25.9502
