BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.009117
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 62.7925
	minimum = 22
	maximum = 264
Network latency average = 60.8336
	minimum = 22
	maximum = 243
Slowest packet = 1210
Flit latency average = 38.0816
	minimum = 5
	maximum = 226
Slowest flit = 21797
Fragmentation average = 10.4518
	minimum = 0
	maximum = 65
Injected packet rate average = 0.00905208
	minimum = 0.003 (at node 60)
	maximum = 0.019 (at node 123)
Accepted packet rate average = 0.00848438
	minimum = 0.001 (at node 41)
	maximum = 0.016 (at node 48)
Injected flit rate average = 0.161443
	minimum = 0.04 (at node 120)
	maximum = 0.342 (at node 123)
Accepted flit rate average= 0.155604
	minimum = 0.018 (at node 41)
	maximum = 0.288 (at node 48)
Injected packet length average = 17.8349
Accepted packet length average = 18.3401
Total in-flight flits = 1408 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 62.4788
	minimum = 22
	maximum = 264
Network latency average = 60.6255
	minimum = 22
	maximum = 243
Slowest packet = 1210
Flit latency average = 37.8491
	minimum = 5
	maximum = 226
Slowest flit = 21797
Fragmentation average = 10.4761
	minimum = 0
	maximum = 65
Injected packet rate average = 0.00891927
	minimum = 0.004 (at node 2)
	maximum = 0.0155 (at node 162)
Accepted packet rate average = 0.00871354
	minimum = 0.005 (at node 30)
	maximum = 0.0135 (at node 22)
Injected flit rate average = 0.15981
	minimum = 0.072 (at node 2)
	maximum = 0.279 (at node 162)
Accepted flit rate average= 0.157719
	minimum = 0.09 (at node 30)
	maximum = 0.243 (at node 22)
Injected packet length average = 17.9174
Accepted packet length average = 18.1004
Total in-flight flits = 1086 (0 measured)
latency change    = 0.00502139
throughput change = 0.0134073
Class 0:
Packet latency average = 61.3168
	minimum = 22
	maximum = 212
Network latency average = 59.6271
	minimum = 22
	maximum = 212
Slowest packet = 4423
Flit latency average = 36.8689
	minimum = 5
	maximum = 195
Slowest flit = 79631
Fragmentation average = 10.4185
	minimum = 0
	maximum = 71
Injected packet rate average = 0.009
	minimum = 0.002 (at node 120)
	maximum = 0.017 (at node 19)
Accepted packet rate average = 0.00891146
	minimum = 0.001 (at node 153)
	maximum = 0.017 (at node 179)
Injected flit rate average = 0.162047
	minimum = 0.036 (at node 120)
	maximum = 0.306 (at node 19)
Accepted flit rate average= 0.160568
	minimum = 0.018 (at node 153)
	maximum = 0.306 (at node 179)
Injected packet length average = 18.0052
Accepted packet length average = 18.0181
Total in-flight flits = 1361 (0 measured)
latency change    = 0.0189509
throughput change = 0.017743
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 63.5916
	minimum = 22
	maximum = 217
Network latency average = 62.0824
	minimum = 22
	maximum = 197
Slowest packet = 5768
Flit latency average = 39.4576
	minimum = 5
	maximum = 180
Slowest flit = 112301
Fragmentation average = 10.2786
	minimum = 0
	maximum = 60
Injected packet rate average = 0.00942187
	minimum = 0.002 (at node 11)
	maximum = 0.019 (at node 172)
Accepted packet rate average = 0.00928646
	minimum = 0.001 (at node 184)
	maximum = 0.019 (at node 16)
Injected flit rate average = 0.169151
	minimum = 0.036 (at node 11)
	maximum = 0.334 (at node 172)
Accepted flit rate average= 0.167552
	minimum = 0.018 (at node 184)
	maximum = 0.342 (at node 16)
Injected packet length average = 17.953
Accepted packet length average = 18.0426
Total in-flight flits = 1753 (1753 measured)
latency change    = 0.0357722
throughput change = 0.0416848
Class 0:
Packet latency average = 63.7241
	minimum = 22
	maximum = 257
Network latency average = 62.1363
	minimum = 22
	maximum = 257
Slowest packet = 7782
Flit latency average = 39.4694
	minimum = 5
	maximum = 240
Slowest flit = 140093
Fragmentation average = 10.4593
	minimum = 0
	maximum = 60
Injected packet rate average = 0.00911458
	minimum = 0.004 (at node 54)
	maximum = 0.0155 (at node 65)
Accepted packet rate average = 0.00915104
	minimum = 0.0045 (at node 163)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.16399
	minimum = 0.072 (at node 54)
	maximum = 0.28 (at node 65)
Accepted flit rate average= 0.164482
	minimum = 0.081 (at node 190)
	maximum = 0.333 (at node 16)
Injected packet length average = 17.992
Accepted packet length average = 17.9741
Total in-flight flits = 1200 (1200 measured)
latency change    = 0.00207967
throughput change = 0.0186666
Class 0:
Packet latency average = 62.5105
	minimum = 22
	maximum = 257
Network latency average = 60.9894
	minimum = 22
	maximum = 257
Slowest packet = 7782
Flit latency average = 38.3841
	minimum = 5
	maximum = 240
Slowest flit = 140093
Fragmentation average = 10.359
	minimum = 0
	maximum = 60
Injected packet rate average = 0.00910417
	minimum = 0.005 (at node 11)
	maximum = 0.015 (at node 65)
Accepted packet rate average = 0.00905035
	minimum = 0.00533333 (at node 190)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.163997
	minimum = 0.0876667 (at node 11)
	maximum = 0.272 (at node 65)
Accepted flit rate average= 0.163177
	minimum = 0.102 (at node 134)
	maximum = 0.294 (at node 16)
Injected packet length average = 18.0133
Accepted packet length average = 18.0299
Total in-flight flits = 1763 (1763 measured)
latency change    = 0.0194152
throughput change = 0.00799553
Draining all recorded packets ...
Draining remaining packets ...
Time taken is 6347 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 63.2292 (1 samples)
	minimum = 22 (1 samples)
	maximum = 257 (1 samples)
Network latency average = 61.6749 (1 samples)
	minimum = 22 (1 samples)
	maximum = 257 (1 samples)
Flit latency average = 39.2821 (1 samples)
	minimum = 5 (1 samples)
	maximum = 289 (1 samples)
Fragmentation average = 10.4802 (1 samples)
	minimum = 0 (1 samples)
	maximum = 68 (1 samples)
Injected packet rate average = 0.00910417 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.015 (1 samples)
Accepted packet rate average = 0.00905035 (1 samples)
	minimum = 0.00533333 (1 samples)
	maximum = 0.0163333 (1 samples)
Injected flit rate average = 0.163997 (1 samples)
	minimum = 0.0876667 (1 samples)
	maximum = 0.272 (1 samples)
Accepted flit rate average = 0.163177 (1 samples)
	minimum = 0.102 (1 samples)
	maximum = 0.294 (1 samples)
Injected packet size average = 18.0133 (1 samples)
Accepted packet size average = 18.0299 (1 samples)
Hops average = 5.0646 (1 samples)
Total run time 2.33282
