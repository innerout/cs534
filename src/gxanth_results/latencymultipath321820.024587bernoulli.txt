BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 241.266
	minimum = 22
	maximum = 746
Network latency average = 233.363
	minimum = 22
	maximum = 736
Slowest packet = 744
Flit latency average = 199.4
	minimum = 5
	maximum = 757
Slowest flit = 18618
Fragmentation average = 47.304
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0240521
	minimum = 0.012 (at node 54)
	maximum = 0.036 (at node 149)
Accepted packet rate average = 0.0138281
	minimum = 0.006 (at node 174)
	maximum = 0.025 (at node 44)
Injected flit rate average = 0.42876
	minimum = 0.216 (at node 54)
	maximum = 0.648 (at node 149)
Accepted flit rate average= 0.258177
	minimum = 0.132 (at node 174)
	maximum = 0.464 (at node 44)
Injected packet length average = 17.8263
Accepted packet length average = 18.6704
Total in-flight flits = 33716 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 430.812
	minimum = 22
	maximum = 1487
Network latency average = 411.941
	minimum = 22
	maximum = 1471
Slowest packet = 1929
Flit latency average = 372.965
	minimum = 5
	maximum = 1454
Slowest flit = 38825
Fragmentation average = 53.3212
	minimum = 0
	maximum = 302
Injected packet rate average = 0.0215026
	minimum = 0.0125 (at node 48)
	maximum = 0.028 (at node 25)
Accepted packet rate average = 0.0144479
	minimum = 0.0085 (at node 30)
	maximum = 0.0225 (at node 152)
Injected flit rate average = 0.384768
	minimum = 0.225 (at node 48)
	maximum = 0.504 (at node 25)
Accepted flit rate average= 0.264924
	minimum = 0.158 (at node 30)
	maximum = 0.405 (at node 152)
Injected packet length average = 17.894
Accepted packet length average = 18.3365
Total in-flight flits = 47795 (0 measured)
latency change    = 0.439975
throughput change = 0.0254691
Class 0:
Packet latency average = 992.972
	minimum = 22
	maximum = 2171
Network latency average = 846.42
	minimum = 22
	maximum = 2165
Slowest packet = 3492
Flit latency average = 800.006
	minimum = 5
	maximum = 2218
Slowest flit = 66044
Fragmentation average = 61.9523
	minimum = 0
	maximum = 281
Injected packet rate average = 0.0160208
	minimum = 0.004 (at node 130)
	maximum = 0.03 (at node 181)
Accepted packet rate average = 0.0148385
	minimum = 0.006 (at node 40)
	maximum = 0.026 (at node 120)
Injected flit rate average = 0.287214
	minimum = 0.069 (at node 130)
	maximum = 0.54 (at node 181)
Accepted flit rate average= 0.267531
	minimum = 0.096 (at node 40)
	maximum = 0.466 (at node 120)
Injected packet length average = 17.9275
Accepted packet length average = 18.0295
Total in-flight flits = 52013 (0 measured)
latency change    = 0.566139
throughput change = 0.0097438
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1149.77
	minimum = 25
	maximum = 2189
Network latency average = 370.008
	minimum = 22
	maximum = 976
Slowest packet = 11416
Flit latency average = 922.349
	minimum = 5
	maximum = 2754
Slowest flit = 85445
Fragmentation average = 32.3996
	minimum = 0
	maximum = 209
Injected packet rate average = 0.0150417
	minimum = 0.005 (at node 22)
	maximum = 0.029 (at node 93)
Accepted packet rate average = 0.0147604
	minimum = 0.006 (at node 88)
	maximum = 0.025 (at node 90)
Injected flit rate average = 0.270432
	minimum = 0.091 (at node 95)
	maximum = 0.514 (at node 93)
Accepted flit rate average= 0.267557
	minimum = 0.095 (at node 88)
	maximum = 0.452 (at node 83)
Injected packet length average = 17.9789
Accepted packet length average = 18.1267
Total in-flight flits = 52500 (41700 measured)
latency change    = 0.13637
throughput change = 9.73312e-05
Class 0:
Packet latency average = 1596.1
	minimum = 25
	maximum = 3098
Network latency average = 768.175
	minimum = 22
	maximum = 1980
Slowest packet = 11416
Flit latency average = 943.15
	minimum = 5
	maximum = 2926
Slowest flit = 99593
Fragmentation average = 57.9373
	minimum = 0
	maximum = 249
Injected packet rate average = 0.0150234
	minimum = 0.0055 (at node 151)
	maximum = 0.0225 (at node 135)
Accepted packet rate average = 0.0148021
	minimum = 0.008 (at node 36)
	maximum = 0.022 (at node 66)
Injected flit rate average = 0.270143
	minimum = 0.1035 (at node 151)
	maximum = 0.4115 (at node 135)
Accepted flit rate average= 0.267578
	minimum = 0.1455 (at node 36)
	maximum = 0.4035 (at node 129)
Injected packet length average = 17.9815
Accepted packet length average = 18.0771
Total in-flight flits = 52961 (52332 measured)
latency change    = 0.27964
throughput change = 7.78589e-05
Class 0:
Packet latency average = 1872.97
	minimum = 25
	maximum = 3789
Network latency average = 899.344
	minimum = 22
	maximum = 2789
Slowest packet = 11416
Flit latency average = 953.921
	minimum = 5
	maximum = 3430
Slowest flit = 173836
Fragmentation average = 66.4605
	minimum = 0
	maximum = 288
Injected packet rate average = 0.0149236
	minimum = 0.00366667 (at node 151)
	maximum = 0.022 (at node 117)
Accepted packet rate average = 0.0148351
	minimum = 0.00933333 (at node 89)
	maximum = 0.0203333 (at node 128)
Injected flit rate average = 0.268688
	minimum = 0.069 (at node 151)
	maximum = 0.392667 (at node 117)
Accepted flit rate average= 0.26759
	minimum = 0.171667 (at node 89)
	maximum = 0.379667 (at node 128)
Injected packet length average = 18.0042
Accepted packet length average = 18.0377
Total in-flight flits = 52447 (52386 measured)
latency change    = 0.147822
throughput change = 4.54156e-05
Class 0:
Packet latency average = 2103.54
	minimum = 25
	maximum = 4608
Network latency average = 945.682
	minimum = 22
	maximum = 3434
Slowest packet = 11416
Flit latency average = 958.637
	minimum = 5
	maximum = 4088
Slowest flit = 194453
Fragmentation average = 69.097
	minimum = 0
	maximum = 297
Injected packet rate average = 0.0149401
	minimum = 0.00675 (at node 24)
	maximum = 0.021 (at node 26)
Accepted packet rate average = 0.0148281
	minimum = 0.0105 (at node 86)
	maximum = 0.01925 (at node 128)
Injected flit rate average = 0.268924
	minimum = 0.1235 (at node 24)
	maximum = 0.37575 (at node 26)
Accepted flit rate average= 0.267663
	minimum = 0.1895 (at node 86)
	maximum = 0.356 (at node 128)
Injected packet length average = 18.0002
Accepted packet length average = 18.051
Total in-flight flits = 52800 (52800 measured)
latency change    = 0.109611
throughput change = 0.000270798
Class 0:
Packet latency average = 2314.84
	minimum = 25
	maximum = 5069
Network latency average = 971.781
	minimum = 22
	maximum = 3953
Slowest packet = 11416
Flit latency average = 962.441
	minimum = 5
	maximum = 4254
Slowest flit = 241401
Fragmentation average = 71.2763
	minimum = 0
	maximum = 307
Injected packet rate average = 0.0148792
	minimum = 0.0082 (at node 24)
	maximum = 0.0212 (at node 26)
Accepted packet rate average = 0.0148375
	minimum = 0.0112 (at node 57)
	maximum = 0.0192 (at node 128)
Injected flit rate average = 0.2678
	minimum = 0.146 (at node 24)
	maximum = 0.3816 (at node 26)
Accepted flit rate average= 0.267744
	minimum = 0.1984 (at node 57)
	maximum = 0.3456 (at node 128)
Injected packet length average = 17.9983
Accepted packet length average = 18.0451
Total in-flight flits = 51929 (51929 measured)
latency change    = 0.0912836
throughput change = 0.000302489
Class 0:
Packet latency average = 2520.83
	minimum = 25
	maximum = 5543
Network latency average = 984.386
	minimum = 22
	maximum = 4382
Slowest packet = 11416
Flit latency average = 962.261
	minimum = 5
	maximum = 4303
Slowest flit = 241415
Fragmentation average = 72.2911
	minimum = 0
	maximum = 370
Injected packet rate average = 0.0149054
	minimum = 0.008 (at node 24)
	maximum = 0.021 (at node 26)
Accepted packet rate average = 0.0148655
	minimum = 0.0118333 (at node 67)
	maximum = 0.0188333 (at node 138)
Injected flit rate average = 0.268373
	minimum = 0.144667 (at node 24)
	maximum = 0.3775 (at node 26)
Accepted flit rate average= 0.268049
	minimum = 0.212833 (at node 67)
	maximum = 0.343667 (at node 138)
Injected packet length average = 18.0051
Accepted packet length average = 18.0316
Total in-flight flits = 52173 (52173 measured)
latency change    = 0.0817123
throughput change = 0.00113734
Class 0:
Packet latency average = 2723.39
	minimum = 25
	maximum = 6332
Network latency average = 993.75
	minimum = 22
	maximum = 4382
Slowest packet = 11416
Flit latency average = 962.571
	minimum = 5
	maximum = 4303
Slowest flit = 241415
Fragmentation average = 73.2421
	minimum = 0
	maximum = 370
Injected packet rate average = 0.01484
	minimum = 0.00857143 (at node 24)
	maximum = 0.0205714 (at node 141)
Accepted packet rate average = 0.0148579
	minimum = 0.012 (at node 42)
	maximum = 0.0185714 (at node 103)
Injected flit rate average = 0.267005
	minimum = 0.153429 (at node 24)
	maximum = 0.368 (at node 141)
Accepted flit rate average= 0.267804
	minimum = 0.215 (at node 42)
	maximum = 0.336857 (at node 70)
Injected packet length average = 17.9922
Accepted packet length average = 18.0243
Total in-flight flits = 50861 (50861 measured)
latency change    = 0.074379
throughput change = 0.000914998
Draining all recorded packets ...
Class 0:
Remaining flits: 387720 387721 387722 387723 387724 387725 387726 387727 387728 387729 [...] (50668 flits)
Measured flits: 387720 387721 387722 387723 387724 387725 387726 387727 387728 387729 [...] (50560 flits)
Class 0:
Remaining flits: 387720 387721 387722 387723 387724 387725 387726 387727 387728 387729 [...] (50757 flits)
Measured flits: 387720 387721 387722 387723 387724 387725 387726 387727 387728 387729 [...] (50559 flits)
Class 0:
Remaining flits: 489222 489223 489224 489225 489226 489227 489228 489229 489230 489231 [...] (51523 flits)
Measured flits: 489222 489223 489224 489225 489226 489227 489228 489229 489230 489231 [...] (50042 flits)
Class 0:
Remaining flits: 503712 503713 503714 503715 503716 503717 503718 503719 503720 503721 [...] (51011 flits)
Measured flits: 503712 503713 503714 503715 503716 503717 503718 503719 503720 503721 [...] (44949 flits)
Class 0:
Remaining flits: 584778 584779 584780 584781 584782 584783 614592 614593 614594 614595 [...] (50957 flits)
Measured flits: 584778 584779 584780 584781 584782 584783 614592 614593 614594 614595 [...] (36068 flits)
Class 0:
Remaining flits: 650736 650737 650738 650739 650740 650741 650742 650743 650744 650745 [...] (50976 flits)
Measured flits: 650736 650737 650738 650739 650740 650741 650742 650743 650744 650745 [...] (24172 flits)
Class 0:
Remaining flits: 690678 690679 690680 690681 690682 690683 690684 690685 690686 690687 [...] (50404 flits)
Measured flits: 690678 690679 690680 690681 690682 690683 690684 690685 690686 690687 [...] (13186 flits)
Class 0:
Remaining flits: 783486 783487 783488 783489 783490 783491 783492 783493 783494 783495 [...] (51033 flits)
Measured flits: 783486 783487 783488 783489 783490 783491 783492 783493 783494 783495 [...] (6862 flits)
Class 0:
Remaining flits: 789407 802834 802835 804996 804997 804998 804999 805000 805001 805002 [...] (50299 flits)
Measured flits: 789407 802834 802835 809712 809713 809714 809715 809716 809717 809718 [...] (3462 flits)
Class 0:
Remaining flits: 852696 852697 852698 852699 852700 852701 852702 852703 852704 852705 [...] (49236 flits)
Measured flits: 852696 852697 852698 852699 852700 852701 852702 852703 852704 852705 [...] (1585 flits)
Class 0:
Remaining flits: 852712 852713 898380 898381 898382 898383 898384 898385 898386 898387 [...] (49150 flits)
Measured flits: 852712 852713 948906 948907 948908 948909 948910 948911 948912 948913 [...] (872 flits)
Class 0:
Remaining flits: 898380 898381 898382 898383 898384 898385 898386 898387 898388 898389 [...] (47759 flits)
Measured flits: 1060200 1060201 1060202 1060203 1060204 1060205 1060206 1060207 1060208 1060209 [...] (243 flits)
Class 0:
Remaining flits: 898380 898381 898382 898383 898384 898385 898386 898387 898388 898389 [...] (48563 flits)
Measured flits: 1060200 1060201 1060202 1060203 1060204 1060205 1060206 1060207 1060208 1060209 [...] (54 flits)
Class 0:
Remaining flits: 1016352 1016353 1016354 1016355 1016356 1016357 1016358 1016359 1016360 1016361 [...] (48398 flits)
Measured flits: 1159110 1159111 1159112 1159113 1159114 1159115 1159116 1159117 1159118 1159119 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1086895 1086896 1086897 1086898 1086899 1086900 1086901 1086902 1086903 1086904 [...] (8553 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1298986 1298987 (2 flits)
Measured flits: (0 flits)
Time taken is 26532 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4277.21 (1 samples)
	minimum = 25 (1 samples)
	maximum = 14705 (1 samples)
Network latency average = 1025.92 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6334 (1 samples)
Flit latency average = 960.974 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6666 (1 samples)
Fragmentation average = 79.5474 (1 samples)
	minimum = 0 (1 samples)
	maximum = 370 (1 samples)
Injected packet rate average = 0.01484 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0205714 (1 samples)
Accepted packet rate average = 0.0148579 (1 samples)
	minimum = 0.012 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.267005 (1 samples)
	minimum = 0.153429 (1 samples)
	maximum = 0.368 (1 samples)
Accepted flit rate average = 0.267804 (1 samples)
	minimum = 0.215 (1 samples)
	maximum = 0.336857 (1 samples)
Injected packet size average = 17.9922 (1 samples)
Accepted packet size average = 18.0243 (1 samples)
Hops average = 5.06153 (1 samples)
Total run time 36.4561
