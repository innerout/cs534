BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.013537
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 275.567
	minimum = 26
	maximum = 969
Network latency average = 223.663
	minimum = 25
	maximum = 809
Slowest packet = 73
Flit latency average = 183.307
	minimum = 6
	maximum = 826
Slowest flit = 10738
Fragmentation average = 52.7461
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0139323
	minimum = 0 (at node 30)
	maximum = 0.048 (at node 31)
Accepted packet rate average = 0.00873958
	minimum = 0.003 (at node 172)
	maximum = 0.016 (at node 124)
Injected flit rate average = 0.248964
	minimum = 0 (at node 30)
	maximum = 0.859 (at node 31)
Accepted flit rate average= 0.164536
	minimum = 0.054 (at node 172)
	maximum = 0.296 (at node 140)
Injected packet length average = 17.8695
Accepted packet length average = 18.8266
Total in-flight flits = 16559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 395.876
	minimum = 26
	maximum = 1845
Network latency average = 340.363
	minimum = 25
	maximum = 1617
Slowest packet = 73
Flit latency average = 297.71
	minimum = 6
	maximum = 1600
Slowest flit = 16595
Fragmentation average = 60.165
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0131771
	minimum = 0.001 (at node 107)
	maximum = 0.033 (at node 173)
Accepted packet rate average = 0.00907552
	minimum = 0.004 (at node 135)
	maximum = 0.015 (at node 44)
Injected flit rate average = 0.235995
	minimum = 0.018 (at node 107)
	maximum = 0.594 (at node 173)
Accepted flit rate average= 0.167273
	minimum = 0.072 (at node 135)
	maximum = 0.274 (at node 44)
Injected packet length average = 17.9095
Accepted packet length average = 18.4313
Total in-flight flits = 26847 (0 measured)
latency change    = 0.303905
throughput change = 0.0163623
Class 0:
Packet latency average = 742.212
	minimum = 23
	maximum = 2832
Network latency average = 678.761
	minimum = 23
	maximum = 2569
Slowest packet = 961
Flit latency average = 635.933
	minimum = 6
	maximum = 2552
Slowest flit = 17315
Fragmentation average = 70.1611
	minimum = 0
	maximum = 182
Injected packet rate average = 0.013
	minimum = 0 (at node 76)
	maximum = 0.046 (at node 108)
Accepted packet rate average = 0.009375
	minimum = 0.003 (at node 88)
	maximum = 0.017 (at node 152)
Injected flit rate average = 0.23474
	minimum = 0 (at node 76)
	maximum = 0.84 (at node 138)
Accepted flit rate average= 0.16888
	minimum = 0.065 (at node 88)
	maximum = 0.306 (at node 152)
Injected packet length average = 18.0569
Accepted packet length average = 18.0139
Total in-flight flits = 39350 (0 measured)
latency change    = 0.466626
throughput change = 0.00951426
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 344.3
	minimum = 23
	maximum = 1012
Network latency average = 288.482
	minimum = 23
	maximum = 951
Slowest packet = 7572
Flit latency average = 844.153
	minimum = 6
	maximum = 3074
Slowest flit = 44027
Fragmentation average = 52.7429
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0134167
	minimum = 0 (at node 21)
	maximum = 0.046 (at node 97)
Accepted packet rate average = 0.00954167
	minimum = 0.001 (at node 138)
	maximum = 0.017 (at node 56)
Injected flit rate average = 0.241245
	minimum = 0 (at node 21)
	maximum = 0.831 (at node 97)
Accepted flit rate average= 0.171818
	minimum = 0.02 (at node 138)
	maximum = 0.299 (at node 56)
Injected packet length average = 17.981
Accepted packet length average = 18.0071
Total in-flight flits = 52729 (35467 measured)
latency change    = 1.15571
throughput change = 0.0170966
Class 0:
Packet latency average = 645.011
	minimum = 23
	maximum = 2087
Network latency average = 585.774
	minimum = 23
	maximum = 1962
Slowest packet = 7620
Flit latency average = 979.039
	minimum = 6
	maximum = 4076
Slowest flit = 42641
Fragmentation average = 60.995
	minimum = 0
	maximum = 159
Injected packet rate average = 0.013651
	minimum = 0.0005 (at node 121)
	maximum = 0.036 (at node 97)
Accepted packet rate average = 0.00960417
	minimum = 0.0045 (at node 138)
	maximum = 0.0155 (at node 16)
Injected flit rate average = 0.245234
	minimum = 0.009 (at node 121)
	maximum = 0.6465 (at node 97)
Accepted flit rate average= 0.172823
	minimum = 0.08 (at node 153)
	maximum = 0.277 (at node 16)
Injected packet length average = 17.9645
Accepted packet length average = 17.9946
Total in-flight flits = 67342 (60747 measured)
latency change    = 0.466211
throughput change = 0.00581641
Class 0:
Packet latency average = 947.575
	minimum = 23
	maximum = 2980
Network latency average = 886.908
	minimum = 23
	maximum = 2904
Slowest packet = 7620
Flit latency average = 1123.01
	minimum = 6
	maximum = 4699
Slowest flit = 55817
Fragmentation average = 65.8111
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0131684
	minimum = 0.00133333 (at node 78)
	maximum = 0.0286667 (at node 97)
Accepted packet rate average = 0.00953993
	minimum = 0.00566667 (at node 17)
	maximum = 0.0153333 (at node 16)
Injected flit rate average = 0.236917
	minimum = 0.024 (at node 78)
	maximum = 0.517 (at node 97)
Accepted flit rate average= 0.171783
	minimum = 0.104 (at node 31)
	maximum = 0.279 (at node 16)
Injected packet length average = 17.9913
Accepted packet length average = 18.0067
Total in-flight flits = 76933 (74503 measured)
latency change    = 0.319303
throughput change = 0.00605375
Draining remaining packets ...
Class 0:
Remaining flits: 86202 86203 86204 86205 86206 86207 86208 86209 86210 86211 [...] (47242 flits)
Measured flits: 136062 136063 136064 136065 136066 136067 136068 136069 136070 136071 [...] (46308 flits)
Class 0:
Remaining flits: 108162 108163 108164 108165 108166 108167 108168 108169 108170 108171 [...] (20513 flits)
Measured flits: 136062 136063 136064 136065 136066 136067 136068 136069 136070 136071 [...] (20150 flits)
Class 0:
Remaining flits: 130824 130825 130826 130827 130828 130829 130830 130831 130832 130833 [...] (1769 flits)
Measured flits: 137196 137197 137198 137199 137200 137201 137202 137203 137204 137205 [...] (1751 flits)
Time taken is 9504 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1876.31 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6222 (1 samples)
Network latency average = 1807.87 (1 samples)
	minimum = 23 (1 samples)
	maximum = 6222 (1 samples)
Flit latency average = 1755.44 (1 samples)
	minimum = 6 (1 samples)
	maximum = 6270 (1 samples)
Fragmentation average = 65.7688 (1 samples)
	minimum = 0 (1 samples)
	maximum = 161 (1 samples)
Injected packet rate average = 0.0131684 (1 samples)
	minimum = 0.00133333 (1 samples)
	maximum = 0.0286667 (1 samples)
Accepted packet rate average = 0.00953993 (1 samples)
	minimum = 0.00566667 (1 samples)
	maximum = 0.0153333 (1 samples)
Injected flit rate average = 0.236917 (1 samples)
	minimum = 0.024 (1 samples)
	maximum = 0.517 (1 samples)
Accepted flit rate average = 0.171783 (1 samples)
	minimum = 0.104 (1 samples)
	maximum = 0.279 (1 samples)
Injected packet size average = 17.9913 (1 samples)
Accepted packet size average = 18.0067 (1 samples)
Hops average = 5.07673 (1 samples)
Total run time 4.86987
