BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.040057
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 305.067
	minimum = 22
	maximum = 843
Network latency average = 290.2
	minimum = 22
	maximum = 772
Slowest packet = 290
Flit latency average = 267.919
	minimum = 5
	maximum = 769
Slowest flit = 27266
Fragmentation average = 24.14
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0383958
	minimum = 0.025 (at node 68)
	maximum = 0.053 (at node 177)
Accepted packet rate average = 0.0148385
	minimum = 0.004 (at node 174)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.68501
	minimum = 0.445 (at node 68)
	maximum = 0.945 (at node 177)
Accepted flit rate average= 0.2735
	minimum = 0.105 (at node 174)
	maximum = 0.435 (at node 44)
Injected packet length average = 17.8407
Accepted packet length average = 18.4317
Total in-flight flits = 80346 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 597.761
	minimum = 22
	maximum = 1536
Network latency average = 577.538
	minimum = 22
	maximum = 1469
Slowest packet = 2431
Flit latency average = 554.751
	minimum = 5
	maximum = 1547
Slowest flit = 58077
Fragmentation average = 25.6005
	minimum = 0
	maximum = 134
Injected packet rate average = 0.0349062
	minimum = 0.021 (at node 128)
	maximum = 0.045 (at node 146)
Accepted packet rate average = 0.0150521
	minimum = 0.008 (at node 116)
	maximum = 0.021 (at node 44)
Injected flit rate average = 0.62624
	minimum = 0.378 (at node 128)
	maximum = 0.808 (at node 146)
Accepted flit rate average= 0.27426
	minimum = 0.152 (at node 116)
	maximum = 0.3915 (at node 63)
Injected packet length average = 17.9406
Accepted packet length average = 18.2208
Total in-flight flits = 137306 (0 measured)
latency change    = 0.48965
throughput change = 0.00277261
Class 0:
Packet latency average = 1512.57
	minimum = 22
	maximum = 2296
Network latency average = 1448.56
	minimum = 22
	maximum = 2235
Slowest packet = 3780
Flit latency average = 1430.73
	minimum = 5
	maximum = 2296
Slowest flit = 92420
Fragmentation average = 22.1608
	minimum = 0
	maximum = 203
Injected packet rate average = 0.0178438
	minimum = 0.002 (at node 19)
	maximum = 0.04 (at node 174)
Accepted packet rate average = 0.0151302
	minimum = 0.006 (at node 132)
	maximum = 0.024 (at node 24)
Injected flit rate average = 0.32276
	minimum = 0.036 (at node 19)
	maximum = 0.715 (at node 26)
Accepted flit rate average= 0.270698
	minimum = 0.108 (at node 132)
	maximum = 0.421 (at node 24)
Injected packet length average = 18.0881
Accepted packet length average = 17.8912
Total in-flight flits = 147864 (0 measured)
latency change    = 0.604804
throughput change = 0.0131604
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1331.82
	minimum = 147
	maximum = 2405
Network latency average = 46.6289
	minimum = 22
	maximum = 738
Slowest packet = 16971
Flit latency average = 1965.29
	minimum = 5
	maximum = 3052
Slowest flit = 124032
Fragmentation average = 4.19588
	minimum = 0
	maximum = 26
Injected packet rate average = 0.015776
	minimum = 0.003 (at node 68)
	maximum = 0.04 (at node 28)
Accepted packet rate average = 0.0148281
	minimum = 0.006 (at node 5)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.283432
	minimum = 0.054 (at node 68)
	maximum = 0.725 (at node 28)
Accepted flit rate average= 0.266427
	minimum = 0.121 (at node 35)
	maximum = 0.429 (at node 38)
Injected packet length average = 17.966
Accepted packet length average = 17.9677
Total in-flight flits = 151106 (52650 measured)
latency change    = 0.135711
throughput change = 0.01603
Class 0:
Packet latency average = 2205.6
	minimum = 147
	maximum = 3653
Network latency average = 840.732
	minimum = 22
	maximum = 1954
Slowest packet = 16971
Flit latency average = 2185.11
	minimum = 5
	maximum = 3896
Slowest flit = 140417
Fragmentation average = 11.6888
	minimum = 0
	maximum = 87
Injected packet rate average = 0.0151302
	minimum = 0.0055 (at node 38)
	maximum = 0.0285 (at node 3)
Accepted packet rate average = 0.0148438
	minimum = 0.009 (at node 48)
	maximum = 0.0205 (at node 178)
Injected flit rate average = 0.271924
	minimum = 0.099 (at node 38)
	maximum = 0.515 (at node 100)
Accepted flit rate average= 0.267406
	minimum = 0.17 (at node 48)
	maximum = 0.3715 (at node 178)
Injected packet length average = 17.9723
Accepted packet length average = 18.0147
Total in-flight flits = 149616 (96388 measured)
latency change    = 0.396162
throughput change = 0.00366172
Class 0:
Packet latency average = 2943.78
	minimum = 147
	maximum = 4574
Network latency average = 1403.03
	minimum = 22
	maximum = 2975
Slowest packet = 16971
Flit latency average = 2346.4
	minimum = 5
	maximum = 4647
Slowest flit = 176143
Fragmentation average = 15.0424
	minimum = 0
	maximum = 154
Injected packet rate average = 0.0152899
	minimum = 0.006 (at node 15)
	maximum = 0.0243333 (at node 100)
Accepted packet rate average = 0.0148837
	minimum = 0.01 (at node 135)
	maximum = 0.0206667 (at node 178)
Injected flit rate average = 0.274823
	minimum = 0.112667 (at node 15)
	maximum = 0.439333 (at node 100)
Accepted flit rate average= 0.268059
	minimum = 0.183333 (at node 135)
	maximum = 0.368 (at node 178)
Injected packet length average = 17.9741
Accepted packet length average = 18.0103
Total in-flight flits = 151808 (131963 measured)
latency change    = 0.250759
throughput change = 0.0024352
Draining remaining packets ...
Class 0:
Remaining flits: 195318 195319 195320 195321 195322 195323 195324 195325 195326 195327 [...] (102629 flits)
Measured flits: 305532 305533 305534 305535 305536 305537 305538 305539 305540 305541 [...] (99649 flits)
Class 0:
Remaining flits: 282726 282727 282728 282729 282730 282731 282732 282733 282734 282735 [...] (53970 flits)
Measured flits: 305860 305861 305862 305863 305864 305865 305866 305867 305868 305869 [...] (53868 flits)
Class 0:
Remaining flits: 323046 323047 323048 323049 323050 323051 323052 323053 323054 323055 [...] (8250 flits)
Measured flits: 323046 323047 323048 323049 323050 323051 323052 323053 323054 323055 [...] (8250 flits)
Time taken is 9798 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4488.7 (1 samples)
	minimum = 147 (1 samples)
	maximum = 7164 (1 samples)
Network latency average = 2774.68 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5801 (1 samples)
Flit latency average = 2739.72 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5888 (1 samples)
Fragmentation average = 18.6935 (1 samples)
	minimum = 0 (1 samples)
	maximum = 289 (1 samples)
Injected packet rate average = 0.0152899 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0148837 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0206667 (1 samples)
Injected flit rate average = 0.274823 (1 samples)
	minimum = 0.112667 (1 samples)
	maximum = 0.439333 (1 samples)
Accepted flit rate average = 0.268059 (1 samples)
	minimum = 0.183333 (1 samples)
	maximum = 0.368 (1 samples)
Injected packet size average = 17.9741 (1 samples)
Accepted packet size average = 18.0103 (1 samples)
Hops average = 5.07548 (1 samples)
Total run time 8.53887
