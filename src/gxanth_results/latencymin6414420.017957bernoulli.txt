BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 171.187
	minimum = 22
	maximum = 510
Network latency average = 167.448
	minimum = 22
	maximum = 509
Slowest packet = 1296
Flit latency average = 138.137
	minimum = 5
	maximum = 539
Slowest flit = 28314
Fragmentation average = 29.5004
	minimum = 0
	maximum = 185
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.0129896
	minimum = 0.004 (at node 41)
	maximum = 0.024 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.240182
	minimum = 0.072 (at node 41)
	maximum = 0.446 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 18.4904
Total in-flight flits = 15949 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 271.228
	minimum = 22
	maximum = 912
Network latency average = 267.344
	minimum = 22
	maximum = 912
Slowest packet = 3679
Flit latency average = 237.534
	minimum = 5
	maximum = 924
Slowest flit = 65658
Fragmentation average = 30.7983
	minimum = 0
	maximum = 191
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.013737
	minimum = 0.007 (at node 153)
	maximum = 0.02 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.251135
	minimum = 0.1385 (at node 153)
	maximum = 0.36 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 18.2817
Total in-flight flits = 26270 (0 measured)
latency change    = 0.368843
throughput change = 0.0436144
Class 0:
Packet latency average = 526.403
	minimum = 25
	maximum = 1192
Network latency average = 522.369
	minimum = 22
	maximum = 1176
Slowest packet = 6141
Flit latency average = 493.331
	minimum = 5
	maximum = 1163
Slowest flit = 112689
Fragmentation average = 32.0444
	minimum = 0
	maximum = 237
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0147865
	minimum = 0.006 (at node 163)
	maximum = 0.026 (at node 16)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.265547
	minimum = 0.108 (at node 163)
	maximum = 0.479 (at node 159)
Injected packet length average = 17.9907
Accepted packet length average = 17.9588
Total in-flight flits = 37079 (0 measured)
latency change    = 0.484753
throughput change = 0.0542709
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 455.512
	minimum = 22
	maximum = 989
Network latency average = 451.554
	minimum = 22
	maximum = 989
Slowest packet = 10256
Flit latency average = 649.519
	minimum = 5
	maximum = 1465
Slowest flit = 152081
Fragmentation average = 24.3958
	minimum = 0
	maximum = 149
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0151406
	minimum = 0.007 (at node 86)
	maximum = 0.028 (at node 90)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.272172
	minimum = 0.14 (at node 154)
	maximum = 0.494 (at node 90)
Injected packet length average = 17.9626
Accepted packet length average = 17.9763
Total in-flight flits = 48812 (46754 measured)
latency change    = 0.155629
throughput change = 0.0243412
Class 0:
Packet latency average = 739.263
	minimum = 22
	maximum = 1881
Network latency average = 735.109
	minimum = 22
	maximum = 1861
Slowest packet = 10293
Flit latency average = 733.568
	minimum = 5
	maximum = 1897
Slowest flit = 189718
Fragmentation average = 31.888
	minimum = 0
	maximum = 248
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.015099
	minimum = 0.0105 (at node 2)
	maximum = 0.0235 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.271753
	minimum = 0.1765 (at node 2)
	maximum = 0.423 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 17.9981
Total in-flight flits = 58114 (58114 measured)
latency change    = 0.383829
throughput change = 0.00154284
Class 0:
Packet latency average = 868.115
	minimum = 22
	maximum = 2105
Network latency average = 863.866
	minimum = 22
	maximum = 2102
Slowest packet = 13109
Flit latency average = 821.219
	minimum = 5
	maximum = 2085
Slowest flit = 235979
Fragmentation average = 33.4771
	minimum = 0
	maximum = 262
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0150868
	minimum = 0.011 (at node 2)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.271642
	minimum = 0.189667 (at node 2)
	maximum = 0.381 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.0053
Total in-flight flits = 68677 (68677 measured)
latency change    = 0.148428
throughput change = 0.000405839
Class 0:
Packet latency average = 962.413
	minimum = 22
	maximum = 2364
Network latency average = 958.14
	minimum = 22
	maximum = 2364
Slowest packet = 13589
Flit latency average = 901.537
	minimum = 5
	maximum = 2347
Slowest flit = 244619
Fragmentation average = 34.0986
	minimum = 0
	maximum = 262
Injected packet rate average = 0.0181263
	minimum = 0.0125 (at node 55)
	maximum = 0.02475 (at node 104)
Accepted packet rate average = 0.0150495
	minimum = 0.011 (at node 104)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.326395
	minimum = 0.225 (at node 55)
	maximum = 0.4425 (at node 104)
Accepted flit rate average= 0.271204
	minimum = 0.19825 (at node 104)
	maximum = 0.3595 (at node 128)
Injected packet length average = 18.0067
Accepted packet length average = 18.0209
Total in-flight flits = 79372 (79372 measured)
latency change    = 0.0979808
throughput change = 0.00161477
Class 0:
Packet latency average = 1052.77
	minimum = 22
	maximum = 2647
Network latency average = 1048.54
	minimum = 22
	maximum = 2647
Slowest packet = 18437
Flit latency average = 983.133
	minimum = 5
	maximum = 2630
Slowest flit = 331881
Fragmentation average = 34.7739
	minimum = 0
	maximum = 276
Injected packet rate average = 0.0180781
	minimum = 0.0132 (at node 121)
	maximum = 0.0226 (at node 104)
Accepted packet rate average = 0.0150927
	minimum = 0.0112 (at node 42)
	maximum = 0.0198 (at node 95)
Injected flit rate average = 0.325454
	minimum = 0.2376 (at node 121)
	maximum = 0.4068 (at node 104)
Accepted flit rate average= 0.27156
	minimum = 0.2016 (at node 42)
	maximum = 0.3534 (at node 95)
Injected packet length average = 18.0027
Accepted packet length average = 17.9928
Total in-flight flits = 88771 (88771 measured)
latency change    = 0.0858308
throughput change = 0.0013109
Class 0:
Packet latency average = 1137.09
	minimum = 22
	maximum = 2910
Network latency average = 1132.9
	minimum = 22
	maximum = 2910
Slowest packet = 18324
Flit latency average = 1063.43
	minimum = 5
	maximum = 2893
Slowest flit = 329849
Fragmentation average = 35.13
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0180833
	minimum = 0.0145 (at node 69)
	maximum = 0.0225 (at node 104)
Accepted packet rate average = 0.0151076
	minimum = 0.0116667 (at node 42)
	maximum = 0.0195 (at node 103)
Injected flit rate average = 0.325551
	minimum = 0.261 (at node 69)
	maximum = 0.405 (at node 104)
Accepted flit rate average= 0.272035
	minimum = 0.212667 (at node 42)
	maximum = 0.351 (at node 103)
Injected packet length average = 18.0028
Accepted packet length average = 18.0064
Total in-flight flits = 98671 (98671 measured)
latency change    = 0.0741498
throughput change = 0.00174355
Class 0:
Packet latency average = 1219.51
	minimum = 22
	maximum = 3303
Network latency average = 1215.35
	minimum = 22
	maximum = 3303
Slowest packet = 22598
Flit latency average = 1142.32
	minimum = 5
	maximum = 3286
Slowest flit = 406781
Fragmentation average = 35.5073
	minimum = 0
	maximum = 290
Injected packet rate average = 0.0180737
	minimum = 0.0144286 (at node 23)
	maximum = 0.022 (at node 104)
Accepted packet rate average = 0.0151399
	minimum = 0.0122857 (at node 63)
	maximum = 0.0188571 (at node 70)
Injected flit rate average = 0.325296
	minimum = 0.260571 (at node 55)
	maximum = 0.396 (at node 104)
Accepted flit rate average= 0.272492
	minimum = 0.223429 (at node 63)
	maximum = 0.339429 (at node 70)
Injected packet length average = 17.9984
Accepted packet length average = 17.9983
Total in-flight flits = 108088 (108088 measured)
latency change    = 0.0675871
throughput change = 0.00167746
Draining all recorded packets ...
Class 0:
Remaining flits: 468090 468091 468092 468093 468094 468095 468096 468097 468098 468099 [...] (118267 flits)
Measured flits: 468090 468091 468092 468093 468094 468095 468096 468097 468098 468099 [...] (60731 flits)
Class 0:
Remaining flits: 526068 526069 526070 526071 526072 526073 526074 526075 526076 526077 [...] (126072 flits)
Measured flits: 526068 526069 526070 526071 526072 526073 526074 526075 526076 526077 [...] (17658 flits)
Class 0:
Remaining flits: 538951 538952 538953 538954 538955 562206 562207 562208 562209 562210 [...] (134526 flits)
Measured flits: 538951 538952 538953 538954 538955 562206 562207 562208 562209 562210 [...] (1176 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 626148 626149 626150 626151 626152 626153 626154 626155 626156 626157 [...] (95159 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 699894 699895 699896 699897 699898 699899 699900 699901 699902 699903 [...] (47657 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 758052 758053 758054 758055 758056 758057 758058 758059 758060 758061 [...] (8489 flits)
Measured flits: (0 flits)
Time taken is 17951 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1468 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4375 (1 samples)
Network latency average = 1463.89 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4359 (1 samples)
Flit latency average = 1729 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5659 (1 samples)
Fragmentation average = 37.8549 (1 samples)
	minimum = 0 (1 samples)
	maximum = 309 (1 samples)
Injected packet rate average = 0.0180737 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.0151399 (1 samples)
	minimum = 0.0122857 (1 samples)
	maximum = 0.0188571 (1 samples)
Injected flit rate average = 0.325296 (1 samples)
	minimum = 0.260571 (1 samples)
	maximum = 0.396 (1 samples)
Accepted flit rate average = 0.272492 (1 samples)
	minimum = 0.223429 (1 samples)
	maximum = 0.339429 (1 samples)
Injected packet size average = 17.9984 (1 samples)
Accepted packet size average = 17.9983 (1 samples)
Hops average = 5.06817 (1 samples)
Total run time 16.2606
