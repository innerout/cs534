BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 292.199
	minimum = 23
	maximum = 963
Network latency average = 285.752
	minimum = 23
	maximum = 951
Slowest packet = 186
Flit latency average = 218.81
	minimum = 6
	maximum = 945
Slowest flit = 2265
Fragmentation average = 153.014
	minimum = 0
	maximum = 815
Injected packet rate average = 0.0267969
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0103125
	minimum = 0.003 (at node 106)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.478594
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.213286
	minimum = 0.078 (at node 106)
	maximum = 0.34 (at node 91)
Injected packet length average = 17.8601
Accepted packet length average = 20.6823
Total in-flight flits = 51659 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 525.203
	minimum = 23
	maximum = 1778
Network latency average = 517.964
	minimum = 23
	maximum = 1746
Slowest packet = 970
Flit latency average = 441.647
	minimum = 6
	maximum = 1871
Slowest flit = 10224
Fragmentation average = 187.383
	minimum = 0
	maximum = 1679
Injected packet rate average = 0.026526
	minimum = 0.0175 (at node 41)
	maximum = 0.0335 (at node 187)
Accepted packet rate average = 0.0114375
	minimum = 0.0065 (at node 40)
	maximum = 0.017 (at node 51)
Injected flit rate average = 0.475794
	minimum = 0.312 (at node 72)
	maximum = 0.603 (at node 187)
Accepted flit rate average= 0.219875
	minimum = 0.127 (at node 40)
	maximum = 0.327 (at node 51)
Injected packet length average = 17.9369
Accepted packet length average = 19.224
Total in-flight flits = 98952 (0 measured)
latency change    = 0.443645
throughput change = 0.0299649
Class 0:
Packet latency average = 1196.33
	minimum = 24
	maximum = 2822
Network latency average = 1186.15
	minimum = 24
	maximum = 2822
Slowest packet = 761
Flit latency average = 1123.5
	minimum = 6
	maximum = 2840
Slowest flit = 12050
Fragmentation average = 221.632
	minimum = 1
	maximum = 1935
Injected packet rate average = 0.0252865
	minimum = 0.011 (at node 116)
	maximum = 0.043 (at node 102)
Accepted packet rate average = 0.012599
	minimum = 0.004 (at node 153)
	maximum = 0.025 (at node 16)
Injected flit rate average = 0.453964
	minimum = 0.197 (at node 116)
	maximum = 0.782 (at node 102)
Accepted flit rate average= 0.226432
	minimum = 0.089 (at node 32)
	maximum = 0.451 (at node 16)
Injected packet length average = 17.9528
Accepted packet length average = 17.9723
Total in-flight flits = 142831 (0 measured)
latency change    = 0.560987
throughput change = 0.0289592
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 206.809
	minimum = 25
	maximum = 1755
Network latency average = 124.299
	minimum = 25
	maximum = 835
Slowest packet = 15085
Flit latency average = 1659.07
	minimum = 6
	maximum = 3708
Slowest flit = 25740
Fragmentation average = 18.8058
	minimum = 1
	maximum = 207
Injected packet rate average = 0.0240521
	minimum = 0.008 (at node 80)
	maximum = 0.042 (at node 134)
Accepted packet rate average = 0.0122396
	minimum = 0.003 (at node 17)
	maximum = 0.022 (at node 56)
Injected flit rate average = 0.431943
	minimum = 0.14 (at node 188)
	maximum = 0.771 (at node 134)
Accepted flit rate average= 0.219432
	minimum = 0.05 (at node 17)
	maximum = 0.38 (at node 56)
Injected packet length average = 17.9586
Accepted packet length average = 17.9281
Total in-flight flits = 183860 (77973 measured)
latency change    = 4.78468
throughput change = 0.0319005
Class 0:
Packet latency average = 493.774
	minimum = 25
	maximum = 2900
Network latency average = 349.174
	minimum = 25
	maximum = 1951
Slowest packet = 15085
Flit latency average = 1896.82
	minimum = 6
	maximum = 4617
Slowest flit = 28943
Fragmentation average = 42.4033
	minimum = 1
	maximum = 411
Injected packet rate average = 0.0240026
	minimum = 0.006 (at node 188)
	maximum = 0.0355 (at node 139)
Accepted packet rate average = 0.0120833
	minimum = 0.0045 (at node 17)
	maximum = 0.018 (at node 151)
Injected flit rate average = 0.431948
	minimum = 0.101 (at node 188)
	maximum = 0.6365 (at node 139)
Accepted flit rate average= 0.216003
	minimum = 0.089 (at node 17)
	maximum = 0.317 (at node 187)
Injected packet length average = 17.9959
Accepted packet length average = 17.8761
Total in-flight flits = 225918 (153365 measured)
latency change    = 0.581166
throughput change = 0.015878
Class 0:
Packet latency average = 1174.78
	minimum = 23
	maximum = 3832
Network latency average = 1003.59
	minimum = 23
	maximum = 2960
Slowest packet = 15085
Flit latency average = 2133.4
	minimum = 6
	maximum = 5496
Slowest flit = 18431
Fragmentation average = 88.6697
	minimum = 0
	maximum = 1046
Injected packet rate average = 0.0237101
	minimum = 0.007 (at node 96)
	maximum = 0.0336667 (at node 145)
Accepted packet rate average = 0.0119184
	minimum = 0.007 (at node 36)
	maximum = 0.0183333 (at node 181)
Injected flit rate average = 0.426696
	minimum = 0.126667 (at node 96)
	maximum = 0.610333 (at node 145)
Accepted flit rate average= 0.21275
	minimum = 0.128333 (at node 17)
	maximum = 0.311 (at node 181)
Injected packet length average = 17.9964
Accepted packet length average = 17.8505
Total in-flight flits = 266221 (218449 measured)
latency change    = 0.579689
throughput change = 0.0152884
Class 0:
Packet latency average = 1826.66
	minimum = 23
	maximum = 4704
Network latency average = 1625.54
	minimum = 23
	maximum = 3940
Slowest packet = 15085
Flit latency average = 2359.15
	minimum = 6
	maximum = 6579
Slowest flit = 37591
Fragmentation average = 117.607
	minimum = 0
	maximum = 1257
Injected packet rate average = 0.0233359
	minimum = 0.0085 (at node 96)
	maximum = 0.03325 (at node 145)
Accepted packet rate average = 0.0117513
	minimum = 0.00675 (at node 17)
	maximum = 0.0165 (at node 151)
Injected flit rate average = 0.419876
	minimum = 0.15575 (at node 96)
	maximum = 0.5995 (at node 145)
Accepted flit rate average= 0.210824
	minimum = 0.1305 (at node 17)
	maximum = 0.29375 (at node 151)
Injected packet length average = 17.9927
Accepted packet length average = 17.9405
Total in-flight flits = 303730 (271799 measured)
latency change    = 0.356871
throughput change = 0.00913454
Class 0:
Packet latency average = 2369.52
	minimum = 23
	maximum = 5612
Network latency average = 2138.06
	minimum = 23
	maximum = 4936
Slowest packet = 15085
Flit latency average = 2607.19
	minimum = 6
	maximum = 7413
Slowest flit = 39054
Fragmentation average = 135.412
	minimum = 0
	maximum = 2572
Injected packet rate average = 0.0227781
	minimum = 0.0086 (at node 132)
	maximum = 0.031 (at node 145)
Accepted packet rate average = 0.0116635
	minimum = 0.0078 (at node 17)
	maximum = 0.015 (at node 68)
Injected flit rate average = 0.409734
	minimum = 0.1552 (at node 132)
	maximum = 0.5592 (at node 145)
Accepted flit rate average= 0.209175
	minimum = 0.142 (at node 17)
	maximum = 0.2648 (at node 68)
Injected packet length average = 17.9881
Accepted packet length average = 17.9341
Total in-flight flits = 336061 (315290 measured)
latency change    = 0.229099
throughput change = 0.0078844
Class 0:
Packet latency average = 2872.18
	minimum = 23
	maximum = 6640
Network latency average = 2607.4
	minimum = 23
	maximum = 5973
Slowest packet = 15085
Flit latency average = 2853.43
	minimum = 6
	maximum = 8060
Slowest flit = 39059
Fragmentation average = 147.318
	minimum = 0
	maximum = 2572
Injected packet rate average = 0.0214583
	minimum = 0.00716667 (at node 132)
	maximum = 0.0293333 (at node 85)
Accepted packet rate average = 0.0115694
	minimum = 0.0085 (at node 17)
	maximum = 0.0151667 (at node 181)
Injected flit rate average = 0.386255
	minimum = 0.129333 (at node 132)
	maximum = 0.5275 (at node 85)
Accepted flit rate average= 0.207373
	minimum = 0.1535 (at node 4)
	maximum = 0.266333 (at node 181)
Injected packet length average = 18.0002
Accepted packet length average = 17.9242
Total in-flight flits = 349851 (336037 measured)
latency change    = 0.175009
throughput change = 0.00868837
Class 0:
Packet latency average = 3310.26
	minimum = 23
	maximum = 7569
Network latency average = 3008.73
	minimum = 23
	maximum = 6876
Slowest packet = 15085
Flit latency average = 3095.73
	minimum = 6
	maximum = 9033
Slowest flit = 88009
Fragmentation average = 153.481
	minimum = 0
	maximum = 2572
Injected packet rate average = 0.0202388
	minimum = 0.00614286 (at node 132)
	maximum = 0.0271429 (at node 177)
Accepted packet rate average = 0.0114955
	minimum = 0.00828571 (at node 4)
	maximum = 0.0145714 (at node 123)
Injected flit rate average = 0.364264
	minimum = 0.110857 (at node 132)
	maximum = 0.486143 (at node 177)
Accepted flit rate average= 0.205787
	minimum = 0.149429 (at node 4)
	maximum = 0.257429 (at node 123)
Injected packet length average = 17.9983
Accepted packet length average = 17.9015
Total in-flight flits = 357131 (348134 measured)
latency change    = 0.132342
throughput change = 0.00770729
Draining all recorded packets ...
Class 0:
Remaining flits: 84546 84547 84548 84549 84550 84551 84552 84553 84554 84555 [...] (359693 flits)
Measured flits: 270839 270840 270841 270842 270843 270844 270845 271008 271009 271010 [...] (346526 flits)
Class 0:
Remaining flits: 84546 84547 84548 84549 84550 84551 84552 84553 84554 84555 [...] (359955 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (338148 flits)
Class 0:
Remaining flits: 84546 84547 84548 84549 84550 84551 84552 84553 84554 84555 [...] (357471 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (323931 flits)
Class 0:
Remaining flits: 141609 141610 141611 141612 141613 141614 141615 141616 141617 141618 [...] (355524 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (306812 flits)
Class 0:
Remaining flits: 162499 162500 162501 162502 162503 168948 168949 168950 168951 168952 [...] (353895 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (283845 flits)
Class 0:
Remaining flits: 176220 176221 176222 176223 176224 176225 176226 176227 176228 176229 [...] (349600 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (257560 flits)
Class 0:
Remaining flits: 176220 176221 176222 176223 176224 176225 176226 176227 176228 176229 [...] (346621 flits)
Measured flits: 271008 271009 271010 271011 271012 271013 271014 271015 271016 271017 [...] (233099 flits)
Class 0:
Remaining flits: 176220 176221 176222 176223 176224 176225 176226 176227 176228 176229 [...] (346843 flits)
Measured flits: 271744 271745 272803 272804 272805 272806 272807 277128 277129 277130 [...] (208678 flits)
Class 0:
Remaining flits: 176220 176221 176222 176223 176224 176225 176226 176227 176228 176229 [...] (341502 flits)
Measured flits: 293544 293545 293546 293547 293548 293549 293550 293551 293552 293553 [...] (182855 flits)
Class 0:
Remaining flits: 176226 176227 176228 176229 176230 176231 176232 176233 176234 176235 [...] (339709 flits)
Measured flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (155562 flits)
Class 0:
Remaining flits: 219569 219570 219571 219572 219573 219574 219575 219576 219577 219578 [...] (338506 flits)
Measured flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (130828 flits)
Class 0:
Remaining flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (337326 flits)
Measured flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (108196 flits)
Class 0:
Remaining flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (335367 flits)
Measured flits: 300096 300097 300098 300099 300100 300101 300102 300103 300104 300105 [...] (88154 flits)
Class 0:
Remaining flits: 356310 356311 356312 356313 356314 356315 356316 356317 356318 356319 [...] (332835 flits)
Measured flits: 356310 356311 356312 356313 356314 356315 356316 356317 356318 356319 [...] (71422 flits)
Class 0:
Remaining flits: 356323 356324 356325 356326 356327 384498 384499 384500 384501 384502 [...] (338398 flits)
Measured flits: 356323 356324 356325 356326 356327 384498 384499 384500 384501 384502 [...] (56645 flits)
Class 0:
Remaining flits: 384498 384499 384500 384501 384502 384503 384504 384505 384506 384507 [...] (337815 flits)
Measured flits: 384498 384499 384500 384501 384502 384503 384504 384505 384506 384507 [...] (43987 flits)
Class 0:
Remaining flits: 396630 396631 396632 396633 396634 396635 396636 396637 396638 396639 [...] (334321 flits)
Measured flits: 396630 396631 396632 396633 396634 396635 396636 396637 396638 396639 [...] (33951 flits)
Class 0:
Remaining flits: 396630 396631 396632 396633 396634 396635 396636 396637 396638 396639 [...] (335158 flits)
Measured flits: 396630 396631 396632 396633 396634 396635 396636 396637 396638 396639 [...] (25965 flits)
Class 0:
Remaining flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (333074 flits)
Measured flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (19390 flits)
Class 0:
Remaining flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (330109 flits)
Measured flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (14163 flits)
Class 0:
Remaining flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (331638 flits)
Measured flits: 439848 439849 439850 439851 439852 439853 439854 439855 439856 439857 [...] (10524 flits)
Class 0:
Remaining flits: 501548 501549 501550 501551 518454 518455 518456 518457 518458 518459 [...] (334618 flits)
Measured flits: 501548 501549 501550 501551 518454 518455 518456 518457 518458 518459 [...] (7684 flits)
Class 0:
Remaining flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (335580 flits)
Measured flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (5524 flits)
Class 0:
Remaining flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (336584 flits)
Measured flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (3944 flits)
Class 0:
Remaining flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (335348 flits)
Measured flits: 583308 583309 583310 583311 583312 583313 583314 583315 583316 583317 [...] (2843 flits)
Class 0:
Remaining flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (331928 flits)
Measured flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (2091 flits)
Class 0:
Remaining flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (330057 flits)
Measured flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (1560 flits)
Class 0:
Remaining flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (325776 flits)
Measured flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (1207 flits)
Class 0:
Remaining flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (324203 flits)
Measured flits: 586998 586999 587000 587001 587002 587003 587004 587005 587006 587007 [...] (771 flits)
Class 0:
Remaining flits: 671868 671869 671870 671871 671872 671873 671874 671875 671876 671877 [...] (323249 flits)
Measured flits: 671868 671869 671870 671871 671872 671873 671874 671875 671876 671877 [...] (600 flits)
Class 0:
Remaining flits: 671868 671869 671870 671871 671872 671873 671874 671875 671876 671877 [...] (321523 flits)
Measured flits: 671868 671869 671870 671871 671872 671873 671874 671875 671876 671877 [...] (438 flits)
Class 0:
Remaining flits: 671872 671873 671874 671875 671876 671877 671878 671879 671880 671881 [...] (317333 flits)
Measured flits: 671872 671873 671874 671875 671876 671877 671878 671879 671880 671881 [...] (342 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (314965 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (202 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (315896 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (155 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (318081 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (108 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (320640 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (108 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (317944 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (108 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (316937 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (90 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (318169 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (90 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (317618 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (72 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (317550 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (72 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (313764 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (54 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (313703 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (54 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (313749 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (54 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (313341 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (54 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (313685 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (54 flits)
Class 0:
Remaining flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (314277 flits)
Measured flits: 747072 747073 747074 747075 747076 747077 747078 747079 747080 747081 [...] (36 flits)
Class 0:
Remaining flits: 1246251 1246252 1246253 1246254 1246255 1246256 1246257 1246258 1246259 1246260 [...] (314863 flits)
Measured flits: 1764972 1764973 1764974 1764975 1764976 1764977 1764978 1764979 1764980 1764981 [...] (18 flits)
Class 0:
Remaining flits: 1301436 1301437 1301438 1301439 1301440 1301441 1301442 1301443 1301444 1301445 [...] (316108 flits)
Measured flits: 1764989 (1 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (282241 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (247936 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (213145 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (178798 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (146210 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (114641 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (84389 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (58564 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (37477 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1426572 1426573 1426574 1426575 1426576 1426577 1426578 1426579 1426580 1426581 [...] (19786 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1889907 1889908 1889909 1917738 1917739 1917740 1917741 1917742 1917743 1917744 [...] (7570 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1917752 1917753 1917754 1917755 1926612 1926613 1926614 1926615 1926616 1926617 [...] (1797 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2127465 2127466 2127467 2127468 2127469 2127470 2127471 2127472 2127473 2161684 [...] (321 flits)
Measured flits: (0 flits)
Time taken is 72695 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9457.09 (1 samples)
	minimum = 23 (1 samples)
	maximum = 49383 (1 samples)
Network latency average = 7729.08 (1 samples)
	minimum = 23 (1 samples)
	maximum = 47817 (1 samples)
Flit latency average = 8201.94 (1 samples)
	minimum = 6 (1 samples)
	maximum = 47691 (1 samples)
Fragmentation average = 202.263 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4584 (1 samples)
Injected packet rate average = 0.0202388 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.0271429 (1 samples)
Accepted packet rate average = 0.0114955 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0145714 (1 samples)
Injected flit rate average = 0.364264 (1 samples)
	minimum = 0.110857 (1 samples)
	maximum = 0.486143 (1 samples)
Accepted flit rate average = 0.205787 (1 samples)
	minimum = 0.149429 (1 samples)
	maximum = 0.257429 (1 samples)
Injected packet size average = 17.9983 (1 samples)
Accepted packet size average = 17.9015 (1 samples)
Hops average = 5.0604 (1 samples)
Total run time 91.8843
