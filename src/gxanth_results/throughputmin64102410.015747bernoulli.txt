BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 218.621
	minimum = 23
	maximum = 956
Network latency average = 215.617
	minimum = 23
	maximum = 956
Slowest packet = 84
Flit latency average = 143.787
	minimum = 6
	maximum = 939
Slowest flit = 1529
Fragmentation average = 133.953
	minimum = 0
	maximum = 749
Injected packet rate average = 0.0158542
	minimum = 0.008 (at node 65)
	maximum = 0.027 (at node 14)
Accepted packet rate average = 0.00924479
	minimum = 0.002 (at node 174)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.282297
	minimum = 0.144 (at node 65)
	maximum = 0.486 (at node 14)
Accepted flit rate average= 0.190411
	minimum = 0.053 (at node 174)
	maximum = 0.342 (at node 44)
Injected packet length average = 17.8058
Accepted packet length average = 20.5966
Total in-flight flits = 18233 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 336.159
	minimum = 23
	maximum = 1895
Network latency average = 332.944
	minimum = 23
	maximum = 1890
Slowest packet = 231
Flit latency average = 239.185
	minimum = 6
	maximum = 1873
Slowest flit = 4175
Fragmentation average = 182.479
	minimum = 0
	maximum = 1809
Injected packet rate average = 0.0156458
	minimum = 0.009 (at node 164)
	maximum = 0.0235 (at node 44)
Accepted packet rate average = 0.010599
	minimum = 0.0055 (at node 83)
	maximum = 0.0155 (at node 22)
Injected flit rate average = 0.280391
	minimum = 0.162 (at node 164)
	maximum = 0.423 (at node 44)
Accepted flit rate average= 0.205352
	minimum = 0.1045 (at node 83)
	maximum = 0.315 (at node 22)
Injected packet length average = 17.9211
Accepted packet length average = 19.3747
Total in-flight flits = 29289 (0 measured)
latency change    = 0.34965
throughput change = 0.0727538
Class 0:
Packet latency average = 576.531
	minimum = 23
	maximum = 2547
Network latency average = 573.309
	minimum = 23
	maximum = 2540
Slowest packet = 614
Flit latency average = 458.03
	minimum = 6
	maximum = 2918
Slowest flit = 3215
Fragmentation average = 257.638
	minimum = 0
	maximum = 2398
Injected packet rate average = 0.0156458
	minimum = 0.005 (at node 67)
	maximum = 0.028 (at node 35)
Accepted packet rate average = 0.0121302
	minimum = 0.003 (at node 184)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.281401
	minimum = 0.101 (at node 67)
	maximum = 0.504 (at node 35)
Accepted flit rate average= 0.22638
	minimum = 0.069 (at node 184)
	maximum = 0.361 (at node 59)
Injected packet length average = 17.9857
Accepted packet length average = 18.6625
Total in-flight flits = 39896 (0 measured)
latency change    = 0.416929
throughput change = 0.0928908
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 321.974
	minimum = 26
	maximum = 968
Network latency average = 318.899
	minimum = 25
	maximum = 961
Slowest packet = 9063
Flit latency average = 608.346
	minimum = 6
	maximum = 3551
Slowest flit = 23839
Fragmentation average = 170.783
	minimum = 1
	maximum = 830
Injected packet rate average = 0.016276
	minimum = 0.008 (at node 154)
	maximum = 0.027 (at node 155)
Accepted packet rate average = 0.0127656
	minimum = 0.005 (at node 48)
	maximum = 0.021 (at node 6)
Injected flit rate average = 0.292349
	minimum = 0.144 (at node 154)
	maximum = 0.486 (at node 155)
Accepted flit rate average= 0.230182
	minimum = 0.082 (at node 48)
	maximum = 0.398 (at node 182)
Injected packet length average = 17.9619
Accepted packet length average = 18.0314
Total in-flight flits = 51951 (33889 measured)
latency change    = 0.790612
throughput change = 0.0165177
Class 0:
Packet latency average = 516.639
	minimum = 25
	maximum = 1938
Network latency average = 513.2
	minimum = 24
	maximum = 1938
Slowest packet = 9162
Flit latency average = 708.974
	minimum = 6
	maximum = 4724
Slowest flit = 14850
Fragmentation average = 214.318
	minimum = 0
	maximum = 1275
Injected packet rate average = 0.0159089
	minimum = 0.008 (at node 154)
	maximum = 0.023 (at node 150)
Accepted packet rate average = 0.0127161
	minimum = 0.0065 (at node 86)
	maximum = 0.02 (at node 0)
Injected flit rate average = 0.286482
	minimum = 0.144 (at node 154)
	maximum = 0.414 (at node 150)
Accepted flit rate average= 0.231633
	minimum = 0.1245 (at node 86)
	maximum = 0.3655 (at node 0)
Injected packet length average = 18.0077
Accepted packet length average = 18.2156
Total in-flight flits = 60911 (50229 measured)
latency change    = 0.37679
throughput change = 0.00626216
Class 0:
Packet latency average = 653.646
	minimum = 25
	maximum = 2876
Network latency average = 650.23
	minimum = 24
	maximum = 2875
Slowest packet = 9381
Flit latency average = 793.121
	minimum = 6
	maximum = 5603
Slowest flit = 16073
Fragmentation average = 238.365
	minimum = 0
	maximum = 2384
Injected packet rate average = 0.0158767
	minimum = 0.00966667 (at node 118)
	maximum = 0.0243333 (at node 3)
Accepted packet rate average = 0.0127656
	minimum = 0.007 (at node 86)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.285889
	minimum = 0.172667 (at node 118)
	maximum = 0.438 (at node 3)
Accepted flit rate average= 0.231648
	minimum = 0.134 (at node 64)
	maximum = 0.334667 (at node 128)
Injected packet length average = 18.0068
Accepted packet length average = 18.1462
Total in-flight flits = 71077 (64441 measured)
latency change    = 0.209605
throughput change = 6.37043e-05
Draining remaining packets ...
Class 0:
Remaining flits: 11149 11150 11151 11152 11153 11154 11155 11156 11157 11158 [...] (33519 flits)
Measured flits: 162342 162343 162344 162345 162346 162347 162348 162349 162350 162351 [...] (29699 flits)
Class 0:
Remaining flits: 25940 25941 25942 25943 25944 25945 25946 25947 25948 25949 [...] (4484 flits)
Measured flits: 163181 163182 163183 163184 163185 163186 163187 164304 164305 164306 [...] (4045 flits)
Time taken is 8529 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1250.33 (1 samples)
	minimum = 25 (1 samples)
	maximum = 5320 (1 samples)
Network latency average = 1246.9 (1 samples)
	minimum = 24 (1 samples)
	maximum = 5305 (1 samples)
Flit latency average = 1301.98 (1 samples)
	minimum = 6 (1 samples)
	maximum = 7562 (1 samples)
Fragmentation average = 259.943 (1 samples)
	minimum = 0 (1 samples)
	maximum = 4159 (1 samples)
Injected packet rate average = 0.0158767 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.0243333 (1 samples)
Accepted packet rate average = 0.0127656 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0183333 (1 samples)
Injected flit rate average = 0.285889 (1 samples)
	minimum = 0.172667 (1 samples)
	maximum = 0.438 (1 samples)
Accepted flit rate average = 0.231648 (1 samples)
	minimum = 0.134 (1 samples)
	maximum = 0.334667 (1 samples)
Injected packet size average = 18.0068 (1 samples)
Accepted packet size average = 18.1462 (1 samples)
Hops average = 5.06802 (1 samples)
Total run time 9.78398
