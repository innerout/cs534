BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 216.11
	minimum = 22
	maximum = 629
Network latency average = 210.833
	minimum = 22
	maximum = 614
Slowest packet = 1506
Flit latency average = 186.366
	minimum = 5
	maximum = 600
Slowest flit = 29770
Fragmentation average = 19.3576
	minimum = 0
	maximum = 89
Injected packet rate average = 0.0223802
	minimum = 0.012 (at node 79)
	maximum = 0.038 (at node 11)
Accepted packet rate average = 0.01375
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 48)
Injected flit rate average = 0.399089
	minimum = 0.216 (at node 79)
	maximum = 0.684 (at node 11)
Accepted flit rate average= 0.252078
	minimum = 0.108 (at node 174)
	maximum = 0.432 (at node 48)
Injected packet length average = 17.8322
Accepted packet length average = 18.333
Total in-flight flits = 28947 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 382.004
	minimum = 22
	maximum = 1252
Network latency average = 376.484
	minimum = 22
	maximum = 1212
Slowest packet = 3296
Flit latency average = 352.701
	minimum = 5
	maximum = 1195
Slowest flit = 59345
Fragmentation average = 19.9075
	minimum = 0
	maximum = 102
Injected packet rate average = 0.0222448
	minimum = 0.0145 (at node 105)
	maximum = 0.031 (at node 98)
Accepted packet rate average = 0.0143932
	minimum = 0.0075 (at node 116)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.398753
	minimum = 0.2555 (at node 105)
	maximum = 0.553 (at node 98)
Accepted flit rate average= 0.261813
	minimum = 0.142 (at node 116)
	maximum = 0.367 (at node 44)
Injected packet length average = 17.9257
Accepted packet length average = 18.19
Total in-flight flits = 53220 (0 measured)
latency change    = 0.434273
throughput change = 0.0371807
Class 0:
Packet latency average = 850.701
	minimum = 22
	maximum = 1574
Network latency average = 844.288
	minimum = 22
	maximum = 1574
Slowest packet = 5949
Flit latency average = 822.576
	minimum = 5
	maximum = 1557
Slowest flit = 107099
Fragmentation average = 19.3144
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0219167
	minimum = 0.01 (at node 84)
	maximum = 0.033 (at node 0)
Accepted packet rate average = 0.015026
	minimum = 0.006 (at node 184)
	maximum = 0.03 (at node 34)
Injected flit rate average = 0.394427
	minimum = 0.18 (at node 84)
	maximum = 0.608 (at node 0)
Accepted flit rate average= 0.270573
	minimum = 0.121 (at node 184)
	maximum = 0.54 (at node 34)
Injected packet length average = 17.9967
Accepted packet length average = 18.0069
Total in-flight flits = 77212 (0 measured)
latency change    = 0.550954
throughput change = 0.0323773
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 116.639
	minimum = 22
	maximum = 925
Network latency average = 78.5158
	minimum = 22
	maximum = 925
Slowest packet = 12986
Flit latency average = 1107.67
	minimum = 5
	maximum = 1978
Slowest flit = 142685
Fragmentation average = 5.59177
	minimum = 0
	maximum = 29
Injected packet rate average = 0.0218437
	minimum = 0.007 (at node 184)
	maximum = 0.036 (at node 49)
Accepted packet rate average = 0.0152344
	minimum = 0.006 (at node 36)
	maximum = 0.024 (at node 40)
Injected flit rate average = 0.393542
	minimum = 0.126 (at node 184)
	maximum = 0.648 (at node 49)
Accepted flit rate average= 0.27374
	minimum = 0.1 (at node 36)
	maximum = 0.455 (at node 78)
Injected packet length average = 18.0162
Accepted packet length average = 17.9685
Total in-flight flits = 100362 (69908 measured)
latency change    = 6.29343
throughput change = 0.0115682
Class 0:
Packet latency average = 999.846
	minimum = 22
	maximum = 1997
Network latency average = 970.17
	minimum = 22
	maximum = 1966
Slowest packet = 12854
Flit latency average = 1260.79
	minimum = 5
	maximum = 2564
Slowest flit = 180737
Fragmentation average = 15.3244
	minimum = 0
	maximum = 132
Injected packet rate average = 0.0208177
	minimum = 0.006 (at node 120)
	maximum = 0.0325 (at node 23)
Accepted packet rate average = 0.015224
	minimum = 0.009 (at node 84)
	maximum = 0.023 (at node 129)
Injected flit rate average = 0.374753
	minimum = 0.1165 (at node 120)
	maximum = 0.592 (at node 23)
Accepted flit rate average= 0.27413
	minimum = 0.162 (at node 84)
	maximum = 0.414 (at node 129)
Injected packet length average = 18.0016
Accepted packet length average = 18.0065
Total in-flight flits = 116360 (113371 measured)
latency change    = 0.883343
throughput change = 0.00142496
Class 0:
Packet latency average = 1459.22
	minimum = 22
	maximum = 3101
Network latency average = 1413.05
	minimum = 22
	maximum = 2936
Slowest packet = 12812
Flit latency average = 1407.49
	minimum = 5
	maximum = 3184
Slowest flit = 210833
Fragmentation average = 17.4342
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0202361
	minimum = 0.00966667 (at node 180)
	maximum = 0.03 (at node 155)
Accepted packet rate average = 0.015191
	minimum = 0.00966667 (at node 84)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.364188
	minimum = 0.169 (at node 180)
	maximum = 0.54 (at node 155)
Accepted flit rate average= 0.272979
	minimum = 0.175 (at node 84)
	maximum = 0.38 (at node 129)
Injected packet length average = 17.9969
Accepted packet length average = 17.9698
Total in-flight flits = 130558 (130396 measured)
latency change    = 0.314809
throughput change = 0.00421659
Draining remaining packets ...
Class 0:
Remaining flits: 244897 244898 244899 244900 244901 244902 244903 244904 244905 244906 [...] (81381 flits)
Measured flits: 244897 244898 244899 244900 244901 244902 244903 244904 244905 244906 [...] (81381 flits)
Class 0:
Remaining flits: 283068 283069 283070 283071 283072 283073 283074 283075 283076 283077 [...] (33867 flits)
Measured flits: 283068 283069 283070 283071 283072 283073 283074 283075 283076 283077 [...] (33867 flits)
Class 0:
Remaining flits: 327114 327115 327116 327117 327118 327119 327120 327121 327122 327123 [...] (1402 flits)
Measured flits: 327114 327115 327116 327117 327118 327119 327120 327121 327122 327123 [...] (1402 flits)
Time taken is 9531 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2148.65 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5017 (1 samples)
Network latency average = 2061.14 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4853 (1 samples)
Flit latency average = 1871.68 (1 samples)
	minimum = 5 (1 samples)
	maximum = 4836 (1 samples)
Fragmentation average = 17.9593 (1 samples)
	minimum = 0 (1 samples)
	maximum = 191 (1 samples)
Injected packet rate average = 0.0202361 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.03 (1 samples)
Accepted packet rate average = 0.015191 (1 samples)
	minimum = 0.00966667 (1 samples)
	maximum = 0.021 (1 samples)
Injected flit rate average = 0.364188 (1 samples)
	minimum = 0.169 (1 samples)
	maximum = 0.54 (1 samples)
Accepted flit rate average = 0.272979 (1 samples)
	minimum = 0.175 (1 samples)
	maximum = 0.38 (1 samples)
Injected packet size average = 17.9969 (1 samples)
Accepted packet size average = 17.9698 (1 samples)
Hops average = 5.10753 (1 samples)
Total run time 9.60656
