BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.046687
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 386.986
	minimum = 23
	maximum = 988
Network latency average = 286.389
	minimum = 23
	maximum = 954
Slowest packet = 46
Flit latency average = 212.888
	minimum = 6
	maximum = 962
Slowest flit = 2872
Fragmentation average = 150.835
	minimum = 0
	maximum = 903
Injected packet rate average = 0.0193594
	minimum = 0 (at node 16)
	maximum = 0.044 (at node 19)
Accepted packet rate average = 0.00976042
	minimum = 0.003 (at node 41)
	maximum = 0.019 (at node 44)
Injected flit rate average = 0.342906
	minimum = 0 (at node 16)
	maximum = 0.792 (at node 19)
Accepted flit rate average= 0.20125
	minimum = 0.081 (at node 174)
	maximum = 0.353 (at node 88)
Injected packet length average = 17.7127
Accepted packet length average = 20.619
Total in-flight flits = 28392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 677.608
	minimum = 23
	maximum = 1953
Network latency average = 471.486
	minimum = 23
	maximum = 1870
Slowest packet = 160
Flit latency average = 378.339
	minimum = 6
	maximum = 1928
Slowest flit = 5230
Fragmentation average = 184.888
	minimum = 0
	maximum = 1467
Injected packet rate average = 0.0161068
	minimum = 0 (at node 60)
	maximum = 0.0335 (at node 19)
Accepted packet rate average = 0.0105078
	minimum = 0.0065 (at node 30)
	maximum = 0.016 (at node 22)
Injected flit rate average = 0.286615
	minimum = 0 (at node 60)
	maximum = 0.5985 (at node 19)
Accepted flit rate average= 0.20175
	minimum = 0.1235 (at node 79)
	maximum = 0.301 (at node 22)
Injected packet length average = 17.7947
Accepted packet length average = 19.2
Total in-flight flits = 34146 (0 measured)
latency change    = 0.428894
throughput change = 0.00247831
Class 0:
Packet latency average = 1594.51
	minimum = 40
	maximum = 2925
Network latency average = 889.963
	minimum = 26
	maximum = 2803
Slowest packet = 4559
Flit latency average = 761.665
	minimum = 6
	maximum = 2944
Slowest flit = 4180
Fragmentation average = 223.036
	minimum = 0
	maximum = 2285
Injected packet rate average = 0.0113177
	minimum = 0 (at node 0)
	maximum = 0.033 (at node 122)
Accepted packet rate average = 0.0111562
	minimum = 0.004 (at node 40)
	maximum = 0.019 (at node 6)
Injected flit rate average = 0.203771
	minimum = 0 (at node 67)
	maximum = 0.584 (at node 122)
Accepted flit rate average= 0.198229
	minimum = 0.066 (at node 184)
	maximum = 0.347 (at node 56)
Injected packet length average = 18.0046
Accepted packet length average = 17.7684
Total in-flight flits = 35182 (0 measured)
latency change    = 0.575037
throughput change = 0.0177614
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 2155.59
	minimum = 47
	maximum = 3822
Network latency average = 352.297
	minimum = 27
	maximum = 965
Slowest packet = 8374
Flit latency average = 834.098
	minimum = 6
	maximum = 3696
Slowest flit = 8730
Fragmentation average = 128.303
	minimum = 1
	maximum = 428
Injected packet rate average = 0.0120208
	minimum = 0 (at node 5)
	maximum = 0.029 (at node 109)
Accepted packet rate average = 0.0110781
	minimum = 0.003 (at node 36)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.216063
	minimum = 0 (at node 71)
	maximum = 0.514 (at node 109)
Accepted flit rate average= 0.198328
	minimum = 0.072 (at node 36)
	maximum = 0.398 (at node 16)
Injected packet length average = 17.974
Accepted packet length average = 17.9027
Total in-flight flits = 38827 (28259 measured)
latency change    = 0.260291
throughput change = 0.000498963
Class 0:
Packet latency average = 2608.65
	minimum = 43
	maximum = 4818
Network latency average = 621.06
	minimum = 27
	maximum = 1968
Slowest packet = 8374
Flit latency average = 875.111
	minimum = 6
	maximum = 4630
Slowest flit = 8738
Fragmentation average = 161.006
	minimum = 1
	maximum = 1440
Injected packet rate average = 0.0111042
	minimum = 0.0005 (at node 108)
	maximum = 0.023 (at node 109)
Accepted packet rate average = 0.0108464
	minimum = 0.005 (at node 36)
	maximum = 0.0185 (at node 16)
Injected flit rate average = 0.199424
	minimum = 0.0145 (at node 108)
	maximum = 0.4105 (at node 109)
Accepted flit rate average= 0.195552
	minimum = 0.1005 (at node 36)
	maximum = 0.325 (at node 16)
Injected packet length average = 17.9594
Accepted packet length average = 18.0293
Total in-flight flits = 36878 (34151 measured)
latency change    = 0.173676
throughput change = 0.0141959
Class 0:
Packet latency average = 3068.9
	minimum = 43
	maximum = 5686
Network latency average = 782.775
	minimum = 26
	maximum = 2836
Slowest packet = 8374
Flit latency average = 904.48
	minimum = 6
	maximum = 5317
Slowest flit = 8747
Fragmentation average = 176.941
	minimum = 0
	maximum = 1810
Injected packet rate average = 0.0109774
	minimum = 0.000333333 (at node 108)
	maximum = 0.021 (at node 109)
Accepted packet rate average = 0.0108073
	minimum = 0.00633333 (at node 36)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.197413
	minimum = 0.00966667 (at node 108)
	maximum = 0.373667 (at node 109)
Accepted flit rate average= 0.194637
	minimum = 0.127 (at node 36)
	maximum = 0.303 (at node 16)
Injected packet length average = 17.9836
Accepted packet length average = 18.0098
Total in-flight flits = 37101 (36586 measured)
latency change    = 0.149972
throughput change = 0.0047007
Class 0:
Packet latency average = 3482.5
	minimum = 43
	maximum = 6627
Network latency average = 870.674
	minimum = 25
	maximum = 3853
Slowest packet = 8374
Flit latency average = 911.576
	minimum = 6
	maximum = 5607
Slowest flit = 74536
Fragmentation average = 184.912
	minimum = 0
	maximum = 2460
Injected packet rate average = 0.0108906
	minimum = 0.00025 (at node 108)
	maximum = 0.01825 (at node 95)
Accepted packet rate average = 0.0108333
	minimum = 0.00725 (at node 36)
	maximum = 0.0155 (at node 129)
Injected flit rate average = 0.195975
	minimum = 0.00725 (at node 108)
	maximum = 0.3265 (at node 95)
Accepted flit rate average= 0.195243
	minimum = 0.13075 (at node 36)
	maximum = 0.27875 (at node 128)
Injected packet length average = 17.9949
Accepted packet length average = 18.0225
Total in-flight flits = 35895 (35718 measured)
latency change    = 0.118764
throughput change = 0.00310554
Class 0:
Packet latency average = 3885.95
	minimum = 43
	maximum = 7684
Network latency average = 919.821
	minimum = 25
	maximum = 4766
Slowest packet = 8374
Flit latency average = 914.777
	minimum = 6
	maximum = 7262
Slowest flit = 52037
Fragmentation average = 191.606
	minimum = 0
	maximum = 2460
Injected packet rate average = 0.0109708
	minimum = 0.0016 (at node 108)
	maximum = 0.0186 (at node 125)
Accepted packet rate average = 0.0108604
	minimum = 0.0074 (at node 36)
	maximum = 0.0156 (at node 129)
Injected flit rate average = 0.197416
	minimum = 0.031 (at node 108)
	maximum = 0.3352 (at node 125)
Accepted flit rate average= 0.195744
	minimum = 0.1352 (at node 36)
	maximum = 0.2802 (at node 129)
Injected packet length average = 17.9946
Accepted packet length average = 18.0236
Total in-flight flits = 37006 (36944 measured)
latency change    = 0.103823
throughput change = 0.00255569
Class 0:
Packet latency average = 4265.8
	minimum = 43
	maximum = 8730
Network latency average = 950.38
	minimum = 25
	maximum = 5778
Slowest packet = 8374
Flit latency average = 916.675
	minimum = 6
	maximum = 7262
Slowest flit = 52037
Fragmentation average = 195.855
	minimum = 0
	maximum = 2703
Injected packet rate average = 0.0109097
	minimum = 0.00183333 (at node 108)
	maximum = 0.0183333 (at node 125)
Accepted packet rate average = 0.0108559
	minimum = 0.00766667 (at node 36)
	maximum = 0.0146667 (at node 129)
Injected flit rate average = 0.196256
	minimum = 0.0345 (at node 108)
	maximum = 0.331167 (at node 125)
Accepted flit rate average= 0.19534
	minimum = 0.140167 (at node 36)
	maximum = 0.263333 (at node 129)
Injected packet length average = 17.9891
Accepted packet length average = 17.9939
Total in-flight flits = 36500 (36485 measured)
latency change    = 0.0890464
throughput change = 0.00206548
Class 0:
Packet latency average = 4636.78
	minimum = 43
	maximum = 9453
Network latency average = 973.765
	minimum = 24
	maximum = 5778
Slowest packet = 8374
Flit latency average = 919.355
	minimum = 6
	maximum = 7262
Slowest flit = 52037
Fragmentation average = 199.344
	minimum = 0
	maximum = 2703
Injected packet rate average = 0.0109487
	minimum = 0.00342857 (at node 108)
	maximum = 0.0187143 (at node 147)
Accepted packet rate average = 0.010872
	minimum = 0.00828571 (at node 36)
	maximum = 0.0137143 (at node 66)
Injected flit rate average = 0.197065
	minimum = 0.063 (at node 108)
	maximum = 0.336 (at node 147)
Accepted flit rate average= 0.19574
	minimum = 0.151714 (at node 162)
	maximum = 0.249 (at node 128)
Injected packet length average = 17.999
Accepted packet length average = 18.004
Total in-flight flits = 37140 (37140 measured)
latency change    = 0.0800076
throughput change = 0.00203998
Draining all recorded packets ...
Class 0:
Remaining flits: 230742 230743 230744 230745 230746 230747 230748 230749 230750 230751 [...] (37711 flits)
Measured flits: 230742 230743 230744 230745 230746 230747 230748 230749 230750 230751 [...] (37711 flits)
Class 0:
Remaining flits: 230742 230743 230744 230745 230746 230747 230748 230749 230750 230751 [...] (37422 flits)
Measured flits: 230742 230743 230744 230745 230746 230747 230748 230749 230750 230751 [...] (37422 flits)
Class 0:
Remaining flits: 230758 230759 315464 315465 315466 315467 326667 326668 326669 326670 [...] (36634 flits)
Measured flits: 230758 230759 315464 315465 315466 315467 326667 326668 326669 326670 [...] (36634 flits)
Class 0:
Remaining flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 364662 [...] (36875 flits)
Measured flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 364662 [...] (36875 flits)
Class 0:
Remaining flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 372654 [...] (36842 flits)
Measured flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 372654 [...] (36842 flits)
Class 0:
Remaining flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 411354 [...] (34968 flits)
Measured flits: 351783 351784 351785 351786 351787 351788 351789 351790 351791 411354 [...] (34968 flits)
Class 0:
Remaining flits: 411354 411355 411356 411357 411358 411359 411360 411361 411362 411363 [...] (36167 flits)
Measured flits: 411354 411355 411356 411357 411358 411359 411360 411361 411362 411363 [...] (36167 flits)
Class 0:
Remaining flits: 411371 482796 482797 482798 482799 482800 482801 482802 482803 482804 [...] (35348 flits)
Measured flits: 411371 482796 482797 482798 482799 482800 482801 482802 482803 482804 [...] (35042 flits)
Class 0:
Remaining flits: 482811 482812 482813 487422 487423 487424 487425 487426 487427 487428 [...] (35087 flits)
Measured flits: 482811 482812 482813 487422 487423 487424 487425 487426 487427 487428 [...] (34595 flits)
Class 0:
Remaining flits: 563126 563127 563128 563129 571194 571195 571196 571197 571198 571199 [...] (36859 flits)
Measured flits: 563126 563127 563128 563129 571194 571195 571196 571197 571198 571199 [...] (36067 flits)
Class 0:
Remaining flits: 591858 591859 591860 591861 591862 591863 591864 591865 591866 591867 [...] (36613 flits)
Measured flits: 591858 591859 591860 591861 591862 591863 591864 591865 591866 591867 [...] (35107 flits)
Class 0:
Remaining flits: 612974 612975 612976 612977 612978 612979 612980 612981 612982 612983 [...] (37935 flits)
Measured flits: 612974 612975 612976 612977 612978 612979 612980 612981 612982 612983 [...] (35282 flits)
Class 0:
Remaining flits: 617446 617447 617448 617449 617450 617451 617452 617453 661253 661254 [...] (36567 flits)
Measured flits: 617446 617447 617448 617449 617450 617451 617452 617453 661253 661254 [...] (32448 flits)
Class 0:
Remaining flits: 721008 721009 721010 721011 721012 721013 721014 721015 721016 721017 [...] (36842 flits)
Measured flits: 721008 721009 721010 721011 721012 721013 721014 721015 721016 721017 [...] (31676 flits)
Class 0:
Remaining flits: 721008 721009 721010 721011 721012 721013 721014 721015 721016 721017 [...] (37273 flits)
Measured flits: 721008 721009 721010 721011 721012 721013 721014 721015 721016 721017 [...] (30800 flits)
Class 0:
Remaining flits: 764820 764821 764822 764823 764824 764825 764826 764827 764828 764829 [...] (37273 flits)
Measured flits: 764820 764821 764822 764823 764824 764825 764826 764827 764828 764829 [...] (30255 flits)
Class 0:
Remaining flits: 774000 774001 774002 774003 774004 774005 774006 774007 774008 774009 [...] (36383 flits)
Measured flits: 774000 774001 774002 774003 774004 774005 774006 774007 774008 774009 [...] (27083 flits)
Class 0:
Remaining flits: 791676 791677 791678 791679 791680 791681 791682 791683 791684 791685 [...] (34166 flits)
Measured flits: 791676 791677 791678 791679 791680 791681 791682 791683 791684 791685 [...] (24272 flits)
Class 0:
Remaining flits: 791676 791677 791678 791679 791680 791681 791682 791683 791684 791685 [...] (36075 flits)
Measured flits: 791676 791677 791678 791679 791680 791681 791682 791683 791684 791685 [...] (24073 flits)
Class 0:
Remaining flits: 791688 791689 791690 791691 791692 791693 840024 840025 840026 840027 [...] (35606 flits)
Measured flits: 791688 791689 791690 791691 791692 791693 840024 840025 840026 840027 [...] (22047 flits)
Class 0:
Remaining flits: 840024 840025 840026 840027 840028 840029 840030 840031 840032 840033 [...] (35236 flits)
Measured flits: 840024 840025 840026 840027 840028 840029 840030 840031 840032 840033 [...] (19968 flits)
Class 0:
Remaining flits: 840600 840601 840602 840603 840604 840605 840606 840607 840608 840609 [...] (37203 flits)
Measured flits: 840600 840601 840602 840603 840604 840605 840606 840607 840608 840609 [...] (19393 flits)
Class 0:
Remaining flits: 840600 840601 840602 840603 840604 840605 840606 840607 840608 840609 [...] (37080 flits)
Measured flits: 840600 840601 840602 840603 840604 840605 840606 840607 840608 840609 [...] (17672 flits)
Class 0:
Remaining flits: 840617 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 [...] (36023 flits)
Measured flits: 840617 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 [...] (14692 flits)
Class 0:
Remaining flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (36044 flits)
Measured flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (13688 flits)
Class 0:
Remaining flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (36245 flits)
Measured flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (11899 flits)
Class 0:
Remaining flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (36020 flits)
Measured flits: 1004796 1004797 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 [...] (10531 flits)
Class 0:
Remaining flits: 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 1004806 1004807 [...] (34336 flits)
Measured flits: 1004798 1004799 1004800 1004801 1004802 1004803 1004804 1004805 1004806 1004807 [...] (8661 flits)
Class 0:
Remaining flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (35748 flits)
Measured flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (7921 flits)
Class 0:
Remaining flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (36852 flits)
Measured flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (7038 flits)
Class 0:
Remaining flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (37046 flits)
Measured flits: 1037520 1037521 1037522 1037523 1037524 1037525 1037526 1037527 1037528 1037529 [...] (6416 flits)
Class 0:
Remaining flits: 1226080 1226081 1226082 1226083 1226084 1226085 1226086 1226087 1228349 1228350 [...] (37313 flits)
Measured flits: 1265850 1265851 1265852 1265853 1265854 1265855 1265856 1265857 1265858 1265859 [...] (5193 flits)
Class 0:
Remaining flits: 1265850 1265851 1265852 1265853 1265854 1265855 1265856 1265857 1265858 1265859 [...] (36442 flits)
Measured flits: 1265850 1265851 1265852 1265853 1265854 1265855 1265856 1265857 1265858 1265859 [...] (4275 flits)
Class 0:
Remaining flits: 1265852 1265853 1265854 1265855 1265856 1265857 1265858 1265859 1265860 1265861 [...] (36000 flits)
Measured flits: 1265852 1265853 1265854 1265855 1265856 1265857 1265858 1265859 1265860 1265861 [...] (4016 flits)
Class 0:
Remaining flits: 1265855 1265856 1265857 1265858 1265859 1265860 1265861 1265862 1265863 1265864 [...] (35104 flits)
Measured flits: 1265855 1265856 1265857 1265858 1265859 1265860 1265861 1265862 1265863 1265864 [...] (3254 flits)
Class 0:
Remaining flits: 1340820 1340821 1340822 1340823 1340824 1340825 1340826 1340827 1340828 1340829 [...] (36539 flits)
Measured flits: 1340820 1340821 1340822 1340823 1340824 1340825 1340826 1340827 1340828 1340829 [...] (2838 flits)
Class 0:
Remaining flits: 1450497 1450498 1450499 1450500 1450501 1450502 1450503 1450504 1450505 1450506 [...] (36073 flits)
Measured flits: 1519422 1519423 1519424 1519425 1519426 1519427 1519428 1519429 1519430 1519431 [...] (2408 flits)
Class 0:
Remaining flits: 1520837 1529316 1529317 1529318 1529319 1529320 1529321 1529322 1529323 1529324 [...] (36668 flits)
Measured flits: 1582066 1582067 1582068 1582069 1582070 1582071 1582072 1582073 1587582 1587583 [...] (2153 flits)
Class 0:
Remaining flits: 1529316 1529317 1529318 1529319 1529320 1529321 1529322 1529323 1529324 1529325 [...] (36236 flits)
Measured flits: 1587582 1587583 1587584 1587585 1587586 1587587 1587588 1587589 1587590 1587591 [...] (1998 flits)
Class 0:
Remaining flits: 1587778 1587779 1588878 1588879 1588880 1588881 1588882 1588883 1588884 1588885 [...] (37618 flits)
Measured flits: 1646658 1646659 1646660 1646661 1646662 1646663 1646664 1646665 1646666 1646667 [...] (1692 flits)
Class 0:
Remaining flits: 1587778 1587779 1646658 1646659 1646660 1646661 1646662 1646663 1646664 1646665 [...] (35535 flits)
Measured flits: 1646658 1646659 1646660 1646661 1646662 1646663 1646664 1646665 1646666 1646667 [...] (1619 flits)
Class 0:
Remaining flits: 1688997 1688998 1688999 1689000 1689001 1689002 1689003 1689004 1689005 1689006 [...] (37698 flits)
Measured flits: 1776292 1776293 1802466 1802467 1802468 1802469 1802470 1802471 1802472 1802473 [...] (1414 flits)
Class 0:
Remaining flits: 1689030 1689031 1689032 1689033 1689034 1689035 1689036 1689037 1689038 1689039 [...] (36398 flits)
Measured flits: 1802466 1802467 1802468 1802469 1802470 1802471 1802472 1802473 1802474 1802475 [...] (1158 flits)
Class 0:
Remaining flits: 1689030 1689031 1689032 1689033 1689034 1689035 1689036 1689037 1689038 1689039 [...] (37273 flits)
Measured flits: 1891048 1891049 1891050 1891051 1891052 1891053 1891054 1891055 1891056 1891057 [...] (1174 flits)
Class 0:
Remaining flits: 1689030 1689031 1689032 1689033 1689034 1689035 1689036 1689037 1689038 1689039 [...] (37451 flits)
Measured flits: 1956070 1956071 1956072 1956073 1956074 1956075 1956076 1956077 1988226 1988227 [...] (868 flits)
Class 0:
Remaining flits: 1778202 1778203 1778204 1778205 1778206 1778207 1778208 1778209 1778210 1778211 [...] (37555 flits)
Measured flits: 1988234 1988235 1988236 1988237 1988238 1988239 1988240 1988241 1988242 1988243 [...] (757 flits)
Class 0:
Remaining flits: 1794510 1794511 1794512 1794513 1794514 1794515 1794516 1794517 1794518 1794519 [...] (36634 flits)
Measured flits: 2011068 2011069 2011070 2011071 2011072 2011073 2011074 2011075 2011076 2011077 [...] (674 flits)
Class 0:
Remaining flits: 1794510 1794511 1794512 1794513 1794514 1794515 1794516 1794517 1794518 1794519 [...] (34810 flits)
Measured flits: 2054105 2061817 2061818 2061819 2061820 2061821 2061822 2061823 2061824 2061825 [...] (382 flits)
Class 0:
Remaining flits: 1900126 1900127 1900128 1900129 1900130 1900131 1900132 1900133 1938180 1938181 [...] (34607 flits)
Measured flits: 2104017 2104018 2104019 2155140 2155141 2155142 2155143 2155144 2155145 2155146 [...] (325 flits)
Class 0:
Remaining flits: 1998234 1998235 1998236 1998237 1998238 1998239 1998240 1998241 1998242 1998243 [...] (37244 flits)
Measured flits: 2176668 2176669 2176670 2176671 2176672 2176673 2176674 2176675 2176676 2176677 [...] (182 flits)
Class 0:
Remaining flits: 1998234 1998235 1998236 1998237 1998238 1998239 1998240 1998241 1998242 1998243 [...] (37082 flits)
Measured flits: 2176668 2176669 2176670 2176671 2176672 2176673 2176674 2176675 2176676 2176677 [...] (147 flits)
Class 0:
Remaining flits: 2053998 2053999 2054000 2054001 2054002 2054003 2054004 2054005 2054006 2054007 [...] (37364 flits)
Measured flits: 2176668 2176669 2176670 2176671 2176672 2176673 2176674 2176675 2176676 2176677 [...] (108 flits)
Class 0:
Remaining flits: 2054011 2054012 2054013 2054014 2054015 2097012 2097013 2097014 2097015 2097016 [...] (37448 flits)
Measured flits: 2176685 2248091 2285748 2285749 2285750 2285751 2285752 2285753 2285754 2285755 [...] (83 flits)
Class 0:
Remaining flits: 2152926 2152927 2152928 2152929 2152930 2152931 2152932 2152933 2152934 2152935 [...] (35000 flits)
Measured flits: 2342664 2342665 2342666 2342667 2342668 2342669 2342670 2342671 2342672 2342673 [...] (72 flits)
Class 0:
Remaining flits: 2152926 2152927 2152928 2152929 2152930 2152931 2152932 2152933 2152934 2152935 [...] (36198 flits)
Measured flits: 2342664 2342665 2342666 2342667 2342668 2342669 2342670 2342671 2342672 2342673 [...] (115 flits)
Class 0:
Remaining flits: 2152926 2152927 2152928 2152929 2152930 2152931 2152932 2152933 2152934 2152935 [...] (36873 flits)
Measured flits: 2342678 2342679 2342680 2342681 2389104 2389105 2389106 2389107 2389108 2389109 [...] (108 flits)
Class 0:
Remaining flits: 2217096 2217097 2217098 2217099 2217100 2217101 2217102 2217103 2217104 2217105 [...] (35285 flits)
Measured flits: 2389104 2389105 2389106 2389107 2389108 2389109 2389110 2389111 2389112 2389113 [...] (116 flits)
Class 0:
Remaining flits: 2217096 2217097 2217098 2217099 2217100 2217101 2217102 2217103 2217104 2217105 [...] (36434 flits)
Measured flits: 2389104 2389105 2389106 2389107 2389108 2389109 2389110 2389111 2389112 2389113 [...] (132 flits)
Class 0:
Remaining flits: 2257308 2257309 2257310 2257311 2257312 2257313 2257314 2257315 2257316 2257317 [...] (36906 flits)
Measured flits: 2389104 2389105 2389106 2389107 2389108 2389109 2389110 2389111 2389112 2389113 [...] (160 flits)
Class 0:
Remaining flits: 2257308 2257309 2257310 2257311 2257312 2257313 2257314 2257315 2257316 2257317 [...] (36115 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (107 flits)
Class 0:
Remaining flits: 2264672 2264673 2264674 2264675 2264676 2264677 2264678 2264679 2264680 2264681 [...] (38249 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2280975 2280976 2280977 2449872 2449873 2449874 2449875 2449876 2449877 2449878 [...] (36024 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2449872 2449873 2449874 2449875 2449876 2449877 2449878 2449879 2449880 2449881 [...] (34406 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2449872 2449873 2449874 2449875 2449876 2449877 2449878 2449879 2449880 2449881 [...] (36283 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2517570 2517571 2517572 2517573 2517574 2517575 2517576 2517577 2517578 2517579 [...] (35701 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2517575 2517576 2517577 2517578 2517579 2517580 2517581 2517582 2517583 2517584 [...] (35142 flits)
Measured flits: 2524014 2524015 2524016 2524017 2524018 2524019 2524020 2524021 2524022 2524023 [...] (54 flits)
Class 0:
Remaining flits: 2527542 2527543 2527544 2527545 2527546 2527547 2527548 2527549 2527550 2527551 [...] (37400 flits)
Measured flits: 2568402 2568403 2568404 2568405 2568406 2568407 2568408 2568409 2568410 2568411 [...] (36 flits)
Class 0:
Remaining flits: 2527542 2527543 2527544 2527545 2527546 2527547 2527548 2527549 2527550 2527551 [...] (36501 flits)
Measured flits: 2568402 2568403 2568404 2568405 2568406 2568407 2568408 2568409 2568410 2568411 [...] (36 flits)
Class 0:
Remaining flits: 2527542 2527543 2527544 2527545 2527546 2527547 2527548 2527549 2527550 2527551 [...] (36859 flits)
Measured flits: 2568402 2568403 2568404 2568405 2568406 2568407 2568408 2568409 2568410 2568411 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 2684754 2684755 2684756 2684757 2684758 2684759 2684760 2684761 2684762 2684763 [...] (6821 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2776626 2776627 2776628 2776629 2776630 2776631 2776632 2776633 2776634 2776635 [...] (714 flits)
Measured flits: (0 flits)
Time taken is 82461 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 14913.4 (1 samples)
	minimum = 43 (1 samples)
	maximum = 70192 (1 samples)
Network latency average = 1070.78 (1 samples)
	minimum = 23 (1 samples)
	maximum = 14929 (1 samples)
Flit latency average = 950.664 (1 samples)
	minimum = 6 (1 samples)
	maximum = 14891 (1 samples)
Fragmentation average = 206.769 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5563 (1 samples)
Injected packet rate average = 0.0109487 (1 samples)
	minimum = 0.00342857 (1 samples)
	maximum = 0.0187143 (1 samples)
Accepted packet rate average = 0.010872 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0137143 (1 samples)
Injected flit rate average = 0.197065 (1 samples)
	minimum = 0.063 (1 samples)
	maximum = 0.336 (1 samples)
Accepted flit rate average = 0.19574 (1 samples)
	minimum = 0.151714 (1 samples)
	maximum = 0.249 (1 samples)
Injected packet size average = 17.999 (1 samples)
Accepted packet size average = 18.004 (1 samples)
Hops average = 5.05498 (1 samples)
Total run time 92.5003
