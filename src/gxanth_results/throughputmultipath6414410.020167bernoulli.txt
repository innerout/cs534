BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 258
	minimum = 23
	maximum = 904
Network latency average = 253.733
	minimum = 23
	maximum = 873
Slowest packet = 431
Flit latency average = 178.983
	minimum = 6
	maximum = 927
Slowest flit = 4131
Fragmentation average = 152.856
	minimum = 0
	maximum = 773
Injected packet rate average = 0.0201406
	minimum = 0.007 (at node 187)
	maximum = 0.032 (at node 17)
Accepted packet rate average = 0.00961458
	minimum = 0.002 (at node 127)
	maximum = 0.017 (at node 49)
Injected flit rate average = 0.358974
	minimum = 0.126 (at node 187)
	maximum = 0.576 (at node 17)
Accepted flit rate average= 0.204323
	minimum = 0.072 (at node 174)
	maximum = 0.354 (at node 167)
Injected packet length average = 17.8234
Accepted packet length average = 21.2514
Total in-flight flits = 30376 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 436.456
	minimum = 23
	maximum = 1744
Network latency average = 431.68
	minimum = 23
	maximum = 1744
Slowest packet = 643
Flit latency average = 330.434
	minimum = 6
	maximum = 1892
Slowest flit = 5305
Fragmentation average = 219.947
	minimum = 0
	maximum = 1647
Injected packet rate average = 0.0199948
	minimum = 0.0105 (at node 112)
	maximum = 0.033 (at node 17)
Accepted packet rate average = 0.0110469
	minimum = 0.006 (at node 81)
	maximum = 0.017 (at node 98)
Injected flit rate average = 0.358484
	minimum = 0.1855 (at node 112)
	maximum = 0.594 (at node 17)
Accepted flit rate average= 0.217799
	minimum = 0.115 (at node 81)
	maximum = 0.3285 (at node 152)
Injected packet length average = 17.9289
Accepted packet length average = 19.7159
Total in-flight flits = 54569 (0 measured)
latency change    = 0.408876
throughput change = 0.061876
Class 0:
Packet latency average = 865.356
	minimum = 25
	maximum = 2757
Network latency average = 860.763
	minimum = 25
	maximum = 2752
Slowest packet = 823
Flit latency average = 741.868
	minimum = 6
	maximum = 2802
Slowest flit = 8699
Fragmentation average = 311.45
	minimum = 0
	maximum = 2456
Injected packet rate average = 0.0201615
	minimum = 0.007 (at node 172)
	maximum = 0.032 (at node 173)
Accepted packet rate average = 0.0124792
	minimum = 0.006 (at node 138)
	maximum = 0.024 (at node 145)
Injected flit rate average = 0.362526
	minimum = 0.126 (at node 172)
	maximum = 0.584 (at node 173)
Accepted flit rate average= 0.231802
	minimum = 0.097 (at node 31)
	maximum = 0.384 (at node 16)
Injected packet length average = 17.9811
Accepted packet length average = 18.5751
Total in-flight flits = 79741 (0 measured)
latency change    = 0.495633
throughput change = 0.0604076
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 268.481
	minimum = 24
	maximum = 966
Network latency average = 263.556
	minimum = 24
	maximum = 960
Slowest packet = 11575
Flit latency average = 1031.25
	minimum = 6
	maximum = 3830
Slowest flit = 8705
Fragmentation average = 134.217
	minimum = 1
	maximum = 633
Injected packet rate average = 0.020724
	minimum = 0.012 (at node 2)
	maximum = 0.031 (at node 48)
Accepted packet rate average = 0.0130677
	minimum = 0.003 (at node 41)
	maximum = 0.024 (at node 78)
Injected flit rate average = 0.37224
	minimum = 0.209 (at node 5)
	maximum = 0.564 (at node 48)
Accepted flit rate average= 0.236807
	minimum = 0.103 (at node 158)
	maximum = 0.427 (at node 182)
Injected packet length average = 17.9618
Accepted packet length average = 18.1216
Total in-flight flits = 105896 (58957 measured)
latency change    = 2.22316
throughput change = 0.0211362
Class 0:
Packet latency average = 608.682
	minimum = 24
	maximum = 1970
Network latency average = 603.714
	minimum = 24
	maximum = 1970
Slowest packet = 11588
Flit latency average = 1187.86
	minimum = 6
	maximum = 4698
Slowest flit = 17027
Fragmentation average = 225.246
	minimum = 1
	maximum = 1751
Injected packet rate average = 0.020237
	minimum = 0.0125 (at node 116)
	maximum = 0.027 (at node 157)
Accepted packet rate average = 0.0130729
	minimum = 0.007 (at node 139)
	maximum = 0.0225 (at node 0)
Injected flit rate average = 0.364182
	minimum = 0.225 (at node 116)
	maximum = 0.4895 (at node 157)
Accepted flit rate average= 0.237984
	minimum = 0.132 (at node 41)
	maximum = 0.427 (at node 0)
Injected packet length average = 17.9959
Accepted packet length average = 18.2044
Total in-flight flits = 128233 (100900 measured)
latency change    = 0.558914
throughput change = 0.00494605
Class 0:
Packet latency average = 926.662
	minimum = 24
	maximum = 2970
Network latency average = 921.051
	minimum = 24
	maximum = 2970
Slowest packet = 11573
Flit latency average = 1342.06
	minimum = 6
	maximum = 5660
Slowest flit = 22601
Fragmentation average = 264.818
	minimum = 0
	maximum = 2132
Injected packet rate average = 0.0202205
	minimum = 0.0133333 (at node 116)
	maximum = 0.027 (at node 57)
Accepted packet rate average = 0.0130729
	minimum = 0.00766667 (at node 139)
	maximum = 0.019 (at node 0)
Injected flit rate average = 0.364168
	minimum = 0.24 (at node 116)
	maximum = 0.487 (at node 57)
Accepted flit rate average= 0.237028
	minimum = 0.139333 (at node 139)
	maximum = 0.343 (at node 0)
Injected packet length average = 18.0099
Accepted packet length average = 18.1312
Total in-flight flits = 152859 (136369 measured)
latency change    = 0.343146
throughput change = 0.0040358
Draining remaining packets ...
Class 0:
Remaining flits: 12899 12900 12901 12902 12903 12904 12905 24048 24049 24050 [...] (113634 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (102832 flits)
Class 0:
Remaining flits: 12899 12900 12901 12902 12903 12904 12905 24048 24049 24050 [...] (75629 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (68614 flits)
Class 0:
Remaining flits: 12899 12900 12901 12902 12903 12904 12905 24048 24049 24050 [...] (40360 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (36423 flits)
Class 0:
Remaining flits: 50382 50383 50384 50385 50386 50387 50388 50389 50390 50391 [...] (10932 flits)
Measured flits: 207972 207973 207974 207975 207976 207977 207978 207979 207980 207981 [...] (9615 flits)
Time taken is 10978 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 2615.77 (1 samples)
	minimum = 24 (1 samples)
	maximum = 7658 (1 samples)
Network latency average = 2608.52 (1 samples)
	minimum = 24 (1 samples)
	maximum = 7658 (1 samples)
Flit latency average = 2507.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9608 (1 samples)
Fragmentation average = 301.089 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5708 (1 samples)
Injected packet rate average = 0.0202205 (1 samples)
	minimum = 0.0133333 (1 samples)
	maximum = 0.027 (1 samples)
Accepted packet rate average = 0.0130729 (1 samples)
	minimum = 0.00766667 (1 samples)
	maximum = 0.019 (1 samples)
Injected flit rate average = 0.364168 (1 samples)
	minimum = 0.24 (1 samples)
	maximum = 0.487 (1 samples)
Accepted flit rate average = 0.237028 (1 samples)
	minimum = 0.139333 (1 samples)
	maximum = 0.343 (1 samples)
Injected packet size average = 18.0099 (1 samples)
Accepted packet size average = 18.1312 (1 samples)
Hops average = 5.0838 (1 samples)
Total run time 14.8256
