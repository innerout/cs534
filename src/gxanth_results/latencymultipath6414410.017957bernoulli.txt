BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 243.877
	minimum = 23
	maximum = 849
Network latency average = 240.282
	minimum = 23
	maximum = 849
Slowest packet = 445
Flit latency average = 163.326
	minimum = 6
	maximum = 920
Slowest flit = 3951
Fragmentation average = 149.353
	minimum = 0
	maximum = 761
Injected packet rate average = 0.0179583
	minimum = 0.008 (at node 46)
	maximum = 0.029 (at node 9)
Accepted packet rate average = 0.00943229
	minimum = 0.001 (at node 41)
	maximum = 0.02 (at node 44)
Injected flit rate average = 0.319563
	minimum = 0.132 (at node 109)
	maximum = 0.522 (at node 9)
Accepted flit rate average= 0.195776
	minimum = 0.054 (at node 41)
	maximum = 0.366 (at node 44)
Injected packet length average = 17.7947
Accepted packet length average = 20.7559
Total in-flight flits = 24475 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 396.31
	minimum = 23
	maximum = 1791
Network latency average = 392.45
	minimum = 23
	maximum = 1747
Slowest packet = 641
Flit latency average = 292.294
	minimum = 6
	maximum = 1918
Slowest flit = 3634
Fragmentation average = 205.093
	minimum = 0
	maximum = 1628
Injected packet rate average = 0.0177526
	minimum = 0.0115 (at node 7)
	maximum = 0.0255 (at node 85)
Accepted packet rate average = 0.0106823
	minimum = 0.0055 (at node 164)
	maximum = 0.0165 (at node 152)
Injected flit rate average = 0.318164
	minimum = 0.207 (at node 7)
	maximum = 0.459 (at node 85)
Accepted flit rate average= 0.21057
	minimum = 0.1225 (at node 153)
	maximum = 0.323 (at node 152)
Injected packet length average = 17.9221
Accepted packet length average = 19.7121
Total in-flight flits = 41847 (0 measured)
latency change    = 0.384631
throughput change = 0.0702581
Class 0:
Packet latency average = 747.552
	minimum = 25
	maximum = 2738
Network latency average = 743.684
	minimum = 25
	maximum = 2738
Slowest packet = 680
Flit latency average = 627.711
	minimum = 6
	maximum = 2813
Slowest flit = 10512
Fragmentation average = 301.299
	minimum = 1
	maximum = 2481
Injected packet rate average = 0.0178802
	minimum = 0.009 (at node 173)
	maximum = 0.031 (at node 187)
Accepted packet rate average = 0.0126875
	minimum = 0.003 (at node 96)
	maximum = 0.022 (at node 16)
Injected flit rate average = 0.321677
	minimum = 0.162 (at node 173)
	maximum = 0.569 (at node 187)
Accepted flit rate average= 0.233734
	minimum = 0.088 (at node 96)
	maximum = 0.399 (at node 33)
Injected packet length average = 17.9907
Accepted packet length average = 18.4224
Total in-flight flits = 58764 (0 measured)
latency change    = 0.469857
throughput change = 0.0991042
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 328.122
	minimum = 25
	maximum = 988
Network latency average = 324.194
	minimum = 25
	maximum = 977
Slowest packet = 10309
Flit latency average = 858.594
	minimum = 6
	maximum = 3667
Slowest flit = 8045
Fragmentation average = 170.384
	minimum = 1
	maximum = 723
Injected packet rate average = 0.0185156
	minimum = 0.009 (at node 46)
	maximum = 0.029 (at node 43)
Accepted packet rate average = 0.0128125
	minimum = 0.006 (at node 36)
	maximum = 0.024 (at node 129)
Injected flit rate average = 0.332589
	minimum = 0.147 (at node 46)
	maximum = 0.522 (at node 43)
Accepted flit rate average= 0.233745
	minimum = 0.087 (at node 43)
	maximum = 0.469 (at node 129)
Injected packet length average = 17.9626
Accepted packet length average = 18.2435
Total in-flight flits = 77875 (48335 measured)
latency change    = 1.27827
throughput change = 4.45643e-05
Class 0:
Packet latency average = 626.564
	minimum = 23
	maximum = 1947
Network latency average = 622.489
	minimum = 23
	maximum = 1934
Slowest packet = 10323
Flit latency average = 978.71
	minimum = 6
	maximum = 4793
Slowest flit = 11037
Fragmentation average = 241.239
	minimum = 0
	maximum = 1360
Injected packet rate average = 0.0181406
	minimum = 0.01 (at node 23)
	maximum = 0.025 (at node 106)
Accepted packet rate average = 0.012849
	minimum = 0.0055 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.326781
	minimum = 0.1865 (at node 23)
	maximum = 0.449 (at node 106)
Accepted flit rate average= 0.233305
	minimum = 0.104 (at node 36)
	maximum = 0.377 (at node 129)
Injected packet length average = 18.0138
Accepted packet length average = 18.1575
Total in-flight flits = 94563 (78023 measured)
latency change    = 0.476315
throughput change = 0.00188639
Class 0:
Packet latency average = 863.402
	minimum = 23
	maximum = 2961
Network latency average = 859.297
	minimum = 23
	maximum = 2961
Slowest packet = 10298
Flit latency average = 1097.24
	minimum = 6
	maximum = 5443
Slowest flit = 16963
Fragmentation average = 269.928
	minimum = 0
	maximum = 2117
Injected packet rate average = 0.0181389
	minimum = 0.0103333 (at node 67)
	maximum = 0.026 (at node 104)
Accepted packet rate average = 0.0128316
	minimum = 0.007 (at node 36)
	maximum = 0.0176667 (at node 66)
Injected flit rate average = 0.326543
	minimum = 0.186 (at node 67)
	maximum = 0.463 (at node 104)
Accepted flit rate average= 0.232441
	minimum = 0.137667 (at node 36)
	maximum = 0.315 (at node 129)
Injected packet length average = 18.0024
Accepted packet length average = 18.1147
Total in-flight flits = 112942 (102708 measured)
latency change    = 0.274308
throughput change = 0.00371585
Class 0:
Packet latency average = 1076
	minimum = 23
	maximum = 3897
Network latency average = 1071.89
	minimum = 23
	maximum = 3897
Slowest packet = 10469
Flit latency average = 1224.16
	minimum = 6
	maximum = 6590
Slowest flit = 15659
Fragmentation average = 288.029
	minimum = 0
	maximum = 3302
Injected packet rate average = 0.0181224
	minimum = 0.012 (at node 67)
	maximum = 0.02375 (at node 31)
Accepted packet rate average = 0.0127708
	minimum = 0.0085 (at node 15)
	maximum = 0.01775 (at node 66)
Injected flit rate average = 0.326266
	minimum = 0.216 (at node 67)
	maximum = 0.4275 (at node 31)
Accepted flit rate average= 0.231624
	minimum = 0.153 (at node 15)
	maximum = 0.31425 (at node 178)
Injected packet length average = 18.0034
Accepted packet length average = 18.1369
Total in-flight flits = 131401 (124694 measured)
latency change    = 0.197581
throughput change = 0.00352846
Class 0:
Packet latency average = 1267.19
	minimum = 23
	maximum = 4871
Network latency average = 1262.92
	minimum = 23
	maximum = 4861
Slowest packet = 10417
Flit latency average = 1351.16
	minimum = 6
	maximum = 7467
Slowest flit = 18141
Fragmentation average = 302.046
	minimum = 0
	maximum = 3302
Injected packet rate average = 0.0180542
	minimum = 0.0132 (at node 23)
	maximum = 0.023 (at node 87)
Accepted packet rate average = 0.012774
	minimum = 0.0098 (at node 80)
	maximum = 0.0168 (at node 138)
Injected flit rate average = 0.325004
	minimum = 0.2402 (at node 23)
	maximum = 0.416 (at node 87)
Accepted flit rate average= 0.231436
	minimum = 0.1702 (at node 84)
	maximum = 0.3168 (at node 138)
Injected packet length average = 18.0016
Accepted packet length average = 18.1178
Total in-flight flits = 148561 (144060 measured)
latency change    = 0.150875
throughput change = 0.000809032
Class 0:
Packet latency average = 1449.23
	minimum = 23
	maximum = 5971
Network latency average = 1444.68
	minimum = 23
	maximum = 5971
Slowest packet = 10300
Flit latency average = 1474.89
	minimum = 6
	maximum = 8653
Slowest flit = 18732
Fragmentation average = 315.117
	minimum = 0
	maximum = 3873
Injected packet rate average = 0.0180217
	minimum = 0.0131667 (at node 115)
	maximum = 0.0228333 (at node 167)
Accepted packet rate average = 0.0127873
	minimum = 0.01 (at node 2)
	maximum = 0.017 (at node 138)
Injected flit rate average = 0.324337
	minimum = 0.237 (at node 115)
	maximum = 0.411 (at node 167)
Accepted flit rate average= 0.231017
	minimum = 0.178167 (at node 88)
	maximum = 0.317167 (at node 138)
Injected packet length average = 17.997
Accepted packet length average = 18.0661
Total in-flight flits = 166330 (163124 measured)
latency change    = 0.125617
throughput change = 0.00181414
Class 0:
Packet latency average = 1608.48
	minimum = 23
	maximum = 6872
Network latency average = 1602.9
	minimum = 23
	maximum = 6872
Slowest packet = 10544
Flit latency average = 1598.3
	minimum = 6
	maximum = 9345
Slowest flit = 35877
Fragmentation average = 325.971
	minimum = 0
	maximum = 5176
Injected packet rate average = 0.0179754
	minimum = 0.0138571 (at node 121)
	maximum = 0.023 (at node 87)
Accepted packet rate average = 0.0127567
	minimum = 0.00985714 (at node 2)
	maximum = 0.0181429 (at node 138)
Injected flit rate average = 0.323577
	minimum = 0.251 (at node 121)
	maximum = 0.413429 (at node 87)
Accepted flit rate average= 0.230134
	minimum = 0.179286 (at node 148)
	maximum = 0.329 (at node 138)
Injected packet length average = 18.0011
Accepted packet length average = 18.0402
Total in-flight flits = 184326 (182265 measured)
latency change    = 0.0990063
throughput change = 0.00383878
Draining all recorded packets ...
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (204400 flits)
Measured flits: 184600 184601 184602 184603 184604 184605 184606 184607 184861 184862 [...] (148937 flits)
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (221850 flits)
Measured flits: 184600 184601 184602 184603 184604 184605 184606 184607 184861 184862 [...] (119328 flits)
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (241221 flits)
Measured flits: 184600 184601 184602 184603 184604 184605 184606 184607 184861 184862 [...] (96038 flits)
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (260123 flits)
Measured flits: 184600 184601 184602 184603 184604 184605 184606 184607 185241 185242 [...] (77989 flits)
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (279630 flits)
Measured flits: 185241 185242 185243 185244 185245 185246 185247 185248 185249 185250 [...] (63610 flits)
Class 0:
Remaining flits: 35878 35879 35880 35881 35882 35883 35884 35885 35886 35887 [...] (301507 flits)
Measured flits: 185241 185242 185243 185244 185245 185246 185247 185248 185249 185250 [...] (52451 flits)
Class 0:
Remaining flits: 35886 35887 35888 35889 35890 35891 93996 93997 93998 93999 [...] (319419 flits)
Measured flits: 186426 186427 186428 186429 186430 186431 186432 186433 186434 186435 [...] (43578 flits)
Class 0:
Remaining flits: 93996 93997 93998 93999 94000 94001 94002 94003 94004 94005 [...] (339407 flits)
Measured flits: 186426 186427 186428 186429 186430 186431 186432 186433 186434 186435 [...] (36420 flits)
Class 0:
Remaining flits: 93996 93997 93998 93999 94000 94001 94002 94003 94004 94005 [...] (363872 flits)
Measured flits: 187146 187147 187148 187149 187150 187151 187152 187153 187154 187155 [...] (30780 flits)
Class 0:
Remaining flits: 93996 93997 93998 93999 94000 94001 94002 94003 94004 94005 [...] (385431 flits)
Measured flits: 187146 187147 187148 187149 187150 187151 187152 187153 187154 187155 [...] (26272 flits)
Class 0:
Remaining flits: 148338 148339 148340 148341 148342 148343 148344 148345 148346 148347 [...] (404480 flits)
Measured flits: 188550 188551 188552 188553 188554 188555 188556 188557 188558 188559 [...] (22390 flits)
Class 0:
Remaining flits: 148338 148339 148340 148341 148342 148343 148344 148345 148346 148347 [...] (424462 flits)
Measured flits: 188550 188551 188552 188553 188554 188555 188556 188557 188558 188559 [...] (18962 flits)
Class 0:
Remaining flits: 148340 148341 148342 148343 148344 148345 148346 148347 148348 148349 [...] (442961 flits)
Measured flits: 188550 188551 188552 188553 188554 188555 188556 188557 188558 188559 [...] (15463 flits)
Class 0:
Remaining flits: 171648 171649 171650 171651 171652 171653 171654 171655 171656 171657 [...] (459650 flits)
Measured flits: 188550 188551 188552 188553 188554 188555 188556 188557 188558 188559 [...] (13128 flits)
Class 0:
Remaining flits: 171648 171649 171650 171651 171652 171653 171654 171655 171656 171657 [...] (476088 flits)
Measured flits: 202209 202210 202211 208944 208945 208946 208947 208948 208949 208950 [...] (11093 flits)
Class 0:
Remaining flits: 171648 171649 171650 171651 171652 171653 171654 171655 171656 171657 [...] (494143 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (9281 flits)
Class 0:
Remaining flits: 171657 171658 171659 171660 171661 171662 171663 171664 171665 181458 [...] (509461 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (7758 flits)
Class 0:
Remaining flits: 171657 171658 171659 171660 171661 171662 171663 171664 171665 181458 [...] (524544 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (6517 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (540955 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (5492 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (554469 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (4762 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (568873 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (3912 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (581507 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (3271 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (600034 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (2821 flits)
Class 0:
Remaining flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (613474 flits)
Measured flits: 208944 208945 208946 208947 208948 208949 208950 208951 208952 208953 [...] (2367 flits)
Class 0:
Remaining flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (622855 flits)
Measured flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (1969 flits)
Class 0:
Remaining flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (636444 flits)
Measured flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (1567 flits)
Class 0:
Remaining flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (646048 flits)
Measured flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (1299 flits)
Class 0:
Remaining flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (651410 flits)
Measured flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (1019 flits)
Class 0:
Remaining flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (660800 flits)
Measured flits: 225072 225073 225074 225075 225076 225077 225078 225079 225080 225081 [...] (878 flits)
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (667978 flits)
Measured flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (728 flits)
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (667770 flits)
Measured flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (588 flits)
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (673933 flits)
Measured flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (451 flits)
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (675455 flits)
Measured flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (414 flits)
Class 0:
Remaining flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (681032 flits)
Measured flits: 232794 232795 232796 232797 232798 232799 232800 232801 232802 232803 [...] (373 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (683977 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (306 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (684403 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (253 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (685312 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (223 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (688504 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (162 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (692422 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (126 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (694440 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (108 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (697153 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (108 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (696125 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (108 flits)
Class 0:
Remaining flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (697843 flits)
Measured flits: 234990 234991 234992 234993 234994 234995 234996 234997 234998 234999 [...] (90 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (698816 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (698772 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (693702 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (691467 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (690953 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (687421 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (688352 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (689377 flits)
Measured flits: 576162 576163 576164 576165 576166 576167 576168 576169 576170 576171 [...] (54 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (694019 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (697438 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (697336 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (697066 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (700987 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (698133 flits)
Measured flits: 609948 609949 609950 609951 609952 609953 609954 609955 609956 609957 [...] (36 flits)
Class 0:
Remaining flits: 675684 675685 675686 675687 675688 675689 675690 675691 675692 675693 [...] (696917 flits)
Measured flits: 675684 675685 675686 675687 675688 675689 675690 675691 675692 675693 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 930468 930469 930470 930471 930472 930473 944100 944101 944102 944103 [...] (659534 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 930468 930469 930470 930471 930472 930473 944100 944101 944102 944103 [...] (624433 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944100 944101 944102 944103 944104 944105 944106 944107 944108 944109 [...] (588994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944100 944101 944102 944103 944104 944105 944106 944107 944108 944109 [...] (554961 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944100 944101 944102 944103 944104 944105 944106 944107 944108 944109 [...] (521577 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944100 944101 944102 944103 944104 944105 944106 944107 944108 944109 [...] (487297 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 944113 944114 944115 944116 944117 1090656 1090657 1090658 1090659 1090660 [...] (454400 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (421175 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (388671 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (355942 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (323049 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (291301 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (259408 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1090656 1090657 1090658 1090659 1090660 1090661 1090662 1090663 1090664 1090665 [...] (228192 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1265688 1265689 1265690 1265691 1265692 1265693 1265694 1265695 1265696 1265697 [...] (197723 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1265688 1265689 1265690 1265691 1265692 1265693 1265694 1265695 1265696 1265697 [...] (167879 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1265688 1265689 1265690 1265691 1265692 1265693 1265694 1265695 1265696 1265697 [...] (137926 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1265688 1265689 1265690 1265691 1265692 1265693 1265694 1265695 1265696 1265697 [...] (108891 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1265688 1265689 1265690 1265691 1265692 1265693 1265694 1265695 1265696 1265697 [...] (81134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1383174 1383175 1383176 1383177 1383178 1383179 1383180 1383181 1383182 1383183 [...] (56735 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (37953 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (22718 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (12403 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (7282 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (3563 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398492 1398493 1398494 1398495 1398496 1398497 1398498 1398499 1398500 1398501 [...] (1447 flits)
Measured flits: (0 flits)
Time taken is 95493 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4043.86 (1 samples)
	minimum = 23 (1 samples)
	maximum = 58929 (1 samples)
Network latency average = 4030.98 (1 samples)
	minimum = 23 (1 samples)
	maximum = 57726 (1 samples)
Flit latency average = 13355.6 (1 samples)
	minimum = 6 (1 samples)
	maximum = 71767 (1 samples)
Fragmentation average = 373.177 (1 samples)
	minimum = 0 (1 samples)
	maximum = 12357 (1 samples)
Injected packet rate average = 0.0179754 (1 samples)
	minimum = 0.0138571 (1 samples)
	maximum = 0.023 (1 samples)
Accepted packet rate average = 0.0127567 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0181429 (1 samples)
Injected flit rate average = 0.323577 (1 samples)
	minimum = 0.251 (1 samples)
	maximum = 0.413429 (1 samples)
Accepted flit rate average = 0.230134 (1 samples)
	minimum = 0.179286 (1 samples)
	maximum = 0.329 (1 samples)
Injected packet size average = 18.0011 (1 samples)
Accepted packet size average = 18.0402 (1 samples)
Hops average = 5.06424 (1 samples)
Total run time 175.766
