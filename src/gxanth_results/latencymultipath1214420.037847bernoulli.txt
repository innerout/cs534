BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.037847
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 297.72
	minimum = 22
	maximum = 858
Network latency average = 284.797
	minimum = 22
	maximum = 800
Slowest packet = 388
Flit latency average = 261.07
	minimum = 5
	maximum = 783
Slowest flit = 21255
Fragmentation average = 23.6384
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0370365
	minimum = 0.017 (at node 0)
	maximum = 0.052 (at node 66)
Accepted packet rate average = 0.0149792
	minimum = 0.006 (at node 174)
	maximum = 0.024 (at node 152)
Injected flit rate average = 0.661297
	minimum = 0.306 (at node 0)
	maximum = 0.924 (at node 66)
Accepted flit rate average= 0.275578
	minimum = 0.114 (at node 174)
	maximum = 0.437 (at node 152)
Injected packet length average = 17.8553
Accepted packet length average = 18.3974
Total in-flight flits = 75213 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 577.223
	minimum = 22
	maximum = 1607
Network latency average = 559.191
	minimum = 22
	maximum = 1511
Slowest packet = 2884
Flit latency average = 535.853
	minimum = 5
	maximum = 1494
Slowest flit = 51929
Fragmentation average = 25.0082
	minimum = 0
	maximum = 147
Injected packet rate average = 0.0341276
	minimum = 0.018 (at node 0)
	maximum = 0.043 (at node 135)
Accepted packet rate average = 0.0151771
	minimum = 0.0085 (at node 135)
	maximum = 0.022 (at node 152)
Injected flit rate average = 0.612279
	minimum = 0.3225 (at node 0)
	maximum = 0.7725 (at node 135)
Accepted flit rate average= 0.276341
	minimum = 0.153 (at node 153)
	maximum = 0.4035 (at node 152)
Injected packet length average = 17.9409
Accepted packet length average = 18.2078
Total in-flight flits = 130873 (0 measured)
latency change    = 0.48422
throughput change = 0.00276116
Class 0:
Packet latency average = 1457.65
	minimum = 23
	maximum = 2300
Network latency average = 1414.39
	minimum = 22
	maximum = 2300
Slowest packet = 4522
Flit latency average = 1396.97
	minimum = 5
	maximum = 2438
Slowest flit = 70690
Fragmentation average = 20.9691
	minimum = 0
	maximum = 166
Injected packet rate average = 0.019224
	minimum = 0.002 (at node 119)
	maximum = 0.048 (at node 47)
Accepted packet rate average = 0.0149792
	minimum = 0.006 (at node 184)
	maximum = 0.026 (at node 56)
Injected flit rate average = 0.346604
	minimum = 0.036 (at node 119)
	maximum = 0.864 (at node 47)
Accepted flit rate average= 0.268182
	minimum = 0.108 (at node 184)
	maximum = 0.455 (at node 56)
Injected packet length average = 18.0298
Accepted packet length average = 17.9037
Total in-flight flits = 146720 (0 measured)
latency change    = 0.604004
throughput change = 0.0304228
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1166.06
	minimum = 25
	maximum = 2166
Network latency average = 98.4352
	minimum = 22
	maximum = 808
Slowest packet = 16931
Flit latency average = 1898.84
	minimum = 5
	maximum = 3192
Slowest flit = 100188
Fragmentation average = 4.39815
	minimum = 0
	maximum = 23
Injected packet rate average = 0.015849
	minimum = 0.003 (at node 159)
	maximum = 0.042 (at node 58)
Accepted packet rate average = 0.0147292
	minimum = 0.004 (at node 110)
	maximum = 0.025 (at node 83)
Injected flit rate average = 0.285776
	minimum = 0.054 (at node 159)
	maximum = 0.762 (at node 58)
Accepted flit rate average= 0.265323
	minimum = 0.079 (at node 110)
	maximum = 0.45 (at node 83)
Injected packet length average = 18.0312
Accepted packet length average = 18.0134
Total in-flight flits = 150750 (52993 measured)
latency change    = 0.250068
throughput change = 0.010777
Class 0:
Packet latency average = 2033.76
	minimum = 25
	maximum = 3569
Network latency average = 749.631
	minimum = 22
	maximum = 1922
Slowest packet = 16931
Flit latency average = 2106.46
	minimum = 5
	maximum = 3988
Slowest flit = 116296
Fragmentation average = 10.32
	minimum = 0
	maximum = 101
Injected packet rate average = 0.0154219
	minimum = 0.005 (at node 57)
	maximum = 0.0315 (at node 156)
Accepted packet rate average = 0.0147917
	minimum = 0.0085 (at node 4)
	maximum = 0.0225 (at node 128)
Injected flit rate average = 0.277552
	minimum = 0.0825 (at node 159)
	maximum = 0.5685 (at node 156)
Accepted flit rate average= 0.266326
	minimum = 0.149 (at node 110)
	maximum = 0.405 (at node 128)
Injected packet length average = 17.9973
Accepted packet length average = 18.0051
Total in-flight flits = 151263 (98447 measured)
latency change    = 0.42665
throughput change = 0.00376458
Class 0:
Packet latency average = 2752.66
	minimum = 25
	maximum = 4264
Network latency average = 1394.57
	minimum = 22
	maximum = 2969
Slowest packet = 16931
Flit latency average = 2293.28
	minimum = 5
	maximum = 4734
Slowest flit = 159099
Fragmentation average = 15.0417
	minimum = 0
	maximum = 183
Injected packet rate average = 0.0150486
	minimum = 0.00666667 (at node 146)
	maximum = 0.0266667 (at node 91)
Accepted packet rate average = 0.0147188
	minimum = 0.00933333 (at node 86)
	maximum = 0.0216667 (at node 128)
Injected flit rate average = 0.271024
	minimum = 0.12 (at node 146)
	maximum = 0.482 (at node 91)
Accepted flit rate average= 0.264861
	minimum = 0.164 (at node 86)
	maximum = 0.391 (at node 128)
Injected packet length average = 18.0099
Accepted packet length average = 17.9948
Total in-flight flits = 150418 (129990 measured)
latency change    = 0.261168
throughput change = 0.00552897
Class 0:
Packet latency average = 3330.9
	minimum = 25
	maximum = 5247
Network latency average = 1943.39
	minimum = 22
	maximum = 3950
Slowest packet = 16931
Flit latency average = 2432.31
	minimum = 5
	maximum = 5235
Slowest flit = 155070
Fragmentation average = 16.8743
	minimum = 0
	maximum = 243
Injected packet rate average = 0.0152487
	minimum = 0.00825 (at node 46)
	maximum = 0.026 (at node 91)
Accepted packet rate average = 0.0147279
	minimum = 0.01075 (at node 86)
	maximum = 0.0195 (at node 128)
Injected flit rate average = 0.274527
	minimum = 0.1485 (at node 46)
	maximum = 0.46875 (at node 91)
Accepted flit rate average= 0.265457
	minimum = 0.1905 (at node 86)
	maximum = 0.351 (at node 128)
Injected packet length average = 18.0033
Accepted packet length average = 18.0241
Total in-flight flits = 153701 (150122 measured)
latency change    = 0.173596
throughput change = 0.00224488
Class 0:
Packet latency average = 3829.99
	minimum = 25
	maximum = 6015
Network latency average = 2314.98
	minimum = 22
	maximum = 4868
Slowest packet = 16931
Flit latency average = 2519.66
	minimum = 5
	maximum = 5338
Slowest flit = 251855
Fragmentation average = 18.2304
	minimum = 0
	maximum = 243
Injected packet rate average = 0.0150313
	minimum = 0.0078 (at node 40)
	maximum = 0.0248 (at node 91)
Accepted packet rate average = 0.014749
	minimum = 0.011 (at node 31)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.270555
	minimum = 0.1404 (at node 40)
	maximum = 0.449 (at node 91)
Accepted flit rate average= 0.265437
	minimum = 0.1986 (at node 31)
	maximum = 0.335 (at node 128)
Injected packet length average = 17.9995
Accepted packet length average = 17.997
Total in-flight flits = 151802 (151555 measured)
latency change    = 0.130311
throughput change = 7.35814e-05
Class 0:
Packet latency average = 4239.24
	minimum = 25
	maximum = 6819
Network latency average = 2495.61
	minimum = 22
	maximum = 5698
Slowest packet = 16931
Flit latency average = 2576.91
	minimum = 5
	maximum = 6298
Slowest flit = 287119
Fragmentation average = 18.6252
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0149705
	minimum = 0.009 (at node 16)
	maximum = 0.0233333 (at node 42)
Accepted packet rate average = 0.0147847
	minimum = 0.0113333 (at node 149)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.269551
	minimum = 0.162 (at node 16)
	maximum = 0.419167 (at node 42)
Accepted flit rate average= 0.266153
	minimum = 0.204333 (at node 149)
	maximum = 0.344667 (at node 128)
Injected packet length average = 18.0055
Accepted packet length average = 18.0019
Total in-flight flits = 150558 (150524 measured)
latency change    = 0.0965405
throughput change = 0.00268747
Class 0:
Packet latency average = 4606
	minimum = 25
	maximum = 7560
Network latency average = 2606.6
	minimum = 22
	maximum = 5878
Slowest packet = 16931
Flit latency average = 2625.71
	minimum = 5
	maximum = 6713
Slowest flit = 304193
Fragmentation average = 18.8828
	minimum = 0
	maximum = 253
Injected packet rate average = 0.0148728
	minimum = 0.00957143 (at node 106)
	maximum = 0.0217143 (at node 174)
Accepted packet rate average = 0.0147887
	minimum = 0.0121429 (at node 171)
	maximum = 0.0182857 (at node 128)
Injected flit rate average = 0.267754
	minimum = 0.172286 (at node 106)
	maximum = 0.390857 (at node 174)
Accepted flit rate average= 0.266171
	minimum = 0.217143 (at node 171)
	maximum = 0.329143 (at node 128)
Injected packet length average = 18.003
Accepted packet length average = 17.9983
Total in-flight flits = 148968 (148968 measured)
latency change    = 0.0796252
throughput change = 6.89525e-05
Draining all recorded packets ...
Class 0:
Remaining flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (149891 flits)
Measured flits: 379350 379351 379352 379353 379354 379355 379356 379357 379358 379359 [...] (149891 flits)
Class 0:
Remaining flits: 450759 450760 450761 450762 450763 450764 450765 450766 450767 450768 [...] (149323 flits)
Measured flits: 450759 450760 450761 450762 450763 450764 450765 450766 450767 450768 [...] (149323 flits)
Class 0:
Remaining flits: 499536 499537 499538 499539 499540 499541 499542 499543 499544 499545 [...] (149297 flits)
Measured flits: 499536 499537 499538 499539 499540 499541 499542 499543 499544 499545 [...] (149297 flits)
Class 0:
Remaining flits: 566262 566263 566264 566265 566266 566267 566268 566269 566270 566271 [...] (146046 flits)
Measured flits: 566262 566263 566264 566265 566266 566267 566268 566269 566270 566271 [...] (146046 flits)
Class 0:
Remaining flits: 599076 599077 599078 599079 599080 599081 599082 599083 599084 599085 [...] (145395 flits)
Measured flits: 599076 599077 599078 599079 599080 599081 599082 599083 599084 599085 [...] (145395 flits)
Class 0:
Remaining flits: 640620 640621 640622 640623 640624 640625 640626 640627 640628 640629 [...] (144822 flits)
Measured flits: 640620 640621 640622 640623 640624 640625 640626 640627 640628 640629 [...] (144822 flits)
Class 0:
Remaining flits: 686970 686971 686972 686973 686974 686975 686976 686977 686978 686979 [...] (142421 flits)
Measured flits: 686970 686971 686972 686973 686974 686975 686976 686977 686978 686979 [...] (142403 flits)
Class 0:
Remaining flits: 722203 722204 722205 722206 722207 722208 722209 722210 722211 722212 [...] (141678 flits)
Measured flits: 722203 722204 722205 722206 722207 722208 722209 722210 722211 722212 [...] (140940 flits)
Class 0:
Remaining flits: 773712 773713 773714 773715 773716 773717 773718 773719 773720 773721 [...] (140736 flits)
Measured flits: 773712 773713 773714 773715 773716 773717 773718 773719 773720 773721 [...] (138054 flits)
Class 0:
Remaining flits: 808974 808975 808976 808977 808978 808979 808980 808981 808982 808983 [...] (139474 flits)
Measured flits: 808974 808975 808976 808977 808978 808979 808980 808981 808982 808983 [...] (133407 flits)
Class 0:
Remaining flits: 829116 829117 829118 829119 829120 829121 829122 829123 829124 829125 [...] (140720 flits)
Measured flits: 829116 829117 829118 829119 829120 829121 829122 829123 829124 829125 [...] (125385 flits)
Class 0:
Remaining flits: 893286 893287 893288 893289 893290 893291 893292 893293 893294 893295 [...] (138829 flits)
Measured flits: 893286 893287 893288 893289 893290 893291 893292 893293 893294 893295 [...] (111404 flits)
Class 0:
Remaining flits: 940591 940592 940593 940594 940595 940596 940597 940598 940599 940600 [...] (139831 flits)
Measured flits: 940591 940592 940593 940594 940595 940596 940597 940598 940599 940600 [...] (95632 flits)
Class 0:
Remaining flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (141398 flits)
Measured flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (77226 flits)
Class 0:
Remaining flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (137796 flits)
Measured flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (55361 flits)
Class 0:
Remaining flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (140379 flits)
Measured flits: 1003014 1003015 1003016 1003017 1003018 1003019 1003020 1003021 1003022 1003023 [...] (39257 flits)
Class 0:
Remaining flits: 1049832 1049833 1049834 1049835 1049836 1049837 1049838 1049839 1049840 1049841 [...] (141026 flits)
Measured flits: 1049832 1049833 1049834 1049835 1049836 1049837 1049838 1049839 1049840 1049841 [...] (23644 flits)
Class 0:
Remaining flits: 1055249 1150542 1150543 1150544 1150545 1150546 1150547 1150548 1150549 1150550 [...] (139546 flits)
Measured flits: 1055249 1150542 1150543 1150544 1150545 1150546 1150547 1150548 1150549 1150550 [...] (13420 flits)
Class 0:
Remaining flits: 1174014 1174015 1174016 1174017 1174018 1174019 1174020 1174021 1174022 1174023 [...] (140635 flits)
Measured flits: 1174014 1174015 1174016 1174017 1174018 1174019 1174020 1174021 1174022 1174023 [...] (7919 flits)
Class 0:
Remaining flits: 1251126 1251127 1251128 1251129 1251130 1251131 1251132 1251133 1251134 1251135 [...] (143347 flits)
Measured flits: 1251126 1251127 1251128 1251129 1251130 1251131 1251132 1251133 1251134 1251135 [...] (4216 flits)
Class 0:
Remaining flits: 1372770 1372771 1372772 1372773 1372774 1372775 1372776 1372777 1372778 1372779 [...] (142441 flits)
Measured flits: 1418598 1418599 1418600 1418601 1418602 1418603 1418604 1418605 1418606 1418607 [...] (1317 flits)
Class 0:
Remaining flits: 1377450 1377451 1377452 1377453 1377454 1377455 1377456 1377457 1377458 1377459 [...] (143956 flits)
Measured flits: 1551924 1551925 1551926 1551927 1551928 1551929 1551930 1551931 1551932 1551933 [...] (385 flits)
Class 0:
Remaining flits: 1508310 1508311 1508312 1508313 1508314 1508315 1508316 1508317 1508318 1508319 [...] (145438 flits)
Measured flits: 1657422 1657423 1657424 1657425 1657426 1657427 1657428 1657429 1657430 1657431 [...] (54 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1543914 1543915 1543916 1543917 1543918 1543919 1543920 1543921 1543922 1543923 [...] (95191 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1596744 1596745 1596746 1596747 1596748 1596749 1596750 1596751 1596752 1596753 [...] (47440 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1628478 1628479 1628480 1628481 1628482 1628483 1628484 1628485 1628486 1628487 [...] (7308 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1771290 1771291 1771292 1771293 1771294 1771295 1771296 1771297 1771298 1771299 [...] (106 flits)
Measured flits: (0 flits)
Time taken is 37658 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9780.45 (1 samples)
	minimum = 25 (1 samples)
	maximum = 23529 (1 samples)
Network latency average = 2836.47 (1 samples)
	minimum = 22 (1 samples)
	maximum = 10166 (1 samples)
Flit latency average = 2760.63 (1 samples)
	minimum = 5 (1 samples)
	maximum = 10149 (1 samples)
Fragmentation average = 20.2093 (1 samples)
	minimum = 0 (1 samples)
	maximum = 253 (1 samples)
Injected packet rate average = 0.0148728 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0217143 (1 samples)
Accepted packet rate average = 0.0147887 (1 samples)
	minimum = 0.0121429 (1 samples)
	maximum = 0.0182857 (1 samples)
Injected flit rate average = 0.267754 (1 samples)
	minimum = 0.172286 (1 samples)
	maximum = 0.390857 (1 samples)
Accepted flit rate average = 0.266171 (1 samples)
	minimum = 0.217143 (1 samples)
	maximum = 0.329143 (1 samples)
Injected packet size average = 18.003 (1 samples)
Accepted packet size average = 17.9983 (1 samples)
Hops average = 5.06097 (1 samples)
Total run time 39.403
