BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.015747
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 213.689
	minimum = 22
	maximum = 826
Network latency average = 151.455
	minimum = 22
	maximum = 701
Slowest packet = 63
Flit latency average = 123.98
	minimum = 5
	maximum = 684
Slowest flit = 14921
Fragmentation average = 25.1858
	minimum = 0
	maximum = 364
Injected packet rate average = 0.0157969
	minimum = 0 (at node 55)
	maximum = 0.053 (at node 118)
Accepted packet rate average = 0.0117708
	minimum = 0.004 (at node 64)
	maximum = 0.02 (at node 48)
Injected flit rate average = 0.282229
	minimum = 0 (at node 55)
	maximum = 0.954 (at node 118)
Accepted flit rate average= 0.217625
	minimum = 0.072 (at node 64)
	maximum = 0.36 (at node 48)
Injected packet length average = 17.8661
Accepted packet length average = 18.4885
Total in-flight flits = 12810 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 303.73
	minimum = 22
	maximum = 1596
Network latency average = 233.233
	minimum = 22
	maximum = 1015
Slowest packet = 63
Flit latency average = 204.348
	minimum = 5
	maximum = 1036
Slowest flit = 50668
Fragmentation average = 29.9886
	minimum = 0
	maximum = 379
Injected packet rate average = 0.0155729
	minimum = 0.0005 (at node 23)
	maximum = 0.042 (at node 43)
Accepted packet rate average = 0.0125807
	minimum = 0.0075 (at node 116)
	maximum = 0.018 (at node 70)
Injected flit rate average = 0.278969
	minimum = 0.009 (at node 23)
	maximum = 0.7485 (at node 43)
Accepted flit rate average= 0.229474
	minimum = 0.135 (at node 153)
	maximum = 0.324 (at node 70)
Injected packet length average = 17.9137
Accepted packet length average = 18.2401
Total in-flight flits = 19522 (0 measured)
latency change    = 0.29645
throughput change = 0.0516353
Class 0:
Packet latency average = 500.151
	minimum = 22
	maximum = 1673
Network latency average = 424.221
	minimum = 22
	maximum = 1443
Slowest packet = 2814
Flit latency average = 388.981
	minimum = 5
	maximum = 1426
Slowest flit = 81377
Fragmentation average = 35.6823
	minimum = 0
	maximum = 375
Injected packet rate average = 0.0161979
	minimum = 0 (at node 38)
	maximum = 0.05 (at node 2)
Accepted packet rate average = 0.0140156
	minimum = 0.006 (at node 4)
	maximum = 0.023 (at node 56)
Injected flit rate average = 0.291833
	minimum = 0 (at node 38)
	maximum = 0.894 (at node 2)
Accepted flit rate average= 0.253255
	minimum = 0.114 (at node 4)
	maximum = 0.419 (at node 152)
Injected packet length average = 18.0167
Accepted packet length average = 18.0695
Total in-flight flits = 26877 (0 measured)
latency change    = 0.392723
throughput change = 0.0939023
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 388.769
	minimum = 22
	maximum = 1178
Network latency average = 315.158
	minimum = 22
	maximum = 943
Slowest packet = 9091
Flit latency average = 507.216
	minimum = 5
	maximum = 1756
Slowest flit = 101177
Fragmentation average = 25.6612
	minimum = 0
	maximum = 204
Injected packet rate average = 0.0150208
	minimum = 0 (at node 168)
	maximum = 0.054 (at node 116)
Accepted packet rate average = 0.0141875
	minimum = 0.005 (at node 36)
	maximum = 0.028 (at node 16)
Injected flit rate average = 0.270219
	minimum = 0 (at node 168)
	maximum = 0.973 (at node 116)
Accepted flit rate average= 0.254318
	minimum = 0.09 (at node 36)
	maximum = 0.516 (at node 16)
Injected packet length average = 17.9896
Accepted packet length average = 17.9255
Total in-flight flits = 29960 (28005 measured)
latency change    = 0.2865
throughput change = 0.00417785
Class 0:
Packet latency average = 579.849
	minimum = 22
	maximum = 2031
Network latency average = 500.873
	minimum = 22
	maximum = 1746
Slowest packet = 9091
Flit latency average = 550.802
	minimum = 5
	maximum = 2151
Slowest flit = 153984
Fragmentation average = 34.3863
	minimum = 0
	maximum = 399
Injected packet rate average = 0.0150651
	minimum = 0 (at node 170)
	maximum = 0.039 (at node 158)
Accepted packet rate average = 0.0141979
	minimum = 0.0085 (at node 36)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.271365
	minimum = 0 (at node 170)
	maximum = 0.702 (at node 158)
Accepted flit rate average= 0.255669
	minimum = 0.153 (at node 36)
	maximum = 0.375 (at node 129)
Injected packet length average = 18.0128
Accepted packet length average = 18.0075
Total in-flight flits = 32830 (32796 measured)
latency change    = 0.329535
throughput change = 0.00528637
Class 0:
Packet latency average = 651.837
	minimum = 22
	maximum = 2341
Network latency average = 573.177
	minimum = 22
	maximum = 2016
Slowest packet = 9091
Flit latency average = 580.752
	minimum = 5
	maximum = 2186
Slowest flit = 153989
Fragmentation average = 34.4791
	minimum = 0
	maximum = 450
Injected packet rate average = 0.0150295
	minimum = 0.00433333 (at node 35)
	maximum = 0.0323333 (at node 7)
Accepted packet rate average = 0.0143299
	minimum = 0.00833333 (at node 36)
	maximum = 0.02 (at node 128)
Injected flit rate average = 0.270549
	minimum = 0.078 (at node 35)
	maximum = 0.581667 (at node 7)
Accepted flit rate average= 0.257738
	minimum = 0.155667 (at node 36)
	maximum = 0.359667 (at node 129)
Injected packet length average = 18.0012
Accepted packet length average = 17.9861
Total in-flight flits = 34246 (34246 measured)
latency change    = 0.110439
throughput change = 0.00802589
Class 0:
Packet latency average = 691.591
	minimum = 22
	maximum = 2517
Network latency average = 611.215
	minimum = 22
	maximum = 2185
Slowest packet = 9091
Flit latency average = 602.42
	minimum = 5
	maximum = 2186
Slowest flit = 153989
Fragmentation average = 36.7631
	minimum = 0
	maximum = 450
Injected packet rate average = 0.0150117
	minimum = 0.0055 (at node 35)
	maximum = 0.03025 (at node 158)
Accepted packet rate average = 0.0143281
	minimum = 0.0095 (at node 36)
	maximum = 0.01925 (at node 128)
Injected flit rate average = 0.27023
	minimum = 0.099 (at node 35)
	maximum = 0.54075 (at node 158)
Accepted flit rate average= 0.257996
	minimum = 0.175 (at node 36)
	maximum = 0.34575 (at node 129)
Injected packet length average = 18.0013
Accepted packet length average = 18.0063
Total in-flight flits = 36258 (36258 measured)
latency change    = 0.0574815
throughput change = 0.00100097
Class 0:
Packet latency average = 718.735
	minimum = 22
	maximum = 3180
Network latency average = 638.939
	minimum = 22
	maximum = 2596
Slowest packet = 13510
Flit latency average = 622.011
	minimum = 5
	maximum = 2579
Slowest flit = 244187
Fragmentation average = 36.9904
	minimum = 0
	maximum = 526
Injected packet rate average = 0.0151635
	minimum = 0.005 (at node 68)
	maximum = 0.0292 (at node 96)
Accepted packet rate average = 0.0143469
	minimum = 0.0106 (at node 86)
	maximum = 0.0184 (at node 128)
Injected flit rate average = 0.272958
	minimum = 0.09 (at node 68)
	maximum = 0.5256 (at node 96)
Accepted flit rate average= 0.258315
	minimum = 0.1908 (at node 86)
	maximum = 0.3296 (at node 128)
Injected packet length average = 18.001
Accepted packet length average = 18.0049
Total in-flight flits = 40921 (40921 measured)
latency change    = 0.0377659
throughput change = 0.00123295
Class 0:
Packet latency average = 746.925
	minimum = 22
	maximum = 3180
Network latency average = 667.929
	minimum = 22
	maximum = 2596
Slowest packet = 13510
Flit latency average = 644.914
	minimum = 5
	maximum = 2579
Slowest flit = 244187
Fragmentation average = 38.061
	minimum = 0
	maximum = 526
Injected packet rate average = 0.0151467
	minimum = 0.0055 (at node 68)
	maximum = 0.0271667 (at node 96)
Accepted packet rate average = 0.0143872
	minimum = 0.0113333 (at node 149)
	maximum = 0.0181667 (at node 128)
Injected flit rate average = 0.272705
	minimum = 0.099 (at node 68)
	maximum = 0.489 (at node 96)
Accepted flit rate average= 0.25895
	minimum = 0.208333 (at node 149)
	maximum = 0.324167 (at node 128)
Injected packet length average = 18.0042
Accepted packet length average = 17.9987
Total in-flight flits = 42649 (42649 measured)
latency change    = 0.0377415
throughput change = 0.00245248
Class 0:
Packet latency average = 773.976
	minimum = 22
	maximum = 3180
Network latency average = 694.792
	minimum = 22
	maximum = 3101
Slowest packet = 13510
Flit latency average = 667.418
	minimum = 5
	maximum = 3084
Slowest flit = 362267
Fragmentation average = 38.4548
	minimum = 0
	maximum = 542
Injected packet rate average = 0.0152582
	minimum = 0.007 (at node 167)
	maximum = 0.0258571 (at node 2)
Accepted packet rate average = 0.0144234
	minimum = 0.0112857 (at node 79)
	maximum = 0.018 (at node 138)
Injected flit rate average = 0.274633
	minimum = 0.126 (at node 167)
	maximum = 0.466286 (at node 2)
Accepted flit rate average= 0.259671
	minimum = 0.205571 (at node 79)
	maximum = 0.324 (at node 138)
Injected packet length average = 17.9991
Accepted packet length average = 18.0035
Total in-flight flits = 47005 (47005 measured)
latency change    = 0.0349512
throughput change = 0.00277843
Draining all recorded packets ...
Class 0:
Remaining flits: 419310 419311 419312 419313 419314 419315 419316 419317 419318 419319 [...] (49899 flits)
Measured flits: 419310 419311 419312 419313 419314 419315 419316 419317 419318 419319 [...] (12645 flits)
Class 0:
Remaining flits: 499687 499688 499689 499690 499691 499692 499693 499694 499695 499696 [...] (51620 flits)
Measured flits: 499687 499688 499689 499690 499691 499692 499693 499694 499695 499696 [...] (1564 flits)
Class 0:
Remaining flits: 511758 511759 511760 511761 511762 511763 511764 511765 511766 511767 [...] (52644 flits)
Measured flits: 511758 511759 511760 511761 511762 511763 511764 511765 511766 511767 [...] (311 flits)
Class 0:
Remaining flits: 531679 531680 531681 531682 531683 541116 541117 541118 541119 541120 [...] (56798 flits)
Measured flits: 531679 531680 531681 531682 531683 (5 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 549882 549883 549884 549885 549886 549887 549888 549889 549890 549891 [...] (17331 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 629712 629713 629714 629715 629716 629717 629718 629719 629720 629721 [...] (3090 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 682740 682741 682742 682743 682744 682745 682746 682747 682748 682749 [...] (744 flits)
Measured flits: (0 flits)
Time taken is 17829 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 858.031 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4302 (1 samples)
Network latency average = 777.61 (1 samples)
	minimum = 22 (1 samples)
	maximum = 4078 (1 samples)
Flit latency average = 840.421 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5028 (1 samples)
Fragmentation average = 40.9034 (1 samples)
	minimum = 0 (1 samples)
	maximum = 542 (1 samples)
Injected packet rate average = 0.0152582 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0258571 (1 samples)
Accepted packet rate average = 0.0144234 (1 samples)
	minimum = 0.0112857 (1 samples)
	maximum = 0.018 (1 samples)
Injected flit rate average = 0.274633 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.466286 (1 samples)
Accepted flit rate average = 0.259671 (1 samples)
	minimum = 0.205571 (1 samples)
	maximum = 0.324 (1 samples)
Injected packet size average = 17.9991 (1 samples)
Accepted packet size average = 18.0035 (1 samples)
Hops average = 5.06962 (1 samples)
Total run time 13.7491
