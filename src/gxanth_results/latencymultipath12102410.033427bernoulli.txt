BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.033427
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 293.935
	minimum = 23
	maximum = 933
Network latency average = 284.896
	minimum = 23
	maximum = 911
Slowest packet = 391
Flit latency average = 253.812
	minimum = 6
	maximum = 931
Slowest flit = 6339
Fragmentation average = 53.6281
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0329635
	minimum = 0.017 (at node 97)
	maximum = 0.048 (at node 118)
Accepted packet rate average = 0.0106302
	minimum = 0.004 (at node 81)
	maximum = 0.018 (at node 76)
Injected flit rate average = 0.587724
	minimum = 0.306 (at node 97)
	maximum = 0.862 (at node 118)
Accepted flit rate average= 0.199906
	minimum = 0.085 (at node 81)
	maximum = 0.331 (at node 132)
Injected packet length average = 17.8295
Accepted packet length average = 18.8055
Total in-flight flits = 75540 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 553.958
	minimum = 23
	maximum = 1821
Network latency average = 543.318
	minimum = 23
	maximum = 1805
Slowest packet = 763
Flit latency average = 512.143
	minimum = 6
	maximum = 1788
Slowest flit = 13751
Fragmentation average = 56.964
	minimum = 0
	maximum = 156
Injected packet rate average = 0.03325
	minimum = 0.0205 (at node 62)
	maximum = 0.043 (at node 117)
Accepted packet rate average = 0.0110781
	minimum = 0.006 (at node 153)
	maximum = 0.0165 (at node 44)
Injected flit rate average = 0.596021
	minimum = 0.369 (at node 62)
	maximum = 0.771 (at node 118)
Accepted flit rate average= 0.203974
	minimum = 0.109 (at node 174)
	maximum = 0.301 (at node 63)
Injected packet length average = 17.9254
Accepted packet length average = 18.4123
Total in-flight flits = 151498 (0 measured)
latency change    = 0.469391
throughput change = 0.0199423
Class 0:
Packet latency average = 1352.68
	minimum = 23
	maximum = 2684
Network latency average = 1339.57
	minimum = 23
	maximum = 2678
Slowest packet = 1361
Flit latency average = 1316.73
	minimum = 6
	maximum = 2680
Slowest flit = 34411
Fragmentation average = 59.4226
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0328594
	minimum = 0.018 (at node 111)
	maximum = 0.051 (at node 123)
Accepted packet rate average = 0.011276
	minimum = 0.005 (at node 20)
	maximum = 0.021 (at node 136)
Injected flit rate average = 0.591062
	minimum = 0.308 (at node 111)
	maximum = 0.925 (at node 123)
Accepted flit rate average= 0.202089
	minimum = 0.091 (at node 20)
	maximum = 0.368 (at node 136)
Injected packet length average = 17.9876
Accepted packet length average = 17.9219
Total in-flight flits = 226259 (0 measured)
latency change    = 0.590475
throughput change = 0.00932966
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 110.51
	minimum = 23
	maximum = 932
Network latency average = 83.2612
	minimum = 23
	maximum = 928
Slowest packet = 19174
Flit latency average = 1934.01
	minimum = 6
	maximum = 3550
Slowest flit = 49218
Fragmentation average = 10.092
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0311562
	minimum = 0.013 (at node 24)
	maximum = 0.053 (at node 26)
Accepted packet rate average = 0.0107917
	minimum = 0.002 (at node 76)
	maximum = 0.018 (at node 65)
Injected flit rate average = 0.560714
	minimum = 0.228 (at node 100)
	maximum = 0.956 (at node 26)
Accepted flit rate average= 0.193255
	minimum = 0.046 (at node 76)
	maximum = 0.336 (at node 95)
Injected packet length average = 17.9968
Accepted packet length average = 17.9078
Total in-flight flits = 296848 (100367 measured)
latency change    = 11.2404
throughput change = 0.0457081
Class 0:
Packet latency average = 227.345
	minimum = 23
	maximum = 1408
Network latency average = 168.545
	minimum = 23
	maximum = 1371
Slowest packet = 20993
Flit latency average = 2304.25
	minimum = 6
	maximum = 4405
Slowest flit = 61991
Fragmentation average = 9.80622
	minimum = 0
	maximum = 35
Injected packet rate average = 0.0300234
	minimum = 0.014 (at node 100)
	maximum = 0.0455 (at node 26)
Accepted packet rate average = 0.010375
	minimum = 0.006 (at node 4)
	maximum = 0.0175 (at node 182)
Injected flit rate average = 0.540461
	minimum = 0.2495 (at node 100)
	maximum = 0.817 (at node 26)
Accepted flit rate average= 0.186984
	minimum = 0.108 (at node 4)
	maximum = 0.3275 (at node 182)
Injected packet length average = 18.0013
Accepted packet length average = 18.0226
Total in-flight flits = 362069 (194840 measured)
latency change    = 0.513911
throughput change = 0.0335367
Class 0:
Packet latency average = 401.938
	minimum = 23
	maximum = 2736
Network latency average = 300.004
	minimum = 23
	maximum = 2733
Slowest packet = 19889
Flit latency average = 2643.49
	minimum = 6
	maximum = 5376
Slowest flit = 69756
Fragmentation average = 9.93347
	minimum = 0
	maximum = 88
Injected packet rate average = 0.0294635
	minimum = 0.014 (at node 100)
	maximum = 0.0413333 (at node 39)
Accepted packet rate average = 0.0101424
	minimum = 0.00533333 (at node 4)
	maximum = 0.0153333 (at node 95)
Injected flit rate average = 0.53004
	minimum = 0.249333 (at node 100)
	maximum = 0.743667 (at node 39)
Accepted flit rate average= 0.182483
	minimum = 0.0986667 (at node 132)
	maximum = 0.283667 (at node 95)
Injected packet length average = 17.9897
Accepted packet length average = 17.9921
Total in-flight flits = 426663 (287825 measured)
latency change    = 0.434377
throughput change = 0.0246694
Class 0:
Packet latency average = 691.599
	minimum = 23
	maximum = 3995
Network latency average = 536.787
	minimum = 23
	maximum = 3979
Slowest packet = 19181
Flit latency average = 2989.38
	minimum = 6
	maximum = 6298
Slowest flit = 77811
Fragmentation average = 12.192
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0292643
	minimum = 0.01375 (at node 100)
	maximum = 0.042 (at node 39)
Accepted packet rate average = 0.0101211
	minimum = 0.00625 (at node 4)
	maximum = 0.0145 (at node 95)
Injected flit rate average = 0.526733
	minimum = 0.24425 (at node 100)
	maximum = 0.757 (at node 39)
Accepted flit rate average= 0.182098
	minimum = 0.1145 (at node 4)
	maximum = 0.26275 (at node 95)
Injected packet length average = 17.9992
Accepted packet length average = 17.9919
Total in-flight flits = 491048 (380996 measured)
latency change    = 0.418829
throughput change = 0.00211415
Class 0:
Packet latency average = 1149.45
	minimum = 23
	maximum = 4978
Network latency average = 960.967
	minimum = 23
	maximum = 4971
Slowest packet = 19249
Flit latency average = 3325.84
	minimum = 6
	maximum = 7145
Slowest flit = 90305
Fragmentation average = 17.1531
	minimum = 0
	maximum = 121
Injected packet rate average = 0.029376
	minimum = 0.0142 (at node 24)
	maximum = 0.0414 (at node 39)
Accepted packet rate average = 0.0101594
	minimum = 0.0068 (at node 4)
	maximum = 0.014 (at node 95)
Injected flit rate average = 0.528635
	minimum = 0.253 (at node 100)
	maximum = 0.7464 (at node 39)
Accepted flit rate average= 0.182861
	minimum = 0.1256 (at node 4)
	maximum = 0.2506 (at node 95)
Injected packet length average = 17.9955
Accepted packet length average = 17.9993
Total in-flight flits = 558330 (475582 measured)
latency change    = 0.398319
throughput change = 0.00417694
Class 0:
Packet latency average = 1824.1
	minimum = 23
	maximum = 6029
Network latency average = 1607.04
	minimum = 23
	maximum = 5986
Slowest packet = 19355
Flit latency average = 3644.18
	minimum = 6
	maximum = 8071
Slowest flit = 105193
Fragmentation average = 25.2926
	minimum = 0
	maximum = 147
Injected packet rate average = 0.029303
	minimum = 0.0141667 (at node 24)
	maximum = 0.0401667 (at node 39)
Accepted packet rate average = 0.0100842
	minimum = 0.00733333 (at node 48)
	maximum = 0.013 (at node 95)
Injected flit rate average = 0.527359
	minimum = 0.255667 (at node 100)
	maximum = 0.724 (at node 39)
Accepted flit rate average= 0.181495
	minimum = 0.133167 (at node 48)
	maximum = 0.233833 (at node 95)
Injected packet length average = 17.9968
Accepted packet length average = 17.9979
Total in-flight flits = 624803 (564364 measured)
latency change    = 0.369857
throughput change = 0.00753006
Class 0:
Packet latency average = 2558.36
	minimum = 23
	maximum = 7101
Network latency average = 2323.43
	minimum = 23
	maximum = 6965
Slowest packet = 19115
Flit latency average = 3978.7
	minimum = 6
	maximum = 8920
Slowest flit = 122959
Fragmentation average = 32.4128
	minimum = 0
	maximum = 152
Injected packet rate average = 0.0291868
	minimum = 0.0144286 (at node 24)
	maximum = 0.0394286 (at node 39)
Accepted packet rate average = 0.0100811
	minimum = 0.00728571 (at node 4)
	maximum = 0.0127143 (at node 95)
Injected flit rate average = 0.525299
	minimum = 0.260286 (at node 24)
	maximum = 0.710571 (at node 39)
Accepted flit rate average= 0.181421
	minimum = 0.132286 (at node 4)
	maximum = 0.227857 (at node 95)
Injected packet length average = 17.9979
Accepted packet length average = 17.9962
Total in-flight flits = 688533 (648350 measured)
latency change    = 0.287003
throughput change = 0.000406021
Draining all recorded packets ...
Class 0:
Remaining flits: 132516 132517 132518 132519 132520 132521 132522 132523 132524 132525 [...] (750725 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (644594 flits)
Class 0:
Remaining flits: 182034 182035 182036 182037 182038 182039 182040 182041 182042 182043 [...] (805529 flits)
Measured flits: 343386 343387 343388 343389 343390 343391 343392 343393 343394 343395 [...] (634935 flits)
Class 0:
Remaining flits: 199710 199711 199712 199713 199714 199715 199716 199717 199718 199719 [...] (847424 flits)
Measured flits: 343404 343405 343406 343407 343408 343409 343410 343411 343412 343413 [...] (621617 flits)
Class 0:
Remaining flits: 206037 206038 206039 206040 206041 206042 206043 206044 206045 242586 [...] (873787 flits)
Measured flits: 343530 343531 343532 343533 343534 343535 343536 343537 343538 343539 [...] (605895 flits)
Class 0:
Remaining flits: 250524 250525 250526 250527 250528 250529 250530 250531 250532 250533 [...] (895137 flits)
Measured flits: 343980 343981 343982 343983 343984 343985 343986 343987 343988 343989 [...] (588246 flits)
Class 0:
Remaining flits: 250524 250525 250526 250527 250528 250529 250530 250531 250532 250533 [...] (913257 flits)
Measured flits: 343980 343981 343982 343983 343984 343985 343986 343987 343988 343989 [...] (568980 flits)
Class 0:
Remaining flits: 275616 275617 275618 275619 275620 275621 275622 275623 275624 275625 [...] (930981 flits)
Measured flits: 343984 343985 343986 343987 343988 343989 343990 343991 343992 343993 [...] (549014 flits)
Class 0:
Remaining flits: 287316 287317 287318 287319 287320 287321 287322 287323 287324 287325 [...] (946899 flits)
Measured flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (526489 flits)
Class 0:
Remaining flits: 318941 322200 322201 322202 322203 322204 322205 322206 322207 322208 [...] (960086 flits)
Measured flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (499195 flits)
Class 0:
Remaining flits: 322200 322201 322202 322203 322204 322205 322206 322207 322208 322209 [...] (966340 flits)
Measured flits: 345348 345349 345350 345351 345352 345353 345354 345355 345356 345357 [...] (470467 flits)
Class 0:
Remaining flits: 359190 359191 359192 359193 359194 359195 359196 359197 359198 359199 [...] (972202 flits)
Measured flits: 359190 359191 359192 359193 359194 359195 359196 359197 359198 359199 [...] (441922 flits)
Class 0:
Remaining flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (973257 flits)
Measured flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (412243 flits)
Class 0:
Remaining flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (969376 flits)
Measured flits: 383526 383527 383528 383529 383530 383531 383532 383533 383534 383535 [...] (382324 flits)
Class 0:
Remaining flits: 441792 441793 441794 441795 441796 441797 441798 441799 441800 441801 [...] (966892 flits)
Measured flits: 441792 441793 441794 441795 441796 441797 441798 441799 441800 441801 [...] (352397 flits)
Class 0:
Remaining flits: 442456 442457 462780 462781 462782 462783 462784 462785 462786 462787 [...] (965969 flits)
Measured flits: 442456 442457 462780 462781 462782 462783 462784 462785 462786 462787 [...] (322799 flits)
Class 0:
Remaining flits: 467460 467461 467462 467463 467464 467465 467466 467467 467468 467469 [...] (963471 flits)
Measured flits: 467460 467461 467462 467463 467464 467465 467466 467467 467468 467469 [...] (293416 flits)
Class 0:
Remaining flits: 489186 489187 489188 489189 489190 489191 489192 489193 489194 489195 [...] (960985 flits)
Measured flits: 489186 489187 489188 489189 489190 489191 489192 489193 489194 489195 [...] (264241 flits)
Class 0:
Remaining flits: 489186 489187 489188 489189 489190 489191 489192 489193 489194 489195 [...] (961280 flits)
Measured flits: 489186 489187 489188 489189 489190 489191 489192 489193 489194 489195 [...] (235508 flits)
Class 0:
Remaining flits: 526824 526825 526826 526827 526828 526829 526830 526831 526832 526833 [...] (961341 flits)
Measured flits: 526824 526825 526826 526827 526828 526829 526830 526831 526832 526833 [...] (207802 flits)
Class 0:
Remaining flits: 526824 526825 526826 526827 526828 526829 526830 526831 526832 526833 [...] (957670 flits)
Measured flits: 526824 526825 526826 526827 526828 526829 526830 526831 526832 526833 [...] (180990 flits)
Class 0:
Remaining flits: 564462 564463 564464 564465 564466 564467 564468 564469 564470 564471 [...] (956660 flits)
Measured flits: 564462 564463 564464 564465 564466 564467 564468 564469 564470 564471 [...] (155174 flits)
Class 0:
Remaining flits: 576306 576307 576308 576309 576310 576311 576312 576313 576314 576315 [...] (953481 flits)
Measured flits: 576306 576307 576308 576309 576310 576311 576312 576313 576314 576315 [...] (130816 flits)
Class 0:
Remaining flits: 602312 602313 602314 602315 607752 607753 607754 607755 607756 607757 [...] (952029 flits)
Measured flits: 602312 602313 602314 602315 607752 607753 607754 607755 607756 607757 [...] (108666 flits)
Class 0:
Remaining flits: 607752 607753 607754 607755 607756 607757 607758 607759 607760 607761 [...] (953496 flits)
Measured flits: 607752 607753 607754 607755 607756 607757 607758 607759 607760 607761 [...] (89076 flits)
Class 0:
Remaining flits: 607752 607753 607754 607755 607756 607757 607758 607759 607760 607761 [...] (950621 flits)
Measured flits: 607752 607753 607754 607755 607756 607757 607758 607759 607760 607761 [...] (71329 flits)
Class 0:
Remaining flits: 619164 619165 619166 619167 619168 619169 619170 619171 619172 619173 [...] (948428 flits)
Measured flits: 619164 619165 619166 619167 619168 619169 619170 619171 619172 619173 [...] (56958 flits)
Class 0:
Remaining flits: 619177 619178 619179 619180 619181 664434 664435 664436 664437 664438 [...] (946221 flits)
Measured flits: 619177 619178 619179 619180 619181 664434 664435 664436 664437 664438 [...] (45354 flits)
Class 0:
Remaining flits: 664434 664435 664436 664437 664438 664439 664440 664441 664442 664443 [...] (941938 flits)
Measured flits: 664434 664435 664436 664437 664438 664439 664440 664441 664442 664443 [...] (34576 flits)
Class 0:
Remaining flits: 702774 702775 702776 702777 702778 702779 702780 702781 702782 702783 [...] (939566 flits)
Measured flits: 702774 702775 702776 702777 702778 702779 702780 702781 702782 702783 [...] (25861 flits)
Class 0:
Remaining flits: 744786 744787 744788 744789 744790 744791 744792 744793 744794 744795 [...] (935256 flits)
Measured flits: 744786 744787 744788 744789 744790 744791 744792 744793 744794 744795 [...] (19318 flits)
Class 0:
Remaining flits: 763884 763885 763886 763887 763888 763889 763890 763891 763892 763893 [...] (934599 flits)
Measured flits: 763884 763885 763886 763887 763888 763889 763890 763891 763892 763893 [...] (13514 flits)
Class 0:
Remaining flits: 776232 776233 776234 776235 776236 776237 776238 776239 776240 776241 [...] (932925 flits)
Measured flits: 776232 776233 776234 776235 776236 776237 776238 776239 776240 776241 [...] (9460 flits)
Class 0:
Remaining flits: 776232 776233 776234 776235 776236 776237 776238 776239 776240 776241 [...] (932370 flits)
Measured flits: 776232 776233 776234 776235 776236 776237 776238 776239 776240 776241 [...] (6453 flits)
Class 0:
Remaining flits: 825948 825949 825950 825951 825952 825953 825954 825955 825956 825957 [...] (932803 flits)
Measured flits: 825948 825949 825950 825951 825952 825953 825954 825955 825956 825957 [...] (4362 flits)
Class 0:
Remaining flits: 832284 832285 832286 832287 832288 832289 832290 832291 832292 832293 [...] (932719 flits)
Measured flits: 832284 832285 832286 832287 832288 832289 832290 832291 832292 832293 [...] (3134 flits)
Class 0:
Remaining flits: 832284 832285 832286 832287 832288 832289 832290 832291 832292 832293 [...] (928498 flits)
Measured flits: 832284 832285 832286 832287 832288 832289 832290 832291 832292 832293 [...] (2083 flits)
Class 0:
Remaining flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (923548 flits)
Measured flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (1374 flits)
Class 0:
Remaining flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (922635 flits)
Measured flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (936 flits)
Class 0:
Remaining flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (922518 flits)
Measured flits: 854946 854947 854948 854949 854950 854951 854952 854953 854954 854955 [...] (599 flits)
Class 0:
Remaining flits: 962784 962785 962786 962787 962788 962789 962790 962791 962792 962793 [...] (918672 flits)
Measured flits: 962784 962785 962786 962787 962788 962789 962790 962791 962792 962793 [...] (461 flits)
Class 0:
Remaining flits: 962784 962785 962786 962787 962788 962789 962790 962791 962792 962793 [...] (913533 flits)
Measured flits: 962784 962785 962786 962787 962788 962789 962790 962791 962792 962793 [...] (288 flits)
Class 0:
Remaining flits: 1028124 1028125 1028126 1028127 1028128 1028129 1028130 1028131 1028132 1028133 [...] (910162 flits)
Measured flits: 1028124 1028125 1028126 1028127 1028128 1028129 1028130 1028131 1028132 1028133 [...] (198 flits)
Class 0:
Remaining flits: 1034982 1034983 1034984 1034985 1034986 1034987 1034988 1034989 1034990 1034991 [...] (907357 flits)
Measured flits: 1034982 1034983 1034984 1034985 1034986 1034987 1034988 1034989 1034990 1034991 [...] (60 flits)
Class 0:
Remaining flits: 1034982 1034983 1034984 1034985 1034986 1034987 1034988 1034989 1034990 1034991 [...] (906623 flits)
Measured flits: 1034982 1034983 1034984 1034985 1034986 1034987 1034988 1034989 1034990 1034991 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1065114 1065115 1065116 1065117 1065118 1065119 1065120 1065121 1065122 1065123 [...] (875582 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1065114 1065115 1065116 1065117 1065118 1065119 1065120 1065121 1065122 1065123 [...] (844793 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1065125 1065126 1065127 1065128 1065129 1065130 1065131 1125144 1125145 1125146 [...] (814097 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1125144 1125145 1125146 1125147 1125148 1125149 1125150 1125151 1125152 1125153 [...] (782853 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1196298 1196299 1196300 1196301 1196302 1196303 1196304 1196305 1196306 1196307 [...] (751879 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1241388 1241389 1241390 1241391 1241392 1241393 1241394 1241395 1241396 1241397 [...] (720321 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1247144 1247145 1247146 1247147 1249200 1249201 1249202 1249203 1249204 1249205 [...] (689064 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249200 1249201 1249202 1249203 1249204 1249205 1249206 1249207 1249208 1249209 [...] (657502 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249200 1249201 1249202 1249203 1249204 1249205 1249206 1249207 1249208 1249209 [...] (626362 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1249200 1249201 1249202 1249203 1249204 1249205 1249206 1249207 1249208 1249209 [...] (595493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1272312 1272313 1272314 1272315 1272316 1272317 1272318 1272319 1272320 1272321 [...] (564846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398942 1398943 1398944 1398945 1398946 1398947 1398948 1398949 1398950 1398951 [...] (533448 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398942 1398943 1398944 1398945 1398946 1398947 1398948 1398949 1398950 1398951 [...] (502313 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398942 1398943 1398944 1398945 1398946 1398947 1398948 1398949 1398950 1398951 [...] (471397 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1398956 1398957 1398958 1398959 1439388 1439389 1439390 1439391 1439392 1439393 [...] (440493 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1439388 1439389 1439390 1439391 1439392 1439393 1439394 1439395 1439396 1439397 [...] (409964 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1467972 1467973 1467974 1467975 1467976 1467977 1467978 1467979 1467980 1467981 [...] (380244 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1533132 1533133 1533134 1533135 1533136 1533137 1533138 1533139 1533140 1533141 [...] (350328 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1533132 1533133 1533134 1533135 1533136 1533137 1533138 1533139 1533140 1533141 [...] (320759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1627578 1627579 1627580 1627581 1627582 1627583 1627584 1627585 1627586 1627587 [...] (291750 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1627578 1627579 1627580 1627581 1627582 1627583 1627584 1627585 1627586 1627587 [...] (262701 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1712034 1712035 1712036 1712037 1712038 1712039 1712040 1712041 1712042 1712043 [...] (233259 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1759619 1759620 1759621 1759622 1759623 1759624 1759625 1763496 1763497 1763498 [...] (204139 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1771398 1771399 1771400 1771401 1771402 1771403 1771404 1771405 1771406 1771407 [...] (175461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1855217 1855218 1855219 1855220 1855221 1855222 1855223 1864818 1864819 1864820 [...] (148364 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1887053 1887054 1887055 1887056 1887057 1887058 1887059 1887060 1887061 1887062 [...] (119460 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1925136 1925137 1925138 1925139 1925140 1925141 1925142 1925143 1925144 1925145 [...] (92436 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1969146 1969147 1969148 1969149 1969150 1969151 1969152 1969153 1969154 1969155 [...] (65870 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1987902 1987903 1987904 1987905 1987906 1987907 1987908 1987909 1987910 1987911 [...] (41296 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2067027 2067028 2067029 2106360 2106361 2106362 2106363 2106364 2106365 2106366 [...] (21211 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2106360 2106361 2106362 2106363 2106364 2106365 2106366 2106367 2106368 2106369 [...] (8181 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2106360 2106361 2106362 2106363 2106364 2106365 2106366 2106367 2106368 2106369 [...] (2759 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2231838 2231839 2231840 2231841 2231842 2231843 2231844 2231845 2231846 2231847 [...] (1038 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2542860 2542861 2542862 2542863 2542864 2542865 2542866 2542867 2542868 2542869 [...] (44 flits)
Measured flits: (0 flits)
Time taken is 88631 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 16086.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 44775 (1 samples)
Network latency average = 15123.6 (1 samples)
	minimum = 23 (1 samples)
	maximum = 44731 (1 samples)
Flit latency average = 22439.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 56867 (1 samples)
Fragmentation average = 71.3187 (1 samples)
	minimum = 0 (1 samples)
	maximum = 174 (1 samples)
Injected packet rate average = 0.0291868 (1 samples)
	minimum = 0.0144286 (1 samples)
	maximum = 0.0394286 (1 samples)
Accepted packet rate average = 0.0100811 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0127143 (1 samples)
Injected flit rate average = 0.525299 (1 samples)
	minimum = 0.260286 (1 samples)
	maximum = 0.710571 (1 samples)
Accepted flit rate average = 0.181421 (1 samples)
	minimum = 0.132286 (1 samples)
	maximum = 0.227857 (1 samples)
Injected packet size average = 17.9979 (1 samples)
Accepted packet size average = 17.9962 (1 samples)
Hops average = 5.07286 (1 samples)
Total run time 68.7994
