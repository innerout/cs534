BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.051107
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=1024
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 322.818
	minimum = 23
	maximum = 925
Network latency average = 298.85
	minimum = 23
	maximum = 880
Slowest packet = 433
Flit latency average = 268.678
	minimum = 6
	maximum = 962
Slowest flit = 2925
Fragmentation average = 52.7621
	minimum = 0
	maximum = 138
Injected packet rate average = 0.0474063
	minimum = 0.036 (at node 43)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.0118854
	minimum = 0.004 (at node 150)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.845885
	minimum = 0.639 (at node 157)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.22249
	minimum = 0.077 (at node 150)
	maximum = 0.378 (at node 152)
Injected packet length average = 17.8433
Accepted packet length average = 18.7195
Total in-flight flits = 121118 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 601.923
	minimum = 23
	maximum = 1883
Network latency average = 566.803
	minimum = 23
	maximum = 1804
Slowest packet = 786
Flit latency average = 537.987
	minimum = 6
	maximum = 1823
Slowest flit = 25596
Fragmentation average = 55.3982
	minimum = 0
	maximum = 159
Injected packet rate average = 0.0483203
	minimum = 0.04 (at node 23)
	maximum = 0.056 (at node 131)
Accepted packet rate average = 0.012224
	minimum = 0.0075 (at node 79)
	maximum = 0.018 (at node 152)
Injected flit rate average = 0.865786
	minimum = 0.716 (at node 23)
	maximum = 1 (at node 131)
Accepted flit rate average= 0.224424
	minimum = 0.1425 (at node 164)
	maximum = 0.3295 (at node 152)
Injected packet length average = 17.9177
Accepted packet length average = 18.3594
Total in-flight flits = 247865 (0 measured)
latency change    = 0.463689
throughput change = 0.00862159
Class 0:
Packet latency average = 1709.18
	minimum = 23
	maximum = 2777
Network latency average = 1654.09
	minimum = 23
	maximum = 2743
Slowest packet = 1548
Flit latency average = 1640.37
	minimum = 6
	maximum = 2726
Slowest flit = 38052
Fragmentation average = 64.557
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0418177
	minimum = 0.011 (at node 108)
	maximum = 0.056 (at node 27)
Accepted packet rate average = 0.0106042
	minimum = 0.003 (at node 28)
	maximum = 0.018 (at node 80)
Injected flit rate average = 0.75263
	minimum = 0.209 (at node 108)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.191099
	minimum = 0.054 (at node 28)
	maximum = 0.324 (at node 73)
Injected packet length average = 17.9979
Accepted packet length average = 18.0211
Total in-flight flits = 355678 (0 measured)
latency change    = 0.647829
throughput change = 0.174389
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 504.741
	minimum = 27
	maximum = 1832
Network latency average = 42.773
	minimum = 25
	maximum = 255
Slowest packet = 26633
Flit latency average = 2365.47
	minimum = 6
	maximum = 3705
Slowest flit = 44088
Fragmentation average = 8.96216
	minimum = 0
	maximum = 30
Injected packet rate average = 0.0421562
	minimum = 0.012 (at node 8)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0102813
	minimum = 0.004 (at node 36)
	maximum = 0.018 (at node 187)
Injected flit rate average = 0.758948
	minimum = 0.211 (at node 180)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.1855
	minimum = 0.085 (at node 36)
	maximum = 0.36 (at node 187)
Injected packet length average = 18.0032
Accepted packet length average = 18.0426
Total in-flight flits = 465772 (142362 measured)
latency change    = 2.38625
throughput change = 0.0301831
Class 0:
Packet latency average = 684.923
	minimum = 27
	maximum = 2625
Network latency average = 68.0877
	minimum = 23
	maximum = 1979
Slowest packet = 26633
Flit latency average = 2746.89
	minimum = 6
	maximum = 4531
Slowest flit = 60785
Fragmentation average = 9.37534
	minimum = 0
	maximum = 30
Injected packet rate average = 0.0422995
	minimum = 0.0125 (at node 88)
	maximum = 0.056 (at node 94)
Accepted packet rate average = 0.0103073
	minimum = 0.0045 (at node 61)
	maximum = 0.017 (at node 78)
Injected flit rate average = 0.761617
	minimum = 0.231 (at node 8)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.184622
	minimum = 0.081 (at node 61)
	maximum = 0.294 (at node 78)
Injected packet length average = 18.0054
Accepted packet length average = 17.9118
Total in-flight flits = 577175 (285802 measured)
latency change    = 0.26307
throughput change = 0.00475351
Class 0:
Packet latency average = 953.517
	minimum = 27
	maximum = 3257
Network latency average = 278.727
	minimum = 23
	maximum = 2840
Slowest packet = 26633
Flit latency average = 3101.61
	minimum = 6
	maximum = 5373
Slowest flit = 97140
Fragmentation average = 9.64204
	minimum = 0
	maximum = 44
Injected packet rate average = 0.0420781
	minimum = 0.013 (at node 88)
	maximum = 0.0556667 (at node 3)
Accepted packet rate average = 0.0103125
	minimum = 0.006 (at node 180)
	maximum = 0.0143333 (at node 165)
Injected flit rate average = 0.757656
	minimum = 0.238667 (at node 88)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.185155
	minimum = 0.113667 (at node 180)
	maximum = 0.253 (at node 102)
Injected packet length average = 18.0059
Accepted packet length average = 17.9544
Total in-flight flits = 685277 (425227 measured)
latency change    = 0.281688
throughput change = 0.00287391
Draining remaining packets ...
Class 0:
Remaining flits: 100386 100387 100388 100389 100390 100391 100392 100393 100394 100395 [...] (652630 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (423003 flits)
Class 0:
Remaining flits: 104276 104277 104278 104279 104280 104281 104282 104283 104284 104285 [...] (619950 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (420144 flits)
Class 0:
Remaining flits: 113454 113455 113456 113457 113458 113459 113460 113461 113462 113463 [...] (587020 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (416606 flits)
Class 0:
Remaining flits: 143370 143371 143372 143373 143374 143375 143376 143377 143378 143379 [...] (554897 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (411883 flits)
Class 0:
Remaining flits: 180306 180307 180308 180309 180310 180311 180312 180313 180314 180315 [...] (521703 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (405119 flits)
Class 0:
Remaining flits: 189738 189739 189740 189741 189742 189743 189744 189745 189746 189747 [...] (488692 flits)
Measured flits: 478548 478549 478550 478551 478552 478553 478554 478555 478556 478557 [...] (395726 flits)
Class 0:
Remaining flits: 198684 198685 198686 198687 198688 198689 198690 198691 198692 198693 [...] (455976 flits)
Measured flits: 478566 478567 478568 478569 478570 478571 478572 478573 478574 478575 [...] (384571 flits)
Class 0:
Remaining flits: 215244 215245 215246 215247 215248 215249 215250 215251 215252 215253 [...] (424274 flits)
Measured flits: 478566 478567 478568 478569 478570 478571 478572 478573 478574 478575 [...] (372118 flits)
Class 0:
Remaining flits: 227844 227845 227846 227847 227848 227849 227850 227851 227852 227853 [...] (392874 flits)
Measured flits: 478566 478567 478568 478569 478570 478571 478572 478573 478574 478575 [...] (356517 flits)
Class 0:
Remaining flits: 243000 243001 243002 243003 243004 243005 243006 243007 243008 243009 [...] (363809 flits)
Measured flits: 478566 478567 478568 478569 478570 478571 478572 478573 478574 478575 [...] (339431 flits)
Class 0:
Remaining flits: 254808 254809 254810 254811 254812 254813 254814 254815 254816 254817 [...] (334250 flits)
Measured flits: 478584 478585 478586 478587 478588 478589 478590 478591 478592 478593 [...] (318830 flits)
Class 0:
Remaining flits: 268056 268057 268058 268059 268060 268061 268062 268063 268064 268065 [...] (304721 flits)
Measured flits: 478818 478819 478820 478821 478822 478823 478824 478825 478826 478827 [...] (295584 flits)
Class 0:
Remaining flits: 274194 274195 274196 274197 274198 274199 274200 274201 274202 274203 [...] (275638 flits)
Measured flits: 478818 478819 478820 478821 478822 478823 478824 478825 478826 478827 [...] (270388 flits)
Class 0:
Remaining flits: 343836 343837 343838 343839 343840 343841 343842 343843 343844 343845 [...] (245235 flits)
Measured flits: 478818 478819 478820 478821 478822 478823 478824 478825 478826 478827 [...] (242358 flits)
Class 0:
Remaining flits: 364824 364825 364826 364827 364828 364829 364830 364831 364832 364833 [...] (216007 flits)
Measured flits: 478926 478927 478928 478929 478930 478931 478932 478933 478934 478935 [...] (214447 flits)
Class 0:
Remaining flits: 364824 364825 364826 364827 364828 364829 364830 364831 364832 364833 [...] (186707 flits)
Measured flits: 479250 479251 479252 479253 479254 479255 479256 479257 479258 479259 [...] (185842 flits)
Class 0:
Remaining flits: 389952 389953 389954 389955 389956 389957 389958 389959 389960 389961 [...] (157391 flits)
Measured flits: 479250 479251 479252 479253 479254 479255 479256 479257 479258 479259 [...] (156959 flits)
Class 0:
Remaining flits: 436824 436825 436826 436827 436828 436829 436830 436831 436832 436833 [...] (127544 flits)
Measured flits: 482688 482689 482690 482691 482692 482693 482694 482695 482696 482697 [...] (127372 flits)
Class 0:
Remaining flits: 436833 436834 436835 436836 436837 436838 436839 436840 436841 460260 [...] (98950 flits)
Measured flits: 484128 484129 484130 484131 484132 484133 484134 484135 484136 484137 [...] (98905 flits)
Class 0:
Remaining flits: 460260 460261 460262 460263 460264 460265 460266 460267 460268 460269 [...] (69828 flits)
Measured flits: 495800 495801 495802 495803 495804 495805 495806 495807 495808 495809 [...] (69810 flits)
Class 0:
Remaining flits: 535140 535141 535142 535143 535144 535145 535146 535147 535148 535149 [...] (40801 flits)
Measured flits: 535140 535141 535142 535143 535144 535145 535146 535147 535148 535149 [...] (40801 flits)
Class 0:
Remaining flits: 602820 602821 602822 602823 602824 602825 602826 602827 602828 602829 [...] (14305 flits)
Measured flits: 602820 602821 602822 602823 602824 602825 602826 602827 602828 602829 [...] (14305 flits)
Class 0:
Remaining flits: 760369 760370 760371 760372 760373 780300 780301 780302 780303 780304 [...] (659 flits)
Measured flits: 760369 760370 760371 760372 760373 780300 780301 780302 780303 780304 [...] (659 flits)
Time taken is 29323 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 15923.1 (1 samples)
	minimum = 27 (1 samples)
	maximum = 24477 (1 samples)
Network latency average = 15664.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 24349 (1 samples)
Flit latency average = 11955.8 (1 samples)
	minimum = 6 (1 samples)
	maximum = 24332 (1 samples)
Fragmentation average = 66.6495 (1 samples)
	minimum = 0 (1 samples)
	maximum = 176 (1 samples)
Injected packet rate average = 0.0420781 (1 samples)
	minimum = 0.013 (1 samples)
	maximum = 0.0556667 (1 samples)
Accepted packet rate average = 0.0103125 (1 samples)
	minimum = 0.006 (1 samples)
	maximum = 0.0143333 (1 samples)
Injected flit rate average = 0.757656 (1 samples)
	minimum = 0.238667 (1 samples)
	maximum = 1 (1 samples)
Accepted flit rate average = 0.185155 (1 samples)
	minimum = 0.113667 (1 samples)
	maximum = 0.253 (1 samples)
Injected packet size average = 18.0059 (1 samples)
Accepted packet size average = 17.9544 (1 samples)
Hops average = 5.21501 (1 samples)
Total run time 27.3507
