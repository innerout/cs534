BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 247.977
	minimum = 23
	maximum = 935
Network latency average = 244.095
	minimum = 23
	maximum = 914
Slowest packet = 239
Flit latency average = 205.485
	minimum = 6
	maximum = 897
Slowest flit = 4319
Fragmentation average = 58.0747
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0200625
	minimum = 0.009 (at node 91)
	maximum = 0.033 (at node 106)
Accepted packet rate average = 0.00941146
	minimum = 0.003 (at node 174)
	maximum = 0.019 (at node 70)
Injected flit rate average = 0.357859
	minimum = 0.162 (at node 91)
	maximum = 0.586 (at node 106)
Accepted flit rate average= 0.176792
	minimum = 0.056 (at node 178)
	maximum = 0.357 (at node 70)
Injected packet length average = 17.8372
Accepted packet length average = 18.7847
Total in-flight flits = 35392 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 463.442
	minimum = 23
	maximum = 1727
Network latency average = 458.048
	minimum = 23
	maximum = 1727
Slowest packet = 809
Flit latency average = 418.232
	minimum = 6
	maximum = 1783
Slowest flit = 13855
Fragmentation average = 62.8901
	minimum = 0
	maximum = 161
Injected packet rate average = 0.0196406
	minimum = 0.013 (at node 127)
	maximum = 0.029 (at node 13)
Accepted packet rate average = 0.00973958
	minimum = 0.0045 (at node 108)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.351849
	minimum = 0.234 (at node 127)
	maximum = 0.522 (at node 123)
Accepted flit rate average= 0.179211
	minimum = 0.0815 (at node 108)
	maximum = 0.316 (at node 22)
Injected packet length average = 17.9143
Accepted packet length average = 18.4003
Total in-flight flits = 67173 (0 measured)
latency change    = 0.464924
throughput change = 0.0134996
Class 0:
Packet latency average = 1142.84
	minimum = 23
	maximum = 2581
Network latency average = 1126.24
	minimum = 23
	maximum = 2550
Slowest packet = 1215
Flit latency average = 1098.52
	minimum = 6
	maximum = 2533
Slowest flit = 21887
Fragmentation average = 67.4917
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0175573
	minimum = 0.001 (at node 28)
	maximum = 0.032 (at node 46)
Accepted packet rate average = 0.00972396
	minimum = 0.003 (at node 83)
	maximum = 0.019 (at node 151)
Injected flit rate average = 0.316964
	minimum = 0.012 (at node 28)
	maximum = 0.576 (at node 46)
Accepted flit rate average= 0.174479
	minimum = 0.062 (at node 61)
	maximum = 0.344 (at node 151)
Injected packet length average = 18.0531
Accepted packet length average = 17.9432
Total in-flight flits = 94927 (0 measured)
latency change    = 0.594482
throughput change = 0.0271194
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 329.046
	minimum = 23
	maximum = 2011
Network latency average = 203.138
	minimum = 23
	maximum = 901
Slowest packet = 11011
Flit latency average = 1622.04
	minimum = 6
	maximum = 3426
Slowest flit = 36989
Fragmentation average = 22.8077
	minimum = 0
	maximum = 140
Injected packet rate average = 0.0132604
	minimum = 0 (at node 12)
	maximum = 0.031 (at node 90)
Accepted packet rate average = 0.00881771
	minimum = 0.003 (at node 123)
	maximum = 0.017 (at node 160)
Injected flit rate average = 0.238651
	minimum = 0 (at node 12)
	maximum = 0.548 (at node 90)
Accepted flit rate average= 0.158667
	minimum = 0.061 (at node 135)
	maximum = 0.298 (at node 27)
Injected packet length average = 17.9973
Accepted packet length average = 17.9941
Total in-flight flits = 110993 (44110 measured)
latency change    = 2.47319
throughput change = 0.0996586
Class 0:
Packet latency average = 1041.46
	minimum = 23
	maximum = 2851
Network latency average = 693.787
	minimum = 23
	maximum = 1893
Slowest packet = 11011
Flit latency average = 1831.39
	minimum = 6
	maximum = 4403
Slowest flit = 40099
Fragmentation average = 45.0466
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0123047
	minimum = 0 (at node 160)
	maximum = 0.0275 (at node 121)
Accepted packet rate average = 0.0089349
	minimum = 0.004 (at node 36)
	maximum = 0.0145 (at node 16)
Injected flit rate average = 0.221117
	minimum = 0 (at node 160)
	maximum = 0.491 (at node 121)
Accepted flit rate average= 0.161529
	minimum = 0.0715 (at node 36)
	maximum = 0.2575 (at node 136)
Injected packet length average = 17.9702
Accepted packet length average = 18.0784
Total in-flight flits = 119030 (76727 measured)
latency change    = 0.684052
throughput change = 0.0177181
Class 0:
Packet latency average = 1637.18
	minimum = 23
	maximum = 4048
Network latency average = 1200.05
	minimum = 23
	maximum = 2893
Slowest packet = 11011
Flit latency average = 2057.48
	minimum = 6
	maximum = 5045
Slowest flit = 33750
Fragmentation average = 56.3596
	minimum = 0
	maximum = 146
Injected packet rate average = 0.0115295
	minimum = 0.00133333 (at node 64)
	maximum = 0.0236667 (at node 93)
Accepted packet rate average = 0.00901563
	minimum = 0.00433333 (at node 17)
	maximum = 0.014 (at node 16)
Injected flit rate average = 0.207663
	minimum = 0.024 (at node 160)
	maximum = 0.426 (at node 93)
Accepted flit rate average= 0.162366
	minimum = 0.0763333 (at node 17)
	maximum = 0.255667 (at node 136)
Injected packet length average = 18.0114
Accepted packet length average = 18.0094
Total in-flight flits = 122508 (98731 measured)
latency change    = 0.36387
throughput change = 0.00515916
Class 0:
Packet latency average = 2232.59
	minimum = 23
	maximum = 4994
Network latency average = 1716.85
	minimum = 23
	maximum = 3954
Slowest packet = 11011
Flit latency average = 2272.93
	minimum = 6
	maximum = 5655
Slowest flit = 56033
Fragmentation average = 62.5522
	minimum = 0
	maximum = 155
Injected packet rate average = 0.01075
	minimum = 0.001 (at node 160)
	maximum = 0.022 (at node 138)
Accepted packet rate average = 0.00894271
	minimum = 0.005 (at node 135)
	maximum = 0.01325 (at node 16)
Injected flit rate average = 0.193534
	minimum = 0.018 (at node 160)
	maximum = 0.39775 (at node 138)
Accepted flit rate average= 0.161307
	minimum = 0.09325 (at node 135)
	maximum = 0.2375 (at node 16)
Injected packet length average = 18.0031
Accepted packet length average = 18.0379
Total in-flight flits = 121289 (108488 measured)
latency change    = 0.266692
throughput change = 0.00656528
Draining remaining packets ...
Class 0:
Remaining flits: 78279 78280 78281 92898 92899 92900 92901 92902 92903 92904 [...] (91937 flits)
Measured flits: 197262 197263 197264 197265 197266 197267 197268 197269 197270 197271 [...] (85672 flits)
Class 0:
Remaining flits: 92898 92899 92900 92901 92902 92903 92904 92905 92906 92907 [...] (63642 flits)
Measured flits: 197444 197445 197446 197447 197448 197449 197450 197451 197452 197453 [...] (61118 flits)
Class 0:
Remaining flits: 113260 113261 113262 113263 113264 113265 113266 113267 113268 113269 [...] (34558 flits)
Measured flits: 197658 197659 197660 197661 197662 197663 197664 197665 197666 197667 [...] (33999 flits)
Class 0:
Remaining flits: 128642 128643 128644 128645 136836 136837 136838 136839 136840 136841 [...] (9851 flits)
Measured flits: 199753 199754 199755 199756 199757 199758 199759 199760 199761 199762 [...] (9811 flits)
Time taken is 11984 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 4198.41 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8982 (1 samples)
Network latency average = 3456.4 (1 samples)
	minimum = 23 (1 samples)
	maximum = 8508 (1 samples)
Flit latency average = 3237.54 (1 samples)
	minimum = 6 (1 samples)
	maximum = 9250 (1 samples)
Fragmentation average = 66.5365 (1 samples)
	minimum = 0 (1 samples)
	maximum = 165 (1 samples)
Injected packet rate average = 0.01075 (1 samples)
	minimum = 0.001 (1 samples)
	maximum = 0.022 (1 samples)
Accepted packet rate average = 0.00894271 (1 samples)
	minimum = 0.005 (1 samples)
	maximum = 0.01325 (1 samples)
Injected flit rate average = 0.193534 (1 samples)
	minimum = 0.018 (1 samples)
	maximum = 0.39775 (1 samples)
Accepted flit rate average = 0.161307 (1 samples)
	minimum = 0.09325 (1 samples)
	maximum = 0.2375 (1 samples)
Injected packet size average = 18.0031 (1 samples)
Accepted packet size average = 18.0379 (1 samples)
Hops average = 5.11873 (1 samples)
Total run time 12.2687
