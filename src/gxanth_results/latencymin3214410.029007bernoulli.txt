BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.029007
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 300.432
	minimum = 23
	maximum = 968
Network latency average = 292.728
	minimum = 23
	maximum = 968
Slowest packet = 116
Flit latency average = 226.645
	minimum = 6
	maximum = 951
Slowest flit = 2105
Fragmentation average = 154.118
	minimum = 0
	maximum = 864
Injected packet rate average = 0.0288333
	minimum = 0.013 (at node 128)
	maximum = 0.043 (at node 3)
Accepted packet rate average = 0.0103906
	minimum = 0.003 (at node 150)
	maximum = 0.018 (at node 103)
Injected flit rate average = 0.514932
	minimum = 0.232 (at node 128)
	maximum = 0.774 (at node 3)
Accepted flit rate average= 0.213339
	minimum = 0.078 (at node 127)
	maximum = 0.345 (at node 136)
Injected packet length average = 17.8589
Accepted packet length average = 20.5318
Total in-flight flits = 58687 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 544.026
	minimum = 23
	maximum = 1846
Network latency average = 535.155
	minimum = 23
	maximum = 1845
Slowest packet = 309
Flit latency average = 458.51
	minimum = 6
	maximum = 1886
Slowest flit = 8618
Fragmentation average = 188.042
	minimum = 0
	maximum = 1770
Injected packet rate average = 0.0285703
	minimum = 0.018 (at node 134)
	maximum = 0.0375 (at node 191)
Accepted packet rate average = 0.0116328
	minimum = 0.006 (at node 30)
	maximum = 0.0175 (at node 119)
Injected flit rate average = 0.512034
	minimum = 0.324 (at node 134)
	maximum = 0.668 (at node 191)
Accepted flit rate average= 0.223003
	minimum = 0.1175 (at node 30)
	maximum = 0.343 (at node 177)
Injected packet length average = 17.9219
Accepted packet length average = 19.1701
Total in-flight flits = 111863 (0 measured)
latency change    = 0.447762
throughput change = 0.0433361
Class 0:
Packet latency average = 1276.66
	minimum = 24
	maximum = 2697
Network latency average = 1263.16
	minimum = 24
	maximum = 2676
Slowest packet = 1057
Flit latency average = 1204.42
	minimum = 6
	maximum = 2860
Slowest flit = 12152
Fragmentation average = 226.132
	minimum = 1
	maximum = 1762
Injected packet rate average = 0.0265
	minimum = 0.012 (at node 120)
	maximum = 0.041 (at node 74)
Accepted packet rate average = 0.012526
	minimum = 0.004 (at node 2)
	maximum = 0.023 (at node 12)
Injected flit rate average = 0.477156
	minimum = 0.222 (at node 120)
	maximum = 0.738 (at node 74)
Accepted flit rate average= 0.22538
	minimum = 0.083 (at node 118)
	maximum = 0.396 (at node 23)
Injected packet length average = 18.0059
Accepted packet length average = 17.9929
Total in-flight flits = 160192 (0 measured)
latency change    = 0.573869
throughput change = 0.0105493
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 238.412
	minimum = 27
	maximum = 1470
Network latency average = 113.83
	minimum = 25
	maximum = 853
Slowest packet = 16176
Flit latency average = 1738.62
	minimum = 6
	maximum = 3832
Slowest flit = 12167
Fragmentation average = 14.3707
	minimum = 0
	maximum = 245
Injected packet rate average = 0.0259063
	minimum = 0.003 (at node 160)
	maximum = 0.046 (at node 11)
Accepted packet rate average = 0.0121302
	minimum = 0.006 (at node 2)
	maximum = 0.023 (at node 106)
Injected flit rate average = 0.465115
	minimum = 0.054 (at node 160)
	maximum = 0.818 (at node 11)
Accepted flit rate average= 0.218328
	minimum = 0.092 (at node 105)
	maximum = 0.426 (at node 106)
Injected packet length average = 17.9538
Accepted packet length average = 17.9987
Total in-flight flits = 207805 (84172 measured)
latency change    = 4.35487
throughput change = 0.0323004
Class 0:
Packet latency average = 546.363
	minimum = 27
	maximum = 2794
Network latency average = 342.427
	minimum = 25
	maximum = 1955
Slowest packet = 16176
Flit latency average = 2023.96
	minimum = 6
	maximum = 4757
Slowest flit = 15443
Fragmentation average = 28.2855
	minimum = 0
	maximum = 607
Injected packet rate average = 0.0258516
	minimum = 0.009 (at node 124)
	maximum = 0.0395 (at node 41)
Accepted packet rate average = 0.0121406
	minimum = 0.0065 (at node 105)
	maximum = 0.018 (at node 34)
Injected flit rate average = 0.464883
	minimum = 0.1605 (at node 124)
	maximum = 0.711 (at node 41)
Accepted flit rate average= 0.217664
	minimum = 0.117 (at node 105)
	maximum = 0.329 (at node 59)
Injected packet length average = 17.9828
Accepted packet length average = 17.9286
Total in-flight flits = 255295 (167072 measured)
latency change    = 0.563639
throughput change = 0.00305086
Class 0:
Packet latency average = 1218.88
	minimum = 27
	maximum = 3857
Network latency average = 950.803
	minimum = 25
	maximum = 2898
Slowest packet = 16176
Flit latency average = 2274.62
	minimum = 6
	maximum = 5524
Slowest flit = 39725
Fragmentation average = 73.506
	minimum = 0
	maximum = 966
Injected packet rate average = 0.0249931
	minimum = 0.006 (at node 124)
	maximum = 0.0353333 (at node 41)
Accepted packet rate average = 0.0120347
	minimum = 0.00733333 (at node 54)
	maximum = 0.0173333 (at node 34)
Injected flit rate average = 0.449625
	minimum = 0.109667 (at node 124)
	maximum = 0.633 (at node 41)
Accepted flit rate average= 0.215847
	minimum = 0.134333 (at node 54)
	maximum = 0.309 (at node 34)
Injected packet length average = 17.99
Accepted packet length average = 17.9354
Total in-flight flits = 295100 (235223 measured)
latency change    = 0.551752
throughput change = 0.00841725
Class 0:
Packet latency average = 1894.48
	minimum = 27
	maximum = 4703
Network latency average = 1602.79
	minimum = 25
	maximum = 3955
Slowest packet = 16176
Flit latency average = 2530.46
	minimum = 6
	maximum = 6387
Slowest flit = 42665
Fragmentation average = 106.967
	minimum = 0
	maximum = 1059
Injected packet rate average = 0.0240052
	minimum = 0.00825 (at node 124)
	maximum = 0.032 (at node 101)
Accepted packet rate average = 0.0119154
	minimum = 0.00825 (at node 54)
	maximum = 0.01575 (at node 19)
Injected flit rate average = 0.43165
	minimum = 0.1475 (at node 124)
	maximum = 0.57425 (at node 101)
Accepted flit rate average= 0.21331
	minimum = 0.151 (at node 190)
	maximum = 0.28125 (at node 19)
Injected packet length average = 17.9815
Accepted packet length average = 17.9021
Total in-flight flits = 328470 (288986 measured)
latency change    = 0.356612
throughput change = 0.011895
Class 0:
Packet latency average = 2503.53
	minimum = 25
	maximum = 5591
Network latency average = 2167.8
	minimum = 25
	maximum = 4934
Slowest packet = 16176
Flit latency average = 2775.3
	minimum = 6
	maximum = 7008
Slowest flit = 42666
Fragmentation average = 126.728
	minimum = 0
	maximum = 1846
Injected packet rate average = 0.0224979
	minimum = 0.0066 (at node 124)
	maximum = 0.0302 (at node 43)
Accepted packet rate average = 0.0117604
	minimum = 0.0086 (at node 122)
	maximum = 0.0154 (at node 103)
Injected flit rate average = 0.404661
	minimum = 0.1182 (at node 124)
	maximum = 0.5426 (at node 43)
Accepted flit rate average= 0.211397
	minimum = 0.155 (at node 64)
	maximum = 0.2874 (at node 142)
Injected packet length average = 17.9866
Accepted packet length average = 17.9753
Total in-flight flits = 346699 (320835 measured)
latency change    = 0.243277
throughput change = 0.00904943
Class 0:
Packet latency average = 3061.38
	minimum = 25
	maximum = 6634
Network latency average = 2700.19
	minimum = 23
	maximum = 5915
Slowest packet = 16176
Flit latency average = 3036.31
	minimum = 6
	maximum = 8067
Slowest flit = 84455
Fragmentation average = 141.646
	minimum = 0
	maximum = 1846
Injected packet rate average = 0.0211311
	minimum = 0.01 (at node 76)
	maximum = 0.0275 (at node 102)
Accepted packet rate average = 0.0116684
	minimum = 0.00833333 (at node 64)
	maximum = 0.0153333 (at node 188)
Injected flit rate average = 0.380113
	minimum = 0.1825 (at node 76)
	maximum = 0.494833 (at node 102)
Accepted flit rate average= 0.20912
	minimum = 0.152333 (at node 64)
	maximum = 0.278833 (at node 188)
Injected packet length average = 17.9883
Accepted packet length average = 17.9219
Total in-flight flits = 358468 (341979 measured)
latency change    = 0.182222
throughput change = 0.0108889
Class 0:
Packet latency average = 3552.33
	minimum = 25
	maximum = 7545
Network latency average = 3151
	minimum = 23
	maximum = 6933
Slowest packet = 16176
Flit latency average = 3290.41
	minimum = 6
	maximum = 9233
Slowest flit = 74211
Fragmentation average = 149.103
	minimum = 0
	maximum = 2384
Injected packet rate average = 0.0198601
	minimum = 0.00857143 (at node 76)
	maximum = 0.0262857 (at node 167)
Accepted packet rate average = 0.0115432
	minimum = 0.00828571 (at node 64)
	maximum = 0.0144286 (at node 90)
Injected flit rate average = 0.357383
	minimum = 0.156429 (at node 76)
	maximum = 0.473143 (at node 167)
Accepted flit rate average= 0.207281
	minimum = 0.147286 (at node 64)
	maximum = 0.263143 (at node 188)
Injected packet length average = 17.995
Accepted packet length average = 17.9571
Total in-flight flits = 363232 (352804 measured)
latency change    = 0.138205
throughput change = 0.00886979
Draining all recorded packets ...
Class 0:
Remaining flits: 71262 71263 71264 71265 71266 71267 71268 71269 71270 71271 [...] (363835 flits)
Measured flits: 289152 289153 289154 289155 289156 289157 289158 289159 289160 289161 [...] (357044 flits)
Class 0:
Remaining flits: 71262 71263 71264 71265 71266 71267 71268 71269 71270 71271 [...] (359658 flits)
Measured flits: 289152 289153 289154 289155 289156 289157 289158 289159 289160 289161 [...] (353875 flits)
Class 0:
Remaining flits: 71262 71263 71264 71265 71266 71267 71268 71269 71270 71271 [...] (356857 flits)
Measured flits: 289152 289153 289154 289155 289156 289157 289158 289159 289160 289161 [...] (347803 flits)
Class 0:
Remaining flits: 121554 121555 121556 121557 121558 121559 121560 121561 121562 121563 [...] (355624 flits)
Measured flits: 289152 289153 289154 289155 289156 289157 289158 289159 289160 289161 [...] (339390 flits)
Class 0:
Remaining flits: 154386 154387 154388 154389 154390 154391 154392 154393 154394 154395 [...] (352484 flits)
Measured flits: 289152 289153 289154 289155 289156 289157 289158 289159 289160 289161 [...] (325836 flits)
Class 0:
Remaining flits: 154386 154387 154388 154389 154390 154391 154392 154393 154394 154395 [...] (350806 flits)
Measured flits: 289192 289193 289194 289195 289196 289197 289198 289199 289200 289201 [...] (307585 flits)
Class 0:
Remaining flits: 219258 219259 219260 219261 219262 219263 219264 219265 219266 219267 [...] (348542 flits)
Measured flits: 291294 291295 291296 291297 291298 291299 291300 291301 291302 291303 [...] (288103 flits)
Class 0:
Remaining flits: 219267 219268 219269 219270 219271 219272 219273 219274 219275 222264 [...] (346143 flits)
Measured flits: 291294 291295 291296 291297 291298 291299 291300 291301 291302 291303 [...] (262144 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (343724 flits)
Measured flits: 291294 291295 291296 291297 291298 291299 291300 291301 291302 291303 [...] (235597 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (344473 flits)
Measured flits: 291294 291295 291296 291297 291298 291299 291300 291301 291302 291303 [...] (211027 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (340536 flits)
Measured flits: 348579 348580 348581 348582 348583 348584 348585 348586 348587 349866 [...] (183635 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (336554 flits)
Measured flits: 349866 349867 349868 349869 349870 349871 349872 349873 349874 349875 [...] (158811 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (333810 flits)
Measured flits: 351936 351937 351938 351939 351940 351941 351942 351943 351944 351945 [...] (134564 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (331600 flits)
Measured flits: 382554 382555 382556 382557 382558 382559 382560 382561 382562 382563 [...] (113469 flits)
Class 0:
Remaining flits: 222264 222265 222266 222267 222268 222269 222270 222271 222272 222273 [...] (330570 flits)
Measured flits: 382554 382555 382556 382557 382558 382559 382560 382561 382562 382563 [...] (94773 flits)
Class 0:
Remaining flits: 385748 385749 385750 385751 385752 385753 385754 385755 385756 385757 [...] (329921 flits)
Measured flits: 385748 385749 385750 385751 385752 385753 385754 385755 385756 385757 [...] (76710 flits)
Class 0:
Remaining flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (329291 flits)
Measured flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (61181 flits)
Class 0:
Remaining flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (330100 flits)
Measured flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (48595 flits)
Class 0:
Remaining flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (330325 flits)
Measured flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (38672 flits)
Class 0:
Remaining flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (328277 flits)
Measured flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (29986 flits)
Class 0:
Remaining flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (325636 flits)
Measured flits: 415260 415261 415262 415263 415264 415265 415266 415267 415268 415269 [...] (23177 flits)
Class 0:
Remaining flits: 467442 467443 467444 467445 467446 467447 467448 467449 467450 467451 [...] (324723 flits)
Measured flits: 467442 467443 467444 467445 467446 467447 467448 467449 467450 467451 [...] (17795 flits)
Class 0:
Remaining flits: 467442 467443 467444 467445 467446 467447 467448 467449 467450 467451 [...] (323847 flits)
Measured flits: 467442 467443 467444 467445 467446 467447 467448 467449 467450 467451 [...] (13910 flits)
Class 0:
Remaining flits: 519120 519121 519122 519123 519124 519125 519126 519127 519128 519129 [...] (327401 flits)
Measured flits: 519120 519121 519122 519123 519124 519125 519126 519127 519128 519129 [...] (10389 flits)
Class 0:
Remaining flits: 519120 519121 519122 519123 519124 519125 519126 519127 519128 519129 [...] (326450 flits)
Measured flits: 519120 519121 519122 519123 519124 519125 519126 519127 519128 519129 [...] (8077 flits)
Class 0:
Remaining flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (329004 flits)
Measured flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (5928 flits)
Class 0:
Remaining flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (327474 flits)
Measured flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (4376 flits)
Class 0:
Remaining flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (325612 flits)
Measured flits: 588672 588673 588674 588675 588676 588677 588678 588679 588680 588681 [...] (3190 flits)
Class 0:
Remaining flits: 588679 588680 588681 588682 588683 588684 588685 588686 588687 588688 [...] (324964 flits)
Measured flits: 588679 588680 588681 588682 588683 588684 588685 588686 588687 588688 [...] (2187 flits)
Class 0:
Remaining flits: 661050 661051 661052 661053 661054 661055 661056 661057 661058 661059 [...] (322810 flits)
Measured flits: 661050 661051 661052 661053 661054 661055 661056 661057 661058 661059 [...] (1415 flits)
Class 0:
Remaining flits: 669522 669523 669524 669525 669526 669527 678168 678169 678170 678171 [...] (324914 flits)
Measured flits: 669522 669523 669524 669525 669526 669527 678168 678169 678170 678171 [...] (1041 flits)
Class 0:
Remaining flits: 678184 678185 678816 678817 678818 678819 678820 678821 678822 678823 [...] (327870 flits)
Measured flits: 678184 678185 678816 678817 678818 678819 678820 678821 678822 678823 [...] (734 flits)
Class 0:
Remaining flits: 849564 849565 849566 849567 849568 849569 849570 849571 849572 849573 [...] (326940 flits)
Measured flits: 849564 849565 849566 849567 849568 849569 849570 849571 849572 849573 [...] (532 flits)
Class 0:
Remaining flits: 849564 849565 849566 849567 849568 849569 849570 849571 849572 849573 [...] (322174 flits)
Measured flits: 849564 849565 849566 849567 849568 849569 849570 849571 849572 849573 [...] (338 flits)
Class 0:
Remaining flits: 889398 889399 889400 889401 889402 889403 889404 889405 889406 889407 [...] (323897 flits)
Measured flits: 889398 889399 889400 889401 889402 889403 889404 889405 889406 889407 [...] (255 flits)
Class 0:
Remaining flits: 889398 889399 889400 889401 889402 889403 889404 889405 889406 889407 [...] (319349 flits)
Measured flits: 889398 889399 889400 889401 889402 889403 889404 889405 889406 889407 [...] (176 flits)
Class 0:
Remaining flits: 1030662 1030663 1030664 1030665 1030666 1030667 1030668 1030669 1030670 1030671 [...] (318314 flits)
Measured flits: 1121922 1121923 1121924 1121925 1121926 1121927 1121928 1121929 1121930 1121931 [...] (114 flits)
Class 0:
Remaining flits: 1030662 1030663 1030664 1030665 1030666 1030667 1030668 1030669 1030670 1030671 [...] (323452 flits)
Measured flits: 1121930 1121931 1121932 1121933 1121934 1121935 1121936 1121937 1121938 1121939 [...] (64 flits)
Class 0:
Remaining flits: 1030662 1030663 1030664 1030665 1030666 1030667 1030668 1030669 1030670 1030671 [...] (321048 flits)
Measured flits: 1269612 1269613 1269614 1269615 1269616 1269617 1269618 1269619 1269620 1269621 [...] (54 flits)
Class 0:
Remaining flits: 1059228 1059229 1059230 1059231 1059232 1059233 1059234 1059235 1059236 1059237 [...] (321473 flits)
Measured flits: 1269612 1269613 1269614 1269615 1269616 1269617 1269618 1269619 1269620 1269621 [...] (54 flits)
Class 0:
Remaining flits: 1059228 1059229 1059230 1059231 1059232 1059233 1059234 1059235 1059236 1059237 [...] (321887 flits)
Measured flits: 1269612 1269613 1269614 1269615 1269616 1269617 1269618 1269619 1269620 1269621 [...] (36 flits)
Class 0:
Remaining flits: 1094195 1094196 1094197 1094198 1094199 1094200 1094201 1181142 1181143 1181144 [...] (322094 flits)
Measured flits: 1269612 1269613 1269614 1269615 1269616 1269617 1269618 1269619 1269620 1269621 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1181142 1181143 1181144 1181145 1181146 1181147 1181148 1181149 1181150 1181151 [...] (285820 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1181142 1181143 1181144 1181145 1181146 1181147 1181148 1181149 1181150 1181151 [...] (250797 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1278000 1278001 1278002 1278003 1278004 1278005 1278006 1278007 1278008 1278009 [...] (217802 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1278012 1278013 1278014 1278015 1278016 1278017 1292256 1292257 1292258 1292259 [...] (183269 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1292256 1292257 1292258 1292259 1292260 1292261 1292262 1292263 1292264 1292265 [...] (149727 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1292256 1292257 1292258 1292259 1292260 1292261 1292262 1292263 1292264 1292265 [...] (117250 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1309122 1309123 1309124 1309125 1309126 1309127 1309128 1309129 1309130 1309131 [...] (87323 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1340892 1340893 1340894 1340895 1340896 1340897 1340898 1340899 1340900 1340901 [...] (61097 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1404954 1404955 1404956 1404957 1404958 1404959 1404960 1404961 1404962 1404963 [...] (38458 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1404954 1404955 1404956 1404957 1404958 1404959 1404960 1404961 1404962 1404963 [...] (20379 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1404955 1404956 1404957 1404958 1404959 1404960 1404961 1404962 1404963 1404964 [...] (8109 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1707804 1707805 1707806 1707807 1707808 1707809 1707810 1707811 1707812 1707813 [...] (2294 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2013616 2013617 2013618 2013619 2013620 2013621 2013622 2013623 2094582 2094583 [...] (46 flits)
Measured flits: (0 flits)
Time taken is 65287 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 10691.6 (1 samples)
	minimum = 25 (1 samples)
	maximum = 42331 (1 samples)
Network latency average = 8276.88 (1 samples)
	minimum = 23 (1 samples)
	maximum = 34230 (1 samples)
Flit latency average = 8282.26 (1 samples)
	minimum = 6 (1 samples)
	maximum = 35342 (1 samples)
Fragmentation average = 200.138 (1 samples)
	minimum = 0 (1 samples)
	maximum = 7227 (1 samples)
Injected packet rate average = 0.0198601 (1 samples)
	minimum = 0.00857143 (1 samples)
	maximum = 0.0262857 (1 samples)
Accepted packet rate average = 0.0115432 (1 samples)
	minimum = 0.00828571 (1 samples)
	maximum = 0.0144286 (1 samples)
Injected flit rate average = 0.357383 (1 samples)
	minimum = 0.156429 (1 samples)
	maximum = 0.473143 (1 samples)
Accepted flit rate average = 0.207281 (1 samples)
	minimum = 0.147286 (1 samples)
	maximum = 0.263143 (1 samples)
Injected packet size average = 17.995 (1 samples)
Accepted packet size average = 17.9571 (1 samples)
Hops average = 5.06295 (1 samples)
Total run time 82.2314
