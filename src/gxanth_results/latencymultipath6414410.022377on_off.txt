BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 316.408
	minimum = 25
	maximum = 979
Network latency average = 250.708
	minimum = 25
	maximum = 961
Slowest packet = 3
Flit latency average = 180.184
	minimum = 6
	maximum = 944
Slowest flit = 71
Fragmentation average = 144.513
	minimum = 0
	maximum = 899
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00931771
	minimum = 0.002 (at node 30)
	maximum = 0.017 (at node 22)
Injected flit rate average = 0.375698
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.196094
	minimum = 0.073 (at node 174)
	maximum = 0.338 (at node 44)
Injected packet length average = 17.8417
Accepted packet length average = 21.0453
Total in-flight flits = 35124 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 511.323
	minimum = 25
	maximum = 1868
Network latency average = 418.977
	minimum = 23
	maximum = 1762
Slowest packet = 209
Flit latency average = 328.94
	minimum = 6
	maximum = 1902
Slowest flit = 7908
Fragmentation average = 206.846
	minimum = 0
	maximum = 1583
Injected packet rate average = 0.0216094
	minimum = 0.0015 (at node 130)
	maximum = 0.0495 (at node 137)
Accepted packet rate average = 0.0107891
	minimum = 0.006 (at node 96)
	maximum = 0.016 (at node 14)
Injected flit rate average = 0.387438
	minimum = 0.027 (at node 130)
	maximum = 0.8855 (at node 137)
Accepted flit rate average= 0.213411
	minimum = 0.121 (at node 96)
	maximum = 0.3275 (at node 88)
Injected packet length average = 17.9291
Accepted packet length average = 19.7804
Total in-flight flits = 67414 (0 measured)
latency change    = 0.381198
throughput change = 0.081147
Class 0:
Packet latency average = 1021.56
	minimum = 31
	maximum = 2951
Network latency average = 894.927
	minimum = 25
	maximum = 2887
Slowest packet = 345
Flit latency average = 786.826
	minimum = 6
	maximum = 2876
Slowest flit = 8409
Fragmentation average = 308.613
	minimum = 0
	maximum = 2548
Injected packet rate average = 0.0219479
	minimum = 0 (at node 131)
	maximum = 0.056 (at node 105)
Accepted packet rate average = 0.012724
	minimum = 0.005 (at node 53)
	maximum = 0.021 (at node 6)
Injected flit rate average = 0.394385
	minimum = 0 (at node 131)
	maximum = 1 (at node 55)
Accepted flit rate average= 0.234755
	minimum = 0.107 (at node 185)
	maximum = 0.375 (at node 191)
Injected packet length average = 17.9692
Accepted packet length average = 18.4499
Total in-flight flits = 98193 (0 measured)
latency change    = 0.499468
throughput change = 0.0909192
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 406.096
	minimum = 28
	maximum = 2752
Network latency average = 266.957
	minimum = 23
	maximum = 933
Slowest packet = 12518
Flit latency average = 1089.07
	minimum = 6
	maximum = 3813
Slowest flit = 11100
Fragmentation average = 140.186
	minimum = 0
	maximum = 748
Injected packet rate average = 0.0221562
	minimum = 0 (at node 57)
	maximum = 0.056 (at node 55)
Accepted packet rate average = 0.0130521
	minimum = 0.004 (at node 21)
	maximum = 0.023 (at node 27)
Injected flit rate average = 0.398849
	minimum = 0 (at node 57)
	maximum = 1 (at node 55)
Accepted flit rate average= 0.23588
	minimum = 0.05 (at node 76)
	maximum = 0.396 (at node 66)
Injected packet length average = 18.0016
Accepted packet length average = 18.0722
Total in-flight flits = 129476 (63389 measured)
latency change    = 1.51556
throughput change = 0.00476937
Class 0:
Packet latency average = 682.863
	minimum = 27
	maximum = 2991
Network latency average = 556.639
	minimum = 23
	maximum = 1959
Slowest packet = 12518
Flit latency average = 1256.58
	minimum = 6
	maximum = 4830
Slowest flit = 11243
Fragmentation average = 212.26
	minimum = 0
	maximum = 1647
Injected packet rate average = 0.021888
	minimum = 0.005 (at node 85)
	maximum = 0.0515 (at node 186)
Accepted packet rate average = 0.0129323
	minimum = 0.0065 (at node 190)
	maximum = 0.022 (at node 128)
Injected flit rate average = 0.393781
	minimum = 0.09 (at node 85)
	maximum = 0.9235 (at node 186)
Accepted flit rate average= 0.236086
	minimum = 0.1125 (at node 43)
	maximum = 0.3975 (at node 128)
Injected packet length average = 17.9907
Accepted packet length average = 18.2555
Total in-flight flits = 158826 (113027 measured)
latency change    = 0.405305
throughput change = 0.000871416
Class 0:
Packet latency average = 976.762
	minimum = 27
	maximum = 4037
Network latency average = 844.261
	minimum = 23
	maximum = 2995
Slowest packet = 12518
Flit latency average = 1446.6
	minimum = 6
	maximum = 5790
Slowest flit = 14303
Fragmentation average = 247.853
	minimum = 0
	maximum = 2240
Injected packet rate average = 0.0215087
	minimum = 0.00433333 (at node 57)
	maximum = 0.051 (at node 186)
Accepted packet rate average = 0.0130538
	minimum = 0.00666667 (at node 190)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.387314
	minimum = 0.078 (at node 57)
	maximum = 0.918 (at node 186)
Accepted flit rate average= 0.237512
	minimum = 0.131333 (at node 190)
	maximum = 0.343 (at node 130)
Injected packet length average = 18.0073
Accepted packet length average = 18.1948
Total in-flight flits = 184388 (153119 measured)
latency change    = 0.300891
throughput change = 0.00600481
Class 0:
Packet latency average = 1298.3
	minimum = 27
	maximum = 5499
Network latency average = 1152.43
	minimum = 23
	maximum = 3949
Slowest packet = 12518
Flit latency average = 1650.29
	minimum = 6
	maximum = 6660
Slowest flit = 10345
Fragmentation average = 273.1
	minimum = 0
	maximum = 2820
Injected packet rate average = 0.0210469
	minimum = 0.004 (at node 57)
	maximum = 0.0425 (at node 186)
Accepted packet rate average = 0.0130156
	minimum = 0.0085 (at node 190)
	maximum = 0.0185 (at node 128)
Injected flit rate average = 0.378867
	minimum = 0.072 (at node 57)
	maximum = 0.7625 (at node 186)
Accepted flit rate average= 0.235784
	minimum = 0.15975 (at node 190)
	maximum = 0.34 (at node 128)
Injected packet length average = 18.0011
Accepted packet length average = 18.1154
Total in-flight flits = 208063 (187017 measured)
latency change    = 0.247663
throughput change = 0.00733001
Class 0:
Packet latency average = 1620.45
	minimum = 27
	maximum = 6772
Network latency average = 1457.81
	minimum = 23
	maximum = 4893
Slowest packet = 12518
Flit latency average = 1843.19
	minimum = 6
	maximum = 7770
Slowest flit = 15741
Fragmentation average = 294.669
	minimum = 0
	maximum = 3724
Injected packet rate average = 0.0207229
	minimum = 0.008 (at node 118)
	maximum = 0.042 (at node 186)
Accepted packet rate average = 0.0129781
	minimum = 0.0094 (at node 64)
	maximum = 0.0186 (at node 128)
Injected flit rate average = 0.3729
	minimum = 0.144 (at node 118)
	maximum = 0.755 (at node 186)
Accepted flit rate average= 0.234874
	minimum = 0.172 (at node 26)
	maximum = 0.332 (at node 128)
Injected packet length average = 17.9946
Accepted packet length average = 18.0977
Total in-flight flits = 230806 (216599 measured)
latency change    = 0.198799
throughput change = 0.00387397
Class 0:
Packet latency average = 1915.84
	minimum = 27
	maximum = 7706
Network latency average = 1735.03
	minimum = 23
	maximum = 5926
Slowest packet = 12518
Flit latency average = 2022.45
	minimum = 6
	maximum = 8573
Slowest flit = 27294
Fragmentation average = 307.976
	minimum = 0
	maximum = 3813
Injected packet rate average = 0.0208437
	minimum = 0.00916667 (at node 169)
	maximum = 0.0401667 (at node 21)
Accepted packet rate average = 0.0129375
	minimum = 0.00966667 (at node 64)
	maximum = 0.0183333 (at node 128)
Injected flit rate average = 0.375095
	minimum = 0.165 (at node 169)
	maximum = 0.721167 (at node 21)
Accepted flit rate average= 0.233955
	minimum = 0.172667 (at node 162)
	maximum = 0.324333 (at node 128)
Injected packet length average = 17.9955
Accepted packet length average = 18.0835
Total in-flight flits = 260893 (250976 measured)
latency change    = 0.154185
throughput change = 0.00392852
Class 0:
Packet latency average = 2202.38
	minimum = 27
	maximum = 8003
Network latency average = 2007.32
	minimum = 23
	maximum = 6902
Slowest packet = 12518
Flit latency average = 2205.25
	minimum = 6
	maximum = 9844
Slowest flit = 9904
Fragmentation average = 321.222
	minimum = 0
	maximum = 5701
Injected packet rate average = 0.0208304
	minimum = 0.00928571 (at node 14)
	maximum = 0.0362857 (at node 21)
Accepted packet rate average = 0.0128728
	minimum = 0.00985714 (at node 162)
	maximum = 0.0175714 (at node 128)
Injected flit rate average = 0.374883
	minimum = 0.168714 (at node 14)
	maximum = 0.653143 (at node 21)
Accepted flit rate average= 0.232578
	minimum = 0.172571 (at node 162)
	maximum = 0.317 (at node 103)
Injected packet length average = 17.997
Accepted packet length average = 18.0675
Total in-flight flits = 289590 (282569 measured)
latency change    = 0.130104
throughput change = 0.00591946
Draining all recorded packets ...
Class 0:
Remaining flits: 14305 14306 14307 14308 14309 14634 14635 14636 14637 14638 [...] (315924 flits)
Measured flits: 225241 225242 225243 225244 225245 225246 225247 225248 225249 225250 [...] (262434 flits)
Class 0:
Remaining flits: 14305 14306 14307 14308 14309 14637 14638 14639 14640 14641 [...] (343307 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (236297 flits)
Class 0:
Remaining flits: 14642 14643 14644 14645 14646 14647 14648 14649 14650 14651 [...] (373030 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (209894 flits)
Class 0:
Remaining flits: 16906 16907 16908 16909 16910 16911 16912 16913 16914 16915 [...] (400258 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (184106 flits)
Class 0:
Remaining flits: 16912 16913 16914 16915 16916 16917 16918 16919 27776 27777 [...] (427089 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (161552 flits)
Class 0:
Remaining flits: 54594 54595 54596 54597 54598 54599 54600 54601 54602 54603 [...] (456235 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (142024 flits)
Class 0:
Remaining flits: 54594 54595 54596 54597 54598 54599 54600 54601 54602 54603 [...] (488696 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (125226 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (514382 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (110597 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (542939 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (98262 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (568633 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (84931 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (596259 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (74190 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (614491 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (65105 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (629054 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (56892 flits)
Class 0:
Remaining flits: 59508 59509 59510 59511 59512 59513 59514 59515 59516 59517 [...] (645301 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (49130 flits)
Class 0:
Remaining flits: 84690 84691 84692 84693 84694 84695 84696 84697 84698 84699 [...] (661590 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (42670 flits)
Class 0:
Remaining flits: 153792 153793 153794 153795 153796 153797 153798 153799 153800 153801 [...] (672646 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (36535 flits)
Class 0:
Remaining flits: 153792 153793 153794 153795 153796 153797 153798 153799 153800 153801 [...] (681535 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (31178 flits)
Class 0:
Remaining flits: 153792 153793 153794 153795 153796 153797 153798 153799 153800 153801 [...] (695097 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (26982 flits)
Class 0:
Remaining flits: 153792 153793 153794 153795 153796 153797 153798 153799 153800 153801 [...] (702708 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (23087 flits)
Class 0:
Remaining flits: 153792 153793 153794 153795 153796 153797 153798 153799 153800 153801 [...] (706523 flits)
Measured flits: 225288 225289 225290 225291 225292 225293 225294 225295 225296 225297 [...] (20402 flits)
Class 0:
Remaining flits: 153802 153803 153804 153805 153806 153807 153808 153809 178542 178543 [...] (711117 flits)
Measured flits: 225291 225292 225293 225294 225295 225296 225297 225298 225299 225300 [...] (17522 flits)
Class 0:
Remaining flits: 153802 153803 153804 153805 153806 153807 153808 153809 178542 178543 [...] (715032 flits)
Measured flits: 267140 267141 267142 267143 267144 267145 267146 267147 267148 267149 [...] (15541 flits)
Class 0:
Remaining flits: 153802 153803 153804 153805 153806 153807 153808 153809 178552 178553 [...] (714133 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (13224 flits)
Class 0:
Remaining flits: 153802 153803 153804 153805 153806 153807 153808 153809 207072 207073 [...] (709970 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (11324 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (708418 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (10001 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (708998 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (8828 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (712185 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (7603 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (712510 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (6592 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (713942 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (5571 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (716122 flits)
Measured flits: 278064 278065 278066 278067 278068 278069 278070 278071 278072 278073 [...] (4835 flits)
Class 0:
Remaining flits: 213048 213049 213050 213051 213052 213053 213054 213055 213056 213057 [...] (716736 flits)
Measured flits: 283806 283807 283808 283809 283810 283811 283812 283813 283814 283815 [...] (4224 flits)
Class 0:
Remaining flits: 401472 401473 401474 401475 401476 401477 401478 401479 401480 401481 [...] (715488 flits)
Measured flits: 401472 401473 401474 401475 401476 401477 401478 401479 401480 401481 [...] (3578 flits)
Class 0:
Remaining flits: 401472 401473 401474 401475 401476 401477 401478 401479 401480 401481 [...] (714648 flits)
Measured flits: 401472 401473 401474 401475 401476 401477 401478 401479 401480 401481 [...] (3009 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (715061 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (2563 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (716177 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (2215 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (718609 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (1834 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (720862 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (1641 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (718358 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (1318 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (718627 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (1132 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (715403 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (849 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (711369 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (777 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (707622 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (693 flits)
Class 0:
Remaining flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (707808 flits)
Measured flits: 424782 424783 424784 424785 424786 424787 424788 424789 424790 424791 [...] (434 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (702781 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (342 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (697985 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (315 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (695328 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (252 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (697135 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (234 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (700025 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (198 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (700966 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (180 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (701094 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (168 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (695347 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (90 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (692283 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (90 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (682918 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (90 flits)
Class 0:
Remaining flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (678190 flits)
Measured flits: 559818 559819 559820 559821 559822 559823 559824 559825 559826 559827 [...] (72 flits)
Class 0:
Remaining flits: 827730 827731 827732 827733 827734 827735 827736 827737 827738 827739 [...] (676461 flits)
Measured flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (54 flits)
Class 0:
Remaining flits: 827730 827731 827732 827733 827734 827735 827736 827737 827738 827739 [...] (675140 flits)
Measured flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (54 flits)
Class 0:
Remaining flits: 827730 827731 827732 827733 827734 827735 827736 827737 827738 827739 [...] (672035 flits)
Measured flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (54 flits)
Class 0:
Remaining flits: 827730 827731 827732 827733 827734 827735 827736 827737 827738 827739 [...] (667623 flits)
Measured flits: 1029636 1029637 1029638 1029639 1029640 1029641 1029642 1029643 1029644 1029645 [...] (54 flits)
Class 0:
Remaining flits: 827730 827731 827732 827733 827734 827735 827736 827737 827738 827739 [...] (667971 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 844830 844831 844832 844833 844834 844835 844836 844837 844838 844839 [...] (669215 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 844830 844831 844832 844833 844834 844835 844836 844837 844838 844839 [...] (673935 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 844830 844831 844832 844833 844834 844835 844836 844837 844838 844839 [...] (676683 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 844830 844831 844832 844833 844834 844835 844836 844837 844838 844839 [...] (672691 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 844830 844831 844832 844833 844834 844835 844836 844837 844838 844839 [...] (669935 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Class 0:
Remaining flits: 900000 900001 900002 900003 900004 900005 900006 900007 900008 900009 [...] (668411 flits)
Measured flits: 1248318 1248319 1248320 1248321 1248322 1248323 1248324 1248325 1248326 1248327 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (633194 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (599154 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (565544 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (531698 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (499079 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (465846 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (433001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (400658 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (368099 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (336054 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (304829 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (275351 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (246134 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032120 1032121 1032122 1032123 1032124 1032125 1032126 1032127 1032128 1032129 [...] (218900 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1032123 1032124 1032125 1032126 1032127 1032128 1032129 1032130 1032131 1032132 [...] (191378 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1171008 1171009 1171010 1171011 1171012 1171013 1171014 1171015 1171016 1171017 [...] (164313 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1279746 1279747 1279748 1279749 1279750 1279751 1279752 1279753 1279754 1279755 [...] (138405 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1279746 1279747 1279748 1279749 1279750 1279751 1279752 1279753 1279754 1279755 [...] (113606 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1372194 1372195 1372196 1372197 1372198 1372199 1372200 1372201 1372202 1372203 [...] (89461 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1400976 1400977 1400978 1400979 1400980 1400981 1400982 1400983 1400984 1400985 [...] (66721 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1400976 1400977 1400978 1400979 1400980 1400981 1400982 1400983 1400984 1400985 [...] (47479 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1400976 1400977 1400978 1400979 1400980 1400981 1400982 1400983 1400984 1400985 [...] (30814 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1400976 1400977 1400978 1400979 1400980 1400981 1400982 1400983 1400984 1400985 [...] (19944 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825920 1825921 1825922 1825923 1825924 1825925 1825926 1825927 1825928 1825929 [...] (12287 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825920 1825921 1825922 1825923 1825924 1825925 1825926 1825927 1825928 1825929 [...] (8893 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825920 1825921 1825922 1825923 1825924 1825925 1825926 1825927 1825928 1825929 [...] (6994 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1825920 1825921 1825922 1825923 1825924 1825925 1825926 1825927 1825928 1825929 [...] (5017 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1995336 1995337 1995338 1995339 1995340 1995341 1995342 1995343 1995344 1995345 [...] (3132 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2300526 2300527 2300528 2300529 2300530 2300531 2300532 2300533 2300534 2300535 [...] (1676 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 2346750 2346751 2346752 2346753 2346754 2346755 2346756 2346757 2346758 2346759 [...] (497 flits)
Measured flits: (0 flits)
Time taken is 105897 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6988.57 (1 samples)
	minimum = 27 (1 samples)
	maximum = 67134 (1 samples)
Network latency average = 6406.45 (1 samples)
	minimum = 23 (1 samples)
	maximum = 57723 (1 samples)
Flit latency average = 15436.3 (1 samples)
	minimum = 6 (1 samples)
	maximum = 78868 (1 samples)
Fragmentation average = 356.183 (1 samples)
	minimum = 0 (1 samples)
	maximum = 9622 (1 samples)
Injected packet rate average = 0.0208304 (1 samples)
	minimum = 0.00928571 (1 samples)
	maximum = 0.0362857 (1 samples)
Accepted packet rate average = 0.0128728 (1 samples)
	minimum = 0.00985714 (1 samples)
	maximum = 0.0175714 (1 samples)
Injected flit rate average = 0.374883 (1 samples)
	minimum = 0.168714 (1 samples)
	maximum = 0.653143 (1 samples)
Accepted flit rate average = 0.232578 (1 samples)
	minimum = 0.172571 (1 samples)
	maximum = 0.317 (1 samples)
Injected packet size average = 17.997 (1 samples)
Accepted packet size average = 18.0675 (1 samples)
Hops average = 5.04385 (1 samples)
Total run time 199.55
