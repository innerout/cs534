BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.026797
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=bernoulli
OVERRIDE Parameter: sim_type=throughput
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 252.912
	minimum = 22
	maximum = 767
Network latency average = 246.297
	minimum = 22
	maximum = 741
Slowest packet = 1135
Flit latency average = 222.86
	minimum = 5
	maximum = 724
Slowest flit = 20447
Fragmentation average = 21.2238
	minimum = 0
	maximum = 88
Injected packet rate average = 0.0267917
	minimum = 0.012 (at node 79)
	maximum = 0.042 (at node 126)
Accepted packet rate average = 0.0140313
	minimum = 0.006 (at node 64)
	maximum = 0.024 (at node 131)
Injected flit rate average = 0.478505
	minimum = 0.216 (at node 79)
	maximum = 0.756 (at node 126)
Accepted flit rate average= 0.258156
	minimum = 0.116 (at node 64)
	maximum = 0.432 (at node 131)
Injected packet length average = 17.8602
Accepted packet length average = 18.3987
Total in-flight flits = 43044 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 465.756
	minimum = 22
	maximum = 1385
Network latency average = 458.3
	minimum = 22
	maximum = 1385
Slowest packet = 2502
Flit latency average = 433.359
	minimum = 5
	maximum = 1368
Slowest flit = 45053
Fragmentation average = 22.0799
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0265156
	minimum = 0.015 (at node 190)
	maximum = 0.0355 (at node 10)
Accepted packet rate average = 0.0146276
	minimum = 0.008 (at node 116)
	maximum = 0.021 (at node 152)
Injected flit rate average = 0.475362
	minimum = 0.269 (at node 190)
	maximum = 0.639 (at node 70)
Accepted flit rate average= 0.265682
	minimum = 0.144 (at node 116)
	maximum = 0.3785 (at node 177)
Injected packet length average = 17.9276
Accepted packet length average = 18.1631
Total in-flight flits = 81362 (0 measured)
latency change    = 0.456986
throughput change = 0.0283272
Class 0:
Packet latency average = 1076.42
	minimum = 22
	maximum = 2033
Network latency average = 1066.27
	minimum = 22
	maximum = 2006
Slowest packet = 5022
Flit latency average = 1046.27
	minimum = 5
	maximum = 1989
Slowest flit = 90413
Fragmentation average = 20.6649
	minimum = 0
	maximum = 169
Injected packet rate average = 0.0241875
	minimum = 0.007 (at node 132)
	maximum = 0.04 (at node 53)
Accepted packet rate average = 0.0150781
	minimum = 0.007 (at node 132)
	maximum = 0.03 (at node 16)
Injected flit rate average = 0.435865
	minimum = 0.126 (at node 132)
	maximum = 0.72 (at node 53)
Accepted flit rate average= 0.271516
	minimum = 0.126 (at node 132)
	maximum = 0.547 (at node 16)
Injected packet length average = 18.0202
Accepted packet length average = 18.0073
Total in-flight flits = 113327 (0 measured)
latency change    = 0.56731
throughput change = 0.0214843
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 355.272
	minimum = 22
	maximum = 1740
Network latency average = 150.738
	minimum = 22
	maximum = 891
Slowest packet = 14915
Flit latency average = 1470.42
	minimum = 5
	maximum = 2495
Slowest flit = 114206
Fragmentation average = 5.90052
	minimum = 0
	maximum = 45
Injected packet rate average = 0.0211094
	minimum = 0.002 (at node 8)
	maximum = 0.038 (at node 46)
Accepted packet rate average = 0.014901
	minimum = 0.006 (at node 113)
	maximum = 0.024 (at node 34)
Injected flit rate average = 0.37976
	minimum = 0.039 (at node 8)
	maximum = 0.698 (at node 46)
Accepted flit rate average= 0.268964
	minimum = 0.108 (at node 113)
	maximum = 0.459 (at node 66)
Injected packet length average = 17.9901
Accepted packet length average = 18.05
Total in-flight flits = 135432 (70221 measured)
latency change    = 2.02985
throughput change = 0.00948858
Class 0:
Packet latency average = 1066.04
	minimum = 22
	maximum = 2618
Network latency average = 856.091
	minimum = 22
	maximum = 1983
Slowest packet = 14915
Flit latency average = 1654.69
	minimum = 5
	maximum = 3222
Slowest flit = 161748
Fragmentation average = 12.4715
	minimum = 0
	maximum = 103
Injected packet rate average = 0.0196094
	minimum = 0.005 (at node 12)
	maximum = 0.035 (at node 46)
Accepted packet rate average = 0.0148906
	minimum = 0.009 (at node 36)
	maximum = 0.0225 (at node 66)
Injected flit rate average = 0.353292
	minimum = 0.096 (at node 12)
	maximum = 0.631 (at node 46)
Accepted flit rate average= 0.268091
	minimum = 0.164 (at node 36)
	maximum = 0.405 (at node 66)
Injected packet length average = 18.0165
Accepted packet length average = 18.004
Total in-flight flits = 147108 (123126 measured)
latency change    = 0.666736
throughput change = 0.0032541
Class 0:
Packet latency average = 1831.98
	minimum = 22
	maximum = 3883
Network latency average = 1570.91
	minimum = 22
	maximum = 2993
Slowest packet = 14915
Flit latency average = 1824.89
	minimum = 5
	maximum = 3848
Slowest flit = 195732
Fragmentation average = 16.9365
	minimum = 0
	maximum = 107
Injected packet rate average = 0.0183351
	minimum = 0.007 (at node 17)
	maximum = 0.0313333 (at node 46)
Accepted packet rate average = 0.0148385
	minimum = 0.01 (at node 79)
	maximum = 0.0203333 (at node 66)
Injected flit rate average = 0.330026
	minimum = 0.126 (at node 17)
	maximum = 0.563333 (at node 46)
Accepted flit rate average= 0.26709
	minimum = 0.18 (at node 79)
	maximum = 0.366 (at node 66)
Injected packet length average = 17.9997
Accepted packet length average = 17.9998
Total in-flight flits = 151147 (145358 measured)
latency change    = 0.418095
throughput change = 0.0037473
Draining remaining packets ...
Class 0:
Remaining flits: 212099 212100 212101 212102 212103 212104 212105 212106 212107 212108 [...] (101427 flits)
Measured flits: 267912 267913 267914 267915 267916 267917 267918 267919 267920 267921 [...] (100768 flits)
Class 0:
Remaining flits: 242087 242088 242089 242090 242091 242092 242093 242094 242095 242096 [...] (53054 flits)
Measured flits: 271494 271495 271496 271497 271498 271499 271500 271501 271502 271503 [...] (53003 flits)
Class 0:
Remaining flits: 333576 333577 333578 333579 333580 333581 333582 333583 333584 333585 [...] (7162 flits)
Measured flits: 333576 333577 333578 333579 333580 333581 333582 333583 333584 333585 [...] (7162 flits)
Time taken is 9591 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 3015.09 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5682 (1 samples)
Network latency average = 2612.51 (1 samples)
	minimum = 22 (1 samples)
	maximum = 5358 (1 samples)
Flit latency average = 2378.67 (1 samples)
	minimum = 5 (1 samples)
	maximum = 5514 (1 samples)
Fragmentation average = 18.6221 (1 samples)
	minimum = 0 (1 samples)
	maximum = 181 (1 samples)
Injected packet rate average = 0.0183351 (1 samples)
	minimum = 0.007 (1 samples)
	maximum = 0.0313333 (1 samples)
Accepted packet rate average = 0.0148385 (1 samples)
	minimum = 0.01 (1 samples)
	maximum = 0.0203333 (1 samples)
Injected flit rate average = 0.330026 (1 samples)
	minimum = 0.126 (1 samples)
	maximum = 0.563333 (1 samples)
Accepted flit rate average = 0.26709 (1 samples)
	minimum = 0.18 (1 samples)
	maximum = 0.366 (1 samples)
Injected packet size average = 17.9997 (1 samples)
Accepted packet size average = 17.9998 (1 samples)
Hops average = 5.11692 (1 samples)
Total run time 7.81411
