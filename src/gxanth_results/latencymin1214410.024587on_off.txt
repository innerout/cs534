BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.024587
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 320.099
	minimum = 25
	maximum = 970
Network latency average = 254.767
	minimum = 25
	maximum = 870
Slowest packet = 85
Flit latency average = 217.472
	minimum = 6
	maximum = 933
Slowest flit = 5212
Fragmentation average = 56.1692
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0211719
	minimum = 0 (at node 104)
	maximum = 0.056 (at node 37)
Accepted packet rate average = 0.00920312
	minimum = 0.002 (at node 174)
	maximum = 0.018 (at node 44)
Injected flit rate average = 0.377542
	minimum = 0 (at node 104)
	maximum = 1 (at node 37)
Accepted flit rate average= 0.174068
	minimum = 0.044 (at node 174)
	maximum = 0.327 (at node 44)
Injected packet length average = 17.8322
Accepted packet length average = 18.914
Total in-flight flits = 39785 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 560.868
	minimum = 23
	maximum = 1978
Network latency average = 468.573
	minimum = 23
	maximum = 1877
Slowest packet = 85
Flit latency average = 429.737
	minimum = 6
	maximum = 1860
Slowest flit = 9035
Fragmentation average = 62.4866
	minimum = 0
	maximum = 143
Injected packet rate average = 0.0202422
	minimum = 0.002 (at node 104)
	maximum = 0.0395 (at node 69)
Accepted packet rate average = 0.00970312
	minimum = 0.005 (at node 101)
	maximum = 0.014 (at node 78)
Injected flit rate average = 0.362435
	minimum = 0.036 (at node 104)
	maximum = 0.7095 (at node 189)
Accepted flit rate average= 0.178776
	minimum = 0.09 (at node 101)
	maximum = 0.2535 (at node 22)
Injected packet length average = 17.9049
Accepted packet length average = 18.4246
Total in-flight flits = 71480 (0 measured)
latency change    = 0.429279
throughput change = 0.0263365
Class 0:
Packet latency average = 1318.67
	minimum = 27
	maximum = 2905
Network latency average = 1155.12
	minimum = 23
	maximum = 2713
Slowest packet = 1367
Flit latency average = 1123.98
	minimum = 6
	maximum = 2696
Slowest flit = 19691
Fragmentation average = 68.5253
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0169219
	minimum = 0 (at node 15)
	maximum = 0.046 (at node 151)
Accepted packet rate average = 0.0096875
	minimum = 0.002 (at node 94)
	maximum = 0.017 (at node 12)
Injected flit rate average = 0.305224
	minimum = 0 (at node 15)
	maximum = 0.824 (at node 151)
Accepted flit rate average= 0.173979
	minimum = 0.042 (at node 94)
	maximum = 0.323 (at node 177)
Injected packet length average = 18.0372
Accepted packet length average = 17.9591
Total in-flight flits = 97260 (0 measured)
latency change    = 0.574671
throughput change = 0.0275715
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 629.16
	minimum = 29
	maximum = 3293
Network latency average = 190.687
	minimum = 27
	maximum = 981
Slowest packet = 11149
Flit latency average = 1651.92
	minimum = 6
	maximum = 3430
Slowest flit = 31985
Fragmentation average = 31.0467
	minimum = 0
	maximum = 130
Injected packet rate average = 0.0131094
	minimum = 0 (at node 80)
	maximum = 0.038 (at node 113)
Accepted packet rate average = 0.00921354
	minimum = 0.002 (at node 79)
	maximum = 0.021 (at node 16)
Injected flit rate average = 0.236141
	minimum = 0 (at node 80)
	maximum = 0.67 (at node 113)
Accepted flit rate average= 0.16576
	minimum = 0.054 (at node 74)
	maximum = 0.361 (at node 16)
Injected packet length average = 18.0131
Accepted packet length average = 17.991
Total in-flight flits = 111424 (43139 measured)
latency change    = 1.09592
throughput change = 0.0495821
Class 0:
Packet latency average = 1244.47
	minimum = 29
	maximum = 4197
Network latency average = 671.83
	minimum = 25
	maximum = 1959
Slowest packet = 11149
Flit latency average = 1889.66
	minimum = 6
	maximum = 4288
Slowest flit = 50604
Fragmentation average = 46.972
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0118255
	minimum = 0.001 (at node 180)
	maximum = 0.029 (at node 155)
Accepted packet rate average = 0.00921354
	minimum = 0.005 (at node 35)
	maximum = 0.016 (at node 34)
Injected flit rate average = 0.212919
	minimum = 0.013 (at node 180)
	maximum = 0.522 (at node 155)
Accepted flit rate average= 0.165633
	minimum = 0.083 (at node 169)
	maximum = 0.2845 (at node 34)
Injected packet length average = 18.0051
Accepted packet length average = 17.9771
Total in-flight flits = 116295 (73285 measured)
latency change    = 0.494435
throughput change = 0.000770404
Class 0:
Packet latency average = 1881.15
	minimum = 29
	maximum = 5254
Network latency average = 1253.84
	minimum = 23
	maximum = 2978
Slowest packet = 11149
Flit latency average = 2106.77
	minimum = 6
	maximum = 5173
Slowest flit = 59665
Fragmentation average = 59.4863
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0112708
	minimum = 0.00133333 (at node 168)
	maximum = 0.0243333 (at node 22)
Accepted packet rate average = 0.0092066
	minimum = 0.00466667 (at node 86)
	maximum = 0.0143333 (at node 34)
Injected flit rate average = 0.202946
	minimum = 0.0266667 (at node 168)
	maximum = 0.442 (at node 43)
Accepted flit rate average= 0.165571
	minimum = 0.0866667 (at node 86)
	maximum = 0.257667 (at node 34)
Injected packet length average = 18.0063
Accepted packet length average = 17.984
Total in-flight flits = 119881 (94875 measured)
latency change    = 0.338455
throughput change = 0.000372238
Class 0:
Packet latency average = 2460.39
	minimum = 29
	maximum = 6003
Network latency average = 1683.92
	minimum = 23
	maximum = 3939
Slowest packet = 11149
Flit latency average = 2306.32
	minimum = 6
	maximum = 5700
Slowest flit = 85571
Fragmentation average = 63.5932
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0106615
	minimum = 0.002 (at node 191)
	maximum = 0.022 (at node 22)
Accepted packet rate average = 0.00913281
	minimum = 0.006 (at node 86)
	maximum = 0.01275 (at node 34)
Injected flit rate average = 0.191988
	minimum = 0.03575 (at node 191)
	maximum = 0.396 (at node 22)
Accepted flit rate average= 0.164249
	minimum = 0.11325 (at node 86)
	maximum = 0.228 (at node 34)
Injected packet length average = 18.0077
Accepted packet length average = 17.9845
Total in-flight flits = 119779 (106574 measured)
latency change    = 0.235424
throughput change = 0.00805171
Class 0:
Packet latency average = 2966.05
	minimum = 29
	maximum = 6767
Network latency average = 2066.65
	minimum = 23
	maximum = 4918
Slowest packet = 11149
Flit latency average = 2479.19
	minimum = 6
	maximum = 6526
Slowest flit = 67265
Fragmentation average = 65.1789
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0102927
	minimum = 0.0022 (at node 184)
	maximum = 0.0196 (at node 22)
Accepted packet rate average = 0.00906458
	minimum = 0.0062 (at node 86)
	maximum = 0.0124 (at node 16)
Injected flit rate average = 0.185148
	minimum = 0.0396 (at node 184)
	maximum = 0.3514 (at node 22)
Accepted flit rate average= 0.163014
	minimum = 0.1144 (at node 86)
	maximum = 0.2206 (at node 16)
Injected packet length average = 17.9883
Accepted packet length average = 17.9836
Total in-flight flits = 119543 (112875 measured)
latency change    = 0.170484
throughput change = 0.00757702
Class 0:
Packet latency average = 3476.48
	minimum = 29
	maximum = 7981
Network latency average = 2412.9
	minimum = 23
	maximum = 5927
Slowest packet = 11149
Flit latency average = 2649.65
	minimum = 6
	maximum = 7605
Slowest flit = 96587
Fragmentation average = 66.2017
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0100799
	minimum = 0.00216667 (at node 191)
	maximum = 0.0175 (at node 38)
Accepted packet rate average = 0.00902257
	minimum = 0.00633333 (at node 36)
	maximum = 0.012 (at node 129)
Injected flit rate average = 0.181508
	minimum = 0.0415 (at node 191)
	maximum = 0.3175 (at node 38)
Accepted flit rate average= 0.162305
	minimum = 0.114 (at node 36)
	maximum = 0.213667 (at node 129)
Injected packet length average = 18.007
Accepted packet length average = 17.9887
Total in-flight flits = 120759 (117677 measured)
latency change    = 0.146822
throughput change = 0.00436743
Class 0:
Packet latency average = 3979.88
	minimum = 29
	maximum = 8909
Network latency average = 2665.88
	minimum = 23
	maximum = 6875
Slowest packet = 11149
Flit latency average = 2785.57
	minimum = 6
	maximum = 8517
Slowest flit = 75137
Fragmentation average = 67.3347
	minimum = 0
	maximum = 156
Injected packet rate average = 0.00988914
	minimum = 0.00185714 (at node 191)
	maximum = 0.0165714 (at node 14)
Accepted packet rate average = 0.00897396
	minimum = 0.00657143 (at node 132)
	maximum = 0.0115714 (at node 95)
Injected flit rate average = 0.178057
	minimum = 0.0355714 (at node 191)
	maximum = 0.296857 (at node 14)
Accepted flit rate average= 0.161571
	minimum = 0.120143 (at node 132)
	maximum = 0.210857 (at node 95)
Injected packet length average = 18.0053
Accepted packet length average = 18.0044
Total in-flight flits = 120607 (119159 measured)
latency change    = 0.126488
throughput change = 0.00454292
Draining all recorded packets ...
Class 0:
Remaining flits: 133632 133633 133634 133635 133636 133637 133638 133639 133640 133641 [...] (120774 flits)
Measured flits: 200376 200377 200378 200379 200380 200381 200382 200383 200384 200385 [...] (119641 flits)
Class 0:
Remaining flits: 134892 134893 134894 134895 134896 134897 134898 134899 134900 134901 [...] (122417 flits)
Measured flits: 201186 201187 201188 201189 201190 201191 201192 201193 201194 201195 [...] (120830 flits)
Class 0:
Remaining flits: 141696 141697 141698 141699 141700 141701 141702 141703 141704 141705 [...] (120274 flits)
Measured flits: 201186 201187 201188 201189 201190 201191 201192 201193 201194 201195 [...] (118149 flits)
Class 0:
Remaining flits: 175932 175933 175934 175935 175936 175937 175938 175939 175940 175941 [...] (119773 flits)
Measured flits: 203562 203563 203564 203565 203566 203567 203568 203569 203570 203571 [...] (116265 flits)
Class 0:
Remaining flits: 175932 175933 175934 175935 175936 175937 175938 175939 175940 175941 [...] (118920 flits)
Measured flits: 275328 275329 275330 275331 275332 275333 275334 275335 275336 275337 [...] (112819 flits)
Class 0:
Remaining flits: 295866 295867 295868 295869 295870 295871 295872 295873 295874 295875 [...] (119567 flits)
Measured flits: 295866 295867 295868 295869 295870 295871 295872 295873 295874 295875 [...] (109721 flits)
Class 0:
Remaining flits: 295866 295867 295868 295869 295870 295871 295872 295873 295874 295875 [...] (119817 flits)
Measured flits: 295866 295867 295868 295869 295870 295871 295872 295873 295874 295875 [...] (104975 flits)
Class 0:
Remaining flits: 303570 303571 303572 303573 303574 303575 303576 303577 303578 303579 [...] (119394 flits)
Measured flits: 303570 303571 303572 303573 303574 303575 303576 303577 303578 303579 [...] (101509 flits)
Class 0:
Remaining flits: 344052 344053 344054 344055 344056 344057 344058 344059 344060 344061 [...] (119705 flits)
Measured flits: 344052 344053 344054 344055 344056 344057 344058 344059 344060 344061 [...] (98077 flits)
Class 0:
Remaining flits: 360666 360667 360668 360669 360670 360671 360672 360673 360674 360675 [...] (117493 flits)
Measured flits: 360666 360667 360668 360669 360670 360671 360672 360673 360674 360675 [...] (92376 flits)
Class 0:
Remaining flits: 421632 421633 421634 421635 421636 421637 421638 421639 421640 421641 [...] (116874 flits)
Measured flits: 421632 421633 421634 421635 421636 421637 421638 421639 421640 421641 [...] (86017 flits)
Class 0:
Remaining flits: 421639 421640 421641 421642 421643 421644 421645 421646 421647 421648 [...] (118214 flits)
Measured flits: 421639 421640 421641 421642 421643 421644 421645 421646 421647 421648 [...] (80831 flits)
Class 0:
Remaining flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (118728 flits)
Measured flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (74578 flits)
Class 0:
Remaining flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (119500 flits)
Measured flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (67623 flits)
Class 0:
Remaining flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (119435 flits)
Measured flits: 455184 455185 455186 455187 455188 455189 455190 455191 455192 455193 [...] (58845 flits)
Class 0:
Remaining flits: 572328 572329 572330 572331 572332 572333 572334 572335 572336 572337 [...] (121943 flits)
Measured flits: 572328 572329 572330 572331 572332 572333 572334 572335 572336 572337 [...] (50671 flits)
Class 0:
Remaining flits: 646596 646597 646598 646599 646600 646601 646602 646603 646604 646605 [...] (122652 flits)
Measured flits: 646596 646597 646598 646599 646600 646601 646602 646603 646604 646605 [...] (43383 flits)
Class 0:
Remaining flits: 658350 658351 658352 658353 658354 658355 658356 658357 658358 658359 [...] (122319 flits)
Measured flits: 658350 658351 658352 658353 658354 658355 658356 658357 658358 658359 [...] (36800 flits)
Class 0:
Remaining flits: 694278 694279 694280 694281 694282 694283 694284 694285 694286 694287 [...] (121740 flits)
Measured flits: 694278 694279 694280 694281 694282 694283 694284 694285 694286 694287 [...] (31296 flits)
Class 0:
Remaining flits: 730422 730423 730424 730425 730426 730427 730428 730429 730430 730431 [...] (118888 flits)
Measured flits: 745344 745345 745346 745347 745348 745349 745350 745351 745352 745353 [...] (25746 flits)
Class 0:
Remaining flits: 735336 735337 735338 735339 735340 735341 735342 735343 735344 735345 [...] (118508 flits)
Measured flits: 746514 746515 746516 746517 746518 746519 746520 746521 746522 746523 [...] (22978 flits)
Class 0:
Remaining flits: 767555 793980 793981 793982 793983 793984 793985 793986 793987 793988 [...] (122244 flits)
Measured flits: 793980 793981 793982 793983 793984 793985 793986 793987 793988 793989 [...] (19089 flits)
Class 0:
Remaining flits: 798552 798553 798554 798555 798556 798557 798558 798559 798560 798561 [...] (121677 flits)
Measured flits: 798552 798553 798554 798555 798556 798557 798558 798559 798560 798561 [...] (15783 flits)
Class 0:
Remaining flits: 798552 798553 798554 798555 798556 798557 798558 798559 798560 798561 [...] (120088 flits)
Measured flits: 798552 798553 798554 798555 798556 798557 798558 798559 798560 798561 [...] (13087 flits)
Class 0:
Remaining flits: 814878 814879 814880 814881 814882 814883 814884 814885 814886 814887 [...] (120270 flits)
Measured flits: 853182 853183 853184 853185 853186 853187 853188 853189 853190 853191 [...] (9386 flits)
Class 0:
Remaining flits: 832698 832699 832700 832701 832702 832703 832704 832705 832706 832707 [...] (119561 flits)
Measured flits: 935064 935065 935066 935067 935068 935069 935070 935071 935072 935073 [...] (7471 flits)
Class 0:
Remaining flits: 917532 917533 917534 917535 917536 917537 917538 917539 917540 917541 [...] (118634 flits)
Measured flits: 948798 948799 948800 948801 948802 948803 948804 948805 948806 948807 [...] (6117 flits)
Class 0:
Remaining flits: 951300 951301 951302 951303 951304 951305 951306 951307 951308 951309 [...] (121565 flits)
Measured flits: 951300 951301 951302 951303 951304 951305 951306 951307 951308 951309 [...] (3977 flits)
Class 0:
Remaining flits: 951300 951301 951302 951303 951304 951305 951306 951307 951308 951309 [...] (121904 flits)
Measured flits: 951300 951301 951302 951303 951304 951305 951306 951307 951308 951309 [...] (2747 flits)
Class 0:
Remaining flits: 997020 997021 997022 997023 997024 997025 997026 997027 997028 997029 [...] (120620 flits)
Measured flits: 1109952 1109953 1109954 1109955 1109956 1109957 1109958 1109959 1109960 1109961 [...] (1849 flits)
Class 0:
Remaining flits: 1009350 1009351 1009352 1009353 1009354 1009355 1009356 1009357 1009358 1009359 [...] (120474 flits)
Measured flits: 1165014 1165015 1165016 1165017 1165018 1165019 1165020 1165021 1165022 1165023 [...] (954 flits)
Class 0:
Remaining flits: 1033128 1033129 1033130 1033131 1033132 1033133 1033134 1033135 1033136 1033137 [...] (120350 flits)
Measured flits: 1175058 1175059 1175060 1175061 1175062 1175063 1175064 1175065 1175066 1175067 [...] (388 flits)
Class 0:
Remaining flits: 1047042 1047043 1047044 1047045 1047046 1047047 1047048 1047049 1047050 1047051 [...] (118241 flits)
Measured flits: 1219734 1219735 1219736 1219737 1219738 1219739 1219740 1219741 1219742 1219743 [...] (162 flits)
Class 0:
Remaining flits: 1047042 1047043 1047044 1047045 1047046 1047047 1047048 1047049 1047050 1047051 [...] (120001 flits)
Measured flits: 1253862 1253863 1253864 1253865 1253866 1253867 1253868 1253869 1253870 1253871 [...] (63 flits)
Class 0:
Remaining flits: 1166004 1166005 1166006 1166007 1166008 1166009 1166010 1166011 1166012 1166013 [...] (121523 flits)
Measured flits: 1264698 1264699 1264700 1264701 1264702 1264703 1264704 1264705 1264706 1264707 [...] (18 flits)
Class 0:
Remaining flits: 1166004 1166005 1166006 1166007 1166008 1166009 1166010 1166011 1166012 1166013 [...] (120049 flits)
Measured flits: 1264698 1264699 1264700 1264701 1264702 1264703 1264704 1264705 1264706 1264707 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1194624 1194625 1194626 1194627 1194628 1194629 1194630 1194631 1194632 1194633 [...] (89954 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1228734 1228735 1228736 1228737 1228738 1228739 1228740 1228741 1228742 1228743 [...] (60796 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1244592 1244593 1244594 1244595 1244596 1244597 1244598 1244599 1244600 1244601 [...] (32266 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1278288 1278289 1278290 1278291 1278292 1278293 1278294 1278295 1278296 1278297 [...] (7670 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1429686 1429687 1429688 1429689 1429690 1429691 1429692 1429693 1429694 1429695 [...] (269 flits)
Measured flits: (0 flits)
Time taken is 51363 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 11299.7 (1 samples)
	minimum = 29 (1 samples)
	maximum = 36088 (1 samples)
Network latency average = 3771.1 (1 samples)
	minimum = 23 (1 samples)
	maximum = 15255 (1 samples)
Flit latency average = 3724.06 (1 samples)
	minimum = 6 (1 samples)
	maximum = 15221 (1 samples)
Fragmentation average = 71.6373 (1 samples)
	minimum = 0 (1 samples)
	maximum = 170 (1 samples)
Injected packet rate average = 0.00988914 (1 samples)
	minimum = 0.00185714 (1 samples)
	maximum = 0.0165714 (1 samples)
Accepted packet rate average = 0.00897396 (1 samples)
	minimum = 0.00657143 (1 samples)
	maximum = 0.0115714 (1 samples)
Injected flit rate average = 0.178057 (1 samples)
	minimum = 0.0355714 (1 samples)
	maximum = 0.296857 (1 samples)
Accepted flit rate average = 0.161571 (1 samples)
	minimum = 0.120143 (1 samples)
	maximum = 0.210857 (1 samples)
Injected packet size average = 18.0053 (1 samples)
Accepted packet size average = 18.0044 (1 samples)
Hops average = 5.06242 (1 samples)
Total run time 38.1144
