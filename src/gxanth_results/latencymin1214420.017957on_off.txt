BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.017957
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=2.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 228.216
	minimum = 24
	maximum = 889
Network latency average = 163.047
	minimum = 22
	maximum = 717
Slowest packet = 21
Flit latency average = 140.004
	minimum = 5
	maximum = 728
Slowest flit = 16573
Fragmentation average = 15.683
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0179427
	minimum = 0 (at node 36)
	maximum = 0.055 (at node 31)
Accepted packet rate average = 0.0123229
	minimum = 0.005 (at node 127)
	maximum = 0.023 (at node 44)
Injected flit rate average = 0.319854
	minimum = 0 (at node 36)
	maximum = 0.978 (at node 31)
Accepted flit rate average= 0.22688
	minimum = 0.108 (at node 41)
	maximum = 0.43 (at node 44)
Injected packet length average = 17.8264
Accepted packet length average = 18.4112
Total in-flight flits = 18449 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 352.27
	minimum = 24
	maximum = 1822
Network latency average = 269.94
	minimum = 22
	maximum = 1304
Slowest packet = 21
Flit latency average = 245.882
	minimum = 5
	maximum = 1287
Slowest flit = 39797
Fragmentation average = 16.9025
	minimum = 0
	maximum = 93
Injected packet rate average = 0.0180104
	minimum = 0.0005 (at node 169)
	maximum = 0.0445 (at node 41)
Accepted packet rate average = 0.0132682
	minimum = 0.0075 (at node 62)
	maximum = 0.0195 (at node 22)
Injected flit rate average = 0.322594
	minimum = 0.009 (at node 169)
	maximum = 0.799 (at node 41)
Accepted flit rate average= 0.241273
	minimum = 0.135 (at node 62)
	maximum = 0.351 (at node 22)
Injected packet length average = 17.9115
Accepted packet length average = 18.1843
Total in-flight flits = 31893 (0 measured)
latency change    = 0.352155
throughput change = 0.0596553
Class 0:
Packet latency average = 697.443
	minimum = 22
	maximum = 2409
Network latency average = 581.847
	minimum = 22
	maximum = 1831
Slowest packet = 2410
Flit latency average = 559.838
	minimum = 5
	maximum = 1830
Slowest flit = 71892
Fragmentation average = 18.4315
	minimum = 0
	maximum = 142
Injected packet rate average = 0.0179271
	minimum = 0 (at node 54)
	maximum = 0.055 (at node 27)
Accepted packet rate average = 0.0143646
	minimum = 0.007 (at node 31)
	maximum = 0.025 (at node 34)
Injected flit rate average = 0.323219
	minimum = 0 (at node 54)
	maximum = 1 (at node 27)
Accepted flit rate average= 0.258396
	minimum = 0.126 (at node 190)
	maximum = 0.464 (at node 34)
Injected packet length average = 18.0296
Accepted packet length average = 17.9884
Total in-flight flits = 44273 (0 measured)
latency change    = 0.494912
throughput change = 0.0662642
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 421.05
	minimum = 22
	maximum = 1132
Network latency average = 337.367
	minimum = 22
	maximum = 962
Slowest packet = 10390
Flit latency average = 750.182
	minimum = 5
	maximum = 2641
Slowest flit = 83009
Fragmentation average = 15.1531
	minimum = 0
	maximum = 100
Injected packet rate average = 0.0175104
	minimum = 0 (at node 9)
	maximum = 0.052 (at node 130)
Accepted packet rate average = 0.0146823
	minimum = 0.006 (at node 17)
	maximum = 0.027 (at node 19)
Injected flit rate average = 0.315078
	minimum = 0 (at node 9)
	maximum = 0.943 (at node 130)
Accepted flit rate average= 0.264448
	minimum = 0.108 (at node 17)
	maximum = 0.481 (at node 178)
Injected packet length average = 17.9938
Accepted packet length average = 18.0114
Total in-flight flits = 53961 (43619 measured)
latency change    = 0.65644
throughput change = 0.0228857
Class 0:
Packet latency average = 749.67
	minimum = 22
	maximum = 2621
Network latency average = 654.106
	minimum = 22
	maximum = 1940
Slowest packet = 10390
Flit latency average = 830.472
	minimum = 5
	maximum = 3099
Slowest flit = 84311
Fragmentation average = 16.4543
	minimum = 0
	maximum = 103
Injected packet rate average = 0.016875
	minimum = 0 (at node 12)
	maximum = 0.0445 (at node 55)
Accepted packet rate average = 0.0146745
	minimum = 0.009 (at node 71)
	maximum = 0.022 (at node 157)
Injected flit rate average = 0.303773
	minimum = 0 (at node 12)
	maximum = 0.8005 (at node 55)
Accepted flit rate average= 0.26456
	minimum = 0.167 (at node 71)
	maximum = 0.396 (at node 157)
Injected packet length average = 18.0014
Accepted packet length average = 18.0286
Total in-flight flits = 59394 (57393 measured)
latency change    = 0.438353
throughput change = 0.000423266
Class 0:
Packet latency average = 942.864
	minimum = 22
	maximum = 3467
Network latency average = 831.855
	minimum = 22
	maximum = 2926
Slowest packet = 10390
Flit latency average = 913.465
	minimum = 5
	maximum = 3264
Slowest flit = 159083
Fragmentation average = 17.434
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0173403
	minimum = 0.00266667 (at node 82)
	maximum = 0.0416667 (at node 110)
Accepted packet rate average = 0.0147101
	minimum = 0.00966667 (at node 36)
	maximum = 0.0213333 (at node 157)
Injected flit rate average = 0.312161
	minimum = 0.048 (at node 82)
	maximum = 0.75 (at node 110)
Accepted flit rate average= 0.264505
	minimum = 0.173667 (at node 36)
	maximum = 0.384 (at node 157)
Injected packet length average = 18.0021
Accepted packet length average = 17.9812
Total in-flight flits = 71738 (71582 measured)
latency change    = 0.204901
throughput change = 0.000206754
Class 0:
Packet latency average = 1078.8
	minimum = 22
	maximum = 4503
Network latency average = 955.034
	minimum = 22
	maximum = 3808
Slowest packet = 10390
Flit latency average = 985.208
	minimum = 5
	maximum = 3791
Slowest flit = 194435
Fragmentation average = 17.993
	minimum = 0
	maximum = 153
Injected packet rate average = 0.0171003
	minimum = 0.004 (at node 82)
	maximum = 0.03925 (at node 130)
Accepted packet rate average = 0.0147227
	minimum = 0.011 (at node 64)
	maximum = 0.021 (at node 128)
Injected flit rate average = 0.307749
	minimum = 0.07075 (at node 82)
	maximum = 0.70625 (at node 130)
Accepted flit rate average= 0.265034
	minimum = 0.198 (at node 104)
	maximum = 0.377 (at node 128)
Injected packet length average = 17.9967
Accepted packet length average = 18.0018
Total in-flight flits = 77211 (77211 measured)
latency change    = 0.126008
throughput change = 0.00199464
Class 0:
Packet latency average = 1191.11
	minimum = 22
	maximum = 5331
Network latency average = 1054.18
	minimum = 22
	maximum = 4268
Slowest packet = 10390
Flit latency average = 1055.5
	minimum = 5
	maximum = 4250
Slowest flit = 228509
Fragmentation average = 18.1372
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0168354
	minimum = 0.005 (at node 81)
	maximum = 0.0378 (at node 130)
Accepted packet rate average = 0.0147354
	minimum = 0.011 (at node 64)
	maximum = 0.0194 (at node 103)
Injected flit rate average = 0.302951
	minimum = 0.09 (at node 81)
	maximum = 0.6822 (at node 130)
Accepted flit rate average= 0.265183
	minimum = 0.2002 (at node 64)
	maximum = 0.3488 (at node 128)
Injected packet length average = 17.9949
Accepted packet length average = 17.9963
Total in-flight flits = 80883 (80883 measured)
latency change    = 0.0942865
throughput change = 0.000563682
Class 0:
Packet latency average = 1285.12
	minimum = 22
	maximum = 5877
Network latency average = 1134.67
	minimum = 22
	maximum = 4453
Slowest packet = 10390
Flit latency average = 1120.13
	minimum = 5
	maximum = 4436
Slowest flit = 273113
Fragmentation average = 18.1974
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0169036
	minimum = 0.00566667 (at node 190)
	maximum = 0.0335 (at node 130)
Accepted packet rate average = 0.0147483
	minimum = 0.0108333 (at node 64)
	maximum = 0.019 (at node 128)
Injected flit rate average = 0.304255
	minimum = 0.102 (at node 190)
	maximum = 0.6045 (at node 130)
Accepted flit rate average= 0.265504
	minimum = 0.195 (at node 64)
	maximum = 0.340333 (at node 128)
Injected packet length average = 17.9994
Accepted packet length average = 18.0024
Total in-flight flits = 89304 (89304 measured)
latency change    = 0.0731556
throughput change = 0.00120905
Class 0:
Packet latency average = 1361.28
	minimum = 22
	maximum = 6829
Network latency average = 1199.65
	minimum = 22
	maximum = 5225
Slowest packet = 10390
Flit latency average = 1174.75
	minimum = 5
	maximum = 5208
Slowest flit = 271995
Fragmentation average = 18.3061
	minimum = 0
	maximum = 170
Injected packet rate average = 0.0167374
	minimum = 0.00728571 (at node 190)
	maximum = 0.0317143 (at node 130)
Accepted packet rate average = 0.0147537
	minimum = 0.0114286 (at node 64)
	maximum = 0.0185714 (at node 181)
Injected flit rate average = 0.301219
	minimum = 0.131143 (at node 190)
	maximum = 0.572143 (at node 130)
Accepted flit rate average= 0.265586
	minimum = 0.208143 (at node 64)
	maximum = 0.333 (at node 181)
Injected packet length average = 17.9968
Accepted packet length average = 18.0013
Total in-flight flits = 92757 (92757 measured)
latency change    = 0.0559469
throughput change = 0.000308635
Draining all recorded packets ...
Class 0:
Remaining flits: 380113 380114 380115 380116 380117 380118 380119 380120 380121 380122 [...] (98988 flits)
Measured flits: 380113 380114 380115 380116 380117 380118 380119 380120 380121 380122 [...] (63345 flits)
Class 0:
Remaining flits: 445230 445231 445232 445233 445234 445235 445236 445237 445238 445239 [...] (104886 flits)
Measured flits: 445230 445231 445232 445233 445234 445235 445236 445237 445238 445239 [...] (36550 flits)
Class 0:
Remaining flits: 475416 475417 475418 475419 475420 475421 475422 475423 475424 475425 [...] (112752 flits)
Measured flits: 475416 475417 475418 475419 475420 475421 475422 475423 475424 475425 [...] (20498 flits)
Class 0:
Remaining flits: 501066 501067 501068 501069 501070 501071 501072 501073 501074 501075 [...] (112813 flits)
Measured flits: 501066 501067 501068 501069 501070 501071 501072 501073 501074 501075 [...] (10601 flits)
Class 0:
Remaining flits: 577728 577729 577730 577731 577732 577733 577734 577735 577736 577737 [...] (116652 flits)
Measured flits: 577728 577729 577730 577731 577732 577733 577734 577735 577736 577737 [...] (5619 flits)
Class 0:
Remaining flits: 621270 621271 621272 621273 621274 621275 621276 621277 621278 621279 [...] (116151 flits)
Measured flits: 621270 621271 621272 621273 621274 621275 621276 621277 621278 621279 [...] (3147 flits)
Class 0:
Remaining flits: 676512 676513 676514 676515 676516 676517 676518 676519 676520 676521 [...] (121388 flits)
Measured flits: 676512 676513 676514 676515 676516 676517 676518 676519 676520 676521 [...] (1579 flits)
Class 0:
Remaining flits: 725130 725131 725132 725133 725134 725135 725136 725137 725138 725139 [...] (121263 flits)
Measured flits: 747918 747919 747920 747921 747922 747923 747924 747925 747926 747927 [...] (898 flits)
Class 0:
Remaining flits: 791478 791479 791480 791481 791482 791483 791484 791485 791486 791487 [...] (122700 flits)
Measured flits: 868896 868897 868898 868899 868900 868901 868902 868903 868904 868905 [...] (522 flits)
Class 0:
Remaining flits: 814320 814321 814322 814323 814324 814325 814326 814327 814328 814329 [...] (122605 flits)
Measured flits: 887562 887563 887564 887565 887566 887567 887568 887569 887570 887571 [...] (273 flits)
Class 0:
Remaining flits: 876996 876997 876998 876999 877000 877001 877002 877003 877004 877005 [...] (122652 flits)
Measured flits: 1058544 1058545 1058546 1058547 1058548 1058549 1058550 1058551 1058552 1058553 [...] (36 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 907686 907687 907688 907689 907690 907691 907692 907693 907694 907695 [...] (74001 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 955206 955207 955208 955209 955210 955211 955212 955213 955214 955215 [...] (27784 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1010430 1010431 1010432 1010433 1010434 1010435 1010436 1010437 1010438 1010439 [...] (2327 flits)
Measured flits: (0 flits)
Time taken is 24865 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 1885.25 (1 samples)
	minimum = 22 (1 samples)
	maximum = 12762 (1 samples)
Network latency average = 1532.24 (1 samples)
	minimum = 22 (1 samples)
	maximum = 6421 (1 samples)
Flit latency average = 1843.72 (1 samples)
	minimum = 5 (1 samples)
	maximum = 6999 (1 samples)
Fragmentation average = 18.8892 (1 samples)
	minimum = 0 (1 samples)
	maximum = 211 (1 samples)
Injected packet rate average = 0.0167374 (1 samples)
	minimum = 0.00728571 (1 samples)
	maximum = 0.0317143 (1 samples)
Accepted packet rate average = 0.0147537 (1 samples)
	minimum = 0.0114286 (1 samples)
	maximum = 0.0185714 (1 samples)
Injected flit rate average = 0.301219 (1 samples)
	minimum = 0.131143 (1 samples)
	maximum = 0.572143 (1 samples)
Accepted flit rate average = 0.265586 (1 samples)
	minimum = 0.208143 (1 samples)
	maximum = 0.333 (1 samples)
Injected packet size average = 17.9968 (1 samples)
Accepted packet size average = 18.0013 (1 samples)
Hops average = 5.08542 (1 samples)
Total run time 19.3752
