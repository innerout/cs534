BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=32
OVERRIDE Parameter: vc_buf_size=144
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 319.13
	minimum = 25
	maximum = 981
Network latency average = 252.532
	minimum = 25
	maximum = 947
Slowest packet = 140
Flit latency average = 187.92
	minimum = 6
	maximum = 979
Slowest flit = 1258
Fragmentation average = 135.299
	minimum = 0
	maximum = 871
Injected packet rate average = 0.0210573
	minimum = 0 (at node 112)
	maximum = 0.056 (at node 33)
Accepted packet rate average = 0.00939583
	minimum = 0.004 (at node 108)
	maximum = 0.017 (at node 44)
Injected flit rate average = 0.375682
	minimum = 0 (at node 112)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.193828
	minimum = 0.087 (at node 108)
	maximum = 0.339 (at node 44)
Injected packet length average = 17.841
Accepted packet length average = 20.6292
Total in-flight flits = 35559 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 525.376
	minimum = 25
	maximum = 1923
Network latency average = 433.009
	minimum = 23
	maximum = 1758
Slowest packet = 289
Flit latency average = 352.223
	minimum = 6
	maximum = 1927
Slowest flit = 5236
Fragmentation average = 175.001
	minimum = 0
	maximum = 1603
Injected packet rate average = 0.0211615
	minimum = 0.002 (at node 130)
	maximum = 0.0465 (at node 46)
Accepted packet rate average = 0.0108437
	minimum = 0.004 (at node 83)
	maximum = 0.0175 (at node 44)
Injected flit rate average = 0.379271
	minimum = 0.036 (at node 130)
	maximum = 0.837 (at node 46)
Accepted flit rate average= 0.207922
	minimum = 0.0965 (at node 83)
	maximum = 0.3195 (at node 44)
Injected packet length average = 17.9227
Accepted packet length average = 19.1744
Total in-flight flits = 66444 (0 measured)
latency change    = 0.392568
throughput change = 0.0677839
Class 0:
Packet latency average = 1073.48
	minimum = 24
	maximum = 2883
Network latency average = 943.972
	minimum = 23
	maximum = 2766
Slowest packet = 426
Flit latency average = 860.508
	minimum = 6
	maximum = 2839
Slowest flit = 7894
Fragmentation average = 227.851
	minimum = 0
	maximum = 2162
Injected packet rate average = 0.022151
	minimum = 0 (at node 99)
	maximum = 0.056 (at node 3)
Accepted packet rate average = 0.0123802
	minimum = 0.004 (at node 184)
	maximum = 0.025 (at node 63)
Injected flit rate average = 0.397464
	minimum = 0 (at node 99)
	maximum = 1 (at node 3)
Accepted flit rate average= 0.226464
	minimum = 0.095 (at node 138)
	maximum = 0.454 (at node 63)
Injected packet length average = 17.9433
Accepted packet length average = 18.2924
Total in-flight flits = 99517 (0 measured)
latency change    = 0.510586
throughput change = 0.0818748
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 370.404
	minimum = 31
	maximum = 1765
Network latency average = 231.731
	minimum = 23
	maximum = 933
Slowest packet = 12407
Flit latency average = 1242.04
	minimum = 6
	maximum = 3590
Slowest flit = 21041
Fragmentation average = 78.377
	minimum = 0
	maximum = 652
Injected packet rate average = 0.0211198
	minimum = 0 (at node 17)
	maximum = 0.056 (at node 113)
Accepted packet rate average = 0.0123802
	minimum = 0.005 (at node 79)
	maximum = 0.021 (at node 129)
Injected flit rate average = 0.381552
	minimum = 0 (at node 17)
	maximum = 1 (at node 33)
Accepted flit rate average= 0.221255
	minimum = 0.079 (at node 135)
	maximum = 0.364 (at node 95)
Injected packet length average = 18.0661
Accepted packet length average = 17.8717
Total in-flight flits = 130026 (63808 measured)
latency change    = 1.89813
throughput change = 0.0235399
Class 0:
Packet latency average = 756.924
	minimum = 27
	maximum = 2924
Network latency average = 611.399
	minimum = 23
	maximum = 1982
Slowest packet = 12407
Flit latency average = 1451.43
	minimum = 6
	maximum = 4708
Slowest flit = 18914
Fragmentation average = 131.638
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0201172
	minimum = 0.002 (at node 17)
	maximum = 0.0515 (at node 135)
Accepted packet rate average = 0.0121432
	minimum = 0.006 (at node 79)
	maximum = 0.0185 (at node 128)
Injected flit rate average = 0.362742
	minimum = 0.034 (at node 17)
	maximum = 0.931 (at node 135)
Accepted flit rate average= 0.218117
	minimum = 0.109 (at node 80)
	maximum = 0.3275 (at node 128)
Injected packet length average = 18.0315
Accepted packet length average = 17.962
Total in-flight flits = 154810 (112309 measured)
latency change    = 0.510646
throughput change = 0.0143869
Class 0:
Packet latency average = 1152.11
	minimum = 26
	maximum = 4069
Network latency average = 984.872
	minimum = 23
	maximum = 2951
Slowest packet = 12407
Flit latency average = 1632.82
	minimum = 6
	maximum = 5631
Slowest flit = 24412
Fragmentation average = 149.756
	minimum = 0
	maximum = 1474
Injected packet rate average = 0.0200417
	minimum = 0.00333333 (at node 151)
	maximum = 0.0423333 (at node 10)
Accepted packet rate average = 0.0120365
	minimum = 0.00766667 (at node 79)
	maximum = 0.0173333 (at node 128)
Injected flit rate average = 0.361019
	minimum = 0.0596667 (at node 151)
	maximum = 0.762 (at node 10)
Accepted flit rate average= 0.215988
	minimum = 0.139333 (at node 80)
	maximum = 0.323 (at node 128)
Injected packet length average = 18.0134
Accepted packet length average = 17.9445
Total in-flight flits = 182936 (155307 measured)
latency change    = 0.34301
throughput change = 0.00985861
Class 0:
Packet latency average = 1516.02
	minimum = 26
	maximum = 5060
Network latency average = 1327.14
	minimum = 23
	maximum = 3881
Slowest packet = 12407
Flit latency average = 1802.89
	minimum = 6
	maximum = 6518
Slowest flit = 16919
Fragmentation average = 161.563
	minimum = 0
	maximum = 1750
Injected packet rate average = 0.0200599
	minimum = 0.0045 (at node 153)
	maximum = 0.0375 (at node 126)
Accepted packet rate average = 0.012
	minimum = 0.00725 (at node 80)
	maximum = 0.01675 (at node 128)
Injected flit rate average = 0.361289
	minimum = 0.081 (at node 153)
	maximum = 0.67325 (at node 126)
Accepted flit rate average= 0.214182
	minimum = 0.1385 (at node 80)
	maximum = 0.3045 (at node 128)
Injected packet length average = 18.0105
Accepted packet length average = 17.8485
Total in-flight flits = 212477 (193680 measured)
latency change    = 0.240042
throughput change = 0.00842999
Class 0:
Packet latency average = 1842.14
	minimum = 26
	maximum = 6130
Network latency average = 1629.87
	minimum = 23
	maximum = 4888
Slowest packet = 12407
Flit latency average = 1983.19
	minimum = 6
	maximum = 7200
Slowest flit = 56616
Fragmentation average = 167.509
	minimum = 0
	maximum = 1750
Injected packet rate average = 0.019576
	minimum = 0.0052 (at node 153)
	maximum = 0.0344 (at node 162)
Accepted packet rate average = 0.0118646
	minimum = 0.0072 (at node 79)
	maximum = 0.0164 (at node 128)
Injected flit rate average = 0.35246
	minimum = 0.0936 (at node 153)
	maximum = 0.6196 (at node 162)
Accepted flit rate average= 0.212546
	minimum = 0.1356 (at node 79)
	maximum = 0.2992 (at node 128)
Injected packet length average = 18.0047
Accepted packet length average = 17.9143
Total in-flight flits = 234125 (221238 measured)
latency change    = 0.177034
throughput change = 0.00769932
Class 0:
Packet latency average = 2147.82
	minimum = 26
	maximum = 7177
Network latency average = 1909.4
	minimum = 23
	maximum = 5909
Slowest packet = 12407
Flit latency average = 2171.53
	minimum = 6
	maximum = 8126
Slowest flit = 62013
Fragmentation average = 170.323
	minimum = 0
	maximum = 2473
Injected packet rate average = 0.0192891
	minimum = 0.00866667 (at node 153)
	maximum = 0.0325 (at node 135)
Accepted packet rate average = 0.0117821
	minimum = 0.00766667 (at node 79)
	maximum = 0.0155 (at node 103)
Injected flit rate average = 0.347277
	minimum = 0.155833 (at node 153)
	maximum = 0.586333 (at node 135)
Accepted flit rate average= 0.210907
	minimum = 0.141167 (at node 80)
	maximum = 0.277667 (at node 103)
Injected packet length average = 18.0038
Accepted packet length average = 17.9006
Total in-flight flits = 256890 (248435 measured)
latency change    = 0.142322
throughput change = 0.00776984
Class 0:
Packet latency average = 2466.42
	minimum = 26
	maximum = 8111
Network latency average = 2208.81
	minimum = 23
	maximum = 6940
Slowest packet = 12407
Flit latency average = 2370.36
	minimum = 6
	maximum = 9073
Slowest flit = 62208
Fragmentation average = 173.462
	minimum = 0
	maximum = 2724
Injected packet rate average = 0.0189271
	minimum = 0.00957143 (at node 56)
	maximum = 0.0295714 (at node 79)
Accepted packet rate average = 0.0116801
	minimum = 0.00771429 (at node 80)
	maximum = 0.015 (at node 103)
Injected flit rate average = 0.340804
	minimum = 0.170143 (at node 136)
	maximum = 0.532 (at node 79)
Accepted flit rate average= 0.209096
	minimum = 0.137571 (at node 80)
	maximum = 0.267429 (at node 103)
Injected packet length average = 18.0061
Accepted packet length average = 17.902
Total in-flight flits = 277042 (271121 measured)
latency change    = 0.129175
throughput change = 0.00866174
Draining all recorded packets ...
Class 0:
Remaining flits: 77274 77275 77276 77277 77278 77279 77280 77281 77282 77283 [...] (295537 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (257580 flits)
Class 0:
Remaining flits: 77274 77275 77276 77277 77278 77279 77280 77281 77282 77283 [...] (306912 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (235320 flits)
Class 0:
Remaining flits: 77274 77275 77276 77277 77278 77279 77280 77281 77282 77283 [...] (313195 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (213057 flits)
Class 0:
Remaining flits: 77274 77275 77276 77277 77278 77279 77280 77281 77282 77283 [...] (318742 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (190989 flits)
Class 0:
Remaining flits: 77288 77289 77290 77291 122454 122455 122456 122457 122458 122459 [...] (326693 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (170083 flits)
Class 0:
Remaining flits: 133125 133126 133127 138654 138655 138656 138657 138658 138659 138660 [...] (331846 flits)
Measured flits: 223218 223219 223220 223221 223222 223223 223224 223225 223226 223227 [...] (152141 flits)
Class 0:
Remaining flits: 138654 138655 138656 138657 138658 138659 138660 138661 138662 138663 [...] (331337 flits)
Measured flits: 225009 225010 225011 225012 225013 225014 225015 225016 225017 228908 [...] (131119 flits)
Class 0:
Remaining flits: 138659 138660 138661 138662 138663 138664 138665 138666 138667 138668 [...] (333723 flits)
Measured flits: 237685 237686 237687 237688 237689 246366 246367 246368 246369 246370 [...] (112600 flits)
Class 0:
Remaining flits: 193843 193844 193845 193846 193847 193848 193849 193850 193851 193852 [...] (332965 flits)
Measured flits: 256842 256843 256844 256845 256846 256847 256848 256849 256850 256851 [...] (95592 flits)
Class 0:
Remaining flits: 268303 268304 268305 268306 268307 284706 284707 284708 284709 284710 [...] (334455 flits)
Measured flits: 268303 268304 268305 268306 268307 284706 284707 284708 284709 284710 [...] (80469 flits)
Class 0:
Remaining flits: 284706 284707 284708 284709 284710 284711 284712 284713 284714 284715 [...] (333584 flits)
Measured flits: 284706 284707 284708 284709 284710 284711 284712 284713 284714 284715 [...] (66035 flits)
Class 0:
Remaining flits: 289962 289963 289964 289965 289966 289967 289968 289969 289970 289971 [...] (336132 flits)
Measured flits: 289962 289963 289964 289965 289966 289967 289968 289969 289970 289971 [...] (53732 flits)
Class 0:
Remaining flits: 302400 302401 302402 302403 302404 302405 302406 302407 302408 302409 [...] (335982 flits)
Measured flits: 302400 302401 302402 302403 302404 302405 302406 302407 302408 302409 [...] (43505 flits)
Class 0:
Remaining flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (331840 flits)
Measured flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (34966 flits)
Class 0:
Remaining flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (331914 flits)
Measured flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (27956 flits)
Class 0:
Remaining flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (333072 flits)
Measured flits: 313200 313201 313202 313203 313204 313205 313206 313207 313208 313209 [...] (21935 flits)
Class 0:
Remaining flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (332365 flits)
Measured flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (17366 flits)
Class 0:
Remaining flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (330793 flits)
Measured flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (13880 flits)
Class 0:
Remaining flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (330255 flits)
Measured flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (10366 flits)
Class 0:
Remaining flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (329994 flits)
Measured flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (7397 flits)
Class 0:
Remaining flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (331808 flits)
Measured flits: 362016 362017 362018 362019 362020 362021 362022 362023 362024 362025 [...] (5482 flits)
Class 0:
Remaining flits: 394984 394985 394986 394987 394988 394989 394990 394991 536148 536149 [...] (330619 flits)
Measured flits: 394984 394985 394986 394987 394988 394989 394990 394991 536148 536149 [...] (4077 flits)
Class 0:
Remaining flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (329579 flits)
Measured flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (2933 flits)
Class 0:
Remaining flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (327747 flits)
Measured flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (2240 flits)
Class 0:
Remaining flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (325549 flits)
Measured flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (1815 flits)
Class 0:
Remaining flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (324278 flits)
Measured flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (1554 flits)
Class 0:
Remaining flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (322077 flits)
Measured flits: 585738 585739 585740 585741 585742 585743 585744 585745 585746 585747 [...] (1102 flits)
Class 0:
Remaining flits: 585740 585741 585742 585743 585744 585745 585746 585747 585748 585749 [...] (319894 flits)
Measured flits: 585740 585741 585742 585743 585744 585745 585746 585747 585748 585749 [...] (920 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (315965 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (774 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (313197 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (628 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (310793 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (505 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (313534 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (407 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (311692 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (378 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (309803 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (360 flits)
Class 0:
Remaining flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (310584 flits)
Measured flits: 609336 609337 609338 609339 609340 609341 609342 609343 609344 609345 [...] (360 flits)
Class 0:
Remaining flits: 779778 779779 779780 779781 779782 779783 779784 779785 779786 779787 [...] (311635 flits)
Measured flits: 893754 893755 893756 893757 893758 893759 893760 893761 893762 893763 [...] (324 flits)
Class 0:
Remaining flits: 835704 835705 835706 835707 835708 835709 835710 835711 835712 835713 [...] (308365 flits)
Measured flits: 893754 893755 893756 893757 893758 893759 893760 893761 893762 893763 [...] (306 flits)
Class 0:
Remaining flits: 875592 875593 875594 875595 875596 875597 875598 875599 875600 875601 [...] (306912 flits)
Measured flits: 991368 991369 991370 991371 991372 991373 991374 991375 991376 991377 [...] (288 flits)
Class 0:
Remaining flits: 875592 875593 875594 875595 875596 875597 875598 875599 875600 875601 [...] (303209 flits)
Measured flits: 1336770 1336771 1336772 1336773 1336774 1336775 1336776 1336777 1336778 1336779 [...] (180 flits)
Class 0:
Remaining flits: 875592 875593 875594 875595 875596 875597 875598 875599 875600 875601 [...] (301972 flits)
Measured flits: 1336770 1336771 1336772 1336773 1336774 1336775 1336776 1336777 1336778 1336779 [...] (180 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (301490 flits)
Measured flits: 1336770 1336771 1336772 1336773 1336774 1336775 1336776 1336777 1336778 1336779 [...] (180 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (302506 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (149 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (302050 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (144 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (300945 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (144 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (300412 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (126 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (301236 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (126 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (300195 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (126 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (300879 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (18 flits)
Class 0:
Remaining flits: 933228 933229 933230 933231 933232 933233 933234 933235 933236 933237 [...] (302148 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (18 flits)
Class 0:
Remaining flits: 959148 959149 959150 959151 959152 959153 959154 959155 959156 959157 [...] (299884 flits)
Measured flits: 1343520 1343521 1343522 1343523 1343524 1343525 1343526 1343527 1343528 1343529 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (266513 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (232124 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (199482 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (168106 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (137319 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1165248 1165249 1165250 1165251 1165252 1165253 1165254 1165255 1165256 1165257 [...] (107978 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494072 1494073 1494074 1494075 1494076 1494077 1494078 1494079 1494080 1494081 [...] (80096 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494072 1494073 1494074 1494075 1494076 1494077 1494078 1494079 1494080 1494081 [...] (57044 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494072 1494073 1494074 1494075 1494076 1494077 1494078 1494079 1494080 1494081 [...] (39495 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494072 1494073 1494074 1494075 1494076 1494077 1494078 1494079 1494080 1494081 [...] (24089 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1494089 1695528 1695529 1695530 1695531 1695532 1695533 1695534 1695535 1695536 [...] (12431 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1897848 1897849 1897850 1897851 1897852 1897853 1897854 1897855 1897856 1897857 [...] (5229 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1944882 1944883 1944884 1944885 1944886 1944887 1944888 1944889 1944890 1944891 [...] (1575 flits)
Measured flits: (0 flits)
Time taken is 74638 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 7317.74 (1 samples)
	minimum = 26 (1 samples)
	maximum = 51410 (1 samples)
Network latency average = 5953 (1 samples)
	minimum = 23 (1 samples)
	maximum = 37162 (1 samples)
Flit latency average = 7816.74 (1 samples)
	minimum = 6 (1 samples)
	maximum = 45548 (1 samples)
Fragmentation average = 204.361 (1 samples)
	minimum = 0 (1 samples)
	maximum = 5256 (1 samples)
Injected packet rate average = 0.0189271 (1 samples)
	minimum = 0.00957143 (1 samples)
	maximum = 0.0295714 (1 samples)
Accepted packet rate average = 0.0116801 (1 samples)
	minimum = 0.00771429 (1 samples)
	maximum = 0.015 (1 samples)
Injected flit rate average = 0.340804 (1 samples)
	minimum = 0.170143 (1 samples)
	maximum = 0.532 (1 samples)
Accepted flit rate average = 0.209096 (1 samples)
	minimum = 0.137571 (1 samples)
	maximum = 0.267429 (1 samples)
Injected packet size average = 18.0061 (1 samples)
Accepted packet size average = 17.902 (1 samples)
Hops average = 5.05432 (1 samples)
Total run time 93.6189
