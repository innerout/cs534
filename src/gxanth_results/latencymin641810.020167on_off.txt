BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.020167
OVERRIDE Parameter: routing_function=min
OVERRIDE Parameter: num_vcs=64
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 323.333
	minimum = 23
	maximum = 977
Network latency average = 255.112
	minimum = 23
	maximum = 894
Slowest packet = 3
Flit latency average = 174.187
	minimum = 6
	maximum = 952
Slowest flit = 3048
Fragmentation average = 167.254
	minimum = 0
	maximum = 732
Injected packet rate average = 0.0183646
	minimum = 0 (at node 45)
	maximum = 0.048 (at node 79)
Accepted packet rate average = 0.00891146
	minimum = 0.001 (at node 150)
	maximum = 0.017 (at node 73)
Injected flit rate average = 0.326729
	minimum = 0 (at node 45)
	maximum = 0.857 (at node 79)
Accepted flit rate average= 0.192094
	minimum = 0.054 (at node 41)
	maximum = 0.333 (at node 48)
Injected packet length average = 17.7913
Accepted packet length average = 21.5558
Total in-flight flits = 26604 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 526.585
	minimum = 23
	maximum = 1911
Network latency average = 416.862
	minimum = 23
	maximum = 1837
Slowest packet = 325
Flit latency average = 305.481
	minimum = 6
	maximum = 1894
Slowest flit = 5596
Fragmentation average = 231.38
	minimum = 0
	maximum = 1690
Injected packet rate average = 0.0165964
	minimum = 0.001 (at node 65)
	maximum = 0.0335 (at node 177)
Accepted packet rate average = 0.010362
	minimum = 0.0055 (at node 116)
	maximum = 0.0165 (at node 152)
Injected flit rate average = 0.296346
	minimum = 0.018 (at node 65)
	maximum = 0.6 (at node 177)
Accepted flit rate average= 0.203518
	minimum = 0.109 (at node 164)
	maximum = 0.31 (at node 152)
Injected packet length average = 17.8561
Accepted packet length average = 19.6409
Total in-flight flits = 36599 (0 measured)
latency change    = 0.385982
throughput change = 0.0561349
Class 0:
Packet latency average = 967.35
	minimum = 30
	maximum = 2944
Network latency average = 712.625
	minimum = 27
	maximum = 2893
Slowest packet = 395
Flit latency average = 586.626
	minimum = 6
	maximum = 2942
Slowest flit = 3544
Fragmentation average = 284.05
	minimum = 1
	maximum = 2732
Injected packet rate average = 0.0146979
	minimum = 0 (at node 4)
	maximum = 0.042 (at node 85)
Accepted packet rate average = 0.0120052
	minimum = 0.005 (at node 62)
	maximum = 0.024 (at node 16)
Injected flit rate average = 0.263536
	minimum = 0 (at node 4)
	maximum = 0.739 (at node 85)
Accepted flit rate average= 0.218193
	minimum = 0.1 (at node 163)
	maximum = 0.411 (at node 16)
Injected packet length average = 17.9302
Accepted packet length average = 18.1748
Total in-flight flits = 45592 (0 measured)
latency change    = 0.455642
throughput change = 0.0672547
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 808.08
	minimum = 33
	maximum = 3056
Network latency average = 308.202
	minimum = 29
	maximum = 918
Slowest packet = 9229
Flit latency average = 727.342
	minimum = 6
	maximum = 3651
Slowest flit = 21288
Fragmentation average = 154.655
	minimum = 2
	maximum = 632
Injected packet rate average = 0.0123854
	minimum = 0 (at node 5)
	maximum = 0.041 (at node 102)
Accepted packet rate average = 0.0121354
	minimum = 0.003 (at node 153)
	maximum = 0.024 (at node 151)
Injected flit rate average = 0.222646
	minimum = 0 (at node 5)
	maximum = 0.744 (at node 102)
Accepted flit rate average= 0.217776
	minimum = 0.077 (at node 153)
	maximum = 0.4 (at node 151)
Injected packet length average = 17.9765
Accepted packet length average = 17.9455
Total in-flight flits = 46619 (24853 measured)
latency change    = 0.197097
throughput change = 0.00191328
Class 0:
Packet latency average = 1109.5
	minimum = 32
	maximum = 4118
Network latency average = 490.712
	minimum = 28
	maximum = 1978
Slowest packet = 9229
Flit latency average = 808.31
	minimum = 6
	maximum = 4585
Slowest flit = 24273
Fragmentation average = 194.933
	minimum = 1
	maximum = 1513
Injected packet rate average = 0.0116094
	minimum = 0 (at node 5)
	maximum = 0.0345 (at node 98)
Accepted packet rate average = 0.0119661
	minimum = 0.006 (at node 180)
	maximum = 0.0185 (at node 70)
Injected flit rate average = 0.208349
	minimum = 0 (at node 5)
	maximum = 0.619 (at node 98)
Accepted flit rate average= 0.214471
	minimum = 0.1115 (at node 180)
	maximum = 0.325 (at node 70)
Injected packet length average = 17.9466
Accepted packet length average = 17.9232
Total in-flight flits = 43569 (31045 measured)
latency change    = 0.271673
throughput change = 0.0154085
Class 0:
Packet latency average = 1405.21
	minimum = 31
	maximum = 4977
Network latency average = 601.135
	minimum = 24
	maximum = 2956
Slowest packet = 9229
Flit latency average = 832.821
	minimum = 6
	maximum = 5653
Slowest flit = 20609
Fragmentation average = 210.878
	minimum = 1
	maximum = 2536
Injected packet rate average = 0.0118438
	minimum = 0 (at node 13)
	maximum = 0.0296667 (at node 98)
Accepted packet rate average = 0.0118872
	minimum = 0.00766667 (at node 116)
	maximum = 0.017 (at node 70)
Injected flit rate average = 0.212507
	minimum = 0 (at node 55)
	maximum = 0.534667 (at node 98)
Accepted flit rate average= 0.212684
	minimum = 0.133667 (at node 36)
	maximum = 0.3 (at node 70)
Injected packet length average = 17.9425
Accepted packet length average = 17.8919
Total in-flight flits = 45954 (37495 measured)
latency change    = 0.210436
throughput change = 0.00840367
Class 0:
Packet latency average = 1670.37
	minimum = 31
	maximum = 5779
Network latency average = 690.474
	minimum = 24
	maximum = 3880
Slowest packet = 9229
Flit latency average = 867.176
	minimum = 6
	maximum = 6732
Slowest flit = 9517
Fragmentation average = 226.371
	minimum = 0
	maximum = 3009
Injected packet rate average = 0.0117005
	minimum = 0 (at node 13)
	maximum = 0.02725 (at node 98)
Accepted packet rate average = 0.0117695
	minimum = 0.0075 (at node 36)
	maximum = 0.0155 (at node 152)
Injected flit rate average = 0.210313
	minimum = 0 (at node 61)
	maximum = 0.49025 (at node 98)
Accepted flit rate average= 0.211164
	minimum = 0.1335 (at node 36)
	maximum = 0.278 (at node 129)
Injected packet length average = 17.9746
Accepted packet length average = 17.9416
Total in-flight flits = 45238 (39315 measured)
latency change    = 0.158744
throughput change = 0.00719803
Class 0:
Packet latency average = 1904.41
	minimum = 31
	maximum = 6693
Network latency average = 765.393
	minimum = 24
	maximum = 4967
Slowest packet = 9229
Flit latency average = 905.009
	minimum = 6
	maximum = 7455
Slowest flit = 30743
Fragmentation average = 236.804
	minimum = 0
	maximum = 3822
Injected packet rate average = 0.0117229
	minimum = 0 (at node 61)
	maximum = 0.0242 (at node 98)
Accepted packet rate average = 0.0117479
	minimum = 0.0082 (at node 52)
	maximum = 0.0154 (at node 103)
Injected flit rate average = 0.210686
	minimum = 0 (at node 61)
	maximum = 0.4368 (at node 98)
Accepted flit rate average= 0.211269
	minimum = 0.146 (at node 42)
	maximum = 0.2774 (at node 129)
Injected packet length average = 17.9722
Accepted packet length average = 17.9835
Total in-flight flits = 45418 (41276 measured)
latency change    = 0.122895
throughput change = 0.000495518
Class 0:
Packet latency average = 2096.27
	minimum = 31
	maximum = 7546
Network latency average = 821.498
	minimum = 24
	maximum = 5740
Slowest packet = 9229
Flit latency average = 926.31
	minimum = 6
	maximum = 8370
Slowest flit = 35394
Fragmentation average = 245.969
	minimum = 0
	maximum = 5019
Injected packet rate average = 0.0116762
	minimum = 0 (at node 61)
	maximum = 0.0251667 (at node 109)
Accepted packet rate average = 0.0117361
	minimum = 0.00766667 (at node 64)
	maximum = 0.0151667 (at node 70)
Injected flit rate average = 0.209942
	minimum = 0 (at node 61)
	maximum = 0.454333 (at node 109)
Accepted flit rate average= 0.210853
	minimum = 0.142333 (at node 42)
	maximum = 0.277167 (at node 70)
Injected packet length average = 17.9803
Accepted packet length average = 17.9662
Total in-flight flits = 44915 (41857 measured)
latency change    = 0.0915229
throughput change = 0.00197033
Class 0:
Packet latency average = 2307.78
	minimum = 31
	maximum = 8510
Network latency average = 868.982
	minimum = 24
	maximum = 6718
Slowest packet = 9229
Flit latency average = 946.113
	minimum = 6
	maximum = 9345
Slowest flit = 35405
Fragmentation average = 253.857
	minimum = 0
	maximum = 5629
Injected packet rate average = 0.0116042
	minimum = 0 (at node 61)
	maximum = 0.0228571 (at node 62)
Accepted packet rate average = 0.0117188
	minimum = 0.00842857 (at node 42)
	maximum = 0.0151429 (at node 103)
Injected flit rate average = 0.208622
	minimum = 0.000714286 (at node 61)
	maximum = 0.410857 (at node 62)
Accepted flit rate average= 0.210641
	minimum = 0.151143 (at node 42)
	maximum = 0.268857 (at node 103)
Injected packet length average = 17.9782
Accepted packet length average = 17.9747
Total in-flight flits = 43362 (41078 measured)
latency change    = 0.0916494
throughput change = 0.00100612
Draining all recorded packets ...
Class 0:
Remaining flits: 9867 9868 9869 9870 9871 9872 9873 9874 9875 9876 [...] (43872 flits)
Measured flits: 168570 168571 168572 168573 168574 168575 168576 168577 168578 168579 [...] (38318 flits)
Class 0:
Remaining flits: 9867 9868 9869 9870 9871 9872 9873 9874 9875 9876 [...] (45552 flits)
Measured flits: 168570 168571 168572 168573 168574 168575 168576 168577 168578 168579 [...] (35488 flits)
Class 0:
Remaining flits: 9867 9868 9869 9870 9871 9872 9873 9874 9875 9876 [...] (48136 flits)
Measured flits: 168570 168571 168572 168573 168574 168575 168576 168577 168578 168579 [...] (33189 flits)
Class 0:
Remaining flits: 35874 35875 35876 35877 35878 35879 35880 35881 35882 35883 [...] (46561 flits)
Measured flits: 171180 171181 171182 171183 171184 171185 171186 171187 171188 171189 [...] (29868 flits)
Class 0:
Remaining flits: 35883 35884 35885 35886 35887 35888 35889 35890 35891 40015 [...] (51504 flits)
Measured flits: 171180 171181 171182 171183 171184 171185 171186 171187 171188 171189 [...] (28468 flits)
Class 0:
Remaining flits: 40015 40016 40017 40018 40019 40020 40021 40022 40023 40024 [...] (53396 flits)
Measured flits: 171180 171181 171182 171183 171184 171185 171186 171187 171188 171189 [...] (25818 flits)
Class 0:
Remaining flits: 40015 40016 40017 40018 40019 40020 40021 40022 40023 40024 [...] (51460 flits)
Measured flits: 171196 171197 173952 173953 173954 173955 173956 173957 173958 173959 [...] (22358 flits)
Class 0:
Remaining flits: 40015 40016 40017 40018 40019 40020 40021 40022 40023 40024 [...] (52796 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (20217 flits)
Class 0:
Remaining flits: 58158 58159 58160 58161 58162 58163 58164 58165 58166 58167 [...] (50321 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (16846 flits)
Class 0:
Remaining flits: 58158 58159 58160 58161 58162 58163 58164 58165 58166 58167 [...] (54277 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (15930 flits)
Class 0:
Remaining flits: 76056 76057 76058 76059 76060 76061 76062 76063 76064 76065 [...] (53250 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (14742 flits)
Class 0:
Remaining flits: 76056 76057 76058 76059 76060 76061 76062 76063 76064 76065 [...] (51597 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (11924 flits)
Class 0:
Remaining flits: 76056 76057 76058 76059 76060 76061 76062 76063 76064 76065 [...] (52291 flits)
Measured flits: 173952 173953 173954 173955 173956 173957 173958 173959 173960 173961 [...] (9320 flits)
Class 0:
Remaining flits: 94841 102816 102817 102818 102819 102820 102821 102822 102823 102824 [...] (54575 flits)
Measured flits: 214668 214669 214670 214671 214672 214673 214674 214675 214676 214677 [...] (8520 flits)
Class 0:
Remaining flits: 102816 102817 102818 102819 102820 102821 102822 102823 102824 102825 [...] (53333 flits)
Measured flits: 214668 214669 214670 214671 214672 214673 214674 214675 214676 214677 [...] (8193 flits)
Class 0:
Remaining flits: 102831 102832 102833 127584 127585 127586 127587 127588 127589 127590 [...] (53250 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (7693 flits)
Class 0:
Remaining flits: 127585 127586 127587 127588 127589 127590 127591 127592 127593 127594 [...] (52414 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (5953 flits)
Class 0:
Remaining flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (56416 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (5020 flits)
Class 0:
Remaining flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (59365 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (5331 flits)
Class 0:
Remaining flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (59469 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (4648 flits)
Class 0:
Remaining flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (57819 flits)
Measured flits: 214683 214684 214685 235044 235045 235046 235047 235048 235049 235050 [...] (3544 flits)
Class 0:
Remaining flits: 235046 235047 235048 235049 235050 235051 235052 235053 235054 235055 [...] (59300 flits)
Measured flits: 235046 235047 235048 235049 235050 235051 235052 235053 235054 235055 [...] (3100 flits)
Class 0:
Remaining flits: 235046 235047 235048 235049 235050 235051 235052 235053 235054 235055 [...] (59018 flits)
Measured flits: 235046 235047 235048 235049 235050 235051 235052 235053 235054 235055 [...] (2584 flits)
Class 0:
Remaining flits: 492637 492638 492639 492640 492641 619254 619255 619256 619257 619258 [...] (59755 flits)
Measured flits: 492637 492638 492639 492640 492641 619254 619255 619256 619257 619258 [...] (2341 flits)
Class 0:
Remaining flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (59410 flits)
Measured flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (2231 flits)
Class 0:
Remaining flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (57662 flits)
Measured flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (1719 flits)
Class 0:
Remaining flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (59106 flits)
Measured flits: 619254 619255 619256 619257 619258 619259 619260 619261 619262 619263 [...] (1312 flits)
Class 0:
Remaining flits: 619255 619256 619257 619258 619259 619260 619261 619262 619263 619264 [...] (60901 flits)
Measured flits: 619255 619256 619257 619258 619259 619260 619261 619262 619263 619264 [...] (1403 flits)
Class 0:
Remaining flits: 635396 635397 635398 635399 644479 644480 644481 644482 644483 644484 [...] (57844 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (1190 flits)
Class 0:
Remaining flits: 660400 660401 695535 695536 695537 718542 718543 718544 718545 718546 [...] (61698 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (1042 flits)
Class 0:
Remaining flits: 718542 718543 718544 718545 718546 718547 718548 718549 718550 718551 [...] (62262 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (940 flits)
Class 0:
Remaining flits: 718542 718543 718544 718545 718546 718547 718548 718549 718550 718551 [...] (60711 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (913 flits)
Class 0:
Remaining flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (61866 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (858 flits)
Class 0:
Remaining flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (62258 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (855 flits)
Class 0:
Remaining flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (62884 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (717 flits)
Class 0:
Remaining flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (61173 flits)
Measured flits: 737444 737445 737446 737447 737448 737449 737450 737451 737452 737453 [...] (417 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (61428 flits)
Measured flits: 1019250 1019251 1019252 1019253 1019254 1019255 1019256 1019257 1019258 1019259 [...] (338 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (63406 flits)
Measured flits: 1316952 1316953 1316954 1316955 1316956 1316957 1316958 1316959 1316960 1316961 [...] (234 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (63806 flits)
Measured flits: 1316952 1316953 1316954 1316955 1316956 1316957 1316958 1316959 1316960 1316961 [...] (196 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (66341 flits)
Measured flits: 1316952 1316953 1316954 1316955 1316956 1316957 1316958 1316959 1316960 1316961 [...] (136 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (64413 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (166 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (61673 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (57 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (62681 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (48 flits)
Class 0:
Remaining flits: 755172 755173 755174 755175 755176 755177 755178 755179 755180 755181 [...] (63396 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (39 flits)
Class 0:
Remaining flits: 1070676 1070677 1070678 1070679 1070680 1070681 1070682 1070683 1070684 1070685 [...] (62322 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1070676 1070677 1070678 1070679 1070680 1070681 1070682 1070683 1070684 1070685 [...] (61643 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (62023 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (62165 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (61237 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (60104 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (61400 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (36 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (63955 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (18 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (65795 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (18 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (66308 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (18 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (64532 flits)
Measured flits: 1741050 1741051 1741052 1741053 1741054 1741055 1741056 1741057 1741058 1741059 [...] (18 flits)
Draining remaining packets ...
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (41440 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1071090 1071091 1071092 1071093 1071094 1071095 1071096 1071097 1071098 1071099 [...] (30116 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1565275 1565276 1565277 1565278 1565279 1668636 1668637 1668638 1668639 1668640 [...] (19481 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1727208 1727209 1727210 1727211 1727212 1727213 1727214 1727215 1727216 1727217 [...] (10274 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1745604 1745605 1745606 1745607 1745608 1745609 1745610 1745611 1745612 1745613 [...] (4469 flits)
Measured flits: (0 flits)
Class 0:
Remaining flits: 1872000 1872001 1872002 1872003 1872004 1872005 1872006 1872007 1872008 1872009 [...] (1603 flits)
Measured flits: (0 flits)
Time taken is 72100 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 6665.27 (1 samples)
	minimum = 31 (1 samples)
	maximum = 55383 (1 samples)
Network latency average = 1331.69 (1 samples)
	minimum = 24 (1 samples)
	maximum = 29454 (1 samples)
Flit latency average = 1584.04 (1 samples)
	minimum = 6 (1 samples)
	maximum = 42219 (1 samples)
Fragmentation average = 308.717 (1 samples)
	minimum = 0 (1 samples)
	maximum = 19005 (1 samples)
Injected packet rate average = 0.0116042 (1 samples)
	minimum = 0 (1 samples)
	maximum = 0.0228571 (1 samples)
Accepted packet rate average = 0.0117188 (1 samples)
	minimum = 0.00842857 (1 samples)
	maximum = 0.0151429 (1 samples)
Injected flit rate average = 0.208622 (1 samples)
	minimum = 0.000714286 (1 samples)
	maximum = 0.410857 (1 samples)
Accepted flit rate average = 0.210641 (1 samples)
	minimum = 0.151143 (1 samples)
	maximum = 0.268857 (1 samples)
Injected packet size average = 17.9782 (1 samples)
Accepted packet size average = 17.9747 (1 samples)
Hops average = 5.04165 (1 samples)
Total run time 101.833
