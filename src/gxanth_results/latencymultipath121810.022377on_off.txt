BEGIN Configuration File: networks/mezzanine_config
topology = mezzanine;
n = 12;

routing_function = multipath;
internal_speedup = 2.0;
latency_thres = 100000.0;
use_read_write = 0;

sample_period = 1000;
injection_rate = 0.5;
packet_size = 18;

noq = 1;
routing_delay = 0;
vc_alloc_delay = 1;
sw_alloc_delay = 1;
st_final_delay = 1;
vc_allocator = islip; 
sw_allocator = islip;
alloc_iters  = 4;
num_vcs     = 12;
buffer_policy = private; // private or shared
buf_size = -1; // -1 for buffer_policy = private;
vc_buf_size = 4096; // size of each VOQ with buffer_policy = private
output_buffer_size = -1;


traffic = uniform;

//mezW = 0.0;
//mezPredefinedPatternType = shift-inter; //shift-qfdb

END Configuration File: networks/mezzanine_config
OVERRIDE Parameter: injection_rate=0.022377
OVERRIDE Parameter: routing_function=multipath
OVERRIDE Parameter: num_vcs=12
OVERRIDE Parameter: vc_buf_size=18
OVERRIDE Parameter: internal_speedup=1.0
OVERRIDE Parameter: injection_process=on_off
OVERRIDE Parameter: sim_type=latency
OVERRIDE Parameter: burst_alpha=0.0038461538
OVERRIDE Parameter: burst_beta=0.1
network: network_0
========================== Routing table  =====================
Number of Subnets: 1
Class 0:
Packet latency average = 336.349
	minimum = 23
	maximum = 987
Network latency average = 227.845
	minimum = 23
	maximum = 904
Slowest packet = 69
Flit latency average = 184.485
	minimum = 6
	maximum = 877
Slowest flit = 7307
Fragmentation average = 48.6845
	minimum = 0
	maximum = 150
Injected packet rate average = 0.012974
	minimum = 0 (at node 71)
	maximum = 0.03 (at node 105)
Accepted packet rate average = 0.00897917
	minimum = 0.002 (at node 41)
	maximum = 0.017 (at node 91)
Injected flit rate average = 0.229698
	minimum = 0 (at node 71)
	maximum = 0.54 (at node 105)
Accepted flit rate average= 0.166802
	minimum = 0.036 (at node 41)
	maximum = 0.306 (at node 91)
Injected packet length average = 17.7045
Accepted packet length average = 18.5766
Total in-flight flits = 13946 (0 measured)
latency change    = 1
throughput change = 1
Class 0:
Packet latency average = 580.111
	minimum = 23
	maximum = 1961
Network latency average = 318.777
	minimum = 23
	maximum = 1438
Slowest packet = 69
Flit latency average = 270.75
	minimum = 6
	maximum = 1389
Slowest flit = 19854
Fragmentation average = 52.2777
	minimum = 0
	maximum = 150
Injected packet rate average = 0.0112161
	minimum = 0 (at node 107)
	maximum = 0.022 (at node 6)
Accepted packet rate average = 0.00896354
	minimum = 0.005 (at node 30)
	maximum = 0.015 (at node 22)
Injected flit rate average = 0.199839
	minimum = 0 (at node 107)
	maximum = 0.3885 (at node 6)
Accepted flit rate average= 0.164263
	minimum = 0.09 (at node 135)
	maximum = 0.2735 (at node 22)
Injected packet length average = 17.817
Accepted packet length average = 18.3257
Total in-flight flits = 15997 (0 measured)
latency change    = 0.420199
throughput change = 0.0154573
Class 0:
Packet latency average = 1360.15
	minimum = 23
	maximum = 2845
Network latency average = 507.44
	minimum = 23
	maximum = 2173
Slowest packet = 3829
Flit latency average = 445.504
	minimum = 6
	maximum = 2151
Slowest flit = 37511
Fragmentation average = 58.3623
	minimum = 0
	maximum = 125
Injected packet rate average = 0.00920833
	minimum = 0 (at node 27)
	maximum = 0.024 (at node 133)
Accepted packet rate average = 0.00894271
	minimum = 0.002 (at node 121)
	maximum = 0.016 (at node 63)
Injected flit rate average = 0.166563
	minimum = 0 (at node 27)
	maximum = 0.424 (at node 133)
Accepted flit rate average= 0.161948
	minimum = 0.036 (at node 121)
	maximum = 0.298 (at node 73)
Injected packet length average = 18.0882
Accepted packet length average = 18.1095
Total in-flight flits = 17015 (0 measured)
latency change    = 0.573495
throughput change = 0.0142954
Warmed up ...Time used is 3000 cycles
Class 0:
Packet latency average = 1890.51
	minimum = 43
	maximum = 3846
Network latency average = 319.333
	minimum = 23
	maximum = 914
Slowest packet = 6207
Flit latency average = 457.72
	minimum = 6
	maximum = 2293
Slowest flit = 38501
Fragmentation average = 53.174
	minimum = 0
	maximum = 138
Injected packet rate average = 0.00932292
	minimum = 0 (at node 157)
	maximum = 0.026 (at node 64)
Accepted packet rate average = 0.00928125
	minimum = 0.004 (at node 61)
	maximum = 0.02 (at node 187)
Injected flit rate average = 0.167203
	minimum = 0.015 (at node 157)
	maximum = 0.475 (at node 64)
Accepted flit rate average= 0.166245
	minimum = 0.068 (at node 91)
	maximum = 0.36 (at node 187)
Injected packet length average = 17.9346
Accepted packet length average = 17.9119
Total in-flight flits = 17334 (16123 measured)
latency change    = 0.280538
throughput change = 0.0258467
Class 0:
Packet latency average = 2313.66
	minimum = 23
	maximum = 4815
Network latency average = 440.166
	minimum = 23
	maximum = 1768
Slowest packet = 6207
Flit latency average = 466.102
	minimum = 6
	maximum = 2375
Slowest flit = 76050
Fragmentation average = 55.5657
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00934115
	minimum = 0.0015 (at node 155)
	maximum = 0.021 (at node 64)
Accepted packet rate average = 0.00924219
	minimum = 0.004 (at node 180)
	maximum = 0.017 (at node 16)
Injected flit rate average = 0.167875
	minimum = 0.027 (at node 155)
	maximum = 0.3785 (at node 64)
Accepted flit rate average= 0.166318
	minimum = 0.074 (at node 180)
	maximum = 0.312 (at node 16)
Injected packet length average = 17.9716
Accepted packet length average = 17.9955
Total in-flight flits = 17841 (17805 measured)
latency change    = 0.182892
throughput change = 0.000438418
Class 0:
Packet latency average = 2629.45
	minimum = 23
	maximum = 5635
Network latency average = 478.401
	minimum = 23
	maximum = 2264
Slowest packet = 6207
Flit latency average = 467.446
	minimum = 6
	maximum = 2950
Slowest flit = 103121
Fragmentation average = 57.4446
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00927257
	minimum = 0.00266667 (at node 155)
	maximum = 0.016 (at node 64)
Accepted packet rate average = 0.00921007
	minimum = 0.005 (at node 4)
	maximum = 0.0163333 (at node 16)
Injected flit rate average = 0.16647
	minimum = 0.046 (at node 155)
	maximum = 0.290333 (at node 64)
Accepted flit rate average= 0.165425
	minimum = 0.09 (at node 4)
	maximum = 0.295333 (at node 16)
Injected packet length average = 17.953
Accepted packet length average = 17.9614
Total in-flight flits = 17706 (17706 measured)
latency change    = 0.120097
throughput change = 0.00539434
Class 0:
Packet latency average = 2951.94
	minimum = 23
	maximum = 6273
Network latency average = 497.707
	minimum = 23
	maximum = 2264
Slowest packet = 6207
Flit latency average = 471.851
	minimum = 6
	maximum = 2950
Slowest flit = 103121
Fragmentation average = 57.792
	minimum = 0
	maximum = 153
Injected packet rate average = 0.00916146
	minimum = 0.0025 (at node 108)
	maximum = 0.015 (at node 64)
Accepted packet rate average = 0.0091263
	minimum = 0.0045 (at node 4)
	maximum = 0.01375 (at node 16)
Injected flit rate average = 0.164783
	minimum = 0.048 (at node 108)
	maximum = 0.27175 (at node 64)
Accepted flit rate average= 0.1641
	minimum = 0.081 (at node 4)
	maximum = 0.2475 (at node 16)
Injected packet length average = 17.9865
Accepted packet length average = 17.981
Total in-flight flits = 17760 (17760 measured)
latency change    = 0.109245
throughput change = 0.00807486
Class 0:
Packet latency average = 3239.14
	minimum = 23
	maximum = 7125
Network latency average = 512.826
	minimum = 23
	maximum = 2560
Slowest packet = 6207
Flit latency average = 477.67
	minimum = 6
	maximum = 2950
Slowest flit = 103121
Fragmentation average = 58.0571
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00910833
	minimum = 0.0022 (at node 108)
	maximum = 0.0146 (at node 145)
Accepted packet rate average = 0.00907083
	minimum = 0.0054 (at node 4)
	maximum = 0.013 (at node 129)
Injected flit rate average = 0.163778
	minimum = 0.042 (at node 108)
	maximum = 0.261 (at node 145)
Accepted flit rate average= 0.163163
	minimum = 0.1006 (at node 4)
	maximum = 0.2332 (at node 129)
Injected packet length average = 17.9811
Accepted packet length average = 17.9876
Total in-flight flits = 17735 (17735 measured)
latency change    = 0.0886676
throughput change = 0.0057474
Class 0:
Packet latency average = 3541.49
	minimum = 23
	maximum = 8449
Network latency average = 520.699
	minimum = 23
	maximum = 2591
Slowest packet = 6207
Flit latency average = 480.521
	minimum = 6
	maximum = 2950
Slowest flit = 103121
Fragmentation average = 58.1515
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00905295
	minimum = 0.002 (at node 108)
	maximum = 0.0141667 (at node 145)
Accepted packet rate average = 0.00904774
	minimum = 0.00583333 (at node 36)
	maximum = 0.0128333 (at node 128)
Injected flit rate average = 0.162942
	minimum = 0.038 (at node 108)
	maximum = 0.254167 (at node 145)
Accepted flit rate average= 0.1627
	minimum = 0.104 (at node 36)
	maximum = 0.229667 (at node 128)
Injected packet length average = 17.9988
Accepted packet length average = 17.9823
Total in-flight flits = 17487 (17487 measured)
latency change    = 0.0853722
throughput change = 0.0028448
Class 0:
Packet latency average = 3841.71
	minimum = 23
	maximum = 9284
Network latency average = 526.707
	minimum = 23
	maximum = 2591
Slowest packet = 6207
Flit latency average = 482.312
	minimum = 6
	maximum = 2950
Slowest flit = 103121
Fragmentation average = 58.4707
	minimum = 0
	maximum = 166
Injected packet rate average = 0.00906324
	minimum = 0.00271429 (at node 108)
	maximum = 0.014 (at node 145)
Accepted packet rate average = 0.00900967
	minimum = 0.00614286 (at node 36)
	maximum = 0.0122857 (at node 128)
Injected flit rate average = 0.163069
	minimum = 0.0505714 (at node 108)
	maximum = 0.252143 (at node 145)
Accepted flit rate average= 0.162306
	minimum = 0.11 (at node 36)
	maximum = 0.220857 (at node 128)
Injected packet length average = 17.9924
Accepted packet length average = 18.0146
Total in-flight flits = 18008 (18008 measured)
latency change    = 0.0781479
throughput change = 0.00242659
Draining all recorded packets ...
Class 0:
Remaining flits: 297288 297289 297290 297291 297292 297293 297294 297295 297296 297297 [...] (18105 flits)
Measured flits: 297288 297289 297290 297291 297292 297293 297294 297295 297296 297297 [...] (18105 flits)
Class 0:
Remaining flits: 329378 329379 329380 329381 333504 333505 333506 333507 333508 333509 [...] (17422 flits)
Measured flits: 329378 329379 329380 329381 333504 333505 333506 333507 333508 333509 [...] (17422 flits)
Class 0:
Remaining flits: 355901 355902 355903 355904 355905 355906 355907 355908 355909 355910 [...] (18114 flits)
Measured flits: 355901 355902 355903 355904 355905 355906 355907 355908 355909 355910 [...] (18078 flits)
Class 0:
Remaining flits: 383562 383563 383564 383565 383566 383567 383568 383569 383570 383571 [...] (17684 flits)
Measured flits: 383562 383563 383564 383565 383566 383567 383568 383569 383570 383571 [...] (17327 flits)
Class 0:
Remaining flits: 413604 413605 413606 413607 413608 413609 413610 413611 413612 413613 [...] (17544 flits)
Measured flits: 413604 413605 413606 413607 413608 413609 413610 413611 413612 413613 [...] (16749 flits)
Class 0:
Remaining flits: 450116 450117 450118 450119 450120 450121 450122 450123 450124 450125 [...] (17684 flits)
Measured flits: 450116 450117 450118 450119 450120 450121 450122 450123 450124 450125 [...] (16317 flits)
Class 0:
Remaining flits: 457103 457104 457105 457106 457107 457108 457109 465065 468342 468343 [...] (17638 flits)
Measured flits: 457103 457104 457105 457106 457107 457108 457109 465065 468342 468343 [...] (15674 flits)
Class 0:
Remaining flits: 511380 511381 511382 511383 511384 511385 511386 511387 511388 511389 [...] (18123 flits)
Measured flits: 511380 511381 511382 511383 511384 511385 511386 511387 511388 511389 [...] (15864 flits)
Class 0:
Remaining flits: 540036 540037 540038 540039 540040 540041 540042 540043 540044 540045 [...] (17601 flits)
Measured flits: 540036 540037 540038 540039 540040 540041 540042 540043 540044 540045 [...] (14656 flits)
Class 0:
Remaining flits: 571554 571555 571556 571557 571558 571559 571560 571561 571562 571563 [...] (17532 flits)
Measured flits: 583738 583739 589518 589519 589520 589521 589522 589523 589524 589525 [...] (13918 flits)
Class 0:
Remaining flits: 602215 602216 602217 602218 602219 602220 602221 602222 602223 602224 [...] (17708 flits)
Measured flits: 602215 602216 602217 602218 602219 602220 602221 602222 602223 602224 [...] (13166 flits)
Class 0:
Remaining flits: 625842 625843 625844 625845 625846 625847 625848 625849 625850 625851 [...] (18075 flits)
Measured flits: 626436 626437 626438 626439 626440 626441 626442 626443 626444 626445 [...] (11963 flits)
Class 0:
Remaining flits: 634032 634033 634034 634035 634036 634037 634038 634039 634040 634041 [...] (18096 flits)
Measured flits: 634032 634033 634034 634035 634036 634037 634038 634039 634040 634041 [...] (10839 flits)
Class 0:
Remaining flits: 689108 689109 689110 689111 692334 692335 692336 692337 692338 692339 [...] (18095 flits)
Measured flits: 689108 689109 689110 689111 698364 698365 698366 698367 698368 698369 [...] (9136 flits)
Class 0:
Remaining flits: 712890 712891 712892 712893 712894 712895 712896 712897 712898 712899 [...] (17681 flits)
Measured flits: 712890 712891 712892 712893 712894 712895 712896 712897 712898 712899 [...] (8078 flits)
Class 0:
Remaining flits: 732330 732331 732332 732333 732334 732335 732336 732337 732338 732339 [...] (18003 flits)
Measured flits: 732330 732331 732332 732333 732334 732335 732336 732337 732338 732339 [...] (7163 flits)
Class 0:
Remaining flits: 779942 779943 779944 779945 779946 779947 779948 779949 779950 779951 [...] (17554 flits)
Measured flits: 784980 784981 784982 784983 784984 784985 784986 784987 784988 784989 [...] (6265 flits)
Class 0:
Remaining flits: 806148 806149 806150 806151 806152 806153 806154 806155 806156 806157 [...] (17737 flits)
Measured flits: 824002 824003 835020 835021 835022 835023 835024 835025 835026 835027 [...] (5284 flits)
Class 0:
Remaining flits: 839826 839827 839828 839829 839830 839831 839832 839833 839834 839835 [...] (17725 flits)
Measured flits: 842454 842455 842456 842457 842458 842459 842460 842461 842462 842463 [...] (4392 flits)
Class 0:
Remaining flits: 863838 863839 863840 863841 863842 863843 863844 863845 863846 863847 [...] (17558 flits)
Measured flits: 863838 863839 863840 863841 863842 863843 863844 863845 863846 863847 [...] (3575 flits)
Class 0:
Remaining flits: 900343 900344 900345 900346 900347 900348 900349 900350 900351 900352 [...] (17890 flits)
Measured flits: 926614 926615 926616 926617 926618 926619 926620 926621 931863 931864 [...] (3413 flits)
Class 0:
Remaining flits: 927054 927055 927056 927057 927058 927059 927060 927061 927062 927063 [...] (17392 flits)
Measured flits: 949248 949249 949250 949251 949252 949253 949254 949255 949256 949257 [...] (2588 flits)
Class 0:
Remaining flits: 965448 965449 965450 965451 965452 965453 965454 965455 965456 965457 [...] (17912 flits)
Measured flits: 973512 973513 973514 973515 973516 973517 973518 973519 973520 973521 [...] (1874 flits)
Class 0:
Remaining flits: 994713 994714 994715 998802 998803 998804 998805 998806 998807 998808 [...] (17890 flits)
Measured flits: 998802 998803 998804 998805 998806 998807 998808 998809 998810 998811 [...] (1457 flits)
Class 0:
Remaining flits: 1033416 1033417 1033418 1033419 1033420 1033421 1033422 1033423 1033424 1033425 [...] (17595 flits)
Measured flits: 1052694 1052695 1052696 1052697 1052698 1052699 1052700 1052701 1052702 1052703 [...] (1166 flits)
Class 0:
Remaining flits: 1066269 1066270 1066271 1066272 1066273 1066274 1066275 1066276 1066277 1066278 [...] (17396 flits)
Measured flits: 1100786 1100787 1100788 1100789 1102842 1102843 1102844 1102845 1102846 1102847 [...] (827 flits)
Class 0:
Remaining flits: 1095792 1095793 1095794 1095795 1095796 1095797 1095798 1095799 1095800 1095801 [...] (17838 flits)
Measured flits: 1123560 1123561 1123562 1123563 1123564 1123565 1123566 1123567 1123568 1123569 [...] (516 flits)
Class 0:
Remaining flits: 1106734 1106735 1106736 1106737 1106738 1106739 1106740 1106741 1106742 1106743 [...] (17565 flits)
Measured flits: 1152828 1152829 1152830 1152831 1152832 1152833 1152834 1152835 1152836 1152837 [...] (316 flits)
Class 0:
Remaining flits: 1162872 1162873 1162874 1162875 1162876 1162877 1162878 1162879 1162880 1162881 [...] (17608 flits)
Measured flits: 1194710 1194711 1194712 1194713 1208844 1208845 1208846 1208847 1208848 1208849 [...] (274 flits)
Class 0:
Remaining flits: 1189674 1189675 1189676 1189677 1189678 1189679 1189680 1189681 1189682 1189683 [...] (17880 flits)
Measured flits: 1232262 1232263 1232264 1232265 1232266 1232267 1232268 1232269 1232270 1232271 [...] (169 flits)
Class 0:
Remaining flits: 1204485 1204486 1204487 1215828 1215829 1215830 1215831 1215832 1215833 1215834 [...] (17613 flits)
Measured flits: 1279818 1279819 1279820 1279821 1279822 1279823 1279824 1279825 1279826 1279827 [...] (36 flits)
Class 0:
Remaining flits: 1252241 1253031 1253032 1253033 1260360 1260361 1260362 1260363 1260364 1260365 [...] (17717 flits)
Measured flits: 1310202 1310203 1310204 1310205 1310206 1310207 1310208 1310209 1310210 1310211 [...] (36 flits)
Class 0:
Remaining flits: 1268118 1268119 1268120 1268121 1268122 1268123 1268124 1268125 1268126 1268127 [...] (17719 flits)
Measured flits: 1310724 1310725 1310726 1310727 1310728 1310729 1310730 1310731 1310732 1310733 [...] (36 flits)
Class 0:
Remaining flits: 1284858 1284859 1284860 1284861 1284862 1284863 1284864 1284865 1284866 1284867 [...] (17801 flits)
Measured flits: 1310724 1310725 1310726 1310727 1310728 1310729 1310730 1310731 1310732 1310733 [...] (36 flits)
Class 0:
Remaining flits: 1317717 1317718 1317719 1317720 1317721 1317722 1317723 1317724 1317725 1330982 [...] (17686 flits)
Measured flits: 1403081 1404018 1404019 1404020 1404021 1404022 1404023 1404024 1404025 1404026 [...] (37 flits)
Draining remaining packets ...
Time taken is 46291 cycles
====== Overall Traffic Statistics ======
====== Traffic class 0 ======
Packet latency average = 9514.5 (1 samples)
	minimum = 23 (1 samples)
	maximum = 35625 (1 samples)
Network latency average = 549.874 (1 samples)
	minimum = 23 (1 samples)
	maximum = 3271 (1 samples)
Flit latency average = 490.377 (1 samples)
	minimum = 6 (1 samples)
	maximum = 3222 (1 samples)
Fragmentation average = 58.7117 (1 samples)
	minimum = 0 (1 samples)
	maximum = 175 (1 samples)
Injected packet rate average = 0.00906324 (1 samples)
	minimum = 0.00271429 (1 samples)
	maximum = 0.014 (1 samples)
Accepted packet rate average = 0.00900967 (1 samples)
	minimum = 0.00614286 (1 samples)
	maximum = 0.0122857 (1 samples)
Injected flit rate average = 0.163069 (1 samples)
	minimum = 0.0505714 (1 samples)
	maximum = 0.252143 (1 samples)
Accepted flit rate average = 0.162306 (1 samples)
	minimum = 0.11 (1 samples)
	maximum = 0.220857 (1 samples)
Injected packet size average = 17.9924 (1 samples)
Accepted packet size average = 18.0146 (1 samples)
Hops average = 5.06776 (1 samples)
Total run time 33.7729
