1, 2, 3, 4 is the mezzanine number, A, B, C, D are the QFDBs of each mezzanine and 1 is the F1 FPGA of a QFDB and 2, 3, 4 th other FPGAs of the QFDB. (examples: 1A1 = A1, 2B1, 12C4)

1. maximum-channel inter-mezzanine communication of two QFDBs
A1 -> B1, A2 -> B2, A3 -> B3, A4 -> B4

2. fully utilized inter-mezzanine communication
A1 -> B1, A2 -> B2, A3 -> B3, A4 -> B4
C1 -> D1, C2 -> D2, C3 -> D3, C4 -> D4

3. maximum inter-QFDB communication
A1 -> A2, A3 -> A4
(B1 -> B2, B3 -> B4)

4. combination of inter-QFDB communication and single-channel inter-mezzanine communication
A1 -> A2, A3 -> B3, B1 -> B2

5. combination of inter-QFDB communication and double-channel inter-mezzanine communication
A1 -> A2, A3 -> B3, A4 -> B4, B1 -> B2

6. fully utilized linearly sparable communication between all FPGAs
1A1 -> 14A1, 1A2 -> 12A2, ..., 1A4 -> 12A4
1B1 -> 14B1, ..., 1C1 -> 12C1, ..., 1D1 -> 12D1
2A1 -> 11A1, ..., 3A1 -> 10A1, ..., 4A1 -> 9A1, ..., 6A1 -> 7A1

7. unique randomized fully utilized communication
1A1 -> X, 1A2 -> Y != X, ..., 6D4 -> Z != {...}

8. randomized fully utilized communication
1A1 -> X, 1A2 -> Y, ..., 12D4 -> Z

9. unique randomized fully utilized single-channel communication between pairs of QFDBs
1A1 -> X, 1B1 -> Y != X, ..., 6D1 -> Z != {...}