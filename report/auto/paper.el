(TeX-add-style-hook
 "paper"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "letterpaper" "twocolumn" "10pt")))
   (TeX-run-style-hooks
    "latex2e"
    "report1"
    "report2"
    "appendix"
    "article"
    "art10"
    "usenix2019_v3"
    "tikz"
    "amsmath"
    "graphicx"
    "filecontents"
    "color"
    "soul"))
 :latex)

