\graphicspath{ {./figures/gxanth_plots/} }

\section{Description \& Implementation}


\subsection{Topology Description}
In this section, we describe the topology in detail and the entities
it consists of. The core of our network topology is the four spine
switches connecting the Mezzanine boards with 10 Gbps links. Each
Mezzanine board contains four Quad-FPGA DaughterBoards ("QFDBs"), A,
B, C, and D, connected in all-to-all fashion with 10 Gbps links.
Furthermore, a QFDB consists of four FPGAs, F1, F2, F3, and F4,
connected in such a way that every FPGA is connected with every other
FPGA in the same QFDB board with 16 Gbps links.

As we mentioned above, for the four QFDBs inside a Mezzanine to
communicate with each other, they use a 10 Gbps link. This connection
is placed on the F1's network interface for each QFDB. Thus, if an
FPGA, except for F1, wants to communicate with a remote destination
inside the same Mezzanine, it has to route its traffic to the F1, that
is directly connected with, and the F1 will route it to the remote
QFDB.

Finally, we use the spine links for intra-Mezzanine communication.
These links connect spine switches with every F1 FPGA in the network
in such a way that the first spine switch connects with the A QFDB of
every Mezzanine, the second spine switch connects with the B QFDB of
every Mezzanine etc.

\subsection{Implementation in Booksim}
The RDMA network topology we implement in Booksim is almost identical
to the described topology. First of all, there are 2 core elements in
Booksim to model a network, nodes, and routers. In our design, the
number of FPGAs in a QFDB and the number of QFDBs in a Mezzanine are
parameterized values alongside the number of Mezzanines in the
network. The only assumption we make is that all the links in our
network have 10 Gbps capacity, compared to the description that there
are links with 16 Gbps (Intra-QFDB) and 18 Gbps (node to the network
interface) capacities. The main reason for this assumption is that the
Booksim Simulator does not support such configuration or at least we
couldn't find any.

\subsubsection{Nodes or Traffic Generators}
When an entity in Booksim is modeled as node it is a traffic
generator. We model the main memory of each FPGA board as a node, with
the ID of the F1 of the A QFDB of the first Mezzanine to be 0. Each
node in the system has a unique ID. These IDs help us to compose the
source-destination pairs to identify the different traffic flows and
route them properly.

\subsubsection{Switch Elements}
The other main entity of Booksim is the router, which is responsible
to receive the incoming packets of the generated flows and to forward
them to the appropriate destination according to selected routing
policy. The network interface that is directly connected to each node
is implemented as a router as well as all the other switch elements in
the topology. The network interfaces connect the FPGA directly with
its neighbors (the other three FPGAs inside QFDB). Each F1 FPGA in the
system has four additional links that allow it to communicate with
remote destinations, outside of QFDB. The first additional link
connects the F1 to one of the four spine switches. The other three
additional links connect the F1 to the other three QFDBs in the same
Mezzanine. The spine switches are also modeled as routers connecting
QFDBs across Mezzanines and they cannot be the source of traffic
generation. We assume cut-through operation for all routers but this
is configurable by setting properly the "routing\_delay" variable in
the Booksim configuration file.

\subsection{Routing Policies}
In this section, we describe the routing policies that exist in
Booksim. The default policy that Booksim provides is single-path.
Booksim uses a map to implement the source -> destinations links. The
keys are the source nodes and the values are the destinations. For
each source, Djikstra's algorithm is run and it calculates the
shortest path for each destination iteratively inserting them as
values to the appropriate source.

For our multi-path implementation, we extend the map's value field to
hold a vector instead of holding a single value. In the first slot, we
insert Djikstra's shortest path while in the remaining slots, we
insert the neighbors (intra-mezzanine QFDBs) that the source node is
connected with. When we transmit a packet from its source, we pick
randomly a slot from the value vector and reroute it via random
destination.

\subsection{Traffic Patterns}
The combinations of the traffic patterns we implement are numerous.
First, we choose if the destination is fixed. If fixed is enabled then
each source will generate traffic only for one destination. You can
also choose to include in the possible destinations the source itself.
Further and combined with above the traffic patterns we implement are


combinations of the following values: are the following:
\begin{itemize}
        \item \textit{fixed}: (default: true), except if <random-X> included): If true
                the initially calculated preferred destination will be the source
                throughout the simulation (does only affect the preferred destination).
                Otherwise the preferred destination will be recalculated every time
                a node wants to send a packet.
        \item \textit{distinct} (default: false), If true then the preferred destination of
                a node cannot be itself (for example if using <random-X>). This
                does only affect the preferred destination.
        \item \textit{shiftTargetArea} variable that can take nine different
                values (default: none):
        \begin{enumerate}
                \item first: all nodes that are not in the <random-X> will have their
                        preferred destination set to node 0 - the rest of the nodes
                        will have themselves as the preferred destination.
                \item mirror: The first node sends to the last node, the
                        second node sends to the penultimate, etc.
                \item inta: intra-QFDB traffic pattern.
                \item qfdb: intra-mezzanine traffic pattern.
                \item mez: each node of a mezzanine sends traffic to the same node
                        of the next mezzanine.
                \item qfdbmez: each node of a mezzanine sends traffic to the same node
                        of the next mezzanine shifted by 4, i.e. the nodes of A QFDB in a
                        mezzanine send traffic to the nodes of B QFDB of the next mezzanine,
                        the nodes of D QFDB in a mezzanine send traffic to the nodes of A
                        QFDB of the next mezzanine, etc.
                \item custom1: only the A QFDB of the first mezzanine generates
                        traffic in the whole system and sends it to the A QFDB of the second
                        mezzanine.
                \item custom2: only the A QFDB of the first mezzanine generates
                        traffic in the whole system and sends it to the next mezzanine
                        selecting nodes randomly (except if using \textit{fixed}).
                \item none: the source node is also the destination node.
        \end{enumerate}
        \item \textit{randomRange} the current preferred destination will
                get shuffled in the given entity (default: none), where:
        \begin{enumerate}
                \item none: the source node is also the destination node. Only
                        available with \textit{distinct} disabled.
                \item qfdb: the current preferred destination will be set to a
                        random node that lies in the same QFDB as the current
                        preferred destination.
                \item mez: the current preferred destination will be set to a
                        random node that lies in the same mezzanine as the current
                        preferred destination.
                \item all: the current preferred destination will be set to a
                        random node of any mezzanine.
        \end{enumerate}
        \item \textit{altRandomArea} (default: all) sets the random area
                that a node's destination can be set when it is not the
                preferred one, where:
        \begin{enumerate}
                \item qfdb: in case the node has to send a packet, but not to
                        the preferred destination, it will be sent to a random
                        node in the same QFDB (can possibly be the same as source).
                \item mez: in case the node has to send a packet, but not to
                        the preferred destination, it will be sent to a random
                        node in the same mezzanine (can possibly be the same as source).
                \item all:in case the node has to send a packet, but not to the
                        preferred destination, it will be sent to a random node in
                        any mezzanine (can possibly be the same as source).
        \end{enumerate}
        \item \textit{mezW} is the parameter that decides the probability
                of whether a packet will be sent to its preferred destination
                or to a random destination. \textit{mezW} \% of the generated
                traffic will be sent to the preferred destination, the rest
                to a random destination (as defined by \textit{altRandomArea}).
\end{itemize}


\subsection{Implementation Difficulties}
The first difficulty we encountered in the project was the
introduction of multi-path routing in Booksim because the simulator
had no notion of multi-path support. To implement multi-path
we tried to use as much code possible Booksim had for us to
be able to use the existing features of the simulator later for
measurements. To implement multi-path routing we use the routing-table
of the simulator and use a naive algorithm for multi-path routing.
When a packet has not left its QFDB yet we pick a random neighbor to
send the packet. A neighbor can either be a node in the same mezzanine
or a spine switch. If the packet is broken in flits, each flit will
follow the same path as the head flit. We choose to randomize only on
the source of the packet due to its simplicity in the implementation.
\section{Simulations - Evaluation}
The experiments we run to measure the performance of our
implementation in Booksim are throughput and latency measurements. For
each experiment, we vary the number of input channels, channel buffer
size, and internal speedup. We evaluate the minimum path and
multi-path routing policies using Bernoulli and bursty injection
processes under uniform traffic patterns.

\subsection{Bernoulli}
In this section, we present the results of the simulations for Bernoulli injection process.
Figures \ref{fig:latv12sp1b},\ref{fig:latv12sp2b} show the latency improvement when internal speedup 2, for buffer size 18 the maximum average latency improves 40\% and
for buffer size 144 it improves 32\%.
Figures \ref{fig:latv32sp1b}, \ref{fig:latv32sp2b} show for buffer size 18 and internal speedup 2 the network tolerates more load.
Figures \ref{fig:latv64sp1b},\ref{fig:latv64sp2b} show that 4 iterations of iSlip cannot take advantage of the 64 virtual input channels and only exploits a small number of them.
In advance the network with internal speedup tolerates 7\% more load.

Figures \ref{fig:thrv12sp1b},\ref{fig:thrv12sp2b} show that for internal speedup 2, throughput improves by 30\%.
Figures \ref{fig:thrv32sp1b},\ref{fig:thrv32sp2b} show that for internal speedup 2, throughput improves by 28\%.
Figures \ref{fig:thrv64sp1b},\ref{fig:thrv64sp2b} show that for internal speedup 2, throughput improves by 20\%.

\subsection{Bursty}
In this section, we present the results of the simulations for the bursty injection process.
Figures \ref{fig:latv12sp1o}, \ref{fig:latv12sp2o}  show the latency improvement when internal speedup 2, for buffer size 18 the maximum average latency improves 40\% and
for buffer size 144 it improves 26\%.
Figures \ref{fig:latv32sp1o}, \ref{fig:latv32sp2o} show for buffer size 18 and internal speedup 2 the network tolerates more load.
Figures \ref{fig:latv64sp1o}, \ref{fig:latv64sp2o} show that 4 iterations of iSlip cannot take advantage of the 64 virtual input channels and only exploits a small number of them.
In advance the network with internal speedup tolerates 10\% more load.

Figures \ref{fig:thrv12sp1o},\ref{fig:thrv12sp2o} show that for internal speedup 2, throughput improves 30\% based on the buffer size.
Figures \ref{fig:thrv32sp1o},\ref{fig:thrv32sp2o} show that for internal speedup 2, throughput improves by 28\% based on the buffer size.
Figures \ref{fig:thrv64sp1o},\ref{fig:thrv64sp2o} show that for internal speedup 2, throughput improves by 20\%.

\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs12internal_speedup1bernoulli}
\caption{}
\label{fig:latv12sp1b}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs32internal_speedup1bernoulli}
\caption{}
\label{fig:latv32sp1b}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs64internal_speedup1bernoulli}
\caption{}
\label{fig:latv64sp1b}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs12internal_speedup2bernoulli}
\caption{}
\label{fig:latv12sp2b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs32internal_speedup2bernoulli}
\caption{}
\label{fig:latv32sp2b}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs64internal_speedup2bernoulli}
\caption{}
\label{fig:latv64sp2b}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs12internal_speedup1bernoulli}
\caption{}
\label{fig:thrv12sp1b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs32internal_speedup1bernoulli}
\caption{}
\label{fig:thrv32sp1b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs64internal_speedup1bernoulli}
\caption{}
\label{fig:thrv64sp1b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs12internal_speedup2bernoulli}
\caption{}
\label{fig:thrv12sp2b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs32internal_speedup2bernoulli}
\caption{}
\label{fig:thrv32sp2b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs64internal_speedup2bernoulli}
\caption{}
\label{fig:thrv64sp2b}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs12internal_speedup1on_off}
\caption{}
\label{fig:latv12sp1o}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs32internal_speedup1on_off}
\caption{}
\label{fig:latv32sp1o}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs64internal_speedup1on_off}
\caption{}
\label{fig:latv64sp1o}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs12internal_speedup2on_off}
\caption{}
\label{fig:latv12sp2o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs32internal_speedup2on_off}
\caption{}
\label{fig:latv32sp2o}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.5]{latencynum_vcs64internal_speedup2on_off}
\caption{}
\label{fig:latv64sp2o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs12internal_speedup1on_off}
\caption{}
\label{fig:thrv12sp1o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs32internal_speedup1on_off}
\caption{}
\label{fig:thrv32sp1o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs64internal_speedup1on_off}
\caption{}
\label{fig:thrv64sp1o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs12internal_speedup2on_off}
\caption{}
\label{fig:thrv12sp2o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs32internal_speedup2on_off}
\caption{}
\label{fig:thrv32sp2o}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{throughputnum_vcs64internal_speedup2on_off}
\caption{}
\label{fig:thrv64sp2o}
\end{figure}


\section{Work Coordination}
To coordinate our effort we identified the strong points each one has
and used them to complete the project. Xenofon and Giorgos implemented
the multi-path routing mechanism as it was the hardest part of the
project and the most time-consuming. Xenofon started by implementing
the RDMA topology, while Giorgos was searching how different parts of
the code interact. When Xenofon finished the RDMA topology, Xenofon
and Giorgos discussed their findings and they proceeded with the
multi-path implementation. Around that time Anthimos started
implementing the traffic patterns with the unbalanced factor but on
the way he implemented traffic patterns that were not connected
with the unbalanced factor w mentioned in 2.2 . After a couple of
days, Xenofon and Giorgos finished the multi-path mechanism and they
were ready to start the simulations for the final report. To
parallelize our effort Xenofon started working the first part of the
project with the theoretical analysis while Giorgos concurrently
prepared the scripts that ran all the simulations. To complete our
project it took around 160 hours as the simulations were long-running
and the coordination was difficult sometimes. From the 160 hours, we
spent 20 hours to write the report.
